![PortalPages](PortalPages.png)
#OPortalPage
Holds FABS pages metadata.
## Fields
### name
page name
### layoutTemplate
UDC represents how portlets in page are arranged
### header
Page header
#PortalPageFunction
Function to open the page

#PortalPagePortlet
Represents the portlet (the screen container) in the page
##Fields
###main
first portlet to be drown in the page, all other portlets receive their inputs from it either directly or indirectly

###position
Will be placed in which position in the column
###column
will be placed in which column in the page
### receivers
portlets in which wait input from this portlet

#PortalPagePortletReceiver
Holds infromation about receiver and sender portlets