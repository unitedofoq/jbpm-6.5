![Screens Diagram](Screen.png)
#OScreen
holds the common information for all screen types
## Fields
### printable
Allow this screen to be printed
### inlineHelp
shows screen functionality, it is provided by user
### name
screen identifier

### header
screen header

### viewPage
determine tha **xhtml** for the current screen

### dataLoadingUserExit
 Data Loading User Exit Java Function, is called when loading data so we can customize the loading conditions.
 Sample function is found in UIFrameworkService.sampleScreenDataLoadingUserExit(ODataMessage, OFunctionParms, OUser)}
### showHeader
Show screen header if true
### showActiveOnly
hide soft deleted rows if true
#ChartScreen
for displaying charts, extend OScreen directly
## Fields
### width
width of displayed chart
### height
height of displayed chart
### chartType
type of chart (pie, bar,...)
### yaxisTitle
### xaxisTitle
They are dds to support i18n
### aggregateFunction
in  which you calculate your legends
### dataFilter
condition to filter data
###  yfilter
condition to filter y axis
###  xfilter
condition to filter x axis
###  dim3Filterfilter
condition to filter dimension legends
# ChartScreenDim3Field
Holds legends data
##Fields
### dim3FieldExpression
# SingleEntityScreen
Holds information for screens that are working on one entity
## Fields
### editable
Are controls that contain data enabled in the screen
### removable
Is deleting data allowed in the screen

### saveAndExit
Close the screen after saving

### save
create Save button for the screen

#TabularScreen
Holds metadata for tabular screens
## Fields
### sheet
Is tabular is excel like

### fastTrack
Can we add new row

### sideBalloon
Will each row holds a side balloon for the entity general actions

### duplicate
Allow duplicate row functionality

### recordsInPage
Number of rows per page, default is 7

### sortExpression
field expression in the screen entity in which the data will be sorted by

### basic
Will this screen support add Basic Detail screen function (which is form) to add more details

#FormScreen
Holds metadata for form screen
## Fields
### columnsNo
How many logical columns the screen will be divided into
### basicDetail
Is this screen will be a detail for a tabular screen, the both must be of the same act on entity
#FABSSetupScreen
Holds the setup data for FABS and Business app that are built on top of it
#FABSSetupScreenKey
#GaugeScreen
Holds metadata for gauge charts only
## Fields
###label
Gauge chart secions labels
### gheight
height of chart
### width
width of chart
### gvalueFieldExp
the field expression of the legend
#GaugeScreenValue
Holds the values and colors of Gauge legends
#M2MRelationScreen
(Mostafa)
#MixScreen
(OTMS Screen)
#MultiLevelScreen
(deprecated, replaced by Trees)
#MultiLevelScreenLevel
(deprecated)
#MultiListScreen
(deprecated)
#OPEyeExaminationScreen
(it was made for demo purpose only)
#ScreenAction
Holds metadata for actions on screen
##Fields
### button
Displays the action as a button in the screen
### render
Render the toBeRendered screen in C2A mode after action runs successfully
### buttonImage
Image for button, if any
# ScreenDTMapping
(Mostafa)
# ScreenDTMappingAttr
(Mostafa)
#ScreenField
Holds the metadata for the screen fields
##Fields
###screenFieldGroup
If any field in the screen belongs to a group, so must the other fields.
### changeFunction
holds a java function to be executed if the value in the field has been changed
### fieldExpression
the field name in the code
### editable
Can I edit that field
### sortIndex
Its order in the screen
### mandatory
Does not need to be mandatory in db too.
###controlSize
### ddTitleOverride
override the title in the associated dd label/header
### ScreenFieldGroup
Holds metadata for screens divisions
##Fields
### expandable
Is it initially opened
### header
title of the group
### numberOfColumns
how many columns in the group
### sortIndex
Index of this group among others groups
#ScreenFilter
(mostafa)
#ScreenFunction
function responsible for opening screens
## Fields
### screenMode
(not used really)
### runInPopup
if true, the screen will be open in pop otherwise it will be opened in the opened page.
