# Entity Base
This package contains the base entities that most of FABS objects and builders extend from. 
## BaseEntity
### Functionality
All FABS and Business Entities must extend this mapped super class, as almost all FABS services dealing with this entity only.
### General Fields
<table>
	<tr>
		<th>Field</th>
		<th>Type</th>
		<th>Description</th>
		<th>Notes</th>
	</tr>
	<tr>
		<td>instID</td>
		<td>long</td>
		<td>counter for the current created an extend class instance</td>
		<td>It is a transient field</td>
	</tr>
<tr>
		<td>changed</td>
		<td>boolean</td>
		<td>when the bases entity is changed in the screen it will be set to true</td>
		<td>It is a transient field</td>
	</tr>
<tr>
		<td>encyrpted</td>
		<td>boolean</td>
		<td>indicates if there is an encrypted data in the associated row</td>
		<td>It is a transient field</td>
	</tr>
<tr>
		<td>dbid</td>
		<td>long</td>
		<td>dbid for all sub entities</td>
		<td>get its value from DBIDGenerationService</td>
	</tr>
<tr>
		<td>cc1</td>
		<td>UDC</td>
		<td>if user needs to ad extera business lookup data</td>
		<td></td>
	</tr>
<tr>
		<td>cc2</td>
		<td>UDC</td>
		<td>if user needs to ad extera business lookup data</td>
		<td></td>
	</tr>
<tr>
		<td>cc3</td>
		<td>UDC</td>
		<td>if user needs to ad extera business lookup data</td>
		<td></td>
	</tr>
<tr>
		<td>cc4</td>
		<td>UDC</td>
		<td>if user needs to ad extera business lookup data</td>
		<td></td>
	</tr>
<tr>
		<td>cc5</td>
		<td>UDC</td>
		<td>if user needs to ad extera business lookup data</td>
		<td></td>
	</tr>
<tr>
		<td>lastMaintDTime</td>
		<td>Date</td>
		<td>last time when row has been modified</td>
		<td></td>
	</tr>
<tr>
		<td>lastMaintUser</td>
		<td>OUser</td>
		<td>last user modified the associated row</td>
		<td></td>
	</tr>
<tr>
		<td>inActive</td>
		<td>boolean</td>
		<td>indicates if the associated row is soft deleted</td>
		<td>in database it is called "deleted"</td>
	</tr>
<tr>
		<td>selected</td>
		<td>boolean</td>
		<td>indicates if the associated row is selected in tabular screen</td>
		<td>It is a transient field</td>
	</tr>
<tr>
		<td>custom1</td>
		<td>String</td>
		<td>spare field to be used by user if he/she wants</td>
		<td></td>
	</tr>
<tr>
		<td>custom2</td>
		<td>String</td>
		<td>spare field to be used by user if he/she wants</td>
		<td></td>
	</tr>
<tr>
		<td>custom3</td>
		<td>String</td>
		<td>spare field to be used by user if he/she wants</td>
		<td></td>
	</tr>
<tr>
		<td>forcedInitializedFields</td>
		<td>List<String></td>
		<td>contains any field expression we had to initialize during loading from DB</td>
		<td>It is used to restore the original row fields values</td>
	</tr>
<tr>
		<td>custom1</td>
		<td>String</td>
		<td>spare field to be used by user if he/she wants</td>
		<td></td>
	</tr>
</table>
### General Methods

<table>
	<tr>
		<th>Method</th>
		<th>Description</th>
		<th>Notes</th>
	</tr>
	<tr>
		<td>generateInstID</td>
		<td>increment the counter for the current created an extend class instance</td>
		<td></td>
	</tr>
<tr>
		<td>forceFieldEmptyInitialization</td>
		<td>initialize a given field expression</td>
		<td></td>
	</tr>
<tr>
		<td>forceFieldInitialization</td>
		<td>initialize a given field expression with a given value</td>
		<td></td>
	</tr>
<tr>
		<td>getAllFields</td>
		<td>return current class fields</td>
		<td></td>
	</tr>
<tr>
		<td>getTranslatableFields</td>
		<td>return current class translatable fields</td>
		<td></td>
	</tr>
<tr>
		<td>isFieldUnique</td>
		<td>check if a given field is marked unique in DB</td>
		<td></td>
	</tr>
<tr>
		<td>getUniqueFields</td>
		<td>return current class unique fields</td>
		<td></td>
	</tr>
<tr>
		<td>getMandatoryFields</td>
		<td>return current class mandatory fields</td>
		<td></td>
	</tr>
<tr>
		<td>getEntityDiscrminatorValue</td>
		<td>return current class discriminator value</td>
		<td></td>
	</tr>
<tr>
		<td>invokeGetter</td>
		<td>invoke getter of a given field expression</td>
		<td>called by FABS services not users</td>
	</tr>
<tr>
		<td>invokeSetter</td>
		<td>invoke setter of a given field expression</td>
		<td>called by FABS services not users</td>
	</tr>
<tr>
		<td>NewFieldsInstances</td>
		<td>called when new row of the current class is created to initialize all class fields</td>
		<td></td>
	</tr>
<tr>
		<td>parseFieldExpression</td>
		<td>return list of ExpressionFieldInfo for the given field expression relative to the current class</td>
		<td>there is another version for a given class too</td>
	</tr>
<tr>
		<td>getFieldType</td>
		<td>return type for the given field expression relative to the current class</td>
		<td></td>
	</tr>
<tr>
		<td>getClassField</td>
		<td> Returns the Field object corresponding to fieldName
     Search for the field in the cls and its super classes as well Return null
      if not found at all</td>
		<td>there is another version for a given class too</td>
	</tr>
<tr>
		<td>isFieldMandatory</td>
		<td>check if a given field is marked as mandatory</td>
		<td></td>
	</tr>
<tr>
		<td> getParentEntityFieldName</td>
		<td>Get list of Parent Entity Class Paths in & of the entity of
      entityClassPath. <br> Parent is defined using</td>
		<td></td>
	</tr>
<tr>
		<td>getChildEntityClassPath</td>
		<td>Get a list of Child Entities Fields Class Paths in & of the entity of
      entityClassPath. <br> Children are defined using</td>
		<td></td>
	</tr>
<tr>
		<td>getValueFromEntity</td>
		<td>a very important method to get value from a given field expression with in a given entity</td>
		<td></td>
	</tr>
<tr>
		<td>setValueFromEntity</td>
		<td>a very important method to set value for a given field expression with in a given entity</td>
		<td></td>
	</tr>
<tr>
		<td>setChanged</td>
		<td>Set entities chain (parent, children) in the fieldExp to changed,
      starting from "this" entity.</td>
		<td></td>
	</tr>
</table>
### OEntityManager
#### Functionality
The main FABS interface that is responsible for loading and saving all kinds of BaseEntities
#### Functions
#####loadEntity
Used to load single entity from database. It has multiple versions based on the information user currently have.
##### loadEntityInternal
It is the end method called by all **loadEntity** method versions. It loads the entity from database **No Security Check**, **Must not be public**.
##### loadAttribute
Load the attribute (of name = "expression" parameter) from database and set it into its owner "entity" (passed parameter).

It assumes that single-valued attributes are loaded by default (EAGER FETCH), so, it doesn't load them. 

It doesn't reload a Since the function doesn't perform complete security validations, then, it must be private and not public.

##### loadEntityDetail
Loads specific fields into an already loaded entity. It forces reloading already loaded list fields.
##### postLoadEntity
This function performs the following on the passed baseEntities:


- Load Text Translations.
- Load entity fields listed in fieldExpressions.
- Force Unloaded Lists Initialization. 

Calls **forceListsIntialization** and doesn't re-load already loaded after loading the entity to do post load actions. Since the function doesn't perform any security validations, then, it must be private and not public.
##### loadEntityList
loads List of BaseEntity objects, it has multiple versions.
##### activateDeactivateEntityInternal 
Private function for entity deletion, don't make it public otherwise there will be a security hole <br>It doesn't call the OEntityAction, and has nothing to do with it.

##### deleteEntityInternal
Permanently removes the entity record (without it's children/composed entities) from database. Logs the transaction in EntityAuditTrail. Removes the related text translations as well. It doesn't call the OEntityAction, and has nothing to do with it. Don't make it public as it will cause security hole because of 'checkSecurity'.

##### saveEntityInternal
Private function for entity saving, don't make it public otherwise there will be a security hole. It saves both updated (merge) and new (persist) entities  It performs the following: Data Native Validation.

##### saveBatchForImport
Save multiple entities at once.

##### saveEntity
It is the public method to save a single entity

##### saveEntityList

It is the public method to save a list of entities once at a time.

##### restoreForcedIntializedFieldsValues
a private method to restore the forced initialized fields values.

##### executeUpdateQuery
public method to execute a given SQL statement.

##### getSystemUser
Return Granted system user with tenant, lanugage. Return the loggedUser if it's system user Calls
##### createViewListQuery
Just runs the query and return the result. If no error, and no records found, returned list is empty (not null)
##### constructEntityOrderByClause
Construct a string with all the order by expressions mentioned in the 'sorts' parameter.
 Facts & Assumptions: 
- it puts "entity." before the expression
- SortExpression has ColumnName "ASC"/"DESC"/None (ASC By Default)
##### getOEntity
return the BaseEntity OEntity
##### loadFullEntity
Load the entity with all its direct children.
##### getUser
 Load the user from its tenant database. This should be the only function in the system to load a user for security and query purposes (not necessary for maintenance). Sets the returned user tenant, so it can be used later on in any query or security
##### findEntityByUnique
Search for entity of type 'searchByValues' in 'em' using:


- The first known unique field found, or if not found.
- The first known unique constraint.

##### callPostLoadForQueryLoadedList
This method is called to do post load on base entities that were loaded through query and not the entity manage normal methods.

##### getTotalNumberOfEntityRecords
Calculate the total Number of records For the passed oentity using the passed conditions.


## ObjectBaseEntity
It is just a wrapper for BaseEntity
## BaseEntityTranslation
### Functionality
The supper mapped class that contains the general data for all translatable classes.
### Fields
#### entityDBID
the dbid of the translated row
#### language
return the language of the translated field

## BaseEntityMetadata
### Functionality
Helper class to retrieve all given BaseEntity meta data 
### General Methods
####getAllFields
Get a list of ALL class fields Including fields of super classes, public, private, and protected.
Is used to get the fields Is used instead of:
- getDeclaredFields: doesn't get the super class fields 
- getAllFields: gets only public fields

#### getClassFields
Get all the fields of the class and its super classes.
If you have the entity instance, then call BaseEntity.getAllFields() instead, it's the same functionality, but probably of a better performance
#### getTranslatableFields
Get a list of ALL class Translatable fields Including fields of super classes, public, private, and protected
####getUniqueFields
Returns list of the unique fields in the class It doesn't return the unique constraint fields
#### getMandatoryFields
Get a list of ALL class Mandatory fields (nullable = false) Including fields of super classes, public, private, protected, and join columns
#### getXToOneRelatedFields
Return a List<Field> of OneToOne & ManyToOne Related Fields
#### getFieldColumnName
Returns the column name of the field in the database table, in lower case.
The column name is concluded from the field specifications.
 No validation is done that the column really exists in the database.
#### getClassObjectTypeField
Return a list of the entity fields that are either of type objectTypeCls or are lists of element type objectTypeCls
#### getFieldType
This function returns the following: 
- Field type if it's not a list 
- 'List' if the field is list
#### getFieldObjectType
This function returns the following: - Field type if it's not a list. List Element type if the field is list
#### getClassField
Returns the Field object corresponding to fieldName. Search for the field in the cls and its super classes as well Return null if not found at all

#### isFieldMandatory
 Return true if the field is not nullable. It considers both the Column & JoinColumn
#### getParentEntityFieldName
Get a list of Parent Entities Field Names in & of the entity of entityClassPath. Parents are defined using given information
#### getMeasuarableFieldUnit
return fields that are marked measurable fields
#### getUniqueConstraintFieldNames
Returns list of the fields names of the first found unique constraint
#### getChildEntityFieldName
Get a list of Child Entities Fields Names in & of the entity of entityClassPath. <br> Children are defined using
#### getParentClassPath
Get list of Parent Entity Class Paths in & of the entity of entityClassPath. Parent is defined using
#### getChildEntityClassPath
Get a list of Child Entities Fields Class Paths in & of the entity of entityClassPath. Children are defined using given information
#### getAnnotation
Returns the annotation if found, checking all super classes as well
#### parseFieldExpression
Return a Field list of the passed field expression, first element is the most left field. It supports nested fields (i.e. expression contains ".") It supports fields of all types (including lists) It supports fields of class ancestors First field in the expression should be in the cls
#### getXToManyRelatedEntities
Return a List<String> of OneToMany & ManyToMany Related Fields Names
#### invokeGetter
Invokes the getter of the field (fieldName) which is declared in the entity or one of it's super classes. 
#### invokeSetter
Invokes the setter of the field (fieldName) which is declared in the entity or one of it's super classes.
 It doesn't call em.clear(), so, the set value will be persisted if entity is persisted
## DBIDGeneration
### Functionality
represents the id for all BsaeEntities. It gets its dbid from within a range defined within each business module
###Fields
<table>
	<tr>
		<th>Field</th>
		<th>Type</th>
		<th>Description</th>
		<th>Notes</th>
	</tr>
	<tr>
		<td>dbidRangeStart</td>
		<td>long</td>
		<td>The begining of the module range</td>
		<td></td>
	</tr>
<tr>
		<td>dbidRangeEnd</td>
		<td>long</td>
		<td>The end of the module range</td>
		<td>It is a transient field</td>
	</tr>
<tr>
		<td>newDBID</td>
		<td>long</td>
		<td>the new dbid to be given the requested new row</td>
		<td>It is a transient field</td>
	</tr>
</table>
### DBIDGenerationService
#### Functionality
It is the service responsible for generating new dbids.
##### generateDBID
Checks the entity should have a newly generated DBID, if so, it generates a new DBID in the datFabase in the module range, and return it. 

Function doesn't change the entity DBID
##### generateDBID (another version) 
Set the entity & one-to-one children DBIDs to be in the entity's module.
If entity is new or (is updated and generate4Update = true), DBID is
generated, otherwise, it's left AS IS If entity has no module
 settings, it's returned AS IS Module is set to "Blank" Module if no
 module is set to the entity Doesn't persist/merge entity/child in the
 database
## ParentEntity
### Functionality
 Used to annotate entity to list the entity field(s) mapped to the entity parent(s). It's used for Composition Parent-Child Relationship. To get entity children, refer to GetEntityChildrenTree, See Also ChildEntity
### fields
 List of the names of fields in the entity that represent parents. e.g. in ScreenField you should find OScreen field as parent.
Second parent is added if the entity represents an association class
## ChildEntity
List of the names of fields in the entity that represent children.  e.g. in OScreen you should find ScreenField field as child.

## ExpressionFieldInfo
### Functionality
Mainly used by BaseEntity.parseFieldExpression()
### Fields
#### fieldParsedExpression
 Main expression parsed to get this field
#### fieldName
field name out of the parsed expression
#### field
Field Object
#### fieldClass
Field Class used in the expression. If field is defined in ancestor class then this value will be different than field.getDeclaringClass()
#### originalField
Original Field, If Field is Translatable


# Entity Setup
![Entity Full Picture](EntityFullPicture.png)
## OEntity
Holds the specifications of an entity, acts as Data Dictionary for the entities.
### Important Fields
#### entityClassPath
Full name of the new entity
#### title
Title that gets inherited by all entities (i.e. OObject, OBusinessObject) to be used in the Balloon
#### getAction
Get the Entity Action given its type from oActions field. It doesn't load the action from database, it just searches for it in the oActions field (@param actionTypeDBID One of the ATDBID_xyz member fields).
Return action: if found null: either there is no corresponding action, or it's not loaded from database for any reason (security, LAZY, etc...)
## OEntityAction
All the actions that can be performed on the entity.
### Importand fields
#### name
the name in which the action will be displayed by
#### sortIndex
action index
#### displayed
to be displayed in balloon or not 
## OEntityField
Holds information for oEntity in entity fields level.
###Important Fields
#### fieldExpression
#### securityTag
#### encrypted
## OOBjectBriefInfoField
Overview: Represents the information displayed in the balloon information panel
## EntityActionPost
Holds the actions that will be executed after the main action execution
## EntityActionPrivilege
Holds the access privilege of the entity
###Important Fields
#### fieldExpression
Binded backbean value for this info field
#### primary
Displayed in the balloon panel or not
#### fieldJavaType
Field type in java
#### sortIndex
Sorting index
#### masterField
Used in header or not
#### titleOverride
Override parent title