## Introduction
The aim of this documentation is to answer the following questions:

* What is _Alert_?
* What _Alert_ does?
* How to create and run _Alert_?
* How to modify and test _Alert_ both _technically_ and from _UI_?


## Business Requirement ##
### Scope ###
Alert is a tool provided by FABS to enrich user experience by providing the following services:

* Send notification to different users. 
* Notification is either by _email_ or _SMS_.
* Sending notification can be _time based_ or _event based_.
* The email body can be _rich_.
* Alert different parameters support _static_ and _dynamic_ values.
* The dynamic values can be provided either by _"FABS Entity Expressions"_ or _"SQL Statements"_.  
* _Alert_ email should support attaching reports files (csv, doc, etc..).
* The attachment can be direct upload or from running a _custom report function_ and attach the output.
 
### Business Stories
The following is some business user stories to illustrate more the _Alert_ scope.
#### Birthday Wishes
##### Story
As an HR personnel, I want to congratulate the Employee on his birthday by sending an email to him/her and his/her colleagues (Configurable – setup according to company nature)
##### Acceptance Criteria
1. Verify that the mail is sent to the employee and his colleagues.
2. Verify that the mail Content (Subject and Body) are formatted as configured by HR Personnel.
3. Verify that the mail is sent in the correct date and time.
4. Verify that the mail is sent with configured Fancy content (Image – Flash).
5. Verify that Employee Personal data is sent in Subject and Body (Data is configurable).


#### End of Paid Maternity
##### Story
As a HR personnel, I need to be alerted by the system about the employees having Maternity Leave with code “003” (the vacation type is configurable as per the customer) which will end in 2 weeks, we need this alert to be sent one time at 9:00 am (Sending time is configurable per customer) to the HR personnel (as static e-mail), the system should send the list of employees in an excel sheet with data example as the following:
> The paid maternity of the attached employees will end on the attached dates, please take the necessary procedures

##### Acceptance Criteria
1. Verify that the mail is sent to all the HR personnel as per the configured TO and CC in the alert e-mail setup.
2. Verify that the mail Content (Subject and Body) are formatted as configured by HR Personnel.
3. Verify that the mail is sent in the correct date and time.
4. Verify that the mail attachment can uploaded and sent correctly.


#### End of Service
##### Story
As a HR department policy and processes, we need the system to automatically generate alert for each department manager CC each supervisor of each employee who is going to be terminated this month, the Department manager and the supervisor should be fetched dynamically per each terminated employee, we need this alert to be sent one time at 9:00 am, the body of the mail should be as the following:

* Subject: _Ramy Hassan Termination actions_
* Body:
>Dear Mr. Mohamed,  
“This is a system generated notification- please do not reply.

>Kindly be advised that the following staff member service with the company will be terminated; therefore please give your instructions in order to take the necessary actions.

>Employee ID: 00563  
Name: Ramy Hassan  
Position: Sales Personnel  
Department: Business development  
Branch: Head Office  
Termination reason: End of service  
Leave date: 30/05/2014

>Thank You and Best Regards”

##### Acceptance Criteria
1. Verify that the mail is sent to all the Managers as per the configured *TO* and *CC* in the alert e-mail setup.
2. Verify that the mail Content (*Subject* and *Body*) are formatted as configured by HR Personnel.
3. Verify that the mail is sent in the correct date and time.
4. Verify that the mail Body details is corresponding to the terminated employees.


#### Delay
##### Story
As an HR personnel, I want to send an e-mail to the employee and his direct manager or supervisor when an employee number of delays per month >5.
##### Acceptance Criteria
1.	Verify that the mail is sent to the concerned people
2.	Verify that the mail is sent right date and time after the 5th time of being late even before the punch in and to be sent on 9 am.
3.	The subject and body are configurable.


#### 3 Month Probation Period
##### Story
As an HR personnel I want to send an e-mail 2 weeks before the end of the probation period ( 3 months from the hiring date) (*static e-mail*).
##### Acceptance Criteria
1.	Verify that the e-mail is sent to the concerned people
2.	Verify that the e-mail is sent at the right date and time
3.	List of employees should be created
4.	The subject and body message is configurable


#### ID expiration
##### Story
As HR personnel, I want to send a mail to the employee and HR coordinator to notify the employee of his/her near expiration of his/her ID 30 days before its expiration. This email should include the same employees until an actions is taken (change his hiring type for example; *this to be configured per customer*)
##### Acceptance Criteria
1.	Verify that the mail is sent to the concerned people.
2.	The mail should be sent 30 days before the expiration date and then once every 5 days till the employee master file is updated with the new date.
3.	The body message and the subject should be configurable.
4.	Verifying that the mail was sent at the right date and time.
5.	Subject to contain the number of reminders

#### Age of Sixty
##### Story
As HR personnel I want to send an e-mail for the employees that will reach the age of sixty 1 month before they reach it.
##### Acceptance Criteria
1.	Verify that the mail is sent to the people concerned
2.	The mail should be sent a month before the employee reach 60 years
3.	Body message and subject are configurable
4.	Verify that the mail is sent at the right date and time
5.	E-mail should contain fascinating contents ( flash )


#### Absence (5 days continuous)
##### Story
As an HR personnel I want to be notified when an employee become absent for 5 continuous days, (this e-mail should be sent to HR manager, personnel section head, branch HR supervisor).
##### Acceptance Criteria
1.	Verify that the mail is sent to the concerned people.
2.	Mail should be sent right after an employee become absent for 5 continuous days.
3.	Body and subject are configurable.


#### Exit Interview
##### Story
Managers and head section of the specified department that are hiring should be alerted when an applicant exit an interview.
##### Acceptance Criteria
1.	Verify that mail is sent to concerned people
2.	Verify that they are sent right on time 
3.	The mail should collect the name of applicant with their time of finishing into 1 mail in case of making more than 1 interview per day.
4.	Body and subject are configurable



#### Employee Status Changes
##### Story
As HR personnel I want to send an e-mail to the employees to notify them every 3 months if he/she has any changes in his personal information that he should update it.


##### Acceptance Criteria
1.	Verify that the email is sent every 3 months to all employees
2.	Body and subject are configurable


#### Pending Annual Vacation
##### Story
I want to notify the employee when he take his annual leave and didn’t continue his vacation notes (vacation form request) and after a week from the vacation start date. It should be sent each week
##### Acceptance Criteria
1.	Verify that the mail is sent to the concerned people ( direct manager, HR department)
2.	Is sent on the right date and time
3.	Body and subject are configurable


#### Military Service Certificate Expiration 
##### Story
As HR Specialist, I want to send an alert as email to the employee warning him to renew his certificate, before a configured period of days of employee's military service certificate expiration date, Include in the CC his direct manager and direct supervisor.
##### Acceptance Criteria
1. Verify that first email is sent at date already defined in the configuration (Ex. 30 days before expiration date).
2. Verify that the mail Contents (Subject and Body) are formatted as configured by HR Personnel.
3. Verify that it send a reminder (Same email with no of days left) every predefined period of days. (Ex. *Send the reminder every 10 days*).
4. Verify that the email's body should contain the employee's personnel data (Ex. *Name, ID no, Military Expiration date*, …).
5. Verify that the email is sent also to the employees included in the CC (Ex. His direct manager, direct supervisor).


#### Quarter Performance Review
##### Story
As HR Manager I want to send an email for certain positions individually, to inform them that their quarterly appraisal review is starting 20 days before end of the quarter at these days (10/03), (10/06), (10/09), (10/12)
##### Acceptance Criteria
1.	Verify that email is sent to the right employee assigned to that position.
2.	Verify that email is sent for each employee individually.
3.	Verify that email is including the required data including employee's data and appraisal data (ex. *Link*) that he need to fill.

#### Sick Leave Return
##### Story
As HR Manager, when there is an employee with Sick leave more than five days, I want to inform his Direct Manager and Direct supervisor about the end date of his sick leave five days earlier and send another email as reminder at vacation's end date to inform them that his vacation is finished. And to send email one day after his vacation end date informing whether he attended or not and if there is a justification for his absence.
##### Acceptance Criteria
1.	Verify that it send email in the exact time (Five days before the end date)
2.	Verify that it send email to the right employees with right positions.
3.	Verify that the email is including the right data related to the employee (*name*, *vacation details*)
4.	Verify that it send the reminder email.


#### General Acceptance Criteria
1.	Receiver should be mandatory on the alert setup
2.	E-mail formatting should be checked to be correct
3.	Log the alert sending attempts with full status to track the customer complains
4.	Subject should be mandatory
5.	Character limitation checking while setting up the alert for the subject and the Body



## Technical Architecture 
### Entities
![Alert Entities](AlertEntities.png)
#### BaseAlert
##### Description
It contains the common fields for all alert types.
##### Fields
<table>
	<tr>
		<th>Field</th>
		<th>Type</th>
		<th>Description</th>
		<th>Notes</th>
	</tr>
	<tr>
		<td>messageBody</td>
		<td>String</td>
		<td>The body of the alert</td>
		<td>can be parametrized</td>
	</tr>
<tr>
		<td>messageSubject</td>
		<td>String</td>
		<td>The subject of the alert</td>
		<td>can be parametrized</td>
	</tr>
<tr>
		<td>title</td>
		<td>String</td>
		<td></td>
		<td>will be used as <b>code</b> for new alert function</td>
	</tr>
<tr>
		<td>sendViaEmail</td>
		<td>boolean</td>
		<td>Allow sending emails</td>
		<td></td>
	</tr>
<tr>
		<td>sendViaSMS</td>
		<td>boolean</td>
		<td>Allow sending SMSs</td>
		<td></td>
	</tr>
</table>
##### Additional Info
It uses **Join Inheritance**


#### CustomAlert
##### Description
Uses normal sqls to get Alert data instead if normal **FABS OEntity**
##### Fields
<table>
	<tr>
		<th>Field</th>
		<th>Type</th>
		<th>Description</th>
		<th>Notes</th>
	</tr>
	<tr>
		<td>filterSQL</td>
		<td>String</td>
		<td>To get all valid entities for given condition(s)</td>
		<td>It always select <b>dbid</b> field only</td>
	</tr>
	<tr>
		<td>paramSQL</td>
		<td>String</td>
		<td>Get the values of the subject and/or body params</td>
		<td>Whatever the sql you will write it must ends with '<b>
		dbid=</b>'</td>
	</tr>
	<tr>
		<td>toSQL</td>
		<td>String</td>
		<td>Get emails of <b>TO</b> group</td>
		<td>Whatever the sql you will write it must ends with '<b>
		dbid=</b>'</td>
	</tr>
	<tr>
		<td>ccSQL</td>
		<td>String</td>
		<td>Get emails of <b>CC</b> group</td>
		<td>Whatever the sql you will write it must ends with '<b>
		dbid=</b>'</td>
	</tr>
</table>
##### Additional Info

#### Alert
##### Description
Uses **OEntity**.
##### Fields
##### Additional Info

#### AlertParam
##### Description
##### Fields
<table>
	<tr>
		<th>Field</th>
		<th>Type</th>
		<th>Description</th>
		<th>Notes</th>
	</tr>
	<tr>
		<td>fieldExpression</td>
		<td>String</td>
		<td></td>
		<td></td>
	</tr>
<tr>
		<td>paramIndex</td>
		<td>int</td>
		<td></td>
		<td></td>
	</tr>
</table>
##### Additional Info

#### BaseCondition
##### Description
##### Fields
<table>
	<tr>
		<th>Field</th>
		<th>Type</th>
		<th>Description</th>
		<th>Notes</th>
	</tr>
	<tr>
		<td>name</td>
		<td>String</td>
		<td></td>
		<td></td>
	</tr>
<tr>
		<td>fieldExpression</td>
		<td>String</td>
		<td></td>
		<td></td>
	</tr>
<tr>
		<td>operator</td>
		<td>UDC</td>
		<td></td>
		<td></td>
	</tr>
<tr>
		<td>conditionValue</td>
		<td>String</td>
		<td></td>
		<td></td>
	</tr>
<tr>
		<td>conditionIndex</td>
		<td>int</td>
		<td></td>
		<td></td>
	</tr>
<tr>
		<td>conditionOperator</td>
		<td>String</td>
		<td></td>
		<td></td>
	</tr>
</table>
##### Additional Info

#### Receiver
##### Description
##### Fields
<table>
	<tr>
		<th>Field</th>
		<th>Type</th>
		<th>Description</th>
		<th>Notes</th>
	</tr>
	<tr>
		<td>email</td>
		<td>String</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>emailExpression</td>
		<td>String</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>sms</td>
		<td>String</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>smsExpression</td>
		<td>String</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>bulk</td>
		<td>boolean</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>receiverType</td>
		<td>UDC</td>
		<td></td>
		<td></td>
	</tr>
</table>
##### Additional Info



### Services
![Alert Detailed Architecture](AlertDetailedArchitecture.png)
### UI
#### Pages
##### Alert Standard Builder
<table>
	<tr>
		<th>Screen</th>
		<th>Functionality</th>
		<th>Type</th>
		<th>Notes</th>
	</tr>
	<tr>
		<td>Alert Manager</td>
		<td>Manage basic alert data</td>
		<td>Tabular</td>
		<td></td>
	</tr>
	<tr>
		<td>Alert Body</td>
		<td>Rich editor to write alery body</td>
		<td>Form</td>
		<td></td>
	</tr>
	<tr>
		<td>Alert Parameters Manager</td>
		<td>Manages alert parameters</td>
		<td>Tabular</td>
		<td>Parameters syntax should in form <strong> <%I></strong> which "<strong> I </strong>" is the index of the parameter</td>
	</tr>
	<tr>
		<td>Alert Conditions Manager</td>
		<td>Manage alert conditions</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>Alert Receivers Manager</td>
		<td>Manage alert receivers</td>
		<td>Tabular</td>
		<td></td>
	</tr>	
</table>
##### Alert Custom Builder
<table>
	<tr>
		<th>Screen</th>
		<th>Functionality</th>
		<th>Type</th>
		<th>Notes</th>
	</tr>
	<tr>
		<td>Custom Alert Managemer</td>
		<td>Manage custom alert SQLs</td>
		<td>Tabular</td>
		<td></td>
	</tr>
</table>


### Unit Tests
#### AlertSerivceTest
Perform unit test on all provided functionalities in ***AlertService***
## Test Cases:
### Scenario 1:
### Scenario 2:

