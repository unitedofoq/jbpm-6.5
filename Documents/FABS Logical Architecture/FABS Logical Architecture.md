#General
![FABS Logical Architecture](FABSLogicalArchitecture.png)
# FABS® Functional Architecture
FABS® functional architecture is divided into four main sections:


- servers and frameworks,
- FABS® engine,
- FABS® GUI tools and 
- FABS®- Based business solutions (see the figure). 

 In the next few subsections we will explore these sections in more details.
## Servers and Frameworks
FABS® back-end is built over powerful ranked niche tools and frameworks,
harmonizing together. It was built using agile philosophy and based on Free
Open Source Software (FOSS) and standard frameworks.
Therefore, each component in FABS® framework is replaceable and scalable
in a highly responsive manner. Hence, FABS® kills IT-vs-Business gap to let
business people focus on business rather than technical aspects. 
## FABS® Engine
FABS® engine consists of many core tools that work behind the scene to
enrich business user experiences.

 One of the most important tools is Entity Manager which is responsible for handling both metadata and business data from the presentation layer to the data layer and vice versa. Another important tool is the Cashing Manager who caches the metadata for later use to enhance the performance and increase memory management.

 An additional tool is Security Manager that provides security for both metadata and business data for all Create, Read, Update and Delete (CRUD) operations. Extra important tool is Multi-Tenancy Manager that handles the active tenants to
work securely, efficiently and isolated from each other. Furthermore, there are
many other important tools like Import and Export Data Manager, Routing,
Collaboration, I18n, Builders and many more.

## FABS® GUI Tools
FABS® provides multiple GUI tools to provide the business user the ability to
build and customize his/her business solution in an easy manner. FABS® GUI
tools can be categorized into four categories (i) application builders, (ii)
administration, (iii) configuration and (iv) services which will be explored in the following subsections. 

## Application Builders 
FABS® builders are tools to enable implementer or business user to build or
modify his/her business solution with the need to write no code, deploy new
applications or restart the application server.

One of the most important FABS® builders is “**Page Builder**” since page
concept in FABS® is extremely important. The page contains two or more
related potlets that are communicating with each other and presenting
complete business functionality in different layouts. Consequently, all FABS®
builders are pages.

Another important builder is “**Screen Builder**”. Through this builder business
user can build the screens he/she wants and assign different CRUD
functionality to it among other functionalities such as control validations,
control types, controls sizes or screen GUI with absolutely no code. To
enhance usability of this builder it provides different types of screens builders
such as “Form Screen” and “Tabular Screen".

One more builder is “**Menu Builder**” that allows business user to build different
menu hierarchies based on different roles. Other available builders are “**Business Process Builder**”, “**Report Builder**”, “**Wizard Builder**”, “**Alert Builder**”, “**Chart Builder**” and “**Business Entity Builder**”.

## Administration
FABS® provides business user with easy-to-use tools to administrate FABS®-
based business solution. One of the most important tools is **User Manager**
which end user can add, edit or remove user and provide the basic information
for him/her like email, password, first and second language.

 Another important tool is **Menu-Role** Manager to define certain menu to certain roles. An additional tool is User-Role Manager to assign defined user to certain roles.  

## Configuration
Configuration tools are a way to help FABS® knows certain information about
**tenants**, **LDAP server**, **process server**, **web services server** or any other important information. 

## FABS® Services
From day one FABS® is ready to present multiple services as Software-as-aService
(SaaS). 
One of the most important services that are provided by
FABS® is “**Alert**”. The FABS® alert service enables the user to send business
information, notifications or events based on certain times or events via
different communications channels like email or SMS.

 One more service is “**Form Designer**” that enables the business user to create business templates that can be filled and send automatically via previous setup. 

An additional important service is “**Server Job**” that enables user to perform non GUI long functionalities, like calculating monthly payroll, with no need to affect the running application performance. 

Further important service is “**Translation**”
that enables business user to provide a translation in any language he/she
wants not just to the Meta data but also the business data. The data are
shown in the user first language preference.

 Moreover, FABS® provides
different “Analytical Tools” tools that diverse on functionality and levels (i.e.
**OLAP**, **Charts**, and **Reports**).

 Furthermore, FABS® presents powerful SOA,
business process and workflow engine supporting (**BPEL**, **BPMN**, **Human
Task**, **Business Rules**). Also, there are different “Audit” tools to enable the
business user to track the system data and actions on different levels. 

Finally,
FABS® provides security on many levels and layers like (**HTTPS**, **LDAP**,
 **data encryptio**n). 
# FABS®-Based Business Solutions 
It is the resultant product built by the business user through FABS® builders
and tools. Once the application is built and configured it is ready to be used
with absolutely no need for any kind of installation or deployment.

