# FABS Physical Architecture#
# FABS Logical Architecture#
# Builders #
##Entity Full Picture##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Tabular Screen Full Picture##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Form Screen Full Picture##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Portal Full Picture##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Wizard Full Picture##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet


##Chart Full Picture##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Alert Full Picture##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Process Full Picture##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Routing Full Picture##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##WebService Full Picture##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Report Full Picture##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Java Entity Full Picture##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Special Screen Full Picture##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Menu Builder##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

#Tools#
##Form Designer##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Audit Trail##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Entity Attachment##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Special Screen Full Picture##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

#Configuration
##Data Dictionary
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##UDC
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Special Screen Full Picture##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##DataType##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##User Message##
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Modules
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##System Images
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

#Process
##Inbox
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Outbox
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Task Delegation
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##My Processes
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##All Active Processes
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Manage User Tenant Role
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Manage User Tenant Role Admin
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

#Function Setup
##Organization Chart
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##M2M Relational Screen
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Java Validation Setup
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Java Function
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Process Function
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

##Screen Function
### Entities
### Services
#### Methods
### Session Beans
#### Methods
### Web Page
### Portlet

#Identity Management
## User Management
## Role Management


#System Parameters
## General Setup
## LDAP Setup

#Cache
##Clear Cache functions