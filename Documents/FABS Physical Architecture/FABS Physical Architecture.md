#General
![FABS Physical Architecture](FABSPhysicalArchitecture.png)
#FABS® Physical Architecture
This view explains the physical components of FABS®. These components
can be categorized into 


1.  application server,
2.  process server, 
3.  LDAP/Active Directory,
4.  database server,
5.  client and 
6.  cloud system.

 All these categories,which will be detailed in the following subsections, can be installed in to different servers with different operating systems with no integration problem at all (see Figure above). 

## Application Server
FABS® works on any application server supports J2EE framework like
Glassfish or Web Logic. On the other hand, portal server can be installed on
the same application server, which FABS® is installed on or a different
application/web server.
 
## Process Server
It is the server that runs and configures business processes. It is integrated
with FABS® through different configuration tools. ## LDAP
It is the location where the user and roles are created and stored beside
FABS® central DB. It is integrated with FABS® through different configuration
tools. 

## Database Server
FABS® runs on any relational DB (i.e. MySQL, SQL Server, Oracle DB.. Etc.)
based on the business needs. On the other hand, FABS® needs the following
databases to be configured 


1. one for portal server,
2. one of business solution data and metadata ,
3. one for process servers,
4. and one for multitenancy configurations. 

## Client
All what clients need to work on FABS® or FABS®-based business solution is
application url and browser that support Java script (i.e. Firefox, Chrome, and
IE) from desktop, laptop or smartphone. There is no any other requirement
required from the user. 

## Cloud System
It is the operating system(s) that holds all previous servers on cloud
environment. On the other hand, FABS® runs on Linux and windows equally,
but not tested on the Mac environment yet. 
