#Create Entity
## Create the file in a proper package
Normally all pages in FABS started with ***com.unitedofoq.fabs.core*** then they named after their functionality like *alert*, *auditTrail* or *uiframework*.
So create your package named properly and also name your entity a meaningful name after its functionality like *FormScreen*, *UDC* or *CustomAlert*.
## What to extend
All entities must extend BaseEntity directly or indirectly. If your new entity is a new type for already existing type of entity {i.e a new type of *OScreen*, or a new type of *UDC*} then your new entity will extend *OScreen* or *UDC* otherwise extend *BaseEntity* directly.
## Entity Relations
Your new entity could, and most probably would, have different type of relations with new/old entities. Defining those relations as (one-to-one, many-to-one or many-to-one) is an EJB thing what matters to us here is what of those relations is considered a parent and what are considered a children
###Parent Entity
It is a many (of me)-to-one(of other entity) relation. Moreover, it is the entity that will my entity screens will be filtered by it as an input. Therefore, I have to consider what the parent relation among all the many-to-one relations my entity has.
For instance, ***ChartScreenDim3Fields*** has many-to-one relation with ***ChartScreen*** and it is the most important relation. Therefore, we add this code above the entity definition  
         `@ParentEntity(fields = {"chartScreen"})`
       ` @Entity`
    `public class ChartScreenDim3Fields{}`
### Child Entity
It is the same concept but in reverse, in ***ChartScreen*** you have to say that ***ChartScreenDim3Fields*** is its child, you can have as many children as needed

    @ChildEntity(fields={"screenInputs", "screenOutputs","screenFilter","screenFunctions", "chartScreenDim3Fields"})
## Translation
Your entity fields might need some translatable data  (i.e *name*, *header* or *title*)
in that case there is a standard procedure to follow
### Translation Entity
- Create translation entity with exactly the name *NewEntityName*Translatoion (i.e *OScreenTranslation*, *DDTranslation*).
- This entity must extends BaseEntityTranslation
- The fields of this entity will be the ones you want to translate with the exact names in the original entity.
- The entity will be in the same package.
- Create translation table sql like any other translation table 
- 
    @Entity
    public class ChartScreenTranslation extends BaseEntityTranslation {
    
    // <editor-fold defaultstate="collapsed" desc="header">
    @Column, 
    private String header;
    
    public void setHeader(String header) {
    this.header = header;
    }
    
    public String getHeader() {
    return header;
    }
    // </editor-fold>
    
    }
### Original Entity Translation fields
Follow the following example to translate field *header*

      @Translatable(translationField="headerTranslated")
    @Column(nullable=false)
    protected String header;
    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }
    
    @Translation(originalField="header")
    @Transient
    protected String headerTranslated;
    
    public String getHeaderTranslated() {
    return headerTranslated;
    }
    
    public void setHeaderTranslated(String headerTranslated) {
    this.headerTranslated = headerTranslated;
    }


      public String getHeaderTranslatedDD(){   return "OScreen_header";  }

## Adding your normal fields   
##Add DD Getters
DD (Data Definition) is the way your field will be displayed in any screen and it must be add if you want your field to be displayed. please follow the following example:

    public String getFieldNameDD() {
    return "EntityName_FieldName";
    }