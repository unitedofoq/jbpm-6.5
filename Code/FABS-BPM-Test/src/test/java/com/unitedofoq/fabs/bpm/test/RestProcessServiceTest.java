/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.bpm.test;

import static org.junit.Assert.assertEquals;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.unitedofoq.fabs.bpm.model.ProcessInstance;
import com.unitedofoq.fabs.bpm.model.status.ProcessState;
import com.unitedofoq.fabs.bpm.rest.services.impl.RestDefinitionServiceImpl;
import com.unitedofoq.fabs.bpm.rest.services.impl.RestProcessServiceImpl;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author bgalal
 */
@RunWith(WeldJUnit4Runner.class)
public class RestProcessServiceTest {
	
	@Inject
	RestProcessServiceImpl restProcessServiceImpl;
	@Inject
	RestDefinitionServiceImpl restDefinitionServiceImpl;
	
	Map<String, String> returnedProcessVariables = new HashMap<>();
	
	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void retrieveProcessVariables() {
		Map<String, String> processVariables = new HashMap<>();
		processVariables.put("yes","String");
		processVariables.put("Initiator","String");
		processVariables.put("lastUser","String");
		processVariables.put("reviewerDecision","String");
		processVariables.put("currentDate","String");
		processVariables.put ("no","String");
		processVariables.put ("funcCode","String");
		processVariables.put("reviewer","String");
		processVariables.put("dbid","String");
		processVariables.put("outType","String");
		processVariables.put("taskMOD","String");
		processVariables.put("Initiator_email","String");
		processVariables.put("entityName","String");
		String processDefId = "com.unitedofoq.user_message.usermessage.UserMessage";
		returnedProcessVariables=restDefinitionServiceImpl.retrieveProcessVariables(processDefId);
		assertEquals(returnedProcessVariables,processVariables);
	}
    
	@After
	public void tearDown() {
	}

    @Test
	public void startProcess() {
		ProcessInstance processInstance=null;
		Map<String,Object> parameters=new HashMap<String,Object>();
		parameters.putAll(returnedProcessVariables);
		String processId = "com.unitedofoq.user_message.usermessage.UserMessage";
		processInstance=restProcessServiceImpl.startProcess(processId,parameters);
		assertEquals(processInstance.getProcessId(),processId);
		assertEquals(processInstance.getState(),ProcessState.ACTIVE);
    }
    @Test
    public void retrieveAllProcessInstances() {
        List<ProcessInstance> processInstances  = new ArrayList<ProcessInstance>(restProcessServiceImpl.retrieveAllProcessInstances());
        assert processInstances != null;
}
    
    @Test
    public void retrieveUserProcessInstances() {
        List<ProcessInstance> processInstances  = new ArrayList<ProcessInstance>(restProcessServiceImpl.retrieveUserProcessInstances("test"));
        assert processInstances != null;
    }
    
    @Test
    public void retrieveProcessInitiator() {
        String s = restProcessServiceImpl.retrieveProcessInitiator(4);
        assert s != null;
    }
}
