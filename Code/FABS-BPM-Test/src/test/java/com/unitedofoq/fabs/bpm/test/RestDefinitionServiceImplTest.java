package com.unitedofoq.fabs.bpm.test;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.unitedofoq.fabs.bpm.rest.services.impl.RestDefinitionServiceImpl;

@RunWith(WeldJUnit4Runner.class)
public class RestDefinitionServiceImplTest {

	@Inject
	RestDefinitionServiceImpl restDefinitionServiceImpl;

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void retrieveProcessVariables() {
		Map<String, String> processVariables = new HashMap<>();
		Map<String, String> returnedProcessVariables = new HashMap<>();
		processVariables.put("yes","String");
		processVariables.put("Initiator","String");
		processVariables.put("lastUser","String");
		processVariables.put("reviewerDecision","String");
		processVariables.put("currentDate","String");
		processVariables.put ("no","String");
		processVariables.put ("funcCode","String");
		processVariables.put("reviewer","String");
		processVariables.put("dbid","String");
		processVariables.put("outType","String");
		processVariables.put("taskMOD","String");
		processVariables.put("Initiator_email","String");
		processVariables.put("entityName","String");
		String processDefId = "com.unitedofoq.user_message.usermessage.UserMessage";
		returnedProcessVariables=restDefinitionServiceImpl.retrieveProcessVariables(processDefId);
		assertEquals(returnedProcessVariables,processVariables);
		
	}
}
