/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.bpm.test;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.api.task.TaskService;
import org.kie.api.task.model.OrganizationalEntity;
import org.kie.api.task.model.Status;
import org.kie.api.task.model.Task;
import org.kie.api.task.model.TaskSummary;
import org.kie.remote.client.api.RemoteRuntimeEngineFactory;

/**
 *
 * @author bgalal
 */
public class RestApiTest {

    @Test
    public void testRestApiConnection() {
        try {
            // the serverRestUrl should contain a URL similar to
            // "http://localhost:8080/jbpm-console/"
            // Setup the factory class with the necessarry information to
            // communicate with the REST services
            System.out.println("Connecting to engine");
            RuntimeEngine engine = RemoteRuntimeEngineFactory
                    .newRestBuilder()
                    .addUrl(new URL("http://1.1.1.82:8080/jbpm-console"))
                    // 177
                    // .addUrl(new
                    // URL("http://1.1.1.235:8080/jbpm-console"))//177
                    .addTimeout(5)
                    // .addDeploymentId("com.unitedofoq.process.test:test-process-project:1.0")
                    .addDeploymentId("com.masoud:fabs-process:1.0")
                    .addUserName("Administrator").addPassword("secret")
                    .disableTaskSecurity()
                    // if you're sending custom class parameters, make sure that
                    // the remote client instance knows about them!
                    // .addExtraJaxbClasses(UserMessage.class)
                    .build();
            // engine.getTaskService().delegate(18, "azzaora", "test");
            // engine.getTaskService().release(18, "test");
            // Collection<ProcessInstance> pi =
            // engine.getKieSession().getProcessInstances();
            List<TaskSummary> taskSummaries = engine.getTaskService()
                    .getTasksAssignedAsPotentialOwner("t", "en-UK");
            Map<String, Object> content = engine.getTaskService()
                    .getTaskContent(taskSummaries.get(0).getId());
            Task t = engine.getTaskService().getTaskById(
                    taskSummaries.get(0).getId());

            Status s = t.getTaskData().getPreviousStatus();
            List<OrganizationalEntity> l = t.getPeopleAssignments()
                    .getPotentialOwners();
            System.out.println("connected to engin " + engine);
            // Create KieSession and TaskService instances and use them
            KieSession ksession = engine.getKieSession();
            TaskService taskService = engine.getTaskService();
            // Each operation on a KieSession, TaskService or AuditLogService
            // (client) instance
            // sends a request for the operation to the server side and waits
            // for the response
            // If something goes wrong on the server side, the client will throw
            // an exception.
            Map<String, Object> params = new HashMap<String, Object>();
            // params.put("initiator", "test");
            // params.put("dbid", "450005516777");//usermessage dbid
            // params.put("initiator_email", "test@liferay.com");
            // params.put("entityName", "UserMessage");
            String taskUserId = "Administrator";
            ProcessInstance processInstance = ksession.startProcess(
                    "com.unitedofoq.user_message.usermessage.UserMessage",
                    params);// UserMessage.UserMessageBsuinessProcess
            long procId = processInstance.getId();
            System.out.println("processid " + procId);
            taskService = engine.getTaskService();
            List<TaskSummary> tasks = taskService
                    .getTasksAssignedAsPotentialOwner(taskUserId, "en-UK");
            long taskId = -1;
            for (TaskSummary task : tasks) {
                if (task.getProcessInstanceId() == procId) {
                    taskId = task.getId();
                }
            }
            if (taskId == -1) {
                throw new IllegalStateException("Unable to find task for "
                        + taskUserId + " in process instance " + procId);
            }
            System.out.println("taskId : " + taskId);
            System.out.println("taskUserId : " + taskUserId);
            taskService.start(taskId, taskUserId);
            // resultData can also just be null
            Map<String, Object> resultData = new HashMap<String, Object>();
            taskService.complete(taskId, taskUserId, resultData);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }
}
