/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.bpm.test;

import com.unitedofoq.fabs.bpm.model.Task;
import com.unitedofoq.fabs.bpm.model.status.TaskStatus;
import java.util.List;
import com.unitedofoq.fabs.bpm.rest.services.impl.RestTaskServiceImpl;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author bgalal
 */
@RunWith(WeldJUnit4Runner.class)
public class RestTaskServiceTest {

   
   

    @Inject
    private RestTaskServiceImpl restTaskServiceImpl;

    @Test
    public void checkResponseStatusAfterClaim() {
        long taskId = 67;
        String userId = "test";
        restTaskServiceImpl.claim(taskId, userId, userId);
//        Task claimedTask = (Task) restTaskServiceImpl.getTaskById(taskId);
//        assertEquals(claimedTask.getTaskData().getStatus(), "In Progerss");
    }

    @Test
    public void checkResponseStatusAfterComplete() {
        long taskId = 67;
        String userId = "test";
        Map data = new HashMap();
        data.put("completedBy", "test");
        data.put("options", "reject");
        restTaskServiceImpl.complete(taskId, userId, data);
//        Task claimedTask = (Task) restTaskServiceImpl.getTaskById(taskId);
//        assertEquals(claimedTask.getTaskData().getStatus(), "Completed");

    }
    
    @Test
   public void getTasksAssignedAsPotentialOwner(){
       List<Task> tasks = restTaskServiceImpl.getTasksAssignedAsPotentialOwner("test");
       assert tasks!=null;
       assert tasks.get(0).getCreatorId().equals("test")||"test".equals(tasks.get(0).getInputs().get("ActorId"));
   }
    @Test
   public void getTaskByIdShouldSuccess(){
       long taskId = 53;
      TaskStatus taskStatus= restTaskServiceImpl.getTaskStatusById(taskId);
      assertEquals(taskStatus, TaskStatus.EXITED);
   }
  @Test
   public void startTask(){
       long taskId=69;
       String userId = "test";
       restTaskServiceImpl.start(taskId, userId);
       assertEquals(restTaskServiceImpl.getTaskStatusById(taskId), TaskStatus.READY);
   }

   
    @Test
    public void retrieveTaskByProcessId() {
        List<Task> processInstances  = restTaskServiceImpl.getProcessInstanceTasks("10");
        assert processInstances != null;        
    }
    
}
