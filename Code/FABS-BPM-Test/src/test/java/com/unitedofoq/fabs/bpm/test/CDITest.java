package com.unitedofoq.fabs.bpm.test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.inject.Inject;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.unitedofoq.fabs.bpm.connector.BPMServiceFactory;
import com.unitedofoq.fabs.bpm.services.TaskService;


/**
 *
 * @author bgalal
 */
@RunWith(WeldJUnit4Runner.class)
public class CDITest {

    @Inject
    BPMServiceFactory bpmServiceFactory;

    public CDITest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void injectServices() {
        TaskService service = bpmServiceFactory.getTaskServiceImpl();
        System.out.println(service.getType());

    }
}
