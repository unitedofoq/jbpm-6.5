package com.unitedofoq.fabs.bpm.test;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

public class WeldContext {

    private final WeldContainer weldContainer;

    private WeldContext() {
        final Weld weld = new Weld();
        this.weldContainer = weld.initialize();
        System.setProperty("fabsPropertyPath","/home/lap/Downloads/glassfish4/glassfish/domains/domain1/config");
    }

    public static WeldContext getInstance() {
        return WeldContext.Instance.SINGLETON;
    }

    private static final class Instance {

        static final WeldContext SINGLETON = new WeldContext();
    }

    public <T> T getBean(final Class<T> type) {
        return this.weldContainer.instance().select(type).get();
    }
}
