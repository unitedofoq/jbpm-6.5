package com.unitedofoq.otms.core.uiframework.portal;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.util.PortalUtil;
import com.unitedofoq.fabs.core.setup.FABSPropertiesServiceLocal;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

public class ReportPortlet extends GenericPortlet {

    @EJB
    FABSPropertiesServiceLocal fabsPropertiesService;
    
    @Override
    public void destroy() {
        super.destroy();
    }

    @Override
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
        if (fabsPropertiesService == null) {
            try {
                InitialContext ctx = new InitialContext();
                fabsPropertiesService = (FABSPropertiesServiceLocal) ctx.lookup(
                        "java:global/ofoq/portal/com.unitedofoq.fabs.core.setup.FABSPropertiesServiceLocal");
            } catch (NamingException ex) {
            }
        }
        String userID = "";
        String reportName = "";
        String portletTitle = "";
        String reportFormat = "";
        PrintWriter writer = null;
        try {
            userID = PortalUtil.getUser(renderRequest).getScreenName();
        } catch (Exception ex) {
        }

        if (userID.equals(""))  {
            try {
                super.doView(renderRequest, renderResponse);
            } catch (Exception ex) {
            }
            return ;
        }

        try {
            String onloadFucntion = "FABS.Portlet.resizeIframe(this);";
            String portletInsId = String.valueOf(renderRequest.getAttribute(WebKeys.PORTLET_ID));

            PortletPreferences preferences = renderRequest.getPreferences();
            reportName = preferences.getValue("reportName", null);
            portletTitle = preferences.getValue("reportTitle", null);
            reportFormat = preferences.getValue("reportFormat", null);

            renderResponse.setTitle("" + portletTitle + "");

            writer = renderResponse.getWriter();

            writer.write("<span id=\"" + portletInsId + "\"/>");

            if (reportName != null && !"".equals(reportName)) {
                writer.write("<script type=\"text/javascript\">");
                writer.write("FABS.Portlet.setWebAppContext(" + fabsPropertiesService.getContextName() + ");");
                writer.write("FABS.Portlet.create({portletId:'" + portletInsId + "',onLoad:'" + onloadFucntion + "',frameheight:'100'});");
                writer.write("FABS.Portlet.setReportSrc({portletId:'" + portletInsId + "',reportName:'" + reportName + "',reportFormat:'" + reportFormat + "'});");
                writer.write("</script>");
            }
            
        } catch (Exception ex) {
        } finally {
            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (Exception e) {
                }
            }
        }
    }

    /**
     * encodes queries so that they may be passed as URL parameters.
     *
     * SPACE(' ') = %20
     * COMMA(',') = %2C
     * EQUAL('=') = %3D
     *
     */
    private String URLEncoder(String query) {
        String encodedQuery = "";

        char [] queryChar = query.toCharArray();
        for (int i=0; i<queryChar.length; i++) {
            switch(queryChar[i]) {
                case ' ': encodedQuery += "%20";        break;
                case ',': encodedQuery += "%2C";        break;
                case '=': encodedQuery += "%3D";        break;
                default : encodedQuery += queryChar[i]; break;
            }
        }

        return encodedQuery;
    }
}
