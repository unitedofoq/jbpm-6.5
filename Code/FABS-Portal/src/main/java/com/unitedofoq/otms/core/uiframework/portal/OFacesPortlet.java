/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.core.uiframework.portal;

import com.liferay.portal.util.PortalUtil;
import com.unitedofoq.fabs.core.setup.FABSPropertiesServiceLocal;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.rmi.PortableRemoteObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author arezk
 */
public class OFacesPortlet extends GenericPortlet {

    @EJB
    FABSPropertiesServiceLocal fabsPropertiesService;
    
    final static Logger logger = LoggerFactory.getLogger(OFacesPortlet.class);


    @Override
    public void destroy() {
        super.destroy();
    }

    @Override
    @SuppressWarnings("CallToThreadDumpStack")
    protected void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {

        String sessionID = renderRequest.getPortletSession().getId();
        InitialContext ctx = null;
        try {
            ctx = new InitialContext();
        } catch (NamingException ex) {
            logger.error("Exception thrown: ",ex);
        }
        if (fabsPropertiesService == null) {
            try {
                
                fabsPropertiesService = (FABSPropertiesServiceLocal) ctx.lookup(
                        "java:global/ofoq/portal/com.unitedofoq.fabs.core.setup.FABSPropertiesServiceLocal");
            } catch (NamingException ex) {
                logger.error("Exception thrown: ",ex);
            }
        }
        String userID = "";
        String userPassword = "";
        String portletId = "";
        String screenName = "";
        PrintWriter writer = null;
        try {
            userID = PortalUtil.getUser(renderRequest).getScreenName();
            userPassword = String.valueOf(PortalUtil.getUserPassword(renderRequest));
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        }

        if (userID.equals("")) {
            try {
                super.doView(renderRequest, renderResponse);
            } catch (Exception ex) {
                logger.error("Exception thrown: ",ex);
            }
            return;
        }

        try {
            portletId = getPortletName();
            renderResponse.setContentType("text/html");
            writer = renderResponse.getWriter();
            String onloadFucntion = "FABS.Portlet.resizeMenuIframe(this);";
            writer.write("<script type=\"text/javascript\" id=\"FABSScript\" src=\"/FABS-Portal/js/fabs.js\"></script>");
            writer.write("<span id=\"" + getPortletName() + "\" class=\"iframeSpan\"/>");
            if (getInitParameter("screenName") != null && !"".equals(getInitParameter("screenName"))) {
                writer.write("<script type=\"text/javascript\">");
                writer.write("function create" + getPortletName() + "(){");
                writer.write("try{");
                writer.write("FABS.Portlet.setWebAppContext(" + fabsPropertiesService.getContextName() + ");");
                writer.write("var openedPortlet = FABS.Portlet.staticList['" + getPortletName() + "'] || 'none';");
                writer.write("if (openedPortlet == 'none'){");
                writer.write("FABS.Portlet.create({portletId:'"
                        + getPortletName() + "',onLoad:'" + onloadFucntion + "',"
                        + " fTitle:'" + getInitParameter("screenName")
                        + "'});");
                writer.write("FABS.Portlet.setSrc({screenName:'" + getInitParameter("screenName")
                        + "',screenInstId:'" + getInitParameter("screenName") // set screen isntance id
                        //with screen Name for the constant portlets as Menu, Inbox...
                        + "',portletId:'" + getPortletName() + "',viewPage:'"
                        + getInitParameter("javax.portlet.faces.defaultViewId.view") + "'});");
                writer.write("Liferay.unbind('create" + getPortletName() + "Event');");
                writer.write("}");
                writer.write("}catch(error){alert(error)}");
                writer.write("}");
                
                writer.write("Liferay.bind('create" + getPortletName() + "Event', function(event, data){"
                        + "create" + getPortletName() + "();"
                        + "});");
                writer.write("Liferay.bind('portletReady', function(data){ "
                        + "Liferay.trigger('create" + getPortletName() + "Event');");
                writer.write("});");
                writer.write("</script>");
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        } finally {
            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (Exception ex) {
                    logger.error("Exception thrown: ",ex);
                }
            }
        }
    }
}
