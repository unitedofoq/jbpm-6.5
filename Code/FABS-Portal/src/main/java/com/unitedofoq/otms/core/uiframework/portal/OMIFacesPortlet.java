/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.core.uiframework.portal;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.util.PortalUtil;
import com.unitedofoq.fabs.core.setup.FABSPropertiesServiceLocal;

/**
 *
 * @author arezk
 */
public class OMIFacesPortlet extends GenericPortlet {

    @EJB
    FABSPropertiesServiceLocal fabsPropertiesService;
    
    final static Logger logger = LoggerFactory.getLogger(OMIFacesPortlet.class);
    
    @Override
    protected void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
        logger.debug("Entering");
        if (fabsPropertiesService == null) {
            try {
                InitialContext ctx = new InitialContext();
                fabsPropertiesService = (FABSPropertiesServiceLocal) ctx.lookup(
                        "java:global/ofoq/portal/com.unitedofoq.fabs.core.setup.FABSPropertiesServiceLocal");
            } catch (NamingException ex) {
                logger.error("Exception thrown: ",ex);
            }
        }
        String userID = "";
        String screenName = "";
        String screenInstId = "";
        String screenHeight = "";
        String screenWidth = "";
        String portletTitle = "";
        String viewPage = "";
        String removeHeader = "";
        String mainPortlet = "";
        PrintWriter writer = null;
        String defaultState = "normal";
        try {
            userID = PortalUtil.getUser(renderRequest).getScreenName();
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        }

        if (userID.equals("")) {
            try {
                super.doView(renderRequest, renderResponse);
            } catch (Exception ex) {
                logger.error("Exception thrown: ",ex);
            }
            logger.debug("Retruning");
            return;
        }

        try {
            String onloadFucntion = "";
            boolean waitLoading = false;
            String enableC2A = "false";
            boolean resizeFrame = true;
            // Get Portlet Instance ID from Request Attributes
            String portletInsId = String.valueOf(renderRequest.getAttribute(WebKeys.PORTLET_ID));

            PortletPreferences preferences = renderRequest.getPreferences();
            screenName = preferences.getValue("screenName", null);
            screenInstId = preferences.getValue("screenInstId", null);
            screenHeight = preferences.getValue("screenHeight", null);
            screenWidth = preferences.getValue("screenWidth", null);
            viewPage = preferences.getValue("viewPage", null);
            portletTitle = preferences.getValue("portletTitle", null);
            removeHeader = preferences.getValue("removeHeader", null);
            mainPortlet = preferences.getValue("mainPortlet", null);
            defaultState = preferences.getValue("defaultState", "normal");
            if ("true".equalsIgnoreCase(preferences.getValue("waitLoading", "false"))) {
                waitLoading = true;
            }

            if ("child".equals(mainPortlet)) {
                enableC2A = "true";
                mainPortlet = "false";
            }
            if (("".equals(screenWidth) || screenWidth == null)) {
                screenWidth = "100%";
            }
            if ("".equals(screenHeight) || screenHeight == null) {
                onloadFucntion = "FABS.Portlet.resizeIframe(this);";
            } else {
                onloadFucntion = "";
            }

            renderResponse.setTitle("" + portletTitle + "");

            if ("MainPortlet".equals(mainPortlet)) {
                enableC2A = "true";
                mainPortlet = "true";
                removeHeader = "true";
            }
            onloadFucntion = (onloadFucntion == null || "".equals(onloadFucntion)) ? "" : onloadFucntion;
            renderResponse.setContentType("text/html");
            writer = renderResponse.getWriter();

            writer.write("<span id=\"" + portletInsId + "\" class=\"iframeSpan\"/>");
            writer.write("<script type=\"text/javascript\">");
            // TODO set WebApp Context Root
            writer.write("FABS.Portlet.setWebAppContext(\"" + fabsPropertiesService.getContextName() + "\");");
            writer.write("function " + portletInsId + "DataRestored(){}");
            writer.write("</script>");

            if (waitLoading) {
              writer.write("<script type=\"text/javascript\">");
                writer.write("FABS.Portlet.staticList['" + portletInsId + "']='" + screenInstId + "';");
                writer.write("function " + portletInsId + "DefaultSrc(){");
                writer.write("try{");
                if ("minimized".equalsIgnoreCase(defaultState)) {
                    writer.write("setTimeout(\"FABS.Portlet.minimize({portletId:'" + portletInsId + "'});\", 3000);");
                }
                writer.write("FABS.Portlet.create({portletId:'" + portletInsId
                        + "',onLoad:'" + onloadFucntion + "',fHeight:'"
                        + screenHeight + "',fWidth:'" + screenWidth + "',fTitle:'"
                        + screenName + "'});");
                writer.write("FABS.Portlet.setSrc({screenName:'" + screenName
                        + "',screenInstId:'" + screenInstId
                        + "',screenHeight:'" + screenHeight
                        + "',portletId:'" + portletInsId
                        + "',viewPage:'" + viewPage
                        + "',enableC2A:'true'});");
                writer.write("}catch(error){}}");
                writer.write("</script>");
            } else if (screenName != null && !"".equals(screenName)) {
                writer.write("<script type=\"text/javascript\">");
//                writer.write("alert('HBM Test');");
                writer.write("function create" + portletInsId + "(){");
                writer.write("try{");
                writer.write("var openedPortlet = FABS.Portlet.staticList['" + screenInstId + "'] || 'none';");
                writer.write("if (openedPortlet == 'none'){");
                if ("minimized".equalsIgnoreCase(defaultState)) {
                    writer.write("setTimeout(\"FABS.Portlet.minimize({portletId:'" + portletInsId + "'});\", 3000);");
                }
                writer.write("FABS.Portlet.create({portletId:'" + portletInsId + "',onLoad:'"
                        + onloadFucntion + "',fHeight:'" + 400 + "',fWidth:'" + screenWidth + "',fTitle:'"
                        + screenName + "'});");
                writer.write("FABS.Portlet.setSrc({screenName:'" + screenName
                        + "',screenInstId:'" + screenInstId
                        + "',screenHeight:'" + screenHeight
                        + "',portletId:'" + portletInsId
                        + "',viewPage:'" + viewPage
                        + "',main:'" + mainPortlet
                        + "',enableC2A:'" + enableC2A + "'});");
                writer.write("Liferay.unbind('create" + portletInsId + "Event');");
                writer.write("}");
                writer.write("}catch(error){alert(error);}");
                writer.write("}");
                //HBM, Check if the session is newly created, wait for Liferay to initialize
                PortletSession session = renderRequest.getPortletSession();
                if (((session.getLastAccessedTime() - session.getCreationTime())/(2 * 1000)) == 0) {
                    writer.write("setTimeout(\"create" + portletInsId+ "();\", 1000);");
                } else {
                // bind the function that will create this portlet
                writer.write("Liferay.bind('create" + portletInsId + "Event', function(data){");
                writer.write("create" + portletInsId + "();");
                writer.write("});");
                writer.write("Liferay.bind('portletReady', function(data){ "
                        + "Liferay.trigger('create" + portletInsId + "Event');");
                writer.write("});");
                }
                writer.write("</script>");
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        } finally {
            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (Exception ex) {
                    logger.error("Exception thrown: ",ex);
                }
            }
            logger.debug("Returning");
        }
    }
}
