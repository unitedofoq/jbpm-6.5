/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.core.uiframework.portal;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.util.PortalUtil;
import com.unitedofoq.fabs.core.setup.FABSPropertiesServiceLocal;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author lap3
 */
public class OrgChartPortlet extends GenericPortlet {

    @EJB
    FABSPropertiesServiceLocal fabsPropertiesService;
     
    final static Logger logger = LoggerFactory.getLogger(OrgChartPortlet.class);
    
    @Override
    public void destroy() {
        super.destroy();
    }

    @Override
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
        if (fabsPropertiesService == null) {
            try {
                InitialContext ctx = new InitialContext();
                fabsPropertiesService = (FABSPropertiesServiceLocal) ctx.lookup(
                        "java:global/ofoq/portal/com.unitedofoq.fabs.core.setup.FABSPropertiesServiceLocal");
            } catch (NamingException ex) {
                logger.error("Exception thrown: ",ex);
            }
        }
//        PortletSession portletSession = renderRequest.getPortletSession();  
//        portletSession.setAttribute("positionId", 1143173);

        String portletId = "";
        String viewPage = "";
        String entityDBID = "" ;
        String entityName = "" ;
        int portletHeight = 500;
        String screenInstId = "";
        String portletTitle = "";
        String userID = "";
        String htmlDir ="";
        String userName = "";
        String fabsWSDLURL = "";
        String otmsWSDLURL = "";
        String otmsORGURL = "";
        String entityActionName = "";

        PrintWriter writer = null;
        try {
            userID = PortalUtil.getUser(renderRequest).getScreenName();
        } catch (Exception ex) {
        }

        if (userID.equals("")) {
            try {
                super.doView(renderRequest, renderResponse);
            } catch (Exception ex) {
            }
            return;
        }

        try {
            String onloadFucntion = "FABS.Portlet.resizeIframe(this);";
            String portletInsId = String.valueOf(renderRequest.getAttribute(WebKeys.PORTLET_ID));

            PortletPreferences preferences = renderRequest.getPreferences();
            viewPage = preferences.getValue("viewPage", null);
            portletTitle = preferences.getValue("portletTitle", null);
            screenInstId = preferences.getValue("screenInstId", null);
            htmlDir = preferences.getValue("htmlDir", null);
            userName = preferences.getValue("userName", null);
            fabsWSDLURL = preferences.getValue("fabsWSDLURL", null);
            otmsWSDLURL = preferences.getValue("otmsWSDLURL", null);
            otmsORGURL = preferences.getValue("otmsORGURL", null);
            if(preferences.getValue("portletHeight", null)!= null && !preferences.getValue("portletHeight", null).equals("")){
                portletHeight = Integer.valueOf(preferences.getValue("portletHeight", null));
            }
            entityDBID = preferences.getValue("entityDBID", null);
            entityName = preferences.getValue("entityName", null);
            entityActionName = preferences.getValue("entityActionName", null);

            renderResponse.setTitle("" + portletTitle + "");

            writer = renderResponse.getWriter();

            writer.write("<span id=\"" + portletInsId + "\" class=\"iframeSpan\"/>");
            writer.write("<script type=\"text/javascript\">");
            writer.write("function " + portletInsId + "DataRestored(){}");
            writer.write("</script>");


            if (viewPage != null && !viewPage.equals("")) {
                
                writer.write("<script type=\"text/javascript\">");
                writer.write("function create" + portletInsId + "(){");
				writer.write("try{");
                writer.write("var openedPortlet = FABS.Portlet.staticList['" + screenInstId + "'] || 'none';");
                writer.write("if (openedPortlet == 'none'){");
//                if ("minimized".equalsIgnoreCase(defaultState)) {
//                    writer.write("setTimeout(\"FABS.Portlet.minimize({portletId:'" + portletInsId + "'});\", 3000);");
//                }
                writer.write("FABS.Portlet.create({portletId:'" + portletInsId + "',onLoad:'" +
                        onloadFucntion + "',fHeight:'" + portletHeight + "'});");
                writer.write("FABS.Portlet.setOrgChartSrc({portletId:'" + portletInsId 
                        + "',screenInstId:'" + screenInstId 
                        + "',htmlDir:'" + htmlDir
                        + "',userName:'" + userName 
                        + "',entityDBID:'" + entityDBID 
                        + "',entityName:'" + entityName 
                        + "',fabsWSDLURL:'" + fabsWSDLURL 
                        + "',otmsWSDLURL:'" + otmsWSDLURL 
                        + "',otmsORGURL:'" + otmsORGURL 
                        + "',viewPage:'" + viewPage 
                        + "',entityActionName:'" + entityActionName
                        + "'});");
                writer.write("Liferay.unbind('create" + portletInsId + "Event');");
                writer.write("}");
                writer.write("}catch(error){alert(error);}");
				writer.write("}");
                // bind the function that will create this portlet
                writer.write("Liferay.bind('create" + portletInsId + "Event', function(data){");
                writer.write("create" + portletInsId + "();");
                writer.write("});");
                writer.write("Liferay.bind('portletReady', function(data){ "
                        + "Liferay.trigger('create" + portletInsId + "Event');");
                writer.write("});");
                writer.write("</script>");
                
//                
//                writer.write("<script type=\"text/javascript\">");
////            writer.write("FABS.Portlet.setWebAppContext('/FABSOrgChart/');");
//                writer.write("FABS.Portlet.create({portletId:'" + portletInsId + "',onLoad:'" + onloadFucntion
//                        + "',frameheight:'300'});");
//                writer.write("FABS.Portlet.setOrgChartSrc({portletId:'" + portletInsId + "',screenInstId:'" + screenInstId
//                        + "',viewPage:'" + viewPage + "'});");
//                writer.write("</script>");
            }else {
                // Close Portlet if the instance is opened without a screen.
                writer.write("<script type=\"text/javascript\">");
                writer.write("try{");
                System.out.println("Portlet Instance : " + portletInsId + " Opened Without Screen ");
                System.out.println("Close Portlet if it isn't new (Recently Added).");
                writer.write("Liferay.bind('portletReady', function(data){ "
                        + "FABS.Portlet.close('" +  portletInsId + "');");
                writer.write("});");
                writer.write("}catch(error){}");
                writer.write("</script>");
            }

        } catch (Exception ex) {
        } finally {
            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (Exception e) {
                }
            }
        }
    }

    /**
     * encodes queries so that they may be passed as URL parameters.
     *
     * SPACE(' ') = %20 COMMA(',') = %2C EQUAL('=') = %3D
     *
     */
    private String URLEncoder(String query) {
        String encodedQuery = "";

        char[] queryChar = query.toCharArray();
        for (int i = 0; i < queryChar.length; i++) {
            switch (queryChar[i]) {
                case ' ':
                    encodedQuery += "%20";
                    break;
                case ',':
                    encodedQuery += "%2C";
                    break;
                case '=':
                    encodedQuery += "%3D";
                    break;
                default:
                    encodedQuery += queryChar[i];
                    break;
            }
        }

        return encodedQuery;
    }
}
