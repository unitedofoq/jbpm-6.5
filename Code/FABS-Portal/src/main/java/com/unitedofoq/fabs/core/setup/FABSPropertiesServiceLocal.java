/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.setup;

import javax.ejb.Local;

/**
 *
 * @author aelzaher
 */
@Local
public interface FABSPropertiesServiceLocal {
    
    public String getContextName();

    public void setContextName(String contextName);
}
