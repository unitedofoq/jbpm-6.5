/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.process;

import com.unitedofoq.fabs.core.delegation.DelegationServiceRemote;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;

import com.unitedofoq.fabs.core.process.HumanTaskServiceRemote;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mostafa
 */
@WebServlet(name = "TaskMailSender", urlPatterns = {"/TaskMailSender", "/sendMail"})
public class TaskMailSender extends HttpServlet {

    @EJB
    OEntityManagerRemote oem;
    final static Logger logger = LoggerFactory.getLogger(TaskMailSender.class);

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.debug("Entering");
        try {
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            String taskId = request.getParameter("taskId");
            String taskName = request.getParameter("taskName");
            String groupId = null;
            String loggedUserLoginName = null;
            OUser loggedUser = null;
            String actorId = request.getParameter("actorId");
            List<String> mailReceivers = new ArrayList<>();
            if (actorId.equalsIgnoreCase("null")) {
                groupId = request.getParameter("groupId");
                loggedUserLoginName = request.getParameter("loggedUser");
            }
            if (taskName != null) {
                taskName = taskName.replace("_", "-");
            }
            if (loggedUserLoginName != null) {
                loggedUser = oem.getUserForLoginName(loggedUserLoginName);
            }
            if (groupId == null || loggedUser == null) {
                mailReceivers.add(actorId);
            } else {
                mailReceivers.addAll(oem.getUsersLoginNamesForGroup(groupId, loggedUser));
            }
            for (String mailReceiver : mailReceivers) {
                TaskMailSenderThread mailSenderThread = new TaskMailSenderThread(taskId, taskName, mailReceiver);
                Thread thread = new Thread(mailSenderThread);
                thread.start();
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
