/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.process;

import com.unitedofoq.fabs.bpm.model.Task;
import com.unitedofoq.fabs.core.delegation.DelegationServiceRemote;
import com.unitedofoq.fabs.core.delegation.TaskDelegation;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;

import com.unitedofoq.fabs.core.process.HumanTaskServiceRemote;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import javax.naming.InitialContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mostafa
 */
public class TaskMailSenderThread implements Runnable {

    private OEntityManagerRemote oem;
    private DelegationServiceRemote delegationService;
    private HumanTaskServiceRemote humanTaskService;
    private String taskId;
    private String taskName;
    private String actorId;
    final static Logger logger = LoggerFactory.getLogger(TaskMailSenderThread.class);
    
    public TaskMailSenderThread(String taskId, String taskName, String actorId) {
        logger.debug("Entering");
        this.taskId = taskId;
        this.taskName = taskName;
        this.actorId = actorId;
        try {
            InitialContext ctx = new InitialContext();
            oem = (OEntityManagerRemote) ctx.lookup(
                    "java:global/ofoq/com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote");
            delegationService = (DelegationServiceRemote) ctx.lookup(
                    "java:global/ofoq/com.unitedofoq.fabs.core.delegation.DelegationServiceRemote");
            humanTaskService = (HumanTaskServiceRemote) ctx.lookup(
                    "java:global/ofoq/com.unitedofoq.fabs.core.process.HumanTaskServiceRemote");
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        }
        logger.debug("Returning");
    }

    @Override
    public void run() {
        logger.debug("Entering");
        if (!sendMailForTask()) {
            logger.warn("Couldn't send mail for task ");
        } else {
            logger.debug("Mail Sent for Task");
        }
        logger.debug("Entering");
    }

    private synchronized boolean sendMailForTask() {
        logger.trace("Entering");
        logger.trace("Starting Send Mail for Task with id {} " , taskId);
        if (taskId == null||taskName==null||actorId==null) {
            logger.trace("Returning with false");
            return false;
        }
        Task task = new Task();
        task.setId(Long.parseLong(taskId));
        task.setName(taskName);
        Map<String, Object> inputs = new HashMap<>();
        inputs.put("ActorId", actorId);
        task.setInputs(inputs);
        String taskOwner = actorId;
        List<TaskDelegation> allDelegates = delegationService.getDelegationsFromUser(0l, oem.getUserForLoginName(taskOwner));
        List<String> allDelegatesUsersLoginNames = new ArrayList<>();
        Calendar currentCal = Calendar.getInstance();
        Date currentDate = currentCal.getTime();
        for(TaskDelegation delegate : allDelegates){
            Date fromDate = delegate.getFromDate();
            Date toDate = delegate.getToDate();
            if (fromDate.before(currentDate)
                    && toDate.after(currentDate)) {
                allDelegatesUsersLoginNames.add(delegate.getDelegate().getLoginName());
            }
        }
        int successCount = 0;
        Boolean isSentToDelegates = true;
        Boolean isSentToOwner = humanTaskService.sendEMailForTask(task, oem.getUserForLoginName(taskOwner));
        if (!allDelegatesUsersLoginNames.isEmpty()) {
            for (String delegateUserLoginName : allDelegatesUsersLoginNames) {
                OUser delegateUser = oem.getUserForLoginName(delegateUserLoginName);
                if (delegateUser == null) {
                    logger.warn("User Not Found", delegateUserLoginName);
                } else {
                    Boolean isSentToDelegate = humanTaskService.sendEMailForTask(task, oem.getUserForLoginName(delegateUserLoginName));
                    isSentToDelegates = isSentToDelegate && isSentToDelegates;
                    if (!isSentToDelegate) {
                        logger.warn("No Mail was sent to User", delegateUserLoginName);
                    }
                }
            }
            if (isSentToOwner && isSentToDelegates) {
                successCount++;
            }
        } else {
            if(isSentToOwner){
                successCount++;
            }
        }
        logger.trace( "{} messages successfully sent.", successCount);
        logger.trace("Returning with true");
        return true;
    }
}