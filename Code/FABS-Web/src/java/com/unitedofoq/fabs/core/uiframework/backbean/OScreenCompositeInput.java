/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenDTMappingAttr;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenInput;

/**
 *
 * @author tarzy
 */
public class OScreenCompositeInput {
    /**
     * {@link ODataMessage} of the input
     */
    public ODataMessage odm = null ;
    /**
     * Composite attribute of the input. Holds the the valid screen inputs 
     * attributes have {@link ScreenDTMappingAttr#partOfCompKey} = true. Is
     * of {@link #input}. 
     * Filled in {@link SingleEntityBean#initCompositeInputAttrs() }
     */
    public ScreenDTMappingAttr attr = null ;
    
    /**
     * The input, owner of {@link #attr}. 
     * Filled in {@link SingleEntityBean#initCompositeInputAttrs() }
     */
    public ScreenInput input = null ;

    @Override
    public String toString() {
        return (   (attr==null? "Null attr" : attr.toString())
                +   "|" 
                +  (odm==null? "Null odm" : odm.toString())) ;
    }
}
