/**
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDLookupType;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntityServiceLocal;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.filter.OFilterServiceLocal;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.report.ReportFilter;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.screen.EntityFilter;
import com.unitedofoq.fabs.core.uiframework.screen.FormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenFilter;
import com.unitedofoq.fabs.core.uiframework.screen.SingleEntityScreen;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.utils.UIFieldUtility;
import com.unitedofoq.fabs.core.webservice.WebServiceFilter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.faces.component.UIColumn;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;
import javax.faces.event.MethodExpressionValueChangeListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectmanymenu.SelectManyMenu;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author bassem
 */
public class Lookup {

    final static Logger logger = LoggerFactory.getLogger(Lookup.class);

    private OEntityManagerRemote oem = OEJB.lookup(OEntityManagerRemote.class);

    private OFilterServiceLocal filterService = OEJB.lookup(OFilterServiceLocal.class);
    public static int numOfOpenedLookups = 0;
    public static final int CHARACTER_LENGTH_IN_PIXEL = 8;

    public int increaseLookupNumber() {
        int newLookupNumber = 0;
        synchronized (this) {
            try {
                numOfOpenedLookups++;
                newLookupNumber = numOfOpenedLookups;
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
        }
        return newLookupNumber;
    }
    public ExpressionFactory exFactory;
    public FacesContext facesContext;
    ELContext elContext = null;
    public static final String EMPTY_ITEM = "-------------------";
    private String valueChangeMethod;
    private SingleEntityBean backBean;
    private List<String> treeConditions;

    public SingleEntityBean getBackBean() {
        return backBean;
    }

    public void setBackBean(SingleEntityBean backBean) {
        this.backBean = backBean;
    }
    private ScreenField screenField;

    // <editor-fold defaultstate="collapsed" desc="dropDownMenu">
    private AutoComplete autoComplete;

    public AutoComplete getAutoComplete() {
        return autoComplete;
    }

    // </editor-fold>
    /**
     *
     * @param beanName
     * @param oScreen
     * @param fieldDD
     * @param fieldExpression
     * @param id Is set to lookupButton as an ID
     * @param fieldTabIndex
     * @param fieldIsMandatory
     * @param exFactory
     * @param facesContext
     * @param loggedUser
     */
    public Lookup(SingleEntityBean backBean, String beanName, String ownerEntityBackBeanExpression,
            DD fieldDD, String fieldExpression, String valueChangeMethod,
            String id, ScreenField screenField, Boolean fieldIsMandatory, OEntityManagerRemote entityManagerRemote,
            OFilterServiceLocal filterService, ExpressionFactory exFactory, FacesContext facesContext, OUser loggedUser) {
        this.backBean = backBean;
        this.valueChangeMethod = valueChangeMethod;
        this.facesContext = facesContext;
        this.elContext = facesContext.getELContext();
        this.oem = entityManagerRemote;
        this.exFactory = exFactory;
        this.loggedUser = loggedUser;
        this.screenField = screenField;
        this.filterService = filterService;
        this.beanName = beanName;
        this.ownerEntityBackBeanExpression = ownerEntityBackBeanExpression;
        if (fieldDD.getLookupScreen() != null && fieldDD.getLookupScreen().getDbid() != 0) {
            this.oScreen = (SingleEntityScreen) fieldDD.getLookupScreen();
        }
        this.fieldDD = fieldDD;
        this.id = id;
        this.fieldTabIndex = screenField.getSortIndex();
        this.fieldIsMandatory = fieldIsMandatory;
        this.fieldExpression = fieldExpression;
        setLookupFilter(fieldDD);
        setLookupHeader(fieldDD);
        evaluateExpLookupInOBackBeanPrefix();
    }

    /**
     *
     * @param buttonID
     * @return
     */
    public Boolean equalsByButtonID(String buttonID) {
        if (null == lookUpButton) {
            return false;
        }
        return lookUpButton.getId().equals(buttonID);
    }

    /**
     *
     * @return
     */
    public HtmlPanelGrid createNonDropDown() {
        logger.debug("Entering");
        inputText = new InputText();
        inputText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_LookupInputText");
        inputText.setSize(fieldDD.getControlSize());
        Class[] parms = new Class[]{ValueChangeEvent.class};
        MethodExpression mb1 = FacesContext.getCurrentInstance().getApplication().
                getExpressionFactory().createMethodExpression(elContext, "#{" + getBeanName()
                        + ".validateLookup}", null, parms);
        inputText.addValueChangeListener(new MethodExpressionValueChangeListener(mb1));
        inputText.setImmediate(true);
        inputText.setReadonly(!screenField.isEditable());
        // TODO: Remove the following line when the editable lookup in Form Screen works fine.
        if (backBean instanceof FormBean) {
            inputText.setReadonly(true);
        }
        if (getScreenField().isEditable()) {
            inputText.setStyleClass("editable_lookup");
        } else {
            inputText.setStyleClass("disabled_lookup");
        }

        inputText.setTabindex(Integer.toString(fieldTabIndex));
        String valueExpression;
        if (beanName.equals("CustomReportFilterBean")) {
//            valueExpression = "#{" + ownerEntityBackBeanExpression + "." + screenField.getFieldExpression() + "}";
            valueExpression = "#{" + getBeanName() + ".selectedData['"
                    + inputText.getId()
                    + "']}";
        } else {
            valueExpression = UIFieldUtility.buildScreenFieldExp("#{" + ownerEntityBackBeanExpression + "." + fieldExpression + "}", true);
        }
        inputText.setValueExpression(
                "value",
                exFactory.createValueExpression(
                        elContext,
                        valueExpression,
                        String.class));

        if (fieldIsMandatory) {
        }

        synchronized (this) {
            lookupPanelGrid = new HtmlPanelGrid();
            lookupPanelGrid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_LookupPanel");
            lookupPanelGrid.setColumns(2);
            lookupPanelGrid.setStyle("border:hidden;");
            lookupPanelGrid.getChildren().add(inputText);
            if (loggedUser.isRtl() && backBean.getCurrentScreen() instanceof FormScreen) {
                lookupPanelGrid.setStyle("margin-right: -6px!important;");
            }
            //<editor-fold defaultstate="collapsed" desc="Create lookUpButton">
            lookUpButton = new CommandButton();
            lookUpButton.setId(id + "_" + increaseLookupNumber());
            lookUpButton.setValue(" ");
            lookUpButton.setTitle(getScreenField().getFieldExpression());
            lookUpButton.setRendered(true);
            lookUpButton.setImmediate(true);
            lookUpButton.setProcess("@this");
            lookUpButton.setStyleClass("lookUpButton_btn");
            lookUpButton.setIcon("lookUpButton_btn_icon");
            Class[] params = {ActionEvent.class};
            MethodExpression me = FacesContext.getCurrentInstance().getApplication().getExpressionFactory().createMethodExpression(
                    elContext, "#{" + getBeanName() + "." + backBeanLookupListenerName + "}",
                    null, params);
            lookUpButton.addActionListener(new MethodExpressionActionListener(me));
            lookUpButton.setActionExpression(
                    exFactory.createMethodExpression(
                            elContext,
                            "#{" + getBeanName() + "." + backBeanLookupActionName + "}",
                            String.class,
                            new Class[0]));
            // Do we need this ? column.getChildren().add(lookUpButton);
            //</editor-fold>

            lookupPanelGrid.getChildren().add(lookUpButton);
        }
        lookupPanelGrid.setStyleClass("ctrlTable");

        //<editor-fold defaultstate="collapsed" desc="Create entityDBID_H">
        entityDBID_H = new HtmlInputHidden();
        entityDBID_H.setValueExpression(
                "binding",
                exFactory.createValueExpression(
                        elContext,
                        expLookupInOBackBeanPrefix + ".entityDBID_H }",
                        String.class));
        entityDBID_H.setId("entityDBID_H" + FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_entityDBIDHD");
        entityDBID_H.setValueExpression(
                "value",
                exFactory.createValueExpression(
                        elContext,
                        "#{" + ownerEntityBackBeanExpression + ".dbid}",
                        String.class));
        logger.debug("Returning");
        return lookupPanelGrid;
    }

    public List<SelectItem> getDropDownElements(DD fieldDD, EntitySetupServiceRemote entitySetupService,
            List<String> lookupFilterCond) {
        logger.debug("Entering");
        String dropDownDisplayField = fieldDD.getLookupDropDownDisplayField();
        final String dropDownDisplayFieldSimpleClassName
                = dropDownDisplayField.substring(0, dropDownDisplayField.indexOf("."));
        String colName
                = dropDownDisplayField.substring(dropDownDisplayField.indexOf(".") + 1);

        //TODO (Performance): get the class from the parseFieldExpression function instead
        Class fldClass;// = null;
        try {
            OEntityDTO fldOEntity = entitySetupService.loadOEntityDTOByClassName(
                    dropDownDisplayFieldSimpleClassName, loggedUser);
            fldClass = Class.forName(fldOEntity.getEntityClassPath());
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning with Null");
            return null;
        }
        try {
            //  the WHERE clause to be used to filter
            //  the query
            List<String> filterConditions = null;
            if (lookupFilterCond != null) {
                filterConditions = new ArrayList<String>();
                filterConditions.addAll(lookupFilterCond);
                /*
                 * If the parent that is passed is null the value of "#Parent" will still the same
                 * and will be submitted to the JPQL which will cause an error so we need to remove it.
                 * The conditions will be refill automatically with th next message
                 */
                for (int i = 0; i < filterConditions.size(); i++) {
                    if (filterConditions.get(i).contains("#Parent")) {
                        filterConditions.remove(i);
                    }

                }
            }
            List<String> fieldExprsn = new ArrayList<String>();
            fieldExprsn.add(colName);

            //  execute Query 1850000173
            //here the filterConditions is always have constant value [oscreen.dbid= 1850000194]
            manySelectList = oem.loadEntityList(
                    fldClass.getSimpleName(), filterConditions, fieldExprsn, fieldExprsn, getLoggedUser());
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            /*
             * If there is no data returned to "manySelectList" return a new emtpy return list
             * to display an empty dropdown filled only with the empty character
             * otherwise the control will not be rendered at all.
             */
            manySelectList = new ArrayList();
        }

        ArrayList<SelectItem> tempSelectItems = new ArrayList<SelectItem>();

        //  Add a 'Not Set' option
        SelectItem selectItem = new SelectItem(null, Lookup.EMPTY_ITEM);
        tempSelectItems.add(selectItem);

        for (int itemIndex = 0; itemIndex < manySelectList.size(); itemIndex++) {
            try {
                String currentItemLabel;// = "";
                currentItemLabel = (String) ((BaseEntity) manySelectList.get(itemIndex)).invokeGetter(colName);
                selectItem = new SelectItem(manySelectList.get(itemIndex), currentItemLabel);
                tempSelectItems.add(selectItem);
            } catch (NoSuchMethodException ex) {
                logger.error("Exception thrown", ex);
            }
        }
        logger.debug("Returning");
        return tempSelectItems;
    }

    /**
     *
     * @return
     */
    public HtmlPanelGrid createDropDown(boolean disabled) {
        logger.debug("Entering");
        DDLookupType ddLookupType = fieldDD.getLookupType();
        HtmlMessage message = new HtmlMessage();
        if (ddLookupType.equals(DDLookupType.DDLT_FIELDEXP)) {
            logger.warn("fieldExpression: {} Is Not Supported Yet In DropDown Mode", fieldExpression);
            logger.debug("Returning with Null");
            return null;
        }
        String defaultValue = fieldDD.getDefaultValue();
        if (!ddLookupType.equals(DDLookupType.DDLT_LTRLDRPDN)) {

            // <editor-fold defaultstate="collapsed" desc="Get Lookup Entity's Class">
            //  get class of field from which the dropdown will be filled
            //  to be used for loading values from database
            String dropDownDisplayField = fieldDD.getLookupDropDownDisplayField();
            final String dropDownDisplayFieldSimpleClassName
                    = dropDownDisplayField.substring(0, dropDownDisplayField.indexOf("."));
            String colName
                    = dropDownDisplayField.substring(dropDownDisplayField.indexOf(".") + 1);

            //TODO (Performance): get the class from the parseFieldExpression function instead
            Class fldClass = null;
            try {
                OEntityDTO fldOEntity = backBean.entitySetupService.loadOEntityDTOByClassName(
                        dropDownDisplayFieldSimpleClassName, loggedUser);
                fldClass = Class.forName(fldOEntity.getEntityClassPath());
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
                logger.debug("Returning with Null");
                return null;
            }
            //</editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Create & Fill dropdownList">
            manySelectList = new ArrayList();
            try {
                //  the WHERE clause to be used to filter
                //  the query
                List<String> filterConditions = null;
                if (getLookupFilter() != null) {
                    filterConditions = new ArrayList<String>();
                    filterConditions.addAll(getLookupFilter());
                    /*
                     * If the parent that is passed is null the value of "#Parent" will still the same
                     * and will be submitted to the JPQL which will cause an error so we need to remove it.
                     * The conditions will be refill automatically with th next message
                     */
                    for (int i = 0; i < filterConditions.size(); i++) {
                        if (filterConditions.get(i).contains("#Parent")) {
                            filterConditions.remove(i);
                        }

                    }
                }
                List<String> fieldExprsn = new ArrayList<String>();
                fieldExprsn.add(colName);

                //  execute Query
                manySelectList = oem.loadEntityList(
                        fldClass.getSimpleName(), filterConditions, fieldExprsn, fieldExprsn, getLoggedUser());
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
                /*
                 * If there is no data returned to "manySelectList" return a new emtpy return list
                 * to display an empty dropdown filled only with the empty character
                 * otherwise the control will not be rendered at all.
                 */
                manySelectList = new ArrayList();
            }
            //</editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Create & Set Value & Binding dropDownMenu">
            //  Binding...
            //  the name of the column from which the combo box will be filled with 'values'
            dropDownMenu = new SelectOneMenu();
            FABSAjaxBehavior behavior = new FABSAjaxBehavior();
            dropDownMenu.addClientBehavior("blur", behavior);
            dropDownMenu.addClientBehavior("change", behavior);
            String drpdwnID = FramworkUtilities.getID(getScreenField(), fieldDD, fieldTabIndex);
            dropDownMenu.setId(drpdwnID);
            dropDownMenu.setStyleClass("dropDownMenu");
            dropDownMenu.setFilter(true);
            dropDownMenu.setFilterMatchMode("contains");
            String valueExpression;// = null;
            if (fieldExpression != null && !fieldExpression.equals("")) {
                String fieldName = fieldExpression.substring(0, fieldExpression.lastIndexOf("."));
                valueExpression
                        = "#{" + ownerEntityBackBeanExpression + "." + fieldName + "}";
            } else {
                valueExpression = "#{" + ownerEntityBackBeanExpression + "}";
            }
            dropDownMenu.setValueExpression(
                    "value",
                    exFactory.createValueExpression(
                            FacesContext.getCurrentInstance().getELContext(),
                            valueExpression,
                            fldClass));
            if (null != valueChangeMethod && !"".equals(valueChangeMethod)) {
                Class[] parms3 = new Class[]{ValueChangeEvent.class};
                MethodExpression mb2 = FacesContext.getCurrentInstance().getApplication().getExpressionFactory()
                        .createMethodExpression(elContext, "#{" + getBeanName() + "." + valueChangeMethod + "}", null, parms3);
                dropDownMenu.addValueChangeListener(new MethodExpressionValueChangeListener(mb2));
            }
            //</editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Fill selectItems & Bind uiSelectItems">
            //Check if the field is translatable
            //Get the field
            Field colField = BaseEntity.getClassField(fldClass, colName);
            if (colField.getAnnotation(Translatable.class) != null) {
                colName = colField.getAnnotation(Translatable.class).translationField();
            }
            selectItems = new ArrayList<SelectItem>();

            //  Add a 'Not Set' option
            SelectItem selectItem = new SelectItem(null, Lookup.EMPTY_ITEM);
            selectItems.add(selectItem);

            for (int itemIndex = 0; itemIndex < manySelectList.size(); itemIndex++) {
                try {
                    String currentItemLabel;// = "";
                    currentItemLabel = (String) ((BaseEntity) manySelectList.get(itemIndex)).invokeGetter(colName);
                    selectItem = new SelectItem(manySelectList.get(itemIndex), currentItemLabel);
                    selectItems.add(selectItem);
                } catch (NoSuchMethodException ex) {
                    logger.error("Exception thrown", ex);
                }
            }
            //</editor-fold>

            if (defaultValue != null && !defaultValue.equals("")) {
                List<String> condition = new ArrayList<String>();
                condition.add(defaultValue);
                UDC udc = null;
                try {
                    udc = (UDC) oem.loadEntity(UDC.class.getSimpleName(), condition, null, loggedUser);
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }
                dropDownMenu.setValue(udc);
            }
        } else {
            // <editor-fold defaultstate="collapsed" desc="Create & Fill dropdownList">
            manySelectList = getLookupFilter();
            //</editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Create & Set Value & Binding dropDownMenu">
            //  Binding...
            //  the name of the column from which the combo box will be filled with 'values'
            dropDownMenu = new SelectOneMenu();
            FABSAjaxBehavior behavior = new FABSAjaxBehavior();
            dropDownMenu.addClientBehavior("blur", behavior);
            dropDownMenu.addClientBehavior("change", behavior);
            dropDownMenu.setDisabled(disabled);
            dropDownMenu.setStyleClass("dropDownMenu");
            dropDownMenu.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_DrpDown");
            dropDownMenu.setFilter(true);
            dropDownMenu.setFilterMatchMode("contains");
            String valueExpression;
            if (fieldExpression != null && !fieldExpression.equals("")) {
                valueExpression = "#{" + ownerEntityBackBeanExpression + "." + fieldExpression + "}";
            } else {
                valueExpression = "#{" + ownerEntityBackBeanExpression + "}";
            }

            dropDownMenu.setValueExpression(
                    "value",
                    exFactory.createValueExpression(
                            FacesContext.getCurrentInstance().getELContext(),
                            valueExpression,
                            String.class));
            if (null != valueChangeMethod && !"".equals(valueChangeMethod)) {
                Class[] parms3 = new Class[]{ValueChangeEvent.class};
                MethodExpression mb2 = FacesContext.getCurrentInstance().getApplication().
                        getExpressionFactory().createMethodExpression(elContext,
                                "#{" + getBeanName() + "." + valueChangeMethod + "}", null, parms3);
                dropDownMenu.addValueChangeListener(new MethodExpressionValueChangeListener(mb2));
            }
            //</editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Fill selectItems & Bind uiSelectItems">
            dropDownMenu.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_DrpDown");
            selectItems = new ArrayList<SelectItem>();
            selectItems.add(new SelectItem(null, Lookup.EMPTY_ITEM));
            for (int itemIndex = 0; itemIndex < manySelectList.size(); itemIndex++) {
                String key = manySelectList.get(itemIndex).toString();
                if (key.contains(",")) {
                    selectItems.add(new SelectItem(key.substring(key.indexOf(",") + 1).trim(), key.substring(0, key.indexOf(","))));
                } else {
                    selectItems.add(new SelectItem(key, key));
                }
            }
            //</editor-fold>

            if (defaultValue != null && !defaultValue.equals("")) {
                dropDownMenu.setValue(defaultValue);
            }
        }
        // <editor-fold defaultstate="collapsed" desc="Create uiSelectItems">
        UISelectItems uiSelectItems = new UISelectItems();
        uiSelectItems.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_uiItems");
        uiSelectItems.setValueExpression(
                "value",
                exFactory.createValueExpression(
                        FacesContext.getCurrentInstance().getELContext(),
                        "#{" + getBeanName() + ".selectItemsList['"
                        + dropDownMenu.getId()
                        + "']}",
                        Object.class));
        //</editor-fold>

        dropDownMenu.getChildren().add(uiSelectItems);

        // <editor-fold defaultstate="collapsed" desc="dropDownMenu Converters(), needs selectItems">
        FramworkUtilities.setControlFldExp(dropDownMenu, fieldExpression);

        Class[] parms = new Class[]{ValueChangeEvent.class};
        MethodExpression methodBinding = FacesContext.getCurrentInstance().getApplication().
                getExpressionFactory().createMethodExpression(elContext, "#{" + getBeanName() + ".updateRow}", null, parms);
        dropDownMenu.addValueChangeListener(new MethodExpressionValueChangeListener(methodBinding));

        FABSDropDownConverter converter = new FABSDropDownConverter(selectItems);
        dropDownMenu.setConverter(converter);
        //</editor-fold>
        UserMessage userMessage = null;
        if (fieldIsMandatory) {
            dropDownMenu.setRequired(true);
            try {
                userMessage = backBean.usrMsgService.getUserMessage("MandatoryField", loggedUser);
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
            message.setTitle(userMessage.getMessageTitleTranslated());
            message.setStyleClass("lkp-message-error-detail");
            message.setFor(dropDownMenu.getId());
            dropDownMenu.setRequiredMessage(userMessage.getMessageTextTranslated());

        }
        dropDownMenu.setStyle("width:" + this.fieldDD.getControlSize() * CHARACTER_LENGTH_IN_PIXEL + "px;");
        synchronized (this) {
            lookupPanelGrid = new HtmlPanelGrid();
            lookupPanelGrid.setCellpadding("0px");
            lookupPanelGrid.setCellspacing("0px");
            lookupPanelGrid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_LookupPanel");
            if (fieldIsMandatory) {
                lookupPanelGrid.getChildren().add(dropDownMenu);
                lookupPanelGrid.getChildren().add(message);
            } else {

                lookupPanelGrid.getChildren().add(dropDownMenu);
            }
        }
        logger.debug("Returning");
        return lookupPanelGrid;
    }

    public HtmlPanelGrid createAutoComplete(boolean disabled) {
        HtmlMessage message = new HtmlMessage();

        // <editor-fold defaultstate="collapsed" desc="Create & Fill AutoCompleteList">
        String dropDownDisplayField = fieldDD.getLookupDropDownDisplayField();
        final String dropDownDisplayFieldSimpleClassName
                = dropDownDisplayField.substring(0, dropDownDisplayField.indexOf("."));
        String colName
                = dropDownDisplayField.substring(dropDownDisplayField.indexOf(".") + 1);

        //TODO (Performance): get the class from the parseFieldExpression function instead
        Class fldClass = null;
        try {
            OEntityDTO fldOEntity = backBean.entitySetupService.loadOEntityDTOByClassName(
                    dropDownDisplayFieldSimpleClassName, loggedUser);
            fldClass = Class.forName(fldOEntity.getEntityClassPath());
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
            return null;
        }
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Create & Fill AutoCompleteList">
        manySelectList = new ArrayList();
        try {
            //  the WHERE clause to be used to filter the query
            List<String> filterConditions = null;
            if (getLookupFilter() != null) {
                filterConditions = new ArrayList<String>();
                filterConditions.addAll(getLookupFilter());

                for (int i = 0; i < filterConditions.size(); i++) {
                    if (filterConditions.get(i).contains("#Parent")) {
                        filterConditions.remove(i);
                    }

                }
            }
            List<String> fieldExprsn = new ArrayList<String>();
            fieldExprsn.add(colName);

            //  execute Query
            manySelectList = oem.loadEntityList(
                    fldClass.getSimpleName(), filterConditions, fieldExprsn, fieldExprsn, getLoggedUser());
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Ecxeption thrown: ", ex);
            // </editor-fold>
            manySelectList = new ArrayList();
        }

        String atocompltID = FramworkUtilities.getID(getScreenField(), fieldDD, fieldTabIndex);
        autoComplete = (AutoComplete) FacesContext.getCurrentInstance().
                getApplication().createComponent(AutoComplete.COMPONENT_TYPE);
        autoComplete.setId(atocompltID);
        String valueExpression = null;
        String fieldName = null;
        if (fieldExpression != null && !fieldExpression.equals("") && fieldExpression.lastIndexOf(".") != -1) {
            fieldName = fieldExpression.substring(0, fieldExpression.lastIndexOf("."));
            valueExpression
                    = "#{" + ownerEntityBackBeanExpression + "." + fieldName + "}";
        } else if (fieldExpression.lastIndexOf(".") == -1) {
            fieldName = fieldExpression;
            valueExpression
                    = "#{" + ownerEntityBackBeanExpression + "." + fieldName + "}";
        } else {
            valueExpression = "#{" + ownerEntityBackBeanExpression + "}";
        }

        autoComplete.setValueExpression("value", exFactory.createValueExpression(elContext,
                valueExpression, fldClass));//"#{autoCompleteView.theme1}"
        autoComplete.setForceSelection(true);
        String var = "entity";
        autoComplete.setVar(var);//"theme"
        autoComplete.setCompleteMethod(exFactory.createMethodExpression(elContext,
                "#{" + getBeanName() + ".completeList}", List.class, new Class[]{String.class}));
        autoComplete.setValueExpression("itemLabel", exFactory.createValueExpression(elContext,
                "#{" + var + "." + colName + "}", String.class));//"#{theme.displayName}"
        autoComplete.setValueExpression("itemValue", exFactory.createValueExpression(elContext,
                "#{" + var + "}", fldClass));
        autoComplete.setMaxResults(3);
        // <editor-fold defaultstate="collapsed" desc="Fill selectItems & Bind uiSelectItems">
        selectItems = new ArrayList<SelectItem>();
        for (int itemIndex = 0; itemIndex < manySelectList.size(); itemIndex++) {
            try {
                BaseEntity selObject = (BaseEntity) manySelectList.get(itemIndex);
                if (null == selObject.invokeGetter(colName)) {
                    continue;
                }
                selectItems.add(new SelectItem(selObject, selObject.invokeGetter(colName).toString()));
            } catch (NoSuchMethodException ex) {
                logger.error("Exception thrown: ", ex);
            }
        }

        // <editor-fold defaultstate="collapsed" desc="Create uiSelectItems">
        UISelectItems uiSelectItems = new UISelectItems();
        uiSelectItems.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_uiItems");
        uiSelectItems.setValueExpression(
                "value",
                exFactory.createValueExpression(
                        FacesContext.getCurrentInstance().getELContext(),
                        "#{" + getBeanName() + ".autoCompleteList['"
                        + autoComplete.getId()
                        + "']}",
                        Object.class));
        //</editor-fold>

        autoComplete.getChildren().add(uiSelectItems);

        // <editor-fold defaultstate="collapsed" desc="dropDownMenu Converters(), needs selectItems">
        final List<SelectItem> SelectItems = selectItems;

        FramworkUtilities.setControlFldExp(autoComplete, fieldExpression);

        FABSAutoCompleteConverter converter = new FABSAutoCompleteConverter(SelectItems);
        autoComplete.setConverter(converter);
        //</editor-fold>

        synchronized (this) {
            lookupPanelGrid = new HtmlPanelGrid();
            lookupPanelGrid.setCellpadding("0px");
            lookupPanelGrid.setCellspacing("0px");
            lookupPanelGrid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_LookupPanel");
            if (fieldIsMandatory) {
                lookupPanelGrid.getChildren().add(autoComplete);
                lookupPanelGrid.getChildren().add(message);
            } else {

                lookupPanelGrid.getChildren().add(autoComplete);
            }
        }
        return lookupPanelGrid;
    }

    public HtmlPanelGrid createMultiSelectionList() {
        logger.debug("Entering");
        DDLookupType ddLookupType = fieldDD.getLookupType();
        HtmlMessage message = new HtmlMessage();
        if (!ddLookupType.equals(DDLookupType.DDLT_MULTISELECTIONLIST)) {
            logger.warn("fieldExpression: {} Is Not Supported Yet In DropDown Mode", fieldExpression);
            logger.debug("Returning with Null");
            return null;
        }

        // <editor-fold defaultstate="collapsed" desc="Get Lookup Entity's Class">
        //  get class of field from which the dropdown will be filled
        //  to be used for loading values from database
        String manyListDisplayField = fieldDD.getLookupDropDownDisplayField();
        final String dropDownDisplayFieldSimpleClassName
                = manyListDisplayField.substring(0, manyListDisplayField.indexOf("."));
        String colName
                = manyListDisplayField.substring(manyListDisplayField.indexOf(".") + 1);

        //TODO (Performance): get the class from the parseFieldExpression function instead
        Class fldClass;// = null;
        try {
            OEntityDTO fldOEntity = backBean.entitySetupService.
                    loadOEntityDTOByClassName(dropDownDisplayFieldSimpleClassName,
                            oem.getSystemUser(loggedUser));
            fldClass = Class.forName(fldOEntity.getEntityClassPath());
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning with Null");
            return null;
        }
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Create & Fill manySelectList">
        manySelectList = new ArrayList();
        try {
            //  the WHERE clause to be used to filter
            //  the query
            List<String> filterConditions;//= new ArrayList();
            filterConditions = filterService.parseDDLookupFilter(fieldDD.getLookupFilter(), loggedUser);
            List<String> fieldExprsn = new ArrayList<String>();
            fieldExprsn.add(colName);

            //  execute Query
            manySelectList = oem.loadEntityList(
                    fldClass.getSimpleName(), filterConditions, fieldExprsn, fieldExprsn, getLoggedUser());
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning with Null");
            return null;
        }
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Create & Set Value & Binding manySelectListBox">
        //  Binding...
        //  the name of the column from which the combo box will be filled with 'values'
        manySelectListbox = new SelectManyMenu();
        manySelectListbox.setId(id + "_DDMenu_" + increaseLookupNumber() + "_" + getFieldExpression().replace(".", "_"));
        manySelectListbox.setStyle("width: 100%;");

        manySelectListbox.setImmediate(false);
        String valueExpression;// = null;
        if (fieldExpression != null && !fieldExpression.equals("")) {
            String fieldName = fieldExpression.substring(0, fieldExpression.lastIndexOf("."));
            valueExpression
                    = "#{" + ownerEntityBackBeanExpression + "." + fieldName + "}";
        } else {
            valueExpression = "#{" + ownerEntityBackBeanExpression + "}";
        }
        manySelectListbox.setValueExpression(
                "value",
                exFactory.createValueExpression(
                        FacesContext.getCurrentInstance().getELContext(),
                        valueExpression,
                        fldClass));
        if (null != valueChangeMethod && !"".equals(valueChangeMethod)) {
            Class[] parms3 = new Class[]{ValueChangeEvent.class};
            MethodExpression mb2 = FacesContext.getCurrentInstance().getApplication().
                    getExpressionFactory().createMethodExpression(elContext,
                            "#{" + getBeanName() + "." + valueChangeMethod + "}", null, parms3);
            manySelectListbox.addValueChangeListener(new MethodExpressionValueChangeListener(mb2));
        }
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Fill selectItems & Bind uiSelectItems">
        selectItems = new ArrayList<SelectItem>();

        //  Add a 'Not Set' option
        SelectItem selectItem = new SelectItem(null, Lookup.EMPTY_ITEM);
        selectItems.add(selectItem);
        for (int itemIndex = 0; itemIndex < manySelectList.size(); itemIndex++) {
            try {
                String currentItemLabel;// = "";
                currentItemLabel = (String) ((BaseEntity) manySelectList.get(itemIndex)).invokeGetter(colName);
                selectItem = new SelectItem(manySelectList.get(itemIndex), currentItemLabel);
                selectItems.add(selectItem);
            } catch (NoSuchMethodException ex) {
                logger.error("Exception thrown", ex);
            }
        }
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Create uiSelectItems">
        UISelectItems uiSelectItems = new UISelectItems();
        uiSelectItems.setValueExpression(
                "value",
                exFactory.createValueExpression(
                        FacesContext.getCurrentInstance().getELContext(),
                        "#{" + getBeanName() + ".selectItemsList['"
                        + manySelectListbox.getId()
                        + "']}",
                        Object.class));
        //</editor-fold>

        manySelectListbox.getChildren().add(uiSelectItems);

        UserMessage userMessage = null;
        if (fieldIsMandatory) {
            manySelectListbox.setRequired(true);
            try {
                userMessage = backBean.usrMsgService.getUserMessage("MandatoryField", loggedUser);
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
            message.setTitle(userMessage.getMessageTitleTranslated());
            message.setStyle("color:red");
            message.setFor(manySelectListbox.getId());
            manySelectListbox.setRequiredMessage(userMessage.getMessageTitleTranslated());
        }
        synchronized (this) {
            if (fieldIsMandatory) {
                lookupPanelGrid = new HtmlPanelGrid();
                lookupPanelGrid.setCellpadding("0px");
                lookupPanelGrid.setCellspacing("0px");
                lookupPanelGrid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_LookupPanel");

                lookupPanelGrid.getChildren().add(manySelectListbox);
                lookupPanelGrid.getChildren().add(message);

            } else {
                lookupPanelGrid.getChildren().add(manySelectListbox);
                //return lookupPanelGrid;
            }
        }
        logger.debug("Returning");
        return lookupPanelGrid;
    }
    // <editor-fold defaultstate="collapsed" desc="OBackBeanLookupHashName">
    private String OBackBeanLookupHashName = "Lookups";

    public String getOBackBeanLookupHashName() {
        return OBackBeanLookupHashName;
    }

    public void setOBackBeanLookupHashName(String OBackBeanLookupHashName) {
        this.OBackBeanLookupHashName = OBackBeanLookupHashName;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="expLookupInOBackBeanPrefix">
    private String expLookupInOBackBeanPrefix;

    private void evaluateExpLookupInOBackBeanPrefix() {
        if (beanName != null && id != null) {
            expLookupInOBackBeanPrefix = "#{" + beanName
                    + "." + OBackBeanLookupHashName + "['" + id + "']";
        }
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="id">
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
        evaluateExpLookupInOBackBeanPrefix();
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="beanName">
    private String beanName;

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
        evaluateExpLookupInOBackBeanPrefix();
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="oScreen">
    private SingleEntityScreen oScreen;

    public SingleEntityScreen getOScreen() {
        return oScreen;
    }

    public void setOScreen(SingleEntityScreen oScreen) {
        this.oScreen = oScreen;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fieldDD">
    private DD fieldDD;

    public DD getFieldDD() {
        return fieldDD;
    }

    public void setFieldDD(DD fieldDD) {
        this.fieldDD = fieldDD;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fieldExpression">
    private String fieldExpression;

    public String getFieldExpression() {
        return fieldExpression;
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="selectItems">
    private List<SelectItem> selectItems;

    public List<SelectItem> getSelectItems() {
        return selectItems;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fieldTabIndex">
    private int fieldTabIndex;

    public int getFieldTabIndex() {
        return fieldTabIndex;
    }

    public void setFieldTabIndex(int fieldTabIndex) {
        this.fieldTabIndex = fieldTabIndex;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fieldIsMandatory">
    private Boolean fieldIsMandatory;

    public Boolean getFieldIsMandatory() {
        return fieldIsMandatory;
    }

    public void setFieldIsMandatory(Boolean fieldIsMandatory) {
        this.fieldIsMandatory = fieldIsMandatory;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="loggedUser">
    private OUser loggedUser;

    public OUser getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(OUser loggedUser) {
        this.loggedUser = loggedUser;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ownerFieldBackBeanExpression">
    /**
     * Is a full expression of the field in the BackBean for which the lookup is
     * displayed, i.e. it should be {@link ownerEntityBackBeanExpression}.field;
     * i.e. it's not relative to {@link ownerEntityBackBeanExpression}
     */
    private String ownerFieldBackBeanExpression;

    public String getOwnerFieldBackBeanExpression() {
        return ownerFieldBackBeanExpression;
    }

    public void setOwnerFieldBackBeanExpression(String ownerFieldBackBeanExpression) {
        this.ownerFieldBackBeanExpression = ownerFieldBackBeanExpression;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ownerEntityBackBeanExpression">
    /**
     * Is expression of the BackBean-field that holds the entity (driven from
     * BaseEntity); for which the Lookup is displayed <br>Example: <br>*
     * currentRow: Tabular Field
     */
    private String ownerEntityBackBeanExpression;

    /**
     * getter of {@link #ownerEntityBackBeanExpression}
     */
    public String getOwnerEntityBackBeanExpression() {
        return ownerEntityBackBeanExpression;
    }

    /**
     * setter of {@link #ownerEntityBackBeanExpression}
     */
    public void setOwnerEntityBackBeanExpression(String ownerEntityBackBeanFieldExpression) {
        this.ownerEntityBackBeanExpression = ownerEntityBackBeanFieldExpression;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lookupPanelGrid">
    private HtmlPanelGrid lookupPanelGrid = null;

    public HtmlPanelGrid getLookupPanelGrid() {
        return lookupPanelGrid;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="inputText">
    private InputText inputText = null;

    public InputText getInputText() {
        return inputText;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lookUpButton">
    private CommandButton lookUpButton = null;

    public CommandButton getLookUpButton() {
        return lookUpButton;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="messageGrid">
    private HtmlPanelGrid messageGrid;

    public HtmlPanelGrid getMessageGrid() {
        return messageGrid;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entityDBID_H">
    private HtmlInputHidden entityDBID_H;

    public HtmlInputHidden getEntityDBID_H() {
        return entityDBID_H;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="messageGrid">
    private String backBeanLookupListenerName = "lookupListener";

    public String getBackBeanLookupListenerName() {
        return backBeanLookupListenerName;
    }

    public void setBackBeanLookupListenerName(String backBeanLookupListenerName) {
        this.backBeanLookupListenerName = backBeanLookupListenerName;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="backBeanLookupActionName">
    private String backBeanLookupActionName = "lookup";

    public String getBackBeanLookupActionName() {
        return backBeanLookupActionName;
    }

    public void setBackBeanLookupActionName(String backBeanLookupActionName) {
        this.backBeanLookupActionName = backBeanLookupActionName;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dropDownMenu">
    private SelectManyMenu manySelectListbox;

    public SelectManyMenu getManyListbox() {
        return manySelectListbox;
    }

    public void setManyListbox(SelectManyMenu manyListbox) {
        this.manySelectListbox = manyListbox;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dropDownMenu">
    private SelectOneMenu dropDownMenu;

    public SelectOneMenu getDropDownMenu() {
        return dropDownMenu;
    }
    // </editor-fold>
    private List multiSelectionList;

    public List getMultiSelectionList() {
        return multiSelectionList;
    }

    public void setMultiSelectionList(List multiSelectionList) {
        this.multiSelectionList = multiSelectionList;
    }
    // <editor-fold defaultstate="collapsed" desc="dropdownList">
    /**
     * List of items to be represented in the dropdown
     */
    private List manySelectList;

    public List getDropdownList() {
        return manySelectList;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Lookup Filter">
    private List<String> lookupFilter = new ArrayList<String>();

    public void setLookupFilter(List<String> lookupFilter) {
        this.lookupFilter = lookupFilter;
    }

    public void buildLookupFilter() {
        setLookupFilter(fieldDD);
        loadLookUpList(lookupFilter);
    }

    private void setLookupFilter(DD fieldDD) {
        if (fieldDD.getLookupFilter() != null) {
            /*  if (beanName.equals("CustomReportFilterFieldsTabularBean")) {
             fieldDD.setLookupFilterTranslated(fieldDD.getLookupFilter());
             }*/
            lookupFilter = filterService.parseDDLookupFilter(fieldDD.getLookupFilter(), loggedUser);
            BaseEntity screenParent;// = null;

            screenParent = backBean.getHeaderObject();

            for (int fltrIdx = 0; fltrIdx < lookupFilter.size(); fltrIdx++) {
                if (lookupFilter.get(fltrIdx).contains("#Parent") && screenParent != null) {
                    lookupFilter.set(fltrIdx, lookupFilter.get(fltrIdx).replaceAll("#ParentEntityName", screenParent.getClassName()) //Replcae the reserved Keyword "#ParentEntityName"
                            .replaceAll("#Parent", " #Parent") //Replcae the reserved Keyword "#Related"
                            .replaceAll("\\(", " \\( ") //Surround braket "(" with spaces so as to split the string with spaces
                            .replaceAll("\\)", " \\) ") //Surround braket ")" with spaces so as to split the string with spaces
                    );
                    //Splitting The string with spaces
                    String[] result = lookupFilter.get(fltrIdx).split(" ");

                    String setupFilterJPQL = "";
                    for (int i = 0; i < result.length; i++) {
                        String string = result[i];
                        //Reassembling the string with the actual values
                        if (string.matches("#Parent[(.)[\\w]*]*")) {//The regular expression to get any condition related to the parent
                            string = string.replace("#Parent.", "");
                            //Replcae with the corresponding value
                            setupFilterJPQL += " " + backBean.getValueFromEntity(screenParent, string);
                        } else {
                            //put the string as it is
                            setupFilterJPQL += " " + string;
                        }
                    }
                    lookupFilter.set(fltrIdx, setupFilterJPQL);

                }

            }
        }
    }

    public List<String> getLookupFilter() {
        return lookupFilter;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Lookup Header">
    String lookupHeader;

    private void setLookupHeader(DD fieldDD) {
        lookupHeader = (fieldDD != null) ? fieldDD.getLabelTranslated() : "";
    }

    public String getLookupHeader() {
        return lookupHeader;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Lookup Screen Utilities">
    // <editor-fold defaultstate="collapsed" desc="Build Lookup Table">
    public DataTable buildLookupTable(String tableVariable, String valueExpression,
            UIColumn levelsColumn, Class lkpClass) {
        DataTable lookupTable = new DataTable();
        logger.debug("Returning");
        return lookupTable;
    }
//</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Build Entity Tree Browser">

    public HtmlPanelGrid buildEntityTreeBrowser(EntitySetupServiceRemote entitySetupService,
            EntityServiceLocal entityServiceLocal, UIFrameworkServiceRemote uiFrameWorkFacade) {
        logger.debug("Entering");
        loadLookUpList(lookupFilter);
        logger.debug("Returning");
        return new HtmlPanelGrid();
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Build Lookup Panel">
    public OEntityDTO getFieldExpressionTreeOEntity(BaseEntity parentObject, String actOnEntityClsName,
            DDServiceRemote ddService, EntitySetupServiceRemote entitySetupService,
            EntityServiceLocal entityServiceLocal,
            UIFrameworkServiceRemote uiFrameWorkFacade) {
        logger.debug("Entering");
        try {
            OEntityDTO entity = null;
            setLookupFilter(fieldDD);
            for (String filter : lookupFilter) {
                if (filter.contains("this")) {
                    logger.debug("Returning");
                    return backBean.entitySetupService.loadOEntityDTOForTree(backBean.getLookupConditionValue(
                            filter.substring(filter.indexOf(".") + 1)), loggedUser);
                } else if (filter.contains("parent")) {
                    if (parentObject instanceof ReportFilter) {
                        logger.debug("Returning");
                        return backBean.entitySetupService.getOReportEntityDTO(
                                ((ReportFilter) parentObject).getReport().getDbid(), loggedUser);
                    } else if (parentObject instanceof ScreenFilter) {
                        logger.debug("Returning");
                        return backBean.entitySetupService.getOScreenEntityDTO(
                                ((SingleEntityScreen) ((ScreenFilter) parentObject).getScreen()).getDbid(), loggedUser);
                    } else if (parentObject instanceof WebServiceFilter) {
                        logger.debug("Returning");
                        return backBean.entitySetupService.getWebServiceEntityDTO(
                                ((WebServiceFilter) parentObject).getService().getServiceFilter().getDbid(), loggedUser);
                    } else if (parentObject instanceof EntityFilter) {
                        return backBean.entitySetupService.loadOEntityDTOByDBID(((EntityFilter) parentObject).getOentity().getDbid(), loggedUser);
                    }

                }
            }
            logger.debug("Returning");
            return entity;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    public HtmlPanelGrid buildLookupPanelPopup(BaseEntity parentObject, String actOnEntityClsName,
            DDServiceRemote ddService, EntitySetupServiceRemote entitySetupService,
            EntityServiceLocal entityServiceLocal,
            UIFrameworkServiceRemote uiFrameWorkFacade) {
        logger.debug("Entering");
        setLookupFilter(fieldDD);
        String valueExpression;
        String tableVariable;
        UIColumn levelsColumn = null;

        // <editor-fold defaultstate="collapsed" desc="Tree Browser Entity Fields Tree...">
        if (getFieldDD().getControlType().getDbid() == DD.CT_LOOKUPATTRIBUTEBROWSER) {
            for (String filter : lookupFilter) {
            }

        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Entity Tree Browser...">
        if (getFieldDD().getControlType().getDbid() == DD.CT_LOOKUPTREE) {
            return buildEntityTreeBrowser(entitySetupService, entityServiceLocal, uiFrameWorkFacade);
        }
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="if the lookup screen is not specified, Return Empty Panel">
        if (getOScreen() == null) {
            return new HtmlPanelGrid();
        }
        // </editor-fold>

        try {
            backBean.lkbClass = Class.forName(
                    backBean.entitySetupService.getOScreenEntityDTO(getOScreen().getDbid(), loggedUser)
                    .getEntityClassPath());
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }

        HtmlPanelGrid lookupPanel = new HtmlPanelGrid();
        lookupPanel.setCellpadding("0px");
        lookupPanel.setCellspacing("0px");

        lookupPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "lookupPanel");
        lookupPanel.setColumns(1);
        lookupPanel.setWidth("100%");

        HtmlPanelGrid middlePanel = new HtmlPanelGrid();
        middlePanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "middlePanel");
        middlePanel.setStyle("position:relative;border:hidden;");
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Multi-Level Screen">
        if (getFieldDD().getLookupType() == DDLookupType.DDLT_MLTILVLSCRN) {
            backBean.mainElements = new ArrayList<SelfRelationTreeElement>();
            //Check if the looked up field class is the same type as current oEntity

            //Get the orginal entity list
            List<BaseEntity> allChildrenList = loadLookUpList(lookupFilter);

            BaseEntity parentElement = null;
            //There are should be two field one list of children and one as parent.
            List<Field> fields = BaseEntity.getClassObjectTypeFields(backBean.lkbClass, backBean.lkbClass);

            for (Field field : fields) {
                if (field.getType().equals(List.class)) {
                    backBean.childFLD = field;
                } else {
                    backBean.parentFLD = field;
                }
            }
            //Is there childern for the current entity

            for (BaseEntity orginalEntity : allChildrenList) {
                SelfRelationTreeElement element = new SelfRelationTreeElement();
                List<BaseEntity> childrens;// = null;

                try {
                    //Get the parent
                    parentElement = (BaseEntity) oem.executeEntityQuery("SELECT o." + backBean.parentFLD.getName()
                            + " FROM " + backBean.lkbClass.getSimpleName()
                            + " o WHERE o.dbid = " + orginalEntity.getDbid() + "L", getLoggedUser());
                    if (parentElement != null) {
                        element.setParentElement(parentElement);
                    }
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                    parentElement = null;
                }
                try {
                    //Get the children
                    childrens = (List<BaseEntity>) oem.executeEntityListQuery("SELECT o"
                            + " FROM " + backBean.lkbClass.getSimpleName()
                            + " o WHERE o." + backBean.parentFLD.getName() + ".dbid = " + orginalEntity.getDbid(), getLoggedUser());
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                    childrens = null;
                }
                if (childrens != null && !childrens.isEmpty()) {
                    element.setHasLevels(true);
                    element.setChildrenElemnt(childrens);
                    element.setImage("/resources/tree_nav_top_open_no_siblings.gif");
                } else {
                    element.setHasLevels(false);
                    element.setImage("/resources/tree_line_blank.gif");
                }
                element.setRelationElement(orginalEntity);
                //mainElements will hold the elements whose has no parents at first.
                if (parentElement == null) {
                    backBean.mainElements.add(element);
                }

            }
            // <editor-fold defaultstate="collapsed" desc="Levels Column">
            levelsColumn = new UIColumn();
            levelsColumn.setId("levelColumn");
            CommandButton levelsImgBtn = new CommandButton();
            levelsImgBtn.setId("LvlImgBtn");

            levelsImgBtn.setValueExpression("image", exFactory.createValueExpression(FacesContext.getCurrentInstance().getELContext(),
                    "#{currentRow.image}", String.class));

            Class[] parms3 = new Class[]{ActionEvent.class};
            MethodExpression mb1 = FacesContext.getCurrentInstance().getApplication().
                    getExpressionFactory().createMethodExpression(elContext, "#{" + getBeanName()
                            + ".toggleSubGroupAction}", null, parms3);
            levelsImgBtn.addActionListener(new MethodExpressionActionListener(mb1));
            levelsColumn.getChildren().add(levelsImgBtn);
            //</editor-fold>
            valueExpression = "#{" + getBeanName() + ".mainElements}";
            tableVariable = ".relationElement";
        } //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Regular Lookup Screen">
        else {
            setLookupFilter(fieldDD);
            backBean.setLookupList(loadLookUpList(getLookupFilter()));
            valueExpression = "#{" + getBeanName() + ".lookupList}";
            tableVariable = "";
        }
        // </editor-fold>
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Return Selection Button">
        CommandButton returnSelectedButton = new CommandButton();
        returnSelectedButton.setImmediate(true);
        String returnSelectedButtonDD = null, returnEmptyButtonDD = null, cancelButtonDD = null;
        returnSelectedButton.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "returnSelectedBTN");
        try {
            returnSelectedButtonDD = backBean.ddService.getDDLabel("returnSelectedButton", loggedUser);
            cancelButtonDD = backBean.ddService.getDDLabel("cancelButton", loggedUser);
            returnEmptyButtonDD = backBean.ddService.getDDLabel("returnEmptyButton", loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        returnSelectedButton.setValue(returnSelectedButtonDD);
        returnSelectedButton.setActionExpression(exFactory.createMethodExpression(
                FacesContext.getCurrentInstance().getELContext(), "#{" + getBeanName()
                + ".returnSelected}", String.class, new Class[0]));
        middlePanel.getChildren().add(returnSelectedButton);
        CommandButton returnEmptyButton = new CommandButton();
        returnEmptyButton.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "returnEmptyBTN");
        returnEmptyButton.setValue(returnEmptyButtonDD);
        returnEmptyButton.setImmediate(true);
        returnEmptyButton.setActionExpression(exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(), "#{" + getBeanName()
                + ".returnEmpty}", String.class, new Class[0]));
        middlePanel.getChildren().add(returnEmptyButton);
        CommandButton cancelButton = new CommandButton();
        cancelButton.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "cancelBTN");
        cancelButton.setValue(cancelButtonDD);
        cancelButton.setImmediate(true);
        cancelButton.setActionExpression(exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(), "#{" + getBeanName()
                + ".exitLookup}", String.class, new Class[0]));
        middlePanel.getChildren().add(cancelButton);
        //</editor-fold>

        DataTable lookupTable = buildLookupTable(tableVariable, valueExpression, levelsColumn, backBean.lkbClass);
        lookupTable.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        HtmlPanelGrid filterControlsPanel = new HtmlPanelGrid();
        filterControlsPanel.setCellpadding("0px");
        filterControlsPanel.setCellspacing("0px");

        filterControlsPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "lookupFilterPanel");
        filterControlsPanel.setStyle("height:30px;");
        middlePanel.getChildren().add(filterControlsPanel);
        middlePanel.getChildren().add(lookupTable);
        lookupPanel.getChildren().add(middlePanel);
        logger.debug("Returning");
        return lookupPanel;
    }
    //</editor-fold>
    //</editor-fold>

    protected List<BaseEntity> loadLookUpList(List<String> conditions) {
        logger.trace("Entering");
        List<String> filteredConditions = new ArrayList<String>();
        filteredConditions.addAll(conditions);
        String entityName;

        entityName = backBean.entitySetupService.getOScreenEntityDTO(
                getOScreen().getDbid(), loggedUser).getEntityClassName();
        try {
            for (int index = 0; index < filteredConditions.size(); index++) {
                if (filteredConditions.get(index).contains("this")) {
                    filteredConditions.set(index,
                            filteredConditions.get(index).replace("this.", ""));

                    String originalCondtExp = filteredConditions.get(index).substring(filteredConditions.get(index).indexOf("=") + 1);
                    String condtExp = filteredConditions.get(index).substring(filteredConditions.get(index).indexOf("=") + 1);
                    int operationValue = 0;

                    if (condtExp.contains("+")) {
                        String lhs = condtExp.substring(0, condtExp.indexOf("+")).trim();
                        String rhs = condtExp.substring(condtExp.indexOf("+") + 1).trim();
                        try {
                            operationValue = Integer.parseInt(rhs);
                            condtExp = lhs;
                        } catch (Exception e) {
                            condtExp = rhs;
                            operationValue = Integer.parseInt(lhs);
                        }
                    } else if (condtExp.contains("-")) {
                        String lhs = condtExp.substring(0, condtExp.indexOf("-")).trim();
                        String rhs = condtExp.substring(condtExp.indexOf("-") + 1).trim();

                        try {
                            operationValue = Integer.parseInt(rhs);
                            condtExp = lhs;

                        } catch (Exception e) {
                            condtExp = rhs;
                            operationValue = Integer.parseInt(lhs);
                        }
                    }

                    String condtValue = backBean.getLookupConditionValue(condtExp);
                    if (condtValue == null) {
                        filteredConditions.remove(index);
                        continue;
                    }
                    if (filteredConditions.get(index).contains("+")) {
                        condtValue = new Integer(Integer.parseInt(condtValue) + operationValue).toString();
                    } else if (filteredConditions.get(index).contains("-")) {
                        condtValue = new Integer(Integer.parseInt(condtValue) - operationValue).toString();
                    }

                    filteredConditions.set(index, filteredConditions.get(index).replace("=" + originalCondtExp, "=" + condtValue));

                }
            }

            List<ScreenField> screenFields = getOScreen().getScreenFields();
            Iterator screenFieldsIterator = screenFields.iterator();

            if (filteredConditions != null && filteredConditions.size() >= 1) {
                if (filteredConditions.contains("##AB")) {
                    filteredConditions.remove("##AB");
                }
            }
            treeConditions = new ArrayList<String>();
            treeConditions.addAll(filteredConditions);
            SessionManager.addSessionAttribute("lookupConditions", filteredConditions);
            logger.debug("Returning with Null");
            return null;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Returning with Null");
            return null;
        }
    }

    /**
     * Determine the field expression entities that should be updated based on
     * the DD lookup specifications <br>e.g. returns "employeeMainPostion" for
     * DD of lookup type = DDLT_DRPDWN
     * screenFieldExpression="employeeMainPostion.position.nameTranslated &
     * dd.lookupDropDownDisplayedField="Position.name"
     *
     * @param screenFieldExpression
     * @param dd
     * @return screenFieldExpression if: <br>1. dd lookup type != DDLT_DRPDWN
     * <br>2. Error, or LookupDropDownDisplayField is null
     * <br>DBUpdatableEntityFieldExp otherwise
     */
    public static String getDBUpdatableEntityFieldExp(String screenFieldExpression, DD dd) {
        logger.debug("Entering");
        if (dd.getLookupType() != DDLookupType.DDLT_DRPDWN) {
            return screenFieldExpression;
        }
        String dropDownDisplayField = dd.getLookupDropDownDisplayField();
        if (dropDownDisplayField == null) {
            logger.debug("Returning with: {}", screenFieldExpression);
            return screenFieldExpression;
        }

        // Parse the expressions
        // Note that dropDownDisplayField is not necessarily part of screenFieldExpression
        // as per the example in the function javadoc, so, we can't simply substring
        // dropDownDisplayField from screenFieldExpression
        int dotsCountInFieldExp = FramworkUtilities.countOccurancesOf(screenFieldExpression, ".");
        int dotsCountInDDDispField = FramworkUtilities.countOccurancesOf(dropDownDisplayField, ".");
        if (dotsCountInFieldExp <= dotsCountInDDDispField || dotsCountInFieldExp == 0) {
            logger.warn("Not Supported DropDown Field: {}", dropDownDisplayField);
            logger.debug("Returning");
            return screenFieldExpression;
        }
        // Get the affected entities expression
        String dbUpdateableFldExp = "";
        String tempRHSScreenFldExp = screenFieldExpression;
        for (int parsedDotsCount = 0;
                parsedDotsCount < (dotsCountInFieldExp - dotsCountInDDDispField);
                parsedDotsCount++) {
            int dotIndex = tempRHSScreenFldExp.indexOf(".");
            if (dotIndex < 0) {
                logger.warn("Error Parsing Field Expression: {}", screenFieldExpression);
                logger.debug("Returning");
                return screenFieldExpression;
            }
            if (!"".equals(dbUpdateableFldExp)) {
                dbUpdateableFldExp += ".";
            }
            dbUpdateableFldExp += tempRHSScreenFldExp.substring(0, dotIndex);
            tempRHSScreenFldExp = tempRHSScreenFldExp.substring(dotIndex + 1,
                    tempRHSScreenFldExp.length());
        }
        logger.debug("Returning");
        return dbUpdateableFldExp;
    }

    public List getLookupFilterForRefresh(DD fieldDD, OFilterServiceLocal filterService, BaseEntity screenParent) {
        logger.debug("Entering");
        List<String> filterCondition = new ArrayList();
        if (fieldDD.getLookupFilter() != null) {
            filterCondition = filterService.parseDDLookupFilter(fieldDD.getLookupFilter(), loggedUser);

            for (int fltrIdx = 0; fltrIdx < filterCondition.size(); fltrIdx++) {
                if (filterCondition.get(fltrIdx).contains("#Parent") && screenParent != null) {
                    filterCondition.set(fltrIdx, filterCondition.get(fltrIdx).replaceAll("#ParentEntityName", screenParent.getClassName()) //Replcae the reserved Keyword "#ParentEntityName"
                            .replaceAll("#Parent", " #Parent") //Replcae the reserved Keyword "#Related"
                            .replaceAll("\\(", " \\( ") //Surround braket "(" with spaces so as to split the string with spaces
                            .replaceAll("\\)", " \\) ") //Surround braket ")" with spaces so as to split the string with spaces
                    );
                    //Splitting The string with spaces
                    String[] result = filterCondition.get(fltrIdx).split(" ");

                    String setupFilterJPQL = "";
                    for (int i = 0; i < result.length; i++) {
                        String string = result[i];
                        //Reassembling the string with the actual values
                        if (string.matches("#Parent[(.)[\\w]*]*")) {//The regular expression to get any condition related to the parent
                            string = string.replace("#Parent.", "");
                            //Replcae with the corresponding value
                            setupFilterJPQL += " " + backBean.getValueFromEntity(screenParent, string);
                        } else {
                            //put the string as it is
                            setupFilterJPQL += " " + string;
                        }
                    }
                    filterCondition.set(fltrIdx, setupFilterJPQL);

                }

            }
        }
        logger.debug("Returning");
        return filterCondition;
    }

    /**
     * @return the screenField
     */
    public ScreenField getScreenField() {
        return screenField;
    }

    /**
     * @param screenField the screenField to set
     */
    public void setScreenField(ScreenField screenField) {
        this.screenField = screenField;
    }
}
