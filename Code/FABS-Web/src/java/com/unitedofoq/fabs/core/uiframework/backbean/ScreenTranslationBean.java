/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.i18n.TextTranslationServiceRemote;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.navigationmenu.NavMenuManagerImpl;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.el.MethodBinding;
import javax.faces.event.ValueChangeEvent;
import org.primefaces.component.column.Column;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.context.RequestContext;

/**
 *
 * @author mibrahim
 */
@ManagedBean(name = "ScreenTranslationBean")
@ViewScoped
public class ScreenTranslationBean extends TabularBean {

    
    TextTranslationServiceRemote textTranslationService = OEJB.lookup(TextTranslationServiceRemote.class);
    HtmlPanelGrid grid = new HtmlPanelGrid();
    private OEntityDTO transOEntity;
    private List<BaseEntity> beanTranslations = new ArrayList<BaseEntity>();
    private List<Field> transFLDs;
    private List<BaseEntity> targetEntities;

    public List<BaseEntity> getBeanTranslations() {
        return beanTranslations;
    }

    public void setBeanTranslations(List<BaseEntity> beanTranslations) {
        this.beanTranslations = beanTranslations;
    }

    public OEntityDTO getTransEntity() {
        return transOEntity;
    }

    public void setTransEntity(OEntityDTO transEntity) {
        this.transOEntity = transEntity;
    }

    /**
     * Creates a new instance of ScreenTranslationBean
     */
    public ScreenTranslationBean() {
    }

    @Override
    protected String getBeanName() {
        return ScreenTranslationBean.class.getSimpleName();
    }

    @Override
    public HtmlPanelGrid buildInnerScreen() {
        logger.debug("Entering");
        try {
            HtmlPanelGrid buttonsPanel = new HtmlPanelGrid();
            grid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_transGrid");

            buttonsPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_btnTransGrid");
            buttonsPanel.setColumns(2);
            buttonsPanel.setCellpadding("5px");
            buttonsPanel.setCellspacing("0px");

            CommandButton saveButton = new CommandButton();
            saveButton.setActionExpression(exFactory.createMethodExpression(elContext,
                    "#{" + getBeanName() + ".saveAction}", String.class, new Class[0]));

            saveButton.setValue(ddService.getDD("saveButton", loggedUser).getLabelTranslated());

            buttonsPanel.getChildren().add(saveButton);

            grid.setColumns(1);
            grid.setCellpadding("5px");
            grid.setCellspacing("0px");
            grid.getChildren().add(messagesPanel.createMessagePanel());
            //<editor-fold defaultstate="collapsed" desc="Load the entities and the translation">
            processInput();
            BaseEntity transEntity = (BaseEntity) Class.forName(transOEntity.getEntityClassPath()).newInstance();
            transFLDs = transEntity.getTranslatableFields();
            /*
             * check if the there one to one relation with other entity & cascade persist
             * so as to manage the tanslation of the relational entity transaltable fields
             */
            // transFLDs.addAll(transEntity.getRelationalTranslatableFields());
            if (null == transFLDs
                    || transFLDs.isEmpty()) {
                UserMessage errorMsg = usrMsgService.getUserMessage(
                        "NoTranslationFound", loggedUser);
                errorMsg.setMessageType(UserMessage.TYPE_WARNING);
                messagesPanel.addMessages(errorMsg);
                logger.debug("Returning");
                return grid;
            }
            beanTranslations = loadBeanTranslations();
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Build Translation Table">
            DataTable table = new DataTable();
            table.setId(getCurrentScreen().getName() + "_gt");
            table.setValueExpression("value",
                    exFactory.createValueExpression(elContext,
                            "#{" + getBeanName() + ".beanTranslations}", ArrayList.class));
            table.setVar("currentRow");
            table.setStyle("position:relative;");
            table.setRows(7);
            //For Paginator
            table.setPaginatorPosition("bottom");
            table.setPaginator(true);
            table.setPaginatorTemplate("{CurrentPageReport}  {FirstPageLink} {PreviousPageLink} {PageLinks} "
                    + "{NextPageLink} {LastPageLink} {RowsPerPageDropdown}");
            //Get the translatable fields
            String ddName = null;
            for (int colIdx = 0; colIdx < transFLDs.size() * 2; colIdx++) {
                Column transColumn = new Column();
                transColumn.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_colno" + colIdx);
                HtmlOutputLabel transHeader = new HtmlOutputLabel();
                transHeader.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_fld" + colIdx);
                if (colIdx % 2 == 0) {
                    ddName = transEntity.getFieldDDName(
                            transFLDs.get(colIdx / 2).getAnnotation(Translatable.class).translationField());

                    transHeader.setValue(ddService.getDD(ddName, loggedUser).getHeader());
                    transColumn.setHeader(transHeader);

                    HtmlOutputLabel defaultLangLbl = new HtmlOutputLabel();
                    defaultLangLbl.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_dfltLBL" + colIdx);

                    defaultLangLbl.setValueExpression("value", exFactory.createValueExpression(elContext,
                            "#{currentRow." + transFLDs.get(colIdx / 2).getName() + "}", String.class));

                    transColumn.getChildren().add(defaultLangLbl);
                } else {
                    transHeader.setValue(ddService.getDD(ddName, loggedUser).getHeaderTranslated());
                    transColumn.setHeader(transHeader);
                    Class[] parms = new Class[]{ValueChangeEvent.class};
                    MethodBinding methodBinding = getFacesContext().getApplication().
                            createMethodBinding("#{" + getBeanName() + ".updateRow}", parms);
                    InputText transText = (InputText) createTextFieldControl(20,
                            FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_transVal" + colIdx,
                            "#{currentRow."
                            + transFLDs.get((colIdx - 1) / 2).getAnnotation(Translatable.class).translationField()
                            + "}",
                            true, null, 2, false, false, false);
                    transText.setValueChangeListener(methodBinding);
                    FramworkUtilities.setControlFldExp(transText,
                            transFLDs.get((colIdx - 1) / 2).getAnnotation(Translatable.class).translationField());
                    transColumn.getChildren().add(transText);

                }
                table.getChildren().add(transColumn);
            }
            //</editor-fold>

            grid.getChildren().add(buttonsPanel);
            grid.getChildren().add(table);
            onloadJSStr += "top." + getPortletInsId() + "DataRestored();";
            onloadJSStr += "top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');";
            logger.debug("Returning");
            return grid;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    @Override
    protected boolean processInput() {
        logger.trace("Entering");
        // super.processInput();
        if (dataMessage == null) {
            logger.trace("Returning with False as Data Message equlas Null");
            return false;
        }
        try {
            oem.getEM(systemUser);
            transOEntity = entitySetupService.loadOEntityDTOByDBID(
                    Long.parseLong(dataMessage.getData().get(0).toString()),
                    loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            oem.closeEM(systemUser);
        }
        logger.trace("Returning with True");
        return true;
    }

    private List<BaseEntity> loadBeanTranslations() {
        try {
            List<String> conditions = new ArrayList<String>();
            List<String> sorts = new ArrayList<String>();
            conditions.addAll((ArrayList<String>) dataMessage.getData().get(1));
            sorts.addAll((ArrayList<String>) dataMessage.getData().get(2));
            oem.getEM(systemUser);
            // Get the orginal entities
            targetEntities = oem.loadEntityList(transOEntity.getEntityClassName(),
                    conditions, null, sorts, loggedUser);

            for (BaseEntity entity : targetEntities) {
                textTranslationService.forceLoadEntityTranslation(entity, loggedUser);
            }
            setLoadedList(targetEntities);
            return targetEntities;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            return new ArrayList<BaseEntity>();
        } finally {
            oem.closeEM(systemUser);
        }
    }

    @Override
    public String saveAction() {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            for (BaseEntity entityTranslation : beanTranslations) {
                if (!entityTranslation.isChanged()) {
                    continue;
                }
                textTranslationService.updateEntityTranslation(entityTranslation, true, loggedUser);
            }
            oFR.addSuccess(usrMsgService.getUserMessage("DataSaved", loggedUser));
            if (beanTranslations.get(0).getClassName().contains("Function")
                    || beanTranslations.get(0).getClassName().equals("OMenu")) {
                NavMenuManagerImpl.getSingleInstance().onUpdateTranslation(null);
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            messagesPanel.clearAndDisplayMessages(oFR);
            RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
            oem.closeEM(systemUser);
            logger.debug("Returning with Null");
            return null;
        }
    }

    @Override
    public void updateRow(ValueChangeEvent vce) {
        logger.debug("Entering");
        UIComponent contolComponent = vce.getComponent();
        String clientID = contolComponent.getClientId(getContext());
        String notJavaID = clientID.substring(0,
                clientID.lastIndexOf(":" + contolComponent.getId()));
        int currentRow = Integer.parseInt(notJavaID.substring(notJavaID.lastIndexOf(":") + 1));
        Object oldValueObj = vce.getOldValue();
        Object newValueObj = vce.getNewValue();
        if (currentRow >= loadedList.size()) {
            logger.warn("Current Row index is greater than the loaded List Size");
            logger.debug("Returning");
            return;
        }
        BaseEntity currentRowObject = (BaseEntity) loadedList.get(currentRow);
        currentRowObject.setChanged(!oldValueObj.equals(newValueObj));
    }
}
