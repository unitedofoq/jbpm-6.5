/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.i18n.TextTranslationServiceRemote;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.screen.FormScreen;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import java.lang.reflect.Field;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.el.MethodBinding;
import javax.faces.event.ValueChangeEvent;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.graphicimage.GraphicImage;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author lap3
 */
@ManagedBean(name = "FormScreenTranslationBean")
@ViewScoped
public class FormScreenTranslationBean extends FormBean {
final static Logger logger = LoggerFactory.getLogger(FormScreenTranslationBean.class);
    
    TextTranslationServiceRemote textTranslationService = OEJB.lookup(TextTranslationServiceRemote.class);
    private BaseEntity translationEntity;
    private List<Field> transFLDs;
    private HtmlPanelGrid grid = new HtmlPanelGrid();

    @Override
    protected String getBeanName() {
        return FormScreenTranslationBean.class.getSimpleName();
    }

    @Override
    public HtmlPanelGrid buildInnerScreen() {
        logger.debug("Entering");
        try {
            grid.setStyleClass("mainTable");
            grid.setColumns(1);
            grid.setCellpadding("0px");
            grid.setCellspacing("0px");
            grid.getChildren().add(messagesPanel.createMessagePanel());

            grid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_transGrid");
            HtmlPanelGrid buttonsPanel = new HtmlPanelGrid();
            buttonsPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_btnTransGrid");
            buttonsPanel.setColumns(1);
            buttonsPanel.setCellpadding("5px");
            buttonsPanel.setCellspacing("0px");
            CommandButton saveButton = new CommandButton();
            saveButton.setActionExpression(exFactory.createMethodExpression(elContext,
                    "#{" + getBeanName() + ".save}", String.class, new Class[0]));
            saveButton.setValue(ddService.getDD("saveButton", loggedUser).getLabelTranslated());
            buttonsPanel.getChildren().add(saveButton);
            processInput();
            transFLDs = getTranslationEntity().getTranslatableFields();
            if (null == transFLDs
                    || transFLDs.isEmpty()) {
                UserMessage errorMsg = usrMsgService.getUserMessage(
                        "NoTranslationFound", loggedUser);
                errorMsg.setMessageType(UserMessage.TYPE_WARNING);
                messagesPanel.addMessages(errorMsg);
                logger.debug("Returning");
                return grid;
            }

            loadEntityTranslations();

            HtmlPanelGrid middlePanel = new HtmlPanelGrid();
            middlePanel.setId(getFacesContext().getViewRoot().createUniqueId() + "MDPanel" + getCurrentScreen().getName());
            middlePanel.setColumns(((FormScreen) getCurrentScreen()).getColumnsNo() * 2);

            String ddName = null;
            for (int colIdx = 0; colIdx < transFLDs.size(); colIdx++) {
                //create label for original field value
                ddName = translationEntity.getFieldDDName(
                        transFLDs.get(colIdx).getAnnotation(Translatable.class).translationField());

                HtmlOutputLabel fldNameLbl = new HtmlOutputLabel();
                fldNameLbl.setValue(ddService.getDD(ddName, loggedUser).getHeader());
                fldNameLbl.setStyleClass("labelTxt");
                middlePanel.getChildren().add(fldNameLbl);

                HtmlPanelGrid panelGrid = new HtmlPanelGrid();
                panelGrid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "MGrid");
                panelGrid.setColumns(2);

                GraphicImage arabicFlg = new GraphicImage();
                GraphicImage usFlg = new GraphicImage();
                arabicFlg.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_arabLang");
                usFlg.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_usLang");

                arabicFlg.setUrl("/resources/flag_icon_egypt.gif");
                arabicFlg.setStyleClass("translation-flag");
                usFlg.setUrl("/resources/flag-icon-us.gif");
                usFlg.setStyleClass("translation-flag");

                HtmlOutputLabel defaultLangLbl = new HtmlOutputLabel();
                defaultLangLbl.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_dfltLBL" + colIdx);

                defaultLangLbl.setValueExpression("value", exFactory.createValueExpression(elContext,
                        "#{" + getBeanName() + ".loadedObject." + transFLDs.get(colIdx).getName() + "}", String.class));
                defaultLangLbl.setStyleClass("labelTxt");
                panelGrid.getChildren().add(defaultLangLbl);
                panelGrid.getChildren().add(usFlg);

                //create text for translated value
                Class[] parms = new Class[]{ValueChangeEvent.class};
                MethodBinding methodBinding = getFacesContext().getApplication().
                        createMethodBinding("#{" + getBeanName() + ".updateRow}", parms);
                InputText transText = (InputText) createTextFieldControl(20,
                        FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_transVal" + colIdx,
                        "#{" + getBeanName() + ".loadedObject."
                        + transFLDs.get(colIdx).getAnnotation(Translatable.class).translationField()
                        + "}",
                        true, null, 2, false, false , false);
                transText.setValueChangeListener(methodBinding);
                panelGrid.getChildren().add(transText);
                panelGrid.getChildren().add(arabicFlg);
                middlePanel.getChildren().add(panelGrid);
            }

            grid.getChildren().add(middlePanel);
            grid.getChildren().add(buttonsPanel);
            onloadJSStr += "top." + getPortletInsId() + "DataRestored();";
            onloadJSStr += "top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');";
            logger.debug("Returning");
            return grid;
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        logger.debug("Returning with Null");
        return null;
    }

    @Override
    protected boolean processInput() {
        if (dataMessage == null) {
            return false;
        }
        setTranslationEntity((BaseEntity) dataMessage.getData().get(0));
        return true;
    }

    private void loadEntityTranslations() {
        logger.debug("Entering");
        try {
            textTranslationService.forceLoadEntityTranslation(getTranslationEntity(), loggedUser);
            setLoadedObject(translationEntity);
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        logger.debug("Returning");
    }

    @Override
    public String save() {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            if (!translationEntity.isChanged()) {
                logger.debug("Returning with empty string");
                return "";
            }
            
            textTranslationService.updateEntityTranslation(translationEntity,true,loggedUser);

            oFR.addSuccess(usrMsgService.getUserMessage("DataSaved", loggedUser));

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            messagesPanel.clearAndDisplayMessages(oFR);
            RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
            oem.closeEM(systemUser);
            logger.debug("Returning with Null");
            return null;
        }
    }

    /**
     * @return the translationEntity
     */
    public BaseEntity getTranslationEntity() {
        return translationEntity;
    }

    /**
     * @param translationEntity the translationEntity to set
     */
    public void setTranslationEntity(BaseEntity translationEntity) {
        this.translationEntity = translationEntity;
    }

    @Override
    public void updateRow(ValueChangeEvent vce) {
        logger.debug("Entering");
        UIComponent contolComponent = vce.getComponent();
        String clientID = contolComponent.getClientId(getContext());
        String notJavaID = clientID.substring(0,
                clientID.lastIndexOf(":" + contolComponent.getId()));
        Object oldValueObj = vce.getOldValue();
        Object newValueObj = vce.getNewValue();
        logger.trace("Value Changed Action Information");
        translationEntity.setChanged(!oldValueObj.equals(newValueObj));
        logger.debug("Returning");
    }
}
