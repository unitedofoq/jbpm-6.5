/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.entitydesigner.dto;

import java.io.IOException;

import java.util.Hashtable;

import javax.faces.component.FacesComponent;

import javax.faces.component.UIComponentBase;

import javax.faces.context.FacesContext;

import javax.faces.context.ResponseWriter;

@FacesComponent(value = "dictionary")
public class RelationComponent extends UIComponentBase {

    Hashtable dictionary = new Hashtable();

    @Override
    public String getFamily() {

        return "translatorComponent";

    }

    public RelationComponent() {

       

    }

    @Override
    public void encodeBegin(FacesContext context) throws IOException {
        String value = (String) getAttributes().get("value");

        if (value != null) {

            ResponseWriter writer = context.getResponseWriter();

            Object translation = dictionary.get(value);

            if (translation == null) {

                writer.write("Sorry word not found!");

            } else {

                writer.write((String) translation);

            }

        }

    }
}