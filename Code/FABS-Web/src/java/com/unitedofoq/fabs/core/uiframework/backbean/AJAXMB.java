/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.liferay.portal.model.Layout;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDCServiceRemote;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalPage;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import com.unitedofoq.fabs.core.uiframework.portal.PortalPageFunction;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author htawab
 */
@ManagedBean(name = "AJAXMB")
@RequestScoped
public class AJAXMB {
final static Logger logger = LoggerFactory.getLogger(AJAXMB.class);
    
    private OEntityManagerRemote oem = OEJB.lookup(OEntityManagerRemote.class);
    
    private UDCServiceRemote uDCService = OEJB.lookup(UDCServiceRemote.class);
    
    private DataTypeServiceRemote dataTypeService = OEJB.lookup(DataTypeServiceRemote.class);
    
    private OFunctionServiceRemote functionService = OEJB.lookup(OFunctionServiceRemote.class);
    
    private UIFrameworkServiceRemote uiFrameWorkFacade = OEJB.lookup(UIFrameworkServiceRemote.class);
    
    private OCentralEntityManagerRemote ocem = OEJB.lookup(OCentralEntityManagerRemote.class);

    /**
     * Creates a new instance of AJAXMB
     */
    public AJAXMB() {
        logger.debug("Entering");
        Map extContextReqParm = SessionManager.getRequestParameterMap();
        String action = (String) extContextReqParm.get("ajaxaction");
        if ("removePortlet".equals(action)) {
            logger.trace("Removing Portlet");
            String portletId, screenInstId;
            portletId = (String) extContextReqParm.get("portletId");
            screenInstId = (String) extContextReqParm.get("screenInstId");
            logger.trace("Before Reomve Portlet From Server List of Portlets: {}, "
                    + "Portlet ID from Request: {}, ScreenInstId: {}",portletId,screenInstId);
            OPortalUtil.removePortlet(portletId, screenInstId);
            logger.debug("Returning, Portlet Removed");
            return;
        }
        if ("signout".equals(action)) {
            logger.debug("Signing Out");
            HttpServletRequest request = (HttpServletRequest) SessionManager.getExternalContext().getRequest();

            request.getSession().invalidate();

            OPortalUtil.removeAllPortlets(extContextReqParm);
            Set<String> keySet = extContextReqParm.keySet();
            ArrayList<String> keys = new ArrayList<String>();
            keys.addAll(keySet);
            Iterator<String> iterator = keys.iterator();
            while (iterator.hasNext()) {
                String string = iterator.next();
                if (string != null && string.contains(OPortalUtil.PAGESCREENS)) {
                    SessionManager.getSessionMap().put(string, null);
                }
            }
            try {
                request = (HttpServletRequest) SessionManager.getExternalContext().getRequest();
                request.getSession(false).invalidate();
                Cookie[] cookies = request.getCookies();
                if (cookies != null) {
                    for (Cookie cookie : cookies) {
                        cookie.setMaxAge(0);
                    }
                }
            } catch (Exception ex) {
                logger.error("Exception thrown",ex);
            }
            logger.debug("Returning, Signed Out");
            return;
        }
        if ("prepareMsgForOrg".equals(action)) {
            logger.debug("Entering, Message For Org");
            String screenInstId = ((String) extContextReqParm.get("screenInstId"));
            String entityDBID = ((String) extContextReqParm.get("entityDBID"));
            String userId = ((String) extContextReqParm.get("userId"));
            String funId = ((String) extContextReqParm.get("funDBID"));
            if (entityDBID != null && !entityDBID.equals("") && !entityDBID.equals("undefined")) {
                addMessage(entityDBID, userId, screenInstId);
            }
        }
        if ("setPortletPre".equals(action)) {
            logger.debug("Setting Portlet Prefences");

            String portletId = ((String) extContextReqParm.get("portletId"));
            String screenName = ((String) extContextReqParm.get("screenName"));
            String screenInstId = ((String) extContextReqParm.get("screenInstId"));
            String portletTitle = ((String) extContextReqParm.get("pTitle"));
            String viewPage = ((String) extContextReqParm.get("viewPage"));
            String screenH = ((String) extContextReqParm.get("screenHeight"));
            String removeH = ((String) extContextReqParm.get("removeH"));
            String mainP = ((String) extContextReqParm.get("mainP"));
            String entityDBID = ((String) extContextReqParm.get("entityDBID"));
            String userId = ((String) extContextReqParm.get("userId"));
            String funId = ((String) extContextReqParm.get("funDBID"));
            if (entityDBID != null && !entityDBID.equals("") && !entityDBID.equals("undefined")) {
                addMessage(entityDBID, userId, screenInstId);
            }

            OPortalUtil.movePortlet(portletId);
            OPortalUtil.setPortletPreferences(portletId, screenName, screenInstId, portletTitle, viewPage, screenH, removeH, mainP, null, "false");

            logger.debug("Returning, Portlet Prefrences Setted");
            return;
        }
        if ("setReportPortletPre".equals(action)) {
logger.trace("Setting Report Portlet Prefences");
            String portletId = ((String) extContextReqParm.get("portletId"));
            String reportName = ((String) extContextReqParm.get("reportName"));
            String reportTitle = ((String) extContextReqParm.get("reportTitle"));
            String reportFormat = ((String) extContextReqParm.get("reportFormat"));

            OPortalUtil.setReportPortletPreferences(portletId, reportName, reportFormat, reportTitle, null);

            // <editor-fold defaultstate="collapsed" desc="Log">
           logger.trace("AJAXMB: After Setting Portlet Pref");
            // </editor-fold>
           logger.debug("Returning");
            return;
        }
        if ("setOrgChartPortletPre".equals(action)) {

            String portletId = ((String) extContextReqParm.get("portletId"));
            String viewPage = ((String) extContextReqParm.get("viewPage"));
            String screenInstId = ((String) extContextReqParm.get("screenInstId"));
            String portletTitle = ((String) extContextReqParm.get("portletTitle"));
            String htmlDir = ((String) extContextReqParm.get("htmlDir"));
            String userName = ((String) extContextReqParm.get("userName"));
            String fabsWSDLURL = ((String) extContextReqParm.get("fabsWSDLURL"));
            String otmsWSDLURL = ((String) extContextReqParm.get("otmsWSDLURL"));
            String otmsORGURL = ((String) extContextReqParm.get("otmsORGURL"));
            String entityActionName = ((String) extContextReqParm.get("entityActionName"));
            int portletHeight = ((Integer) extContextReqParm.get("portletHeight"));
            Long entityDBID = ((Long) extContextReqParm.get("entityDBID"));

//            OPortalUtil.setOrgChartPortletPreferences(portletId, viewPage, screenInstId, portletTitle,
//                    entityDBID, portletHeight, null, htmlDir, userName, fabsWSDLURL, otmsWSDLURL);
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.trace("AJAXMB: After Setting OrgChart Portlet Pref");
            // </editor-fold>
            return;
        }
        if ("openOrgChart".equals(action)) {
            try {
                logger.debug("Opening Org Chart");
                HashMap<String, Object> params = new HashMap<String, Object>();
                String funDBID = ((String) extContextReqParm.get("funDBID"));
                String userName = ((String) extContextReqParm.get("userName"));
                String entityDBID = ((String) extContextReqParm.get("entityDBID"));
                String entityType = ((String) extContextReqParm.get("entityType"));

                OUser loggedUser = getOEntityManager().getUserForLoginName(userName);

                BaseEntity entity = (BaseEntity) getOEntityManager().loadEntity(
                        entityType, Collections.singletonList("dbid = " + entityDBID), null, loggedUser);

                PortalPageFunction portalPageFunction = (PortalPageFunction) getOEntityManager().loadEntity(
                        OFunction.class.getSimpleName(), Collections.singletonList("dbid = " + funDBID), null, loggedUser);

                OPortalPage portalPage = (OPortalPage) oem.loadEntity(OPortalPage.class.getSimpleName(),
                        portalPageFunction.getPortalPage().getDbid(),
                        null, null, loggedUser);
                ODataMessage message = null;
                if (portalPageFunction.getOdataType() == null) {
                    // Send Void DBID by default
                    ODataType voidDataType = getDataTypeSevice().loadODataType("VoidDT", loggedUser);
                    message = new ODataMessage();
                    message.setData(new ArrayList<Object>());
                    message.setODataType(voidDataType);
                    message.getData().add("OrgChartMode");
                } else {
                    ODataType entityDT = getDataTypeSevice().loadODataType(entity.getClass().getSimpleName(), loggedUser);
                    message = new ODataMessage();
                    List data = new ArrayList();
                    data.add(entity);
                    data.add(loggedUser);
                    data.add("OrgChartMode");
                    message.setODataType(entityDT);
                    message.setData(data);
                }

                params.put(OPortalUtil.MESSAGE, message);

                List<Long> portalPageData = (List<Long>) getOfunctionService().getPortalPageData(portalPageFunction.getDbid(), loggedUser);
                OPortalUtil.addOPortletPage(portalPageData, oem, dataTypeService, loggedUser, params,
                        getUIFrameWorkService(), getUDCService());
                logger.debug("Returning, Org Chart Opened");
                return;
            } catch (Exception ex) {
                logger.error("Exception thrown",ex);
            }
        }
        if ("closeLiferayLayouts".equals(action)) {
            try {
                List<Layout> layouts = LayoutLocalServiceUtil.getLayouts(0, 100);
                for (Layout layout : layouts) {
                    if (layout.getFriendlyURL().equals("/manage")
                            || layout.getFriendlyURL().equals("/home")) {
                        continue;
                    }
                    LayoutLocalServiceUtil.deleteLayout(layout);
                }
            } catch (Exception ex) {
               logger.error("Exception thrown",ex);
            }
            logger.debug("Returning, Org Chart Opened");
            return;
        }
    }

    public OEntityManagerRemote getOEntityManager() {
        logger.debug("Entering");
        if (oem == null) {
            try {
                logger.trace("Entity Manager em equals Null, i will set it as \"java:global/ofoq/com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote\"");
                InitialContext ctx = new InitialContext();
                oem = (OEntityManagerRemote) ctx.lookup(
                        "java:global/ofoq/com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote");
            } catch (NamingException ex) {
                logger.error("Exception thrown",ex);
            }
        }
        logger.debug("Returning");
        return oem;
    }

    public UDCServiceRemote getUDCService() {
        logger.debug("Entering");
        if (uDCService == null) {
            logger.trace("Entity Manager em equals Null, i will set it as \"java:global/ofoq/com.unitedofoq.fabs.core.udc.UDCServiceRemote\"");
            try {
                InitialContext ctx = new InitialContext();
                uDCService = (UDCServiceRemote) ctx.lookup(
                        "java:global/ofoq/com.unitedofoq.fabs.core.udc.UDCServiceRemote");
            } catch (NamingException ex) {
                logger.error("Exception thrown",ex);
            }
        }
        logger.debug("Returning");
        return uDCService;
    }

    public OFunctionServiceRemote getOfunctionService() {
        logger.debug("Entering");
        if (functionService == null) {
            try {
                InitialContext ctx = new InitialContext();
                functionService = (OFunctionServiceRemote) ctx.lookup(
                        "java:global/ofoq/com.unitedofoq.fabs.core.function.OFunctionServiceRemote");
            } catch (NamingException ex) {
                logger.error("Exception thrown",ex);
            }
        }
        logger.debug("Returning");
        return functionService;
    }

    public DataTypeServiceRemote getDataTypeSevice() {
        logger.debug("Entering");
        if (dataTypeService == null) {
            try {
                InitialContext ctx = new InitialContext();
                dataTypeService = (DataTypeServiceRemote) ctx.lookup(
                        "java:global/ofoq/com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote");
            } catch (NamingException ex) {
                logger.error("Exception thrown",ex);
            }
        }
        logger.debug("Returning");
        return dataTypeService;
    }

    public UIFrameworkServiceRemote getUIFrameWorkService() {
        logger.debug("Entering");
        if (uiFrameWorkFacade == null) {
            try {
                InitialContext ctx = new InitialContext();
                uiFrameWorkFacade = (UIFrameworkServiceRemote) ctx.lookup(
                        "java:global/ofoq/com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote");
            } catch (NamingException ex) {
                logger.error("Exception thrown",ex);
            }
        }
        logger.debug("Returning");
        return uiFrameWorkFacade;
    }

    public void addMessage(String entityDBID, String userId, String screenInstId) {
        logger.debug("Entering");
        String userName = OPortalUtil.getUserLoginName(Long.parseLong(userId));
        OUser loggedUser = getOEntityManager().getUserForLoginName(userName);
        try {
            List data = new ArrayList();
            data.add(Long.parseLong(entityDBID));
            data.add(loggedUser);
            data.add("OrgChart");
            ODataType dataType = getDataTypeSevice().loadODataType("DBID", loggedUser);

            ODataMessage message = new ODataMessage(dataType, data);
            SessionManager.setScreenSessionAttribute(screenInstId, OPortalUtil.MESSAGE, message);
            SessionManager.setScreenSessionAttribute(screenInstId, "ScreenModeVar", OScreen.SM_EDIT);
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        } finally {
            oem.closeEM(loggedUser);
        }
        logger.debug("Returning");
        return;
    }

    private String tempP = "";

    public String getTempP() {
        return tempP;
    }

    public void setTempP(String tempP) {
        this.tempP = tempP;
    }
}
