/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO;
import com.unitedofoq.fabs.core.entitysetup.dto.OObjectBriefInfoFieldDTO;
import com.unitedofoq.fabs.core.security.SecurityServiceRemote;
import com.unitedofoq.fabs.core.security.user.UserPrivilege.ConstantUserPrivilege;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.screen.dto.OScreenDTO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;
import org.primefaces.component.commandlink.CommandLink;
import org.primefaces.component.graphicimage.GraphicImage;
import org.primefaces.component.overlaypanel.OverlayPanel;
import org.primefaces.component.scrollpanel.ScrollPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Entity Balloon <br> Wiki: {@link http://1.1.1.193:8090/OTMSWiki/wiki/Balloon}
 *
 * @author bassem
 */
public class Balloon implements Serializable {
final static Logger logger = LoggerFactory.getLogger(Balloon.class);
    /**
     * Is the field name in OBackBean which holds the balloons hash
     */
    static final String OBackBeanBalloonsHashName = "balloons";
    private ExpressionFactory exFactory = FacesContext.getCurrentInstance().getApplication().getExpressionFactory();
    private ELContext elContext = FacesContext.getCurrentInstance().getELContext();
    private OBackBean backBean;
    private String entityClassPath;
    private String entityTitle;
    private SecurityServiceRemote securityService;

    /**
     * Constructor
     *
     * @param ownerEntityBackBeanExpression Refer to
     * {@link #ownerEntityBackBeanExpression}
     * @param beanName Refer to {@link #beanName}
     * @param id Refer to {@link #id}
     * @param oEntity Refer to {@link #oentity}
     * @param oScreen Refer to {@link #oScreen}
     * @param exFactory
     * @param elContext
     * @param uiFrameWorkFacade
     * @param loggedUser
     */
    public Balloon(OBackBean backBean, String ownerEntityBackBeanExpression, String beanName, String id,
            long oEntityDBID, String entityClassPath, String entityTitle, String oScreenName,
            ExpressionFactory exFactory, ELContext elContext,
            UIFrameworkServiceRemote uiFrameWorkFacade, SecurityServiceRemote securityService, OUser loggedUser) {
        logger.debug("Entering");
        try {
            logger.debug("Initializing Balloon Object");

            this.ownerEntityBackBeanExpression = ownerEntityBackBeanExpression;
            this.id = id;
            this.entityTitle = entityTitle;
            this.beanName = beanName;
            this.oentityDBID = oEntityDBID;
            this.entityClassPath = entityClassPath;
            this.ownerScreen = uiFrameWorkFacade.getScreenDTOObject(oScreenName, loggedUser);
            this.loggedUser = loggedUser;
            this.backBean = backBean;
            if ("rtl".equals(backBean.getHtmlDir())) {
                righToLeft = true;
            }

            this.exFactory = FacesContext.getCurrentInstance().getApplication().getExpressionFactory();
            this.elContext = elContext = FacesContext.getCurrentInstance().getELContext();
            this.uiFrameWorkFacade = uiFrameWorkFacade;
            this.securityService = securityService;

            // Put into ArrayList to avoid error: Cannot convert [OEntityAction[dbid=3], ...]
            // of type class java.util.Vector to class java.util.ArrayList
            //<editor-fold defaultstate="collapsed" desc="Static Actions">
            //<editor-fold defaultstate="collapsed" desc="AttachmentAction">

            if(securityService.checkPrivilegeAuthority(ConstantUserPrivilege.Balloon_ActionAttatchement, loggedUser)){
                OEntityActionDTO entityAction = new OEntityActionDTO();
                entityAction.setName(backBean.ddService.getDDLabel("AttachmentAction", loggedUser));
                entityAction.setDbid(1430304);
                entityAction.setOfuntionDBID(1430304);
                entityAction.setDisplayed(true);
                entityAction.setSortIndex(10);
                actions.add(entityAction);
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="View Attachment">
            if (securityService.checkPrivilegeAuthority(ConstantUserPrivilege.Balloon_ViewAttatchement, loggedUser)) {
                OEntityActionDTO entityAction = new OEntityActionDTO();
                entityAction = new OEntityActionDTO();
                entityAction.setName(backBean.ddService.getDDLabel("attachmentGallariaPnlHdr", loggedUser));
                entityAction.setDbid(14412345);
                entityAction.setOfuntionDBID(14412345);
                entityAction.setDisplayed(true);
                entityAction.setSortIndex(1);
                actions.add(entityAction);
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="FormDesign">

            if(securityService.checkPrivilegeAuthority(ConstantUserPrivilege.Balloon_ActionFormDesigner, loggedUser)){
                OEntityActionDTO entityActionDTO = new OEntityActionDTO();

                entityActionDTO.setName(backBean.ddService.getDDLabel("FormDesign", loggedUser));
                entityActionDTO.setDbid(1448475);
                entityActionDTO.setOfuntionDBID(1448475);
                entityActionDTO.setDisplayed(true);
                entityActionDTO.setSortIndex(11);
                actions.add(entityActionDTO);//TODO:Activate When Merge Complete

            }
            //</editor-fold>
            //</editor-fold>
            List<OEntityActionDTO> entityActions = uiFrameWorkFacade.getDisplayedDTOActions(oEntityDBID, loggedUser);
            if (entityActions == null) {
                return;
            }
            for (OEntityActionDTO oea : entityActions) {
                // Check ofunctin as it's used in the values and bindings, causes
                // run-time error if not properly set
                if (oea.getOfuntionDBID() > 0) {
                    // Action has function
                    // Check user authority on the action
                    try {
                    } catch (Exception ex) {
                        // User is not authorized on the action
                        logger.trace("User is not authorized");
                        logger.error("Exception thrown",ex);
                        continue;  // don't display it
                    }
                    // Add action to list to be displayed
                    actions.add(oea);
                } else {
                    // Action has no function
                    // DO NOT Add action to list to be displayed
                    logger.warn("Action has Null function, will not be displayed");
                }
            }
            evaluateExpBallonInOBackBeanPrefix();

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        logger.debug("Returning");
    }

    /**
     * Create the full balloon screen (controls, bindings, etc...)
     *
     * @param ownerFieldBackBeanExpression Is
     * {@link #ownerFieldBackBeanExpression}
     * @return Balloon HtmlPanelGrid of the balloonButton (from which a balloon
     * is displayed)
     */
    public OverlayPanel create(String ownerFieldBackBeanExpression) {
        logger.debug("Entering");
        try {
            setOwnerFieldBackBeanExpression(ownerFieldBackBeanExpression);
            createBalloonButtonPanel();
            createMainTablePanel();
            ScrollPanel actionPanel = new ScrollPanel();
            for (OEntityActionDTO oEntityActionDTO : actions) {
                HtmlPanelGrid grid = createActionFieldControl(oEntityActionDTO);
                String displayCodition = oEntityActionDTO.getDisplayCondition();
                if (null != displayCodition && !displayCodition.equals("")) {
                    checkCondition(grid, displayCodition);
                }
                grid.getChildren().add(createEntityDBIDControl(oEntityActionDTO));
                actionPanel.getChildren().add(grid);
            }

            actionPanel.setStyleClass("actionTablePanel");
            actionPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "actionTablePanel");
            mainTablePanel.getChildren().add(actionPanel);
            createBriefInfoOuterPanel();
            List<OObjectBriefInfoFieldDTO> briefInfoFields = uiFrameWorkFacade.getBriefInfoFieldDTOs(oentityDBID, loggedUser);
            if (briefInfoFields == null || briefInfoFields.isEmpty()) {
                logger.warn("No Brief Info Fields Found");
            } else {
                for (OObjectBriefInfoFieldDTO briefInfoField : briefInfoFields) {
                    createBriefInfoFieldControl(briefInfoField);
                }
                ScrollPanel briefPanel = new ScrollPanel();
                briefPanel.setStyleClass("briefPanel");
                briefPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_briefPanel" + id);
                briefPanel.getChildren().add(briefInfoOuterPanel);
                mainTablePanel.getChildren().add(briefPanel);
            }
            balloonButtonPanel.getChildren().add(mainTablePanel);

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        balloonButtonPanel.setStyle("position:absolute !important");
        mainTablePanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "mainTablePanel");
        logger.debug("Returning");
        return balloonButtonPanel;
    }
    // <editor-fold defaultstate="collapsed" desc="ownerFieldBackBeanExpression">
    /**
     * Is a full expression of the field in the BackBean for which the balloon
     * is displayed, i.e. it should be
     * {@link #ownerEntityBackBeanExpression}.field; i.e. it's not relative to
     * {@link #ownerEntityBackBeanExpression} <br> Is null if Balloon is
     * displayed for the entity {@link #ownerEntityBackBeanExpression}
     */
    private String ownerFieldBackBeanExpression;

    /**
     * getter of {@link #ownerFieldBackBeanExpression}
     */
    public String getOwnerFieldBackBeanExpression() {
        return ownerFieldBackBeanExpression;
    }

    public void setOwnerFieldBackBeanExpression(String ownerFieldBackBeanExpression) {
        this.ownerFieldBackBeanExpression = ownerFieldBackBeanExpression;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="balloonType">
    /**
     * Is expression of the BackBean-field that holds the entity (driven from
     * BaseEntity); for which the Balloon is displayed (either the entity
     * itself, or one of its fields) <br>Example: <br>* currentRow: Tabular
     * Field, Side Balloon <br>* getBeanName() + ".parentObject": Header field
     * <br>* getBeanName() + ".loadedObject": Form Field
     */
    private String ownerEntityBackBeanExpression;

    /**
     * getter of {@link #ownerEntityBackBeanExpression}
     */
    public String getOwnerEntityBackBeanExpression() {
        return ownerEntityBackBeanExpression;
    }

    /**
     * setter of {@link #ownerEntityBackBeanExpression}
     */
    public void setOwnerEntityBackBeanExpression(String ownerEntityBackBeanFieldExpression) {
        this.ownerEntityBackBeanExpression = ownerEntityBackBeanFieldExpression;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="beanName">
    /**
     * Is the name of the bean in which the balloon is displayed <br>Examples:
     * <br>* TabularBean <br>* FormBean
     */
    private String beanName = "";

    /**
     * getter of {@link #beanName}
     */
    public String getBeanName() {
        return beanName;
    }

    /**
     * setter of {@link #beanName}
     */
    public void setBeanName(String beanName) {
        this.beanName = beanName;
        evaluateExpBallonInOBackBeanPrefix();
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="actions">
    private List<OEntityActionDTO> actions = new ArrayList<OEntityActionDTO>();

    public List<OEntityActionDTO> getActions() {
        return actions;
    }

    public void setActions(List<OEntityActionDTO> actions) {
        this.actions = actions;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="oentity">
    /**
     * OEntity for which the balloon is displayed, from which the actions and
     * brief information are defined
     */
    private long oentityDBID;

    public long getOentityDBID() {
        return oentityDBID;
    }

    public void setOentityDBID(long oentityDBID) {
        this.oentityDBID = oentityDBID;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="rightToLeft, dir, url">
    private boolean righToLeft = false;
    private String dir = "left";
    private String url = "balloon.png";

    public boolean isRighToLeft() {
        return righToLeft;
    }

    public void setRighToLeft(boolean righToLeft) {
        this.righToLeft = righToLeft;
        dir = (righToLeft) ? "right" : "left";
        url = (righToLeft) ? "balloonAr.gif" : "balloon.png";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="id, actionTableVar">
    private String actionTableVar = "balloonActionRow";
    /**
     * Is the balloon id, which should be unique on the Bean level
     */
    private String id = "autoID";

    /**
     * getter of {@link #id}
     */
    public String getId() {
        return id;
    }

    /**
     * setter of {@link #id}
     */
    public void setId(String id) {
        this.id = id;
        actionTableVar = "balloonActionRow" + id;
        evaluateExpBallonInOBackBeanPrefix();
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="expBallonInOBackBean">
    /**
     * Is the expression of the balloon object in the BackBean e.g.
     * "#{my-bean-name.balloons['_3']"; note: no "}" is appended <br> You need
     * to append your fields and close with "}" <br> Is initializaed using
     * {@link #evaluateExpBallonInOBackBeanPrefix()}
     */
    private String expBallonInOBackBeanPrefix;

    /**
     * getter of {@link #expBallonInOBackBeanPrefix}
     */
    public String getExpBallonInOBackBeanPrefix() {
        return expBallonInOBackBeanPrefix;
    }

    /**
     * setter of {@link #expBallonInOBackBeanPrefix}
     */
    private void evaluateExpBallonInOBackBeanPrefix() {
        if (beanName != null && id != null) {
            expBallonInOBackBeanPrefix = "#{" + beanName
                    + "." + OBackBeanBalloonsHashName + "['" + id + "']";
        }
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="balloonButtonPanel">
    OverlayPanel balloonButtonPanel;

    private OverlayPanel createBalloonButtonPanel() {
        logger.debug("Entering");
        balloonButtonPanel = new OverlayPanel();
        balloonButtonPanel.setId(id);
        balloonButtonPanel.setAppendToBody(true);
        String entityExpression = ownerFieldBackBeanExpression != null
                ? ownerFieldBackBeanExpression : ownerEntityBackBeanExpression;
        String valueExpression = "#{"
                + entityExpression + "==null?'false':'true'}";
        balloonButtonPanel.setValueExpression(
                "rendered",
                exFactory.createValueExpression(
                        elContext,
                        valueExpression,
                        Boolean.class));

        // <editor-fold defaultstate="collapsed" desc="Log">
//        OLog.logInformation("Balloon Button Information", loggedUser, Level.INFO,
//                "Balloon ID", id,
//                "Owner Entity Back Bean Expression", ownerEntityBackBeanExpression,
//                "Owner Field Back Bean Expression", ownerFieldBackBeanExpression,
//                "Balloon Button Rendered Expression", valueExpression
//                );
        // </editor-fold>
        return balloonButtonPanel;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Screen Mode">
    HtmlInputHidden screenMode_H;

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Entity Name & DBID (Control & Value)">
    /**
     * Hidden field holds the DBID of the entity
     * {@link #ownerEntityBackBeanExpression} (loaded in screen) for which the
     * balloon is displayed
     */
    private HtmlOutputText entityDBID_H;

    public HtmlOutputText getEntityDBID_H() {
        return entityDBID_H;
    }

    public void setEntityDBID_H(HtmlOutputText entityDBID_H) {
        this.entityDBID_H = entityDBID_H;
    }

    public Long getEntityDBID() {
        if (entityDBID_H != null) {
            return Long.parseLong(entityDBID_H.getAttributes().get("value").toString());
        } else {
            return 0L;
        }
    }

    public String getEntityClassPath() {
        return entityClassPath;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Action Table HtmlPanelGrid">
    HtmlPanelGrid actionsTablePanel;
    private String leftActionstableStyle;
    private String rightActionstableStyle;
    // </editor-fold>

    /**
     * Binds the value to the balloon {@link #actions} through
     * expBallonInOBackBeanPrefix
     *
     * @return {@link #actionsTable}
     */
    // <editor-fold defaultstate="collapsed" desc="actionFunction_H, Action Function">
    private HtmlInputHidden actionFunction_H;

    public HtmlInputHidden getActionFunction_H() {
        return actionFunction_H;
    }

    public void setActionFunction_H(HtmlInputHidden actionFunction_H) {
        this.actionFunction_H = actionFunction_H;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Action Link">
    private CommandLink link;
    private CommandLink windowLink;

    public final static String WINDOWLINKPOSTFIX = "Window";

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="No-Expression Controls Creation">
    private HtmlPanelGrid mainTablePanel;

    private HtmlPanelGrid createMainTablePanel() {
        logger.debug("Entering");
        mainTablePanel = new HtmlPanelGrid();
        mainTablePanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "MTPanel");
        mainTablePanel.setColumns(2);
        mainTablePanel.setBorder(0);
        mainTablePanel.setCellpadding("0px");
        mainTablePanel.setCellspacing("0px");
        mainTablePanel.setStyleClass("balloonTable");
        mainTablePanel.setDir(backBean.getHtmlDir());
logger.debug("Returning");
        return mainTablePanel;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Brief Info Fields">
    private HtmlPanelGrid briefInfoOuterPanel;

    private HtmlPanelGrid createBriefInfoOuterPanel() {
        logger.trace("Entering");
        briefInfoOuterPanel = new HtmlPanelGrid();
        briefInfoOuterPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "BFInFileds");
        briefInfoOuterPanel.setStyleClass("briefInfoOuterPanel");
        logger.trace("Returning");
        return briefInfoOuterPanel;
    }

    private HtmlOutputText createBriefInfoFieldControl(
            OObjectBriefInfoFieldDTO briefInfoField) {
        logger.trace("Entering");
        HtmlOutputText lbl;

        // Label
        lbl = new HtmlOutputText();
        lbl.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "lbl" + briefInfoField.getDbid());
        lbl.setValue(briefInfoField.getTitleOverride() + "  ");
        if (null == briefInfoField.getTitleOverride()
                || briefInfoField.getTitleOverride().trim().isEmpty()) {
            backBean.ddService.getDDLabel(beanName, loggedUser);
            String briefInfo = (null == briefInfoField.getDdLabel()) ? ""
                    : briefInfoField.getDdLabel();
            lbl.setValue(briefInfo + "  ");
        }
        lbl.setStyleClass("bln-lbl-title");
        // Value
        HtmlOutputText lbl2 = new HtmlOutputText();
        lbl2.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "lbl2");
        lbl2.setStyleClass("bln-lbl-brf");
        String valueExpression = "#{"
                + (ownerFieldBackBeanExpression != null
                        ? ownerFieldBackBeanExpression : ownerEntityBackBeanExpression)
                + "." + briefInfoField.getFieldExpression() + "}";
        lbl2.setValueExpression(
                "value",
                exFactory.createValueExpression(
                        elContext,
                        valueExpression,
                        String.class));
        briefInfoOuterPanel.getChildren().add(lbl);
        briefInfoOuterPanel.getChildren().add(lbl2);
logger.trace("Returning");
        return lbl;
    }
    // </editor-fold>
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="loggedUser">
    OUser loggedUser;

    public OUser getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(OUser loggedUser) {
        this.loggedUser = loggedUser;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ownerScreen">
    /**
     * Is the OScreen of the screen in which the balloon is displayed
     */
    OScreenDTO ownerScreen;

    public OScreenDTO getOwnerScreen() {
        return ownerScreen;
    }

    public void setOwnerScreen(OScreenDTO ownerScreen) {
        this.ownerScreen = ownerScreen;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="uiFrameWorkFacade">
    private UIFrameworkServiceRemote uiFrameWorkFacade;
    // </editor-fold>

    private HtmlPanelGrid createActionFieldControl(OEntityActionDTO oEntityActionDTO) {
        logger.trace("Entering");
        HtmlPanelGrid grid = new HtmlPanelGrid();
        grid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_grid");
        grid.setColumns(2);
        Class[] params = {ActionEvent.class};

        windowLink = new CommandLink();//id +//oEntityActionDTO.getDbid()
        String screenFnID = id + "_" + oEntityActionDTO.getDbid()
                + "_actionLink_" + WINDOWLINKPOSTFIX + "_" + oEntityActionDTO.getOfuntionDBID();
        String nonScreenFnID = id + "_" + oEntityActionDTO.getDbid()
                + "_actionLink_Function_" + oEntityActionDTO.getOfuntionDBID();
        String screenFnImg = "/resources/open_window.png";
        String nonScreenFnImg = "/resources/run_function.png";

        GraphicImage windowImage = new GraphicImage();
        windowImage.setId(FacesContext.getCurrentInstance().
                getViewRoot().createUniqueId() + "_IMG");

        windowImage.setStyleClass("balloonWindowImage");

        if (oEntityActionDTO.getOfunctionType() != null && oEntityActionDTO.getOfunctionType().equals("SCREEN")) {
            windowLink.setId(screenFnID);
            windowImage.setUrl(screenFnImg);
        } else {
            windowLink.setId(nonScreenFnID);
            windowImage.setUrl(nonScreenFnImg);
        }
        windowLink.getChildren().add(windowImage);
        windowLink.setStyleClass("ballonActionButton");
        windowLink.setImmediate(true);

        MethodExpression me = FacesContext.getCurrentInstance().getApplication().
                getExpressionFactory().createMethodExpression(
                        elContext, "#{" + getBeanName() + ".balloonAction}",
                        void.class, params);
        windowLink.addActionListener(new MethodExpressionActionListener(me));
        link = new CommandLink();
        link.setId("actionLink_" + oEntityActionDTO.getDbid()
                + id + "_" + oEntityActionDTO.getOfuntionDBID());
        link.setStyleClass("balloon_action_link");
        link.setImmediate(true);
        link.setProcess("@this");
        link.setPartialSubmit(true);

        link.setValue(oEntityActionDTO.getName());
        link.addActionListener(new MethodExpressionActionListener(me));

        grid.getChildren().add(windowLink);
        grid.getChildren().add(link);
        grid.setStyleClass("balloon-list-panel");
        logger.trace("Returning");
        return grid;
    }

    private UIComponent checkCondition(HtmlPanelGrid actionsColumn, String displayCondition) {
        String entityExpression = ownerFieldBackBeanExpression != null
                ? ownerFieldBackBeanExpression : ownerEntityBackBeanExpression;

        String[] conds = displayCondition.split(";");
        StringBuilder parsedConditions = new StringBuilder("#{");
        parsedConditions.append(entityExpression);
        parsedConditions.append("!= null");
        StringBuilder prackts = new StringBuilder();

        for (int i = 0; i < conds.length; i++) {
            parsedConditions.append("? ");//'false'
            String cond = conds[i];
            parsedConditions.append("(");
            parsedConditions.append(entityExpression);
            parsedConditions.append(".");
            if (cond.toLowerCase().contains(" in ")) {
                String[] in = cond.split("(?i)in");
                parsedConditions.append(in[0]);
                parsedConditions.append("==");
                String[] inVals = in[1].split(",");
                parsedConditions.append(inVals[0]);
                if(inVals.length > 1){
                    for (int j = 1; j < inVals.length ; j++){
                        parsedConditions.append(" || ");
                        parsedConditions.append(entityExpression);
                        parsedConditions.append(".");
                        parsedConditions.append(in[0]);
                        parsedConditions.append("==");
                        parsedConditions.append(inVals[j]);
                    }
                }
            } else {
                if (cond.contains("=") && !cond.contains("!")) {
                    cond = cond.replaceAll("=", "==");
                    conds[i] = cond;
                }
                parsedConditions.append(cond);
            }
            prackts.append("): 'false'");
        }
        parsedConditions.append(prackts);
        parsedConditions.append("}");

        actionsColumn.setValueExpression(
                "rendered",
                exFactory.createValueExpression(
                        elContext,
                        parsedConditions.toString(),
                        Boolean.class));
        return actionsColumn;
    }

    private UIComponent createEntityDBIDControl(OEntityActionDTO oEntityActionDTO) {
        logger.trace("Entering");
        String valueExpression = "#{"
                + (ownerFieldBackBeanExpression != null
                        ? ownerFieldBackBeanExpression : ownerEntityBackBeanExpression)
                + ".dbid}";
        String bindingExpression = expBallonInOBackBeanPrefix + ".entityDBID_H }";

        entityDBID_H = new HtmlOutputText();
        entityDBID_H.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_entityDBID_H_" + id + oEntityActionDTO.getDbid());
        entityDBID_H.setStyle("height:0px!important;position:absolute;font-size:0px;z-index:-9999999999");
        entityDBID_H.setValueExpression(
                "binding",
                exFactory.createValueExpression(
                        elContext,
                        bindingExpression,
                        String.class));
        entityDBID_H.setValueExpression(
                "value",
                exFactory.createValueExpression(
                        elContext,
                        valueExpression,
                        String.class));
        logger.trace("Balloon Entity with DBID: {} is Hidden",id);
        logger.trace("Returning");
        return entityDBID_H;
    }
}
