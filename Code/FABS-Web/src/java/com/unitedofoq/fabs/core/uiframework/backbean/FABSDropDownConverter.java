/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.backbean;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.SelectItem;
import org.primefaces.component.selectonemenu.SelectOneMenu;

/**
 *
 * @author htawab
 */
@FacesConverter(value="FABSDropDownConverter",forClass=SelectOneMenu.class)
public class FABSDropDownConverter implements Converter, Serializable {
    List<String> conditions = new ArrayList<String>();
    List<SelectItem> SelectItems = null;
    public FABSDropDownConverter(List<SelectItem> selectItems) {
        this.SelectItems = selectItems;
    }

    public FABSDropDownConverter() {
        SelectItems = new ArrayList<SelectItem>();
    }

    //  Called when an item is selected from the combo box. 'component' will
    //  always contain the 'value' of the selected item from the combo box.
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String label) {
        //  if the 'Not Set' value was selected...
        if (label.equals(Lookup.EMPTY_ITEM)) {
            return null;
        }

        for (SelectItem currentSelectItem : SelectItems) {
            if (currentSelectItem.getLabel().equals(label)) {
                return currentSelectItem.getValue();
            }
        }

        // if corresponding object was not found...
        return null;
    }

    //  Called when filling the combo box with 'values'. 'arg2' will
    // always contain the object from which the 'value' is obtained
    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
        if (object == null) {
            return Lookup.EMPTY_ITEM;
        }

        for (SelectItem currentSelectItem : SelectItems) {
            if (currentSelectItem.getValue() != null) {
                if (currentSelectItem.getValue().equals(object)) {
                    return currentSelectItem.getLabel();
                }
            }
        }
        // if corresponding label was not found...
        return "";
    }
}

