package com.unitedofoq.fabs.core.uiframework.backbean;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.DocumentException;
import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.unitedofoq.fabs.core.content.AttachmentDTO;
import com.unitedofoq.fabs.core.content.EntityAttachmentServiceLocal;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ComputedField;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntityAction;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.filter.FilterDDRequiredException;
import com.unitedofoq.fabs.core.filter.FilterField;
import com.unitedofoq.fabs.core.filter.OFilter;
import com.unitedofoq.fabs.core.filter.OFilter.FilterType;
import com.unitedofoq.fabs.core.filtermaps.ValueMapperFactory;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.i18n.TextTranslationException;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.report.customReports.CustRepFilterFld;
import com.unitedofoq.fabs.core.report.customReports.CustomReport;
import com.unitedofoq.fabs.core.report.customReports.CustomReportDynamicFilter;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.security.user.UserPrivilege.ConstantUserPrivilege;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkEntityManagerRemote;
import com.unitedofoq.fabs.core.uiframework.backbean.wizard.WizardPanel;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import com.unitedofoq.fabs.core.uiframework.portal.ScreenRenderInfo;
import com.unitedofoq.fabs.core.uiframework.screen.EntityFilter;
import com.unitedofoq.fabs.core.uiframework.screen.EntityFilterScreenAction;
import com.unitedofoq.fabs.core.uiframework.screen.FormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.M2MRelationScreen;
import com.unitedofoq.fabs.core.uiframework.screen.OBackBeanException;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenAction;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenFunction;
import com.unitedofoq.fabs.core.uiframework.screen.SingleEntityScreen;
import com.unitedofoq.fabs.core.uiframework.screen.TabularScreen;
import com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenDTMappingAttrDTO;
import com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenInputDTO;
import com.unitedofoq.fabs.core.uiframework.screen.dto.TabularScreenDTO;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.fabs.core.utils.UIFieldUtility;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.EventObject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.el.MethodBinding;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;
import javax.faces.event.ValueChangeEvent;
import javax.naming.InitialContext;
import javax.persistence.CascadeType;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.OneToOne;
import javax.persistence.OptimisticLockException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;
import javax.persistence.Transient;
import javax.sql.DataSource;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.primefaces.component.ajaxstatus.AjaxStatus;
import org.primefaces.component.celleditor.CellEditor;
import org.primefaces.component.column.Column;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.dialog.Dialog;
import org.primefaces.component.graphicimage.GraphicImage;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.inputtextarea.InputTextarea;
import org.primefaces.component.menu.Menu;
import org.primefaces.component.menubutton.MenuButton;
import org.primefaces.component.menuitem.MenuItem;
import org.primefaces.component.overlaypanel.OverlayPanel;
import org.primefaces.component.panel.Panel;
import org.primefaces.component.password.Password;
import org.primefaces.component.roweditor.RowEditor;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.component.selectoneradio.SelectOneRadio;
import org.primefaces.component.separator.Separator;
import org.primefaces.component.sheet.Sheet;
import org.primefaces.component.submenu.Submenu;
import org.primefaces.component.tooltip.Tooltip;
import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.event.data.FilterEvent;
import org.primefaces.event.data.PageEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.StreamedContent;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mibrahim
 */
@ManagedBean(name = "TabularBean")
@ViewScoped
public class TabularBean extends SingleEntityBean {

    final static org.slf4j.Logger logger = LoggerFactory.getLogger(TabularBean.class);
    protected LazyDataModel<BaseEntity> lazyModel;

    public LazyDataModel<BaseEntity> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<BaseEntity> lazyModel) {
        this.lazyModel = lazyModel;
    }

    DataTable dataTable;

    public DataTable getDataTable() {
        return dataTable;
    }

    public void setDataTable(DataTable dataTable) {
        this.dataTable = dataTable;
    }

    protected String selectedRow;
    protected List<BaseEntity> loadedList;

    private UIFrameworkEntityManagerRemote uiFrameworkEntityManager = OEJB.lookup(UIFrameworkEntityManagerRemote.class);

    EntityAttachmentServiceLocal entityAttachmentService = OEJB.lookup(EntityAttachmentServiceLocal.class);
    ;
    // <editor-fold defaultstate="collapsed" desc="Fields and its Setters and
    // Getters">

    OFunctionServiceRemote functionService = OEJB.lookup(OFunctionServiceRemote.class);
    BaseEntity currentEntityObject;
    private String lookUpButtonID;
    private int rowIndex = -1;
    SelectOneMenu menu;
    protected String sortColumnName;
    protected boolean ascending;
    protected boolean m2MShowAll = false;
    // we only want to resort if the oder or column has changed.
    protected String oldSort;
    protected boolean oldAscending;
    private boolean selectFRow = false;
    private boolean rowSelected = false;
    private String alookupText;
    String selected;
    boolean multipleSelection = true;
    HtmlInputHidden hidden;
    // ODataMessage dataMessage;
    List<ScreenField> screenFields;
    ArrayList<String> condt;
    private HtmlPanelGrid messageGrid;
    protected boolean showEditor;
    protected boolean showActions;
    // private String M2MSetupClassName;
    private Class m2MSetupClass;
    EntityFilterScreenAction entityFilterScreenAction;

    public SelectOneMenu getMenu() {
        return menu;
    }

    public void setMenu(SelectOneMenu menu) {
        this.menu = menu;
    }

    /**
     * Gets the sortColumnName column.
     *
     * @return column to sortColumnName
     */
    public String getSortColumnName() {
        return sortColumnName;
    }

    /**
     * Sets the sortColumnName column
     *
     * @param sortColumnName column to sortColumnName
     */
    public void setSortColumnName(String sortColumnName) {
        oldSort = this.sortColumnName;
        this.sortColumnName = sortColumnName;

    }

    /**
     * Is the sortColumnName ascending.
     *
     * @return true if the ascending sortColumnName otherwise false.
     */
    public boolean isAscending() {
        return ascending;
    }

    /**
     * Set sortColumnName type.
     *
     * @param ascending true for ascending sortColumnName, false for desending
     * sortColumnName.
     */
    public void setAscending(boolean ascending) {
        oldAscending = this.ascending;
        this.ascending = ascending;
    }

    public boolean isMultipleSelection() {
        return multipleSelection;
    }

    public void setMultipleSelection(boolean multipleSelection) {
        this.multipleSelection = multipleSelection;
    }

    public boolean isRowSelected() {
        // List selectedList = getSelectedEntity();
        if (selectedEntities == null || selectedEntities.length == 0) {
            return false;
        }
        return true;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }

    public List getLoadedList() {
        if (loadedList == null || loadedList.isEmpty()) {
            loadedList = new ArrayList<BaseEntity>();
            return loadedList;
        }

        return loadedList;
    }

    @Override
    public String refresh() {
        logger.debug("Entering");
        if (currentScreen.isBlockOnLoad()) {
            RequestContext.getCurrentInstance().execute("blockScreen()");
        }
        HashMap<String, Object> screenParamMap = SessionManager.getScreenSessionAttribute(getPortletInsId());
        String isBackfromDetail = (null == screenParamMap) ? ""
                : (String) screenParamMap.get(OPortalUtil.ISBACKFROMDETAIL);

        try {
            if (null != dataMessage && null != dataMessage.getData()
                    && (!dataMessage.getData().isEmpty() || "true".equals(isBackfromDetail))) {
                invalidInputMode = false;
                if (getAddButton() != null) {
                    getAddButton().setDisabled(false);
                }
                if (getAddRowButton() != null) {
                    getAddRowButton().setDisabled(false);
                }
                if (getSaveButton() != null) {
                    getSaveButton().setDisabled(false);
                }
                if (getSaveExitButton() != null) {
                    getSaveExitButton().setDisabled(false);
                }
                if (getShowHideButton() != null) {
                    getShowHideButton().setDisabled(false);
                }
            } else {
                invalidInputMode = true;
                if (getAddButton() != null) {
                    getAddButton().setDisabled(true);
                }
                if (getAddRowButton() != null) {
                    getAddRowButton().setDisabled(true);
                }
                if (getSaveButton() != null) {
                    getSaveButton().setDisabled(true);
                }
                if (getSaveExitButton() != null) {
                    getSaveExitButton().setDisabled(true);
                }
                if (getShowHideButton() != null) {
                    getShowHideButton().setDisabled(true);
                }
            }
            if (!"true".equalsIgnoreCase(isBackfromDetail)) {
                dataMessage = (ODataMessage) getScreenParam(OPortalUtil.MESSAGE);
                loadedList = getLoadedList();
                ((LazyBaseEntityModel) lazyModel).clearNewEntities();
            } else {
                BaseEntity baseEntity = (BaseEntity) screenParamMap.get(OPortalUtil.BACKFROMDETAILOBJECT);
                if (null != baseEntity) {
                    ((LazyBaseEntityModel) lazyModel).addRow(baseEntity, getFirst());
                }
                SessionManager.setScreenSessionAttribute(getPortletInsId(), OPortalUtil.ISBACKFROMDETAIL, "false");
            }
            messagesPanel.clearMessages();
            validateInputSetMapping();
            processInput();
            setFirst(0);
            lazyModel.getPageSize();
            loadedList = getLoadedList();
            getLazyModel().setWrappedData(getLoadedList());

            OScreen screen = getCurrentScreen();
            String portletID = this.getPortletInsId();
            String detail;// = "";
            String title;
            if (headerObject != null && masterField != null && !dataMessage.getData().isEmpty()) {
                detail = (String) BaseEntity.getValueFromEntity(headerObject, masterField.getFieldExpression());
                if (null != detail && !detail.equals("")) {
                    detail = " - " + detail;
                }
                title = screen.getHeaderTranslated() + detail;
            } else {
                title = screen.getHeaderTranslated();
            }

            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("top.FABS.Portlet.setTitle({title:'" + title.replace("\\", "\\\\").replace("\'", "\\'")
                    + "',portletId:'" + portletID + "'});");

            if (getScreenHeight() == null || getScreenHeight().equals("")) {
                RequestContext.getCurrentInstance()
                        .execute("top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');");
            } else {
                RequestContext.getCurrentInstance().execute("top.FABS.Portlet.updateIframeSizeWithHeight({iframeName:'"
                        + getPortletInsId() + "Frame'" + ",screenHeight:'" + getScreenHeight() + "'});");
            }
            if (compositeInputRequired) // If "sender" not parent input dataMessage, we need to reset
            // currentScreenInput to reflect it, as init() is not called
            // in this case
            {
                initCurrentScreenInput();
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning with Null");
        return null;

    }

    public void setLoadedList(List loadedList) {
        logger.debug("Entering");
        selectedEntities = (null == selectedEntities || selectedEntities.length == 0)
                ? new BaseEntity[loadedList.size()]
                : selectedEntities;
        this.loadedList = loadedList;
        try {
            if (selectFRow && toBeRenderScreens != null) {
                // Select first one
                if (!getLoadedList().isEmpty()) {
                    ((BaseEntity) getLoadedList().get(0)).setSelected(true);
                    selectedEntities[0] = (BaseEntity) loadedList.get(0);
                    selectionAction(null);
                } // onloadJSStr = "";

                ODataMessage oDM = constructSelectedEntityODM();
                if (oDM != null) {
                    for (ScreenRenderInfo screenRenderInfo : toBeRenderScreens) {
                        if (null != RequestContext.getCurrentInstance()) {
                            OPortalUtil.renderScreen(currentScreen, screenRenderInfo.screenInstId, oDM);
                        } else {// In the init there is no ajax call and js cannot be called directly
                            OPortalUtil.onInitRenderScreen(currentScreen, screenRenderInfo.screenInstId, oDM);
                            onloadJSStr += "try {top.FABS.Portlet.render({screenInstId:'"
                                    + screenRenderInfo.screenInstId + "'});} catch(err){};";
                        }
                    }
                }
            } else if (c2AMode) {
                if (loadedList.size() > 0) {
                    ((BaseEntity) loadedList.get(0)).setSelected(true);
                    selectedEntities = new BaseEntity[1];
                    selectedEntities[0] = (BaseEntity) loadedList.get(0);

                }
                selectionAction(null);

            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
    }

    public int lookupReturnedRecordsNum = 0;

    public HtmlPanelGrid getMessageGrid() {
        return buildMessagePanel();
    }

    public void setMessageGrid(HtmlPanelGrid messageGrid) {
        this.messageGrid = messageGrid;
    }

    /// Filter
    public static String TO_B_OPENED_SCREEN_INST_ID_VAR = "TO_B_OPENED_SCREEN_INST_ID";
    public static final String FROM_DELIM = "#FROM";
    public static final String TO_DELIM = "#TO";
    public static final Integer DEFAULT_PAGE_SIZE = 7;
    private FilterType currentFilterType;

    public FilterType getCurrentFilterType() {
        return currentFilterType;
    }

    public void setCurrentFilterType(FilterType currentFilterType) {
        this.currentFilterType = currentFilterType;
    }

    // </editor-fold>
    protected List<BaseEntity> loadList(String entityName, List<String> conditions, List<String> sortConditions) {
        logger.debug("Entering");
        try {
            if (isScreenOpenedForLookup()) {
                //
                try {
                    List<String> lookupConditions = (List<String>) SessionManager
                            .getSessionAttribute("lookupConditions");
                    conditions.addAll(0, lookupConditions);
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }
                // </editor-fold>
            }

            // <editor-fold defaultstate="collapsed" desc="Update conditions with
            // ScreenDataLoadingUEConditions">
            OFunctionResult dataLoadingUEConditions = uiFrameWorkFacade.getScreenDataLoadingUEConditions(currentScreen,
                    headerObject, conditions, dataMessage, loggedUser);
            if (dataLoadingUEConditions.getErrors().isEmpty()) {
                conditions.clear();
                conditions.addAll((List) dataLoadingUEConditions.getReturnedDataMessage().getData().get(0));
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Update conditions inCase
            // CurrentScreenDDs Loading from Gear">
            if (currentScreen.getName().equals("CurrentScreenDDs")) {
                dataMessage = ((HashMap<String, ODataMessage>) SessionManager.getSessionAttribute(screenInstId))
                        .get("SCREEN_INPUT");
                extractScreenFieldsDDs(conditions);
            }
            // </editor-fold>

            List<String> screenFieldExpressions = new ArrayList<String>();

            // <editor-fold defaultstate="collapsed" desc="Fill screenFieldExpressions from
            // screenFields">
            Iterator screenFieldsIterator = screenFields.iterator();

            while (screenFieldsIterator.hasNext()) {
                screenFieldExpressions.add(((ScreenField) screenFieldsIterator.next()).getFieldExpression());
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Sorting">
            for (int i = 0; i < sortConditions.size(); i++) {
                String sortCondition = sortConditions.get(i);
                String sortField = sortCondition;
                String newSortCondition = "";
                int index = sortCondition.indexOf(" ");
                if (index != -1) {
                    sortField = sortField.substring(0, sortField.indexOf(" "));
                }
                newSortCondition = sortCondition.replace(sortField, "lower(" + sortField + ")");
                Field field;
                Class actOnClass = actOnEntityCls;
                while (sortField.contains(".")) {
                    String currentField = sortField.substring(0, sortField.indexOf("."));
                    sortField = sortField.substring(currentField.length() + 1);
                    field = BaseEntity.getClassField(actOnClass, currentField);
                    if (field != null) {
                        actOnClass = field.getType();
                    }
                }
                field = BaseEntity.getClassField(actOnClass, sortField);
                if (field != null) {
                    if (field.getType().getName().contains("String")) {
                        sortConditions.remove(i);
                        sortConditions.add(i, newSortCondition);
                    }
                    if (field.getAnnotation(Translation.class) != null || (field.getAnnotation(Transient.class) != null
                            && field.getAnnotation(ComputedField.class) == null)) {
                        sortConditions.remove(i);
                    }
                }
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Lazy Load Data">
            List<Integer> indicies = ((LazyBaseEntityModel) lazyModel).getIndicies();
            int count = 0, newEntitiesSize = indicies.size();
            try {
                count = (int) oem.getTotalNumberOfEntityDTORecords(getOactOnEntity(), dataFilter, loggedUser);
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
            lazyModel.setRowCount(count + newEntitiesSize);
            int currentIndicies = 0, beforeIndicies = 0;
            for (int i = 0; i < indicies.size(); i++) {
                Integer index = indicies.get(i);
                if (index < getFirst()) {
                    beforeIndicies++;
                }
                if (index >= getFirst() && index < getFirst() + lazyModel.getPageSize()) {
                    currentIndicies++;
                }
            }
            int startIndex = Math.max(0, getFirst() - beforeIndicies),
                    maxResults = lazyModel.getPageSize() - currentIndicies;
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="For M2M">
            if (getCurrentScreen() instanceof M2MRelationScreen) {
                List<BaseEntity> resultSet = oem.loadEntityList(entityName, conditions, screenFieldExpressions,
                        sortConditions, getLoggedUser());
                if (resultSet.isEmpty() || !((M2MRelationScreen) getCurrentScreen()).isShowRelatedOnly()) {
                    m2MShowAll = true;
                }

                if (areAllCompositeInputsAvailable() && m2MShowAll) {
                    List m2mList = loadManyToManySetupList();
                    logger.trace("M2M List Information");
                    if (m2mList != null) {
                        resultSet.addAll(m2mList);
                    }
                }

                lazyModel.setRowCount(resultSet.size());
                maxResults = Math.min(lazyModel.getRowCount(), startIndex + lazyModel.getPageSize());
                logger.debug("Returning");
                return resultSet.subList(startIndex, maxResults);
            }
            // </editor-fold>
            if ((((TabularScreen) getCurrentScreen()).isSheet())) {
                logger.debug("Returning");
                return oem.loadEntityList(entityName, conditions, screenFieldExpressions, sortConditions,
                        getLoggedUser());
            } else {
                logger.debug("Returning");
                return oem.loadEntityList(entityName, conditions, screenFieldExpressions, sortConditions, startIndex,
                        maxResults, getLoggedUser());
            }

        } catch (UserNotAuthorizedException unaex) {
            messagesPanel.addErrorMessage(usrMsgService.getUserMessage("UserNotAuthorized", systemUser));
            // TODO: why is that? How to display the message?
            logger.debug("Returning with empty ArrayList");
            return new ArrayList<BaseEntity>();

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex, true);
            logger.debug("Returning with Null");
            return null;
        }
    }

    protected boolean firstTime = false;

    @Override
    public HtmlPanelGrid buildInnerScreen() {
        logger.trace("Entering");
        // </editor-fold>

        htmlPanelGrid = new HtmlPanelGrid();

        Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();

        if (sessionMap.get("ActiveSession") == null || !sessionMap.get("ActiveSession").equals("tue")) {
            if (!getBeanName().equals(DockMenuBean.class.getSimpleName())) {
                logger.trace("Closing portlet because of session");
                String closePortletFName = "alert('" + getPortletInsId().toString() + "');";
                RequestContext.getCurrentInstance().execute(closePortletFName);

                OPortalUtil.removePortlet(getPortletInsId(), screenInstId);
            }
        }
        try {
            firstTime = true;
            htmlPanelGrid.setId(getFacesContext().getViewRoot().createUniqueId() + "htmlPanelGrid");
            htmlPanelGrid.setColumns(1);
            htmlPanelGrid.setDir(htmlDir);
            htmlPanelGrid.setStyle("width:100%;border:hidden");

            htmlPanelGrid.getChildren().add(messagesPanel.createMessagePanel());

            lazyModel = new LazyBaseEntityModel(loadedList, this);

            filterfields = new ArrayList<FilterField>();
            FilterBuilder filterBuilder = new FilterBuilder(this, filterServiceBean, oem, exFactory, timeZoneService,
                    loggedUser);
            int j = 0;
            EntityFilter entityFilter = null;
            entityFilterScreenAction = (EntityFilterScreenAction) oem.loadEntity(
                    EntityFilterScreenAction.class.getSimpleName(),
                    Collections.singletonList("screen.dbid = " + currentScreen.getDbid()), null, loggedUser);
            if (entityFilterScreenAction != null) {
                entityFilter = entityFilterScreenAction.getEntityFilter();
            }
            if (entityFilter != null && entityFilter.isActive()) {
                filterfields.addAll(oem.loadEntityList(FilterField.class.getSimpleName(),
                        Collections.singletonList("filter.dbid = " + entityFilter.getDbid()), null,
                        Collections.singletonList("fieldIndex"), loggedUser));
                if (entityFilter.isShowInsideScreen()) {
                    HtmlPanelGrid entityFilterGrid = filterBuilder.buildFilterPanel(htmlPanelGrid, filterfields,
                            "entityFilterGeneralPanel", "Entity Filter", j);
                    if (currentScreen.getScreenFilter() == null || !currentScreen.getScreenFilter().isActive()
                            || !currentScreen.getScreenFilter().isShowInsideScreen()) {
                        filterBuilder.buildFilterButtonPanel(entityFilterGrid, filterfields);
                    }
                }
            }
            if (currentScreen.getScreenFilter() != null && currentScreen.getScreenFilter().isActive()) {
                j = filterfields.size();
                filterfields.addAll(oem.loadEntityList(FilterField.class.getSimpleName(),
                        Collections.singletonList("filter.dbid = " + getCurrentScreen().getScreenFilter().getDbid()),
                        null, Collections.singletonList("fieldIndex"), loggedUser));
                if (currentScreen.getScreenFilter().isShowInsideScreen()) {
                    HtmlPanelGrid screenFilterGrid = filterBuilder.buildFilterPanel(htmlPanelGrid, filterfields,
                            "screenFilterGeneralPanel", "Screen Filter", j);
                    filterBuilder.buildFilterButtonPanel(screenFilterGrid, filterfields);
                }
            }

            try {
                validateInputSetMapping();
            } catch (OBackBeanException ex) {
                UserMessage errorMsg = usrMsgService.getUserMessage(ex.getMessage(), systemUser);
                errorMsg.setMessageType(UserMessage.TYPE_ERROR);
                // TODO: usage of ex.getMessage is not guaranteed at all; fix that
                if (errorMsg != null) {
                    messagesPanel.addMessages(errorMsg);
                }
            }

            if (getCurrentScreen() instanceof M2MRelationScreen) {
                M2MRelationScreen relScreen = (M2MRelationScreen) getCurrentScreen();
                m2MSetupClass = uiFrameWorkFacade.getFieldClass(getOactOnEntity().getEntityClassPath(),
                        relScreen.getSetupFieldExpression(), systemUser);
            }

            // Parse all the sort expressions
            String sortExpression = ((TabularScreen) getCurrentScreen()).getSortExpression();

            dataSort = new ArrayList<String>();
            if (sortExpression != null && !sortExpression.equals("")) {
                String exps[] = sortExpression.split(",");
                for (int i = 0; i < exps.length; i++) {
                    String string = exps[i].trim();
                    dataSort.add(string);
                }
            }

            if (restorePDataFromSessoion) {
                setLoadedList((List) restoreObject("loadedList"));
                setDataFilter((ArrayList<String>) restoreObject("dataFilter"));
                setDataSort((ArrayList<String>) restoreObject("dataSort"));
            } else {
                processInput();
            }
            Panel middlePanel = new Panel();
            if (screenLanguage.equalsIgnoreCase("Arabic")) {
                middlePanel.setStyle("position:relative;text-align:right;border:hidden;padding:0px;");
            } else {
                middlePanel.setStyle("position:relative;border:hidden;padding:0px;");
            }
            // TODO: Remove After
            if (!getCurrentScreen().getName().equals("EntityRequestHistory")) {

                HtmlPanelGrid generalbuttonsGrid = new HtmlPanelGrid();
                generalbuttonsGrid.setId(getFacesContext().getViewRoot().createUniqueId() + "_GeneralButtonsGrid");
                generalbuttonsGrid.setStyleClass("buttonsGeneralPanel");

                generalbuttonsGrid.setId(getFacesContext().getViewRoot().createUniqueId() + "_bgImg");

                HtmlPanelGrid buttonsGrid = new HtmlPanelGrid();
                buttonsGrid.setId(getFacesContext().getViewRoot().createUniqueId() + "_ButtonsGrid");

                buttonsGrid.setColumns(9 + getCurrentScreen().getScreenActions().size());
                HtmlPanelGrid loadingIconPanel = new HtmlPanelGrid();
                loadingIconPanel.setId(getFacesContext().getViewRoot().createUniqueId() + "_load");
                loadingIconPanel.setStyleClass("loading-icon");
                AjaxStatus loadingState = new AjaxStatus();
                loadingState.setId(getFacesContext().getViewRoot().createUniqueId() + "_loadAjax");
                // loadingState.setStyleClass("loading-active");
                GraphicImage loadingImage = new GraphicImage();
                loadingImage.setId(getFacesContext().getViewRoot().createUniqueId() + "_loadImg");
                loadingImage.setStyleClass("loading-active");
                loadingState.getFacets().put("start", loadingImage);
                HtmlOutputText outputText = new HtmlOutputText();
                outputText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_CompleteLBL");
                loadingState.getFacets().put("complete", outputText);
                loadingIconPanel.getChildren().add(loadingState);
                buttonsGrid.getChildren().add(loadingIconPanel);
                if (isScreenOpenedForLookup()) {
                    buildLookupComponent(buttonsGrid);
                } else {
                    if (!processMode) {
                        createButtons(buttonsGrid);
                        createScreenActionsButtons(buttonsGrid);
                    }
                    // createTabularScreenMenuBar(buttonsGrid);
                }
                generalbuttonsGrid.getChildren().add(buttonsGrid);
                middlePanel.getChildren().add(generalbuttonsGrid);
            }

            // Adding Routing Panel
            if (routingMode) {
                htmlPanelGrid.getChildren().add(this.createRoutingPanel());
            }

            if ((((TabularScreen) getCurrentScreen()).isSheet())) {
                middlePanel.getChildren().add(buildSheet("loadedList", "currentRow", "updateRow"));
            } else if (!((TabularScreen) currentScreen).isSave() && !((TabularScreen) currentScreen).isFastTrack()
                    && !((TabularScreen) currentScreen).isSaveAndExit()) {
                dataTable = buildNonEditableTable("loadedList", "currentRow", "updateRow");
                dataTable.setStyle("width:100%; margin-left:auto;margin-right:auto;");
                dataTable.setStyleClass("customDataTable");
                middlePanel.getChildren().add(dataTable);
            } else {
                dataTable = buildTable("loadedList", "currentRow", "updateRow");
                dataTable.setStyle("width:100%; margin-left:auto;margin-right:auto;");
                dataTable.setStyleClass("customDataTable");
                middlePanel.getChildren().add(dataTable);
            }
            htmlPanelGrid.getChildren().add(middlePanel);

            if (wizardMode) {
                wizardPanel = new WizardPanel();
                Separator wizardSeparator = new Separator();
                wizardSeparator.setId(getFacesContext().getViewRoot().createUniqueId() + "WizardSep");
                wizardSeparator.setStyleClass("wizard_separator");
                Separator wizardDivider = new Separator();
                wizardDivider.setId(getFacesContext().getViewRoot().createUniqueId() + "WizardDiv");
                wizardSeparator.setStyleClass("wizard_driver");
                htmlPanelGrid.getChildren().add(wizardSeparator);
                htmlPanelGrid.getChildren().add(wizardPanel.createWizardPanel(getBeanName(), exFactory, elContext,
                        wizardInfo, true, ddService, loggedUser));
            }

            htmlPanelGrid.setStyle("position:relative");
            restorePDataFromSessoion = false;
            onloadJSStr += "top." + getPortletInsId() + "DataRestored();";

            if (getScreenHeight() == null || getScreenHeight().equals("")) {
                onloadJSStr += "top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');";
            } else {
                onloadJSStr += "top.FABS.Portlet.updateIframeSizeWithHeight({iframeName:'" + getPortletInsId()
                        + "Frame'" + ",screenHeight:'" + getScreenHeight() + "'});";
            }

            UIComponent mbutton = (UIComponent) addGearBoxToPortletOptions();

            // if(c2AMode){
            // if(isShowManagePortlePage()){
            // middlePanel.getChildren().add(mbutton);
            // }
            // }else{
            middlePanel.getChildren().add(mbutton);
            // }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.trace("Returning");
        return htmlPanelGrid;
    }

    private void buildLookupComponent(HtmlPanelGrid buttonsGrid) {
        buttonsGrid.getChildren().add(buildLookupBtnPanel("setLookupData"));
        createButtons(buttonsGrid);
    }

    @Override
    public String setLookupData() {
        logger.debug("Entering");
        try {
            updateMainScrLookupPickedElementsCache();

            BaseEntity[] list = getSelectedEntities();
            if (list == null || list.length == 0) {
                logger.warn("Invalid Slected List");
            } else {
                SessionManager.addSessionAttribute("lookupRetData", list[0]);
                if (list.length > 1) {
                    SessionManager.addSessionAttribute("lookupRetDataList", list);
                } else {
                    SessionManager.addSessionAttribute("lookupRetDataList", null);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        String orScreen = (String) SessionManager.getSessionAttribute("lookupORScreen");
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("top.FABS.Portlet.lookupReturned({portletId:'" + orScreen + "'});");
        logger.trace("Calling JS closePopupNoID with PortletInsId: {}", getPortletInsId());
        RequestContext.getCurrentInstance().execute("top.FABS.Portlet.closePopupNoID('" + getPortletInsId() + "');");
        logger.debug("Returning with Null");
        return null;
    }

    private void updateMainScrLookupPickedElementsCache() {
        Long lookupMainScreenInstID = getLookupMainScrInstID(currentScreen.getName());
        String lookupFieldExp = (String) SessionManager.getSessionAttribute("lookupFieldExp");
        TabularScreenDTO.setLookupPickedElement(lookupMainScreenInstID, currentScreen.getName() + "_" + lookupFieldExp,
                new LinkedList(lookupPickedElements));
    }

    /**
     * Cache lookup selected indexes according to lookup selection action. Data
     * cached into server session with key of main screen instId and lookup
     * field expression. Removes cached data that already displayed on screen
     * then add the new actual selected entities using selectedLookupInexes.
     *
     * @param selectedLookupInexes
     */
    private void updateLookupPickedElementsCache(String selectedLookupInexes) {
        String lookupFieldExp = (String) SessionManager.getSessionAttribute("lookupFieldExp");
        Long lookupMainScreenInstID = getLookupMainScrInstID(currentScreen.getName());
        String pickedElementsSessionKey = getPickedElementKey(lookupMainScreenInstID, lookupFieldExp);

        lookupPickedElements = (LinkedList) SessionManager.getSessionAttribute(pickedElementsSessionKey);
        if (lookupPickedElements == null) {
            lookupPickedElements = new LinkedList();
        }
        removeOldDisplayedElements(lookupPickedElements);
        appendLookupCache(lookupPickedElements, selectedLookupInexes);
        SessionManager.addSessionAttribute(pickedElementsSessionKey, lookupPickedElements);
    }

    /**
     * removes from lookupPickedElements data that displayed on screen on
     * current page
     *
     * @param lookupPickedElements
     */
    private void removeOldDisplayedElements(LinkedList lookupPickedElements) {
        for (BaseEntity displayedElement : loadedList) {
            lookupPickedElements.remove(displayedElement);
        }
    }

    /**
     * appends lookupPickedElements with new selectedLookupInexes.
     *
     * @param lookupPickedElements
     * @param selectedLookupInexes
     */
    private void appendLookupCache(LinkedList lookupPickedElements, String selectedLookupInexes) {
        LinkedList selectedEntityList = new LinkedList();
        if (selectedLookupInexes != null && !selectedLookupInexes.equals("")) {
            String[] selectedIndexes;
            if (selectedLookupInexes.contains(",")) {
                selectedIndexes = selectedLookupInexes.split(",");
            } else {
                selectedIndexes = new String[1];
                selectedIndexes[0] = selectedLookupInexes;
            }
            for (String selectedIndex : selectedIndexes) {
                selectedEntityList.add(loadedList.get(Integer.valueOf(selectedIndex) - 1));
            }
            lookupPickedElements.addAll(selectedEntityList);
        }
    }

    /**
     * Returns the key for session attribute of lookup cached data using main
     * screen instance id and lookup field expression.
     *
     * @param lookupMainScreenInstID
     * @param lookupFldExp
     * @return
     */
    public String getPickedElementKey(long lookupMainScreenInstID, String lookupFldExp) {
        return lookupMainScreenInstID + "_" + lookupFldExp + "_lookupPickedElements";
    }

    /**
     * Returns the key for session attribute of lookup cached data using the
     * last opened lookup field in the main screen.
     *
     * @return
     */
    public String getPickedElementKey() {
        String lookupFieldExp = (String) SessionManager.getSessionAttribute("lookupFieldExp");
        Long lookupMainScreenInstID = currentScreen.getInstID();
        return getPickedElementKey(lookupMainScreenInstID, lookupFieldExp);
    }

    @Override
    public String wizardFinishAction() {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            saveRowObjectListAction();
            if (!exit) {
                // TODO : ADD ERROR MESSAGE
                logger.debug("Returning with empty string");
                return "";
            }
            return super.wizardFinishAction();
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
            messagesPanel.clearAndDisplayMessages(oFR);
            oem.closeEM(loggedUser);
            RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
            logger.debug("Returning with Null");
            return null;
        }

    }

    @Override
    public String wizardNextAction() {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            oem.getEM(loggedUser);
            messagesPanel.clearMessages();
            saveRowObjectListAction();
            ODataMessage wizardDataMessage = null;
            if (!exit) {
                return "";
            }

            if (wizardInfo.getStep().isMustSelect()) {
                if (selectedEntities[0] != null) {
                    try {
                        ODataType entityDataType = dataTypeService.loadODataType(getOactOnEntity().getEntityClassName(),
                                systemUser);
                        wizardDataMessage = (ODataMessage) constructOutput(entityDataType, selectedEntities[0])
                                .getReturnedDataMessage();
                    } catch (Exception ex) {
                        logger.error("Exception thrown", ex);
                        oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
                        messagesPanel.clearAndDisplayMessages(oFR);
                        oem.closeEM(loggedUser);
                        RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
                    }
                } else {
                    oFR.addError(usrMsgService.getUserMessage("SelectionError", systemUser));
                    messagesPanel.clearAndDisplayMessages(oFR);
                    oem.closeEM(loggedUser);
                    RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
                    logger.debug("Returning with empty string");
                    return "";
                }
            }

            // wizardCtrl.next(portletInsId, wizardDataMessage);
            return super.wizardNextAction(wizardDataMessage);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            // FIXME: display user message
            logger.debug("Returning with Null");
            return null;
        } finally {
            oem.closeEM(loggedUser);
        }
    }

    /**
     *
     * @param event
     * @return Don't return null, null refreshes the page and clears the
     * getSelectedEntity() for unknown reasons
     */
    public String selectionAction(SelectEvent event) {
        logger.debug("Entering");
        if (!c2AMode || null == selectedEntities || selectedEntities.length == 0) {
            logger.debug("Returning with \" \" as Click 2 Action equals Null");
            return "";
        }
        String time = "TIME_IS_" + System.currentTimeMillis();
        // FIXME: This solution is a work around for the first selected row in the
        // C2A Screens.. try to find another solution
        if (selectedEntities.length == 2) {
            ((BaseEntity) loadedList.get(0)).setSelected(false);
            ArrayUtils.remove(selectedEntities, 0);
        }

        try {
            if (toBeRenderScreens != null) {
                ODataMessage message = constructSelectedEntityODM();

                if (message == null) {
                    logger.debug("Returning with \" \" as Data Message equals Null");
                    return "";
                }

                for (ScreenRenderInfo screenRenderInfo : toBeRenderScreens) {

                    OPortalUtil.addPassedScreenInputDataMessage(screenRenderInfo.screenInstId, message);
                    onloadJSStr += "try {top.FABS.Portlet.render({screenInstId:'" + screenRenderInfo.screenInstId
                            + "'});} catch(err){};";
                    OPortalUtil.renderScreen(currentScreen, screenRenderInfo.screenInstId, message);
                    if (screenRenderInfo != null) {
                        logger.trace("Calling JS render for Screen Instance Id: {} with Message: {}",
                                screenRenderInfo.screenInstId, message);
                    }
                }

            }
            logger.debug("Returning with \" \"");
            return "";
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
        }
        logger.debug("Returning with \" \"");
        return "";
    }

    public String dblClkAction(SelectEvent event) {
        logger.debug("Entering");
        if (!lookupMode || null == selectedEntities || selectedEntities.length == 0) {
            logger.debug("Returning with \" \" as lookup mode or selected entity is empty");
            return "";
        }
        if (lookupPickedElements != null && !lookupPickedElements.isEmpty() && lookupPickedElements.size() > 1) {
            return "";
        }
        setLookupData();
        logger.debug("Returning with \" \"");
        return "";
    }

    public String onFilter(FilterEvent event) {
        return "";
    }

    public String rowUnSelectAction(UnselectEvent event) {
        return "";
    }

    public String clkAction(SelectEvent event) {
        logger.debug("Entering");
        BaseEntity entity = ((BaseEntity) event.getObject());
        currentEntityObject = entity;
        logger.debug("Returning");
        return "";
    }

    public String outputTextSelectionAction() {
        logger.debug("Entering");
        if (!c2AMode || selectedEntities.length == 0) {
            logger.debug("Returning as click to action or entity lenth equals 0");
            return "";
        }
        String time = "TIME_IS_" + System.currentTimeMillis();
        // FIXME: This solution is a work around for the first selected row in the
        // C2A Screens.. try to find another solution
        if (selectedEntities.length == 2) {
            ((BaseEntity) loadedList.get(0)).setSelected(false);
            ArrayUtils.remove(selectedEntities, 0);
        }

        if (selectFRow) {
            selectFRow = false;
        } else {
        }
        try {
            if (toBeRenderScreens != null) {
                ODataMessage message = constructSelectedEntityODM();
                if (message == null) {
                    logger.debug("Returning with \" \" as Data Message equals Null");
                    return "";
                }
                for (ScreenRenderInfo screenRenderInfo : toBeRenderScreens) {

                    OPortalUtil.addPassedScreenInputDataMessage(screenRenderInfo.screenInstId, message);

                    onloadJSStr += "try {top.FABS.Portlet.render({screenInstId:'" + screenRenderInfo.screenInstId
                            + "'});} catch(err){};";
                    if (screenRenderInfo != null) {
                        logger.trace("Calling JS render for Screen: {}", screenRenderInfo.screenInstId);
                    }
                }

            }
            logger.debug("Returning with \" \"");
            return "";
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
        }
        logger.debug("Returning with \" \"");
        return "";
    }

    @Override
    public UIComponent createScreenControl(int controlMode, ScreenField screenField, String ownerScreenExpression,
            int screenFieldIndex, String valueChangeMethod) {
        if (getCurrentScreen() instanceof M2MRelationScreen) {
            screenField.setMandatory(false);
        }
        return super.createScreenControl(controlMode, screenField, ownerScreenExpression, screenFieldIndex,
                valueChangeMethod);
    }

    @Override
    protected boolean onInit(Map<String, String> requestParams) {
        logger.trace("Entering");
        try {
            if (!super.onInit(requestParams)) {
                logger.trace("Returning with False");
                return false;
            }
            showEditor = true;
            showActions = true;
            if (processMode) {
                showActions = false;
            }

            if ("true".equalsIgnoreCase(requestParams.get("selectFRow"))) {
                selectFRow = true;
            }
            currentFilterType = FilterType.SCREEN_FILTER;
            screenFields = getCurrentScreen().getScreenFields();
            String msg = securityService.getEntityFieldsTagMsg(actOnEntityCls, screenFields, loggedUser);
            if (null != msg) {
                setHasSecuirtyTags(true);
                setSecurityTagMsg(msg);
            }
            filterControls = new ArrayList<UIComponent>();
            logger.trace("Returning with True");
            return true;

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Returning with False");
            return false;

        }
    }

    @Override
    protected String getBeanName() {
        return "TabularBean";
    }

    @Override
    public ODataMessage saveProcessRecords() {
        logger.debug("Entering");
        saveRowObjectListAction();
        ODataType outputDataType = null;// getTaskInfo().getAeTTask().getoOutputDataType();
        if (outputDataType.getName().equals("VoidDT")) {
            ODataMessage outputDataMessage = new ODataMessage();
            outputDataMessage.setODataType(outputDataType);
            List<Object> data = new ArrayList<Object>();
            data.add("Void");
            outputDataMessage.setData(data);
            logger.debug("Returning");
            return outputDataMessage;
        }
        logger.debug("Returning");
        return (ODataMessage) constructOutput(outputDataType, null).getReturnedDataMessage();
    }

    List<String> fieldExpressions;

    /**
     *
     * @return null in case of error <br>
     * Otherwise, the new instance created and initialized
     */
    protected BaseEntity createNewInstance() {
        logger.trace("Entering");
        try {
            fieldExpressions = new ArrayList<String>();
            BaseEntity newEntity = (BaseEntity) actOnEntityCls.newInstance();

            initializeDDDefaultValue(newEntity);
            // <editor-fold defaultstate="collapsed" desc="Intialize new instance using
            // fields expression">

            /**
             * Commented since we don't need to create new instances for empty
             * fields.
             *
             * newEntity.NewFieldsInstances(fieldExpressions);
             */
            // </editor-fold>
            initializeScreenInput(newEntity);

            if (initializeCompositeInput(newEntity)) {
                return null;
            }

            // <editor-fold defaultstate="collapsed" desc="Intialize new instance if
            // parentDBID passed">
            if ((dataMessage.getODataType().getDbid() == ODataType.DTDBID_PARENTDBID && headerObject != null)
                    || (dataMessage.getData().size() == 3
                    && String.valueOf(dataMessage.getData().get(2)).equals("OrgChart"))) {
                String parentFieldName = null;
                // <editor-fold defaultstate="collapsed" desc="Fill parentFieldName, condier
                // multiparents">
                List<String> parentFieldNames = BaseEntity
                        .getParentEntityFieldName(getOactOnEntity().getEntityClassPath());
                if (parentFieldNames.size() == 1) {
                    // Only one parent found
                    parentFieldName = parentFieldNames.get(0);

                } else if (parentFieldNames.size() > 1) {
                    // More than one parent found
                    // Decide the type of the passed parent
                    OFunctionResult parentOFR = uiFrameWorkFacade.getParentFromParentDBIDDTO(getOactOnEntity(),
                            (Long) dataMessage.getData().get(0), loggedUser);
                    if (!parentOFR.getErrors().isEmpty()) {
                        messagesPanel.addMessages(parentOFR);
                        logger.trace("Returning with Null");
                        return null;
                    }
                    if (parentOFR.getReturnValues().isEmpty()) {
                        // No parent found matches the DBID
                        logger.warn("Parent DBID Not Found In Any Of The Parents Tables");
                        // FIXME: meaningful error message
                        messagesPanel.addErrorMessage(usrMsgService.getUserMessage("SystemInternalError", systemUser));
                        logger.trace("Returning with Null");
                        return null;
                    }
                    parentFieldName = (String) parentOFR.getReturnValues().get(1); // parent field name

                }
                // </editor-fold>
                if (dataMessage.getData().size() == 3
                        && String.valueOf(dataMessage.getData().get(2)).equals("OrgChart")) {
                    BaseEntity orgEmployee = oem.loadEntity("Employee",
                            Collections.singletonList("dbid = " + dataMessage.getData().get(0)), null, loggedUser);

                    FramworkUtilities.setValueInEntity(newEntity, parentFieldName, orgEmployee, true);
                    // FIXME: check return if has errors
                    entityValueIntializationListener(newEntity, parentFieldName, orgEmployee);
                } else {
                    FramworkUtilities.setValueInEntity(newEntity, parentFieldName, headerObject);
                    // FIXME: check return if has errors
                    entityValueIntializationListener(newEntity, parentFieldName, headerObject);
                }
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Apply Screen Filter
            // Initialization">
            if (getCurrentScreen().getScreenFilter() != null && !getCurrentScreen().getScreenFilter().isInActive()
                    && getCurrentScreen().getScreenFilter().isInitialize()) {
                List<FilterField> filterFields = getCurrentScreen().getScreenFilter().getFilterFields();
                for (FilterField filterField : filterFields) {
                    if (filterField.isInActive()) {
                        continue;
                    }

                    if (filterField.isFromTo()) // Not supported
                    {
                        continue;
                    }

                    if (!filterField.getOperator().equals("=")) {
                        continue;
                    }

                    String value = filterField.getFieldValue();
                    if (filterField.getFieldDD().getControlType().getDbid() == DD.CT_DROPDOWN
                            || filterField.getFieldDD().getControlType().getDbid() == DD.CT_LOOKUPSCREEN) {
                        try {
                            BaseEntity currentObject = null;

                            String objectFieldExpression = filterField.getFieldExpression().substring(0,
                                    filterField.getFieldExpression()
                                            .indexOf(getLookupFldExpression(filterField.getFieldExpression())) - 1);

                            List<String> fldExps = new ArrayList<String>();

                            for (int i = 0; i < fieldExpressions.size(); i++) {
                                if (fieldExpressions.get(i).startsWith(objectFieldExpression + ".")) {
                                    fldExps.add(fieldExpressions.get(i).substring(objectFieldExpression.length() + 1));
                                }
                            }

                            currentObject = getLookupBaseEntity(filterField.getFieldExpression(), value, fldExps);

                            BaseEntity.setValueInEntity(newEntity, objectFieldExpression, currentObject);

                        } catch (Exception ex) {
                            logger.error("Exception thrown", ex);
                        }

                    } else if (filterField.getFieldDD().getControlType().getDbid() == DD.CT_CALENDAR) {
                        if (value != null && !value.equals("")) {
                            try {
                                BaseEntity.setValueInEntity(newEntity,
                                        getLookupFldExpression(filterField.getFieldExpression()),
                                        FilterField.dateFormat.parse(value));
                            } catch (Exception ex) {
                                logger.error("Exception thrown", ex);
                            }
                        }
                    } else {
                        if (value != null) {
                            List<ExpressionFieldInfo> infos = BaseEntity.parseFieldExpression(actOnEntityCls,
                                    filterField.getFieldExpression());
                            Field field = (null == infos || infos.isEmpty()) ? null : infos.get(0).field;
                            if (field.getAnnotation(Translation.class) != null) {
                                BaseEntity.setValueInEntity(newEntity,
                                        field.getAnnotation(Translation.class).originalField(), value);
                                BaseEntity.setValueInEntity(newEntity, filterField.getFieldExpression(), value);
                            } else {
                                BaseEntity.setValueInEntity(newEntity, filterField.getFieldExpression(), value);
                            }
                        }
                    }
                }
            }
            // </editor-fold>

            applyHardCodedInitialization(newEntity);
            applyFunctionInitialization(newEntity);

            return newEntity;

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
            logger.trace("Returning with Null");
            return null;
        }
    }

    private boolean initializeCompositeInput(BaseEntity newEntity) {
        // <editor-fold defaultstate="collapsed" desc="Intialize new instance using
        // compositeInput, return on error">
        if (!areAllCompositeInputsAvailable()) {
            messagesPanel.addErrorMessage(usrMsgService.getUserMessage("IncompleteCompositeInput", systemUser));
            logger.trace("Returning with Null");
            return true;
        }
        initEntityWithComposedInput(newEntity);
        // </editor-fold>
        return false;
    }

    private void initializeScreenInput(BaseEntity newEntity) {
        // <editor-fold defaultstate="collapsed" desc="Intialize new instance using
        // ScreenInput forInitialization">
        if (dataMessage != null) {
            if (currentScreenInput != null && !dataMessage.getData().isEmpty()) {
                for (ScreenDTMappingAttrDTO mapAttr : currentScreenInput.getAttributesMapping()) {
                    // Note that, ODataType.DTDBID_PARENTDBID natively supported input
                    // has isForInitialization()=false, it will be handled in a later
                    // code in this function
                    if (!mapAttr.isForInitialization()) {
                        continue;
                    }

                    newEntity.NewFieldsInstances(Collections.singletonList(mapAttr.getFieldExpression()));
                    Object value = null;
                    if (mapAttr.getAttributeExpression() == null
                            || mapAttr.getAttributeExpression().equalsIgnoreCase("")) {
                        value = dataMessage.getData().get(0);
                    } else {
                        value = getValueFromEntity(dataMessage.getData().get(0), mapAttr.getAttributeExpression());
                    }
                    FramworkUtilities.setValueInEntity(newEntity, mapAttr.getFieldExpression(), value, true);
                    // FIXME: check return if has errors
                    entityValueIntializationListener(newEntity, mapAttr.getFieldExpression(),
                            dataMessage.getData().get(0));
                }
            }
        }
        // </editor-fold>
    }

    private void initializeDDDefaultValue(BaseEntity newEntity) throws NumberFormatException, EntityNotFoundException,
            RollbackException, NonUniqueResultException, OptimisticLockException, NoResultException,
            TransactionRequiredException, UserNotAuthorizedException, EntityExistsException {
        // <editor-fold defaultstate="collapsed" desc="fill fieldExpressions, initialize
        // using DD Default Values">
        for (ScreenField screenField : screenFields) {
            String fieldExp = screenField.getFieldExpression();

            if (screenField.getDd().getDefaultValue() != null && !"".equals(screenField.getDd().getDefaultValue())) // DD
            // Default
            // Value
            // initialization
            // is
            // required
            {
                String defaultValue = screenField.getDd().getDefaultValue();

                if (screenField.getDd().getControlType().getDbid() == DD.CT_TEXTFIELD
                        || screenField.getDd().getControlType().getDbid() == DD.CT_TEXTAREA) // Field is text
                {
                    if (FramworkUtilities.isFloat(defaultValue)) {
                        FramworkUtilities.setValueInEntity(newEntity, fieldExp,
                                BigDecimal.valueOf(Float.valueOf(defaultValue.toString())));
                    } else {
                        // Default value is not number, so, it's text
                        FramworkUtilities.setValueInEntity(newEntity, fieldExp, defaultValue);
                    }
                } else if (screenField.getDd().getControlType().getDbid() == DD.CT_CALENDAR) // Field is calendar
                {
                    FramworkUtilities.setValueInEntity(newEntity, fieldExp, timeZoneService.getUserCDT(loggedUser));
                } else if (screenField.getDd().getControlType().getDbid() == DD.CT_CHECKBOX
                        && ("1".equals(defaultValue) || "true".equals(defaultValue))) // Field is checkbox with true
                // value
                {
                    FramworkUtilities.setValueInEntity(newEntity, fieldExp, true);
                } else if (screenField.getDd().getControlType().getDbid() == DD.CT_DROPDOWN) {
                    List<String> condition = new ArrayList<String>();
                    condition.add(defaultValue);
                    UDC udc = (UDC) oem.loadEntity(UDC.class.getSimpleName(), condition, null, loggedUser);
                    FramworkUtilities.setValueInEntity(newEntity, fieldExp, udc);
                } else if (screenField.getDd().getControlType().getDbid() == DD.CT_LITERALDROPDOWN) {
                    FramworkUtilities.setValueInEntity(newEntity, fieldExp, defaultValue);
                }
            } else // DD Default Value initialization is NOT required
            {
                fieldExpressions.add(fieldExp);
            }
        }
        // </editor-fold>
    }

    @Override
    public String lookupReturned() {
        logger.debug("Entering");
        if (fieldLookup.getOScreen() == null || !currentScreen.isMultiSelection()) {
            return super.lookupReturned();
        }
        lookupPickedElements = new LinkedList(TabularScreenDTO.listLookupPickedElement(currentScreen.getInstID(),
                fieldLookup.getOScreen().getName() + "_" + fieldLookup.getFieldExpression()));
        if (lookupPickedElements == null || lookupPickedElements.isEmpty() || lookupPickedElements.size() == 1) {
            if (!TabularScreenDTO.isScreenInMultiSelectionMode(currentScreen.getInstID())) {
                clearLookupTransformedElementsCache();
                LazyBaseEntityModel baseEntityModel = ((LazyBaseEntityModel) lazyModel);
                if (baseEntityModel.getNewEntities().isEmpty() && getFieldExpLookupData() != null
                        && baseEntityModel.getMasterNewEntity() != null) {
                    ((List) baseEntityModel.getWrappedData()).clear();
                    baseEntityModel.addRow(baseEntityModel.getMasterNewEntity(), 0);
                }
            }
            return super.lookupReturned();
        }

        transformLookupPickedEntities();
        if (editAllClicked) {
            RequestContext.getCurrentInstance().execute("editAll()");
        } else if (lookupTransformedPickedElements != null && !lookupTransformedPickedElements.isEmpty()
                && lookupTransformedPickedElements.size() != 1) {
            int lastRowIndex = lookupTransformedPickedElements.size() - 1;
            RequestContext.getCurrentInstance().execute("editRowList(" + lastRowIndex + ")");
        } else if (currentScreen instanceof TabularScreen && !fieldLookup.getId().contains("_filterField")) {
            RequestContext.getCurrentInstance()
                    .execute("KeepRowEditable(" + getObjectRecordID(Long.parseLong(recordID)) + ")");
        }
        if (fieldLookup.getFieldDD().getControlType().getDbid() == DD.CT_LOOKUPATTRIBUTEBROWSER) {
            if (getFieldExpLookupData() == null) {
                returnEmpty();
            } else {
                returnTreeSelectedField();
            }
        } else if (getLookupData() == null) {
            returnEmpty();
        } else if (null != getLookupDataList() && getLookupDataList().length > 1) {
            returnSelectedList();

        } else {
            returnSelected();
        }
        logger.debug("Returning with Null");
        return null;
    }

    private void transformLookupPickedEntities() {
        lookupMainScr = getCurrentScreen().getInstID();
        if (!lookupPickedElements.isEmpty()) {
            List lookupPickedList = new LinkedList(lookupPickedElements);
            SessionManager.addSessionAttribute("lookupPickedElementsMainScr", lookupMainScr);
            List mainObjList = new ArrayList();
            BaseEntity transformedPickedEntity = ((LazyBaseEntityModel) lazyModel).getMasterNewEntity();
            int lastIndex = lookupPickedList.size() - 1;
            for (int i = 0; i <= lastIndex; i++) {
                Object lookupPickedElement = lookupPickedList.get(i);
                try {
                    transformedPickedEntity = transformedPickedEntity.createNewInstance();
                    transformedPickedEntity.invokeSetter(fieldLookup.getFieldExpression().substring(0,
                            fieldLookup.getFieldExpression().lastIndexOf(".")), lookupPickedElement);
                    transformedPickedEntity.generateInstID();
                    mainObjList.add(transformedPickedEntity);
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }
            }
            lookupTransformedPickedElements = new LinkedList(mainObjList);
            SessionManager.getSessionMap().remove("lookupTransformedPickedElements");
            SessionManager.addSessionAttribute("lookupTransformedPickedElements", lookupTransformedPickedElements);
        }
    }

    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    private Dialog popup;

    public Dialog getPopup() {
        return popup;
    }

    public void setPopup(Dialog popup) {
        this.popup = popup;
    }

    @Override
    protected int getObjectRecordID(long dbid) {
        for (int i = 0; i < loadedList.size(); i++) {
            if (loadedList.get(i).getDbid() == dbid) {
                return i;
            }
        }
        return 0;
    }

    public void lookupListener(ActionEvent ae) {
        logger.debug("Entering");
        int count = count(liferayUsers.get(loggedUser.getLoginName()));
        if (count == 0) {
            logout();
            logger.debug("Returning ascount equals 0");
            return;
        }
        lkpBtn = (CommandButton) ae.getComponent();
        lookUpButtonID = ae.getComponent().getId();
        if (Character.isDigit(lookUpButtonID.charAt(lookUpButtonID.length() - 1))) {
            lookUpButtonID = lookUpButtonID.substring(0, lookUpButtonID.lastIndexOf("_"));
        }
        try {
            String RowNumber = ae.getComponent().getClientId(FacesContext.getCurrentInstance());
            StringTokenizer stringTokenizer = new StringTokenizer(RowNumber, ":");
            while (stringTokenizer.hasMoreTokens()) {
                String token = stringTokenizer.nextToken();
                if (FramworkUtilities.isNumber(token)) {
                    rowIndex = Integer.parseInt(token);
                }
            }
            logger.debug("Returning");
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
    }

    @Override
    public String getLookupConditionValue(String conditionExpression) {
        BaseEntity currentEntity = loadedList
                .get(FramworkUtilities.getObjectIndexInTabularScreen(lkpBtn, getContext()) % lazyModel.getPageSize());
        return String.valueOf(getValueFromEntity(currentEntity, conditionExpression));
    }

    public String lookup() {
        logger.debug("Entering");
        fieldLookup.setLookupFilter(new ArrayList<String>());
        // Get the value of lookupScreenName from lookupHashMap by the lookUpButtonID
        // Key
        fieldLookup = ((Lookup) getLookupHashMap().get(lookUpButtonID));

        String openedScreenInstId = "";

        // If the lookup screen is not that of the tree
        if (fieldLookup.getFieldDD().getControlType().getDbid() != DD.CT_LOOKUPATTRIBUTEBROWSER) {
            lookupHeaderStr = fieldLookup.getLookupHeader();
            if (lookupHeaderStr == null) {
                lookupHeaderStr = "";
            }
            fieldLookup.setLookupFilter(new ArrayList<String>());
        }

        // Get the value of recordID from recordIDhidden Value
        recordID = fieldLookup.getEntityDBID_H().getValue().toString();

        if (fieldLookup.getFieldDD().getControlType().getDbid() == DD.CT_LOOKUPATTRIBUTEBROWSER) {
            try {
                lookupPanelRendered = true;
                ODataType voidDataType = dataTypeService.loadODataType("VoidDT", systemUser);
                ODataMessage voidDataMessage = new ODataMessage();
                List data = new ArrayList();
                data.add(fieldLookup.getFieldExpressionTreeOEntity(headerObject, getOactOnEntity().getEntityClassName(),
                        ddService, entitySetupService, entityServiceLocal, uiFrameWorkFacade));
                voidDataMessage.setData(data);
                voidDataMessage.setODataType(voidDataType);
                openedScreenInstId = generateScreenInstId("FieldsTreeBrowser");
                SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.MESSAGE, voidDataMessage);
                SessionManager.setScreenSessionAttribute(openedScreenInstId, "isLookup", true);
                SessionManager.addSessionAttribute("lookupORScreen", getPortletInsId());
                openScreenInPopup("FieldsTreeBrowser", openedScreenInstId, POPUP_LOOKUPTYPE);
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
            logger.debug("Returning with Null");
            return null;
        }
        try {
            ODataType voidDataType = dataTypeService.loadODataType("VoidDT", systemUser);
            ODataMessage voidDataMessage = new ODataMessage();
            voidDataMessage.setData(new ArrayList<Object>());
            voidDataMessage.setODataType(voidDataType);
            openedScreenInstId = generateScreenInstId("OTMSPortletPopup");
            SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.MESSAGE, voidDataMessage);
            fieldLookup.getOScreen().getScreenInputs();
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        fieldLookup.buildLookupFilter();
        SessionManager.addSessionAttribute("lookupORScreenFilter", fieldLookup.getLookupFilter());
        SessionManager.addSessionAttribute("lookupORScreen", getPortletInsId());
        SessionManager.addSessionAttribute("lookupFieldExp", fieldLookup.getFieldExpression());
        openScreenInPopup(fieldLookup.getOScreen().getName(), fieldLookup.getOScreen().getViewPage(), lookupHeaderStr,
                openedScreenInstId, POPUP_LOOKUPTYPE);
        logger.debug("Returning with Null");
        TabularScreenDTO.createLookupMainScr(getCurrentScreen().getInstID(),
                fieldLookup.getOScreen().getName() + "_" + fieldLookup.getFieldExpression());
        setLookupMainScrInstID(fieldLookup.getOScreen().getName(), getCurrentScreen().getInstID());
        if (currentEntityObject != null) {
            functionService.setCurrentEntityObject(currentEntityObject);
        }
        return null;
    }

    @Override
    protected BaseEntity getObjectForLookup(long dbid) {
        logger.trace("Entering");
        List<BaseEntity> list = loadedList;
        if (dbid == 0) {
            return list.get(rowIndex % lazyModel.getPageSize());
        }
        for (int idx = 0; idx < list.size(); idx++) {
            if (list.get(idx).getDbid() == dbid) {
                ((LazyBaseEntityModel) lazyModel).setAddMode(true);
                if (list != null) {
                    logger.trace("Returning with: {}", list.get(idx));
                }
                return list.get(idx);
            }
        }
        logger.trace("Returning with Null");
        return null;
    }

    // <editor-fold defaultstate="collapsed" desc="C2A, LOOKUP handling functions!">
    public String getAlookupText() {
        return alookupText;
    }

    public void setAlookupText(String alookupText) {
        this.alookupText = alookupText;
    }
    // </editor-fold >

    /**
     * Generates filter expression from both attr & odm
     *
     * @param attr
     * @param odm
     * @return Valid expression when successfully generated <br>
     * null: no filter required as well; or error for which a user message added
     * to panel
     */
    protected String generateFilterExpression(ScreenDTMappingAttrDTO attr, ODataMessage odm) {
        logger.trace("Entering");
        try {
            if (attr == null || odm == null || odm.getData().isEmpty()) // FIXME: is it error? if so, add error to oFR
            {
                logger.trace("Returning with Null as Null attributes or Data Message");
                return null;
            }
            Object odmFirstElement = odm.getData().get(0);

            String fldExp = attr.getFieldExpression();
            if (fldExp.equalsIgnoreCase("this")) {
                if (restorePDataFromSessoion) {
                    logger.trace("Returning with Null");
                    return null;
                } else {
                    if (attr.getAttributeExpression() != null && !attr.getAttributeExpression().equals("")) {
                        return ("dbid = "
                                + getValueFromEntity(odmFirstElement, attr.getAttributeExpression() + ".dbid"));
                    } else {
                        return ("dbid = " + ((BaseEntity) odmFirstElement).getDbid());
                    }
                }
            }
            if (!attr.isForFilter()) // Only interested in filter attributes, and not set for this attribute
            {
                logger.trace("Returning with Null");
                return null; // Do nothing
            }
            Object value = null;
            if (attr.getAttributeExpression() != null && !attr.getAttributeExpression().equals("")) {
                value = getValueFromEntity(odmFirstElement, attr.getAttributeExpression());
            } else {
                value = odmFirstElement;
            }

            if (value instanceof BaseEntity) {
                // Consider when attribute has ".dbid"; use exisiting if found
                // and add it if not found. Added automatically by
                // UIFrameworkService.getParentAsScreenInput
                int fldExpLen = fldExp.length();
                String dbidSubstr = (fldExpLen > 5 ? fldExp.substring(fldExpLen - 5) : "");
                return (dbidSubstr.equalsIgnoreCase(".dbid") ? fldExp : fldExp + ".dbid") + " = "
                        + ((BaseEntity) value).getDbid();
            } else if (value instanceof String) {
                return (fldExp + " = '" + value + "'");
            } else {
                return (fldExp + " = " + value);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
            logger.trace("Returning with Null");
            return null;

        } finally {
        }
    }

    /**
     * Checks super.processInput first and returns false if failed <br>
     * Sets the filter found in the {@link OBackBean#currentScreenInput} into
     * {@link #inputFilter} <br>
     * Sets the filter found in the {@link OBackBean#compositeInputs} into
     * {@link #inputFilter} if {@link OBackBean#compositeInputRequired} <br>
     * Calls {@link TabularBean#updateDataFilter() } for the set inputFilter
     *
     * @return true: Successful processing false: Error, messagePanel is updated
     * with errors
     */
    @Override
    protected boolean processInput() {
        logger.trace("Entering");
        try {
            // Set inputFilter
            if (!super.processInput()) // Error
            {
                // Reset all, as may be related to old input in case of refreshing
                inputFilter = new ArrayList<String>();
                setLoadedList(new ArrayList());
                try {
                    currentScreen = (OScreen) textTranslationServiceRemote.loadEntityTranslation(currentScreen,
                            loggedUser);
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }
                onloadJSStr += "top.FABS.Portlet.setTitle({portletId:'" + getPortletInsId() + "', title:'"
                        + getCurrentScreen().getHeaderTranslated().replace("\\", "\\\\").replace("\'", "\\'") + "'});";
                logger.trace("Returning");
                return false;
            }
            // add this condition for EntityFilterMenuItem
            if (currentScreen.getName().equals("ManageEntityFilters")) {// related to EntityFilter Menu item.
                OScreen oScreen = (OScreen) SessionManager.getSessionAttribute("ParentScreenFromMenuItem");
                if (oScreen != null) {
                    inputFilter = new ArrayList<String>();
                    inputFilter.add("oentity.dbid = "
                            + entitySetupService.getOScreenEntityDTO(oScreen.getDbid(), loggedUser).getDbid());
                }
            }

            if (currentScreen.getName().equals("ManageCustomReportDynamicFilters")) {

                inputFilter = new ArrayList<String>();
                CustomReport customReport = (CustomReport) SessionManager
                        .getSessionAttribute("EntityFiltersForCustomReport");
                if (customReport != null) {
                    inputFilter.add("customReport.dbid=" + customReport.getDbid());
                }
            }
            if (dataMessage.getODataType().getDbid() == ODataType.DTDBID_VOID) // VoidDT, no processing required
            {
                if (!compositeInputRequired && inputFilter == null) // Only reset if composite input is not required.
                // As, if required,
                // some old valid inputs may be already in the filter, don't lose them
                {
                    inputFilter = new ArrayList<String>();
                }
                logger.trace("Returning with True");
                return true;
            }
            if (processMode && dataMessage.getODataType().getName().equalsIgnoreCase("DBID")) {
                inputFilter = new ArrayList<String>();
                inputFilter.add("dbid = " + dataMessage.getData().get(0));
                messagesPanel.addMessages(updateDataFilter());
                logger.trace("Returning with True");
                return true;
            }

            // Decide whether we need to refresh the input filter and load
            // data again or not. This is recommended for performance & flicking.
            // And, also, in PortalPage when selecting a new row in a screen that
            // is sender with no fitler in its attribute, without this check, it
            // will filter again the screen, losing all the rows insterted without
            // saving, e.g. 'Portal Page Composite' page in Testing Module
            boolean need2RefreshInputFilter = false;
            for (ScreenDTMappingAttrDTO curInputMapAttr : currentScreenInput.getAttributesMapping()) {
                if (curInputMapAttr.isForFilter()) {
                    need2RefreshInputFilter = true; // At least one attribute has filter
                    break;
                }
            }
            if (dataMessage.getData().size() == 3 && String.valueOf(dataMessage.getData().get(2)).equals("OrgChart")) {
                String parentFieldName = null;
                // <editor-fold defaultstate="collapsed" desc="Fill parentFieldName, condier
                // multiparents">
                List<String> parentFieldNames = BaseEntity
                        .getParentEntityFieldName(getOactOnEntity().getEntityClassPath());
                if (parentFieldNames.size() == 1) {
                    // Only one parent found
                    parentFieldName = parentFieldNames.get(0);

                } else if (parentFieldNames.size() > 1) {
                    // More than one parent found
                    // Decide the type of the passed parent
                    OFunctionResult parentOFR = null;
                    if (dataMessage.getData().size() == 3
                            && String.valueOf(dataMessage.getData().get(2)).equals("OrgChart")) {
                        parentOFR = uiFrameWorkFacade.getParentFromParentDBIDDTO(getOactOnEntity(),
                                (Long) dataMessage.getData().get(0), loggedUser);
                    } else {
                        parentOFR = uiFrameWorkFacade.getParentFromParentDBIDDTO(getOactOnEntity(),
                                (Long) dataMessage.getData().get(0), loggedUser);
                    }
                    if (!parentOFR.getErrors().isEmpty()) {
                        messagesPanel.addMessages(parentOFR);
                        logger.trace("Returning with False");
                        return false;
                    }
                    if (parentOFR.getReturnValues().isEmpty()) {
                        // No parent found matches the DBID
                        logger.warn("Parent DBID Not Found In Any Of The Parents Tables");
                        // FIXME: meaningful error message
                        messagesPanel.addErrorMessage(usrMsgService.getUserMessage("SystemInternalError", systemUser));
                        logger.trace("Returning with False");
                        return false;
                    }
                    parentFieldName = (String) parentOFR.getReturnValues().get(1); // parent field name
                }
                if (parentFieldName != null) {
                    inputFilter = new ArrayList<String>(); // Reset filter to refresh it
                    inputFilter.add(parentFieldName + ".dbid = " + dataMessage.getData().get(0));
                    messagesPanel.addMessages(updateDataFilter());
                    logger.trace("Returning with True");
                    return true;
                }
            }
            if (need2RefreshInputFilter) {
                inputFilter = new ArrayList<String>(); // Reset filter to refresh it
                // Generate filter expression from currentScreenInput
                for (ScreenDTMappingAttrDTO curInputMapAttr : currentScreenInput.getAttributesMapping()) {
                    String generatedFilterExp = generateFilterExpression(curInputMapAttr, dataMessage);
                    if (generatedFilterExp != null) {
                        inputFilter.add(generatedFilterExp);
                    }
                }
                // Generate filter expression from compositeInput
                // FIXED: avoid repeated expressions
                for (OScreenCompositeInput compInput : compositeInputs) {
                    ScreenDTMappingAttrDTO screenAtt = uiFrameWorkFacade
                            .getScreenInputAttMappingDTO(compInput.attr.getDbid(), loggedUser);
                    String generatedFilterExp = generateFilterExpression(screenAtt, compInput.odm);
                    if (generatedFilterExp != null) {
                        String checkFilterRecords[] = generatedFilterExp.split("=");
                        if (checkFilterRecords[0].trim() != null && !inputFilter.isEmpty()) {
                            for (Iterator<String> it = inputFilter.iterator(); it.hasNext();) {
                                String inputFilterExp = it.next();
                                if (inputFilterExp.startsWith(checkFilterRecords[0])) {
                                    it.remove();
                                }
                            }
                        }
                        inputFilter.add(generatedFilterExp);
                    }
                }
            }
            // add this condition for EntityFilterMenuItem
            if (currentScreen.getName().equals("ManageEntityFilterScreenActions_1")) {
                OScreen oScreen = (OScreen) SessionManager.getSessionAttribute("ParentScreenFromMenuItem");
                if (oScreen != null) {
                    inputFilter = new ArrayList<String>();
                    inputFilter.add("screen.dbid = " + oScreen.getDbid());
                }
            }

            if (currentScreen.getName().equals("ManageCustomReportDynamicFilterFields")) {

                inputFilter = new ArrayList<String>();
                CustomReportDynamicFilter dynamicFilter = (CustomReportDynamicFilter) SessionManager
                        .getSessionAttribute("CustomReportDynamicFilterENTITY");

                if (dynamicFilter != null) {
                    inputFilter.add("customReportDynamicFilter.dbid=" + dynamicFilter.getDbid());
                }
            }
            logger.trace("Returning with True");
            return true;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
            logger.trace("Returning with False");
            return false;
        }
    }
    // Commented as we do not need it any more
    // As we set changed = true in entity with editRow
    // Also we set changed = false in entity with cancelEditRow
    // About Validations added as JSF Validators
    //

    public void updateRow(ValueChangeEvent vce) {
        logger.debug("Entering");
        // $$TODO: sometimes old value is null and new value is "", why is that?
        try {
            UIComponent contolComponent = vce.getComponent();

            String compFieldExp = FramworkUtilities.getControlFldExp(contolComponent);

            if (compFieldExp == null || compFieldExp.contains("valuesMap")) {
                // Filter Field
                logger.debug("Returning");
                return;
            }

            String clientID = contolComponent.getClientId(getContext());
            String notJavaID = clientID.substring(0, clientID.lastIndexOf(":" + contolComponent.getId()));
            if (notJavaID == null || notJavaID.equalsIgnoreCase("tf")) {
                logger.debug("Returning");
                return;
            }
            int currentRow = (Integer.parseInt(notJavaID.substring(notJavaID.lastIndexOf(":") + 1)))
                    % lazyModel.getPageSize();
            Object oldValueObj = vce.getOldValue();
            Object newValueObj = vce.getNewValue();
            if (currentRow >= loadedList.size()) {
                logger.warn("Current Row index is greater than the loaded List Size");
                logger.debug("Returning");
                return;
            }
            BaseEntity currentRowObject = (BaseEntity) loadedList.get(currentRow);
            int rowIndexInDataTable = this.currentPage * dataTable.getRows() + currentRow;
            // lookupTransformedPickedElements.remove(rowIndexInDataTable);

            List<ScreenField> screenFields = getCurrentScreen().getScreenFields();
            ScreenField changedScrFld = null;

            applyDepencyInitialization(currentRowObject, newValueObj,
                    FramworkUtilities.getControlFldExp(contolComponent));
            int fldIndex = 0;
            // <editor-fold defaultstate="collapsed" desc="Get Component ScreenField">
            for (int idx = 0; idx < screenFields.size(); idx++) {
                if (screenFields.get(idx).getFieldExpression().equals(compFieldExp)) {
                    changedScrFld = screenFields.get(idx);
                    fldIndex = idx;
                    break;
                }
            }

            // FIXME: Commented from version r18.1.7 as there's no usage
            boolean valueCanged = Objects.equals(oldValueObj, newValueObj);
            // if (oldValueObj != null && newValueObj != null && compFieldExp != null) {
            // if (oldValueObj.equals(newValueObj)) {
            // valueCanged = false;
            // } else if (oldValueObj.toString().equals(newValueObj.toString())) {
            // valueCanged = false;
            // } else {
            // List<ExpressionFieldInfo> expressionFieldInfos =
            // BaseEntity.parseFieldExpression(actOnEntityCls, compFieldExp);
            // Class cls = expressionFieldInfos.get(expressionFieldInfos.size() -
            // 1).field.getType();
            //
            // if (cls == BigDecimal.class
            // && !oldValueObj.toString().equals("")
            // && !newValueObj.toString().equals("")) {
            // BigDecimal oldDecVal = new BigDecimal(oldValueObj.toString());
            // BigDecimal newDecVal = new BigDecimal(newValueObj.toString());
            //
            // if (oldDecVal.compareTo(newDecVal) != 0) {
            // valueCanged = true;
            // }
            //
            // } else {
            // valueCanged = true;
            // }
            // }
            // }
            if (changedScrFld != null && changedScrFld.getChangeFunction() != null && !valueCanged) {
                ODataType changeDT = dataTypeService.loadODataType("EntityChangeField", loggedUser);
                List changeData = new ArrayList();

                changeData.add(currentRowObject);
                changeData.add(compFieldExp);
                changeData.add(vce.getNewValue());

                ODataMessage changeODM = new ODataMessage(changeDT, changeData);

                OFunctionResult ofr = functionServiceRemote.runFunction(changedScrFld.getChangeFunction(), changeODM,
                        null, loggedUser);

                if (ofr.getReturnedDataMessage() != null && ofr.getReturnedDataMessage().getODataType() != null
                        && !ofr.getReturnedDataMessage().getData().isEmpty() && ofr.getReturnedDataMessage().getData()
                        .get(0).getClass().getName().equals(getOactOnEntity().getEntityClassPath())) {
                    currentRowObject = (BaseEntity) ofr.getReturnedDataMessage().getData().get(0);
                    for (int i = 0; i < screenFields.size(); i++) {
                        RequestContext.getCurrentInstance().update(getDataTable().getClientId() + ":" + currentRow + ":"
                                + FramworkUtilities.getID(screenFields.get(i), screenFields.get(i).getDd(), i));

                    }
                    RequestContext.getCurrentInstance().execute("KeepRowEditable(" + currentRow + ")");
                }
                messagesPanel.clearAndDisplayMessages(ofr);

            }

            if (FramworkUtilities.isComponentValueChanged(vce)) {
                // Value Changed
                // Get the changed screen field DD and set the relevant entity(s) to changed
                for (ScreenField sf : screenFields) {
                    if (!sf.getFieldExpression().equals(compFieldExp)) {
                        continue;
                    }
                    currentRowObject.setChanged(Lookup.getDBUpdatableEntityFieldExp(compFieldExp, sf.getDd()));
                    break;
                }
            }
            functionService.setCurrentEntityObject(currentRowObject);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
        }
    }

    public void validateUniquness(FacesContext context, UIComponent validate, Object value) {
        logger.debug("Entering");
        try {
            // screenFields
            //
            // if (true) {
            // logger.debug("Returning as i did't do anything");
            // return;
            // }
            oem.getEM(loggedUser);

            String notJavaID = validate.getClientId(context).substring(0,
                    validate.getClientId(context).lastIndexOf(":" + validate.getId()));
            int currentRow = Integer.parseInt(notJavaID.substring(notJavaID.lastIndexOf(":") + 1))
                    % lazyModel.getPageSize();
            Object oldValue = getValueFromEntity(loadedList.get(currentRow),
                    ((ExpressionFieldInfo) uniqueFields.get(validate.getId())).fieldParsedExpression);

            if ((((BaseEntity) loadedList.get(currentRow)).getDbid() != 0 && oldValue != null
                    && !value.equals(oldValue)) || ((BaseEntity) loadedList.get(currentRow)).getDbid() == 0) {
                List<BaseEntity> baseEntitys = oem.executeEntityListQuery(
                        uniqueFieldsQueries.get(validate.getId()) + "'" + value + "'", getLoggedUser());
                if (baseEntitys != null && baseEntitys.size() > 0) {
                    ((InputText) validate).setValid(false);
                    HtmlMessage message = new HtmlMessage();
                    UserMessage userMessage = usrMsgService.getUserMessage("UniqueValidation", loggedUser);
                    message.setTitle(userMessage.getMessageTitleTranslated());
                    message.setStyleClass("inlineErrorMsgTitle");
                    message.setFor(validate.getClientId(context));

                    FacesMessage msg = new FacesMessage(userMessage.getMessageTextTranslated());
                    dataTable.processUpdates(context);
                    context.addMessage(validate.getClientId(context), msg);// here is the problem
                    logger.debug("Returning");
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            oem.closeEM(loggedUser);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Filter Area">
    ArrayList<String> inputFilter, gridFilter, screenFilter;

    public ArrayList<String> getGridFilter() {
        return gridFilter;
    }

    public OFunctionResult setGridFilter(ArrayList<String> gridFilter) {
        this.gridFilter = gridFilter;
        return updateDataFilter();
    }

    public ArrayList<String> getInputFilter() {
        return inputFilter;
    }

    public OFunctionResult setInputFilter(ArrayList<String> inputFilter) {
        this.inputFilter = inputFilter;
        return updateDataFilter();
    }

    public ArrayList<String> getScreenFilter() {
        return screenFilter;
    }

    public OFunctionResult setScreenFilter(ArrayList<String> screenFilter) {
        this.screenFilter = screenFilter;
        return updateDataFilter();
    }

    protected OFunctionResult updateDataFilter() {
        OFunctionResult oFR = new OFunctionResult();
        try {

            if (restorePDataFromSessoion) {
                return oFR;
            }

            if (invalidInputMode) {
                loadedList = new ArrayList<BaseEntity>();
                return oFR;
            }

            if (firstTime) {
                dataFilter = new ArrayList<String>();
                List<String> tempFilter = null;

                tempFilter = getGridFilter();
                if (tempFilter != null) {
                    dataFilter.addAll(tempFilter);
                }
                tempFilter = getInputFilter();
                if (tempFilter != null) {
                    dataFilter.addAll(tempFilter);
                }
                tempFilter = getScreenFilter();
                if (tempFilter != null) {
                    dataFilter.addAll(tempFilter);
                }

                if (getActivityStateFilter() != null) {
                    dataFilter.addAll(getActivityStateFilter());
                }
                if (getActivityStateFilter() == null && !getCurrentScreen().isShowActiveOnly()) {
                    dataFilter.add("##ACTIVEANDINACTIVE");
                }
                if (getCurrentScreen().getScreenFilter() != null
                        && !getCurrentScreen().getScreenFilter().isInActive()) {
                    dataFilter.addAll(filterServiceBean.getFilterConditions(getCurrentScreen().getScreenFilter(),
                            getUserSessionInfo(), loggedUser));
                }
                dataFilter.add(OEntityManager.CONFCOND_CASE_INSENSITIVE);
                firstTime = false;
            }

            if (entityFilterScreenAction != null && entityFilterScreenAction.getEntityFilter() != null) {
                entityFilterScreenAction.getEntityFilter().setFilterFields(filterfields);
                dataFilter.addAll(filterServiceBean.getFilterConditions(entityFilterScreenAction.getEntityFilter(),
                        getUserSessionInfo(), loggedUser));
            }
            List<BaseEntity> tmpList = loadList(getOactOnEntity().getEntityClassName(), dataFilter, getDataSort());

            if (tmpList.size() > 0) {
                if (loadedList != null && loadedList.size() == tmpList.size() + 1) { //23826
                    BaseEntity firstRow = loadedList.get(0);
                    setLoadedList(tmpList);
                    loadedList.add(0, firstRow);
                } else {
                    setLoadedList(tmpList);
                }
                if (null != fieldExpressions) {
                    for (String fldExp : fieldExpressions) {
                        loadAttribute(fldExp);// If there null relation initialize it for
                        // display only
                    }
                }
            } else {
                setLoadedList(new ArrayList());
            }

        } catch (FilterDDRequiredException ex) {
            oFR.addError(usrMsgService.getUserMessage("FilterDDRequired", Collections.singletonList(ex.getFieldName()),
                    systemUser));
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
        }
        logger.debug("Returning");
        return oFR;
    }

    public String rectifyM2MCondition(String cond) {
        logger.debug("Entering");
        String condition = cond.trim();
        condition = condition.replaceAll(" ", "");

        if (condition.startsWith("#Parent") && headerObject != null) {
            String expression = condition.replace("#Parent.", "");
            condition = getValueFromEntity(headerObject, expression).toString();
        } else if (condition.startsWith("#CompositeInput(")) {
            String paramName = condition.substring(condition.indexOf("(") + 1, condition.indexOf(")"));
            String expression = condition.substring(condition.indexOf(")") + 2);

            for (int i = 0; i < compositeInputs.size(); i++) {
                if (compositeInputs.get(i).odm != null && compositeInputs.get(i).odm.getData().size() > 0
                        && compositeInputs.get(i).odm.getData().get(0).getClass().getSimpleName().contains(paramName)) {

                    condition = getValueFromEntity(compositeInputs.get(i).odm.getData().get(0), expression).toString();

                }
            }

        }
        logger.debug("Returning");
        return condition;
    }

    public List loadManyToManySetupList() {
        logger.debug("Entering");
        try {

            String setupFieldExpression = ((M2MRelationScreen) getCurrentScreen()).getSetupFieldExpression();
            String setupFilter = ((M2MRelationScreen) getCurrentScreen()).getM2MFilter();

            List<String> screenFieldExpressions = new ArrayList<String>();

            // <editor-fold defaultstate="collapsed" desc="Fill screenFieldExpressions from
            // screenFields">
            Iterator screenFieldsIterator = screenFields.iterator();
            while (screenFieldsIterator.hasNext()) {
                String fldExp = ((ScreenField) screenFieldsIterator.next()).getFieldExpression();
                if (fldExp.startsWith(setupFieldExpression)) {
                    fldExp = fldExp.substring(fldExp.indexOf(".") + 1);
                    screenFieldExpressions.add(fldExp);
                }
            }
            // </editor-fold>

            List<String> setupCond = new ArrayList<String>();
            setupCond.add(OEntityManager.CONFCOND_CASE_INSENSITIVE);
            String dataFilterConditions = "";

            // Adding the conditions that the screen data is initialy loaded so as to
            // reverse the condition to get the NOT EXISITNG records
            for (int i = 0; i < dataFilter.size(); i++) {
                // In order to apply the filter values, we must skip its values
                // from the existingRecordsQuery's 'DBID NOT IN'
                String currentDataFilter = dataFilter.get(i);
                if (getGridFilter() != null && !getGridFilter().isEmpty()) {
                    if (getGridFilter().contains(currentDataFilter)) {
                        // Remove the token before the first Dot
                        setupCond.add(currentDataFilter.substring(currentDataFilter.indexOf(".") + 1));
                        continue;
                    }
                }
                if (!dataFilterConditions.equals("") && !currentDataFilter.startsWith("##")) {
                    dataFilterConditions += " And mm." + currentDataFilter;
                } else if (!currentDataFilter.startsWith("##")) {
                    dataFilterConditions += " mm." + currentDataFilter;
                }
            }

            if (!dataFilterConditions.equals("")) {
                // Building a query near to that the screen data is loaded with to be put in the
                // "NOT IN" condition
                String existingRecordsQuery = "select mm." + setupFieldExpression + ".dbid From "
                        + getOactOnEntity().getEntityClassName() + " mm Where " + dataFilterConditions;

                // To load the NOT Exisiting Records
                setupCond.add("dbid not in (" + existingRecordsQuery + ")");
            }
            // Processing the setup filter that defined in the M2MRelationScreen Object
            if (setupFilter != null && !setupFilter.equals("")) {
                if (headerObject != null) {
                    setupFilter = setupFilter.replaceAll("#ParentEntityName", headerObject.getClassName());
                }

                // Replcae the reserved Keyword "#ParentEntityName"
                setupFilter = setupFilter.replaceAll("#Related.", "") // Replcae the reserved Keyword "#Related"
                        .replaceAll("\\(", " \\( ") // Surround braket "(" with spaces so as to split the string with
                        // spaces
                        .replaceAll("\\)", " \\) "); // Surround braket ")" with spaces so as to split the string with
                // spaces

                String[] filters = setupFilter.split("AND");

                for (int i = 0; i < filters.length; i++) {
                    String filter = filters[i];
                    String[] result = filter.split("=");
                    if (filter.contains("#Parent") && headerObject == null) {
                        logger.warn("Null Header Object while #Parent Is Found In Filter");
                        continue;
                    }

                    if (result.length == 2) {
                        String lhs = result[0];
                        String rhs = result[1];

                        setupCond.add(rectifyM2MCondition(lhs) + " = " + rectifyM2MCondition(rhs));

                    } else {
                        logger.warn("Error In M2M Filter");
                    }
                }
            }

            if (getCurrentScreen().getDataLoadingUserExit() != null) {
                ODataType screenDataLoadingUEDT = dataTypeService.loadODataType("ScreenDataLoadingUE", loggedUser);
                ODataMessage screenDataLoadingUEDM = new ODataMessage();
                screenDataLoadingUEDM.setODataType(screenDataLoadingUEDT);
                List<Object> odmData = new ArrayList<Object>();
                odmData.add(setupCond);
                odmData.add(headerObject);
                odmData.add(dataMessage);
                odmData.add(getCurrentScreen());
                screenDataLoadingUEDM.setData(odmData);

                OFunctionResult functionRetFR = functionService.runFunction(getCurrentScreen().getDataLoadingUserExit(),
                        screenDataLoadingUEDM, null, loggedUser);

                if (functionRetFR.getErrors().isEmpty()) {
                    setupCond.clear();
                    setupCond.addAll((List) functionRetFR.getReturnedDataMessage().getData().get(0));
                }

                // screen name
                OFunctionParms functionParms = new OFunctionParms();
                functionParms.getParams().put("ScreenName", getCurrentScreen().getName());

                // Call the function
                functionRetFR = functionService.runFunction(getCurrentScreen().getDataLoadingUserExit(),
                        screenDataLoadingUEDM, functionParms, loggedUser);
                if (functionRetFR.getErrors().isEmpty()) {
                    setupCond.clear();
                    setupCond.addAll((List) functionRetFR.getReturnedDataMessage().getData().get(0));
                }
            }

            if (getCurrentScreen().getOactOnEntity().getDataLoadingUserExit() != null) {

                ODataType screenDataLoadingUEDT = dataTypeService.loadODataType("ScreenDataLoadingUE", loggedUser);
                ODataMessage screenDataLoadingUEDM = new ODataMessage();
                screenDataLoadingUEDM.setODataType(screenDataLoadingUEDT);
                List<Object> odmData = new ArrayList<Object>();
                odmData.add(setupCond);
                odmData.add(headerObject);
                odmData.add(dataMessage);
                odmData.add(getCurrentScreen());
                screenDataLoadingUEDM.setData(odmData);

                OFunctionResult functionRetFR = functionService.runFunction(getCurrentScreen().getDataLoadingUserExit(),
                        screenDataLoadingUEDM, null, loggedUser);

                if (functionRetFR.getErrors().isEmpty()) {
                    setupCond.clear();
                    setupCond.addAll((List) functionRetFR.getReturnedDataMessage().getData().get(0));
                }

                // screen name
                OFunctionParms functionParms = new OFunctionParms();
                functionParms.getParams().put("ScreenName", getCurrentScreen().getName());

                // Call the function
                functionRetFR = functionService.runFunction(
                        getCurrentScreen().getOactOnEntity().getDataLoadingUserExit(), screenDataLoadingUEDM,
                        functionParms, loggedUser);

                if (functionRetFR.getErrors().isEmpty()) {
                    setupCond.clear();
                    setupCond.addAll((List) functionRetFR.getReturnedDataMessage().getData().get(0));
                }
            }

            // Load The Setup List Where are not Existing in the M2M relation
            List<BaseEntity> setupList = oem.loadEntityList(m2MSetupClass.getSimpleName(), setupCond,
                    screenFieldExpressions, null, getLoggedUser());

            List<BaseEntity> m2MList = new ArrayList<BaseEntity>();
            if (setupList != null) {
                // Creating new instances with the screen regular initialization and with the
                // Setup Object
                for (int i = 0; i < setupList.size(); i++) {
                    BaseEntity newSetupM2M = createNewInstance();
                    if (newSetupM2M == null) {
                        // FIXME: display error message
                        logger.warn("Error Creating New Instance");
                        break;
                    }
                    FramworkUtilities.setValueInEntity(newSetupM2M, setupFieldExpression, setupList.get(i));
                    applyDepencyInitialization(newSetupM2M, setupList.get(i), setupFieldExpression);
                    applyHardCodedInitialization(newSetupM2M);
                    m2MList.add(newSetupM2M);
                }
            }
            logger.debug("Returning");
            return m2MList;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    HashMap<String, String> filterMap = new HashMap<String, String>();

    public HashMap<String, String> getFilterMap() {
        return filterMap;
    }

    public void setFilterMap(HashMap<String, String> filterMap) {
        this.filterMap = filterMap;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Screen Data Backup ">
    @Override
    public void backupScreenData() {
        backupObject("loadedList", getLoadedList());
        backupObject("dataFilter", getDataFilter());
        backupObject("dataSort", getDataSort());
    }
    // </editor-fold>

    @Override
    protected void buildScreenExpression(UIComponent component) {
        Class[] parms = new Class[]{ValueChangeEvent.class};
        MethodBinding methodBinding = getFacesContext().getApplication()
                .createMethodBinding("#{" + getBeanName() + ".updateRow}", parms);
        if (component instanceof InputText) {
            ((InputText) component).setValueChangeListener(methodBinding);
        } else if (component instanceof org.primefaces.component.calendar.Calendar) {
            ((org.primefaces.component.calendar.Calendar) component).setValueChangeListener(methodBinding);
        } else if (component instanceof Password) {
            ((Password) component).setValueChangeListener(methodBinding);
        } else if (component instanceof InputTextarea) {
            ((InputTextarea) component).setValueChangeListener(methodBinding);
        } else if (component instanceof SelectOneRadio) {
            ((SelectOneRadio) component).setValueChangeListener(methodBinding);
        } else if (component instanceof SelectBooleanCheckbox) {
            ((SelectBooleanCheckbox) component).setValueChangeListener(methodBinding);
        }
    }

    public void validateLookup(ValueChangeEvent vce) {
        logger.debug("Entering");
        if (vce.getNewValue().equals(vce.getOldValue())) {
            logger.debug("Returning");
            return;
        }
        String query = "";
        try {
            List<ExpressionFieldInfo> infos = BaseEntity.parseFieldExpression(
                    Class.forName(getOactOnEntity().getEntityClassPath()),
                    FramworkUtilities.getControlFldExp(vce.getComponent()));
            ExpressionFieldInfo info = infos.get(infos.size() - 1);
            if (null != vce.getNewValue() && "".equals(vce.getNewValue())) {
                int idx = FramworkUtilities.getObjectIndexInTabularScreen(vce.getComponent(), getContext());
                BaseEntity baseEntity = (BaseEntity) getLoadedList().get(idx);
                FramworkUtilities.setValueInEntity(baseEntity, infos.get(0).fieldName, info.fieldClass.newInstance());
                FramworkUtilities.setValueInEntity(baseEntity, "changed", true);
                return;
            } else {

                BaseEntity entity = getLookupObject(info.fieldClass.getSimpleName(), info.fieldName,
                        vce.getNewValue().toString());
                if (entity == null) {

                    ((InputText) vce.getComponent()).setValid(false);
                    HtmlMessage message = new HtmlMessage();
                    UserMessage userMessage = usrMsgService.getUserMessage("InvalidValue", loggedUser);
                    message.setTitle(userMessage.getMessageTitleTranslated());
                    message.setStyleClass("inlineErrorMsgTitle");
                    message.setFor(vce.getComponent().getClientId(getContext()));

                    FacesMessage msg = new FacesMessage(userMessage.getMessageTextTranslated());
                    getContext().addMessage(vce.getComponent().getClientId(getContext()), msg);

                } else {
                    int idx = FramworkUtilities.getObjectIndexInTabularScreen(vce.getComponent(), getContext());
                    BaseEntity baseEntity = (BaseEntity) loadedList.get(idx);
                    FramworkUtilities.setValueInEntity(baseEntity, infos.get(0).fieldName, entity);
                    FramworkUtilities.setValueInEntity(baseEntity, "changed", true);
                }
                logger.trace("Query: {}", query);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }

    }

    public BaseEntity getLookupObject(String fldCls, String fldName, String value) {
        logger.trace("Entering");
        List<String> lkpconditions = new ArrayList<String>();
        lkpconditions.add(fldName + " ='" + value + "'");
        try {
            return (BaseEntity) oem.loadEntity(fldCls, lkpconditions, null, loggedUser);// executeEntityQuery(query,
            // systemUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Returning with Null");
            return null;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Create Buttons">
    /**
     * Create the screen buttons. <br>
     * You can override this function or any of the createXYZButton() in your
     * inherited class for your custom behaviour
     *
     * @param buttonPanel Panel to which the buttons will be added
     */
    protected void createButtons(HtmlPanelGrid buttonPanel) {
        createAddButton(buttonPanel);
        createEditAllButton(buttonPanel);
        createAddRowButton(buttonPanel);
        if (showSaveButton) {
            createSaveButton(buttonPanel);
        }

        if (showSaveExitButton) {
            createSaveExitButton(buttonPanel);
        }

        createShowHideRelationRecordsButton(buttonPanel);
    }

    private CommandButton editAllButton;

    public CommandButton getEditAllButton() {
        return editAllButton;
    }

    public void setEditAllButton(CommandButton editAllButton) {
        this.editAllButton = editAllButton;
    }

    protected CommandButton createEditAllButton(HtmlPanelGrid buttonPanel) {
        TabularScreen screen = (TabularScreen) getCurrentScreen();
        if (!screen.isEditable()) {
            return null;
        }
        if (screen instanceof M2MRelationScreen) {
            return null;
        }
        setEditAllButton(createButton(buttonPanel, ddService.getDDLabel("editAllButton", loggedUser), "editAllAction",
                "editAllDisabled", getBeanName()));
        editAllButton.setImmediate(false);
        editAllButton.setTitle("Edit All");
        editAllButton.setValue("Edit All");
        editAllButton.setStyleClass("edit_all_btn");
        editAllButton.setIcon("new_row_btn_icon");
        editAllButton.setUpdate("tf:generalPanel");
        editAllButton.setOncomplete("editAll()");
        editAllButton.setValueExpression("binding", exFactory.createValueExpression(elContext,
                "#{" + getBeanName() + ".editAllButton}", CommandButton.class));
        setAddRowBtn(getAddRowButton());

        return getEditAllButton();
    }

    public void editAllAction() {
        editAllClicked = true;
    }

    /**
     * Create & Return SaveExit Button <br>
     * Action is bound to "saveRowObjectListExitAction" <br>
     * Is not displayed if Screen is not Editable & not Removable & not FasTrack
     * (allow Add New Record) <br>
     * Override this function for your custom behavior
     *
     * @return Return the created button <br>
     * null: no button will be created in the screen
     */
    private CommandButton saveExitButton;

    protected CommandButton createSaveExitButton(HtmlPanelGrid buttonPanel) {
        TabularScreen screen = (TabularScreen) getCurrentScreen();
        if (!screen.isEditable() && !screen.isRemovable() && !screen.isFastTrack() && !screen.isSaveAndExit()) {
            return null;
        }
        // DD saveExitButtonDD = ddService.getDD("saveExitButton", loggedUser);
        setSaveExitButton(createButton(buttonPanel, ddService.getDDLabel("saveExitButton", loggedUser),
                "saveRowObjectListExitAction", "saveExitDisabled", getBeanName()));
        // enforce validations on input controls
        getSaveExitButton().setImmediate(false);
        getSaveExitButton().setStyleClass("save_exit_btn");
        getSaveExitButton().setIcon("save_exit_btn_icon");
        getSaveExitButton().setValueExpression("binding", exFactory.createValueExpression(elContext,
                "#{" + getBeanName() + ".saveExitButton}", CommandButton.class));
        return getSaveExitButton();
    }

    public boolean getEditAllDisabled() {
        if (currentScreenInput == null || dataMessage == null || dataMessage.getData() == null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean getSaveDisabled() {
        if (currentScreenInput == null || dataMessage == null || dataMessage.getData() == null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean getSaveExitDisabled() {
        if (currentScreenInput == null || dataMessage == null || dataMessage.getData() == null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean getDeleteDisabled() {
        if (currentScreenInput == null || dataMessage == null || dataMessage.getData() == null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean getDuplicateDisabled() {
        if (currentScreenInput == null || dataMessage == null || dataMessage.getData() == null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean getNewRowDisabled() {
        if (currentScreenInput == null || dataMessage == null || dataMessage.getData() == null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean getAddDisabled() {
        if (currentScreenInput == null || dataMessage == null || dataMessage.getData() == null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Create & Return Save Button <br>
     * Action is bound to "saveRowObjectListAction" <br>
     * Is not displayed if Screen is not Editable & not Removable & not FasTrack
     * (allow Add New Record) <br>
     * Override this function for your custom behavior
     *
     * @return Return the created button <br>
     * null: no button will be created in the screen
     */
    private CommandButton saveButton;

    protected CommandButton createSaveButton(HtmlPanelGrid buttonPanel) {
        logger.trace("Entering");
        TabularScreen screen = (TabularScreen) getCurrentScreen();
        if (!screen.isEditable() && !screen.isRemovable() && !screen.isFastTrack()) {
            logger.trace("Returning with Null");
            return null;
        }
        // DD saveButtonDD = ddService.getDD("saveButton", loggedUser);
        setSaveButton(createButton(buttonPanel, ddService.getDDLabel("saveButton", loggedUser),
                "saveRowObjectListAction", "saveDisabled", getBeanName()));
        // enforce validations on input controls
        getSaveButton().setImmediate(false);
        getSaveButton().setStyleClass("save_btn");
        getSaveButton().setOnstart("validateMandaroryFields();");
        getSaveButton().setIcon("save_btn_icon");
        getSaveButton().setValueExpression("binding",
                exFactory.createValueExpression(elContext, "#{" + getBeanName() + ".saveButton}", CommandButton.class));
        if (getSaveButton() != null) {
            logger.trace("Returning with: {}", getSaveButton().getTitle());
        }
        return getSaveButton();
    }

    /**
     * Create & Return Add Button <br>
     * Action is bound to "addAction" <br>
     * Is not displayed if Screen is not FasTrack (allow Add New Record) or is
     * not Basic <br>
     * Is not created for M2MRelationScreen <br>
     * Override this function for your custom behavior
     *
     * @return Return the created button <br>
     * null: no button will be created in the screen
     */
    private CommandButton addButton;

    protected CommandButton createAddButton(HtmlPanelGrid buttonPanel) {
        TabularScreen screen = (TabularScreen) getCurrentScreen();
        if (!screen.isBasic()) {
            return null;
        }
        if (screen instanceof M2MRelationScreen) {
            return null;
        }
        // DD addButtonDD = ddService.getDD("addButton", loggedUser);
        setAddButton(createButton(buttonPanel, ddService.getDDLabel("addButton", loggedUser), "addAction",
                "addDisabled", getBeanName()));
        // This will skip validation on input controls
        getAddButton().setImmediate(false);
        getAddButton().setStyleClass("add_btn");
        getAddButton().setIcon("add_btn_icon");
        getAddButton().setAjax(true);
        getAddButton().setUpdate("tf:generalPanel");
        getAddButton().setValue("add");
        getAddButton().setValueExpression("binding",
                exFactory.createValueExpression(elContext, "#{" + getBeanName() + ".addButton}", CommandButton.class));
        return getAddButton();
    }

    /**
     * Create & Return AddNew Button <br>
     * Action is bound to "addRowAction" <br>
     * Is not created for M2MRelationScreen <br>
     * Is not displayed if Screen is not FasTrack (allow Add New Record) <br>
     * Override this function for your custom behavior
     *
     * @return Return the created button <br>
     * null: no button will be created in the screen
     */
    private CommandButton addRowButton;

    protected CommandButton createAddRowButton(HtmlPanelGrid buttonPanel) {
        TabularScreen screen = (TabularScreen) getCurrentScreen();
        if (!screen.isFastTrack()) {
            return null;
        }
        if (screen instanceof M2MRelationScreen) {
            return null;
        }
        setAddRowButton(createButton(buttonPanel, ddService.getDDLabel("addRowButton", loggedUser), "addRowAction",
                "newRowDisabled", getBeanName()));
        // This will skip validation on input controls
        getAddRowButton().setImmediate(false);
        getAddRowButton().setStyleClass("new_row_btn");
        getAddRowButton().setIcon("new_row_btn_icon");
        getAddRowButton().setUpdate("tf:generalPanel");
        getAddRowButton().setOncomplete("editLastDatatableRow();");
        getAddRowButton().setValueExpression("binding", exFactory.createValueExpression(elContext,
                "#{" + getBeanName() + ".addRowButton}", CommandButton.class));
        setAddRowBtn(getAddRowButton());
        return getAddRowButton();

    }

    /**
     * Create & Return ShowHide Button <br>
     * Is ONLY created for M2MRelationScreen <br>
     * Action is bound to "showHideAction" <br>
     * Override this function for your custom behavior
     *
     * @return Return the created button <br>
     * null: no button will be created in the screen
     */
    private CommandButton showHideButton;

    protected CommandButton createShowHideRelationRecordsButton(HtmlPanelGrid buttonPanel) {
        if (!(getCurrentScreen() instanceof M2MRelationScreen)) {
            return null;
        }
        String showHideButtonDD = ddService.getDDLabel("showHideButton", loggedUser);
        setShowHideButton(createButton(buttonPanel, showHideButtonDD, "showHideAction", null, getBeanName()));
        getShowHideButton().setValue(showHideButtonDD);
        getShowHideButton().setImmediate(true);
        getShowHideButton().setUpdate("tf:generalPanel");
        getShowHideButton().setId(getFacesContext().getViewRoot().createUniqueId() + "showHideBTN");
        getShowHideButton().setStyleClass("general_btn");
        getShowHideButton().setValueExpression("binding", exFactory.createValueExpression(elContext,
                "#{" + getBeanName() + ".showHideButton}", CommandButton.class));
        return getShowHideButton();
    }

    // <editor-fold defaultstate="collapsed" desc="More Actions Menu">
    protected void createMoreActionsMenu(HtmlPanelGrid buttonPanel) {
    }

    protected MenuItem createDeleteMenu() {
        TabularScreen screen = (TabularScreen) getCurrentScreen();
        if (!screen.isRemovable()) {
            return null;
        }
        if (getCurrentScreen() instanceof M2MRelationScreen) {
            return null;
        }
        MenuItem deleteAction = createMenuItem("deleteButton", "deleteRowObjectAction", null, "deleteDisabled", null);
        deleteAction.setId(getFacesContext().getViewRoot().createUniqueId() + "DeleteACTN");
        String deleteButtonDD = ddService.getDDLabel("deleteButton", loggedUser);
        deleteAction.setValue(deleteButtonDD);
        // this will skips input controls validation
        deleteAction.setImmediate(false);
        String deletionMsg = "";

        deletionMsg += "Are you sure you want to delete the selected record(s)?";

        deleteAction.setOnclick("var conf = confirm('" + deletionMsg + "'); if (!conf){ return false}");
        return deleteAction;
    }

    protected CommandButton createDeleteButton(HtmlPanelGrid editRowPanel) {
        TabularScreen screen = (TabularScreen) getCurrentScreen();
        if (!screen.isRemovable()) {
            return null;
        }
        if (getCurrentScreen() instanceof M2MRelationScreen) {
            return null;
        }
        Class[] parms3 = new Class[]{ActionEvent.class};
        CommandButton deleteAction = createButton(editRowPanel, "deleteButton", "deleteRowObjectAction",
                "deleteDisabled", getBeanName(), parms3);
        deleteAction.setId(getFacesContext().getViewRoot().createUniqueId() + "DeleteACTN");
        String deleteButtonDD = ddService.getDDLabel("deleteButton", loggedUser);
        deleteAction.setValue(" ");
        deleteAction.setTitle(deleteButtonDD);
        // this will skips input controls validation
        deleteAction.setImmediate(true);
        deleteAction.setProcess("@this");
        deleteAction.setStyleClass("delete_btn");
        String deletionMsg = "";
        deletionMsg += "Are you sure you want to delete the selected record(s)?";
        deleteAction.setOnclick("var conf = confirm('" + deletionMsg + "'); if (!conf){ return false}");
        return deleteAction;
    }

    protected CommandButton createDeleteWithoutConfButton(HtmlPanelGrid editRowPanel) {
        TabularScreen screen = (TabularScreen) getCurrentScreen();

        Class[] parms3 = new Class[]{ActionEvent.class};
        CommandButton deleteAction = createButton(editRowPanel, "deleteWithoutConfButton", "deleteRowObjectAction",
                "deleteDisabled", getBeanName(), parms3);
        deleteAction.setId(getFacesContext().getViewRoot().createUniqueId() + "DeleteWithoutConfACTN");
        String deleteButtonDD = ddService.getDDLabel("deleteButton", loggedUser);
        deleteAction.setValue(" ");
        deleteAction.setTitle(deleteButtonDD);
        // this will skips input controls validation
        deleteAction.setImmediate(true);
        deleteAction.setProcess("@this");
        deleteAction.setStyleClass("deletewithoutconf_btn");

        // deleteAction.setRendered(false);
        return deleteAction;
    }

    protected CommandButton createDuplicateButton(HtmlPanelGrid editRowPanel) {
        TabularScreen screen = (TabularScreen) getCurrentScreen();
        if (!screen.isFastTrack() || !screen.isDuplicate()) {
            return null;
        }
        if (getCurrentScreen() instanceof M2MRelationScreen) {
            return null;
        }
        Class[] parms3 = new Class[]{ActionEvent.class};
        CommandButton duplicateAction = createButton(editRowPanel, "duplicateButton", "duplicateRowObjectAction",
                "duplicateDisabled", getBeanName(), parms3);
        duplicateAction.setId(getFacesContext().getViewRoot().createUniqueId() + "DuplicateACTN");
        String duplicationMsg = "Are you sure you want to duplicate the selected record(s)?";
        duplicateAction.setOnclick("var conf = confirm('" + duplicationMsg + "'); if (!conf){ return false}");
        String duplicateItemDD = ddService.getDDLabel("duplicateButton", loggedUser);
        duplicateAction.setStyleClass("duplicate_btn");
        duplicateAction.setProcess("@this");
        duplicateAction.setValue(" ");
        duplicateAction.setTitle(duplicateItemDD);
        return duplicateAction;
    }

    protected MenuItem createDuplicateMenu() {
        TabularScreen screen = (TabularScreen) getCurrentScreen();
        if (!screen.isFastTrack()) {
            return null;
        }
        if (getCurrentScreen() instanceof M2MRelationScreen) {
            return null;
        }

        MenuItem duplicateAction = createMenuItem("duplicateButton", "duplicateRowObjectAction", null,
                "duplicateDisabled", null);
        duplicateAction.setId(getFacesContext().getViewRoot().createUniqueId() + "DuplicateACTN");

        String duplicationMsg = "Are you sure you want to duplicate the selected record(s)?";
        duplicateAction.setOnclick("var conf = confirm('" + duplicationMsg + "'); if (!conf){ return false}");
        String duplicateItemDD = ddService.getDDLabel("duplicateButton", loggedUser);
        duplicateAction.setValue(duplicateItemDD);
        return duplicateAction;
    }

    // <editor-fold defaultstate="collapsed" desc="Buttons Actions">
    public String showHideAction() {
        logger.debug("Entering");
        if (m2MShowAll) {
            List tmplist = new ArrayList();
            for (int i = 0; i < loadedList.size(); i++) {
                if (((BaseEntity) loadedList.get(i)).getDbid() != 0) {
                    tmplist.add(loadedList.get(i));
                }
            }
            loadedList.addAll(tmplist);
            m2MShowAll = false;
        } else {
            loadedList.addAll(loadManyToManySetupList());
            m2MShowAll = true;
        }
        RequestContext.getCurrentInstance().update("tf:generalPanel");
        logger.debug("Returning");
        return "";
    }

    public void duplicateRowObjectAction(ActionEvent event) {
        logger.debug("Returning");
        OFunctionResult oFR = new OFunctionResult();
        try {
            oem.getEM(loggedUser);
            int currentInstanceID = -1;
            String value = event.getComponent().getParent().getParent().getClientId();// get("instID");
            value = value.substring(0, value.lastIndexOf(":"));
            value = value.substring(value.lastIndexOf(":") + 1);
            currentInstanceID = (Integer.parseInt(value) % lazyModel.getPageSize());
            if (currentInstanceID == -1) {
                logger.debug("Returning");
                return;
            }
            OFunctionResult returnResults = new OFunctionResult();
            OEntityDTO oentity = entitySetupService
                    .loadOEntityDTOByClassName(loadedList.get(currentInstanceID).getClassName(), loggedUser);
            OFunctionParms functionParms = new OFunctionParms();
            functionParms.getParams().put("ScreenName", getCurrentScreen().getName());
            functionParms.getParams().put("ScreenDT", getDataMessage());
            // If Duplicating OPortalPage:
            ODataMessage odm = new ODataMessage();
            if (oentity.getEntityClassName().equals("OPortalPage")) {
                BaseEntity portalPage = loadedList.get(currentInstanceID);
                odm = dataTypeService.constructEntityDefaultODM(portalPage, loggedUser);
                returnResults = uiFrameworkEntityManager.duplicateOPortalPage(odm, null, loggedUser);
                oFR.append(returnResults);
            } else {
                // If Duplicating entities other than OPortalPage:
                BaseEntity baseEntity = loadedList.get(currentInstanceID);
                odm = dataTypeService.constructEntityDefaultODM(baseEntity, loggedUser);
                // returnResults = uiFrameworkEntityManager.duplicateEntity(odm, null,
                // loggedUser);
                OEntityActionDTO duplicateAction = entitySetupService.getOEntityDTOActionDTO(
                        getOactOnEntity().getDbid(), OEntityActionDTO.ATDBID_DUPLICATE, loggedUser);
                if (duplicateAction != null) {
                    returnResults = entitySetupService.executeAction(duplicateAction, odm, functionParms, loggedUser);
                } else {
                    returnResults = uiFrameworkEntityManager.duplicateEntity(odm, null, loggedUser);
                    logger.warn("No Duplicate Action Assigned For That Entity");
                }

                oFR.append(returnResults);
            }

            // Add duplicated entities to loadedList:
            if (returnResults.getErrors().size() <= 0) {
                List retVals = returnResults.getReturnValues();
                if (retVals != null) {
                    if (retVals.size() > 0) {
                        BaseEntity duplicatedEntity = (BaseEntity) retVals.get(0);
                        loadedList.add(duplicatedEntity);
                    }
                }
            }
        } catch (Exception ex) {
            logger.debug("Returning");
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
        } finally {
            oem.closeEM(loggedUser);
            messagesPanel.clearAndDisplayMessages(oFR);
            RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
            RequestContext.getCurrentInstance().update(dataTable.getClientId());
            logger.debug("Returning");
        }
    }

    public void deleteRowObjectAction(ActionEvent event) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            OEntityActionDTO deleteAction = entitySetupService.getOEntityDTOActionDTO(getOactOnEntity().getDbid(),
                    OEntityActionDTO.ATDBID_DELETE, loggedUser);

            if (deleteAction == null) {
                logger.warn("No Deletion Action Found");
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser));
                messagesPanel.clearAndDisplayMessages(oFR);
                logger.debug("Returning");
                return;
            }
            ODataType entityDataType = getActOnEntityDataType();
            if (deleteAction != null) {
                oFR.append(runParentValidation(deleteAction));
            }

            List<BaseEntity> successfullyDeleted = new ArrayList();
            int currentInstanceID = -1;
            String value = event.getComponent().getParent().getParent().getClientId();// get("instID");
            value = value.substring(0, value.lastIndexOf(":"));
            value = value.substring(value.lastIndexOf(":") + 1);
            currentInstanceID = (Integer.parseInt(value) % lazyModel.getPageSize());
            if (currentInstanceID == -1) {
                logger.debug("Returning");
                return;
            }
            OFunctionParms functionParms = new OFunctionParms();
            functionParms.getParams().put("ScreenName", getCurrentScreen().getName());
            functionParms.getParams().put("ScreenDT", getDataMessage());
            // Hard delete for screen filter
            if (entityDataType.getName().equalsIgnoreCase("ScreenFilter")) {
                BaseEntity entity = loadedList.get(currentInstanceID);
                List<String> conditions = new ArrayList();
                conditions.add("filter.dbid=" + entity.getDbid());
                List<FilterField> filterFields = oem.loadEntityList(FilterField.class.getSimpleName(), conditions, null,
                        null, loggedUser);
                if (filterFields != null && !filterFields.isEmpty()) {
                    for (FilterField field : filterFields) {
                        oem.deleteEntity(field, loggedUser);
                    }
                }
                OFilter ofilter = (OFilter) oem.loadEntity(OFilter.class.getSimpleName(),
                        Collections.singletonList("dbid = " + entity.getDbid()), null, loggedUser);

                if (ofilter != null) {
                    oem.deleteEntity(ofilter, loggedUser);
                }
                oem.deleteEntity(entity, loggedUser);
                oFR.addSuccess(usrMsgService.getUserMessage("FilterIsSuccessFullyDeleted", loggedUser));
                successfullyDeleted.add(entity);
            } else if (entityDataType != null && deleteAction != null && oFR.getErrors().isEmpty()) {
                BaseEntity entity = loadedList.get(currentInstanceID);
                ODataMessage entityDataMessage = (ODataMessage) constructOutput(entityDataType, entity)
                        .getReturnedDataMessage();

                OFunctionResult ofr = entitySetupService.executeAction(deleteAction, entityDataMessage, functionParms,
                        getLoggedUser());
                oFR.append(ofr);
                if (ofr.getErrors() != null && ofr.getErrors().isEmpty()) {
                    successfullyDeleted.add(entity);
                }
            }

            // Remove the successfully deleted entities only.
            try {
                List<BaseEntity> newList = new ArrayList<BaseEntity>((List) lazyModel.getWrappedData());
                for (BaseEntity baseEntity : successfullyDeleted) {
                    loadedList.remove(baseEntity);
                    newList.remove(baseEntity);
                }
                lazyModel.setWrappedData(newList);
            } catch (Exception e) {
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser), e);
                messagesPanel.clearAndDisplayMessages(oFR);
            }
            messagesPanel.clearAndDisplayMessages(oFR);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            if (getMessages() != null) {
                UserMessage o = new UserMessage(); // FIXME: use Database UserMessage
                o.setMessageText(" Cannot Delete The Selected Record!");
                o.setMessageTitle("Error Message");
                getMessages().add(o);
                setMessageGrid(buildMessagePanel());
            } else {
                messagesPanel.addErrorMessage(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
            }
        }
        RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
        RequestContext.getCurrentInstance().update(dataTable.getClientId());
        logger.debug("Returning");
    }

    UserMessageServiceRemote messageService = OEJB.lookup(UserMessageServiceRemote.class);

    public void deleteRowWithoutConfirmation(int rowNum) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            if (rowNum == -1) {
                logger.debug("Returning");
                return;
            }
            BaseEntity entity = loadedList.get(rowNum);
            try {
                loadedList.remove(entity);
                ((LazyBaseEntityModel) lazyModel).removeRow(entity);
            } catch (Exception e) {
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser), e);
                messagesPanel.clearAndDisplayMessages(oFR);
            }
            messagesPanel.clearAndDisplayMessages(oFR);
        } catch (Exception ex) {
            messagesPanel.addErrorMessage(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
        }
        RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
        RequestContext.getCurrentInstance().update(dataTable.getClientId());
        logger.debug("Returning");
    }

    public String saveRowObjectListAction() {
        logger.debug("Entering");
        int count = count(liferayUsers.get(loggedUser.getLoginName()));
        if (count == 0) {
            logout();
            logger.debug("Returning as count equals 0");
            return null;
        }
        OFunctionResult oFR = new OFunctionResult();
        // <editor-fold defaultstate="collapsed" desc="Log">
        Long timeBegin = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>
        try {
            OEntityActionDTO parentPreSave = entitySetupService.getOEntityDTOActionDTO(getOactOnEntity().getDbid(),
                    OEntityActionDTO.ATDBID_ParentPreSave, loggedUser);
            if (parentPreSave != null && getLoadedList().size() > 0) {
                oFR.append(runParentValidation(parentPreSave));
            }
            if (oFR.getErrors() != null && oFR.getErrors().size() > 0) {
                messagesPanel.clearAndDisplayMessages(oFR);
                RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
                logger.debug("Returning with Null");
                return null;
            }
            List<BaseEntity> lodedListTmp = new ArrayList<>(loadedList);
            if (Objects.equals(lookupMainScr, getCurrentScreen().getInstID())) {
                lodedListTmp.addAll(lookupTransformedPickedElements);
                clearLookupPickedCache();
            }

            List<BaseEntity> savedTempList = new ArrayList<BaseEntity>();
            // TODO $$ Performance: send the entitylist to the save/update action, support
            // the data types for this
            HashSet savedEntities = new HashSet();
            for (BaseEntity entity : lodedListTmp) {
                if (!savedEntities.contains(entity)) {
                    oFR.append(saveRow(entity, savedTempList));
                    savedEntities.add(entity);
                }
            }
            for (int i = 0; i < savedTempList.size(); i++) {
                ((LazyBaseEntityModel) lazyModel).removeRow(savedTempList.get(i));
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            if (oFR.getErrors() == null || oFR.getErrors().isEmpty()) {
                RequestContext.getCurrentInstance().update("tf:generalPanel");
            }
            messagesPanel.clearAndDisplayMessages(oFR);
            RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
        }
        logger.debug("Returning with Null");
        return null;
    }

    private void clearLookupPickedCache() {
        lookupMainScr = 0;
        clearLookupTransformedElementsCache();
        SessionManager.addSessionAttribute("lookupPickedElementsMainScr", 0l);
        Map<String, Object> sessionMap = SessionManager.getSessionMap();
        for (Map.Entry<String, Object> sessionElement : sessionMap.entrySet()) {
            String key = sessionElement.getKey();
            if (key.contains(currentScreen.getInstID().toString())) {
                sessionMap.remove(key);
                break;
            }
        }
        ((LazyBaseEntityModel) lazyModel).setMasterNewEntity(null);
        TabularScreenDTO.destroyLookupMainScr(currentScreen.getInstID());
    }

    private void clearLookupTransformedElementsCache() {
        lookupTransformedPickedElements = new LinkedList();
        SessionManager.addSessionAttribute("lookupTransformedPickedElements", new LinkedList());
    }

    public void saveRowObjectListExitAction() {
        saveRowObjectListAction();

        if (exit) {
            if (openedAsPopup) {
                logger.trace("Calling JS closePopupNoID with PortletInsId: {}", getPortletInsId());
                RequestContext.getCurrentInstance()
                        .execute("top.FABS.Portlet.closePopupNoID('" + getPortletInsId() + "');");

            } else {
                OPortalUtil.removePortlet(getPortletInsId(), screenInstId);
            }
        }
    }

    protected int currentPage;

    public void onPage(PageEvent event) {
        currentPage = event.getPage();
        if (getAddRowButton() != null) {
            RequestContext.getCurrentInstance().update(getAddRowButton().getClientId());
        }
    }

    public String addRowAction() {
        final DataTable currentDataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
                .findComponent(dataTable.getClientId());
        if (currentDataTable != null) {
            int first = 0;
            currentDataTable.setFirst(first);
            setFirst(first);
        }
        logger.debug("Returning with Null");

        int count = count(liferayUsers.get(loggedUser.getLoginName()));
        if (count == 0) {
            logout();
            logger.debug("Returning with Null");
            return null;
        }
        try {
            oem.getEM(loggedUser);
            messagesPanel.clearMessages();

            if (currentScreen.getScreenFilter() != null && !currentScreen.getScreenFilter().isInActive()
                    && currentScreen.getScreenFilter().isShowInsideScreen()) {
                updateFilterFields(filterfields);
            }

            BaseEntity newEntity = createNewInstance();
            if (loadedList != null && loadedList.size() > 0 && loadedList.get(0).getDbid() == 0) {
                return "";
            }
            functionService.setCurrentEntityObject(newEntity);
            if (newEntity == null) {
                // Assuming createNewInstance displayed corresponding error message
                logger.debug("Returning with \" \"");
                return "";
            }
            newEntity.generateInstID();
            if (loadedList == null) {
                loadedList = new ArrayList<BaseEntity>();
            }
            ((LazyBaseEntityModel) lazyModel).setMasterNewEntity(newEntity);
            ((LazyBaseEntityModel) lazyModel).addRow(newEntity, getFirst());
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
                    .put("entityForAttachment" + getCurrentScreen().getDbid(), newEntity);
            functionService.setCurrentEntityObject(newEntity);
        } catch (Exception ex) {
            logger.debug("Returning with Null");
            messagesPanel.addErrorMessage(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex, true);
        } finally {
            oem.closeEM(loggedUser);
        }

        // RequestContext.getCurrentInstance().execute(
        // "top.FABS.Portlet.updateIframeSize('" + portletInsId + "Frame');");
        if (getScreenHeight() == null || getScreenHeight().equals("") || getScreenHeight().equals("undefined")
                || getScreenHeight().equals("null")) {
            RequestContext.getCurrentInstance()
                    .execute("top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');");
        } else {
            RequestContext.getCurrentInstance().execute("top.FABS.Portlet.updateIframeSizeWithHeight({iframeName:'"
                    + getPortletInsId() + "Frame'" + ",screenHeight:'" + getScreenHeight() + "'});");
        }

        for (String id : fieldIDs) {
            RequestContext.getCurrentInstance().update("tf:" + id);
        }
        logger.debug("Returning with \" \"");
        return "";
    }

    public String addAction() {
        logger.debug("Returning");
        try {
            oem.getEM(loggedUser);

            FormScreen formScreen = (FormScreen) uiFrameWorkFacade.getBasicDetailScreenByOEntityDTO(getOactOnEntity(),
                    systemUser);
            if (formScreen == null) {
                logger.warn("Error Getting Basic Detail Screen");
                messagesPanel.clearAndDisplayMessages(
                        functionService.constructOFunctionResultFromError("NoScreenFound", systemUser));
                oem.closeEM(loggedUser);
                logger.debug("Returning with Null");
                return null;
            }
            String openedScreenInstId = generateScreenInstId(formScreen.getName());
            SessionManager.setScreenSessionAttribute(openedScreenInstId, MODE_VAR, String.valueOf(OScreen.SM_ADD));
            SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.C2A_VAR, false);
            // Pass the same data message
            if (dataMessage != null) {
                SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.MESSAGE, dataMessage);
            }
            SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.BASICSCREENID, getPortletInsId());
            SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.BASICSCREENMSG, dataMessage);

            // Open the new portlet or screen
            return openScreenInPopup(formScreen, openedScreenInstId, POPUP_SCREENTYPE);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex, true);
            logger.debug("Returning with Null");
            return null;
        } finally {
            oem.closeEM(loggedUser);
        }
    }

    // </editor-fold>
    List<String> lookupFldExps = new ArrayList<String>();
    List<String> lookupCtrlsExps = new ArrayList<String>();

    @Override
    public void loadAttribute(String expression) {
        logger.debug("Entering");
        if (null == loadedList) {
            logger.debug("Returning as loaded list equals Null");
            return;
        }
        lookupCtrlsExps.add(expression);
        if (expression.contains(".")) {
            lookupFldExps.add(expression.substring(0, expression.indexOf(".")));
        }
        int count = 0;
        for (BaseEntity entity : loadedList) {
            count++;
            List<ExpressionFieldInfo> expressionFldsInfo = null;
            Object currentParentObject = null; // Shouldn't be BaseEntity to be ready for any attribute type
            int expressionFieldsCount = -1;
            int fieldIndex = -1;

            try {
                // Parse field expression in all cases (having "." or not) for code
                // simplicity
                expressionFldsInfo = entity.parseFieldExpression(expression);
                if (expressionFldsInfo == null) {
                    logger.warn("Null Expression Field Info");
                    logger.debug("Returning");
                    return;
                }
                if (expressionFldsInfo.size() < 0) {
                    logger.warn("Empty Expression Field Info List");
                    logger.debug("Returning");
                    return;
                }
                // Check if primitive, no additional processing needed
                if (expressionFldsInfo.get(0).field.getType().isPrimitive()) {
                    logger.debug("Returning");
                    return;
                }

                // Initialize loop variables
                currentParentObject = entity;
                expressionFieldsCount = expressionFldsInfo.size();
                boolean fieldIsList = false;
                Field expField = null;

                // Loop on the expression fields
                for (fieldIndex = 0; fieldIndex < expressionFieldsCount; fieldIndex++) {
                    expField = expressionFldsInfo.get(fieldIndex).field;
                    fieldIsList = BaseEntity.getFieldType(expField).getSimpleName().equals("List");
                    if (fieldIsList) {
                        logger.debug("Returning");
                        return;
                    } else {
                        // Field is not list
                        // No need to load it, as it should be already loaded
                        // $$ OneToX Association Fetch is by default EAGER, take
                        // action if it's LAZY

                        // Set loop variables for next expression field
                        if (fieldIndex == (expressionFieldsCount - 1)) {
                            // Last field in expression
                            // No need for loop variables setting
                            // End the function
                            break;
                        }
                        try {
                            Object fieldValue = ((BaseEntity) currentParentObject).invokeGetter(expField);
                            if (fieldValue != null) {
                                // Field Value Is Not Null
                                // Set the field to be a parent of the next field in
                                // the expression
                                currentParentObject = fieldValue;
                            }
                        } catch (NoSuchMethodException ex) {
                            // No getter for the field, return
                            logger.error("Exception thrown", ex);
                            logger.debug("Returning");
                            return;
                        }
                    }
                }
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
        }
    }

    protected void addMenuItems(HeaderPanel header) {
        // FIXME: is it still used? It was @override for a function in OBackBean called
        // by createHeaderPanel
        MenuItem importMenu = new MenuItem();
        importMenu.setId(getCurrentScreen().getName() + "_ImportData");
        Class[] parms2 = new Class[]{ActionEvent.class};
        ExpressionFactory ef = FacesContext.getCurrentInstance().getApplication().getExpressionFactory();
        MethodExpression mb = ef.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                "#{" + getBeanName() + ".screenMenuAction}", null, parms2);
        MethodExpressionActionListener al = new MethodExpressionActionListener(mb);
        importMenu.addActionListener(al);
        importMenu.setValue(ddService.getDDLabel("fileLabel", loggedUser));
        header.getMainItem().getChildren().add(importMenu);
    }

    public void editRow(RowEditEvent event) {
        logger.debug("Returning");
        OFunctionResult ofr = new OFunctionResult();
        BaseEntity entity = ((BaseEntity) event.getObject());
        int activeRowInTable = getCurrentSelectedRowInDataTable(event);
        int activeRowInPage = getRowIndexInPage(event);
        boolean lookupPickedMode = isLookupPickedMode();
        try {
            List<BaseEntity> savedEntities = new ArrayList<BaseEntity>();
            entity.setChanged(true);
            OEntityActionDTO parentPreSave = entitySetupService.getOEntityDTOActionDTO(getOactOnEntity().getDbid(),
                    OEntityActionDTO.ATDBID_ParentPreSave, loggedUser);
            if (parentPreSave != null && getLoadedList().size() > 0) {
                ofr.append(runParentValidation(parentPreSave));
            }
            if (ofr.getErrors() == null || ofr.getErrors().isEmpty()) {
                ofr = saveRow(entity, savedEntities);
            }
            if (savedEntities.size() == 1) {
                removeTrasfromPickedElement(entity, activeRowInTable);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            FacesContext.getCurrentInstance().validationFailed();
        } finally {
            int currentRow = -1;
            if (ofr.getErrors() != null && !ofr.getErrors().isEmpty()) {
                currentRow = getObjectRecordID(entity.getDbid());
                if (lookupPickedMode) {
                    RequestContext.getCurrentInstance().execute("editRow(" + currentRow + ")");
                } else {
                    RequestContext.getCurrentInstance().execute("setSelectedRow(" + currentRow + ")");
                }
            } else {
                if (lookupPickedMode) {
                    RequestContext.getCurrentInstance().execute("deleteRow(" + activeRowInPage + ")");
                } else {
                    RequestContext.getCurrentInstance().execute("setSelectedRow(" + -10 + ")");
                }
            }
            messagesPanel.clearAndDisplayMessages(ofr);
            RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
            logger.debug("Returning");
        }
    }

    private void removeTrasfromPickedElement(BaseEntity entity, int activeRowInTable) {

        if (lookupPickedElements.size() == 1) {
            clearLookupPickedCache();
        } else if (lookupPickedElements.size() > activeRowInTable) {
            lookupPickedElements = (LinkedList) SessionManager.getSessionAttribute(getPickedElementKey());
            if (lookupPickedElements != null && !lookupPickedElements.isEmpty()) {
                String lookupFieldExp = (String) SessionManager.getSessionAttribute("lookupFieldExp");
                try {

                    lookupPickedElements
                            .remove(entity.invokeGetter(lookupFieldExp.substring(0, lookupFieldExp.indexOf("."))));
                    SessionManager.addSessionAttribute(getPickedElementKey(), lookupPickedElements);
                } catch (NoSuchMethodException ex) {
                    Logger.getLogger(TabularBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        if (lookupTransformedPickedElements != null && lookupTransformedPickedElements.size() > activeRowInTable) {
            lookupTransformedPickedElements.remove(activeRowInTable);
            SessionManager.addSessionAttribute("lookupTransformedPickedElements", lookupTransformedPickedElements);
        }
        // eModifications
        if (lazyModel != null && !((List) lazyModel.getWrappedData()).isEmpty()) {
            ((LazyBaseEntityModel) lazyModel).removeRow(entity);
        }
    }

    public boolean isLookupPickedMode() {
        return lookupTransformedPickedElements != null && !lookupTransformedPickedElements.isEmpty();
    }

    public int getCurrentSelectedRowInDataTable(EventObject event) {
        return ((DataTable) event.getSource()).getRowIndex();
    }

    private int getRowIndexInPage(RowEditEvent event) {
        int activeRowInTable = getCurrentSelectedRowInDataTable(event);
        int screenRecords = ((TabularScreen) currentScreen).getRecordsInPage();
        return Math.abs(screenRecords * currentPage - activeRowInTable);
    }

    public void cancelEditRow(RowEditEvent event) {
        int activeRowInTable = getCurrentSelectedRowInDataTable(event);
        int activeRowInPage = getRowIndexInPage(event);
        BaseEntity entity = ((BaseEntity) event.getObject());
        lookupTransformedPickedElements = (List) SessionManager.getSessionAttribute("lookupTransformedPickedElements");
        boolean lookupPickedMode = isLookupPickedMode();
        if (entity.getDbid() == 0) {
            if (lookupPickedMode) {
                removeTrasfromPickedElement(entity, activeRowInTable);
            } else {
                RequestContext.getCurrentInstance().execute("window.location.reload(false)");
                return;
            }
        }
        entity.setChanged(false);
        if (lookupPickedMode) {
            RequestContext.getCurrentInstance().execute("deleteRow(" + activeRowInPage + ")");
        } else {
            int currentRow = getObjectRecordID(entity.getDbid());
            RequestContext.getCurrentInstance().execute("setSelectedRow(" + currentRow + ")");
            RequestContext.getCurrentInstance().execute("setSelectedRow(" + -10 + ")");
        }
    }

    public DataTable buildTable(String bindingExpression, String tableVariableExpression, String valueChangeMethod) {
        logger.debug("Entering");
        FacesContext fc = FacesContext.getCurrentInstance();
        ExpressionFactory ef = fc.getApplication().getExpressionFactory();
        // <editor-fold defaultstate="collapsed" desc="Define Table and Its Properties">
        DataTable table = new DataTable();
        table.setEditable(true);

        setTableBehavior(table, fc, ef);

        FABSAjaxBehavior rowEdit = new FABSAjaxBehavior();
        MethodExpression rowEditMe = ef.createMethodExpression(fc.getELContext(), "#{" + getBeanName() + ".editRow}",
                null, new Class[]{RowEditEvent.class});
        rowEdit.setListener(rowEditMe);
        rowEdit.setImmediate(false);

        rowEdit.setOnstart("validateMandaroryFields();");
        rowEdit.setOncomplete("KeepEditable()");

        FABSAjaxBehavior rowCancelEdit = new FABSAjaxBehavior();
        MethodExpression rowCancelEditMe = ef.createMethodExpression(fc.getELContext(),
                "#{" + getBeanName() + ".cancelEditRow}", null, new Class[]{RowEditEvent.class});
        rowCancelEdit.setListener(rowCancelEditMe);
        table.addClientBehavior("rowEditCancel", rowCancelEdit);
        table.addClientBehavior("rowEdit", rowEdit);

        if (lookupMode) {
            setTableDoubleClickBehavior(ef, fc, table);
        }

        FABSAjaxBehavior rowClick = new FABSAjaxBehavior();
        MethodExpression rowClickMe = ef.createMethodExpression(fc.getELContext(), "#{" + getBeanName() + ".clkAction}",
                null, new Class[]{SelectEvent.class});
        rowClick.setListener(rowClickMe);
        table.addClientBehavior("rowSelect", rowClick);

        FABSAjaxBehavior onFilterBehavior = new FABSAjaxBehavior();
        MethodExpression onFilter = ef.createMethodExpression(fc.getELContext(), "#{" + getBeanName() + ".onFilter}",
                null, new Class[]{FilterEvent.class});
        onFilterBehavior.setListener(onFilter);
        table.addClientBehavior("filter", onFilterBehavior);

        // </editor-fold>
        createActionColumns(table, ((TabularScreen) currentScreen).isEditable());

        createTableColumns(table, true, tableVariableExpression, valueChangeMethod);
        logger.debug("Returning");
        return table;
    }

    private void createTableColumns(DataTable table, boolean editable, String tableVariableExpression,
            String valueChangeMethod) {
        logger.trace("Entering");
        // <editor-fold defaultstate="collapsed" desc="Iterate Screen Fields and Create
        // Them">
        for (int screenFieldIndex = 0; screenFieldIndex < screenFields.size(); screenFieldIndex++) {
            ScreenField screenField = (ScreenField) screenFields.get(screenFieldIndex);
            Column column = new Column();
            column.setId("column" + screenFieldIndex);

            Field field = screenField.getExpInfos(getOactOnEntity().getEntityClassPath())
                    .get(screenField.getExpInfos(getOactOnEntity().getEntityClassPath()).size() - 1).field;
            if (field.getAnnotation(Transient.class) == null || (field.getAnnotation(ComputedField.class) != null
                    || field.getAnnotation(Translation.class) != null)) {
                String fldExp = UIFieldUtility.buildScreenFieldExp(
                        "#{" + tableVariableExpression + "." + screenField.getFieldExpression() + "}", false);
                ValueExpression valueExpression = exFactory.createValueExpression(elContext, fldExp, String.class);
                column.setValueExpression("sortBy", valueExpression);
                if (screenField.getDd().getControlType().getDbid() != DD.CT_CALENDAR
                        && screenField.getDd().getControlType().getDbid() != DD.CT_CHECKBOX) {
                    column.setValueExpression("filterBy", valueExpression);
                    column.setFilterMatchMode("contains");
                }
            }

            DD headerDD = screenField.getDd();
            try {
                headerDD = ddService.getDD(headerDD.getName(), loggedUser);
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }

            HtmlOutputLabel label = new HtmlOutputLabel();
            label.setId("columnHeader" + screenFieldIndex);

            /**
             * ************************************
             */
            if (fABSSetupLocal.loadKeySetup("EnableInlineHelp", loggedUser).isBvalue()
                    && screenFields.get(screenFieldIndex).getDd().isEnableInlineHelp()) {
                HtmlGraphicImage helpImg = new HtmlGraphicImage();
                helpImg.setId("helpImg" + screenFieldIndex);
                helpImg.setUrl("/resources/help.png");
                helpImg.setStyleClass("hlp-img");
                label.getChildren().add(helpImg);
                Tooltip toolTip = new Tooltip();
                toolTip.setId("toolTip" + screenFieldIndex);
                toolTip.setFor(label.getId());
                String toolTipData
                        = // ddService.getDDLabel("InlineHelpTitle", loggedUser)+ " " +
                        screenField.getDd().getInlineHelpTranslated();
                toolTip.setValue(toolTipData);
                toolTip.setShowEffect("slide");
                // toolTip.setHideEffect("slide");
                toolTip.setStyleClass("tool-tip");
                label.setValueExpression("rendered", exFactory.createValueExpression(elContext,
                        "#{" + getBeanName() + ".toolTipRender}", boolean.class));
                label.getChildren().add(toolTip);

            }
            /**
             * ************************************
             */
            // Check Mandatory Field to add astrix
            if (screenField.isMandatory()) {
                HtmlGraphicImage mandatoryImg = new HtmlGraphicImage();
                mandatoryImg.setId("mandatory_Img" + screenFieldIndex);
                mandatoryImg.setUrl("/resources/mandatory.gif");
                mandatoryImg.setStyleClass("mandatory-img");
                label.getChildren().add(mandatoryImg);
            }
            if (screenField.getDdTitleOverrideTranslated() != null
                    && !"".equals(screenField.getDdTitleOverrideTranslated())) {
                label.setValue(screenField.getDdTitleOverrideTranslated());
                column.setHeader(label);
            } else if (null != screenField.getDdTitleOverride() && !screenField.getDdTitleOverride().equals("")) {
                label.setValue(screenField.getDdTitleOverride());
                column.setHeader(label);
            } else {
                label.setValue(headerDD.getHeaderTranslated());
                column.setHeader(label);
            }
            HtmlPanelGrid grid = null;
            if (screenField.getDd().isBalloon()) {
                Column blnColumn = new Column();
                blnColumn.setId("blncolumn" + screenFieldIndex);
                blnColumn.setHeader(new HtmlOutputLabel());
                grid = new HtmlPanelGrid();
                grid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_grid");
                grid.setColumns(2);
                grid.setStyleClass("bln-grid-panel");

                CommandButton button = new CommandButton();
                button.setId("openButton" + "_" + screenFieldIndex + "_" + CNTRL_INPUTMODE);
                button.setValue("..");
                button.setStyleClass("bln-btn-tbl");
                button.setOnclick("balloonPositionManager(event, this.id);");
                grid.getChildren().add(button);
                OverlayPanel balloonOverLay = new OverlayPanel();
                OEntityDTO balloonEntity = uiFrameWorkFacade.getFieldBallonOEntityDTO(getOactOnEntity(),
                        screenField.getExpInfos(getOactOnEntity().getEntityClassPath()), loggedUser);
                balloonOverLay = createBalloonPanel(balloonEntity, "_" + screenFieldIndex, false, "currentRow",
                        screenField.getExpInfos(getOactOnEntity().getEntityClassPath()));
                balloonOverLay.setFor(button.getId());
                balloonOverLay.setDynamic(true);
                balloonOverLay.setHideEffect("fade");
                balloonOverLay.setShowEffect("explode");
                balloonOverLay.setId("balloonOverLay" + "_" + screenFieldIndex + "_" + CNTRL_INPUTMODE);
                balloonOverLay.setStyleClass("balloonOverLay");
                balloonOverLay.setStyle("display:none; position:absolute;");
                balloonOverLay.setOnHide("displayNone(this.id);");
                balloonOverLay.setOnShow("hideBreifSeCtion(this.id);");

                grid.getChildren().add(balloonOverLay);
                // add the balloons in a genaral panel added to the form middle panel at the end
                // then specify the position of appearance in the portlet according to the place
                // of the button over the datatable
                blnColumn.getChildren().add(grid);
                blnColumn.setStyleClass("bln-column");
                // Don't display the button if the Entity has no DBID, i.e. new rowst
                String ownerFieldBackBeanExpression = null;

                if (screenField.getExpInfos(getOactOnEntity().getEntityClassPath()) != null) {
                    // Balloon for a field
                    int balloonFieldIndex = 0;
                    try {
                        balloonFieldIndex = uiFrameWorkFacade.getBalloonFieldIndex(
                                Class.forName(balloonEntity.getEntityClassPath()),
                                screenField.getExpInfos(getOactOnEntity().getEntityClassPath()));
                    } catch (Exception ex) {
                        logger.error("Exception thrown", ex);
                    }
                    switch (balloonFieldIndex) {
                        case -2:
                            // Error
                            break;
                        case -1:
                            // Owner Entity itself
                            ownerFieldBackBeanExpression = "currentRow";
                        default:
                            // A field in the expressionFieldInfos
                            ownerFieldBackBeanExpression = "currentRow";
                            for (int fieldIndex = 0; fieldIndex <= balloonFieldIndex; fieldIndex++) {
                                ownerFieldBackBeanExpression += "." + screenField
                                        .getExpInfos(getOactOnEntity().getEntityClassPath()).get(fieldIndex).fieldName;
                            }
                    }
                }
                String renderExpression = "#{" + ownerFieldBackBeanExpression + "==null?'false':"
                        + ownerFieldBackBeanExpression + ".dbid != 0}";
                grid.setValueExpression("rendered",
                        exFactory.createValueExpression(elContext, renderExpression, String.class));
                table.getChildren().add(blnColumn);
            }
            if (screenField.getDd().getControlType().getDbid() == DD.CT_LOOKUPSCREEN) {
                lookupCtrlsExps.add(screenField.getFieldExpression());
            }

            UIComponent outputText = createScreenControl(CNTRL_OUTPUTMODE, screenField, tableVariableExpression,
                    screenFieldIndex, valueChangeMethod);

            if (editable) {
                CellEditor cellEditor = new CellEditor();
                cellEditor.setId("cell" + screenFieldIndex);
                UIComponent scrCtrlComp = createScreenControl(CNTRL_INPUTMODE, screenField, tableVariableExpression,
                        screenFieldIndex, valueChangeMethod);
                cellEditor.getFacets().put("input", scrCtrlComp);
                cellEditor.getFacets().put("output", outputText);
                column.getChildren().add(cellEditor);
            } else {
                column.getChildren().add(outputText);
            }
            column.setStyleClass("column-business");
            table.getChildren().add(column);
        }
        // </editor-fold>
    }

    public boolean getEditablePencilDisplayState() {
        if (!((TabularScreen) currentScreen).isEditable()) {
            // return "display: None !important";
            return true;
        } else {
            return false/* "" */;
        }
    }

    private void createActionColumns(DataTable table, boolean editable) {
        // <editor-fold defaultstate="collapsed" desc="Row Editor Column">
        Column editorColumn = new Column();
        HtmlPanelGrid editRowPanel = new HtmlPanelGrid();
        int width = 20;
        editorColumn.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_column_editor");
        editRowPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_editRowPanel");
        editRowPanel.setStyleClass("editor-panel");
        editorColumn.setHeaderText("");
        CommandButton deleteWithoutConfButton = createDeleteWithoutConfButton(editRowPanel);
        editorColumn.getChildren().add(deleteWithoutConfButton);

        editorColumn.setResizable(false);

        if (!currentScreen.getName().equals("ManageNotifications")) {// editable
            width += 20;
            RowEditor rowEditor = new RowEditor();

            rowEditor.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_rowEditor");
            editorColumn.getChildren().add(rowEditor);
        }
        if (((TabularScreen) getCurrentScreen()).isRemovable()) {
            CommandButton deleteButton = createDeleteButton(editRowPanel);
            HtmlOutputLabel outputLabel = new HtmlOutputLabel();
            outputLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "cellEditorLBLOUT");
            CellEditor cellEditor = new CellEditor();
            cellEditor.setId("cellEditor_deleteColumn");
            HtmlOutputLabel inputLabel = new HtmlOutputLabel();
            inputLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "cellEditorLBLIN");
            cellEditor.getFacets().put("output", deleteButton != null ? deleteButton : outputLabel);
            cellEditor.getFacets().put("input", inputLabel);
            editorColumn.getChildren().add(cellEditor);
        }
        if (showActions) {
            CommandButton duplicateButton = createDuplicateButton(editRowPanel);
            HtmlOutputLabel outputLabel = new HtmlOutputLabel();
            outputLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "cellEditorLBLOUTAct");
            if (duplicateButton != null && ((TabularScreen) getCurrentScreen()).isRemovable()) {
                width += 20;
            }
            if (editable) {
                CellEditor cellEditor = new CellEditor();
                cellEditor.setId("cellEditor_duplicateColumn");
                HtmlOutputLabel inputLabel = new HtmlOutputLabel();
                inputLabel
                        .setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "cellEditorLBLINAct");
                cellEditor.getFacets().put("output", duplicateButton != null ? duplicateButton : outputLabel);
                cellEditor.getFacets().put("input", inputLabel);
                editorColumn.getChildren().add(cellEditor);
            } else {
                editorColumn.getChildren().add(duplicateButton != null ? duplicateButton : outputLabel);
            }
        }
        editorColumn.setWidth(width);
        editorColumn.setStyleClass("editor-column");
        editorColumn.setStyle("padding:0px!important;width:" + width + "px;");
        table.getChildren().add(editorColumn);
        // </editor-fold>
        if (!isScreenOpenedForLookup()) {

            // <editor-fold defaultstate="collapsed" desc="Side Balloon Column">
            if (((TabularScreen) getCurrentScreen()).isSideBalloon()) {
                // column=new UIColumn
                Column bcolumn = new Column();
                bcolumn.setId("bcolumn");
                HtmlOutputLabel headerBallonlbl = new HtmlOutputLabel();
                headerBallonlbl.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
                bcolumn.setHeader(headerBallonlbl);
                bcolumn.setStyleClass("sideBalloonStyle");
                HtmlPanelGrid grid = new HtmlPanelGrid();
                grid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_grid");
                grid.setColumns(2);
                grid.setStyleClass("bln-grid-panel");

                CommandButton button = new CommandButton();
                button.setId("openButton" + "_" + screenFields.size());
                button.setValue("..");
                button.setStyleClass("bln-btn-tbl");
                button.setOnclick("balloonPositionManager(event, this.id)");
                grid.getChildren().add(button);
                OverlayPanel balloonOverLay = new OverlayPanel();
                balloonOverLay = createBalloonPanel(getOactOnEntity(), "_" + screenFields.size(), false, "currentRow",
                        null // for the actOnEntity
                );
                balloonOverLay.setDynamic(true);
                balloonOverLay.setHideEffect("fade");
                balloonOverLay.setFor(button.getId());
                balloonOverLay.setStyleClass("balloonOverLay");
                balloonOverLay.setStyle("display:none;");
                balloonOverLay.setStyle("display:none;position:absolute;");
                balloonOverLay.setOnHide("displayNone(this.id);");
                balloonOverLay.setOnShow("hideBreifSeCtion(this.id);");
                balloonOverLay.setId("balloonOverLay" + "_" + screenFields.size());
                grid.getChildren().add(balloonOverLay);
                bcolumn.getChildren().add(grid);
                table.getChildren().add(bcolumn);
            }

            if (getCurrentScreen() instanceof M2MRelationScreen) {
                CellEditor cellEditor = new CellEditor();
                cellEditor.setId("cell" + "1220");
                Column selCol = new Column();
                selCol.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "sel_col");
                SelectBooleanCheckbox m2mCheckbox = new SelectBooleanCheckbox();
                m2mCheckbox.setId("m2mCheck_CheckBox_0");
                m2mCheckbox.setImmediate(true);
                m2mCheckbox.setValueExpression("value",
                        exFactory.createValueExpression(elContext, "#{currentRow.m2mCheck}", Boolean.class));

                buildScreenExpression(m2mCheckbox);
                FramworkUtilities.setControlFldExp(m2mCheckbox, "m2mCheck");

                SelectBooleanCheckbox m2mCheckboox = new SelectBooleanCheckbox();
                m2mCheckboox.setId("m2mCheck_CheckBox_0o");
                m2mCheckbox.setImmediate(true);

                m2mCheckboox.setValueExpression("value",
                        exFactory.createValueExpression(elContext, "#{currentRow.m2mCheck}", Boolean.class));

                buildScreenExpression(m2mCheckboox);
                FramworkUtilities.setControlFldExp(m2mCheckboox, "m2mCheck");
                FABSAjaxBehavior behavior = new FABSAjaxBehavior();
                m2mCheckbox.addClientBehavior("change", behavior);
                Class[] parms = new Class[]{ValueChangeEvent.class};
                MethodBinding methodBinding = getFacesContext().getApplication()
                        .createMethodBinding("#{" + getBeanName() + ".updateRow}", parms);
                m2mCheckbox.setValueChangeListener(methodBinding);

                FABSAjaxBehavior behavioro = new FABSAjaxBehavior();
                m2mCheckboox.addClientBehavior("change", behavioro);
                Class[] parmso = new Class[]{ValueChangeEvent.class};
                MethodBinding methodBindingo = getFacesContext().getApplication()
                        .createMethodBinding("#{" + getBeanName() + ".updateRow}", parmso);
                m2mCheckboox.setValueChangeListener(methodBindingo);

                cellEditor.getFacets().put("output", m2mCheckboox);
                cellEditor.getFacets().put("input", m2mCheckbox);

                selCol.setStyleClass("column-business");
                selCol.getChildren().add(cellEditor);
                table.getChildren().add(selCol);
            }
            // //</editor-fold>
        }
    }

    public DataTable buildNonEditableTable(String bindingExpression, String tableVariableExpression,
            String valueChangeMethod) {
        FacesContext fc = FacesContext.getCurrentInstance();
        ExpressionFactory ef = fc.getApplication().getExpressionFactory();
        DataTable table = new DataTable();
        table.setEditable(false);
        setTableBehavior(table, fc, ef);

        createActionColumns(table, false);

        createTableColumns(table, false, tableVariableExpression, valueChangeMethod);
        return table;
    }

    private void setTableBehavior(DataTable table, FacesContext fc, ExpressionFactory ef) {
        logger.trace("Entering");
        setTablePaginatorBehavior(table, ef, fc);

        table.setValueExpression("value", exFactory.createValueExpression(elContext,
                "#{" + getBeanName() + ".lazyModel}", LazyBaseEntityModel.class));
        setTableSelectionBehavior(table, ef, fc);

        setTableUnselectionBehavior(ef, fc, table);

        if (lookupMode) {
            setTableDoubleClickBehavior(ef, fc, table);
        }
        logger.trace("Returning");
    }

    private void setTableDoubleClickBehavior(ExpressionFactory ef, FacesContext fc, DataTable table) {
        FABSAjaxBehavior rowdblClick = new FABSAjaxBehavior();
        MethodExpression rowdblClickMe = ef.createMethodExpression(fc.getELContext(),
                "#{" + getBeanName() + ".dblClkAction}", null, new Class[]{SelectEvent.class});
        rowdblClick.setListener(rowdblClickMe);
        table.addClientBehavior("rowDblselect", rowdblClick);
    }

    private void setTableUnselectionBehavior(ExpressionFactory ef, FacesContext fc, DataTable table) {
        MethodExpression unselectMethod = ef.createMethodExpression(fc.getELContext(),
                "#{" + getBeanName() + ".rowUnSelectAction}", null, new Class[]{UnselectEvent.class});
        FABSAjaxBehavior unselectionBehavior = new FABSAjaxBehavior();
        unselectionBehavior.setListener(unselectMethod);
        unselectionBehavior.setOnsuccess("lookupSelectionListener()");
        table.addClientBehavior("rowUnselect", unselectionBehavior);
        table.setFilterEvent("enter");
    }

    private void setTableSelectionBehavior(DataTable table, ExpressionFactory ef, FacesContext fc) {
        table.setValueExpression("selection", exFactory.createValueExpression(elContext,
                "#{" + getBeanName() + ".selectedEntities}", BaseEntity[].class));
        if (currentScreen.getName().contains("lookup")) {
            table.setValueExpression("rowKey",
                    exFactory.createValueExpression(elContext, "#{currentRow.dbid}", Long.class));
        } else {
            table.setValueExpression("rowKey",
                    exFactory.createValueExpression(elContext, "#{currentRow.instID}", Long.class));
        }
        table.setVar("currentRow");
        table.setResizableColumns(true);
        table.setSelectionMode("multiple");
        table.setStyleClass("dt-table");

        MethodExpression me = ef.createMethodExpression(fc.getELContext(), "#{" + getBeanName() + ".selectionAction}",
                null, new Class[]{SelectEvent.class});
        FABSAjaxBehavior selectionBehavior = new FABSAjaxBehavior();
        selectionBehavior.setListener(me);
        selectionBehavior.setOnsuccess("lookupSelectionListener()");
        table.addClientBehavior("rowSelect", selectionBehavior);
        table.setFilterEvent("enter");
    }

    private void setTablePaginatorBehavior(DataTable table, ExpressionFactory ef, FacesContext fc) {
        table.setId(getCurrentScreen().getName() + "_generalTable");
        table.setLazy(true);
        String noRecordsMsgDD = ddService.getDDLabel("noRecordsMsg", loggedUser);
        table.setEmptyMessage(noRecordsMsgDD);
        FABSAjaxBehavior pageBehavior = new FABSAjaxBehavior();
        MethodExpression pageMe = ef.createMethodExpression(fc.getELContext(), "#{" + getBeanName() + ".onPage}", null,
                new Class[]{PageEvent.class});
        pageBehavior.setListener(pageMe);
        table.addClientBehavior("page", pageBehavior);

        // For Paginator
        table.setPaginatorPosition("bottom");
        table.setPaginator(true);
        table.setPaginatorTemplate("{CurrentPageReport}  {FirstPageLink} {PreviousPageLink} {PageLinks} "
                + "{NextPageLink} {LastPageLink} {JumpToPageDropdown}");
        if (getCurrentScreen() instanceof TabularScreen
                && ((TabularScreen) getCurrentScreen()).getRecordsInPage() != 0) {
            table.setRows(((TabularScreen) getCurrentScreen()).getRecordsInPage());
            lazyModel.setPageSize(((TabularScreen) getCurrentScreen()).getRecordsInPage());
        } else {
            // Current Screen Not Tabular Screen, or RecordsInPage=0
            logger.trace("Current Screen Not Tabular Screen, or RecordsInPage=0");
            // </editor-fold>
            lazyModel.setPageSize(DEFAULT_PAGE_SIZE);
            table.setRows(DEFAULT_PAGE_SIZE); // TODO: get from system setup
        }
    }

    public Sheet buildSheet(String bindingExpression, String tableVariableExpression, String valueChangeMethod) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Define Table and Its Properties">
        Sheet sheet = new Sheet();
        HtmlOutputLabel hlbl = new HtmlOutputLabel();
        hlbl.setValue("List of Cars");
        sheet.setHeader(hlbl);
        sheet.setStyleClass("dt-table");
        sheet.setVar("currentRow");
        sheet.setId(getCurrentScreen().getName() + "_generalTable");
        // For Paginator
        sheet.setPaginatorPosition("bottom");
        sheet.setPaginator(true);
        sheet.setPaginatorTemplate("{CurrentPageReport}  {FirstPageLink} {PreviousPageLink} {PageLinks} "
                + "{NextPageLink} {LastPageLink} {RowsPerPageDropdown}");
        sheet.setValueExpression("value",
                exFactory.createValueExpression(elContext, "#{" + getBeanName() + ".loadedList}", ArrayList.class));

        // <editor-fold defaultstate="collapsed" desc="Iterate Screen Fields and Create
        // Them">
        for (int screenFieldIndex = 0; screenFieldIndex < screenFields.size(); screenFieldIndex++) {
            ScreenField screenField = (ScreenField) screenFields.get(screenFieldIndex);
            Column column = new Column();
            column.setId("column" + screenFieldIndex);

            InputText inputText = new InputText();
            inputText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_sheetFld_"
                    + screenField.getDbid());
            // inputText
            Integer decimalDigits = null;
            if (screenField.getDd().getDecimalMask() != null) {
                decimalDigits = screenField.getDd().getDecimalMask();
                FABSDecimalConverter decimalConverter = new FABSDecimalConverter(decimalDigits);
                inputText.setConverter(decimalConverter);
            }
            FABSAjaxBehavior behavior = new FABSAjaxBehavior();
            inputText.addClientBehavior("blur", behavior);
            Class[] parms = new Class[]{ValueChangeEvent.class};
            MethodBinding methodBinding = getFacesContext().getApplication()
                    .createMethodBinding("#{" + getBeanName() + ".updateRow}", parms);
            inputText.setValueChangeListener(methodBinding);
            FramworkUtilities.setControlFldExp(inputText, screenField.getFieldExpression());
            inputText.setReadonly(!screenField.isEditable());
            inputText.setRequired(screenField.isMandatory());
            ValueExpression valueExpression = exFactory.createValueExpression(elContext,
                    "#{" + tableVariableExpression + "." + screenField.getFieldExpression() + "}", String.class);
            column.setValueExpression("sortBy", valueExpression);

            DD headerDD = screenField.getDd();
            try {
                headerDD = ddService.getDD(headerDD.getName(), loggedUser);
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }

            HtmlOutputLabel label = new HtmlOutputLabel();
            label.setId("columnHeader" + screenFieldIndex);

            if (screenField.getDdTitleOverrideTranslated() != null
                    && !"".equals(screenField.getDdTitleOverrideTranslated())) {
                label.setValue(screenField.getDdTitleOverrideTranslated());
            } else if (null != screenField.getDdTitleOverride() && !screenField.getDdTitleOverride().equals("")) {
                label.setValue(screenField.getDdTitleOverride());
            } else {
                label.setValue(headerDD.getHeaderTranslated());
            }
            column.setHeaderText(label.getValue().toString());
            inputText.setValueExpression("value", valueExpression);
            column.setWidth(screenField.getDd().getControlSize());

            column.getChildren().add(inputText);
            sheet.getChildren().add(column);
        }
        // </editor-fold>
        logger.debug("Returning");
        return sheet;
    }

    // <editor-fold defaultstate="collapsed" desc="Multi-Level Lookup Screen
    // Actions">
    public void toggleSubGroupAction(ActionEvent event) {
        CommandButton lvlBtn = (CommandButton) event.getComponent();
        int index = FramworkUtilities.getObjectIndexInTabularScreen(lvlBtn, getContext());
        if (lvlBtn.getImage().equals("/resources/tree_line_blank.gif")) {
            return;
        }
        if (lvlBtn.getImage().equals("/resources/tree_nav_top_open_no_siblings.gif")) {
            mainElements.get(index).setImage("/resources/tree_nav_top_close_no_siblings.gif");
            addSiblings(index);
        } else {
            mainElements.get(index).setImage("/resources/tree_nav_top_open_no_siblings.gif");
            removeSiblings(index);
        }
    }

    private void addSiblings(int index) {
        SelfRelationTreeElement mainElement = mainElements.get(index);
        List<BaseEntity> childrenElements = mainElement.getChildrenElemnt();
        List<BaseEntity> grandChildern;

        for (BaseEntity child : childrenElements) {
            SelfRelationTreeElement element = new SelfRelationTreeElement();
            try {
                grandChildern = (List<BaseEntity>) oem.executeEntityListQuery("SELECT o" + " FROM "
                        + lkbClass.getSimpleName() + " o WHERE o." + parentFLD.getName() + ".dbid = " + child.getDbid(),
                        getLoggedUser());
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
                grandChildern = null;

            }

            if (grandChildern != null && !grandChildern.isEmpty()) {
                element.setHasLevels(true);
                element.setChildrenElemnt(grandChildern);
                element.setImage("/resources/tree_nav_top_open_no_siblings.gif");
            } else {
                element.setHasLevels(false);
                element.setImage("/resources/tree_line_blank.gif");
            }
            element.setRelationElement(child);
            element.setParentElement(mainElement.getRelationElement());
            mainElements.add(++index, element);
        }

    }

    private void removeSiblings(int index) {
        SelfRelationTreeElement mainElement = mainElements.get(index);
        List<BaseEntity> mainchildren = mainElement.getChildrenElemnt();
        for (BaseEntity childEntity : mainchildren) {
            for (int i = 0; i < mainElements.size(); i++) {
                SelfRelationTreeElement element = mainElements.get(i);
                if (element.getParentElement() != null
                        && element.getParentElement().equals(mainElement.getRelationElement())
                        && element.getRelationElement().equals(childEntity)) {
                    mainElements.remove(element);
                    continue;
                }
            }

        }
    }

    // </editor-fold>
    private String filtTxtStyle;

    public String getFiltTxtStyle() {
        int height = TableColumnHeader.chkRequiredHeight;
        if (loadedList != null && !loadedList.isEmpty()) {
            height = 30 - height;
        } else {
            height = 45 - height;
        }

        return "position:relative; top:-" + height + "px;width:100%";
    }

    public void setFiltTxtStyle(String filtTxtStyle) {
        this.filtTxtStyle = filtTxtStyle;
    }

    private String filtChkStyle;

    public String getFiltChkStyle() {
        int height = TableColumnHeader.chkRequiredHeight;
        if (loadedList != null && !loadedList.isEmpty()) {
            height = 30 - height;
        } else {
            height = 45 - height;
        }
        return "position:relative; top:-" + height + ";";
    }

    public void setFiltChkStyle(String filtChkStyle) {
        this.filtChkStyle = filtChkStyle;
    }

    // <editor-fold defaultstate="collapsed" desc="showPaginator">
    private boolean showPaginator = false;

    public boolean isShowPaginator() {
        if (loadedList == null || loadedList.size() <= lazyModel.getPageSize()) {
            return false;
        }
        return true;
    }

    public void setShowPaginator(boolean showPaginator) {
        this.showPaginator = showPaginator;
    }
    // </editor-fold>

    @Override
    public void screenTranslationMenuAction(ActionEvent actionEvent) {
        try {
            oem.getEM(loggedUser);
            ODataType oDataType = dataTypeService.loadODataType("DBID", systemUser);
            if (oDataType != null) {
                ODataMessage message = new ODataMessage();
                message.setODataType(oDataType);
                List data = new ArrayList();
                data.add(getOactOnEntity().getDbid());
                data.add(getDataFilter());
                data.add(getDataSort());
                message.setData(data);
                String openedScreenInstId = generateScreenInstId("ScreenTranslationSetup");
                SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.MESSAGE, message);
                openScreen("ScreenTranslationSetup", openedScreenInstId);
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            oem.closeEM(loggedUser);
        }
    }

    public void validateNumeric(FacesContext context, UIComponent validate, Object value) {
        boolean isNumeric = (value == null) ? false : FramworkUtilities.isNumber(value.toString());
        if (!isNumeric) {
            ((InputText) validate).setValid(false);
            UserMessage userMessage = usrMsgService.getUserMessage("NumericValidation", loggedUser);
            HtmlMessage message = new HtmlMessage();
            message.setTitle(userMessage.getMessageTitleTranslated());
            message.setStyleClass("inlineErrorMsgTitle");
            message.setFor(validate.getClientId(context));

            FacesMessage msg = new FacesMessage(userMessage.getMessageTextTranslated());
            dataTable.processUpdates(context);
            context.addMessage(validate.getClientId(context), msg);
        }
    }

    /**
     * Support VoidDT as an input in case of lookup or in case actOnEntity has
     * no parent. Support DBIDList. <br>
     * Calls {@link OBackBean#getValidScreenInputs()} and includes its returns
     *
     * @return
     */
    @Override
    protected List<ScreenInputDTO> getValidScreenInputs() {
        List<ScreenInputDTO> screenInputs = null;
        try {
            screenInputs = super.getValidScreenInputs();

            // If it's lookup, then support VoidDT Input
            if (isScreenOpenedForLookup()) {
                // <editor-fold defaultstate="collapsed" desc="Add VoidDT as a natively
                // supported input">
                ScreenInputDTO voidInput = uiFrameWorkFacade.getVoidAsScreenInput(screenInputs, loggedUser);
                if (voidInput != null) // Not found in screenInputs and got successfully
                // Support it
                {
                    screenInputs.add(voidInput);
                }
                // </editor-fold>
            } else {
                // Not opened for lookup
                if (getOactOnEntity().getEntityParentClassPath() == null) {
                    // No parent found for actOnEntity
                    // <editor-fold defaultstate="collapsed" desc="Add VoidDT if actOnEntity has no
                    // parent">
                    // Add VoidDT as a natively supported input
                    ScreenInputDTO voidInput = uiFrameWorkFacade.getVoidAsScreenInput(screenInputs, loggedUser);
                    if (voidInput != null) // Not found in screenInputs and got successfully
                    // Support it
                    {
                        screenInputs.add(voidInput);
                    }
                    // </editor-fold>
                }
            }

            // <editor-fold defaultstate="collapsed" desc="Natively Support dbidList">
            ScreenInputDTO dbidListInput = uiFrameWorkFacade.getDBIDListAsScreenInput(screenInputs, loggedUser);
            if (dbidListInput != null) // Not found in screenInputs and got successfully
            // Support it
            {
                screenInputs.add(dbidListInput);
            }
            // </editor-fold>

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);

        } finally {
            return screenInputs;
        }
    }

    /**
     *
     * @param entity
     * @param fieldExpression
     * @param value
     */
    @Override
    protected OFunctionResult entityValueIntializationListener(BaseEntity entity, String fieldExpression,
            Object value) {
        OFunctionResult oFR = super.entityValueIntializationListener(entity, fieldExpression, value);
        if (!oFR.getErrors().isEmpty()) {
            return oFR;
        }
        // FIXME: if there is an error, append to oFR
        applyDepencyInitialization(entity, value, fieldExpression);
        return oFR;
    }

    public UIComponent addGearBoxToPortletOptions() {
        logger.debug("Entering");
        Class[] parms2 = new Class[]{ActionEvent.class};
        ExpressionFactory ef = FacesContext.getCurrentInstance().getApplication().getExpressionFactory();
        MethodExpression mb = ef.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                "#{" + getBeanName() + ".screenMenuAction}", null, parms2);
        MethodExpressionActionListener al = new MethodExpressionActionListener(mb);
        // <editor-fold defaultstate="collapsed" desc="ImportDataMenuItem">
        MenuItem importDataMenuItem = new MenuItem();
        importDataMenuItem.setId(getFacesContext().getViewRoot().createUniqueId() + "ImportDataMenuItem");
        importDataMenuItem.addActionListener(al);
        importDataMenuItem.setValue("ImportDataMenuItem");
        importDataMenuItem.setAjax(true);
        importDataMenuItem.setPartialSubmit(true);
        importDataMenuItem.setImmediate(true);
        String importDataGear = ddService.getDDLabel("importDataGear", loggedUser);
        importDataMenuItem.setValue(importDataGear);

        MenuButton button = (MenuButton) new GearOptionsHandler(loggedUser).addGearToPortletOptions(this);

        // customize option button options according to user roles
        if (securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_button, loggedUser)) {
            button.getChildren().add(importDataMenuItem);
            addFilterOptionsToGear(button);
        } else {
            if (securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_importdata, loggedUser)) {
                button.getChildren().add(importDataMenuItem);
            }
            if (securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_showoptions, loggedUser)) {
                addFilterOptionsToGear(button);
            }
        }
        logger.debug("Returning");
        return button;
    }

    public void addFilterOptionsToGear(UIComponent button) {
        logger.debug("Entering");
        Class[] parms2 = new Class[]{ActionEvent.class};
        ExpressionFactory ef = FacesContext.getCurrentInstance().getApplication().getExpressionFactory();
        MethodExpression mb = ef.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                "#{" + getBeanName() + ".filterAction}", null, parms2);
        MethodExpressionActionListener al = new MethodExpressionActionListener(mb);
        // <editor-fold defaultstate="collapsed" desc="Only Active">
        MenuItem onlyActive = new MenuItem();
        onlyActive.setId(getFacesContext().getViewRoot().createUniqueId() + "onlyActive");
        onlyActive.addActionListener(al);
        onlyActive.setAjax(true);
        onlyActive.setPartialSubmit(true);
        onlyActive.setImmediate(true);
        // Active
        String onlyActiveGear = ddService.getDDLabel("onlyActiveGear", loggedUser);
        onlyActive.setValue(onlyActiveGear);
        button.getChildren().add(onlyActive);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Active & InActive">
        MenuItem ActiveInActive = new MenuItem();
        ActiveInActive.setId(getFacesContext().getViewRoot().createUniqueId() + "ActiveInActive");
        ActiveInActive.addActionListener(al);
        ActiveInActive.setAjax(true);
        ActiveInActive.setPartialSubmit(true);
        ActiveInActive.setImmediate(true);
        String activeInActiveGear = ddService.getDDLabel("activeInActiveGear", loggedUser);
        ActiveInActive.setValue(activeInActiveGear);
        button.getChildren().add(ActiveInActive);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Only InActive">
        MenuItem onlyInActive = new MenuItem();
        onlyInActive.setId(getFacesContext().getViewRoot().createUniqueId() + "onlyInActive");
        onlyInActive.addActionListener(al);
        onlyInActive.setAjax(true);
        onlyInActive.setPartialSubmit(true);
        onlyInActive.setImmediate(true);
        String onlyInActiveGear = ddService.getDDLabel("onlyInActiveGear", loggedUser);
        onlyInActive.setValue(onlyInActiveGear);
        button.getChildren().add(onlyInActive);
        logger.debug("Returning");
        // </editor-fold>
    }

    public void filterAction(ActionEvent actionEvent) {
        logger.debug("Entering");
        if (actionEvent.getComponent().getId().contains("ActiveInActive")) {
            TabularScreenLoadAll();
        } else if (actionEvent.getComponent().getId().contains("onlyInActive")) {
            TabularScreenLoadInactive();
        } else {
            TabularScreenLoadActive();
        }
        RequestContext.getCurrentInstance().update("tf:generalPanel");
        logger.debug("Returning");
    }

    @Override
    protected OFunctionResult preRunScreenFunction(OFunction ofunction, BaseEntity entity,
            ODataMessage passedDataMessage) {
        logger.trace("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            if (c2AMode && ofunction instanceof ScreenFunction) {
                if (toBeRenderScreens == null) {
                    toBeRenderScreens = new ArrayList();
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser));

        } finally {
            logger.trace("Returning");
            return oFR;
        }
    }

    @Override
    protected List<BaseEntity> getLoadedEntity() {
        return loadedList;
    }

    @Override
    protected List<BaseEntity> getAllEntities() {
        logger.trace("Entering");
        List<BaseEntity> allEntities = new ArrayList<BaseEntity>();
        for (int i = 0; i < ((LazyBaseEntityModel) getLazyModel()).getNewEntities().size(); i++) {
            BaseEntity entity = ((LazyBaseEntityModel) getLazyModel()).getNewEntities().get(i);
            if (!entity.isInActive()) {
                allEntities.add(entity);
            }
        }
        dataFilter.clear();
        List tempFilter = getInputFilter();
        if (tempFilter != null) {
            dataFilter.addAll(tempFilter);
        }
        tempFilter = getScreenFilter();
        if (tempFilter != null) {
            dataFilter.addAll(tempFilter);
        }
        dataFilter.add(OEntityManager.CONFCOND_GET_ACTIVEONLY);
        if (getCurrentScreen().getScreenFilter() != null && !getCurrentScreen().getScreenFilter().isInActive()) {
            try {
                dataFilter.addAll(filterServiceBean.getFilterConditions(getCurrentScreen().getScreenFilter(),
                        getUserSessionInfo(), loggedUser));
            } catch (FilterDDRequiredException ex) {
                logger.error("Exception thrown", ex);
            }
        }
        dataFilter.add(OEntityManager.CONFCOND_CASE_INSENSITIVE);

        try {
            for (int i = 0; i < getLoadedList().size(); i++) {
                BaseEntity entity = (BaseEntity) getLoadedList().get(i);
                if (entity.getDbid() != 0 && (!entity.isInActive() || (entity.isInActive() && entity.isChanged()))) {
                    allEntities.add(entity);
                }
            }
            String dbids = "";
            for (int i = 0; i < allEntities.size(); i++) {
                if (i != 0) {
                    dbids += ",";
                }
                dbids += ((BaseEntity) allEntities.get(i)).getDbid();
            }
            List<String> cond = new ArrayList<String>(dataFilter);
            if (!dbids.isEmpty()) {
                cond.add("NOT dbid in (" + dbids + ")");
            }
            if (!(getCurrentScreen() instanceof M2MRelationScreen)) {
                if (getCurrentScreen().getDataLoadingUserExit() == null) {
                    allEntities.addAll(oem.loadEntityList(getOactOnEntity().getEntityClassName(), cond,
                            fieldExpressions, dataSort, oem.getSystemUser(loggedUser)));
                }
            } else {
                allEntities.clear();
                allEntities.addAll(loadedList);
            }

        } catch (Exception ex) {
            logger.warn("Coulcn't load entites");
            logger.error("Exception thrown", ex);
        }
        logger.trace("Returning");
        return allEntities;
    }

    protected List<BaseEntity> getAllEntitiesWithCurrentConds() {
        logger.trace("Entering");
        List<BaseEntity> allEntities = new ArrayList<BaseEntity>();
        for (int i = 0; i < ((LazyBaseEntityModel) getLazyModel()).getNewEntities().size(); i++) {
            BaseEntity entity = ((LazyBaseEntityModel) getLazyModel()).getNewEntities().get(i);
            allEntities.add(entity);
        }

        try {
            for (int i = 0; i < getLoadedList().size(); i++) {
                BaseEntity entity = (BaseEntity) getLoadedList().get(i);
                if (entity.getDbid() != 0 && !entity.isInActive()) {
                    allEntities.add(entity);
                }
            }
            String dbids = "";
            for (int i = 0; i < allEntities.size(); i++) {
                if (i != 0) {
                    dbids += ",";
                }
                dbids += ((BaseEntity) allEntities.get(i)).getDbid();
            }
            List<String> cond = new ArrayList<String>(dataFilter);
            if (!dbids.isEmpty()) {
                cond.add("NOT dbid in (" + dbids + ")");
            }
            if (!(getCurrentScreen() instanceof M2MRelationScreen)) {
                allEntities.addAll(oem.loadEntityList(getOactOnEntity().getEntityClassName(), cond, fieldExpressions,
                        dataSort, oem.getSystemUser(loggedUser)));
            } else {
                allEntities.clear();
                allEntities.addAll(loadedList);
            }

        } catch (Exception ex) {
            logger.warn("Couldn't load all entities");
            logger.error("Exception thrown", ex);
        }
        logger.trace("Returning");
        return allEntities;
    }

    // <editor-fold defaultstate="collapsed" desc="Filter Loaded List By Activty
    // State Menu">
    protected boolean createTabularScreenMenuBar(HtmlPanelGrid buttonPanel, CommandButton menuBar, DD mainMenuItemTitle,
            String iconPath, MenuItem... customItems) {
        logger.trace("Entering");
        Menu mainMenu = new Menu();
        mainMenu.setId(getFacesContext().getViewRoot().createUniqueId() + "_MenuAction");
        mainMenu.setOverlay(true);
        mainMenu.setStyleClass("more_action_main_menu");
        mainMenu.setTrigger(menuBar.getId());
        Submenu mainMenuItem = new Submenu();
        mainMenuItem.setLabel("");
        mainMenuItem.setId(getFacesContext().getViewRoot().createUniqueId() + "_MenuActionMainItem");
        mainMenuItem.setStyleClass("more_action_main_menu_item");
        mainMenu.getChildren().add(mainMenuItem);
        buttonPanel.getChildren().add(mainMenu);
        boolean containItems = false;

        for (int i = 0; i < customItems.length; i++) {
            if (customItems[i] != null) {
                containItems = true;
                customItems[i].setStyleClass("more_action_menu_item");
                mainMenuItem.getChildren().add(customItems[i]);
            }
        }

        List<ScreenAction> screenActions = getCurrentScreen().getScreenActions();

        for (int i = 0; i < screenActions.size(); i++) {
            if (screenActions.get(i).isInActive()) {
                continue;
            }

            if (screenActions.get(i).isButton()) {
                continue;
            }

            OEntityAction action = screenActions.get(i).getAction();
            MenuItem item = createMenuItem(action.getNameTranslated(), null, "customScreenActionsAction", null,
                    action.getDbid());
            if (item != null) {
                containItems = true;
                mainMenuItem.getChildren().add(item);
            }
        }
        logger.trace("Returning");
        return containItems;
    }

    protected void createTabularScreenMenuBar(HtmlPanelGrid buttonPanel) {
        CommandButton activeMenuButton = new CommandButton();
        activeMenuButton.setIcon("active_btn_icon");
        activeMenuButton.setStyleClass("active_btn");
        CommandButton delAndDupMenuButton = new CommandButton();
        delAndDupMenuButton.setValue(ddService.getDDLabel("TabularscreenMoreActions", loggedUser));
        delAndDupMenuButton.setIcon("action_btn_icon");
        delAndDupMenuButton.setStyleClass("action_btn");
        activeMenuButton.setId(getFacesContext().getViewRoot().createUniqueId() + "_ActionsMenuBar");
        delAndDupMenuButton.setId(getFacesContext().getViewRoot().createUniqueId() + "_DelAndDupMenuButton");

        MenuItem delItem = createDeleteMenu();
        if (null != delItem) {
            delItem.setUpdate("tf:generalPanel");
        }
        MenuItem dupItem = createDuplicateMenu();
        if (null != dupItem) {
            dupItem.setUpdate("tf:generalPanel");
        }
        boolean isMenuItems = false;
        isMenuItems = createTabularScreenMenuBar(buttonPanel, delAndDupMenuButton, null/*
                 * ddService.getDD(
                 * "TabularscreenMoreActions",
                 * loggedUser)
                 */, // It is never used
                "actionicon", delItem, dupItem, createActivityMenuItem("TabularScreenLoadAll", null),
                createActivityMenuItem("TabularScreenLoadActive", null),
                createActivityMenuItem("TabularScreenLoadInactive", null));
        if (isMenuItems) {
            buttonPanel.getChildren().add(delAndDupMenuButton);
        }
    }

    private MenuItem createActivityMenuItem(String value, DD dd) {
        MenuItem item = createMenuItem(ddService.getDD(value, loggedUser).getLabel(), value, null, null, null);

        item.setUpdate("tf:generalPanel");
        return item;
    }

    /**
     * @return the loadAll
     */
    public String TabularScreenLoadAll() {
        loadWithActivityFilter(OEntityManager.CONFCOND_GET_ACTIVEANDINACTIVE);
        return "";
    }

    /**
     * @return the loadAll
     */
    public String TabularScreenLoadActive() {
        loadWithActivityFilter(OEntityManager.CONFCOND_GET_ACTIVEONLY);
        return "";
    }

    /**
     * @return the loadAll
     */
    public String TabularScreenLoadInactive() {
        loadWithActivityFilter(OEntityManager.CONFCOND_GET_INACTIVEONLY);
        return "";
    }

    void loadWithActivityFilter(String activityFilterValue) {
        activityStateFilter = new ArrayList<String>();
        activityStateFilter.add(activityFilterValue);
        updateDataFilter();
        loadedList = loadList(getOactOnEntity().getEntityClassName(), dataFilter, getDataSort());
    }

    // </editor-fold>
    private ArrayList<String> activityStateFilter;

    public ArrayList<String> getActivityStateFilter() {
        return activityStateFilter;
    }

    public void setActivityStateFilter(ArrayList<String> activityStateFilter) {
        this.activityStateFilter = activityStateFilter;
    }

    List<FilterField> filterfields = null;

    public String runAction() {
        try {
            List<FilterField> filterFields = updateFilterFields(filterfields);
            if (currentScreen.getScreenFilter() != null) {
                currentScreen.getScreenFilter().setFilterFields(filterFields);
            }
            updateDataFilter();

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
        }
        return "";
    }

    public String saveAction() {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        if (filterfields != null) {
            for (int idx = 0; idx < filterfields.size(); idx++) {
                oFR.append(entitySetupService.callEntityUpdateAction(filterfields.get(idx), loggedUser));
            }

            messagesPanel.clearMessages();
            messagesPanel.addMessages(oFR);
        }

        if (getScreenHeight() == null || getScreenHeight().equals("")) {
            RequestContext.getCurrentInstance()
                    .execute("top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');");
        } else {
            RequestContext.getCurrentInstance().execute("top.FABS.Portlet.updateIframeSizeWithHeight({iframeName:'"
                    + getPortletInsId() + "Frame'" + ",screenHeight:'" + getScreenHeight() + "'});");
        }
        editAllClicked = false;
        logger.debug("Returning with \" \"");
        return "";
    }

    public String runSaveAction() {
        saveAction();
        runAction();
        return "";
    }

    protected List<FilterField> updateFilterFields(List<FilterField> filterFields) {
        logger.trace("Entering");
        for (int idx = 0; idx < filterFields.size(); idx++) {
            if (filterFields.get(idx).isInActive()) {
                continue;
            }

            if (!filterFields.get(idx).isFromTo()) {
                if (valuesMap.get(filterFields.get(idx).getFieldExpression()) != null) {
                    if (valuesMap.get(filterFields.get(idx).getFieldExpression()) instanceof Date) {
                        Date date = (Date) valuesMap.get(filterFields.get(idx).getFieldExpression());
                        filterFields.get(idx).setFieldValue(FilterField.dateFormat.format(date));
                    } else if (valuesMap.get(filterFields.get(idx).getFieldExpression()) instanceof BaseEntity) {
                        if (((BaseEntity) valuesMap.get(filterFields.get(idx).getFieldExpression())).getDbid() != 0) {
                            filterFields.get(idx)
                                    .setFieldValue(String.valueOf(
                                            ((BaseEntity) valuesMap.get(filterFields.get(idx).getFieldExpression()))
                                                    .getDbid()));
                        } else {
                            filterFields.get(idx).setFieldValue(null);
                        }
                    } else {
                        filterFields.get(idx)
                                .setFieldValue(valuesMap.get(filterFields.get(idx).getFieldExpression()).toString());
                    }
                }
            } else {
                if (valuesMap.get(filterFields.get(idx).getFieldExpression() + FROM_DELIM) != null
                        && !valuesMap.get(filterFields.get(idx).getFieldExpression() + FROM_DELIM).equals("")
                        && valuesMap.get(filterFields.get(idx).getFieldExpression() + TO_DELIM) != null
                        && !valuesMap.get(filterFields.get(idx).getFieldExpression() + TO_DELIM).equals("")) {
                    if (valuesMap.get(filterFields.get(idx).getFieldExpression() + FROM_DELIM) instanceof Date
                            && valuesMap.get(filterFields.get(idx).getFieldExpression() + TO_DELIM) instanceof Date) {
                        Date from = (Date) valuesMap.get(filterFields.get(idx).getFieldExpression() + FROM_DELIM);
                        Date to = (Date) valuesMap.get(filterFields.get(idx).getFieldExpression() + TO_DELIM);

                        filterFields.get(idx).setFieldValue(FilterField.dateFormat.format(from));
                        filterFields.get(idx).setFieldValueTo(FilterField.dateFormat.format(to));

                    } else if (valuesMap
                            .get(filterFields.get(idx).getFieldExpression() + FROM_DELIM) instanceof BaseEntity
                            && valuesMap
                                    .get(filterFields.get(idx).getFieldExpression() + TO_DELIM) instanceof BaseEntity) {
                        if (((BaseEntity) valuesMap.get(filterFields.get(idx).getFieldExpression() + FROM_DELIM))
                                .getDbid() != 0) {
                            filterFields.get(idx).setFieldValue(String.valueOf(((BaseEntity) valuesMap
                                    .get(filterFields.get(idx).getFieldExpression() + FROM_DELIM)).getDbid()));
                        } else {
                            filterFields.get(idx).setFieldValue(null);
                        }
                        if (((BaseEntity) valuesMap.get(filterFields.get(idx).getFieldExpression() + TO_DELIM))
                                .getDbid() != 0) {
                            filterFields.get(idx).setFieldValueTo(String.valueOf(
                                    ((BaseEntity) valuesMap.get(filterFields.get(idx).getFieldExpression() + TO_DELIM))
                                            .getDbid()));
                        } else {
                            filterFields.get(idx).setFieldValueTo(null);
                        }

                    } else {
                        String filterFieldVal = valuesMap.get(filterFields.get(idx).getFieldExpression() + FROM_DELIM)
                                .toString();
                        String filterFieldValTo = valuesMap.get(filterFields.get(idx).getFieldExpression() + TO_DELIM)
                                .toString();
                        filterFields.get(idx).setFieldValue(filterFieldVal);
                        filterFields.get(idx).setFieldValueTo(filterFieldValTo);
                    }
                } else {
                    filterFields.get(idx).setFieldValue("");
                }
            }
        }
        logger.trace("Returning");
        return filterFields;
    }

    public File fileEntrtyReturned() {
        File file = new File((String) SessionManager.getSessionAttribute("ImportDataFilePath"));

        if (file != null) {
            returnImportedData(file);
        }
        RequestContext.getCurrentInstance().update("tf:generalPanel");
        return file;

    }

    private StreamedContent file;

    public StreamedContent getFile() {
        return file;
    }

    public StreamedContent createExcelTemplate() {
        try {

            HSSFWorkbook excelWorkbook = new HSSFWorkbook();
            HSSFSheet excelSheet = null;
            HSSFRow sheetRow = null;
            HSSFCell rowCell = null;

            excelSheet = excelWorkbook.createSheet(currentScreen.getName());// name of sheet.
            sheetRow = excelSheet.createRow(0);
            sheetRow.setHeight((short) 400);
            for (int i = 0; i < screenFields.size(); i++) {
                String fieldExp = screenFields.get(i).getFieldExpression();
                List<ExpressionFieldInfo> infos = BaseEntity.parseFieldExpression(actOnEntityCls, fieldExp);
                ExpressionFieldInfo efi = infos.get(infos.size() - 1);
                Field field = efi.field;
                // Field field = BaseEntity.getClassField(actOnEntityCls,fieldExp);
                if (field.getType().getSimpleName().equalsIgnoreCase("date")) {
                    CellStyle cellStyle = excelWorkbook.createCellStyle();
                    CreationHelper createHelper = excelWorkbook.getCreationHelper();
                    short dateFormat = createHelper.createDataFormat().getFormat("yyyy-MM-dd");
                    cellStyle.setDataFormat(dateFormat);
                } else if (field.getType().getSimpleName().equalsIgnoreCase("string")) {
                    DataFormat fmt = excelWorkbook.createDataFormat();
                    CellStyle textStyle = excelWorkbook.createCellStyle();
                    textStyle.setDataFormat(fmt.getFormat("@"));
                    excelSheet.setDefaultColumnStyle(i, textStyle);
                }
                excelSheet.setColumnWidth(i, 8000);
            }

            for (int cellIndex = 0; cellIndex < screenFields.size(); cellIndex++) {
                rowCell = sheetRow.createCell(cellIndex);
                rowCell.setCellType(HSSFCell.CELL_TYPE_STRING);
                rowCell.setCellValue(new HSSFRichTextString(screenFields.get(cellIndex).getFieldExpression()));

                HSSFCellStyle cellStyle = excelWorkbook.createCellStyle();
                cellStyle.setFillBackgroundColor(IndexedColors.BLACK.getIndex());
                cellStyle.setAlignment(cellStyle.ALIGN_CENTER);
                cellStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
                rowCell.setCellStyle(cellStyle);
            }

            FileOutputStream fileOut = new FileOutputStream("ExcelGenerator.xls");// name of file.
            excelWorkbook.write(fileOut);
            fileOut.close();

            File initialFile = new File("ExcelGenerator.xls");
            InputStream targetStream = new FileInputStream(initialFile);
            file = new DefaultStreamedContent(targetStream, "text/csv", "ExcelGenerator.xls");
            initialFile.delete();

        } catch (IOException ex) {
            logger.error("Exception thrown: ", ex);
        }
        return file;
    }

    // <editor-fold desc="Import Excel Sheet" defaultstate="collapsed">
    /**
     *
     * @param file Excel sheet 2003
     * @see parse the file and upload it to act on table
     */
    public void returnImportedData(final File file) {
        try {
            UserMessage message = new UploadExcelService(this, loggedUser).uploadSheet(file);
            messagesPanel.clearAndDisplayMessages(message);
            RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
            if (getScreenHeight() == null || getScreenHeight().equals("")) {
                RequestContext.getCurrentInstance()
                        .execute("top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');");
            } else {
                RequestContext.getCurrentInstance().execute("top.FABS.Portlet.updateIframeSizeWithHeight({iframeName:'"
                        + getPortletInsId() + "Frame'" + ",screenHeight:'" + getScreenHeight() + "'});");
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }

    }

    // </editor-fold>
    private List<String> mapValueAccordingToType(String value, Field field) {
        ValueMapperFactory valueMapperFactory = new ValueMapperFactory(field, ddService);
        return valueMapperFactory.getMapper().mapValue(value);

    }

    public String makeQueryFromListOfMappedValues(String beforeTheQuery, List<String> mappedValues,
            String afterTheQuery, String separator) {
        StringBuilder result = new StringBuilder();
        for (String mappedValue : mappedValues) {
            result = result.append(separator).append(beforeTheQuery).append(mappedValue).append(afterTheQuery);
        }

        return result.substring(separator.length());

    }

    public String makeQueryFromListOfMappedValuesByOr(String beforeTheQuery, List<String> mappedValues,
            String afterTheQuery) {
        return makeQueryFromListOfMappedValues(beforeTheQuery, mappedValues, afterTheQuery,
                OEntityManagerRemote.CONDITIONAL_SHADOWED_OR);
    }

    public void updateFilterForTransient(Map<String, String> filters) {
        logger.debug("Entering");
        logger.debug("Entering");
        messagesPanel.clearMessages();

        // <editor-fold defaultstate="collapsed" desc="check need sort">
        String sortField = null, order = "asc";
        boolean sortDone = (dataSort == null || dataSort.isEmpty());
        if (!sortDone) {
            int index = dataSort.get(dataSort.size() - 1).lastIndexOf(" ") + 1;
            if (index == 0) {
                index = dataSort.get(dataSort.size() - 1).length();
            }
            order = dataSort.get(dataSort.size() - 1).substring(index);
            sortField = dataSort.get(dataSort.size() - 1).substring(0, index);
            dataSort.remove(dataSort.size() - 1);
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Filters">
        getFilterMap().clear();
        if (gridFilter != null) {
            gridFilter.clear();
        }
        for (Map.Entry<String, String> entry : filters.entrySet()) {
            String fieldFilterKey = entry.getKey();
            String value = entry.getValue();
            String fldExpression = fieldFilterKey;
            List<ExpressionFieldInfo> expressionFieldInfos = BaseEntity.parseFieldExpression(actOnEntityCls,
                    fldExpression);

            // <editor-fold defaultstate="collapsed" desc="Update Expression If More Than
            // Transient Field Found In The Expression">
            String newFieldExpression = "";
            for (int i = 0; i < expressionFieldInfos.size() - 1; i++) {
                Field field = expressionFieldInfos.get(i).field;
                newFieldExpression += (i == 0 ? "" : ".") + field.getName();
                if (field.getAnnotation(Transient.class) != null) {
                    if (field.getAnnotation(Translation.class) != null) {
                        if (!FramworkUtilities.isFieldNumber(newFieldExpression, actOnEntityCls)) {
                            String originalField = ((Translation) field.getAnnotation(Translation.class))
                                    .originalField();
                            newFieldExpression = newFieldExpression.replace((i == 0 ? "" : ".") + field.getName(),
                                    (i == 0 ? "" : ".") + originalField);
                        }
                    } else if (field.getAnnotation(ComputedField.class) != null) {
                        String originalField = ((ComputedField) field.getAnnotation(ComputedField.class))
                                .filterLHSExpression()[0];
                        newFieldExpression = newFieldExpression.replace((i == 0 ? "" : ".") + field.getName(),
                                (i == 0 ? "" : ".") + originalField);
                    }
                }
            }
            newFieldExpression += (newFieldExpression.isEmpty() ? "" : ".")
                    + expressionFieldInfos.get(expressionFieldInfos.size() - 1).field.getName();
            fldExpression = newFieldExpression;
            // </editor-fold>

            Field field = expressionFieldInfos.get(expressionFieldInfos.size() - 1).field;
            // field.getClass();
            if (value == null || "".equals(value)) {

                // Value is blank
                // Remove this RESET-value expression from the filter map any way,
                // So it's not included in future filtering any more
                getFilterMap().remove(fieldFilterKey);
                continue;
            }

            // Value is not blank
            String operator = value.substring(0, 1);
            String operand = null;
            if (value.length() > 1) {
                operand = value.substring(1);
            } else {
                operand = value; // FIXME: what is this case?
            }
            value = value.toLowerCase();
            // value = arabicTextwhiteCard(value, loggedUser);
            List<String> valuesMapped = mapValueAccordingToType(value, field);

            List<String> arabicCorrectedWords = new ArrayList<String>();
            for (String valueMapped : valuesMapped) {
                arabicCorrectedWords.add(arabicTextwhiteCard(valueMapped, loggedUser));
            }
            valuesMapped = arabicCorrectedWords;
            ArrayList<String> valuesMappedLowerCase = new ArrayList<String>();
            for (String valueMapped : valuesMapped) {
                valuesMappedLowerCase.add(valueMapped.toLowerCase());
            }

            // Check if the field to be filtered is a computed field.
            // isFieldNumber(screenFields.get(currentComponent).getFieldExpression());
            if (field.getAnnotation(Translation.class) != null) {
                if (!FramworkUtilities.isFieldNumber(fldExpression, actOnEntityCls)) {
                    // Field is not number
                    String originalField = ((Translation) field.getAnnotation(Translation.class)).originalField();
                    String fieldName = ((Translation) field.getAnnotation(Translation.class)).originalField();
                    if (fldExpression.contains(".")) {
                        fieldName = fldExpression.substring(0, fldExpression.lastIndexOf(".") + 1) + originalField;
                    }

                    if (loggedUser.getFirstLanguage().getDbid() != ENGLISH_VAR) {

                        // <editor-fold defaultstate="collapsed" desc="Handle Arabic Language">
                        List<String> transConditions = new ArrayList<String>();
                        // value = arabicTextwhiteCard(value, loggedUser);
                        // <editor-fold defaultstate="collapsed" desc="This code fragment is only for
                        // handling difference between SQLServer and othe DB Servers serching with
                        // Regex" >
                        Connection connection = null;
                        try {
                            DataSource dataSource = ((DataSource) new InitialContext()
                                    .lookup("jdbc/" + loggedUser.getTenant().getPersistenceUnitName()));
                            connection = dataSource.getConnection();

                            if (connection.getMetaData().getDatabaseProductName().toLowerCase()
                                    .contains("sql server")) {
                                // for exam
                                List<String> correctedBracketsValues = new ArrayList<String>();
                                for (String valueMapped : valuesMapped) {
                                    correctedBracketsValues.add(valueMapped.replace("(", "[").replace(")", "]"));
                                }

                                String tempQuery = makeQueryFromListOfMappedValuesByOr(originalField + " LIKE '%",
                                        correctedBracketsValues, "%'");
                                ;

                                transConditions.add(tempQuery);
                            } else {
                                transConditions.add(makeQueryFromListOfMappedValuesByOr(originalField + " REGEXP '",
                                        valuesMapped, "'"));
                            }

                        } catch (Exception ex) {
                            logger.error("Exception thrown", ex);
                        } finally {

                            try {
                                connection.close();
                            } catch (Exception ex) {
                                logger.info(ex.getMessage());
                            }
                        }
                        // </editor-fold>
                        transConditions.add("language.dbid in (" + loggedUser.getFirstLanguage().getDbid() + ","
                                + ENGLISH_VAR + ")");

                        List<Long> translations = null;
                        List<String> sorts = new ArrayList<String>();
                        if (fieldFilterKey.equals(sortField) && !sortDone) {
                            sorts.add(originalField + " " + order);
                        }
                        try {
                            translations = textTranslationServiceRemote
                                    .getFilterEntitiesDBIDs(
                                            (expressionFieldInfos.size() == 1 ? actOnEntityCls.getSimpleName()
                                            : expressionFieldInfos
                                                    .get(expressionFieldInfos.size() - 1).fieldClass
                                                    .getSimpleName())
                                            + "Translation",
                                            transConditions, sorts, loggedUser,
                                            expressionFieldInfos.get(expressionFieldInfos.size() - 1).fieldClass
                                                    .getName());
                        } catch (Exception ex) {
                            logger.error("Exception thrown", ex);
                        }
                        String textTransQuery = "";
                        if (null != translations) {
                            for (long textTranslationDBID : translations) {
                                textTransQuery += textTranslationDBID + ",";
                            }
                            if (!textTransQuery.isEmpty()) {
                                textTransQuery = textTransQuery.substring(0, textTransQuery.length() - 1).trim();
                            }
                        }
                        // </editor-fold>

                        if (textTransQuery.isEmpty()) {

                            // <editor-fold defaultstate="collapsed" desc="Translanted & Computed">
                            expressionFieldInfos = BaseEntity.parseFieldExpression(actOnEntityCls, fieldName);
                            field = expressionFieldInfos.get(expressionFieldInfos.size() - 1).field;
                            if (field.getAnnotation(ComputedField.class) != null) {
                                // Field Is Computed
                                String[] filterLHSExpression;
                                ComputedField compFldAnnot = field.getAnnotation(ComputedField.class);
                                if (compFldAnnot == null) {
                                    logger.warn("Transient Field Should Have the ComputedField Annotation");
                                    messagesPanel.addErrorMessage(
                                            usrMsgService.getUserMessage("SystemInternalError", systemUser));
                                    continue;
                                }
                                filterLHSExpression = compFldAnnot.filterLHSExpression();
                                String fldExp1 = ""; // fldExpression
                                if (fldExpression.contains(".")) {
                                    fldExp1 += fldExpression.substring(0, fldExpression.lastIndexOf("."));
                                }
                                String tempFldExpression = "";
                                for (String cfField : filterLHSExpression) {
                                    tempFldExpression += "," + fldExp1 + "." + cfField + ", ' '";
                                }
                                tempFldExpression = tempFldExpression.substring(1, tempFldExpression.length() - 5);
                                if (tempFldExpression.contains(",")) {
                                    tempFldExpression = "CONCAT(" + tempFldExpression + ")";
                                }
                                getFilterMap().put(fieldFilterKey, makeQueryFromListOfMappedValuesByOr(
                                        tempFldExpression + " LIKE '%", valuesMapped, "%'"));
                                if (fieldFilterKey.equals(sortField) && !sortDone) {
                                    String newSort = "lower(" + tempFldExpression + ")";
                                    dataSort.add(newSort + " " + order);
                                    sortDone = true;
                                }
                                continue;
                            }
                            // </editor-fold>
                            getFilterMap().put(fieldName, makeQueryFromListOfMappedValuesByOr(fieldName + " LIKE '%",
                                    valuesMappedLowerCase, "%'"));
                            if (fieldFilterKey.equals(sortField) && !sortDone) {
                                sortDone = true;
                                dataSort.add(fieldName + " " + order);
                            }
                        } else {
                            String expression = fieldName.substring(0, fieldName.lastIndexOf(".") + 1);
                            getFilterMap().put(fieldFilterKey, expression + "dbid IN (" + textTransQuery + ")");
                            if (fieldFilterKey.equals(sortField) && !sortDone) {
                                sortDone = true;
                                int index = (order.trim().equalsIgnoreCase("desc") ? 1 + translations.size() : 1);
                                String sortExpression = "CASE " + expression + "dbid ";
                                if (null != translations) {
                                    for (long textTranslationDBID : translations) {
                                        sortExpression += " WHEN " + textTranslationDBID + " THEN " + index++;
                                    }
                                }
                                sortExpression += " ELSE " + (sorts.get(0).contains("asc") ? 0 : index) + " END ";
                                dataSort.add(sortExpression);
                            }
                        }
                        continue;
                    } else {
                        // Non-Arabic Case
                        // default
                        fldExpression = fieldName;
                        // <editor-fold defaultstate="collapsed" desc="Translanted & Computed">
                        if (field.getAnnotation(ComputedField.class) != null) {
                            // Field Is Computed
                            String[] filterLHSExpression;
                            ComputedField compFldAnnot = field.getAnnotation(ComputedField.class);
                            if (compFldAnnot == null) {
                                logger.warn("Transient Field Should Have the ComputedField Annotation");
                                messagesPanel.addErrorMessage(
                                        usrMsgService.getUserMessage("SystemInternalError", systemUser));
                                continue;
                            }
                            filterLHSExpression = compFldAnnot.filterLHSExpression();
                            String fldExp1 = "";
                            if (fldExpression.contains(".")) {
                                fldExp1 += fldExpression.substring(0, fldExpression.lastIndexOf("."));
                            }
                            String tempFldExpression = "";
                            for (String cfField : filterLHSExpression) {
                                tempFldExpression += "," + fldExp1 + "." + cfField + ", ' '";
                            }
                            tempFldExpression = tempFldExpression.substring(1, tempFldExpression.length() - 5);
                            if (tempFldExpression.contains(",")) {
                                tempFldExpression = "CONCAT(" + tempFldExpression + ")";
                            }
                            getFilterMap().put(fieldFilterKey, makeQueryFromListOfMappedValuesByOr(
                                    tempFldExpression + " LIKE '%", valuesMapped, "%'"));
                            if (fieldFilterKey.equals(sortField) && !sortDone) {
                                String newSort = "lower(" + tempFldExpression + ")";
                                dataSort.add(newSort + " " + order);
                                sortDone = true;
                            }
                            continue;
                        }
                        // </editor-fold>
                        if (fieldFilterKey.equals(sortField) && !sortDone) {
                            dataSort.add("lower(" + fldExpression + ") " + order);
                            sortDone = true;
                        }
                    }
                }
            }
            if (field.getAnnotation(ComputedField.class) != null) {
                // Field is number
                // Field Is Computed
                String[] filterLHSExpression;
                ComputedField compFldAnnot = field.getAnnotation(ComputedField.class);
                if (compFldAnnot == null) {
                    logger.warn("Transient Field Should Have the ComputedField Annotation");
                    messagesPanel.addErrorMessage(usrMsgService.getUserMessage("SystemInternalError", systemUser));
                    continue;
                }
                filterLHSExpression = compFldAnnot.filterLHSExpression();
                String fldExp1 = ""; // fldExpression
                if (fldExpression.contains(".")) {
                    fldExp1 += fldExpression.substring(0, fldExpression.lastIndexOf("."));
                }
                String tempFldExpression = "";
                for (String cfField : filterLHSExpression) {
                    tempFldExpression += "," + fldExp1 + "." + cfField + ", ' '";
                }
                tempFldExpression = tempFldExpression.substring(1, tempFldExpression.length() - 5);
                if (tempFldExpression.contains(",")) {
                    tempFldExpression = "CONCAT(" + tempFldExpression + ")";
                }
                getFilterMap().put(fieldFilterKey,
                        makeQueryFromListOfMappedValuesByOr(tempFldExpression + " LIKE '%", valuesMapped, "%'"));
                if (fieldFilterKey.equals(sortField) && !sortDone) {
                    String newSort = "lower(" + tempFldExpression + ")";
                    dataSort.add(newSort + " " + order);
                    sortDone = true;
                }
                continue;
            }
            fieldFilterKey = fldExpression;
            if (operand != null && !"".equals(operand) // operand is valid
                    && FramworkUtilities.isFieldNumber(fldExpression, actOnEntityCls)) {
                if (operator.equals("=") || operator.equals(">") || operator.equals("<")) {
                    getFilterMap().put(fieldFilterKey,
                            makeQueryFromListOfMappedValuesByOr(fldExpression + operator + "'%", valuesMapped, "%'"));
                } else {
                    getFilterMap().put(fieldFilterKey,
                            makeQueryFromListOfMappedValuesByOr(fldExpression + " LIKE '%", valuesMapped, "%'"));
                }
            } else {
                boolean containsCrypticArabWords = false;
                for (String valueMapped : valuesMapped) {
                    if (valueMapped.contains("(أ|ا|ء|ؤ|ئ|إ|آ)") || valueMapped.contains("(ى|ي)")
                            || valueMapped.contains("(ه|ة)")) {
                        containsCrypticArabWords = true;
                    }
                }
                if (containsCrypticArabWords) {
                    ArrayList<String> valuesMappedWithOutBrackets = new ArrayList<String>();
                    for (String valueMapped : valuesMapped) {
                        valuesMappedWithOutBrackets.add(valueMapped.replace("(", "[").replace(")", "]"));
                    }

                    Connection connection = null;
                    try {
                        DataSource dataSource = ((DataSource) new InitialContext()
                                .lookup("jdbc/" + loggedUser.getTenant().getPersistenceUnitName()));
                        connection = dataSource.getConnection();
                        if (connection.getMetaData().getDatabaseProductName().toLowerCase().contains("sql server")) {
                            String tempValue = value.replace("(", "[");
                            tempValue = tempValue.replace(")", "]");
                            getFilterMap().put(fieldFilterKey, makeQueryFromListOfMappedValuesByOr(
                                    fldExpression + " LIKE '%", valuesMappedWithOutBrackets, "%'"));
                        } else {
                            getFilterMap().put(fieldFilterKey, makeQueryFromListOfMappedValuesByOr(
                                    fldExpression + " REGEXP '", valuesMappedWithOutBrackets, "'"));
                        }
                    } catch (Exception ex) {
                        logger.error("Exception thrown", ex);
                    } finally {

                        try {
                            connection.close();
                        } catch (Exception ex) {
                            logger.info(ex.getMessage());
                        }
                    }
                } else {
                    getFilterMap().put(fieldFilterKey,
                            makeQueryFromListOfMappedValuesByOr(fldExpression + " LIKE '%", valuesMapped, "%'"));
                }
            }
        }
        if (gridFilter == null) {
            gridFilter = new ArrayList<String>();
        }
        gridFilter.addAll(getFilterMap().values());
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="sort">
        if (!sortDone) {
            if (sortField != null && !sortField.isEmpty()) {
                String fldExpression = sortField;
                if (fldExpression.contains("lower(")) {
                    fldExpression = fldExpression.trim().substring(6, fldExpression.lastIndexOf(")"));
                }
                List<ExpressionFieldInfo> expressionFieldInfos = BaseEntity.parseFieldExpression(actOnEntityCls,
                        fldExpression.trim());

                if (expressionFieldInfos != null && !expressionFieldInfos.isEmpty()) {

                    // <editor-fold defaultstate="collapsed" desc="Update Expression If More Than
                    // Transient Field Found In The Expression">
                    String newFieldExpression = "";
                    for (int i = 0; i < expressionFieldInfos.size() - 1; i++) {
                        Field field = expressionFieldInfos.get(i).field;
                        newFieldExpression += (i == 0 ? "" : ".") + field.getName();
                        if (field.getAnnotation(Transient.class) != null) {
                            if (field.getAnnotation(Translation.class) != null) {
                                if (!FramworkUtilities.isFieldNumber(newFieldExpression, actOnEntityCls)) {
                                    String originalField = ((Translation) field.getAnnotation(Translation.class))
                                            .originalField();
                                    newFieldExpression = newFieldExpression.replace(
                                            (i == 0 ? "" : ".") + field.getName(), (i == 0 ? "" : ".") + originalField);
                                }
                            } else if (field.getAnnotation(ComputedField.class) != null) {
                                String originalField = ((ComputedField) field.getAnnotation(ComputedField.class))
                                        .filterLHSExpression()[0];
                                newFieldExpression = newFieldExpression.replace((i == 0 ? "" : ".") + field.getName(),
                                        (i == 0 ? "" : ".") + originalField);
                            }
                        }
                    }
                    newFieldExpression += (newFieldExpression.isEmpty() ? "" : ".")
                            + expressionFieldInfos.get(expressionFieldInfos.size() - 1).field.getName();
                    fldExpression = newFieldExpression;
                    // </editor-fold>

                    Field field = expressionFieldInfos.get(expressionFieldInfos.size() - 1).field;

                    // <editor-fold defaultstate="collapsed" desc="transient">
                    if (field.getAnnotation(Translation.class) != null) {
                        if (!FramworkUtilities.isFieldNumber(fldExpression, actOnEntityCls)) {
                            // Field is not number
                            String originalField = ((Translation) field.getAnnotation(Translation.class))
                                    .originalField();
                            String fieldName = ((Translation) field.getAnnotation(Translation.class)).originalField();
                            if (fldExpression.contains(".")) {
                                fieldName = fldExpression.substring(0, fldExpression.lastIndexOf(".") + 1)
                                        + originalField;
                            }

                            if (loggedUser.getFirstLanguage().getDbid() != ENGLISH_VAR) {
                                // <editor-fold defaultstate="collapsed" desc="Handle Arabic Language">
                                List<String> transConditions = new ArrayList<String>();
                                transConditions.add("language.dbid in (" + loggedUser.getFirstLanguage().getDbid() + ","
                                        + ENGLISH_VAR + ")");

                                List<Long> translations = null;
                                List<String> sorts = new ArrayList<String>();
                                sorts.add(originalField + " " + order);
                                try {
                                    // nothing called (List<BaseEntityTranslation>) oem.loadEntityList()
                                    translations = textTranslationServiceRemote.getFilterEntitiesDBIDs(
                                            (expressionFieldInfos.size() == 1 ? actOnEntityCls.getSimpleName()
                                            : expressionFieldInfos
                                                    .get(expressionFieldInfos.size() - 1).fieldClass
                                                    .getSimpleName())
                                            + "Translation",
                                            transConditions, sorts, loggedUser, null);
                                } catch (Exception ex) {
                                    logger.error("Exception thrown", ex);
                                }
                                // </editor-fold>
                                if (translations.isEmpty()) {
                                    // <editor-fold defaultstate="collapsed" desc="Translanted & Computed">
                                    if (field.getAnnotation(ComputedField.class) != null) {
                                        // Field Is Computed
                                        String[] filterLHSExpression;
                                        ComputedField compFldAnnot = field.getAnnotation(ComputedField.class);
                                        if (compFldAnnot == null) {
                                            logger.warn("Transient Field Should Have the ComputedField Annotation");
                                            messagesPanel.addErrorMessage(
                                                    usrMsgService.getUserMessage("SystemInternalError", systemUser));
                                            logger.debug("Returning");
                                            return;
                                        }
                                        filterLHSExpression = compFldAnnot.filterLHSExpression();
                                        String fldExp1 = ""; // fldExpression
                                        if (fldExpression.contains(".")) {
                                            fldExp1 += fldExpression.substring(0, fldExpression.lastIndexOf("."));
                                        }
                                        String tempFldExpression = "";
                                        for (String cfField : filterLHSExpression) {
                                            tempFldExpression += "," + fldExp1 + "." + cfField + ", ' '";
                                        }
                                        tempFldExpression = tempFldExpression.substring(1,
                                                tempFldExpression.length() - 5);
                                        if (tempFldExpression.contains(",")) {
                                            tempFldExpression = "CONCAT(" + tempFldExpression + ")";
                                        }
                                        String newSort = "lower(" + tempFldExpression + ")";
                                        dataSort.add(newSort + " " + order);
                                        sortDone = true;
                                        logger.debug("Returning");
                                        return;
                                    }
                                    // </editor-fold>
                                    sortDone = true;
                                    dataSort.add(fieldName + " " + order);
                                } else {
                                    String expression = fieldName.substring(0, fieldName.lastIndexOf(".") + 1);
                                    if (!sortDone) {
                                        sortDone = true;
                                        int index = 1;
                                        String sortExpression = "CASE " + expression + "dbid ";
                                        if (null != translations) {
                                            for (Long textTranslation : translations) {
                                                sortExpression += " WHEN " + textTranslation + " THEN " + index++;
                                            }
                                        }
                                        sortExpression += " ELSE " + (order.equalsIgnoreCase("asc") ? index : index)
                                                + " END ";
                                        dataSort.add(sortExpression);
                                    }
                                }
                            } else {
                                // Non-Arabic Case
                                fldExpression = fieldName;
                                // <editor-fold defaultstate="collapsed" desc="Translanted & Computed">
                                if (field.getAnnotation(ComputedField.class) != null) {
                                    // Field Is Computed
                                    String[] filterLHSExpression;
                                    ComputedField compFldAnnot = field.getAnnotation(ComputedField.class);
                                    if (compFldAnnot == null) {
                                        logger.warn("Transient Field Should Have the ComputedField Annotation");
                                        messagesPanel.addErrorMessage(
                                                usrMsgService.getUserMessage("SystemInternalError", systemUser));
                                        logger.debug("Returning");
                                        return;
                                    }
                                    filterLHSExpression = compFldAnnot.filterLHSExpression();
                                    String fldExp1 = ""; // fldExpression
                                    if (fldExpression.contains(".")) {
                                        fldExp1 += fldExpression.substring(0, fldExpression.lastIndexOf("."));
                                    }
                                    String tempFldExpression = "";
                                    for (String cfField : filterLHSExpression) {
                                        tempFldExpression += "," + fldExp1 + "." + cfField + ", ' '";
                                    }
                                    tempFldExpression = tempFldExpression.substring(1, tempFldExpression.length() - 5);
                                    if (tempFldExpression.contains(",")) {
                                        tempFldExpression = "CONCAT(" + tempFldExpression + ")";
                                    }
                                    String newSort = "lower(" + tempFldExpression + ")";
                                    dataSort.add(newSort + " " + order);
                                    sortDone = true;
                                }
                                // </editor-fold>
                                if (!sortDone) {
                                    dataSort.add("lower(" + fldExpression + ") " + order);
                                    sortDone = true;
                                }
                            }
                        }
                        logger.debug("Returning");
                        return;
                    }
                    if (field.getAnnotation(ComputedField.class) != null) {
                        // Field Is Computed
                        String[] filterLHSExpression;
                        ComputedField compFldAnnot = field.getAnnotation(ComputedField.class);
                        filterLHSExpression = compFldAnnot.filterLHSExpression();
                        String fldExp1 = ""; // fldExpression
                        if (fldExpression.contains(".")) {
                            fldExp1 += fldExpression.substring(0, fldExpression.lastIndexOf("."));
                        }
                        String tempFldExpression = "";
                        for (String cfField : filterLHSExpression) {
                            tempFldExpression += "," + fldExp1 + "." + cfField + ", ' '";
                        }
                        tempFldExpression = tempFldExpression.substring(1, tempFldExpression.length() - 5);
                        if (tempFldExpression.contains(",")) {
                            tempFldExpression = "CONCAT(" + tempFldExpression + ")";
                        }
                        if (!sortDone) {
                            String newSort = "lower(" + tempFldExpression + ")";
                            dataSort.add(newSort + " " + order);
                        }
                    } else {
                        dataSort.add(fldExpression + " " + order);
                    }
                    // </editor-fold>

                }
            }
        }
        if (dataSort != null && dataSort.isEmpty()) {
            dataSort.add("dbid desc");
        }
        // </editor-fold>
    }

    protected int firstIndex = 0;

    public void setFirst(int first) {
        firstIndex = first;
    }

    public int getFirst() {
        return firstIndex;
    }

    // <editor-fold defaultstate="collapsed" desc="Save Row">
    public OFunctionResult saveRow(BaseEntity entity, List<BaseEntity> savedTempList) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        // <editor-fold defaultstate="collapsed" desc="Log">
        Long timeBegin = (Calendar.getInstance()).getTimeInMillis();
        boolean liferayAddedEnabled = false;
        // </editor-fold>
        try {
            boolean validationSuccess = validateMandatoryFields();
            if (!validationSuccess) {
                logger.debug("Returning with Null");
                return null;
            }

            if (this.currentScreen.getName().equals("ManageCustomReportDynamicFilters")) {
                CustomReport customReport = (CustomReport) SessionManager
                        .getSessionAttribute("EntityFiltersForCustomReport");
                if (entity instanceof CustomReportDynamicFilter) {
                    ((CustomReportDynamicFilter) entity).setCustomReport(customReport);
                }
            }

            if (this.currentScreen.getName().equals("ManageCustomReportDynamicFilterFields")) {
                CustomReportDynamicFilter dynamicFilter = (CustomReportDynamicFilter) SessionManager
                        .getSessionAttribute("CustomReportDynamicFilterENTITY");
                if (dynamicFilter != null) {
                    ((CustRepFilterFld) entity).setCustomReportDynamicFilter(dynamicFilter);
                }
            }
            ODataType entityDataType = getActOnEntityDataType();
            if (entityDataType == null) {
                oFR.addError(usrMsgService.getUserMessage("EntityDataTypeNotFound",
                        Collections.singletonList(getOactOnEntity().getTitle()), systemUser));
                logger.debug("Returning with Null");
                return null;
            }

            OEntityActionDTO addAction = entitySetupService.getOEntityDTOActionDTO(getOactOnEntity().getDbid(),
                    OEntityActionDTO.ATDBID_CREATE, loggedUser);
            OEntityActionDTO updateAction = entitySetupService.getOEntityDTOActionDTO(getOactOnEntity().getDbid(),
                    OEntityActionDTO.ATDBID_UPDATE, loggedUser);

            boolean save = true;
            // TODO $$ Performance: send the entitylist to the save/update action, support
            // the data types for this
            {
                OFunctionResult entityOFR = null;
                // to avoid getting errors for entities that already exist in loadedList
                // we should validate only the new entities
                if (entity.getDbid() == 0 || entity.isChanged()) {
                    for (int currentField = 0; currentField < lookupMandatoryFields.size(); currentField++) {
                        // FIXME : Execlude Cascade Persist Object
                        if (getValueFromEntity(entity, lookupMandatoryFields.get(currentField)) == null
                                || getValueFromEntity(entity, lookupMandatoryFields.get(currentField)).toString()
                                        .equals("")) {
                            try {
                                String lookupID = lookupMandatoryFields.get(currentField).replace(".", "_")
                                        + "_Lookup_";

                                String label = lookupMandatoryFields.get(currentField);
                                if (getLookupHashMap().get(lookupID) != null) {
                                    label = ((Lookup) getLookupHashMap().get(lookupID)).getFieldDD()
                                            .getHeaderTranslated();
                                }
                                UserMessage message = messageService.getUserMessage("LookupFieldValidation",
                                        Collections.singletonList(label), loggedUser);
                                message.setMessageText(message.getMessageText() + label);

                                oFR.addError(message);
                            } catch (Exception ex) {
                                logger.warn("Lookup Validation Message Error");
                                logger.error("Exception thrown", ex);
                            }
                            save = false;
                            // break;
                        } else {
                            save = true;
                        }
                    }
                }
                for (int currentField = 0; currentField < lookupFldExps.size(); currentField++) {
                    BaseEntity baseEntity = (BaseEntity) getValueFromEntity(entity, lookupFldExps.get(currentField));
                    Field fld = BaseEntity.getClassField(actOnEntityCls, lookupFldExps.get(currentField));
                    boolean cascade = false;

                    if (fld.getAnnotation(OneToOne.class) != null) {
                        OneToOne oneTOneAnnot = fld.getAnnotation(OneToOne.class);
                        for (int i = 0; i < oneTOneAnnot.cascade().length; i++) {
                            CascadeType type = oneTOneAnnot.cascade()[i];
                            if (type == CascadeType.ALL || type == CascadeType.PERSIST) {
                                cascade = true;
                            }
                        }
                    }

                    FABSEntitySpecs entityFABSSpecs = entity.getClass().getAnnotation(FABSEntitySpecs.class);

                    if (entityFABSSpecs != null && entityFABSSpecs.customCascadeFields() != null) {
                        for (String childFieldName : entityFABSSpecs.customCascadeFields()) {
                            if (lookupFldExps.get(currentField).startsWith(childFieldName)) {
                                cascade = true;
                            }
                        }
                    }

                    if (baseEntity != null && baseEntity.getDbid() == 0 && !cascade) {
                        FramworkUtilities.setValueInEntity(entity, lookupFldExps.get(currentField), null);
                    }
                }

                // Apply Sequence Initialization
                applySequenceInitialization(entity);
                OFunctionParms functionParms = new OFunctionParms();
                functionParms.getParams().put("ScreenName", getCurrentScreen().getName());
                functionParms.getParams().put("ScreenDT", getDataMessage());
                // M2M Save strategy
                if (getCurrentScreen() instanceof M2MRelationScreen) {
                    // This field must be a transient field in the M2M entity
                    String significantField = "m2mCheck";

                    // Not used any more --> String significantField = ((M2MRelationScreen)
                    // getCurrentScreen()).getSignificantFieldExpression();
                    String setupFieldExp = ((M2MRelationScreen) getCurrentScreen()).getSetupFieldExpression();
                    Object value = getValueFromEntity(entity, significantField);

                    if (entity.getDbid() == 0 && save && !(value == null // The siginficant value should not be null or
                            // empty string or boolean flase
                            || value.equals("") || (value instanceof Boolean && value.toString().equals("false")))) {
                        ODataMessage entityDataMessage = (ODataMessage) constructOutput(entityDataType, entity)
                                .getReturnedDataMessage();
                        // liferayAddedEnabled=true;
                        entityOFR = entitySetupService.executeAction(addAction, entityDataMessage, functionParms,
                                getLoggedUser());
                        oFR.append(entityOFR);
                        ODataMessage routingDataMessage = new ODataMessage();
                        routingDataMessage.setODataType(entityDataType);
                        List routingMsgData = new ArrayList();
                        routingMsgData.add(currentScreen);
                        routingMsgData.add(entity);
                        routingMsgData.add(loggedUser);
                        routingDataMessage.setData(routingMsgData);
                        // TODO:Commendted for Testing

                    } else if (entity.getDbid() != 0 && entity.isChanged() && !(value == null // The siginficant value
                            // should not be null or
                            // empty string or boolean
                            // flase
                            || value.equals("") || (value instanceof Boolean && value.toString().equals("false")))) {
                        ODataMessage entityDataMessage = (ODataMessage) constructOutput(entityDataType, entity)
                                .getReturnedDataMessage();
                        // liferayAddedEnabled=false;
                        entityOFR = entitySetupService.executeAction(updateAction, entityDataMessage, functionParms,
                                getLoggedUser());
                        oFR.append(entityOFR);
                    } else if (entity.getDbid() != 0 // To delete the Objects that have the significant field with no
                            // value (-ve value)
                            && (value == null || value.equals("")
                            || (value instanceof Boolean && value.toString().equals("false")))) {

                        // Backup the value of the
                        Object relValue = getValueFromEntity(entity, setupFieldExp);
                        try {
                            oem.deleteEntity(entity, loggedUser); // FIXME: should call related action instead
                            UserMessage successMsg = usrMsgService.getUserMessage("DataRemoved", systemUser);
                            oFR.addSuccess(successMsg);

                        } catch (Exception ex) {
                            logger.error("Exception thrown", ex);
                            UserMessage errorMsg = usrMsgService.getUserMessage("RemoveError", systemUser);
                            oFR.addError(errorMsg);
                        }
                        // Adding new sbstitute Object instead of the deleted one
                        BaseEntity substituteEntity = createNewInstance();
                        substituteEntity.generateInstID();

                        // Restoring the Setup value
                        FramworkUtilities.setValueInEntity(substituteEntity, setupFieldExp, relValue);
                        entityServiceLocal.constructEntityODataMessage(substituteEntity, loggedUser);
                        savedTempList.add(substituteEntity);
                    } else {
                        savedTempList.add(entity);
                    }
                } else { // Regular Save strategy
                    if (entity.getDbid() == 0 && save) {
                        logger.trace("Entity Should Be Saved");
                        // Adding new record
                        if (addAction != null) {
                            ODataMessage entityDataMessage = (ODataMessage) constructOutput(entityDataType, entity)
                                    .getReturnedDataMessage();
                            OScreen screen = (OScreen) SessionManager.getSessionAttribute("ParentScreenFromMenuItem");
                            if (entity instanceof EntityFilterScreenAction && screen != null
                                    && (((EntityFilterScreenAction) entity).getScreen() == null
                                    || ((EntityFilterScreenAction) entity).getScreen().getDbid() != screen
                                    .getDbid())) {// this is for EntityFilterMenuItem in optionButton
                                ((EntityFilterScreenAction) entity).setScreen((SingleEntityScreen) screen);
                            }
                            entityOFR = entitySetupService.executeAction(addAction, entityDataMessage, functionParms,
                                    getLoggedUser());
                            liferayAddedEnabled = true;
                            oFR.append(entityOFR);
                        } else {
                            logger.warn("Act On Entity Has No Add Action");
                            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser));
                        }

                    } else if (entity.isChanged() && save) {
                        if (updateAction != null) {//
                            ODataMessage entityDataMessage = (ODataMessage) constructOutput(entityDataType, entity)
                                    .getReturnedDataMessage();
                            liferayAddedEnabled = false;
                            entityOFR = entitySetupService.executeAction(updateAction, entityDataMessage, functionParms,
                                    getLoggedUser());
                            oFR.append(entityOFR);
                        } else {
                            logger.warn("Act On Entity Has No Update Action");
                            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser));
                        }
                    } else {
                        savedTempList.add(entity);
                    }
                }
                if (entityOFR != null && null != entityOFR.getReturnedDataMessage()
                        && null != entityOFR.getReturnedDataMessage().getData()
                        && !entityOFR.getReturnedDataMessage().getData().isEmpty()) {
                    savedTempList.add((BaseEntity) entityOFR.getReturnedDataMessage().getData().get(0));
                }
            }
            List<String> lkpExps = new ArrayList<String>();
            lkpExps.addAll(lookupCtrlsExps);
            lookupFldExps.clear();
            lookupCtrlsExps.clear();
            for (String string : lkpExps) {
                loadAttribute(string);
            }
            if (oFR.getErrors().size() > 0) {
                exit = false;
            } else {
                exit = true;
            }
            // RDO This code to update all opened screens in the page that act on the same
            // entity
            if (oFR.getErrors() == null || oFR.getErrors().isEmpty()) {
                updateOpenedScreenOfSameActOnEntity();
            }
            if (entity.isPreSavedEntity()) {
                List<AttachmentDTO> attachments = null;
                // get attachments after save entity
                attachments = entityAttachmentService.getPreSave(entity, loggedUser);
                entityAttachmentService.clearAttachmentListBeforeSave(entity, loggedUser);
                if (attachments != null) {

                    if (attachments != null && attachments.size() > 0) {
                        for (AttachmentDTO attachment : attachments) {
                            entityAttachmentService.put(attachment, entity.getClass().getName(), entity.getDbid(),
                                    entity, loggedUser);
                        }
                    }
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
                            .put("entityForAttachment" + getCurrentScreen().getDbid(), null);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
            logger.debug("Returning");
            return oFR;

        } finally {

            if (screenInstId.contains("ViewUsersList") && oFR.getSuccesses() != null && !oFR.getSuccesses().isEmpty()) {
                if (liferayAddedEnabled) {
                    if (oFR.getErrors() == null || oFR.getErrors().isEmpty()) {
                        OPortalUtil.addUser((OUser) entity);
                    }
                }
                OPortalUtil.updateUserPassWord(((OUser) entity).getLoginName(), ((OUser) entity).getPassword(),
                        ((OUser) entity).getPassword());

            }
            // if validation error and entity is new entity push the new entity in the first
            // row
            if (oFR.getErrors() != null && !oFR.getErrors().isEmpty()) {
                if (entity.getDbid() == 0) {
                    ((LazyBaseEntityModel) lazyModel).addRow(entity, getFirst());
                }
            } else {
                if (!(currentScreen instanceof M2MRelationScreen)) {
                    ((LazyBaseEntityModel) lazyModel).addRowAfterSave(entity,
                            ((LazyBaseEntityModel) lazyModel).getEntityIndex(entity.getInstID()));
                }
            }
        }
        logger.debug("Returning");
        return oFR;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="extractScreenFieldsDDs">

    private void extractScreenFieldsDDs(List<String> conditions) {
        TabularScreen screenFieldsDDs = (TabularScreen) dataMessage.getData().get(0);
        String concatenatedDDsNames = "";
        int fieldsSize = 0;
        for (ScreenField field : screenFieldsDDs.getScreenFields()) {
            fieldsSize++;
            if (fieldsSize == screenFieldsDDs.getScreenFields().size()) {
                concatenatedDDsNames += "'" + field.getDd().getName() + "'";
            } else {
                concatenatedDDsNames += "'" + field.getDd().getName() + "'" + ",";
            }
        }
        String condition = "name in(" + concatenatedDDsNames + ")";
        conditions.add(condition);
    }
    // </editor-fold>

    public String reloadDataOnly() {
        logger.debug("Entering");
        setLoadedList(loadList(getOactOnEntity().getEntityClassName(), dataFilter, getDataSort()));
        String call = null;
        if (getScreenHeight() == null || getScreenHeight().equals("")) {
            RequestContext.getCurrentInstance()
                    .execute("top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');");
        } else {
            RequestContext.getCurrentInstance().execute("top.FABS.Portlet.updateIframeSizeWithHeight({iframeName:'"
                    + getPortletInsId() + "Frame'" + ",screenHeight:'" + getScreenHeight() + "'});");
        }
        logger.debug("Returning with Null");
        return null;
    }

    public void fieldNameAction(ActionEvent ae) {
        logger.debug("Entering");
        try {
            oem.getEM(loggedUser);

            FormScreen formScreen = (FormScreen) uiFrameWorkFacade.getBasicDetailScreenByOEntityDTO(getOactOnEntity(),
                    systemUser);
            if (formScreen == null) {
                logger.warn("Error Getting Basic Detail Screen");
                messagesPanel.clearAndDisplayMessages(
                        functionService.constructOFunctionResultFromError("NoScreenFound", systemUser));
                oem.closeEM(loggedUser);
                logger.debug("Returning");
                return;
            }
            // to pass the data message
            UIComponent contolComponent = ae.getComponent();
            String clientID = contolComponent.getClientId(getContext());
            String notJavaID = clientID.substring(0, clientID.lastIndexOf(":" + contolComponent.getId()));
            int currentRow = Integer.parseInt(notJavaID.substring(notJavaID.lastIndexOf(":") + 1))
                    % getLazyModel().getPageSize();

            if (currentRow >= loadedList.size()) {
                logger.warn("Current Row index: {} is greater than the loaded List Size", currentRow);
                logger.debug("Returning");
                return;
            }
            BaseEntity currentRowObject = (BaseEntity) loadedList.get(currentRow);
            ODataMessage message;
            ODataType requiredDataType = (ODataType) oem.loadEntity(ODataType.class.getSimpleName(),
                    Collections.singletonList("dbid = 3"), null, loggedUser);
            message = (ODataMessage) constructOutput(requiredDataType, currentRowObject).getReturnedDataMessage();
            String openedScreenInstId = generateScreenInstId(formScreen.getName());
            SessionManager.setScreenSessionAttribute(openedScreenInstId, MODE_VAR, String.valueOf(OScreen.SM_EDIT));
            SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.C2A_VAR, false);
            SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.MESSAGE, message);

            // Open the new portlet or screen
            openScreenInPopup(formScreen, openedScreenInstId, POPUP_SCREENTYPE);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex, true);
            logger.debug("Returning");
            return;
        } finally {
            oem.closeEM(loggedUser);
        }
    }

    public boolean validateMandatoryFields() {
        logger.debug("Entering");
        try {
            List<BaseEntity> newEntities = ((LazyBaseEntityModel) lazyModel).getNewEntities();
            OFunctionResult oFR = new OFunctionResult();
            if (newEntities != null && !(newEntities.isEmpty())) {
                for (BaseEntity entity : newEntities) {
                    for (ScreenField scrField : screenFields) {
                        if (scrField.isMandatory()) {
                            Object valueInEntity = BaseEntity.getValueFromEntity(entity, scrField.getFieldExpression());
                            if (valueInEntity == null) {
                                UserMessage errorMsg = new UserMessage();
                                errorMsg.setMessageText(scrField.getFieldExpression() + " is mandatory field");
                                oFR.addError(usrMsgService.getUserMessage("EmptyMandatoryField",
                                        Collections.singletonList(scrField.getFieldExpression()), systemUser));
                                messagesPanel.clearAndDisplayMessages(oFR);
                                logger.debug("Returning with False");
                                return false;
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
        }
        logger.debug("Returning with True");
        return true;

    }

    /**
     * @return the selectedRow
     */
    public String getSelectedRow() {
        return selectedRow;
    }

    /**
     * @param selectedRow the selectedRow to set
     */
    public void setSelectedRow(String selectedRow) {
        this.selectedRow = selectedRow;
    }

    public String customedDynamicBalloonStyle;

    /**
     * @return the customedDynamicBalloonStyle
     */
    public String getCustomedDynamicBalloonStyle() {
        return customedDynamicBalloonStyle;
    }

    /**
     * @param customedDynamicBalloonStyle the customedDynamicBalloonStyle to set
     */
    public void setCustomedDynamicBalloonStyle(String customedDynamicBalloonStyle) {
        this.customedDynamicBalloonStyle = customedDynamicBalloonStyle;
    }

    public String arabicTextwhiteCard(String arabicText, OUser loggedUser) {
        logger.debug("Entering");
        String replacedArabicText = arabicText.replaceAll("[أؤءئآإا]", "(أ|ا|ء|ؤ|ئ|إ|آ)");
        replacedArabicText = replacedArabicText.replaceAll("[ىي]", "(ى|ي)");
        replacedArabicText = replacedArabicText.replaceAll("[هة]", "(ه|ة)");
        replacedArabicText = replacedArabicText.replaceAll("ـ", "");
        replacedArabicText = replacedArabicText.replaceAll("%+", "%");
        logger.debug("Returning");
        return replacedArabicText;
    }

    /**
     * @return the addButton
     */
    public CommandButton getAddButton() {
        return addButton;
    }

    /**
     * @param addButton the addButton to set
     */
    public void setAddButton(CommandButton addButton) {
        this.addButton = addButton;
    }

    /**
     * @return the addRowButton
     */
    public CommandButton getAddRowButton() {
        return addRowButton;
    }

    /**
     * @param addRowButton the addRowButton to set
     */
    public void setAddRowButton(CommandButton addRowButton) {
        this.addRowButton = addRowButton;
    }

    /**
     * @return the saveButton
     */
    public CommandButton getSaveButton() {
        return saveButton;
    }

    /**
     * @param saveButton the saveButton to set
     */
    public void setSaveButton(CommandButton saveButton) {
        this.saveButton = saveButton;
    }

    /**
     * @return the showHideButton
     */
    public CommandButton getShowHideButton() {
        return showHideButton;
    }

    /**
     * @param showHideButton the showHideButton to set
     */
    public void setShowHideButton(CommandButton showHideButton) {
        this.showHideButton = showHideButton;
    }

    /**
     * @return the saveExitButton
     */
    public CommandButton getSaveExitButton() {
        return saveExitButton;
    }

    /**
     * @param saveExitButton the saveExitButton to set
     */
    public void setSaveExitButton(CommandButton saveExitButton) {
        this.saveExitButton = saveExitButton;
    }

    public void preProcess(Object document)
            throws IOException, BadElementException, DocumentException, com.lowagie.text.DocumentException {
        if (document instanceof Document) {

            Document pdf = (Document) document;
            pdf.setPageSize(PageSize.A4.rotate());
            pdf.open();
            String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm ").format(Calendar.getInstance().getTime());
            pdf.add(new Phrase("Date : " + timeStamp));
            Paragraph paragraph = new Paragraph(getCurrentScreen().getHeader(),
                    FontFactory.getFont(FontFactory.HELVETICA, 17, Font.NORMAL, new Color(0, 0, 0)));
            paragraph.setAlignment(CNTRL_OUTPUTMODE);
            pdf.add(paragraph);
            pdf.add(new Phrase(""));

        }
    }

    @Override
    public String returnSelectedList() {
        logger.debug("Entering");
        try {
            List<ScreenField> screenFieldList = getCurrentScreen().getScreenFields();
            ScreenField changedScrFld = getChangedField(screenFieldList);
            List<BaseEntity> lookupPickedElementList = TabularScreenDTO.listLookupPickedElement(
                    currentScreen.getInstID(),
                    fieldLookup.getOScreen().getName() + "_" + fieldLookup.getFieldExpression());
            if (lookupPickedElementList != null && !lookupPickedElementList.isEmpty()) {
                if (isFieldFiltered()) {
                    setFieldFilterMap(lookupPickedElementList);
                } else {
                    if (!recordID.equals("")) {
                        List<BaseEntity> entityList = lookupTransformedPickedElements;
                        if (changedScrFld.getChangeFunction() != null) {
                            List changeData = new LinkedList();
                            for (int i = 0; i < entityList.size(); i++) {
                                BaseEntity entity = entityList.get(i);
                                ODataType changeDT = dataTypeService.loadODataType("EntityChangeField", loggedUser);
                                changeData.add(0, entity);// getLoadedEntity()
                                changeData.add(1, fieldLookup.getFieldExpression());
                                changeData.add(2, lookupPickedElementList.get(i));
                                changeData.add(3, screenFieldList);

                                entity = callEntityFieldChangeFunction(changeDT, changeData, changedScrFld, entity);

                                FramworkUtilities.setValueInEntity(entity, fieldLookup.getFieldExpression(),
                                        lookupPickedElementList, true);
                                applyDepencyInitialization(entity, lookupPickedElementList,
                                        fieldLookup.getFieldExpression());
                                translateFields(screenFieldList, entity);
                            }
                        }

                        lookupPanelRendered = false;
                        logger.trace("Lookup Selection Information");
                    }
                }
            } else {
                logger.warn("Null Selected Entity");
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning with Null");
        return null;
    }

    private BaseEntity callEntityFieldChangeFunction(ODataType changeDT, List changeData, ScreenField changedScrFld,
            BaseEntity entity) {
        ODataMessage changeODM = new ODataMessage(changeDT, changeData);
        OFunctionResult ofr = functionServiceRemote.runFunction(changedScrFld.getChangeFunction(), changeODM, null,
                loggedUser);
        if (ofr.getReturnedDataMessage() != null && ofr.getReturnedDataMessage().getODataType() != null
                && !ofr.getReturnedDataMessage().getData().isEmpty() && ofr.getReturnedDataMessage().getData().get(0)
                .getClass().getName().equals(getOactOnEntity().getEntityClassPath())) {
            entity = (BaseEntity) ofr.getReturnedDataMessage().getData().get(0);
            if (entity.getDbid() != 0 && mode == OScreen.SM_ADD) {
                mode = OScreen.SM_EDIT;
            }

        }
        return entity;
    }

    private void translateFields(List<ScreenField> screenFieldList, BaseEntity entity)
            throws TextTranslationException, NoSuchMethodException {
        String expParent = fieldLookup.getFieldExpression().substring(0,
                fieldLookup.getFieldExpression().lastIndexOf("."));
        for (int idx = 0; idx < screenFieldList.size(); idx++) {
            if (screenFieldList.get(idx).getFieldExpression().startsWith(expParent)
                    && !screenFieldList.get(idx).getFieldExpression().equals(fieldLookup.getFieldExpression())) {
                if (screenFieldList.get(idx).getFieldExpression().contains(".")) {
                    String fieldExp = screenFieldList.get(idx).getFieldExpression().substring(0,
                            screenFieldList.get(idx).getFieldExpression().lastIndexOf("."));
                    if (screenFieldList.get(idx).getFieldExpression().substring(expParent.length() + 1).contains(".")) {
                        BaseEntity entityField = (BaseEntity) entity.invokeGetter(fieldExp);
                        if (entityField != null) {
                            textTranslationServiceRemote.loadEntityTranslation(entityField, loggedUser);

                        }
                    }
                }
            }
        }
    }

    private ScreenField getChangedField(List<ScreenField> screenFields) {
        for (int idx = 0; idx < screenFields.size(); idx++) {
            if (screenFields.get(idx).getFieldExpression().equals(fieldLookup.getFieldExpression())) {
                return screenFields.get(idx);
            }
        }
        return null;
    }

    private void setFieldFilterMap(List<BaseEntity> selectedEntity) {
        String filterFieldExp = fieldLookup.getOwnerEntityBackBeanExpression().substring(
                fieldLookup.getOwnerEntityBackBeanExpression().indexOf("['") + 2,
                fieldLookup.getOwnerEntityBackBeanExpression().indexOf("']"));
        valuesMap.put(filterFieldExp, selectedEntity.get(0));
    }

    @Override
    public void addClaimButtonToPanelGroup(String buttonID, String buttonLabel, String actionMethodName,
            HtmlPanelGrid panelLayout, Class[] params, String portletInsId, String beanName, String screenLanguage,
            boolean invalidInputMode, OScreen currentScreen) {
        throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose
        // Tools | Templates.
    }

    private boolean isFieldFiltered() {
        return null != fieldLookup && fieldLookup.getOwnerEntityBackBeanExpression() != null
                && fieldLookup.getOwnerEntityBackBeanExpression().contains("valuesMap");
    }

    /**
     * Lookup select event listener from JS function "lookupSelectionListener"
     * that get selected indexes from lookup tabular.
     *
     */
    public void lookupSelectionListener() {
        if (isLookupMode() && currentScreen.isMultiSelection()) {
            String selectedIndexes = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
                    .get("selected");
            updateLookupPickedElementsCache(selectedIndexes);
        }
    }

    public void validateHTML(FacesContext context, UIComponent validate, Object valueObj) {
        if (valueObj != null) {
            String valueStr = valueObj.toString();
            if (valueStr.contains("<") || valueStr.contains(">") || valueStr.contains("&lt") || valueStr.contains("&gt")
                    || valueStr.contains("&amp") || valueStr.contains("&")) {
                ((InputText) validate).setValid(false);
                HtmlMessage message = new HtmlMessage();
                UserMessage userMessage = usrMsgService.getUserMessage("HtmlNotAllowed", loggedUser);
                message.setTitle(userMessage.getMessageTitleTranslated());
                message.setStyleClass("inlineErrorMsgTitle");
                message.setFor(validate.getClientId(context));

                FacesMessage msg = new FacesMessage(userMessage.getMessageTextTranslated());
                dataTable.processUpdates(context);
                context.addMessage(validate.getClientId(context), msg);
            }

        }
    }
}
