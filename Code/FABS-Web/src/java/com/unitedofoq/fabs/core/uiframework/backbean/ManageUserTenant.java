package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.central.OTenant;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@ViewScoped
@ManagedBean
public class ManageUserTenant {

    private OCentralEntityManagerRemote centEM = OEJB.lookup(OCentralEntityManagerRemote.class);
    final static Logger logger = LoggerFactory.getLogger(ManageUserTenant.class);

    private String loginName;
    private Long selectedTenantId;
    private Map<String, Long> userTenants;

    @PostConstruct
    private void onInit(){
        Map<String, String> requestParams = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
        String userId = requestParams.get("UD");
        // get user login name
        loginName = OPortalUtil.getUserLoginName(Long.parseLong(userId));
        loadTenantList();
    }

    /**
     * Load Current user list of eligible tenants
     */
    private void loadTenantList(){
        List<OTenant> eligibleTenants = centEM.getUserEligibleTenants(loginName);
        userTenants = new TreeMap<>();
        for (OTenant tenant : eligibleTenants) {
            String tenantName = tenant.getName();
            String tenantCompanyName = tenant.getCompanyName();
            if (tenantCompanyName == null || "".equalsIgnoreCase(tenantCompanyName.trim())) {
                userTenants.put(tenantName, tenant.getId());
            } else {
                userTenants.put(tenantCompanyName, tenant.getId());
            }
        }
    }

    /**
     * Action Listener to update user tenant based on the selectedTenantId, and Logout the user afterwards.
     */
    public void changeTenant(){
        if(selectedTenantId != null){
            logger.debug("change current user tenant to %s", selectedTenantId);
            centEM.updateOCentralUserDefaultTenant(loginName, selectedTenantId);
            // force user logout after changing tenant
            RequestContext.getCurrentInstance().execute("top.FABS.Session.logout();");
        }
    }

    public void onTenantChange() {}

    //<editor-fold desc="Getters/Setters">
    public Long getSelectedTenantId() {
        return selectedTenantId;
    }

    public void setSelectedTenantId(Long selectedTenantId) {
        this.selectedTenantId = selectedTenantId;
    }

    public Map<String, Long> getUserTenants() {
        return userTenants;
    }

    public void setUserTenants(Map<String, Long> userTenants) {
        this.userTenants = userTenants;
    }
    //</editor-fold>

}
