/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.backbean.wizard;

import com.unitedofoq.fabs.core.uiframework.screen.wizard.WizardStep;

/**
 *
 * @author mmohamed
 */
public class ScreenWizardInformation {

    private WizardStep step;

    private boolean enableFinish;

    private boolean enableSkip;

    private boolean enableBack;

    private boolean enableNext;

    public boolean isEnableBack() {
        return enableBack;
    }

    public void setEnableBack(boolean enableBack) {
        this.enableBack = enableBack;
    }

    public boolean isEnableNext() {
        return enableNext;
    }

    public void setEnableNext(boolean enableNext) {
        this.enableNext = enableNext;
    }

    public ScreenWizardInformation() {
    }

    public boolean isEnableSkip() {
        return enableSkip;
    }

    public void setEnableSkip(boolean enableSkip) {
        this.enableSkip = enableSkip;
    }

    public ScreenWizardInformation(WizardStep step, boolean enableFinish) {
        this.step = step;
        this.enableFinish = enableFinish;
    }

    public boolean isEnableFinish() {
        return enableFinish;
    }

    public void setEnableFinish(boolean enableFinish) {
        this.enableFinish = enableFinish;
    }

    public WizardStep getStep() {
        return step;
    }

    public void setStep(WizardStep step) {
        this.step = step;
    }
    
}
