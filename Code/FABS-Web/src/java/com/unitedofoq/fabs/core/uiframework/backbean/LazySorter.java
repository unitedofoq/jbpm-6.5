/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import java.util.Comparator;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mibrahim
 */
public class LazySorter implements Comparator<BaseEntity> {
final static Logger logger = LoggerFactory.getLogger(LazySorter.class);
    private String sortField;
   
    private SortOrder sortOrder;
   
    public LazySorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }

    @Override
    public int compare(BaseEntity o1, BaseEntity o2) {
        try {
            Object value1 = BaseEntity.class.getField(this.sortField).get(o1);
            Object value2 = BaseEntity.class.getField(this.sortField).get(o2);

            int value = ((Comparable)value1).compareTo(value2);
           
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception ex) {
            logger.error("Exception thrown",ex);
            throw new RuntimeException();
        }
    }
}

