/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.backbean;


import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitysetup.OObjectBriefInfoField;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.uiframework.screen.OBackBeanException;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import java.util.List;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIColumn;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlPanelGrid;

/**
 *
 * @author nkhalil
 */
@ManagedBean(name="MultiListBean")
@ViewScoped
public class MultiListBean extends FormBean{
    private HtmlPanelGrid listsGrid;
    @Override
    public BaseEntity getLoadedObject() {
        return loadedObject;
    }

    @Override
    public void setLoadedObject(BaseEntity loadedObject) {
        this.loadedObject = loadedObject;
    }

    public HtmlPanelGrid getListsGrid() {
        if (listsGrid == null){
            listsGrid = new HtmlPanelGrid();
            listsGrid.getChildren().add(buildFacesList());
        }
        return listsGrid;
    }

    public void setListsGrid(HtmlPanelGrid listsGrid) {
        this.listsGrid = listsGrid;
    }

    private HtmlPanelGrid buildFacesList() {
        logger.trace("Entering");
        try {
            validateInputSetMapping();
        } catch (OBackBeanException ex) {
            logger.error("Exception thrown",ex);
            UserMessage errorMsg = usrMsgService.getUserMessage(
                    ex.getMessage(), loggedUser);
            //TODO: usage of ex.getMessage is not guaranteed at all; fix that
            getMessages().clear();
            if (errorMsg != null) {
                getMessages().add(errorMsg);
               // buildMessagePanel();
            }
            return new HtmlPanelGrid();
        }
        if (restorePDataFromSessoion) {
            setLoadedObject((BaseEntity) restoreObject("loadedObject"));
        } else {
            processInput();
        }
        List<OObjectBriefInfoField> biFields = null;
        List<ExpressionFieldInfo> expFieldInfos;
        OObjectBriefInfoField masterField = null;
        HtmlPanelGrid generalGrid = new HtmlPanelGrid();
        List<ScreenField> screenFields = getCurrentScreen().getScreenFields();
        
        generalGrid.setColumns(screenFields.size());
        generalGrid.setBorder(1);

        for (int scrFldIndex = 0; scrFldIndex < screenFields.size(); scrFldIndex++) {
            try {
                expFieldInfos = BaseEntity.parseFieldExpression(Class.forName(getOactOnEntity().getEntityClassPath()),
                        screenFields.get(scrFldIndex).getFieldExpression());
                Class currentListClass = BaseEntity.getFieldObjectType(expFieldInfos.get(1).field);
                OEntityDTO currentListOEntity = entitySetupService.
                        loadOEntityDTO(currentListClass.getName(), getLoggedUser());
                biFields = entitySetupService.getObjectDTOBriefInfoFields(currentListOEntity, getLoggedUser());

                HtmlDataTable dataTable = new HtmlDataTable();
                dataTable.setId("table_" + screenFields.get(scrFldIndex).getFieldExpression().replace(".", "_") + "_" + scrFldIndex);
                dataTable.setVar("tableRow" + scrFldIndex);
                dataTable.setValueExpression("value",
                exFactory.createValueExpression(elContext,
                "#{" + getBeanName() + ".loadedObject."+ screenFields.get(scrFldIndex).getFieldExpression()+"}", List.class));
                
                UIColumn column = new UIColumn();
                column.setId("column_" + screenFields.get(scrFldIndex).getFieldExpression().replace(".", "_") + "_" + scrFldIndex);

                HtmlOutputLabel columnHeader = new HtmlOutputLabel();
                columnHeader.setId("columnHeader_" + screenFields.get(scrFldIndex).
                        getFieldExpression().replace(".", "_") + "_" + scrFldIndex);
                columnHeader.setValue(screenFields.get(scrFldIndex).getDd().getHeader());
                column.setHeader(columnHeader);

                for (int briefInfoIdx = 0; briefInfoIdx < biFields.size(); briefInfoIdx++) {
                    if(biFields.get(briefInfoIdx).isMasterField()){
                        masterField = biFields.get(briefInfoIdx);
                        break;
                    }
                }
                HtmlOutputLabel label = new HtmlOutputLabel();
                label.setId("label_" + screenFields.get(scrFldIndex).getFieldExpression().replace(".", "_") + "_" + scrFldIndex);
                label.setValueExpression("value", exFactory.createValueExpression(elContext, "#{tableRow" + scrFldIndex+
                        "."+ masterField.getFieldExpression()+"}"
                        , String.class));
                column.getChildren().add(label);
                dataTable.getChildren().add(column);
                generalGrid.getChildren().add(dataTable);
            } catch (Exception ex) {
                logger.error("Exception thrown",ex);
            }
        }
        logger.trace("Returning");
        return generalGrid;
    }

    @Override
    protected String getBeanName() {
        return MultiListBean.class.getSimpleName();
    }

    @Override
    protected boolean processInput(){
        if (!super.processInput())
            return false ;
        
        for (int idx = 0; idx < currentScreenInput.getAttributesMapping().size(); idx++) {
            if (currentScreenInput.getAttributesMapping().get(idx).getFieldExpression().equalsIgnoreCase("this")) {
                loadedObject = (BaseEntity) dataMessage.getData().get(idx);
                break;
            } 
        }
        
        return true ;
    }
}
