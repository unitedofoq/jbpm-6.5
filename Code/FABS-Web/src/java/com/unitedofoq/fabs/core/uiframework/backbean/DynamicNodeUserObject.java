package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.uiframework.backbean.DynamicNodeUserObject.NodeProperty;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>The
 * <code>NodeUserObject</code> represents a nodes user object. This particular
 * IceUserobject implementation store extra information on how many times the
 * parent node is clicked on. It is also responsible for copying and delete its
 * self.</p>
 * <p/>
 * <p>In this example pay particularly close attention to the
 * <code>wrapper</code> instance variable on IceUserObject. The
 * <code>wrapper</code> allows for direct manipulations of the parent tree. </p>
 */
public class DynamicNodeUserObject {

    // panel stack which will be manipulated when a command links action is fired.
    private TreeBrowser tree;
    private String nodeField;
    private String nodeClass;
    private String nodeGenericType;
    private String nodeExpression;
    private String nodeToolTip;
    private String customStyleClass;
    private String nodeColor = "black";
    private String description;
    private Class parentClass;
    private List<NodeProperty> nodeProperties;
    static private Map<DynamicNodeUserObject, List<NodeProperty>> allNodesProperties = new HashMap();

    public List<NodeProperty> getNodeProperties() {
        List<NodeProperty> properties = allNodesProperties.get(this);
        if (properties == null) {
            properties = createNodePropertiesTable(this);
        }
        return properties;
    }

    public void setNodeProperties(List<NodeProperty> nodeProperties) {
        this.nodeProperties = nodeProperties;
    }

    public Class getParentClass() {
        return parentClass;
    }

    public void setParentClass(Class parentClass) {
        this.parentClass = parentClass;
    }

    public String getMeasureUnit() {
        if (parentClass == null) {
            return "";
        }
        if (nodeField == null) {
            return "";
        }
        if ("".equals(nodeField)) {
            return "";
        }

        String measureUnit = BaseEntity.getMeasuarableFieldUnit(nodeField, parentClass);

        return (measureUnit != null) ? measureUnit : "";
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DynamicNodeUserObject(String fieldExpression,
            String styleClass) {
        nodeExpression = fieldExpression;
        customStyleClass = styleClass;
    }

    public DynamicNodeUserObject() {
    }

     /**
     * Registers a user click with this object and updates the selected node in
     * the ObjectAttributeBrowserBean.
     *
     * @param event that fired this method
     */
    public void nodeClicked() {
        //tree.setSelectedNodeObject(this);
        tree.addChildren();
    }

    public String getNodeToolTip() {
        return nodeToolTip;
    }

    public void setNodeToolTip(String nodeToolTip) {
        this.nodeToolTip = nodeToolTip;
    }

    public String getNodeClass() {
        return nodeClass;
    }

    public void setNodeClass(String nodeClass) {
        this.nodeClass = nodeClass;
    }

    public String getNodeGenericType() {
        return nodeGenericType;
    }

    public void setNodeGenericType(String genericType) {
        this.nodeGenericType = genericType;
    }

    public String getNodeExpression() {
        return nodeExpression;
    }

    public void setNodeExpression(String nodeExpression) {
        this.nodeExpression = nodeExpression;
    }

    public String getNodeField() {
        return nodeField;
    }

    public void setNodeField(String nodeField) {
        this.nodeField = nodeField;
    }

    public String getNodeColor() {
        return nodeColor;
    }

    public void setNodeColor(String nodeColor) {
        this.nodeColor = nodeColor;
    }

    private List<NodeProperty> createNodePropertiesTable(DynamicNodeUserObject curentNode) {
        List<NodeProperty> currentNodeProperties = new ArrayList<NodeProperty>();
        /* Measure Unit Property*/
        NodeProperty measureUnitProperty = new NodeProperty();
        measureUnitProperty.setProperty("Unit Field:");
        measureUnitProperty.setValue(getMeasureUnit());

        currentNodeProperties.add(measureUnitProperty);

        allNodesProperties.put(this, currentNodeProperties);
        return currentNodeProperties;
    }

    /**
     * @return the styleClass
     */
    public String getCustomStyleClass() {
        return customStyleClass;
    }

    /**
     * @param styleClass the styleClass to set
     */
    public void setCustomStyleClass(String customStyleClass) {
        this.customStyleClass = customStyleClass;
    }

    public class NodeProperty {

        private String property;
        private String value;

        public String getProperty() {
            return property;
        }

        public void setProperty(String property) {
            this.property = property;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
