/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.backbean;

/**
 *
 * @author arezk
 */
public class DisplayedMenuArray {

    public DisplayedMenuArray(boolean includeSubs, boolean assigned) {
        this.includeSubs = includeSubs;
        this.assigned = assigned;
    }
    
    private boolean includeSubs;
    private boolean assigned;

    public boolean isAssigned() {
        return assigned;
    }

    public void setAssigned(boolean assigned) {
        this.assigned = assigned;
    }

    public boolean isIncludeSubs() {
        return includeSubs;
    }

    public void setIncludeSubs(boolean includeSubs) {
        this.includeSubs = includeSubs;
    }
}
