/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.backbean;


import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.report.OReport;
import com.unitedofoq.fabs.core.report.OReportLayout;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlPanelGrid;

/**
 * <p>Session scope data bean for your application.  Create properties
 *  here to represent cached data that should be made available across
 *  multiple HTTP requests for an individual user.</p>
 *
 * <p>An instance of this class will be created for you automatically,
 * the first time your application evaluates a value binding expression
 * or method binding expression that references a managed bean using
 * this class.</p>
 *
 * @version ReportLayout.java
 * @version Created on Dec 26, 2010, 3:54:12 PM
 * @author melsayed
 */
@ManagedBean(name="ReportLayout")
@ViewScoped
public class ReportLayout extends FormBean {
    // <editor-fold defaultstate="collapsed" desc="Managed Component Definition">

    /**
     * <p>Automatically managed component initialization.  <strong>WARNING:</strong>
     * This method is automatically generated, so any user-specified code inserted
     * here is subject to being replaced.</p>
     */
    private void _init() throws Exception {
    }
    // </editor-fold>

    private HtmlPanelGrid layoutPanel = new HtmlPanelGrid();
    /**
     * <p>Construct a new session data bean instance.</p>
     */
    public ReportLayout() {
    }

    private boolean refreshLayout;

    public boolean isRefreshLayout() {
        System.out.println("Refreshing ReportLayout Screen");
        if(getScreenParam(OReportLayout.class.getName())!=null){
            layoutPanel.getChildren().clear();
            layoutPanel.getChildren().add(buildInnerPanel());
            SessionManager.setScreenSessionAttribute(getCurrentScreen().getName(), 
                    OReportLayout.class.getName(), null);
        }
        return refreshLayout;
    }

    public void setRefreshLayout(boolean refreshLayout) {
        this.refreshLayout = refreshLayout;
    }
    
    public HtmlPanelGrid buildInnerPanel() {
        logger.debug("Entering");
        OReportLayout oReportLayout = null;
        if(restorePDataFromSessoion){
            setLoadedObject((BaseEntity) restoreObject("loadedObject"));
        }else{
            dataMessage = (ODataMessage)getScreenParam(OReportLayout.class.getName());
            if(dataMessage!= null && dataMessage.getODataType().getName().equalsIgnoreCase("OReport"))
            {
                OReport oReport = (OReport) dataMessage.getData().get(0);
                if(oReport != null) oReportLayout = oReport.getLayoutType();
            }
            } 
        setLayoutPanel(new HtmlPanelGrid());
        layoutPanel.setColumns(1);
        layoutPanel.setCellpadding("10px");
        layoutPanel.setCellspacing("10px");

        try
        {
            HtmlGraphicImage graphicImage = new HtmlGraphicImage();

            if(oReportLayout == null || oReportLayout.getSnapshotPath()== null
                    ||oReportLayout.getSnapshotPath().equals(""))
            {
                graphicImage.setUrl("/resources/empty.bmp");
                graphicImage.setWidth("100%");
                graphicImage.setHeight("100%");
                layoutPanel.getChildren().add(graphicImage);
            }
            else
            {
                graphicImage.setUrl(oReportLayout.getSnapshotPath());
                graphicImage.setWidth("100%");
                graphicImage.setHeight("100%");
                layoutPanel.getChildren().add(graphicImage);
            }
        }
        catch(Exception ex)
        {
           logger.error("Exception thrown",ex); 
        }
logger.debug("Returning");
        return layoutPanel;
    }

    @Override
    protected String getBeanName() {
        return "ReportLayout";
    }

    @Override
    public ODataMessage saveProcessRecords() {
        return null;
    }

    @Override
    protected BaseEntity getObjectForLookup(long dbid) {
        return null;
    }

    @Override
    protected void buildScreenExpression(UIComponent component) {
        return;
    }
    
    /**
     * @return the layoutPanel
     */
    public HtmlPanelGrid getLayoutPanel() {
        return buildInnerPanel();
    }

    /**
     * @param layoutPanel the layoutPanel to set
     */
    public void setLayoutPanel(HtmlPanelGrid layoutPanel) {
        this.layoutPanel = layoutPanel;
    }
    
}
