/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.backbean;


import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.i18n.TextTranslation;
import com.unitedofoq.fabs.core.i18n.Translatable;

import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.el.MethodExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIColumn;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;
import javax.faces.event.ValueChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nkhalil
 */
@ManagedBean(name="TranslationBean")
@ViewScoped
public class TranslationBean extends TabularBean{
    final static Logger logger = LoggerFactory.getLogger(TranslationBean.class);
    private HtmlPanelGrid grid = new HtmlPanelGrid();
    private boolean fetchEnable;
    private String validationString="";// = true;

    public String getValidationString() {        
        return validationString;
    }

    public void setValidationString(String validationString) {
        this.validationString = validationString;
    }

    public boolean isFetchEnable() {
        return !(transEntity != null &&
                translatedFLD != null &&
                translation.getLanguage() != null);
    }

    public void setFetchEnable(boolean fetchEnable) {
        this.fetchEnable = fetchEnable;
    }

    


    public TextTranslation getTranslation() {
        return translation;
    }

    public void setTranslation(TextTranslation translation) {
        this.translation = translation;
    }

    public List<BaseEntity> getBeanTranslations() {
        return beanTranslations;
    }

    public void setBeanTranslations(List<BaseEntity> beanTranslations) {
        this.beanTranslations = beanTranslations;
    }
    

    private OEntityDTO transEntity;
    private String fldExp;
    List<BaseEntity> beanTranslations = new ArrayList<BaseEntity>();
    TextTranslation translation = new TextTranslation();
    Field translatedFLD;
    public String getFldExp() {
        return fldExp;
    }

    public void setFldExp(String fldExp) {
        this.fldExp = fldExp;
    }


    public OEntityDTO getTransEntity() {
        return transEntity;
    }

    public void setTransEntity(OEntityDTO transEntity) {
        this.transEntity = transEntity;
    }


    /** Creates a new instance of TranslationBean */
    public TranslationBean() {
    }

    @Override
    public HtmlPanelGrid buildInnerScreen() { 
        logger.debug("Entering");
        HtmlPanelGrid buttonsPanel = new HtmlPanelGrid();
        grid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
      
        buttonsPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        buttonsPanel.setColumns(2);
        buttonsPanel.setCellpadding("5px");
        buttonsPanel.setCellspacing("0px");

        HtmlCommandButton saveButton = new HtmlCommandButton();
        saveButton.setActionExpression(exFactory.createMethodExpression(elContext,
                "#{" + getBeanName() + ".saveAction}", String.class, new Class[0]));
        
        saveButton.setValue(ddService.getDD("saveButton", loggedUser).getLabelTranslated());

        buttonsPanel.getChildren().add(saveButton);

        grid.setColumns(1);
        grid.setCellpadding("5px");
        grid.setCellspacing("0px");
        messagesPanel = new UserMessagePanel(exFactory, elContext, getBeanName(), ddService, this, loggedUser);
        grid.getChildren().add(messagesPanel.createMessagePanel());        

        //Create Entity Control
        HtmlPanelGrid entityPanel = new HtmlPanelGrid();
        entityPanel.setColumns(2);
        entityPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        String lookupID=FacesContext.getCurrentInstance().getViewRoot().createUniqueId();
        DD entityDD = ddService.getDD("TextTranslation_entityDBID", loggedUser);
        fieldLookup = new Lookup(this,
                                getBeanName(),
                                getBeanName() + ".transEntity",
                                entityDD,
                                "entityClassPath",
                                null,
                                lookupID,
                                new ScreenField(),
                                false,
                                oem, filterServiceBean,
                                exFactory,
                                getFacesContext(),
                                loggedUser);

        getLookupHashMap().put(lookupID, fieldLookup);
        HtmlOutputLabel entityLabel = new HtmlOutputLabel();
        entityLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        entityLabel.setValue(ddService.getDD("TranslationEntityLabel", loggedUser).getLabelTranslated());

        entityPanel.getChildren().add(entityLabel);
        entityPanel.getChildren().add(fieldLookup.createNonDropDown());

        //Create Translation field Attribute
        HtmlPanelGrid fieldPanel = new HtmlPanelGrid();
        fieldPanel.setColumns(3);
        fieldPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        String fldlookupID=FacesContext.getCurrentInstance().getViewRoot().createUniqueId();
        DD fldDD = ddService.getDD("TextTranslation_fieldExpression", loggedUser);
        fieldLookup = new Lookup(this,
                                getBeanName(),
                                getBeanName()+".translation",
                                fldDD,
                                 "fieldExpression",
                                null,
                                fldlookupID,
                                new ScreenField(),
                                false,
                                oem, filterServiceBean,
                                exFactory,
                                getFacesContext(),
                                loggedUser);

        getLookupHashMap().put(fldlookupID, fieldLookup);
        fieldLookup.setLookupFilter(Collections.singletonList("this.transEntity.entityClassPath"));
        HtmlOutputLabel fldLabel = new HtmlOutputLabel();
        fldLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        fldLabel.setValue(ddService.getDD("TranslationFieldLabel", loggedUser).getLabelTranslated());        
        fieldPanel.getChildren().add(fldLabel);
        fieldPanel.getChildren().add(fieldLookup.createNonDropDown());
        
        HtmlOutputLabel validLabel = new HtmlOutputLabel();
        validLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        
        validLabel.setStyle("color:red");
        validLabel.setValueExpression("value",exFactory.createValueExpression(
                elContext, "#{"+ getBeanName() + ".validationString}", String.class));
        fieldPanel.getChildren().add(validLabel);

        //Create Translation Language Control
        HtmlPanelGrid langPanel = new HtmlPanelGrid();
        langPanel.setColumns(2);
        langPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        String langlookupID=FacesContext.getCurrentInstance().getViewRoot().createUniqueId();
        DD langDD = ddService.getDD("TextTranslation_language", loggedUser);
        fieldLookup = new Lookup(this,
                                getBeanName(),
                                getBeanName()+".translation",
                                langDD,
                                 "language.dbid",
                                null,
                                langlookupID,
                                new ScreenField(),
                                false,
                                oem, filterServiceBean,
                                exFactory,
                                getFacesContext(),
                                loggedUser);

        
        HtmlOutputLabel langLabel = new HtmlOutputLabel();
        langLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        langLabel.setValue(ddService.getDD("TranslationLanguageLabel", loggedUser).getLabelTranslated());
        langPanel.getChildren().add(langLabel);
        langPanel.getChildren().add(fieldLookup.createDropDown(false));
        getSelectItemsList().put(fieldLookup.getDropDownMenu().getId(),
                            fieldLookup.getSelectItems());


        //Create the load data Button
        HtmlCommandButton loadBeanTranslationsButton = new HtmlCommandButton();
        loadBeanTranslationsButton.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        loadBeanTranslationsButton.setValue(ddService.getDD("FetchTranslationsLabel", loggedUser).getLabelTranslated());
        loadBeanTranslationsButton.setValueExpression("disabled",exFactory.createValueExpression(
                elContext, "#{"+ getBeanName() + ".fetchEnable}", Boolean.class));
         Class[] parms2 = new Class[]{ActionEvent.class};
        MethodExpression mb = exFactory.createMethodExpression(elContext,
                "#{" + getBeanName() + ".loadBeanTranslationsBtn}", null, parms2);
        MethodExpressionActionListener al = new MethodExpressionActionListener(mb);
        loadBeanTranslationsButton.addActionListener(al);
        
        grid.getChildren().add(buttonsPanel);
        grid.getChildren().add(entityPanel);
        grid.getChildren().add(fieldPanel);
        grid.getChildren().add(langPanel);
        grid.getChildren().add(loadBeanTranslationsButton);
logger.debug("Returning");
        return grid;
    }

    @Override
    protected String getBeanName() {
        return "TranslationBean";
    }

    @Override
    public String returnSelected() {
        try {
            if (getLookupData() != null) {
                transEntity = (OEntityDTO) getLookupData();
                actOnEntityCls = Class.forName(transEntity.getEntityClassPath());
                setLookupPanelRendered(false);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        return null;
    }

    @Override
    public String getLookupConditionValue(String conditionExpression){
        if(transEntity != null)return transEntity.getEntityClassPath();
        return null;
    }

    @Override
    public String returnTreeSelectedExpression() {  
        logger.debug("Returning with Null");
        return null;
    }

    private List<BaseEntity> loadBeanTranslations() {
        logger.trace("Entering");
        String dbids = "(";        
        try {
            oem.getEM(systemUser);
            // Get the orginal entities
            List<BaseEntity> targetEntities = oem.loadEntityList(transEntity.getEntityClassName(),
                null, null, Collections.singletonList("dbid"), loggedUser);
            
            // Put DBIDs in list to be used in IN STATEMENT
            for (BaseEntity baseEntity : targetEntities) {
                dbids+=baseEntity.getDbid()+",";
            }
            dbids =  dbids.substring(0,dbids.lastIndexOf(","));
            dbids += ")";

            // Text Translation Conditions
            List<String> textTransConds = new ArrayList<String>();
            textTransConds.add("entityDBID IN" + dbids);
            textTransConds.add("fieldExpression='" + transEntity.getEntityClassName() +
                    "." + translation.getFieldExpression() + "'") ;
            textTransConds.add("language.dbid=" + translation.getLanguage().getDbid());
            
            // Load associated translations
            List<TextTranslation> textTranslations = oem.loadEntityList(TextTranslation.class.getSimpleName(),
                    textTransConds, null, Collections.singletonList("entityDBID"), loggedUser);

            // Put each loaded translation-entity into the corresponding owner-entity
            int trnsEntityIdx = 0;
            if (textTranslations != null && !textTranslations.isEmpty()) {
                for (int targtEntityIdx = 0; targtEntityIdx < targetEntities.size(); targtEntityIdx++) {
                    if (trnsEntityIdx < textTranslations.size() &&
                            targetEntities.get(targtEntityIdx).getDbid() == textTranslations.get(trnsEntityIdx).getEntityDBID())
                    {
                        BaseEntity.setValueInEntity(targetEntities.get(targtEntityIdx),
                                translatedFLD.getAnnotation(Translatable.class).translationField(),
                                textTranslations.get(trnsEntityIdx).getTextValue());
                        trnsEntityIdx++;
                    }else{
                        BaseEntity.setValueInEntity(targetEntities.get(targtEntityIdx),
                                translatedFLD.getAnnotation(Translatable.class).translationField(),
                                "");
                    }
                }
            }else{
                for (int targtEntityIdx = 0; targtEntityIdx < targetEntities.size(); targtEntityIdx++) {
                    BaseEntity.setValueInEntity(targetEntities.get(targtEntityIdx),
                                translatedFLD.getAnnotation(Translatable.class).translationField(),
                                "");
                }
            }
            setLoadedList(targetEntities);
            logger.debug("Returning");
            return targetEntities;
        } catch (Exception ex) {
           logger.error("Exception thrown",ex);
           logger.debug("Returning");
           return new ArrayList<BaseEntity>();
        } finally {
            oem.closeEM(systemUser);
        }
    }
    public void loadBeanTranslationsBtn(ActionEvent event)
    {
        try{
        oem.getEM(loggedUser);
        beanTranslations = loadBeanTranslations();
        //Build Translation Table
        HtmlDataTable table = new HtmlDataTable();
        table.setId(getCurrentScreen().getName() + "_gt");
        table.setValueExpression("value",
                exFactory.createValueExpression(elContext,
                "#{" + getBeanName() + ".beanTranslations}", ArrayList.class));
        table.setVar("currentRow");
        table.setStyle("position:relative;");
        table.setRows(7);
        // <editor-fold defaultstate="collapsed" desc="Side Balloon Column">
            //column=new UIColumn
            UIColumn bcolumn = new UIColumn();
            bcolumn.setId("bcolumn");
            HtmlOutputLabel headerBallonlbl = new HtmlOutputLabel();
            bcolumn.setHeader(headerBallonlbl);
            bcolumn.setId("col_balloon");
            table.getChildren().add(bcolumn);

        //</editor-fold>
        UIColumn defaultLngColumn = new UIColumn();
        defaultLngColumn.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        HtmlOutputLabel defaultLangLbl = new HtmlOutputLabel();
        defaultLangLbl.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        defaultLangLbl.setValueExpression("value", exFactory.createValueExpression(elContext,
                "#{currentRow."+ fldExp+"}", String.class));
        HtmlOutputLabel defaultHeader = new HtmlOutputLabel();
        defaultHeader.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        defaultHeader.setValue(ddService.getDD("DefaultLangLabel", loggedUser).getLabelTranslated());
        defaultLngColumn.setHeader(defaultHeader);
        defaultLngColumn.getChildren().add(defaultLangLbl);

        //Translation Text
        UIColumn transColumn = new UIColumn();
        HtmlOutputLabel transHeader = new HtmlOutputLabel();
        transHeader.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        transHeader.setValue(ddService.getDD("TransValueLabel", loggedUser).getLabelTranslated());
        transColumn.setHeader(transHeader);
        transColumn.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        String transFLD = "#{currentRow."+ translatedFLD.getAnnotation(Translatable.class).translationField()+"}";
        HtmlInputText transText = (HtmlInputText) createTextFieldControl(20,
                FacesContext.getCurrentInstance().getViewRoot().createUniqueId(),
                transFLD,
                true, null, 2, false, false ,false);
        FramworkUtilities.setControlFldExp(transText,translatedFLD.getAnnotation(Translatable.class).translationField());
       
        transColumn.getChildren().add(transText);

        table.getChildren().add(defaultLngColumn);
        table.getChildren().add(transColumn);

        htmlPanelGrid.getChildren().add(table);
        }catch(Exception ex){
         logger.error("Exception thrown",ex);
        }
    }

    public String saveAction() {
        OFunctionResult oFR = new OFunctionResult();
        try {
            oem.getEM(systemUser);
            for (BaseEntity entityTranslation : beanTranslations) {
                if (!entityTranslation.isChanged()) continue ;
            }
            
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            
        } finally {
            messagesPanel.clearAndDisplayMessages(oFR);
            oem.closeEM(systemUser);
            logger.debug("Returning with Null ");
            return null;
        }
    }

    @Override
    public void updateRow(ValueChangeEvent vce) {
        
    }
    
}

