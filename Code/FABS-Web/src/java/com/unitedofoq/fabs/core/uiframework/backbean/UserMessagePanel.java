/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import com.unitedofoq.fabs.core.function.OFunctionResult;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.faces.component.UIColumn;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.context.FacesContext;
import org.primefaces.component.panel.Panel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mmohamed
 */
public class UserMessagePanel implements Serializable {
final static Logger logger = LoggerFactory.getLogger(UserMessagePanel.class);
    HtmlPanelGroup groupPanel;
    HtmlPanelGroup headerPanel;
    Panel panelC;
    private DDServiceRemote dDService;
    private OUser loggedUser;

    public Panel getPanelC() {
        return panelC;
    }

    public void setPanelC(Panel panelC) {
        this.panelC = panelC;
    }

    void updateHeaderLabel() {
        logger.debug("Entering");
        int errors = 0, warnings = 0, successes = 0;
        for (int idx = 0; idx < messages.size(); idx++) {
            if (messages.get(idx).getMessageType() != null && messages.get(idx).getMessageType() == UserMessage.TYPE_ERROR) {
                errors++;
            } else if (messages.get(idx).getMessageType() != null && messages.get(idx).getMessageType() == UserMessage.TYPE_WARNING) {
                warnings++;
            } else if (messages.get(idx).getMessageType() != null && messages.get(idx).getMessageType() == UserMessage.TYPE_SUCCESS) {
                successes++;
            }

        }
        String headerText = "User Messages ";
        if (errors != 0) {
            headerText += " | " + errors + " Error(s)";
        }
        if (warnings != 0) {
            headerText += " | " + warnings + " Warning(s)";
        }
        if (successes != 0) {
            headerText += " | " + successes + " Success(es)";
        }
        // Check header label is not null, as it might be called in an early
        // stage
        if (headerLabel != null) {
            headerLabel.setValue(headerText);
        }
    }
    String usrMsgStyle = "usr-msg-green";
    private ExpressionFactory exFactory = null;
    private ELContext elContext = null;
    private String beanName;
    private List<UserMessage> messages = new ArrayList<UserMessage>();

    public List<UserMessage> getMessages() {
        return messages;
    }
    private OBackBean ownerBean;

    public OBackBean getOwnerBean() {
        return ownerBean;
    }

    public void setOwnerBean(OBackBean ownerBean) {
        this.ownerBean = ownerBean;
    }

    public UserMessagePanel(ExpressionFactory exFactory, ELContext elContext,
            String beanName, DDServiceRemote ddService, OBackBean ownerBean, OUser loggedUser) {
        this.exFactory = exFactory;
        this.elContext = elContext;
        this.beanName = beanName;
        this.ownerBean = ownerBean;
        this.dDService = ddService;
        this.loggedUser = loggedUser;
    }

    public Panel createMessagePanel() {
        panelC = new Panel();
        panelC.setToggleable(true);
        panelC.setToggleSpeed(500);
        panelC.setStyleClass(usrMsgStyle);

        panelC.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "panelC");
        panelC.setCollapsed(true);
        String renderExpression = "#{empty " + beanName + ".messagesPanel.messages[0] ? 'false':'true'}";
        panelC.setValueExpression(
                "visible",
                exFactory.createValueExpression(
                elContext,
                renderExpression,
                Boolean.class));

        String bindingExpression = "#{" + beanName + ".collapsiblePanel}";
        panelC.setValueExpression(
                "binding",
                exFactory.createValueExpression(
                elContext,
                bindingExpression,
                String.class));

        createMsgBodyHeader();
        panelC.getFacets().put("header", headerPanel);

        createMsgBodyPanel();
        panelC.getChildren().add(groupPanel);
logger.trace("Returning");
        return panelC;
    }

    void createMsgBodyHeader() {
        logger.trace("Entering");
        headerPanel = new HtmlPanelGroup();
        headerPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_MsgHeaderPnl");

        String bindingExpression = "#{" + beanName + ".messagesPanel.headerLabel}";


        headerLabel.setValue(dDService.getDD("MsgHeader", loggedUser).getLabelTranslated());
        headerLabel.setValueExpression(
                "binding",
                exFactory.createValueExpression(
                elContext,
                bindingExpression,
                String.class));
        headerLabel.setStyleClass("usr-msg-header-lbl");

        headerLabel.setDir(ownerBean.getHtmlDir());
        headerPanel.setStyleClass("usr-msg-header-pnl");
        if (loggedUser.getFirstLanguage().getDbid() == OUser.ARABIC) {
            headerPanel.setStyleClass("usr-msg-header-RTL");
        }
        headerPanel.getChildren().add(headerLabel);
        logger.trace("Returning");
    }

    void createMsgBodyPanel() {
        logger.trace("Entering");
        groupPanel = new HtmlPanelGroup();
        groupPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "groupPanel");

        HtmlPanelGrid gridPanel = new HtmlPanelGrid();
        gridPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "InrgridPanel");

        gridPanel.setStyleClass("userMsgInnerTbl");

        gridPanel.getChildren().add(createMessagestable());

        groupPanel.getChildren().add(gridPanel);
        logger.trace("Returning");
    }

    HtmlDataTable createMessagestable() {
        logger.trace("Entering");
        String variable = "msg";
        HtmlDataTable table = new HtmlDataTable();
        table.setStyleClass("userMsgMessagesTable");

        table.setDir(ownerBean.getHtmlDir());
        table.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "messageTBL");
        table.setVar(variable);
        table.setBorder(0);
        table.setValueExpression("value",
                exFactory.createValueExpression(elContext,
                "#{" + beanName + ".messagesPanel.messages}", ArrayList.class));


        UIColumn column1 = new UIColumn();
        column1.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "col1");
        HtmlGraphicImage img = new HtmlGraphicImage();
        img.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "msgImg");
        img.setDir(ownerBean.getHtmlDir());
        img.setHeight("20px");
        img.setWidth("20px");
        img.setValueExpression("url",
                exFactory.createValueExpression(elContext,
                "#{" + variable + ".messageType == " + UserMessage.TYPE_ERROR + " ? '/resources/error.png' "
                + ": " + variable + ".messageType == " + UserMessage.TYPE_WARNING + " ? '/resources/warning.png' "
                + ": " + variable + ".messageType == " + UserMessage.TYPE_SUCCESS + " ? '/resources/success.png' "
                + ": '/resources/NAIcon.gif'}", String.class));


        column1.getChildren().add(img);


        column1.setValueExpression("styleClass",
                exFactory.createValueExpression(elContext,
                "#{" + variable + ".messageType == " + UserMessage.TYPE_ERROR + " ? 'usr-msg-error' "
                + ": " + variable + ".messageType == " + UserMessage.TYPE_WARNING + " ? 'usr-msg-warning' "
                + ": " + variable + ".messageType == " + UserMessage.TYPE_SUCCESS + " ? 'usr-msg-success' "
                + ": 'usr-msg-info'}", String.class));


        UIColumn column2 = new UIColumn();
        column2.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "col2");

        HtmlOutputLabel label1 = new HtmlOutputLabel();
        label1.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "lbl1");
        label1.setValueExpression(
                "value",
                exFactory.createValueExpression(
                elContext,
                "#{" + variable + ".messageTitleTranslated}",
                String.class));

        column2.setValueExpression("styleClass",
                exFactory.createValueExpression(elContext,
                "#{" + variable + ".messageType == " + UserMessage.TYPE_ERROR + " ? 'errorMsg' "
                + ": " + variable + ".messageType == " + UserMessage.TYPE_WARNING + " ? 'warningMsg' "
                + ": " + variable + ".messageType == " + UserMessage.TYPE_SUCCESS + " ? 'successMsg' "
                + ": ''}", String.class));

        label1.setStyleClass("usrMsgTitle");

        column2.getChildren().add(label1);

        UIColumn column3 = new UIColumn();
        column3.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "clo3");

        HtmlOutputLabel label2 = new HtmlOutputLabel();
        label2.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "lbl2");
        label2.setValueExpression(
                "value",
                exFactory.createValueExpression(
                elContext,
                "#{" + variable + ".messageTextTranslated}",
                String.class));

        column3.setValueExpression("styleClass",
                exFactory.createValueExpression(elContext,
                "#{" + variable + ".messageType == " + UserMessage.TYPE_ERROR + " ? 'errorMsg' "
                + ": " + variable + ".messageType == " + UserMessage.TYPE_WARNING + " ? 'warningMsg' "
                + ": " + variable + ".messageType == " + UserMessage.TYPE_SUCCESS + " ? 'successMsg' "
                + ": ''}", String.class));

        label2.setStyleClass("usrMsgText");

        column3.getChildren().add(label2);

        UIColumn column4 = new UIColumn();
        column4.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_Column4");

        column1.setValueExpression("styleClass",
                exFactory.createValueExpression(elContext,
                "#{" + variable + ".messageType == " + UserMessage.TYPE_ERROR + " ? 'errorMsg' "
                + ": " + variable + ".messageType == " + UserMessage.TYPE_WARNING + " ? 'warningMsg' "
                + ": " + variable + ".messageType == " + UserMessage.TYPE_SUCCESS + " ? 'successMsg' "
                + ": ''}", String.class));


        Panel msgDetailsPanel = new Panel();
        msgDetailsPanel.setToggleable(true);
        msgDetailsPanel.setToggleSpeed(1000);
        msgDetailsPanel.setId("MenuBar_" + FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                + "_msgDetailsPanel");

        msgDetailsPanel.setHeader(dDService.getDD("MsgDetails", loggedUser).getLabelTranslated());

        msgDetailsPanel.setValueExpression(
                "rendered",
                exFactory.createValueExpression(
                elContext,
                "#{" + variable + ".exceptionDetails != null}",
                boolean.class));

        HtmlPanelGroup detailsHeaderPanel = new HtmlPanelGroup();
        detailsHeaderPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                + "_detailsHeaderPanel");
        detailsHeaderPanel.setStyle("width:70px");
        HtmlOutputLabel headerLbl = new HtmlOutputLabel();
        headerLbl.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_headerLbl");
        headerLbl.setValue(dDService.getDD("MsgDetails", loggedUser).getLabelTranslated());
        headerLbl.setStyle("position:relative; left:10px;");
        detailsHeaderPanel.getChildren().add(headerLbl);

        HtmlPanelGroup detailsBodyPanel = new HtmlPanelGroup();
        detailsBodyPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_detailsBodyPanel");

        HtmlPanelGrid gridPanel = new HtmlPanelGrid();
        gridPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_gridPanel");

        HtmlOutputText text = new HtmlOutputText();
        text.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_txt");
        text.setValueExpression(
                "value",
                exFactory.createValueExpression(
                elContext,
                "#{" + variable + ".exceptionDetails}",
                String.class));


        gridPanel.getChildren().add(text);
        detailsBodyPanel.getChildren().add(gridPanel);
        msgDetailsPanel.getChildren().add(detailsBodyPanel);
        column4.getChildren().add(msgDetailsPanel);
        table.getChildren().add(column1);
        table.getChildren().add(column2);
        table.getChildren().add(column3);
        table.getChildren().add(column4);
logger.trace("Returning");
        return table;
    }

    public void addMessages(UserMessage... msgs) {
        logger.debug("Entering");
        if (msgs == null) {
            logger.warn("Passed Null Messages");
            logger.debug("Returning");
            return;
        }
        List<UserMessage> tmpMsgs = new ArrayList<UserMessage>();
        Collections.addAll(tmpMsgs, msgs);
        addMessages(tmpMsgs);
        logger.debug("Returning");
    }

    /**
     * Clears the user message panel and display all the user messages in msgs
     */
    public void clearAndDisplayMessages(UserMessage... msgs) {
        clearMessages();
        addMessages(msgs);
    }

    /**
     * Sets {@link #ownerBean} {@link OBackBean#expandMessagePanel} to true if
     * there is error/warning message
     *
     * @param msgs
     */
    public void addMessages(List<UserMessage> msgs) {
        logger.debug("Entering");
        for (UserMessage userMessage : msgs) {
            if (!messages.contains(userMessage)) {
                messages.add(userMessage);
            }
        }
        for (UserMessage msg : messages) {
            if (msg.getMessageType() == null || msg.getMessageType() != UserMessage.TYPE_SUCCESS) {
                panelC.setCollapsed(false);
                if(ownerBean.getCollapsiblePanel() !=null)
                    ownerBean.getCollapsiblePanel().setCollapsed(false);
                break;
            } else {
                panelC.setCollapsed(true);
                ownerBean.getCollapsiblePanel().setCollapsed(true);
            }
        }
        updateHeaderLabel();
        logger.debug("Returning");
    }

    /**
     * Clear the user message panel and display all the user messages in msgs
     */
    public void clearAndDisplayMessages(List<UserMessage> msgs) {
        clearMessages();
        addMessages(msgs);
    }

    public void clearMessages() {
        messages.clear();
        updateHeaderLabel();
    }
    HtmlOutputLabel headerLabel = new HtmlOutputLabel();

    public HtmlOutputLabel getHeaderLabel() {
        return headerLabel;
    }

    public void setHeaderLabel(HtmlOutputLabel headerLabel) {
        this.headerLabel = headerLabel;
    }

    public void addMessages(OFunctionResult oFR) {
        logger.debug("Entering");
        for (int i = 0; i < oFR.getErrors().size(); i++) {
            oFR.getErrors().get(i).setMessageType(UserMessage.TYPE_ERROR);
        }
        for (int i = 0; i < oFR.getWarnings().size(); i++) {
            oFR.getWarnings().get(i).setMessageType(UserMessage.TYPE_WARNING);
        }
        for (int i = 0; i < oFR.getSuccesses().size(); i++) {
            oFR.getSuccesses().get(i).setMessageType(UserMessage.TYPE_SUCCESS);
        }
        addMessages(oFR.getErrors());
        addMessages(oFR.getWarnings());
        addMessages(oFR.getSuccesses());
        logger.debug("Returning");
    }

    /**
     * Clear the user message panel and display all the user messages in oFR
     */
    public void clearAndDisplayMessages(OFunctionResult oFR) {
        clearMessages();
        addMessages(oFR);
    }

    public void addErrorMessage(UserMessage errMsg) {
        OFunctionResult oFR = new OFunctionResult();
        oFR.addError(errMsg);
        addMessages(oFR);
    }

    public void addErrorMessage(UserMessage errMsg, Exception ex) {
        OFunctionResult oFR = new OFunctionResult();
        oFR.addError(errMsg, ex);
        addMessages(oFR);
    }

    public void addErrorMessage(UserMessage errMsg, Exception ex, boolean clearFirst) {
        if (clearFirst) {
            clearMessages();
        }
        OFunctionResult oFR = new OFunctionResult();
        oFR.addError(errMsg, ex);
        addMessages(oFR);
    }
}
