/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntityServiceLocal;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.filter.FilterDDRequiredException;
import com.unitedofoq.fabs.core.filter.OFilterServiceLocal;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.i18n.Translation;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import com.unitedofoq.fabs.core.uiframework.portal.ScreenRenderInfo;
import com.unitedofoq.fabs.core.uiframework.screen.FormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.MultiLevelScreen;
import com.unitedofoq.fabs.core.uiframework.screen.MultiLevelScreenLevel;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenFilter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.el.ELContext;
import javax.persistence.OneToMany;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nkhalil
 */
public class LevelsTree {
final static Logger logger = LoggerFactory.getLogger(LevelsTree.class);
    private List<MultiLevelScreenLevel> levels;
    private OUser loggedUser;
    private SingleEntityBean multiLevelBean;
    private EntitySetupServiceRemote entitySetupService;
    private OFilterServiceLocal filterService;
    private OEntityManagerRemote oem;    
    private MultiLevelScreen levelScreen;
    //  list of objects that are used in the TreeNodes
    private List<LevelNode> nodeObjects;
    // object reference to expand node
    private LevelNode selectedLevelObject = null;
    private LevelNode root;
    private UIFrameworkServiceRemote uiFrameWorkFacade;
    private EntityServiceLocal entityServiceLocal;
    //private OIMageServiceLocal imageService;
    private ELContext elContext = null;

    public LevelNode getRoot() {
        return root;
    }

    public void setRoot(LevelNode root) {
        this.root = root;
    }

    public LevelsTree(MultiLevelScreen levelScreen, OUser loggedUser, SingleEntityBean multiLevelBean,
            EntitySetupServiceRemote entitySetupService,
            OEntityManagerRemote oem, EntityServiceLocal entityServiceLocal,
            UIFrameworkServiceRemote uiFrameWorkFacade, OFilterServiceLocal filterService) {
        this.levelScreen = levelScreen;
        this.loggedUser = loggedUser;
        this.multiLevelBean = multiLevelBean;
        this.entitySetupService = entitySetupService;
        this.oem = oem;
        this.uiFrameWorkFacade = uiFrameWorkFacade;
        this.entityServiceLocal = entityServiceLocal;
        this.filterService = filterService;
        elContext = multiLevelBean.getElContext();
        nodeObjects = new ArrayList<LevelNode>();
    }

    public void addChildren(List<String> additionalConditions, LevelNode levelNode) {
        logger.debug("Entering");
        if (levelNode.isLoadedBefore()) {
            logger.debug("Returning");
            return;
        }
        try {
            //It is the first level.
            levels = uiFrameWorkFacade.getTreeLevels(levelScreen.getDbid(), loggedUser);
            if (levelNode.getScreenLevel() == null) {
                //Iterate screen levels and render the level of order 0 only
                for (MultiLevelScreenLevel multiLevelScreenLevel : levels) {
                    if (multiLevelScreenLevel.getLevelOrder() == 0) {
                        List<String> conditions = new ArrayList<String>();
                        /* 1- "multiLevelBean" field is a SingleEntityScreen so we can use the functionalities provided
                         * in SingleEntityBean like "getCurrentScreen()".
                         * 2- When we are using lookup tree "getCurrentScreenInput()" is called twice one for the screen
                         * that has the lookup button and the tree screen.
                         * 3- Therefore we need to check the "getCurrentScreenInput()" belongs to which screen otherwise
                         * the lookup screen conditions will be wrong and will not run correctly.
                         */
                        if (multiLevelBean.getCurrentScreen() instanceof MultiLevelScreen &&
                                multiLevelBean.getCurrentScreenInput() != null &&
                                multiLevelBean.getCurrentScreenInput().getOdataTypeDBID() != 0 &&
                                !"VoidDT".equals(multiLevelBean.getCurrentScreenInput().getOdataTypeName()) &&
                                null != multiLevelBean.getCurrentScreenInput().getOdataTypeName()) {
                            conditions.add(multiLevelBean.getCurrentScreenInput().
                                    getAttributesMapping().get(0).
                                    getFieldExpression() + ".dbid = "
                                    + ((BaseEntity) multiLevelBean.getDataMessage().getData().get(0)).getDbid());

                        }if (multiLevelBean.getCurrentScreen() instanceof MultiLevelScreen &&
                               ((MultiLevelBean) multiLevelBean).getCurrentScreen().getScreenFilter() != null) {
                            List<String> s = applyScreenFltr(
                                    multiLevelBean.getCurrentScreen().getScreenFilter());
                            if(null != s && !s.isEmpty())
                                conditions.addAll(s);
                        }
                        if(additionalConditions != null && !additionalConditions.isEmpty())
                            conditions.addAll(additionalConditions);

                        if (multiLevelScreenLevel.getParentOactOnEntity() != null) {
                            //It is self relation
                            //Get the child field
                            Field nodelFLD = BaseEntity.getClassField(
                                    Class.forName(multiLevelScreenLevel.getActOnEntity().getEntityClassPath()),
                                    multiLevelScreenLevel.getFldExpInParentEntity());
                            if (nodelFLD.getAnnotation(OneToMany.class) != null) {
                                String mappedByFld = nodelFLD.getAnnotation(OneToMany.class).mappedBy();
                                conditions.add(mappedByFld + " is null");
                            }

                        }
                        //Get the data of the first level
                        Field field;//= null;
                        List<ExpressionFieldInfo> expressionFieldInfos =
                                BaseEntity.parseFieldExpression(
                                Class.forName(multiLevelScreenLevel.getActOnEntity().getEntityClassPath()),
                                multiLevelScreenLevel.getDisplayedField());
                        field = expressionFieldInfos.get(expressionFieldInfos.size() - 1).field;
                        String fldExp = (field.getAnnotation(Translation.class) != null)
                                ? field.getAnnotation(Translation.class).originalField() : field.getName();
                        List<String> orderBy = null;
                        if (expressionFieldInfos.size() == 1) {
                            orderBy = new ArrayList<String>();
                            orderBy.add(fldExp);
                        }
                        OFunctionResult dataLoadingUEConditions = uiFrameWorkFacade.getScreenDataLoadingUEConditions
                                (levelScreen, null, conditions, multiLevelBean.getDataMessage(), loggedUser);
                        
                        if (dataLoadingUEConditions.getErrors().isEmpty()) {
                            conditions.clear();
                            conditions.addAll((List) dataLoadingUEConditions.getReturnedDataMessage().getData().get(0));
                        }
                        List<BaseEntity> levelData = uiFrameWorkFacade.getLevelEntites(
                                multiLevelScreenLevel.getActOnEntity().getEntityClassName(),
                                conditions, Collections.singletonList(multiLevelScreenLevel.getDisplayedField()),
                                orderBy,
                                loggedUser);
                        //Create node for each item in the list
                        boolean firstNodeNotClicked = false;
                        for (BaseEntity nodeEntity : levelData) {
                            LevelNode childObject = new LevelNode("folder",
                                    (String) BaseEntity.getValueFromEntity(
                                    nodeEntity, multiLevelScreenLevel.getDisplayedField()), root);
                            
                            nodeObjects.add(childObject);
                            if (multiLevelScreenLevel.getLevelImage() != null) {
                                childObject.setImage(multiLevelScreenLevel.getLevelImage().getImage());
                            }
                            childObject.setParentField(multiLevelScreenLevel.getParentLevelField());
                            childObject.setNodeBaseEntity(nodeEntity);

                            childObject.setLevelOrder(multiLevelScreenLevel.getLevelOrder());


                            childObject.setScreenLevel(multiLevelScreenLevel);

                            hasChildren(childObject);
                            if (multiLevelScreenLevel.isC2a()
                                    && !firstNodeNotClicked) {
                                sendData(childObject);
                                firstNodeNotClicked = true;
                            }
                        }
                        break;
                    }
                }
            } else {//It is not the root
                //Remove the dummy element
                levelNode.getChildren().clear();
                //Get the recursive data, if any
                //Get Parent Entity   
                
                List<String> conditions = null;
                OFunctionResult dataLoadingUEConditions = uiFrameWorkFacade.
                        getScreenDataLoadingUEConditions(levelScreen, levelNode.getScreenLevel().getParentOactOnEntity()
                                , conditions, multiLevelBean.getDataMessage(), loggedUser);
                if (dataLoadingUEConditions.getErrors().isEmpty()
                        && ((List) dataLoadingUEConditions.getReturnedDataMessage().getData().get(0)).size() != 0) {
                    conditions = new ArrayList<String>();
                    conditions.addAll((List) dataLoadingUEConditions.getReturnedDataMessage().getData().get(0));
                }
                
                if (levelNode.getScreenLevel().getParentOactOnEntity() != null
                        && levelNode.getScreenLevel().getParentOactOnEntity().equals(
                        levelNode.getScreenLevel().getActOnEntity())) {
                    List<BaseEntity> childrenEntitys = uiFrameWorkFacade.getSelfRecursiveLevelEntityChildren(
                            levelNode.getNodeBaseEntity(),conditions,
                            levelNode.getScreenLevel().getActOnEntity(),
                            Collections.singletonList(levelNode.getScreenLevel().getFldExpInParentEntity()), loggedUser);
                    
                    if (childrenEntitys != null) {
                        for (BaseEntity child : childrenEntitys) {
                            LevelNode childObject = new LevelNode("folder", (String) BaseEntity.getValueFromEntity(
                                    child, levelNode.getScreenLevel().getDisplayedField()),
                                    levelNode);
                            nodeObjects.add(childObject);
                            if (levelNode.getScreenLevel().getLevelImage() != null) {
                                childObject.setImage(levelNode.getScreenLevel().getLevelImage().getImage());
                            }
                            childObject.setScreenLevel(levelNode.getScreenLevel());
                            childObject.setLevelOrder(levelNode.getScreenLevel().getLevelOrder());
                            childObject.setParentField(levelNode.getScreenLevel().getParentLevelField());
                            childObject.setNodeBaseEntity(child);
                            hasChildren(childObject);
                        }
                    }
                }
                //Get other level data
                for (MultiLevelScreenLevel multiLevelScreenLevel : levels) {
                    if (multiLevelScreenLevel.getLevelOrder() == levelNode.getLevelOrder() + 1) {
                        List<BaseEntity> childrenEntitys = uiFrameWorkFacade.getLevelEntityChildren(
                                levelNode.getNodeBaseEntity(),conditions,
                                multiLevelScreenLevel.getParentOactOnEntity(),
                                multiLevelScreenLevel.getActOnEntity(),
                                Collections.singletonList(multiLevelScreenLevel.getFldExpInParentEntity()),
                                multiLevelScreenLevel.getDisplayedField(), loggedUser);
                        for (BaseEntity child : childrenEntitys) {
                            LevelNode childObject = new LevelNode("folder", (String) BaseEntity.getValueFromEntity(
                                    child, multiLevelScreenLevel.getDisplayedField()),
                                    levelNode);
                            nodeObjects.add(childObject);
                            if (multiLevelScreenLevel.getLevelImage() != null) {
                                childObject.setImage(multiLevelScreenLevel.getLevelImage().getImage());
                            }
                            childObject.setScreenLevel(multiLevelScreenLevel);
                            childObject.setLevelOrder(multiLevelScreenLevel.getLevelOrder());
                            childObject.setParentField(multiLevelScreenLevel.getParentLevelField());
                            childObject.setNodeBaseEntity(child);
                            hasChildren(childObject);
                        }
                        break;
                    }

                }


            }
            levelNode.setLoadedBefore(true);
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        finally{
            logger.debug("Returning");
        }
    }
    public LevelNode getSelectedNode(TreeNode treeNode){
        logger.debug("Entering");
        for (LevelNode levelNode : nodeObjects) {
          try{
            if (levelNode.getId() == (((LevelNode)(treeNode.getChildren().get(0)).getParent()).getId())
                        && levelNode.getParent().equals(treeNode.getParent())) {
                logger.debug("Returning with: {}",levelNode.getId());
                return levelNode;
    }
          }catch(Exception ex){
            continue;
          }
        }
        logger.debug("Returning with Null");
        return null;
    }
    public void deleteNodeActionListener(TreeNode treeNode) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            for (LevelNode levelNode : nodeObjects) {
                if (levelNode.getData().equals(treeNode.getData())
                        && levelNode.getParent().equals(treeNode.getParent())) {
                    oem.getEM(loggedUser);
                    OEntityDTO selectedNodeOEntity = entitySetupService.
                            getRoutingEntityDTO(levelNode.getScreenLevel().
                            getDbid(), loggedUser);                            
                    OEntityActionDTO deleteAction = entitySetupService.
                            getOEntityDTOActionDTO(selectedNodeOEntity.getDbid(),
                            OEntityActionDTO.ATDBID_DELETE, loggedUser);                            
                    ODataMessage entityDataMessage = entityServiceLocal.constructEntityODataMessage(
                            levelNode.getNodeBaseEntity(), oem.getSystemUser(loggedUser));
                    oFR.append(entitySetupService.executeAction(
                            deleteAction, entityDataMessage, new OFunctionParms(), loggedUser));
                    treeNode.getChildren().clear();
                    treeNode.getParent().getChildren().remove(treeNode);
                    treeNode.setParent(null);
                    treeNode = null;                   

                    multiLevelBean.messagesPanel.clearAndDisplayMessages(oFR);
                }

            }
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);      
        }
        finally{
            logger.debug("Returning");
        }
    }

    public void newNodeActionListener(TreeNode treeNode) {
        try {
            for (LevelNode levelNode : nodeObjects) {
                if (levelNode.getData().equals(treeNode.getData())
                        && levelNode.getParent().equals(treeNode.getParent())) {
                    OEntityDTO selectedNodeOEntity = entitySetupService.
                            getRoutingEntityDTO(levelNode.getScreenLevel().
                            getDbid(), loggedUser);                    
                    FormScreen formScreen = (FormScreen) uiFrameWorkFacade.
                            getBasicDetailScreenByOEntityDTO(selectedNodeOEntity,
                            oem.getSystemUser(loggedUser));
                    String screenInstId;// = "";
                    screenInstId = OBackBean.generateScreenInstId(formScreen.getName());
                    SessionManager.setScreenSessionAttribute(
                            screenInstId, MultiLevelBean.MODE_VAR, String.valueOf(OScreen.SM_ADD));
                    multiLevelBean.openScreen(formScreen, screenInstId);
                    break;
                }
            }

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
    }

    public void editNodeActionListener(TreeNode treeNode) {
        logger.debug("Entering");
        try {
            for (LevelNode levelNode : nodeObjects) {
                if (levelNode.getData().equals(treeNode.getData())
                        && levelNode.getParent().equals(treeNode.getParent())) {
                    oem.getEM(loggedUser);
                     OEntityDTO selectedNodeOEntity = entitySetupService.
                            getRoutingEntityDTO(levelNode.getScreenLevel().
                            getDbid(), loggedUser);                    
                    List data = new ArrayList();
                    data.add(levelNode.getNodeBaseEntity().getDbid());

                    ODataType dataType = multiLevelBean.dataTypeService.loadODataType("DBID", oem.getSystemUser(loggedUser));

                    ODataMessage message = new ODataMessage(dataType, data);

                    FormScreen formScreen = (FormScreen) uiFrameWorkFacade.
                            getBasicDetailScreenByOEntityDTO(selectedNodeOEntity, 
                            oem.getSystemUser(loggedUser));
                    String screenInstId = OBackBean.generateScreenInstId(formScreen.getName());

                    SessionManager.setScreenSessionAttribute(screenInstId,
                            OPortalUtil.MESSAGE, message);
                    SessionManager.setScreenSessionAttribute(
                            screenInstId,
                            MultiLevelBean.MODE_VAR,
                            OScreen.SM_EDIT);

                    multiLevelBean.openScreenCheckFilter(formScreen, message, screenInstId);
                }

            }

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
        }

    }
    public void nodeActionListener(NodeSelectEvent event) {
        logger.debug("Entering");
        try {
            oem.getEM(loggedUser);
            LevelNode levelNode = ((LevelNode)event.getTreeNode());
            if (null == levelNode || !levelNode.getScreenLevel().isC2a()) {
                logger.trace("levelNode equals null");
                logger.debug("Returning");
                return;
            }
            sendData(levelNode);


        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
        }

    }

    private void sendData(LevelNode levelNode) {
        logger.trace("Entering");
        try {
            oem.getEM(loggedUser);
            List data = new ArrayList();
            data.add(levelNode.getNodeBaseEntity());
            ODataType dataType = multiLevelBean.dataTypeService.loadODataType(
                    levelNode.getScreenLevel().getActOnEntity().getEntityClassName()
                    , oem.getSystemUser(loggedUser));
            
            ODataMessage message = new ODataMessage(dataType, data);
           if (message != null && multiLevelBean.toBeRenderScreens != null) {
                for (ScreenRenderInfo screenRenderInfo : multiLevelBean.toBeRenderScreens) {
                    OPortalUtil.addPassedScreenInputDataMessage(screenRenderInfo.screenInstId, message);
                     OPortalUtil.renderScreen(levelScreen,screenRenderInfo.screenInstId, message);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.trace("Returning");
        }
    }
    
        private List<String> applyScreenFltr(ScreenFilter screenFilter) {
            logger.trace("Entering");
        //Apply Screen Filter Initialization
        if (screenFilter != null &&
            !screenFilter.isInActive() &&
            screenFilter.isInitialize()) {

                 List<String> filterConditions = new ArrayList<String>();
                try {
                    filterConditions = filterService.getFilterConditions(screenFilter, null, loggedUser);
                } catch (FilterDDRequiredException ex) {
                    logger.error("Exception thrown",ex);
                }
                logger.trace("Returning");
                 return filterConditions;
        }
        logger.trace("Returning with Null");
       return null;
    }
    public void nodeExpandListener(NodeExpandEvent event) {
        logger.debug("Entering");
        try {
            LevelNode levelNode = (LevelNode) event.getTreeNode();
            if (!levelNode.getChildren().isEmpty()
                    && levelNode.getChildren().get(0).getData().toString().equals("")) {
                levelNode.getChildren().remove(0);
            }
            addChildren(null, levelNode);

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
        }

    }
    private void hasChildren(LevelNode levelNode) {
        logger.trace("Entering");
        if (null == levelNode.getData()) {
            logger.trace("Returning as level Node Data equals Null");
            return;
        }
        try {

            int currentLevelOrder = levelNode.getLevelOrder() + 1;
            //Is it recursive node
            if (levelNode.getScreenLevel().getParentOactOnEntity() != null
                    && levelNode.getScreenLevel().getParentOactOnEntity().equals(
                    levelNode.getScreenLevel().getActOnEntity())) {
                List<Field> relatedFlds = BaseEntity.getXToOneRelatedFields(levelNode.getNodeBaseEntity().getClass());
                String parentFldExp = null;
                for (Field field : relatedFlds) {
                    if (field.getType().getSimpleName().equals(
                            levelNode.getNodeBaseEntity().getClassName())) {
                        parentFldExp = field.getName();
                        break;
                    }
                }
                if (parentFldExp == null) {
                    logger.trace("Returning as parent field equals Null");
                    return;
                }
                long parentEntityDBID = levelNode.getNodeBaseEntity().getDbid();
                String childEntityName = levelNode.getNodeBaseEntity().getClassName();
                boolean hasChildren = uiFrameWorkFacade.hasEntityHasChildren(childEntityName,
                        parentEntityDBID, parentFldExp, loggedUser);
                LevelNode node;
                if (hasChildren) {
                    node = new LevelNode("folder", "", levelNode);

                } else {
                    node = new LevelNode("document", "", levelNode);
                }

            }
            for (MultiLevelScreenLevel multiScreenLevel : levels) {
                if (multiScreenLevel.getLevelOrder() == currentLevelOrder) {
                    String parentFldExp = (String) BaseEntity.getParentEntityFieldName(
                            multiScreenLevel.getActOnEntity().getEntityClassPath()).get(0);
                    long parentEntityDBID = levelNode.getNodeBaseEntity().getDbid();
                    String childEntityName = multiScreenLevel.getActOnEntity().getEntityClassName();
                    boolean hasChildren = uiFrameWorkFacade.hasEntityHasChildren(childEntityName,
                            parentEntityDBID, parentFldExp, loggedUser);
                    LevelNode node;
                    if (hasChildren) {
                        node = new LevelNode("folder", "", levelNode);

                    } else {
                        node = new LevelNode("document", "", levelNode);
                    }
                }
            }



        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
    }
}
