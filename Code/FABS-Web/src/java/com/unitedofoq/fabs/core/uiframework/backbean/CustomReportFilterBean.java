/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.content.AttachmentDTO;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDLookupType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.report.customReports.CustomReport;
import com.unitedofoq.fabs.core.report.customReports.CustomReportDynamicFilter;
import com.unitedofoq.fabs.core.report.customReports.CustRepFilterFld;
import com.unitedofoq.fabs.core.report.customReports.CustomReportParameter;
import com.unitedofoq.fabs.core.report.customReports.CustomReportServiceLocal;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.panel.Panel;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;

/**
 *
 * @author mostafa
 */
@ManagedBean(name = "CustomReportFilterBean")
@ViewScoped
public class CustomReportFilterBean extends FormBean {

    final static Logger logger = LoggerFactory.getLogger(CustomReportFilterBean.class);
    private String reportFilterDBID = "";
    private OFunctionResult oFR = new OFunctionResult();
    private List<CustRepFilterFld> reportFilterFields = new ArrayList<CustRepFilterFld>();
    private List<CustomReportParameter> reportParameters = new ArrayList<CustomReportParameter>();
    private HashMap<String, Object> valuesMap = new HashMap<String, Object>();
    private HashMap<String, Object> parametersMap = new HashMap<String, Object>();
    private CustomReport customReport;
    private CustRepFilterFld customReportFilterField;
    private CustomReportDynamicFilter dynamicFilter;
    private AttachmentDTO entityAttachment;
    private HashMap selectedData = new HashMap();
    private HashMap<String,String> fieldNameById = new HashMap<String,String>();

    protected UIFrameworkServiceRemote uiFrameWorkFacade = OEJB.lookup(UIFrameworkServiceRemote.class);
    private boolean fitPage = false;
    
    private CustomReportServiceLocal customReportService = OEJB.lookup(CustomReportServiceLocal.class);;
    
    public HashMap getSelectedData() {
        return selectedData;
    }

    public void setSelectedData(HashMap selectedData) {
        this.selectedData = selectedData;
    }

    private boolean revertLanguage;

    public boolean isRevertLanguage() {
        return revertLanguage;
    }

    public void setRevertLanguage(boolean revertLanguage) {
        this.revertLanguage = revertLanguage;
    }

    private boolean applyingFilter = true;

    public boolean isApplyingFilter() {
        return applyingFilter;
    }

    public void setApplyingFilter(boolean applyingFilter) {
        this.applyingFilter = applyingFilter;
    }

    @Override
    protected String getBeanName() {
        return "CustomReportFilterBean";
    }

    public List<CustomReportParameter> getReportParameters() {
        return reportParameters;
    }

    public void setReportParameters(List<CustomReportParameter> reportParameters) {
        this.reportParameters = reportParameters;
    }

    public HashMap<String, Object> getParametersMap() {
        return parametersMap;
    }

    public void setParametersMap(HashMap<String, Object> parametersMap) {
        this.parametersMap = parametersMap;
    }

    public boolean isFitPage() {
        return fitPage;
    }

    public void setFitPage(boolean fitPage) {
        this.fitPage = fitPage;
    }

    @Override
    public void init() {
        logger.debug("Entering");
        try {
            super.init();
            if (dataMessage != null && dataMessage.getData() != null
                    && !dataMessage.getData().isEmpty()) {

                reportParameters = ((CustomReport) dataMessage.getData().get(1)).getCustomReportParameters();
                customReport = (CustomReport) dataMessage.getData().get(1);
                if (customReport.getCustomReportFilter() != null) {
                    reportFilterDBID = String.valueOf(customReport.getCustomReportFilter().getDbid());
                    reportFilterFields = oem.loadEntityList(CustRepFilterFld.class.getSimpleName(), Collections.singletonList("customReportFilter.dbid = " + customReport.getCustomReportFilter().getDbid()), null, null, loggedUser);
                }
                entityAttachment = (AttachmentDTO) dataMessage.getData().get(2);
                mode = OScreen.SM_EDIT;
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
            messagesPanel.clearAndDisplayMessages(oFR);
        }
        logger.debug("Returning");
    }

    @Override
    public HtmlPanelGrid buildInnerScreen() {
        logger.debug("Entering");
        htmlPanelGrid = new HtmlPanelGrid();
        htmlPanelGrid.setStyleClass("mainTable");
        htmlPanelGrid.setId(getFacesContext().getViewRoot().createUniqueId() + portletId + "CustomRepFiltGeneralPanel");
        htmlPanelGrid.setCellpadding("0px");
        htmlPanelGrid.setCellspacing("0px");
        htmlPanelGrid.setStyleClass("mainTable");
        htmlPanelGrid.getChildren().add(messagesPanel.createMessagePanel());

        SelectBooleanCheckbox revertLanguageComp = new SelectBooleanCheckbox();
        revertLanguageComp.setId("RevertLang");
        revertLanguageComp.setStyleClass("labelTxt");
        revertLanguageComp.setValueExpression("value",
                exFactory.createValueExpression(
                        elContext,
                        "#{CustomReportFilterBean.revertLanguage}",
                        Boolean.class));
        revertLanguageComp.setItemLabel(ddService.getDD("custom_report_revert_lang", loggedUser).getHeaderTranslated());
        htmlPanelGrid.getChildren().add(revertLanguageComp);
        try {
            logger.trace("Building Custom Report Filter Fields");
            HtmlPanelGrid middlePanel = new HtmlPanelGrid();
            middlePanel.setId(getFacesContext().getViewRoot().createUniqueId() + "MDPanel" + getCurrentScreen().getName());
            middlePanel.setColumns(3);
            middlePanel.setCellpadding("0");
            middlePanel.setCellspacing("2");
            middlePanel.setStyleClass("ctrlTable");
            int screenFieldIndex = 0;
            Collections.sort(reportFilterFields, new Comparator<CustRepFilterFld>() {
                @Override
                public int compare(CustRepFilterFld obj1, CustRepFilterFld obj2) {
                    return obj1.getSortIndex() - obj2.getSortIndex();
                }
            });

            Panel collapsible = new Panel();
            collapsible.setToggleable(true);
            collapsible.setCollapsed(reportFilterFields.isEmpty());
            collapsible.setToggleSpeed(500);
            collapsible.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_Grop"
                    + customReport.getDbid());
            collapsible.setStyleClass("sectionCollapsible");
            String filterGroupHeader = ddService.getDD("custom_report_filter_group_header", loggedUser).getHeaderTranslated();
            collapsible.setHeader(filterGroupHeader);

            if (customReport.isViewEntityFilter()) {

                HtmlPanelGrid middlePanelForEntityFilter = new HtmlPanelGrid();
                middlePanelForEntityFilter.setId(getFacesContext().getViewRoot().createUniqueId() + "MDPanel" + getCurrentScreen().getName());
                middlePanelForEntityFilter.setColumns(3);
                middlePanelForEntityFilter.setCellpadding("0");
                middlePanelForEntityFilter.setCellspacing("2");
                middlePanelForEntityFilter.setStyleClass("ctrlTable");
                Panel panelForEntityFilter = new Panel();
                panelForEntityFilter.setToggleable(true);
                //ElRashed Requirementnever to be collapsed
                panelForEntityFilter.setCollapsed(false);
                panelForEntityFilter.setToggleSpeed(500);
                panelForEntityFilter.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_Grop"
                        + customReport.getDbid());
                panelForEntityFilter.setStyleClass("sectionCollapsible");
                panelForEntityFilter.setHeader("Custom Report Dynamic Filter");
                customReportFilterField = generateEntityFilterLookup();
                if (customReportFilterField != null) {
                    createCustomReportFilterFieldControl(middlePanelForEntityFilter, customReportFilterField, screenFieldIndex);
                    generateFieldOperator(customReportFilterField, middlePanelForEntityFilter, screenFieldIndex);
                    panelForEntityFilter.getChildren().add(middlePanelForEntityFilter);
                    htmlPanelGrid.getChildren().add(panelForEntityFilter);
                }
            }

            for (CustRepFilterFld field : reportFilterFields) {
                screenFieldIndex++;
                createCustomReportFilterFieldControl(middlePanel, field, screenFieldIndex);
                generateFieldOperator(field, middlePanel, screenFieldIndex);
            }
            collapsible.getChildren().add(middlePanel);
            htmlPanelGrid.getChildren().add(collapsible);
            logger.trace("Building Custom Report Parameters");
            middlePanel = new HtmlPanelGrid();
            middlePanel.setId(getFacesContext().getViewRoot().createUniqueId() + "MDPanel" + getCurrentScreen().getName());
            middlePanel.setColumns(2);
            middlePanel.setCellpadding("0");
            middlePanel.setCellspacing("2");
            screenFieldIndex = 0;
            Collections.sort(reportParameters, new Comparator<CustomReportParameter>() {
                @Override
                public int compare(CustomReportParameter obj1, CustomReportParameter obj2) {
                    return obj1.getSortIndex() - obj2.getSortIndex();
                }
            });

            collapsible = new Panel();
            collapsible.setToggleable(true);
            collapsible.setCollapsed(reportParameters.isEmpty());
            collapsible.setToggleSpeed(500);
            collapsible.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_Grop"
                    + customReport.getDbid());

            String paramGroupHeader = ddService.getDD("custom_report_params_group_header", loggedUser).getHeaderTranslated();
            collapsible.setHeader(paramGroupHeader);

            for (CustomReportParameter parameter : reportParameters) {
                screenFieldIndex++;
                createCustomReportParameterControl(middlePanel, parameter, screenFieldIndex);
            }
            collapsible.getChildren().add(middlePanel);
            htmlPanelGrid.getChildren().add(collapsible);
            // </editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Run Button">
            HtmlPanelGrid buttonPanel = new HtmlPanelGrid();
            buttonPanel.setId(getFacesContext().getViewRoot().createUniqueId() + "_buttonPanel");
            buttonPanel.setColumns(1);

            CommandButton runReport = new CommandButton();
            runReport = createButton(buttonPanel, "run", "runReport", null, getBeanName());
            runReport.setStyleClass("report_btn");
            runReport.setAjax(true);
            runReport.setUpdate("frmScrForm:middlePanel");
            //</editor-fold>

            htmlPanelGrid.getChildren().add(buttonPanel);

            SelectBooleanCheckbox fitPageComp = new SelectBooleanCheckbox();
            fitPageComp.setId("fitPage");
            fitPageComp.setStyleClass("labelTxt");
            fitPageComp.setValueExpression("value",
                    exFactory.createValueExpression(
                            elContext,
                            "#{CustomReportFilterBean.fitPage}",
                            Boolean.class));
            fitPageComp.setItemLabel(ddService.getDD("custom_report_fit_page", loggedUser).getHeaderTranslated());
            htmlPanelGrid.getChildren().add(fitPageComp);

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        if (getScreenHeight() == null || getScreenHeight().equals("")) {
            onloadJSStr += "top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');";
        } else {
            onloadJSStr += "top.FABS.Portlet.updateIframeSizeWithHeight({iframeName:'" + getPortletInsId() + "Frame'"
                    + ",screenHeight:'" + getScreenHeight() + "'});";
        }
        logger.debug("Returning");
        return htmlPanelGrid;
    }

    public CustRepFilterFld generateEntityFilterLookup() {

        try {
            customReportFilterField = (CustRepFilterFld) oem.loadEntity(CustRepFilterFld.class.getSimpleName(), Collections.singletonList(
                            "fieldName='customreportfilterfieldforlookup'"), null, loggedUser);
            if (customReportFilterField != null) {
                customReportFilterField.setVal1Dd(customReportFilterField.getVal1Dd());
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }

        return customReportFilterField;
    }

    public void generateFieldOperator(CustRepFilterFld field, HtmlPanelGrid middlePanel, int screenFieldIndex) {

        if (field.getOperator().toLowerCase().equals("between") || field.getOperator().toLowerCase().equals("range")) {
            createControl(middlePanel, field, ++screenFieldIndex, false);
        } else {
            HtmlOutputLabel operatorLabel = new HtmlOutputLabel();
            operatorLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "operator");
            operatorLabel.setStyleClass("labelTxt");
            operatorLabel.setValue("");
            middlePanel.getChildren().add(operatorLabel);
        }
    }

    private void createCustomReportFilterFieldControl(HtmlPanelGrid panel,
            CustRepFilterFld filterField, int screenFieldIndex) {
        logger.trace("Entering");
        try {
            // create label for field name
            HtmlPanelGrid labelGrid = createLabel(filterField.getFieldName() + screenFieldIndex, filterField);
            panel.getChildren().add(labelGrid);

            // create label for operator
            HtmlOutputLabel operatorLabel = new HtmlOutputLabel();
            operatorLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "operator");
            operatorLabel.setStyleClass("labelOperator");
            if (filterField.getOperator().equals("=")) {
                operatorLabel.setValue(" IS ");
            } else if (filterField.getOperator().equalsIgnoreCase("between")
                    || filterField.getOperator().equalsIgnoreCase("range")) {
                operatorLabel.setValue("From..To");
            } else {
                operatorLabel.setValue(filterField.getOperator());
            }

            createControl(panel, filterField, screenFieldIndex, true);

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            logger.trace("Returning");
        }
    }

    private void createCustomReportParameterControl(HtmlPanelGrid panel,
            CustomReportParameter parameter, int screenFieldIndex) {
        logger.trace("Entering");
        try {
            // create label for field name
            HtmlPanelGrid labelGrid = createLabel(parameter.getParameterName() + screenFieldIndex, parameter);
            panel.getChildren().add(labelGrid);
            createControl(panel, parameter, screenFieldIndex);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            logger.trace("Returning");
        }
    }

    public boolean checkMandatoryLookupFieldValidity(boolean save, OFunctionResult oFR) {

        for (int currentField = 0; currentField < lookupMandatoryFields.size(); currentField++) {
            String lookupID = lookupMandatoryFields.get(currentField);

            if (getLookupHashMap().get(lookupID) != null && ((Lookup) getLookupHashMap().get(lookupID)).getInputText() != null && ((Lookup) getLookupHashMap().get(lookupID)).getInputText().getValue().equals("")) {
                try {

                    String label = fieldNameById.get(lookupID);

                    UserMessage message = usrMsgService.getUserMessage("LookupFieldValidation",
                            Collections.singletonList(label), loggedUser);
                    message.setMessageText(message.getMessageText() + label);
                    exit = false;
                    RequestContext.getCurrentInstance().execute("scroll(0,0)");
                    oFR.addError(message);
                } catch (Exception ex) {
                    logger.warn("Lookup Validation Message Error");
                    logger.error("Exception thrown", ex);
                }
                save = false;
            }
        }

        return save;

    }

    private void createControl(HtmlPanelGrid panel,
            CustRepFilterFld filterField, int screenFieldIndex, boolean isFirst) {
        logger.trace("Entering");
        try {
            //create control for value
            String fldExp = "";
            if (isFirst) {
                valuesMap.put(filterField.getFieldName(), null);
                fldExp = "#{" + getBeanName() + "." + "valuesMap['" + filterField.getFieldName() + "']" + "}";
            } else {
                valuesMap.put(filterField.getFieldName() + "_2", null);
                fldExp = "#{" + getBeanName() + "." + "valuesMap['" + filterField.getFieldName() + "_2']" + "}";
            }
            UIComponent component = null;
            if (filterField.getVal1Dd().getControlType().getDbid() == DD.CT_TEXTFIELD) {
                component = createTextFieldControl(20, "filterfld_fld" + screenFieldIndex, fldExp, true,
                        filterField.getVal1Dd(), screenFieldIndex, false, false, false);
            } else if (filterField.getVal1Dd().getControlType().getDbid() == DD.CT_CHECKBOX) {
                component = createCheckBoxControl("filterfld_fld" + screenFieldIndex, fldExp, true,
                        filterField.getVal1Dd(), screenFieldIndex, false);
            } else if (filterField.getVal1Dd().getControlType().getDbid() == DD.CT_CALENDAR) {
                component = createCalendarControl("filterfld_fld" + screenFieldIndex, fldExp, screenFieldIndex,
                        true, filterField.getVal1Dd(), screenFieldIndex, false);
            } else if (filterField.getVal1Dd().getControlType().getDbid() == DD.CT_DROPDOWN
                    || filterField.getVal1Dd().getControlType().getDbid() == DD.CT_LOOKUPSCREEN
                    || filterField.getVal1Dd().getControlType().getDbid() == DD.CT_LITERALDROPDOWN) {
                DD fieldDD = filterField.getVal1Dd();

//                fieldDD.setLookupFilterTranslated(fieldDD.getLookupFilter());
                HtmlPanelGrid lookupPanelGrid = new HtmlPanelGrid();
                lookupPanelGrid.setId(FacesContext.getCurrentInstance().
                        getViewRoot().createUniqueId() + "_lOOKUP");

                String lookupExp = null;
                if ((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("")
                        && fieldDD.getLookupType() == DDLookupType.DDLT_DRPDWN)
                        || (null != fieldDD.getLookupFilter() && !"".equals(fieldDD.getLookupFilter()))
                        && fieldDD.getLookupType() == DDLookupType.DDLT_LTRLDRPDN) {
                    lookupExp = "";//getLookupFldExpression(filterfields.get(i).getFieldExpression());//getDRDFldExpression(filterfields.get(i).getFieldExpression());
                } else {
                    lookupExp = getLookupFldExpression(filterField.getFieldName());
                }

                ScreenField screenField = new ScreenField();
                screenField.setDd(filterField.getVal1Dd());
                screenField.setEditable(true);
                screenField.setFieldExpression(filterField.getVal1FieldExpression());

                if (!isFirst) {
                    screenField.setSortIndex(screenFieldIndex + 60);
                } else {
                    screenField.setSortIndex(screenFieldIndex);
                }

                String lookupID = "";
                if (isFirst) {
                    lookupID = filterField.getFieldName().replace(".", "_") + "_Lookup_";
                    fldExp = getBeanName() + "." + "valuesMap['" + filterField.getFieldName() + "']";
                } else {
                    String tempStr = filterField.getFieldName() + "_2";
                    lookupID = tempStr.replace(".", "_") + "_Lookup_";
                    fldExp = getBeanName() + "." + "valuesMap['" + filterField.getFieldName() + "_2']";
                }
                fieldDD = ddService.getDD(fieldDD.getName(), loggedUser);
                if (filterField.isMandatory()) {
                    lookupMandatoryFields.add(lookupID);
                    fieldNameById.put(lookupID, filterField.getHeader());
                }

                fieldLookup = new Lookup(this,
                        getBeanName(),
                        fldExp,
                        fieldDD,
                        lookupExp,
                        null,
                        lookupID,
                        screenField,
                        false,
                        oem, filterServiceBean,
                        exFactory,
                        getFacesContext(),
                        loggedUser);

                getLookupHashMap().put(lookupID, fieldLookup);

                if ((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("")
                        && fieldDD.getLookupType() == DDLookupType.DDLT_DRPDWN)
                        || (null != fieldDD.getLookupFilter() && !"".equals(fieldDD.getLookupFilter()))
                        && fieldDD.getLookupType() == DDLookupType.DDLT_LTRLDRPDN) {
                    lookupPanelGrid.getChildren().add(fieldLookup.createDropDown(false));
                    fieldLookup.getDropDownMenu().setStyleClass("dropdown-list-LTR");
                    if (loggedUser.isRtl()) {
                        fieldLookup.getDropDownMenu().setStyleClass("dropdown-list-RTL");
                    }
                    getSelectItemsList().put(fieldLookup.getDropDownMenu().getId(),
                            fieldLookup.getSelectItems());

                } else if ((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("")
                        && fieldDD.getLookupType() == DDLookupType.DDLT_MULTISELECTIONLIST)) {
                    lookupPanelGrid.getChildren().add(fieldLookup.createDropDown(false));
                    fieldLookup.getDropDownMenu().setStyleClass("dropdown-list-LTR");
                    if (loggedUser.isRtl()) {
                        fieldLookup.getDropDownMenu().setStyleClass("dropdown-list-RTL");
                    }
                    getSelectItemsList().put(fieldLookup.getDropDownMenu().getId(),
                            fieldLookup.getSelectItems());
                } else {

                    try {
                        BaseEntity newEntity = (BaseEntity) Class.forName(entitySetupService.
                                getOScreenEntityDTO(fieldLookup.getOScreen().getDbid(), loggedUser).getEntityClassPath())
                                .newInstance();
                        newEntity.NewFieldsInstances(Collections.singletonList(screenField.getFieldExpression()));
                        if (isFirst) {
                            valuesMap.put(filterField.getFieldName(), newEntity);
                            fldExp = "#{" + getBeanName() + "." + "valuesMap['" + filterField.getFieldName() + "']" + "}";
                        } else {
                            valuesMap.put(filterField.getFieldName() + "_2", newEntity);
                            fldExp = "#{" + getBeanName() + "." + "valuesMap['" + filterField.getFieldName() + "'_2]" + "}";
                        }
                    } catch (Exception ex) {
                        logger.trace("Invalid value for type lookup");
                        logger.error("Exception thrown", ex);
                        valuesMap.put(filterField.getFieldName(), null);
                    }

                    lookupPanelGrid.getChildren().add(fieldLookup.createNonDropDown());
                    FramworkUtilities.setControlFldExp(fieldLookup.getInputText(), screenField.getFieldExpression());
                }
                component = lookupPanelGrid;
            }
            panel.getChildren().add(component);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            logger.trace("Returning");
        }
    }

    private void createControl(HtmlPanelGrid panel,
            CustomReportParameter parameter, int screenFieldIndex) {
        logger.trace("Entering");
        try {
            //create control for value
            String fldExp = "";
            boolean isMandatory = parameter.isMandatory();
            parametersMap.put(parameter.getParameterName() + "#_#" + parameter.getParameterType(), null);
            fldExp = "#{" + getBeanName() + "." + "parametersMap['" + parameter.getParameterName() + "#_#" + parameter.getParameterType() + "']" + "}";

            UIComponent component = null;
            if (parameter.getParameterDd().getControlType().getDbid() == DD.CT_TEXTFIELD) {
                component = createTextFieldControl(20, "filterfld_fld" + screenFieldIndex, fldExp, true,
                        parameter.getParameterDd(), screenFieldIndex, isMandatory, false, false);
            } else if (parameter.getParameterDd().getControlType().getDbid() == DD.CT_CHECKBOX) {
                component = createCheckBoxControl("filterfld_fld" + screenFieldIndex, fldExp, true,
                        parameter.getParameterDd(), screenFieldIndex, isMandatory);
            } else if (parameter.getParameterDd().getControlType().getDbid() == DD.CT_CALENDAR) {
                component = createCalendarControl("filterfld_fld" + screenFieldIndex, fldExp, screenFieldIndex,
                        true, parameter.getParameterDd(), screenFieldIndex, isMandatory);
            } else if (parameter.getParameterDd().getControlType().getDbid() == DD.CT_DROPDOWN
                    || parameter.getParameterDd().getControlType().getDbid() == DD.CT_LOOKUPSCREEN
                    || parameter.getParameterDd().getControlType().getDbid() == DD.CT_LITERALDROPDOWN) {
                DD fieldDD = parameter.getParameterDd();

//                fieldDD.setLookupFilterTranslated(fieldDD.getLookupFilter());
                //String lookupID = filterfields.get(i).getFieldExpression().replace(".", "_") + "_Lookup_";
                HtmlPanelGrid lookupPanelGrid = new HtmlPanelGrid();
                lookupPanelGrid.setId(FacesContext.getCurrentInstance().
                        getViewRoot().createUniqueId());

                String lookupExp = null;
                if ((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("")
                        && fieldDD.getLookupType() == DDLookupType.DDLT_DRPDWN)
                        || (null != fieldDD.getLookupFilter() && !"".equals(fieldDD.getLookupFilter()))
                        && fieldDD.getLookupType() == DDLookupType.DDLT_LTRLDRPDN) {
                    lookupExp = "";//getLookupFldExpression(filterfields.get(i).getFieldExpression());//getDRDFldExpression(filterfields.get(i).getFieldExpression());
                } else {
                    lookupExp = getLookupFldExpression(parameter.getParameterName());
                }

                ScreenField screenField = new ScreenField();
                screenField.setDd(parameter.getParameterDd());
                screenField.setEditable(true);
                screenField.setFieldExpression(parameter.getFieldEXPR());

                String lookupID = "";

                lookupID = parameter.getParameterName().replace(".", "_") + "_Lookup_";
                fldExp = getBeanName() + "." + "parametersMap['" + parameter.getParameterName() + "#_#" + parameter.getParameterType() + "']";
                fieldDD = ddService.getDD(fieldDD.getName(), loggedUser);
                if (isMandatory) {
                    lookupMandatoryFields.add(lookupID);
                    fieldNameById.put(lookupID, parameter.getHeader());
                }

                fieldLookup = new Lookup(this,
                        getBeanName(),
                        fldExp,
                        fieldDD,
                        lookupExp,
                        null,
                        lookupID,
                        screenField,
                        isMandatory,
                        oem, filterServiceBean,
                        exFactory,
                        getFacesContext(),
                        loggedUser);

                getLookupHashMap().put(lookupID, fieldLookup);

                if ((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("")
                        && fieldDD.getLookupType() == DDLookupType.DDLT_DRPDWN)
                        || (null != fieldDD.getLookupFilter() && !"".equals(fieldDD.getLookupFilter()))
                        && fieldDD.getLookupType() == DDLookupType.DDLT_LTRLDRPDN) {
                    lookupPanelGrid.getChildren().add(fieldLookup.createDropDown(false));
                    fieldLookup.getDropDownMenu().setStyleClass("dropdown-list-LTR");
                    if (loggedUser.isRtl()) {
                        fieldLookup.getDropDownMenu().setStyleClass("dropdown-list-RTL");
                    }
                    getSelectItemsList().put(fieldLookup.getDropDownMenu().getId(),
                            fieldLookup.getSelectItems());

                } else if ((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("")
                        && fieldDD.getLookupType() == DDLookupType.DDLT_MULTISELECTIONLIST)) {
                    lookupPanelGrid.getChildren().add(fieldLookup.createDropDown(false));
                    fieldLookup.getDropDownMenu().setStyleClass("dropdown-list-LTR");
                    if (loggedUser.isRtl()) {
                        fieldLookup.getDropDownMenu().setStyleClass("dropdown-list-RTL");
                    }
                    getSelectItemsList().put(fieldLookup.getDropDownMenu().getId(),
                            fieldLookup.getSelectItems());
                } else {

                    try {
                        BaseEntity newEntity = (BaseEntity) Class.forName(entitySetupService.
                                getOScreenEntityDTO(fieldLookup.getOScreen().getDbid(), loggedUser).getEntityClassPath())
                                .newInstance();
                        newEntity.NewFieldsInstances(Collections.singletonList(screenField.getFieldExpression()));
                        parametersMap.put(parameter.getParameterName() + "#_#" + parameter.getParameterType(), newEntity);
                        fldExp = "#{" + getBeanName() + "." + "parametersMap['" + parameter.getParameterName() + "#_#" + parameter.getParameterType() + "']" + "}";

                    } catch (Exception ex) {
                        logger.trace("Invalid value for type lookup");
                        logger.error("Exception thrown", ex);
                        valuesMap.put(parameter.getParameterName(), null);
                    }

                    lookupPanelGrid.getChildren().add(fieldLookup.createNonDropDown());
                    FramworkUtilities.setControlFldExp(fieldLookup.getInputText(), screenField.getFieldExpression());
                }
                component = lookupPanelGrid;
            }
            panel.getChildren().add(component);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            logger.trace("Returning");
        }
    }

    private UIComponent getInputDateForCalender(boolean isMandatory, UIComponent calendar) {
        if (isMandatory) {
            return calendar.getChildren().get(0);
        }
        return calendar;
    }

    protected UIComponent createCalendarControl(String id, String fieldExpression,
            int sortIndex, boolean editable,
            DD screenFieldDD, int screenFieldIndex, boolean isMandatory) {

        UIComponent calendar = createCalendarControlGeneric(id, fieldExpression, sortIndex, editable, screenFieldDD, screenFieldIndex, isMandatory);
        org.primefaces.component.calendar.Calendar inputDate = (org.primefaces.component.calendar.Calendar) getInputDateForCalender(isMandatory, calendar);
        if (isMandatory) {
            inputDate.setRequired(true);
        }
        inputDate.setId(id + "_inputText" + screenFieldIndex);
        return calendar;
    }

    public HtmlPanelGrid createLabel(String id, CustRepFilterFld screenField) {
        logger.debug("Entering");
        try {
            HtmlPanelGrid headerLabelGrid = new HtmlPanelGrid();
            headerLabelGrid.setStyle("border:hidden;");
            HtmlOutputLabel outputLabel = new HtmlOutputLabel();
            if (id.contains(".")) {
                outputLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_"
                        + id.substring(id.lastIndexOf(".") + 1) + "_Label");
            } else {
                outputLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_"
                        + id + "_Label");
            }
            outputLabel.setStyleClass("labelTxt");
            String title = screenField.getTitleTranslated();
            outputLabel.setValue(title == null || title.equals("") ? screenField.getHeader() : title);
            HtmlOutputLabel astriskLabel = new HtmlOutputLabel();
            astriskLabel.setId(getFacesContext().getViewRoot().createUniqueId() + "astriskLBL" + screenField.getDbid());
            if (screenField.isMandatory()) {
                astriskLabel.setValue("*");
            } else {
                astriskLabel.setStyle("padding-right:5px");
            }
            astriskLabel.setStyleClass("labelAsterisk");

            headerLabelGrid.setId(getFacesContext().getViewRoot().createUniqueId() + "headerLabelGrid" + screenField.getDbid());

            headerLabelGrid.setColumns(2);
            headerLabelGrid.setCellpadding("0");
            headerLabelGrid.setCellspacing("0");
            headerLabelGrid.getChildren().add(outputLabel);
            headerLabelGrid.setStyleClass("labelContainer");
            headerLabelGrid.getChildren().add(astriskLabel);
            logger.debug("Returning");
            return headerLabelGrid;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning");
            return new HtmlPanelGrid();
        }
    }

    public HtmlPanelGrid createLabel(String id, CustomReportParameter screenField) {
        logger.debug("Entering");
        try {
            HtmlPanelGrid headerLabelGrid = new HtmlPanelGrid();
            headerLabelGrid.setStyle("border:hidden;");
            HtmlOutputLabel outputLabel = new HtmlOutputLabel();
            if (id.contains(".")) {
                outputLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_"
                        + id.substring(id.lastIndexOf(".") + 1).replace(" ", "_") + "_Label");
            } else {
                outputLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_"
                        + id.replace(" ", "_") + "_Label");
            }
            outputLabel.setStyleClass("labelTxt");
            outputLabel.setValue(screenField.getHeader());
            HtmlOutputLabel astriskLabel = new HtmlOutputLabel();
            astriskLabel.setId(getFacesContext().getViewRoot().createUniqueId() + "astriskLBL" + screenField.getDbid());
            if (screenField.isMandatory()) {
                astriskLabel.setValue("*");
            } else {
                astriskLabel.setStyle("padding-right:5px");
            }
            astriskLabel.setStyleClass("labelAsterisk");

            headerLabelGrid.setId(getFacesContext().getViewRoot().createUniqueId() + "headerLabelGrid" + screenField.getDbid());

            headerLabelGrid.setColumns(2);
            headerLabelGrid.setCellpadding("0");
            headerLabelGrid.setCellspacing("0");
            headerLabelGrid.getChildren().add(outputLabel);
            headerLabelGrid.setStyleClass("labelContainer");
            headerLabelGrid.getChildren().add(astriskLabel);
            logger.debug("Returning");
            return headerLabelGrid;
        } catch (Exception ex) {
            logger.trace("Error Creating Header: {}", ex.getMessage());
            logger.error("Exception thrown", ex);
            logger.debug("Returning");
            return new HtmlPanelGrid();
        }
    }

    @Override
    protected OScreen loadObject(String entityName, ArrayList<String> conditions,
            List<String> fieldExpressions, OUser loggedUser) throws EntityExistsException,
            EntityNotFoundException, NonUniqueResultException, NoResultException,
            OptimisticLockException, RollbackException, TransactionRequiredException, UserNotAuthorizedException {
        return null;
    }

    /**
     * @return the reportFilterDBID
     */
    public String getReportFilterDBID() {
        return reportFilterDBID;
    }

    /**
     * @param reportFilterDBID the reportFilterDBID to set
     */
    public void setReportFilterDBID(String reportFilterDBID) {
        this.reportFilterDBID = reportFilterDBID;
    }

    public void loadReportFilterFields() {
        try {
            reportFilterFields = oem.loadEntityList(CustRepFilterFld.class.getSimpleName(),
                    Collections.singletonList("customReportFilter.dbid = " + reportFilterDBID), null, null, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
    }

    /**
     * @return the reportFillerFields
     */
    public List<CustRepFilterFld> getReportFillerFields() {
        return reportFilterFields;
    }

    /**
     * @param reportFillerFields the reportFillerFields to set
     */
    public void setReportFillerFields(List<CustRepFilterFld> reportFillerFields) {
        this.reportFilterFields = reportFillerFields;
    }

    /**
     * @return the valuesMap
     */
    public HashMap<String, Object> getValuesMap() {
        return valuesMap;
    }

    /**
     * @param valuesMap the valuesMap to set
     */
    public void setValuesMap(HashMap<String, Object> valuesMap) {
        this.valuesMap = valuesMap;
    }

    @Override
    public String returnSelected() {
        if (!selectedData.equals("")) {
//            selectedData = "";
        }
        logger.debug("Entering");
        try {
            if (getLookupData() != null) {
                String mapKey = fieldLookup.getOwnerEntityBackBeanExpression().substring(
                        fieldLookup.getOwnerEntityBackBeanExpression().indexOf("'") + 1,
                        fieldLookup.getOwnerEntityBackBeanExpression().lastIndexOf("'"));
                if (!mapKey.contains("#")) {
                    valuesMap.put(mapKey, getLookupData());
                    String fieldExpression = fieldLookup.getScreenField().getFieldExpression();
                    selectedData.put(fieldLookup.getInputText().getId(), BaseEntity.getValueFromEntity(getLookupData(), fieldExpression));
                } else {
                    parametersMap.put(mapKey, getLookupData());
                    String fieldExpression = fieldLookup.getScreenField().getFieldExpression();
                    selectedData.put(fieldLookup.getInputText().getId(), BaseEntity.getValueFromEntity(getLookupData(), fieldExpression));
                }
                setLookupPanelRendered(false);
                if (getLookupData() instanceof CustomReportDynamicFilter) {
                    dynamicFilter = (CustomReportDynamicFilter) getLookupData();
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning Null");
        return null;
    }

    public String returnSelectedList() {
        logger.debug("Entering");
        try {
            if (getLookupDataList() != null) {
                String mapKey = fieldLookup.getOwnerEntityBackBeanExpression().substring(
                        fieldLookup.getOwnerEntityBackBeanExpression().indexOf("'") + 1,
                        fieldLookup.getOwnerEntityBackBeanExpression().lastIndexOf("'"));
                valuesMap.remove(mapKey);
                if (!mapKey.contains("#")) {
                    Object[] lookupDataList = getLookupDataList();

                    String fieldExpression = fieldLookup.getScreenField().getFieldExpression();
                    String ConcData = BaseEntity.getValueFromEntity(getLookupDataList()[0], fieldExpression).toString();

                    for (int j = 0; j < lookupDataList.length; j++) {
                        valuesMap.put(mapKey + "-" + j, lookupDataList[j]);
//                        selectedData += lookupDataList[j].invokeGetter("get"+mapKey).toString() + ",";
                        if (j + 1 != lookupDataList.length) {
                            ConcData += " , " + BaseEntity.getValueFromEntity(getLookupDataList()[j + 1], fieldExpression).toString();
                        }
                    }
                    selectedData.put(fieldLookup.getInputText().getId(), ConcData);
                } else {
                    parametersMap.put(mapKey, getLookupDataList());
                }
                setLookupPanelRendered(false);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning Null");
        return null;
    }

    @Override
    public String returnEmpty() {
        logger.debug("Entering");
        try {
            String mapKey = fieldLookup.getOwnerEntityBackBeanExpression().
                    substring(fieldLookup.getOwnerEntityBackBeanExpression().
                            indexOf("'") + 1, fieldLookup.getOwnerEntityBackBeanExpression().lastIndexOf("'"));
            if (mapKey.contains("#")) {
                parametersMap.put(mapKey, Class.forName(entitySetupService.
                        getOScreenEntityDTO(fieldLookup.getOScreen().getDbid(), loggedUser).getEntityClassPath()).newInstance());
            } else {
                valuesMap.put(mapKey, Class.forName(entitySetupService.
                        getOScreenEntityDTO(fieldLookup.getOScreen().getDbid(), loggedUser).getEntityClassPath()).newInstance());
            }//to clear custom report parameter lookup
            if (selectedData != null && selectedData.size() != 0) {
                String fieldExpression = fieldLookup.getScreenField().getFieldExpression();
                selectedData.put(fieldLookup.getInputText().getId(), BaseEntity.getValueFromEntity(getLookupData(), fieldExpression));
            }
            setLookupPanelRendered(false);
            if (mapKey.equals("customreportfilterfieldforlookup")) {
                dynamicFilter = null;
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning with Null");
        return null;
    }

    public String runReport() {
        logger.debug("Entering");
        try {
            OFunctionResult oFRTemp = new OFunctionResult();
            boolean isMandatoryFieldsValid = checkMandatoryLookupFieldValidity(true, oFRTemp);
            customReport.setRevertLang(isRevertLanguage());

            customReport.setFitPage(fitPage);

            if (!isApplyingFilter()) {
                customReport.setApplyingFilter(false);
            }
            if (isMandatoryFieldsValid) {
                String whereConditions = constructReportFilter();
                openCustomReport(generateFilterQuery(whereConditions), parametersMap, customReport, entityAttachment);
            }
            messagesPanel.clearAndDisplayMessages(oFRTemp);

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning with empty string");
        return "";

    }

    public String generateFilterQuery(String whereCondition) {

        List<CustRepFilterFld> filterFields = null;
        String filterCondition = "";
        try {
            if (dynamicFilter != null) {
                List<String> conds = new ArrayList<String>();
                conds.add("customReportDynamicFilter.dbid = " + dynamicFilter.getDbid());
                conds.add("##ACTIVEANDINACTIVE");
                filterFields = oem.loadEntityList(CustRepFilterFld.class.getSimpleName(), conds, null, null, loggedUser);
                if (filterFields == null || filterFields.size() == 0) {
                    return whereCondition;
                }
//                filterFields = dynamicFilter.getCustomreportfilterfields();
                if (filterFields.size() > 0) {
                    Collections.sort(filterFields, new Comparator<CustRepFilterFld>() {
                        @Override
                        public int compare(final CustRepFilterFld object1, final CustRepFilterFld object2) {
                            return Integer.valueOf(object1.getSortIndex()).compareTo(object2.getSortIndex());
                        }
                    });
                }
                String rowOperator = "";
                Map<String, String> map = uiFrameWorkFacade.getFieldsNamesAndDataTypeInView(dynamicFilter.getCustomReport().getCustomReportFilter().getViewName(), loggedUser);
                for (CustRepFilterFld filterField : filterFields) {
                    boolean singlcot = map.get(filterField.getFieldName()).toUpperCase().contains("VARCHAR")
                            || map.get(filterField.getFieldName()).toUpperCase().contains("DATE");
                    String leftBrac = filterField.getLeftBrackets() == null ? "" : filterField.getLeftBrackets();
                    filterCondition = filterCondition.equals("") ? leftBrac : " " + filterCondition + " " + rowOperator + " " + leftBrac;
                    rowOperator = filterField.getRowOperator();
                    filterCondition += filterField.getFieldName() + " ";
                    filterCondition += filterField.getOperator() + " ";
                    String operator = filterField.getOperator().trim().toLowerCase();
                    String filterFieldVal1 = filterField.getVal1().trim();
                    if ((operator.equals("between") || operator.equals("not between")) && singlcot) {
                        String[] vals = filterFieldVal1.toLowerCase().split("and");
                        filterCondition += "'" + vals[0] + "' AND '" + vals[1] + "'";
                    } else if (operator.equals("in") || operator.equals("not in")) {
                        String[] vals = filterFieldVal1.toLowerCase().split(",");
                        filterCondition += "(";
                        String cot = singlcot ? "'" : "";
                        for (int i = 0; i < vals.length; i++) {
                            if (i == vals.length - 1) {
                                filterCondition += cot + vals[i] + cot;
                            } else {
                                filterCondition += cot + vals[i] + cot + ", ";
                            }
                        }
                        filterCondition += ")";
                    } else if (operator.equals("is") || operator.equals("is not")) {
                        filterCondition += filterFieldVal1;
                    } else {
                        filterCondition += singlcot ? " '" + filterFieldVal1 + "' " : " " + filterFieldVal1 + " ";
                    }
                    filterCondition += filterField.getRightBrackets() == null ? "" : filterField.getRightBrackets();
                }

                if ((whereCondition == null || whereCondition.equals("")) && !filterCondition.equals("")) {
                    whereCondition = " Where " + filterCondition;

                } else if (!whereCondition.equals("") && !filterCondition.equals("")) {
                    whereCondition += " AND " + filterCondition;
                }
            }

        } catch (Exception ex) {

        }
        return whereCondition;
    }

    //<editor-fold defaultstate="collapsed" desc="Construct Filter">
    public String constructReportFilter() {
        logger.debug("Entering");
        String whereCondition = "";
        String value = "";
        String value_2 = "";
        List<String> valueList = new ArrayList<String>();
        boolean firstCondition = false;
        System.out.println("");
        try {
            for (CustRepFilterFld field : reportFilterFields) {
                if (field.getVal1Dd().getControlType().getDbid() == DD.CT_LOOKUPSCREEN
                        || field.getVal1Dd().getControlType().getDbid() == DD.CT_DROPDOWN) {
                    String expression = "dbid";
                    if (field.getVal1FieldExpression().contains(".")) {
                        expression = field.getVal1FieldExpression().substring(0, field.getVal1FieldExpression().lastIndexOf(".")) + ".dbid";
                    }
                    if (field.getOperator().equalsIgnoreCase("between") || field.getOperator().equalsIgnoreCase("range")) {
                        value = String.valueOf(BaseEntity.getValueFromEntity(valuesMap.get(field.getFieldName()), expression));
                        value_2 = String.valueOf(BaseEntity.getValueFromEntity(valuesMap.get(field.getFieldName() + "_2"), expression));

                    } else if (field.getOperator().equalsIgnoreCase("like")) {
                        value = "'%" + String.valueOf(BaseEntity.getValueFromEntity(valuesMap.get(field.getFieldName()), expression)) + "%'";
                    } else {
                        //chech if valuesMap has field.getFieldName() element if not get field.getFieldName()-0,1,...
                        if (valuesMap.containsKey(field.getFieldName())) {
                            value = String.valueOf(BaseEntity.getValueFromEntity(valuesMap.get(field.getFieldName()), expression));
                        } else {
                            //case multi selection
                            int j = 0;
                            while (true) {
                                if (!valuesMap.containsKey(field.getFieldName() + "-" + j)) {
                                    break;
                                } else {
                                    valueList.add(String.valueOf(BaseEntity.getValueFromEntity(valuesMap
                                            .get(field.getFieldName() + "-" + j), expression)));
                                }
                                j++;
                            }
                        }

                    }

                } else {
                    if (field.getOperator().equalsIgnoreCase("between") || field.getOperator().equalsIgnoreCase("range")) {
                        value = String.valueOf(valuesMap.get(field.getFieldName()));
                        value_2 = String.valueOf(valuesMap.get(field.getFieldName() + "_2"));
                    } else if (field.getOperator().equalsIgnoreCase("like")) {
                        value = "'%" + valuesMap.get(field.getFieldName()) + "%'";
                    } else {
                        value = String.valueOf(valuesMap.get(field.getFieldName()));
                    }
                    if (field.getVal1Dd().getControlType().getDbid() == DD.CT_CALENDAR) {
                        Date date = (Date) valuesMap.get(field.getFieldName());
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        if (date != null) {
                            value = sdf.format(date);
                            value = "'" + value + "'";
                        }
                        if (value_2 != null && !value_2.isEmpty()) {
                            Date date_2 = (Date) valuesMap.get(field.getFieldName() + "_2");
                            if (date_2 != null) {
                                value_2 = sdf.format(date_2);
                                value_2 = "'" + value_2 + "'";
                            }
                        }
                    }
                }
                if (null != valueList && !valueList.isEmpty()) {
                    if (!firstCondition) {
                        whereCondition += " AND ";
                    }
                    whereCondition += "(" + field.getFieldName() + " " + field.getOperator() + " " + valueList.get(0);
                    for (int k = 1; k < valueList.size(); k++) {
                        whereCondition += " OR " + field.getFieldName() + " " + field.getOperator() + " " + valueList.get(k);
                    }
                    whereCondition += ")";
                    valueList.clear();
                }
                if (field.getOperator().equalsIgnoreCase("between") || field.getOperator().equalsIgnoreCase("range")) {

                    if (!firstCondition) {
                        whereCondition += " AND ";
                    }
                    firstCondition = false;

                    if (!(value == null || value.isEmpty() || value.equalsIgnoreCase("null"))) {
                        whereCondition += field.getFieldName() + " >= " + value;
                    }
                    if (!(value_2 == null || value_2.isEmpty() || value_2.equalsIgnoreCase("null"))) {
                        if (valuesMap.get(field.getFieldName()) != null) {
                            whereCondition += " AND ";
                        }
                        whereCondition += field.getFieldName() + " <= " + value_2;
                    }
                    continue;
                }
                if (value != null && !value.contains("null") && !value.isEmpty() && !value.equals("'%%'")
                        && !value.contains("false") && !((value.equals("0") || value.equals("'%0%'"))
                        && (field.getVal1Dd().getControlType().getDbid() == DD.CT_LOOKUPSCREEN
                        || field.getVal1Dd().getControlType().getDbid() == DD.CT_DROPDOWN))) {
                    if (!firstCondition) {
                        whereCondition += " AND ";
                    }
                    whereCondition += field.getFieldName() + " " + field.getOperator() + " " + value;
                    firstCondition = false;
                    value = null;
                }
            }

            if (whereCondition.contains("true")) {
                whereCondition = whereCondition.replaceAll("true", "1");
            } else if (whereCondition.contains("true")) {
                whereCondition = whereCondition.replaceAll("false", "0");
            }
            whereCondition = whereCondition.trim();
            whereCondition = whereCondition.replaceAll("AND AND", "AND").replace("AND  AND", "AND");
            if (whereCondition.startsWith("AND")) {
                whereCondition = whereCondition.replaceFirst("AND", "");
            }

            if (whereCondition.endsWith("AND")) {
                whereCondition = whereCondition.substring(0, whereCondition.lastIndexOf("AND"));
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return whereCondition == null || whereCondition.isEmpty() ? null : " WHERE " + whereCondition;
    }
    //</editor-fold>

    public CustomReport getCustomReport() {
        return customReport;
    }

    public void setCustomReport(CustomReport customReport) {
        this.customReport = customReport;
    }

    public AttachmentDTO getEntityAttachment() {
        return entityAttachment;
    }

    public void setEntityAttachment(AttachmentDTO entityAttachment) {
        this.entityAttachment = entityAttachment;
    }

    @Override
    public String getLookupConditionValue(String conditionExpression) {
        logger.debug("Entering");
        try {
            BaseEntity lookupSelectedEntity = (BaseEntity) valuesMap.get(conditionExpression);
            for (CustRepFilterFld field : reportFilterFields) {
                String expression = "dbid";
                if (field.getVal1FieldExpression().contains(".")) {
                    expression = field.getVal1FieldExpression().substring(0, field.getVal1FieldExpression().lastIndexOf(".")) + ".dbid";
                }
                Object valueToBeFiltered = BaseEntity.getValueFromEntity(lookupSelectedEntity, expression);
                if (valueToBeFiltered.toString().equals("0")) {
                    logger.debug("Returning with Null");
                    return null;
                } else {
                    logger.debug("Returning");
                    return valueToBeFiltered.toString();
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning with Null");
            return null;
        }
        logger.debug("Returning with Null");
        return null;
    }
}
