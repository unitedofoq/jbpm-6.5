/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.Exception.BasicException;
import com.unitedofoq.fabs.core.content.AttachmentDTO;
import com.unitedofoq.fabs.core.content.EntityAttachmentServiceLocal;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.encryption.DataEncryptorServiceLocal;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.security.user.UserPrivilege.ConstantUserPrivilege;
import com.unitedofoq.fabs.core.setup.FABSSetup;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.io.filefilter.MagicNumberFileFilter;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mibrahim
 */
@ManagedBean(name = "EntityAttachmentBean")
@ViewScoped
public class EntityAttachmentBean extends FormBean {

    final static Logger logger = LoggerFactory.getLogger(EntityAttachmentBean.class);

    DataEncryptorServiceLocal dataEncryptorService = OEJB.lookup(DataEncryptorServiceLocal.class);

    private List<AttachmentDTO> entityAttachments;

    EntityAttachmentServiceLocal entityAttachmentService = OEJB.lookup(EntityAttachmentServiceLocal.class);
    private List<String> icons;
    private long entityDBID;

    private FABSSetupLocal fABSSetupLocal = OEJB.lookup(FABSSetupLocal.class);

    private AttachmentDTO selectedAttachment;

    private BaseEntity entity = null;

    public BaseEntity getEntity() {
        return entity;
    }

    public void setEntity(BaseEntity entity) {
        this.entity = entity;
    }

    public List<String> getImages() {
        return icons;
    }

    public void setImages(List<String> images) {
        this.icons = images;
    }

    public List<AttachmentDTO> getEntityAttachments() {
        return entityAttachments;
    }

    public void setEntityAttachments(List<AttachmentDTO> entityAttachments) {
        this.entityAttachments = entityAttachments;
    }
    private String confirmDialogStr;
    private String confirmBtnStr;
    private String declineBtnStr;
    private String statusDialogHdrStr;
    private String attachmentGallariaPnlHdrStr;
    private String downloadPnlHdrStr;
    private String downloadLinkStr;
    private String editorStr;
    private String attachedFileLblStr;
    private String attachedFileDescLblStr;
    private String mimeTypeLblStr;
    private String uploadPnlStr;
    private String noRecordStr;
    private String browseBtnStr;
    private String uploadLabelStr;
    private String cancelButtonStr;
    private boolean viewPnl;
    private boolean deleteAttachmentButton;
    private boolean downloadAttachmentButton;
    private boolean uploadPnl;
    private String attachedFileDateTime;
    private String attachmentUser;

    public String getAttachmentUser() {
        return attachmentUser;
    }

    public void setAttachmentUser(String attachmentUser) {
        this.attachmentUser = attachmentUser;
    }

    public String getAttachedFileDateTime() {
        return attachedFileDateTime;
    }

    public void setAttachedFileDateTime(String attachedFileDateTime) {
        this.attachedFileDateTime = attachedFileDateTime;
    }

    public boolean isViewPnl() {
        return viewPnl;
    }

    public void setViewPnl(boolean viewPnl) {
        this.viewPnl = viewPnl;
    }

    public boolean isDeleteAttachmentButton() {
        return deleteAttachmentButton;
    }

    public void setDeleteAttachmentButton(boolean deleteAttachmentButton) {
        this.deleteAttachmentButton = deleteAttachmentButton;
    }

    public boolean isDownloadAttachmentButton() {
        return downloadAttachmentButton;
    }

    public void setDownloadAttachmentButton(boolean downloadAttachmentButton) {
        this.downloadAttachmentButton = downloadAttachmentButton;
    }

    public boolean isUploadPnl() {
        return uploadPnl;
    }

    public void setUploadPnl(boolean uploadPnl) {
        this.uploadPnl = uploadPnl;
    }

    public String getCancelButtonStr() {
        return cancelButtonStr;
    }

    public void setCancelButtonStr(String cancelButtonStr) {
        this.cancelButtonStr = cancelButtonStr;
    }

    public String getUploadLabelStr() {
        return uploadLabelStr;
    }

    public void setUploadLabelStr(String uploadLabelStr) {
        this.uploadLabelStr = uploadLabelStr;
    }

    public String getBrowseBtnStr() {
        return browseBtnStr;
    }

    public void setBrowseBtnStr(String browseBtnStr) {
        this.browseBtnStr = browseBtnStr;
    }

    public String getNoRecordStr() {
        return noRecordStr;
    }

    public void setNoRecordStr(String noRecordStr) {
        this.noRecordStr = noRecordStr;
    }

    public String getUploadPnlStr() {
        return uploadPnlStr;
    }

    public void setUploadPnlStr(String uploadPnlStr) {
        this.uploadPnlStr = uploadPnlStr;
    }

    public String getMimeTypeLblStr() {
        return mimeTypeLblStr;
    }

    public void setMimeTypeLblStr(String mimeTypeLblStr) {
        this.mimeTypeLblStr = mimeTypeLblStr;
    }

    public String getAttachedFileDescLblStr() {
        return attachedFileDescLblStr;
    }

    public void setAttachedFileDescLblStr(String attachedFileDescLblStr) {
        this.attachedFileDescLblStr = attachedFileDescLblStr;
    }

    public String getAttachedFileLblStr() {
        return attachedFileLblStr;
    }

    public void setAttachedFileLblStr(String attachedFileLblStr) {
        this.attachedFileLblStr = attachedFileLblStr;
    }

    public String getEditorStr() {
        return editorStr;
    }

    public void setEditorStr(String editorStr) {
        this.editorStr = editorStr;
    }

    public String getDownloadLinkStr() {
        return downloadLinkStr;
    }

    public void setDownloadLinkStr(String downloadLinkStr) {
        this.downloadLinkStr = downloadLinkStr;
    }

    public String getDownloadPnlHdrStr() {
        return downloadPnlHdrStr;
    }

    public void setDownloadPnlHdrStr(String downloadPnlHdrStr) {
        this.downloadPnlHdrStr = downloadPnlHdrStr;
    }

    public String getAttachmentGallariaPnlHdrStr() {
        return attachmentGallariaPnlHdrStr;
    }

    public void setAttachmentGallariaPnlHdrStr(String attachmentGallariaPnlHdrStr) {
        this.attachmentGallariaPnlHdrStr = attachmentGallariaPnlHdrStr;
    }

    public String getStatusDialogHdrStr() {
        return statusDialogHdrStr;
    }

    public void setStatusDialogHdrStr(String statusDialogHdrStr) {
        this.statusDialogHdrStr = statusDialogHdrStr;
    }

    public String getDeclineBtnStr() {
        return declineBtnStr;
    }

    public void setDeclineBtnStr(String declineBtnStr) {
        this.declineBtnStr = declineBtnStr;
    }

    public String getConfirmBtnStr() {
        return confirmBtnStr;
    }

    public void setConfirmBtnStr(String confirmBtnStr) {
        this.confirmBtnStr = confirmBtnStr;
    }

    public String getConfirmDialogStr() {
        return confirmDialogStr;
    }

    public void setConfirmDialogStr(String confirmDialogStr) {
        this.confirmDialogStr = confirmDialogStr;
    }

    public void setSelectedAttachment(AttachmentDTO selectedAttachment) {
        this.selectedAttachment = selectedAttachment;
    }

    public AttachmentDTO getSelectedAttachment() {
        return selectedAttachment;
    }

    @Override
    protected boolean onInit(Map<String, String> requestParams) {
        super.onInit(requestParams);
        processInput();
        //view attachement screen component according to user privilege
        if (securityService.checkPrivilegeAuthority(ConstantUserPrivilege.Attachment_viewAll, loggedUser)) {
            setViewPnl(true);
            setDeleteAttachmentButton(true);
            setDownloadAttachmentButton(true);
            setUploadPnl(true);
        } else {
            setViewPnl(securityService.checkPrivilegeAuthority(ConstantUserPrivilege.Attachment_viewPnl, loggedUser));
            setDeleteAttachmentButton(securityService.checkPrivilegeAuthority(ConstantUserPrivilege.Attachment_deleteButton, loggedUser));
            setDownloadAttachmentButton(securityService.checkPrivilegeAuthority(ConstantUserPrivilege.Attachment_downloadButton, loggedUser));
            setUploadPnl(securityService.checkPrivilegeAuthority(ConstantUserPrivilege.Attachment_uploadPnl, loggedUser));
        }
        //view attachement screen component according to Entity authority
        if (entityDBID != 0) {
            String entityClassPath = (String) dataMessage.getData().get(0);
            try {
                List<String> conditions = new ArrayList<String>();
                conditions.add("entityClassPath = '" + entityClassPath + "'");
                OEntity entity = (OEntity) oem.loadEntity(OEntity.class.getSimpleName(), conditions, null, loggedUser);

                String deleteButtonCondition = entity.getDeleteAttachDispCond();
                String uploadPnlCondition = entity.getUploadAttachDispCond();

                String entityName = entityClassPath.split("\\.")[entityClassPath.split("\\.").length - 1];
                if (deleteButtonCondition != null && !deleteButtonCondition.equals("")) {
                    BaseEntity attachmentEntity = applyAttachmentDisplayCondition(deleteButtonCondition, conditions, entityName);
                    if (attachmentEntity != null && attachmentEntity.getDbid() != 0) {
                        setDeleteAttachmentButton(true);
                    } else {
                        setDeleteAttachmentButton(false);
                    }
                }
                if (uploadPnlCondition != null && !uploadPnlCondition.equals("")) {
                    BaseEntity attachmentEntity = applyAttachmentDisplayCondition(uploadPnlCondition, conditions, entityName);
                    if (attachmentEntity != null && attachmentEntity.getDbid() != 0) {
                        setUploadPnl(true);
                    } else {
                        setUploadPnl(false);
                    }
                }
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
        }
        //onloadJSStr += "top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');";
        DD confirmDialogDD = ddService.getDD("confirmDialog", loggedUser);
        setConfirmDialogStr(confirmDialogDD.getLabelTranslated());

        DD confirmBtnDD = ddService.getDD("confirmBtn", loggedUser);
        setConfirmBtnStr(confirmBtnDD.getLabelTranslated());

        DD declineBtnDD = ddService.getDD("declineBtn", loggedUser);
        setDeclineBtnStr(declineBtnDD.getLabelTranslated());

        DD statusDialogHdrDD = ddService.getDD("statusDialogHdr", loggedUser);
        setStatusDialogHdrStr(statusDialogHdrDD.getLabelTranslated());

        DD attachmentGallariaPnlHdrDD = ddService.getDD("attachmentGallariaPnlHdr", loggedUser);
        setAttachmentGallariaPnlHdrStr(attachmentGallariaPnlHdrDD.getLabelTranslated());

        DD downloadPnlHdrDD = ddService.getDD("downloadPnlHdr", loggedUser);
        setDownloadPnlHdrStr(downloadPnlHdrDD.getLabelTranslated());

        DD downloadLinkDD = ddService.getDD("downloadLinkStr", loggedUser);
        setDownloadLinkStr(downloadLinkDD.getLabelTranslated());

        DD editorDD = ddService.getDD("editorStr", loggedUser);
        setEditorStr(editorDD.getLabelTranslated());

        DD attachedFileLblDD = ddService.getDD("attachedFileLbl", loggedUser);
        setAttachedFileLblStr(attachedFileLblDD.getLabelTranslated());

        DD attachedFileDescLblDD = ddService.getDD("attachedFileDescLbl", loggedUser);
        setAttachedFileDescLblStr(attachedFileDescLblDD.getLabelTranslated());

        DD mimeTypeLblDD = ddService.getDD("mimeTypeLbl", loggedUser);
        setMimeTypeLblStr(mimeTypeLblDD.getLabelTranslated());

        DD uploadPnlDD = ddService.getDD("uploadPnlStr", loggedUser);
        setUploadPnlStr(uploadPnlDD.getLabelTranslated());

        DD noRecordsMsgDD = ddService.getDD("noRecordsMsg", loggedUser);
        setNoRecordStr(noRecordsMsgDD.getLabelTranslated());

        DD browselblDD = ddService.getDD("browselbl", loggedUser);
        setBrowseBtnStr(browselblDD.getLabelTranslated());

        DD uploadLabelDD = ddService.getDD("uploadLabel", loggedUser);
        setUploadLabelStr(uploadLabelDD.getLabelTranslated());

        DD cancelButtonDD = ddService.getDD("cancelButton", loggedUser);
        setCancelButtonStr(cancelButtonDD.getLabelTranslated());

        DD attachedFileDateTimeDD = ddService.getDD("attachedFileDateTime", loggedUser);
        setAttachedFileDateTime(attachedFileDateTimeDD.getLabelTranslated());

        DD attachmentUserDD = ddService.getDD("attachmentUser", loggedUser);
        setAttachmentUser(attachmentUserDD.getLabelTranslated());

        return true;
    }

    private BaseEntity applyAttachmentDisplayCondition(String uploadPnlCondition, List<String> conditions, String entityName) throws Exception {
        conditions.clear();
        conditions.add("dbid = " + entityDBID);
        conditions.add("##ACTIVEANDINACTIVE");
        String[] usrConditions = uploadPnlCondition.split("(?i) and ");
        for (String cond : usrConditions) {
            conditions.add(cond);
        }
        BaseEntity attachmentEntity = (BaseEntity) oem.loadEntity(entityName, conditions, null, loggedUser);
        return attachmentEntity;
    }

    private byte[] uploadedFileToByteArray(UploadedFile uploadedFile) {
        InputStream in = null;
        byte[] bytes = null;
        try {
            in = uploadedFile.getInputstream();
            int read;
            int i = (int) uploadedFile.getSize();
            bytes = new byte[i];
            read = in.read(bytes);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            try {
                in.close();
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
            return bytes;
        }
    }

    private File getFile(UploadedFile uploadedFile) {
        try {

            File javaFile = new File("java_" + uploadedFile.getFileName());
            OutputStream javaFileStream = new FileOutputStream(javaFile);
            byte[] fileContents = uploadedFile.getContents();

            javaFileStream.write(fileContents);
            javaFileStream.close();

            return javaFile;
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(EntityAttachmentBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void handleFileUpload(FileUploadEvent event) {
        logger.debug("Entering");

        boolean acceptFile = true;
        try {

            UploadedFile uploadedFile = event.getFile();
            acceptFile = checkAttachmentNameSecurity(uploadedFile.getFileName());

            if (acceptFile) {

                byte[] bytes = uploadedFileToByteArray(uploadedFile);
                File file = getFile(uploadedFile);

                if (checkAllowaedFiles(file)) {

                    AttachmentDTO attachmentDTO = new AttachmentDTO();
                    attachmentDTO.setAttachment(bytes);
                    attachmentDTO.setMimeType(uploadedFile.getContentType());
                    attachmentDTO.setName(uploadedFile.getFileName());
                    attachmentDTO.setAttachmentUser(loggedUser.getLoginName());
                    int publicKey = checkIfEncrptionKeys(attachmentDTO, bytes);

                    if (publicKey == 0) {
                        if (entity.isPreSavedEntity()) {
                            entityAttachmentService.putPreSave(attachmentDTO, entity, loggedUser);
                            addAttachmentAndIconToLists(attachmentDTO);
                        } else {
                            AttachmentDTO attachment = entityAttachmentService.put(attachmentDTO, entity.getClass().getName(), entityDBID, entity, loggedUser);
                            if (attachment.getId() != null && !attachment.getId().equals("0")) {
                                attachmentDTO.setId(attachment.getId());
                                attachmentDTO.setAttachmentDateTime(attachment.getAttachmentDateTime());
                                addAttachmentAndIconToLists(attachmentDTO);
                            } else {
                                FacesContext context = FacesContext.getCurrentInstance();
                                context.addMessage(null, new FacesMessage("Error!", "not Saved"));
                            }
                        }
                    }
                } else {

                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Insecure File ", "Not supported"));
                    logger.debug("Returning from: validateUploadedFile");
                }

            } else {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid File Type or Invalid File Name ", "Not supported"));
                logger.debug("Returning from: validateUploadedFile");
            }
        } catch (BasicException ex) {
            logger.error("Exception thrown", ex);
            FacesContext context = FacesContext.getCurrentInstance();

            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getUserMessage().getExceptionDetails()));
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            logger.debug("Returning");
        }
    }

    public boolean checkAttachmentNameSecurity(String attachmentName) {
        Pattern pattern = Pattern.compile("^([[\\w- ]|[\\u0600-\\u06ff]|[\\u0750-\\u077f]|[\\ufb50-\\ufbc1]|[\\ufbd3-\\ufd3f]|[\\ufd50-\\ufd8f]|[\\ufd92-\\ufdc7]|[\\ufe70-\\ufefc]|[\\uFDF0-\\uFDFD]]+)\\.([a-zA-Z0-9]+)$");
        Matcher matcher = pattern.matcher(attachmentName);
        return matcher.matches();
    }

    private boolean checkAttachmentExtension(String attachmentName) {

        boolean isBlockedExtension = false;
        ArrayList<String> blockedExtensions = new ArrayList<String>();
        blockedExtensions.add("exe");

        FABSSetup fABSSetup = fABSSetupLocal.loadKeySetup("blockedFilesExtensions", loggedUser);

        if (fABSSetup != null && !fABSSetup.getSvalue().trim().equals("") && !fABSSetup.equals(null)) {

            if (!fABSSetup.getSvalue().contains(",")) {

                blockedExtensions.add(fABSSetup.getSvalue().trim());
            } else {

                for (String extension : fABSSetup.getSvalue().split(",")) {

                    blockedExtensions.add(extension.trim());
                }

            }

        }
        String fileExtension = attachmentName.substring(attachmentName.lastIndexOf(".") + 1);

        for (String extension : blockedExtensions) {
            if (fileExtension.equalsIgnoreCase(extension)) {

                isBlockedExtension = true;
            }
        }

        return isBlockedExtension;
    }

    private boolean checkAllowaedFiles(File file) {
        boolean isAllowed = false;
        ArrayList<String> allowedMagicNumbers = new ArrayList<String>();
        FABSSetup fABSSetup = fABSSetupLocal.loadKeySetup("allowedFiles", loggedUser);
        if (fABSSetup != null && !fABSSetup.getSvalue().trim().equals("") && !fABSSetup.equals(null)) {
            if (!fABSSetup.getSvalue().contains(";")) {
                allowedMagicNumbers.add(fABSSetup.getSvalue().trim());
            } else {
                for (String magicOffsetNumber : fABSSetup.getSvalue().split(";")) {
                    allowedMagicNumbers.add(magicOffsetNumber.trim());
                }
            }
            for (String magicNumber : allowedMagicNumbers) {
                byte[] magicNumberValue = hexStringToByteArray(magicNumber.substring(0, magicNumber.indexOf(":")));
                long offset = Long.valueOf(magicNumber.substring(magicNumber.indexOf(":") + 1, magicNumber.length()));
                MagicNumberFileFilter filter = new MagicNumberFileFilter(magicNumberValue, offset);
                if (filter.accept(file)) {
                    isAllowed = true;
                }
            }
        }
        return isAllowed;
    }

    private byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public void addAttachmentAndIconToLists(AttachmentDTO attachmentDTO) {
        String iconPath;
        iconPath = getAttachmentIconPath(attachmentDTO);
        icons.add(iconPath);
        entityAttachments.add(attachmentDTO);
    }

    public StreamedContent downloadContent(AttachmentDTO attachmentDTO) throws UnsupportedEncodingException {
        InputStream inputStream = new ByteArrayInputStream(attachmentDTO.getAttachment());
        String attachmentFileName = URLEncoder.encode(attachmentDTO.getName(), "UTF-8").replaceAll(
                "\\+", "%20");
        return new DefaultStreamedContent(inputStream, attachmentDTO.getMimeType(),
                attachmentFileName);
    }

    public void deleteAttachment() {
        logger.debug("Entering");
        icons.remove(entityAttachments.indexOf(selectedAttachment));
        entityAttachments.remove(selectedAttachment);

        if (entity.isPreSavedEntity()) {
            entityAttachmentService.deleteForPresave(entity, selectedAttachment, loggedUser);
        } else {
            try {
                entityAttachmentService.delete(String.valueOf(selectedAttachment.getId()), loggedUser, entity.getClass().getName());
            } catch (BasicException ex) {
                //logger.error("Exception thrown", ex);
                logger.error("Exception thrown", ex);
                FacesContext context = FacesContext.getCurrentInstance();

                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getUserMessage().getExceptionDetails()));

            }
        }
        RequestContext.getCurrentInstance().update("downloadPnl");
        logger.debug("Returning");
    }

    @Override
    protected boolean processInput() {
        logger.trace("Entering");
        icons = new ArrayList<String>();
        entityAttachments = new ArrayList<AttachmentDTO>();
        entityDBID = (Long) dataMessage.getData().get(1);
        entity = (BaseEntity) dataMessage.getData().get(2);
        if (entity.isPreSavedEntity() && entityDBID == 0) {
            //case of form screen or tabular create object
            List<AttachmentDTO> attachmentListDTOs = entityAttachmentService.getPreSave(entity, loggedUser);
            if (attachmentListDTOs != null && attachmentListDTOs.size() > 0) {
                for (AttachmentDTO attachmentDTO : attachmentListDTOs) {
                    addAttachmentAndIconToLists(attachmentDTO);
                }
            }
        } else if (entity.isPreSavedEntity() && entityDBID != 0) {
            //case of form screen update object
            List<AttachmentDTO> attachmentDTOs;
            try {
                attachmentDTOs = entityAttachmentService.get(entityDBID, entity.getClass().getName(), loggedUser);
                List<AttachmentDTO> attachmentListDTOs = entityAttachmentService.getPreSave(entity, loggedUser);
                if (attachmentListDTOs != null && !attachmentListDTOs.isEmpty()) {
                    attachmentDTOs.addAll(attachmentListDTOs);
                }
                if (attachmentDTOs != null && attachmentDTOs.size() > 0) {
                    for (AttachmentDTO attachmentDTO : attachmentDTOs) {
                        addAttachmentAndIconToLists(attachmentDTO);
                    }
                }
            } catch (BasicException ex) {
                logger.error("Exception thrown", ex);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getUserMessage().getExceptionDetails()));
            }
        } else {
            List<AttachmentDTO> attachmentDTOs = null;
            try {
                attachmentDTOs = entityAttachmentService.get(entityDBID, entity.getClass().getName(), loggedUser);
            } catch (BasicException ex) {
                logger.error("Exception thrown", ex);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getUserMessage().getExceptionDetails()));
            }
            if (attachmentDTOs != null && attachmentDTOs.size() > 0) {
                for (AttachmentDTO attachmentDTO : attachmentDTOs) {
                    addAttachmentAndIconToLists(attachmentDTO);
                }
            }
        }

        logger.trace("Returning");
        return true;
    }

    private int checkIfEncrptionKeys(AttachmentDTO attachmentDTO, byte[] bytes) throws Exception {
        logger.trace("Entering");
        int publicKey = 0;
        if (attachmentDTO.getName().equalsIgnoreCase("publicKey.key")
                || attachmentDTO.getName().equalsIgnoreCase("privateKey.key")) {
            attachmentDTO.setName(attachmentDTO.getName().toLowerCase());
            if (entityAttachmentService.hasAttachments(attachmentDTO.getName(), loggedUser, entity.getClass().getName())) {
                logger.warn("there is encryption configuration publicKey.key privateKey.key please delete old files before uploading others");
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Error!", "there is encryption configuration publicKey.key or privateKey.key please delete old files before uploading others"));
                throw new Exception();
            } else if (attachmentDTO.getName().equalsIgnoreCase("publicKey.key")) {
                dataEncryptorService.changePublicPrivateKeys(attachmentDTO, entityDBID, bytes, systemUser);
                publicKey = 1;
            }
        }
        logger.trace("Returning");
        return publicKey;
    }

    public String getAttachmentIconPath(AttachmentDTO attach) {
        logger.trace("Entering");
        String mimeType = attach.getMimeType();
        String sysSep = File.separator;
        String path = sysSep + "resources" + sysSep + "icon" + sysSep;
        if (mimeType.toLowerCase().contains("text")) {
            logger.trace("Returning");
            return (mimeType.toLowerCase().contains("html"))
                    ? path + "html-icon.png" : path + "text-icon.gif";
        } else if (mimeType.toLowerCase().contains("application")) {
            if (mimeType.toLowerCase().contains("word")) {
                logger.trace("Returning");
                return path + "word-icon.png";
            }
            if (mimeType.toLowerCase().contains("powerpoint")) {
                logger.trace("Returning");
                return path + "powerpoint-icon.png";
            }
            if (mimeType.toLowerCase().contains("excel")) {
                logger.trace("Returning");
                return path + "excel-icon.png";
            }
            if (mimeType.toLowerCase().contains("pdf")) {
                logger.trace("Returning");
                return path + "pdf-icon.png";
            }
            if (mimeType.toLowerCase().contains("flash")) {
                logger.trace("Returning");
                return path + "flash-icon.png";
            }
            if (mimeType.toLowerCase().contains("stream")) {
                logger.trace("Returning");
                return path + "rar-icon.png";
            }
            logger.trace("Returning");
            return path + "application-icon.png";
        } else if (mimeType.toLowerCase().contains("image")) {
            logger.trace("Returning");
            return oimageServiceLocal.getFilePath(attach.getAttachment(), attach.getName(), systemUser);
        } else if (mimeType.toLowerCase().contains("message")) {
            logger.trace("Returning");
            return path + "mht-icon.png";
        } else {
            logger.trace("Returning");
            return path + "Unknown-con.png";
        }
    }

    public void onEdit(RowEditEvent event) {
        logger.debug("Entering");
        AttachmentDTO dba = (AttachmentDTO) event.getObject();
        try {
            entityAttachmentService.update(dba, loggedUser, entity.getClass().getName());
        } catch (BasicException ex) {
            // logger.error("Exception thrown", ex);
            logger.error("Exception thrown", ex);
            FacesContext context = FacesContext.getCurrentInstance();

            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getUserMessage().getExceptionDetails()));

        }
    }

    public void onCancel(RowEditEvent event) {
    }

    public void handleClose(CloseEvent event) {
//        RequestContext.getCurrentInstance().execute("top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');");
    }

    public void handleToggle(ToggleEvent event) {
//        RequestContext.getCurrentInstance().execute("top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');");
    }
}
