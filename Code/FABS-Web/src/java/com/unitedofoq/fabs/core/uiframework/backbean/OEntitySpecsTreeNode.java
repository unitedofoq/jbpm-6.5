package com.unitedofoq.fabs.core.uiframework.backbean;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;


/**
 *
 * @author mrashad
 */

/**
 * <p>The <code>NodeUserObject</code> represents a nodes for OEntity specification.  This
 * particular IceUserobject implementation store extra information on who is children , parents and fields of oentity
 * <p/>
 * <p>In this example pay particularly close attention to the
 * <code>wrapper</code> instance variable on IceUserObject.  The <code>wrapper</code>
 * allows for direct manipulations of the parent tree. </p>
 */
public class OEntitySpecsTreeNode extends DefaultTreeNode {


    // panel stack which will be manipulated when a command links action is fired.
    private OEntitySpecsTree tree;

    private String nodeClass;
    
    private String nodeType;

    private String nodeExpression;
    
    private String nodeColor = "black";
    
    private Class parentClass;
    
    private byte[] image;
    
    public Class getParentClass() {
        return parentClass;
    }

    public void setParentClass(Class parentClass) {
        this.parentClass = parentClass;
    } 

    public OEntitySpecsTreeNode(Object data, TreeNode parent) {
        super(data, parent);
    }
    
    public OEntitySpecsTreeNode(String type, Object data, TreeNode parent) {
        super(type, data, parent);
    }

    /**
     * Registers a user click with this object and updates the selected node in the ObjectAttributeBrowserBean.
     *
     * @param event that fired this method
     */
    public void nodeClicked() {
        tree.setSelectedNodeObject(this);
        tree.addChildren();
    }

    public String getNodeClass() {
        return nodeClass;
    }

    public void setNodeClass(String nodeClass) {
        this.nodeClass = nodeClass;
    }

    public String getNodeExpression() {
        return nodeExpression;
    }

    public void setNodeExpression(String nodeExpression) {
        this.nodeExpression = nodeExpression;
    }


    /**
     * @return the nodeType
     */
    public String getNodeType() {
        return nodeType;
    }

    /**
     * @param nodeType the nodeType to set
     */
    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    /**
     * @return the image
     */
    public byte[] getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(byte[] image) {
        this.image = image;
    }

    /**
     * @return the nodeColor
     */
    public String getNodeColor() {
        return nodeColor;
    }

    /**
     * @param nodeColor the nodeColor to set
     */
    public void setNodeColor(String nodeColor) {
        this.nodeColor = nodeColor;
    }
    
}
