package com.unitedofoq.fabs.core.uiframework.backbean;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.function.OFunctionResult;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.screen.FormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.PrsnlzdFormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.PrsnlzdTabularScreen;
import com.unitedofoq.fabs.core.uiframework.screen.SingleEntityScreen;
import com.unitedofoq.fabs.core.uiframework.screen.TabularScreen;
import com.unitedofoq.fabs.core.validation.FABSException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlPanelGrid;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author fabs
 */
@ManagedBean(name = "PrsnlzFormBean")
@ViewScoped
public class PrsnlzFormBean extends FormBean {
    
    private OScreen passedPrsnlzdOScreen;
    private OScreen copyOfOScreen ;
    private BaseEntity copyOfOScreenWithNoBoolean ;
    private BaseEntity copyOfLoadedObject ;
    private String originalOScreenDBID;
    OFunctionResult oFR = new OFunctionResult();
    
    protected UIFrameworkServiceRemote uiFrameWorkFacade = OEJB.lookup(UIFrameworkServiceRemote.class);

    public PrsnlzFormBean() {
    }
	
    @Override
    protected String getBeanName() {
        return "PrsnlzFormBean";
    }
      
    @Override
    public void init() {
        super.init();
        originalOScreenDBID = String.valueOf(dataMessage.getData().get(0));
        if (originalOScreenDBID != null) {
            try {
                loadScreenObjects();
                mode = OScreen.SM_EDIT;
            } catch (Exception ex) {
                logger.error("Exception thrown",ex);
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
                messagesPanel.clearAndDisplayMessages(oFR);
            }
        }
     }
    
    @Override
    public HtmlPanelGrid buildInnerScreen() {
        htmlPanelGrid = super.buildInnerScreen();
        return htmlPanelGrid;
    }

    @Override
    protected boolean processInput() {
        if (! super.processInput())
            return false;
        copyOfLoadedObject = createCopyOfOScreen((OScreen)loadedObject);
        return true;
    }
    
    private OScreen createCopyOfOScreen(OScreen toBeCopied) {
        logger.trace("Entering");
        OScreen copyOfLoadedOScreen = null;
        try {
            List<String> screenFieldExpressions = new ArrayList<String>();

            FABSEntitySpecs entityFABSSpecs = passedPrsnlzdOScreen.getClass().getAnnotation(FABSEntitySpecs.class);
            String[] prsnlzableFieldsNames = entityFABSSpecs.personalizableFields();
            for (int fieldsCounter = 0; fieldsCounter < prsnlzableFieldsNames.length; fieldsCounter++) {
                screenFieldExpressions.add(prsnlzableFieldsNames[fieldsCounter]);
            }

            if (passedPrsnlzdOScreen instanceof FormScreen) {
                copyOfLoadedOScreen = (FormScreen) Class.forName(FormScreen.class.getName()).newInstance();
            } else if (passedPrsnlzdOScreen instanceof TabularScreen) {
                copyOfLoadedOScreen = (TabularScreen) Class.forName(TabularScreen.class.getName()).newInstance();
            }
            copyOfLoadedOScreen.NewFieldsInstances(screenFieldExpressions);
            for (String ScrFieldExp : screenFieldExpressions) {
                FramworkUtilities.setValueInEntity(copyOfLoadedOScreen, ScrFieldExp,
                        FramworkUtilities.getValueFromEntity(toBeCopied, ScrFieldExp));
            }
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        logger.trace("Returning");
        return copyOfLoadedOScreen;
    }
     
    @Override
    public String save() {
        logger.debug("Entering");
        try {
            oFR = new OFunctionResult();
            loadScreenObjects();
            BaseEntity toBeSavedScr = determinePrsnlzdOScreenChanges(); 
            if(toBeSavedScr == null)
            {
                logger.warn("SystemInternalError, to Be saves Screen equals Null");
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
                messagesPanel.clearAndDisplayMessages(oFR);
                logger.debug("Returning with Null");
                return null;
            }
            if(passedPrsnlzdOScreen.isPrsnlzd() && copyOfOScreen != null)
            {
                // update
                BaseEntity toBeUpdateScreen = null;
                List<String> conditions = new ArrayList<String>();
                //reload copy
                conditions.add("prsnlzdOriginal_DBID = " + passedPrsnlzdOScreen.getDbid() );
                conditions.add("prsnlzdUser_DBID = " + loggedUser.getDbid());
                
                if (passedPrsnlzdOScreen instanceof TabularScreen)
                    toBeUpdateScreen = (PrsnlzdTabularScreen)oem.loadEntity(PrsnlzdTabularScreen.class.getSimpleName(),conditions , null, loggedUser);
                else if (passedPrsnlzdOScreen instanceof FormScreen)
                    toBeUpdateScreen = (PrsnlzdFormScreen)oem.loadEntity(PrsnlzdFormScreen.class.getSimpleName(),conditions , null, loggedUser);
                
                FABSEntitySpecs entityFABSSpecs = passedPrsnlzdOScreen.getClass().getAnnotation(FABSEntitySpecs.class);
                String[] prsnlzableFieldsNames = entityFABSSpecs.personalizableFields();
                if (entityFABSSpecs != null && prsnlzableFieldsNames != null) {
                    for (String personalizableField : prsnlzableFieldsNames) {
                        FramworkUtilities.setValueInEntity(toBeUpdateScreen, personalizableField, FramworkUtilities.getValueFromEntity(toBeSavedScr, personalizableField));
                    }
                }
                toBeUpdateScreen.invokeSetter("name", passedPrsnlzdOScreen.getName() + "_" + loggedUser.getDbid());
                toBeUpdateScreen.invokeSetter("omodule", passedPrsnlzdOScreen.getOmodule());               
                toBeUpdateScreen.invokeSetter("oactOnEntity", passedPrsnlzdOScreen.invokeGetter("oactOnEntity"));
                if (!passedPrsnlzdOScreen.isPrsnlzd()) {
                    toBeUpdateScreen.invokeSetter("prsnlzdOriginal_DBID", passedPrsnlzdOScreen.getDbid());
                    toBeUpdateScreen.invokeSetter("prsnlzdUser_DBID", loggedUser.getDbid());
                }
                oem.saveEntity(toBeUpdateScreen, loggedUser);
                oFR.addSuccess(usrMsgService.getUserMessage("DataSaved", loggedUser));
            }
            else
            {
                //insert
                OEntityDTO entityDTO = entitySetupService.getOScreenEntityDTO(
                        passedPrsnlzdOScreen.getDbid(), loggedUser);
                OEntity entity = entitySetupService.loadOEntity(
                        entityDTO.getEntityClassPath(), loggedUser);
                if(passedPrsnlzdOScreen instanceof FormScreen) {
                    ((PrsnlzdFormScreen)toBeSavedScr).setOactOnEntity(
                            entity);
                }
                else if(passedPrsnlzdOScreen instanceof TabularScreen) {
                    ((PrsnlzdTabularScreen)toBeSavedScr).setOactOnEntity(entity);
                }
                            
                oem.saveEntity(toBeSavedScr, loggedUser);
                    passedPrsnlzdOScreen.setPrsnlzd(true);
                oem.saveEntity(passedPrsnlzdOScreen, loggedUser);
                oFR.addSuccess(usrMsgService.getUserMessage("DataSaved", loggedUser));
            }
        }catch (Exception ex) {
            logger.debug("Returning");
                if(ex instanceof FABSException)
                    oFR.addError(usrMsgService.getUserMessage("ModuleError", loggedUser), ex);
                else
                    oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
            }finally {
            oem.closeEM(loggedUser);
            messagesPanel.clearAndDisplayMessages(oFR);
            }
            logger.debug("Returning WITH Null");
        return null;
    }
    //</editor-fold>
      
    // <editor-fold defaultstate="collapsed" desc="determinePrsnlzdOScreenChanges">
    private BaseEntity determinePrsnlzdOScreenChanges()
    {
        logger.trace("Entering");
        BaseEntity toBeSavedCopy = null;
        if(passedPrsnlzdOScreen instanceof FormScreen)
            toBeSavedCopy = new PrsnlzdFormScreen();
        else if (passedPrsnlzdOScreen instanceof TabularScreen)
            toBeSavedCopy = new PrsnlzdTabularScreen();
        try {
            FABSEntitySpecs entityFABSSpecs = passedPrsnlzdOScreen.getClass().getAnnotation(FABSEntitySpecs.class);
            String[] prsnlzableFieldsNames = entityFABSSpecs.personalizableFields();
            Object loadedFieldValue = null;
            Object currentFieldValue = null;
            Object toBeSavedFieldValue = null;
            if (entityFABSSpecs != null && prsnlzableFieldsNames != null) {
                for (String personalizableField : prsnlzableFieldsNames) {
                    Field currentField = BaseEntity.getClassField(passedPrsnlzdOScreen.getClass(), personalizableField);
                    if (passedPrsnlzdOScreen.isPrsnlzd() && copyOfOScreen != null) {
                        
                        loadedFieldValue = FramworkUtilities.getValueFromEntity(copyOfLoadedObject, personalizableField);
                        currentFieldValue = FramworkUtilities.getValueFromEntity(loadedObject, personalizableField);
                        boolean currentbool = false;
                        boolean loadedbool = false;
                        if (loadedFieldValue != null && currentFieldValue != null) {
                            if (currentField.getType() == boolean.class) {
                                currentbool = new Boolean(currentFieldValue.toString()).booleanValue();
                                loadedbool = new Boolean(loadedFieldValue.toString()).booleanValue();

                                if (!(loadedbool ^ currentbool)) {
                                    toBeSavedFieldValue = 2;
                                }else {
                                    if(currentbool)
                                        toBeSavedFieldValue = 1;
                                    else
                                        toBeSavedFieldValue = 0;
                                    
                                }
                            } else if (loadedFieldValue == currentFieldValue) {
                                toBeSavedFieldValue = null;
                            } else 
                                toBeSavedFieldValue = currentFieldValue;
                        }
                    } else // copy value is null
                    {
                        loadedFieldValue = FramworkUtilities.getValueFromEntity(copyOfLoadedObject, personalizableField);
                        currentFieldValue = FramworkUtilities.getValueFromEntity(loadedObject, personalizableField);
                        if (currentField.getType() == boolean.class) {
                            boolean currentbool = new Boolean(currentFieldValue.toString()).booleanValue();
                            boolean loadedbool = new Boolean(loadedFieldValue.toString()).booleanValue();
                            if (!(loadedbool ^ currentbool)) {
                                toBeSavedFieldValue = 2;
                            }else {
                                if(currentbool)
                                    toBeSavedFieldValue = 1;
                                else
                                    toBeSavedFieldValue = 0;
                            }
                        } else if (loadedFieldValue == currentFieldValue) {
                            toBeSavedFieldValue = null;
                        } else 
                            toBeSavedFieldValue = currentFieldValue;
                    }
                    toBeSavedCopy.invokeSetter(personalizableField, toBeSavedFieldValue);
                }
                toBeSavedCopy.invokeSetter("name", passedPrsnlzdOScreen.getName() + "_" + loggedUser.getDbid());
                toBeSavedCopy.invokeSetter("omodule", passedPrsnlzdOScreen.getOmodule());
                

                if (!passedPrsnlzdOScreen.isPrsnlzd() || (passedPrsnlzdOScreen.isPrsnlzd() && copyOfOScreen == null)) {
                    toBeSavedCopy.invokeSetter("prsnlzdOriginal_DBID", passedPrsnlzdOScreen.getDbid());
                    toBeSavedCopy.invokeSetter("prsnlzdUser_DBID", loggedUser.getDbid());
                }
            }
         }catch (Exception ex) {
            logger.error("Exception thrown",ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
            logger.trace("Returning with Null");
            return null;
        }
        logger.trace("Returning");
        return toBeSavedCopy;
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="loadObject">
    @Override
    protected OScreen loadObject(String entityName, ArrayList<String> conditions,
            List<String> fieldExpressions, OUser loggedUser) throws EntityExistsException, 
            EntityNotFoundException, NonUniqueResultException,NoResultException, 
            OptimisticLockException, RollbackException, TransactionRequiredException, UserNotAuthorizedException
    {
        logger.trace("Entering");
        OScreen returnedScreen = null;
        returnedScreen= (OScreen)oem.loadEntity(entityName, conditions, fieldExpressions, loggedUser);
        
        BaseEntity copyOScreen = null;
        if (returnedScreen.isPrsnlzd())
        {
            OUser screenUser ;
            if(loggedUser.getDbid() == 0)
                screenUser = loggedUser.getOnBehalfOf();
            else
                screenUser = loggedUser;
            
            ArrayList<String> copyConditions = new ArrayList<String>();
            copyConditions.add("prsnlzdOriginal_DBID = " + returnedScreen.getDbid());
            copyConditions.add("prsnlzdUser_DBID = " + screenUser.getDbid());
            if(returnedScreen instanceof TabularScreen)
                copyOScreen = (PrsnlzdTabularScreen) oem.loadEntity(PrsnlzdTabularScreen.class.getSimpleName(), copyConditions , null, loggedUser);
            else if (returnedScreen instanceof FormScreen)
                copyOScreen = (PrsnlzdFormScreen) oem.loadEntity(PrsnlzdFormScreen.class.getSimpleName(), copyConditions , null, loggedUser);
            if(copyOScreen != null)
            {
                OFunctionResult receivedOFR = null;
                if (returnedScreen instanceof SingleEntityScreen) {
                    receivedOFR = uiFrameWorkFacade.deductPrsnlzdOScreen(returnedScreen, copyOScreen, loggedUser);
                    returnedScreen = (OScreen) receivedOFR.getReturnValues().get(0);
                }
            }
        }
        logger.trace("Returning");
        return returnedScreen;
    }
    // </editor-fold>
    
    private void loadScreenObjects()
    {
        logger.trace("Entering");
        try {
            passedPrsnlzdOScreen = (OScreen) oem.loadEntity(OScreen.class.getSimpleName(),Collections.singletonList("dbid = '" + originalOScreenDBID + "'"),null,loggedUser);
            if(passedPrsnlzdOScreen.isPrsnlzd()){
                List<String> conditions = new ArrayList<String>();
                conditions.add("prsnlzdOriginal_DBID = '" + passedPrsnlzdOScreen.getDbid() + "'");
                conditions.add("prsnlzdUser_DBID = '" + loggedUser.getDbid() + "'");
                copyOfOScreen = (OScreen) oem.loadEntity(OScreen.class.getSimpleName(), conditions, null, loggedUser);
                if (passedPrsnlzdOScreen instanceof TabularScreen)
                   copyOfOScreenWithNoBoolean = (PrsnlzdTabularScreen) oem.loadEntity(PrsnlzdTabularScreen.class.getSimpleName(), conditions, null, loggedUser);
                else if (passedPrsnlzdOScreen instanceof FormScreen)
                    copyOfOScreenWithNoBoolean = (PrsnlzdFormScreen) oem.loadEntity(PrsnlzdFormScreen.class.getSimpleName(), conditions, null, loggedUser);
           }
        } catch (UserNotAuthorizedException ex) {
              logger.error("Exception thrown",ex);
        }
        logger.trace("Returning");
    }
    
}
