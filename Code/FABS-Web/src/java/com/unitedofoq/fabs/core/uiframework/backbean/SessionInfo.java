/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.backbean;


import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.udc.UDCServiceRemote;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
//import com.unitedofoq.otms.foundation.employee.EmployeeUser;
import java.util.List;


import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;


/**
 *
 * @author mmohamed
 */
@ManagedBean(name="SessionInfo")
@ViewScoped
public class SessionInfo extends FormBean{

    String currentHat;
    final long UDCHatType_ID = 264;

    String userNameLabelValue;
    String hatLabelValue;

    public String getHatLabelValue() {
        return hatLabelValue;
    }

    public void setHatLabelValue(String hatLabelValue) {
        this.hatLabelValue = hatLabelValue;
    }

    public String getUserNameLabelValue() {
        return userNameLabelValue;
    }

    public void setUserNameLabelValue(String userNameLabelValue) {
        this.userNameLabelValue = userNameLabelValue;
    }

    public void changeHat(ValueChangeEvent vce) {        
        SessionManager.addSessionAttribute(HAT_VAR, vce.getNewValue());
    }
    
   // UDCServiceRemote UDCService;

    List<SelectItem> hatItems;

    public List<SelectItem> getHatItems() {

        return hatItems;
    }

    public void setHatItems(List<SelectItem> hats) {
        this.hatItems = hats;
    }

    public String getCurrentHat() {
        return SessionManager.getSessionAttribute(HAT_VAR) == null? "" :SessionManager.getSessionAttribute(HAT_VAR).toString();
    }

    public void setCurrentHat(String currentHat) {
        this.currentHat = currentHat;
    }

    HtmlPanelGrid headerPanel;

    public void setHeaderPanel(HtmlPanelGrid headerPanel) {
        this.headerPanel = headerPanel;
    }

    @PostConstruct
    void postConstruct(){
        if (getLoggedUser().getFirstLanguage().getDbid() == 24) {
            userNameLabelValue = " اسم المستخدم";
            hatLabelValue = " الهيئة";
        }else{
            userNameLabelValue = "Username ";
            hatLabelValue = "Hat ";
        }
                
        try {
            otmsPage = OPortalUtil.isCurrentPageOTMS(getLoggedUser(), oem);
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
    }

    
    private OEntityManagerRemote oem = OEJB.lookup(OEntityManagerRemote.class);

    
    UDCServiceRemote UDCService = OEJB.lookup(UDCServiceRemote.class);

    public SessionInfo(){
        super();
        
    }

    public boolean isUserNOTEmployee() {
        return userNOTEmployee;
    }

    public void setUserNOTEmployee(boolean userNOTEmployee) {
        this.userNOTEmployee = userNOTEmployee;
    }



    boolean userNOTEmployee = true;

    @Override
    protected String getBeanName() {
        return "SessionInfo";
    }

    public void pref_action(ActionEvent ae) {
        String option = ae.getComponent().getId();
        System.out.println("option clicked: " + option);

        if("save_pref".equals(option))
        {
            try {
                OPortalUtil.saveCurrentPagePreferences(getLoggedUser(), oem);
            } catch (Exception ex) {
               logger.error("Exception thrown: ",ex);
            }
            otmsPage = true;
        }
        else
        {
            if("del_pref".equals(option))
            {
                try {
                    OPortalUtil.deleteCurrentPagePreferences(getLoggedUser(), oem);
                } catch (Exception ex) {
                }
            }
            otmsPage = false;
        }
    }

    private boolean otmsPage;

    public boolean isOtmsPage() {
        return otmsPage;
    }

    public void setOtmsPage(boolean otmsPage) {
        this.otmsPage = otmsPage;
    }
}
