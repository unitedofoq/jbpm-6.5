/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDLookupType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.filter.OFilterServiceLocal;
import com.unitedofoq.fabs.core.function.OFunctionResult;

import com.unitedofoq.fabs.core.setup.FABSSetup;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nkhalil
 */
@ManagedBean(name = "FABSSetupBean")
@ViewScoped
public class FABSSetupBean extends FormBean {
final static Logger logger = LoggerFactory.getLogger(FABSSetupBean.class);
    private HashMap<String, Object> fabsSetupMap = new HashMap<String, Object>();
    private HashMap<String, FABSSetup> fabsSetupMapkeys = new HashMap<String, FABSSetup>();
    private List<FABSSetup> setups = new ArrayList<FABSSetup>();

    public HashMap<String, FABSSetup> getFabsSetupMapkeys() {
        return fabsSetupMapkeys;
    }

    public void setFabsSetupMapkeys(HashMap<String, FABSSetup> fabsSetupMapkeys) {
        this.fabsSetupMapkeys = fabsSetupMapkeys;
    }

    public FABSSetupBean() {
        try {
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
    }

    @Override
    public OEntityDTO getOactOnEntity() {
        try {
            return entitySetupService.loadOEntityDTO(FABSSetup.class.getName(),
                    systemUser);
        } catch (Exception ex) {
            logger.trace("Current Screen: {}",currentScreen);
           logger.error("Exception thrown",ex);
            return null;
        }
    }

    @Override
    protected String getBeanName() {
        return FABSSetupBean.class.getSimpleName();
    }
    
    OFilterServiceLocal filterService = OEJB.lookup(OFilterServiceLocal.class);

    @Override
    public HtmlPanelGrid buildInnerScreen() {
        logger.debug("Entering");
        HtmlPanelGrid generalPanel = new HtmlPanelGrid();
        try {
            oem.getEM(loggedUser);
            //FIXME: following code to be unified in FormScreen instead as much as possible

            //<editor-fold defaultstate="collapsed" desc="generalPanel settings">
            generalPanel.setColumns(1);
            generalPanel.setCellpadding("5px");
            generalPanel.setCellspacing("0px");
            //</editor-fold>

            HtmlPanelGrid buttonsPanel = new HtmlPanelGrid();
            //<editor-fold defaultstate="collapsed" desc="buttonsPanel settings">
            buttonsPanel.setColumns(2);
            buttonsPanel.setCellpadding("5px");
            buttonsPanel.setCellspacing("0px");
            buttonsPanel.setStyle("float:right; margin-right: 20px;");
            //</editor-fold>

            messagesPanel = new UserMessagePanel(exFactory, elContext, getBeanName(), ddService, this, loggedUser);
            generalPanel.getChildren().add(messagesPanel.createMessagePanel());

            HtmlPanelGrid middlePanel = new HtmlPanelGrid();
            //<editor-fold defaultstate="collapsed" desc="middlePanel settings">
            middlePanel.setColumns(4);
            if (getLoggedUser().getFirstLanguage().getDbid() == ARABIC_VAR) {
                middlePanel.setStyle("text-align:right;");
            }
            //</editor-fold>

            HtmlCommandButton saveButton = new HtmlCommandButton();
            HtmlCommandButton saveAndExitButton = new HtmlCommandButton();
            //<editor-fold defaultstate="collapsed" desc="saveButton & saveAndExitButton settings">
            String id = "button";
            saveButton.setId(id + "_saveButton");
            saveAndExitButton.setId(id + "_saveExitButton");

            saveButton.setValue(ddService.getDD("saveButton", loggedUser).getLabelTranslated());
            saveAndExitButton.setValue(ddService.getDD("saveExitButton", loggedUser).getLabelTranslated());

            saveButton.setActionExpression(exFactory.createMethodExpression(elContext,
                    "#{" + getBeanName() + ".saveAction}", String.class, new Class[0]));
            saveAndExitButton.setActionExpression(exFactory.createMethodExpression(elContext,
                    "#{" + getBeanName() + ".saveExit}", String.class, new Class[0]));
            //</editor-fold>
            //FIXME: exchange the following buttons order
            buttonsPanel.getChildren().add(saveAndExitButton);
            buttonsPanel.getChildren().add(saveButton);

            HtmlPanelGrid valueGrid = new HtmlPanelGrid();
            middlePanel.getChildren().add(valueGrid);
            generalPanel.getChildren().add(middlePanel);
            // Hide the 'Save' and 'Save & Exit' buttons in process mode:
            if (getScreenParam("taskID") == null) {
                generalPanel.getChildren().add(buttonsPanel);
            }

            if (getCurrentScreen().getScreenFilter() != null
                    && !getCurrentScreen().getScreenFilter().isInActive()) {
                // Screen has a valid filter
                // Get groups setups
                setups = fABSSetupLocal.loadGroupSetups(filterService.getFilterConditions(
                        getCurrentScreen().getScreenFilter(),
                        getUserSessionInfo(), loggedUser), loggedUser);

            } else {
                // Screen doesn't have a valid filter
                // FIXME: replace with meaningful user message
                logger.warn("Screen doesn't have a valid filter");
                messagesPanel.addMessages(usrMsgService.getUserMessage("SystemInternalError", systemUser));
                return generalPanel;
            }

            if (getCurrentScreen().getScreenFilter() != null
                    && !getCurrentScreen().getScreenFilter().isInActive()) {
                setups = fABSSetupLocal.loadGroupSetups(filterService.getFilterConditions(
                        getCurrentScreen().getScreenFilter(), getUserSessionInfo(),
                        loggedUser), loggedUser);
            }
            if (setups != null) {
                for (FABSSetup fABSSetup : setups) {
                    fabsSetupMap.put(fABSSetup.getSetupKey(), fABSSetup);
                    fabsSetupMapkeys.put(fABSSetup.getSetupKey(), fABSSetup);
                }

                for (FABSSetup fABSSetup : setups) {
                    if (fABSSetup.getDd() == null) {
                        logger.warn("FABSSetup Key Must Have DD");
                        continue;
                    }
                    valueGrid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
                    valueGrid.setColumns(2);

                    HtmlOutputLabel valueLabel = new HtmlOutputLabel();
                    valueLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
                    valueLabel.setValueExpression("value", exFactory.createValueExpression(elContext,
                            "#{" + getBeanName() + ".fabsSetupMapkeys['" + fABSSetup.getSetupKey() + "'].dd.label}",
                            String.class));
                    valueGrid.getChildren().add(valueLabel);

                    ScreenField valueSetupScreenField = new ScreenField();
                    valueSetupScreenField.setDd(fABSSetup.getDd());
                    valueSetupScreenField.setEditable(true);
                    valueSetupScreenField.setFieldExpression(fABSSetup.getFieldExpression());
                    String valueFldExp = "#{" + getBeanName() + "."
                            + "fabsSetupMap['" + fABSSetup.getSetupKey() + "']" + "}";
                    UIComponent component = null;
                    long ctrlTypeDBID = fABSSetup.getDd().getControlType().getDbid();
                    if (ctrlTypeDBID == DD.CT_TEXTFIELD) {
                        component = createTextFieldControl(fABSSetup.getDd().getControlSize(),
                                "setupfld_fld" + fABSSetup.getDbid(),
                                valueFldExp, true, fABSSetup.getDd(), 0, false, false , false);
                        fabsSetupMap.put(fABSSetup.getSetupKey(), fABSSetup.getSvalue());
                    } else if (ctrlTypeDBID == DD.CT_CHECKBOX) {
                        component = createCheckBoxControl("setupfld_fld" + fABSSetup.getDbid(),
                                valueFldExp, true, fABSSetup.getDd(), 0, false);
                        fabsSetupMap.put(fABSSetup.getSetupKey(), fABSSetup.isBvalue());
                    } else if (ctrlTypeDBID == DD.CT_CALENDAR) {
                        component = createCalendarControl("setupfld_fld" + fABSSetup.getDbid(),
                                valueFldExp, 0,
                                true, fABSSetup.getDd(), 0, false);
                        fabsSetupMap.put(fABSSetup.getSetupKey(), Date.parse(fABSSetup.getSvalue()));
                    } else if (ctrlTypeDBID == DD.CT_DROPDOWN
                            || ctrlTypeDBID == DD.CT_LOOKUPSCREEN) {
                        HtmlPanelGrid lookupPanelGrid = new HtmlPanelGrid();
                        lookupPanelGrid.setId(FacesContext.getCurrentInstance().
                                getViewRoot().createUniqueId());
                        String lookupExp = null;
                        if ((fABSSetup.getDd().getLookupDropDownDisplayField() != null
                                && !fABSSetup.getDd().getLookupDropDownDisplayField().equals("")
                                && fABSSetup.getDd().getLookupType() == DDLookupType.DDLT_DRPDWN)
                                || (null != fABSSetup.getDd().getLookupFilter() && !"".equals(fABSSetup.getDd().getLookupFilter()))
                                && fABSSetup.getDd().getLookupType() == DDLookupType.DDLT_LTRLDRPDN) {
                            lookupExp = "";
                            BaseEntity currentObject = null;
                            currentObject = getLookupBaseEntity(fABSSetup.getFieldExpression(), fABSSetup.getSvalue(), fABSSetup.getDd());
                            fabsSetupMap.remove(fABSSetup.getSetupKey());
                            fabsSetupMap.put(fABSSetup.getSetupKey(), currentObject);
                            //        , currentObject);
                        } else {
                            lookupExp = fABSSetup.getFieldExpression().substring(
                                    fABSSetup.getFieldExpression().indexOf(".") + 1);
                            BaseEntity currentObject = null;
                            currentObject = getLookupBaseEntity(fABSSetup.getFieldExpression(), fABSSetup.getSvalue(), fABSSetup.getDd());
                            fabsSetupMap.remove(fABSSetup.getSetupKey());
                            fabsSetupMap.put(fABSSetup.getSetupKey(), currentObject);
                        }

                        valueFldExp = getBeanName() + "." + "fabsSetupMap['" + fABSSetup.getSetupKey() + "']";
                        String lookupID = fABSSetup.getSetupKey() + "_Lookup_" + fABSSetup.getDbid();
                        fieldLookup = new Lookup(this,
                                getBeanName(),
                                valueFldExp,
                                fABSSetup.getDd(),
                                lookupExp,
                                null,
                                lookupID,
                                valueSetupScreenField,
                                false,
                                oem, filterServiceBean,
                                exFactory,
                                getFacesContext(),
                                loggedUser);

                        getLookupHashMap().put(lookupID, fieldLookup);

                        if ((fABSSetup.getDd().getLookupDropDownDisplayField() != null
                                && !fABSSetup.getDd().getLookupDropDownDisplayField().equals("")
                                && fABSSetup.getDd().getLookupType() == DDLookupType.DDLT_DRPDWN)
                                || (null != fABSSetup.getDd().getLookupFilter() && !"".equals(fABSSetup.getDd().getLookupFilter()))
                                && fABSSetup.getDd().getLookupType() == DDLookupType.DDLT_LTRLDRPDN) {
                            lookupPanelGrid.getChildren().add(fieldLookup.createDropDown(false));
                            getSelectItemsList().put(fieldLookup.getDropDownMenu().getId(),
                                    fieldLookup.getSelectItems());

                        } else {
                            lookupPanelGrid.getChildren().add(fieldLookup.createNonDropDown());
                            FramworkUtilities.setControlFldExp(fieldLookup.getInputText(), valueSetupScreenField.getFieldExpression());
                        }

                        component = lookupPanelGrid;
                    } else {
                        component = createTextFieldControl(20, "setupfld_fld" + fABSSetup.getDbid(),
                                valueFldExp, true, valueSetupScreenField.getDd(), 0, false, false ,false);
                    }
                    valueGrid.getChildren().add(component);
                }
            } else {
            }

            restorePDataFromSessoion = false;
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(
                    "top." + getPortletInsId() + "DataRestored();");

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            OFunctionResult oFR = new OFunctionResult();
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
            messagesPanel.addMessages(oFR);

        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return generalPanel;
        }
    }

    public HashMap<String, Object> getFabsSetupMap() {
        return fabsSetupMap;
    }

    public void setFabsSetupMap(HashMap<String, Object> fabsSetupMap) {
        this.fabsSetupMap = fabsSetupMap;
    }

    //FIXME: move this function to DDService & unify in system
    private BaseEntity getLookupBaseEntity(String fieldExpression, String value, DD setupDD) {
        logger.debug("Entering");
        try {
            OEntityDTO entity = entitySetupService.loadOEntityDTO(
                    fieldExpression.substring(0, fieldExpression.indexOf(".")), systemUser);
            Class setupClass = Class.forName(entity.getEntityClassPath());
            String fldExp = fieldExpression.substring(fieldExpression.indexOf(".") + 1);
            List<ExpressionFieldInfo> infos = BaseEntity.parseFieldExpression(setupClass, fldExp);
            ExpressionFieldInfo info = infos.get(infos.size() - 1);
            if (value != null && !value.equals("")) {
                if (info.field.getType().equals(String.class)) {
                    if (setupDD.getControlType().getDbid() == DD.CT_LOOKUPSCREEN) {
                        logger.debug("Returning");
                        return (BaseEntity) oem.loadEntity(fieldExpression.substring(0, fieldExpression.indexOf(".")),
                                Collections.singletonList("dbid"
                                + " =" + value), null, loggedUser);
                    } else if (setupDD.getControlType().getDbid() == DD.CT_DROPDOWN) {
                        List<String> condts = new ArrayList<String>();
                        condts.add(fldExp + " like '" + value + "'");
                        condts.add(setupDD.getLookupFilter());
                        logger.debug("Returning");
                        return (BaseEntity) oem.loadEntity(fieldExpression.substring(0, fieldExpression.indexOf(".")),
                                condts, null, loggedUser);
                    } else {
                        logger.debug("Returning");
                        return (BaseEntity) oem.loadEntity(fieldExpression.substring(0, fieldExpression.indexOf(".")),
                                Collections.singletonList(fldExp
                                + " like '" + value + "'"), null, loggedUser);
                    }

                } else {
                    logger.debug("Returning");
                    return (BaseEntity) oem.loadEntity(fieldExpression.substring(0, fieldExpression.indexOf(".")),
                            Collections.singletonList(fldExp
                            + " = " + value), null, loggedUser);
                }

            } else {
                logger.debug("Returning");
                return (BaseEntity) setupClass.newInstance();
            }

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    public String saveAction() {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            oem.closeEM(loggedUser);
            OEntityDTO setupOEntity = entitySetupService.loadOEntityDTOByClassName(
                    "FABSSetup", systemUser);
            OEntityActionDTO updateAction = entitySetupService.getOEntityDTOActionDTO(
                    setupOEntity.getDbid(), OEntityActionDTO.ATDBID_UPDATE, loggedUser);                    
            OEntityActionDTO createAction = entitySetupService.getOEntityDTOActionDTO(
                    setupOEntity.getDbid(), OEntityActionDTO.ATDBID_CREATE, loggedUser);                    
            for (String setupKey : fabsSetupMapkeys.keySet()) {
                FABSSetup setup = fabsSetupMapkeys.get(setupKey);
                if (setup.getDd() == null) {
                    logger.warn("FABSSetup Key Must Have DD");
                    continue;
                }

                if (fabsSetupMapkeys.get(setupKey).getDd().getControlType().getDbid()
                        == DD.CT_TEXTFIELD) {
                    String sValue = (String) fabsSetupMap.get(setupKey);
                    if (sValue.equals(setup.getSvalue())) {
                        continue;
                    }
                    setup.setSvalue(sValue.toString());
                    oFR.append(fABSSetupLocal.saveSetup(setup, updateAction, loggedUser));
                } else if (fabsSetupMapkeys.get(setupKey).getDd().getControlType().getDbid()
                        == DD.CT_CALENDAR) {
                    Date sValue = (Date) fabsSetupMap.get(setupKey);
                    if (sValue.toString().equals(setup.getSvalue())) {
                        continue;
                    }
                    setup.setSvalue(sValue.toString());
                    oFR.append(fABSSetupLocal.saveSetup(setup, updateAction, loggedUser));
                } else if (fabsSetupMapkeys.get(setupKey).getDd().getControlType().getDbid()
                        == DD.CT_CHECKBOX) {
                    boolean bValue = Boolean.parseBoolean(fabsSetupMap.get(setupKey).toString());
                    if (bValue == setup.isBvalue()) {
                        continue;
                    }
                    setup.setBvalue(bValue);
                    oFR.append(fABSSetupLocal.saveSetup(setup, createAction, loggedUser));
                    oFR.append(fABSSetupLocal.saveSetup(setup, updateAction, loggedUser));
                } else if (fabsSetupMapkeys.get(setupKey).getDd().getControlType().getDbid()
                        == DD.CT_DROPDOWN) {
                    String sValue = BaseEntity.getValueFromEntity(fabsSetupMap.get(setupKey),
                            setup.getFieldExpression().substring(
                            setup.getFieldExpression().indexOf(".") + 1)).toString();
                    if (sValue.equals(setup.getSvalue())) {
                        continue;
                    }
                    setup.setSvalue(sValue);
                    oFR.append(fABSSetupLocal.saveSetup(setup, updateAction, loggedUser));
                } else if (fabsSetupMapkeys.get(setupKey).getDd().getControlType().getDbid()
                        == DD.CT_LOOKUPSCREEN) {
                    String sValue = BaseEntity.getValueFromEntity(fabsSetupMap.get(setupKey),
                            "dbid").toString();
                    if (sValue.equals(setup.getSvalue())) {
                        continue;
                    }
                    setup.setSvalue(sValue);
                    oFR.append(fABSSetupLocal.saveSetup(setup, updateAction, loggedUser));
                }
            }

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            oem.closeEM(loggedUser);
            messagesPanel.clearAndDisplayMessages(oFR);
        }
        logger.debug("returning with Null");
        return null;
    }

    @Override
    public String returnSelected() {
        logger.debug("Entering");
        if (getLookupData() != null) {
            String mapKey = fieldLookup.getId().substring(0, fieldLookup.getId().indexOf("_Looku"));
            fabsSetupMap.remove(mapKey);
            fabsSetupMap.put(mapKey, getLookupData());
            setLookupPanelRendered(false);
        }
        logger.debug("Returning with Null");
        return null;
    }

    @Override
    public void saveExit() {
        logger.debug("Entering");
        saveAction();

        if (exit) {
            if (openedAsPopup) {
                logger.trace("Calling JS closePopupNoID with PortletInsId: {}",getPortletInsId());
                String param = currentScreen.getName();
                RequestContext.getCurrentInstance().execute("top.FABS.Portlet.closePopupNoID('" + param + "');");
            } else {
                OPortalUtil.removePortlet(getPortletInsId(), screenInstId);
            }
        }
        logger.debug("Returning");
    }
}
