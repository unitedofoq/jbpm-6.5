/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.websocket;

import javax.websocket.Session;

/**
 *
 * @author mmasoud
 */
public class WebSocketSession {
    
    private Session session;
    private String userLoginName;

    public WebSocketSession() {
    }

    public WebSocketSession(Session session, String userLoginName) {
        this.session = session;
        this.userLoginName = userLoginName;
    }
    
    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public String getUserLoginName() {
        return userLoginName;
    }

    public void setUserLoginName(String userLoginName) {
        this.userLoginName = userLoginName;
    }
}
