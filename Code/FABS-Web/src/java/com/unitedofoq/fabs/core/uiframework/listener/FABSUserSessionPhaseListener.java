package com.unitedofoq.fabs.core.uiframework.listener;

import com.liferay.portal.kernel.cache.SingleVMPoolUtil;
import org.primefaces.context.RequestContext;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.RequestDispatcher;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * JSF Phase Listener for sync FABS Session wz LF session
 *
 * FABS-Web is not a portal appm but a normal JSF application integrated with Liferay through an IFrame,
 * which results in having two separate sessions active in the same time one for Liferay and the other for FABS-Web
 *
 * To make the two session act as one, by making sure that LF session is active, and If no LF active session found
 * invalidates current FABS session, and display error message
 *
 */
public class FABSUserSessionPhaseListener  implements PhaseListener{

    @Override
    public void afterPhase(PhaseEvent phaseEvent) {

    }

    /**
     * Called before the phase executed, checks for the LF active session, if found continue, otherwise timeout
     */
    @Override
    public void beforePhase(PhaseEvent event) {
        logger.log(Level.INFO, "In FABSUserSessionPhaseListener.beforePhase");

        FacesContext fc = event.getFacesContext();
        ExternalContext ec = fc.getExternalContext();

        try {
            String userId = ec.getRequestParameterMap().get("UD");
            if( userId == null ) { // if postBack get the userId from session, no request params passed.
                userId =  String.valueOf(ec.getSessionMap().get("USERID"));
//            } else{ // new request, get userId from request parameters
//                userId =  ;
                // add userId to session to use later in a postBack requests.
                //ec.getSessionMap().put("P_UD", userId);
            }
            if ( ! userLFSessionActive(userId) ) {
                //user doesn't have an active session, due to logout or session timeout
                    clearSessionAndDisplayError(userId);
            }
        }catch (Exception e){
            logger.log(Level.SEVERE, "exception while checking for LF active session", e);
        }
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }


    /**
     * handles timeout for the current session:
     *  - Clear Session attributes
     *  - Redirect to error page, displaying the session timeout message
     * @param userId current LF userId
     */
    private void clearSessionAndDisplayError(String userId){
        logger.log(Level.WARNING, "Session Timed Out for user {0}, redirect to error page!", userId);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Flash flash = facesContext.getExternalContext().getFlash();
        flash.put("errorMessage", "Session Time Out : Please Reload the Current Page . ");
        try {
            // Clear Session instead of session invalidation
            facesContext.getExternalContext().getSessionMap().clear();
            // redirect to error page
            String errorPageLocation = "/errorPage.iface";
            facesContext.setViewRoot(facesContext.getApplication().getViewHandler().createView(facesContext, errorPageLocation));
            facesContext.getPartialViewContext().setRenderAll(true);
            facesContext.renderResponse();
        } catch(Exception e){
            logger.log(Level.SEVERE, "Exception redirecting to error page",e);
        }
    }

    /**
     * @param userId current LF userId
     * @return true if the current user have LF active session
     */
    private boolean userLFSessionActive(String userId){
        logger.log(Level.INFO, "check if user {0} is LoggedIn to Liferay, and the session is active", userId );
        return SingleVMPoolUtil.getCache("sessionCache").get(userId) != null;
    }

    private Logger logger = Logger.getLogger(FABSUserSessionPhaseListener.class.getName());

}
