/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;

import com.unitedofoq.fabs.core.setup.FABSSetup;
import com.unitedofoq.fabs.core.uiframework.backbean.wizard.WizardPanel;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.el.MethodExpression;
import javax.el.ValueExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.context.FacesContext;
import org.primefaces.component.tree.Tree;
import org.primefaces.component.tree.UITreeNode;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mrashad
 */
@ManagedBean(name = "OEntitySpecsTreeBean")
@ViewScoped
public class OEntitySpecsTreeBean extends SingleEntityBean {
    final static Logger logger = LoggerFactory.getLogger(OEntitySpecsTreeBean.class);
    // tree default model, used as a value for the tree component
    private OEntitySpecsTree oentityTree;
    private OEntityDTO  entity;
    private List<String> conditions ;
   
    private OEntitySpecsTreeNode root;
    private OEntitySpecsTreeNode selectedNode;
   
    
    @Override
    public void init() {
        super.init();
        if(dataMessage != null){
            entity = (OEntityDTO)  dataMessage.getData().get(0);
        }
     }

    @Override
    public void nodeActionListener(NodeSelectEvent event) {
        try {
            oentityTree.nodeActionListener(event);    
            setSelectedNode(oentityTree.getSelectedNodeObject());
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            UserMessage errorMsg = usrMsgService.getUserMessage("SystemInternalError", systemUser);
            errorMsg.setException(ex);
            messagesPanel.clearAndDisplayMessages(errorMsg);
        }
    }
    
    public void nodeExpandListener(NodeExpandEvent event) {
          try {
            oentityTree.nodeExpandListener(event);            
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            UserMessage errorMsg = usrMsgService.getUserMessage("SystemInternalError", systemUser);
            errorMsg.setException(ex);
            messagesPanel.clearAndDisplayMessages(errorMsg);
        }
    }

    @Override
    protected String getBeanName() {
        return OEntitySpecsTreeBean.class.getSimpleName();
    }

    @Override
    protected boolean processInput() {
        try {
            if (!super.processInput()) {
                return false;
            }
            oentityTree = new OEntitySpecsTree(Class.forName(entity.getEntityClassPath()), loggedUser,
                    entitySetupService, ddService, oem);
            root = new OEntitySpecsTreeNode("Root", null);
            oentityTree.createRootNode(root);
            oentityTree.setSelectedNodeObject(root);
            oentityTree.getSelectedNodeObject().setExpanded(true);
            setSelectedNode(root);
            getSelectedNode().setExpanded(true);
            oentityTree.addChildren();
            return true;

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        return false;
    }

    
    @Override
    public void loadAttribute(String expression) {
    }
    
    
    @Override
    public ODataMessage saveProcessRecords() {
        return null;
    }

    @Override
    protected BaseEntity getObjectForLookup(long dbid) {
        return null;
    }
    
    @Override
    protected void buildScreenExpression(UIComponent component) {
    }

    @Override
    protected List<BaseEntity> getLoadedEntity() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    //Tree myTree = new Tree();
    
    @Override
    protected HtmlPanelGrid buildInnerScreen() {
        logger.trace("Entering");
        Long timeBeforeEM = (Calendar.getInstance()).getTimeInMillis() ;
        htmlPanelGrid = new HtmlPanelGrid();
        
        messagesPanel = new UserMessagePanel(exFactory, elContext, getBeanName(), ddService, this, loggedUser);
        FABSSetup fabsSetup = null;
        try {
            List<String> setupCondtions = new ArrayList<String>();
                setupCondtions.add("setupKey = 'WebContext'");
                // FIXME: possibility to get NonUniqueResultException(); manage that
                fabsSetup = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                        setupCondtions, null, oem.getSystemUser(loggedUser));
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            messagesPanel.addErrorMessage(
                    usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
        }
        
        //
        processInput();
        
        HtmlPanelGroup buttonsPanel = new HtmlPanelGroup();
        HtmlPanelGrid buttonsGrid = new HtmlPanelGrid();
        buttonsGrid.setId(getFacesContext().getViewRoot().createUniqueId() + "_ButtonsGrid");
        createScreenActionsButtons(buttonsGrid);
        htmlPanelGrid.getChildren().add(buttonsGrid);

        Tree myTree = new Tree();
        myTree.setVar("item");
        myTree.setId("oentitySpecsTree");
        //Set the value
        myTree.setValueExpression("value",
                exFactory.createValueExpression(elContext, "#{" + getBeanName() + ".root}", OEntitySpecsTreeNode.class));
        //Set Node Selection Event
        Class[] selectionEvent = new Class[]{NodeSelectEvent.class};
        MethodExpression selectMB = exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                "#{" + getBeanName() + ".nodeActionListener}", null, selectionEvent);
        FABSAjaxBehavior selectionBehavior = new FABSAjaxBehavior();
        selectionBehavior.setListener(selectMB);
        selectionBehavior.setUpdate("oentitySpecsTree");
        
        myTree.addClientBehavior("select", selectionBehavior);
        myTree.setDynamic(true);
//        //Selection Properties
        ValueExpression selectedNodesVe = exFactory.
            createValueExpression(elContext, "#{" + getBeanName() + ".selectedNode}",OEntitySpecsTreeNode.class);
        myTree.setSelectionMode("single");
        myTree.setValueExpression("selection", selectedNodesVe);
        
        /*************************************************
         * Define Node properties
         *************************************************/
        
        UITreeNode treeNodeFolder = new UITreeNode();
        treeNodeFolder.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()+
                "_nodeFolder");
        ValueExpression nodeTxtVe = exFactory.
            createValueExpression(elContext, "#{item}", String.class);
        //treeNode.setType("document");
        /*************************************************
         * Define the display label
         *************************************************/        
        HtmlOutputText nodeText = new HtmlOutputText();
        HtmlOutputText nodeText2 = new HtmlOutputText();
        nodeText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()+
                "_nodeTxt");        
        nodeText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()+
                "_nodeTxt2");    
        nodeText.setValueExpression("value", nodeTxtVe);
        nodeText2.setValueExpression("value", nodeTxtVe);
        
        treeNodeFolder.getChildren().add(nodeText);
        myTree.getChildren().add(treeNodeFolder);
        htmlPanelGrid.getChildren().add(myTree);
        
        htmlPanelGrid.getChildren().add(messagesPanel.createMessagePanel());
        htmlPanelGrid.getChildren().add(myTree);
         if (wizardMode) {
            wizardPanel = new WizardPanel();
            HtmlPanelGrid wizardSeparator = new HtmlPanelGrid();
            wizardSeparator.setId(getFacesContext().getViewRoot().createUniqueId() + "WizardSep");
            wizardSeparator.setStyleClass("wizard_separator");
            htmlPanelGrid.getChildren().add(wizardSeparator);
            htmlPanelGrid.getChildren().add(wizardPanel.createWizardPanel(getBeanName(), exFactory, elContext, wizardInfo, true, ddService, loggedUser));
        }
        onloadJSStr += "top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');";
        logger.trace("Returning");
        return htmlPanelGrid;
    }

    /**
     * @return the root
     */
    public OEntitySpecsTreeNode getRoot() {
        return root;
    }

    /**
     * @param root the root to set
     */
    public void setRoot(OEntitySpecsTreeNode root) {
        this.root = root;
    }

    /**
     * @return the selectedNode
     */
    public OEntitySpecsTreeNode getSelectedNode() {
        return selectedNode;
    }

    /**
     * @param selectedNode the selectedNode to set
     */
    public void setSelectedNode(OEntitySpecsTreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    @Override
    public void addClaimButtonToPanelGroup(String buttonID, String buttonLabel, String actionMethodName, HtmlPanelGrid panelLayout, Class[] params, String portletInsId, String beanName, String screenLanguage, boolean invalidInputMode, OScreen currentScreen) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
