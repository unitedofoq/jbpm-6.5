/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.central.OTenant;
import com.unitedofoq.fabs.core.uiframework.entitydesigner.dto.OTenantDTO;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mahmed
 */
@ViewScoped
@ManagedBean(name = "ManageAdminTenantBean")
public class ManageAdminTenantBean {

    @EJB
    private OCentralEntityManagerRemote centEM;
    final static Logger logger = LoggerFactory.getLogger(ManageAdminTenantBean.class);

    private String loginName;
    private List<OTenantDTO> userTenants;

    @PostConstruct
    private void onInit() {
        Map<String, String> requestParams = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
        String userId = requestParams.get("UD");
        loginName = OPortalUtil.getUserLoginName(Long.parseLong(userId));
        loadTenantList();
    }

    /**
     * Load all tenants
     */
    private void loadTenantList() {
        List<OTenant> allTenants = centEM.getAllTenants();
        userTenants = new ArrayList<>();
        for (OTenant tenant : allTenants) {
            OTenantDTO oTenantDTO = new OTenantDTO();
            oTenantDTO.setId(tenant.getId());
            oTenantDTO.setCompanyName(tenant.getCompanyName());
            oTenantDTO.setName(tenant.getName());
            userTenants.add(oTenantDTO);
        }
    }

    /**
     * Action Listener to update company name based on the selectedTenantId.
     */
    public void onRowEdit(RowEditEvent event) {
        OTenantDTO oTenantDTO = (OTenantDTO) event.getObject();
        logger.debug("Updated tenant id is:  %s", oTenantDTO.getId());
        boolean flag = true;
        List<OTenant> allTenants = centEM.getAllTenants();
        for (OTenant tenant : allTenants) {
            if (tenant.getCompanyName() != null) {
                if (tenant.getCompanyName().equalsIgnoreCase(oTenantDTO.getCompanyName()) && tenant.getId() != oTenantDTO.getId()) {
                    flag = false;
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Can't duplicate company name ", "Not supported"));
                    break;
                }
            }
        }
        if (flag) {
            try {
                centEM.updateTenantCompanyName(oTenantDTO.getId(), oTenantDTO.getCompanyName());
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Can't duplicate company name ", "Not supported"));
            }
        }
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public List<OTenantDTO> getUserTenants() {
        return userTenants;
    }

    public void setUserTenants(List<OTenantDTO> userTenants) {
        this.userTenants = userTenants;
    }

}
