/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author mibrahim
 */
public class BaseEntityDataModel extends ListDataModel<BaseEntity> implements SelectableDataModel<BaseEntity> {

    public BaseEntityDataModel(List<BaseEntity> list) {
        super(list);
    }

    public BaseEntityDataModel() {
    }
    

    @Override
    public Object getRowKey(BaseEntity t) {
        return t.getInstID();
    }

    @Override
    public BaseEntity getRowData(String rowKey) {
        List<BaseEntity> baseEntitys = (List<BaseEntity>) getWrappedData();  
          
        for(BaseEntity baseEntity : baseEntitys) {  
            if(baseEntity.getInstID().equals(rowKey))  
                return baseEntity;  
        }  
          
        return null; 
    }
  
    
}
