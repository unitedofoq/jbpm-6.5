/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.portal;

/**
 *
 * @author mibrahim
 */
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Layout;
import com.liferay.portal.model.LayoutTypePortlet;
import com.liferay.portal.model.Portlet;
import com.liferay.portal.model.PortletConstants;
import com.liferay.portal.model.ResourceConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserTracker;
import com.liferay.portal.security.ldap.PortalLDAPImporterUtil;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.service.PortletPreferencesLocalServiceUtil;
import com.liferay.portal.service.ResourceLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.permission.PortletPermissionUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portal.util.PortletKeys;
import com.liferay.portlet.calendar.model.CalEvent;
import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.setup.FABSSetup;
import com.unitedofoq.fabs.core.setup.FABSSetupLite;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.udc.UDCServiceRemote;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.backbean.OBackBean;
import com.unitedofoq.fabs.core.uiframework.backbean.SessionManager;
import com.unitedofoq.fabs.core.uiframework.orgchart.OrgChartFunction;
import com.unitedofoq.fabs.core.uiframework.orgchart.OrgChartFunctionLite;
import com.unitedofoq.fabs.core.uiframework.screen.FormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.OScreenLite;
import com.unitedofoq.fabs.core.uiframework.screen.TabularScreen;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mibrahim
 */
public class OPortalUtil {

    public static RequestContext reqContext;
    final static Logger logger = LoggerFactory.getLogger(OPortalUtil.class);
    public static String portalPageID;

    /**
     * Check if the portlet is added to layout or not
     *
     * @param portletName
     * @return true if the portlet already added, false if not.
     *
     */
    public static boolean isPortletAdded(String portletName) {
        logger.debug("Entering");
        if (portletName == null || "".equals(portletName)) {
            logger.debug("Returning with false");
            return false;
        }
        try {
            String lPLID = (String) SessionManager.getSessionAttribute("layoutPLID");
            List<String> screens = null;
            if (SessionManager.getSessionAttribute(OPortalUtil.PAGESCREENS + "_" + lPLID) != null) {
                screens = (List<String>) SessionManager.getSessionAttribute(OPortalUtil.PAGESCREENS + "_" + lPLID);
            } else {
                screens = new ArrayList<String>();
            }
            if (screens.size() > 0) {
                for (String screenName : screens) {
                    if (screenName.equalsIgnoreCase(portletName)) {
                        logger.debug("Returning with true");
                        return true;
                    }
                }
            }
            screens.add(portletName);
            SessionManager.addSessionAttribute(OPortalUtil.PAGESCREENS + "_" + lPLID, screens);

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }
        Layout cLayout = getCurrentLayout();
        if (cLayout == null) {
            logger.debug("Returning with false");
            return false;
        }

        List<Portlet> portlets;
        try {
            portlets = ((LayoutTypePortlet) cLayout.getLayoutType()).getAllPortlets();
            long ownerId = PortletKeys.PREFS_OWNER_ID_DEFAULT;
            int ownerType = PortletKeys.PREFS_OWNER_TYPE_LAYOUT;
            for (Portlet portlet : portlets) {
                try {
                    if (!"OTMSPortlet".equals(portlet.getPortletName())) {
                        continue;
                    }
                    PortletPreferences prefs = PortletPreferencesLocalServiceUtil.getPreferences(
                            cLayout.getCompanyId(), ownerId, ownerType, cLayout.getPlid(), portlet.getPortletId());
                    if (prefs == null) {
                        continue;
                    }
                    String pScreenName = prefs.getValue("screenName", null);
                    if (pScreenName == null || "".equals(pScreenName)) {
                        continue;
                    }
                    if (portletName.equalsIgnoreCase(pScreenName)) {
                        logger.debug("Returning with true");
                        return true;
                    }
                } catch (Exception ex) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.error("Exception thrown: ", ex);
                    // </editor-fold>
                }
                if (portletName.equals(portlet.getPortletName())) {
                    logger.debug("Returning with true");
                    return true;
                }
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown");
            // </editor-fold>
        }
        logger.debug("Returning with false");
        return false;
    }

    /**
     * Clear all session data stored for this portlet.
     *
     * @param portletId
     */
    public synchronized static void removePortletSessionData(String portletId) {
        logger.debug("Entering");
        try {
            if (portletId == null || "".equals(portletId)) {
                logger.debug("Returning");
                return;
            }
            Layout cLayout = getCurrentLayout();
            if (cLayout == null) {
                logger.debug("Returning");
                return;
            }
            String portletName = portletId;
            if (portletId.lastIndexOf("_WAR") > -1) {
                portletName = portletId.substring(0, portletId.lastIndexOf("_WAR"));
            }
            // Get Portlet Preferences
            if (portletId.contains("OTMSPortlet")) {
                PortletPreferences prefs
                        = PortletPreferencesLocalServiceUtil.getPreferences(cLayout.getCompanyId(),
                                PortletKeys.PREFS_OWNER_ID_DEFAULT, PortletKeys.PREFS_OWNER_TYPE_LAYOUT,
                                cLayout.getPlid(), portletId);
                if (prefs == null) {
                    logger.debug("Returning");
                    return;
                }
                String pScreenName = prefs.getValue("screenName", null);
                if (pScreenName == null || "".equals(pScreenName)) {
                    logger.debug("Returning");
                    return;
                }
                portletName = pScreenName;
            }
            try {
                String lPLID = (String) SessionManager.getSessionAttribute("layoutPLID");
                List<String> screens = null;
                if (SessionManager.getSessionAttribute(OPortalUtil.PAGESCREENS + "_" + lPLID) != null) {
                    screens = (List<String>) SessionManager.getSessionAttribute(OPortalUtil.PAGESCREENS + "_" + lPLID);
                    screens.remove(portletName);
                    SessionManager.addSessionAttribute(OPortalUtil.PAGESCREENS + "_" + lPLID, screens);
                }
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown: ", ex);
                // </editor-fold>
            }
            // Remove any session attributes for this portlet, Also Remove Portlet from Render Group
            SessionManager.addSessionAttribute(portletName, null);
            String lPLID = (String) SessionManager.getSessionAttribute("layoutPLID");
            SessionManager.addSessionAttribute(portletName + "_" + lPLID, null);
            //==========
            // Remove Portlet From Render Group
            removeScreenFromRenderGroup(portletName);
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }
        logger.debug("Returning");
    }

    public synchronized static void removePortletSessionData(String portletId, String screenInstId) {
        logger.debug("Entering");
        try {
            try {
                String lPLID = (String) SessionManager.getSessionAttribute("layoutPLID");
                List<String> screens = null;
                if (SessionManager.getSessionAttribute(OPortalUtil.PAGESCREENS + "_" + lPLID) != null) {
                    screens = (List<String>) SessionManager.getSessionAttribute(OPortalUtil.PAGESCREENS + "_" + lPLID);
                    screens.remove(screenInstId);
                    SessionManager.addSessionAttribute(OPortalUtil.PAGESCREENS + "_" + lPLID, screens);
                }
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown: ", ex);
                // </editor-fold>
            }
            // Remove any session attributes for this portlet, Also Remove Portlet from Render Group
            if (screenInstId.endsWith("_wizard")) {
                return;
            }

            SessionManager.addSessionAttribute(screenInstId, null);
            String lPLID = (String) SessionManager.getSessionAttribute("layoutPLID");
            SessionManager.addSessionAttribute(screenInstId + "_" + lPLID, null);
            //==========
            // Remove Portlet From Render Group
            removeScreenFromRenderGroup(screenInstId);
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }
        logger.debug("Returning");
    }

    /**
     * Remove Portlet using JS code, also clear all session data stored for this
     * portlet by calling removePortletSessionData(portletID) function.
     * <BR><B>PS</B> this Method only remove current port
     *
     * @param portletID
     */
    public static void removePortlet(String portletID, String screenInstId) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.debug("portletID: {}", portletID);
        // </editor-fold>
        String closePortletFName = "top.FABS.Portlet.closePortletAction('" + portletID + "');";
        RequestContext.getCurrentInstance().execute(closePortletFName);
        removePortletSessionData(portletID, screenInstId);
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.debug("portletID: {}", portletID);
        logger.debug("Returning");
        // </editor-fold>
    }

    public static void removeProcessPortlet(String callerScreenID, String tobeRefreshedID, String tobeClosedID) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.debug("portletID: {}", tobeClosedID);
        // </editor-fold>
        String closePortletFName
                = "top.FABS.Portlet.closePortletAndRefreshAnother({screenName:'" + tobeClosedID
                + "',screenInstId:'" + tobeRefreshedID + "'});";
        RequestContext.getCurrentInstance().execute(closePortletFName);
        removePortletSessionData(tobeClosedID, callerScreenID);
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.debug("portletID: {}", tobeClosedID);
        logger.debug("Returning");
        // </editor-fold>
    }

    /**
     *
     * @param portletId
     * @param requestMap
     * @deprecated use removePortlet(String portletID)
     */
    public static void removePortlet(String portletId, Map requestMap) {
        logger.debug("Entering");
        ThemeDisplay themeDisplay = (ThemeDisplay) requestMap.get(WebKeys.THEME_DISPLAY);
        Layout layout = themeDisplay.getLayout();

        boolean deletePortlet = false;
        try {
            layout = LayoutLocalServiceUtil.getFriendlyURLLayout(layout.getGroupId(), layout.isPrivateLayout(), layout.getFriendlyURL());
            portletId = portletId + "_WAR_OTMSDemo";

            LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
            List<String> portletsId = layoutTypePortlet.getPortletIds();
            for (String portletID : portletsId) {
                System.out.println("portletID: " + portletID);
            }

            layoutTypePortlet.removePortletId(themeDisplay.getUserId(), "new");

            if (layoutTypePortlet.hasPortletId(portletId)) {
                System.out.println("Portal do have a portlet ID: " + portletId);
                System.out.println("themeDisplay.getUserId(): " + themeDisplay.getUserId());
                deletePortlet = true;
                layoutTypePortlet.removePortletId(themeDisplay.getUserId(), portletId);
                LayoutLocalServiceUtil.updateLayout(layout.getGroupId(), layout.isPrivateLayout(), layout.getLayoutId(), layout.getTypeSettings());
            }
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.debug("Portlets List IDs: {}", portletsId);
            // </editor-fold>
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }
        if (deletePortlet) {
            String rootPortletId = PortletConstants.getRootPortletId(portletId);
            try {
                ResourceLocalServiceUtil.deleteResource(layout.getCompanyId(), rootPortletId,
                        ResourceConstants.SCOPE_INDIVIDUAL,
                        PortletPermissionUtil.getPrimaryKey(layout.getPlid(), portletId));
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown: ", ex);
                // </editor-fold>
            }
        }
        logger.debug("Returning");
    }

    /**
     *
     * @param portletID
     * @param actionRequest
     * @deprecated use removePortlet(String portletID)
     */
    public static void removePortlet(String portletID, ActionRequest actionRequest) {
        logger.debug("Entering");
        Layout layout = (Layout) actionRequest.getAttribute(WebKeys.LAYOUT);
        LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
        ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

        layoutTypePortlet.removePortletId(themeDisplay.getUserId(), portletID + "_WAR_OTMS-Demo");
        try {
            LayoutLocalServiceUtil.updateLayout(layout.getGroupId(),
                    layout.isPrivateLayout(), layout.getLayoutId(), layout.getTypeSettings());
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }
        logger.debug("Returning");
    }

    public static ArrayList<String> getQueryStringValue(RenderRequest request) {
        ArrayList<String> key = new ArrayList<String>();
        if (request.getAttribute("javax.servlet.forward.query_string") != null) {
            String queryString = request.getAttribute("javax.servlet.forward.query_string").toString();
            String query[];
            if (queryString.contains("&")) {
                query = queryString.split("&");
                String param1[] = query[0].split("=");
                String param2[] = query[1].split("=");
                if (param1[0].equals("objValue")) {
                    key.add(param1[1]);
                }
                if (param2[0].equals("objType")) {
                    key.add(param2[1]);
                }
            }
        }
        return key;
    }

    public static void addUser(String password, String username, String email,
            String firstName, String middleName, String lastName) {

        logger.debug("Entering");
        try {
            PortletRequest request = (PortletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            long compID = (PortalUtil.getCompany(PortalUtil.getHttpServletRequest(request))).getCompanyId();
            ServiceContext serviceContext = ServiceContextFactory.getInstance(CalEvent.class.getName(), request);
            long creatorUserID = UserLocalServiceUtil.getDefaultUserId(compID);
            UserLocalServiceUtil.addUser(creatorUserID,
                    compID, false, password, password, false, firstName, email, 0, "",
                    Locale.ENGLISH, firstName, middleName, lastName, 0, 0,
                    true, 1, 1, 1985, "", new long[0], new long[0], new long[0], new long[0], false, serviceContext);
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }
        logger.debug("Returning");
    }

    public static void updateUserPassWord(String scrName, String newPassword, String confirmationPassword) {
        logger.debug("Entering");
        try {
            Layout cLayout = getCurrentLayout();
            User user = UserLocalServiceUtil.getUserByScreenName(cLayout.getCompanyId(), scrName);
            UserLocalServiceUtil.updatePassword(user.getUserId(), newPassword, confirmationPassword, Boolean.FALSE);
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }
        logger.debug("Returning");
    }

    public static void importUser(String scrName) {
        logger.debug("Entering");
        try {
            long companyId = PortalUtil.getDefaultCompanyId();
            PortalLDAPImporterUtil.importLDAPUserByScreenName(companyId,
                    scrName);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        logger.debug("Returning");
    }

    public static void addUser(OUser user) {
        logger.debug("Entering");
        try {

            long companyId = PortalUtil.getDefaultCompanyId();
            long creatorUserID = UserLocalServiceUtil
                    .getDefaultUserId(companyId);
            String sn = ".";

            if (user.getDisplayName() != null
                    && !user.getDisplayName().equals("")) {
                if (user.getDisplayName().contains(" ")) {
                    sn = user.getDisplayName().substring(
                            user.getDisplayName().lastIndexOf(" ") + 1);
                }
            }
            User addedUser = UserLocalServiceUtil.addUser(creatorUserID, companyId, false,
                    user.getPassword(), user.getPassword(), false,
                    user.getLoginName(), user.getEmail(), 0, "",
                    Locale.ENGLISH, user.getLoginName(), "", sn, 0, 0, true, 1,
                    1, 1985, "", new long[0], new long[0], new long[0],
                    new long[0], false, null);

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        logger.debug("Returning");
    }

    public static boolean isPortletGlobalView() {
        logger.debug("Entering");
        try {
            RenderRequest request = (RenderRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
            Layout layout = themeDisplay.getLayout();
            // <editor-fold defaultstate="collapsed" desc="Log">
            if (layout != null) {
                logger.debug("Layout URL: {}", layout.getFriendlyURL());
            }
            // </editor-fold>
            if (layout.getFriendlyURL().contains("-details-for") || layout.getFriendlyURL().contains("global-view")) {
                logger.debug("Returning with true");
                return true;
            }
            logger.debug("Returning with false");
            return false;
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown");
            // </editor-fold>
            logger.debug("Returning with false");
            return false;
        }
    }

    /**
     *
     * @param portletID
     * @deprecated use openScreen(
     * @param String screenName) in OBackBean
     */
    public static void addPortlet(String portletID) {
        logger.debug("Entering");
        try {
            String userPreference = "TOP_RIGHT";
            //Get current Instance from FacesContext & then Get External Context
            FacesContext facesContext = FacesContext.getCurrentInstance();

            ExternalContext ec = facesContext.getExternalContext();

            //Get it straight from the request map (JSF recommended way - portable)
            Map requestMap = ec.getRequestMap();

            //Note:you Should Add these tags in web.xml to be able to retrieve layout and themedisplay from requestMap
            //<context-param>
            //<param-name>com.icesoft.faces.hiddenPortletAttributes</param-name>
            //<param-value>LAYOUT , THEME_DISPLAY</param-value>
            //</context-param>
            //Get Layout from requestMap using the WebKeys.Layout Key
            Layout layout = (Layout) requestMap.get(WebKeys.LAYOUT);
            //Get themeDisplay from requestMap using the WebKeys.THEME_DISPLAY Key
            ThemeDisplay themeDisplay = (ThemeDisplay) requestMap.get(WebKeys.THEME_DISPLAY);
            LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
            String[] addedPortlet = null;

            if (portletID.endsWith(",")) {
                portletID = portletID.substring(0, portletID.length() - 1);
                addedPortlet = portletID.split(",");
            } else {
                addedPortlet = new String[1];
                addedPortlet[0] = portletID;
            }

            List<String> targetColumns = layoutTypePortlet.getLayoutTemplate().getColumns();

            for (int addedPortletIndexer = 0; addedPortletIndexer < addedPortlet.length; addedPortletIndexer++) {
                if ("TOP_RIGHT".equalsIgnoreCase(userPreference)) {
                    // portletIdInc = layoutTypePortlet.addPortletId(themeDisplay.getUserId(), addedPortlet[addedPortletIndexer] + "_WAR_OHRWebApp");
                    if (Validator.isNotNull(targetColumns.get(targetColumns.size() - 1)))//&& Validator.isNotNull(portletIdInc))
                    {
                        layoutTypePortlet.movePortletId(themeDisplay.getUserId(), addedPortlet[addedPortletIndexer] + "_WAR_OTMS-Demo", targetColumns.get(targetColumns.size() - 1), 0);
                    }

                } else if ("TOP_LEFT".equalsIgnoreCase(userPreference)) {
                    //  portletIdInc = layoutTypePortlet.addPortletId(themeDisplay.getUserId(), addedPortlet[addedPortletIndexer] + "_WAR_OHRWebApp");
                    if (Validator.isNotNull(targetColumns.get(0)))// && Validator.isNotNull(portletIdInc))
                    {
                        layoutTypePortlet.movePortletId(themeDisplay.getUserId(), addedPortlet[addedPortletIndexer] + "_WAR_OTMS-Demo", targetColumns.get(0), 1);
                    }
                }
                try {
                    LayoutLocalServiceUtil.updateLayout(layout.getGroupId(), layout.isPrivateLayout(), layout.getLayoutId(), layout.getTypeSettings());
                } catch (Exception ex) {
                    logger.error("Exception thrown: ", ex);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        logger.debug("Returning");
    }

    /**
     *
     * @param htScreen
     * @param requestMap
     * @deprecated use openScreen(
     * @param String screenName) in OBackBean
     */
    public static void addPortlet(String htScreen, Map requestMap) {
        logger.debug("Entering");
        ThemeDisplay themeDisplay = (ThemeDisplay) requestMap.get(WebKeys.THEME_DISPLAY);
        Layout layout = themeDisplay.getLayout();
        long in1 = layout.getGroupId();
        boolean in2 = layout.isPrivateLayout();
        String in3 = layout.getFriendlyURL();
        try {
            try {
                layout = LayoutLocalServiceUtil.getFriendlyURLLayout(in1, in2, in3);
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown", ex);
                // </editor-fold>
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
        }
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.debug("Adding Portlet. Layout: {} htScreen to be opened {}_WAR_OTMS-Demo", layout, htScreen);
        // </editor-fold>
        LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
        try {
            layoutTypePortlet.addPortletId(themeDisplay.getUserId(), htScreen + "_WAR_OTMS-Demo");
            LayoutLocalServiceUtil.updateLayout(
                    layout.getGroupId(), layout.isPrivateLayout(), layout.getLayoutId(), layout.getTypeSettings());
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }

        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.debug("HTScreen should be added here");
        logger.debug("Returning");
        // </editor-fold>
    }

    public static void addPortletForGuest(String portletID) {
        logger.debug("Entering");
        ActionRequest request = (ActionRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String userPreference = "TOP_RIGHT";
        Layout layout = (Layout) request.getAttribute(WebKeys.LAYOUT);
        LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
        String[] addedPortlet = null;
        if (portletID.endsWith(",")) {
            portletID = portletID.substring(0, portletID.length() - 1);
            addedPortlet = portletID.split(",");
        } else {
            addedPortlet = new String[1];
            addedPortlet[0] = portletID;
        }
        List<String> targetColumns = layoutTypePortlet.getLayoutTemplate().getColumns();
        for (int addedPortletIndexer = 0; addedPortletIndexer < addedPortlet.length; addedPortletIndexer++) {

            try {
                // portletIdInc = layoutTypePortlet.addPortletId(themeDisplay.getUserId(), addedPortlet[addedPortletIndexer] + "_WAR_OHRWebApp");
                if (Validator.isNotNull(targetColumns.get(targetColumns.size() - 1)))//&& Validator.isNotNull(portletIdInc))
                {
                    layoutTypePortlet.movePortletId(themeDisplay.getUserId(),
                            addedPortlet[addedPortletIndexer] + "_WAR_OHRWebApp",
                            targetColumns.get(targetColumns.size() - 1), 0);
                }
                LayoutLocalServiceUtil.updateLayout(
                        layout.getGroupId(), layout.isPrivateLayout(), layout.getLayoutId(), layout.getTypeSettings());
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown: ", ex);
                // </editor-fold>
            }
        }
        logger.debug("Returning");
    }

    public static void addDefaultPortlets(Map requestMap) {
        logger.debug("Entering");
        ThemeDisplay themeDisplay = (ThemeDisplay) requestMap.get(WebKeys.THEME_DISPLAY);
        Layout layout = themeDisplay.getLayout();
        try {
            layout = LayoutLocalServiceUtil.getFriendlyURLLayout(
                    layout.getGroupId(), layout.isPrivateLayout(), layout.getFriendlyURL());
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }

        LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
        layoutTypePortlet.setLayoutTemplateId(themeDisplay.getUserId(), "2_columns_ii");

        try {
            LayoutLocalServiceUtil.updateLayout(
                    layout.getGroupId(), layout.isPrivateLayout(), layout.getLayoutId(),
                    layout.getTypeSettings());
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }

        addPortlet("Inbox", requestMap, 1, 1);
        addNonOTMSPortlet("8", requestMap, 2, 1);
    }

    public static String getUserLoginName(long userId) {
        logger.debug("Entering");
        String userLoginName = "";
        try {
            User user = UserLocalServiceUtil.getUserById(userId);
            userLoginName = user.getScreenName();
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }
        logger.debug("Returning with: {}", userLoginName);
        return userLoginName;
    }

    public static void addPortlet(String htScreen, Map requestMap, int col, int row) {
        logger.debug("Entering");
        addNonOTMSPortlet(htScreen + "_WAR_OTMS-Demo", requestMap, row, row);
        logger.debug("Returning");
    }

    public static void addNonOTMSPortlet(String portletID, Map requestMap, int col, int row) {
        logger.debug("Entering");
        ThemeDisplay themeDisplay = (ThemeDisplay) requestMap.get(WebKeys.THEME_DISPLAY);
        Layout layout = themeDisplay.getLayout();
        try {
            layout = LayoutLocalServiceUtil.getFriendlyURLLayout(layout.getGroupId(),
                    layout.isPrivateLayout(), layout.getFriendlyURL());
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.debug("Adding Non OTMS Portlet. Layout: {}, htScreen to be opened: {} ", layout, portletID);
        // </editor-fold>

        LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();

        // <editor-fold defaultstate="collapsed" desc="Log">
        if (null != layoutTypePortlet && null != layoutTypePortlet.getLayoutTemplate()) {
            logger.debug("Adding Non OTMS Portlet. targetColumns: {}", layoutTypePortlet.getLayoutTemplate().getColumns());
        }
        // </editor-fold>

        try {
            layoutTypePortlet.addPortletId(themeDisplay.getUserId(), portletID);
            layoutTypePortlet.movePortletId(themeDisplay.getUserId(), portletID, "column-" + col, row);
            LayoutLocalServiceUtil.updateLayout(
                    layout.getGroupId(), layout.isPrivateLayout(), layout.getLayoutId(),
                    layout.getTypeSettings());
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }
        logger.debug("Returning");
    }

    // you try request.redirect with dockmenu.ifaces
    public static void removeAllPortlets(Map requestMap) {
        logger.debug("Entering");
        Layout layout = getCurrentLayout();// (Layout) request.getAttribute(WebKeys.LAYOUT);
        LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
        String userName = (String) SessionManager.getSessionAttribute("userID");
        User user = null;
        try {
            user = UserLocalServiceUtil.getUserByScreenName(layout.getCompanyId(), userName);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }

        List<String> portletsId = layoutTypePortlet.getPortletIds();

        for (String portletID : portletsId) {
            layoutTypePortlet.removePortletId(user.getUserId(), portletID, true);
        }
        logger.debug("Returning");
    }

    public static void movePortlet(String portletIdInc) {
        logger.debug("Entering");
        try {
            Layout clayout = getCurrentLayout();
            LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) clayout.getLayoutType();
            String userName = (String) SessionManager.getSessionAttribute("userID");
            User user = UserLocalServiceUtil.getUserByScreenName(clayout.getCompanyId(), userName);
            layoutTypePortlet.movePortletId(user.getUserId(), portletIdInc, "column-2", 1);
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown");
            // </editor-fold>
        }
        logger.debug("Returning");
    }
    public static final String MESSAGE = "SCREEN_INPUT";
    public static final String RENDERGROUPS = "RENDER_GROUPS";
    public static final String LOOKUPMESSAGE = "LOOKUP_MESSAGE";
    public static final String LOOKUPRENDERGROUP = "LOOKUP_RENDER_GROUP";
    public static final String PAGESCREENS = "PAGE_SCREENS";
    public static final String SCREENPASSEDINPUT = "PASSED_INPUTS";

    /**
     * Get a stored Object from session by key
     *
     * @param key
     * @return Object Stored in session with the key param
     */
    public static Object getSharedSessionAttribute_old(String key) {
        return SessionManager.getSessionAttribute(key);
    }

    /**
     * Store Object value in current user session, don't use this function to
     * store Object for screen or for a group of screen, only use it to store
     * something across user session. You should use
     * <B>setScreenSessionAttribute </B> to set value for screen, or
     * <B>setSessionAttributeForRenderGroup</B> to set value for a group of
     * screens
     *
     * @param key
     * @param value
     *
     * @see setScreenSessionAttribute(String screenName, String key, Object
     * value)
     * @see setSessionAttributeForRenderGroup(String groupName, String key,
     * Object value)
     */
    public static void setSharedSessionAttribute_old(String key, Object value) {
        SessionManager.addSessionAttribute(key, value);
    }

    /**
     * Store Object For Screen in the current user session
     *
     * @param screenInstId screenInstId to set attribute for
     * @param key
     * @param value
     */
    public static synchronized void setSessionAttributeForScreen_Old(String screenInstId, String key, Object value) {
        logger.debug("Entering");
        HashMap<String, Object> screenParamsMap = null;
        String lPLID = (String) SessionManager.getSessionAttribute("layoutPLID");
        if (SessionManager.getSessionAttribute(screenInstId + "_" + lPLID) == null) {
            screenParamsMap = new HashMap<String, Object>(1);
        } else {
            screenParamsMap = (HashMap<String, Object>) SessionManager.getSessionAttribute(screenInstId + "_" + lPLID);
        }
        screenParamsMap.put(key, value);
        SessionManager.addSessionAttribute(screenInstId + "_" + lPLID, screenParamsMap);

        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.debug("Added Session Screen Attribute. Screen Instance: {}, Key: {}, Value: {}", screenInstId, key, value);
        logger.debug("Returning");
        // </editor-fold>
    }

    /**
     * Return Object from Screen in the current user session
     *
     * @param screenName screenName to set attribute for
     */
    public static synchronized HashMap<String, Object> getSessionAttributeForScreen_Old(String screenInstId) {
        logger.debug("Entering");
        HashMap<String, Object> screenParamsMap = null;
        String lPLID = (String) SessionManager.getSessionAttribute("layoutPLID");
        if (SessionManager.getSessionAttribute(screenInstId + "_" + lPLID) != null) {
            {
                screenParamsMap = (HashMap<String, Object>) SessionManager.getSessionAttribute(screenInstId + "_" + lPLID);
            }
        }
        logger.debug("Returning");
        return screenParamsMap;
    }

    /**
     * Add Screen to render Group, so if there any data to sent to this group
     * should be sent to all group screen as well
     *
     * @param screenName
     * @param groupName
     */
    public static synchronized void addScreenToRenderGroup(String screenInstId, String groupName) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.debug("screenInstId: {}, groupName: {}", screenInstId, groupName);
        // </editor-fold>

        HashMap<String, ArrayList<String>> renderGroupMap = null;
        if (SessionManager.getSessionAttribute(RENDERGROUPS) == null) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.debug("Initialize Render Group Map. groupName: {}", groupName);
            // </editor-fold>
            renderGroupMap = new HashMap<String, ArrayList<String>>(1);
        } else {
            renderGroupMap = (HashMap<String, ArrayList<String>>) SessionManager.getSessionAttribute(RENDERGROUPS);
        }
        ArrayList<String> screens = renderGroupMap.get(groupName);
        if (screens == null) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.debug("Initialize Screen List For Group. groupName: {}", groupName);
            // </editor-fold>
            screens = new ArrayList<String>();
        }
        screens.add(screenInstId);
        renderGroupMap.put(groupName, screens);
        SessionManager.addSessionAttribute(RENDERGROUPS, renderGroupMap);

        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.debug("screenInstId: {}, groupName: {}", screenInstId, groupName);
        logger.debug("Returning");
        // </editor-fold>
    }

    /**
     * Remove Screen From Group, called when the portlet is removed.
     *
     * @param screenName
     */
    public static synchronized void removeScreenFromRenderGroup(String screenInstId) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.debug("screenInstId: {}", screenInstId);
        // </editor-fold>

        HashMap<String, ArrayList<String>> renderGroupMap = null;
        if (SessionManager.getSessionAttribute(RENDERGROUPS) == null) {
            logger.debug("Returning");
            return;
        }
        renderGroupMap = (HashMap<String, ArrayList<String>>) SessionManager.getSessionAttribute(RENDERGROUPS);
        if (renderGroupMap.isEmpty()) {
            logger.debug("Returning");
            return;
        }
        Iterator<String> keyIterator = renderGroupMap.keySet().iterator();

        while (keyIterator.hasNext()) {
            String groupName = keyIterator.next();
            ArrayList<String> screens = renderGroupMap.get(groupName);
            if (screens.remove(screenInstId)) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.debug("Screen Removed From Group. screenInstId: {}, groupName: {}", screenInstId, groupName);
                // </editor-fold>
            } else {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("Error Removing Screen From Group. screenInstId: {} groupName: {}", screenInstId, groupName);
                // </editor-fold>
            }
            renderGroupMap.put(groupName, screens);
        }
        SessionManager.addSessionAttribute(RENDERGROUPS, renderGroupMap);
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.debug("screenInstId: {}", screenInstId);
        logger.debug("Returning");
        // </editor-fold>
    }

    /**
     * Store Object into current user session for this group(to all group
     * screens)
     *
     * @param groupName
     * @param key
     * @param value
     */
    public synchronized static void setSessionAttributeForRenderGroup(String groupName, String key, Object value) {
        logger.debug("Entering");
        HashMap<String, ArrayList<String>> renderGroupMap = null;
        if (SessionManager.getSessionAttribute(RENDERGROUPS) == null) {
            logger.debug("Returning");
            return;
        }
        renderGroupMap = (HashMap<String, ArrayList<String>>) SessionManager.getSessionAttribute(RENDERGROUPS);
        ArrayList<String> screens = renderGroupMap.get(groupName);
        if (screens == null) {
            logger.debug("Returning");
            return;
        }
        Iterator<String> iterator = screens.iterator();
        while (iterator.hasNext()) {
            String screenName = iterator.next();
        }
        logger.debug("Returning");
    }

    public static Layout getCurrentLayout() {
        logger.debug("Entering");
        String lPLID = (String) SessionManager.getSessionAttribute("layoutPLID");
        if (lPLID == null || "".equals(lPLID)) {
            logger.debug("Current Layout ID is null");
            lPLID = (String) SessionManager.getSessionAttribute("oldLayoutPLID");
            logger.debug("Returning with Null");
            return null;
        }
        Layout cLayout = null;
        try {
            cLayout = LayoutLocalServiceUtil.getLayout(Long.valueOf(lPLID));
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }
        if (cLayout == null) {
            try {
                lPLID = String.valueOf(SessionManager.getSessionAttribute("oldLayoutPLID"));
                cLayout = LayoutLocalServiceUtil.getLayout(Long.valueOf(lPLID));
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown: ", ex);
                // </editor-fold>
            }
        } else {
            logger.debug("Returning with: {}", cLayout.getLayoutId());
        }
        return cLayout;
    }

    public static synchronized void addOPortletPage(
            OPortalPage oPortalPage, OEntityManagerRemote oem,
            DataTypeServiceRemote dataTypeService, OUser loggedUser,
            Map<String, Object> params, UIFrameworkServiceRemote frameworkServiceRemote) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>
        String userName = (String) SessionManager.getSessionAttribute("userID");
        Layout cLayout = getCurrentLayout(), layout = null;
        List<PortalPagePortlet> pagePortlets = oPortalPage.getPortlets();
        // <editor-fold defaultstate="collapsed" desc="Sort Portlets">
        Collections.sort(pagePortlets, new Comparator() {
            public int compare(Object o1, Object o2) {
                PortalPagePortlet pagePortlet1 = (PortalPagePortlet) o1;
                PortalPagePortlet pagePortlet2 = (PortalPagePortlet) o2;
                String col1 = pagePortlet1.getColumn();
                String col2 = pagePortlet2.getColumn();
                String pos1 = String.valueOf(pagePortlet1.getPosition());
                String pos2 = String.valueOf(pagePortlet2.getPosition());

                if (col1.compareTo(col2) != 0) {
                    return col1.compareTo(col2);
                } else {
                    return pos1.compareTo(pos2);
                }
            }
        });
        // </editor-fold>
        try {
            ODataType voidDataType = dataTypeService.loadODataType("VoidDT", loggedUser);
            User user = UserLocalServiceUtil.getUserByScreenName(cLayout.getCompanyId(), userName);
            boolean privatelayout = cLayout.isPrivateLayout();
            long parentLayoutId = cLayout.getParentLayoutId(); //0
            long groupId = cLayout.getGroupId(); //10171
            long userId = user.getUserId(); //10169

            ServiceContext serviceContext = new ServiceContext();
            serviceContext.setScopeGroupId(groupId);
            layout = LayoutLocalServiceUtil.addLayout(userId, groupId, privatelayout, parentLayoutId, oPortalPage.getHeader(),
                    oPortalPage.getHeader(), oPortalPage.getName(), "portlet", false, "", serviceContext);
            LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();

            UDC layoutTemplate = oPortalPage.getLayoutTemplate();
            if (layoutTemplate == null) { //1028187 4
                layoutTemplate = new UDC();
                layoutTemplate.setValue("1");
            }
            try {
                if ("1".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "2_columns_ii"); //2_columns_ii 30-70
                } else if ("2".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "2_columns_iii");//2_columns_iii  70/30
                } else if ("3".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "2_columns_i");//2_columns_i  50-50
                } else if ("4".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "1_column");
                } else if ("5".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "3_columns");
                } else if ("6".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "1_2_columns_i"); // 30-70
                } else if ("7".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "1_2_columns_ii"); // 70-30
                } else if ("8".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "1_2_1_columns");
                } else if ("9".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "2_2_columns");
                } else if ("10".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "freeform");
                }
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown: ", ex);
                // </editor-fold>
            }

            //RDO HashMap of portalPage screens
            HashMap<String, OScreen> portalPageScreensInstances = new HashMap<String, OScreen>();

            // generate screenInstId for all portlets
            for (PortalPagePortlet portlet : pagePortlets) {
                portlet.setPortletScreenInstId(OBackBean.generateScreenInstId(portlet.getScreen().getName()));
                //RDO
                portalPageScreensInstances.put(portlet.getPortletScreenInstId(), portlet.getScreen());
            }
            //RDO
            oPortalPage.setPageScreenInstancesList(portalPageScreensInstances);
            List<String> screenInstancesInPage = new ArrayList<String>();

            for (PortalPagePortlet portlet : pagePortlets) {
                if (portlet.isInActive()) {
                    continue;
                }
                HashMap<String, Object> screenParamsMap = new HashMap<String, Object>();
//                List toBeRenderedScreens = getToBeRenderedScreenList(portlet, pagePortlets);
                String x = portlet.getPortletScreenInstId();
//               screenParamsMap.put("PortletPagePortlet", toBeRenderedScreens);
                if (portlet.getParentPortlet() == null || portlet.getParentPortlet().getDbid() == 0) {
                    ODataMessage voidDataMessage = new ODataMessage();
                    voidDataMessage.setData(new ArrayList<Object>());
                    voidDataMessage.setODataType(voidDataType);
                    screenParamsMap.put(OPortalUtil.MESSAGE, voidDataMessage);
                }
                if (params != null) {
                    screenParamsMap.putAll(params);
                    screenParamsMap.put("screenProcessMode", portlet.getScreen().getName());
                }
                //RDO add portal page map to screen parameters pam
                screenParamsMap.put(OBackBean.PORTAL_PAGE, oPortalPage);
                SessionManager.setScreenSessionMap(portlet.getPortletScreenInstId(), screenParamsMap);
                screenInstancesInPage.add(portlet.getPortletScreenInstId());

                String portletIdInc = layoutTypePortlet.addPortletId(user.getUserId(),
                        "OTMSPortlet_WAR_FABS-Portal", "column-" + portlet.getColumn(), portlet.getPosition(), false);
                // layoutTypePortlet.movePortletId(user.getUserId(), portletIdInc, "column-" + portlet.getColumn(), portlet.getPosition());
                String viewPage = portlet.getScreen().getViewPage();

                OrgChartFunction chartFunction = null;
                if (viewPage == null || "".equals(viewPage)) {
                    if (portlet.getScreen() instanceof TabularScreen) {
                        viewPage = "TabularScreen.iface";
                    } else if (portlet.getScreen() instanceof FormScreen) {
                        viewPage = "FormScreen.iface";
//                        HashMap<String, Object> screenParamsMap = new HashMap<String, Object>(1);
                        screenParamsMap.put("ScreenModeVar", 18);
                        SessionManager.setScreenSessionMap(portlet.getPortletScreenInstId(), screenParamsMap);
                    }
                } else if (viewPage.equals("EmployeeViewr.jspx")) {

                    try {
                        chartFunction = (OrgChartFunction) oem.loadEntity(OrgChartFunction.class.getSimpleName(),
                                Collections.singletonList("code = 'empchart'"), null, loggedUser);
                        viewPage = chartFunction.getViewPage().getCode() + ".jspx";
                    } catch (Exception ex) {
                        logger.error("Exception thrown: ", ex);
                    }
                }

                String defaultState = portlet.getDefaultState() != null ? portlet.getDefaultState().getValue() : "normal";
                String waitLoading = portlet.getParentPortlet() != null ? "true" : "false";
                String mainPortletStr = (portlet.isMain()) ? "MainPortlet" : "child";
                OScreen screen = (OScreen) oem.loadEntity(OScreen.class.getSimpleName(),
                        portlet.getScreen().getDbid(), null, null, loggedUser);
                if (viewPage.equals("EmployeeViewr.jspx")) {
                    //String portletIdInc = layoutTypePortlet.addPortletId(user.getUserId(), "EmployeeViewr_WAR_FABS-Portal");
                    //String viewPage = chartFunction.getViewPage().getCode() + ".jspx";
                    String htmlDir = (loggedUser.isRtl()) ? "rtl" : "ltr";
                    // load
                    portletIdInc = layoutTypePortlet.addPortletId(user.getUserId(), "EmployeeViewr_WAR_FABS-Portal");
                    FABSSetup fabsOrgChartWSDL = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                            Collections.singletonList("setupKey = 'FABSOrgChartWSDL'"), null, loggedUser);

                    FABSSetup otmsOrgChartWSDL = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                            Collections.singletonList("setupKey = 'OTMSOrgChartWSDL'"), null, loggedUser);

                    setOrgChartPortletPreferences(portletIdInc, viewPage, portletIdInc, "Employee Viewer", 0l, ""/* entityClassName*/,
                            chartFunction.getPortletHeight(), layout, htmlDir, loggedUser.getLoginName(),
                            fabsOrgChartWSDL.getSvalue()/*.replace("1.1.1.195:58080", "1.1.1.6:7892")*/,
                            otmsOrgChartWSDL.getSvalue()/*.replace("1.1.1.195:58080", "1.1.1.6:7892")*/, "");
                } else {
                    setPortletPreferences(portletIdInc, portlet.getScreen().getName(),
                            portlet.getPortletScreenInstId(), screen.getHeaderTranslated(),
                            viewPage, portlet.getHeight(), portlet.getWidth(), "true",
                            mainPortletStr, layout, waitLoading, defaultState);
                }
            }
            LayoutLocalServiceUtil.updateLayout(layout.getGroupId(), layout.isPrivateLayout(),
                    layout.getLayoutId(), layout.getTypeSettings());
            SessionManager.addSessionAttribute("oldLayoutPLID", cLayout.getPlid()); //157413
            if (params != null) {
                ODataMessage message = (ODataMessage) params.get(MESSAGE);
                if (message != null && message.getData() != null
                        && !message.getData().isEmpty()
                        && (message.getData().size() == 1 && message.getData().get(0).equals("OrgChartMode"))
                        || (message.getData().size() == 3 && message.getData().get(2).equals("OrgChartMode"))) {
                    SessionManager.addSessionAttribute("OrgChartScreensInPage", screenInstancesInPage); //157413
                }
            }
            //response
            RequestContext.getCurrentInstance().execute(
                    "top.FABS.Portlet.addBrowserTab({tabURL:'" + layout.getFriendlyURL()
                    + "', selectTab:'true'});");

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }
        logger.debug("Returning");
    }

    // <editor-fold defaultstate="collapsed" desc="Perforance Method">
    public static synchronized void addOPortletPage(
            List<Long> portalPageData, OEntityManagerRemote oem,
            DataTypeServiceRemote dataTypeService, OUser loggedUser,
            Map<String, Object> params, UIFrameworkServiceRemote frameworkServiceRemote,
            UDCServiceRemote uDCServiceRemote) {
        logger.debug("Entering");
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        String userName = (String) SessionManager.getSessionAttribute("userID");
        Layout cLayout = getCurrentLayout(), layout = null;
        long portletPageDBID = Long.valueOf(String.valueOf(portalPageData.get(0)));
        OPortalPageLite oPortalPage = frameworkServiceRemote.getPortalPage(portletPageDBID, loggedUser);
        List<PortalPagePortletLite> pagePortlets = frameworkServiceRemote.getPortalPagePortlets(portletPageDBID, loggedUser);
        Collections.sort(pagePortlets);
        try {
            ODataType voidDataType = dataTypeService.loadODataType("VoidDT", loggedUser);
            User user = UserLocalServiceUtil.getUserByScreenName(cLayout.getCompanyId(), userName);
            boolean privatelayout = true;
            long parentLayoutId = cLayout.getParentLayoutId();
            long groupId = cLayout.getGroupId();
            long userId = user.getUserId();
            ServiceContext serviceContext = new ServiceContext();
            serviceContext.setScopeGroupId(groupId);
            // HBM: No Random Number in Page Header any more :)
            layout = LayoutLocalServiceUtil.addLayout(userId, groupId, privatelayout, parentLayoutId, oPortalPage.getHeader(),
                    oPortalPage.getHeader(), oPortalPage.getName(), "portlet", false, "", serviceContext);
            LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();

            UDC layoutTemplate = new UDC();//oPortalPage.getLayoutTemplate();
            if (oPortalPage.getLayoutTemplateUDCValue() == null || oPortalPage.getLayoutTemplateUDCValue().equals("")) {
                layoutTemplate.setValue("1");
            } else {
                layoutTemplate.setValue(oPortalPage.getLayoutTemplateUDCValue());
            }

            try {
                if ("1".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "2_columns_ii"); //2_columns_ii 30-70
                } else if ("2".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "2_columns_iii");//2_columns_iii  70/30
                } else if ("3".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "2_columns_i");//2_columns_i  50-50
                } else if ("4".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "1_column");
                } else if ("5".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "3_columns");
                } else if ("6".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "1_2_columns_i"); // 30-70
                } else if ("7".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "1_2_columns_ii"); // 70-30
                } else if ("8".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "1_2_1_columns");
                } else if ("9".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "2_2_columns");
                } else if ("10".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "freeform");
                }
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown: ", ex);
                // </editor-fold>
            }

            //RDO HashMap of portalPage screens
            HashMap<String, OScreenLite> portalPageScreensInstances = new HashMap<String, OScreenLite>();

            // generate screenInstId for all portlets
            for (PortalPagePortletLite portlet : pagePortlets) {
                OScreenLite screenLite = frameworkServiceRemote.getOScreenLite(
                        portlet.getScreenDBID(), loggedUser);
                String portletScreenInstId = OBackBean.generateScreenInstId(portlet.getScreenName());
//                //RDO
                portalPageScreensInstances.put(portletScreenInstId, screenLite);
                portlet.setScreenInstId(portletScreenInstId);
            }
            //RDO
            oPortalPage.setPageScreenInstancesList(portalPageScreensInstances);
            List<String> screenInstancesInPage = new ArrayList<String>();

            for (PortalPagePortletLite portlet : pagePortlets) {
                HashMap<String, Object> screenParamsMap = new HashMap<String, Object>();
                List toBeRenderedScreens = getToBeRenderedScreenList(portlet.getPagePortletDBID(),
                        frameworkServiceRemote, pagePortlets, loggedUser);
                screenParamsMap.put("PortletPagePortlet", toBeRenderedScreens);
                if (portlet.getParentPortletDBID() == 0) {
                    ODataMessage voidDataMessage = new ODataMessage();
                    voidDataMessage.setData(new ArrayList<Object>());
                    voidDataMessage.setODataType(voidDataType);
                    screenParamsMap.put(OPortalUtil.MESSAGE, voidDataMessage);
                }
                if (params != null) {
                    screenParamsMap.putAll(params);
                    screenParamsMap.put("screenProcessMode", portlet.getScreenName());
                }
                //RDO add portal page map to screen parameters pam
                screenParamsMap.put(OBackBean.PORTAL_PAGE, oPortalPage);
                SessionManager.setScreenSessionMap(portlet.getScreenInstId(), screenParamsMap);
                screenInstancesInPage.add(portlet.getScreenInstId());

                String portletIdInc = layoutTypePortlet.addPortletId(user.getUserId(), "OTMSPortlet_WAR_FABS-Portal", "column-" + portlet.getPcolumn(), portlet.getPosition(), false);
                //layoutTypePortlet.movePortletId(user.getUserId(), portletIdInc, "column-" + portlet.getPcolumn(), portlet.getPosition());
                String viewPage = portlet.getScreenViewPage();

                OrgChartFunctionLite chartFunctionLite = null;
                if (viewPage == null || "".equals(viewPage)) {
                    if (portlet.getScreenType().equals("TABULAR")
                            || portlet.getScreenType().equals("M2M")
                            || portlet.getScreenType().equals("RM")) { //portlet.getScreen() instanceof TabularScreen
                        viewPage = "TabularScreen.iface";
                    } else if (portlet.getScreenType().equals("FORM")
                            || portlet.getScreenType().equals("FABSSetup")
                            || portlet.getScreenType().equals("FunctionGroup")
                            || portlet.getScreenType().equals("GCHART")) {
                        viewPage = "FormScreen.iface";
//                        HashMap<String, Object> screenParamsMap = new HashMap<String, Object>(1);
                        screenParamsMap.put("ScreenModeVar", 18);
                        SessionManager.setScreenSessionMap(portlet.getScreenInstId(), screenParamsMap);
                    }
                    if (viewPage == null || "".equals(viewPage)) {
                        // Set default view pages
                        if (portlet.getScreenType().equals("RM")) {
                            viewPage = "Prescription.iface";
                        } else if (portlet.getScreenType().equals("TABULAR")) {
                            viewPage = "TabularScreen.iface";
                        } else if (portlet.getScreenType().equals("FABSSetup")) {
                            viewPage = "FABSSetup.iface";
                        } else if (portlet.getScreenType().equals("GCHART")) {
                            viewPage = "GaugeScreen.iface";
                        } else if (portlet.getScreenType().equals("CHART")) {
                            viewPage = "ChartScreen.iface";
                        } else if (portlet.getScreenType().equals("FORM")) {
                            viewPage = "FormScreen.iface";
                        } else if (portlet.getScreenType().equals("MultiList")) {
                            viewPage = "MultiListScreen.iface";
                        } else if (portlet.getScreenType().equals("Mix")) {
                            viewPage = "MixScreen.iface";
                        } else if (portlet.getScreenType().equals("MultiLevel")) {
                            viewPage = "MultiLevelScreen.iface";
                        } else if (portlet.getScreenType().equals("FunctionGroup")) {
                            viewPage = "FunctionGroupScreen.iface";
                        }
                    }
                } else if (viewPage.equals("EmployeeViewr.jspx")) {
                    try {
                        chartFunctionLite = frameworkServiceRemote.getOrgChartFunctionLite("empchart", loggedUser);
//                        chartFunction = (OrgChartFunction) oem.loadEntity(OrgChartFunction.class.getSimpleName(),
//                                Collections.singletonList("code = 'empchart'"), null, loggedUser);
                        viewPage = chartFunctionLite.getViewPageCode() + ".jspx";
                    } catch (Exception ex) {
                        logger.error("Exception thrown: ", ex);
                    }
                }

                String defaultState = (portlet.getDefaultStateUDCValue() != null && !portlet.getDefaultStateUDCValue().equals("")) ? portlet.getDefaultStateUDCValue() : "normal";
                String waitLoading = portlet.getParentPortletDBID() != 0 ? "true" : "false";
                String mainPortletStr = (portlet.isMain()) ? "MainPortlet" : "child";

                OScreenLite screen = frameworkServiceRemote.getOScreenLite(portlet.getScreenDBID(), loggedUser);
                if (null == screen) {
                    continue;
                }

                if ("EmployeeViewr.jspx".equals(viewPage)) {
                    //String portletIdInc = layoutTypePortlet.addPortletId(user.getUserId(), "EmployeeViewr_WAR_FABS-Portal");
                    //String viewPage = chartFunction.getViewPage().getCode() + ".jspx";
                    String htmlDir = (loggedUser.isRtl()) ? "rtl" : "ltr";
                    // load
                    portletIdInc = layoutTypePortlet.addPortletId(user.getUserId(), "EmployeeViewr_WAR_FABS-Portal");
                    FABSSetupLite fabsOrgChartWSDL = frameworkServiceRemote.getFABSSetupLite("FABSOrgChartWSDL", loggedUser);
                    FABSSetupLite otmsOrgChartWSDL = frameworkServiceRemote.getFABSSetupLite("OTMSOrgChartWSDL", loggedUser);

                    setOrgChartPortletPreferences(portletIdInc, viewPage, portletIdInc, "Employee Viewer", 0l, ""/* entityClassName*/,
                            chartFunctionLite.getPortletHeight(), layout, htmlDir, loggedUser.getLoginName(),
                            fabsOrgChartWSDL.getSvalue()/*.replace("1.1.1.195:58080", "1.1.1.6:7892")*/,
                            otmsOrgChartWSDL.getSvalue()/*.replace("1.1.1.195:58080", "1.1.1.6:7892")*/, "");
                } else {
                    portalPageID = Objects.toString(oPortalPage.getId());
                    setPortletPreferences(portletIdInc, portlet.getScreenName(),//"FUN_UnitList_FilterByLevelIndexOne"
                            portlet.getScreenInstId(), screen.getHeader(),
                            viewPage, portlet.getHeight(), portlet.getWidth(), "true",
                            mainPortletStr, layout, waitLoading, defaultState);
                }

            }
            LayoutLocalServiceUtil.updateLayout(layout.getGroupId(), layout.isPrivateLayout(),
                    layout.getLayoutId(), layout.getTypeSettings());
            SessionManager.addSessionAttribute("oldLayoutPLID", cLayout.getPlid());
            if (params != null) {
                ODataMessage message = (ODataMessage) params.get(MESSAGE);
                if (message != null && message.getData() != null
                        && !message.getData().isEmpty()
                        && (message.getData().size() == 1 && message.getData().get(0).equals("OrgChartMode"))
                        || (message.getData().size() == 3 && message.getData().get(2).equals("OrgChartMode"))) {
                    SessionManager.addSessionAttribute("OrgChartScreensInPage", screenInstancesInPage); //157413
                }
            }
            //response
            RequestContext.getCurrentInstance().execute(
                    "top.FABS.Portlet.addBrowserTab({tabURL:'"
                    + layout.getFriendlyURL() + "?closeAllPortlets=true"
                    + "',username:'" + userName
                    + "', selectTab:'true'});");

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }

    }

    public static void setPortletPreferences(String portletId, String screenName,
            String screenInstId, String portletTitle, String viewPage,
            String screenHeight, String removeHeader, String mainPortlet, Layout layout, String waitLoading) {
        setPortletPreferences(portletId, screenName, screenInstId, portletTitle, viewPage, screenHeight, null,
                removeHeader, mainPortlet, layout, waitLoading, null);
    }

    public static void setPortletPreferences(String portletId, String screenName,
            String screenInstId, String portletTitle, String viewPage,
            String screenHeight, String screenWidth, String removeHeader, String mainPortlet, Layout layout,
            String waitLoading, String defaultState) {
        logger.debug("Entering");
        Layout cLayout = layout == null ? getCurrentLayout() : layout;
        long ownerId = PortletKeys.PREFS_OWNER_ID_DEFAULT;
        int ownerType = PortletKeys.PREFS_OWNER_TYPE_LAYOUT;
        try {
            // Retrieve the portlet preferences for the journal portlet instance just created
            PortletPreferences prefs = PortletPreferencesLocalServiceUtil.getPreferences(
                    cLayout.getCompanyId(), ownerId, ownerType, cLayout.getPlid(), portletId);
            prefs.setValue("screenName", screenName);
            prefs.setValue("screenInstId", screenInstId);
            prefs.setValue("viewPage", viewPage);
            String utf8PortletTitle = new String(portletTitle.getBytes("UTF-8"));
            prefs.setValue("portletTitle", utf8PortletTitle);
            prefs.setValue("screenHeight", screenHeight);
            prefs.setValue("screenWidth", screenWidth);
            prefs.setValue("removeHeader", removeHeader);
            prefs.setValue("mainPortlet", mainPortlet);
            prefs.setValue("waitLoading", waitLoading);
            prefs.setValue("defaultState", defaultState);
            prefs.store();

            //
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }
        logger.debug("Returning");
    }

    public static void setOrgChartPortletPreferences(String portletId, String viewPage, String screenInstId,
            String portletTitle, Long entityDBID, String className, int portletHeight, Layout layout,
            String htmlDir, String userName, String fabsWSDLURL, String otmsWSDLURL, String entityActionName) {

        Layout cLayout = layout == null ? getCurrentLayout() : layout;
        long ownerId = PortletKeys.PREFS_OWNER_ID_DEFAULT;
        int ownerType = PortletKeys.PREFS_OWNER_TYPE_LAYOUT;
        try {
            // Retrieve the portlet preferences for the journal portlet instance just created
            PortletPreferences prefs = PortletPreferencesLocalServiceUtil.getPreferences(
                    cLayout.getCompanyId(), ownerId, ownerType, cLayout.getPlid(), portletId);
            prefs.setValue("screenInstId", screenInstId);
            prefs.setValue("viewPage", viewPage);
            prefs.setValue("entityDBID", String.valueOf(entityDBID));
            prefs.setValue("entityName", className);
//prefs.setValue("userLang", String.valueOf(userLang));
            String utf8PortletTitle = new String(portletTitle.getBytes("UTF-8"));
            prefs.setValue("portletTitle", utf8PortletTitle);
            prefs.setValue("portletHeight", String.valueOf(portletHeight));
            prefs.setValue("htmlDir", htmlDir);
            prefs.setValue("userName", userName);
            prefs.setValue("fabsWSDLURL", fabsWSDLURL);
            prefs.setValue("otmsWSDLURL", otmsWSDLURL);
            prefs.setValue("entityActionName", entityActionName);
            prefs.store();
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.debug("Portlet Preferences Set. portletId: {}", portletId);
        // </editor-fold>
    }

    public static void setReportPortletPreferences(String portletId, String reportName,
            String reportFormat, String reportTitle, Layout layout) {
        logger.debug("Entering");
        Layout cLayout = layout == null ? getCurrentLayout() : layout;
        long ownerId = PortletKeys.PREFS_OWNER_ID_DEFAULT;
        int ownerType = PortletKeys.PREFS_OWNER_TYPE_LAYOUT;
        try {
            // Retrieve the portlet preferences for the journal portlet instance just created
            PortletPreferences prefs = PortletPreferencesLocalServiceUtil.getPreferences(
                    cLayout.getCompanyId(), ownerId, ownerType, cLayout.getPlid(), portletId);
            prefs.setValue("reportName", reportName);
            prefs.setValue("reportTitle", reportTitle);
            prefs.setValue("reportFormat", reportFormat);
            prefs.store();
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }
        logger.debug("Returning");
    }

    static class PortletPosition {

        public String portletId;
        public String columnName;
        public int position;

        public PortletPosition(String portletId, String columnName, int position) {
            this.portletId = portletId;
            this.columnName = columnName;
            this.position = position;
        }

        @Override
        public String toString() {
            return "portletId: " + portletId + "\ncolumnName: " + columnName + "\nposition: " + position;
        }

        @Override
        public boolean equals(Object obj) {
            if (((PortletPosition) obj).portletId.equals(this.portletId)) {
                return true;
            }
            return false;
        }
    }

    static private List<PortletPosition> parsePosition(String positions) {
        logger.debug("Entering");
        List<PortletPosition> portletPositions = new ArrayList<PortletPosition>();

        String lines[] = positions.split("\n");
        for (String line : lines) {
            String cloumnName;
            int position = 1;

            int beginIndex = 0;
            int endIndex = line.indexOf("=");

            cloumnName = line.substring(beginIndex, endIndex);

            beginIndex = endIndex + 1;

            String portletsIds = line.substring(beginIndex);
            String[] portlets = portletsIds.split(",");
            for (String portletId : portlets) {
                PortletPosition portletPosition = new PortletPosition(portletId.trim(),
                        cloumnName.trim(), position);
                portletPositions.add(portletPosition);

                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.debug("Parsing Portlets. portletId: {} portletPosition: {}", portletId, portletPosition);
                // </editor-fold>

                position++;
            }
        }
        if (portletPositions != null) {
            logger.debug("Returning with portletPostions of size: {}", portletPositions.size());
        }
        return portletPositions;

    }

    public static boolean isCurrentPageOTMS(OUser loggedUser, OEntityManagerRemote oem)
            throws Exception {

        Layout cLayout = getCurrentLayout();

        List<String> conditions = new ArrayList<String>();
        conditions.add("portalPageID = " + cLayout.getPlid());
        conditions.add("user.dbid = " + loggedUser.getDbid());

        OUserPortalPage userPortalPage = (OUserPortalPage) oem.loadEntity(OUserPortalPage.class.getSimpleName(), conditions, null, oem.getSystemUser(loggedUser));

        if (userPortalPage != null) {
            return true;
        } else {
            return false;
        }
    }

    public static void deleteCurrentPagePreferences(OUser loggedUser, OEntityManagerRemote oem)
            throws Exception {
        Layout cLayout = getCurrentLayout();

        List<String> conditions = new ArrayList<String>();
        conditions.add("portalPageID = " + cLayout.getPlid());
        conditions.add("user.dbid = " + loggedUser.getDbid());

        OUserPortalPage userPortalPage = (OUserPortalPage) oem.loadEntity(OUserPortalPage.class.getSimpleName(), conditions, null, oem.getSystemUser(loggedUser));

        if (userPortalPage != null) {
            //Delete Childrens
            oem.deleteEntityList(oem.loadEntityList("OUserPortalPagePortlet",
                    Collections.singletonList("userPortalPage.dbid = " + userPortalPage.getDbid()),
                    null, null, oem.getSystemUser(loggedUser)), oem.getSystemUser(loggedUser));

            //Delete Parent
            oem.deleteEntity(userPortalPage, oem.getSystemUser(loggedUser));
        }
    }

    public static void saveCurrentPagePreferences(OUser loggedUser, OEntityManagerRemote oem) throws Exception {
        logger.debug("Entering");
        Layout cLayout = getCurrentLayout();

        List<String> conditions = new ArrayList<String>();
        conditions.add("portalPageID = " + cLayout.getPlid());
        conditions.add("user.dbid = " + loggedUser.getDbid());

        OUserPortalPage userPortalPage = (OUserPortalPage) oem.loadEntity(OUserPortalPage.class.getSimpleName(), conditions, null, oem.getSystemUser(loggedUser));

        if (userPortalPage != null) {
            //Delete Childrens
            oem.deleteEntityList(oem.loadEntityList("OUserPortalPagePortlet",
                    Collections.singletonList("userPortalPage.dbid = " + userPortalPage.getDbid()),
                    null, null, oem.getSystemUser(loggedUser)), oem.getSystemUser(loggedUser));

            //Delete Parent
            oem.deleteEntity(userPortalPage, oem.getSystemUser(loggedUser));
        }

        OUserPortalPage newUserPortalPage = new OUserPortalPage();
        newUserPortalPage.setUser(loggedUser);
        newUserPortalPage.setPortalPageID(cLayout.getPlid());
        newUserPortalPage.setName(cLayout.getName(""));

        LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) cLayout.getLayoutType();
        newUserPortalPage.setLayoutType(layoutTypePortlet.getLayoutTemplate().getLayoutTemplateId());
        if (layoutTypePortlet != null && layoutTypePortlet.getPortlets() != null) {
            logger.debug("Portlets Size: {}", layoutTypePortlet.getPortlets().size());
        }

        List<PortletPosition> portletPositions = parsePosition(cLayout.getTypeSettings());
        long ownerId = PortletKeys.PREFS_OWNER_ID_DEFAULT;
        int ownerType = PortletKeys.PREFS_OWNER_TYPE_LAYOUT;

        List<OUserPortalPagePortlet> newUserPortalPagePortlets = new ArrayList<OUserPortalPagePortlet>();
        Iterator<Portlet> iter = layoutTypePortlet.getPortlets().iterator();
        while (iter.hasNext()) {
            Portlet portlet = iter.next();
            OUserPortalPagePortlet userPortalPagePortlet = new OUserPortalPagePortlet();
            userPortalPagePortlet.setUserPortalPage(newUserPortalPage);
            userPortalPagePortlet.setPortletId(portlet.getPortletId());

            if ("OTMSPortlet".equals(portlet.getPortletName())) {
                PortletPreferences prefs = PortletPreferencesLocalServiceUtil.getPreferences(cLayout.getCompanyId(),
                        ownerId, ownerType, cLayout.getPlid(), portlet.getPortletId());
                if (prefs != null) {
                    String screenName = prefs.getValue("screenName", null);
                    if (screenName != null) {
                        List<String> conditions2 = new ArrayList<String>();
                        conditions2.add("name = '" + screenName + "'");

                        OScreen screen = (OScreen) oem.loadEntity(OScreen.class.getSimpleName(), conditions2, null, oem.getSystemUser(loggedUser));

                        userPortalPagePortlet.setScreen(screen);
                    }
                }
            }

            for (PortletPosition portletPosition : portletPositions) {
                if (portletPosition.portletId.equals(portlet.getPortletId())) {
                    userPortalPagePortlet.setPosition(portletPosition.position);
                    userPortalPagePortlet.setColumn(portletPosition.columnName);
                    break;
                }
            }
            if (userPortalPagePortlet != null) {
                logger.debug("Current Page Preferences Saved. PortletId: {} Position: {} Column: {}",
                        userPortalPagePortlet.getPortletId(), userPortalPagePortlet.getPosition(), userPortalPagePortlet.getColumn());
            }
            newUserPortalPagePortlets.add(userPortalPagePortlet);
        }
        newUserPortalPage.setUserPortlets(newUserPortalPagePortlets);
        oem.saveEntity(newUserPortalPage, loggedUser);
        logger.debug("Returning");
    }
    // <editor-fold defaultstate="collapsed" desc="renderScreen Functions">
    public final static String C2A_VAR = "c2aPassedParam";
    public final static String BASICSCREENID = "BASIC_SCREEN_ID";
    public final static String ISBACKFROMDETAIL = "IS_BACK_FROM_DETAIL";
    public final static String BASICMESSAGE = "BASIC_MESSAGE";
    public final static String BACKFROMDETAILOBJECT = "BACK_FROM_DETAIL_OBJECT";
    public final static String BASICSCREENMSG = "BASIC_SCREEN_MSG";
    public final static String TOBERENDREDSCREENS = "TO_BERENDRED_SCREENS";

    /**
     * Sets sendODM to screen OPortalUtil.MESSAGE & calls {@link #renderScreen(String)
     * }. Calls {@link #addPassedScreenInputDataMessage(String, ODataMessage) }
     * using the passed parameters. Calls render() function in fabs.js
     */
    public static boolean renderScreen(
            OScreen callerscreen, String screenInstId, ODataMessage screenODM) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        if (callerscreen != null) {
            logger.debug("Caller Screen: {}, Screen Instance ID: {}, oDM: {}", callerscreen.getInstID(), screenInstId, screenODM);
        }
        // </editor-fold>
        addPassedScreenInputDataMessage(screenInstId, screenODM);
        SessionManager.setScreenSessionAttribute(screenInstId, OPortalUtil.MESSAGE, screenODM);
        SessionManager.setScreenSessionAttribute(screenInstId, C2A_VAR,
                "TIME_IS_" + System.currentTimeMillis());
        RequestContext.getCurrentInstance().execute(
                "try {top.FABS.Portlet.render({screenInstId:'" + screenInstId + "'});} catch(err){}");
        logger.debug("Returning with true");
        return true;
    }

    public static boolean onInitRenderScreen(
            OScreen callerscreen, String screenInstId, ODataMessage screenODM) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        if (callerscreen != null) {
            logger.debug("Caller Screen: {}, Screen Instance ID: {}, oDM: {}", callerscreen.getInstID(), screenInstId, screenODM);
        }
        // </editor-fold>
        addPassedScreenInputDataMessage(screenInstId, screenODM);
        SessionManager.setScreenSessionAttribute(screenInstId, OPortalUtil.MESSAGE, screenODM);
        SessionManager.setScreenSessionAttribute(screenInstId, C2A_VAR,
                "TIME_IS_" + System.currentTimeMillis());
        logger.debug("Returning with true ");
        return true;
    }

    public static boolean renderInbox(OScreen caller, String inboxID, ODataMessage odm) {
        return renderScreen(caller, inboxID, odm);
    }
    // </editor-fold>

    /**
     * This Function is a helper function that will get all the screens that
     * will be rendered upon the selection action on this portlet. Sets
     * {@link ScreenRenderInfo#screenLoadingRequired} true for
     * {@link #receivers} screens.
     */
    public static List<ScreenRenderInfo> getToBeRenderedScreenList(long parentPortletDBID,
            UIFrameworkServiceRemote frameWorkSevice, List<PortalPagePortletLite> allPagePortlets, OUser loggedUser) {
        List<PortalPagePortletLite> toBeRenderedList = frameWorkSevice.getToBeRenderList(parentPortletDBID, loggedUser);

        List<ScreenRenderInfo> screens = null;
        String screenInstId = "";
//        List<PortalPagePortletLite> toBeRenderedList = receivedPortalPagePortlet.getToBeRenderedList();

        if (toBeRenderedList != null && !toBeRenderedList.isEmpty()) {
            screens = new ArrayList<ScreenRenderInfo>();
            Iterator<PortalPagePortletLite> portlalPagePortletIterator = toBeRenderedList.iterator();
            while (portlalPagePortletIterator.hasNext()) {
                PortalPagePortletLite toBeRenderedPgePortlet = portlalPagePortletIterator.next();
                // loop for all page portlets to get the generated screenInstId
                for (PortalPagePortletLite portlet : allPagePortlets) {
                    if (!portlet.getScreenName().equals(toBeRenderedPgePortlet.getScreenName())) {
                        continue;
                    } else {
                        screenInstId = portlet.getScreenInstId();
                        break;
                    }
                }
                screens.add(new ScreenRenderInfo(toBeRenderedPgePortlet.getScreenName(),
                        screenInstId, toBeRenderedPgePortlet));
            }
        }
        List<PortalPagePortletReceiverLite> receivers = frameWorkSevice.getPortletReceiversList(parentPortletDBID, loggedUser);
        if (receivers != null && !receivers.isEmpty()) {
            if (screens == null) {
                screens = new ArrayList<ScreenRenderInfo>();
            }
            Iterator<PortalPagePortletReceiverLite> receiversIt = receivers.iterator();
            while (receiversIt.hasNext()) {
                PortalPagePortletReceiverLite receiver = receiversIt.next();
                // loop for all page portlets to get the generated screenInstId
                for (PortalPagePortletLite portlet : allPagePortlets) {
                    if (!portlet.getScreenName().equals(receiver.getReceiverScreenName())) {
                        continue;
                    } else {
                        screens.add(new ScreenRenderInfo(
                                receiver.getReceiverScreenName(),
                                portlet.getScreenInstId(), portlet));
                        break;
                    }
                }
            }
        }
        return screens;
    }

    /**
     * Store Object For Screen in the current user session
     *
     * @param screenName screenInstId to set attribute for
     * @param ODataMessage data message to be used to generate hashMap to be
     * saved
     */
    public static synchronized void addPassedScreenInputDataMessage(String screenInstId, ODataMessage passedODM) {
        HashMap<String, Object> screenParamMap = null;

        screenParamMap = (HashMap<String, Object>) SessionManager.getSessionMap().get(
                screenInstId.substring(0, screenInstId.lastIndexOf("_")));
        if (screenParamMap == null) {
            screenParamMap = new HashMap<String, Object>(1);
        }

        HashMap<String, Object> passedInputsMap = null;
        if (!screenParamMap.isEmpty()) {
            passedInputsMap = (HashMap<String, Object>) screenParamMap.get(SCREENPASSEDINPUT);
        }

        if (passedInputsMap == null) {
            passedInputsMap = new HashMap<String, Object>(1);
        }
        // Fill passed input map
        // Get passedInputMapKey
        // Override data message for the same key
        if (passedODM != null) {
            String passInputsMapKey = String.valueOf(passedODM.getODataType().getDbid());
            passedInputsMap.put(passInputsMapKey, passedODM);
        }

        // to add the sent passedInputsMap to screen map
        screenParamMap.put(SCREENPASSEDINPUT, passedInputsMap);
        SessionManager.setScreenSessionMap(screenInstId.substring(0, screenInstId.lastIndexOf("_")), screenParamMap);
//        SessionManager.setScreenSessionAttribute(screenInstId, SCREENPASSEDINPUT, passedInputsMap);
    }

    /**
     * RDO Sets sendODM to screen OPortalUtil.MESSAGE & calls {@link #renderScreen(String)
     * }. Calls {@link #addPassedScreenInputDataMessage(String, ODataMessage) }
     * using the passed parameters. Calls render() function in fabs.js
     */
    public static boolean reloadDataOnly(String screenInstId) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.debug("Screen Instance ID: {}", screenInstId);
        // </editor-fold>

        String call = "top.FABS.Portlet.ReloadDataOnly({screenInstId:'" + screenInstId + "'});";
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute(call);
        logger.debug("Returning with true");
        return true;
    }

    public static synchronized void addOPortletPageForOrg(
            OPortalPage oPortalPage, OrgChartFunction chartFunction, BaseEntity entity, OEntityManagerRemote oem,
            DataTypeServiceRemote dataTypeService, OUser loggedUser,
            Map<String, Object> params, String entityActionName) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>
        try {
            String userName = (String) SessionManager.getSessionAttribute("userID");
            Layout cLayout = getCurrentLayout(), layout = null;
            ODataType voidDataType = dataTypeService.loadODataType("VoidDT", loggedUser);
            User user = UserLocalServiceUtil.getUserByScreenName(cLayout.getCompanyId(), userName);
            boolean privatelayout = cLayout.isPrivateLayout();
            long parentLayoutId = cLayout.getParentLayoutId();
            long groupId = cLayout.getGroupId();
            long userId = user.getUserId();

            ServiceContext serviceContext = new ServiceContext();
            serviceContext.setScopeGroupId(groupId);
            layout = LayoutLocalServiceUtil.addLayout(userId, groupId, privatelayout, parentLayoutId, oPortalPage.getHeader(),
                    oPortalPage.getHeader(), oPortalPage.getName(), "portlet", false, "", serviceContext);
            LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();

            UDC layoutTemplate = new UDC();
            layoutTemplate.setValue("4");
            try {
                if ("1".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "2_columns_ii"); //2_columns_ii 30-70
                } else if ("2".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "2_columns_iii");//2_columns_iii  70/30
                } else if ("3".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "2_columns_i");//2_columns_i  50-50
                } else if ("4".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "1_column");
                } else if ("5".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "3_columns");
                } else if ("6".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "1_2_columns_i"); // 30-70
                } else if ("7".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "1_2_columns_ii"); // 70-30
                } else if ("8".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "1_2_1_columns");
                } else if ("9".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "2_2_columns");
                } else if ("10".equals(layoutTemplate.getValue())) {
                    layoutTypePortlet.setLayoutTemplateId(userId, "freeform");
                }
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown: ", ex);
                // </editor-fold>
            }
            String portletIdInc = layoutTypePortlet.addPortletId(user.getUserId(), "EmployeeViewr_WAR_FABS-Portal");
            String viewPage = chartFunction.getViewPage().getCode() + ".jspx";
            String htmlDir = (loggedUser.isRtl()) ? "rtl" : "ltr";
            // load
            FABSSetup fabsOrgChartWSDL = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                    Collections.singletonList("setupKey = 'FABSOrgChartWSDL'"), null, loggedUser);

            FABSSetup otmsOrgChartWSDL = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                    Collections.singletonList("setupKey = 'OTMSOrgChartWSDL'"), null, loggedUser);

            String className = entity.getClass().getSimpleName();

            // need to add new preferences for entity type (position/unit)
            setOrgChartPortletPreferences(portletIdInc, viewPage, portletIdInc, "Employee Viewer",
                    entity.getDbid(), className, chartFunction.getPortletHeight(), layout, htmlDir,
                    loggedUser.getLoginName(), fabsOrgChartWSDL.getSvalue(),
                    otmsOrgChartWSDL.getSvalue(), entityActionName);

            LayoutLocalServiceUtil.updateLayout(layout.getGroupId(), layout.isPrivateLayout(),
                    layout.getLayoutId(), layout.getTypeSettings());
            RequestContext.getCurrentInstance().execute(
                    "top.FABS.Portlet.addBrowserTab({tabURL:'" + layout.getFriendlyURL()
                    + "', selectTab:'true'});");

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
    }

    public static boolean isUserLoggedIn(long userId) {

        try {
            User user = UserLocalServiceUtil.getUser(userId);
            Class<?> liveUsers = PortalClassLoaderUtil.getClassLoader().loadClass("com.liferay.portal.liveusers.LiveUsers");
            System.out.println(liveUsers);
            Method getSessionUsers = liveUsers.getDeclaredMethod("getSessionUsers", long.class);
            Object map = getSessionUsers.invoke(null, user.getCompanyId());
            Map<String, UserTracker> sessionUsers = null;
            sessionUsers = (ConcurrentHashMap<String, UserTracker>) map;
            for (Map.Entry<String, UserTracker> entry : sessionUsers.entrySet()) {
                UserTracker userTracker = (UserTracker) entry.getValue();
                if (userTracker.getUserId() == userId) {
                    return true;
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        return false;
    }

    public static boolean isUserAuthorized(HttpServletRequest request, String userId) {
        Cookie[] browserCookies = request.getCookies();
        boolean userAuthorized = false;
        for (Cookie browserCookie : browserCookies) {
            if (browserCookie.getName().equals("lfusidc")) {
                try {
                    String userComment = getUserComments(userId);
                    if (browserCookie.getValue().equals(userComment)) {
                        userAuthorized = true;
                    }
                } catch (Exception ex) {
                    logger.debug(userId, ex);
                }
            }
        }
        return userAuthorized;
    }

    private static String getUserComments(String userId) {
        try {
            String selectSQL = "select session from user_session where userid = " + userId;
            DataSource dataSource = ((DataSource) new InitialContext().lookup("jdbc/LiferayPortal"));
            Connection connect = dataSource.getConnection();
            Statement statement = connect.createStatement();
            ResultSet rs = statement.executeQuery(selectSQL);
            rs.next();
            String comment = rs.getString(1);
            rs.close();
            statement.close();
            connect.close();
            return comment;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return "";
    }
}
