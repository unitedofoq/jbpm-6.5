/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean.filter;


import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDLookupType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.filter.FilterField;
import com.unitedofoq.fabs.core.filter.OFilter.FilterType;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;

import com.unitedofoq.fabs.core.report.OReport;
import com.unitedofoq.fabs.core.uiframework.backbean.FormBean;
import com.unitedofoq.fabs.core.uiframework.backbean.FramworkUtilities;
import com.unitedofoq.fabs.core.uiframework.backbean.Lookup;
import com.unitedofoq.fabs.core.uiframework.backbean.OBackBean;
import com.unitedofoq.fabs.core.uiframework.backbean.SessionManager;
import com.unitedofoq.fabs.core.uiframework.backbean.UserMessagePanel;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.uiframework.screen.SingleEntityScreen;
import com.unitedofoq.fabs.core.webservice.OWebService;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mmohamed
 */
@ManagedBean(name="RunFilterBean")
@ViewScoped
public class RunFilterBean extends FormBean {
final static Logger logger = LoggerFactory.getLogger(RunFilterBean.class);
    public static String TO_B_OPENED_SCREEN_INST_ID_VAR = "TO_B_OPENED_SCREEN_INST_ID";
    public static String FILTER_TYPE_VAR = "FILTER_TYPE";
    public static String FILTER_MESSAGE_VAR = "MESSAGE_VAR";
    public static String PASSED_VAR = "PASSED_ENTITY";
    public static final String FROM_DELIM = "#FROM";
    public static final String TO_DELIM = "#TO";
    private BaseEntity passedEntity; 
    private FilterType currentFilterType;
    private String toBeOpenedScreenInstId;
    private ODataMessage dataMsg;
    private HashMap<String, Object> valuesMap = new HashMap<String, Object>();

    @Override
    public String getLookupConditionValue(String conditionExpression){
        if(currentFilterType == FilterType.REPORT_FILTER){
            return (String)getSessionMap().get("headerObjectDBID");
        }
        return null;
    }
    @Override
    public OEntityDTO getOactOnEntity() {
        logger.debug("Entering");
        if (currentFilterType == FilterType.REPORT_FILTER) {
            return entitySetupService.getOReportEntityDTO(
                    ((OReport) passedEntity).getDbid(),loggedUser);
        } else if (currentFilterType == FilterType.SCREEN_FILTER) {
            if (passedEntity instanceof SingleEntityScreen) {    
                logger.debug("Returning");
                return entitySetupService.getOScreenEntityDTO(
                        ((SingleEntityScreen) passedEntity).getDbid(),loggedUser);
            }
        } else if (currentFilterType == FilterType.WEBSERVICE_FILTER) {
            logger.debug("Returning");
            return entitySetupService.getWebServiceEntityDTO(
                    ((OWebService) passedEntity).getServiceFilter().getDbid(),
                    loggedUser);
        }
        logger.debug("Returning with Null");
        return null;
    }

    public FilterType getCurrentFilterType() {
        return currentFilterType;
    }

    public void setCurrentFilterType(FilterType currentFilterType) {
        this.currentFilterType = currentFilterType;
    }

    public ODataMessage getDataMsg() {
        return dataMsg;
    }

    public void setDataMsg(ODataMessage dataMsg) {
        this.dataMsg = dataMsg;
    }

    public BaseEntity getPassedEntity() {
        return passedEntity;
    }

    public void setPassedEntity(BaseEntity passedEntity) {
        this.passedEntity = passedEntity;
    }

    public HashMap<String, Object> getValuesMap() {
        return valuesMap;
    }

    public void setValuesMap(HashMap<String, Object> valuesMap) {
        this.valuesMap = valuesMap;
    }

    @Override
    protected String getBeanName() {
        return "RunFilterBean";
    }

    @Override
    protected boolean onInit(Map<String, String> requestParams) 
    {
        logger.trace("Entering");
        if (!super.onInit(requestParams))   return false ;
        try {
            if (getScreenParam(FILTER_TYPE_VAR) != null) {
                currentFilterType = (FilterType) getScreenParam(FILTER_TYPE_VAR);
            }
            if (getScreenParam(PASSED_VAR) != null) {
                passedEntity = (BaseEntity) getScreenParam(PASSED_VAR);
            }
            if (getScreenParam(FILTER_MESSAGE_VAR) != null) {
                dataMsg = (ODataMessage) getScreenParam(FILTER_MESSAGE_VAR);
            }
            
            if (getScreenParam(TO_B_OPENED_SCREEN_INST_ID_VAR) != null) {
                toBeOpenedScreenInstId = (String) getScreenParam(TO_B_OPENED_SCREEN_INST_ID_VAR);
            }
                        
            if (currentFilterType == FilterType.REPORT_FILTER) {
                String screenHeader = "Filter For Report: ";
                try {
                    OReport report = ((OReport) passedEntity);
                    if (report.getTitle() != null)
                        screenHeader += report.getTitle();
                }
                catch (Exception ex) {
                    logger.error("Exception thrown",ex);
                }
                currentScreen.setHeader(screenHeader);
            } else if (currentFilterType == FilterType.WEBSERVICE_FILTER) {
                currentScreen.setHeader(currentScreen.getHeaderTranslated() + " For Web Service ");
            } else if (currentFilterType == FilterType.SCREEN_FILTER) {
                currentScreen.setHeader(currentScreen.getHeaderTranslated() + " For Screen ");
            }
            actOnEntityCls = Class.forName(getOactOnEntity().getEntityClassPath());
            logger.trace("Returning with True");
            return true ;
            
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            logger.trace("Returning with False");
            return false ;
        }
    }

    public List<ScreenField> getCurrentScreenFields() {
        return currentScreenFields;
    }

    public void setCurrentScreenFields(List<ScreenField> currentScreenFields) {
        this.currentScreenFields = currentScreenFields;
    }
    List<ScreenField> currentScreenFields;

    /**
     * Set {@link OBackBean#headerObject} to {@link #passedEntity} & initialize header
     * panel. Calls {@link SingleEntityScreen#createHeaderPanelLayout(OEntity) } & sets
     * {@link #htmlPanelGrid}
     * 
     * @return 
     * true: success
     * <br>false: failure, user message is added to {@link #messagesPanel}
     * 
     * @see 
     * {@link SingleEntityBean#processInputForHeader() } for super call information
     * 
     */
    @Override
    protected boolean processInputForHeader() {
        logger.trace("Entering");
        // we don't call super function
        headerObject = passedEntity;
        try {
            OEntityDTO entity = entitySetupService.loadOEntityDTO(
                    passedEntity.getClass().getSimpleName(), loggedUser);
            createHeaderPanelLayout(entity);
            if (headerPanelLayout != null) {
                htmlPanelGrid.getChildren().add(headerPanelLayout);
            }
            logger.trace("Returning with True");
            return true ;
        }
        catch (Exception ex) {
            logger.error("Exception thrown",ex);
            messagesPanel.addErrorMessage(
                    usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);                
            logger.trace("Returning with False");
            return false;
        } 
    }

    @Override
    public HtmlPanelGrid buildInnerScreen() {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Method Fields">
        htmlPanelGrid = new HtmlPanelGrid();
        htmlPanelGrid.setId("generalPanel");
        HtmlPanelGrid buttonsPanel = new HtmlPanelGrid();
        buttonsPanel.setId("buttonsPanel");

        htmlPanelGrid.setColumns(1);
        buttonsPanel.setColumns(3);
        htmlPanelGrid.setCellpadding("5px");
        buttonsPanel.setCellpadding("5px");
        htmlPanelGrid.setCellspacing("0px");
        buttonsPanel.setCellspacing("0px");
        

        messagesPanel = new UserMessagePanel(exFactory, elContext, getBeanName(), ddService, this, loggedUser);
        htmlPanelGrid.getChildren().add(messagesPanel.createMessagePanel());

        HtmlPanelGrid middlePanel = new HtmlPanelGrid();

        middlePanel.setColumns(2);

        if (getLoggedUser().getFirstLanguage().getDbid() == 24) {
            middlePanel.setStyle("text-align:right;");
        }

        currentScreenFields = new ArrayList<ScreenField>();

        List<FilterField> filterfields = null;
        if (currentFilterType == FilterType.REPORT_FILTER) {
            filterfields = ((OReport) passedEntity).getReportFilter().getFilterFields();
        } else if (currentFilterType == FilterType.WEBSERVICE_FILTER) {
            filterfields = ((OWebService) passedEntity).getServiceFilter().getFilterFields();
        } else if (currentFilterType == FilterType.SCREEN_FILTER) {
            filterfields = ((OScreen) passedEntity).getScreenFilter().getFilterFields();
        }

        if (filterfields != null) {
            for (int i = 0; i < filterfields.size(); i++) {

                if (filterfields.get(i).isInActive()) {
                    continue;
                }

                ScreenField screenField = new ScreenField();
                String value = filterfields.get(i).getFieldValue();
                String valueTo = filterfields.get(i).getFieldValueTo();
                screenField.setDd(filterfields.get(i).getFieldDD());
                screenField.setEditable(true);
                screenField.setFieldExpression(filterfields.get(i).getFieldExpression());

                middlePanel.getChildren().add(createLabel(
                        "filterfld" +
                        i, screenField));

                if (!filterfields.get(i).isFromTo()) {
                    if (filterfields.get(i).getFieldDD().getControlType().getDbid() == DD.CT_DROPDOWN 
                            || filterfields.get(i).getFieldDD().getControlType().getDbid() == DD.CT_LOOKUPSCREEN) {
                        try {                            
                            BaseEntity currentObject = null;
                            currentObject = getLookupBaseEntity(filterfields.get(i).getFieldExpression(), value,null);
                            valuesMap.put(filterfields.get(i).getFieldExpression(), currentObject);
                        } catch (Exception ex) {
                            logger.warn("Invalid value for type lookup");
                            logger.error("Exception thrown",ex);
                            valuesMap.put(filterfields.get(i).getFieldExpression(), null);
                        }


                    } else if (filterfields.get(i).getFieldDD().getControlType().getDbid() == DD.CT_CALENDAR) {
                        if (value != null && !value.equals("")) {
                            
                            if(value.equalsIgnoreCase("#current_date")){
                                valuesMap.put(filterfields.get(i).getFieldExpression(), timeZoneService.getUserCDT(loggedUser));
                            }else{
                            
                            try {
                                valuesMap.put(filterfields.get(i).getFieldExpression(), FilterField.dateFormat.parse(value));
                            } catch (Exception ex) {
                                logger.error("Exception thrown",ex);
                            }
                            }
                        } else {
                            valuesMap.put(filterfields.get(i).getFieldExpression() + FROM_DELIM, null);
                        }
                    } else {
                        valuesMap.put(filterfields.get(i).getFieldExpression(), value);
                    }

                    String fldExp = "#{" + getBeanName() + "." + "valuesMap['" + filterfields.get(i).getFieldExpression() + "']" + "}";
                    UIComponent component = null;
                    if (filterfields.get(i).getFieldDD().getControlType().getDbid() == DD.CT_TEXTFIELD) {
                        component = createTextFieldControl(20, "filterfld_fld" + i, fldExp, true, screenField.getDd(), i, false, false ,false);
                    } else if (filterfields.get(i).getFieldDD().getControlType().getDbid() == DD.CT_CHECKBOX) {
                        component = createCheckBoxControl("filterfld_fld" + i, fldExp, true, screenField.getDd(), i, false);
                    } else if (filterfields.get(i).getFieldDD().getControlType().getDbid() == DD.CT_CALENDAR) {
                        component = createCalendarControl("filterfld_fld" + i, fldExp, i, true, screenField.getDd(), i, false);
                    } else if (filterfields.get(i).getFieldDD().getControlType().getDbid() == DD.CT_DROPDOWN
                            || filterfields.get(i).getFieldDD().getControlType().getDbid() == DD.CT_LOOKUPSCREEN
                            || filterfields.get(i).getFieldDD().getControlType().getDbid() == DD.CT_LITERALDROPDOWN) {
                        DD fieldDD = screenField.getDd();
                        HtmlPanelGrid lookupPanelGrid = new HtmlPanelGrid();
                        lookupPanelGrid.setId(FacesContext.getCurrentInstance().
                                getViewRoot().createUniqueId());

                        String lookupExp = null;
                        if ((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("") &&
                                fieldDD.getLookupType() == DDLookupType.DDLT_DRPDWN) ||
                                (null != fieldDD.getLookupFilter() && !"".equals(fieldDD.getLookupFilter())) &&
                                fieldDD.getLookupType() == DDLookupType.DDLT_LTRLDRPDN) {
                            lookupExp = "";//getLookupFldExpression(filterfields.get(i).getFieldExpression());//getDRDFldExpression(filterfields.get(i).getFieldExpression());
                        }else{
                            lookupExp = getLookupFldExpression(filterfields.get(i).getFieldExpression());
                        }

                        fldExp = getBeanName() + "." + "valuesMap['" + filterfields.get(i).getFieldExpression() + "']";
                        String lookupID = filterfields.get(i).getFieldExpression().replace(".", "_") + "_Lookup_";
                        fieldLookup = new Lookup(this,
                                getBeanName(),
                                fldExp,
                                screenField.getDd(),
                                lookupExp,
                                null,
                                lookupID,
                                screenField,
                                false,
                                oem, filterServiceBean,
                                exFactory,
                                getFacesContext(),
                                loggedUser);

                        getLookupHashMap().put(lookupID, fieldLookup);

                        if ((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("") &&
                                fieldDD.getLookupType() == DDLookupType.DDLT_DRPDWN) ||
                                (null != fieldDD.getLookupFilter() && !"".equals(fieldDD.getLookupFilter())) &&
                                fieldDD.getLookupType() == DDLookupType.DDLT_LTRLDRPDN) {
                            lookupPanelGrid.getChildren().add(fieldLookup.createDropDown(false));
                            getSelectItemsList().put(fieldLookup.getDropDownMenu().getId(),
                            fieldLookup.getSelectItems());

                        }else if((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("") &&
                                fieldDD.getLookupType() == DDLookupType.DDLT_MULTISELECTIONLIST)) {
                            lookupPanelGrid.getChildren().add(fieldLookup.createMultiSelectionList());
                            getSelectItemsList().put(fieldLookup.getManyListbox().getId(),
                            fieldLookup.getSelectItems());
                        }else {
                            lookupPanelGrid.getChildren().add(fieldLookup.createNonDropDown());
                            FramworkUtilities.setControlFldExp(fieldLookup.getInputText(), screenField.getFieldExpression());
                        }

                        component = lookupPanelGrid;
                    }else if(filterfields.get(i).getFieldDD().getControlType().getDbid() == DD.CT_TEXTFIELD){
                        component = createTextFieldControl(20, "filterfld_fld" + i, fldExp, true, screenField.getDd(), i, false, false ,false);
                    }else{
                        component = createControl(CNTRL_INPUTMODE,Integer.parseInt(Long.toString(filterfields.get(i).getFieldDD().getControlType().getDbid())),
                                filterfields.get(i).getFieldDD(), i, screenField, false, false, false,false);
                    }
                    
                    middlePanel.getChildren().add(component);

                } else {
                    String fromVal = null;
                    String toVal = null;
                    if (value != null && !value.equals("") && valueTo != null && !valueTo.equals("")) {
                        fromVal = value;
                        toVal = valueTo;

                    } else {
                        logger.warn("From-To Structure Error");
                    }
                    if (filterfields.get(i).getFieldDD().getControlType().getDbid() == DD.CT_DROPDOWN || filterfields.get(i).getFieldDD().getControlType().getDbid() == DD.CT_LOOKUPSCREEN) {
                        try {                                                        
                            BaseEntity currentObject = null;
                            BaseEntity currentObjectTo = null;

                            currentObject = getLookupBaseEntity(filterfields.get(i).getFieldExpression(), value,null);
                            currentObjectTo = getLookupBaseEntity(filterfields.get(i).getFieldExpression(), valueTo,null);

                            valuesMap.put(filterfields.get(i).getFieldExpression() + FROM_DELIM, currentObject);
                            valuesMap.put(filterfields.get(i).getFieldExpression() + TO_DELIM, currentObjectTo);

                        } catch (Exception e) {
                            valuesMap.put(filterfields.get(i).getFieldExpression() + FROM_DELIM, null);
                            valuesMap.put(filterfields.get(i).getFieldExpression() + TO_DELIM, null);
                        }
                    } else if (filterfields.get(i).getFieldDD().getControlType().getDbid() == DD.CT_CALENDAR) {
                        if (fromVal != null && !fromVal.equals("")) {
                            try {
                                valuesMap.put(filterfields.get(i).getFieldExpression() + FROM_DELIM, FilterField.dateFormat.parse(fromVal));
                            } catch (Exception e) {
                                valuesMap.put(filterfields.get(i).getFieldExpression() + FROM_DELIM, null);
                            }
                        } else {
                            valuesMap.put(filterfields.get(i).getFieldExpression() + FROM_DELIM, null);
                        }

                        if (toVal != null && !toVal.equals("")) {
                            try {
                                valuesMap.put(filterfields.get(i).getFieldExpression() + TO_DELIM, FilterField.dateFormat.parse(toVal));
                            } catch (Exception e) {
                                valuesMap.put(filterfields.get(i).getFieldExpression() + TO_DELIM, null);
                            }
                        } else {
                            valuesMap.put(filterfields.get(i).getFieldExpression() + TO_DELIM, null);
                        }

                    } else {
                        valuesMap.put(filterfields.get(i).getFieldExpression() + FROM_DELIM, fromVal);
                        valuesMap.put(filterfields.get(i).getFieldExpression() + TO_DELIM, toVal);
                    }


                    String fldExpFrom = "#{" + getBeanName() + "." + "valuesMap['" + filterfields.get(i).getFieldExpression() + FROM_DELIM + "']" + "}";
                    String fldExpTo = "#{" + getBeanName() + "." + "valuesMap['" + filterfields.get(i).getFieldExpression() + TO_DELIM + "']" + "}";

                    HtmlPanelGrid internalGrid = new HtmlPanelGrid();
                    internalGrid.setId("FromToGrid" + i);
                    internalGrid.setColumns(3);
                    internalGrid.setCellpadding("0px");
                    internalGrid.setCellspacing("0px");

                    OutputLabel text2 = new OutputLabel();
                    text2.setValue(ddService.getDD("RunFilterBeanTo", loggedUser).getLabelTranslated());
                    text2.setId("Txt2"+i);
                    
                    UIComponent component1 = null;
                    UIComponent component2 = null;

                    if (filterfields.get(i).getFieldDD().getControlType().getDbid() == DD.CT_TEXTFIELD) {
                        component1 = createTextFieldControl(20, "filterfld_fld_From" + i, fldExpFrom,
                                true, screenField.getDd(), i, false, false ,false);
                        component2 = createTextFieldControl(20, "filterfld_fld_To" + i, fldExpTo,
                                true, screenField.getDd(), i, false, false ,false);
                    } else if (filterfields.get(i).getFieldDD().getControlType().getDbid() == DD.CT_CHECKBOX) {
                        component1 = createCheckBoxControl("filterfld_fld" + i, fldExpFrom, true, screenField.getDd(), i, false);
                        component2 = createCheckBoxControl("filterfld_fld" + i, fldExpTo, true, screenField.getDd(), i, false);
                    } else if (filterfields.get(i).getFieldDD().getControlType().getDbid() == DD.CT_CALENDAR) {
                        component1 = createCalendarControl("filterfld_fld" + i, fldExpFrom, i, true, screenField.getDd(), i, false);
                        component2 = createCalendarControl("filterfld_fld" + i, fldExpTo, i, true, screenField.getDd(), i, false);
                    } else if (filterfields.get(i).getFieldDD().getControlType().getDbid() == DD.CT_DROPDOWN || filterfields.get(i).getFieldDD().getControlType().getDbid() == DD.CT_LOOKUPSCREEN) {
                        DD fieldDD = screenField.getDd();
                        //String lookupID = filterfields.get(i).getFieldExpression().replace(".", "_") + "_Lookup_";
                        HtmlPanelGrid lookupPanelGrid = new HtmlPanelGrid();
                        lookupPanelGrid.setId(FacesContext.getCurrentInstance().
                                getViewRoot().createUniqueId());

                        String lookupExp = null;
                         if ((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("") &&
                                fieldDD.getLookupType() == DDLookupType.DDLT_DRPDWN) ||
                                (null != fieldDD.getLookupFilter() && !"".equals(fieldDD.getLookupFilter())) &&
                                fieldDD.getLookupType() == DDLookupType.DDLT_LTRLDRPDN) {
                            lookupExp = "";//getDRDFldExpression(filterfields.get(i).getFieldExpression());
                        }else{
                            lookupExp = getLookupFldExpression(filterfields.get(i).getFieldExpression());
                        }
                        String fldExp1;
                        fldExp1 = getBeanName() + "." + "valuesMap['" + filterfields.get(i).getFieldExpression() + FROM_DELIM + "']";
                        String firstLookupID =  filterfields.get(i).getFieldExpression().replace(".", "_") + "_Lookup_1";
                        String secondLookupID =  filterfields.get(i).getFieldExpression().replace(".", "_") + "_Lookup_2";
                        fieldLookup = new Lookup(this,
                                getBeanName(),
                                fldExp1,
                                screenField.getDd(),
                                lookupExp,
                                null,
                                firstLookupID,
                                screenField,
                                false,
                                oem, filterServiceBean,
                                exFactory,
                                getFacesContext(),
                                loggedUser);

                        getLookupHashMap().put(firstLookupID, fieldLookup);

                        if ((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("") &&
                                fieldDD.getLookupType() == DDLookupType.DDLT_DRPDWN) ||
                                (null != fieldDD.getLookupFilter() && !"".equals(fieldDD.getLookupFilter())) &&
                                fieldDD.getLookupType() == DDLookupType.DDLT_LTRLDRPDN) {
                            lookupPanelGrid.getChildren().add(fieldLookup.createDropDown(false));
                            getSelectItemsList().put(fieldLookup.getDropDownMenu().getId(),
                            fieldLookup.getSelectItems());
                        }else if((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("") &&
                                fieldDD.getLookupType() == DDLookupType.DDLT_MULTISELECTIONLIST)) {
                            lookupPanelGrid.getChildren().add(fieldLookup.createMultiSelectionList());
                            getSelectItemsList().put(fieldLookup.getManyListbox().getId(),
                            fieldLookup.getSelectItems());
                        }else {
                            
                            lookupPanelGrid.getChildren().add(fieldLookup.createNonDropDown());
                            FramworkUtilities.setControlFldExp(fieldLookup.getInputText(), screenField.getFieldExpression());
                        }

                        component1 = lookupPanelGrid;


                        HtmlPanelGrid lookupPanelGridTo = new HtmlPanelGrid();
                        lookupPanelGridTo.setId(FacesContext.getCurrentInstance().
                                getViewRoot().createUniqueId());

                        String fldExp2;
                        fldExp2 = getBeanName() + "." + "valuesMap['" + filterfields.get(i).getFieldExpression() + TO_DELIM + "']";

                        fieldLookup = new Lookup(this,
                                getBeanName(),
                                fldExp2,
                                screenField.getDd(),
                                lookupExp,
                                null,
                                secondLookupID,
                                screenField,
                                false,
                                oem, filterServiceBean,
                                exFactory,
                                getFacesContext(),
                                loggedUser);

                        getLookupHashMap().put(secondLookupID, fieldLookup);

                        if ((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("") &&
                                fieldDD.getLookupType() == DDLookupType.DDLT_DRPDWN) ||
                                (null != fieldDD.getLookupFilter() && !"".equals(fieldDD.getLookupFilter())) &&
                                fieldDD.getLookupType() == DDLookupType.DDLT_LTRLDRPDN) {
                            lookupPanelGridTo.getChildren().add(fieldLookup.createDropDown(false));
                            getSelectItemsList().put(fieldLookup.getDropDownMenu().getId(),
                            fieldLookup.getSelectItems());
                        }else if((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("") &&
                                fieldDD.getLookupType() == DDLookupType.DDLT_MULTISELECTIONLIST)) {
                            lookupPanelGrid.getChildren().add(fieldLookup.createMultiSelectionList());
                            getSelectItemsList().put(fieldLookup.getManyListbox().getId(),
                            fieldLookup.getSelectItems());
                        }else {
                            lookupPanelGridTo.getChildren().add(fieldLookup.createNonDropDown());
                            FramworkUtilities.setControlFldExp(fieldLookup.getInputText(), screenField.getFieldExpression());
                        }

                        component2 = lookupPanelGridTo;
                    } else {
                        component1 = createTextFieldControl(20, "filterfld_fld_From" + i, fldExpFrom,
                                true, screenField.getDd(), i, false, false ,false);
                        component2 = createTextFieldControl(20, "filterfld_fld_To" + i, fldExpTo,
                                true, screenField.getDd(), i, false, false ,false);
                    }

                    internalGrid.getChildren().add(component1);
                    internalGrid.getChildren().add(text2);
                    internalGrid.getChildren().add(component2);

                    middlePanel.getChildren().add(internalGrid);
                }

            }
        }

        //<editor-fold defaultstate="collapsed" desc="Save Button and Category Code">

        CommandButton saveButton = new CommandButton();
        CommandButton runButton = new CommandButton();
        CommandButton saveRunButton = new CommandButton();


        String id = "button";

        saveButton.setId(id + "_saveButton");
        saveRunButton.setId(id + "_saveRunButton");
        runButton.setId(id + "_runButton");


        saveButton.setValue(ddService.getDD("saveButton", loggedUser).getLabelTranslated());
        saveRunButton.setValue(ddService.getDD("saveRunButton", loggedUser).getLabelTranslated());
        runButton.setValue(ddService.getDD("runButton", loggedUser).getLabelTranslated());

        saveButton.setActionExpression(exFactory.createMethodExpression(elContext,
                "#{" + getBeanName() + ".saveAction}", String.class, new Class[0]));


        saveRunButton.setActionExpression(exFactory.createMethodExpression(elContext,
                "#{" + getBeanName() + ".runSaveAction}", String.class, new Class[0]));

        runButton.setActionExpression(exFactory.createMethodExpression(elContext,
                "#{" + getBeanName() + ".runAction}", String.class, new Class[0]));

        //</editor-fold>
        //   ADD middlePanel TO htmlPanelGrid
        buttonsPanel.getChildren().add(runButton);
        buttonsPanel.getChildren().add(saveRunButton);
        buttonsPanel.getChildren().add(saveButton);

        buttonsPanel.setStyle("float:right; margin-right: 20px;");
        htmlPanelGrid.getChildren().add(middlePanel);
        //Hide the 'Save' and 'Save & Exit' buttons in process mode:
        if (getScreenParam("taskID") == null) {
            htmlPanelGrid.getChildren().add(buttonsPanel);
        }

        restorePDataFromSessoion = false;
       RequestContext context = RequestContext.getCurrentInstance();         context.execute(
                "top." + getPortletInsId() + "DataRestored();");
       logger.debug("Returning");
        return htmlPanelGrid;
    }

    @Override
    public HtmlPanelGrid createLabel(String id, ScreenField screenField) {
        logger.debug("Entering");
        try {
            //String controlLabel = screenField.getDd().getLabel();
            DD fieldDD = screenField.getDd();

            String controlLabel = (screenField.getDdTitleOverride() != null && !screenField.getDdTitleOverride().equals("")) ? screenField.getDdTitleOverride() : fieldDD.getLabel();
            String astrick = "";
            OutputLabel OutputLabel = new OutputLabel();
            if (id.contains(".")) {
                OutputLabel.setId(id.substring(id.lastIndexOf(".") + 1) + "__Label");
            } else {
                OutputLabel.setId(id + "_Label");
            }
            OutputLabel.setStyleClass("OTMSlabel");
            OutputLabel.setValue(controlLabel);

            OutputLabel astriskLabel = new OutputLabel();
            astriskLabel.setValue(astrick);
            astriskLabel.setStyle("color:red");
            HtmlPanelGrid headerLabelGrid = new HtmlPanelGrid();

            headerLabelGrid.setColumns(2);
            headerLabelGrid.setCellpadding("0");
            headerLabelGrid.setCellspacing("0");
            headerLabelGrid.getChildren().add(OutputLabel);
            headerLabelGrid.getChildren().add(astriskLabel);
logger.debug("Returning");
            return headerLabelGrid;
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            logger.debug("Returning");
            return new HtmlPanelGrid();
        }
    }

    private List<FilterField> updateFilterFields(List<FilterField> filterFields) {
        logger.trace("Entering");
        for (int idx = 0; idx < filterFields.size(); idx++) {
            if (filterFields.get(idx).isInActive()) {
                continue;
            }

            if (!filterFields.get(idx).isFromTo()) {
            if(valuesMap.get(filterFields.get(idx).getFieldExpression())!=null){
                if (valuesMap.get(filterFields.get(idx).getFieldExpression()) instanceof Date) {
                    Date date = (Date) valuesMap.get(filterFields.get(idx).getFieldExpression());
                    filterFields.get(idx).setFieldValue(FilterField.dateFormat.format(date));
                } else if (valuesMap.get(filterFields.get(idx).getFieldExpression()) instanceof BaseEntity) {
                    if(((BaseEntity)valuesMap.get(filterFields.get(idx).getFieldExpression())).getDbid() != 0){
                    filterFields.get(idx).setFieldValue(String.valueOf(((BaseEntity) valuesMap.get(filterFields.get(idx).getFieldExpression())).getDbid()));
                    }else{
                        filterFields.get(idx).setFieldValue(null);
                    }
                } else {
                    
                    filterFields.get(idx).setFieldValue(valuesMap.get(filterFields.get(idx).getFieldExpression()).toString());
                }
                }
            } else {
                if (valuesMap.get(filterFields.get(idx).getFieldExpression() + FROM_DELIM) != null && !valuesMap.get(filterFields.get(idx).getFieldExpression() + FROM_DELIM).equals("") && valuesMap.get(filterFields.get(idx).getFieldExpression() + TO_DELIM) != null && !valuesMap.get(filterFields.get(idx).getFieldExpression() + TO_DELIM).equals("")) {
                    if (valuesMap.get(filterFields.get(idx).getFieldExpression() + FROM_DELIM) instanceof Date && valuesMap.get(filterFields.get(idx).getFieldExpression() + TO_DELIM) instanceof Date) {
                        Date from = (Date) valuesMap.get(filterFields.get(idx).getFieldExpression() + FROM_DELIM);
                        Date to = (Date) valuesMap.get(filterFields.get(idx).getFieldExpression() + TO_DELIM);

                        filterFields.get(idx).setFieldValue(FilterField.dateFormat.format(from));
                        filterFields.get(idx).setFieldValueTo(FilterField.dateFormat.format(to));

                    } else if (valuesMap.get(filterFields.get(idx).getFieldExpression() + FROM_DELIM) instanceof BaseEntity && valuesMap.get(filterFields.get(idx).getFieldExpression() + TO_DELIM) instanceof BaseEntity) {
                        if(((BaseEntity)valuesMap.get(filterFields.get(idx).getFieldExpression()+ FROM_DELIM)).getDbid() != 0){
                        filterFields.get(idx).setFieldValue(String.valueOf(((BaseEntity) valuesMap.get(filterFields.get(idx).getFieldExpression() + FROM_DELIM)).getDbid()));
                        }else{
                            filterFields.get(idx).setFieldValue(null);
                        }
                        if(((BaseEntity)valuesMap.get(filterFields.get(idx).getFieldExpression()+ TO_DELIM)).getDbid() != 0){
                        filterFields.get(idx).setFieldValueTo(String.valueOf(((BaseEntity) valuesMap.get(filterFields.get(idx).getFieldExpression() + TO_DELIM)).getDbid()));
                        }else{
                            filterFields.get(idx).setFieldValueTo(null);
                        }

                    } else {
                        String filterFieldVal = valuesMap.get(filterFields.get(idx).getFieldExpression() + FROM_DELIM).toString();
                        String filterFieldValTo = valuesMap.get(filterFields.get(idx).getFieldExpression() + TO_DELIM).toString();
                        filterFields.get(idx).setFieldValue(filterFieldVal);
                        filterFields.get(idx).setFieldValueTo(filterFieldValTo);
                    }
                } else {
                    filterFields.get(idx).setFieldValue("");
                }
            }
        }
        logger.trace("Returning");
        return filterFields;
    }

    public String runAction() {
        logger.trace("Entering");
        try {            
            if (currentFilterType == FilterType.REPORT_FILTER) {
                logger.trace("REPORT_FILTER");
                OReport report = (OReport) getPassedEntity();
                List<FilterField> filterFields = updateFilterFields(report.getReportFilter().getFilterFields());
                report.getReportFilter().setFilterFields(filterFields);
                openReport(report);
            }
            else if (currentFilterType == FilterType.WEBSERVICE_FILTER) {
                logger.trace("WEBSERVICE_FILTER");
                OWebService service = (OWebService) getPassedEntity();
                List<FilterField> filterFields = updateFilterFields((service.getServiceFilter()).getFilterFields());
                service.getServiceFilter().setFilterFields(filterFields);
                runWebService(service, dataMsg);
            }
            else if (currentFilterType == FilterType.SCREEN_FILTER) {
                logger.trace("SCREEN_FILTER");
                OScreen screen = (OScreen) getPassedEntity();
                
                if(toBeOpenedScreenInstId== null )
                    toBeOpenedScreenInstId= generateScreenInstId(screen.getName());
                
                List<FilterField> filterFields = updateFilterFields((screen.getScreenFilter()).getFilterFields());
                screen.getScreenFilter().setFilterFields(filterFields);
                if(!wizardMode){
                    SessionManager.setScreenSessionAttribute(toBeOpenedScreenInstId, 
                            OPortalUtil.MESSAGE, dataMsg);
                }
                SessionManager.setScreenSessionAttribute(toBeOpenedScreenInstId, 
                        OBackBean.WIZARD_VAR, wizardMode);
                SessionManager.setScreenSessionAttribute(toBeOpenedScreenInstId, 
                        OBackBean.MODE_VAR, mode);
                SessionManager.setScreenSessionAttribute(toBeOpenedScreenInstId, 
                        OBackBean.FILTER_VAR, screen.getScreenFilter());

                if(wizardMode){
                   RequestContext context = RequestContext.getCurrentInstance();         context.execute(
                        "top.FABS.Portlet.setSrc({screenName:'" + screen.getName()
                            + "',screenInstId:'" + toBeOpenedScreenInstId
                            + "',screenHeight:'" + getScreenHeight()
                            + "',portletId:'" + getPortletInsId()
                            + "',viewPage:'" + screen.getViewPage()
                            + "',enableC2A:'fasle'});");
                } else {
                    openScreen(screen, toBeOpenedScreenInstId);
                }

            } else {
                logger.warn("Unsupported Filter Type");
            }
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            messagesPanel.addErrorMessage(usrMsgService.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
        } 
        logger.trace("Returning with empty string");
        return "";
    }

    public String saveAction() {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            OEntityDTO fieldOEntity = entitySetupService.
                    loadOEntityDTO(FilterField.class.getName(), systemUser);
            OEntityActionDTO updateAction = entitySetupService.getOEntityDTOActionDTO(
                    fieldOEntity.getDbid(), OEntityActionDTO.ATDBID_UPDATE, loggedUser);
            List<FilterField> filterFields = null;
            if (currentFilterType == FilterType.REPORT_FILTER) {
                logger.trace("REPORT_FILTER");
                OReport report = (OReport) getPassedEntity();
                filterFields = updateFilterFields(report.getReportFilter().getFilterFields());
            } else if (currentFilterType == FilterType.WEBSERVICE_FILTER) {
                logger.trace("WEBSERVICE_FILTER");
                OWebService service = (OWebService) getPassedEntity();
                filterFields = updateFilterFields(service.getServiceFilter().getFilterFields());
            } else if (currentFilterType == FilterType.SCREEN_FILTER) {
                logger.trace("SCREEN_FILTER");
                OScreen screen = (OScreen) getPassedEntity();
                filterFields = updateFilterFields(screen.getScreenFilter().getFilterFields());
            }
            if (filterFields != null && updateAction != null) {
                for (int idx = 0; idx < filterFields.size(); idx++) {
                    ODataMessage msg = entityServiceLocal.constructEntityODataMessage(filterFields.get(idx), loggedUser);
                    oFR.append(entitySetupService.executeAction(updateAction, msg, new OFunctionParms(), loggedUser));
                }
            } else {
                logger.warn("Error Saving the filter fields");
            }
            messagesPanel.clearMessages();
            messagesPanel.addMessages(oFR);
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            oFR.addError(usrMsgService.getUserMessage(
                    "SystemInternalError", loggedUser));
            messagesPanel.clearMessages();
            messagesPanel.addMessages(oFR);
        }
        RequestContext context = RequestContext.getCurrentInstance();         
        context.execute( "top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');");
        logger.debug("Returning \" \" ");
        return "";
    }

    @Override
    protected void buildScreenExpression(UIComponent component) {
        //Do Nothing
    }

    @Override
    public String returnSelected() {
        logger.debug("Entering");
        if (getLookupData() != null) {
            String mapKey = fieldLookup.getOwnerEntityBackBeanExpression().substring(
                    fieldLookup.getOwnerEntityBackBeanExpression().indexOf("'") + 1,
                    fieldLookup.getOwnerEntityBackBeanExpression().lastIndexOf("'"));
            valuesMap.put(mapKey, getLookupData());
            setLookupPanelRendered(false);
        }
        logger.debug("Returning with Null");
        return null;
    }

    @Override
    public String returnEmpty() {
        logger.debug("Entering");
        try{
            String mapKey = fieldLookup.getOwnerEntityBackBeanExpression().
                    substring(fieldLookup.getOwnerEntityBackBeanExpression().
                    indexOf("'") + 1, fieldLookup.getOwnerEntityBackBeanExpression().lastIndexOf("'"));
            valuesMap.put(mapKey, Class.forName(entitySetupService.
                    getOScreenEntityDTO(fieldLookup.getOScreen().getDbid(), loggedUser).getEntityClassPath()
                    ).newInstance());
            setLookupPanelRendered(false);
            } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            }
        logger.debug("Returning with Null");
        return null;
    }
    public String runSaveAction() {
        saveAction();
        runAction();
        return "";
    }
}
