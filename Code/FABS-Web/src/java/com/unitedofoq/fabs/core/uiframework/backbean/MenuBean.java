/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.function.JavaFunction;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OMenu;
import com.unitedofoq.fabs.core.function.OMenuFunction;
import com.unitedofoq.fabs.core.function.ServerJob;
import com.unitedofoq.fabs.core.function.WebServiceFunction;
import com.unitedofoq.fabs.core.multimedia.OIMageServiceLocal;
import com.unitedofoq.fabs.core.report.ReportFunction;
import com.unitedofoq.fabs.core.security.user.UserMenu;
import com.unitedofoq.fabs.core.uiframework.backbean.wizard.WizardController;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalPage;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import com.unitedofoq.fabs.core.uiframework.portal.PortalPageFunction;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenFunction;
import com.unitedofoq.fabs.core.uiframework.screen.UDCScreenFunction;
import com.unitedofoq.fabs.core.uiframework.screen.wizard.WizardFunction;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.MethodExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author htawab
 */
@ManagedBean(name = "MenuBean")
@ViewScoped
public class MenuBean extends FormBean {

    final static Logger logger = LoggerFactory.getLogger(MenuBean.class);
    
    DataTypeServiceRemote dtService = OEJB.lookup(DataTypeServiceRemote.class);
    
    OIMageServiceLocal imageService = OEJB.lookup(OIMageServiceLocal.class);

    private List<UserMenu> assignedMenuList = new ArrayList<UserMenu>();

    private String RESOURCES_UP = "/resources/up.jpg";
    private String RESOURCES_BLANK = "/resources/blank_transparent.png";
    private String RESOURCES_FOUND = "/resources/found.png";
    private String RESOURCES_DEFAULT = "/resources/default.png";
    private String RESOURCES_SEARCH = "/resources/search.png";
    private String IS = "IncludeSubs";
    private String AS = "Assigned";
    private String DISPLAYED_LEVEL = "displayedLevel";
    private String MOUSE_CLICKED = "mouseClicked";

    public MenuBean() {
    }

    /**
     * Counts of calling init() function
     */
    long initCallCount = 0;

    @PostConstruct
    @Override
    public void init() {
        initCallCount++;
        if (initCallCount > 1) // init is called twice for no clear reason and repeats all the same
        // functions, so, no stop second call processing to save time
        {
            return;
        }
        super.init();
        assignedMenuList = functionServiceRemote.getUserMenus(getLoggedUser());
    }

    @Override
    public HtmlPanelGrid buildInnerScreen() {
        HtmlPanelGrid generalPanel = new HtmlPanelGrid();
        generalPanel.setStyle("width:100%");
        generalPanel.setDir(htmlDir);
        generalPanel.setColumns(1);
        HtmlPanelGrid middlePanel;
        String menuCode = (String) SessionManager.getSessionAttribute(MENU_DBID);
        middlePanel = createMenu(menuCode);
        middlePanel.setDir(htmlDir);
        middlePanel.setStyle("width:100%");
        // ADD middlePanel TO generalPanel
        generalPanel.getChildren().add(middlePanel);

        return generalPanel;
    }

    @Override
    protected String getBeanName() {
        return "MenuBean";
    }

    private Integer displayedLevel = (Integer) SessionManager.getSessionAttribute(DISPLAYED_LEVEL);

    public HtmlPanelGrid createMenu(String menuCode) {
        logger.debug("Entering");
        Boolean menuClicked = (Boolean) SessionManager.getSessionAttribute(MOUSE_CLICKED);
        // Root menu
        if (menuCode == null) {
            signOn();
        } // Non-root menus
        else if (menuClicked != null && menuClicked == true) // Menu clicked
        {
            clickMenu(menuCode, false, true);
        } else // Refresh
        {
            clickMenu(menuCode, false, false);
        }
        logger.debug("Returning");
        return displayMenu();
    }

    private HtmlPanelGrid displayMenu() {
        logger.trace("Entering");
        //<editor-fold defaultstate="collapsed" desc="Variables">
        // Menus, Functions Panel
        HtmlPanelGrid mainPanel;
        HtmlPanelGrid itemsPanel;
        List<OMenu> oMenus = new ArrayList<OMenu>();
        List<OFunction> functions = new ArrayList<OFunction>();
        HtmlCommandLink funcLink;
        HtmlGraphicImage funcImg;
        HtmlPanelGrid funcNameCellPanel;
        HtmlPanelGrid funcImgCellPanel;
        HtmlCommandLink menuLink;
        HtmlGraphicImage menuImg;
        HtmlPanelGrid menuNameCellPanel;
        HtmlPanelGrid menuImgCellPanel;
        UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Intialize Main Panel & Items Panel">
        mainPanel = new HtmlPanelGrid();
        mainPanel.setStyle("width:100%");
        itemsPanel = new HtmlPanelGrid();
        itemsPanel.setStyle("width:100%");
        itemsPanel.setId("mainPanel");
        itemsPanel.setCellpadding("0px");
        itemsPanel.setCellspacing("0px");

        itemsPanel.setStyleClass("MenuItems");
        itemsPanel.setColumns(2);
        //</editor-fold>

        try {
            oem.getEM(loggedUser);
            // Construct OMenu List to be displayed
            Iterator<String> displayedMenuCodes = displayedMenus.keySet().iterator();
            while (displayedMenuCodes.hasNext()) {
                OMenu toBeDisplayedMenu = (OMenu) oem.loadEntity("OMenu",
                        Collections.singletonList("hierCode='" + displayedMenuCodes.next() + "'"), null,
                        oem.getSystemUser(loggedUser));
                oMenus.add(toBeDisplayedMenu);
            }

            Comparator menuComparator = new Comparator() {
                @Override
                public int compare(Object o1, Object o2) {
                    OMenu msg1 = (OMenu) o1;
                    OMenu msg2 = (OMenu) o2;
                    Integer level1index = msg1.getSortIndex();
                    Integer level2index = msg2.getSortIndex();
                    logger.trace("Returning");
                    return level1index.compareTo(level2index);
                }
            };
            Collections.sort(oMenus, menuComparator);

            Comparator functionsComparator = new Comparator() {
                @Override
                public int compare(Object o1, Object o2) {
                    OMenuFunction msg1 = (OMenuFunction) o1;
                    OMenuFunction msg2 = (OMenuFunction) o2;
                    Integer level1index = msg1.getSortIndex();
                    Integer level2index = msg2.getSortIndex();
                    logger.trace("Returning");
                    return level1index.compareTo(level2index);
                }
            };
            Collections.sort(toBeDisplayedFunctions, functionsComparator);

            //<editor-fold defaultstate="collapsed" desc=" Add htmlPanelGrid for Previous Menu">
            // Add htmlPanelGrid for Previous
            HtmlPanelGrid pMenuHeader = new HtmlPanelGrid();
            HtmlOutputText outputText = new HtmlOutputText();
            pMenuHeader.setColumns(2);

            outputText.setValue(SessionManager.getSessionAttribute(CURRENT_MENU_STR) + "    ");
            outputText.setId(viewRoot.createUniqueId() + "pMenuLabel");
            outputText.setStyleClass("MenuHeader");

            HtmlCommandButton commandButton = new HtmlCommandButton();
            commandButton.setType("image");
            commandButton.setId(viewRoot.createUniqueId() + "pMenuID");
            String imageURL = imageService.getImagePath("up.jpg", loggedUser);
            if (imageURL.equals("")) {
                commandButton.setImage(RESOURCES_UP);
            } else {
                commandButton.setImage(imageURL);
            }
            //No need to refersh page

            Class[] upParms = new Class[]{ActionEvent.class};
            MethodExpression upMethodExpr = exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                    "#{" + getBeanName() + ".upBTNActionListener}", null, upParms);
            MethodExpressionActionListener upBTNActionListener = new MethodExpressionActionListener(upMethodExpr);

            commandButton.addActionListener(upBTNActionListener);

            pMenuHeader.getChildren().add(outputText);
            pMenuHeader.getChildren().add(commandButton);

            displayedLevel = (Integer) SessionManager.getSessionAttribute(DISPLAYED_LEVEL);
            if (displayedLevel != 1) {
                mainPanel.getChildren().add(pMenuHeader);
            }

            HtmlPanelGrid space = new HtmlPanelGrid();
            space.setStyle("height:1px");

            HtmlPanelGrid space2 = new HtmlPanelGrid();
            space2.setStyle("height:1px");
            //</editor-fold>

            if (oMenus == null) {
                oMenus = new ArrayList<OMenu>();
            }

            for (OMenuFunction function : toBeDisplayedFunctions) {
                if (function.getOfunction() != null) {
                    functions.add(function.getOfunction());
                }
            }
            if (functions == null) {
                functions = new ArrayList<OFunction>();
            }

            //<editor-fold defaultstate="collapsed" desc="Creating Controls for Menus">
            for (OMenu cmenu : oMenus) {

                menuNameCellPanel = new HtmlPanelGrid();
                menuNameCellPanel.setStyleClass("MenuNameCellPanel");
                menuImgCellPanel = new HtmlPanelGrid();

                menuNameCellPanel.setId("MPL" + cmenu.getCode() + viewRoot.createUniqueId() + "NCP");
                menuImgCellPanel.setId("MIPL" + cmenu.getCode() + viewRoot.createUniqueId() + "ICP");

                menuLink = new HtmlCommandLink();
                String genLinkID = generateLinkID(cmenu.getDbid());
                menuLink.setId(genLinkID);
                menuLink.setValue(" " + cmenu.getNameTranslated());
                menuLink.setTitle("");
                menuLink.setStyleClass("MenuLink");
                // Menu Action Listener
                Class[] parms = new Class[]{ActionEvent.class};
                MethodExpression menuMethodExpr = exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                        "#{" + getBeanName() + ".menuProcessAction}", null, parms);
                MethodExpressionActionListener menuActionListener = new MethodExpressionActionListener(menuMethodExpr);
                menuLink.addActionListener(menuActionListener);

                HtmlOutputText seperator = new HtmlOutputText();
                seperator.setId("MDTS" + cmenu.getCode() + viewRoot.createUniqueId());
                seperator.setValue(" ");

                // Function Description
                HtmlOutputText menuDescText = new HtmlOutputText();
                menuDescText.setId("MDT" + cmenu.getCode() + viewRoot.createUniqueId());
                menuDescText.setValue(cmenu.getDescriptionTranslated());
                menuDescText.setStyleClass("MenuDescText");

                HtmlGraphicImage imgSep = new HtmlGraphicImage();
                imgSep.setUrl("/resources/menu_sep.png");
                imgSep.setStyle("width:100%");

                menuNameCellPanel.getChildren().add(menuLink);
                menuNameCellPanel.getChildren().add(menuDescText);
                menuNameCellPanel.getChildren().add(imgSep);

                // Transparent Image before function Image
                HtmlGraphicImage blankImgB = new HtmlGraphicImage();
                blankImgB.setId("bIM" + cmenu.getCode() + viewRoot.createUniqueId() + "B");
                String blankImgBPath = imageService.getImagePath("blank_transparent.png", loggedUser);
                if (!blankImgBPath.equals("")) {
                    blankImgB.setUrl(blankImgBPath);
                } else {
                    blankImgB.setUrl(RESOURCES_BLANK);
                }
                blankImgB.setHeight("10");
                blankImgB.setWidth("20");

                // Menu Image
                menuImg = new HtmlGraphicImage();
                menuImg.setId("IM" + cmenu.getCode() + viewRoot.createUniqueId());
                String menuImgPath = functionServiceRemote.getOMenuImagePath(cmenu, loggedUser);
                if (!menuImgPath.equals("")) {
                    menuImg.setUrl(menuImgPath);
                } else {
                    menuImg.setUrl(RESOURCES_FOUND);
                }
                menuImg.setStyleClass("MenuImg");

                // Transparent Image before function Image
                HtmlGraphicImage blankImgA = new HtmlGraphicImage();
                blankImgA.setId("IM" + cmenu.getCode() + "A" + viewRoot.createUniqueId());
                String blankImgAPath = imageService.getImagePath("blank_transparent.png", loggedUser);
                if (!blankImgAPath.equals("")) {
                    blankImgA.setUrl(blankImgAPath);
                } else {
                    blankImgA.setUrl(RESOURCES_BLANK);
                }
                blankImgA.setHeight("10");
                blankImgA.setWidth("35");
                menuImgCellPanel.getChildren().add(menuImg);
                // Add Function to Functions htmlPanelGrid
                itemsPanel.getChildren().add(menuImgCellPanel);
                itemsPanel.getChildren().add(menuNameCellPanel);
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Creating Controls for Functions">
            for (OFunction function : functions) {
                funcNameCellPanel = new HtmlPanelGrid();
                funcNameCellPanel.setStyleClass("FunctionNameCellPanel");
                funcImgCellPanel = new HtmlPanelGrid();
                funcNameCellPanel.setId("MPL" + viewRoot.createUniqueId() + "NCP");
                funcImgCellPanel.setId("MPL" + viewRoot.createUniqueId() + "ICP");
                funcLink = new HtmlCommandLink();
                String genLinkID = generateLinkID(function.getDbid());
                funcLink.setId(genLinkID);
                funcLink.setTitle("");
                if (function.getNameTranslated() == null || "".equals(function.getNameTranslated())) {
                    funcLink.setValue(" " + function.getName());
                } else {
                    funcLink.setValue(" " + function.getNameTranslated());
                }

                Class[] parms = new Class[]{ActionEvent.class};
                MethodExpression functionMethodExpr = exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                        "#{" + getBeanName() + ".functionActionListener}", null, parms);
                MethodExpressionActionListener functionActionListener = new MethodExpressionActionListener(functionMethodExpr);
                funcLink.addActionListener(functionActionListener);
                funcLink.setStyleClass("FunctionLink");
                HtmlOutputText seperator = new HtmlOutputText();
                seperator.setId("SP" + viewRoot.createUniqueId() + function.getCode());
                seperator.setValue(" ");

                // Function Description
                HtmlOutputText funcDescText = new HtmlOutputText();
                funcDescText.setId("FDT" + viewRoot.createUniqueId() + function.getCode());
                funcDescText.setValue(function.getDescriptionTranslated());
                funcDescText.setStyleClass("FunctionDescText");
                funcNameCellPanel.getChildren().add(funcLink);
                if (function.getDescriptionTranslated() != null && !function.getDescriptionTranslated().equals("")) {
                    funcNameCellPanel.getChildren().add(funcDescText);
                }
                HtmlGraphicImage imgSep = new HtmlGraphicImage();
                imgSep.setUrl("/resources/menu_sep.png");
                imgSep.setStyle("width:100%");
                funcNameCellPanel.getChildren().add(imgSep);
                // Transparent Image before function Image
                HtmlGraphicImage blankImgB = new HtmlGraphicImage();
                blankImgB.setId("IM" + viewRoot.createUniqueId() + "B");
                String blankImgBPath = imageService.getImagePath("blank_transparent.png", loggedUser);
                if (!blankImgBPath.equals("")) {
                    blankImgB.setUrl(blankImgBPath);
                } else {
                    blankImgB.setUrl(RESOURCES_BLANK);
                }
                blankImgB.setHeight("10");
                blankImgB.setWidth("20");

                // Function Image
                funcImg = new HtmlGraphicImage();
                funcImg.setId("FIM" + viewRoot.createUniqueId() + function.getCode());
                String funcImgPath = functionServiceRemote.getOFunctionImagePath(function, loggedUser);
                if (!funcImgPath.equals("")) {
                    funcImg.setUrl(funcImgPath);
                } else {
                    funcImg.setUrl(RESOURCES_DEFAULT);
                }
                funcImg.setStyleClass("FunctionImg");
                // Transparent Image before function Image
                HtmlGraphicImage blankImgA = new HtmlGraphicImage();
                blankImgA = new HtmlGraphicImage();
                blankImgA.setId("IM" + viewRoot.createUniqueId() + "A");
                String blankImgAPath = imageService.getImagePath("blank_transparent.png", loggedUser);
                if (!blankImgAPath.equals("")) {
                    blankImgA.setUrl(blankImgAPath);
                } else {
                    blankImgA.setUrl(RESOURCES_BLANK);
                }
                blankImgA.setHeight("10");
                blankImgA.setWidth("35");
                funcImgCellPanel.getChildren().add(funcImg);
                // Add Function to Functions htmlPanelGrid
                itemsPanel.getChildren().add(funcImgCellPanel);
                itemsPanel.getChildren().add(funcNameCellPanel);

            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Create Function Search InputText">
            HtmlInputText searchInputText = new HtmlInputText();
            searchInputText.setSize(20);
            searchInputText.setId("searchInputText");
            searchInputText.setImmediate(true);
            HtmlCommandButton searchButton = new HtmlCommandButton();
            searchButton.setId("searchButton");
            Class[] parms = new Class[]{ActionEvent.class};
            MethodExpression searchMethodExpr = exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                    "#{" + getBeanName() + ".searchButtonAction}", null, parms);
            MethodExpressionActionListener searchButtonAction = new MethodExpressionActionListener(searchMethodExpr);
            searchButton.addActionListener(searchButtonAction);
            String searchImageURL = imageService.getImagePath("search.png", loggedUser);
            if (!searchImageURL.equals("")) {
                searchButton.setImage(searchImageURL);
            } else {
                searchButton.setImage(RESOURCES_SEARCH);
            }

            HtmlPanelGrid searchPanel = new HtmlPanelGrid();
            searchPanel.setId("searchPanel");
            searchPanel.setColumns(2);
            searchPanel.getChildren().add(searchInputText);
            searchPanel.getChildren().add(searchButton);
            mainPanel.getChildren().add(searchPanel);
            mainPanel.getChildren().add(itemsPanel);
            //</editor-fold>

            mainPanel.setDir(htmlDir);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.trace("Returning");
            return mainPanel;
        }
    }

    public void searchButtonAction(ActionEvent event) {
        menuPanel.getChildren().add(searchAndDisplayFunctions());
        oem.closeEM(systemUser);
    }

    private HtmlPanelGrid searchAndDisplayFunctions() {
        logger.trace("Entering");
        UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
        HtmlPanelGrid resultsPanel = null;
        HtmlGraphicImage funcImg;
        HtmlPanelGrid funcImgCellPanel;
        HtmlPanelGrid pMenuHeader = new HtmlPanelGrid();
        pMenuHeader.setColumns(2);
        HtmlOutputText outputText = new HtmlOutputText();
        outputText.setValue("Search Results");
        outputText.setId(viewRoot.createUniqueId() + "pMenuLabel");
        outputText.setStyleClass("SearchResultHeader");
        HtmlCommandButton backButton = new HtmlCommandButton();
        backButton.setType("image");
        backButton.setId(viewRoot.createUniqueId() + "pMenuID");
        String imageURL = imageService.getImagePath("up.jpg", loggedUser);
        if (imageURL.equals("")) {
            backButton.setImage(RESOURCES_UP);
        } else {
            backButton.setImage(imageURL);
        }

        backButton.setOnclick("setTimeout('window.location.reload(true)',200);");
        pMenuHeader.getChildren().add(outputText);
        pMenuHeader.getChildren().add(backButton);
        HtmlCommandLink funcLink;
        try {
            oem.getEM(loggedUser);
            resultsPanel = new HtmlPanelGrid();
            HtmlInputText searchInputText = (HtmlInputText) menuPanel.findComponent("searchInputText");
            menuPanel.getChildren().clear();
            String keyword = searchInputText.getValue().toString();

            List<UserMenu> userMenus = new ArrayList<UserMenu>();
            List<OMenu> userMenusSubMenus = new ArrayList<OMenu>();
            List<OMenuFunction> menuFunctions = new ArrayList<OMenuFunction>();

            // Get all user menus
            userMenus = functionServiceRemote.getUserMenus(loggedUser);
            for (UserMenu userMenu : userMenus) {
                if (userMenu.getHierCode() == null) {
                    logger.warn("Null OMenu in UserMenu with DBID: {}", userMenu.getMenu_DBID());
                    continue;
                }

                List<OMenuFunction> userMenuFunctions = new ArrayList<OMenuFunction>();
                for (OMenuFunction oMenuFunction : userMenuFunctions) {
                    if (!menuFunctions.contains(oMenuFunction)) {
                        menuFunctions.add(oMenuFunction);
                    }
                }
                if (userMenu.isIncludeSubs()) {
                    userMenusSubMenus = oem.loadEntityList("OMenu", Collections.singletonList(
                            "hierCode LIKE '" + userMenu.getHierCode() + "%'"), null, null, loggedUser);
                    for (OMenu subMenuUserMenu : userMenusSubMenus) {
                        List<OMenuFunction> userMenusSubMenusMenuFunction = subMenuUserMenu.getMenuFunction();
                        for (OMenuFunction oMenuFunction : userMenusSubMenusMenuFunction) {
                            if (!menuFunctions.contains(oMenuFunction)) {
                                menuFunctions.add(oMenuFunction);
                            }
                        }
                    }
                }
            }

            HtmlPanelGrid funcNameCellPanel = new HtmlPanelGrid();
            HtmlPanelGrid itemsPanel = new HtmlPanelGrid();
            itemsPanel.setColumns(2);
            itemsPanel.setStyle("width:100%");

            OFunction function = null;
            OFunction tempFunction = new OFunction();
            for (BaseEntity menuFunction : menuFunctions) {
                function = ((OMenuFunction) menuFunction).getOfunction();
                if (function == null) {
                    continue;
                }
                if (function.getName() == null || function.getName().equals("")) {
                    continue;
                }

                // Cannot compare 'code' and 'dbid'. Errors will occure if there
                // are duplication in function code
                if (tempFunction.getDbid() == function.getDbid()) {
                    continue;
                }
                tempFunction = function;
                if (Pattern.compile(Pattern.quote(keyword), Pattern.CASE_INSENSITIVE).matcher(function.getName()).find()) {
                    funcNameCellPanel = new HtmlPanelGrid();
                    funcNameCellPanel.setStyleClass("FunctionNameCellPanel");
                    funcImgCellPanel = new HtmlPanelGrid();

                    funcNameCellPanel.setId("MPL" + viewRoot.createUniqueId());
                    funcImgCellPanel.setId("MPL" + viewRoot.createUniqueId());

                    funcLink = new HtmlCommandLink();
                    String genLinkID = generateLinkID(function.getDbid());
                    funcLink.setId(genLinkID);
                    funcLink.setTitle("");
                    if (function.getNameTranslated() == null || "".equals(function.getNameTranslated())) {
                        funcLink.setValue(" " + function.getName());
                    } else {
                        funcLink.setValue(" " + function.getNameTranslated());
                    }
                    Class[] parms2 = new Class[]{ActionEvent.class};
                    MethodExpression functionMethodExpr = exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                            "#{" + getBeanName() + ".functionActionListener}", null, parms2);
                    MethodExpressionActionListener functionActionListener = new MethodExpressionActionListener(functionMethodExpr);
                    funcLink.addActionListener(functionActionListener);
                    funcLink.setStyleClass("FunctionLink");

                    HtmlOutputText seperator = new HtmlOutputText();
                    seperator.setId("SP" + function.getCode() + viewRoot.createUniqueId());
                    seperator.setValue(" ");
                    HtmlOutputText funcDescText = new HtmlOutputText();
                    funcDescText.setId("FDT" + function.getCode() + viewRoot.createUniqueId());
                    if (function.getDescriptionTranslated() == null || "".equals(function.getDescriptionTranslated())) {
                        funcDescText.setValue(function.getDescription());
                    } else {
                        funcDescText.setValue(function.getDescriptionTranslated());
                    }
                    funcDescText.setStyleClass("FunctionDescText");
                    funcNameCellPanel.getChildren().add(funcLink);
                    //funcNameCellPanel.getChildren().add(seperator);
                    if (function.getDescriptionTranslated() != null && !function.getDescriptionTranslated().equals("")) {
                        funcNameCellPanel.getChildren().add(funcDescText);
                    }

                    // Transparent Image before function Image
                    HtmlGraphicImage blankImgB = new HtmlGraphicImage();
                    blankImgB.setId("IM" + viewRoot.createUniqueId() + "B");
                    String blankImgBPath = imageService.getImagePath("blank_transparent.png", loggedUser);
                    if (!blankImgBPath.equals("")) {
                        blankImgB.setUrl(blankImgBPath);
                    } else {
                        blankImgB.setUrl(RESOURCES_BLANK);
                    }
                    blankImgB.setHeight("10");
                    blankImgB.setWidth("20");

                    // Function Image
                    funcImg = new HtmlGraphicImage();
                    funcImg.setId("FIM" + function.getCode() + viewRoot.createUniqueId());
                    String funcImgPath = functionServiceRemote.getOFunctionImagePath(function, loggedUser);
                    if (!funcImgPath.equals("")) {
                        funcImg.setUrl(funcImgPath);
                    } else {
                        funcImg.setUrl(RESOURCES_DEFAULT);
                    }

                    funcImg.setHeight("35");
                    funcImg.setWidth("35");
                    // Transparent Image before function Image
                    HtmlGraphicImage blankImgA = new HtmlGraphicImage();
                    blankImgA = new HtmlGraphicImage();
                    blankImgA.setId("IM" + viewRoot.createUniqueId() + "A");
                    String blankImgAPath = imageService.getImagePath("blank_transparent.png", loggedUser);
                    if (!blankImgAPath.equals("")) {
                        blankImgA.setUrl(blankImgAPath);
                    } else {
                        blankImgA.setUrl(RESOURCES_BLANK);
                    }
                    blankImgA.setHeight("10");
                    blankImgA.setWidth("35");

                    // Image Cell htmlPanelGrid
                    funcImgCellPanel.getChildren().add(blankImgB);
                    funcImgCellPanel.getChildren().add(funcImg);
                    funcImgCellPanel.getChildren().add(blankImgA);

                    // Add Function to Functions htmlPanelGrid
                    itemsPanel.getChildren().add(funcImgCellPanel);
                    itemsPanel.getChildren().add(funcNameCellPanel);
                }
            }

            resultsPanel.getChildren().add(pMenuHeader);
            resultsPanel.getChildren().add(itemsPanel);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            oem.closeEM(loggedUser);
        }
        logger.trace("Returning");
        return resultsPanel;
    }

    private static Map<String, DisplayedMenuArray> displayedMenus
            = new HashMap<String, DisplayedMenuArray>();
    private static Map<String, DisplayedMenuArray> prevDisplayedMenus
            = new HashMap<String, DisplayedMenuArray>();

    public void menuProcessAction(ActionEvent event) throws AbortProcessingException {
        logger.debug("Entering");
        try {
            oem.getEM(systemUser);
            logger.trace("Menu Process Action Started");
//            displayedMenus.clear();
            // 'substring(5)' is used to get the dbid of the function, by substracting
            // the first 5 characters, which are (by generateLinkID): "ID" + 2 random numbers + "_"
            long clickedMenuDBID = Long.valueOf(((HtmlCommandLink) event.getComponent()).getId().substring(5));
            String clickedMenuTitle = String.valueOf(((HtmlCommandLink) event.getComponent()).getValue());
            SessionManager.addSessionAttribute(MENU_DBID, String.valueOf(clickedMenuDBID));
            SessionManager.addSessionAttribute(CURRENT_MENU_STR, clickedMenuTitle);
            SessionManager.addSessionAttribute(MOUSE_CLICKED, true);
            menuPanel.getChildren().clear();
            menuPanel.getChildren().add(buildInnerScreen());
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            oem.closeEM(systemUser);
            logger.debug("Returning");
        }
    }

    public void upBTNActionListener(ActionEvent event) {
        logger.debug("Entering");
        logger.trace("Previous Button Click Action Started");
        String MenuDBID = null;
        try {
            menuPanel.getChildren().clear();
            displayedLevel = (Integer) SessionManager.getSessionAttribute(DISPLAYED_LEVEL);
            // Root menu
            if (displayedLevel == 2) {
                signOn();
                SessionManager.addSessionAttribute(CURRENT_MENU_STR, "");
            } // If menu level greater than 2, click on parent of menu level -2
            else {
                MenuDBID = (String) SessionManager.getSessionAttribute(MENU_DBID);
                oem.getEM(loggedUser);
                OMenu currentMenu = (OMenu) oem.loadEntity("OMenu", Long.valueOf(MenuDBID), null, null,
                        oem.getSystemUser(loggedUser));
                String currentMenuCode = currentMenu.getHierCode();
                if (currentMenuCode.length() < 6) {
                    if (null != currentMenu) {
                        logger.warn("Unhandled menu: {} navigation case", currentMenu.getName());
                    }
                    signOn();
                    SessionManager.addSessionAttribute(CURRENT_MENU_STR, "");
                    // Will goto finally to display the menu before existing
                    logger.debug("Returning");
                    return;
                }
                String upMenuCode = currentMenuCode.substring(0, currentMenuCode.length() - 3);
                OMenu upMenu = (OMenu) oem.loadEntity("OMenu",
                        Collections.singletonList("hierCode = '" + upMenuCode + "'"), null,
                        oem.getSystemUser(loggedUser));

                String upMenuDBID = String.valueOf(upMenu.getDbid());
                SessionManager.addSessionAttribute(DISPLAYED_LEVEL, --displayedLevel);
                // Goto up menu
                clickMenu(upMenuDBID, true, false);
                SessionManager.addSessionAttribute(MENU_DBID, upMenuDBID);
                SessionManager.addSessionAttribute(CURRENT_MENU_STR, upMenu.getName());
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            oem.closeEM(systemUser);
            menuPanel.getChildren().add(displayMenu());
            logger.debug("Returning");
        }
    }

    public void functionActionListener(ActionEvent event) {
        logger.debug("Entering");
        try {
            oem.getEM(systemUser);
            String scrInstId = "";
            // 'substring(5)' is used to get the dbid of the function, by substracting
            // the first 5 characters, which are: "ID" + 2 random numbers + "_"
            long DBID = Long.valueOf(((HtmlCommandLink) event.getComponent()).getId().substring(5));
            OFunction ofunction = null;
            ofunction = functionServiceRemote.getOFunction(DBID, loggedUser);
            if (ofunction instanceof UDCScreenFunction) {
                UDCScreenFunction screenFunction = (UDCScreenFunction) ofunction;
                ODataType parentDT = dataTypeService.loadODataType("ParentDBID", systemUser);
                //FIXME: performance, load this ODataType once static (ondemand) in the class
                ODataMessage parentDataMessage = new ODataMessage();
                List dataList = new ArrayList();
                dataList.add(screenFunction.getType().getDbid());
                parentDataMessage.setData(dataList);
                parentDataMessage.setODataType(parentDT);

                //call construct screenInstId
                scrInstId = generateScreenInstId(screenFunction.getOscreen().getName());
                if (screenFunction.getScreenMode() != null) {
                    SessionManager.setScreenSessionAttribute(
                            scrInstId, MODE_VAR, screenFunction.getScreenMode().getDbid());
                }
                SessionManager.setScreenSessionAttribute(
                        scrInstId, OPortalUtil.MESSAGE, parentDataMessage);

                SessionManager.setScreenSessionAttribute(
                        scrInstId, OBackBean.WIZARD_VAR, false);

                openScreenCheckFilter(screenFunction.getOscreen(), parentDataMessage, scrInstId);
            } else if (ofunction instanceof ScreenFunction) {
                ScreenFunction screenFunction = (ScreenFunction) ofunction;
                logger.trace("Opening ScreenFunction");
                scrInstId = generateScreenInstId(screenFunction.getOscreen().getName());
                ODataType voidDataType = dataTypeService.loadODataType("VoidDT", systemUser);
                //FIXME: performance, load this ODataType once static (ondemand) in the class
                ODataMessage voidDataMessage = new ODataMessage();
                voidDataMessage.setData(new ArrayList<Object>());
                voidDataMessage.setODataType(voidDataType);

                if (screenFunction.getScreenMode() != null) {
                    SessionManager.setScreenSessionAttribute(
                            scrInstId, MODE_VAR, screenFunction.getScreenMode().getDbid());
                }
                SessionManager.setScreenSessionAttribute(
                        scrInstId, OPortalUtil.MESSAGE, voidDataMessage);

                SessionManager.setScreenSessionAttribute(
                        scrInstId, OBackBean.WIZARD_VAR, false);

                openScreenCheckFilter(screenFunction.getOscreen(), voidDataMessage, scrInstId);

            } else if (ofunction instanceof PortalPageFunction) {
                PortalPageFunction portalPageFunction = (PortalPageFunction) ofunction;
                ArrayList<String> fieldExpressions = new ArrayList<String>();
                fieldExpressions.add("portlets");
                OPortalPage oPortalPage = (OPortalPage) oem.loadEntity(
                        "OPortalPage", portalPageFunction.getPortalPage().getDbid(),
                        null, fieldExpressions, getLoggedUser());
            } else if (ofunction instanceof ReportFunction) {
                ReportFunction reportFunction = (ReportFunction) ofunction;
                openReportCheckFilter(reportFunction.getReport(), null);
            } else if (ofunction instanceof WizardFunction) {
                ODataType voidDataType = dataTypeService.loadODataType("VoidDT", systemUser);
                //FIXME: performance, load this ODataType once static (ondemand) in the class
                ODataMessage voidDataMessage = new ODataMessage();
                voidDataMessage.setData(new ArrayList<Object>());
                voidDataMessage.setODataType(voidDataType);

                WizardController wizardController = new WizardController(oem,
                        ((WizardFunction) ofunction).getWizard(), loggedUser, usrMsgService);
                wizardController.openWizard(voidDataMessage);

            } else if (ofunction instanceof WebServiceFunction) {
                //FIXME: add any log
            } else if (ofunction instanceof JavaFunction) {
                ODataMessage oDM = new ODataMessage();
                List<Object> data = new ArrayList();
                data.add(getLoggedUser());
                oDM.setData(data);
                OFunctionResult oFR = functionServiceRemote.runFunction(ofunction, oDM, null, getLoggedUser());
                String code = (String) oFR.getReturnValues().get(0);
                if (code != null) {
                    // Should be server job DBID of server job function if so
                    OScreen addEditServerJobScreen = (OScreen) oem.loadEntity("FormScreen",
                            Collections.singletonList("name='AddEditServerJob'"), null,
                            oem.getSystemUser(loggedUser));
                    ServerJob serverJob = (ServerJob) oem.loadEntity("ServerJob",
                            Collections.singletonList("dbid=" + code), null,
                            oem.getSystemUser(loggedUser));
                    ODataType serviceJobDT = dtService.loadODataType("ServerJob",
                            oem.getSystemUser(loggedUser));
                    ODataMessage screenDM = new ODataMessage();
                    screenDM.setData(new ArrayList<Object>());
                    screenDM.getData().add(serverJob);
                    screenDM.setODataType(serviceJobDT);
                    scrInstId = generateScreenInstId(addEditServerJobScreen.getName());
                    SessionManager.setScreenSessionAttribute(scrInstId,
                            OPortalUtil.MESSAGE, screenDM);
                    SessionManager.setScreenSessionAttribute(scrInstId, MODE_VAR, "18");

                    openScreenCheckFilter(addEditServerJobScreen, screenDM, scrInstId);
                }
            } else {
                ODataMessage oDM = new ODataMessage();
                List<Object> data = new ArrayList();
                data.add(getLoggedUser());
                oDM.setData(data);
                OFunctionResult oFR = functionServiceRemote.runFunction(ofunction, oDM, null, getLoggedUser());
            }
            logger.trace("Exit processAction");
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            oem.closeEM(systemUser);
            logger.debug("Returning");
        }
    }

    /*
     * This method is associated with function hyperlink action.
     */
    public String HL_Action() {
        return null;
    }
    protected long currentMenuID;
    protected final String MENU_DBID = "MENU_HIERCODE";
    protected final String PREVIOUS_MENU_DBID = "PMENU_HIERCODE";
    protected final String PREVIOUS_MENU_STR = "PMENU_STR";
    protected final String CURRENT_MENU_STR = "CMENU_STR";
    protected final String CURRENT_MENU_CODE = "CMENU_CODE";
    protected HtmlPanelGrid menuPanel = new HtmlPanelGrid();

    //<editor-fold defaultstate="collapsed" desc="Getter/Setters">
    public HtmlPanelGrid getMenuPanel() {
        logger.debug("Entering");
        try {
            oem.getEM(loggedUser);
            menuPanel.getChildren().clear();
            menuPanel.getChildren().add(buildInnerScreen());
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return menuPanel;
        }
    }

    public void setMenuPanel(HtmlPanelGrid menuPanel) {
        this.menuPanel = menuPanel;
    }

    boolean signedIn;

    private void signOn() {
        signedIn = true;
        displayedLevel = 1;
        SessionManager.addSessionAttribute(DISPLAYED_LEVEL, 1);
        SessionManager.addSessionAttribute(MENU_DBID, null);
        displayedMenus.clear();
        toBeDisplayedFunctions.clear();
        // Get all assigned menus
        for (UserMenu assignedMenu : assignedMenuList) {
            String hierCode = assignedMenu.getHierCode();
            // If menu code is root
            if (hierCode.length() == 3) {
                if (!displayedMenus.containsKey(hierCode)) {
                    displayedMenus.put(hierCode, new DisplayedMenuArray(false, true));
                }

                // If include subs, override the menu with include subs
                if (assignedMenu.isIncludeSubs()) {
                    displayedMenus.put(hierCode, new DisplayedMenuArray(true, true));
                }
            } // If menu not root
            else {
                // Get parent menu by code of menu level & add it to displayedMenus
                String parentCodeOfMenuLevel = hierCode.substring(0, 3);
                if (!displayedMenus.containsKey(parentCodeOfMenuLevel)) {
                    displayedMenus.put(parentCodeOfMenuLevel, new DisplayedMenuArray(false, false));
                }
            }
        }
        prevDisplayedMenus.putAll(displayedMenus);
    }

    private List<OMenuFunction> toBeDisplayedFunctions = new ArrayList<OMenuFunction>();

    private void clickMenu(String menuCode, boolean fromUp, boolean menuClicked) {
        try {
            // reset MOUSE_CLICKED
            SessionManager.addSessionAttribute(MOUSE_CLICKED, false);
            displayedLevel = (Integer) SessionManager.getSessionAttribute(DISPLAYED_LEVEL);
            if (menuClicked) {
                SessionManager.addSessionAttribute(DISPLAYED_LEVEL, displayedLevel + 1);
            } else // put same menu level when refresh
            {
                SessionManager.addSessionAttribute(DISPLAYED_LEVEL, displayedLevel);
            }

            toBeDisplayedFunctions.clear();
            // Get current menu
            OMenu clickedMenu = (OMenu) oem.loadEntity("OMenu", Long.valueOf(menuCode), null, null,
                    oem.getSystemUser(loggedUser));
            String clickedMenuCode = clickedMenu.getHierCode();
            Boolean includeSubs = false;
            Boolean assigned = false;
            if (fromUp) {
                if (!prevDisplayedMenus.isEmpty()) {
                    includeSubs = prevDisplayedMenus.get(clickedMenuCode).isIncludeSubs();
                    assigned = prevDisplayedMenus.get(clickedMenuCode).isAssigned();
                }
            } else if (!menuClicked) {
                if (!prevDisplayedMenus.isEmpty()) {
                    includeSubs = prevDisplayedMenus.get(clickedMenuCode).isIncludeSubs();
                    assigned = prevDisplayedMenus.get(clickedMenuCode).isAssigned();
                } else {
                    // User clicked refresh
                    if (SessionManager.getSessionAttribute(IS).toString().equals("true")) {
                        includeSubs = true;
                    } else {
                        includeSubs = false;
                    }

                    if (SessionManager.getSessionAttribute(AS).toString().equals("true")) {
                        assigned = true;
                    } else {
                        assigned = false;
                    }
                }
            } else {
                if (!displayedMenus.isEmpty()) {
                    includeSubs = displayedMenus.get(clickedMenuCode).isIncludeSubs();
                    SessionManager.addSessionAttribute(IS, includeSubs.toString());
                    assigned = displayedMenus.get(clickedMenuCode).isAssigned();
                    SessionManager.addSessionAttribute(AS, assigned.toString());
                } else {
                    // User clicked refresh
                    if (SessionManager.getSessionAttribute(IS).toString().equals("true")) {
                        includeSubs = true;
                    } else {
                        includeSubs = false;
                    }

                    if (SessionManager.getSessionAttribute(AS).toString().equals("true")) {
                        assigned = true;
                    } else {
                        assigned = false;
                    }
                }
            }

            prevDisplayedMenus.putAll(displayedMenus);
            displayedMenus.clear();

            // If currentMenu includes sub menus
            if (includeSubs) {
                // Get all sub menus
                List<OMenu> allSubMenus = functionServiceRemote.getUserMenuSubMenus(
                        loggedUser, clickedMenu, displayedLevel, loggedUser);

                for (OMenu subMenu : allSubMenus) {
                    String hierCode = subMenu.getHierCode();
                    if (!displayedMenus.containsKey(hierCode)) {
                        displayedMenus.put(hierCode, new DisplayedMenuArray(true, false));
                    }
                }
                // Current menu doesn't include sub menus
            } else {
                List<UserMenu> assgndMenusPrntCrntLvl = new ArrayList<UserMenu>();
                for (UserMenu assignedMenu : assignedMenuList) {
                    // Get all assigned menus with parent code (in menu level -1)?
                    if (assignedMenu.getHierCode().startsWith(clickedMenuCode)) {
                        assgndMenusPrntCrntLvl.add(assignedMenu);
                    }
                }

                for (UserMenu ampcl : assgndMenusPrntCrntLvl) {
                    // If menu of menu level
                    String hierCode = ampcl.getHierCode();
                    if (hierCode.length()
                            == (menuClicked ? displayedLevel + 1 : displayedLevel) * 3) {
                        // If not already added, add it
                        if (!displayedMenus.containsKey(hierCode)) {
                            displayedMenus.put(hierCode, new DisplayedMenuArray(false, true));
                        }
                        // If include subs, override the menu with include subs
                        if (ampcl.isIncludeSubs()) {
                            displayedMenus.put(hierCode, new DisplayedMenuArray(true, true));
                        }
                    } // Menu not in menu level
                    else if (hierCode.length() > clickedMenuCode.length()) {
                        String displayedParentMenuCode = hierCode.substring(0,
                                ((menuClicked ? displayedLevel + 1 : displayedLevel) * 3));
                        if (!displayedMenus.containsKey(displayedParentMenuCode)) {
                            displayedMenus.put(displayedParentMenuCode, new DisplayedMenuArray(
                                    false, false));
                        }
                    }
                }
            }

            // If current menu is assigned or includeSubs, display functions
            if ((!prevDisplayedMenus.isEmpty() && prevDisplayedMenus.get(clickedMenuCode).isAssigned()) || includeSubs || assigned) {
                toBeDisplayedFunctions = functionServiceRemote.
                        getUserMenuFunctions(getLoggedUser(),
                                clickedMenu, menuClicked ? displayedLevel + 1 : displayedLevel,
                                getLoggedUser());
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
    }
    //</editor-fold>

    /**
     *
     * @param itemDBID
     * @param isMenu true: if itemDBID is for menu
     * <br>false: if itemDBID is for function
     * @return
     */
    protected String generateLinkID(long itemDBID) {
        logger.trace("Entering");
        long generatedID = 10 + (int) (Math.random() * (99 - 10) + 1);
        if (generatedID < 10 || generatedID > 99) {
            logger.warn("Error in generating the menu code");
        }
        logger.trace("Returning with Generated Id: {}_{}", generatedID, itemDBID);
        return "ID" + generatedID + "_" + itemDBID;
    }
}
