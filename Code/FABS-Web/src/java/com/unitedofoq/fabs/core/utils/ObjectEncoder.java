/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author mostafan
 */
public class ObjectEncoder {
    public static byte[] encodeObjectToBytes(Serializable obj){
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            ObjectOutputStream obj_out = new ObjectOutputStream(bos);
            obj_out.writeObject(obj);
            return bos.toByteArray();
        } catch (IOException ex) {
            Logger.getLogger(ObjectEncoder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static String encodeObjectToBase64(Serializable obj){
        byte[] bytes =encodeObjectToBytes(obj);
        if(bytes == null){
            return null;
        }
        else{
            return new String(Base64.encodeBase64(bytes));
        }
    }
    public static Object decodeBytesToObject(byte[] bytes) {
        if(bytes == null){
            return null;
        }
        else{    
            try {
                ObjectInputStream obj_in = null;
                ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
                obj_in = new ObjectInputStream(bis);
                return obj_in.readObject();
            } catch (IOException ex) {
                Logger.getLogger(ObjectEncoder.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ObjectEncoder.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
    }
    public static Object decodeBase64ToObject(String base64) {
        byte[] bytes = Base64.decodeBase64(base64.getBytes());
        return decodeBytesToObject(bytes);
    
    }
    
    
    
    
    
    
}
