/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.alert.entities.FiredAlert;
import com.unitedofoq.fabs.core.alert.services.AlertServiceLocal;
import com.unitedofoq.fabs.core.comunication.ServerJobInbox;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.process.HumanTaskServiceRemote;
import com.unitedofoq.fabs.core.routing.ORoutingBeanLocal;
import com.unitedofoq.fabs.core.routing.ORoutingTaskInstance;
import com.unitedofoq.fabs.core.security.user.Inbox;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.OUserDTO;
import com.unitedofoq.fabs.core.security.user.UserServiceRemote;
import static com.unitedofoq.fabs.core.uiframework.backbean.InboxPage.logger;
import com.unitedofoq.fabs.core.uiframework.screen.SingleEntityScreen;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;

/**
 *
 * @author mmasoud
 */
public class InboxDataLoader {
    
    private final OUser loggedUser;
    private final HumanTaskServiceRemote humanTaskService;
    private final UserServiceRemote userService;
    private final AlertServiceLocal alertServiceLocal;
    private final OEntityManagerRemote oem;
    private final ORoutingBeanLocal routingService;
    private final EntitySetupServiceRemote entitySetupService;
    private final DDServiceRemote ddService;
    private final Map<String, String> filterFieldsMap;
    private final DataTable dataTable;
    private final SingleEntityScreen currentScreen;
    private final List oldListProvider;
    private final List listProvider;
    private final List<Long> displayedIDs;
    private final Map<String, Inbox> myTasks;
    private final String portletInsId; 
    private String onloadJSStr;
    private int unread, allTasks;
    private final boolean corporate;
    
    public InboxDataLoader(OUser loggedUser, HumanTaskServiceRemote humanTaskService, 
            UserServiceRemote userService, AlertServiceLocal alertServiceLocal,
            OEntityManagerRemote oem, ORoutingBeanLocal routingService,
            EntitySetupServiceRemote entitySetupService, Map<String, String> filterFieldsMap,
            DataTable dataTable, DDServiceRemote ddService, SingleEntityScreen currentScreen,
            String protletInsId){
        this.loggedUser = loggedUser;
        this.humanTaskService = humanTaskService;
        this.userService = userService;
        this.alertServiceLocal = alertServiceLocal;
        this.oem = oem;
        this.routingService = routingService;
        this.entitySetupService = entitySetupService;
        this.ddService = ddService;
        this.filterFieldsMap = filterFieldsMap;
        this.dataTable = dataTable;
        this.currentScreen = currentScreen;
        this.portletInsId = protletInsId;
        onloadJSStr = "";
        displayedIDs = new ArrayList<>();
        oldListProvider = new ArrayList();
        listProvider = new ArrayList();
        myTasks = new HashMap<>();
        unread = 0;
        allTasks = 0;
        corporate = false;
    }
    
    private void filltable() {
        logger.trace("Entering");
        try {
            ArrayList listProviderTMP = new ArrayList();
            List<Inbox> tasks = null;
            List<FiredAlert> firedAlerts = null;
            List<ServerJobInbox> serverJobInboxes = null;
            List<ORoutingTaskInstance> routingTaskInboxes = null;
            try {
//                tasks = humanTaskService.getUserInboxOnLoad(loggedUser);
                
                for (Inbox task : tasks) {
                    if (task.getTask() != null) {
                        displayedIDs.add(task.getTask().getId());
                    }
                }
                
                Comparator<Inbox> comparator = new Comparator<Inbox>() {
                    @Override
                    public int compare(Inbox o1, Inbox o2) {
                        return (int) (o2.getId() - o1.getId());
                    }
                };
                Collections.sort(tasks, comparator);
               
                String fromMailDN = "";
                String fromUserDN = "";
                
                for (Inbox task : tasks) {
                    myTasks.put(Long.toString(task.getId()), task);
                    if (task.getFromMail() != null) {
                        fromMailDN = userService.getUserDTO(task.getFromMail(), loggedUser).getDisplayName();
                    }
                    
                    task.setFromMail(fromMailDN);
                    
                    if (task.getFromUser() != null) {
                        fromUserDN = userService.getUserDTO(task.getFromUser(), loggedUser).getDisplayName();
                    }
                    
                    task.setFromUser(fromUserDN);
                    
                    unread += task.getState().equals("READY") || task.getState().equals("RESERVED") ? 1 : 0;
                }
                
                allTasks = tasks.size();
                firedAlerts = alertServiceLocal.getAllFiredAlert(loggedUser);
                serverJobInboxes = oem.loadEntityList(ServerJobInbox.class.getSimpleName(),
                        Collections.singletonList("userLoginName='" + loggedUser.getLoginName() + "'"), null,
                        null, loggedUser);
                OFunctionResult returnedOFR = routingService.getTaskInbox(loggedUser);
                if (returnedOFR != null && returnedOFR.getReturnValues() != null) {
                    routingTaskInboxes = returnedOFR.getReturnValues();
                }
            } catch (Exception ex) {
                logger.warn("Error While Parsing User");
                logger.error("Exception thrown", ex);
            }
            
            if (tasks != null && !tasks.isEmpty()) {
                for (Inbox curtaskInfo : tasks) {
                    String ln = curtaskInfo.getInitiator();
                    OUserDTO initdto = new OUserDTO();
                    initdto.setLoginName(ln);
                    if (ln != null) {
                        initdto = userService.getUserDTO(ln, loggedUser);
                        
                        if (initdto != null) {
                            if (initdto.getTenantID() != loggedUser.getTenant().getId() && !corporate) {
                                logger.warn("Request recieved From Another Tenant While Corporate is Disabled. Initiator Loginname", ln);
                                listProviderTMP.add(curtaskInfo);
                                continue;
                            }
                        } else {
                            continue;
                        }
                        listProviderTMP.add(curtaskInfo);
                    }
                }
            }

            //<editor-fold defaultstate="collapsed" desc="Handle Fired Alerts">
            if (firedAlerts != null && !firedAlerts.isEmpty()) {
                for (FiredAlert firedAlert : firedAlerts) {
                    Inbox inbox = new Inbox();
                    inbox.setId(firedAlert.getDbid());
                    
                    inbox.setFromUser("System Alert");
                    inbox.setProcess(false);
                    inbox.setTitle(firedAlert.getMessageSubject() + " <br> " + firedAlert.getMessageBody());
                    inbox.setState(firedAlert.getType());
                    inbox.setFromMail("System Alert");
                    inbox.setDate("" + DateFormat.getDateTimeInstance(DateFormat.DEFAULT,
                            DateFormat.SHORT).format(firedAlert.getAlertDate()));
                    inbox.setText("FA:" + firedAlert.getDbid());
                    listProviderTMP.add(inbox);
                }
            }
            //</editor-fold>

            if (null != serverJobInboxes && !serverJobInboxes.isEmpty()) {
                for (ServerJobInbox serverJobInbox : serverJobInboxes) {
                    Inbox inbox = new Inbox();
                    inbox.setId(serverJobInbox.getDbid());
                    inbox.setTitle(serverJobInbox.getTitle());
                    inbox.setText(serverJobInbox.getText());
                    inbox.setDate("" + serverJobInbox.getDoneDate());
                    listProviderTMP.add(inbox);
                }
            }
            if (routingTaskInboxes != null && !routingTaskInboxes.isEmpty()) {
                for (ORoutingTaskInstance routingTaskInstance : routingTaskInboxes) {
                    Inbox inbox = new Inbox();
                    inbox.setId(routingTaskInstance.getDbid());
                    OEntityDTO routingEntity = entitySetupService
                            .getOReportEntityDTO(routingTaskInstance.getDbid(), loggedUser);
                    routingTaskInstance.getRoutingOEntity();
                    inbox.setText("RM: " + routingTaskInstance.getDbid());
                    inbox.setDate("" + DateFormat.getDateTimeInstance(DateFormat.DEFAULT,
                            DateFormat.SHORT).format(routingTaskInstance.getAssignedTime()));
                    inbox.setColored(1);
                    listProviderTMP.add(inbox);
                }
            }
            oldListProvider.addAll(listProviderTMP);
            if (filterFieldsMap.isEmpty()) {
                listProvider.addAll(listProviderTMP);
            }
        } catch (Exception ex) {
            logger.warn("No Inbox Items Found");
            logger.error("Exception thrown", ex);
        }
        RequestContext context = RequestContext.getCurrentInstance();
        if (dataTable != null) {
            context.update(dataTable.getClientId());
        }
        String title;
        DD inboxTitle;
        if (allTasks == 0) {
            inboxTitle = ddService.getDD("NoInboxTitle", loggedUser);
        } else {
            inboxTitle = ddService.getDD("InboxTitle", loggedUser);
        }
        if (inboxTitle == null) {
            title = currentScreen.getHeaderTranslated() + " " + (allTasks == 0 ? ""
                    : " (" + unread + " Out of " + allTasks + ")");
        } else {
            title = currentScreen.getHeaderTranslated() + " " + inboxTitle.getHeaderTranslated();
        }
        title = title.replaceAll("UNREAD", unread + "").replaceAll("READ", (allTasks - unread) + "")
                .replaceAll("ALL", allTasks + "");
        context.execute("top.FABS.Portlet.setTitle({title:'" + title
                + "',portletId:'" + portletInsId + "'});");
        if (onloadJSStr.isEmpty()) {
            onloadJSStr += "top.FABS.Portlet.setTitle({title:'" + title
                    + "',portletId:'" + portletInsId + "'});";
        }
       
    }
}
