/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.entitydesigner.dto;

/**
 *
 * @author Mostafa
 */
public class RelationDTO {

    private int relationId;
    private EntityDTO source;
    private EntityDTO destination;
    private String sourceRelationType;
    private String destinationRelationType;
    private FieldDTO sourceField;
    private FieldDTO destinationField;
    private String relationName;

    public String getRelationName() {
        return relationName;
    }

    public void setRelationName(String relationName) {
        this.relationName = relationName;
    }

    public String getDestinationRelationType() {
        return destinationRelationType;
    }

    public void setDestinationRelationType(String destinationRelationType) {
        this.destinationRelationType = destinationRelationType;
    }

    public String getSourceRelationType() {
        return sourceRelationType;
    }

    public void setSourceRelationType(String sourceRelationType) {
        this.sourceRelationType = sourceRelationType;
    }

    /**
     * @return the source
     */
    public EntityDTO getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(EntityDTO source) {
        this.source = source;
    }

    /**
     * @return the destination
     */
    public EntityDTO getDestination() {
        return destination;
    }

    /**
     * @param destination the destination to set
     */
    public void setDestination(EntityDTO destination) {
        this.destination = destination;
    }

    public FieldDTO getDestinationField() {
        return destinationField;
    }

    public void setDestinationField(FieldDTO destinationField) {
        this.destinationField = destinationField;
    }

    public FieldDTO getSourceField() {
        return sourceField;
    }

    public void setSourceField(FieldDTO sourceField) {
        this.sourceField = sourceField;
    }

    public int getRelationId() {
        return relationId;
    }

    public void setRelationId(int relationId) {
        this.relationId = relationId;
    }
}
