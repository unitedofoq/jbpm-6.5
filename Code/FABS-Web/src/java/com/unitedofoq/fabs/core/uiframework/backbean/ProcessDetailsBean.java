/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;


import com.unitedofoq.fabs.core.process.jBPM.ProcessInstance;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlPanelGrid;
import org.primefaces.component.graphicimage.GraphicImage;
import org.primefaces.model.StreamedContent;

@ManagedBean(name = "ProcessDetailsBean")
@ViewScoped
public class ProcessDetailsBean extends FormBean {
    
    @Override
    @PostConstruct
    public void init() {
        super.init();
    }
    
    ProcessInstance processInstance;
    
    private StreamedContent content;
    
    @Override
    public HtmlPanelGrid buildInnerScreen() {
        logger.debug("Entering");
        htmlPanelGrid = new HtmlPanelGrid();
        htmlPanelGrid.setStyleClass("mainTable");
        try {
            processInput();
            Long entryTime = (Calendar.getInstance()).getTimeInMillis();
            // </editor-fold>
            HtmlPanelGrid containerPanel = new HtmlPanelGrid();
            containerPanel.setId(getFacesContext().getViewRoot().createUniqueId() + "_ContainerGrid");
            setGraphicImage(new GraphicImage());
            getGraphicImage().setId(getFacesContext().getViewRoot().createUniqueId() + "_ProcessDetails");
//            getGraphicImage().setUrl(processService.getProcessEngineURL(loggedUser) + "/gwt-console-server/rs/process/definition/"
//                        + processInstance.getProcessDefinition().getName() + "/image");
            getGraphicImage().setAlt("Process " + processInstance.getName() + " " + processInstance.getId());
            containerPanel.getChildren().add(getGraphicImage());
            htmlPanelGrid.getChildren().add(containerPanel);
            if (getScreenHeight() == null || getScreenHeight().equals("")) {
                onloadJSStr += "top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');";
            } else {
                onloadJSStr += "top.FABS.Portlet.updateIframeSizeWithHeight({iframeName:'" + getPortletInsId() + "Frame'"
                        + ",screenHeight:'" + getScreenHeight() + "'});";
            }
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            messagesPanel.addMessages(functionServiceRemote.constructOFunctionResultFromError(
                    "SystemInternalError", ex, loggedUser));
            logger.debug("Returning");
            return htmlPanelGrid;
        }
        logger.debug("Returning");
        return htmlPanelGrid;
    }

    //TODO added by Belal, this method should be removed, actually the whole class should be removed. But the deletion impact is kind of big
    @Override
    protected boolean processInput() {
        logger.trace("Entering");
        try {
            List data = getDataMessage().getData();
            String processId = data.get(2).toString();
            long processInstanceId = (Long) data.get(1);
            //processInstance = processService.getProcessInstance(processInstanceId, processId, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        logger.trace("Returning with True");
        return true;
    }

    public StreamedContent getContent() {
        return content;
    }

    public void setContent(StreamedContent content) {
        this.content = content;
    }
}
