/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;


import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import java.util.ArrayList;
import java.util.List;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import org.primefaces.component.ajaxstatus.AjaxStatus;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.graphicimage.GraphicImage;
import org.primefaces.component.panel.Panel;
import org.primefaces.event.RowEditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MEA
 */
@ManagedBean(name = "UserTenantRoleBean")
@ViewScoped
public class UserTenantRoleBean extends UserTenantRoleAdminBean {
final static Logger logger = LoggerFactory.getLogger(UserTenantRoleBean.class);
    @Override
    protected String getBeanName() {
        return "UserTenantRoleBean";
    }

    /**
     *
     */
    @Override
    public void init() {
        super.init();
        try {
            listProvider = oCentralEntityManager.getUserTenantRolesData(null);
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
            messagesPanel.clearAndDisplayMessages(oFR);
        }
    }

    @Override
    public HtmlPanelGrid buildInnerScreen() {
logger.debug("Entering");
        HtmlPanelGrid grid = new HtmlPanelGrid();
        grid.setColumns(1);
        grid.setDir(htmlDir);
        grid.setStyle("width:100%;border:hidden");

        if (messagesPanel == null) {
            messagesPanel = new UserMessagePanel(exFactory, elContext, getBeanName(), ddService, this, loggedUser);
        }

        Panel middlePanel = new Panel();
        if (screenLanguage.equalsIgnoreCase("Arabic")) {
            middlePanel.setStyle("position:relative;text-align:right;border:hidden;padding:0px;");
        } else {
            middlePanel.setStyle("position:relative;border:hidden;padding:0px;");
        }

        grid.getChildren().add(messagesPanel.createMessagePanel());
        List<ScreenField> screenFields = getCurrentScreen().getScreenFields();
        HtmlPanelGrid generalbuttonsGrid = new HtmlPanelGrid();
        generalbuttonsGrid.setId(getFacesContext().getViewRoot().createUniqueId() + "_GeneralButtonsGrid");
        generalbuttonsGrid.setStyleClass("buttonsGeneralPanel");

        generalbuttonsGrid.setId(getFacesContext().getViewRoot().createUniqueId() + "_bgImg");

        HtmlPanelGrid buttonsGrid = new HtmlPanelGrid();
        buttonsGrid.setId(getFacesContext().getViewRoot().createUniqueId() + "_ButtonsGrid");

        buttonsGrid.setColumns(9 + getCurrentScreen().getScreenActions().size());
        HtmlPanelGrid loadingIconPanel = new HtmlPanelGrid();
        loadingIconPanel.setId(getFacesContext().getViewRoot().createUniqueId() + "_load");
        loadingIconPanel.setStyleClass("loading-icon");
        AjaxStatus loadingState = new AjaxStatus();
        loadingState.setId(getFacesContext().getViewRoot().createUniqueId() + "_loadAjax");
        GraphicImage loadingImage = new GraphicImage();
        loadingImage.setId(getFacesContext().getViewRoot().createUniqueId() + "_loadImg");
        loadingImage.setStyleClass("loading-active");
        loadingState.getFacets().put("start", loadingImage);
        HtmlOutputText outputText = new HtmlOutputText();
        outputText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                + "_CompleteLBL");
        loadingState.getFacets().put("complete", outputText);
        loadingIconPanel.getChildren().add(loadingState);
        buttonsGrid.getChildren().add(loadingIconPanel);
        if (isScreenOpenedForLookup()) {
            buttonsGrid.getChildren().add(buildLookupBtnPanel("setLookupData"));
            createButtons(buttonsGrid);
        } else {
            if (!processMode) {
                createAddRowButton(buttonsGrid);
                createScreenActionsButtons(buttonsGrid);
            }
        }
        generalbuttonsGrid.getChildren().add(buttonsGrid);
        grid.getChildren().add(generalbuttonsGrid);


        // <editor-fold defaultstate="collapsed" desc="GetScreenFields & Build Table">
        dataTable1 = new DataTable();
        dataTable1.setResizableColumns(true);
        dataTable1.setStyleClass("dt-table");
        dataTable1.setFilterEvent("enter");
        dataTable1.setId(getCurrentScreen().getName() + "_dataTable1");
        dataTable1.setRows(pageSize);
        dataTable1.setEditable(true);
        dataTable1.setValueExpression("value",
                exFactory.createValueExpression(elContext, "#{" + getBeanName() + ".listProvider}", ArrayList.class));

        dataTable1.setVar("currentRow");
        dataTable1.setPaginatorAlwaysVisible(true);
        //For Paginator
        dataTable1.setPaginatorPosition("bottom");
        dataTable1.setPaginator(true);
        dataTable1.setPaginatorTemplate("{CurrentPageReport}  {FirstPageLink} {PreviousPageLink} {PageLinks} "
                + "{NextPageLink} {LastPageLink} {RowsPerPageDropdown}");

        FacesContext fc = FacesContext.getCurrentInstance();
        ExpressionFactory ef = fc.getApplication().getExpressionFactory();
        // <editor-fold defaultstate="collapsed" desc="Define Table and Its Properties">


        FABSAjaxBehavior rowEdit = new FABSAjaxBehavior();
        MethodExpression rowEditMe = ef.createMethodExpression(
                fc.getELContext(), "#{" + getBeanName() + ".editRow}",
                null, new Class[]{RowEditEvent.class});
        rowEdit.setListener(rowEditMe);
        rowEdit.setImmediate(false);

        rowEdit.setOnstart("validateMandaroryFields();");
        rowEdit.setOncomplete("KeepEditable()");

        FABSAjaxBehavior rowCancelEdit = new FABSAjaxBehavior();
        MethodExpression rowCancelEditMe = ef.createMethodExpression(
                fc.getELContext(), "#{" + getBeanName() + ".cancelEditRow}",
                null, new Class[]{RowEditEvent.class});
        rowCancelEdit.setListener(rowCancelEditMe);
        dataTable1.addClientBehavior("rowEditCancel", rowCancelEdit);
        dataTable1.addClientBehavior("rowEdit", rowEdit);
        //</editor-fold>

        createTableColumns(dataTable1, true, "currentRow", "updateRow");
        //</editor-fold>
        middlePanel.getChildren().add(dataTable1);
        grid.getChildren().add(middlePanel);
        logger.debug("Returning");
        return grid;
    }
    // </editor-fold>
}
