/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.process.HumanTaskServiceRemote;
import com.unitedofoq.fabs.core.security.user.Inbox;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import javax.naming.NamingException;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mmasoud
 */
public class InboxLazyDataModel extends LazyDataModel<Inbox> {

    final static Logger LOGGER = LoggerFactory.getLogger(LazyBaseEntityModel.class);

    private HumanTaskServiceRemote humanTaskService = null;
    private final OUser loggedUser;
    private List<Inbox> inboxList;

    public InboxLazyDataModel(OUser loggedUser, List<Inbox> tasks) {
        this.loggedUser = loggedUser;
        this.inboxList = tasks;
        try {
            humanTaskService = (HumanTaskServiceRemote) (new javax.naming.InitialContext()).lookup("java:global/ofoq/com.unitedofoq.fabs.core.process.HumanTaskServiceRemote");
        } catch (NamingException ex) {
            java.util.logging.Logger.getLogger(InboxLazyDataModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Inbox> load(int first, int pageSize, String sortedField, SortOrder sortOrder, Map<String, String> filterMap) {
        LOGGER.debug("Entering");
        String fieldToSort = null;
        String order = sortOrder.name().substring(0, sortOrder.name().indexOf("ENDING"));
        Map<String, String> filters = null;
        if (sortedField == null) {
            fieldToSort = "t.createdOn";
            order = "DESC";
        } else {
            fieldToSort = adaptFieldParameter(sortedField);
        }
        if (filterMap != null) {
            filters = new HashMap<>();
            for (Entry filter : filterMap.entrySet()) {
                filters.put(adaptFieldParameter((String) filter.getKey()), (String) filter.getValue());
            }
        }

        Entry<Long, List<Inbox>> entry = humanTaskService.
                getUserInboxOnLoad(loggedUser, first / pageSize, pageSize, fieldToSort, order, filters)
                .entrySet().iterator().next();
        this.setRowCount(entry.getKey().intValue());
        inboxList.clear();
        inboxList.addAll(entry.getValue());
        return entry.getValue();
    }

    public Inbox getTaskById(Long taskId) {
        List<Inbox> loadedList = (List<Inbox>) getWrappedData();
        for (Inbox task : loadedList) {
            if (task.getId() == taskId) {
                return task;
            }
        }
        return null;
    }

    private String adaptFieldParameter(String fieldParameter) {
        switch (fieldParameter) {
            case "initiator":
                fieldParameter = "td.requesterDisplayname";
                break;
            case "fromMail":
                fieldParameter = "td.lastuserDisplayname";
                break;
            case "title":
                fieldParameter = "td.taskname";
                break;
            case "date":
                fieldParameter = "t.createdOn";
                break;
            case "state":
                fieldParameter = "t.status";
        }
        return fieldParameter;
    }

}
