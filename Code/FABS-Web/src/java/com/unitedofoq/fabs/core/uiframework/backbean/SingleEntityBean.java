/*
 4 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.datatype.ODataTypeAttribute;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDLookupType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitysetup.EntityAttributeInit;
import com.unitedofoq.fabs.core.entitysetup.OEntityAction;
import com.unitedofoq.fabs.core.entitysetup.OObjectBriefInfoField;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.filter.OFilterServiceLocal;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.i18n.TextTranslationException;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.multimedia.OImage;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.setup.FABSPropertiesServiceLocal;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import com.unitedofoq.fabs.core.uiframework.portal.ScreenRenderInfo;
import com.unitedofoq.fabs.core.uiframework.screen.M2MRelationScreen;
import com.unitedofoq.fabs.core.uiframework.screen.MultiLevelScreen;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenAction;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.uiframework.screen.SingleEntityScreen;
import com.unitedofoq.fabs.core.uiframework.screen.TabularScreen;
import com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenDTMappingAttrDTO;
import com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenInputDTO;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.utils.UIFieldUtility;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.el.MethodExpression;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlOutputLink;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.MethodBinding;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.MethodExpressionValidator;
import javax.faces.validator.ValidatorException;
import javax.persistence.EntityManager;
import javax.persistence.OneToOne;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.commandlink.CommandLink;
import org.primefaces.component.graphicimage.GraphicImage;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.inputtextarea.InputTextarea;
import org.primefaces.component.menu.Menu;
import org.primefaces.component.menuitem.MenuItem;
import org.primefaces.component.message.Message;
import org.primefaces.component.password.Password;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.component.selectonelistbox.SelectOneListbox;
import org.primefaces.component.submenu.Submenu;
import org.primefaces.context.RequestContext;
import org.primefaces.extensions.component.ckeditor.CKEditor;
import org.primefaces.extensions.component.timepicker.TimePicker;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nkhalil
 */
public abstract class SingleEntityBean extends OBackBean {

    public static final int CHARACTER_LENGTH_IN_PIXEL = 8;
    final static Logger logger = LoggerFactory.getLogger(SingleEntityBean.class);
    List<String> fieldIDs = new ArrayList<String>();
    Map<String, String> fieldsControlIdsMap = new HashMap<String, String>();

    private OFilterServiceLocal filterService = OEJB.lookup(OFilterServiceLocal.class);

    private FABSPropertiesServiceLocal fabsPropertiesServiceLocal = OEJB.lookup(FABSPropertiesServiceLocal.class);
    String transFldExp;
    protected Map<String, UIComponent> translatedCtrls = new HashMap<String, UIComponent>();
    private String imageURL;
    protected TreeNode[] selectedNodes;
    protected OObjectBriefInfoField masterField;
    String lastAutoCompleteComponentID;
    private final int COMPLEX_NONLOOKUP_CONTROL[] = {DD.CT_CALENDAR};//the controls that are panel grids even if not mandatory
    List lookupTransformedPickedElements = new LinkedList();
    LinkedList<BaseEntity> lookupPickedElements = new LinkedList();
    long lookupMainScr;

    private boolean isComplexNonLookUpControl(int controlType) {
        boolean answer = false;
        for (int i = 0; i < COMPLEX_NONLOOKUP_CONTROL.length; i++) {
            answer |= (controlType == COMPLEX_NONLOOKUP_CONTROL[i]);
        }
        return answer;

    }

    public TreeNode[] getSelectedNodes() {
        return selectedNodes;
    }

    public void setSelectedNodes(TreeNode[] selectedNodes) {
        this.selectedNodes = selectedNodes;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
    protected Class actOnEntityCls;
    protected List<SelfRelationTreeElement> mainElements = new ArrayList<SelfRelationTreeElement>();
    protected Field parentFLD = null, childFLD = null;
    protected Class lkbClass = null;
    protected CommandButton lkpBtn;
    protected String imageString;
    protected HashMap<String, Object> valuesMap = new HashMap<String, Object>();

    public HashMap<String, Object> getValuesMap() {
        return valuesMap;
    }

    public void setValuesMap(HashMap<String, Object> valuesMap) {
        this.valuesMap = valuesMap;
    }

    public String getImageString() {
        return imageString;
    }

    public void setImageString(String imageString) {
        this.imageString = imageString;
    }

    public List<SelfRelationTreeElement> getMainElements() {
        return mainElements;
    }

    public void setMainElements(List<SelfRelationTreeElement> mainElements) {
        this.mainElements = mainElements;
    }

    public ArrayList<String> getDataFilter() {
        return dataFilter;
    }

    public void setDataFilter(ArrayList<String> dataFilter) {
        this.dataFilter = dataFilter;
    }
    public ArrayList<String> dataFilter = new ArrayList<String>();

    // <editor-fold defaultstate="collapsed" desc="Abstract Functions">
    /**
     * Must be overridden by subclasses to return list/single loaded entities.
     * Loaded entities are the displayed ones.
     *
     * @return Empty list if nothing loaded. Shouldn't return null
     */
    @Override
    abstract protected List<BaseEntity> getLoadedEntity();
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Screen Inputs/Outputs Handling ">
    /**
     * Validates inputs, and set {@link SingleEntityBean#headerObject} if so
     *
     * @return false: in case of invalid input or error <br>true: success
     */
    @Override
    protected boolean processInput() {
        logger.trace("Entering");
        if (!super.processInput()) {
            logger.trace("Returning with False");
            return false;
        }

        try {
            // Process parent passed if any
            if (!compositeInputRequired) {
                // Simple mode, one passed object at a time
                if (dataMessage.getODataType().getDbid() == ODataType.DTDBID_VOID) // Not object passed
                {
                    setHeaderObject(null);  // Reset it if any code initialized it
                    // Cut it short and return success
                    //return true;
                }
            } else // Composite input is required
            {
                if (dataMessage.getODataType().getDbid() == ODataType.DTDBID_VOID) // Not object passed
                {
                    // Leave header object AS IS. If it's null, so be it,
                    // if already preset to one of the composite inputs (in the
                    // code below) then leave it
                    // Cut it short and return success
                    logger.trace("Returning with True");
                    return true;
                }
            }
            logger.trace("Returning");
            return processInputForHeader();

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(
                    usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
            logger.trace("Returning with False");
            return false;
        }
    }

    /**
     * <br>. Case oDT is ODataType.DTDBID_PARENTDBID, entity must be the parent
     * itself, then oDM is returned with entity DBID & oDT
     *
     * @param oDT
     * @param entity
     * @return
     */
    @Override
    public OFunctionResult constructOutput(ODataType oDT, BaseEntity entity) {
        logger.debug("Entering");
        OFunctionResult oFR = super.constructOutput(oDT, entity);
        try {
            if (oFR.getReturnedDataMessage() != null) // Case supported
            {
                if (null != getSelectedEntityBriefInfoField(entity)
                        && !"null".equalsIgnoreCase(getSelectedEntityBriefInfoField(entity))) {
                }
                logger.debug("Returning");
                return oFR;
            }
            if (!oFR.getErrors().isEmpty()) // Error
            {
                logger.debug("Returning");
                return oFR;
            }
            // Case Not Supported by super class with no errors

            ODataMessage odm = new ODataMessage();
            List odmData = new ArrayList();
            // <editor-fold defaultstate="collapsed" desc="Case DTDBID_PARENTDBID">
            if (oDT.getDbid() == ODataType.DTDBID_DBID
                    || oDT.getDbid() == ODataType.DTDBID_PARENTDBID) {
                odmData.add(entity.getDbid());
                odm.setODataType(oDT);
                odm.setData(odmData);
                oFR.setReturnedDataMessage(odm);
                logger.debug("Returning");
                return oFR;
            }
            // </editor-fold>

            // Check parent & parent list
            boolean parentDT = false;
            boolean parentListDT = false;
            // <editor-fold defaultstate="collapsed" desc="init parentDT, parentListDT">
            List<Class> superClasses = BaseEntity.getSuperEntityClasses(
                    Class.forName(
                            entitySetupService.getOScreenEntityDTO(currentScreen.getDbid(), loggedUser).
                            getEntityClassPath()));
            for (int idx = 0; idx < superClasses.size(); idx++) {
                if (oDT.getName().equalsIgnoreCase(superClasses.get(idx).getSimpleName() + "List")) {
                    parentListDT = true;
                    break;
                }
            }
            // why do we merge them? what is the relation?
            for (int i = 0; i < superClasses.size(); i++) {
                if (oDT.getName().equalsIgnoreCase(superClasses.get(i).getSimpleName())) {
                    parentDT = true;
                    break;
                }
            }
            // </editor-fold>

            if (null != actOnEntityCls
                    && oDT.getName().equals(actOnEntityCls.getSimpleName()) || parentDT) {
                odmData.add(entity);
                odmData.add(getLoggedUser());
                odm.setData(odmData);
                odm.setODataType(oDT);
                oFR.setReturnedDataMessage(odm);
                logger.debug("Returning");
                return oFR;
            }

            /*
             * MailMerge
             */
            if (oDT.getDbid() == ODataType.MAILMERGE_DBID) {

                odmData.add(getOactOnEntity().getDbid());
                odmData.add(entity);
                odmData.add(getLoggedUser());
                odm.setODataType(oDT);
                odm.setData(odmData);
                oFR.setReturnedDataMessage(odm);
                logger.debug("Returning");
                return oFR;
            }

            if (oDT.getName().equals(
                    entitySetupService.getOScreenEntityDTO(currentScreen.getDbid(), loggedUser)
                    .getEntityClassName() + "List")
                    || parentListDT) {
                if (headerObject != null) {
                    odmData.add(headerObject);
                } else {
                    odmData.add(new BaseEntity());
                }

                List<BaseEntity> objectsList = null;
                List<BaseEntity> tobeDeletedList = null;

                if (currentScreen instanceof M2MRelationScreen) {
                    objectsList = new ArrayList<BaseEntity>();
                    tobeDeletedList = new ArrayList<BaseEntity>();
                    for (int i = 0; i < getAllEntities().size(); i++) {
                        boolean checked = Boolean.parseBoolean(getValueFromEntity(getAllEntities().get(i), "m2mCheck").toString());
                        if (checked) {
                            objectsList.add(getAllEntities().get(i));
                        } else if (getAllEntities().get(i).getDbid() != 0) {
                            tobeDeletedList.add(getAllEntities().get(i));
                        }
                    }
                } else if (!compositeInputRequired) {
                    objectsList = getAllEntities();//uiFrameWorkFacade.getDBUnionDisplayRecords(headerObject, getAllEntities(), loggedUser);
                } else {
                    // Entities new & old with screen & input filter
                    objectsList = getAllEntities();
                }
                odmData.add(objectsList);

                if (tobeDeletedList != null && !tobeDeletedList.isEmpty()) {
                    odmData.add(tobeDeletedList);
                } else {
                    odmData.add(new ArrayList<BaseEntity>());
                }
                odm.setData(odmData);
                odm.setODataType(oDT);
                oFR.setReturnedDataMessage(odm);
                logger.debug("Returning");
                return oFR;
            }

            if (oFR.getReturnedDataMessage() != null) {
                oFR.getReturnedDataMessage().getData().add(getSelectedEntityBriefInfoField(entity));
            }
            logger.debug("Returning");
            return oFR;    // Empty odm, not supported

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
            logger.debug("Returning");
            return oFR;
        }
    }
    // </editor-fold>

    protected String getSelectedEntityBriefInfoField(BaseEntity entity) {
        logger.trace("Entering");
        try {
            //Load brief info field of selected entity
            OObjectBriefInfoField masterField = null;
            List<OObjectBriefInfoField> briefInfoFields = null;
            briefInfoFields = entitySetupService.getObjectBriefInfoFields(entitySetupService.loadOEntity(actOnEntityCls.getName(), loggedUser), loggedUser);
            String briefInfoFieldValue;
            if (briefInfoFields != null && !briefInfoFields.isEmpty()) {
                for (int i = 0; i < briefInfoFields.size(); i++) {
                    if (briefInfoFields.get(i).isMasterField()) {
                        masterField = briefInfoFields.get(i);
                        break;
                    }
                }
                if (masterField != null) {
                    String fldExpr = masterField.getFieldExpression();
                    briefInfoFieldValue = String.valueOf(entity.invokeGetter(fldExpr));
                    logger.trace("Returning");
                    return briefInfoFieldValue;
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Returning with \"default\" ");
            return "default";
        }
        logger.trace("Returning with \"default\" ");
        return "default";
    }

    protected void createScreenActionsButtons(HtmlPanelGrid buttonPanel) {
        createScreenActionsButtons(buttonPanel, null);
    }

    protected void createScreenActionsButtons(HtmlPanelGrid buttonPanel, String element2Update) {
        logger.debug("Entering");
        List<ScreenAction> screenActions = getValidScreenActions();
        sortValidScreenActions(screenActions);
        addScreenActionsButtonsToButtonPanel(screenActions, buttonPanel, element2Update);
        logger.debug("Returning");
    }

    private void addScreenActionsButtonsToButtonPanel(List<ScreenAction> screenActions, HtmlPanelGrid buttonPanel, String element2Update) {
        logger.trace("Returning");
        //Rehab: in case processMode to add action buttons we should increase columns #
        buttonPanel.setColumns(buttonPanel.getColumns() + screenActions.size());
        for (int i = 0; i < screenActions.size(); i++) {
            ScreenAction screenAction = screenActions.get(i);
            OEntityAction action = screenAction.getAction();
            Long actionDBID = action.getDbid();
            CommandButton button = createButton(buttonPanel, "", null, null, getBeanName());
            button.setStyleClass(action.getName() + "_btn ");
            button.setTitle(action.getNameTranslated());
            button.getAttributes().put("actionName", action.getName());
            String widgetVar = "custom_button" + i;
            String dimScreenIfNeeded = (screenAction.getScreenDimmingButton()) ? "ScreenBlocker.show();" : "";
            String unDimScreenIfNeeded = (screenAction.getScreenDimmingButton()) ? "ScreenBlocker.hide();" : "";
            button.setWidgetVar(widgetVar);
            button.setOnclick(widgetVar + ".disable();" + dimScreenIfNeeded);
            button.setOncomplete(widgetVar + ".enable();" + unDimScreenIfNeeded);
            screenAction.getButtonImage();
            byte[] image = null;
            try {
                Object fileFromEntity = getFileFromEntity(screenAction, "buttonImage");
                if (fileFromEntity != null) {
                    image = ((OImage) fileFromEntity).getImage();
                }
                image = (null != getFileFromEntity(screenAction, "buttonImage"))
                        ? (byte[]) ((OImage) getFileFromEntity(screenAction, "buttonImage")).getImage() : null;
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
            if (image != null) {
                String imageName = action.getName().replace(" ", "") + "_btn.png";
                // add the image in its path
                String imagePath = oimageServiceLocal.getFilePath(image, imageName, loggedUser);
                //set style of button as image background with this url & backgroung:cover
                button.setStyle("background-image:url(" + fabsPropertiesServiceLocal.getContextName() + imagePath + ")"
                        + "!important;height:30px;width:30px;background-size:cover");
                button.setValue("");
                //button.setStyle("background-image:url("+imagePath+")!important;");
            } else {
                logger.trace("image equals Null, I will put general_btn");
                button.setValue(action.getNameTranslated());
                button.setStyleClass("general_btn");
            }
            Class[] parms4 = new Class[]{ActionEvent.class};
            MethodExpression mb1 = getFacesContext().getApplication().getExpressionFactory().
                    createMethodExpression(elContext, "#{" + getBeanName()
                            + ".customScreenActionsAction}", null, parms4);
            button.addActionListener(new MethodExpressionActionListener(mb1));

            button.setUpdate(element2Update);

            HtmlInputHidden actionIDHidden = new HtmlInputHidden();
            actionIDHidden.setId(getFacesContext().getViewRoot().createUniqueId());
            actionIDHidden.setValue(String.valueOf(actionDBID));
            button.getChildren().add(actionIDHidden);
            logger.trace("Returning");
        }
    }

    private void sortValidScreenActions(List<ScreenAction> screenActions) {
        Comparator<ScreenAction> comparator = new Comparator<ScreenAction>() {
            public int compare(ScreenAction c1, ScreenAction c2) {
                return c1.getAction().getSortIndex() - c2.getAction().getSortIndex();
            }
        };

        Collections.sort(screenActions, comparator);
    }

    private List<ScreenAction> getValidScreenActions() {
        logger.trace("Entering");
        List<ScreenAction> screenActions = getCurrentScreen().getScreenActions();
        for (ScreenAction screenAction : screenActions) {
            if (screenAction.isInActive()) {
                continue;   // Ignore not active ones
            }
            if (!screenAction.isButton()) {
                continue;   // Interested only in buttons actions
            }
            OEntityAction action = screenAction.getAction();
            try {
                action = (OEntityAction) textTranslationServiceRemote.
                        loadEntityTranslation(action, loggedUser);
            } catch (TextTranslationException ex) {
                logger.error("Exception thrown", ex);
            }
            Long actionDBID = action.getDbid();
            if (actionDBID == null || actionDBID == 0) {
                // Invalid action DBID, error
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("Invalid Action DBID. screenAction: {}", screenAction);
                // </editor-fold>
                messagesPanel.addErrorMessage(
                        usrMsgService.getUserMessage("SystemInternalError", systemUser));
                continue;
            }
        }
        logger.trace("Returning");
        return screenActions;
    }

    protected void createMoreActionsMenu(HtmlPanelGrid buttonPanel, MenuItem... customItems) {
        logger.trace("Entering");
        Menu moreActionsMenu = new Menu();
        moreActionsMenu.setId(getFacesContext().getViewRoot().createUniqueId() + "_ActionsMenuBar");
        //moreActionsMenu.setOrientation("vertical");
        moreActionsMenu.setStyleClass("more_action_main_menu");
        Submenu mainMenuItem = new Submenu();
        mainMenuItem.setLabel(" ");
        mainMenuItem.setId(getFacesContext().getViewRoot().createUniqueId() + "_MenuActionMainItem");
        mainMenuItem.setStyleClass("more_action_main_menu_item");
        moreActionsMenu.getChildren().add(mainMenuItem);

        boolean containItems = false;

        for (int i = 0; i < customItems.length; i++) {
            if (customItems[i] != null) {
                customItems[i].setStyleClass("more_action_menu_item");
                containItems = true;
                mainMenuItem.getChildren().add(customItems[i]);
            }
        }
        List<ScreenAction> screenActions = getCurrentScreen().getScreenActions();
        for (int i = 0; i < screenActions.size(); i++) {
            if (screenActions.get(i).isInActive()) {
                continue;
            }

            if (screenActions.get(i).isButton()) {
                continue;
            }

            OEntityAction action = screenActions.get(i).getAction();
            MenuItem item = createMenuItem(action.getNameTranslated(), null,
                    "customScreenActionsAction", null, action.getDbid());
            if (item != null) {
                containItems = true;
                mainMenuItem.getChildren().add(item);
            }
        }

        if (containItems) {
            buttonPanel.getChildren().add(moreActionsMenu);
        }
    }

    public void customScreenActionsAction(ActionEvent event) {
        logger.debug("Entering");
        try {
            oem.getEM(loggedUser);
            messagesPanel.clearMessages();

            if (!(event.getComponent() instanceof MenuItem
                    || event.getComponent() instanceof CommandButton)) {
                logger.warn("Component Not Supported In Action");
                messagesPanel.addErrorMessage(
                        usrMsgService.getUserMessage("SystemInternalError", systemUser));
                logger.debug("Returning");
                return;
            }
            UIComponent comp = event.getComponent();
            if (comp.getChildren().isEmpty()) {
                logger.warn("BaseEntity DBID Not Found In Component");
                messagesPanel.addErrorMessage(
                        usrMsgService.getUserMessage("SystemInternalError", systemUser));
                logger.debug("Returning");
                return;
            }
            long actionDBID = Long.parseLong(((HtmlInputHidden) comp.getChildren().get(0)).getValue().toString());
            OEntityActionDTO action = entitySetupService.getActionDTO(actionDBID, loggedUser);

            List<String> conds = new ArrayList<String>();
            conds.add("action.dbid = " + actionDBID);
            ScreenAction sa = (ScreenAction) oem.loadEntity(ScreenAction.class.getSimpleName(), conds, null, loggedUser);

            int currentInstanceID = -1;
            String value = event.getComponent().getParent().getParent().getClientId();//get("instID");
            value = value.substring(0, value.lastIndexOf(":"));
            value = value.substring(value.lastIndexOf(":") + 1);
            currentInstanceID = (FramworkUtilities.isNumber(value)) ? Integer.parseInt(value) : currentInstanceID;
            if (currentInstanceID == -1) {
                OFunctionResult oFR = null;
                for (BaseEntity entity : getSelectedEntities()) {
                    oFR = runActionForEntity(action, entity);
                    messagesPanel.addMessages(oFR);
                    messagesPanel.clearAndDisplayMessages(oFR);
                    RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
                }
                if (oFR.getReturnValues() != null && !oFR.getReturnValues().isEmpty()) {
                    exportedFile = downloadFiles(oFR);
                    RequestContext.getCurrentInstance().execute("simulateDownloadBtnClick();");
                }
                if (getScreenHeight() == null || getScreenHeight().equals("")) {
                    RequestContext.getCurrentInstance().execute(
                            "top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');");
                    logger.trace("Calling JS updateIframeSize({}Frame)", getPortletInsId());
                } else {
                    RequestContext.getCurrentInstance().execute(
                            "top.FABS.Portlet.updateIframeSizeWithHeight({iframeName:'" + getPortletInsId() + "Frame'"
                            + ",screenHeight:'" + getScreenHeight() + "'});");
                    logger.trace("Calling JS updateIframeSizeWithHeight()");
                }

                if (getCurrentScreen() instanceof TabularScreen) {
                    RequestContext.getCurrentInstance().update("tf:generalPanel");
                }

                if (sa != null && sa.isExitAfterActionButton()) {
                    exit();
                }
                logger.debug("Returning");
                return;
            }

            BaseEntity selectedEntity = getSelectedEntities()[currentInstanceID];
            OFunctionResult oFR = runActionForEntity(action, selectedEntity);
            messagesPanel.addMessages(oFR);
            if (getScreenHeight() == null || getScreenHeight().equals("")) {
                RequestContext.getCurrentInstance().execute(
                        "top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');");
                logger.trace("Calling JS updateIframeSize");
            } else {
                RequestContext.getCurrentInstance().execute(
                        "top.FABS.Portlet.updateIframeSizeWithHeight({iframeName:'" + getPortletInsId() + "Frame'"
                        + ",screenHeight:'" + getScreenHeight() + "'});");
                logger.trace("Calling JS updateIframeSizeWithHeight");
            }
            messagesPanel.clearAndDisplayMessages(oFR);
            if (sa != null && sa.isExitAfterActionButton()) {
                exit();
            }
            RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(
                    usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);

        } finally {
            logger.debug("Returning");
            oem.closeEM(loggedUser);
        }

    }
    
    protected void exit() {
        if (openedAsPopup) {
            String param = currentScreen.getName();
            RequestContext.getCurrentInstance().execute("top.FABS.Portlet.closePopupNoID('" + param + "');");
            logger.trace("Calling JS closePopupNoID with PortletInsId: {}", getPortletInsId());
        } else {
            OPortalUtil.removePortlet(getPortletInsId(), screenInstId);
        }
    }

    // </editor-fold>
    /**
     * Creates the button, assign the name according to the loggedUser language,
     * bind to action, add to the panel, and return it
     *
     * @return The created button
     */
    protected CommandButton createButton(HtmlPanelGrid buttonPanel,
            String name, String actionName, String disabledFLDName,
            String beanName) {
        return createButton(buttonPanel, name, actionName, disabledFLDName, beanName, null);
    }

    protected CommandButton createButton(HtmlPanelGrid buttonPanel,
            String name, String actionName, String disabledFLDName,
            String beanName, Class[] params) {
        logger.trace("Entering");
        CommandButton button = new CommandButton();
        CommandLink label = new CommandLink();
        label.setTitle(name);
        label.setValue(name);
        label.setStyle("color:#0066CC; font-size:11px;text-decoration:none;font-family:Arial;");
        label.setDir(htmlDir);
        label.setAjax(true);
        //it works for form screens only-for ALRashed
        label.setUpdate("frmScrForm:middlePanel");
        button.setTitle(name);
        button.setValue(name);
        if (screenLanguage.equalsIgnoreCase("Arabic")) {
            button.setStyle("position:relative;text-align:right;");
        } else {
            button.setStyle("position:relative");
        }
        button.setDir(htmlDir);

        if (actionName != null) {
            if (null != params) {
                MethodExpression mb = exFactory.createMethodExpression(elContext,
                        "#{" + beanName + "." + actionName + "}", null, params);
                MethodExpressionActionListener al = new MethodExpressionActionListener(mb);
                button.addActionListener(al);
                label.addActionListener(al);

            } else {
                button.setActionExpression(
                        exFactory.createMethodExpression(
                                elContext,
                                "#{" + beanName + "." + actionName + "}",
                                String.class,
                                new Class[0]));
                label.setActionExpression(
                        exFactory.createMethodExpression(
                                elContext,
                                "#{" + beanName + "." + actionName + "}",
                                String.class,
                                new Class[0]));
            }
        }

        if (disabledFLDName != null && !disabledFLDName.equals("")) {
            button.setValueExpression("disabled",
                    exFactory.createValueExpression(
                            elContext,
                            "#{" + beanName + "." + disabledFLDName + "}",
                            boolean.class));
            label.setValueExpression("disabled",
                    exFactory.createValueExpression(
                            elContext,
                            "#{" + beanName + "." + disabledFLDName + "}",
                            boolean.class));
        }
        String buttonsId = getFacesContext().getViewRoot().createUniqueId();

        HtmlPanelGrid btnPanel = new HtmlPanelGrid();
        btnPanel.setStyle("position:relative;margin:0px;border:0px;padding:0px;");
        btnPanel.setId(buttonsId + "_butPanel");

        HtmlPanelGrid buttonTiltlePanel = new HtmlPanelGrid();
        buttonTiltlePanel.setId(buttonsId + "_butTitlePanel");
        buttonTiltlePanel.setStyleClass("buttonTitleLinkPanel");

        CommandLink buttonTitleLink = new CommandLink();//buttonTitleLink
        buttonTitleLink.setId(buttonsId + "_butTitle");
        buttonTitleLink.setStyleClass("buttonTitleLink");
        buttonTitleLink.setStyle("display:none");
        buttonTitleLink.setValue(name);
        buttonTitleLink.setOnclick("top.FABS.Portlet.buttonTitleAction(this.id,'" + getPortletInsId() + "');return false;");

        buttonTitleLink.getChildren().add(buttonTitleLink);
        buttonTitleLink.setStyleClass("buttonTitleLink");

        button.setId(buttonsId + "_button");
        buttonTiltlePanel.getChildren().add(buttonTitleLink);

        btnPanel.getChildren().add(button);
        btnPanel.getChildren().add(buttonTiltlePanel);

        HtmlPanelGrid grid = new HtmlPanelGrid();
        grid.setColumns(1);
        grid.setId(buttonsId + "_butPanelGrid");
        grid.getChildren().add(btnPanel);
        buttonPanel.getChildren().add(button);

        if (currentScreen.isShowButtonsTitle()) {
            buttonPanel.getChildren().add(label);
        }
        if (invalidInputMode) {
            button.setDisabled(true);
        }
        logger.trace("Returning");
        return button;
    }
    // </editor-fold>

    /**
     *
     * @param name
     * @param actionName
     * @param actionListenerName
     * @param disabledFLDName
     * @param actionDBID
     * @return
     */
    protected MenuItem createMenuItem(
            String name, String actionName, String actionListenerName,
            String disabledFLDName, Long actionDBID) {
        logger.trace("Entering");
        MenuItem item = new MenuItem();
        item.setValue(name);
        if (loggedUser.getFirstLanguage().getValue().equalsIgnoreCase("Arabic")) {
            item.setStyle("position:relative;text-align:right;");
        }
        if (actionName != null) {
            Class[] parms4 = new Class[]{ActionEvent.class};
            MethodExpression mb1 = getFacesContext().getApplication().getExpressionFactory().
                    createMethodExpression(elContext, "#{" + getBeanName()
                            + "." + actionName + "}", null, parms4);
            item.addActionListener(new MethodExpressionActionListener(mb1));
        }

        if (actionListenerName != null) {
            Class[] parms4 = new Class[]{ActionEvent.class};
            MethodExpression mb1 = getFacesContext().getApplication().getExpressionFactory().
                    createMethodExpression(elContext, "#{" + getBeanName()
                            + "." + actionListenerName + "}", null, parms4);
            item.addActionListener(new MethodExpressionActionListener(mb1));
        }

        item.setId(getFacesContext().getViewRoot().createUniqueId());

        if (disabledFLDName != null && !disabledFLDName.equals("")) {
            item.setValueExpression("disabled",
                    exFactory.createValueExpression(
                            elContext,
                            "#{" + getBeanName() + "." + disabledFLDName + "}",
                            boolean.class));
        }

        if (actionDBID != null && actionDBID != 0) {
            HtmlInputHidden actionIDHidden = new HtmlInputHidden();
            actionIDHidden.setId(getFacesContext().getViewRoot().createUniqueId());
            actionIDHidden.setValue(actionDBID);
            item.getChildren().add(actionIDHidden);
        }
        logger.trace("Returning");
        return item;
    }

    protected CommandButton createActionButton(
            String name, String actionName, String actionListenerName,
            String disabledFLDName, Long actionDBID) {
        logger.trace("Entering");
        CommandButton actionButton = new CommandButton();
        actionButton.setValue(name);
        if (loggedUser.getFirstLanguage().getValue().equalsIgnoreCase("Arabic")) {
            actionButton.setStyle("position:relative;text-align:right;");
        }
        if (actionName != null) {
            Class[] parms4 = new Class[]{ActionEvent.class};
            MethodExpression mb1 = getFacesContext().getApplication().getExpressionFactory().
                    createMethodExpression(elContext, "#{" + getBeanName()
                            + "." + actionName + "}", null, parms4);
            actionButton.addActionListener(new MethodExpressionActionListener(mb1));
        }

        if (actionListenerName != null) {
            Class[] parms4 = new Class[]{ActionEvent.class};
            MethodExpression mb1 = getFacesContext().getApplication().getExpressionFactory().
                    createMethodExpression(elContext, "#{" + getBeanName()
                            + "." + actionListenerName + "}", null, parms4);
            actionButton.addActionListener(new MethodExpressionActionListener(mb1));
        }

        actionButton.setId(getFacesContext().getViewRoot().createUniqueId());

        if (disabledFLDName != null && !disabledFLDName.equals("")) {
            actionButton.setValueExpression("disabled",
                    exFactory.createValueExpression(
                            elContext,
                            "#{" + getBeanName() + "." + disabledFLDName + "}",
                            boolean.class));
        }

        if (actionDBID != null && actionDBID != 0) {
            HtmlInputHidden actionIDHidden = new HtmlInputHidden();
            actionIDHidden.setId(getFacesContext().getViewRoot().createUniqueId());
            actionIDHidden.setValue(actionDBID);
            actionButton.getChildren().add(actionIDHidden);
        }
        logger.trace("Returning");
        return actionButton;
    }

    protected UIComponent createHeader(String id, ScreenField screenField,
            boolean isLookup, int maxCheckBoxSpc, int maxTextFldSpc) {
        logger.trace("Entering");
        try {
            TableColumnHeader header = new TableColumnHeader(exFactory, elContext,
                    screenField, getBeanName(), id, loggedUser, oem);
            return header.createHeader(this, isLookup, maxCheckBoxSpc, maxTextFldSpc);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(
                    usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
            logger.trace("Returning with Null");
            return null;
        } finally {
        }
    }

    void renderSession() {
    }
    // </editor-fold>

    public OEntityDTO getOactOnEntity() {
        return ((SingleEntityScreen) currentScreen != null)
                ? entitySetupService.getOScreenEntityDTO(currentScreen.getDbid(), loggedUser) : null;
    }
    // <editor-fold defaultstate="collapsed" desc="Controls Creation Common Functions">
    private HashMap selectItemsList = new HashMap();

    public HashMap getSelectItemsList() {
        return selectItemsList;
    }

    public void setSelectItemsList(HashMap selectItemsList) {
        this.selectItemsList = selectItemsList;
    }
    private HashMap lookupHashMap = new HashMap();

    public HashMap getLookupHashMap() {
        return lookupHashMap;
    }
    HashMap<String, TableColumnHeader> columnsHeaders = new HashMap<String, TableColumnHeader>();

    public HashMap<String, TableColumnHeader> getColumnsHeaders() {
        return columnsHeaders;
    }

    public void setColumnsHeaders(HashMap<String, TableColumnHeader> columnsHeaders) {
        this.columnsHeaders = columnsHeaders;
    }

    public String getLookupConditionValue(String conditionExpression) {
        return null;
    }

    protected HtmlPanelGrid buildMessagePanel() {
        HtmlPanelGrid msgGrid = new HtmlPanelGrid();
        return msgGrid;
    }

    public void setLookupHashMap(HashMap lookupHashMap) {
        this.lookupHashMap = lookupHashMap;
    }

    private HashMap autoCompleteList = new HashMap();

    public HashMap getAutoCompleteList() {
        return autoCompleteList;
    }

    public void setAutoCompleteList(HashMap autoCompleteList) {
        this.autoCompleteList = autoCompleteList;
    }

    public List<BaseEntity> completeList(String query) {
        String id = (String) UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()).getId();
        List<SelectItem> entities = (List<SelectItem>) autoCompleteList.get(id);//fieldLookup.getAutoComplete().getId()
        List<BaseEntity> filteredEntities = new ArrayList<BaseEntity>();
        FacesContext context = FacesContext.getCurrentInstance();
        for (int i = 0; i < entities.size(); i++) {
            SelectItem entity = entities.get(i);
            if (((String) entity.getLabel()).toLowerCase().startsWith(query.toLowerCase())) {
                filteredEntities.add((BaseEntity) entity.getValue());
            }
        }
        return filteredEntities;
    }

    public UIComponent createScreenControl(int controlMode, ScreenField screenField, String ownerScreenExpression,
            int screenFieldIndex, String valueChangeMethod) {
        logger.debug("Entering");
        String screenFieldExpression = screenField.getFieldExpression();
        int screenFieldsSize = getCurrentScreen().getScreenFields().size();
        boolean screenFieldIsMandatroy = screenField.isMandatory() && !(currentScreen instanceof M2MRelationScreen);
        HtmlPanelGrid grid = new HtmlPanelGrid();
        grid.setStyle("border:hidden;");
        grid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "grid"
                + (screenFieldExpression.contains(".") ? screenFieldExpression.substring(0, screenFieldExpression.indexOf(".") - 1)
                : screenFieldExpression));
        DD fieldDD = screenField.getDd();

        if (fieldDD == null) {
            // Happened after migration of Foundation, where some DDs are missing
            logger.warn("Screen Field Shouldn't Have Null DD");
            messagesPanel.addErrorMessage(
                    usrMsgService.getUserMessage("ScreenFieldDDNotFound",
                            Collections.singletonList("screenFieldExpression"),
                            systemUser));
            logger.debug("Returning with Null");
            return null;
        }
        Integer decimalDigits = checkDecimalDigitsNeededToMaskField(fieldDD);
        try {
            logger.trace("Handling Screen Field");
            fieldDD = (DD) textTranslationServiceRemote.loadEntityTranslation(fieldDD, loggedUser);
            List<ExpressionFieldInfo> screenFieldExpInfos
                    = screenField.getExpInfos(getOactOnEntity().getEntityClassPath());
            if (screenFieldExpInfos == null) {
                logger.warn("Screen Field Parsing Error");
                logger.trace("Returning with NUll");
                return null;
            }
            boolean expIsDirectEntityField = screenFieldExpInfos.size() == 1;
            ExpressionFieldInfo fieldInfo = screenFieldExpInfos.get(screenFieldExpInfos.size() - 1);
            boolean isUnique = BaseEntity.isFieldUnique(fieldInfo.field);
            if (fieldInfo.originalField != null) {
                isUnique = BaseEntity.isFieldUnique(fieldInfo.originalField);
            }
            boolean isNumeric = isFieldNumeric(fieldInfo.field.getType());

            // <editor-fold defaultstate="collapsed" desc="Get fieldDD, fieldBallonOEntity">
            if (fieldDD == null) {
                if (screenField != null && screenField.getDd() != null) {
                    logger.warn("Screen Field DD: {} Is Null", screenField.getDd().getName());
                }
                logger.trace("Returning with NUll");
                return null;  // Error
            }
            //</editor-fold>
            screenFieldIsUnique(isUnique, fieldInfo, screenField, fieldDD, screenFieldIndex);

            int controlType = (int) fieldDD.getControlType().getDbid();
            Class controlParameterType = controlType == DD.CT_CALENDAR ? Date.class : String.class;

            if (controlType == DD.CT_IMAGE) {
                controlParameterType = StreamedContent.class;
            }
            DDLookupType ddLookupType = fieldDD.getLookupType();
            checkLookupFieldIsNotOneToOne(ddLookupType, screenFieldExpInfos, screenField, screenFieldExpression);
            String expLeftParentName = null, expRightOfFirstDot = null;
            if (!expIsDirectEntityField) {
                // Expression has a "."
                // <editor-fold defaultstate="collapsed" desc="expLeftParentName, expRightOfFirstDot">
                // Get the First Part before "." in field Experssion, for binding
                // ex: If fieldExpression:language.value, then parentExpr=language
                // Expression like currentRow[currency]
                expLeftParentName = screenFieldExpInfos.get(0).fieldName;
                // The rest of expression like [currency].value.anythingelse
                expRightOfFirstDot = screenFieldExpression.substring(screenFieldExpression.indexOf("."));
                // </editor-fold>
            }
            boolean specialchar = fieldDD.isSpecialChar();
            if (ddLookupType.equals(DDLookupType.DDLT_NONE)) {
                createNonLookupControl(controlType, fieldDD, controlMode,
                        screenFieldIndex, screenField, screenFieldIsMandatroy,
                        isUnique, specialchar, isNumeric, decimalDigits,
                        expIsDirectEntityField, ownerScreenExpression,
                        expLeftParentName, expRightOfFirstDot,
                        controlParameterType, screenFieldExpression, grid);
            } else {
                createLookupControl(screenFieldIsMandatroy,
                        screenFieldExpression, controlMode, fieldDD,
                        ownerScreenExpression, screenFieldsSize,
                        valueChangeMethod, screenField, screenFieldIndex, grid);
            }
            //</editor-fold>
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }

        return grid;
    }

    private void createLookupControl(boolean screenFieldIsMandatroy, String screenFieldExpression, int controlMode, DD fieldDD, String ownerScreenExpression, int screenFieldsSize, String valueChangeMethod, ScreenField screenField, int screenFieldIndex, HtmlPanelGrid grid) {
        // Field is lookup
        if (screenFieldIsMandatroy) {
            lookupMandatoryFields.add(screenFieldExpression);
        }

        String lookupID = screenFieldExpression.replace(".", "_") + "_Lookup_";
        HtmlPanelGrid lookupPanelGrid = new HtmlPanelGrid();
        lookupPanelGrid.setStyle("border:1px!important;padding:0px!important;margin:0px!important");
        lookupPanelGrid.setId(FacesContext.getCurrentInstance().
                getViewRoot().createUniqueId());
        if (controlMode == CNTRL_OUTPUTMODE && fieldDD.getLookupType() != DDLookupType.DDLT_LTRLDRPDN) {
            createLookupInOutputMode(ownerScreenExpression,
                    screenFieldExpression, fieldDD, screenFieldsSize,
                    lookupPanelGrid);

        } else {
            createLookupInInputMode(ownerScreenExpression, fieldDD,
                    screenFieldExpression, valueChangeMethod, lookupID,
                    screenField, screenFieldIsMandatroy,
                    lookupPanelGrid, controlMode, screenFieldIndex);
        }

        grid.getChildren().add(lookupPanelGrid);
        lookupHashMap.put(lookupID, fieldLookup);
    }

    private void createLookupInInputMode(String ownerScreenExpression, DD fieldDD, String screenFieldExpression, String valueChangeMethod, String lookupID, ScreenField screenField, boolean screenFieldIsMandatroy, HtmlPanelGrid lookupPanelGrid, int controlMode, int screenFieldIndex) {
        fieldLookup = new Lookup(
                this,
                getBeanName(),
                ownerScreenExpression,
                fieldDD,
                screenFieldExpression,
                valueChangeMethod,
                lookupID,
                screenField,
                screenFieldIsMandatroy, oem,
                filterService, exFactory,
                getFacesContext(),
                loggedUser);
        if ((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("")
                && fieldDD.getLookupType() == DDLookupType.DDLT_DRPDWN)
                || (null != fieldDD.getLookupFilter() && !"".equals(fieldDD.getLookupFilter()))
                && fieldDD.getLookupType() == DDLookupType.DDLT_LTRLDRPDN) {
            createDropDown(screenField, lookupPanelGrid,
                    controlMode, screenFieldExpression,
                    ownerScreenExpression, fieldDD, screenFieldIndex);
        } else if (fieldDD.getLookupType() == DDLookupType.DDLT_AUTOCOMPLETE) {
            createAutoComplete(screenField, lookupPanelGrid,
                    controlMode, screenFieldExpression,
                    ownerScreenExpression, fieldDD, screenFieldIndex);
        } else {
            createLookupScreenControl(screenFieldExpression, lookupPanelGrid);
        }
    }

    private void createLookupScreenControl(String screenFieldExpression, HtmlPanelGrid lookupPanelGrid) {
        loadAttribute(screenFieldExpression);
        lookupPanelGrid.setStyleClass("lookup-panel");
        lookupPanelGrid.getChildren().add(fieldLookup.createNonDropDown());
        FramworkUtilities.setControlFldExp(fieldLookup.getInputText(), screenFieldExpression);
        fieldIDs.add(fieldLookup.getInputText().getId());
    }

    private void createDropDown(ScreenField screenField, HtmlPanelGrid lookupPanelGrid, int controlMode, String screenFieldExpression, String ownerScreenExpression, DD fieldDD, int screenFieldIndex) {
        if (screenField.isEditable()) {
            getCurrentScreen().getScreenFields().size();
            lookupPanelGrid.getChildren().add(fieldLookup.createDropDown(controlMode == CNTRL_OUTPUTMODE));
            fieldIDs.add(fieldLookup.getDropDownMenu().getId());
//                            fieldsControlIdsMap.put(String.valueOf(screenField.getDbid()), fieldLookup.getDropDownMenu().getId());
            FramworkUtilities.setControlFldExp(fieldLookup.getDropDownMenu(), screenFieldExpression);
            selectItemsList.put(fieldLookup.getDropDownMenu().getId(),
                    fieldLookup.getSelectItems());
            fieldLookup.getDropDownMenu().setStyleClass("dropdown-list-LTR");
            if (loggedUser.isRtl()) {
                fieldLookup.getDropDownMenu().setStyleClass("dropdown-list-RTL");
            }
        } else {
            String fieldExpression = "#{" + ownerScreenExpression + "." + screenFieldExpression + "}";
            if ("Literal Drop-down".equals(fieldDD.getControlType().getValueTranslated())) {
                fieldExpression = UIFieldUtility.buildFieldExpForDropDown(fieldExpression, fieldDD);
            }
            lookupPanelGrid.getChildren().add(
                    createLabelControl(fieldExpression, fieldDD, screenFieldIndex));
        }
    }

    private void createAutoComplete(ScreenField screenField, HtmlPanelGrid lookupPanelGrid, int controlMode, String screenFieldExpression, String ownerScreenExpression, DD fieldDD, int screenFieldIndex) {
        if (screenField.isEditable()) {
            getCurrentScreen().getScreenFields().size();
            lookupPanelGrid.getChildren().add(fieldLookup.createAutoComplete(controlMode == CNTRL_OUTPUTMODE));
            fieldIDs.add(fieldLookup.getAutoComplete().getId());
            FramworkUtilities.setControlFldExp(fieldLookup.getAutoComplete(), screenFieldExpression);
            autoCompleteList.put(fieldLookup.getAutoComplete().getId(),
                    fieldLookup.getSelectItems());
        } else {
            lookupPanelGrid.getChildren().add(
                    createLabelControl("#{" + ownerScreenExpression + "." + screenFieldExpression + "}", fieldDD, screenFieldIndex));
        }
    }

    private void createLookupInOutputMode(String ownerScreenExpression, String screenFieldExpression, DD fieldDD, int screenFieldsSize, HtmlPanelGrid lookupPanelGrid) {
        HtmlOutputLabel outputText = new HtmlOutputLabel();
        outputText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_LBLTXT");
        String fldExp = UIFieldUtility.buildScreenFieldExp("#{" + ownerScreenExpression + "." + screenFieldExpression + "}", false);
        outputText.setValueExpression("value",
                exFactory.createValueExpression(elContext,
                        fldExp, String.class));
        outputText.setStyle("display:-moz-inline-box;"
                + "width:" + fieldDD.getControlSize());
        outputText.setOnclick("selectCurrentRow(this, " + screenFieldsSize + ");");
        lookupPanelGrid.getChildren().add(outputText);
    }

    private void createNonLookupControl(int controlType, DD fieldDD, int controlMode, int screenFieldIndex, ScreenField screenField, boolean screenFieldIsMandatroy, boolean isUnique, boolean isSpecialChar, boolean isNumeric, Integer decimalDigits, boolean expIsDirectEntityField, String ownerScreenExpression, String expLeftParentName, String expRightOfFirstDot, Class controlParameterType, String screenFieldExpression, HtmlPanelGrid grid) {
        // <editor-fold defaultstate="collapsed" desc="Create currentControl, currentControlWithoutMandatoryPanel, set control ID">
        UIComponent currentControl;
        controlType = setControlTypeToCommandLink(fieldDD, controlMode, controlType);
        currentControl = createControl(controlMode, controlType,
                fieldDD, screenFieldIndex, screenField,
                screenFieldIsMandatroy, isUnique, isSpecialChar, isNumeric);
        setDecimalConverterToControl(decimalDigits, controlType, currentControl);
        HtmlOutputText linkLabel = createOutputText(controlType, fieldDD);

        createInputControl(controlMode, controlType, currentControl,
                screenFieldIsMandatroy, expIsDirectEntityField,
                ownerScreenExpression, expLeftParentName,
                expRightOfFirstDot, controlParameterType,
                screenFieldExpression, linkLabel);

        if (currentControl != null && currentControl instanceof HtmlPanelGrid) {
            HtmlPanelGrid ctrlGrid = (HtmlPanelGrid) currentControl;
            ctrlGrid.setStyle("border:hidden;");
            ctrlGrid.setStyleClass("ctrlTable");
        }
        if (controlType == DD.CT_URLLINK) {
            currentControl.getChildren().add(linkLabel);
        }
        grid.getChildren().add(currentControl);
//                }
    }

    private void createInputControl(int controlMode, int controlType, UIComponent currentControl, boolean screenFieldIsMandatroy, boolean expIsDirectEntityField, String ownerScreenExpression, String expLeftParentName, String expRightOfFirstDot, Class controlParameterType, String screenFieldExpression, HtmlOutputText linkLabel) {
        // </editor-fold>
        if (controlMode == CNTRL_INPUTMODE) {
            UIComponent currentControlWithoutMandatoryPanel;
            if (controlType == DD.CT_TEXTFIELD || controlType == DD.CT_TEXTAREA) {
                currentControlWithoutMandatoryPanel = currentControl.getChildren().get(0);
            } else if (controlType == DD.CT_URLLINK || controlType == DD.CT_BARCODEIMAGE) {
                currentControlWithoutMandatoryPanel = currentControl;
            } else {
                currentControlWithoutMandatoryPanel
                        = (screenFieldIsMandatroy || isComplexNonLookUpControl(controlType)) ? currentControl.getChildren().get(0) : currentControl;
            }
            if (!expIsDirectEntityField) {
                // Expression has a "."
                String fldExp = UIFieldUtility.buildScreenFieldExp("#{" + ownerScreenExpression + "." + expLeftParentName
                        + expRightOfFirstDot + "}", true);
                currentControlWithoutMandatoryPanel.setValueExpression(
                        "value",
                        exFactory.createValueExpression(
                                elContext, fldExp,
                                controlParameterType));
            } else {
                // Expression is an Entity Direct Field
                if (!(controlType == DD.CT_URLLINK)) {
                    currentControlWithoutMandatoryPanel.setValueExpression(
                            "value",
                            exFactory.createValueExpression(
                                    elContext,
                                    "#{" + ownerScreenExpression + "." + screenFieldExpression + "}",
                                    controlParameterType));
                }
                if (controlType == DD.CT_LINK || controlType == DD.CT_URLLINK) {

                    currentControlWithoutMandatoryPanel.getChildren().add(linkLabel);
                }
            }
        }
        //</editor-fold>
    }

    private HtmlOutputText createOutputText(int controlType, DD fieldDD) {
        HtmlOutputText linkLabel = new HtmlOutputText();
        if (controlType == DD.CT_URLLINK) {
            linkLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
            linkLabel.setValue(fieldDD.getDefaultValue());
        }
        return linkLabel;
    }

    private void setDecimalConverterToControl(Integer decimalDigits, int controlType, UIComponent currentControl) {
        // <editor-fold defaultstate="collapsed" desc="FABSDecimalConverter">
        if (decimalDigits != null
                && controlType == DD.CT_TEXTFIELD) {
            FABSDecimalConverter decimalConverter = new FABSDecimalConverter(decimalDigits);
            if (currentControl instanceof InputText) {
                ((InputText) currentControl).setConverter(decimalConverter);
            } else if (currentControl instanceof HtmlPanelGrid) {
                ((InputText) (currentControl).getChildren().get(0)).setConverter(decimalConverter);
            }
        }
    }

    private int setControlTypeToCommandLink(DD fieldDD, int controlMode, int controlType) {
        //TODO : Linkable Field
        if (fieldDD.isBalloon() && getCurrentScreen() instanceof TabularScreen
                && controlMode == CNTRL_OUTPUTMODE) { // to open field as link
            controlType = DD.CT_COMMANDLINK;
        }
        return controlType;
    }

    private void checkLookupFieldIsNotOneToOne(DDLookupType ddLookupType, List<ExpressionFieldInfo> screenFieldExpInfos, ScreenField screenField, String screenFieldExpression) {
        if (ddLookupType.equals(DDLookupType.DDLT_NONE)
                // Field is not lookup
                && screenFieldExpInfos.size() > 1
                // Field expression has "."
                && screenField.isEditable()
                // Field is editable
                && screenFieldExpInfos.get(0).field.getAnnotation(OneToOne.class) == null) // Field is not OneToOne (OneToOne is mostly composition doesn't need lookup)
        {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Inconsistency, Editable Field Is Not Lookup and Has '.'. Field Expression: {}, Screen: {}", screenFieldExpression, getCurrentScreen());
            // </editor-fold>
        }
    }

    private void screenFieldIsUnique(boolean isUnique, ExpressionFieldInfo fieldInfo, ScreenField screenField, DD fieldDD, int screenFieldIndex) {
        if (isUnique) {
            String fieldName = fieldInfo.fieldName;
            if (fieldInfo.originalField != null) {
                fieldName = fieldInfo.originalField.getName();
            }
            uniqueFieldsQueries.put(FramworkUtilities.getID(screenField, fieldDD, screenFieldIndex),
                    "SELECT o FROM " + fieldInfo.fieldClass.getSimpleName() + " o "
                    + "WHERE o." + fieldName
                    + " = ");
            uniqueFields.put(FramworkUtilities.getID(screenField, fieldDD, screenFieldIndex), fieldInfo);
        }
    }

    private Integer checkDecimalDigitsNeededToMaskField(DD fieldDD) {
        Integer decimalDigits = null;
        if (fieldDD.getDecimalMask() != null) {
            decimalDigits = fieldDD.getDecimalMask();
        }
        return decimalDigits;
    }

    /**
     * Create the PanelGrid of the control (= "only" Control + Error Message )
     *
     * @param controlType
     * @param screenFieldDD
     * @param screenFieldIndex
     * @param screenField
     * @param isMandatory
     * @param mode
     * @return <br> null: Error <br> Otherwise, created PanelGrid
     */
    protected UIComponent createControl(int controlMode, int controlType, DD screenFieldDD,
            int screenFieldIndex, ScreenField screenField,
            boolean isMandatory, boolean isUnique, boolean isNumeric, boolean isSpecialChar) {
        logger.trace("Entering");
        int dimension = CHARACTER_LENGTH_IN_PIXEL * screenFieldDD.getControlSize();
        int screenFieldsSize = getCurrentScreen().getScreenFields().size();
        if (controlMode == CNTRL_OUTPUTMODE) {
            if (controlType == DD.CT_CALENDAR) {
                UIComponent outputText = new org.primefaces.component.calendar.Calendar();
                if (null != screenFieldDD.getCalendarPattern()) {
                    ((org.primefaces.component.calendar.Calendar) outputText).
                            setPattern(screenFieldDD.getCalendarPattern().getValue());
                } else {
                    ((org.primefaces.component.calendar.Calendar) outputText).setPattern("MMM dd, yyyy");
                }

                ((org.primefaces.component.calendar.Calendar) outputText).setDisabled(true);
                ((org.primefaces.component.calendar.Calendar) outputText).setOnclick("selectCurrentRow(this, " + screenFieldsSize + ");");
                outputText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_ColTXT");
                outputText.setValueExpression("value",
                        exFactory.createValueExpression(elContext,
                                "#{currentRow." + screenField.getFieldExpression() + "}", Date.class));
                logger.trace("Returning");
                return outputText;

            } else if (controlType == DD.CT_CHECKBOX) {
                UIComponent outputText = new SelectBooleanCheckbox();
                ((SelectBooleanCheckbox) outputText).setDisabled(true);
                outputText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_ColTXT");
                outputText.setValueExpression("value",
                        exFactory.createValueExpression(elContext,
                                "#{currentRow." + screenField.getFieldExpression() + "}", Boolean.class));
                logger.trace("Returning");
                return outputText;
            } else if (controlType == DD.CT_URLLINK) {
                UIComponent outputText = new HtmlOutputLink();
                outputText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_ColLINK");
                ((HtmlOutputLink) outputText).setValue(screenFieldDD.getDefaultValue());
                ((HtmlOutputLink) outputText).setTarget("WWW");
                ((HtmlOutputLink) outputText).setStyle("width:50px; height:15px;");
                logger.trace("Returning");
                return outputText;
            } else if (controlType == DD.CT_TEXTAREA) {
                UIComponent outputText = new InputTextarea();
                outputText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_ColLINK");
                outputText.setValueExpression("value",
                        exFactory.createValueExpression(elContext,
                                "#{currentRow." + screenField.getFieldExpression() + "}", String.class));
                ((InputTextarea) outputText).setDisabled(true);
                ((InputTextarea) outputText).setStyle("width:" + dimension + "px;height:100px; max-width: " + dimension + "px; margin-right: 30px!important;");
                ((InputTextarea) outputText).setOnclick("selectCurrentRow(this, " + screenFieldsSize + ");");
                logger.trace("Returning");
                return outputText;
            }// TODO : Linkable Field
            else if (controlType == DD.CT_COMMANDLINK) {
                UIComponent commandLink = new CommandLink();
                commandLink.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_ColCommLINK");
                ((CommandLink) commandLink).setTabindex(Integer.toString(screenFieldIndex));
                if (loggedUser.getFirstLanguage().getDbid() == OUser.ARABIC) {
                    ((CommandLink) commandLink).setStyle("width:auto!important;height:auto!important; margin-left: 30px!important;");
                } else {
                    ((CommandLink) commandLink).setStyle("width:auto!important;height:auto!important; margin-right: 30px!important;");
                }
                commandLink.setValueExpression("value",
                        exFactory.createValueExpression(elContext,
                                "#{currentRow." + screenField.getFieldExpression() + "}", String.class));
                ((CommandLink) commandLink).setRendered(true);
                ((CommandLink) commandLink).setImmediate(true);
                ((CommandLink) commandLink).setProcess("@this");
                ((CommandLink) commandLink).setPartialSubmit(true);
                Class[] params = {ActionEvent.class};
                MethodExpression me = FacesContext.getCurrentInstance().getApplication().
                        getExpressionFactory().createMethodExpression(
                                elContext, "#{" + getBeanName() + ".fieldNameAction}",
                                null, params);
                ((CommandLink) commandLink).addActionListener(new MethodExpressionActionListener(me));
                return commandLink;
            } else if (controlType == DD.CT_PASSWORD) {
                Password password = new Password();
                password.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_ColPass");
                password.setValueExpression("value",
                        exFactory.createValueExpression(elContext,
                                UIFieldUtility.buildScreenFieldExp("#{currentRow." + screenField.getFieldExpression() + "}", true), String.class));
                password.setRedisplay(true);
                password.setDisabled(true);
                logger.trace("Returning");
                return password;
            } else {
                HtmlOutputLabel outputText = new HtmlOutputLabel();
                outputText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_ColmnOutTXT");
                ((HtmlOutputLabel) outputText).setStyle("width:" + screenFieldDD.getControlSize() + " px;");
                outputText.setOnclick("selectCurrentRow(this, " + screenFieldsSize + ");");
                if (null != screenFieldDD.getDecimalMask()) {
                    ((HtmlOutputLabel) outputText).setConverter(new FABSDecimalConverter(screenFieldDD.getDecimalMask()));
                }

                outputText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_outLBL");

                String fldExp = UIFieldUtility.buildScreenFieldExp("#{currentRow." + screenField.getFieldExpression() + "}", false);
                outputText.setValueExpression("value",
                        exFactory.createValueExpression(elContext,
                                fldExp, String.class));

                Integer decimalOutDigits = checkDecimalDigitsNeededToMaskField(screenFieldDD);
                if (decimalOutDigits != null) {
                    FABSDecimalConverter decimalConverter = new FABSDecimalConverter(decimalOutDigits);
                    outputText.setConverter(decimalConverter);
                } else {
                    outputText.setConverter(new URLConverter());
                    outputText.setEscape(false);
                }
                return outputText;
            }
        }
        String id = FramworkUtilities.getID(screenField, screenFieldDD, screenFieldIndex);
        fieldIDs.add(id);
        switch (controlType) {
            case DD.CT_TEXTFIELD:
                return createTextFieldControl(screenField, screenFieldDD,
                        screenFieldIndex, isMandatory, isUnique, isSpecialChar, isNumeric);
            case DD.CT_CALENDAR:
                return createCalendarControl(screenField, screenFieldDD,
                        screenFieldIndex, isMandatory);
            case DD.CT_CHECKBOX:
                return createCheckBoxControl(screenField, screenFieldDD,
                        screenFieldIndex, isMandatory);
            case DD.CT_LABEL:
                return createLabelControl(screenField, screenFieldDD);
            case DD.CT_IMAGE:
                return createImageControl(screenField, screenFieldDD);
            case DD.CT_BARCODEIMAGE:
                return createBarcodeControl(screenField, screenFieldDD);
            case DD.CT_TEXTAREA:
                return createTextAreaControl(screenField, screenFieldDD,
                        screenFieldIndex, isMandatory);
            case DD.CT_RICHTEXT:
                return createRichTextControl(screenField, screenFieldDD,
                        screenFieldIndex, isMandatory);
            case DD.CT_FILEUPLOAD:
            case DD.CT_FILEDOWNLOAD:
            case DD.CT_FILE:
            case DD.CT_LINK:
                return createOutputLinkControl(FacesContext.getCurrentInstance().
                        getViewRoot().createUniqueId(),
                        screenFieldDD, screenFieldIndex);
            case DD.CT_URLLINK:
                return createURLLinkControl(screenField, screenFieldDD, screenFieldIndex);
            case DD.CT_PASSWORD:
                logger.trace("Returning");
                return createPasswordTextFieldControl(screenField, screenFieldDD,
                        screenFieldIndex, isMandatory);
            default:
                logger.trace("Returning with Null");
                return null;
        }
    }
    private byte[] image;
    private GraphicImage graphicImage;

    public UIComponent createImageControl(ScreenField screenField,
            DD screenFieldDD) {
        try {

            setGraphicImage(new GraphicImage());
            getGraphicImage().setId(FramworkUtilities.getID(screenField, screenFieldDD, 0));
            getGraphicImage().setStyleClass("img-cntrl");
            setImage((byte[]) getFileFromEntity(getLoadedEntity(), screenField.getFieldExpression()));

            if (getImage() != null) {
                String imgNameExp = screenField.getFieldExpression().substring(0, screenField.getFieldExpression().lastIndexOf(".")) + "." + "name";
                String imageName = (String) getFileFromEntity(getLoadedEntity(), imgNameExp);
                String imagePath = oimageServiceLocal.getFilePath(getImage(), imageName, loggedUser);
                getGraphicImage().setValue(getImage());
                getGraphicImage().setUrl(imagePath);
            } else {
                getGraphicImage().setUrl("/resources/default.png");
            }
            getGraphicImage().setValueExpression("binding",
                    exFactory.createValueExpression(
                            elContext, "#{" + getBeanName() + ".graphicImage}", GraphicImage.class));
            return getGraphicImage();
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Returning with Null");
            return null;
        }
    }

    public UIComponent createBarcodeControl(ScreenField screenField,
            DD screenFieldDD) {
        logger.debug("Entering");
        try {

            setGraphicImage(new GraphicImage());
            String id = FramworkUtilities.getID(screenField, screenFieldDD, 0);
            getGraphicImage().setId(id);
            getGraphicImage().setStyleClass("img-cntrl");
            String toencode = (String) FramworkUtilities.
                    getValueFromEntity(getLoadedEntity().get(0), screenField.getFieldExpression());
            BitMatrix bitMatrix = new Code128Writer().encode(
                    toencode, BarcodeFormat.CODE_128, 300, 400);
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            String realPath = externalContext.getRealPath("/resources/cached/" + id + ".png");

            File file = new File(realPath);
            MatrixToImageWriter.writeToStream(bitMatrix,
                    "png", new FileOutputStream(file));
            String s = realPath.substring(realPath.indexOf("/resources/cached/"));
            getGraphicImage().setUrl(s);
            getGraphicImage().setValueExpression("binding",
                    exFactory.createValueExpression(
                            elContext, "#{" + getBeanName() + ".graphicImage}", GraphicImage.class));
            return getGraphicImage();
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    public static Object getFileFromEntity(Object entity, String expression)
            throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        logger.debug("Entering");
        if (entity == null) {
            logger.debug("Returning with Null");
            return entity;
        }
        String currentExpression = null;
        boolean leaf;
        if (expression.contains(".")) {
            currentExpression = expression.substring(0, expression.indexOf("."));
            expression = expression.substring(expression.indexOf(".") + 1);
            leaf = false;
        } else {
            currentExpression = expression;
            leaf = true;
        }
        Object currentEntity = null;
        String methodName;
        Collection<?> col = null;
        if (entity instanceof Collection<?>) {
            col = (Collection<?>) entity;
            currentEntity = col.toArray()[0];
        } else {
            currentEntity = entity;
        }

        Field currentField = BaseEntity.getClassField(currentEntity.getClass(), currentExpression);//entity.getClass().getDeclaredField(currentExpression);
        methodName = "get" + Character.toString(currentExpression.charAt(0)).toUpperCase() + currentExpression.substring(1);
        if (currentField.getType() == List.class) {
            List objects = (List) currentEntity.getClass().getMethod(methodName).invoke(entity);
            if (leaf) {
                return objects;
            } else {
                List resultObjects = new ArrayList();
                for (Object object : objects) {
                    Object o = getFileFromEntity(object, expression);
                    if (o != null) {
                        resultObjects.add(o);
                    }
                }
                logger.debug("Returning");
                return resultObjects;
            }
        } else {
            Object obj = currentEntity.getClass().getMethod(methodName).invoke(currentEntity);
            if (leaf) {
                logger.debug("Returning");
                return obj;
            } else {
                BaseEntity checkObjDBID = (BaseEntity) obj;
                if (null == checkObjDBID || checkObjDBID.getDbid() == 0) {
                    logger.debug("Returning with Null");
                    return null;
                }
                logger.debug("Returning");
                return getFileFromEntity(obj, expression);
            }
        }
    }

    public static Object getFileNameFromEntity(Object entity, String expression)
            throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        logger.debug("Entering");
        if (entity == null) {
            logger.debug("Returning ");
            return entity;
        }
        String currentExpression = null;
        boolean leaf;
        if (expression.contains(".")) {
            currentExpression = expression.substring(0, expression.indexOf("."));
            expression = expression.substring(expression.indexOf(".") + 1);
            leaf = false;
        } else {
            currentExpression = expression;
            leaf = true;
        }
        Object currentEntity = null;
        String methodName;
        Collection<?> col = null;
        if (entity instanceof Collection<?>) {
            col = (Collection<?>) entity;
            currentEntity = col.toArray()[0];
        } else {
            currentEntity = entity;
        }

        Field currentField = BaseEntity.getClassField(currentEntity.getClass(), currentExpression);//entity.getClass().getDeclaredField(currentExpression);
        methodName = "get" + Character.toString(currentExpression.charAt(0)).toUpperCase() + currentExpression.substring(1);
        if (currentField.getType() == List.class) {
            List objects = (List) currentEntity.getClass().getMethod(methodName).invoke(entity);
            if (leaf) {
                return objects;
            } else {
                List resultObjects = new ArrayList();
                for (Object object : objects) {
                    Object o = getFileFromEntity(object, expression);
                    if (o != null) {
                        resultObjects.add(o);
                    }
                }
                logger.debug("Returning");
                return resultObjects;
            }
        } else {
            Object obj = currentEntity.getClass().getMethod(methodName).invoke(currentEntity);
            if (leaf) {
                logger.debug("Returning");
                return obj;
            } else {
                BaseEntity checkObjDBID = (BaseEntity) obj;
                if (checkObjDBID.getDbid() == 0) {
                    logger.debug("Returning with Null");
                    return null;
                }
                logger.debug("Returning");
                return getFileFromEntity(obj, expression);
            }
        }
    }

    protected UIComponent createOutputLinkControl(String id,
            DD screenFieldDD, int screenFieldIndex) {
        HtmlOutputLink outputLink = new HtmlOutputLink();
        outputLink.setTarget("WWW");
        buildScreenExpression(outputLink);
        outputLink.setId(id);
        outputLink.setTabindex(Integer.toString(screenFieldIndex));
        return outputLink;
    }

    protected UIComponent createSelectManyListBoxControl(ScreenField screenField,
            DD screenFieldDD, int screenFieldIndex, boolean isMandatory,
            boolean isUnique, boolean isNumeric, BaseEntity entity, String fieldExp) {
        logger.trace("Entering");
        String id = FramworkUtilities.getID(screenField, screenFieldDD, screenFieldIndex);

        SelectOneListbox selectManyListbox = new SelectOneListbox();

        selectManyListbox.setId(id);
        FramworkUtilities.setControlFldExp(selectManyListbox, screenField.getFieldExpression());
        buildScreenExpression(selectManyListbox);
        selectManyListbox.setTabindex(Integer.toString(screenFieldIndex));
        selectManyListbox.setValueExpression(
                "value",
                exFactory.createValueExpression(
                        elContext,
                        screenField.getFieldExpression(),
                        Boolean.class));
        String getFirstGroupListQuery = "SELECT distinct entity."
                + fieldExp
                + " FROM " + entity.getClassName() + " entity   "
                + " where entity.inActive = false    ";
        logger.trace("Query: {}", getFirstGroupListQuery);
        EntityManager em = null;
        List<SelectItem> controlValues = null;
        List<String> fieldValues = null;
        try {
            em = oem.getEM(loggedUser);
            fieldValues = oem.executeEntityListQuery(getFirstGroupListQuery, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            oem.closeEM(loggedUser);
        }

        for (String fieldValue : fieldValues) {
            controlValues.add(new SelectItem(fieldValue, fieldValue));
        }
        UISelectItems uiSelectItems = new UISelectItems();
        uiSelectItems.setId(getFacesContext().getViewRoot().createUniqueId() + "_uiItems");

        uiSelectItems.setValue(controlValues);
        selectManyListbox.getChildren().add(uiSelectItems);
        if (isMandatory) {
            selectManyListbox.setRequired(isMandatory);

            HtmlPanelGrid panelGrid = new HtmlPanelGrid();
            panelGrid.setStyle("border:hidden;");
            panelGrid.setColumns(1);
            panelGrid.getChildren().add(selectManyListbox);
            UserMessage userMessage = usrMsgService.getUserMessage("MandatoryField", loggedUser);
            Message message = new Message();
            selectManyListbox.setRequiredMessage(userMessage.getMessageTextTranslated());
            message.setFor(screenField.getFieldExpression() + "_multiSelectMenu"
                    + screenFieldIndex);
            panelGrid.getChildren().add(message);
            logger.trace("Returning");
            return panelGrid;
        }
        logger.trace("Returning");
        return selectManyListbox;
    }

    private UIComponent createPasswordTextFieldControl(ScreenField screenField,
            DD screenFieldDD, int screenFieldIndex, boolean isMandatory) {
        String id = FramworkUtilities.getID(screenField, screenFieldDD, screenFieldIndex);
        Password password = new Password();
        int controlSize = screenField.getControlSize();
        if (controlSize == 0) {
            controlSize = screenField.getDd().getControlSize();
        }
        password.setSize(controlSize);
        FramworkUtilities.setControlFldExp(password, screenField.getFieldExpression());

        password.setId(id);
        /*Flag indicating that any existing value in this field should be rendered
         when the form is created. Because this is a potential security risk,
         password values are not displayed by default. */
        password.setRedisplay(true);
        buildScreenExpression(password);
        password.setDir(htmlDir);
        if (htmlDir.equals("rtl")) {
            password.setStyle("text-align:right");
        }
        //The tab index is not working when it is set in tabular screen!!
        //By Commenting it it is working on both types of screens !!!
        if (mode == OScreen.SM_ADD) {
            if (screenFieldDD.getDefaultValue() != null && !screenFieldDD.getDefaultValue().equals("")) {
                password.setValue(screenFieldDD.getDefaultValue());
            }
        }
        password.setDisabled(!screenField.isEditable());
        FABSAjaxBehavior behavior = new FABSAjaxBehavior();
        password.addClientBehavior("blur", behavior);

        if (isMandatory) {
            behavior.setUpdate(id + "MandMsg");
            password.addClientBehavior("blur", behavior);

            password.setRequired(isMandatory);

            HtmlPanelGrid panelGrid = new HtmlPanelGrid();
            panelGrid.setId(id + "MGrid");
            panelGrid.setColumns(1);
            panelGrid.getChildren().add(password);
            panelGrid.setStyleClass("ctrlTable");
            panelGrid.setStyle("border:hidden;");
            // FIXME: Nothing is done with this message (not displayed in screen)
            // , use message parameters instead
            UserMessage userMessage = usrMsgService.getUserMessage("MandatoryField", loggedUser);
            Message message = new Message();
            message.setId(id + "MandMsg");
            password.setRequiredMessage(screenFieldDD.getLabelTranslated() + " "
                    + userMessage.getMessageTextTranslated());
            message.setFor(id);
            panelGrid.getChildren().add(message);
            logger.trace("Returning");
            return panelGrid;
        }
        logger.trace("Returning");
        return password;
    }

    private UIComponent createTextFieldControl(ScreenField screenField,
            DD screenFieldDD, int screenFieldIndex, boolean isMandatory, boolean isUnique, boolean specialchar, boolean isNumeric) {
        String id = FramworkUtilities.getID(screenField, screenFieldDD, screenFieldIndex);

        Message message = new Message();
        message.setId(id + "MandMsg");
        InputText inputText = new InputText();
        int controlSize = screenField.getControlSize();
        if (controlSize == 0) {
            controlSize = screenField.getDd().getControlSize();
        }
        inputText.setSize(controlSize);
        FramworkUtilities.setControlFldExp(inputText, screenField.getFieldExpression());
        //That id is used in the manadatory message
        inputText.setId(id);
        FABSAjaxBehavior behavior = new FABSAjaxBehavior();
        inputText.addClientBehavior("blur", behavior);

        boolean htmlAllowed = screenFieldDD.getHtmlAllowed() != null
                ? screenFieldDD.getHtmlAllowed()
                : false;
        if (!htmlAllowed) {

            Class[] parms = new Class[]{FacesContext.class, UIComponent.class, Object.class};
            MethodExpression mExp = getFacesContext().getApplication().getExpressionFactory().
                    createMethodExpression(elContext, "#{" + getBeanName()
                            + ".validateHTML}", null, parms);
            inputText.addValidator(new MethodExpressionValidator(mExp));
            inputText.setValidatorMessage(usrMsgService.getUserMessage("HtmlNotAllowed", loggedUser).getMessageTextTranslated());

        }

        Class[] parms = new Class[]{ValueChangeEvent.class};
        MethodBinding methodBinding = getFacesContext().getApplication().
                createMethodBinding("#{" + getBeanName() + ".updateRow}", parms);
        inputText.setValueChangeListener(methodBinding);
        inputText.setDir(htmlDir);
        if (htmlDir.equals("rtl")) {
            inputText.setStyle("text-align:right");
        }

        if (mode == OScreen.SM_ADD) {
            if (screenFieldDD.getDefaultValue() != null && !screenFieldDD.getDefaultValue().equals("")) {
                inputText.setValue(screenFieldDD.getDefaultValue());
            }
        }
        inputText.setDisabled(!screenField.isEditable());
        HtmlPanelGrid panelGrid = new HtmlPanelGrid();

        panelGrid.setId(id + "MGrid");
        panelGrid.setColumns(1);
        panelGrid.getChildren().add(inputText);

        if (isUnique) {
            behavior.setUpdate(id + "MandMsg");
            Class[] parms2 = new Class[]{FacesContext.class, UIComponent.class, Object.class};
            MethodExpression mb1 = getFacesContext().getApplication().getExpressionFactory().
                    createMethodExpression(elContext, "#{" + getBeanName()
                            + ".validateUniquness}", null, parms2);
            inputText.addValidator(new MethodExpressionValidator(mb1));
            inputText.setValidatorMessage(
                    usrMsgService.getUserMessage("UniqueValidation", loggedUser).getMessageTextTranslated());
            message.setFor(id);

            if (!panelGrid.getChildren().contains(message)) {
                panelGrid.getChildren().add(message);
            }
        }
        if (screenFieldDD.getSpCharPattern() != null && !screenFieldDD.getSpCharPattern().isEmpty()) {
            behavior.setUpdate(id + "MandMsg");
            Class[] parms2 = new Class[]{FacesContext.class, UIComponent.class, Object.class};
            MethodExpression mb1 = getFacesContext().getApplication().getExpressionFactory().
                    createMethodExpression(elContext, "#{" + getBeanName()
                            + ".validateSpecialChar}", null, parms2);
            inputText.addValidator(new MethodExpressionValidator(mb1));
            inputText.setValidatorMessage(
                    usrMsgService.getUserMessage(screenFieldDD.getPatternMessage().getMessageTitle(), loggedUser).getMessageTextTranslated());
            message.setFor(id);

            if (!panelGrid.getChildren().contains(message)) {
                panelGrid.getChildren().add(message);
            }
        }
        if (isNumeric) {
            message.setFor(id);
            if (!panelGrid.getChildren().contains(message)) {
                panelGrid.getChildren().add(message);
            }
        }
        if (isMandatory) {

            behavior.setUpdate(id + "MandMsg");
            inputText.addClientBehavior("blur", behavior);

            inputText.setRequired(isMandatory);
            UserMessage userMessage = usrMsgService.getUserMessage("MandatoryField", loggedUser);
            inputText.setRequiredMessage(screenFieldDD.getLabelTranslated() + " "
                    + userMessage.getMessageTextTranslated());
            message.setFor(id);
            if (!panelGrid.getChildren().contains(message)) {
                panelGrid.getChildren().add(message);
            }
            logger.trace("Returning");
            return panelGrid;
        }
        panelGrid.setStyle("border:hidden;");
        logger.trace("Returning");
        return panelGrid;
    }

    public void validateSpecialChar(FacesContext context, UIComponent validate, Object value) {
        logger.debug("Entering");
        try {
            List<ScreenField> screenFields = getCurrentScreen().getScreenFields();
            String fielsexp = validate.getId().substring(0, validate.getId().indexOf("_"));
            String pattern = "";
            String msg1 = "";

            for (int i = 0; i < screenFields.size(); i++) {
                if (screenFields.get(i).getFieldExpression().equals(fielsexp)) {
                    pattern = screenFields.get(i).getDd().getSpCharPattern();
                    msg1 = screenFields.get(i).getDd().getPatternMessage().getMessageTitle();
                    break;
                }

            }
            oem.getEM(loggedUser);

            if (pattern != null && !pattern.isEmpty()) {

                Pattern p = Pattern.compile(pattern);
                Matcher m = p.matcher(value.toString());
                if (!m.matches()) {
                    HtmlMessage message = new HtmlMessage();
                    UserMessage userMessage = usrMsgService.getUserMessage(msg1, loggedUser);
                    message.setTitle(userMessage.getMessageTitleTranslated());
                    message.setStyleClass("inlineErrorMsgTitle");
                    message.setFor(validate.getClientId(context));

                    FacesMessage msg = new FacesMessage(userMessage.getMessageTextTranslated());
                    if (this instanceof TabularBean) {
                        TabularBean tabularBean = (TabularBean) this;
                        tabularBean.getDataTable().processUpdates(context);
                    } else if (this instanceof FormBean) {
                        ((HtmlInputText) validate).setValid(false);
                    }
                    context.addMessage(validate.getClientId(context), msg);// here is the problem
                    logger.debug("Returning");

                }
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
    }

    public void validateDate(FacesContext context, UIComponent validate, Object value) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Validation Error", "Invalid Email Format.");
        //we didn't use the value delibratly because it some times false because sdf.setLenient(true);
        org.primefaces.component.calendar.Calendar cal = (org.primefaces.component.calendar.Calendar) validate;

        try {
            logger.debug("Entering");

            SimpleDateFormat sdf = new SimpleDateFormat(cal.calculatePattern()); //.parse(cal.getSubmittedValue())
            sdf.setLenient(false);
            String submitedValue = (String) cal.getSubmittedValue();
            if (submitedValue.trim().length() != 0) {
                Date parse = sdf.parse(submitedValue);
                if (parse.getYear() < 0 || parse.getYear() > 300) {
                    throw new ValidatorException(message);
                }
            }
        } catch (ParseException ex) {
            throw new ValidatorException(message);
        }

    }

    private UIComponent createCalendarControl(ScreenField screenField,
            DD screenFieldDD, int screenFieldIndex, boolean isMandatory) {
        if (screenFieldDD.getCalendarPattern().getValue() != null && "HH:mm".equalsIgnoreCase(screenFieldDD.getCalendarPattern().getValue())) {
            return createTimeControl(screenField, screenFieldDD, screenFieldIndex, isMandatory);
        }
        return createDateControl(screenField, screenFieldDD, screenFieldIndex, isMandatory);
    }

    private UIComponent createDateControl(ScreenField screenField,
            DD screenFieldDD, int screenFieldIndex, boolean isMandatory) {
        logger.trace("Entering");
        org.primefaces.component.calendar.Calendar inputDate = new org.primefaces.component.calendar.Calendar();
        if (screenFieldDD.getCalendarPattern() != null && screenFieldDD.getCalendarPattern().getValue() != null
                && !"".equals(screenFieldDD.getCalendarPattern().getValue())) {
            inputDate.setPattern(screenFieldDD.getCalendarPattern().getValue());
        }
        Class[] dateFormatParameterType = new Class[]{FacesContext.class, UIComponent.class, Object.class};
        MethodExpression dateFormatValidationExpression = getFacesContext().getApplication().getExpressionFactory().
                createMethodExpression(elContext, "#{" + getBeanName()
                        + ".validateDate}", null, dateFormatParameterType);
        inputDate.addValidator(new MethodExpressionValidator(dateFormatValidationExpression));
        inputDate.setValidatorMessage("invalid date");

        inputDate.setYearRange("c-50:c+50");
        if (null != screenFieldDD.getCalendarPattern()) {
            inputDate.setPattern(screenFieldDD.getCalendarPattern().getValue());
        }
        inputDate.setEffect("slideDown");
        inputDate.setNavigator(true);
        return getCalenderControlFactory(inputDate, screenField, screenFieldDD, screenFieldIndex, isMandatory);

    }

    private UIComponent createTimeControl(ScreenField screenField,
            DD screenFieldDD, int screenFieldIndex, boolean isMandatory) {
        TimePicker timePicker = new TimePicker();
        timePicker.setMode("popup");
        return getCalenderControlFactory(timePicker, screenField, screenFieldDD, screenFieldIndex, isMandatory);
    }

    private UIComponent getCalenderControlFactory(HtmlInputText calendarComponent, ScreenField screenField,
            DD screenFieldDD, int screenFieldIndex, boolean isMandatory) {
        logger.trace("Entering");
        String id = FramworkUtilities.getID(screenField, screenFieldDD, screenFieldIndex);
        calendarComponent.setId(id);
        FABSAjaxBehavior behavior = new FABSAjaxBehavior();
        calendarComponent.addClientBehavior("blur", behavior);
        calendarComponent.addClientBehavior("change", behavior);
        calendarComponent.addClientBehavior("close", behavior);
        if (calendarComponent instanceof TimePicker) {
            calendarComponent.addClientBehavior("timeSelect", behavior);
        } else {
            calendarComponent.addClientBehavior("dateSelect", behavior);
        }
        FramworkUtilities.setControlFldExp(calendarComponent, screenField.getFieldExpression());
        if (screenFieldDD.getDefaultValue() != null
                && screenFieldDD.getDefaultValue().equalsIgnoreCase("today") && mode == OScreen.SM_ADD) {
            calendarComponent.setValue(timeZoneService.getUserCDT(loggedUser));
        }

        calendarComponent.setOnmouseover("updateFrameHeightForCal('" + getPortletInsId() + "')");
        Class[] parms = new Class[]{ValueChangeEvent.class};
        MethodBinding methodBinding = getFacesContext().getApplication().
                createMethodBinding("#{" + getBeanName() + ".updateRow}", parms);
        calendarComponent.setValueChangeListener(methodBinding);
        calendarComponent.setDir(htmlDir);
        if ("rtl".equals(htmlDir)) {
            calendarComponent.setStyle("text-align:right");
        }
        calendarComponent.setDisabled(!screenField.isEditable());
        behavior.setUpdate(id + "MandMsg");

        calendarComponent.setRequired(isMandatory);
        HtmlPanelGrid panelGrid = new HtmlPanelGrid();

        panelGrid.setId(FramworkUtilities.getID(screenField, screenFieldDD, screenFieldIndex) + "MGrid");
        panelGrid.setColumns(1);
        UserMessage userMessage = usrMsgService.getUserMessage("MandatoryField", loggedUser);
        Message message = new Message();
        message.setId(FramworkUtilities.getID(screenField, screenFieldDD, screenFieldIndex) + "MandMsg");
        calendarComponent.setRequiredMessage(userMessage.getMessageTextTranslated());
        panelGrid.getChildren().add(calendarComponent);
        message.setFor(calendarComponent.getId());
        panelGrid.getChildren().add(message);

        return panelGrid;

    }

    private UIComponent createCheckBoxControl(ScreenField screenField,
            DD screenFieldDD, int screenFieldIndex, boolean isMandatory) {

        SelectBooleanCheckbox checkbox = new SelectBooleanCheckbox();
        checkbox.setId(FramworkUtilities.getID(screenField, screenFieldDD, screenFieldIndex));
        checkbox.setImmediate(true);
        checkbox.setDir(htmlDir);
        checkbox.getAttributes().put("media", "print");
        FramworkUtilities.setControlFldExp(checkbox, screenField.getFieldExpression());
        if (htmlDir.equals("rtl")) {
            checkbox.setStyle("text-align:right;");
        }
        FABSAjaxBehavior behavior = new FABSAjaxBehavior();
        checkbox.addClientBehavior("change", behavior);
        Class[] parms = new Class[]{ValueChangeEvent.class};
        MethodBinding methodBinding = getFacesContext().getApplication().
                createMethodBinding("#{" + getBeanName() + ".updateRow}", parms);
        checkbox.setValueChangeListener(methodBinding);
        //The tab index is not working when it is set in tabular screen!!
        //By Commenting it it is working on both types of screens !!!
        if (screenFieldDD != null && screenFieldDD.getDefaultValue() != null
                && (screenFieldDD.getDefaultValue().equals("1")
                || screenFieldDD.getDefaultValue().equalsIgnoreCase("true")) && mode == OScreen.SM_ADD) {
            checkbox.setValue(true);
        }
        checkbox.setDisabled(!screenField.isEditable());

        if (isMandatory) {
            checkbox.setRequired(isMandatory);

            HtmlPanelGrid panelGrid = new HtmlPanelGrid();
            panelGrid.setStyle("border:hidden;");
            panelGrid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "pnGrid");
            panelGrid.setColumns(1);
            panelGrid.getChildren().add(checkbox);
            UserMessage userMessage = usrMsgService.getUserMessage("MandatoryField", loggedUser);
            Message message = new Message();
            checkbox.setRequiredMessage(userMessage.getMessageTextTranslated());
            logger.trace("Returning");
            return panelGrid;
        }
        logger.trace("Returning");
        return checkbox;
    }

    private UIComponent createLabelControl(ScreenField screenField,
            DD screenFieldDD) {
        logger.trace("Entering");
        HtmlOutputLabel htmlOutputText = null;
        FABSAjaxBehavior behavior = new FABSAjaxBehavior();
        String id = FramworkUtilities.getID(screenField, screenFieldDD, screenField.getSortIndex());
        fieldIDs.add(id);
        if (htmlOutputText != null) {
            htmlOutputText.setId(id);
            logger.trace("Returning");
            return htmlOutputText;
        } else {
            htmlOutputText = new HtmlOutputLabel();
            htmlOutputText.setId(id);
        }
        htmlOutputText.setDir(htmlDir);
        if (htmlDir.equals("rtl")) {
            htmlOutputText.setStyle("text-align:right");
        }
        FramworkUtilities.setControlFldExp(htmlOutputText, screenField.getFieldExpression());
        //The tab index is not working when it is set in tabular screen!!
        //By Commenting it it is working on both types of screens !!!
        if (mode == OScreen.SM_ADD && screenFieldDD.getDefaultValue() != null
                && !screenFieldDD.getDefaultValue().equals("")) {
            htmlOutputText.setValue(screenFieldDD.getDefaultValue());
        }
        if (screenFieldDD.getCalendarPattern() != null) {
            FABSDateConverter converter = new FABSDateConverter(screenFieldDD.getCalendarPattern().getValue());
            htmlOutputText.setConverter(converter);
        } else if (screenFieldDD.getDecimalMask() != null) {
            FABSDecimalConverter converter = new FABSDecimalConverter(screenFieldDD.getDecimalMask());
            htmlOutputText.setConverter(converter);
        }
        htmlOutputText.setStyleClass("labelTxtValue");
        logger.trace("Entering");
        return htmlOutputText;
    }

    private UIComponent createRichTextControl(ScreenField screenField,
            DD screenFieldDD, int screenFieldIndex, boolean isMandatory) {
        logger.trace("Entering");
        CKEditor inputRichText = new CKEditor();
        inputRichText.setCustomConfig("/FABS-FileUploader/js/ckeditor.js");
        inputRichText.setId(FramworkUtilities.getID(screenField, screenFieldDD, screenFieldIndex));
        FramworkUtilities.setControlFldExp(inputRichText, screenField.getFieldExpression());
        buildScreenExpression(inputRichText);
        if (isMandatory) {
            inputRichText.setRequired(isMandatory);
            HtmlPanelGrid panelGrid = new HtmlPanelGrid();
            panelGrid.setStyle("border:hidden;");
            panelGrid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "pnlGrid1");
            panelGrid.setColumns(1);
            panelGrid.getChildren().add(inputRichText);
            UserMessage userMessage = usrMsgService.getUserMessage("MandatoryField", loggedUser);
            Message message = new Message();
            inputRichText.setRequiredMessage(userMessage.getMessageTextTranslated());
            logger.trace("Returning");
            return panelGrid;
        }
        logger.trace("Returning");
        return inputRichText;
    }

    private UIComponent createTextAreaControl(ScreenField screenField,
            DD screenFieldDD, int screenFieldIndex, boolean isMandatory) {
        logger.trace("Entering");
        int dimension = CHARACTER_LENGTH_IN_PIXEL * screenFieldDD.getControlSize();
        InputTextarea inputTextarea = new InputTextarea();
        String id = FramworkUtilities.getID(screenField, screenFieldDD, screenFieldIndex);
        inputTextarea.setId(id);
        FramworkUtilities.setControlFldExp(inputTextarea, screenField.getFieldExpression());
        Class[] parms = new Class[]{ValueChangeEvent.class};
        MethodBinding methodBinding = getFacesContext().getApplication().
                createMethodBinding("#{" + getBeanName() + ".updateRow}", parms);
        inputTextarea.setValueChangeListener(methodBinding);
        FABSAjaxBehavior behavior = new FABSAjaxBehavior();
        inputTextarea.addClientBehavior("blur", behavior);
        inputTextarea.setDir(htmlDir);
        if (htmlDir.equals("rtl")) {
            inputTextarea.setStyle("text-align:right");
        }
        if (htmlDir.equalsIgnoreCase("rtl")) {
            inputTextarea.setStyle("width:" + dimension + "px;height:100px!important; max-width: " + dimension + "px!important; margin-right: 2px!important;");
        } else {
            inputTextarea.setStyle("width:" + dimension + "px;height:100px!important; max-width: " + dimension + "px!important; margin-right: 30px!important;");
        }
        //The tab index is not working when it is set in tabular screen!!
        //By Commenting it it is working on both types of screens !!!
        if (mode == OScreen.SM_ADD) {
            if (!(screenFieldDD.getDefaultValue() == null
                    || screenFieldDD.getDefaultValue().isEmpty())) {
                inputTextarea.setValue(screenFieldDD.getDefaultValue());
            }
        }
        inputTextarea.setDisabled(!screenField.isEditable());
        HtmlPanelGrid panelGrid = new HtmlPanelGrid();

        panelGrid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "pnlGrid1");
        panelGrid.setColumns(1);
        panelGrid.getChildren().add(inputTextarea);
        panelGrid.setStyle("border:hidden;");
        if (isMandatory) {
            inputTextarea.setRequired(isMandatory);
            UserMessage userMessage = usrMsgService.getUserMessage("MandatoryField", loggedUser);
            inputTextarea.setRequiredMessage(userMessage.getMessageTextTranslated());
            Message message = new Message();
            message.setFor(id);
            if (!panelGrid.getChildren().contains(message)) {
                panelGrid.getChildren().add(message);
            }
            logger.trace("Returning");
            return panelGrid;
        }
        logger.trace("Returning");
        return panelGrid;
    }

    private UIComponent createURLLinkControl(ScreenField screenField, DD screenFieldDD, int screenFieldIndex) {
        logger.trace("Entering");
        HtmlOutputLink urlLink = new HtmlOutputLink();
        urlLink.setId("ulrLink" + FramworkUtilities.getID(screenField, screenFieldDD, screenFieldIndex));
        urlLink.setValue(screenFieldDD.getDefaultValue());
        urlLink.setTarget("WWW");
        urlLink.setStyle("width:50px; height:15px;");
        urlLink.setTitle(screenFieldDD.getDefaultValue());
        FramworkUtilities.setControlFldExp(urlLink, screenField.getFieldExpression());
        logger.trace("Returning");
        return urlLink;
    }

    @Override
    protected abstract void buildScreenExpression(UIComponent component);
    // </editor-fold>

    protected boolean editAllClicked = false;

    // <editor-fold defaultstate="collapsed" desc="Lookup Functions">
    public String lookupReturned() {
        logger.debug("Entering");
        if (editAllClicked) {
            RequestContext.getCurrentInstance().execute("editAll()");
        } else if (currentScreen instanceof TabularScreen && !fieldLookup.getId().contains("_filterField")) {
            RequestContext.getCurrentInstance().execute("KeepRowEditable(" + getObjectRecordID(
                    Long.parseLong(recordID)) + ")");
        }
        if (fieldLookup.getFieldDD().getControlType().getDbid() == DD.CT_LOOKUPATTRIBUTEBROWSER) {
            if (getFieldExpLookupData() == null) {
                returnEmpty();
            } else {
                returnTreeSelectedField();
            }
        }
        if (getLookupData() == null) {
            returnEmpty();
        } else {
            returnSelected();
        }
        logger.debug("Returning with Null");
        return null;
    }

    protected Object[] getLookupDataList() {
        try {
            return (Object[]) SessionManager.getSessionAttribute("lookupRetDataList");
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        return null;
    }

    protected Object getLookupData() {
        try {
            return SessionManager.getSessionAttribute("lookupRetData");
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        return null;
    }

    protected String getFieldExpLookupData() {
        logger.trace("Entering");
        try {
            logger.trace("Returning");
            return (null == SessionManager.getSessionAttribute("lookupRetData"))
                    ? null : String.valueOf(SessionManager.getSessionAttribute("lookupRetData"));
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.trace("Returning with Null");
        return null;
    }

    protected BaseEntity[] selectedEntities;

    public BaseEntity[] getSelectedEntities() {
        return selectedEntities;
    }

    public void setSelectedEntities(BaseEntity[] selectedEntities) {
        this.selectedEntities = selectedEntities;
    }

    public String setLookupData() {
        logger.debug("Entering");
        try {
            BaseEntity[] list = getSelectedEntities();
            if (list == null || list.length == 0) {
                logger.warn("Invalid Slected List");
            } else {
                SessionManager.addSessionAttribute("lookupRetData", list[0]);
                if (list.length > 1) {
                    SessionManager.addSessionAttribute("lookupRetDataList", list);
                } else {
                    SessionManager.addSessionAttribute("lookupRetDataList", null);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        String orScreen = (String) SessionManager.getSessionAttribute("lookupORScreen");
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("top.FABS.Portlet.lookupReturned({portletId:'" + orScreen + "'});");
        logger.trace("Calling JS closePopupNoID with PortletInsId: {}", getPortletInsId());
        RequestContext.getCurrentInstance().execute("top.FABS.Portlet.closePopupNoID('" + getPortletInsId() + "');");
        logger.debug("Returning with Null");
        return null;
    }

    public String returnTreeSelectedExpression() {
        logger.debug("Entering");
        if (getTreeLookupData() != null) {
            SessionManager.addSessionAttribute("lookupRetData", getTreeLookupData());
            String orScreen = (String) SessionManager.getSessionAttribute("lookupORScreen");
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("top.FABS.Portlet.lookupReturned({portletId:'" + orScreen + "'});");
            logger.trace("Calling JS closePopupNoID with PortletInsId: {}", getPortletInsId());
            RequestContext.getCurrentInstance().execute("top.FABS.Portlet.closePopupNoID('" + getPortletInsId() + "');");

        }
        logger.debug("Returning with Null");
        return null;
    }

    public String returnTreeSelectedField() {
        logger.debug("Entering");
        if (getFieldExpLookupData() != null) {
            BaseEntity entity = null;
            try {
                entity = getObjectForLookup(Long.parseLong(recordID));
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
            FramworkUtilities.setValueInEntity(entity, fieldLookup.getFieldExpression(), getFieldExpLookupData());
            FramworkUtilities.setValueInEntity(entity, "changed", true);
            applyDepencyInitialization(entity, getFieldExpLookupData(), fieldLookup.getFieldExpression());
            lookupPanelRendered = false;
        }
        logger.debug("Returning");
        return null;
    }

    private String getTreeLookupData() {
        logger.debug("Entering");
        if (entityFieldsTreeBrowser != null && entityFieldsTreeBrowser.getSelectedNodeObject() != null) {
            return entityFieldsTreeBrowser.getSelectedNodeAsDynamic().getNodeExpression();
        }
        logger.debug("Returning");
        return null;
    }

    public BaseEntity returnTreeSelectedNode() {
        logger.debug("Entering");
        if (getEntityTreeLookupData() != null) {
            SessionManager.addSessionAttribute("lookupRetData", getEntityTreeLookupData());
            String orScreen = (String) SessionManager.getSessionAttribute("lookupORScreen");
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("top.FABS.Portlet.lookupReturned({portletId:'" + orScreen + "'});");
            logger.trace("Calling JS closePopupNoID with PortletInsId: {}", getPortletInsId());
            RequestContext.getCurrentInstance().execute("top.FABS.Portlet.closePopupNoID('" + getPortletInsId() + "');");
        }
        logger.debug("Returning with Null");
        return null;
    }

    private BaseEntity getEntityTreeLookupData() {
        LevelNode levelNode = entityTreeBrowser.getSelectedNode(selectedNodes[0]);
        if (entityTreeBrowser != null && levelNode != null) {
            return levelNode.getNodeBaseEntity();
        }
        logger.debug("Returning with Null");
        return null;
    }

    public String exitLookup() {
        logger.trace("Calling JS closePopupNoID with PortletInsId: {}", getPortletInsId());
        logger.debug("Entering");
        lookupPanelRendered = false;
        logger.trace("Calling JS closePopupNoID with PortletInsId: {}", getPortletInsId());
        RequestContext.getCurrentInstance().execute("top.FABS.Portlet.closePopupNoID('" + getPortletInsId() + "');");
        logger.debug("Returning with Null");
        return null;
    }

    public void setLookupPanelPopup(HtmlPanelGrid lookupPanelPopup) {
        this.lookupPanelPopup = lookupPanelPopup;
    }

    public boolean isLookupPanelRendered() {
        return lookupPanelRendered;
    }

    public void setLookupPanelRendered(boolean lookupPanelRendered) {
        this.lookupPanelRendered = lookupPanelRendered;
    }

    public String returnSelected() {
        logger.debug("Entering");
        try {
            BaseEntity selectedEntity = (BaseEntity) getLookupData();
            if (selectedEntity != null) {
                if (null != fieldLookup
                        && fieldLookup.getOwnerEntityBackBeanExpression() != null
                        && fieldLookup.getOwnerEntityBackBeanExpression().contains("valuesMap")) {
                    String filterFieldExp = fieldLookup.getOwnerEntityBackBeanExpression()
                            .substring(fieldLookup.getOwnerEntityBackBeanExpression().indexOf("['") + 2,
                                    fieldLookup.getOwnerEntityBackBeanExpression().indexOf("']"));
                    valuesMap.put(filterFieldExp, selectedEntity);
                } else if (!recordID.equals("")) {
                    BaseEntity entity = getObjectForLookup(Long.parseLong(recordID));
                    List<ScreenField> screenFields = getCurrentScreen().getScreenFields();

                    ScreenField changedScrFld = null;

                    // <editor-fold defaultstate="collapsed" desc="Get Component ScreenField">
                    for (int idx = 0; idx < screenFields.size(); idx++) {
                        if (screenFields.get(idx).getFieldExpression().equals(fieldLookup.getFieldExpression())) {
                            changedScrFld = screenFields.get(idx);
                            break;
                        }
                    }
                    if (changedScrFld != null && changedScrFld.getChangeFunction() != null) {
                        ODataType changeDT = dataTypeService.loadODataType("EntityChangeField", loggedUser);
                        List changeData = new ArrayList();
                        if (this instanceof FormBean) {
                            changeData.add(((FormBean) this).getLoadedObject());
                            //RequestContext.getCurrentInstance().update("frmScrForm");
                        } else {
                            changeData.add(entity);//getLoadedEntity()
                        }
                        changeData.add(fieldLookup.getFieldExpression());
                        changeData.add(selectedEntity);
                        changeData.add(screenFields);

                        ODataMessage changeODM = new ODataMessage(changeDT, changeData);
                        OFunctionResult ofr = functionServiceRemote.
                                runFunction(changedScrFld.getChangeFunction(),
                                        changeODM, null, loggedUser);
                        if (ofr.getReturnedDataMessage() != null
                                && ofr.getReturnedDataMessage().getODataType() != null
                                && !ofr.getReturnedDataMessage().getData().isEmpty()
                                && ofr.getReturnedDataMessage().getData().get(0).getClass().getName().
                                equals(getOactOnEntity().getEntityClassPath())) {
                            entity = (BaseEntity) ofr.getReturnedDataMessage().getData().get(0);
                            if (this instanceof FormBean) {
                                ((FormBean) this).setLoadedObject(entity);
                                RequestContext.getCurrentInstance().reset("frmScrForm");
                            }
                            if (entity.getDbid() != 0 && mode == OScreen.SM_ADD) {
                                mode = OScreen.SM_EDIT;
                            }
                        }
                    }
                    FramworkUtilities.setValueInEntity(entity, fieldLookup.getFieldExpression(), selectedEntity, true);
                    applyDepencyInitialization(entity, selectedEntity, fieldLookup.getFieldExpression());

                    //<editor-fold defaultstate="collapsed" desc="For Translating Other Expressions not in the lookup screen">
                    String expParent = fieldLookup.getFieldExpression().substring(0,
                            fieldLookup.getFieldExpression().lastIndexOf("."));
                    for (int idx = 0; idx < screenFields.size(); idx++) {
                        if (screenFields.get(idx).getFieldExpression().startsWith(expParent)
                                && !screenFields.get(idx).getFieldExpression().equals(fieldLookup.getFieldExpression())) {
                            if (screenFields.get(idx).getFieldExpression().contains(".")) {
                                String fieldExp = screenFields.get(idx).getFieldExpression()
                                        .substring(0, screenFields.get(idx).getFieldExpression().lastIndexOf("."));
                                if (screenFields.get(idx).getFieldExpression().substring(expParent.length() + 1).contains(".")) {
                                    BaseEntity entityField = (BaseEntity) entity.invokeGetter(fieldExp);
                                    if (entityField != null) {
                                        textTranslationServiceRemote.loadEntityTranslation(
                                                entityField, loggedUser);

                                    }
                                }
                            }
                        }
                    }
                    //</editor-fold>

                    lookupPanelRendered = false;
                    logger.trace("Lookup Selection Information");
                }
            } else {
                logger.warn("Null Selected Entity");
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning with Null");
        return null;
    }

    public String returnSelectedList() {
        logger.debug("Entering");
        try {
            BaseEntity[] selectedEntitysList = (BaseEntity[]) getLookupDataList();
            if (selectedEntitysList != null && selectedEntitysList.length > 1) {
                if (null != fieldLookup
                        && fieldLookup.getOwnerEntityBackBeanExpression() != null
                        && fieldLookup.getOwnerEntityBackBeanExpression().contains("valuesMap")) {
                    String filterFieldExp = fieldLookup.getOwnerEntityBackBeanExpression()
                            .substring(fieldLookup.getOwnerEntityBackBeanExpression().indexOf("['") + 2,
                                    fieldLookup.getOwnerEntityBackBeanExpression().indexOf("']"));
                    for (int j = 0; j < selectedEntitysList.length; j++) {
                        valuesMap.put(filterFieldExp + "-" + j, selectedEntitysList[j]);
                    }
                } else {

                    BaseEntity entity = getObjectForLookup(Long.parseLong(recordID));
                    FramworkUtilities.setValueInEntity(entity, fieldLookup.getFieldExpression(), selectedEntitysList[0], true);
                    applyDepencyInitialization(entity, selectedEntitysList[0], fieldLookup.getFieldExpression());

                    List<ScreenField> screenFields = getCurrentScreen().getScreenFields();

                    //<editor-fold defaultstate="collapsed" desc="For Translating Other Expressions not in the lookup screen">
                    String expParent = fieldLookup.getFieldExpression().substring(0,
                            fieldLookup.getFieldExpression().lastIndexOf("."));
                    for (int idx = 0; idx < screenFields.size(); idx++) {
                        if (screenFields.get(idx).getFieldExpression().startsWith(expParent)
                                && !screenFields.get(idx).getFieldExpression().equals(fieldLookup.getFieldExpression())) {
                            String fieldExp = screenFields.get(idx).getFieldExpression()
                                    .substring(0, screenFields.get(idx).getFieldExpression().lastIndexOf("."));
                            if (screenFields.get(idx).getFieldExpression().substring(expParent.length() + 1).contains(".")) {
                                textTranslationServiceRemote.loadEntityTranslation(
                                        (BaseEntity) entity.invokeGetter(fieldExp), loggedUser);
                            }
                        }
                    }
                    //</editor-fold>

                    ScreenField changedScrFld = null;

                    // <editor-fold defaultstate="collapsed" desc="Get Component ScreenField">
                    for (int idx = 0; idx < screenFields.size(); idx++) {
                        if (screenFields.get(idx).getFieldExpression().equals(fieldLookup.getFieldExpression())) {
                            changedScrFld = screenFields.get(idx);
                            break;
                        }
                    }
                    boolean anyValueChanged = false;
                    if (changedScrFld != null && changedScrFld.getChangeFunction() != null) {
                        ODataType changeDT = dataTypeService.loadODataType("EntityChangeField", loggedUser);
                        List changeData = new ArrayList();
                        if (this instanceof FormBean) {
                            changeData.add(((FormBean) this).getLoadedObject());
                            RequestContext.getCurrentInstance().update("frmScrForm");
                        } else {
                            changeData.add(entity);//getLoadedEntity()
                        }
                        changeData.add(fieldLookup.getFieldExpression());
                        changeData.add(selectedEntitysList[0]);
                        changeData.add(screenFields);

                        ODataMessage changeODM = new ODataMessage(changeDT, changeData);
                        OFunctionResult ofr = functionServiceRemote.
                                runFunction(changedScrFld.getChangeFunction(),
                                        changeODM, null, loggedUser);
                        if (ofr.getReturnedDataMessage() != null
                                && ofr.getReturnedDataMessage().getODataType() != null
                                && !ofr.getReturnedDataMessage().getData().isEmpty()
                                && ofr.getReturnedDataMessage().getData().get(0).getClass().getName().
                                equals(getOactOnEntity().getEntityClassPath())) {
                            entity = (BaseEntity) ofr.getReturnedDataMessage().getData().get(0);
                            if (this instanceof FormBean) {
                                ((FormBean) this).setLoadedObject(entity);
                            }
                            if (entity.getDbid() != 0 && mode == OScreen.SM_ADD) {
                                mode = OScreen.SM_EDIT;
                            }
                        }
                    }
                    lookupPanelRendered = false;
                    logger.trace("Lookup Selection Information");
                }
            } else {
                logger.warn("Null Selected Entity");
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning with Null");
        return null;
    }

    public void updateFieldsIDs() {
    }

    public void updateFieldsIDs(String updaterFieldID) {
    }

    public String setEmptyLookupData() {
        logger.trace("Calling JS closePopupNoID with PortletInsId: {}", getPortletInsId());
        SessionManager.addSessionAttribute("lookupRetData", null);
        String orScreen = (String) SessionManager.getSessionAttribute("lookupORScreen");
        RequestContext.getCurrentInstance().execute("top.FABS.Portlet.lookupReturned({portletId:'" + orScreen + "'});");
        RequestContext.getCurrentInstance().execute("top.FABS.Portlet.closePopupNoID('" + getPortletInsId() + "');");
        return null;
    }

    public String exitPopupLookup() {
        logger.trace("Calling JS closePopupNoID with PortletInsId: {}", getPortletInsId());
        RequestContext.getCurrentInstance().execute("top.FABS.Portlet.closePopupNoID('" + getPortletInsId() + "');");
        return null;
    }

    public String returnEmpty() {
        logger.debug("Entering");
        try {
            BaseEntity entity = getObjectForLookup(Long.parseLong(recordID));

            if (null == entity && null != fieldLookup.getOScreen()) {
                if (fieldLookup.getOwnerEntityBackBeanExpression() != null
                        && fieldLookup.getOwnerEntityBackBeanExpression().contains("valuesMap")) {
                    String filterFieldExp = fieldLookup.getOwnerEntityBackBeanExpression()
                            .substring(fieldLookup.getOwnerEntityBackBeanExpression().indexOf("['") + 2,
                                    fieldLookup.getOwnerEntityBackBeanExpression().indexOf("']"));
                    BaseEntity newEntity = (BaseEntity) Class.forName(entitySetupService.
                            getOScreenEntityDTO(fieldLookup.getOScreen().getDbid(), loggedUser).getEntityClassPath())
                            .newInstance();
                    valuesMap.put(filterFieldExp, newEntity);
                    logger.debug("Returning with Null");
                    return null;
                }
            }

            if (null != fieldLookup.getOScreen()) {
                FramworkUtilities.setValueInEntity(entity, fieldLookup.getFieldExpression(),
                        Class.forName(
                                entitySetupService.getOScreenEntityDTO(
                                        fieldLookup.getOScreen().getDbid(), loggedUser).
                                getEntityClassPath()).newInstance());
                loadAttribute(fieldLookup.getFieldExpression());
            } else {
                FramworkUtilities.setValueInEntity(entity, fieldLookup.getFieldExpression(), null);
            }

            FramworkUtilities.setValueInEntity(entity, "changed", true);
            List<ScreenField> screenFields = getCurrentScreen().getScreenFields();
            ScreenField changedScrFld = null;

            // <editor-fold defaultstate="collapsed" desc="Get Component ScreenField">
            for (int idx = 0; idx < screenFields.size(); idx++) {
                if (screenFields.get(idx).getFieldExpression().equals(fieldLookup.getFieldExpression())) {
                    changedScrFld = screenFields.get(idx);
                    break;
                }
            }

            if (changedScrFld != null && changedScrFld.getChangeFunction() != null) {
                ODataType changeDT = dataTypeService.loadODataType("EntityChangeField", loggedUser);
                List changeData = new ArrayList();

                changeData.add(entity);
                changeData.add(fieldLookup.getFieldExpression());
                changeData.add(null);

                ODataMessage changeODM = new ODataMessage(changeDT, changeData);

                OFunctionResult ofr = functionServiceRemote.runFunction(changedScrFld.getChangeFunction(), changeODM, null, loggedUser);

                if (ofr.getReturnedDataMessage() != null && ofr.getReturnedDataMessage().getODataType() != null
                        && ofr.getReturnedDataMessage().getODataType().getName().equals(getOactOnEntity().getEntityClassName())
                        && !ofr.getReturnedDataMessage().getData().isEmpty()
                        && ofr.getReturnedDataMessage().getData().get(0).getClass().getName().equals(getOactOnEntity().getEntityClassPath())) {
                    entity = (BaseEntity) ofr.getReturnedDataMessage().getData().get(0);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning with Null");
        return null;
    }

    public HtmlPanelGrid getLookupPanelPopup() {
        logger.debug("Entering");
        if (lookupPanelRendered) {
            try {
                oem.getEM(loggedUser);
                lookupPanelPopup.getChildren().clear();
                String entityClassName = (null == actOnEntityCls) ? "" : actOnEntityCls.getSimpleName();
                lookupPanelPopup.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_LokupPanelPopup");
                lookupPanelPopup.getChildren().add(fieldLookup.buildLookupPanelPopup(
                        headerObject, entityClassName, ddService, entitySetupService, entityServiceLocal,
                        uiFrameWorkFacade));
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            } finally {
                oem.closeEM(loggedUser);
            }
        }
        logger.debug("Returning");
        return lookupPanelPopup;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc=" Lookup Action Listener/ Value Change Listener">
    public void lookupFilterChangeListener(ValueChangeEvent vce) {
    }

    public void lookupFilterActionListener(ActionEvent actionEvent) {
        try {
            oem.getEM(loggedUser);
            CommandButton checkButton = (CommandButton) actionEvent.getComponent();
            String fieldExperssion = checkButton.getId().substring(0, checkButton.getId().lastIndexOf("_"));
            fieldExperssion = fieldExperssion.replace("_", ".");
            fieldLookup.setLookupFilter(removeEntryFromList(
                    fieldExperssion, fieldLookup.getLookupFilter()));
            if ("/resources/unchecked.GIF".equals(checkButton.getImage())) {
                checkButton.setImage("/resources/partialchecked.GIF");
            } else if ("/resources/partialchecked.GIF".equals(checkButton.getImage())) {
                checkButton.setImage("/resources/checked.GIF");
                fieldLookup.getLookupFilter().add(fieldExperssion + " = true");
            } else if ("/resources/checked.GIF".equals(checkButton.getImage())) {
                checkButton.setImage("/resources/unchecked.GIF");
                fieldLookup.getLookupFilter().add(fieldExperssion + " = false");
            }
            setLookupList(fieldLookup.loadLookUpList(
                    fieldLookup.getLookupFilter()));
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            //FIXME: how to display error to user, e.g. User Message
        } finally {
            oem.closeEM(loggedUser);
        }
    }

    public void lookupChangeFilterValue(ValueChangeEvent event) {
        logger.debug("Entering");
        InputText uIComponent = new InputText();
        uIComponent = (InputText) event.getComponent();

        String fieldExperssion = uIComponent.getId().substring(
                0, uIComponent.getId().lastIndexOf("_"));
        fieldExperssion = fieldExperssion.replace("_", ".");
        fieldLookup.setLookupFilter(removeEntryFromList(fieldExperssion,
                fieldLookup.getLookupFilter()));
        String value = (String) event.getNewValue();
        if (value == null || "".equals(value)) {
            String oldValue = (String) event.getOldValue();
            if (oldValue != null && !"".equals(oldValue)) {
                setLookupList(fieldLookup.loadLookUpList(
                        fieldLookup.getLookupFilter()));
            }
            logger.debug("Returning");
            return;
        }
        if (FramworkUtilities.isNumber(value)) {
            fieldLookup.getLookupFilter().add(fieldExperssion + " = " + value);
        } else {
            fieldLookup.getLookupFilter().add(
                    fieldExperssion + " LIKE '%" + value.toLowerCase() + "%'");
        }
    }

    public void lookupFilterChangeListener(ActionEvent vce) {
        InputText uIComponent = new InputText();
        try {
            oem.getEM(loggedUser);
            setLookupList(fieldLookup.loadLookUpList(
                    fieldLookup.getLookupFilter()));
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            //FIXME: how to display error to user, e.g. User Message
        } finally {
            oem.closeEM(loggedUser);
            //uIComponent.setFocus(false);//requestFocus();
        }
    }
    //</editor-fold>

    /*
     * Provide lookup table with data and must be in single entity bean.
     */
    protected List<BaseEntity> lookupList = new ArrayList<BaseEntity>();

    public List<BaseEntity> getLookupList() {
        return lookupList;
    }

    public void setLookupList(List<BaseEntity> lookupList) {
        this.lookupList = lookupList;
    }

    /**
     * Creates a new instance of SingleEntityBean
     */
    public SingleEntityBean() {
    }

    @Override
    public SingleEntityScreen getCurrentScreen() {
        return (SingleEntityScreen) super.getCurrentScreen();
    }

    abstract protected BaseEntity getObjectForLookup(long dbid);

    protected int getObjectRecordID(long dbid) {
        return 0;
    }

    abstract public void loadAttribute(String expression);
    // <editor-fold defaultstate="collapsed" desc="actOnEntityDataType & getter">
    /**
     * The DataType of the actOnEntity <br>Don't add setActOnEntityDataType, as
     * it's set in the getter {@link #getActOnEntityDataType()} if not found
     * (cashed)
     */
    private ODataType actOnEntityDataType;

    /**
     * Returns the DataType of {@link #currentScreen} actOnEntity. <br> It
     * cashes the data type for performance purpose, and if not already loaded
     * the function will load it from database
     *
     * @return <br>actOnEntity DataType when successful <br>null: error, refer
     * to log
     */
    public ODataType getActOnEntityDataType() {
        logger.debug("Entering");
        if (actOnEntityDataType != null) {
            logger.trace("Cached actOnEntityDataType Is Used");
            logger.debug("Returning");
            return actOnEntityDataType;
        }
        try {
            actOnEntityDataType = dataTypeService.loadODataType(getOactOnEntity().getEntityClassName(), systemUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        //FIXME: if it's null return display errors in UserMessagPanel or something
        // <editor-fold defaultstate="collapsed" desc="Log">
        if (actOnEntityDataType == null) {
            logger.warn("Act On Entity Has No Data Type");
        } else {
            logger.warn("actOnEntityDataType Is Loaded");
        }
        // </editor-fold>
        logger.debug("Returning");
        return actOnEntityDataType;
    }
    // </editor-fold>

    public void applyFunctionInitialization(BaseEntity entity) {
        List<EntityAttributeInit> attributeInits = null;
        try {
            attributeInits = entitySetupService.
                    getObjectDTOEntityAttributeInits(getOactOnEntity(), loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        for (EntityAttributeInit attrInit : attributeInits) {
            try {
                if (attrInit.isInActive()) {
                    continue;
                }
                if (attrInit.getFieldExpression() == null) {
                    logger.warn("Field Expression is Null");
                    continue;
                }

                if (attrInit.getFunction() != null) {
                    OFunctionResult initFuncResult = runFunctionForEntity(attrInit.getFunction(), entity, "");
                    ODataMessage returnedDM = initFuncResult.getReturnedDataMessage();
                    if (returnedDM.getODataType().getName().equals("SingleObjectValue")
                            && returnedDM.getData().size() == 1) {
                        BaseEntity.setValueInEntity(entity, attrInit.getFieldExpression(), returnedDM.getData().get(0));
                    } else {
                        logger.warn("Invalid returned Data Message");
                    }
                }
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }

        }
    }

    public void applySequenceInitialization(BaseEntity entity) {
        List<EntityAttributeInit> attributeInits = null;
        try {
            attributeInits = entitySetupService.
                    getObjectDTOEntityAttributeInits(getOactOnEntity(), loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        for (EntityAttributeInit attrInit : attributeInits) {

            if (attrInit.isInActive()) {
                continue;
            }

            if (attrInit.getFieldExpression() == null) {
                logger.warn("Field Expression is Null");
                continue;
            }

            if (attrInit.isSequence() || attrInit.isSequencePerParent()) {
                List<ExpressionFieldInfo> expressionFieldInfos = BaseEntity.parseFieldExpression(actOnEntityCls, attrInit.getFieldExpression());
                ExpressionFieldInfo expressionFieldInfo = expressionFieldInfos.get(expressionFieldInfos.size() - 1);

                Object originalValue = null;
                try {
                    originalValue = BaseEntity.getValueFromEntity(entity, attrInit.getFieldExpression());
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }

                if ((originalValue != null
                        && originalValue instanceof BaseEntity
                        && ((BaseEntity) originalValue).getDbid() != 0)
                        || (originalValue instanceof String
                        && !originalValue.toString().equals(""))
                        || (originalValue != null && originalValue instanceof Number)
                        && ((Number) originalValue).intValue() != 0) {
                    logger.trace("The Field Already has value and will not be initialized");
                    continue;
                }
                Object initVlaue = null;
                List<String> fieldExpression = new ArrayList<String>();
                fieldExpression.add(attrInit.getFieldExpression());
                List<BaseEntity> dbList = null;
                List<String> dbListconditions = new ArrayList<String>();

                //FIXME : The logic needs a revisit
                if (attrInit.isSequencePerParent()) {
                    if (headerObjectIsParent) {
                        // headerObjectIsParent
                        if (headerObject != null) {
                            // Header object passed
                            OFunctionResult parentOFR = null;
                            try {
                                parentOFR = uiFrameWorkFacade.getParentFromParentDBID(
                                        entitySetupService.loadOEntity(
                                                getOactOnEntity().getEntityClassPath(), loggedUser),
                                        headerObject.getDbid(), loggedUser);
                            } catch (UserNotAuthorizedException ex) {
                                logger.error("Exception thrown", ex);
                            }
                            if (!parentOFR.getErrors().isEmpty()) {
                                // Error found
                                logger.warn("Error Getting The Parent");
                                // FIXME: meaningful error message
                            } else if (parentOFR.getReturnValues().isEmpty()) {
                                // No parent found matches the DBID
                                // Consider it error
                                logger.warn("Parent DBID Not Found In Any Of The Parents Tables");
                                // FIXME: meaningful error message
                            } else {
                                // Parent got successfully
                                dbListconditions.add((parentOFR.getReturnValues().get(1)) // parentFieldName
                                        + ".dbid = " + headerObject.getDbid());
                            }
                        } else {
                            // Header object Not passed
                            // FIXME: Is there a chance for that? since headerObjectIsParent is true?
                            List<String> entityParentFields = getOactOnEntity().getEntityParentFieldName();
                            if (entityParentFields != null && !entityParentFields.isEmpty()) {
                                // Parents found for the actOnEntity
                                for (int i = 0; i < dataFilter.size(); i++) {
                                    if (entityParentFields.size() == 1) {
                                        if (dataFilter.get(i).contains(entityParentFields.get(0))) {
                                            dbListconditions.add(dataFilter.get(i));
                                            break;
                                        }
                                    } else if (entityParentFields.size() == 2) {
                                        if (dataFilter.get(i).contains(entityParentFields.get(0))) {
                                            dbListconditions.add(dataFilter.get(i));
                                            break;
                                        }
                                        if (dataFilter.get(i).contains(entityParentFields.get(1))) {
                                            dbListconditions.add(dataFilter.get(i));
                                            break;
                                        }
                                    } else {
                                        logger.warn("More Than 2 Parents Case Not Supported");
                                    }
                                }
                            } else {
                                // No Parents found for the actOnEntity
                                logger.warn("Case Invalid, headerIsParent While Not Parent Found For Entity");
                            }
                        }
                    } else {
                        logger.warn("The Current Entity does not have Parent Entity");
                    }

                    if (dbListconditions.isEmpty()) {
                        logger.warn("The Current Entity could not have parent sequence initialization");
                    }
                }

                try {
                    dbList = oem.loadEntityList(getOactOnEntity().getEntityClassName(), dbListconditions, null, Collections.singletonList(attrInit.getFieldExpression()), loggedUser);
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }
                Object latestValue = null;

                if (dbList != null && !dbList.isEmpty()) {
                    try {
                        latestValue = dbList.get(dbList.size() - 1).invokeGetter(attrInit.getFieldExpression());
                    } catch (NoSuchMethodException ex) {
                        logger.error("Exception thrown", ex);
                    }
                }

                if (latestValue == null) {
                    if (expressionFieldInfo.field.getType() == BigDecimal.class) {
                        initVlaue = BigDecimal.valueOf(1);
                    } else if (expressionFieldInfo.field.getType() == BigInteger.class) {
                        initVlaue = BigInteger.valueOf(1);
                    } else {
                        initVlaue = 1;
                    }
                } else if (latestValue instanceof Integer) {
                    initVlaue = Integer.parseInt(latestValue.toString()) + 1;
                } else if (latestValue instanceof Long) {
                    initVlaue = Long.parseLong(latestValue.toString()) + 1;
                } else if (latestValue instanceof Double) {
                    initVlaue = Double.parseDouble(latestValue.toString()) + 1;
                } else if (expressionFieldInfo.field.getType() == BigDecimal.class) {
                    initVlaue = BigDecimal.valueOf(Double.parseDouble(latestValue.toString()) + 1);
                } else if (expressionFieldInfo.field.getType() == BigInteger.class) {
                    initVlaue = BigInteger.valueOf(Long.parseLong(latestValue.toString()) + 1);
                } else {
                    initVlaue = 1;
                }

                if (initVlaue != null) {
                    BaseEntity.setValueInEntity(entity, attrInit.getFieldExpression(), initVlaue);
                }

            }
        }
    }

    public void applyHardCodedInitialization(BaseEntity entity) {
        List<EntityAttributeInit> attributeInits = null;
        try {
            attributeInits = entitySetupService.
                    getObjectDTOEntityAttributeInits(getOactOnEntity(), loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        for (EntityAttributeInit attrInit : attributeInits) {

            if (attrInit.isInActive()) {
                continue;
            }

            if (attrInit.getFieldExpression() == null) {
                logger.warn("Field Expression is Null");
                continue;
            }

            if (attrInit.getHardCodedValue() != null && !attrInit.getHardCodedValue().equals("")) {
                List<ExpressionFieldInfo> expressionFieldInfos = BaseEntity.parseFieldExpression(actOnEntityCls, attrInit.getFieldExpression());
                ExpressionFieldInfo expressionFieldInfo = expressionFieldInfos.get(expressionFieldInfos.size() - 1);

                Object originalValue = null;
                try {
                    originalValue = BaseEntity.getValueFromEntity(entity, attrInit.getFieldExpression());
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }

                if ((originalValue != null
                        && originalValue instanceof BaseEntity
                        && ((BaseEntity) originalValue).getDbid() != 0)
                        || (originalValue instanceof String
                        && !originalValue.toString().equals(""))
                        || (originalValue instanceof Integer
                        && Integer.parseInt(originalValue.toString()) != 0)
                        || (originalValue instanceof Double
                        && Double.parseDouble(originalValue.toString()) != 0)
                        || (originalValue instanceof Long
                        && Long.parseLong(originalValue.toString()) != 0)
                        || (originalValue instanceof BigDecimal
                        && BigDecimal.valueOf(Double.parseDouble(originalValue.toString())) != null
                        && BigDecimal.valueOf(Double.parseDouble(originalValue.toString())) != BigDecimal.valueOf(0))) {
                    logger.trace("The Field Already has value and will not be initialized");
                    continue;
                }

                Object initVlaue = null;

                if (expressionFieldInfo.field.getType() == Integer.class || expressionFieldInfo.field.getType() == int.class) {
                    initVlaue = Integer.parseInt(attrInit.getHardCodedValue());
                } else if (expressionFieldInfo.field.getType() == Long.class || expressionFieldInfo.field.getType() == long.class) {
                    initVlaue = Long.parseLong(attrInit.getHardCodedValue());
                } else if (expressionFieldInfo.field.getType() == Double.class || expressionFieldInfo.field.getType() == double.class) {
                    initVlaue = Double.parseDouble(attrInit.getHardCodedValue());
                } else if (expressionFieldInfo.field.getType() == Short.class || expressionFieldInfo.field.getType() == short.class) {
                    initVlaue = Short.parseShort(attrInit.getHardCodedValue());
                } else if (expressionFieldInfo.field.getType() == Boolean.class || expressionFieldInfo.field.getType() == boolean.class) {
                    initVlaue = Boolean.parseBoolean(attrInit.getHardCodedValue());
                } else if (expressionFieldInfo.field.getType() == BigDecimal.class) {
                    initVlaue = BigDecimal.valueOf(Double.parseDouble(attrInit.getHardCodedValue()));
                } else if (expressionFieldInfo.field.getType() == Date.class) {
                    try {
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        initVlaue = dateFormat.parse(attrInit.getHardCodedValue());
                    } catch (ParseException ex) {
                        logger.trace("The Initialization Value is not a valid Date format");
                        logger.error("Exception thrown", ex);
                    }
                } else {
                    initVlaue = attrInit.getHardCodedValue();
                }

                if (initVlaue != null) {
                    BaseEntity.setValueInEntity(entity, attrInit.getFieldExpression(), initVlaue);
                }
            }
        }
    }

    public void applyDepencyInitialization(BaseEntity entity, Object newValue, String expression) {

        if (newValue == null || expression == null || expression.equals("")) {
            return;
        }

        //Lookup OR DropDown OR DT initializtion
        if (newValue instanceof BaseEntity) {
            List<ExpressionFieldInfo> infos = BaseEntity.parseFieldExpression(actOnEntityCls, expression);
            if (!(infos.get(infos.size() - 1)).field.getType().isInstance(newValue)) {
                expression = getLookupEntityExpression(expression);
            }
        }
        List<EntityAttributeInit> attributeInits = null;
        try {
            attributeInits = entitySetupService.
                    getObjectDTOEntityAttributeInits(getOactOnEntity(), loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        for (EntityAttributeInit attrInit : attributeInits) {

            if (attrInit.isInActive()) {
                continue;
            }

            if (attrInit.getInitializationExpression() != null
                    && attrInit.getInitializationExpression().startsWith(expression)) {
                if (attrInit.getFieldExpression() == null) {
                    logger.warn("Field Expression is Null");
                    continue;
                }
                Object initVlaue = null;
                if (attrInit.getInitializationExpression().equals(expression)) {
                    initVlaue = newValue;
                } else {
                    String tmpExpression = attrInit.getInitializationExpression().replace(expression + ".", "");
                    try {
                        initVlaue = BaseEntity.getValueFromEntity(newValue, tmpExpression);
                    } catch (Exception ex) {
                        logger.error("Exception thrown", ex);
                    }
                }

                if (initVlaue != null) {
                    List<ExpressionFieldInfo> infos = BaseEntity.parseFieldExpression(actOnEntityCls,
                            attrInit.getFieldExpression());
                    Field field = (null == infos || infos.isEmpty()) ? null : infos.get(0).field;
                    if (field.getAnnotation(Translation.class) != null) {
                        BaseEntity.setValueInEntity(entity, field.getAnnotation(Translation.class).originalField(), initVlaue);
                    }
                    try {
                        if (initVlaue instanceof BaseEntity) {
                            textTranslationServiceRemote.loadEntityTranslation((BaseEntity) initVlaue, loggedUser);
                        }
                    } catch (Exception ex) {
                        logger.error("Exception thrown", ex);
                    }
                    BaseEntity.setValueInEntity(entity, attrInit.getFieldExpression(), initVlaue);
                }
            }
        }
    }

    public int getLookupFieldIndex(String fieldExpression) {
        List<ExpressionFieldInfo> infos = BaseEntity.parseFieldExpression(actOnEntityCls, fieldExpression);
        return uiFrameWorkFacade.getBalloonFieldIndex(actOnEntityCls, infos);
    }

    public String getLookupEntityExpression(String fieldExpression) {
        int index = getLookupFieldIndex(fieldExpression);
        List<ExpressionFieldInfo> infos = BaseEntity.parseFieldExpression(actOnEntityCls, fieldExpression);

        String fldExpression = "";
        for (int fieldIndex = 0; fieldIndex <= index; fieldIndex++) {
            if (fieldIndex != 0) {
                fldExpression += ".";
            }
            fldExpression += infos.get(fieldIndex).fieldName;
        }
        return fldExpression;
    }

    public BaseEntity getLookupBaseEntity(String fieldExpression, String dbid, List<String> flds) {
        try {
            logger.debug("Entering");
            Class baseEntityClass = null;
            if (getLookupFieldIndex(fieldExpression) == -1) {
                baseEntityClass = Class.forName(getOactOnEntity().getEntityClassPath());
            } else {
                String fldExpression = getLookupEntityExpression(fieldExpression);
                List<ExpressionFieldInfo> expressionFieldInfos = BaseEntity.parseFieldExpression(actOnEntityCls, fldExpression);
                ExpressionFieldInfo expressionFieldInfo = expressionFieldInfos.get(expressionFieldInfos.size() - 1);
                baseEntityClass = expressionFieldInfo.field.getType();
            }

            if (dbid != null && !dbid.equals("") && !dbid.equals("0")) {
                String entityName = baseEntityClass.getSimpleName();
                return (BaseEntity) oem.loadEntity(entityName, Long.parseLong(dbid), null, flds, loggedUser);
            } else {
                logger.debug("Returning");
                return (BaseEntity) baseEntityClass.newInstance();
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    public String getLookupFldExpression(String originalFldExpression) {

        String entityExp = getLookupEntityExpression(originalFldExpression);

        if (!entityExp.equals(originalFldExpression) && !entityExp.equals("")) {
            return originalFldExpression.replace(entityExp + ".", "");
        } else {
            return originalFldExpression;
        }

    }

    public String getDRDFldExpression(String originalFldExpression) {
        String entityExp = getLookupEntityExpression(originalFldExpression);

        if (!entityExp.equals(originalFldExpression) && entityExp.contains(".")) {
            return originalFldExpression.replace(entityExp.substring(0, entityExp.lastIndexOf(".")) + ".", "");
        } else {
            return originalFldExpression;
        }
    }

    private boolean isFieldNumeric(Class type) {
        return (type == int.class || type == Integer.class
                || type == long.class || type == Long.class
                || type == double.class || type == Double.class
                || type == BigDecimal.class);
    }

    /**
     * Calls {@link OBackBean#getValidScreenInputs()}, and: <br>1. Adds DBID DT
     * Input if not found in OBackBean returned inputs <br>2. Adds Parent DT
     * Input if not found in OBackBean returned inputs, and if entity has parent
     *
     * @return
     */
    @Override
    protected List<ScreenInputDTO> getValidScreenInputs() {
        List<ScreenInputDTO> screenInputs = null;
        try {
            screenInputs = super.getValidScreenInputs();
            if (screenInputs == null) {
                screenInputs = new ArrayList<ScreenInputDTO>();
            }

            // <editor-fold defaultstate="collapsed" desc="Add DBID DT as a natively supported input">
            ScreenInputDTO dbidInput = uiFrameWorkFacade.getDBIDAsScreenInput(screenInputs, true, loggedUser);
            if (dbidInput != null) // Not found in screenInputs and got successfully
            // Support it
            {
                screenInputs.add(dbidInput);
            }
            // </editor-fold>

            // ParentDBID DT is not added as a natively supported input,
            // it's complex for multi-parent support, and is rather covered in
            // processInput
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(
                    usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
        } finally {
        }
        return screenInputs;
    }

    /**
     * Function is overridden to support more complex inputs of parent object(s)
     * (or one of its subclasses) & "this" (or one of its subclasses) It calls {@link OBackBean#initCurrentScreenInput()
     * } and checks the currentScreenInput first
     */
    @Override
    protected void initCurrentScreenInput() {
        logger.trace("Entering");
        super.initCurrentScreenInput();
        if (currentScreenInput != null) // Successfully intialized
        // Just return, no need for additional intialization
        {
            logger.trace("Returning");
            return;
        }

        if (dataMessage == null) {
            logger.trace("Returning as Data Message equals Null");
            return;
        }

        // Not intialized, no matched supported inputs found
        // Try complex ones
        if (dataMessage.getODataType().getName() == null) // DataType has alreadt problem, not able to know about it
        // Leave currentScreenInput null
        {
            logger.trace("Returning");
            return;
        }
        try {
            List<String> parentEntityClassNames
                    = BaseEntity.getParentClassName(getOactOnEntity().getEntityClassPath());
            String parentFieldName = null; // Null means no matching parent found

            if (dataMessage.getODataType().getDbid() == ODataType.DTDBID_PARENTDBID) {
                // Parent_DBID input
                // <editor-fold defaultstate="collapsed" desc="Manage DTDBID_PARENTDBID & return">
                List<String> parentFieldNames = BaseEntity.getParentEntityFieldName(getOactOnEntity().getEntityClassPath());
                if (parentEntityClassNames.size() == 1) {
                    // Only one parent found
                    setCurrentScreenInputToParent(parentFieldNames.get(0) + ".dbid", true);
                    return;

                } else if (parentEntityClassNames.size() == 2) {
                    // Two parents found
                    // Decide the type of the passed parent
                    OFunctionResult parentOFR = uiFrameWorkFacade.
                            getParentFromParentDBIDDTO(
                                    getOactOnEntity(), (Long) dataMessage.getData().get(0), loggedUser);
                    if (!parentOFR.getErrors().isEmpty()) {
                        messagesPanel.addMessages(parentOFR);
                        logger.trace("Returning");
                        return;
                    }
                    if (parentOFR.getReturnValues().isEmpty()) {
                        // No parent found matches the DBID
                        logger.warn("Parent DBID Not Found In Any Of The Parents Tables");
                        // FIXME: meaningful error message
                        messagesPanel.addErrorMessage(
                                usrMsgService.getUserMessage("SystemInternalError", systemUser));
                        logger.trace("Returning");
                        return;
                    }
                    setCurrentScreenInputToParent(parentOFR.getReturnValues().get(1) //parent field name
                            + ".dbid", false);
                    logger.trace("Returning");
                    return;    // No further processing needed

                } else {
                    // Error case not supported
                    logger.warn("More Than Two Parents Not Supported");
                    // FIXME: error user message
                }
                // </editor-fold>
                logger.trace("Returning");
                return;
            }
            // Not Parent_DBID input

            OEntityDTO sentEntityOEntity = null;
            sentEntityOEntity = entitySetupService.loadOEntityDTOByClassName(
                    dataMessage.getODataType().getName(), loggedUser);

            boolean child = false;  // True if one of the sentEntityOEntity super
            // classes is of the screen oactOnEntity
            boolean childOfParent = false;  // True if one of the oactOnEntity
            // parent classes is equal to one of the sentEntityOEntity super
            // classes
            int childOfParentIndex = -1;    // Index of the parent found for
            // childOfParent in the parent list. Valid only if childOfParent
            // is true
            // <editor-fold defaultstate="collapsed" desc="Fill child, childOfParents for super classes">
            if (sentEntityOEntity != null) {
                List<Class> superClasses = BaseEntity.getSuperClasses(
                        Class.forName(sentEntityOEntity.getEntityClassPath()));
                for (int i = 0; i < superClasses.size(); i++) {
                    if (superClasses.get(i).getSimpleName().equalsIgnoreCase(getOactOnEntity().getEntityClassName())) {
                        child = true;
                        break;
                    } else {
                        for (int iName = 0; iName < parentEntityClassNames.size(); iName++) {
                            String parentEntityClassName = parentEntityClassNames.get(iName);
                            if (superClasses.get(i).getSimpleName().equalsIgnoreCase(parentEntityClassName)) {
                                childOfParent = true;
                                childOfParentIndex = iName;
                                break;
                            }
                        }
                        if (childOfParent) {
                            break;
                        }
                    }
                }
            }
            // </editor-fold>

            if (dataMessage.getODataType().getName().equalsIgnoreCase(
                    getOactOnEntity().getEntityClassName())
                    || child) {
                // <editor-fold defaultstate="collapsed" desc="Initialize for "this" passed object or one of its subclassess objects, return">
                currentScreenInput = new ScreenInputDTO();
                ScreenDTMappingAttrDTO dTMapping = new ScreenDTMappingAttrDTO();
                dTMapping.setFieldExpression("this");
                if (dataMessage.getODataType().getODataTypeAttribute() != null
                        && dataMessage.getODataType().getODataTypeAttribute().size() > 0) {
                    List<ODataTypeAttribute> attrs = dataMessage.getODataType().getODataTypeAttribute();
                    ODataTypeAttribute first_attr = attrs.get(0);
                    int last_seq = first_attr.getSeq();
                    for (int i = 0; i < attrs.size(); i++) {
                        int seq = attrs.get(i).getSeq();
                        if (seq < last_seq) {
                            first_attr = attrs.get(i);
                            last_seq = seq;
                        }
                    }
                    dTMapping.setOdataTypeAttributeDBID(first_attr.getDbid());
                }
                currentScreenInput.getAttributesMapping().add(dTMapping);
                logger.trace("Returning");
                return; // currentScreenInput initialized
                // </editor-fold>
            }

            // Initialize for parent object & parent subclass object passed
            // <editor-fold defaultstate="collapsed" desc="Filll parentFieldName with corresponding one if found">
            List<String> parentFieldNames = BaseEntity.getParentEntityFieldName(getOactOnEntity().getEntityClassPath());
            if (childOfParent) {
                parentFieldName = parentFieldNames.get(childOfParentIndex);    // Assuming
                // index of parentFieldNames is consistent with index of parentClassNames
            } else if (parentEntityClassNames != null && !parentEntityClassNames.isEmpty()) {
                for (int iName = 0; iName < parentEntityClassNames.size(); iName++) {
                    String parentEntityClassName = parentEntityClassNames.get(iName);
                    if (dataMessage.getODataType().getName().equalsIgnoreCase(parentEntityClassName)) {
                        parentFieldName = parentFieldNames.get(iName); // Assuming
                        // index of parentFieldNames is consistent with index of parentClassNames
                    }
                }
            }
            // </editor-fold>

            if (parentFieldName != null) {
                setCurrentScreenInputToParent(parentFieldName, true);
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
    }

    protected void setCurrentScreenInputToParent(String parentFieldName, boolean is4ParentObject) {
        logger.trace("Entering");
        currentScreenInput = new ScreenInputDTO();
        ScreenDTMappingAttrDTO dTMapping = new ScreenDTMappingAttrDTO();
        dTMapping.setFieldExpression(parentFieldName);
        dTMapping.setForFilter(true);
        dTMapping.setForInitialization(is4ParentObject);
        // setting it for parent DBID causes error.
        // Don't do this except for parent object, generates error in setValueInEntity()
        if (dataMessage.getODataType().getODataTypeAttribute() != null
                && dataMessage.getODataType().getODataTypeAttribute().size() > 0) {
            //FIXME: !!
            dTMapping.setOdataTypeAttributeDBID(dataMessage.getODataType().getODataTypeAttribute().get(0).getDbid());
        }
        currentScreenInput.getAttributesMapping().add(dTMapping);
        logger.trace("Returning");
        return;    // currentScreenInput initialized
    }

    @Override
    protected OFunctionResult entityValueIntializationListener(
            BaseEntity entity, String fieldExpression, Object value) {
        return super.entityValueIntializationListener(entity, fieldExpression, value);
    }
    /**
     * Is set in {@link SingleEntityBean#processInputForHeader
     * () }. Indicates if {@link OBackBean#headerObject} is parent of the
     * actOnEntity
     */
    protected boolean headerObjectIsParent = false;

    /**
     * Sets
     * {@link OBackBean#headerObject}, {@link SingleEntityBean#headerObjectIsParent},
     * creates and sets {@link OBackBean#headerPanel}, and sets portlet title.
     *
     * Preconditions: <br>- of {@link UIFrameworkService#getEntityParentFromODMInput(ODataMessage, ScreenInput, SingleEntityScreen, OUser)
     * }
     * with passed values of {@link OBackBean#dataMessage}, {@link OBackBean#currentScreenInput},
     * {@link OBackBean#currentScreen} <br> - {@link OBackBean#innerScreenPanel}
     * is valid
     *
     * @return true: <br>1. {@link OScreen#showHeader} is false, doesn't create
     * any thing
     */
    protected boolean processInputForHeader() {
        logger.trace("Entering");
        try {
            if (processMode) {
                //String screenInProcessTitle = taskInfo.getAeTTask().getName();
                String screenInProcessTitle = processingTask.getName();
                String regex = "([a-z])([A-Z])";
                String replacement = "$1 $2";
                screenInProcessTitle = screenInProcessTitle.replaceAll(regex, replacement);
                onloadJSStr
                        += "top.FABS.Portlet.setTitle({portletId:'" + getPortletInsId()
                        + "', title:'" + screenInProcessTitle.replace("\\", "\\\\").replace("\'", "\\'") + "'});";
            }
            // <editor-fold defaultstate="collapsed" desc="Set headerObject, headerObjectIsParent">
            OFunctionResult oFR = uiFrameWorkFacade.getEntityParentFromODMInput(
                    dataMessage, currentScreenInput, getOactOnEntity(), loggedUser);
            if (!oFR.getErrors().isEmpty()) {
                messagesPanel.addMessages(oFR);
                logger.trace("Returning");
                return false;
            }
            if (oFR.getReturnValues() != null && !oFR.getReturnValues().isEmpty() && oFR.getReturnValues().get(0) != null) {
                // Always set header to passed parent input regardless of
                // compositeInputRequired value
                setHeaderObject((BaseEntity) oFR.getReturnValues().get(0));
                headerObjectIsParent = true;
            } else // ActOnEntity has NO parent OR parent Object is NOT passed in dataMessage
            {
                if (compositeInputRequired) // Composite input is required
                {
                    // Accept only parent header for headerObject, keep last header unchanged
                    // even if it's null
                    if (headerObject == null) {
                        // Assuming compositeInputs is filled by the already passed
                        // inputs, search for a parent in it and use it. This happens
                        // in case all the parent portlet & senders are opened, then
                        // our screen is opened with a dataMessage of non-parent input
                        for (OScreenCompositeInput compInput : compositeInputs) {
                            if (compInput.odm == null || compInput.input == null) {
                                continue;
                            }
                            ScreenInputDTO screenInputDTO = uiFrameWorkFacade.getScreenInputDTO(compInput.input.getDbid(), loggedUser);
                            OFunctionResult parentOFR = uiFrameWorkFacade.getEntityParentFromODMInput(
                                    compInput.odm, screenInputDTO,
                                    getOactOnEntity(), loggedUser);
                            if (parentOFR.getReturnValues() != null
                                    && !parentOFR.getReturnValues().isEmpty()) // Parent object found
                            {
                                // Set the header object to parent
                                setHeaderObject((BaseEntity) parentOFR.getReturnValues().get(0));
                                headerObjectIsParent = true;
                                logger.trace("Header Object (Parent) Is Got from CompositeInputs with current Screen: {}, header Object: {}", currentScreen, headerObject);
                            }
                        }
                    }
                } else // Composite input is NOT required
                {
                    if (dataMessage != null
                            && dataMessage.dataIsBaseEntity()) // Passed object is BaseEntity
                    {
                        headerObject = (BaseEntity) dataMessage.getData().get(0);
                    }
                }
            }
            // </editor-fold>

            OEntityDTO headerObjectOEntity = null;
            if (headerObject != null) {
                headerObjectOEntity = entitySetupService.
                        loadOEntityDTO(headerObject.getClassName(), systemUser);
            }

            // <editor-fold defaultstate="collapsed" desc="Create headerPanel">
            if (getCurrentScreen().isShowHeader()) {
                // Need to make suer header panel is created
                if (headerPanelLayout == null) {
                    // Not already created
                    // Create it
                    createHeaderPanelLayout(headerObjectOEntity);
                    headerPanelLayout.setId(getFacesContext().getViewRoot().createUniqueId() + "HeaderPanel");
                    if (headerPanelLayout != null) {
                        if (null == htmlPanelGrid) {
                            htmlPanelGrid = new HtmlPanelGrid();
                        }
                        htmlPanelGrid.getChildren().add(0, headerPanelLayout);
                    }
                }
                if (getScreenHeight() == null || getScreenHeight().equals("")) {
                    onloadJSStr
                            += "top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');";
                } else {
                    onloadJSStr
                            += "top.FABS.Portlet.updateIframeSizeWithHeight({iframeName:'" + getPortletInsId() + "Frame'"
                            + ",screenHeight:'" + getScreenHeight() + "'});";
                }
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Change portlet title if required">
            if (headerObject != null
                    && !getCurrentScreen().isShowHeader()) // There is a valid header object with no show header required
            {
                // display it in the portlet title
                List<OObjectBriefInfoField> briefInfoFields = entitySetupService.
                        getObjectDTOBriefInfoFields(
                                headerObjectOEntity, oem.getSystemUser(loggedUser));
                int briefInfoFieldsCount = briefInfoFields == null ? 0 : briefInfoFields.size();
                masterField = null;
                for (int i = 0; i < briefInfoFieldsCount; i++) {
                    if (briefInfoFields.get(i).isMasterField()) {
                        masterField = briefInfoFields.get(i);
                        break;
                    }
                }
                if (masterField != null) {
                    try {
                        currentScreen = (OScreen) textTranslationServiceRemote.loadEntityTranslation(currentScreen, loggedUser);
                    } catch (Exception ex) {
                        logger.error("Exception thrown", ex);
                    }
                    String fldExpr = masterField.getFieldExpression();
                    String portletTitle = (String) headerObject.invokeGetter(fldExpr);
                    if (portletTitle != null) {
                        portletTitle = getCurrentScreen().getHeaderTranslated() + " - " + portletTitle;
                        onloadJSStr
                                += "top.FABS.Portlet.setTitle({portletId:'" + getPortletInsId()
                                + "', title:'" + portletTitle.replace("\\", "\\\\").replace("\'", "\\'") + "'});";
                        logger.trace("Calling JS setTitle");
                    }
                } else {
                    logger.warn("Entity: {} must have master brief info field", headerObjectOEntity);
                }
            }

            if (headerObject == null) {
                String portletTitle = "";
                if (compositeInputRequired) {
                    List<String> titles = new ArrayList<String>();
                    for (OScreenCompositeInput compInput : compositeInputs) {
                        if (compInput.odm == null || compInput.input == null) {
                            continue;
                        }
                        int odmDataSize = compInput.odm.getData().size();
                        if (0 != odmDataSize
                                && !compInput.odm.getData().get(odmDataSize - 1).
                                toString().equalsIgnoreCase("default")) {
                            titles.add(compInput.odm.getData().get(odmDataSize - 1).toString());
                        }
                    }
                    Collections.sort(titles);
                    for (String title : titles) {
                        portletTitle = portletTitle + " - " + title;
                    }
                } else {
                    int odmDataSize = dataMessage.getData().size();
                    if (0 != odmDataSize
                            && !dataMessage.getData().get(odmDataSize - 1).
                            toString().equalsIgnoreCase("default")
                            && dataMessage.getODataType().getDbid() != ODataType.DTDBID_DBID) {
                        portletTitle = portletTitle + " - " + dataMessage.getData().get(odmDataSize - 1).toString();
                    }
                }
                try {
                    currentScreen = (OScreen) textTranslationServiceRemote.loadEntityTranslation(getCurrentScreen(),
                            loggedUser);
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }
                onloadJSStr
                        += "top.FABS.Portlet.setTitle({portletId:'" + getPortletInsId()
                        + "', title:'" + (getCurrentScreen().getHeaderTranslated() + portletTitle).replace("\\", "\\\\").replace("\'", "\\'") + "'});";
                logger.trace("CAlling JS setTitle");
            }
            // </editor-fold>
            logger.trace("Returning with True");
            return true;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(
                    usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
            logger.trace("Returning with False");
            return false;
        } finally {
        }
    }
    boolean hasSecuirtyTags;

    public boolean isHasSecuirtyTags() {
        return hasSecuirtyTags;
    }

    public void setHasSecuirtyTags(boolean hasSecuirtyTags) {
        this.hasSecuirtyTags = hasSecuirtyTags;
    }
    String securityTagMsg;

    public String getSecurityTagMsg() {
        return securityTagMsg;
    }

    public void setSecurityTagMsg(String securityTagMsg) {
        this.securityTagMsg = securityTagMsg;
    }

    @Override
    protected boolean onInit(Map<String, String> requestParams) {
        logger.trace("Entering");
        try {
            if (!super.onInit(requestParams)) {
                return false;
            }

            if (getOactOnEntity() != null) {
                if (!(currentScreen instanceof MultiLevelScreen)) //FIXME: subclass reference, shouldn't be here
                {
                    try {
                        actOnEntityCls = Class.forName(
                                getOactOnEntity().getEntityClassPath());
                    } catch (Exception ex) {
                        logger.error("Exception thrown", ex);
                    }
                }
            } else {
                // currentScreen.getOactOnEntity() = null !
                logger.warn("Null ActOnEntity");
            }

            showSaveExitButton = showSaveExitButton
                    && ((SingleEntityScreen) getCurrentScreen()).isSaveAndExit();
            showSaveButton = ((SingleEntityScreen) getCurrentScreen()).isSave();
            logger.trace("Returning with True");
            return true;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Returning with True");
            return false;
        }
    }

    /**
     * Returns ODataMessage of the {@link #getSelectedEntity()} first element in
     * the {@link ODataMessage#data}; and {@link #getActOnEntityDataType() }
     * in {@link ODataMessage#oDataType}. Uses {@link #constructOutput(ODataType, BaseEntity)
     * } to do that. Accepts null selected entity.
     *
     * @return null: in case of error
     */
    protected ODataMessage constructSelectedEntityODM() {
        logger.trace("Entering");
        try {
            ODataMessage oDM = null;
            ODataType entityDataType = getActOnEntityDataType();
            if (getLoadedEntity().size() > 0 // One or more entities are loaded
                    && selectedEntities.length != 0 // one or more entities are selected
                    ) {
                // Screen has records

                // Construct output for it
                OFunctionResult outputFR = constructOutput(entityDataType, (BaseEntity) selectedEntities[0]);
                if (!outputFR.getErrors().isEmpty()) {
                    messagesPanel.addMessages(outputFR);
                } else {
                    oDM = outputFR.getReturnedDataMessage();

                    if (oDM == null) {
                        logger.trace("Screen Doesn't Support ParentDBID, No Render Is Done Upon Selection Change");
                    }
                }
            } else {
                // Screen has no records or has records but no one is selected
                // Construct output for null entity
                oDM = new ODataMessage();
                oDM.setODataType(entityDataType);
                oDM.setData(new ArrayList<Object>());
            }
            logger.trace("Returning");
            return oDM;

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Returning with Null");
            return null;
        }
    }

    protected List<BaseEntity> getAllEntities() {
        return getLoadedEntity();
    }

    /**
     * @deprecated use {@link #getLoadedEntity() } instead
     */
    public List<BaseEntity> getActiveEntities() {
        return getLoadedEntity();
    }

    /**
     * @deprecated use {@link #getLoadedEntity() }
     */
    public List<BaseEntity> getDisplayedEntities() {
        return getLoadedEntity();
    }

    /**
     * Returns {@link OFunctionResult} returned from either: <br>1. {@link EntitySetupServiceRemote#executeAction(OEntityAction, ODataMessage, OFunctionParms, OUser)
     * }
     * if action is {@link OEntityAction#ATDBID_ParentPreSave} <br>2. {@link OFunctionServiceRemote#runFunction(OFunction, ODataMessage, OFunctionParms, OUser)
     * }
     * if action is {@link OEntityAction#ATDBID_DELETE} and
     * {@link OEntityAction#parentValidation} has value <br>Passing BaseEntity
     * list got from {@link #constructOutput(ODataType, BaseEntity) }, and List
     * of ActOnEntity as datatype
     *
     * @param action
     * @return empty OFunctionResult if nothing happened, otherwise, messages,
     * <br>No error returned if: action = null, delete action has no parent
     * validation, entity has no parent, action is not supported
     */
    protected OFunctionResult runParentValidation(OEntityAction action) {
        logger.trace("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            if (action == null) {
                // No action to run
                logger.trace("Returning");
                return oFR;
            }
            // Make sure OEntity has parent
            List<String> entityParents = getOactOnEntity().getEntityParentClassPath();
            if (entityParents == null || entityParents.isEmpty()) {
                // Entity has no parent
                // Nothing to do
                logger.warn("No Parent But Continue Validation");
            }
            ODataType presaveDataType = dataTypeService.loadODataType(getOactOnEntity().getEntityClassName()
                    + "List", systemUser);
            // As List DT is not always exist - added by A El-Zaher
            if (presaveDataType == null) {
                if (getOactOnEntity() != null) {
                    logger.warn("DataType: {} List do not exist", getOactOnEntity().getEntityClassName());
                }
                logger.trace("Returning");
                return oFR;
            }
            // new BaseEntity is passed because the data type maps lodedList and
            // parent and thye two object are not related to any object but to screen
            ODataMessage presaveDataMessage = (ODataMessage) constructOutput(presaveDataType,
                    new BaseEntity()).getReturnedDataMessage();
            if (action.getActionType().getDbid() == OEntityAction.ATDBID_DELETE) {
                OFunction validation = action.getParentValidation();
                if (validation == null) {
                    logger.trace("Returning");
                    return oFR;
                }
                oFR.append(functionServiceRemote.runFunction(
                        validation, presaveDataMessage, new OFunctionParms(), getLoggedUser()));
            } else if (action.getActionType().getDbid() == OEntityAction.ATDBID_ParentPreSave) {
                oFR.append(entitySetupService.executeAction(
                        action, presaveDataMessage, new OFunctionParms(), getLoggedUser()));
            } else {
                logger.warn("Function Called For Unsupported Action");
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.trace("Returning");
            return oFR;
        }
    }

    protected OFunctionResult runParentValidation(OEntityActionDTO action) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            if (action == null) {
                // No action to run
                return oFR;
            }
            // Make sure OEntity has parent
            List<String> entityParents = getOactOnEntity().getEntityParentClassPath();
            if (entityParents == null || entityParents.isEmpty()) {
                // Entity has no parent
                // Nothing to do
                logger.warn("No Parent But Continue Validation");
            }
            ODataType presaveDataType = dataTypeService.loadODataType(getOactOnEntity().getEntityClassName()
                    + "List", systemUser);
            // As List DT is not always exist - added by A El-Zaher
            if (presaveDataType == null) {
                if (getOactOnEntity() != null) {
                    logger.warn("DataType: {} List do not exist", getOactOnEntity().getEntityClassName());
                }
                logger.trace("Returning");
                return oFR;
            }
            // new BaseEntity is passed because the data type maps lodedList and
            // parent and thye two object are not related to any object but to screen
            ODataMessage presaveDataMessage = (ODataMessage) constructOutput(presaveDataType,
                    new BaseEntity()).getReturnedDataMessage();
            if (action.getActionTypeDBID() == OEntityActionDTO.ATDBID_DELETE) {
                OFunction validation = functionServiceRemote.getOFunction(action.getParentValidationDBID(), loggedUser);
                if (validation == null) {
                    logger.trace("Returning");
                    return oFR;
                }
                oFR.append(functionServiceRemote.runFunction(
                        validation, presaveDataMessage, new OFunctionParms(), getLoggedUser()));
            } else if (action.getActionTypeDBID() == OEntityActionDTO.ATDBID_ParentPreSave) {
                oFR.append(entitySetupService.executeAction(
                        action, presaveDataMessage, new OFunctionParms(), getLoggedUser()));
            } else {
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.trace("Returning");
            return oFR;
        }
    }

    /**
     * Render {@link #toBeRenderScreens} in case of {@link ScreenAction#render}
     * is found true for this screen of the function related EntityAction
     *
     * @param ae
     * @param ofunction
     * @param entity
     * @param ofrRunFunctionResult
     */
    @Override
    protected void postRunBalloonFunction(ActionEvent ae, OFunction ofunction,
            BaseEntity entity, OFunctionResult ofrRunFunctionResult) {
        logger.debug("Entering");
        try {
            if (!ofrRunFunctionResult.getErrors().isEmpty()) {
                logger.debug("Returning");
                return;
            }
            if (toBeRenderScreens == null || toBeRenderScreens.isEmpty()) {
                logger.debug("Returning");
                return;
            }

            //FIXME: case not supported: two OEntity actions are of the same function
            ArrayList<String> conds = new ArrayList<String>();
            conds.add("oscreen.dbid=" + currentScreen.getDbid());
            conds.add("action.ofunction.dbid=" + ofunction.getDbid());
            conds.add("render=true");
            // Get the corresponding action and check if it has ScreenAction record
            ScreenAction screenAction = (ScreenAction) oem.loadEntity(ScreenAction.class.getSimpleName(),
                    conds, null, systemUser);
            if (screenAction == null) {
                logger.debug("Returning");
                return;
            }

            if (screenAction.isRender() && c2AMode) {
                ODataMessage oDM = null;
                ODataType entityDataType = getActOnEntityDataType();
                OFunctionResult outputFR = constructOutput(entityDataType, entity);
                if (!outputFR.getErrors().isEmpty()) {
                    messagesPanel.addMessages(outputFR);
                } else {
                    oDM = outputFR.getReturnedDataMessage();
                    if (oDM == null) {
                        logger.warn("Null OData Message from constructOutput");
                        logger.debug("Returning");
                        return;
                    }
                }
                for (ScreenRenderInfo screenRenderInfo : toBeRenderScreens) {
                    OPortalUtil.renderScreen(currentScreen, screenRenderInfo.screenInstId, oDM);
                }
            }
            logger.debug("Returning");

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(
                    usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
            logger.debug("Returning");

        } finally {
        }
    }

    public HtmlPanelGroup buildLookupBtnPanel(String returnMethodExpression) {
        HtmlPanelGroup buttonsPanel = new HtmlPanelGroup();
        buttonsPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                + "_buttonsPanel");
        //Return Selected Button
        CommandButton returnSelectedButton = new CommandButton();
        String ddReturnValue = ddService.getDDLabel("returnSelectedButton", loggedUser);
        returnSelectedButton.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                + "returnSelectedBTN");
        returnSelectedButton.setValue(ddReturnValue);
        returnSelectedButton.setImmediate(false);
        returnSelectedButton.setActionExpression(exFactory.createMethodExpression(
                FacesContext.getCurrentInstance().getELContext(), "#{" + getBeanName()
                + "." + returnMethodExpression + "}", String.class, new Class[0]));
        returnSelectedButton.setStyleClass("pick_btn");
        returnSelectedButton.setIcon("pick_btn_icon");
        returnSelectedButton.setAlt(ddReturnValue);
        returnSelectedButton.setTitle(ddReturnValue);
        buttonsPanel.getChildren().add(returnSelectedButton);

        //Return Empty Button
        CommandButton returnEmptyButton = new CommandButton();
        String ddReturnEmptyValue = ddService.getDDLabel("returnEmptyButton", loggedUser);
        returnEmptyButton.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "returnEmptyBTN");
        returnEmptyButton.setValue(ddReturnEmptyValue);
        returnEmptyButton.setAlt(ddReturnEmptyValue);
        returnEmptyButton.setTitle(ddReturnEmptyValue);
        returnEmptyButton.setPartialSubmit(true);
        returnEmptyButton.setImmediate(true);
        returnEmptyButton.setActionExpression(exFactory.createMethodExpression(
                FacesContext.getCurrentInstance().getELContext(), "#{" + getBeanName()
                + ".setEmptyLookupData}", String.class, new Class[0]));
        returnEmptyButton.setStyleClass("reset_btn");
        returnEmptyButton.setIcon("reset_btn_icon");
        buttonsPanel.getChildren().add(returnEmptyButton);

        return buttonsPanel;
    }

    @Override
    public void routingTaskButtonAction(ActionEvent event) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            logger.debug("Entering");
            //TODO : get id of function from button id as will be created during button creation
            String actionID = null;
            String btnID = ((CommandButton) event.getComponent()).getId();
            actionID = btnID.substring(btnID.lastIndexOf("_") + 1);

            ODataMessage oDM = new ODataMessage();
            ODataType odataType = dataTypeService.loadODataType("OFunction", systemUser);
            oDM.setODataType(odataType);
            List data = new ArrayList();
            data.add(actionID);
            data.add(routingTaskInstID);
            data.add(routingTaskComment);
            data.add(loggedUser);
            oDM.setData(data);
            oFR.append(routingService.runTaskAction(oDM, null, loggedUser));
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        messagesPanel.clearAndDisplayMessages(oFR);
        if (exit) {
            if (openedAsPopup) {
                logger.trace("Calling JS closePopupNoID with PortletInsId: {}", getPortletInsId());
                RequestContext.getCurrentInstance().execute("top.FABS.Portlet.closePopupNoID('" + getPortletInsId() + "');");

            } else {
                OPortalUtil.removePortlet(getPortletInsId(), screenInstId);
            }
        }
    }

    /**
     * @return the image
     */
    public byte[] getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(byte[] image) {
        this.image = image;
    }

    /**
     * @return the graphicImage
     */
    public GraphicImage getGraphicImage() {
        return graphicImage;
    }

    /**
     * @param graphicImage the graphicImage to set
     */
    public void setGraphicImage(GraphicImage graphicImage) {
        this.graphicImage = graphicImage;
    }

    public long getLookupMainScrInstID(String lookupScrName) {
        HashMap<String, Long> lookupMainScrInstID = (HashMap<String, Long>) SessionManager.getSessionAttribute("lookupMainScreenInstID");
        return (lookupMainScrInstID == null || lookupMainScrInstID.get(lookupScrName) == null) ? 0 : lookupMainScrInstID.get(lookupScrName);
    }

    public void setLookupMainScrInstID(String lookupScrName, long mainScrInstID) {
        HashMap<String, Long> lookupMainScrInstID = (HashMap<String, Long>) SessionManager.getSessionAttribute("lookupMainScreenInstID");
        if (lookupMainScrInstID == null) {
            lookupMainScrInstID = new HashMap<>();
        }
        lookupMainScrInstID.put(lookupScrName, mainScrInstID);
        SessionManager.addSessionAttribute("lookupMainScreenInstID", lookupMainScrInstID);
    }

    public void validateHTML(FacesContext context, UIComponent validate, Object valueObj) {
        if (valueObj != null) {
            String valueStr = valueObj.toString();
            if (valueStr.contains("<") || valueStr.contains(">")
                    || valueStr.contains("&lt") || valueStr.contains("&gt")
                    || valueStr.contains("&amp") || valueStr.contains("&")) {
                ((InputText) validate).setValid(false);
                HtmlMessage message = new HtmlMessage();
                UserMessage userMessage = usrMsgService.getUserMessage("HtmlNotAllowed", loggedUser);
                message.setTitle(userMessage.getMessageTitleTranslated());
                message.setStyleClass("inlineErrorMsgTitle");
                message.setFor(validate.getClientId(context));

                FacesMessage msg = new FacesMessage(userMessage.getMessageTextTranslated());
//                dataTable.processUpdates(context);
                context.addMessage(validate.getClientId(), msg);
            }
        }
    }
}
