package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.content.AttachmentDTO;
import com.unitedofoq.fabs.core.content.EntityAttachmentServiceLocal;
import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.filter.OFilter;
import com.unitedofoq.fabs.core.function.JavaFunction;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.function.OMenu;
import com.unitedofoq.fabs.core.function.OMenuFunction;
import com.unitedofoq.fabs.core.function.ServerJob;
import com.unitedofoq.fabs.core.function.URLFunction;
import com.unitedofoq.fabs.core.function.WebServiceFunction;
import com.unitedofoq.fabs.core.i18n.TextTranslationServiceRemote;
import com.unitedofoq.fabs.core.multimedia.OIMageServiceLocal;
import com.unitedofoq.fabs.core.report.ReportFunction;
import com.unitedofoq.fabs.core.report.customReports.CustomReport;
import com.unitedofoq.fabs.core.report.customReports.CustomReportFilter;
import com.unitedofoq.fabs.core.report.customReports.CustRepFunction;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.RoleMenu;
import com.unitedofoq.fabs.core.security.user.RoleUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.security.user.UserServiceRemote;
import com.unitedofoq.fabs.core.setup.FABSSetup;
import com.unitedofoq.fabs.core.udc.UDCServiceRemote;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.backbean.filter.RunFilterBean;
import com.unitedofoq.fabs.core.uiframework.backbean.wizard.WizardController;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.navigationmenu.NavMenuManagerImpl;
import com.unitedofoq.fabs.core.uiframework.orgchart.OrgChartFunction;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenFunction;
import com.unitedofoq.fabs.core.uiframework.screen.UDCScreenFunction;
import com.unitedofoq.fabs.core.uiframework.screen.wizard.Wizard;
import com.unitedofoq.fabs.core.uiframework.screen.wizard.WizardFunction;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.faces.application.Application;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.component.menuitem.MenuItem;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Satef
 */
@ManagedBean(name = "DockMenuBean")
@ViewScoped
public class DockMenuBean extends FormBean {

    final static Logger logger = LoggerFactory.getLogger(DockMenuBean.class);
    protected HtmlPanelGrid menuPanel = new HtmlPanelGrid();

    
    private DataTypeServiceRemote dataTypeService = OEJB.lookup(DataTypeServiceRemote.class);
    
    DataTypeServiceRemote dtService = OEJB.lookup(DataTypeServiceRemote.class);
    
    OIMageServiceLocal imageService = OEJB.lookup(OIMageServiceLocal.class);
    
    UDCServiceRemote uDCService = OEJB.lookup(UDCServiceRemote.class);
    protected OUser loggedUser;
    public ExpressionFactory exFactory;
    public ELContext elContext;

    public DockMenuBean() {
        Application App = FacesContext.getCurrentInstance().getApplication();
        exFactory = App.getExpressionFactory();
        elContext = FacesContext.getCurrentInstance().getELContext();
    }
    // <editor-fold defaultstate="collapsed" desc="test">
    
    private OFunctionServiceRemote functionServiceRemote = OEJB.lookup(OFunctionServiceRemote.class);

    
    protected OEntityManagerRemote oem = OEJB.lookup(OEntityManagerRemote.class);
    
    protected DDServiceRemote ddService = OEJB.lookup(DDServiceRemote.class);
    
    private UserServiceRemote userServiceRemote = OEJB.lookup(UserServiceRemote.class);

    private String layoutPlid = null;
    private String screenInstId;
    private String screenHeight;
    private String screenLanguage;
    private String htmlDir;
    private OUser systemUser = null;
    private UserMessagePanel messagesPanel;
    protected boolean c2AMode;
    protected boolean showManagePortlePage = false;
    private String portletInsId;
    protected String portletId;
    protected boolean portalMode;
    protected boolean openedAsPopup;
    protected boolean processMode;
    protected boolean routingMode;

    public String getPortletInsId() {
        return portletInsId;
    }

    public void setPortletInsId(String portletInsId) {
        this.portletInsId = portletInsId;
    }

    protected boolean onInit(Map<String, String> requestParams) {
        logger.trace("Entering");
        String userId = requestParams.get("UD");
        SessionManager.addSessionAttribute(new String("USERID"), userId);
        if (userId == null || "".equals(userId)) {
            logger.warn("Invalid UD Request Parameter");
            logger.trace("Returning with False");
            return false;
        }
        if (!OPortalUtil.isUserLoggedIn(Long.valueOf(userId))) {
            return false;
        }
        HttpServletRequest request = (HttpServletRequest) SessionManager.getExternalContext().getRequest();
        if (!OPortalUtil.isUserAuthorized(request, userId)) {
            return false;
        }
        layoutPlid = (String) requestParams.get("lPLID");

        String screenName = (String) requestParams.get("screenName");
        screenInstId = ((String) requestParams.get("screenInstId"));
        screenHeight = (String) requestParams.get("screenHeight");

        if (screenInstId == null) {
            logger.trace("Returning with False  as Screen Instance Id equals Null");
            return false;
        }
        String loginName = OPortalUtil.getUserLoginName(Long.parseLong(userId));
        logger.trace("Initializing OBackBean");
        if (loginName == null) {
            logger.trace("Returning with False  as Screen loginName equals Null");
            return false;
        }
        if (!liferayUsers.containsKey(loginName)) {
            liferayUsers.put(loginName, userId);
        }
        try {
            loggedUser = userServiceRemote.getUserfromLoginNameAndTenant(loginName);
            SessionManager.addSessionAttribute("loggedUser", loggedUser);
            if (loggedUser == null) {
                logger.trace("Returning with False");
                return false;
            }
            screenLanguage = loggedUser.getFirstLanguage().getValue();
            setHtmlDir((loggedUser.isRtl()) ? "rtl" : "ltr");

            systemUser = oem.getSystemUser(loggedUser);
            if (systemUser == null) {
                logger.warn("Error Getting Granted User");
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Returning with False");
            return false;
        }
        SessionManager.addSessionAttribute("LoggedUser", loggedUser);
        SessionManager.addSessionAttribute("SystemUser", systemUser);
        SessionManager.addSessionAttribute("userID", loginName);
        SessionManager.addSessionAttribute("layoutPLID", layoutPlid);
        messagesPanel = new UserMessagePanel(exFactory, elContext, getBeanName(), ddService, null, loggedUser);
        SessionManager.addSessionAttribute("MessagePanel", messagesPanel);
        if (requestParams.get("EnableC2A") != null) {
            c2AMode = "true".equalsIgnoreCase(requestParams.get("EnableC2A"));
        }
        if (requestParams.get("main") != null) {
            showManagePortlePage = "true".equalsIgnoreCase(requestParams.get("main"));
        }
        if (requestParams.get("portletId") != null) {
            portletId = requestParams.get("portletId").toString();
        }
        if (requestParams.get("portletInsId") != null) {
            setPortletInsId(requestParams.get("portletInsId").toString());
        }
        if (requestParams.get("portalMode") != null) {
            portalMode = "true".equalsIgnoreCase(requestParams.get("portalMode"));
        }
        if (requestParams.get("isPopup") != null) {
            openedAsPopup = "true".equalsIgnoreCase(requestParams.get("isPopup"));
        }

        if (screenName == null) {
            screenName = portletId;
        }

        try {
            oem.getEM(systemUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }

        if (screenName.equals("DockMenu")) {
            logger.trace("Returning with False");
            return false;
        }
        logger.trace("Returning with True");
        return true;
    }

    public static synchronized String generateScreenInstId(String scrName) {
        logger.debug("Entering");
        String screenInstId = scrName + "_" + OBackBean.SCREENS_COUNTER;
        OBackBean.SCREENS_COUNTER++;
        logger.debug("Returning with Screen Instance Id: {}", screenInstId);
        return screenInstId;
    }

    public void openFilter(BaseEntity entity, ODataMessage message, OFilter.FilterType type, String toBOpenedScreenInstId) {
        logger.debug("Entering");
        try {
            String screenName = "RunFilter";
            OScreen filterScreen = (OScreen) oem.loadEntity(OScreen.class.getSimpleName(),
                    Collections.singletonList("name = '" + screenName + "'"), null, systemUser);
            String filterScreenInstId = generateScreenInstId(filterScreen.getName());

            SessionManager.setScreenSessionAttribute(filterScreenInstId, RunFilterBean.PASSED_VAR, entity);
            SessionManager.setScreenSessionAttribute(filterScreenInstId, RunFilterBean.FILTER_TYPE_VAR, type);
            SessionManager.setScreenSessionAttribute(filterScreenInstId, RunFilterBean.FILTER_MESSAGE_VAR, message);
            SessionManager.setScreenSessionAttribute(filterScreenInstId,
                    RunFilterBean.TO_B_OPENED_SCREEN_INST_ID_VAR, toBOpenedScreenInstId);

            openScreen(filterScreen, filterScreenInstId);
        } catch (UserNotAuthorizedException ex) {
            logger.error("Exception thrown", ex);
        } finally {
            logger.debug("Returning");
        }
    }

    public OFunctionResult openScreenCheckFilter(
            OScreen screen, ODataMessage dataMessage, String toBOpenedScreenInstId) {
        logger.debug("Entering");
        OFunctionResult ofr = new OFunctionResult();
        try {
            boolean nullData = false;
            boolean runFilter = false;
            screen = (OScreen) oem.loadEntity(OScreen.class.getSimpleName(), screen.getDbid(),
                    null, null, oem.getSystemUser(loggedUser));
            if (screen.getScreenFilter() != null
                    && !screen.getScreenFilter().isInActive()) {
                for (int i = 0; i < screen.getScreenFilter().getFilterFields().size(); i++) {
                    if (screen.getScreenFilter().getFilterFields().get(i).isInActive()) {
                        continue;
                    }

                    if (screen.getScreenFilter().getFilterFields().get(i).getFieldValue() == null
                            || screen.getScreenFilter().getFilterFields().get(i).getFieldValue().equals("")) {
                        nullData = true;
                    }
                }
                runFilter = screen.getScreenFilter().isAlwaysShow() || (nullData && !screen.getScreenFilter().isShowInsideScreen());
            }
            if (runFilter) {
                openFilter(screen, dataMessage, OFilter.FilterType.SCREEN_FILTER, toBOpenedScreenInstId);
            } else {
                openScreen(screen, toBOpenedScreenInstId);
            }
            logger.debug("Returning");
            return ofr;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return ofr;
    }
    
    protected TextTranslationServiceRemote textTranslationServiceRemote = OEJB.lookup(TextTranslationServiceRemote.class);

    public String openScreen(String screenName, String viewPage, String title, String screenInstId, boolean scrnC2AMode) {
        logger.trace("Entering");
        portalMode = true;
        if (portalMode) {
            logger.trace("Portal Mode, Adding New Protlet");
            if (viewPage.equals("EmployeeViewr.jspx")) {
                OrgChartFunction chartFunction;// = null;
                try {
                    chartFunction = (OrgChartFunction) oem.loadEntity(OrgChartFunction.class.getSimpleName(),
                            Collections.singletonList("code = 'empchart'"), null, loggedUser);

                    if (chartFunction != null) {
                        FABSSetup fabsOrgChartWSDL = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                                Collections.singletonList("setupKey = 'FABSOrgChartWSDL'"), null, loggedUser);

                        FABSSetup otmsOrgChartWSDL = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                                Collections.singletonList("setupKey = 'OTMSOrgChartWSDL'"), null, loggedUser);

                        String call = "top.FABS.Portlet.openOrgChart({viewPage:'" + chartFunction.getViewPage().getCode() + ".jspx"
                                + "',screenInstId:'" + generateScreenInstId("Employee Viewer")
                                + "',htmlDir:'" + getHtmlDir()
                                + "',portletHeight:'" + chartFunction.getPortletHeight()
                                + "',userName:'" + loggedUser.getLoginName()
                                + "',fabsWSDLURL:'" + fabsOrgChartWSDL.getSvalue()
                                + "',otmsWSDLURL:'" + otmsOrgChartWSDL.getSvalue()
                                + "',portletTitle:'Employee Viewer'});";
                        RequestContext context = RequestContext.getCurrentInstance();
                        context.execute(call);
                    }
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }
            } else {

                if (title != null && title.toLowerCase().equals("filter screen")) {
                    title = ddService.getDD("custom_report_header", loggedUser).getHeaderTranslated();
                    title = title + " - " + SessionManager.getSessionAttribute("reportName");
                }

                String call = "top.FABS.Portlet.open({screenName:'" + screenName + "',"
                        + "screenInstId:'" + screenInstId + "'"
                        //                        + "screenInstId:*top.FABS.Portlet.generatPortletID({screenName:'" + screenName+"'})"
                        + ",viewPage:'" + viewPage + "',portletTitle:'" + title + "',screenHeight:'"
                        + "" + "',enableC2A:'" + scrnC2AMode + "'});";
                RequestContext context = RequestContext.getCurrentInstance();
                OPortalUtil.reqContext = context;
                context.execute(call);
            }
            logger.trace("Returning with Null");
            return null;
        } else {
            logger.trace("Returning with Screen Name: {}", screenName);
            return screenName;
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Open Custom Report">
    public OFunctionResult openCustomReport(CustomReport customReport, AttachmentDTO attachment) {
        logger.debug("Entering");
        OFunctionResult ofr = new OFunctionResult();
        try {
            if (customReport.getJavaFunctionUserExit() != null) {
                SessionManager.addSessionAttribute("UserExitJavaFunction", customReport.getJavaFunctionUserExit());
            }
            SessionManager.addSessionAttribute("reportName", customReport.getReportNameTranslated());

            if (customReport.getCustomReportFilter() != null
                    && customReport.getCustomReportFilter().isShowBeforRun()
                    && customReport.getCustomReportFilter().getCustomreportfilterfields() != null
                    && !customReport.getCustomReportFilter().getCustomreportfilterfields().isEmpty()
                    || (customReport.getCustomReportParameters() != null
                    && !customReport.getCustomReportParameters().isEmpty())) {

                String openedScreenInstId = generateScreenInstId("RunFilterForCustomReport");
                ODataMessage message = new ODataMessage();
                ODataType oDataType = dataTypeService.loadODataType(CustomReportFilter.class.getSimpleName(), loggedUser);
                message.setODataType(oDataType);
                List data = new ArrayList();
                data.add(customReport.getCustomReportFilter());
                data.add(customReport);
                data.add(attachment);
                message.setData(data);
                SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.MESSAGE,
                        message);
                openScreen("RunFilterForCustomReport", openedScreenInstId);

            } else {
                logger.debug("Returning");
                return openCustomReport(null, null, customReport, attachment);
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning");
            return ofr;
        }
        logger.debug("Returning");
        return ofr;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Open Custom Report With Filters And Or Parameters">
    public OFunctionResult openCustomReport(String whereCondition, Map<String, Object> parametersMap, CustomReport customReport, AttachmentDTO attachment) {
        logger.debug("Entering");
        OFunctionResult ofr = new OFunctionResult();
        try {
            String viewPage = "birtReportWithExcel.jsp";//"CustomReportServlet";
            String reportName = customReport.getReportName();
            SessionManager.addSessionAttribute("UserExitJavaFunction", customReport.getJavaFunctionUserExit());
            SessionManager.addSessionAttribute("reportName", reportName);
            SessionManager.addSessionAttribute("Filters", whereCondition);
            SessionManager.addSessionAttribute("EntityAttachment", attachment);
            SessionManager.addSessionAttribute("parametersMap", parametersMap);
            SessionManager.addSessionAttribute("loggedUser", loggedUser);
            SessionManager.addSessionAttribute("localized", customReport.isLocalized());
            SessionManager.addSessionAttribute("revertLang", customReport.isRevertLang());
            RequestContext context = RequestContext.getCurrentInstance();
            String x = "top.FABS.Portlet.openReport({reportName:'" + customReport.getReportName()
                    + "',reportFormat:'HTML',reportTitle:'" + customReport.getReportName() + "', viewPage:'" + viewPage + "'});";
            context.execute(x);
            logger.debug("Returning");
            return ofr;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning");
            return ofr;
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Open Screen">
    public String openScreen(String screenName, String passedScreenInstId) {
        OScreen screen;// = null;
        screen = uiFrameWorkFacade.getScreenObject(screenName, loggedUser);
        return openScreen(screen, passedScreenInstId);
    }
    //</editor-fold>

    public String openScreen(OScreen oScreen, String openedScreenInstId) {
        logger.debug("Entering");
        try {
            if (c2AMode) {
                oScreen = (OScreen) textTranslationServiceRemote.loadEntityTranslation(oScreen, loggedUser);
                logger.debug("Returning");
                return openScreen(oScreen.getName(), oScreen.getViewPage(), oScreen.getHeaderTranslated(), openedScreenInstId, true);
            } else {
                oScreen = (OScreen) textTranslationServiceRemote.loadEntityTranslation(oScreen, loggedUser);
                logger.debug("Returning");
                return openScreen(oScreen.getName(), oScreen.getViewPage(), oScreen.getHeaderTranslated(), openedScreenInstId, false);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning with Null");
            return null;
        }
    }
    
    protected UIFrameworkServiceRemote uiFrameWorkFacade = OEJB.lookup(UIFrameworkServiceRemote.class);
    
    protected UserMessageServiceRemote usrMsgService = OEJB.lookup(UserMessageServiceRemote.class);
    
    public int callCount = 0;

    public String getHtmlDir() {
        return (loggedUser.isRtl()) ? "rtl" : "ltr";
    }

    public void setHtmlDir(String htmlDir) {
        this.htmlDir = htmlDir;
    }
    // </editor-fold>

    public void actionPerformed(ActionEvent arg0) {
    }

    public HtmlPanelGrid getMenuPanel() {
        logger.debug("Entering");
        try {
            if (menuPanel.getChildCount() == 0) {
                System.out.println("add panal to menu panel..............");
                menuPanel.getChildren().add(buildInnerScreen1());
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            logger.debug("Returning");
            return menuPanel;
        }
    }

    public void setMenuPanel(HtmlPanelGrid menuPanel) {
        this.menuPanel = menuPanel;
    }

    protected String getBeanName() {
        return DockMenuBean.class.getSimpleName();
    }

    @PostConstruct
    public void init() {
        Map<String, String> requestParams = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
        onInit(requestParams);
    }

    public boolean checkLang(long langid) {
        return (langid == new Long("24"));
    }

    public HtmlPanelGrid buildInnerScreen1() {
        HtmlPanelGrid genePanelGrid = NavMenuManagerImpl.getSingleInstance().getUserPanel(loggedUser);
        customedDynamicStyle = NavMenuManagerImpl.getSingleInstance().customedDynamicStyle;
        return genePanelGrid;
    }
    
    EntityAttachmentServiceLocal attachmentService = OEJB.lookup(EntityAttachmentServiceLocal.class);

    public void functionActionListener(ActionEvent event) {
        if (null == loggedUser) {
            loggedUser = (OUser) SessionManager.getSessionAttribute("loggedUser");
        }
        int count = count(liferayUsers.get(loggedUser.getLoginName()));
        if (count == 0) {
            logout();
            return;
        }
        try {
            String funHtmlID = ((MenuItem) event.getComponent()).getId();
            funHtmlID = funHtmlID.substring(0, funHtmlID.lastIndexOf("_"));
            String funDBID = funHtmlID.substring(
                    funHtmlID.lastIndexOf("_") + 1);
            long functionDBID = Long.parseLong(funDBID);
            String ftype = functionServiceRemote.getFunctionsType(functionDBID, loggedUser);
            OFunction ofunction = functionServiceRemote.getOFunction(functionDBID, loggedUser);
            if (ofunction instanceof UDCScreenFunction) {
                UDCScreenFunction screenFunction = (UDCScreenFunction) ofunction;
                ODataType parentDT = dataTypeService.loadODataType("ParentDBID", systemUser);
                //FIXME: performance, load this ODataType once static (ondemand) in the class
                ODataMessage parentDataMessage = new ODataMessage();
                List dataList = new ArrayList();
                dataList.add(screenFunction.getType().getDbid());
                parentDataMessage.setData(dataList);
                parentDataMessage.setODataType(parentDT);
                String openedScreenInstId = generateScreenInstId(screenFunction.getOscreen().getName());

                if (screenFunction.getScreenMode() != null) {

                    SessionManager.setScreenSessionAttribute(
                            openedScreenInstId,
                            OBackBean.MODE_VAR, screenFunction.getScreenMode().getDbid());
                }
                SessionManager.setScreenSessionAttribute(
                        openedScreenInstId,
                        OPortalUtil.MESSAGE, parentDataMessage);
                SessionManager.setScreenSessionAttribute(
                        openedScreenInstId,
                        OBackBean.WIZARD_VAR, false);
                openScreenCheckFilter(screenFunction.getOscreen(), parentDataMessage, openedScreenInstId);
            } else if (ofunction instanceof ScreenFunction) {
                ScreenFunction screenFunction = (ScreenFunction) ofunction;
                logger.trace("Opening ScreenFunction");
                if (null == systemUser) {
                    systemUser = (OUser) SessionManager.getSessionAttribute("loggedUser");
                }
                ODataType voidDataType = dataTypeService.loadODataType("VoidDT", systemUser);
                //FIXME: performance, load this ODataType once static (ondemand) in the class
                ODataMessage voidDataMessage = new ODataMessage();
                voidDataMessage.setData(new ArrayList<Object>());
                voidDataMessage.setODataType(voidDataType);
                String openedScreenInstId = generateScreenInstId(screenFunction.getOscreen().getName());
                if (screenFunction.getScreenMode() != null) {

                    SessionManager.setScreenSessionAttribute(
                            openedScreenInstId,
                            OBackBean.MODE_VAR, screenFunction.getScreenMode().getDbid());
                }
                SessionManager.setScreenSessionAttribute(
                        openedScreenInstId,
                        OPortalUtil.MESSAGE, voidDataMessage);
                SessionManager.setScreenSessionAttribute(
                        openedScreenInstId,
                        OBackBean.WIZARD_VAR, false);
                openScreenCheckFilter(screenFunction.getOscreen(), voidDataMessage, openedScreenInstId);
            } else if (ofunction instanceof CustRepFunction) {
                CustRepFunction function = (CustRepFunction) ofunction;
                CustomReport rprt = (CustomReport) oem.loadEntity(CustomReport.class.getSimpleName(),
                        Collections.singletonList("dbid=" + function.getCustomReport().getDbid()), null, loggedUser);
           
                AttachmentDTO attachmentDTO = attachmentService.get(function.getCustomReport().getDbid(),
                        function.getCustomReport().getClassName(), loggedUser).get(0);
                openCustomReport(rprt, attachmentDTO);
            } else if (ftype.equals("PortalPage")) {
                if (checkLiferayUser(loggedUser) && checkMenuAuthority(loggedUser, ofunction)) {

                    List<Long> portalPageData = (List<Long>) functionServiceRemote.getPortalPageData(functionDBID, loggedUser);
                    OPortalUtil.addOPortletPage(portalPageData, oem, dataTypeService, loggedUser, null,
                            uiFrameWorkFacade, uDCService);
                }

            } else if (ofunction instanceof URLFunction) {
                ODataMessage odm = new ODataMessage();
                OFunctionResult oFR = functionServiceRemote.runFunction(ofunction, odm, new OFunctionParms(), loggedUser);
            } else if (ofunction instanceof ReportFunction) {
                ReportFunction reportFunction = (ReportFunction) ofunction;
                openReportCheckFilter(reportFunction.getReport(), null);
            } else if (ofunction instanceof WizardFunction) {
                ODataType voidDataType = dataTypeService.loadODataType("VoidDT", loggedUser);
                //FIXME: performance, load this ODataType once static (ondemand) in the class
                ODataMessage voidDataMessage = new ODataMessage();
                voidDataMessage.setData(new ArrayList<Object>());
                voidDataMessage.setODataType(voidDataType);
                Wizard wizard = ((WizardFunction) ofunction).getWizard();
                textTranslationServiceRemote.loadEntityTranslation(wizard, loggedUser);
                WizardController wizardController = new WizardController(oem,
                        wizard, loggedUser, usrMsgService);
                wizardController.openWizard(voidDataMessage);

            } else if (ofunction instanceof WebServiceFunction) {
                //FIXME: add any log
            } else if (ofunction instanceof JavaFunction) {
                ODataMessage oDM = new ODataMessage();
                List<Object> data = new ArrayList();
                data.add(loggedUser);
                oDM.setData(data);
                OFunctionResult oFR = functionServiceRemote.runFunction(ofunction, oDM, null, loggedUser);
                String code = oFR.getReturnValues().isEmpty() ? null : (String) oFR.getReturnValues().get(0);
                if (code != null) {
                    // Should be server job DBID of server job function if so
                    OScreen addEditServerJobScreen = (OScreen) oem.loadEntity("FormScreen",
                            Collections.singletonList("name='AddEditServerJob'"), null,
                            oem.getSystemUser(loggedUser));
                    ServerJob serverJob = (ServerJob) oem.loadEntity("ServerJob",
                            Collections.singletonList("dbid=" + code), null,
                            oem.getSystemUser(loggedUser));
                    ODataType serviceJobDT = dtService.loadODataType("ServerJob",
                            oem.getSystemUser(loggedUser));
                    ODataMessage screenDM = new ODataMessage();
                    screenDM.setData(new ArrayList<Object>());
                    screenDM.getData().add(serverJob);
                    screenDM.setODataType(serviceJobDT);
                    String openedScreenInstId = generateScreenInstId(addEditServerJobScreen.getName());
                    SessionManager.setScreenSessionAttribute(openedScreenInstId,
                            OPortalUtil.MESSAGE, screenDM);
                    SessionManager.setScreenSessionAttribute(
                            openedScreenInstId,
                            OBackBean.MODE_VAR, "18");

                    openScreenCheckFilter(addEditServerJobScreen, screenDM, openedScreenInstId);
                }
            } else if (ofunction instanceof OrgChartFunction) {
                // load
                FABSSetup fabsOrgChartWSDL = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                        Collections.singletonList("setupKey = 'FABSOrgChartWSDL'"), null, loggedUser);

                FABSSetup otmsOrgChartWSDL = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                        Collections.singletonList("setupKey = 'OTMSOrgChartWSDL'"), null, loggedUser);

                OrgChartFunction chartFunction = (OrgChartFunction) ofunction;
                String call = "top.FABS.Portlet.openOrgChart({viewPage:'" + chartFunction.getViewPage().getCode() + ".jspx"
                        + "',screenInstId:'" + generateScreenInstId("Employee Viewer")
                        + "',htmlDir:'" + getHtmlDir()
                        + "',portletHeight:'" + chartFunction.getPortletHeight()
                        + "',userName:'" + loggedUser.getLoginName()
                        + "',fabsWSDLURL:'" + fabsOrgChartWSDL.getSvalue()
                        + "',otmsWSDLURL:'" + otmsOrgChartWSDL.getSvalue()
                        + "',portletTitle:'Employee Viewer'});";
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute(call);
            } else {
                ODataMessage oDM = new ODataMessage();
                List<Object> data = new ArrayList();
                data.add(loggedUser);
                oDM.setData(data);
                OFunctionResult oFR = functionServiceRemote.runFunction(ofunction, oDM, null, loggedUser);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }

    }
    public String customedDynamicStyle;

    public String getCustomedDynamicStyle() {
        return customedDynamicStyle;
    }

    public void setCustomedDynamicStyle(String customedDynamicStyle) {
        this.customedDynamicStyle = customedDynamicStyle;
    }

    public boolean checkMenuAuthority(OUser loggedUser, OFunction oFunction) {

        boolean isAuthorized = false;
        List<RoleUser> userRoles = loggedUser.getRoleUsers();
        List<OMenuFunction> oMenuFunction = oFunction.getMenuFunction();
        HashSet<OMenu> roleMenus = new HashSet<>();
        HashSet<OMenu> functionMenus = new HashSet<>();

        if (userRoles != null && oMenuFunction != null) {

            for (RoleUser roleUser : userRoles) {
                if (roleUser.getOrole() != null && roleUser.getOrole().getOroleMenus() != null) {
                    for (RoleMenu roleMenu : roleUser.getOrole().getOroleMenus()) {

                        if (roleMenu.getOmenu() != null) {
                            roleMenus.add(roleMenu.getOmenu());
                            roleMenus.addAll(roleMenu.getOmenu().getChildMenues());
                        }

                    }
                }
            }

            for (OMenuFunction menuFunction : oMenuFunction) {

                if (menuFunction.getOmenu() != null) {

                    functionMenus.add(menuFunction.getOmenu());
                    functionMenus.addAll(menuFunction.getOmenu().getChildMenues());
                }

            }

            for (OMenu funMenu : functionMenus) {

                for (OMenu menu : roleMenus) {

                    if (funMenu.equals(menu)) {
                        isAuthorized = true;
                        return isAuthorized;
                    }
                }
            }
        }
        return isAuthorized;
    }

    public boolean checkLiferayUser(OUser _loggedUser) {

        boolean isTheActualUser = false;

        String requestUrl = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("referer");
        long liferayUserId = Long.parseLong(requestUrl.split("&UD=")[1].split("&")[0]);

        String userScreenName = OPortalUtil.getUserLoginName(liferayUserId);

        if (userScreenName != null && userScreenName.equals(_loggedUser.getLoginName())) {

            isTheActualUser = true;
        }

        return isTheActualUser;
    }
}
