/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDLookupType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.filter.FilterField;
import com.unitedofoq.fabs.core.filter.OFilterServiceLocal;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.timezone.TimeZoneServiceLocal;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.utils.CSSStaticConstants;
import java.util.List;
import javax.el.ExpressionFactory;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.context.FacesContext;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.panel.Panel;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rsamir
 */
public class FilterBuilder {
    
    final static org.slf4j.Logger logger = LoggerFactory.getLogger(TabularBean.class);
    public static final String FROM_DELIM = TabularBean.FROM_DELIM;
    public static final String TO_DELIM = TabularBean.TO_DELIM;
    
    private TabularBean tabularBean;
    private OUser loggedUser;
    private TimeZoneServiceLocal timeZoneService;
    private OFilterServiceLocal filterServiceBean;
    private OEntityManagerRemote oem;
    private ExpressionFactory exFactory;

    public FilterBuilder(TabularBean tabularBean, OFilterServiceLocal filterServiceBean, 
            OEntityManagerRemote oem, ExpressionFactory exFactory, TimeZoneServiceLocal timeZoneService, OUser loggedUser) {
        this.tabularBean = tabularBean;
        this.loggedUser = loggedUser;
        this.timeZoneService = timeZoneService;
        this.filterServiceBean = filterServiceBean;
        this.oem = oem;
        this.exFactory = exFactory;
    }
    
    public HtmlPanelGrid buildFilterPanel(HtmlPanelGrid htmlPanelGrid, List<FilterField> filterFields,
            String pnlGridId, String pnlHeader, int j) {
        logger.debug("Entering");
        Panel entityFilterColpanel = new Panel();
        HtmlPanelGrid entityFilterMiddlePanel = new HtmlPanelGrid();
        entityFilterMiddlePanel.setColumns(2);
        if (loggedUser.getFirstLanguage().getDbid() == 24) {
            entityFilterMiddlePanel.setStyle(CSSStaticConstants.MIDDLE_PANEL_STYLE);
        }
        HtmlPanelGrid entityFilterGrid = buildFilterPanelGrid(pnlGridId , 1);
        entityFilterGrid.getChildren().add(entityFilterMiddlePanel);
        entityFilterColpanel = buildFilterMainPanel(entityFilterMiddlePanel, pnlHeader,filterFields, j);
        htmlPanelGrid.getChildren().add(entityFilterColpanel);
        entityFilterColpanel.getChildren().add(entityFilterGrid);
        logger.debug("Returning");
        return entityFilterGrid;
    }
    
    private HtmlPanelGrid buildFilterPanelGrid(String id, int noOfCol) {
        logger.debug("Entering");
        HtmlPanelGrid pnlGrid = new HtmlPanelGrid();
        pnlGrid.setId(id);
        pnlGrid.setColumns(noOfCol);
        pnlGrid.setCellpadding("0px");
        pnlGrid.setCellspacing("0px");
        logger.debug("Returning");
        return pnlGrid;
    }
    
    private Panel buildFilterMainPanel(HtmlPanelGrid middlePanel, String header,List<FilterField> filterFields, int j) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Method Fields">

        Panel collapsible = new Panel();
        collapsible.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_Grop");
        collapsible.setStyleClass(CSSStaticConstants.FILTER_COLLAPSIBLE_CLASS);
        collapsible.getFacets().put("header", createFilterHeaderPanel(header));
        
        if (filterFields != null) {
            for (int i = j; i < filterFields.size(); i++) {
                buildFilterField(middlePanel,filterFields.get(i),i);
            }
        }
        
        logger.debug("Returning");
        return collapsible;
    }
    
    private HtmlPanelGroup createFilterHeaderPanel(String header) {         
        HtmlPanelGroup headerGroup = new HtmlPanelGroup();
        headerGroup.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        HtmlOutputLabel collasibleHeader = new HtmlOutputLabel();
        collasibleHeader.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_GropHdr");
        collasibleHeader.setValue(header);
        collasibleHeader.setStyleClass(CSSStaticConstants.FILTER_LABEL_CLASS);
        headerGroup.getChildren().add(collasibleHeader);
        return headerGroup;
    }
    
    private void buildFilterField(HtmlPanelGrid middlePanel, FilterField filterField, int i) throws NumberFormatException {

        ScreenField screenField = new ScreenField();
        String value = filterField.getFieldValue();
        String valueTo = filterField.getFieldValueTo();
        screenField.setDd(filterField.getFieldDD());
        screenField.setEditable(true);
        screenField.setFieldExpression(filterField.getFieldExpression());

        middlePanel.getChildren().add(createFilterLabel(
                "filterfld" + i, screenField));

        if (!filterField.isFromTo()) {
            if (filterField.getFieldDD().getControlType().getDbid() == DD.CT_DROPDOWN
                    || filterField.getFieldDD().getControlType().getDbid() == DD.CT_LOOKUPSCREEN) {
                try {
                    BaseEntity currentObject = null;
                    currentObject = tabularBean.getLookupBaseEntity(filterField.getFieldExpression(), value, null);
                    if (null == currentObject) {
                        return;
                    }
                    tabularBean.valuesMap.put(filterField.getFieldExpression(), currentObject);
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                    return;
                }

            } else if (filterField.getFieldDD().getControlType().getDbid() == DD.CT_CALENDAR) {
                if (value != null && !value.equals("")) {

                    if (value.equalsIgnoreCase("#current_date")) {
                        tabularBean.valuesMap.put(filterField.getFieldExpression(), timeZoneService.getUserCDT(loggedUser));
                    } else {

                        try {
                            tabularBean.valuesMap.put(filterField.getFieldExpression(), FilterField.dateFormat.parse(value));
                        } catch (Exception ex) {
                            logger.error("Exception thrown", ex);
                        }
                    }
                } else {
                    tabularBean.valuesMap.put(filterField.getFieldExpression() + FROM_DELIM , null);
                }
            } else {
                tabularBean.valuesMap.put(filterField.getFieldExpression(), value);
            }

            String fldExp = "#{" + tabularBean.getBeanName() + "." + "valuesMap['" + filterField.getFieldExpression() + "']" + "}";
            UIComponent component = null;
            if (filterField.getFieldDD().getControlType().getDbid() == DD.CT_TEXTFIELD) {
                component = tabularBean.createTextFieldControl(20, "filterfld_fld" + i, fldExp, true, screenField.getDd(), i, false, false, false);
            } else if (filterField.getFieldDD().getControlType().getDbid() == DD.CT_CHECKBOX) {
                component = tabularBean.createCheckBoxControl("filterfld_fld" + i, fldExp, true, screenField.getDd(), i, false);
            } else if (filterField.getFieldDD().getControlType().getDbid() == DD.CT_CALENDAR) {
                component = tabularBean.createCalendarControl("filterfld_fld" + i, fldExp, i, true, screenField.getDd(), i, false);
            } else if (filterField.getFieldDD().getControlType().getDbid() == DD.CT_DROPDOWN
                    || filterField.getFieldDD().getControlType().getDbid() == DD.CT_LOOKUPSCREEN
                    || filterField.getFieldDD().getControlType().getDbid() == DD.CT_LITERALDROPDOWN) {
                DD fieldDD = screenField.getDd();
                HtmlPanelGrid lookupPanelGrid = new HtmlPanelGrid();
                lookupPanelGrid.setId(FacesContext.getCurrentInstance().
                        getViewRoot().createUniqueId());

                String lookupExp = null;
                if ((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("")
                        && fieldDD.getLookupType() == DDLookupType.DDLT_DRPDWN)
                        || (null != fieldDD.getLookupFilter() && !"".equals(fieldDD.getLookupFilter()))
                        && fieldDD.getLookupType() == DDLookupType.DDLT_LTRLDRPDN) {
                    lookupExp = "";//getLookupFldExpression(filterfields.get(i).getFieldExpression());//getDRDFldExpression(filterfields.get(i).getFieldExpression());
                } else {
                    lookupExp = tabularBean.getLookupFldExpression(filterField.getFieldExpression());
                }

                fldExp = tabularBean.getBeanName() + "." + "valuesMap['" + filterField.getFieldExpression() + "']";
                String lookupID = filterField.getFieldExpression().replace(".", "_") + "_Lookup_filterField";
                screenField.setEditable(false);
                tabularBean.fieldLookup = new Lookup(tabularBean,
                        tabularBean.getBeanName(),
                        fldExp,
                        screenField.getDd(),
                        lookupExp,
                        null,
                        lookupID,
                        screenField,
                        false,
                        oem, filterServiceBean,
                        exFactory,
                        FacesContext.getCurrentInstance(),
                        loggedUser);

                tabularBean.getLookupHashMap().put(lookupID, tabularBean.fieldLookup);

                if ((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField()
                        .equals("")
                        && fieldDD.getLookupType() == DDLookupType.DDLT_DRPDWN)
                        || (null != fieldDD.getLookupFilter() && !"".equals(fieldDD.getLookupFilter()))
                        && fieldDD.getLookupType() == DDLookupType.DDLT_LTRLDRPDN) {
                    lookupPanelGrid.getChildren().add(tabularBean.fieldLookup.createDropDown(false));
                    tabularBean.getSelectItemsList().put(tabularBean.fieldLookup.getDropDownMenu().getId(),
                            tabularBean.fieldLookup.getSelectItems());

                } else if ((fieldDD.getLookupDropDownDisplayField() != null
                        && !fieldDD.getLookupDropDownDisplayField().equals("")
                        && fieldDD.getLookupType() == DDLookupType.DDLT_MULTISELECTIONLIST)) {
                    lookupPanelGrid.getChildren().add(tabularBean.fieldLookup.createMultiSelectionList());
                    tabularBean.getSelectItemsList().put(tabularBean.fieldLookup.getManyListbox().getId(),
                            tabularBean.fieldLookup.getSelectItems());
                } else {
                    lookupPanelGrid.getChildren().add(tabularBean.fieldLookup.createNonDropDown());
                    FramworkUtilities.setControlFldExp(tabularBean.fieldLookup.getInputText(), screenField.getFieldExpression());
                }

                component = lookupPanelGrid;
            } else if (filterField.getFieldDD().getControlType().getDbid() == DD.CT_TEXTFIELD) {
                component = tabularBean.createTextFieldControl(20, "filterfld_fld" + i, fldExp, true, screenField.getDd(),
                        i, false, false, false);
            } else {
                component = tabularBean.createControl(tabularBean.CNTRL_INPUTMODE, Integer.parseInt(Long.toString(
                        filterField.getFieldDD().getControlType().getDbid())),
                        filterField.getFieldDD(), i, screenField, false, false, false, false);
            }

            middlePanel.getChildren().add(component);

        } else {
            String fromVal = null;
            String toVal = null;
            if (value != null && !value.equals("") && valueTo != null && !valueTo.equals("")) {
                fromVal = value;
                toVal = valueTo;

            } else {
                logger.warn("From-To Structure Error");
            }
            if (filterField.getFieldDD().getControlType().getDbid() == DD.CT_DROPDOWN || filterField.getFieldDD().getControlType().getDbid() == DD.CT_LOOKUPSCREEN) {
                try {
                    BaseEntity currentObject = null;
                    BaseEntity currentObjectTo = null;

                    currentObject = tabularBean.getLookupBaseEntity(filterField.getFieldExpression(), value, null);
                    currentObjectTo = tabularBean.getLookupBaseEntity(filterField.getFieldExpression(), valueTo, null);

                    tabularBean.valuesMap.put(filterField.getFieldExpression() + FROM_DELIM, currentObject);
                    tabularBean.valuesMap.put(filterField.getFieldExpression() + TO_DELIM, currentObjectTo);

                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                    tabularBean.valuesMap.put(filterField.getFieldExpression() + FROM_DELIM, null);
                    tabularBean.valuesMap.put(filterField.getFieldExpression() + TO_DELIM, null);
                }
            } else if (filterField.getFieldDD().getControlType().getDbid() == DD.CT_CALENDAR) {
                if (fromVal != null && !fromVal.equals("")) {
                    try {
                        tabularBean.valuesMap.put(filterField.getFieldExpression() + FROM_DELIM, FilterField.dateFormat.parse(fromVal));
                    } catch (Exception ex) {
                        logger.error("Exception thrown", ex);
                        tabularBean.valuesMap.put(filterField.getFieldExpression() + FROM_DELIM, null);
                    }
                } else {
                    tabularBean.valuesMap.put(filterField.getFieldExpression() + FROM_DELIM, null);
                }

                if (toVal != null && !toVal.equals("")) {
                    try {
                        tabularBean.valuesMap.put(filterField.getFieldExpression() + TO_DELIM, FilterField.dateFormat.parse(toVal));
                    } catch (Exception e) {
                        tabularBean.valuesMap.put(filterField.getFieldExpression() + TO_DELIM, null);
                    }
                } else {
                    tabularBean.valuesMap.put(filterField.getFieldExpression() + TO_DELIM, null);
                }

            } else {
                tabularBean.valuesMap.put(filterField.getFieldExpression() + FROM_DELIM, fromVal);
                tabularBean.valuesMap.put(filterField.getFieldExpression() + TO_DELIM, toVal);
            }

            String fldExpFrom = "#{" + tabularBean.getBeanName() + "." + "valuesMap['" + filterField.getFieldExpression() + FROM_DELIM + "']" + "}";
            String fldExpTo = "#{" + tabularBean.getBeanName() + "." + "valuesMap['" + filterField.getFieldExpression() + TO_DELIM + "']" + "}";

            HtmlPanelGrid internalGrid = new HtmlPanelGrid();
            internalGrid.setId("FromToGrid" + i);
            internalGrid.setColumns(3);
            internalGrid.setCellpadding("0px");
            internalGrid.setCellspacing("0px");

            HtmlOutputText text2 = new HtmlOutputText();
            text2.setValue(tabularBean.ddService.getDD("RunFilterBeanTo", loggedUser).getLabelTranslated());
            text2.setId("Txt2" + i);

            UIComponent component1 = null;
            UIComponent component2 = null;

            if (filterField.getFieldDD().getControlType().getDbid() == DD.CT_TEXTFIELD) {
                component1 = tabularBean.createTextFieldControl(20, "filterfld_fld_From" + i, fldExpFrom,
                        true, screenField.getDd(), i, false, false, false);
                component2 = tabularBean.createTextFieldControl(20, "filterfld_fld_To" + i, fldExpTo,
                        true, screenField.getDd(), i, false, false, false);
            } else if (filterField.getFieldDD().getControlType().getDbid() == DD.CT_CHECKBOX) {
                component1 = tabularBean.createCheckBoxControl("filterfld_fld" + i, fldExpFrom, true, screenField.getDd(), i, false);
                component2 = tabularBean.createCheckBoxControl("filterfld_fld" + i, fldExpTo, true, screenField.getDd(), i, false);
            } else if (filterField.getFieldDD().getControlType().getDbid() == DD.CT_CALENDAR) {
                component1 = tabularBean.createCalendarControl("filterfld_fld" + i, fldExpFrom, i, true, screenField.getDd(), i, false);
                component2 = tabularBean.createCalendarControl("filterfld_fld" + i, fldExpTo, i, true, screenField.getDd(), i, false);
            } else if (filterField.getFieldDD().getControlType().getDbid() == DD.CT_DROPDOWN 
                    || filterField.getFieldDD().getControlType().getDbid() == DD.CT_LOOKUPSCREEN) {
                DD fieldDD = screenField.getDd();
                HtmlPanelGrid lookupPanelGrid = new HtmlPanelGrid();
                lookupPanelGrid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());

                String lookupExp = null;
                if ((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("")
                        && fieldDD.getLookupType() == DDLookupType.DDLT_DRPDWN)
                        || (null != fieldDD.getLookupFilter() && !"".equals(fieldDD.getLookupFilter()))
                        && fieldDD.getLookupType() == DDLookupType.DDLT_LTRLDRPDN) {
                    lookupExp = "";//getDRDFldExpression(filterfields.get(i).getFieldExpression());
                } else {
                    lookupExp = tabularBean.getLookupFldExpression(filterField.getFieldExpression());
                }
                String fldExp1;
                fldExp1 = tabularBean.getBeanName() + "." + "valuesMap['" + filterField.getFieldExpression() + FROM_DELIM + "']";
                String firstLookupID = filterField.getFieldExpression().replace(".", "_") + "_Lookup_1_filterField";
                String secondLookupID = filterField.getFieldExpression().replace(".", "_") + "_Lookup_2_filterField";
                tabularBean.fieldLookup = new Lookup(tabularBean,
                        tabularBean.getBeanName(),
                        fldExp1,
                        screenField.getDd(),
                        lookupExp,
                        null,
                        firstLookupID,
                        screenField,
                        false,
                        oem, filterServiceBean,
                        exFactory,
                        FacesContext.getCurrentInstance(),
                        loggedUser);

                tabularBean.getLookupHashMap().put(firstLookupID, tabularBean.fieldLookup);

                if ((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("")
                        && fieldDD.getLookupType() == DDLookupType.DDLT_DRPDWN)
                        || (null != fieldDD.getLookupFilter() && !"".equals(fieldDD.getLookupFilter()))
                        && fieldDD.getLookupType() == DDLookupType.DDLT_LTRLDRPDN) {
                    lookupPanelGrid.getChildren().add(tabularBean.fieldLookup.createDropDown(false));
                   tabularBean.getSelectItemsList().put(tabularBean.fieldLookup.getDropDownMenu().getId(),
                            tabularBean.fieldLookup.getSelectItems());
                } else if ((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("")
                        && fieldDD.getLookupType() == DDLookupType.DDLT_MULTISELECTIONLIST)) {
                    lookupPanelGrid.getChildren().add(tabularBean.fieldLookup.createMultiSelectionList());
                    tabularBean.getSelectItemsList().put(tabularBean.fieldLookup.getManyListbox().getId(),
                            tabularBean.fieldLookup.getSelectItems());
                } else {

                    lookupPanelGrid.getChildren().add(tabularBean.fieldLookup.createNonDropDown());
                    FramworkUtilities.setControlFldExp(tabularBean.fieldLookup.getInputText(), screenField.getFieldExpression());
                }

                component1 = lookupPanelGrid;

                HtmlPanelGrid lookupPanelGridTo = new HtmlPanelGrid();
                lookupPanelGridTo.setId(FacesContext.getCurrentInstance().
                        getViewRoot().createUniqueId());

                String fldExp2;
                fldExp2 = tabularBean.getBeanName() + "." + "valuesMap['" + filterField.getFieldExpression() + TO_DELIM + "']";

                tabularBean.fieldLookup = new Lookup(tabularBean,
                        tabularBean.getBeanName(),
                        fldExp2,
                        screenField.getDd(),
                        lookupExp,
                        null,
                        secondLookupID,
                        screenField,
                        false,
                        oem, filterServiceBean,
                        exFactory,
                        FacesContext.getCurrentInstance(),
                        loggedUser);

                tabularBean.getLookupHashMap().put(secondLookupID, tabularBean.fieldLookup);

                if ((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("")
                        && fieldDD.getLookupType() == DDLookupType.DDLT_DRPDWN)
                        || (null != fieldDD.getLookupFilter() && !"".equals(fieldDD.getLookupFilter()))
                        && fieldDD.getLookupType() == DDLookupType.DDLT_LTRLDRPDN) {
                    lookupPanelGridTo.getChildren().add(tabularBean.fieldLookup.createDropDown(false));
                    tabularBean.getSelectItemsList().put(tabularBean.fieldLookup.getDropDownMenu().getId(),
                            tabularBean.fieldLookup.getSelectItems());
                } else if ((fieldDD.getLookupDropDownDisplayField() != null && !fieldDD.getLookupDropDownDisplayField().equals("")
                        && fieldDD.getLookupType() == DDLookupType.DDLT_MULTISELECTIONLIST)) {
                    lookupPanelGrid.getChildren().add(tabularBean.fieldLookup.createMultiSelectionList());
                    tabularBean.getSelectItemsList().put(tabularBean.fieldLookup.getManyListbox().getId(),
                            tabularBean.fieldLookup.getSelectItems());
                } else {
                    lookupPanelGridTo.getChildren().add(tabularBean.fieldLookup.createNonDropDown());
                    FramworkUtilities.setControlFldExp(tabularBean.fieldLookup.getInputText(), screenField.getFieldExpression());
                }

                component2 = lookupPanelGridTo;
            } else {
                component1 = tabularBean.createTextFieldControl(20, "filterfld_fld_From" + i, fldExpFrom,
                        true, screenField.getDd(), i, false, false, false);
                component2 = tabularBean.createTextFieldControl(20, "filterfld_fld_To" + i, fldExpTo,
                        true, screenField.getDd(), i, false, false, false);
            }

            internalGrid.getChildren().add(component1);
            internalGrid.getChildren().add(text2);
            internalGrid.getChildren().add(component2);

            middlePanel.getChildren().add(internalGrid);
        }
    }

    public void buildFilterButtonPanel(HtmlPanelGrid filterGrid, List<FilterField> filterfields) {
        //<editor-fold defaultstate="collapsed" desc="Save Button and Category Code">
        HtmlPanelGrid buttonsPanel = buildFilterPanelGrid("FilterButtonsPanel", 3);
        CommandButton saveButton = new CommandButton();
        CommandButton runButton = new CommandButton();
        CommandButton saveRunButton = new CommandButton();

        String id = "button";

        saveButton.setId(id + "_saveButton");
        saveRunButton.setId(id + "_saveRunButton");
        runButton.setId(id + "_runButton");

        saveButton.setValue(tabularBean.ddService.getDD("saveButton", loggedUser).getLabelTranslated());
        saveRunButton.setValue(tabularBean.ddService.getDD("saveRunButton", loggedUser).getLabelTranslated());
        runButton.setValue(tabularBean.ddService.getDD("runButton", loggedUser).getLabelTranslated());

        saveButton.setActionExpression(exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                "#{" + tabularBean.getBeanName() + ".saveAction}", String.class, new Class[0]));

        saveRunButton.setActionExpression(exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                "#{" + tabularBean.getBeanName() + ".runSaveAction}", String.class, new Class[0]));

        runButton.setActionExpression(exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                "#{" + tabularBean.getBeanName() + ".runAction}", String.class, new Class[0]));

        runButton.setUpdate("tf:generalPanel");
        runButton.setStyleClass(CSSStaticConstants.RUN_BUTTON_CLASS);
        //</editor-fold>
        //   ADD middlePanel TO htmlPanelGrid
        buttonsPanel.getChildren().add(runButton);
        buttonsPanel.setStyle(CSSStaticConstants.BUTTON_PANEL_STYLE);
        
        //Hide the 'Save' and 'Save & Exit' buttons in process mode:
        if (tabularBean.getScreenParam("taskID") == null) {
            filterGrid.getChildren().add(buttonsPanel);
        }

        tabularBean.restorePDataFromSessoion = false;
        tabularBean.onloadJSStr
                += "top." + tabularBean.getPortletInsId() + "DataRestored();";

        List<FilterField> filterFields = tabularBean.updateFilterFields(filterfields);
        if(tabularBean.currentScreen.getScreenFilter() != null){
            tabularBean.currentScreen.getScreenFilter().setFilterFields(filterFields);
        }        
    }

    private HtmlPanelGrid createFilterLabel(String id, ScreenField screenField) {
        logger.debug("Entering");
        try {

            DD fieldDD = screenField.getDd();

            String controlLabel = (screenField.getDdTitleOverride() != null && !screenField.getDdTitleOverride().equals("")) ? screenField.getDdTitleOverride() : fieldDD.getLabel();
            String astrick = "";
            HtmlOutputLabel htmlOutputLabel = new HtmlOutputLabel();
            if (id.contains(".")) {
                htmlOutputLabel.setId(id.substring(id.lastIndexOf(".") + 1) + "__Label");
            } else {
                htmlOutputLabel.setId(id + "_Label");
            }
            htmlOutputLabel.setStyleClass(CSSStaticConstants.OTMS_LABEL_CLASS);
            htmlOutputLabel.setValue(controlLabel);

            HtmlOutputLabel astriskLabel = new HtmlOutputLabel();
            astriskLabel.setId(id + "_astrick");
            astriskLabel.setValue(astrick);
            astriskLabel.setStyle("color:red");
            HtmlPanelGrid headerLabelGrid = new HtmlPanelGrid();

            headerLabelGrid.setColumns(2);
            headerLabelGrid.setCellpadding("0");
            headerLabelGrid.setCellspacing("0");
            headerLabelGrid.getChildren().add(htmlOutputLabel);
            headerLabelGrid.getChildren().add(astriskLabel);
            logger.debug("Returning");
            return headerLabelGrid;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            return new HtmlPanelGrid();
        }
    }
}
