package com.unitedofoq.fabs.core.uiframework.backbean;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.EngineException;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.engine.api.PDFRenderOption;
import org.eclipse.birt.report.model.api.CellHandle;
import org.eclipse.birt.report.model.api.DataItemHandle;
import org.eclipse.birt.report.model.api.DesignConfig;
import org.eclipse.birt.report.model.api.ElementFactory;
import org.eclipse.birt.report.model.api.IDesignEngine;
import org.eclipse.birt.report.model.api.IDesignEngineFactory;
import org.eclipse.birt.report.model.api.LabelHandle;
import org.eclipse.birt.report.model.api.OdaDataSetHandle;
import org.eclipse.birt.report.model.api.OdaDataSourceHandle;
import org.eclipse.birt.report.model.api.PropertyHandle;
import org.eclipse.birt.report.model.api.ReportDesignHandle;
import org.eclipse.birt.report.model.api.RowHandle;
import org.eclipse.birt.report.model.api.SessionHandle;
import org.eclipse.birt.report.model.api.StructureFactory;
import org.eclipse.birt.report.model.api.TableHandle;
import org.eclipse.birt.report.model.api.activity.SemanticException;
import org.eclipse.birt.report.model.api.elements.structures.ComputedColumn;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;

import com.ibm.icu.util.ULocale;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.filter.OFilterServiceLocal;
import com.unitedofoq.fabs.core.filter.UserSessionInfo;
import com.unitedofoq.fabs.core.i18n.TextTranslationServiceRemote;

import com.unitedofoq.fabs.core.multimedia.OImage;
import com.unitedofoq.fabs.core.report.ComparisonCrossTabReport;
import com.unitedofoq.fabs.core.report.CrossTabReport;
import com.unitedofoq.fabs.core.report.HeaderField;
import com.unitedofoq.fabs.core.report.MasterDetailReport;
import com.unitedofoq.fabs.core.report.OReport;
import com.unitedofoq.fabs.core.report.OReportField;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserServiceRemote;
import com.unitedofoq.fabs.core.timezone.TimeZoneServiceLocal;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.report.customReports.BirtEngine;
import org.eclipse.birt.core.framework.IPlatformContext;
import org.eclipse.birt.core.framework.PlatformServletContext;
import org.slf4j.LoggerFactory;

public class Report extends HttpServlet {
final static org.slf4j.Logger logger = LoggerFactory.getLogger(Report.class);
    
    private OEntityManagerRemote oem = OEJB.lookup(OEntityManagerRemote.class);
    
    private DDServiceRemote ddServiceRemote = OEJB.lookup(DDServiceRemote.class);
    
    private OFilterServiceLocal filterServiceLocal = OEJB.lookup(OFilterServiceLocal.class);
    
    private TimeZoneServiceLocal timeZoneService = OEJB.lookup(TimeZoneServiceLocal.class);
    
    private UserServiceRemote userServiceRemote = OEJB.lookup(UserServiceRemote.class);
    
    private TextTranslationServiceRemote textTranslationService = OEJB.lookup(TextTranslationServiceRemote.class);
    private OUser loggedUser;
    private OReport report;
    private ReportService reportService;
    private JasperPrint jasperPrint;
    private DynamicReport dynamicReport;
    private UserSessionInfo userSessionInfo;
    //private OImage oImage;
    
    ReportDesignHandle reportDesignHandle = null;
    ElementFactory elementFactory = null;
    StructureFactory structFactory = null;	

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        generateReport(request, response);
    }

    public void generateReport(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // <editor-fold defaultstate="collapsed" desc="UserSessionInfo">
        userSessionInfo = (UserSessionInfo) request.getSession().getAttribute("UserSessionInfo");
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="loggedUser">
        loggedUser = (OUser) request.getSession().getAttribute("loggedUser");
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Load Report Creation Parameters">
        String outputFormat = request.getParameter("output");
        String reportName = request.getParameter("report");
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Load Report Specifications">
        report = (OReport) request.getSession().getAttribute(OBackBean.REPORT_VAR);
//        report = (OReport) reportService.loadReportSpecs(reportName);
        // </editor-fold>

        jasperPrint = null;
        dynamicReport = null;


        try {

            oem.getEM(loggedUser);

            reportService = new ReportService(report,
                    oem,
                    filterServiceLocal,
                    loggedUser);

            report = reportService.getReport();

            //  NOTE:
            //  Some 1% reports can have an actOnEntity while others do not

        
                // <editor-fold defaultstate="collapsed" desc="Prepare Report Parameters">
                Map reportParameters = new HashMap();

                //  to be used in case of Master Detail report...
                BaseEntity headerObject = null;
                List<String> headerFieldsLabels = new ArrayList<String>();
                List<String> headerFieldsClassNames = new ArrayList<String>();
                List<String> headerFieldsExpressions = new ArrayList<String>();

                //  if it is a Master Detail report...
                if (report instanceof MasterDetailReport) {
                    MasterDetailReport masterDetailReport = (MasterDetailReport) report;

                    // <editor-fold defaultstate="collapsed" desc="Header Object">
                    //  Load Header Object Attributes
                    String headerObjectClassName = masterDetailReport.getHeaderObjectEntity().getEntityClassName();
                    String headerObjectDBID = (String) request.getSession().getAttribute("headerObjectDBID");

                    //  Load Header Object from Database
                    List<HeaderField> headerFields = oem.loadEntityList("HeaderField", 
                            Collections.singletonList("headerFieldReport.dbid=" + masterDetailReport.getDbid()), 
                            null, null, loggedUser);
//                    List<HeaderField> headerFields = masterDetailReport.getHeaderFields();
                    if (headerObjectClassName != null && !headerObjectClassName.equals("")
                            && headerObjectDBID != null && !headerObjectDBID.equals("")) {
                        headerObject = reportService.loadHeaderObject(
                                headerObjectClassName,
                                headerObjectDBID);
                    }
                    List<String> fieldExps = new ArrayList<String>();
                    for (HeaderField headerField : headerFields) {
                        fieldExps.add(headerField.getHeaderFieldExpression());
                    }
                    headerObject = oem.loadEntityDetail(headerObject, null, fieldExps, loggedUser);

                    // </editor-fold>

                    // <editor-fold defaultstate="collapsed" desc="Prepare Header Fields">

                    // <editor-fold defaultstate="collapsed" desc="Sort Header Fields">
                    //  Comparator
                    Comparator headerFieldsComparator = new Comparator() {

                        public int compare(Object o1, Object o2) {
                            HeaderField mdf1 = (HeaderField) o1;
                            HeaderField mdf2 = (HeaderField) o2;
                            return mdf1.getSortIndex().compareTo(mdf2.getSortIndex());
                        }
                    };

                    //  Sort
                    Collections.sort(headerFields, headerFieldsComparator);
                    // </editor-fold>

                    // <editor-fold defaultstate="collapsed" desc="Populate Header Fields">
                    for (HeaderField headerField : headerFields) {
                        //  Header Field Label
                        headerFieldsLabels.add(headerField.getLabel());

                        //  Header Field Expression
                        String exp = headerField.getHeaderFieldExpression();
                        headerFieldsExpressions.add(exp);

                        //  get field representing last item in field expression
                        //  to obtain class name
                        if (exp != null && !exp.equals("")) {
                            try {
                                Field fld = null;
                                List<ExpressionFieldInfo> expressionFieldInfos = BaseEntity.parseFieldExpression(headerObject.getClass(), exp);
                                ExpressionFieldInfo expressionFieldInfo = expressionFieldInfos.get(expressionFieldInfos.size() - 1);
                                fld = expressionFieldInfo.field;
                                Class clazz = fld.getType();
                                clazz = ReportService.convertToClassCounterpart(clazz);
                                headerFieldsClassNames.add(clazz.getName());
                            } catch (Exception ex) {
                                // <editor-fold defaultstate="collapsed" desc="LOG">
                                logger.error("Exception thrown",ex);

                                headerFieldsClassNames.add(String.class.getName());
                            }
                        } else {
                            headerFieldsClassNames.add(String.class.getName());
                        }
                    }
                    // </editor-fold>

                    // </editor-fold>

                    // <editor-fold defaultstate="collapsed" desc="Add Header Fields to Report Parameters">
                    if (headerObject != null) {
                        //
                        //  The header fields will be added to parameters passed
                        //  to the JasperReport
                        //
                        //  In the upcoming section "Prepare Report Template",
                        //  text fields will be added to the report header that
                        //  will use these passed header fields parameters
                        //
                        for (int i = 0; i < headerFieldsExpressions.size(); i++) {
                            String headerFieldExpression = headerFieldsExpressions.get(i);
                            try {
                                if (headerFieldExpression != null && !headerFieldExpression.equals("")) {
                                    //  obtain header value
                                    Object headerFieldValue = headerObject.invokeGetter(headerFieldExpression);

                                    //  set obtained value
                                    reportParameters.put("param" + i, headerFieldValue);
                                } else {
                                    //  pass empty value
                                    reportParameters.put("param" + i, "");
                                }
                            } catch (NoSuchMethodException ex) {
                                logger.error("Exception thrown",ex);

                                //  pass empty value
                                reportParameters.put("param" + i, "");
                            }
                        }
                    }
                    // </editor-fold>
                }

                reportParameters.put("reportTitle", report.getTitle());
                reportParameters.put("date", timeZoneService.getUserCDT(loggedUser));

                /* generate report logo dynamically by selecting
                 * the image from the database
                 * if the report has a specific logo selected
                 * during the creation of the report
                 * this specific logo will be added to the report
                 *
                 * if there is no logo  for the report
                 * the default logo of the system  will be
                 * the default logo
                 */
                byte[] logo = null;
                //check if the report has a spicif logo
                if (report.getLogo() != null && report.getLogo().getImage() != null) {
                    //if specific logo was uploaded get it
                    logo = report.getLogo().getImage();
                } else {
                    /* if there is no specific logo
                     * get the default logo with the name DEAFULTLOGO.JPEG
                     * from the OImage entity
                     */

                    List<String> conditions = new ArrayList<String>();
                    conditions.add("name = 'DEAFULTLOGO.JPEG'");
                    logo = ((OImage) oem.loadEntity(OImage.class.getSimpleName(), conditions, null, loggedUser)).getImage();

                }
                //at the end of all condition add the logo as parameter for report
                reportParameters.put("logo", logo);

                // </editor-fold>

                // <editor-fold defaultstate="collapsed" desc="Load Report Result Set">
                ArrayList<BaseEntity> rs = null;
                ArrayList<BaseEntity> rsAssociateList = null;

                if (report instanceof MasterDetailReport) {
                    rs = (ArrayList<BaseEntity>) reportService.retrieveDetail(headerObject, userSessionInfo);
                } else {
                    //tabular
                    rs = (ArrayList<BaseEntity>) reportService.loadReportResultSet(userSessionInfo);
                }
                // </editor-fold>

                //  if no data was loaded from database...
                if (rs == null || rs.size() == 0) {
                    jasperPrint = buildEmptyReport(getServletContext(),
                            report,
                            loggedUser);
                    
                     //buildBirtEmptyReport(getServletContext(), request,  response);
                } else {
                    //  Note: Comparison crosstab reports are considered to be
                    //        crosstab reports as well
                    if (report instanceof CrossTabReport) {
                        rsAssociateList = (ArrayList<BaseEntity>) reportService.loadAssociateList();
                    }

                    //  if it is a comparison crosstab report
                    if (report instanceof ComparisonCrossTabReport) //  process result set before filling the report with it
                    {
                        rs = (ArrayList<BaseEntity>) reportService.preRunProcessResultSet(rs);
                    }

                    //  if it is an 80% report...
                        // <editor-fold defaultstate="collapsed" desc="Create 80% Report">
                        // Load the report fields from database, incase of lazy loaded relations:
                        List<OReportField> reportFields = report.getFields();
                        List<String> fieldExps = new ArrayList<String>();
                        List<BaseEntity> tmpList = new ArrayList<BaseEntity>();
                        for (OReportField reportField : reportFields) {
                            fieldExps.add(reportField.getFieldExpression());
                        }

                        for (BaseEntity result : rs) {
                            tmpList.add(oem.loadEntityDetail(result, null, fieldExps, loggedUser));
                        }

                        // Replace current 'rs' with the now loaded from DB:
                        rs.clear();
                        rs.addAll(tmpList);
                        ReportDS ds = new ReportDS(rs, oem, loggedUser);
                        if (rsAssociateList != null) {
                            CrossTabReport crosstabReport = (CrossTabReport) report;
                            String groupByEntityAttribute = crosstabReport.getGroupByEntityAttribute();    //  e.g. payroll.costCenter
                            if (groupByEntityAttribute != null && !groupByEntityAttribute.equals("")) {
                                List<BaseEntity> detailrs = rs;     //  e.g. Employee
                                rs = new ArrayList<BaseEntity>();   //  e.g. CostCenter

                                for (BaseEntity baseEntity : detailrs) {
                                    BaseEntity groupByEntity = (BaseEntity) baseEntity.invokeGetter(groupByEntityAttribute);
                                    if (!rs.contains(groupByEntity)) {
                                        rs.add(groupByEntity);
                                    }
                                }
                                ds = new ReportDS(rs, detailrs, rsAssociateList, groupByEntityAttribute, oem, loggedUser);
                            } else {
                                ds = new ReportDS(rs, rsAssociateList, oem, loggedUser);
                            }
                        }
                        
                        
                        
                        dynamicReport = new ReportGenerator(report,
                                reportService,
                                getServletContext(),
                                headerFieldsLabels,
                                headerFieldsClassNames,
                                ddServiceRemote,
                                oem,
                                loggedUser).buildReport();

                        jasperPrint = DynamicJasperHelper.generateJasperPrint(
                                dynamicReport, new ClassicLayoutManager(), ds, reportParameters);
                        // </editor-fold>
                    }
                    ReportService reportService1 = new ReportService(report, oem, filterServiceLocal, loggedUser);
                    reportService1.exportReport(jasperPrint, dynamicReport, outputFormat, request, response);
    
        } catch (Exception ex) {
logger.error("Exception thrown",ex);
        } finally {
            logger.debug("Returning");
            oem.closeEM(loggedUser);
        }
    }
      public static JasperPrint buildEmptyReport(ServletContext Application,
            OReport Report,
            OUser LoggedUser) {
          logger.debug("Entering");
        String emptyTemplateFile = Application.getRealPath("/empty.jrxml");

        JasperPrint jasperPrint = null;
        try {
            JasperReport jasperReport = JasperCompileManager.compileReport(emptyTemplateFile);
            JRDataSource JRDS = null;
            jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap(), JRDS);
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
logger.debug("Returning");
        return jasperPrint;
    }
      
      public  void buildBirtEmptyReport(ServletContext Application,
              HttpServletRequest request, HttpServletResponse response) {
          logger.debug("Entering");
        String emptyTemplateFile = Application.getRealPath("/birtEmptyReport.rptdesign");

        
        String fileName = "/report/tmp" + new Date().getTime() + ".pdf";
        String filepath = Application.getRealPath(fileName);
     
        IReportRunnable design = null;
        IReportEngine birtReportEngine;
        IPlatformContext context = new PlatformServletContext(getServletContext());
        birtReportEngine = BirtEngine.getBirtEngine(context);

        try {
            //Open report design
            design = birtReportEngine.openReportDesign(emptyTemplateFile);
            
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        
        //create task to run and render report
        IRunAndRenderTask task = birtReportEngine.createRunAndRenderTask(design);
  
        //task.setParameterValue("Title", "Text");
        PDFRenderOption renderOption = new PDFRenderOption();
        renderOption.setOutputFileName(filepath);
        renderOption.setOutputFormat(PDFRenderOption.OUTPUT_FORMAT_PDF);
        
        try {
            renderOption.setOutputStream(response.getOutputStream());
        } catch (IOException ex) {
            logger.error("Exception thrown",ex);
        }
        task.setRenderOption(renderOption);

        try {
            //run report
            task.run();
        } catch (EngineException ex) {
            logger.error("Exception thrown",ex);
        }
        task.close();
        request.getSession().setAttribute("reportPath", fileName);
        response.setHeader("Content-Disposition", "inline; filename=" + filepath);
        request.setAttribute("reportType", "BIRT");
        response.setContentType("application/pdf");
        try {
            
          response.sendRedirect("birtReport.jsp");
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        finally{
            logger.debug("Returning");
        }
        
    }
      
      public  void buildBirtReportXXX(  ServletContext Application,
                                        HttpServletRequest request, 
                                        HttpServletResponse response ,
                                        byte[] logo ,
                                        List<String> fExps ,
                                        String query) {
          
          //here i tried to enhance the query
          String qr="select " ;
          for (String fieldExpression : fExps)
          {
              qr+=fieldExpression;
          }
        String fileName = "/report/tmp" + new Date().getTime() + ".pdf";
        String filepath = getServletContext().getRealPath(fileName);
        //**********************************************************************
        DesignConfig config = new DesignConfig( );
	IDesignEngine engine = null;
        
	try{
		Platform.startup( config );
		IDesignEngineFactory factory = (IDesignEngineFactory) Platform.
                        createFactoryObject( IDesignEngineFactory.EXTENSION_DESIGN_ENGINE_FACTORY );
		engine = factory.createDesignEngine( config );

	}catch( Exception ex){
		logger.error("Exception thrown",ex);
	}
        
	SessionHandle session = engine.newSessionHandle( ULocale.ENGLISH ) ;
	reportDesignHandle = session.createDesign();
	elementFactory = reportDesignHandle.getElementFactory( );
        //**********************************************************************
        IReportRunnable design = null;
        IReportEngine birtReportEngine;
        IPlatformContext context = new PlatformServletContext(getServletContext());
        birtReportEngine = BirtEngine.getBirtEngine(context);
        String templateFile = Application.getRealPath("/birtEmptyFirstTemplate.rptdesign");  
        
        IRunAndRenderTask task = birtReportEngine.createRunAndRenderTask(design);
 
        //Task.setParameterValue("Tiltle", "Text");
        //**********************************************************************
          try {

              createDataSources();
              buildDataSet(query);
              
              TableHandle table = elementFactory.newTableItem("table", fExps.size(),1,1,1);
              table.setWidth("100%");
              table.setDataSet(reportDesignHandle.findDataSet("DataSet"));

              PropertyHandle computedSet = table.getColumnBindings();
              ComputedColumn cs1 = null;

              for (int j = 0; j < fExps.size(); j++) {
                  cs1 = StructureFactory.createComputedColumn();
                  cs1.setName((String) fExps.get(j).toLowerCase());
                  cs1.setExpression("dataSetRow[\"" + (String) fExps.get(j).toLowerCase()+ "\"]");
                  computedSet.addItem(cs1);
              }

              // table header
              RowHandle tableheader = (RowHandle) table.getHeader().get(0);

              for (int j = 0; j < fExps.size(); j++) {
                  LabelHandle label1 = elementFactory.newLabel((String) fExps.get(j));
                  label1.setText((String) fExps.get(j));
                  CellHandle cell = (CellHandle) tableheader.getCells().get(j);
                  cell.getContent().add(label1);
              }
              
              // table detail
              RowHandle tabledetail = (RowHandle) table.getDetail().get(0);

              for (int j = 0; j < fExps.size(); j++) {
                  CellHandle cell = (CellHandle) tabledetail.getCells().get(j);
                  DataItemHandle data = elementFactory.newDataItem("data_" + (String) fExps.get(j) );
                  data.setResultSetColumn((String) fExps.get(j).toLowerCase());
                  cell.getContent().add(data);
              }

              reportDesignHandle.getBody( ).add( table );

          } catch (Exception ex) {
              logger.error("Exception thrown",ex);
          } 

        //**********************************************************************
        //i dont need to save the template  after adding datasource and dataset
        //i should comment the next five lines after testing purposes
        try {
            reportDesignHandle.saveAs(templateFile);
        } catch (IOException ex) {
            logger.error("Exception thrown",ex);
        }
        //the previous five lines should be commented
        
        try {
            design=null;
            design = birtReportEngine.openReportDesign(templateFile);
            task=null;
            task = birtReportEngine.createRunAndRenderTask(design);
        } catch (EngineException ex) {
            logger.error("Exception thrown",ex);
        }

        PDFRenderOption renderOption = new PDFRenderOption();
        renderOption.setOutputFileName(filepath);
        renderOption.setOutputFormat(PDFRenderOption.OUTPUT_FORMAT_PDF);
        
        try {
            renderOption.setOutputStream(response.getOutputStream());
        } catch (IOException ex) {
            logger.error("Exception thrown",ex);
        }
        task.setRenderOption(renderOption);

        try {
            task.run();    
        } catch (EngineException ex) {
            logger.error("Exception thrown",ex);
        }
        task.close();
        request.getSession().setAttribute("reportPath", fileName);
        response.setHeader("Content-Disposition", "inline; filename=" + filepath);
        request.setAttribute("reportType", "BIRT");
        response.setContentType("application/pdf");
        try {
          response.sendRedirect("birtReport.jsp");
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
logger.debug("Returning");
    }
      
    private void createDataSources() throws SemanticException
	{
            OdaDataSourceHandle dsHandle = elementFactory.newOdaDataSource("Data Source", "org.eclipse.birt.report.data.oda.jdbc");
            dsHandle.setProperty("odaDriverClass", loggedUser.getTenant().getConnDriver());
            dsHandle.setProperty("odaURL", loggedUser.getTenant().getConnURL());//"jdbc:mysql://1.1.1.189:3306/otms152fri?characterEncoding=UTF-8");
            dsHandle.setProperty("odaUser", loggedUser.getTenant().getConnUser());//"root");
            dsHandle.setProperty("odaPassword",  loggedUser.getTenant().getConnPassword());//"root");
            reportDesignHandle.getDataSources().add(dsHandle);
	}
      
     private void buildDataSet(String query) throws SemanticException
	{
            	OdaDataSetHandle dsHandle = elementFactory.newOdaDataSet( "DataSet","org.eclipse.birt.report.data.oda.jdbc.JdbcSelectDataSet" );
		dsHandle.setDataSource( "Data Source" );
		dsHandle.setQueryText( query );
		reportDesignHandle.getDataSets( ).add( dsHandle );
	}
     
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "This servlet is responsible for producing reports in FABS.";
    }// </editor-fold>
}
