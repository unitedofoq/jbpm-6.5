/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.servlet;

import com.unitedofoq.fabs.core.content.AttachmentDTO;
import com.unitedofoq.fabs.core.content.db.DBAttachment;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserServiceRemote;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;


import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MEA
 */
@WebServlet(name = "ViewAttachmentServlet", urlPatterns = {"/ViewAttachmentServlet"})
public class ViewAttachmentServlet extends HttpServlet {

    private OUser loggedUser;
    @EJB
    private UserServiceRemote userServiceRemote;
    final static Logger logger = LoggerFactory.getLogger(ViewAttachmentServlet.class);
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    //<editor-fold defaultstate="collapsed" desc="CustomReports Handling">
        AttachmentDTO entityAttachment = (AttachmentDTO) request.getSession().getAttribute("EntityAttachment");
        loggedUser = (OUser) request.getSession().getAttribute("loggedUser");
        String whereConditions = (String) request.getSession().getAttribute("Filters");
        viewAttachment(entityAttachment, request, response);
        //</editor-fold>
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="View Attachment">
    
    
    void viewAttachment(AttachmentDTO attachment,
            HttpServletRequest request,
            HttpServletResponse response) {
        logger.debug("Entering");
        OutputStream outStream = null;
        String inputFilePath = getServletContext().getRealPath(
                "/" );//report/report
        inputFilePath = inputFilePath + new Date().getTime() + attachment.getName();
        try {
            File reportDir = new File(getServletContext().getRealPath("/"));//report
            reportDir.mkdir();

            File file2 = new File(inputFilePath);
            outStream = new FileOutputStream(file2);
            byte[] buffer = new byte[4096];
            buffer = (byte[]) attachment.getAttachment();
            int length = buffer.length;
            outStream.write(buffer, 0, length);
            if (outStream != null) {
                outStream.close();
            }
            logger.debug("File Created..");
        } catch (IOException ex) {
            logger.error("Exception thrown: ",ex);
        }
        if(inputFilePath.lastIndexOf("/") !=-1)
        {
            request.getSession().setAttribute("reportPath",
                    "/" + inputFilePath.substring(inputFilePath.lastIndexOf("/")));//report
        }else if(inputFilePath.lastIndexOf("\\") !=-1)
        {
            request.getSession().setAttribute("reportPath", "/" + inputFilePath.substring(inputFilePath.lastIndexOf("\\")+1));//report/
        }
        
        response.setHeader("Content-Disposition", "inline; filename=" + inputFilePath);
        request.setAttribute("reportType", "BIRT");
        try {
            response.sendRedirect("viewAttachment.jsp");
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        }
    }

    
    //</editor-fold>
}
