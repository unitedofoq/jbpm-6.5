/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

/**
 *
 * @author aelzaher
 */
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "ChangeUserPassword")
@ViewScoped
public class ChangeUserPasswordBean extends FormBean {

    @Override
    protected String getBeanName() {
        return "ChangeUserPassword";
    }

    @Override
    @PostConstruct
    public void init() {
        super.init();
    }
}
