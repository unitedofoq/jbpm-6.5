/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.listener;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Web application lifecycle listener.
 *
 * @author Hammad
 */
public class FABSSessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext externalContext = context.getExternalContext();
        String maximumNumberOfViewsPerSession = externalContext.getInitParameter("MaximumNumberOfViewsPerSession");
        se.getSession().setAttribute("com.sun.faces.application.view.activeViewMapsSize", Integer.parseInt(maximumNumberOfViewsPerSession));

    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {}
}
