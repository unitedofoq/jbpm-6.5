/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;


import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author nkamal
 */
@FacesConverter(value = "FABSDecimalConverter", forClass =UIComponent.class)
public class FABSDecimalConverter implements Converter, Serializable {

    int decimalDigits = 0;

    public FABSDecimalConverter(int decimalDigits) {
        this.decimalDigits = decimalDigits;
    }

    public FABSDecimalConverter() {
    }

    //  Called when an text is written in the screen field. 'label' will
    //  always contain the 'value' .
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String label) {

        return label;
    }

    //  Called when displaying input text with a converter. 'object' will
    // always contain the value 
    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object object) {

        // if corresponding label was not found...
        DecimalFormat df = new DecimalFormat("#0");
        df.setMaximumFractionDigits(decimalDigits);
        df.setMinimumFractionDigits(decimalDigits);
        if (object == null || object.toString().isEmpty()) {
            return df.format(0);
        }
        BigDecimal decimalvalue = new BigDecimal(object.toString());
        String formattedDecimalValue = df.format(decimalvalue);

        return formattedDecimalValue;
    }
}
