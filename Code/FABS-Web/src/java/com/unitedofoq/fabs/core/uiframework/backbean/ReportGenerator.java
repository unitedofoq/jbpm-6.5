package com.unitedofoq.fabs.core.uiframework.backbean;

import ar.com.fdvs.dj.domain.DJCalculation;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.builders.GroupBuilder;
import ar.com.fdvs.dj.domain.constants.Border;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.GroupLayout;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.Transparency;
import ar.com.fdvs.dj.domain.constants.VerticalAlign;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;
import ar.com.fdvs.dj.domain.entities.columns.PropertyColumn;
import ar.com.fdvs.dj.domain.entities.conditionalStyle.ConditionalStyle;
import ar.com.fdvs.dj.domain.entities.conditionalStyle.StatusLightCondition;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;

import com.unitedofoq.fabs.core.report.ComparisonCrossTabReport;
import com.unitedofoq.fabs.core.report.CrossTabField;
import com.unitedofoq.fabs.core.report.CrossTabReport;
import com.unitedofoq.fabs.core.report.OReport;
import com.unitedofoq.fabs.core.report.OReportField;
import com.unitedofoq.fabs.core.report.OReportGroup;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.awt.Color;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReportGenerator {
final static Logger logger = LoggerFactory.getLogger(ReportGenerator.class);
    private OReport report;
    private ReportService reportService;
    private ServletContext application;
    private List<String> headerFieldsLabels;
    private List<String> headerFieldsClassNames;
    private DDServiceRemote ddServiceRemote;
    private OEntityManagerRemote oem;
    private OUser loggedUser;
    private Style columnHeaderStyle;    //  Column Header Style
    private Style columnDetailStyle;    //  Column Detail Style
    private Style groupHeaderStyle;     //  Group Header Style
    private Style groupDetailStyle;     //  Group Detail Style
    //  to be used in the creation of the 'Grand Total' column
    private List<String> crosstabTotalsColNames;
    //  to be used in making the names of the 'TOTAL' columns unique
    private int crosstabTotalIndex;

    /**
     * CONSTRUCTOR
     *
     * @param report The report to be generated
     * @param reportService The reportService to be used throughout the report
     * generation process
     * @param application The servlet context to be used for determining correct
     * locations of the jasper template files to be used
     * @param headerFieldsLabels The labels of the header fields (Master-Detail
     * Reports Only)
     * @param headerFieldsClassNames The class names of the values to be
     * supplied to the header fields (Master-Detail Reports Only)
     * @param ddServiceRemote To be used for retrieving DDs
     * @param oem To be used in retrieving the SYSTEM user of 'loggedUser'
     * @param loggedUser The logged user who is requesting the report
     */
    public ReportGenerator(OReport report,
            ReportService reportService,
            ServletContext application,
            List<String> headerFieldsLabels,
            List<String> headerFieldsClassNames,
            DDServiceRemote ddServiceRemote,
            OEntityManagerRemote oem,
            OUser loggedUser) {
        this.report = report;
        this.reportService = reportService;
        this.application = application;
        this.headerFieldsLabels = headerFieldsLabels;
        this.headerFieldsClassNames = headerFieldsClassNames;
        this.ddServiceRemote = ddServiceRemote;
        this.oem = oem;
        this.loggedUser = loggedUser;
    }

    /**
     * Builds the report
     *
     * @return The generated report in the form of a DynamicReport object
     *
     * @throws java.lang.Exception
     */
    public DynamicReport buildReport() throws Exception {

        // <editor-fold defaultstate="collapsed" desc="COMPARATORS">
        //  used for sorting report 'fields' using their sort index
        Comparator fieldsComparator = new Comparator() {
            public int compare(Object o1, Object o2) {
                OReportField rf1 = (OReportField) o1;
                OReportField rf2 = (OReportField) o2;
                return rf1.getSortIndex().compareTo(rf2.getSortIndex());
            }
        };

        //  used for sorting report 'groups' using their sort index
        Comparator groupsComparator = new Comparator() {
            public int compare(Object o1, Object o2) {
                OReportGroup g1 = (OReportGroup) o1;
                OReportGroup g2 = (OReportGroup) o2;
                return g1.getGroupField().getSortIndex().compareTo(
                        g2.getGroupField().getSortIndex());
            }
        };
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="COMMON">
        FastReportBuilder drb = new FastReportBuilder();

        //  Report Columns that will constitute the report
        List<AbstractColumn> reportColumns = new ArrayList<AbstractColumn>();

        //  Style for Group Footer's Aggregate Value
        Style aggregateValueStyle = new Style();
        aggregateValueStyle.setFont(Font.ARIAL_SMALL_BOLD);
        aggregateValueStyle.setVerticalAlign(VerticalAlign.TOP);
        aggregateValueStyle.setBorderTop(Border.THIN);
        aggregateValueStyle.setPattern("###0.00");

        //  Report's Act-on Entity
        Class cls = null;
        if (report.getOactOnEntity() != null) {
            cls = Class.forName(report.getOactOnEntity().getEntityClassPath());
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="DJ GROUP BUILDERS">
        List<OReportGroup> groups = report.getGroups();
        Collections.sort(groups, groupsComparator);
        Map groupBuilders = new HashMap();
        for (OReportGroup group : groups) {
            GroupBuilder gb = new GroupBuilder()
                    .setGroupLayout(GroupLayout.VALUE_FOR_EACH);

            groupBuilders.put(group, gb);
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="COLUMNS">

        /* Get fields to be displayed, and sort by 'sortIndex' */
        List<OReportField> columnFields = report.getFields();
        Collections.sort(columnFields, fieldsComparator);

        //  index for the total columns of crosstab fields
        crosstabTotalIndex = 0;

        //  for storing the names of the total columns of crosstab fields
        crosstabTotalsColNames = new ArrayList<String>();

        columnHeaderStyle = new Style("columnHeaderStyle");   //  Column Header Style
        columnDetailStyle = new Style("columnDetailStyle");   //  Column Detail Style
        groupHeaderStyle = new Style("groupHeaderStyle");     //  Group Header Style
        groupDetailStyle = new Style("groupDetailStyle");     //  Group Detail Style

        Font columnHeaderFont = new Font();
        columnHeaderFont.setFontSize(8);
        columnHeaderFont.setFontName("Arial");
        columnHeaderFont.setPdfFontName("Arial.ttf");
        columnHeaderFont.setPdfFontEncoding(Font.PDF_ENCODING_Identity_H_Unicode_with_horizontal_writing);
        columnHeaderFont.setPdfFontEmbedded(true);
        columnHeaderFont.setBold(true);
        columnHeaderFont.setItalic(false);
        columnHeaderFont.setUnderline(false);

        //  columnHeaderStyle.setFont(font);
        Style customedColumnHeaderStyle = new Style();
        customedColumnHeaderStyle.setHorizontalAlign(HorizontalAlign.RIGHT);
        customedColumnHeaderStyle.setVerticalAlign(VerticalAlign.MIDDLE);
        customedColumnHeaderStyle.setBackgroundColor(Color.decode("#CCCCFF"));
        customedColumnHeaderStyle.setTextColor(Color.BLACK);
//        customedColumnHeaderStyle.setBorderColor(Color.decode("#fff");
        customedColumnHeaderStyle.setBorder(Border.NO_BORDER);
        customedColumnHeaderStyle.setTransparency(Transparency.OPAQUE);
        customedColumnHeaderStyle.setFont(columnHeaderFont);

        Font columnDataFont = new Font();
        columnDataFont.setFontSize(8);
        columnDataFont.setFontName("Arial");
        columnDataFont.setPdfFontName("Arial.ttf");
        columnDataFont.setPdfFontEncoding(Font.PDF_ENCODING_Identity_H_Unicode_with_horizontal_writing);
        columnDataFont.setPdfFontEmbedded(true);
        columnDataFont.setBold(true);
        columnDataFont.setItalic(false);
        columnDataFont.setUnderline(false);

        //  columnHeaderStyle.setFont(font);
        Style customedColumnDataStyle = new Style();
        customedColumnDataStyle.setHorizontalAlign(HorizontalAlign.RIGHT);
        customedColumnDataStyle.setVerticalAlign(VerticalAlign.MIDDLE);
        customedColumnDataStyle.setBackgroundColor(Color.decode("#FFFFFF"));
        customedColumnDataStyle.setTextColor(Color.BLACK);
//        customedColumnDataStyle.setBorderColor(Color.decode("#fff");
        customedColumnDataStyle.setBorder(Border.NO_BORDER);
        customedColumnDataStyle.setTransparency(Transparency.OPAQUE);
        customedColumnDataStyle.setFont(columnHeaderFont);
       
        Font reportTitleFont = new Font();
        reportTitleFont.setFontSize(22);
        reportTitleFont.setFontName("Arial");
        reportTitleFont.setPdfFontName("Arial.ttf");
        reportTitleFont.setPdfFontEncoding(Font.PDF_ENCODING_Identity_H_Unicode_with_horizontal_writing);
        reportTitleFont.setPdfFontEmbedded(true);
        reportTitleFont.setBold(true);
        reportTitleFont.setItalic(false);
        reportTitleFont.setUnderline(false);

        //  columnHeaderStyle.setFont(font);
        Style reportTitleStyle = new Style();
        reportTitleStyle.setHorizontalAlign(HorizontalAlign.CENTER);
        reportTitleStyle.setVerticalAlign(VerticalAlign.MIDDLE);
        reportTitleStyle.setBackgroundColor(Color.decode("#FFFFFF"));
        reportTitleStyle.setTextColor(Color.BLACK);
//        reportTitleStyle.setBorderColor(Color.decode("#fff");
        reportTitleStyle.setBorder(Border.NO_BORDER);
        reportTitleStyle.setTransparency(Transparency.OPAQUE);
        reportTitleStyle.setFont(reportTitleFont);

        /* Adding column fields to 'reportColumns' */
        boolean firstCrosstabFieldPassed = false;
        for (OReportField field : columnFields) {
            // <editor-fold defaultstate="collapsed" desc="Regular Field">
            //  if it is a regular field (i.e. not a crosstab field)
            if (!(field instanceof CrossTabField)) {
//                //  Effect of the above piece of code
//                //  * Crosstab Reports: 'associateField' to NOT be displayed
//                //  * Comparison Report: 'associateField' to be displayed

                //  get the field representing the last item in the
                //  field expression
                Field fld = null;
                List<ExpressionFieldInfo> expressionFieldInfos = BaseEntity.parseFieldExpression(cls, field.getFieldExpression());
                ExpressionFieldInfo expressionFieldInfo = expressionFieldInfos.get(expressionFieldInfos.size() - 1);
                fld = expressionFieldInfo.field;

                //  get the field's type to assign to the report column
                Class clazz = fld.getType();

                //  if it is a primitive, convert it to its "class" counterpart
                clazz = ReportService.convertToClassCounterpart(clazz);

                //  determine whether or not the parent class (i.e. first item
                //  in the field expression) is a 'List'
                boolean isList = false;
                boolean isGroupField = false;

                //  determine whether or not the current field is used
                //  in grouping.
                for (OReportGroup group : groups) {
                    if (group.getGroupField().equals(field)) {
                        isGroupField = true;
                        break;
                    }
                }

                //  NOTE:
                //  a field cannot be used for gropuing if its parent class
                //  is a 'List'

                //  if the current field is not used in grouping...
                if (!isGroupField) {
                    Class firstItemCls = expressionFieldInfos.get(0).field.getType();
                    isList = firstItemCls.getName().endsWith("List");
                }

                //  Column Header Name
                String colName = fld.getName();

                //  if there is a title override...
                if (field.getDdTitleOverride() != null && !field.getDdTitleOverride().equals("")) //  use the title override
                {
                    colName = field.getDdTitleOverride();
                } //  otherwise, get the column header name from the corresponding DD
                else {
                    try {
                        String ddName = BaseEntity.getFieldDDName(cls, fld.getName());
                        DD dd = ddServiceRemote.getDD(ddName, oem.getSystemUser(loggedUser));
                        if (dd != null && dd.getLabel() != null && !dd.getLabel().equals("")) {
                            colName = dd.getLabel();
                        }
                    } catch (Exception ex) {
                        logger.error("Exception thrown",ex);
                    }
                }

                //  Column Width
                Integer columnWidth = field.getWidth();

                //  Field Display Expression
                String fieldDescription = (isList) ? "LIST" : "";

                //  Field Display Expression
                if (field.getFieldDisplayExpression() != null) {
                    fieldDescription += field.getFieldDisplayExpression();
                }

                //  Column Specifications
                ColumnBuilder columnBuilder = ColumnBuilder.getNew()
                        .setColumnProperty(field.getFieldExpression(), clazz)
                        .setTitle(colName)
                        .setHeaderStyle(customedColumnHeaderStyle)
                        .setStyle(customedColumnDataStyle)
                        .setWidth(columnWidth)
                        .setFieldDescription(fieldDescription);

                //  if the current field is used for grouping...
                if (isGroupField) {
                    columnBuilder = ColumnBuilder.getNew()
                            .setColumnProperty(field.getFieldExpression(), clazz)
                            .setTitle(colName)
                            .setHeaderStyle(customedColumnHeaderStyle)
                            .setStyle(customedColumnDataStyle)
                            .setWidth(columnWidth)
                            .setFieldDescription(fieldDescription);
                }

                if (clazz == Date.class) {
                    columnBuilder.setPattern("dd/MM/yy");
                }

                //  Field Column to be added to report
                AbstractColumn fieldColumn = columnBuilder.build();

                //  Make use of the 'name' attribute of 'fieldColumn'
                //  to pass the 'property' name through
                fieldColumn.setName(field.getFieldExpression());

                //  Add to 'reportColumns'
                reportColumns.add(fieldColumn);

                //  is the current field to be aggregated or used in grouping?
                for (int i = 0; i < groups.size(); i++) {
                    OReportGroup group = groups.get(i);
                    //  Grouping
                    if (isGroupField && group.getGroupField().equals(field)) {
                        GroupBuilder gb = (GroupBuilder) groupBuilders.get(group);
                        gb.setCriteriaColumn((PropertyColumn) fieldColumn);
                        groupBuilders.put(group, gb);
                    }
                }
            } // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Crosstab Field">
            //  if it is a crosstab field
            else if (field instanceof CrossTabField) {
                CrossTabReport crosstabReport = (CrossTabReport) report;
                CrossTabField crosstabField = (CrossTabField) field;

                //  if 'report' is a comparison crosstab report...
                if (report instanceof ComparisonCrossTabReport) {
                    //  if this is the first crosstab field...
                    if (!firstCrosstabFieldPassed) {
                        crosstabReport.setAssociateFieldColumnWidth(crosstabField.getWidth());
                        AbstractColumn associateColumn = createAssociateFieldColumn();
                        String associateFieldAttr = crosstabReport.getAssociateFieldAttribute();
                        associateColumn.setName(associateFieldAttr);

                        //  Add to 'reportColumns'
                        reportColumns.add(associateColumn);

                        firstCrosstabFieldPassed = true;
                    }
                }

                //  Effect of the above piece of code
                //  * Crosstab Reports: 'associateField' to NOT be displayed
                //  * Comparison Report: 'associateField' to be displayed

                //  Load data required for creating columns of the
                //  crosstab field
                List<BaseEntity> crossTabCreationResultSet =
                        reportService.loadCrossTabResultSetForFieldsCreation(crosstabField);
                reportService.addCrossTabField(field.getFieldExpression(), crossTabCreationResultSet.size());
                //  Load data used to fill the crosstab field's columns
                List<BaseEntity> crossTabFillingResultSet =
                        reportService.loadCrossTabResultSetForFilling();

                //  the class of the data used for filling
                Class fillClazz;

                //  if the there is data retrieved for filling...
                if (crossTabFillingResultSet.size() != 0) {
                    BaseEntity fillEntity = crossTabFillingResultSet.get(0);
                    Class classs = fillEntity.getClass();
                    Field fld = BaseEntity.getClassField(classs, crosstabReport.getFieldExpression());
                    fillClazz = fld.getType();
                    fillClazz = ReportService.convertToClassCounterpart(fillClazz);

                    if (crossTabCreationResultSet.size() != 0) {
                        List<String> crosstabColNames = new ArrayList<String>();

                        for (BaseEntity entity : crossTabCreationResultSet) {
                            String columnPropertyName = crosstabReport.getRelEntityAttribute()
                                    + "_"
                                    + crosstabReport.getCrossEntityAttribute()
                                    + "_"
                                    + crosstabReport.getFieldExpression()
                                    + "_"
                                    + entity.getDbid();

                            //  Column Width
                            Integer columnWidth = crosstabField.getWidth();

                            if (crosstabReport.getAssociateField() != null) {
                                String associateField = crosstabReport.getAssociateFieldAttribute();
                                int dotIndex = associateField.indexOf(".");
                                associateField = associateField.substring(0, dotIndex);
                                columnPropertyName += "_" + associateField;
                            }

                            crosstabColNames.add(columnPropertyName);

                            //  Field Display Expression
                            String fieldDescription = "CROSSTAB";
                            if (crosstabField.getFieldDisplayExpression() != null) {
                                fieldDescription += crosstabField.getFieldDisplayExpression();
                            }

                            //  Create Column
                            ColumnBuilder columnBuilder = ColumnBuilder.getNew();

                            columnBuilder.setColumnProperty(columnPropertyName, fillClazz)
                                    .setTitle((String) entity.invokeGetter(crosstabReport.getLabelExpression()))
                                    .setHeaderStyle(columnHeaderStyle)
                                    .setStyle(columnDetailStyle)
                                    .setWidth(columnWidth)
                                    .setFieldDescription(fieldDescription);

                            if (report instanceof ComparisonCrossTabReport) {
                                columnBuilder.addConditionalStyles(createAbsoluteConditionalStyles());
                            }

                            if (fillClazz == Date.class) {
                                columnBuilder.setPattern("dd/MM/yy");
                            }

                            AbstractColumn fieldColumn = columnBuilder.build();

                            //  Make use of the 'name' attribute of 'fieldColumn'
                            //  to pass the 'property' name through
                            fieldColumn.setName(columnPropertyName);

                            //  Add to 'reportColumns'
                            reportColumns.add(fieldColumn);

                            //  Global Footer Variable for Column
                            Style globalFooterStyle = new Style();
                            globalFooterStyle.setHorizontalAlign(HorizontalAlign.LEFT);
                            globalFooterStyle.setBackgroundColor(Color.GRAY);
                            globalFooterStyle.setFont(Font.ARIAL_SMALL_BOLD);
                            globalFooterStyle.setPattern("###0.00");
                            drb.addGlobalFooterVariable(fieldColumn, DJCalculation.SUM, globalFooterStyle);
                            drb.setGrandTotalLegend("");

                            //  NOTE:
                            //  Cross tab fields cannot used for grouping

                            //  is the current field to be aggregated?
                            for (OReportGroup group : groups) {
                                //  Aggregation
                                GroupBuilder gb = (GroupBuilder) groupBuilders.get(group);
                                gb.addFooterVariable(fieldColumn, DJCalculation.SUM, aggregateValueStyle, null, null);
                                groupBuilders.put(group, gb);
                            }
                        }

                        if (crosstabField.isViewTotalColumn()) {
                            //  Create the 'TOTAL' Column for the crosstab field
                            AbstractColumn fieldColumn = createTotalsColumn(crosstabColNames,
                                    crosstabField.getWidth());

                            //  Add to 'reportColumns'
                            reportColumns.add(fieldColumn);

                            //  is the current field to be aggregated?
                            for (OReportGroup group : groups) {
                                //  Aggregation
                                GroupBuilder gb = (GroupBuilder) groupBuilders.get(group);
                                gb.addFooterVariable(fieldColumn, DJCalculation.SUM, aggregateValueStyle, null, null);
                                groupBuilders.put(group, gb);
                            }

                            //  Global Footer Variable for Column
                            Style globalFooterStyle = new Style();
                            globalFooterStyle.setHorizontalAlign(HorizontalAlign.LEFT);
                            globalFooterStyle.setBackgroundColor(Color.GRAY);
                            globalFooterStyle.setFont(Font.ARIAL_SMALL_BOLD);
                            globalFooterStyle.setPattern("###0.00");
                            drb.addGlobalFooterVariable(fieldColumn, DJCalculation.SUM, globalFooterStyle);
                            drb.setGrandTotalLegend("");
                        }
                    }
                }
            }
            // </editor-fold>
        }

        if (report instanceof CrossTabReport) {
            CrossTabReport crossTabReport = (CrossTabReport) report;
            if (crossTabReport.isViewGrandTotalColumn()) {
                //  Create the 'TOTAL' Column for the crosstab field
                AbstractColumn fieldColumn = createTotalsColumn(crosstabTotalsColNames,
                        50);

                //  Add to 'reportColumns'
                reportColumns.add(fieldColumn);

                //  is the current field to be aggregated?
                for (OReportGroup group : groups) {
                    //  Aggregation
                    GroupBuilder gb = (GroupBuilder) groupBuilders.get(group);
                    gb.addFooterVariable(fieldColumn, DJCalculation.SUM, aggregateValueStyle, null, null);
                    groupBuilders.put(group, gb);
                }

                //  Global Footer Variable for Column
                Style globalFooterStyle = new Style();
                globalFooterStyle.setHorizontalAlign(HorizontalAlign.LEFT);
                globalFooterStyle.setBackgroundColor(Color.GRAY);
                globalFooterStyle.setFont(Font.ARIAL_SMALL_BOLD);
                globalFooterStyle.setPattern("###0.00");
                drb.addGlobalFooterVariable(fieldColumn, DJCalculation.SUM, globalFooterStyle);
                drb.setGrandTotalLegend("");
            }
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="BUILD REPORT">
        //  Acquire report template
        String templateFile = reportService.createTemplateFile(application,
                headerFieldsLabels,
                headerFieldsClassNames);

        // Update template according to Report Specification
        drb.setTemplateFile(templateFile);

//        //  Use styles that exists in jrxml file
//        Style columnHeaderStyle = new Style("columnHeaderStyle");   //  Column Style
//        Style columnDetailStyle = new Style("columnDetailStyle");   //  Column Detail Style
//        drb.setDefaultStyles(null, null, columnHeaderStyle, columnDetailStyle);

//        //  Add Image Banner  (i.e. Image in Report Header)
//        drb.addFirstPageImageBanner("/otms/logo.jpg", new Integer(90), new Integer(31), ImageBanner.ALIGN_RIGHT);

        //  Add Page Footer (Page X of Y)
//        drb.addAutoText(AutoText.AUTOTEXT_PAGE_X_SLASH_Y,
//                        AutoText.POSITION_FOOTER,
//                        AutoText.ALIGNMENT_RIGHT);

        //  Resize the columns' widths proportionally to meet the page width
        drb.setUseFullPageWidth(true);

        //  Add report columns
        for (AbstractColumn reportColumn : reportColumns) {
            drb.addColumn(reportColumn);
        }

        //  Add DJ groups
        for (OReportGroup group : groups) {
            GroupBuilder gb = (GroupBuilder) groupBuilders.get(group);
            gb.setGroupLayout(GroupLayout.VALUE_IN_HEADER_WITH_HEADERS)
                    .setReprintHeaderOnEachPage(Boolean.TRUE);
            drb.addGroup(gb.build());
        }
        //  Build and return the report template
        drb.setTitle(report.getTitle());
        drb.setTitleStyle(reportTitleStyle);
        logger.debug("Returning");
        return drb.build();
        // </editor-fold>

    }

    /**
     * Creates the associate field column.
     *
     * There is a transient field that needs to be filled before this function
     * is called: CrossTabReport.java :: associateFieldColumnWidth
     *
     * @return The associate field column
     */
    private AbstractColumn createAssociateFieldColumn() {
        CrossTabReport crosstabReport = (CrossTabReport) report;
        String associateFieldAttr = crosstabReport.getAssociateFieldAttribute();

        // <editor-fold defaultstate="collapsed" desc="Field Expression">
        String fieldExpression = associateFieldAttr;
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Field Description">
        String fieldDescription = "ASSOCIATE";
        int dotIndex = associateFieldAttr.indexOf(".");
        fieldDescription += associateFieldAttr.substring(dotIndex + 1);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column Header Name">
        String colName = crosstabReport.getAssociateFieldAttribute();

        //  if there is a title override...
        if (crosstabReport.getAssociateFieldTitleOverride() != null
                && !crosstabReport.getAssociateFieldTitleOverride().equals("")) //  use the title override
        {
            colName = crosstabReport.getAssociateFieldTitleOverride();
        } //  otherwise, get the column header name from the corresponding DD
        else {
            try {
                String ddName = BaseEntity.getFieldDDName(crosstabReport.getAssociateField(), fieldDescription);
                DD dd = ddServiceRemote.getDD(ddName, oem.getSystemUser(loggedUser));
                if (dd != null && dd.getLabel() != null && !dd.getLabel().equals("")) {
                    colName = dd.getLabel();
                }
            } catch (Exception ex) {
                logger.error("Exception thrown",ex);
            }
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Create Associate Field Column">
        AbstractColumn fieldColumn = null;

        //  Column Specifications
        ColumnBuilder columnBuilder = ColumnBuilder.getNew()
                .setColumnProperty(fieldExpression, String.class)
                .setTitle(colName)
                .setHeaderStyle(columnHeaderStyle)
                .setStyle(columnDetailStyle)
                .setWidth(crosstabReport.getAssociateFieldColumnWidth())
                .setFieldDescription(fieldDescription);

        try {
            //  Field Column to be added to report
            fieldColumn = columnBuilder.build();
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        // </editor-fold>
logger.debug("Returning");
        return fieldColumn;
    }

    /**
     * Creates conditional styles for displaying the 'absolute' values of the
     * numerical values instead of their actual values
     *
     * @return The conditional styles to be applied on the numerical values to
     * display their 'absolute' values
     */
    private ArrayList createAbsoluteConditionalStyles() {
        logger.trace("Entering");
        //  Create styles for +ve & -ve values
        Style defaultStyle = new Style();
        defaultStyle.setPattern("###0.00");

        Style switchSignStyle = new Style();
        switchSignStyle.setPattern("-###0.00;###0.00");

        //  Create conditions for when to apply each of the styles
        StatusLightCondition negative = new StatusLightCondition(new Double(-9999999999.00), new Double(-0.00000001));
        StatusLightCondition positive = new StatusLightCondition(new Double(-0.00000001), new Double(9999999999.00));

        //  Create the conditional styles
        ArrayList conditionalStyles = new ArrayList();
        conditionalStyles.add(new ConditionalStyle(negative, switchSignStyle));
        conditionalStyles.add(new ConditionalStyle(positive, defaultStyle));
logger.trace("Returning");
        return conditionalStyles;
    }

    /**
     * Creates a 'TOTAL' column for the intended columns.
     *
     * @param crosstabColNames The names of the columns to be considered in the
     * 'TOTAL' column. The values in these columns will aggregated in the
     * 'TOTAL' column to be created.
     * @param width The width of the 'TOTAL' column
     *
     * @return The 'TOTAL' column
     *
     * @throws ar.com.fdvs.dj.domain.builders.ColumnBuilderException
     */
    private AbstractColumn createTotalsColumn(List<String> crosstabColNames,
            int width) {
logger.trace("Entering");
        //  the 'TOTAL' column
        AbstractColumn fieldColumn = null;

        try {
            String fieldDescription = "TOTAL";
            for (int i = 0; i < crosstabColNames.size(); i++) {
                fieldDescription += crosstabColNames.get(i);
                if (i != crosstabColNames.size() - 1) {
                    fieldDescription += "|";
                }
            }

            //  create a unique name for the 'TOTAL' column
            String colPropertyName = "CrossTabTotal" + crosstabTotalIndex;

            //  create the 'TOTAL' column
            ColumnBuilder columnBuilder = ColumnBuilder.getNew();
            columnBuilder.setColumnProperty(colPropertyName, Double.class)
                    .setFieldDescription(fieldDescription)
                    .setTitle("Total")
                    .setHeaderStyle(columnHeaderStyle)
                    .setStyle(columnDetailStyle)
                    .setWidth(width)
                    .setPattern("###0.00");

            //  record the names of the 'TOTAL' columns that are
            //  getting created to be used in the 'Grand Total' column
            crosstabTotalsColNames.add(colPropertyName);

            //  increment 'crosstabTotalIndex'
            crosstabTotalIndex++;

            fieldColumn = columnBuilder.build();
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
logger.trace("Returning");
        return fieldColumn;
    }
}
