/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.report.customReports.CustomReportDynamicFilter;

import com.unitedofoq.fabs.core.report.customReports.CustomReportFilter;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MEA
 */
@ManagedBean(name = "CustomReportFilterFieldsTabularBean")
@ViewScoped
public class CustomReportFilterFieldsTabularBean extends TabularBean {
final static Logger logger = LoggerFactory.getLogger(CustomReportFilterFieldsTabularBean.class);
    OFunctionResult oFR = new OFunctionResult();
    
    
    protected UIFrameworkServiceRemote uiFrameWorkFacade = OEJB.lookup(UIFrameworkServiceRemote.class);

    @Override
    protected String getBeanName() {
        return "CustomReportFilterFieldsTabularBean";
    }

    /**
     *
     */
    @Override
    public void init() {
        logger.debug("Entering");
        try {
                    super.init();
            String viewName ="";
            CustomReportFilter filter;
            if (dataMessage.getData().get(0) instanceof CustomReportDynamicFilter) {
                filter = ((CustomReportDynamicFilter) dataMessage.getData().get(0)).getCustomReport().getCustomReportFilter();
            } else {
                filter = (CustomReportFilter) dataMessage.getData().get(0);
            }
            viewName = filter.getViewName();
            if (viewName != null && !viewName.equals("")) {
                List<String> fieldsNames;
                fieldsNames = uiFrameWorkFacade.getFiledsNamesInView(viewName, loggedUser);
                if (fieldsNames != null && !fieldsNames.isEmpty()) {
                    String lookUpFilterStr;
                    lookUpFilterStr = "";
                    for (int count = 0; count < fieldsNames.size(); count++) {
                        lookUpFilterStr += fieldsNames.get(count) + "," + fieldsNames.get(count);
                        if (count != (fieldsNames.size() - 1)) {
                            lookUpFilterStr += ";";
                        }
                    }
                    for (ScreenField field : getCurrentScreen().getScreenFields()) {
                        if (field.getFieldExpression().equals("fieldName")) {
                            field.getDd().setLookupFilter(lookUpFilterStr);
//                            field.getDd().setLookupFilterTranslated(lookUpFilterStr);
                            break;
                        }
                    }
                }
                
                else
                {
                  for (ScreenField field : getCurrentScreen().getScreenFields()) {
                        if (field.getFieldExpression().equals("fieldName")) {
                            field.getDd().setLookupFilter(" , ");
//                            field.getDd().setLookupFilterTranslated(" , ");
                            break;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
            messagesPanel.clearAndDisplayMessages(oFR);
        }
        finally{
            logger.debug("Returning");
        }
    }

    /**
     *
     * @param entityName
     * @param conditions
     * @param sortConditions
     * @return
     */
    @Override
    protected List<BaseEntity> loadList(
            String entityName, List<String> conditions, List<String> sortConditions) {
        return super.loadList(entityName, conditions, sortConditions);
    }
}
