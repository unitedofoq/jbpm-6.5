/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.utils;

/**
 * The CSS style classes that are used in the different JSF managed beans or classes on FABS-Web.
 * 
 * @author lap
 */
public final class CSSStaticConstants {
    private CSSStaticConstants() {}
    
    public static final String OTMS_LABEL_CLASS = "OTMSlabel";
    public static final String FILTER_LABEL_CLASS = "filterLabel";
    public static final String FILTER_COLLAPSIBLE_CLASS = "filterCollapsible";
    public static final String RUN_BUTTON_CLASS = "run_btn";
    public static final String MIDDLE_PANEL_STYLE = "text-align:right; padding-left:50px; padding-right:50px;";
    public static final String BUTTON_PANEL_STYLE = "float:right; margin-right: 20px;";
    
}
