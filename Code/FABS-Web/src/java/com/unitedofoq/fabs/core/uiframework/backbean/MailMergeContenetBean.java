/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.filter.OFilterServiceLocal;

import com.unitedofoq.fabs.core.mailmerge.MailMergeBasic;
import com.unitedofoq.fabs.core.mailmerge.MailMergeContent;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author 3dly
 */
@ManagedBean(name = "MailMergeContenetBean")
@ViewScoped
public class MailMergeContenetBean extends FormBean {


    private OFilterServiceLocal filterService = OEJB.lookup(OFilterServiceLocal.class);
    String userMessage = "";

    @Override
    protected String getBeanName() {
        return "MailMergeContenetBean";
    }
    private ArrayList<String> entityList = new ArrayList<String>();
    private ArrayList<String> entityPropertiesList = new ArrayList<String>();
    private String propertyName = "";
    private String contentValue = "";
    private BaseEntity actOnEntity;
    private String fieldExp;
    private String insertButtonName;

    @Override
    protected boolean onInit(Map<String, String> requestParams) {
        logger.trace("Entering");
        try {
            super.onInit(requestParams);
            insertButtonName = ddService.getDDLabel("InsertButton", loggedUser);

            MailMergeBasic mailMergeBasic = null;
            mailMergeBasic = (MailMergeBasic) dataMessage.getData().get(0);
            contentValue = ((MailMergeContent) oem.loadEntity(
                    MailMergeContent.class.getSimpleName(), Collections.singletonList(
                    "mailMergeBasic.dbid=" + mailMergeBasic.getDbid()), null, loggedUser)).getTemplateContent();
logger.trace("Returning with True");
            return true;
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        logger.trace("Returning with False");
        return false;
    }

    public String getInsertButtonName() {
        return insertButtonName;
    }

    public void setInsertButtonName(String insertButtonName) {
        this.insertButtonName = insertButtonName;
    }

    public String getContentValue() {
        return contentValue;
    }

    public void setContentValue(String contentValue) {
        this.contentValue = contentValue;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public ArrayList<String> getEntityPropertiesList() {
        return entityPropertiesList;
    }

    public void setEntityPropertiesList(ArrayList<String> entityPropertiesList) {
        this.entityPropertiesList = entityPropertiesList;
    }
    private String entityName = "";

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public void setEntityList(ArrayList<String> entityList) {
        this.entityList = entityList;
    }

    public ArrayList<String> getEntityList() {
        return entityList;
    }

    public String getFieldExp() {
        return fieldExp;
    }

    public void setFieldExp(String fieldExp) {
        this.fieldExp = fieldExp;
    }

    public void getEntityProperties() {
        try {
            entityPropertiesList = new ArrayList<String>();
            propertyName = "";

            Class entityClass = Class.forName("entity." + entityName);
            PropertyDescriptor[] propertyDescriptors =
                    Introspector.getBeanInfo(entityClass).getPropertyDescriptors();

            for (PropertyDescriptor propertyDescriptor1 : propertyDescriptors) {

                entityPropertiesList.add(propertyDescriptor1.getName());
            }
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
    }

    public void insertProperty() {
        String exp = ((MailMergeContent) loadedObject).getFieldExpression();
        setFieldExp(exp);
    }

    public String saveAction() {
        try {
            MailMergeBasic mailMergeBasic;
            MailMergeContent mailMergeContent = null;
            mailMergeBasic = (MailMergeBasic) dataMessage.getData().get(0);

            mailMergeContent = ((MailMergeContent) oem.loadEntity(
                    MailMergeContent.class.getSimpleName(), Collections.singletonList(
                    "mailMergeBasic.dbid=" + mailMergeBasic.getDbid()), null, loggedUser));

            if (null == mailMergeContent) {//new mailMerge Contents
                mailMergeContent = new MailMergeContent();
                mailMergeBasic.setMailMergeContent(mailMergeContent);
                mailMergeContent.setMailMergeBasic(mailMergeBasic);

                mailMergeContent.setTemplateContent(getContentValue());
                String exp = ((MailMergeContent) loadedObject).getFieldExpression();
                setFieldExp(exp);

                oem.saveEntity(mailMergeContent, loggedUser);
            } else { //update mailMergeContente
                mailMergeContent.setTemplateContent(getContentValue());
                oem.merge(mailMergeContent, loggedUser);
            }
            userMessage = "Changes Saved Successfully";
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            userMessage = "Changes Cannot be Saved";
        }
        return userMessage;
    }

    public String getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }
}
