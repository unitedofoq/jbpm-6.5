/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.backbean.wizard;

import com.unitedofoq.fabs.core.dd.DDServiceRemote; 
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import org.primefaces.component.commandbutton.CommandButton;

/**
 *
 * @author mmohamed
 */
public class WizardPanel {

    public WizardPanel() {
    }
    
    private String beanName;
    HtmlPanelGrid wizardPanel;
    private ExpressionFactory exFactory = null;
    private ELContext elContext = null;

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }
    private CommandButton backButton;
    private CommandButton nextButton;
    private CommandButton skipButton;
    private CommandButton finishButton;

    public HtmlPanelGrid createWizardPanel(String beanName, ExpressionFactory exFactory, ELContext elContext,
            ScreenWizardInformation info, boolean disableNext, DDServiceRemote ddService, OUser loggedUser){
        
        this.beanName = beanName;
        this.elContext = elContext;
        this.exFactory = exFactory;

        wizardPanel = new HtmlPanelGrid();
        wizardPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()+"WizardPanel");

        wizardPanel.setColumns(4);
        wizardPanel.setCellpadding("5px");
        wizardPanel.setCellspacing("0px");

        backButton = new CommandButton();
        backButton.setId("_WizardBack");
        
        backButton.setValue(" ");
 //       backButton.setValue(ddService.getDD("backButton", loggedUser).getLabelTranslated());
        backButton.setAlt(ddService.getDD("backButton", loggedUser).getLabelTranslated());
        backButton.setTitle(ddService.getDD("backButton", loggedUser).getLabelTranslated());
        backButton.setStyleClass("wizard_back");
        backButton.setImage("/resources/wizard_img_btn.png");
        backButton.setDisabled(!info.isEnableBack());
        backButton.setActionExpression(exFactory.createMethodExpression(elContext,
                    "#{" + getBeanName() + ".wizardBackAction}", String.class, new Class[0]));
        wizardPanel.getChildren().add(backButton);

        nextButton = new CommandButton();
        nextButton.setId("_WizardNext");
        nextButton.setStyleClass("wizard_next");
        nextButton.setImage("/resources/wizard_img_btn.png");
        nextButton.setValue(" ");
//        nextButton.setValue(ddService.getDD("nextButton", loggedUser).getLabelTranslated());
        nextButton.setTitle(ddService.getDD("nextButton", loggedUser).getLabelTranslated());
        nextButton.setAlt(ddService.getDD("nextButton", loggedUser).getLabelTranslated());
        
        nextButton.setActionExpression(exFactory.createMethodExpression(elContext,
                "#{" + getBeanName() + ".wizardNextAction}", String.class, new Class[0]));
        
        if(info.isEnableNext() && disableNext && info.getStep().isMustSelect()) {
        
            nextButton.setValueExpression("disabled",
                    exFactory.createValueExpression(
                    elContext,
                    "#{" + beanName + ".rowSelected == false}",
                    boolean.class));
        }else{
            nextButton.setDisabled(!info.isEnableNext());
        }
        nextButton.setImmediate(false);
        //nextButton.setPartialSubmit(true);
        //nextButton.setAjax(true);
        //nextButton.setUpdate("MainLayout");
        //nextButton.setProcess("@form");
        //nextButton.setType("submit");
        wizardPanel.getChildren().add(nextButton);

        skipButton = new CommandButton();
        skipButton.setId("_WizardSkip");
        skipButton.setValue(" ");
//        skipButton.setValue(ddService.getDD("skipButton", loggedUser).getLabelTranslated());
        skipButton.setAlt(ddService.getDD("skipButton", loggedUser).getLabelTranslated());
        skipButton.setTitle(ddService.getDD("skipButton", loggedUser).getLabelTranslated());
        skipButton.setImage("/resources/wizard_img_btn.png");
        skipButton.setStyleClass("wizard_skip");
        skipButton.setDisabled(!info.isEnableSkip());
        skipButton.setActionExpression(exFactory.createMethodExpression(elContext,
                    "#{" + getBeanName() + ".wizardSkipAction}", String.class, new Class[0]));
        wizardPanel.getChildren().add(skipButton);

        finishButton = new CommandButton();
        finishButton.setId("_WizardFinish");
        finishButton.setImage("/resources/wizard_img_btn.png");
        finishButton.setStyleClass("wizard_exit");
        finishButton.setValue(" ");
//        finishButton.setValue(ddService.getDD("finishButton", loggedUser).getLabelTranslated());
        finishButton.setAlt(ddService.getDD("finishButton", loggedUser).getLabelTranslated());
        finishButton.setTitle(ddService.getDD("finishButton", loggedUser).getLabelTranslated());
        finishButton.setDisabled(!info.isEnableFinish());
        finishButton.setActionExpression(exFactory.createMethodExpression(elContext,
                    "#{" + getBeanName() + ".wizardFinishAction}", String.class, new Class[0]));
        wizardPanel.getChildren().add(finishButton);

        wizardPanel.setStyle("vertival-align: left; float:left; margin-left: 0px;");
        
        return wizardPanel;
    }

    public ELContext getElContext() {
        return elContext;
    }

    public void setElContext(ELContext elContext) {
        this.elContext = elContext;
    }

    public ExpressionFactory getExFactory() {
        return exFactory;
    }

    public void setExFactory(ExpressionFactory exFactory) {
        this.exFactory = exFactory;
    }

    public HtmlPanelGrid getWizardPanel() {
        return wizardPanel;
    }

    public void setWizardPanel(HtmlPanelGrid wizardPanel) {
        this.wizardPanel = wizardPanel;
    }

    public void disbleButtons(){
        backButton.setDisabled(true);
        nextButton.setDisabled(true);
        skipButton.setDisabled(true);
        finishButton.setDisabled(true);
    }
    
}
