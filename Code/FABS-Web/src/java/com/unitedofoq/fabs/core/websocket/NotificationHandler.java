/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.websocket;

import com.unitedofoq.fabs.core.notification.Notification;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.websocket.Session;

/**
 *
 * @author mmasoud
 */
@ApplicationScoped
public class NotificationHandler {

    private final Set<WebSocketSession> webSocketSessions = Collections.synchronizedSet(new HashSet<WebSocketSession>());
    
    void addSession(WebSocketSession webSocketSession) {
        webSocketSessions.add(webSocketSession);
    }

    public void removeSession(Session session) {
        synchronized(webSocketSessions) {
            Iterator<WebSocketSession> iterator = webSocketSessions.iterator();
            while(iterator.hasNext()) {
                WebSocketSession webSocketSession = iterator.next();
                if(webSocketSession.getSession().getId().equals(session.getId())) {
                    iterator.remove();
                }
            }
        }
    }

    public void sendNotification(Notification notification) {
        synchronized(webSocketSessions) {
            Iterator<WebSocketSession> iterator = webSocketSessions.iterator();
            while(iterator.hasNext()) {
                WebSocketSession webSocketSession = iterator.next();
                if(webSocketSession.getUserLoginName().equals(notification.getUserLoginName())) {
                    try {
                        webSocketSession.getSession().getBasicRemote().sendText(notification.toJson().toString());
                    } catch (IOException ex) {
                        iterator.remove();
                        Logger.getLogger(NotificationHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
    
}
