/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import java.io.Serializable;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author mibrahim
 */
@FacesConverter(value = "URLConverter", forClass = HtmlOutputLabel.class)
public class URLConverter implements Converter, Serializable {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value != null && value.contains("http")) {
            value += "<a href=\"" + value
                    + "\" target=\"_blank\">" + value
                    + "</a>";
        }
        return value;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != null && value.toString().contains("http")) {
            //String tmp = value.toString();
            String tmp = "<a href=\"" + value
                    + "\" target=\"_blank\">" + value
                    + "</a>";
            value = tmp;
        }
        return value.toString();
    }
}
