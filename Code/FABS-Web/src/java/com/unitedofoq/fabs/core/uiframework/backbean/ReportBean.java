/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ComputedField;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.i18n.Translation;

import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.uiframework.screen.TabularScreen;
import com.unitedofoq.fabs.core.uiframework.screen.report.ReportScreenField;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.persistence.Transient;
import org.primefaces.component.column.Column;
import org.primefaces.component.columngroup.ColumnGroup;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.panel.Panel;
import org.primefaces.component.row.Row;

/**
 *
 * @author mostafa
 */
@ManagedBean(name = "ReportBean")
@ViewScoped
public class ReportBean extends TabularBean {

    private int totalCount = 0;

    @Override
    public void init() {
        super.init();
        OScreen scr = currentScreen;
    }

    @Override
    protected String getBeanName() {
        return "ReportBean";
    }

    @Override
    public HtmlPanelGrid buildInnerScreen() {
        processInput();
        Panel middlePanel = new Panel();
        middlePanel.getChildren().add(buildTable("", "", ""));
        HtmlPanelGrid grid = new HtmlPanelGrid();
        grid.getChildren().add(middlePanel);
        return grid;
    }

    @Override
    protected boolean processInput() {
        boolean result = super.processInput();
        dataSort = new ArrayList<String>();
        List<String> sortExp = new ArrayList<String>();
        String sortExpress = ((TabularScreen) getCurrentScreen()).getSortExpression();
        if (sortExpress != null && !sortExpress.equals("")) {
            String exps[] = sortExpress.split(",");
            for (int i = 0; i < exps.length; i++) {
                String string = exps[i].trim();
                sortExp.add(string);
            }
        }
        for (String sortExpression : sortExp) {
            if (!sortExpression.contains(" ")) {
                if (sortExpression.startsWith("lower(")) {
                    sortExpression = sortExpression.trim().substring(6, sortExpression.lastIndexOf(")"));
                }
                if (!dataSort.contains(sortExpression)) {
                    dataSort.add(sortExpression);
                }
            } else {
                if (sortExpression.startsWith("lower(")) {
                    sortExpression = sortExpression.trim().substring(6, sortExpression.lastIndexOf(")"));
                    if (!dataSort.contains(sortExpression)) {
                        dataSort.add(sortExpression);
                    }
                }
            }
        }
        updateDataFilter();
        if (loadedList != null && !loadedList.isEmpty()) {
            totalCount = loadedList.size();
        } else {
            totalCount = 0;
        }
        return result;

    }

    @Override
    protected List<BaseEntity> loadList(
            String entityName, List<String> conditions, List<String> sortConditions) {
        logger.trace("Entering");
        try {
            // <editor-fold defaultstate="collapsed" desc="Update conditions with ScreenDataLoadingUEConditions">
            OFunctionResult dataLoadingUEConditions = uiFrameWorkFacade.getScreenDataLoadingUEConditions(
                    currentScreen, headerObject, conditions, dataMessage, loggedUser);
            if (dataLoadingUEConditions.getErrors().isEmpty()) {
                conditions.clear();
                conditions.addAll((List) dataLoadingUEConditions.getReturnedDataMessage().getData().get(0));
            }
            //</editor-fold> 

            List<String> screenFieldExpressions = new ArrayList<String>();

            // <editor-fold defaultstate="collapsed" desc="Fill screenFieldExpressions from screenFields">
            Iterator screenFieldsIterator = screenFields.iterator();
            while (screenFieldsIterator.hasNext()) {
                screenFieldExpressions.add(
                        ((ScreenField) screenFieldsIterator.next()).getFieldExpression());
            }
            // </editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Sorting">
            for (int i = 0; i < sortConditions.size(); i++) {
                String sortCondition = sortConditions.get(i);
                String sortField = sortCondition;
                String newSortCondition = "";
                int index = sortCondition.indexOf(" ");
                if (index != -1) {
                    sortField = sortField.substring(0, sortField.indexOf(" "));
                }
                newSortCondition = sortCondition.replace(sortField, "lower(" + sortField + ")");
                Field field;
                Class actOnClass = actOnEntityCls;
                while (sortField.contains(".")) {
                    String currentField = sortField.substring(0, sortField.indexOf("."));
                    sortField = sortField.substring(currentField.length() + 1);
                    field = BaseEntity.getClassField(actOnClass, currentField);
                    if (field != null) {
                        actOnClass = field.getType();
                    }
                }
                field = BaseEntity.getClassField(actOnClass, sortField);
                if (field != null) {
                    if (field.getType().getName().contains("String")) {
                        sortConditions.remove(i);
                        sortConditions.add(i, newSortCondition);
                    }
                    if (field.getAnnotation(Translation.class) != null
                            || (field.getAnnotation(Transient.class) != null
                            && field.getAnnotation(ComputedField.class) == null)) {
                        sortConditions.remove(i);
                    }
                }
            }
            //</editor-fold>
            logger.trace("Returning");
            return oem.loadEntityList(
                    entityName, conditions, screenFieldExpressions,
                    sortConditions,
                    getLoggedUser());
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            logger.trace("Returning");
            return new ArrayList<BaseEntity>();
        }

    }

    @Override
    public DataTable buildTable(String bindingExpression,
            String tableVariableExpression, String valueChangeMethod) {
        logger.debug("Entering");
        try {
            // <editor-fold defaultstate="collapsed" desc="Define Table and Its Properties">
            DataTable table = new DataTable();
            table.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_generalTable");
            String noRecordsMsgDD = ddService.getDDLabel("noRecordsMsg", loggedUser);
            table.setEmptyMessage(noRecordsMsgDD);
            table.setValueExpression("value",
                    exFactory.createValueExpression(elContext,
                    "#{" + getBeanName() + ".loadedList}", ArrayList.class));
            table.setVar("currentRow");
            table.setStyleClass("dt-table");
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="get groupby fields">
            HashMap<String, ReportScreenField> groupByFields = new HashMap<String, ReportScreenField>();
            List<ReportScreenField> hasNotGroupByFieldList = new ArrayList<ReportScreenField>();
            for (ScreenField scrFld : getCurrentScreen().getScreenFields()) {
                ReportScreenField field = (ReportScreenField) scrFld;
                if (field.getGroupByField() != null) {
                    groupByFields.put(String.valueOf(field.getGroupByField().getDbid()), field.getGroupByField());
                } else {
                    hasNotGroupByFieldList.add(field);
                }
            }
            //</editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Iterate has not group by fields to build columns">
            for (int screenFieldIndex = 0; screenFieldIndex < hasNotGroupByFieldList.size(); screenFieldIndex++) {
                ReportScreenField screenField = (ReportScreenField) hasNotGroupByFieldList.get(screenFieldIndex);

                String id = FramworkUtilities.getID(screenField, screenField.getDd(), screenField.getSortIndex());
                Column column = new Column();
                column.setStyle("text-align: left !important;vertical-align: top!important ");
                column.setId(id);
                // <editor-fold defaultstate="collapsed" desc="create column header">
                HtmlOutputLabel headerLabel = new HtmlOutputLabel();
                headerLabel.setId("columnHeader" + id);
                if (screenField.getDdTitleOverrideTranslated() != null
                        && !"".equals(screenField.getDdTitleOverrideTranslated())) {
                    headerLabel.setValue(screenField.getDdTitleOverrideTranslated());
                    column.setHeader(headerLabel);
                } else if (null != screenField.getDdTitleOverride()
                        && !screenField.getDdTitleOverride().equals("")) {
                    headerLabel.setValue(screenField.getDdTitleOverride());
                    column.setHeader(headerLabel);
                } else {
                    headerLabel.setValue(screenField.getDd().getHeaderTranslated());
                    column.setHeader(headerLabel);
                }
                // </editor-fold> 
                HtmlOutputLabel columnLbl = new HtmlOutputLabel();
                columnLbl.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_ColmnLbl");
                columnLbl.setValueExpression("value",
                        exFactory.createValueExpression(elContext,
                        "#{currentRow." + screenField.getFieldExpression() + "}", String.class));
                column.getChildren().add(columnLbl);
                table.getChildren().add(column);
                if (groupByFields.get(String.valueOf(screenField.getDbid())) != null) {
                    Column columnEmptyInner = new Column();
                    columnEmptyInner.setHeaderText(" ");
                    columnEmptyInner.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "innerEmptyCol");
                    //create sub table
                    List<ReportScreenField> groupedScreenFields = null;
                    groupedScreenFields = oem.loadEntityList(ReportScreenField.class.getSimpleName(),
                            Collections.singletonList("groupByField.dbid = " + screenField.getDbid()),
                            null, Collections.singletonList("sortIndex"), loggedUser);
                    if (groupedScreenFields != null) {
                        DataTable subTable = createInnerTable(groupByFields, "currentRow", groupedScreenFields,
                                screenField.getFieldExpression());
                        if (subTable != null) {
                            columnEmptyInner.getChildren().add(subTable);
                            table.getChildren().add(columnEmptyInner);
                        }
                    }
                }
            }
            // create count aggregation function
            ColumnGroup countGrp = new ColumnGroup();
            countGrp.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_countColmn");
            countGrp.setType("footer");
            Row countRow = new Row();
            countRow.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_parentCountRow");
            Column parentCountCol = new Column();
            parentCountCol.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_totslCountCol");
            parentCountCol.setValueExpression("footerText",
                    exFactory.createValueExpression(elContext,
                    "Count : "+"#{" + getBeanName() + ".totalCount}", String.class));
            countRow.getChildren().add(parentCountCol);
            countGrp.getChildren().add(countRow);
            table.getChildren().add(countGrp);
            logger.debug("Returning");
            return table;
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            logger.debug("Returning with Null");
            return null;
        }
        //</editor-fold>
    }

    private DataTable createInnerTable(HashMap<String, ReportScreenField> groupByFields, String valueExpression,
            List<ReportScreenField> fieldsToBeCreated, String groupedByFieldExp) {
        logger.trace("Entering");
        try {
            // <editor-fold defaultstate="collapsed" desc="Define Table and Its Properties">
            DataTable subTable = new DataTable();
            subTable.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_innerTable");
            String noRecordsMsgDD = ddService.getDDLabel("noRecordsMsg", loggedUser);
            subTable.setEmptyMessage(noRecordsMsgDD);
            subTable.setStyleClass("dt-table");
            String exp = fieldsToBeCreated.get(0).getFieldExpression();
            String columnListName = "";
            if(groupedByFieldExp.contains(".")){
                groupedByFieldExp = groupedByFieldExp.substring(0,groupedByFieldExp.lastIndexOf("."));
                int listIndex5 = exp.indexOf(groupedByFieldExp)+groupedByFieldExp.length()+1;
                columnListName = exp.substring(listIndex5);
                if (columnListName.contains(".")) {
                    columnListName = columnListName.substring(0,columnListName.indexOf("."));
                } else {
                    columnListName = exp;
                }

            }else{
               columnListName = exp.substring(0,exp.indexOf("."));
            }
            
            String subTableVar = valueExpression + columnListName;
            subTable.setVar(subTableVar);
            subTable.setValueExpression("value",
                    exFactory.createValueExpression(elContext,
                    "#{" + valueExpression + "." + columnListName + "}", Object.class));

            //</editor-fold>
            //to add column group for aggregation
            ColumnGroup countGrp = new ColumnGroup();
            countGrp.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_countInnerColmn");
            countGrp.setType("footer");
            Row countRow = new Row();
            countRow.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_parentInnerCountRow");
            for (int fieldIndex = 0; fieldIndex < fieldsToBeCreated.size(); fieldIndex++) {
                Column column = new Column();
                String colId = FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "Inner"
                        + fieldsToBeCreated.get(fieldIndex).getSortIndex();
                column.setId(colId);
                column.setStyle("text-align: left !important;");
                // <editor-fold defaultstate="collapsed" desc="create column header">
                HtmlOutputLabel headerLabel = new HtmlOutputLabel();
                headerLabel.setId("columnHeader" + colId);
                if (fieldsToBeCreated.get(fieldIndex).getDdTitleOverrideTranslated() != null
                        && !"".equals(fieldsToBeCreated.get(fieldIndex).getDdTitleOverrideTranslated())) {
                    headerLabel.setValue(fieldsToBeCreated.get(fieldIndex).getDdTitleOverrideTranslated());
                    column.setHeader(headerLabel);
                } else if (null != fieldsToBeCreated.get(fieldIndex).getDdTitleOverride()
                        && !fieldsToBeCreated.get(fieldIndex).getDdTitleOverride().equals("")) {
                    headerLabel.setValue(fieldsToBeCreated.get(fieldIndex).getDdTitleOverride());
                    column.setHeader(headerLabel);
                } else {
                    headerLabel.setValue(fieldsToBeCreated.get(fieldIndex).getDd().getHeaderTranslated());
                    column.setHeader(headerLabel);
                }
                // </editor-fold>
                HtmlOutputLabel columnLbl = new HtmlOutputLabel();
                columnLbl.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "subInner"
                        + fieldsToBeCreated.get(fieldIndex).getSortIndex());
                String expValue = fieldsToBeCreated.get(fieldIndex).getFieldExpression();
                int listIndex1 = expValue.indexOf(columnListName)+columnListName.length()+1;
                String remainingExp = expValue.substring(listIndex1);
                columnLbl.setValueExpression("value",
                        exFactory.createValueExpression(elContext,
                        "#{" + subTableVar + "." + remainingExp + "}", String.class));
                column.getChildren().add(columnLbl);
                subTable.getChildren().add(column);
                if (groupByFields.get(String.valueOf(fieldsToBeCreated.get(fieldIndex).getDbid())) != null) {
                    Column columnEmptyInner = new Column();
                    columnEmptyInner.setHeaderText(" ");
                    columnEmptyInner.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "innerEmptyCol");
                    //create sub table
                    List<ReportScreenField> groupedScreenFields = null;
                    groupedScreenFields = oem.loadEntityList(ReportScreenField.class.getSimpleName(),
                            Collections.singletonList("groupByField.dbid = " + fieldsToBeCreated.get(fieldIndex).getDbid()),
                            null, Collections.singletonList("sortIndex"), loggedUser);
                    if (groupedScreenFields != null) {
                        DataTable innerTable = createInnerTable(groupByFields, subTableVar, groupedScreenFields
                                ,fieldsToBeCreated.get(fieldIndex).getFieldExpression() );
                        if (innerTable != null) {
                            columnEmptyInner.getChildren().add(innerTable);
                            subTable.getChildren().add(columnEmptyInner);
                        }
                    }
                }
                
                // add aggregation function group
                if(fieldsToBeCreated.get(fieldIndex).getAggregationFieldExp() != null 
                        && !fieldsToBeCreated.get(fieldIndex).getAggregationFieldExp().equals("")){
                    
                    String computedExp = fieldsToBeCreated.get(fieldIndex).getAggregationFieldExp();
                    String computedFldExpToBind = "";
                    if (computedExp.contains(".")) {
                        String[] subFieldExp = computedExp.split("\\.");
                        int listIndex = subFieldExp.length - 1;
                        computedFldExpToBind = subFieldExp[listIndex];
                    } else {
                        computedFldExpToBind = computedExp;
                    }
                    Column parentCountCol = new Column();
                    parentCountCol.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_totslInnerCountCol");
                    parentCountCol.setValueExpression("footerText",
                            exFactory.createValueExpression(elContext,
                            "Count : " + "#{" + valueExpression + "." + computedFldExpToBind + "}", String.class));
                    countRow.getChildren().add(parentCountCol);
                    countGrp.getChildren().add(countRow);
                    subTable.getChildren().add(countGrp);
                }
            }
            logger.trace("Returning");
            return subTable;
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            logger.trace("Returning with Null");
            return null;
        }


    }

    private Column createGroupByColumn(String columnHeader) {
        Column column = new Column();
        column.setHeaderText(columnHeader);
        column.setRowspan(2);
        return column;
    }

    /**
     * @return the totalCount
     */
    public int getTotalCount() {
        return totalCount;
    }

    /**
     * @param totalCount the totalCount to set
     */
    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}
