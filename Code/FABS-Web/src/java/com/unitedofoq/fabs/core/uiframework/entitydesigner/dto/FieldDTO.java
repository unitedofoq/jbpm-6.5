/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.entitydesigner.dto;

/**
 *
 * @author Mostafa
 */
public class FieldDTO {
    private String fieldName="";
    private String fieldType="";

    public FieldDTO()
    {
    }
    
    public FieldDTO(FieldDTO fieldDTO)
    {
        fieldName = fieldDTO.getFieldName();
        fieldType = fieldDTO.getFieldType();
    }
    /**
     * @return the fieldName
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * @param fieldName the fieldName to set
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * @return the fieldType
     */
    public String getFieldType() {
        return fieldType;
    }

    /**
     * @param fieldType the fieldType to set
     */
    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

}
