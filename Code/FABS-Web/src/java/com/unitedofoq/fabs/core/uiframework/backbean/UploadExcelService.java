package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import static com.unitedofoq.fabs.core.uiframework.backbean.TabularBean.logger;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.fabs.core.validation.EntityDataValidationException;
import com.unitedofoq.fabs.core.validation.FABSException;
import com.unitedofoq.fabs.core.validation.OEntityDataValidationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;

public class UploadExcelService {

    private final OEntityManagerRemote oem = OEJB.lookup(OEntityManagerRemote.class);

    private final UserMessageServiceRemote usrMsgService = OEJB.lookup(UserMessageServiceRemote.class);

    private final TabularBean tabularBean;

    private final Map<String, Integer> textColumnSizes = new HashMap<>();

    OUser loggedUser;

    public UploadExcelService(TabularBean tabularBean, OUser loggedUser) {
        this.loggedUser = loggedUser;
        this.tabularBean = tabularBean;
    }

    public UserMessage uploadSheet(final File file) throws IOException, UserNotAuthorizedException, FABSException {
        int rowNum = 1;
        try {
            final InputStream inputStream = new FileInputStream(file);
            file.delete();
            final HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            final HSSFSheet sheet = workbook.getSheetAt(0);
            List<BaseEntity> entitys = new ArrayList<>();
            cacheColumnsSize(tabularBean.createNewInstance());
            for (Row row : sheet) {
                if (isRowEmpty(row)) {
                    continue;
                }
                OFunctionResult rowFunctionResult = createSheetRow(row, file, entitys);
                if (!rowFunctionResult.getErrors().isEmpty()) {
                    return rowFunctionResult.getErrors().get(0);
                }
                rowNum++;
            }
            oem.saveEntityList(entitys, loggedUser);
            inputStream.close();
            return usrMsgService.getUserMessage("upload_success", loggedUser, --rowNum + "");
        } catch (EntityExistsException | EntityNotFoundException | NonUniqueResultException | NoResultException | OptimisticLockException | RollbackException | TransactionRequiredException | OEntityDataValidationException | EntityDataValidationException ex) {
            Logger.getLogger(TabularBean.class.getName()).log(Level.SEVERE, null, ex);
            return usrMsgService.getUserMessage("upload_failed_row", loggedUser, --rowNum + "");
        }
    }

    private OFunctionResult createSheetRow(Row row, final File file, List<BaseEntity> entitys) {
        OFunctionResult ofr = new OFunctionResult();

        BaseEntity newEntity = createEmptyEntity(file);
        for (int cellIndex = 0; cellIndex < tabularBean.fieldExpressions.size(); cellIndex++) {
            Cell cell = row.getCell(cellIndex);
            OFunctionResult mandatoryFieldOFR = checkIfMandatoryIsNull(tabularBean.getCurrentScreen().getScreenFields().get(cellIndex), row, cell);
            if (!mandatoryFieldOFR.getErrors().isEmpty()) {
                return mandatoryFieldOFR;
            }
            if (cell != null) {
                ofr = createSheetCell(cell, newEntity);
            }
            if (!ofr.getErrors().isEmpty()) {
                return ofr;
            }
        }
        entitys.add(newEntity);
        return ofr;
    }

    private OFunctionResult createSheetCell(Cell cell, BaseEntity newEntity) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            int cellIndex = cell.getColumnIndex();
            trimCellValue(cell);
            List<ExpressionFieldInfo> fieldInfos = BaseEntity.parseFieldExpression(tabularBean.actOnEntityCls,
                    tabularBean.fieldExpressions.get(cellIndex));
            if (tabularBean.getCurrentScreen().getScreenFields().get(cellIndex).getDd().getControlType()
                    .getDbid() == DD.CT_LOOKUPSCREEN
                    || tabularBean.getCurrentScreen().getScreenFields().get(cellIndex).getDd().getControlType()
                            .getDbid() == DD.CT_LOOKUPMULTILEVEL
                    || tabularBean.getCurrentScreen().getScreenFields().get(cellIndex).getDd().getControlType()
                            .getDbid() == DD.CT_DROPDOWN) {
                ofr = createSheetCellLookup(fieldInfos, cell, newEntity, cellIndex);
            } else if (fieldInfos.get(fieldInfos.size() - 1).field.getType().getSimpleName()
                    .equalsIgnoreCase("String")) {
                ofr = createSheetCellText(cell, newEntity, cellIndex);
            } else if (fieldInfos.get(fieldInfos.size() - 1).field.getType().getSimpleName()
                    .equalsIgnoreCase("BigDecimal")) {
                ofr = createSheetCellDecimal(cellIndex, newEntity, cell);
            } else if (fieldInfos.get(fieldInfos.size() - 1).field.getType().getSimpleName().equals("Date")) {
                ofr = createSheetCellDate(cell, newEntity, cellIndex);
            } else if (fieldInfos.get(fieldInfos.size() - 1).field.getType().getSimpleName()
                    .equals("boolean")) {
                ofr = createSheetCellBoolean(cell, newEntity, cellIndex);
            } else if (fieldInfos.get(fieldInfos.size() - 1).field.getType().getSimpleName()
                    .equalsIgnoreCase("int") || fieldInfos.get(fieldInfos.size() - 1).field.getType().getSimpleName()
                    .equalsIgnoreCase("integer")) {
                ofr = createSheetCellNumber(cell, newEntity, cellIndex);
            } else {
                ofr = createSheetCellOther(cell, newEntity, cellIndex);
            }
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            if (ofr.getErrors().isEmpty()) {
                ofr.addError(usrMsgService.getUserMessage("upload_failed_cell", loggedUser, (cell.getRowIndex() + 1) + "", (cell.getColumnIndex() + 1) + ""));
            }
        }
        return ofr;
    }

    private OFunctionResult checkIfMandatoryIsNull(ScreenField screenField, Row row, Cell cell) {
        OFunctionResult ofr = new OFunctionResult();
        if (screenField.isMandatory() && (cell == null || StringUtils.isBlank(cell.getStringCellValue()))) {
            String fieldName = StringUtils.isNotBlank(screenField.getDdTitleOverride())
                    ? screenField.getDdTitleOverride() : screenField.getDd().getLabel();
            String rowNum = (row.getRowNum() + 1) + "";
            List<String> msgParams = new LinkedList<>();
            msgParams.add(fieldName);
            msgParams.add(rowNum);
            ofr.addError(usrMsgService.getUserMessage("sheet_mand_field_err", msgParams, loggedUser));
        }
        return ofr;
    }

    private void trimCellValue(Cell cell) {
        if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
            cell.setCellValue(cell.getStringCellValue().trim());
        }
    }

    private OFunctionResult createSheetCellOther(Cell cell, BaseEntity newEntity, int cellIndex) throws NumberFormatException {
        OFunctionResult ofr = new OFunctionResult();

        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                FramworkUtilities.setValueInEntity(newEntity, tabularBean.fieldExpressions.get(cellIndex), cell.getStringCellValue());
                break;
            case Cell.CELL_TYPE_NUMERIC:
                FramworkUtilities.setValueInEntity(newEntity, tabularBean.fieldExpressions.get(cellIndex), cell.getNumericCellValue());
                break;
            default:
                String fieldName = StringUtils.isNotBlank(tabularBean.screenFields.get(cellIndex).getDdTitleOverride())
                        ? tabularBean.screenFields.get(cellIndex).getDdTitleOverride() : tabularBean.screenFields.get(cellIndex).getDd().getLabel();
                String rowNum = (cell.getRowIndex() + 1) + "";
                ofr.addError(usrMsgService.getUserMessage("sheet_field_unknown_err", loggedUser, fieldName, rowNum));
                return ofr;
        }
        return ofr;
    }

    private OFunctionResult createSheetCellNumber(Cell cell, BaseEntity newEntity, int cellIndex) {
        OFunctionResult ofr = new OFunctionResult();

        if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
            try {
                Double.parseDouble(cell.getStringCellValue());
            } catch (NumberFormatException e) {
                logger.error(e.getLocalizedMessage());
                return decimalFieldError(cellIndex, cell);
            }
            FramworkUtilities.setValueInEntity(newEntity, tabularBean.fieldExpressions.get(cellIndex),
                    cell.getStringCellValue());
        } else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
            String val = String.valueOf(cell.getNumericCellValue());
            if (val.endsWith(".0")) {
                val = val.replace(".0", "");
            }
            FramworkUtilities.setValueInEntity(newEntity, tabularBean.fieldExpressions.get(cellIndex),
                    val);
        }
        return ofr;
    }

    private OFunctionResult createSheetCellBoolean(Cell cell, BaseEntity newEntity, int cellIndex) {
        OFunctionResult ofr = new OFunctionResult();

        switch (cell.getCellType()) {
            case HSSFCell.CELL_TYPE_BOOLEAN:
                FramworkUtilities.setValueInEntity(newEntity, tabularBean.fieldExpressions.get(cellIndex),
                        cell.getBooleanCellValue());
                break;
            case Cell.CELL_TYPE_STRING:
                if (cell.getStringCellValue().toLowerCase().equals("true") || cell.getStringCellValue().toLowerCase().equals("false")) {
                    FramworkUtilities.setValueInEntity(newEntity, tabularBean.fieldExpressions.get(cellIndex),
                            "true".equals(cell.getStringCellValue().toLowerCase()));
                } else {
                    return booleanFieldError(cellIndex, cell);
                }
                break;
            case Cell.CELL_TYPE_NUMERIC:
                if (cell.getNumericCellValue() == 0 || cell.getNumericCellValue() == 1) {
                    FramworkUtilities.setValueInEntity(newEntity, tabularBean.fieldExpressions.get(cellIndex),
                            cell.getNumericCellValue() == 1);
                }
                break;
            default:
                return booleanFieldError(cellIndex, cell);

        }
        return ofr;
    }

    private OFunctionResult booleanFieldError(int cellIndex, Cell cell) {
        OFunctionResult ofr = new OFunctionResult();
        String fieldName = StringUtils.isNotBlank(tabularBean.screenFields.get(cellIndex).getDdTitleOverride())
                ? tabularBean.screenFields.get(cellIndex).getDdTitleOverride() : tabularBean.screenFields.get(cellIndex).getDd().getLabel();
        String rowNum = (cell.getRowIndex() + 1) + "";
        ofr.addError(usrMsgService.getUserMessage("sheet_field_boolean_err", loggedUser, fieldName, rowNum));
        return ofr;
    }

    private OFunctionResult createSheetCellDate(Cell cell, BaseEntity newEntity, int cellIndex) {
        OFunctionResult ofr = new OFunctionResult();

        try {
            Date resultDate;
            if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                SimpleDateFormat smf = new SimpleDateFormat("dd/MM/yyyy");
                DataFormatter dataFormatter = new DataFormatter();
                String value = dataFormatter.formatCellValue(cell);
                resultDate = smf.parse(value);
            } else {
                resultDate = cell.getDateCellValue();
            }
            FramworkUtilities.setValueInEntity(newEntity, tabularBean.fieldExpressions.get(cellIndex),
                    HSSFDateUtil.getJavaDate(HSSFDateUtil.getExcelDate(resultDate)));

        } catch (ParseException e) {
            logger.error(e.getLocalizedMessage());
            return dateFieldError(cellIndex, cell);
        }
        return ofr;
    }

    private OFunctionResult dateFieldError(int cellIndex, Cell cell) {
        OFunctionResult ofr = new OFunctionResult();
        String fieldName = StringUtils.isNotBlank(tabularBean.screenFields.get(cellIndex).getDdTitleOverride())
                ? tabularBean.screenFields.get(cellIndex).getDdTitleOverride() : tabularBean.screenFields.get(cellIndex).getDd().getLabel();
        String rowNum = (cell.getRowIndex() + 1) + "";
        ofr.addError(usrMsgService.getUserMessage("sheet_field_date_err", loggedUser, fieldName, rowNum));
        return ofr;
    }

    private OFunctionResult createSheetCellDecimal(int cellIndex, BaseEntity newEntity, Cell cell) {
        OFunctionResult ofr = new OFunctionResult();

        String removeMask = tabularBean.fieldExpressions.get(cellIndex);
        if (removeMask.contains("Mask")) {
            int index = removeMask.length() - 4;
            removeMask = removeMask.substring(0, index);
        }
        if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
            FramworkUtilities.setValueInEntity(newEntity, removeMask,
                    cell.getNumericCellValue());
        } else {
            BigDecimal cellBigDecimal;
            try {
                cellBigDecimal = new BigDecimal(cell.getStringCellValue());
            } catch (Exception e) {
                logger.error(e.getLocalizedMessage());
                return decimalFieldError(cellIndex, cell);
            }
            FramworkUtilities.setValueInEntity(newEntity, removeMask,
                    cellBigDecimal);
        }
        return ofr;
    }

    private OFunctionResult decimalFieldError(int cellIndex, Cell cell) {
        OFunctionResult ofr = new OFunctionResult();
        String fieldName = StringUtils.isNotBlank(tabularBean.screenFields.get(cellIndex).getDdTitleOverride())
                ? tabularBean.screenFields.get(cellIndex).getDdTitleOverride() : tabularBean.screenFields.get(cellIndex).getDd().getLabel();
        String rowNum = (cell.getRowIndex() + 1) + "";
        ofr.addError(usrMsgService.getUserMessage("sheet_field_number_err", loggedUser, fieldName, rowNum));
        return ofr;
    }

    private OFunctionResult createSheetCellText(Cell cell, BaseEntity newEntity, int cellIndex) {
        OFunctionResult ofr = new OFunctionResult();

        if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
            String val = String.valueOf(new BigDecimal(cell.getNumericCellValue()));
            if (val.endsWith(".0")) {
                val = val.replace(".0", "");
            }
            FramworkUtilities.setValueInEntity(newEntity, tabularBean.fieldExpressions.get(cellIndex), val);
        } else {
            ofr = validateColumnSize(tabularBean.fieldExpressions.get(cellIndex), cell);
            if (ofr.getErrors().isEmpty()) {
                FramworkUtilities.setValueInEntity(newEntity, tabularBean.fieldExpressions.get(cellIndex),
                        cell.getStringCellValue());
            }
        }
        return ofr;
    }

    private OFunctionResult createSheetCellLookup(List<ExpressionFieldInfo> fieldInfos, Cell cell, BaseEntity newEntity, int cellIndex) {
        OFunctionResult ofr = new OFunctionResult();

        String removeTranslated = fieldInfos.get(fieldInfos.size() - 1).fieldName;
        if (removeTranslated.contains("Translated")) {
            removeTranslated = removeTranslated.replace("Translated", "");
        }
        if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
            FramworkUtilities.setValueInEntity(newEntity, tabularBean.fieldExpressions.get(cellIndex),
                    tabularBean.getLookupObject(fieldInfos.get(fieldInfos.size() - 1).fieldClass.getSimpleName(),
                            removeTranslated, String.valueOf(cell.getNumericCellValue())));
        } else {
            FramworkUtilities.setValueInEntity(newEntity, tabularBean.fieldExpressions.get(cellIndex),
                    tabularBean.getLookupObject(fieldInfos.get(fieldInfos.size() - 1).fieldClass.getSimpleName(),
                            removeTranslated, cell.getStringCellValue()));
        }
        return ofr;
    }

    private BaseEntity createEmptyEntity(final File file1) {
        BaseEntity newEntity = tabularBean.createNewInstance();
        newEntity.setCustom1(file1.getName().substring(0, file1.getName().lastIndexOf(".")));
        newEntity.setCustom2(new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").format(new Date()));
        newEntity.generateInstID();
        return newEntity;
    }

    private boolean isRowEmpty(Row row) {
        logger.trace("Entering");
        for (int c = row.getFirstCellNum(); c <= row.getLastCellNum(); c++) {
            Cell cell = row.getCell(c);
            if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                logger.trace("Returning with False");
                return false;
            }
        }
        logger.trace("Returning with True");
        return true;
    }

    private OFunctionResult validateColumnSize(String fldExp, Cell cell) {
        OFunctionResult ofr = new OFunctionResult();
        String[] fldExpArr = fldExp.split("\\.");
        String colName = fldExpArr[fldExpArr.length - 1].toLowerCase();
        Integer colSize = textColumnSizes.get(colName);
        if (colSize != null && colSize != 0 && colSize < cell.getStringCellValue().length()) {
            String rowIndex = (cell.getRowIndex() + 1) + "";
            ScreenField scrFld = tabularBean.getCurrentScreen().getScreenFields().get(cell.getColumnIndex());
            String fldName = StringUtils.isNotBlank(scrFld.getDdTitleOverride()) ? scrFld.getDdTitleOverride() : scrFld.getDd().getLabel();
            ofr.addError(usrMsgService.getUserMessage("sheet_cell_range", loggedUser, rowIndex, fldName, colSize + ""));
        }
        return ofr;
    }

    private void cacheColumnsSize(BaseEntity entity) {
        try {
            String tableName = entity.getEntityTableName().get(0);
            String sql = "select column_name, character_maximum_length"
                    + " from information_schema.columns"
                    + " where table_name = '" + tableName + "' and DATA_TYPE like 'varchar'";
            List<Object[]> columnsSizeList = oem.executeEntityListNativeQuery(sql, loggedUser);
            columnsSizeList.forEach((colSize) -> {
                textColumnSizes
                        .put(String.valueOf(colSize[0]).toLowerCase(),
                                Integer.valueOf(String.valueOf(colSize[1])));
            });
        } catch (UserNotAuthorizedException | EntityExistsException
                | EntityNotFoundException | NonUniqueResultException
                | NoResultException | OptimisticLockException
                | RollbackException | TransactionRequiredException ex) {
            Logger.getLogger(UploadExcelService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
