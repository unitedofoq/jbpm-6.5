/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.process.jBPM.ProcessInstance;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import java.util.ArrayList;
import java.util.List;
import javax.el.MethodExpression;
import javax.el.ValueExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;
import org.primefaces.component.column.Column;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.commandlink.CommandLink;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.panel.Panel;

/**
 *
 * @author mostafa
 */
@ManagedBean(name = "ProcessInstanceBean")
@ViewScoped
public class ProcessInstanceBean extends TabularBean {
    
    private ProcessLazyDataModel processLazyDataModel;
    private int pageSize = 10;
    @Override
    public void init() {
        super.init();
        processLazyDataModel = new ProcessLazyDataModel(loggedUser);
    }
    
    private List<ProcessInstance> processes = new ArrayList<ProcessInstance>();

    @Override
    public HtmlPanelGrid buildInnerScreen() {
        logger.debug("Entering");
        HtmlPanelGrid grid = new HtmlPanelGrid();
        grid.setColumns(1);
        grid.setWidth("100%");
        List<ScreenField> screenFields = getCurrentScreen().getScreenFields();
        Panel middlePanel = new Panel();
        middlePanel.setStyle("position:relative");

        // <editor-fold defaultstate="collapsed" desc="GetScreenFields & Build Table">
        DataTable processInstsTable = new DataTable();
        
        processInstsTable.setResizableColumns(true);
        processInstsTable.setStyleClass("dt-table");
        processInstsTable.setId(getCurrentScreen().getName() + "_inboxTable");
        processInstsTable.setRows(pageSize);//For Paginator
        processInstsTable.setVar("currentRow");
        processInstsTable.setPaginatorAlwaysVisible(false);
        //For Paginator
        processInstsTable.setPaginatorPosition("bottom");
        processInstsTable.setPaginator(true);
        processInstsTable.setPaginatorTemplate("{CurrentPageReport}  {FirstPageLink} {PreviousPageLink} {PageLinks} "
                + "{NextPageLink} {LastPageLink} {RowsPerPageDropdown}");
        
        processInstsTable.setValueExpression("value",
                exFactory.createValueExpression(elContext, "#{" + getBeanName() + ".processes}", ArrayList.class));
        
        processInstsTable.setLazy(true);
        processInstsTable.setValue(processLazyDataModel);
        processInstsTable.setFilterEvent("enter");
        
        Class[] params = {ActionEvent.class};
        MethodExpression me = FacesContext.getCurrentInstance().getApplication().getExpressionFactory().createMethodExpression(
                elContext, "#{" + getBeanName() + ".taskActionListener}",
                null, params);

        MethodExpressionActionListener processActionListener = new MethodExpressionActionListener(
                me);

        MethodExpression mep = FacesContext.getCurrentInstance().getApplication().getExpressionFactory().createMethodExpression(
                elContext, "#{" + getBeanName() + ".taskActionListener}",
                null, params);

        MethodExpressionActionListener processActionListenerp = new MethodExpressionActionListener(
                mep);
        //fields
        for (int scrFldExpIndx = 0; scrFldExpIndx < screenFields.size(); scrFldExpIndx++) {

            ScreenField screenField = (ScreenField) screenFields.get(scrFldExpIndx);
            Column column = new Column();
            column.setId("column" + scrFldExpIndx);
            column.setStyleClass("column-business");
            ValueExpression valueExpression = exFactory.createValueExpression(
                    elContext, "#{currentRow." + screenField.getFieldExpression() + "}",
                    String.class);
            column.setValueExpression("sortBy", valueExpression);
            column.setValueExpression("filterBy", valueExpression);
            column.setFilterMatchMode("contains");

            DD headerDD = screenField.getDd();
            try {
                headerDD = (DD) oem.loadEntity(DD.class.getSimpleName(), headerDD.getDbid(), null, null, loggedUser);
            } catch (Exception ex) {
                logger.error("Exception thrown",ex);
            }

            if (screenFields.get(scrFldExpIndx).getDdTitleOverride() == null
                    || screenFields.get(scrFldExpIndx).getDdTitleOverride().equals("")) {
                column.setHeaderText(headerDD.getHeaderTranslated());
            } else {
                column.setHeaderText(screenFields.get(scrFldExpIndx).getDdTitleOverride());
            }

            if ("name".equals(screenField.getFieldExpression())) {
                CommandButton commandButton = new CommandButton();
                commandButton.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_PRC_INST");
                commandButton.setType("image");
                commandButton.setValueExpression("title",
                        exFactory.createValueExpression(elContext, "#{currentRow['id']}", String.class));
                commandButton.setStyleClass("info_btn");
                commandButton.addActionListener(processActionListenerp);

                CommandLink commandLink = new CommandLink();
                commandLink.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_Task_Link");
                commandLink.setValueExpression("value",
                        exFactory.createValueExpression(elContext, "#{currentRow['name']}", String.class));
                commandLink.setValueExpression("title",
                        exFactory.createValueExpression(elContext, "#{currentRow['id']}", String.class));
                ((CommandLink) commandLink).setRendered(true);
                commandLink.setImmediate(true);
                commandLink.setProcess("@this");
                commandLink.setPartialSubmit(true);
                commandLink.addActionListener(processActionListener);
                column.getChildren().add(commandLink);
            } else if ("description".equals(screenField.getFieldExpression())) {
                CommandButton commandButton = new CommandButton();
                commandButton.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_PRC_DESC_INST");
                commandButton.setType("image");
                commandButton.setValueExpression("title",
                        exFactory.createValueExpression(elContext, "#{currentRow['id']}", String.class));
                commandButton.setStyleClass("info_btn");
                commandButton.addActionListener(processActionListenerp);

                CommandLink commandLink = new CommandLink();
                commandLink.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_Task_Desc_Link");
                commandLink.setValueExpression("value",
                        exFactory.createValueExpression(elContext, "#{currentRow['description']}", String.class));
                commandLink.setValueExpression("title",
                        exFactory.createValueExpression(elContext, "#{currentRow['id']}", String.class));
                ((CommandLink) commandLink).setRendered(true);
                commandLink.setImmediate(true);
                commandLink.setProcess("@this");
                commandLink.setPartialSubmit(true);
                commandLink.addActionListener(processActionListener);
                column.getChildren().add(commandLink);
            }else {
                HtmlOutputText outputText = new HtmlOutputText();
                outputText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_outputText");
                outputText.setValueExpression("value",
                        exFactory.createValueExpression(elContext,
                        "#{currentRow['" + screenField.getFieldExpression() + "']}", String.class));
                outputText.setStyleClass("OTMSTextBox");
                outputText.setStyle("text-align: left; border: none; background-color: inherit;");

                column.getChildren().add(outputText);
            }
            processInstsTable.getChildren().add(column);
        }

        grid.getChildren().add(processInstsTable);
        logger.debug("Returning");
        return grid;
    }
    
    @Override
    protected String getBeanName() {
        return "ProcessInstanceBean";
    }

    public void taskActionListener(ActionEvent ae) {
        logger.debug("Entering");
        if (ae.getComponent() instanceof CommandLink) {
            String processInstanceId = ((CommandLink) ae.getComponent()).getTitle();
            ODataMessage message = new ODataMessage();
            ODataType oDataType = dataTypeService.loadODataType("DBID", loggedUser);
            message.setODataType(oDataType);
            List data = new ArrayList();
            data.add(loggedUser);
            data.add(Long.valueOf(processInstanceId));
            String processID = "";
            for (int i = 0; i < processes.size(); i++) {
                if (processes.get(i).getId().equals(processInstanceId)) {
                    processID = processes.get(i).getName();
                }
            }
            data.add(processID);
            message.setData(data);
            String screenInstanceID = generateScreenInstId("ProcessTasks");
            SessionManager.setScreenSessionAttribute(screenInstanceID,
                    OPortalUtil.MESSAGE, message);
            openScreen("ProcessTasks", screenInstanceID);
        } else {
            String processInstanceId = ((CommandButton) ae.getComponent()).getTitle();
            ODataMessage message = new ODataMessage();
            ODataType oDataType = dataTypeService.loadODataType("DBID", loggedUser);
            message.setODataType(oDataType);
            List data = new ArrayList();
            data.add(loggedUser);
            data.add(Long.valueOf(processInstanceId));
            String processID = "";
            for (int i = 0; i < processes.size(); i++) {
                if (processes.get(i).getId().equals(processInstanceId)) {
                    processID = processes.get(i).getProcessDefinition().getPackageName() 
                            + "." + processes.get(i).getProcessDefinition().getName();
                }
            }
            data.add(processID);
            message.setData(data);
            String screenInstanceID = generateScreenInstId("ProcessDetails");
            SessionManager.setScreenSessionAttribute(screenInstanceID,
                    OPortalUtil.MESSAGE, message);
            openScreen("ProcessDetails", screenInstanceID);
            logger.debug("Returning");
        }
    }

    public List<ProcessInstance> getProcesses() {
        return processes;
    }

    public void setProcesses(List<ProcessInstance> processes) {
        this.processes = processes;
    }
    
}
