package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;

import static com.unitedofoq.fabs.core.uiframework.backbean.SingleEntityBean.logger;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.LoggerFactory;

@ManagedBean(name = "ImportDataBean")
@ViewScoped
public class ImportDataBean {
final static org.slf4j.Logger logger = LoggerFactory.getLogger(ImportDataBean.class);
    
    UserMessageServiceRemote userMessageService = OEJB.lookup(UserMessageServiceRemote.class);

    public void cancel() {
        logger.debug("Entering");
        RequestContext context = RequestContext.getCurrentInstance();
                String param = "{popID:'" + (String) SessionManager.getSessionAttribute("ImportDataCurrentScreen")+"'}";
                logger.trace("Calling JS closePopupNoID with PortletInsId: {}",param);
                context.execute(
                        "top.FABS.Portlet.closePopupNoID("+param +");");
                logger.trace("Calling JS closePopupNoID({});",param);
                logger.debug("Returning");
    }

    public void uploadedFile(FileUploadEvent event) {
        logger.debug("Entering");
        OutputStream out = null;
        try {
            UploadedFile uploadedFile = (UploadedFile) event.getFile();
            File file = new File(uploadedFile.getFileName());
            InputStream in = uploadedFile.getInputstream();
            out = new FileOutputStream(file);
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();
            out.close();
            SessionManager.addSessionAttribute("ImportDataFilePath", file.getAbsolutePath());
            String orScreen = (String) SessionManager.getSessionAttribute("ImportDataCurrentScreen");
            
            RequestContext.getCurrentInstance().execute("top.FABS.Portlet.fileEntrtyReturned({portletId:'" + orScreen + "'})");
            
            String param = (String) SessionManager.getSessionAttribute("openedScreenInstId");//"{popID:'" + (String) SessionManager.getSessionAttribute("openedScreenInstId")+"'}";
                RequestContext.getCurrentInstance().execute(
                        "top.FABS.Portlet.closePopupNoID(\'ImportDataBean\');");
                logger.trace("Calling JS closePopupNoID(\'ImportDataBean\');");
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        } finally {
            try {
                out.close();
            } catch (IOException ex) {
                logger.error("Exception thrown",ex);
            }
            logger.debug("Returning");
        }
    }
}
