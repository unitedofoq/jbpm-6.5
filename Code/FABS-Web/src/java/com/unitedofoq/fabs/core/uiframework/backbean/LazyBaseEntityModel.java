/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import com.unitedofoq.fabs.core.filter.FilterDDRequiredException;
import static com.unitedofoq.fabs.core.uiframework.backbean.OBackBean.count;
import static com.unitedofoq.fabs.core.uiframework.backbean.OBackBean.liferayUsers;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.uiframework.screen.TabularScreen;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mibrahim
 */
public class LazyBaseEntityModel extends LazyDataModel<BaseEntity> {

    final static Logger logger = LoggerFactory.getLogger(LazyBaseEntityModel.class);
    private List<BaseEntity> newEntities;
    private BaseEntity masterNewEntity;//Same record in newEntities since newEntities will be cleared.
    private List<BaseEntity> datasource;
    private TabularBean tabularBean;
    private boolean addMode = false;
    private List<Integer> indicies = new ArrayList<Integer>();

    public boolean isAddMode() {
        return addMode;
    }

    public void setAddMode(boolean addMode) {
        this.addMode = addMode;
    }

    public BaseEntity getMasterNewEntity() {
        return masterNewEntity;
    }

    public void setMasterNewEntity(BaseEntity masterNewEntity) {
        this.masterNewEntity = masterNewEntity;
    }

    public LazyBaseEntityModel(List<BaseEntity> datasource) {
        this.datasource = datasource;
        newEntities = new ArrayList<BaseEntity>();
    }

    public LazyBaseEntityModel(List<BaseEntity> datasource, TabularBean tabularBean) {
        this.datasource = datasource;
        newEntities = new ArrayList<BaseEntity>();
        this.tabularBean = tabularBean;
        int rowCount;

        if (tabularBean.getCurrentScreen() instanceof TabularScreen) {
            rowCount = ((TabularScreen) tabularBean.getCurrentScreen()).getRecordsInPage();
            setRowCount(rowCount == 0 ? TabularBean.DEFAULT_PAGE_SIZE : rowCount);
        } else {
            setRowCount(20);
        }

    }

    @Override
    public BaseEntity getRowData(String rowKey) {
        for (BaseEntity baseEntity : datasource) {
            if (baseEntity.getInstID() == Long.parseLong(rowKey)) {
                return baseEntity;
            }
        }
        return null;
    }

    @Override
    public Object getRowKey(BaseEntity BaseEntity) {
        if (tabularBean.getCurrentScreen().getName().contains("lookup")) {
            return BaseEntity.getDbid();
        } else {
            return BaseEntity.getInstID();
        }
    }

    @Override
    public Object getWrappedData() {
        return datasource;
    }

    @Override
    public void setWrappedData(Object list) {
        super.setWrappedData(list);
        datasource = (List<BaseEntity>) list;
    }

    @Override
    public List<BaseEntity> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, String> filters) {
        int count = count(liferayUsers.get(tabularBean.loggedUser.getLoginName()));
        if (count == 0) {
            tabularBean.logout();
            return null;
        }
        if (tabularBean.invalidInputMode) {
            if (getWrappedData() == null) {
                setWrappedData(new ArrayList<BaseEntity>());
            }
            return (List<BaseEntity>) getWrappedData();
        }
        if (isAddMode()) {
            setAddMode(false);
            return (List<BaseEntity>) getWrappedData();
        }

        tabularBean.lookupTransformedPickedElements = (LinkedList) SessionManager.getSessionAttribute("lookupTransformedPickedElements");
        SessionManager.getSessionMap().remove("lookupTransformedPickedElements");
        Object lookupMainScr = SessionManager.getSessionAttribute("lookupPickedElementsMainScr");
        tabularBean.lookupMainScr = lookupMainScr == null ? 0 : Long.valueOf(lookupMainScr.toString());
        LinkedList loadLookupPickedList = null;
        int lookupPickedElementsSize = 0;
        if (Objects.equals(tabularBean.getCurrentScreen().getInstID(), tabularBean.lookupMainScr)) {
            int recordsPerPage = ((TabularScreen) tabularBean.getCurrentScreen()).getRecordsInPage();
            if (tabularBean.lookupTransformedPickedElements.size() > first) {
                lookupPickedElementsSize = tabularBean.lookupTransformedPickedElements.size();
                int last = first + recordsPerPage;
                last = tabularBean.lookupTransformedPickedElements.size() >= last ? last : tabularBean.lookupTransformedPickedElements.size();
                loadLookupPickedList = new LinkedList(tabularBean.lookupTransformedPickedElements.subList(first, last));
                int lastRowIndex = loadLookupPickedList.size() - 1;
                if (loadLookupPickedList.size() < pageSize) {
                    first = 0;
                }
                RequestContext.getCurrentInstance().execute("editRowList(" + lastRowIndex + ")");
                if (!getNewEntities().isEmpty()) {
                    removeRow(getNewEntities().get(0));
                }
            } else if (!tabularBean.lookupTransformedPickedElements.isEmpty()) {
                first = first - tabularBean.lookupTransformedPickedElements.size();
            }

        }
        tabularBean.setFirst(first);
        sortLoadData(sortField, sortOrder);

        filterLoadData(filters);
        setRowCount(getRowCount() + lookupPickedElementsSize);

        int countAfter = 0;
        for (int i = 0; i < getIndicies().size(); i++) {
            Integer index = getIndicies().get(i);
            if (index >= first && index < first + getPageSize()
                    && !newEntities.isEmpty()
                    && i < newEntities.size()) {
                if (!tabularBean.loadedList.contains(getNewEntities().get(i))) {
                    tabularBean.loadedList.add(index % getPageSize(), getNewEntities().get(i));
                }
                countAfter = i + 1;
            }
        }
        if (tabularBean.loadedList.size() < getPageSize()
                && countAfter < newEntities.size()
                && countAfter + getPageSize() - tabularBean.loadedList.size() < newEntities.size()) {
            List<BaseEntity> baseEntitys = newEntities.subList(countAfter,
                    Math.min(countAfter + getPageSize() - tabularBean.loadedList.size(), newEntities.size()));
            for (BaseEntity baseEntity : baseEntitys) {
                if (!tabularBean.loadedList.contains(baseEntity)) {
                    tabularBean.loadedList.add(baseEntity);
                }
            }

        }

        List<BaseEntity> data = tabularBean.loadedList;
        if (loadLookupPickedList != null) {
            tabularBean.dataSort.clear();
            tabularBean.dataFilter.clear();
            data.addAll(0, loadLookupPickedList);
        }
        if (tabularBean.loadedList.size() < 2) {
            tabularBean.customedDynamicBalloonStyle += "top:0px!important;";
        }
        setWrappedData(data);
        if (getRowCount() < datasource.size()) {
            setRowCount(datasource.size());
        }
        if (data != null && !data.isEmpty() && getRowCount() != 0) {//&& tabularBean.c2AMode) {
            //for multi selection filter
            if (tabularBean.getCurrentScreen().getName().contains("lookup")) {
                Object selectedObjects = tabularBean.dataTable.getSelection();
                ArrayList<BaseEntity> selectedObjectsList;

                if (selectedObjects instanceof ArrayList) {
                    selectedObjectsList = (ArrayList<BaseEntity>) selectedObjects;
                } else {
                    //check for first load as array has null elements
                    if (((BaseEntity[]) selectedObjects)[0] != null) {
                        selectedObjectsList = new ArrayList<BaseEntity>(Arrays.asList((BaseEntity[]) selectedObjects));
                    } else {
                        selectedObjectsList = new ArrayList<BaseEntity>();
                    }
                }

                for (BaseEntity baseEntity : data) {
                    if (selectedObjectsList.contains(baseEntity)) {
                        baseEntity.setSelected(true);
                    }
                }

                tabularBean.selectedEntities = selectedObjectsList.toArray(new BaseEntity[selectedObjectsList.size()]);
            } else {
                if (!tabularBean.isLookupMode() || !tabularBean.currentScreen.isMultiSelection()) {
                    data.get(0).setSelected(true);

                    tabularBean.selectedEntities = new BaseEntity[]{data.get(0)};

                }
            }
            tabularBean.selectionAction(null);
        }
        if (tabularBean.dataTable != null) {
            //tabularBean.dataTable.setPaginatorAlwaysVisible(false);
            RequestContext.getCurrentInstance().update(tabularBean.dataTable.getClientId());
        }
        if (tabularBean.getScreenHeight() == null || tabularBean.getScreenHeight().equals("") || tabularBean.getScreenHeight().equals("null")) {
            RequestContext.getCurrentInstance().execute(
                    "top.FABS.Portlet.updateIframeSize('" + tabularBean.getPortletInsId() + "Frame');");

        } else {
        }
        if (tabularBean.isLookupMode()) {
            markSelectedElements(data);
        }
        return data;
    }

    private void markSelectedElements(List<BaseEntity> data) {
        if (data != null && !data.isEmpty()) {
            long lookupMainScrInstID = tabularBean.getLookupMainScrInstID(tabularBean.getCurrentScreen().getName());
            String lookupFieldExp = (String) SessionManager.getSessionAttribute("lookupFieldExp");
            LinkedList lookupPickedElementList = (LinkedList) SessionManager.getSessionAttribute(tabularBean.getPickedElementKey(lookupMainScrInstID, lookupFieldExp));
            if (lookupPickedElementList != null && !lookupPickedElementList.isEmpty()) {
                int selectedEntitiesArrSize = 0;
                for (BaseEntity loadedElement : data) {
                    if (lookupPickedElementList.contains(loadedElement)) {
                        loadedElement.setSelected(true);
                        selectedEntitiesArrSize++;
                    }
                }
                if (selectedEntitiesArrSize != 0) {
                    tabularBean.selectedEntities = new BaseEntity[selectedEntitiesArrSize];
                    int i = 0;
                    for (BaseEntity element : data) {
                        if (element.isSelected()) {
                            tabularBean.selectedEntities[i++] = element;
                        }
                    }
                }
            }
        }
    }

    private void filterLoadData(Map<String, String> filters) {
        tabularBean.dataFilter = new ArrayList<String>();
        List<String> tempFilter = null;

        tabularBean.updateFilterForTransient(filters);
        tempFilter = tabularBean.getGridFilter();
        if (tempFilter != null) {
            tabularBean.dataFilter.addAll(tempFilter);
        }
        tempFilter = tabularBean.getInputFilter();
        if (tempFilter != null) {
            tabularBean.dataFilter.addAll(tempFilter);
        }
        tempFilter = tabularBean.getScreenFilter();
        if (tempFilter != null) {
            tabularBean.dataFilter.addAll(tempFilter);
        }

        if (tabularBean.getActivityStateFilter() != null) {
            tabularBean.dataFilter.addAll(tabularBean.getActivityStateFilter());
        }
        if (tabularBean.getActivityStateFilter() == null && !tabularBean.getCurrentScreen().isShowActiveOnly()) {
            tabularBean.dataFilter.add("##ACTIVEANDINACTIVE");
        }
        if (tabularBean.getCurrentScreen().getScreenFilter() != null
                && !tabularBean.getCurrentScreen().getScreenFilter().isInActive()) {
            try {
                tabularBean.dataFilter.addAll(tabularBean.filterServiceBean.getFilterConditions(
                        tabularBean.getCurrentScreen().getScreenFilter(),
                        tabularBean.getUserSessionInfo(), tabularBean.loggedUser));
            } catch (FilterDDRequiredException ex) {
                logger.error("Exception thrown", ex);
            }
        }
        tabularBean.dataFilter.add(OEntityManager.CONFCOND_CASE_INSENSITIVE);

        if (!filters.isEmpty()) {
            for (Map.Entry<String, String> entry : filters.entrySet()) {
                //loop on screen fields default values
                List<ScreenField> screenFields = tabularBean.getCurrentScreen().getScreenFields();
                for (int screenFieldIndex = 0; screenFieldIndex < screenFields.size(); screenFieldIndex++) {
                    String defaultValue = screenFields.get(screenFieldIndex).getDd().getDefaultValue();
                    String fieldName = tabularBean.getCurrentScreen().getScreenFields().get(screenFieldIndex).getFieldExpression();

                    //handling that the field have decimal mask with no default value
                    Integer decimalMask = tabularBean.getCurrentScreen().getScreenFields().get(screenFieldIndex).getDd().getDecimalMask();
                    if (decimalMask != null) {
                        defaultValue = "0";
                    }
                    //handling searching by many 00000
                    if (FramworkUtilities.isNumber(entry.getValue()) && Integer.parseInt(entry.getValue()) < 1) {
                        entry.setValue("0");
                    }
                    //handling that searching by default value which is not in DB
                    if (entry.getValue().equals(defaultValue) && entry.getKey().equals(fieldName)) {
                        tabularBean.dataFilter.set(0, tabularBean.dataFilter.get(0).concat("AND entity." + fieldName + "=" + entry.getValue() + " OR entity." + fieldName + " IS NULL "));
                    }
                }
            }
        }
        tabularBean.updateDataFilter();
    }

    private void sortLoadData(String sortField, SortOrder sortOrder) {
        tabularBean.dataSort.clear();
        if (sortField == null) {
            List<String> sortExp = new ArrayList<String>();
            String sortExpress = ((TabularScreen) tabularBean.getCurrentScreen()).getSortExpression();
            if (sortExpress != null && !sortExpress.equals("")) {
                String exps[] = sortExpress.split(",");
                for (int i = 0; i < exps.length; i++) {
                    String string = exps[i].trim();
                    sortExp.add(string);
                }
            }
            for (String sortExpression : sortExp) {
                if (!sortExpression.contains(" ")) {
                    if (sortExpression.startsWith("lower(")) {
                        sortExpression = sortExpression.trim().substring(6, sortExpression.lastIndexOf(")"));
                    }
                    if (!tabularBean.dataSort.contains(sortExpression)) {
                        tabularBean.dataSort.add(sortExpression);
                    }
                } else {
                    if (sortExpression.startsWith("lower(")) {
                        sortExpression = sortExpression.trim().substring(6, sortExpression.lastIndexOf(")"));
                        if (!tabularBean.dataSort.contains(sortExpression)) {
                            tabularBean.dataSort.add(sortExpression);
                        }
                    } else if (sortExpression.matches(".* desc")) {
                        if (!tabularBean.dataSort.contains(sortExpression)) {
                            tabularBean.dataSort.add(sortExpression);
                        }
                    }
                }
            }
        } else {
            if (sortOrder.equals(SortOrder.ASCENDING)) {
                tabularBean.dataSort.add(sortField + " asc");
            }
            if (sortOrder.equals(SortOrder.DESCENDING)) {
                tabularBean.dataSort.add(sortField + " desc");
            }
        }
    }

    public void addRow(BaseEntity newEntity, int index) {
        if (newEntities != null && !newEntities.isEmpty()) {
            newEntities.clear();
        }
        if (datasource == null) {
            datasource = new ArrayList<BaseEntity>();
        }
        if (datasource.contains(newEntity)) {
            return;
        }
        int c = 0;
        for (int i = 0; i < getIndicies().size(); i++) {
            if (index <= getIndicies().get(i)) {
                getIndicies().set(i, getIndicies().get(i) + 1);
            } else {
                c = i + 1;
            }
        }
        int place = index % getPageSize();
        getIndicies().add(c, index);
        newEntities.add(c, newEntity);
        datasource.add(place, newEntity);
        if (datasource.size() > getPageSize()) {
            datasource.remove(datasource.size() - 1);
        }
        tabularBean.setLoadedList(datasource);
    }

    public void addRowAfterSave(BaseEntity newEntity, int index) {
        int c = getEntityIndex(newEntity.getInstID());
        if (c >= tabularBean.getFirst() && c < tabularBean.getFirst() + getPageSize()) {
            if (!datasource.contains(newEntity)) {
                datasource.add(c % getPageSize(), newEntity);
                datasource = datasource.subList(0, Math.min(datasource.size(), getPageSize()));
            }
        }
    }

    public void removeRow(BaseEntity newEntity) {
        for (int i = 0; i < newEntities.size(); i++) {
            BaseEntity entity = newEntities.get(i);
            if (entity.getInstID() == newEntity.getInstID()) {
                newEntities.remove(i);
                getIndicies().remove(i);
                return;
            }
        }
    }

    public int getNewEntitiesSize() {
        return newEntities.size();
    }

    public List<BaseEntity> getNewEntities() {
        return newEntities;
    }

    public void clearNewEntities() {
        newEntities.clear();
    }

    public int getEntityIndex(long instId) {
        for (int i = 0; i < newEntities.size(); i++) {
            BaseEntity baseEntity = newEntities.get(i);
            if (baseEntity.getInstID() == instId) {
                return getIndicies().get(i);
            }
        }
        return -1;
    }

    public List<Integer> getIndicies() {
        return indicies;
    }

    public void setIndicies(List<Integer> indicies) {
        this.indicies = indicies;
    }
}
