/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.servlet;

import com.unitedofoq.fabs.core.alert.services.AlertServiceLocal;

import java.util.Calendar;
import java.util.Timer;
import javax.ejb.EJB;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author aelzaher
 */
public class AlertServlet implements ServletContextListener {

    @EJB
    AlertServiceLocal alertService;
    final static Logger logger = LoggerFactory.getLogger(AlertServlet.class);
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.debug("Entering");
        ServletContext servletContext = sce.getServletContext();
            // create the timer and timer task objects
            Timer timer = new Timer();
         
        try {
            // get a calendar to initialize the start time 30 seconds after deployment
            Calendar calendar = Calendar.getInstance();
            calendar.set(calendar.getTime().getYear(), calendar.getTime().getMonth(), calendar.getTime().getDay(), 
                    10, 35, 0);

            // save our timer for later use
            servletContext.setAttribute("AlertTimer", timer);
        } catch (Exception e) {
            servletContext.log("Problem initializing the function refresh alerts: " + e.getMessage());
        }
        logger.debug("Returning");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ServletContext servletContext = sce.getServletContext();

        // get our timer from the Context
        Timer timer = (Timer) servletContext.getAttribute("AlertTimer");

        // cancel all pending tasks in the timers queue
        if (timer != null) {
            timer.cancel();
        }

        // remove the timer from the servlet context
        servletContext.removeAttribute("AlertTimer");
    }

}
