/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.servlet;

import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.central.OCentralUser;
import com.unitedofoq.fabs.central.OTenant;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserServiceRemote;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.ejb.EJB;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This servlet is used to synchronize OUsers with LDAP every interval
 * @author arezk
 */
public class IntervalServlet implements ServletContextListener {

    @EJB
    OEntityManagerRemote oem;
    @EJB
    UserServiceRemote userServiceRemote;
    @EJB
    private OCentralEntityManagerRemote centralOEM ;
    final static Logger logger = LoggerFactory.getLogger(IntervalServlet.class);
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext servletContext = sce.getServletContext();
            // create the timer and timer task objects
            Timer timer = new Timer();
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    try {
                    } catch (Exception ex) {
                        logger.error("Exception thrown: ",ex);
                    }
                }
            };
    
        try {
            // get a calendar to initialize the start time
            Calendar calendar = Calendar.getInstance();
            Date startTime = calendar.getTime();

            // schedule the task to run every 60 mins
            timer.scheduleAtFixedRate(timerTask, startTime, 1000 * 60 * 60);

            // save our timer for later use
            servletContext.setAttribute("timer", timer);
        } catch (Exception e) {
            servletContext.log("Problem initializing the task that was to synch users and roles: " + e.getMessage());
        }
    }
    
    @Override    
    public void contextDestroyed(ServletContextEvent sce) {
        ServletContext servletContext = sce.getServletContext();

        // get our timer from the Context
        Timer timer = (Timer) servletContext.getAttribute("timer");

        // cancel all pending tasks in the timers queue
        if (timer != null) {
            timer.cancel();
        }

        // remove the timer from the servlet context
        servletContext.removeAttribute("timer");
    }
}

