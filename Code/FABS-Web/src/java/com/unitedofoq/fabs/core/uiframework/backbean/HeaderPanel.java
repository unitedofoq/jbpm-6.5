/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;


import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.OObjectBriefInfoField;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;

import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.context.FacesContext;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.menubar.Menubar;
import org.primefaces.component.menuitem.MenuItem;
import org.primefaces.component.spacer.Spacer;
import org.slf4j.LoggerFactory;

/**
 *
 * @author arezk
 */
public class HeaderPanel {
final static org.slf4j.Logger logger = LoggerFactory.getLogger(HeaderPanel.class);
    // <editor-fold defaultstate="collapsed" desc="Fields with Getters and Setters">
    private ExpressionFactory exFactory;
    private ELContext elContext;
    private OBackBean backBean;
    private String screenMenuAction = "screenMenuAction";
    private String screenMenuTranslationAction = "screenTranslationMenuAction";
    private CommandButton portletPageButton;
    private Menubar screenUtilitiesMenu;
    private MenuItem mainItem;
    private HtmlPanelGrid headerPanel;
    private HtmlPanelGrid grid;
    private HtmlPanelGrid headerPanelGrid;
    private OObjectBriefInfoField masterField;
    private String id;
    private EntitySetupServiceRemote oEntitySetupServiceLocal;
    private OEntityManagerRemote oem;
    private DDServiceRemote ddService;

    public ExpressionFactory getExFactory() {
        return exFactory;
    }

    public HtmlPanelGrid getHeaderPanelGrid() {
        return headerPanelGrid;
    }

    public void setHeaderPanelGrid(HtmlPanelGrid headerPanelGrid) {
        this.headerPanelGrid = headerPanelGrid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public OObjectBriefInfoField getMasterField() {
        return masterField;
    }

    public void setMasterField(OObjectBriefInfoField masterField) {
        this.masterField = masterField;
    }

    public String getScreenFieldsAction() {
        return screenMenuAction;
    }

    public void setScreenFieldsAction(String screenFieldsAction) {
        this.screenMenuAction = screenFieldsAction;
    }

    public OBackBean getBackBean() {
        return backBean;
    }

    public ELContext getElContext() {
        return elContext;
    }

    public MenuItem getMainItem() {
        return mainItem;
    }

    public void setMainItem(MenuItem mainItem) {
        this.mainItem = mainItem;
    }

    public Menubar getScreenUtilitiesMenu() {
        return screenUtilitiesMenu;
    }

    public void setScreenUtilitiesMenu(Menubar screenUtilitiesMenu) {
        this.screenUtilitiesMenu = screenUtilitiesMenu;
    }

    public HtmlPanelGrid getHeaderPanel() {
        return headerPanel;
    }

    public HtmlPanelGrid getGrid() {
        return grid;
    }

    public CommandButton getPortletPageButton() {
        return portletPageButton;
    }

    public void setPortletPageButton(CommandButton portletPageButton) {
        this.portletPageButton = portletPageButton;
    }
    private OUser loggedUser;

    public OUser getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(OUser loggedUser) {
        this.loggedUser = loggedUser;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Constructor">
    /**
     *
     * @param backbean
     * @param exFactory
     * @param context
     * @param headerEntityOEntity
     */
    public HeaderPanel(
            OBackBean backBean,
            ExpressionFactory exFactory,
            FacesContext facesContext,
            OEntityDTO headerEntityOEntity,
            String headerPanelId,
            EntitySetupServiceRemote entitySetupServiceLocal,
            OEntityManagerRemote oem,
            DDServiceRemote ddService,
            OUser loggedUser) {
        this.exFactory = exFactory;
        this.backBean = backBean;
        this.displayedEntityOEntity = headerEntityOEntity;
        this.screenUtilitiesMenu = new Menubar();
        this.mainItem = new MenuItem();
        this.portletPageButton = new CommandButton();
        //this.commandLink = new HtmlCommandLink();
        this.masterField = new OObjectBriefInfoField();
        this.headerPanelGrid = new HtmlPanelGrid();
        this.headerPanel = new HtmlPanelGrid();
        this.grid = new HtmlPanelGrid();
        this.elContext = facesContext.getELContext();
        this.ddService = ddService;
        this.oem = oem;
        this.oEntitySetupServiceLocal = entitySetupServiceLocal;
        this.id = headerPanelId;
        this.loggedUser = loggedUser;
        id = id.replaceAll(" ", "");
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Init Header Panel">
    private void initHeaderPanel() {
        // Create header panel
        headerPanel.setStyle("position:relative;");
        // Create header
        headerPanelGrid.setColumns(7);
        headerPanelGrid.setId("headerPanelGrid");
        headerPanelGrid.setBorder(0);
        headerPanelGrid.setCellpadding("3px");
        headerPanelGrid.setCellspacing("3px");
    }
    // </editor-fold>

    private void initButton() {

    }

    // <editor-fold defaultstate="collapsed" desc="Portlet Page Button">
    private void initPortletPageButton() {
        portletPageButton.setStyle("width: 30px; height: 30px; position:absolute; top:3px; right:5px");
        portletPageButton.setImage("/resources/cnfg.jpg");
        portletPageButton.setImmediate(true);
        portletPageButton.setPartialSubmit(true);
        //portletPageButton.setStyle("position:absolute; width:20px; height:20px");
        portletPageButton.setId(id + "_screenFields");
        DD portletPageButtonDD = ddService.getDD("portletPageButton", loggedUser);
        portletPageButton.setTitle(portletPageButtonDD.getLabelTranslated());
        portletPageButton.setActionExpression(exFactory.createMethodExpression(
                elContext,
                "#{" + backBean.getBeanName() + ".portletPageAction}",
                String.class, new Class[0]));
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Init Panel Grid">
    private void initPanelGrid() {
        grid.setId(id + "_sfpanelID");
        grid.setColumns(1);
        headerPanelGrid.getChildren().add(grid);
        if (backBean.isShowManagePortlePage()) {
            grid.getChildren().add(portletPageButton);
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Init Header Title">
    private void createHeaderTitle() {
        HtmlOutputLabel screenTitleLabel = new HtmlOutputLabel();
        screenTitleLabel.setStyleClass("headerTitle");
        screenTitleLabel.setId(id + "screenTitleLBL");
        if (backBean.currentScreenInput != null && backBean.currentScreenInput.getInputHeader() != null
                && !backBean.currentScreenInput.getInputHeader().equals("")) {
            screenTitleLabel.setValue(backBean.currentScreenInput.getInputHeader());
        } else if (backBean.getCurrentScreen().getHeaderTranslated() != null) {
            screenTitleLabel.setValue(backBean.getCurrentScreen().getHeaderTranslated());
        }
        headerPanelGrid.getChildren().add(screenTitleLabel);
        if (!backBean.isC2AMode()) {
            HtmlPanelGrid menuAndTips = new HtmlPanelGrid();
            menuAndTips.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_MenuAndTips");

            //headerPanelGrid.getChildren().add(screenUtilitiesMenu);
            if (backBean.fABSSetupLocal.loadKeySetup("EnableInlineHelp", loggedUser).isBvalue()) {
                HtmlGraphicImage helpImg = new HtmlGraphicImage();
                helpImg.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_helpImage");
                helpImg.setUrl("/resources/help.png");
                helpImg.setStyleClass("hdr-hlp-img"); 
                HtmlPanelGroup group = new HtmlPanelGroup();
                group.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_group");
                group.getChildren().add(helpImg);
            }
            headerPanelGrid.getChildren().add(menuAndTips);
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Create Master Fields">
    private void createMasterFields() {
        logger.trace("Entering");
        // Header Object is not null, create fields for it
        // Get Header Entity brief info fields
        try {
            String headerEntityEntityName = displayedEntityOEntity.getEntityClassName();
            List<OObjectBriefInfoField> briefInfoFields;

            briefInfoFields = oEntitySetupServiceLocal.getObjectDTOBriefInfoFields(
                    displayedEntityOEntity, oem.getSystemUser(loggedUser));

            // Get Master Field
            int briefInfoFieldsCount = briefInfoFields == null ? 0 : briefInfoFields.size();
            for (int i = 0; i < briefInfoFieldsCount; i++) {
                if (briefInfoFields.get(i).isMasterField()) {
                    masterField = briefInfoFields.get(i);
                    break;
                }
            }
            if (briefInfoFieldsCount == 0 || masterField == null) {
                logger.warn("Entity: {} must have master brief info",headerEntityEntityName);
                logger.trace("Returning");
                return;
            }
            // </editor-fold>

            // Create master field label
            HtmlOutputLabel masterFieldLabel = new HtmlOutputLabel();
            masterFieldLabel.setStyle("font-size: 18px; color: #0099dd;font-family: Arial;");
            masterFieldLabel.setId(id + "masterFldLabel");
            List<String> fieldExpression = new ArrayList<String>();
            if (masterField.getFieldExpression() == null) {
                logger.trace("Returning");
                return;
            }
            fieldExpression.add(masterField.getFieldExpression());
            if (displayedEntityBackBeanExpression == null) {
                logger.warn("HeaderEntity of type: {} must be loaded",headerEntityEntityName);
                logger.trace("Returning");
                return;
            }

            masterFieldLabel.setValueExpression(
                    "value", exFactory.createValueExpression(elContext, "#{"
                    + backBean.getBeanName() + "." + displayedEntityBackBeanExpression + "."
                    + masterField.getFieldExpression() + "}", String.class));
            headerPanelGrid.getChildren().add(masterFieldLabel);
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        logger.trace("Returning");
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Create BriefInfoFields">
    private void createBriefInfoFields() {
        logger.trace("Entering");
        HtmlPanelGrid basicGrid = new HtmlPanelGrid();
        basicGrid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "HeaderBscGrid");
        basicGrid.setColumns(4);
        basicGrid.setBorder(0);
        basicGrid.setCellpadding("5px");
        basicGrid.setCellspacing("5px");
        HtmlOutputText lbl;

        if (displayedEntityOEntity != null && backBean.getHeaderObject() != null) {

            List<OObjectBriefInfoField> briefInfoFields = null;
            try {
                briefInfoFields = backBean.entitySetupService.getObjectDTOBriefInfoFields(
                        displayedEntityOEntity, backBean.systemUser);
            } catch (Exception ex) {
                logger.error("Exception thrown",ex);
            }

            for (int i = 0; i < briefInfoFields.size(); i++) {
                if (briefInfoFields.get(i).isPrimary()) {
                    lbl = new HtmlOutputText();
                    lbl.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "lbl");
                    List<String> bicondt = new ArrayList<String>();
                    bicondt = new ArrayList<String>();
                    bicondt.add("fieldExpression='DD.label'");
                    bicondt.add("entityDBID=" + briefInfoFields.get(i).getDd().getDbid());

                    if (briefInfoFields.get(i).getTitleOverride() != null) {
                        if (!briefInfoFields.get(i).getTitleOverride().isEmpty()) {
                            lbl.setValue(briefInfoFields.get(i).getTitleOverride() + "  ");
                        } else {
                            lbl.setValue(briefInfoFields.get(i).getDd().getLabel() + "  ");
                        }
                    } else {
                        lbl.setValue(briefInfoFields.get(i).getDd().getLabel() + "  ");
                    }

                    lbl.setStyle(";font-weight:bold;");
                    basicGrid.getChildren().add(lbl);

                    lbl = new HtmlOutputText();
                    lbl.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "lblP");
                    lbl.setStyle("");
                    String tmpAttr = briefInfoFields.get(i).getFieldExpression();
                    lbl.setValueExpression("value",
                            exFactory.createValueExpression(elContext, "#{" + backBean.getBeanName() + ".headerObject."
                            + tmpAttr + "}", String.class));
                    basicGrid.getChildren().add(lbl);
                }
            }
        }

        HtmlPanelGrid space = new HtmlPanelGrid();
        space.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "Space");
        space.setStyle("height:10px");
        if (basicGrid.getChildCount() > 0) {
        }
        headerPanel.getChildren().add(basicGrid);
        HtmlPanelGrid separator = new HtmlPanelGrid();
        separator.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "Sep");
        separator.setStyle("width:100%;height:1px");
        Spacer divider = new Spacer();
        divider.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "Divider");
        divider.setStyle("width:100%;height:1px");
        HtmlPanelGrid space2 = new HtmlPanelGrid();
        space2.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "Sep2");
        space2.setStyle("height:10px");
        logger.trace("Returning");
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Create Balloon">
    private void createBalloon() {
        logger.trace("Entering");
        String ddName = null;
        try {
            if (masterField.getFieldExpression() == null) {
                logger.trace("Returning");
                return;
            }
            ddName = BaseEntity.getFieldDDName(Class.forName(displayedEntityOEntity.getEntityClassPath()),
                    masterField.getFieldExpression());
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        if (ddName != null && !ddName.equals("")) {
            DD dataDictionary = ddService.getDD(ddName, oem.getSystemUser(loggedUser));
            if (null != dataDictionary && dataDictionary.isBalloon()) {
            }
        }
        logger.trace("Returning");
    }
    // </editor-fold>

    public HtmlPanelGrid createHeaderPanel() {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>
        initHeaderPanel();
        if (!backBean.isC2AMode()) {
            initButton();
        } else if (backBean.isShowManagePortlePage()) {
            initPortletPageButton();
        }

        initPanelGrid();
        createHeaderTitle();
        if (displayedEntityOEntity != null && backBean.getHeaderObject() != null) {
            if (!backBean.getHeaderObject().getClass().getSimpleName().equals(
                    displayedEntityOEntity.getEntityClassName())) 
            {
                logger.warn("Passed Header Object is Not the Same as header balloon type");
            }
            createMasterFields();
            createBalloon();
        }

        headerPanel.getChildren().add(headerPanelGrid);
        createBriefInfoFields();
        Long endTime = (Calendar.getInstance()).getTimeInMillis();
        logger.debug("Successfully created the Header Panel");
        logger.debug("Returning");
        return headerPanel;
    }

    // <editor-fold defaultstate="collapsed" desc="displayedEntity">
    private OEntityDTO displayedEntityOEntity;
    public OEntityDTO getDisplayedEntityOEntity() {
        return displayedEntityOEntity;
    }

    public void setDisplayedEntityOEntity(OEntityDTO displayedEntityOEntity) {
        this.displayedEntityOEntity = displayedEntityOEntity;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="displayedEntityBackBeanExpression">
    private String displayedEntityBackBeanExpression = "headerObject";
    public String getDisplayedEntityBackBeanExpression() {
        return displayedEntityBackBeanExpression;
    }

    public void setDisplayedEntityBackBeanExpression(String displayedEntityBackBeanExpression) {
        this.displayedEntityBackBeanExpression = displayedEntityBackBeanExpression;
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="displayedEntity">
    private BaseEntity displayedEntity ;
    public BaseEntity getDisplayedEntity() {
        return displayedEntity;
    }

    public void setDisplayedEntity(BaseEntity displayedEntity) {
        this.displayedEntity = displayedEntity;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="HtmlPanelGrid">
    private HtmlPanelGrid HtmlPanelGrid ;
    public HtmlPanelGrid getHtmlPanelGrid() {
        return HtmlPanelGrid;
    }

    public void setHtmlPanelGrid(HtmlPanelGrid HtmlPanelGrid) {
        this.HtmlPanelGrid = HtmlPanelGrid;
    }
    //</editor-fold>
}
