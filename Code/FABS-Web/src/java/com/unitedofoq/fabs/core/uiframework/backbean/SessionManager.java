/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import java.util.HashMap;
import java.util.Map;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author htawab
 */
public class SessionManager {

    public static ExternalContext getExternalContext() {
        return FacesContext.getCurrentInstance().getExternalContext();
    }

    public static Map<String, Object> getSessionMap() {
        return FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    }

    public static Map<String, String> getRequestParameterMap() {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    }

    /**
     * Store Object value in current user session. Don't use this function to
     * store Object for screen or for a group of screen, only use it to store
     * something across user session.
     *
     * @param key
     * @param Value
     */
    public static void addSessionAttribute(String key, Object Value) {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(key);
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(key, Value);
    }

    /**
     * Gets values from current user session.
     *
     * @param key
     * @return
     */
    public static Object getSessionAttribute(String key) {
        if (FacesContext.getCurrentInstance() == null) {
            return null;
        }
        return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(key);
    }

    /**
     * Set screen session information in the current user session Must be
     * consistent with {@link #getScreenSessionAttribute(java.lang.String) }
     *
     * @param screenInstId {@link OBackBean#screenInstId} of the screen to set
     * attribute for
     * @param key
     * @param value
     */
    public static synchronized void setScreenSessionAttribute(String screenInstId, String key, Object value) {
        // String lPLID = (String) SessionManager.getSessionAttribute("layoutPLID"); not used any more
        // screenInstId is unique per screen instance
        String screenSessionKeyName = screenInstId /*+ "_" + lPLID*/;
        Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        HashMap<String, Object> screenParamsMap = (HashMap<String, Object>) sessionMap.get(screenSessionKeyName);
        if (screenParamsMap == null) {
            screenParamsMap = new HashMap<String, Object>(1);
        }
        screenParamsMap.put(key, value);
        sessionMap.put(screenSessionKeyName, screenParamsMap);
    }

    public static synchronized void setScreenSessionMap(String screenInstId, HashMap<String, Object> screenParamsMap) {
        // String lPLID = (String) SessionManager.getSessionAttribute("layoutPLID"); not used any more
        // screenInstId is unique per screen instance
        String screenSessionKeyName = screenInstId /*+ "_" + lPLID*/;
        Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        sessionMap.put(screenSessionKeyName, screenParamsMap);

    }

    /**
     * Get screen session information from the current user session Must be
     * consistent with {@link #setScreenSessionAttribute(String, String, Object)
     * }
     *
     * @param screenInstId {@link OBackBean#screenInstId} of the screen to get
     * attribute for
     */
    public static synchronized HashMap<String, Object> getScreenSessionAttribute(String screenInstId) {
        // String lPLID = (String) SessionManager.getSessionAttribute("layoutPLID"); not used any more
        // screenInstId is unique per screen instance
        return (HashMap<String, Object>) SessionManager.getSessionAttribute(screenInstId /*+ "_" + lPLID*/);
    }

    public static Map<String, Object> getBrowserCookieMap() {
        return SessionManager.getExternalContext().getRequestCookieMap();
    }

    public static Cookie getCookieValue(String cookieName) {
        Object cookieObj = getBrowserCookieMap().get(cookieName);
        if (cookieObj != null) {
            return (Cookie) cookieObj;
        }
        return null;
    }

    public static void removeCookie(String cookieName) {
        Object cookieObj = getCookieValue(cookieName);
        if (cookieObj != null) {
            Cookie cookie = (Cookie) cookieObj;
            cookie.setMaxAge(0);
            cookie.setValue(null);
            ((HttpServletResponse) SessionManager.getExternalContext().getResponse()).addCookie(cookie);
        }
    }
}
