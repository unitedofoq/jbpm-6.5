/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.rest;

import com.unitedofoq.fabs.core.security.user.UserServiceRemote;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author mmasoud
 */
@Path("userInfoWebService")
public class UserInfoRest {
    
    private UserServiceRemote userService;
    
    public UserInfoRest(){
        try {
            userService = (UserServiceRemote)(new javax.naming.InitialContext()).lookup("java:global/ofoq/com.unitedofoq.fabs.core.security.user.UserServiceRemote");
        } catch (NamingException ex) {
            Logger.getLogger(UserInfoRest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @POST
    @Path("/getDisplayNames")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String getDisplayNames(String users) {
        String requesterDisplayName = userService.getUserDisplayedName(users.split("#")[0]);
        String lastuserDisplayName = userService.getUserDisplayedName(users.split("#")[1]);
        return requesterDisplayName+"#"+lastuserDisplayName;
    }
}
