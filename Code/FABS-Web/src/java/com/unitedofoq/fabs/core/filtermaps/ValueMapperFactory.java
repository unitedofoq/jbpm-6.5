/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.filtermaps;

import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mostafan
 */
public class ValueMapperFactory {
    
    private LiteralDropDownMapper literalDropDownMapper =  new LiteralDropDownMapper();
    private  ValueMapper[] allValueMappers={new BooleanValueMapper(),literalDropDownMapper,new DefaultValueMapper()};
    private ValueMapper valueMapper = null;
    
    public ValueMapperFactory(Field field,DDServiceRemote ddService){
        literalDropDownMapper.setDDService(ddService); 
        for (ValueMapper allValueMapper : allValueMappers) {
            if (allValueMapper.isValidMapperForField(field)) {
                valueMapper = allValueMapper;
                break;
            }
        }
    
    }
    
    public ValueMapper getMapper(){
        return valueMapper;
    }
    
    
    
    
    
    
    
    
    
}

