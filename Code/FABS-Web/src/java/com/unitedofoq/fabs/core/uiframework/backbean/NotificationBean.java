/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.security.user.Notification;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.SelectEvent;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mahmed
 */
@ManagedBean(name = "NotificationBean")
@ViewScoped
public class NotificationBean extends TabularBean {

    final static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(NotificationBean.class);

    
    protected DataTypeServiceRemote dataTypeService = OEJB.lookup(DataTypeServiceRemote.class);

    @Override
    protected boolean onInit(Map<String, String> requestParams) {
        ODataType dt = new ODataType();
        dt.setName("VoidDT");
        dt.setDbid(1);
        ODataMessage voidDataMessage = new ODataMessage();
        voidDataMessage.setData(new ArrayList<Object>());
        voidDataMessage.setODataType(dt);
        SessionManager.setScreenSessionAttribute("ManageNotifications", OPortalUtil.MESSAGE, voidDataMessage);

        requestParams.put("screenName", "ManageNotifications");
        requestParams.put("screenInstId", "ManageNotifications");
        super.onInit(requestParams);

        return true;
    }

    @Override
    public String getBeanName() {
        return "NotificationBean";
    }

    @Override
    protected List<BaseEntity> loadList(
            String entityName, List<String> conditions, List<String> sortConditions) {
        // filter list and get current user notifications only.
        conditions.add("reciever= \'" + getLoggedUser().getLoginName() + "\'");
        return super.loadList(entityName, conditions, sortConditions);
    }

    public String selectionAction(SelectEvent event) {
        LOGGER.debug("Entering");
        if (null != event && null != event.getObject()) {
            Notification selectedNotification = (Notification) event.getObject();
            String screenName = selectedNotification.getScreenName();

            ODataMessage message = new ODataMessage();
            ODataType oDataType = dataTypeService.loadODataType("DBID", loggedUser);
            message.setODataType(oDataType);
            List data = new ArrayList();
            data.add(selectedNotification.getEntitydbid());
            message.setData(data);
            String screenInstanceID = generateScreenInstId(screenName);
            SessionManager.setScreenSessionAttribute(screenInstanceID,
                    OPortalUtil.MESSAGE, message);
            SessionManager.setScreenSessionAttribute(screenInstanceID,
                    MODE_VAR, OScreen.SM_EDIT);
            openScreen(screenName, screenInstanceID);
        }
        return null;
    }
}
