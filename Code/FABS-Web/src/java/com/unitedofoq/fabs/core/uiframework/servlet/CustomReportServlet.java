/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.servlet;

import com.unitedofoq.fabs.core.content.AttachmentDTO;
import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.encryption.DataEncryptorServiceLocal;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.JavaFunction;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.report.customReports.AbstractReportGenerator;
import com.unitedofoq.fabs.core.report.customReports.CSVReportGenerator;
import com.unitedofoq.fabs.core.report.customReports.CustomReportServiceLocal;
import com.unitedofoq.fabs.core.report.customReports.ExcelReportGenerator;
import com.unitedofoq.fabs.core.report.customReports.PDFReportGenerator;
import com.unitedofoq.fabs.core.report.customReports.ReportDTO;
import com.unitedofoq.fabs.core.report.customReports.TextReportGenerator;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserServiceRemote;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import com.unitedofoq.fabs.core.utils.ObjectEncoder;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import javax.ejb.EJB;
import javax.mail.internet.MimeUtility;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Melad
 */
@WebServlet(name = "CustomReportServlet", urlPatterns = {"/CustomReportServlet"})
public class CustomReportServlet extends HttpServlet {
    //<editor-fold defaultstate="collapsed" desc="variable Section">

    final static Logger logger = LoggerFactory.getLogger(CustomReportServlet.class);
    final int BUFF_SIZE = 1024;
    @EJB
    private UserServiceRemote userServiceRemote;
    @EJB
    protected DataTypeServiceRemote dataTypeService;
    @EJB
    OFunctionServiceRemote functionService;
    @EJB
    protected OEntityManagerRemote oem;
    @EJB
    DataEncryptorServiceLocal dataEncryptorService;
    @EJB
    FABSSetupLocal fABSSetupLocal;

    @EJB
    private CustomReportServiceLocal customReportService;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Get Method">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(final HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response, new RequestMap() {
            @Override
            public Object getAndClear(String key) {
                Object value = request.getSession().getAttribute(key);
                //fix drag and drop report portlt
                //request.getSession().setAttribute(key, null);
                return value;
            }
        });
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Post Method">

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(final HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response, new RequestMap() {
            @Override
            public Object getAndClear(String key) {
                return ObjectEncoder.decodeBase64ToObject(request.getParameter(key));
            }
        });
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="processRequest">

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @param dataContainer
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, RequestMap dataContainer)
            throws ServletException, IOException {
        // <editor-fold defaultstate="collapsed" desc="Get Parameters from Session">
        String reportName = (String) dataContainer.getAndClear("reportName");
        AttachmentDTO attachment = (AttachmentDTO) dataContainer.getAndClear("EntityAttachment");
        HashMap<String, Object> parametersMap = (HashMap<String, Object>) dataContainer.getAndClear("parametersMap");
        parametersMap = (parametersMap != null) ? parametersMap : new HashMap<String, Object>();
        OUser loggedUser = (OUser) request.getSession().getAttribute("loggedUser");
        JavaFunction userExit = (JavaFunction) dataContainer.getAndClear("UserExitJavaFunction");
        String whereConditions = (String) dataContainer.getAndClear("Filters");

        boolean localized = Boolean.valueOf(dataContainer.getAndClear("localized") == null ? "false" : dataContainer.getAndClear("localized").toString());
        boolean revertLang = Boolean.valueOf(dataContainer.getAndClear("revertLang") == null ? "false" : dataContainer.getAndClear("revertLang").toString());
        boolean applyingFilter = Boolean.valueOf(dataContainer.getAndClear("applyingFilter") == null ? "false" : dataContainer.getAndClear("applyingFilter").toString());
        boolean fitPage = Boolean.valueOf(dataContainer.getAndClear("fitPage") == null ? "false" : dataContainer.getAndClear("fitPage").toString());
        //</editor-fold>

        if (null == attachment) {
            response.sendRedirect("reportNotFound.jsp");
            return;
        }

        String reportType = request.getParameter("Export");
        String reportTypeLowerCase = (reportType == null) ? null : reportType.toLowerCase();
        genrateReport(whereConditions, parametersMap, attachment, localized, revertLang, applyingFilter, fitPage, loggedUser, request, response,
                userExit, reportName, reportTypeLowerCase);
    }

    // <editor-fold defaultstate="collapsed" desc="Generate Custom Report">
    void genrateReport(String whereConditions,
            HashMap<String, Object> parametersMap,
            AttachmentDTO attachment,
            boolean localized,
            boolean revertLang,
            boolean applyingFilter, boolean fitPage,
            OUser loggedUser,
            HttpServletRequest request,
            HttpServletResponse response,
            JavaFunction userExit,
            String reportName,
            String reportType) {
        logger.trace("Entering");
        //Added by Belal: implementing the strategy design pattern as per the current available report types
        AbstractReportGenerator reportGenerator = null;
        Boolean isInline = false;
        ReportDTO reportDTO = new ReportDTO(attachment, localized, revertLang, applyingFilter, fitPage, parametersMap, whereConditions, reportName, userExit, loggedUser);
        if (reportType == null) {
            isInline = true;
            reportType = "pdf";
        }

        if (reportType.equalsIgnoreCase("pdf")) {
            reportGenerator = new PDFReportGenerator(reportDTO);
        } else if (reportType.equalsIgnoreCase("excel")) {
            reportGenerator = new ExcelReportGenerator(reportDTO);
        } else if (reportType.equalsIgnoreCase("text")) {
            reportGenerator = new TextReportGenerator(reportDTO);
        } else {
            reportGenerator = new CSVReportGenerator(reportDTO);
        }

        reportGenerator.setCustomReportService(customReportService);
        ByteArrayInputStream is = customReportService.generateReport(reportGenerator);
        generateFileGeneric(request, response, is, reportName, isInline, reportType.toLowerCase().replace("excel", "xls"));
    }
    // </editor-fold>

    void generateFileGeneric(HttpServletRequest request, HttpServletResponse response, ByteArrayInputStream is, String reportName, Boolean isInline, String formatName) {
        logger.trace("Entering");
        String loadType = (isInline) ? "inline" : "attachment";
        if (isInline) {
            request.setAttribute("reportType", "BIRT");
        }
        try {
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            response.setContentType("application/" + formatName + "; charset=utf-8");
            // sets HTTP header
            response.setHeader("Content-Disposition", loadType + "; filename=\"" + MimeUtility.encodeWord(reportName + "." + formatName) + "\"");

            byte[] byteBuffer = new byte[BUFF_SIZE];
            // reads the file's bytes and writes them to the response stream
            while ((is != null) && ((length = is.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }
            is.close();
            outStream.close();
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Srevlet Info">
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "This Servlet is used to Generate Cstom Reports";
    }
    //</editor-fold>
}
