/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;


import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mustafa
 */
@FacesConverter(value = "FABSDateConverter", forClass = HtmlOutputLabel.class)
public class FABSDateConverter implements Converter, Serializable {
final static Logger logger = LoggerFactory.getLogger(FABSDateConverter.class);
    String dateFormat = "dd-MM-yyyy";    

    public FABSDateConverter(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public FABSDateConverter() {
    }

    //  Called when an text is written in the screen field. 'label' will
    //  always contain the 'value' .
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String label) {
        logger.debug("Entering");
         if (label == null || label.equals("")) {
             logger.debug("Returning with current Date");
            return new Date();
            }
         
          
        SimpleDateFormat dateFormater = new SimpleDateFormat(dateFormat);
        try {
            return (Object)dateFormater.parse(label);
        } catch (ParseException ex) {
            logger.error("Exception thrown",ex);
        }finally{
            logger.debug("Returning with Null");
            return null;
        }
        
    }

    //  Called when displaying input text with a converter. 'object' will
    // always contain the value 
    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
        logger.debug("Entering");
        // if corresponding label was not found...       
        if (object == null || object.toString().isEmpty()) {
            logger.debug("Returning with empty string");
            return "";
        }
 
        if(object instanceof Date){        
        SimpleDateFormat dateFormater = new SimpleDateFormat(dateFormat);
        return dateFormater.format((Date)object);
        }else{                    
        try{            
            String defaultDateFormat = "EEE MMM dd HH:mm:ss zzz yyyy";
            Date d1 = new SimpleDateFormat(defaultDateFormat).parse(object.toString());
            SimpleDateFormat dateFormater = new SimpleDateFormat(dateFormat);
            logger.debug("Returning");
            return dateFormater.format((Date)d1);
            
        }catch (Exception ex){
            logger.error("Exception thrown",ex);
            if(null!=object)
            logger.debug("Returning with: {}",object.toString());
            return object.toString();
        }
            
        }
    }
}
