/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;


import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import com.unitedofoq.fabs.core.function.OFunctionResult;

import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import com.unitedofoq.fabs.core.uiframework.screen.ChartScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ChartScreenDim3Field;
import com.unitedofoq.fabs.core.uiframework.screen.FormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.validation.FABSException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import org.primefaces.component.chart.UIChart;
import org.primefaces.component.chart.bar.BarChart;
import org.primefaces.component.chart.line.LineChart;
import org.primefaces.component.chart.pie.PieChart;
import org.primefaces.component.panel.Panel;
import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author bassem
 */
@ManagedBean(name = "ChartBean")
@ViewScoped
public class ChartBean extends OBackBean {    
    final static Logger logger = LoggerFactory.getLogger(OEntityManager.class);
    private ChartScreen chartScreen;          
    private UIChart chart;   
    private Panel htmlPanelGrid;
    private List<ChartSeries> chartSerieses;
    private List<String> xaxixLabels;
    public void setHtmlPanelGrid(Panel htmlPanelGrid) {
        this.htmlPanelGrid = htmlPanelGrid;
    }
    
    @Override
    protected List<BaseEntity> getLoadedEntity(){
        return null;
    }
    
    Panel fabsScreenGrid ;

    public Panel getFabsScreenGrid() {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>
        try {
            oem.getEM(systemUser);
            fabsScreenGrid = buildInnerScreenInternal();
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        } finally {
            oem.closeEM(loggedUser);
        }
        logger.debug("Returning");
        return fabsScreenGrid;
    }

    public void setFabsScreenGrid(Panel fabsScreenGrid) {
        this.fabsScreenGrid = fabsScreenGrid;
    }
    
    private Panel buildInnerScreenInternal() {
        try {            
            logger.trace("Entering");
            oem.getEM(systemUser);
            Panel innerScreenPanel = new Panel();
            innerScreenPanel.setId(getFacesContext().getViewRoot().createUniqueId() + "innerScreenPanel");
            FacesContext fc = FacesContext.getCurrentInstance();
            ExpressionFactory ef = fc.getApplication().getExpressionFactory();
            MethodExpression me = ef.createMethodExpression(
            fc.getELContext(), "#{"+getBeanName()+".itemSelect}", 
            null, new Class[] {ItemSelectEvent.class });
            
            FABSAjaxBehavior behavior = new FABSAjaxBehavior();
            
            behavior.setListener(me);            
            
            chartScreen = (ChartScreen) getCurrentScreen();
            String  chartType = chartScreen.getChartType().getValue();
            if (chartType.equalsIgnoreCase("line")) {
                chart = new LineChart();
                setChartCommonProporties(false);                
                innerScreenPanel.getChildren().add(chart);
            } else if (chartType.equalsIgnoreCase("area")) {
                chart = new LineChart();
                setChartCommonProporties(false);
                ((LineChart)chart).setFill(true);
                ((LineChart)chart).addClientBehavior("itemSelect", behavior);
                innerScreenPanel.getChildren().add(chart);
            }else if (chartType.equalsIgnoreCase("areastacked")) {
                chart = new LineChart();
                setChartCommonProporties(false);
                ((LineChart)chart).setFill(true);
                ((LineChart)chart).setStacked(true);
                innerScreenPanel.getChildren().add(chart);
            }else if (chartType.equalsIgnoreCase("bar")) {
                chart = new BarChart();
                setChartCommonProporties(false);            
                innerScreenPanel.getChildren().add(chart);
            }else if (chartType.equalsIgnoreCase("barhorizontal")) {
                chart = new BarChart();
                setChartCommonProporties(false);               
                ((BarChart)chart).setOrientation("horizontal");
                innerScreenPanel.getChildren().add(chart);
            }else if (chartType.equalsIgnoreCase("barstacked")) {
                chart = new BarChart();
                setChartCommonProporties(false);              
                ((BarChart)chart).setStacked(true);
                innerScreenPanel.getChildren().add(chart);
            }else if (chartType.equalsIgnoreCase("filled pie")) {
                chart = new PieChart();
                setChartCommonProporties(true);                               
                innerScreenPanel.getChildren().add(chart);
            }else if (chartType.equalsIgnoreCase("unfilled pie")) {
                chart = new PieChart();
                setChartCommonProporties(true);              
                ((PieChart)chart).setFill(false);
                ((PieChart)chart).setSliceMargin(5);
                ((PieChart)chart).setDiameter(150);
                innerScreenPanel.getChildren().add(chart);
            }
            chart.addClientBehavior("itemSelect", behavior);
            logger.debug("Returning");
            return innerScreenPanel;
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            logger.debug("Returning");
            return new Panel();
        }
    }    
    
    private void setChartCommonProporties(boolean pie)throws Exception{
        chart.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                + "_" + chartScreen.getDbid() + "_LineBarChart");                
        DD yAxisDD = ddService.getDD(chartScreen.getYaxisTitle().getName(), loggedUser);        
        DD xAxisDD = ddService.getDD(chartScreen.getXaxisTitle().getName(), loggedUser);        
        chart.setTitle(chartScreen.getHeaderTranslated());
        chart.setStyle("width:" + chartScreen.getWidth() + "px;"
                + "height:" + chartScreen.getHeight() + "px;");
        //Reolad DD to load the translations
        chart.setYaxisLabel(yAxisDD.getLabelTranslated());
        chart.setXaxisLabel(xAxisDD.getLabelTranslated());
        //categoryModel = setLineChartData();
        if(!pie)
            chart.setValue(setLineandBarChartsData());
        else
            chart.setValue(setPieData());
        chart.setLegendPosition("nw");
    }  
   
    /** Creates a new instance of ChartBean */
    public ChartBean() {
        
    }   
  

    @Override
    protected String getBeanName() {
        return ChartBean.class.getSimpleName();
    }

    @Override
    public ODataMessage saveProcessRecords() {
        return null;
    }

    @Override
    protected void buildScreenExpression(UIComponent component) {
    }

    private CartesianChartModel setLineandBarChartsData() throws Exception, FABSException, ClassNotFoundException {            
        logger.debug("Entering");
        CartesianChartModel cartesianChartModel = new CartesianChartModel();                
        xaxixLabels = getXAxisLabels();
        chartSerieses = getChartSerieses();           
        String dateFunction = (null == chartScreen.getXaxisDateUnit())? null:
                chartScreen.getXaxisDateUnit().getValue();
        chartSerieses = setChartSeriesesPoints(dateFunction,
                        chartSerieses, xaxixLabels);
        for (ChartSeries chartSeries : chartSerieses) {
            cartesianChartModel.addSeries(chartSeries);
        }        
        logger.debug("Returning");
        return cartesianChartModel;
    }
    private PieChartModel setPieData() throws Exception{
        logger.debug("Entering");
        PieChartModel pieChartModel = new PieChartModel();
        String dateFunction = (null == chartScreen.getXaxisDateUnit())? null:
                chartScreen.getXaxisDateUnit().getValue();     
        xaxixLabels = getXAxisLabels();
        pieChartModel.setData(setPieChartPoints(dateFunction, xaxixLabels));
        logger.debug("Returning");
        return pieChartModel;
    }

    public void itemSelect(ItemSelectEvent event) {
        logger.debug("Entering");
        try {
            int itemIndex = event.getItemIndex();
            int seriesIdx = event.getSeriesIndex();
            Map<Object, Number> data = chartSerieses.get(seriesIdx).getData();
            Collection dataValues =  data.values();
            double point = Double.parseDouble(dataValues.toArray()[itemIndex].toString());

            FormScreen formScreen = (FormScreen) uiFrameWorkFacade.getBasicDetailScreen(chartScreen.getDim3OEntity(), systemUser);
            String openedScreenInstId=OBackBean.generateScreenInstId(formScreen.getName());
            if (formScreen == null) {
                oem.closeEM(loggedUser);
                logger.debug("Returning");
                return;
            }
            SessionManager.setScreenSessionAttribute(
                    openedScreenInstId, MODE_VAR, String.valueOf(OScreen.SM_EDIT));
            SessionManager.setScreenSessionAttribute(
                    openedScreenInstId, OPortalUtil.C2A_VAR, false);
            List<String> conditions = new ArrayList<String>();
            conditions.add(chartScreen.getDim3Filter());
            String dim3FldExp = chartScreen.getChartScreenDim3Fields().get(seriesIdx).getDim3FieldExpression();
            conditions.add(dim3FldExp + "=" + point);
            List<BaseEntity> entities = oem.loadEntityList(chartScreen.getDim3OEntity().getEntityClassName(), conditions, null, null, loggedUser);
            BaseEntity entity = null;
            if (entities != null && entities.size() == 1) {
                entity = entities.get(0);
            } else{//TODO:Add check if it is not a date.
               for (BaseEntity baseEntity : entities) {
                   if (chartScreen.getXaxisDateUnit() != null) {
                       String dateFormate = (chartScreen.getXaxisDateUnit().getValue().equalsIgnoreCase("YEAR"))?"yyyy":"mm";
                       Date date = (Date) BaseEntity.getValueFromEntity(baseEntity, chartScreen.getXaxisLabelFieldExp());
                       SimpleDateFormat simpleDateformat = new SimpleDateFormat(dateFormate);
                       if (simpleDateformat.format(date).equals(xaxixLabels.get(itemIndex))) {
                           entity = baseEntity;
                           break;
                       }
                   }else{
                       String xValue = (String) BaseEntity.getValueFromEntity(baseEntity, chartScreen.getXaxisLabelFieldExp());
                       if(xValue.equalsIgnoreCase(xaxixLabels.get(itemIndex))){
                           entity = baseEntity;
                           break;
                       }
                   }
               }
           }
            List dataEntity = new ArrayList();
            dataEntity.add(entity);
             ODataType dataType = dataTypeService.loadODataType(chartScreen.getDim3OEntity().getEntityClassName(), loggedUser);
             ODataMessage message = new ODataMessage(dataType, dataEntity);
            // Pass the same data message
            if (message != null) {
                SessionManager.setScreenSessionAttribute(
                        openedScreenInstId, OPortalUtil.MESSAGE, message);
            }
            openScreen(formScreen, openedScreenInstId);

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
        }
    }

    private List<String> getXAxisLabels() throws Exception{     
        logger.trace("Entering");
        List<String> labels = new ArrayList<String>();
        List<String> conditions = new ArrayList<String>();
        if(null != chartScreen.getXfilter()){
            conditions.add(chartScreen.getXfilter());
        }                
        if (null == chartScreen.getXaxisDateUnit()) {
            
            if (getCurrentScreen().getDataLoadingUserExit() != null) {
                ODataType screenDataLoadingUEDT = dataTypeService.loadODataType("ScreenDataLoadingUE", loggedUser);
                ODataMessage screenDataLoadingUEDM = new ODataMessage();
                screenDataLoadingUEDM.setODataType(screenDataLoadingUEDT);
                List<Object> odmData = new ArrayList<Object>();
                odmData.add(conditions);
                odmData.add(headerObject);
                odmData.add(dataMessage);
                odmData.add(getCurrentScreen());
                screenDataLoadingUEDM.setData(odmData);

                // Call the function
                OFunctionResult functionRetFR = functionServiceRemote.runFunction(
                        getCurrentScreen().getDataLoadingUserExit(), screenDataLoadingUEDM, null, loggedUser);

                if (functionRetFR.getErrors().isEmpty()) {
                    conditions.clear();
                    conditions.addAll((List) functionRetFR.getReturnedDataMessage().getData().get(0));
                }
            }
            
            List<BaseEntity> entitys = oem.loadEntityList(chartScreen.getXaxisOEntity().getEntityClassName(), 
                    conditions, Collections.singletonList(chartScreen.getXaxisLabelFieldExp()), null, loggedUser);
            for (BaseEntity baseEntity : entitys) {
                String xlbl = BaseEntity.getValueFromEntity(baseEntity,chartScreen.getXaxisLabelFieldExp()).toString();
                if(!labels.contains(xlbl))
                    labels.add(xlbl);
            }
        } else {            
            labels.clear();
            if (chartScreen.getXaxisDateUnit().getValue().equalsIgnoreCase("DAY")) {
                labels.add("Sat.");
                labels.add("Sun.");
                labels.add("Mon.");
                labels.add("Tus.");
                labels.add("Wed.");
                labels.add("Thu.");
                labels.add("Fri.");
            } else if (chartScreen.getXaxisDateUnit().getValue().equalsIgnoreCase("MONTH")) {
                labels.add("Jan.");
                labels.add("Feb.");
                labels.add("Mar.");
                labels.add("Apr.");
                labels.add("May.");
                labels.add("Jun.");
                labels.add("Jul.");
                labels.add("Aug.");
                labels.add("Sep.");
                labels.add("Oct.");
                labels.add("Nov.");
                labels.add("Dec.");
            } else if (chartScreen.getXaxisDateUnit().getValue().equalsIgnoreCase("YEAR")) {
                Calendar startDateCal = Calendar.getInstance();
                startDateCal.setTime(chartScreen.getStartDate());

                Calendar endDateCal = Calendar.getInstance();
                endDateCal.setTime(chartScreen.getEndDate());
                for (int i = startDateCal.get(Calendar.YEAR); i < endDateCal.get(Calendar.YEAR); i++) {
                    labels.add(String.valueOf(i));
                }
            }
         
        }
        logger.trace("Returning");
        return labels;
    }

    private List<ChartSeries> getChartSerieses() throws Exception{
        logger.trace("Entering");
        List<ChartSeries> serieses = new ArrayList<ChartSeries>();
        List<ChartScreenDim3Field> chartScreenDim3Fields = null;        
        List<UDC> dim3UDCs = new ArrayList<UDC>();
        
        //Get Dim3 Labels, it is either a udc or ChartScreenDim3Fields
        if (chartScreen.getChartScreenDim3Fields() != null && !chartScreen.getChartScreenDim3Fields().isEmpty()) {
            chartScreenDim3Fields = chartScreen.getChartScreenDim3Fields();
            for (int i = 0; i < chartScreenDim3Fields.size(); i++) {
                String ddName = BaseEntity.getFieldDDName(Class.forName(
                        chartScreenDim3Fields.get(i).getChartScreen().getDim3OEntity().getEntityClassPath()),
                        chartScreenDim3Fields.get(i).getDim3FieldExpression());
                if(null == ddName)ddName = "SCBCase_caseNOTranslated";
                DD dd = ddService.getDD(ddName, loggedUser);                
                serieses.add(new ChartSeries(dd.getLabelTranslated()));                
            }            
        }else {
            List<BaseEntity> entitys = oem.loadEntityList(chartScreen.getDim3OEntity().getEntityClassName(), 
                    Collections.singletonList(chartScreen.getDim3Filter()),
                    Collections.singletonList(chartScreen.getDim3LabelFieldExp()), null, loggedUser);            
            for (BaseEntity baseEntity : entitys) {
                UDC udc = (UDC)BaseEntity.getValueFromEntity(baseEntity, chartScreen.getDim3LabelFieldExp());
                if(!dim3UDCs.contains(udc))
                    dim3UDCs.add(udc);
            }
            for (UDC udc : dim3UDCs) {    
                udc = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udc.getDbid(),null,
                        null, loggedUser);
                serieses.add(new ChartSeries(udc.getValueTranslated()));
            }
           
        }
        logger.trace("Returning");
        return serieses;
    }

    private Map<String,Number> setPieChartPoints(String dateFunction,
            List<String> xaxixLabels) throws Exception {
        logger.trace("Returning");
        Map<String,Number> data = new HashMap<String, Number>();
        String dateFormate;
        if (dateFunction.equalsIgnoreCase("Year")) {
            dateFormate = "yyyy";
        } else if (dateFunction.equalsIgnoreCase("Month")) {
            dateFormate = "mm";
        } else {
            dateFormate = "dd";
        }
        List<String> conditions = new ArrayList<String>();
        if (chartScreen.getXaxisCalcFieldExp() != null && !"".endsWith(chartScreen.getXaxisCalcFieldExp())) {
            conditions.add(chartScreen.getXaxisCalcFieldExp());
        }
        if (chartScreen.getDim3LabelFieldExp() != null && !"".equals(chartScreen.getXaxisCalcFieldExp())) {
            conditions.add(chartScreen.getXaxisCalcFieldExp());
        }
        if (chartScreen.getYfilter() != null && !"".equals(chartScreen.getYfilter())) {
            conditions.add(chartScreen.getYfilter());
        }
        for (int xaxixIdx = 0; xaxixIdx < xaxixLabels.size(); xaxixIdx++) {
            for (int dim3Idx = 0; dim3Idx < chartScreen.getChartScreenDim3Fields().size(); dim3Idx++) {
                double point = 0;
                int countForNonDim3Fields = 0;
                List<Double> pointData = new ArrayList<Double>();
                List<BaseEntity> entitys = oem.loadEntityList(
                        chartScreen.getDim3OEntity().getEntityClassName(),
                        Collections.singletonList(chartScreen.getDim3Filter()), conditions, null, loggedUser);
                if (null != dateFunction) {
                    SimpleDateFormat simpleDateformat = new SimpleDateFormat(dateFormate);
                    for (BaseEntity baseEntity : entitys) {
                        Date date = (Date) BaseEntity.getValueFromEntity(baseEntity,
                                chartScreen.getXaxisLabelFieldExp());
                        if(null == date)continue;
                        if (simpleDateformat.format(date).equals(xaxixLabels.get(xaxixIdx))) {
                        
                                pointData.add(Double.parseDouble( BaseEntity.getValueFromEntity(
                                    baseEntity, chartScreen.getChartScreenDim3Fields().get(dim3Idx).
                                    getDim3FieldExpression()).toString()));

                        }
                    }
                }else{
                    for (BaseEntity baseEntity : entitys) {
                         pointData.add((Double) BaseEntity.getValueFromEntity(
                                    baseEntity, chartScreen.getChartScreenDim3Fields().get(dim3Idx).
                                    getDim3FieldExpression()));
                    }
                }

                if (chartScreen.getAggregateFunction().getValue().equalsIgnoreCase("SUM")) {
                    for (Double pointD : pointData) {
                        point += pointD;
                    }
                } else if (chartScreen.getAggregateFunction().getValue().equalsIgnoreCase("COUNT")) {
                    point = pointData.size();
                } else {
                    for (Double pointD : pointData) {
                        point += pointD;
                    }
                    point = point / pointData.size();
                }
                point = (countForNonDim3Fields == 0)?point:countForNonDim3Fields;
                data.put(xaxixLabels.get(xaxixIdx), point);
            }
        }
        logger.trace("Returning");
        return data;
    }

    private List<ChartSeries> setChartSeriesesPoints(String dateFunction, List<ChartSeries> chartSerieses,
            List<String> xaxixLabels) throws Exception {
        logger.trace("Entering");
        String dateFormate = null;
        if(null !=  dateFunction){
            if (dateFunction.equalsIgnoreCase("Year")) {
                dateFormate = "yyyy";
            } else if (dateFunction.equalsIgnoreCase("Month")) {
                dateFormate = "MM";
            } else {
                dateFormate = "dd";
            }
        }
        List<String> conditions = new ArrayList<String>();
        if (chartScreen.getXaxisCalcFieldExp() != null && !"".endsWith(chartScreen.getXaxisCalcFieldExp())) {
            conditions.add(chartScreen.getXaxisCalcFieldExp());
        }
        if (chartScreen.getDim3LabelFieldExp() != null && !"".equals(chartScreen.getXaxisCalcFieldExp())) {
            conditions.add(chartScreen.getXaxisCalcFieldExp());
        }
        if (chartScreen.getYfilter() != null && !"".equals(chartScreen.getYfilter())) {
            conditions.add(chartScreen.getYfilter());
        }
        String xAxis = chartScreen.getXaxisCalcFieldExp();
        if(xaxixLabels.size() == 0){
            xaxixLabels.add("");
        }
        for (int xaxixIdx = 0; xaxixIdx < xaxixLabels.size(); xaxixIdx++) {
            for (int dim3Idx = 0; dim3Idx < chartSerieses.size(); dim3Idx++) {
                double point = 0;
                int countForNonDim3Fields = 0;
                List<Double> pointData = new ArrayList<Double>();
                
                List<String> loadingConds = new ArrayList<String>();
                loadingConds.add(chartScreen.getDim3Filter());
                //User Exit
                if (getCurrentScreen().getDataLoadingUserExit() != null) {
                ODataType screenDataLoadingUEDT = dataTypeService.loadODataType("ScreenDataLoadingUE", loggedUser);
                ODataMessage screenDataLoadingUEDM = new ODataMessage();
                screenDataLoadingUEDM.setODataType(screenDataLoadingUEDT);
                List<Object> odmData = new ArrayList<Object>();
                odmData.add(loadingConds);
                odmData.add(headerObject);
                odmData.add(dataMessage);
                odmData.add(getCurrentScreen());
                screenDataLoadingUEDM.setData(odmData);

                // Call the function
                OFunctionResult functionRetFR = functionServiceRemote.runFunction(
                        getCurrentScreen().getDataLoadingUserExit(), screenDataLoadingUEDM, null, loggedUser);

                if (functionRetFR.getErrors().isEmpty()) {
                    loadingConds.clear();
                    loadingConds.addAll((List) functionRetFR.getReturnedDataMessage().getData().get(0));
                }
            }
                
                List<BaseEntity> entitys = oem.loadEntityList(
                        chartScreen.getDim3OEntity().getEntityClassName(),
                        loadingConds,conditions, null, loggedUser);
                if (null != dateFunction) {
                    SimpleDateFormat simpleDateformat = new SimpleDateFormat(dateFormate);
                    for (BaseEntity baseEntity : entitys) {
                        Date date = (Date) BaseEntity.getValueFromEntity(baseEntity,
                                chartScreen.getXaxisLabelFieldExp());
                        if(null == date)continue;
                        if (Integer.parseInt(simpleDateformat.format(date)) == xaxixIdx + 1) {
                            if(null == chartScreen.getChartScreenDim3Fields()||
                                    chartScreen.getChartScreenDim3Fields().isEmpty()){
                                UDC udc = (UDC)BaseEntity.getValueFromEntity(baseEntity, chartScreen.getDim3LabelFieldExp());
                                udc = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udc.getDbid(), null, null, loggedUser);
                                if(udc.getValue().equalsIgnoreCase(chartSerieses.get(dim3Idx).getLabel()))
                                    countForNonDim3Fields++;
                            }
                            else{
                                String pointStr =(null != BaseEntity.getValueFromEntity(
                                    baseEntity, chartScreen.getChartScreenDim3Fields().get(dim3Idx).
                                    getDim3FieldExpression()).toString())?BaseEntity.getValueFromEntity(
                                    baseEntity, chartScreen.getChartScreenDim3Fields().get(dim3Idx).
                                    getDim3FieldExpression()).toString():"0";
                                pointStr = ("".equals(pointStr))?"0":pointStr;
                                pointData.add(Double.parseDouble( pointStr));
                            }

                        }
                    }
                }else{
                    for (BaseEntity baseEntity : entitys) {
                        String xAxisLBL = (String) BaseEntity.getValueFromEntity(
                                    baseEntity, xAxis);
                        if(!xAxisLBL.equals(xaxixLabels.get(xaxixIdx)))continue;
                        String pointStr = (null == BaseEntity.getValueFromEntity(
                                    baseEntity, chartScreen.getChartScreenDim3Fields().get(dim3Idx).
                                    getDim3FieldExpression()))?"0":
                                BaseEntity.getValueFromEntity(
                                    baseEntity, chartScreen.getChartScreenDim3Fields().get(dim3Idx).
                                    getDim3FieldExpression()).toString();
                        double pointdbl = Double.parseDouble(pointStr);
                         pointData.add(pointdbl);
                    }
                }

                if (chartScreen.getAggregateFunction().getValue().equalsIgnoreCase("SUM")) {
                    for (Double pointD : pointData) {
                        point += pointD;
                    }
                } else if (chartScreen.getAggregateFunction().getValue().equalsIgnoreCase("COUNT")) {
                    point = pointData.size();
                } else {
                    for (Double pointD : pointData) {
                        point += pointD;
                    }
                    point = point / pointData.size();
                }
                point = (countForNonDim3Fields == 0)?point:countForNonDim3Fields;
                chartSerieses.get(dim3Idx).set(xaxixLabels.get(xaxixIdx), point);
            }
        }
        logger.trace("Returning");
        return chartSerieses;
    }

    @Override
    protected HtmlPanelGrid buildInnerScreen() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void addClaimButtonToPanelGroup(String buttonID, String buttonLabel, String actionMethodName, HtmlPanelGrid panelLayout, Class[] params, String portletInsId, String beanName, String screenLanguage, boolean invalidInputMode, OScreen currentScreen) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
