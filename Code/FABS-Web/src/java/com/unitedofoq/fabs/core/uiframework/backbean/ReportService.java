/**
 * Following is supplementary information that you will find great difficulty
 * in figuring out on your own...
 * ------------------------------------
 * If you wish to assign a background color to a JR control (text field,
 * static text, etc.), you will need to set its style to OPAQUE.
 *
 * To do so for JR-class controls, you will use something like this:
 *      JRDesignStyle cellStyle = new JRDesignStyle();
 *      cellStyle.setMode(ModeEnum.OPAQUE);
 *
 * However, if you intend to do so for DJ-class controls, you will instead use:
 *      Style myStyle = new Style();
 *      columnHeaderStyle.setTransparency(Transparency.OPAQUE);
 * ------------------------------------
 */

package com.unitedofoq.fabs.core.uiframework.backbean;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.filter.FilterDDRequiredException;
import com.unitedofoq.fabs.core.filter.OFilterServiceLocal;
import com.unitedofoq.fabs.core.filter.UserSessionInfo;
import com.unitedofoq.fabs.core.report.ComparisonCrossTabReport;
import com.unitedofoq.fabs.core.report.CrossTabField;
import com.unitedofoq.fabs.core.report.CrosstabFieldColumn;
import com.unitedofoq.fabs.core.report.CrossTabReport;
import com.unitedofoq.fabs.core.report.MasterDetailReport;
import com.unitedofoq.fabs.core.report.OReport;
import com.unitedofoq.fabs.core.report.OReportField;
import com.unitedofoq.fabs.core.report.OReportGroup;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignParameter;
import net.sf.jasperreports.engine.design.JRDesignStaticText;
import net.sf.jasperreports.engine.design.JRDesignStyle;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.j2ee.servlets.BaseHttpServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReportService {

    // <editor-fold defaultstate="collapsed" desc="report">
    private OReport report;
    public OReport getReport() {
        return report;
    }
    // </editor-fold>

    private OEntityManagerRemote oem;
    private OUser loggedUser;
    private OFilterServiceLocal filterServiceLocal;
    final static Logger logger = LoggerFactory.getLogger(Lookup.class);
    private CrossTabReport crosstabReport;

    /**
     * CONSTRUCTOR
     *
     * @param report
     *      The report that will utilize the functions in ReportService.java
     * @param oem
     *      For logging purposes in case of errors
     * @param oFilterServiceLocal
     *      The OFilterServiceLocal instance to be used in filtering the
     *      result set used for filling the report
     * @param loggedUser
     *      The current user
     */
    public ReportService(OReport report,
                         OEntityManagerRemote oem,
                         OFilterServiceLocal oFilterServiceLocal,
                         OUser loggedUser) {
        this.report = report;
        this.oem = oem;
        this.loggedUser = loggedUser ;
        this.filterServiceLocal = oFilterServiceLocal;

        if (report instanceof CrossTabReport)
            report = initCrosstabReport();
    }

    /**
     * Loads the report specifications from the DB to be used in creating
     * the report
     *
     * @param reportName
     *      The name of the report to be loaded from the DB.
     *
     * @return
     *      The OReport object containing the report specifications.
     */
    public OReport loadReportSpecs(String reportName) {
        logger.debug("Entering");
        report = null;
        List<String> conditions = new ArrayList<String>();

        // <editor-fold defaultstate="collapsed" desc="LOAD">
        try {
            conditions.add("name='" + reportName + "'");
            report = (OReport) oem.loadEntity("OReport",
                                              conditions,
                                              new ArrayList<String>(),
                                              oem.getSystemUser(loggedUser));
        }
        catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="LOG">
         logger.error("Exception thrown: ",ex);
            // </editor-fold>
        }
        // </editor-fold>
        logger.debug("Returning");
        return report;
    }

    /**
     * Loads the result set to be used for filling the report
     *
     * @return
     *      The result set for filling the report
     *      (in the form of List of "BaseEntity")
     */
    public List<BaseEntity> loadReportResultSet(UserSessionInfo userSessionInfo) {
        logger.debug("Entering");
        List<BaseEntity> resultSet = new ArrayList<BaseEntity>();
        List<String> conditions = new ArrayList<String>();
        List<String> sortExpressions = new ArrayList<String>();
        // <editor-fold defaultstate="collapsed" desc="SORTS">
        List<OReportGroup> groups = report.getGroups();
        if (groups!=null && groups.size()!=0) {

            // <editor-fold defaultstate="collapsed" desc="COMPARATOR">
            //  used for sorting report fields using their sort index
            Comparator groupsComparator = new Comparator() {
                public int compare(Object o1, Object o2) {
                    OReportGroup g1 = (OReportGroup) o1;
                    OReportGroup g2 = (OReportGroup) o2;
                    return g1.getGroupField().getSortIndex().compareTo(
                           g2.getGroupField().getSortIndex());
                }
            };
            // </editor-fold>

            Collections.sort(groups, groupsComparator);
            for (OReportGroup group : groups){
                sortExpressions.add(group.getGroupField().getFieldExpression());
                 if(group.getGroupField().getFieldExpression().contains(".")){
                   String[] joinEntity= (group.getGroupField().getFieldExpression()).split("\\.");
                   // conditions.add(""+joinEntity[0]+" IS NOT NULL");
                }
                // sortExpressions.add(group.getGroupField().getSortIndex()+"");
            }
        }
      
        if (report.getSort() != null && !report.getSort().equals("")) {
            String resultSetSort = report.getSort();
            while (resultSetSort.indexOf(";") != -1) {
                
                String currentSort = resultSetSort.substring(0, resultSetSort.indexOf(";"));
                resultSetSort = resultSetSort.substring(resultSetSort.indexOf(";") + 1);
                sortExpressions.add(currentSort);
            }
            sortExpressions.add(resultSetSort);
             
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTERS">
        if (report.getReportFilter() != null
         && report.getReportFilter().isAlwaysShow()
         && !report.getReportFilter().isInActive())
            try {
            conditions = filterServiceLocal.getFilterConditions(
                    report.getReportFilter(), userSessionInfo, loggedUser);
        } catch (FilterDDRequiredException ex) {
                logger.error("Exception thrown: ",ex);
        }

        if (report.getFilter() != null && !report.getFilter().equals("")) {
            String resultSetFilter = report.getFilter();
            while (resultSetFilter.indexOf(";") != -1) {
                String currentFilter = resultSetFilter.substring(0, resultSetFilter.indexOf(";"));
                resultSetFilter = resultSetFilter.substring(resultSetFilter.indexOf(";") + 1);
                conditions.add(currentFilter);
            }
            conditions.add(resultSetFilter);
        }

        if (report instanceof CrossTabReport) {
            for (int i=0; i<conditions.size(); i++) {
                String condition = conditions.get(i);
                if (condition.indexOf("periodicSalaryElements.payCalculatedPeriod") != -1) {
                    conditions.remove(i);
                    break;
                }
            }
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="LOAD">
        String entityName = report.getOactOnEntity().getEntityClassName();
        if (report instanceof CrossTabReport) {
            String groupByEntityAttribute = crosstabReport.getGroupByEntityAttribute();    //  e.g. payroll.costCenter
            if (groupByEntityAttribute != null && !groupByEntityAttribute.equals(""))
                entityName = crosstabReport.getDetailEntity().getEntityClassName();
        }
        try {
            resultSet = oem.loadEntityList(entityName,
                                           conditions,
                                           new ArrayList<String>(),
                                           sortExpressions,
                                           oem.getSystemUser(loggedUser));
        }
        catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="LOG">
            logger.error("Exception thrown: ",ex);
            // </editor-fold>
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="COMPARISON REPORT">
        //
        //  The following code is for:
        //
        //    - removing 'RelEntities' (from inside the 'Entities')
        //      that are NOT related to the 'ComparisonValues'
        //      specified in the comparison report
        //
        //    - removing 'Entities' from the result set if, after the
        //      previous step, there are no 'RelEntities' left in it
        //
        if (report instanceof ComparisonCrossTabReport) {
            ComparisonCrossTabReport comparisonReport = (ComparisonCrossTabReport) crosstabReport;
            String relEntityAttribute = comparisonReport.getRelEntityAttribute();
            String associateFieldAttr = comparisonReport.getAssociateFieldAttribute();
            associateFieldAttr = associateFieldAttr.substring(0, associateFieldAttr.indexOf("."));

            for (int i=0; i<resultSet.size(); i++) {
                BaseEntity entity = resultSet.get(i);
                try {
                    List<BaseEntity> relEntities = (List<BaseEntity>) entity.invokeGetter(relEntityAttribute);
                    if (relEntities == null || relEntities.size() == 0) {
                        resultSet.remove(i);
                        i--;
                    }
                    else {
                        boolean toBeIncluded = false;
                        for (int j=0; j<resultSet.size(); j++) {
                            BaseEntity relEntity = relEntities.get(j);
                            BaseEntity ascFld = (BaseEntity) relEntity.invokeGetter(associateFieldAttr);
                            if (ascFld != null) {
                                if (ascFld.equals(comparisonReport.getComparisonValue1())
                                 || ascFld.equals(comparisonReport.getComparisonValue2())) {
                                    toBeIncluded = true;
                                }
                                else {
                                    relEntities.remove(j);
                                    j--;
                                }
                            }
                        }
                        if (toBeIncluded) {
                            entity.invokeSetter(relEntityAttribute, relEntities);
                            resultSet.set(i, entity);
                        }
                        else {
                            resultSet.remove(i);
                            i--;
                        }
                    }
                }
                catch (Exception ex) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                   logger.error("Exception thrown: ",ex);
                    // </editor-fold>

                    break;
                }
            }
        }
        // </editor-fold>
        logger.debug("Returning");
        return resultSet;
    }

    /**
     * Loads the header object to be used in obtaining the values of the
     * header fields.
     *
     * @param headerObjectClassName
     *      The name of the entity's class
     * @param headerObjectDBID
     *      The DBID of the entity instance to be loaded
     */
    public BaseEntity loadHeaderObject(String headerObjectClassName,
                                       String headerObjectDBID) {
        BaseEntity headerObject = null;
        List<String> conditions = new ArrayList<String>();
        conditions.add("dbid=" + headerObjectDBID);

        // <editor-fold defaultstate="collapsed" desc="LOAD">
        try {
            headerObject = oem.loadEntity(
                                headerObjectClassName,
                                conditions,
                                new ArrayList<String>(),
                                oem.getSystemUser(loggedUser));
        }
        catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="LOG">
            logger.error("Exception thrown: ",ex);
            // </editor-fold>
        }
        // </editor-fold>

        return headerObject;
    }

    /**
     * Loads the 'detail' of the Master-Detail report
     *
     * @param headerObject
     *      The header object of the Master-Detail report
     * @param detailEntityAttribute
     *      The field expression representing the detail of the
     *      Master-Detail report (relative to the header object)
     *
     * @return
     *      The 'detail' of the Master-Detail report
     *      (in the form of List of "BaseEntity")
     */
    public List<BaseEntity> retrieveDetail(BaseEntity headerObject, UserSessionInfo userSessionInfo) {
        logger.debug("Entering");
        MasterDetailReport masterDetailReport = (MasterDetailReport) report;
        String detailEntityAttribute = masterDetailReport.getDetailEntityAttribute();

        List<BaseEntity> detail = new ArrayList<BaseEntity>();

        // <editor-fold defaultstate="collapsed" desc="RETRIEVE">
        detail.add(headerObject);
        while (true) {
            boolean lastLoop = false;

            String currentItem = "";
            if (detailEntityAttribute.indexOf(".") != -1) {
                //  retrieve current item in 'detailEntityAttribute'
                int dotIndex = detailEntityAttribute.indexOf(".");
                currentItem = detailEntityAttribute.substring(0, dotIndex);
                detailEntityAttribute = detailEntityAttribute.substring(dotIndex+1);
            }
            else {
                //  'detailEntityAttribute' contains the last item
                currentItem = detailEntityAttribute;

                //  mark this loop as being the last
                lastLoop = true;
            }

            List<BaseEntity> auxDetail = new ArrayList<BaseEntity>();
            for (BaseEntity entity : detail) {
                try {
                    if (entity.invokeGetter(currentItem) instanceof List) {
                        List<BaseEntity> list = (List) entity.invokeGetter(currentItem);
                        for (BaseEntity listEntity : list) {
                            if (!auxDetail.contains(listEntity))
                                auxDetail.add(listEntity);
                        }
                    }
                    else
                        auxDetail.add((BaseEntity)entity.invokeGetter(currentItem));
                }
                catch (Exception ex) {
                    // <editor-fold defaultstate="collapsed" desc="LOG">
                    logger.error("Exception thrown: ",ex);
                    // </editor-fold>
                }
            }

            //  update 'detail'
            detail = auxDetail;

            if (lastLoop)
                break;
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTER">
        List<String> conditions = new ArrayList<String>();
        if (masterDetailReport.getReportFilter() != null 
         && masterDetailReport.getReportFilter().isAlwaysShow()
         && !masterDetailReport.getReportFilter().isInActive())
            try {
            conditions = filterServiceLocal.getFilterConditions(
                    masterDetailReport.getReportFilter(), userSessionInfo, 
                    loggedUser);//report.getReportFilter().getFilterString();
        } catch (FilterDDRequiredException ex) {
            
            logger.error("Exception thrown: ",ex);
        }//report.getReportFilter().getFilterString();

        if (masterDetailReport.getFilter() != null && !masterDetailReport.getFilter().equals("")) {
            String resultSetFilter = masterDetailReport.getFilter();
            while (resultSetFilter.indexOf(";") != -1) {
                String currentFilter = resultSetFilter.substring(0, resultSetFilter.indexOf(";"));
                resultSetFilter = resultSetFilter.substring(resultSetFilter.indexOf(";") + 1);
                conditions.add(currentFilter);
            }
            conditions.add(resultSetFilter);
        }

        if (detail.size() != 0 && conditions.size() != 0) {
            List<BaseEntity> auxDetail = new ArrayList<BaseEntity>();
            for (BaseEntity baseEntity : detail) {
                for (String condition : conditions) {
                    if(condition.indexOf("dbid") != -1){
                        String leftHandSide = condition.substring(0, condition.indexOf("=")).trim();
                        String dbidValue = condition.substring(condition.indexOf("=") + 1).trim();
                        Long value = Long.parseLong(dbidValue);
                        try {
                            if(value == baseEntity.invokeGetter(leftHandSide));
                                auxDetail.add(baseEntity);
                        }
                        catch (Exception ex){
                            // <editor-fold defaultstate="collapsed" desc="LOG">
                            logger.error("Exception thrown: ",ex);
                            // </editor-fold>
                        }
                    }
                }
            }
            detail = auxDetail;
        }
        // </editor-fold>
        logger.debug("Returning");
        return detail;
    }

    /**
     * Initialize the crosstab report
     * (i.e. populate the @Transient fields within the crosstab report)
     *
     * @return
     *      The initialized crosstab report
     */
    public CrossTabReport initCrosstabReport() {
        logger.debug("Entering");
        crosstabReport = (CrossTabReport) report;
        String associateFieldAttr = crosstabReport.getAssociateFieldAttribute();
        String relEntityAttribute = crosstabReport.getRelEntityAttribute();

        try {
            // <editor-fold defaultstate="collapsed" desc="Get Class of Act-On Entity">
            //  get class of "Act-On Entity"
            Class actOnEntityCls = Class.forName(crosstabReport.getOactOnEntity().getEntityClassPath());
            if (crosstabReport.getDetailEntity() != null)
                actOnEntityCls = Class.forName(crosstabReport.getDetailEntity().getEntityClassPath());
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Get Class of Relation Entity">
            //  get field representing "Relation Entity" <LIST>
            //  (i.e. get the field representing the last item in the
            //   field expression)
            Field relEntityFld = null;
            List<ExpressionFieldInfo> expressionFieldInfos = BaseEntity.parseFieldExpression(actOnEntityCls, relEntityAttribute);
            ExpressionFieldInfo expressionFieldInfo = expressionFieldInfos.get(expressionFieldInfos.size() - 1);
            relEntityFld = expressionFieldInfo.field;
//            Field relEntityFld = BaseEntity.getClassField(actOnEntityCls, relEntityAttribute);

            //  get the generic type of the list
            //  (i.e. get the type of list's elements)
            ParameterizedType pt = (ParameterizedType) relEntityFld.getGenericType();
            //  e.g.
            //  pt.getActualTypeArguments()[0].toString() = "class com.unitedofoq.otms.core.udc.UDC"
            //  pt.getActualTypeArguments()[0].toString().substring(6) = "com.unitedofoq.otms.core.udc.UDC"
            String relEntityClsName = pt.getActualTypeArguments()[0].toString().substring(6);
            Class relEntityCls = Class.forName(relEntityClsName);
            crosstabReport.setRelEntity(relEntityCls);

//            Class relEntityCls = relEntityFld.getGenericType().getClass();
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Get Class of Associate Field">
            associateFieldAttr = associateFieldAttr.substring(0, associateFieldAttr.indexOf("."));
            Field associateFld = BaseEntity.getClassField(relEntityCls, associateFieldAttr);
            Class associateFldCls = associateFld.getType();
            crosstabReport.setAssociateField(associateFldCls);
            // </editor-fold>
        }
        catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="LOG">
            logger.error("Exception thrown: ",ex);
            // </editor-fold>
        }
        logger.debug("Returning");
        return crosstabReport;
    }

    /**
     * Loads the result set for filling the cross tab section of the report
     *
     * @return
     *      The result set containing the columns data in the cross tab
     *      section of the report
     */
    public List<BaseEntity> loadAssociateList() {
        logger.debug("Entering");
        List<BaseEntity> resultSet = new ArrayList<BaseEntity>();

        if (crosstabReport.getAssociateField() != null) {
            List<String> conditions = new ArrayList<String>();
            List<String> sortExpressions = new ArrayList<String>();
            Class associateFldCls = crosstabReport.getAssociateField();

            // <editor-fold defaultstate="collapsed" desc="LOAD">
            try {
                // <editor-fold defaultstate="collapsed" desc="FILTERS">
                //  if it is comparison report...
                if (crosstabReport instanceof ComparisonCrossTabReport) {
                    ComparisonCrossTabReport comparisonCrossTabReport
                            = (ComparisonCrossTabReport) crosstabReport;

                    conditions.add("dbid IN ("
                                   + comparisonCrossTabReport.getComparisonValue1().getDbid()
                                   + ", "
                                   + comparisonCrossTabReport.getComparisonValue2().getDbid()
                                   + ")");
                }

                //  if it is a crosstab report...
                //IMPReports
//                else
//                    conditions.add("dbid="
//                                   + crosstabReport.getCalculatedPeriod().getDbid());
                // </editor-fold>

                resultSet = oem.loadEntityList(associateFldCls.getSimpleName(),
                                               conditions,
                                               new ArrayList(),
                                               sortExpressions,
                                               oem.getSystemUser(loggedUser));
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="LOG">
                logger.error("Exception thrown: ",ex);
                // </editor-fold>
            }
            // </editor-fold>

        }
        logger.debug("Returning");
        return resultSet;
    }

    /**
     * Processes the result set in case the report is a comparison report
     *
     * @param rs
     *      The result set to be processed for the report
     *
     * @return
     *      The processed result set
     */
    public List<BaseEntity> preRunProcessResultSet(List<BaseEntity> rs) {
        if (rs.size() == 0)
            return rs;

        ComparisonCrossTabReport comparisonReport
                = (ComparisonCrossTabReport) crosstabReport;

        BaseEntity comparisonValue1 = comparisonReport.getComparisonValue1();
        String relEntityAttribute = comparisonReport.getRelEntityAttribute();
        String associateFieldAttr = comparisonReport.getAssociateFieldAttribute();
        associateFieldAttr = associateFieldAttr.substring(0, associateFieldAttr.indexOf("."));
        String alteredField = comparisonReport.getFieldExpression();

        for (BaseEntity entity : rs) {
            try {
                List<BaseEntity> relEntities = (List<BaseEntity>) entity.invokeGetter(relEntityAttribute);
                for (BaseEntity relEntityInstance : relEntities) {
                    BaseEntity compValue = (BaseEntity) relEntityInstance.invokeGetter(associateFieldAttr);
                    if (compValue.equals(comparisonValue1)) {
                        try {
                            BigDecimal BigDecimalValue = (BigDecimal) relEntityInstance.invokeGetter(alteredField);
                            double doubleValue = (double) BigDecimalValue.doubleValue();
                            BigDecimalValue = BigDecimal.valueOf(-doubleValue);
                            relEntityInstance.invokeSetter(alteredField, BigDecimalValue);
                        }
                        catch (Exception ex) {
                            // <editor-fold defaultstate="collapsed" desc="LOG">
                            logger.error("Exception thrown: ",ex);
                            // </editor-fold>
                        }
                    }
                }
            }
            catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="LOG">
                logger.error("Exception thrown: ",ex);
                // </editor-fold>
            }
        }
        logger.debug("Returning ");
        return rs;
    }

    /**
     * Loads the result set for 'creating' the cross tab section of the report
     * 
     * @param entityName
     *      The simple class name to be used to load the columns data in 
     *      the cross tab section of the report
     * 
     * @return
     *      The result set containing the columns data in the cross tab 
     *      section of the report
     */
    public List<BaseEntity> loadCrossTabResultSetForFieldsCreation(CrossTabField crossTabField) {
        //  'finalCrossEntities' is the list of filtered cross entities
        logger.debug("Entering");
        List<BaseEntity> finalCrossEntities = new ArrayList<BaseEntity>();

        String crossEntityName = crossTabField.getCrossEntity().getEntityClassName();

        // <editor-fold defaultstate="collapsed" desc="LOAD">
        //  'crossEntities' is the list of cross entities loaded from
        //  the 'crossEntity' records.
        //  This is to make sure that cross entities that are not
        //  referenced in the 'crossEntity' records are not considered.
        List<BaseEntity> crossEntities = new ArrayList<BaseEntity>();
        String query = "SELECT entity FROM " + crossEntityName + " entity";
        try {
            crossEntities = oem.executeEntityListQuery(query, loggedUser);
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="LOG">
            logger.trace("Query: {}",query);
            logger.error("Exception thrown: ",ex);
            // </editor-fold>
        }
        // </editor-fold>

        List<CrosstabFieldColumn> crosstabFieldColumns = crossTabField.getCrosstabFieldColumns();
        if (crosstabFieldColumns != null && crosstabFieldColumns.size() != 0) {

            // <editor-fold defaultstate="collapsed" desc="FILTER">
            for (CrosstabFieldColumn crosstabFieldColumn : crosstabFieldColumns) {
                for (BaseEntity crossEntity : crossEntities) {
                    if (crossEntity.getDbid() == crosstabFieldColumn.getCrossEntityDBID()) {
                        if (!finalCrossEntities.contains(crossEntity))
                            finalCrossEntities.add(crossEntity);
                        break;
                    }
                }
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="SORT">
            HashMap crossEntitiesSort = new HashMap();
            for (CrosstabFieldColumn crosstabFieldColumn : crosstabFieldColumns) {
                crossEntitiesSort.put(crosstabFieldColumn.getCrossEntityDBID(),
                                      crosstabFieldColumn.getSortIndex());
            }
            final HashMap CrossEntitiesSort = crossEntitiesSort;

            Comparator crossEntitiesComparator = new Comparator() {
                public int compare(Object o1, Object o2) {
                    BaseEntity b1 = (BaseEntity) o1;
                    BaseEntity b2 = (BaseEntity) o2;
                    Integer index1 = (Integer) CrossEntitiesSort.get(b1.getDbid());
                    Integer index2 = (Integer) CrossEntitiesSort.get(b2.getDbid());
                    return index1.compareTo(index2);
                }
            };

            Collections.sort(finalCrossEntities, crossEntitiesComparator);
            // </editor-fold>
            logger.debug("Returning");
            return finalCrossEntities;
        }

        else
        {
            logger.debug("Returning");
            return crossEntities;
        }
    }

    /**
     * Loads the result set for filling the cross tab section of the report
     *
     * @return
     *      The result set containing the columns data in the cross tab
     *      section of the report
     */
    public List<BaseEntity> loadCrossTabResultSetForFilling() {
        logger.debug("Entering");
        Class relEntity = crosstabReport.getRelEntity();

        List<BaseEntity> resultSet = new ArrayList<BaseEntity>();
        List<String> conditions = new ArrayList<String>();
        List<String> sortExpressions = new ArrayList<String>();

        // <editor-fold defaultstate="collapsed" desc="SORTS">
        if (crosstabReport instanceof ComparisonCrossTabReport) {
            ComparisonCrossTabReport comparisonReport
                    = (ComparisonCrossTabReport) crosstabReport;

            String associateFieldAttr = comparisonReport.getAssociateFieldAttribute();
            sortExpressions.add(associateFieldAttr);
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTERS">
        if (crosstabReport instanceof ComparisonCrossTabReport) {
            ComparisonCrossTabReport comparisonReport
                    = (ComparisonCrossTabReport) crosstabReport;

            String associateFieldAttr = comparisonReport.getAssociateFieldAttribute();
            associateFieldAttr = associateFieldAttr.substring(0, associateFieldAttr.indexOf("."));

            conditions.add(associateFieldAttr
                           + ".dbid IN ("
                           + comparisonReport.getComparisonValue1().getDbid()
                           + ", "
                           + comparisonReport.getComparisonValue2().getDbid()
                           + ")");
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="LOAD">
        try {
            resultSet = oem.loadEntityList(relEntity.getSimpleName(),
                                           conditions,
                                           new ArrayList(),
                                           sortExpressions,
                                           oem.getSystemUser(loggedUser));
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="LOG">
            logger.error("Exception thrown: ",ex);
            // </editor-fold>
        }
        // </editor-fold>
        logger.debug("Returning");
        return resultSet;
    }

    /**
     * Converts a primitive to its 'class' counterpart.
     *
     * @param clazz
     *      The primitive type that will be converted to its
     *      "CLASS" counterpart.
     *      <P>If the passed type is not a primitive, it will be returned
     *      as is.</P>
     * @return
     *      The "CLASS" counterpart of the passed type if it was a primitive,
     *      or the passed type if it was not a primitive to begin with.
     */
    public static Class convertToClassCounterpart(Class clazz) {
        if      (clazz == byte.class)       return Byte.class;
        else if (clazz == short.class)      return Short.class;
        else if (clazz == int.class)        return Integer.class;
        else if (clazz == long.class)       return Long.class;
        else if (clazz == float.class)      return Float.class;
        else if (clazz == double.class)     return Double.class;
        else if (clazz == boolean.class)    return Boolean.class;
        else if (clazz == char.class)       return Character.class;
        else if (clazz == BigDecimal.class) return Double.class;

        return clazz;
    }

    /**
     * Adds header fields to the passed 'base' report template, and returns
     * the path of the 'resulting' report template
     *
     * @param application
     *      Servlet Context (for specifying a 'relative' location to create
     *      the resulting template file).
     * @param baseTemplateFile
     *      The 'base' report to add the header fields to.
     * @param headerFieldsLabels
     *      The labels for the text fields to be added to the "Title" band in
     *      in 'baseTemplateFile'.
     * @param headerFieldsClasses
     *      The class types for the values of the header fields.
     *
     * @return
     *      The path of the 'resulting' template file.
     */
    public String createTemplateFile(ServletContext application,
                                     List<String> headerFieldsLabels,
                                     List<String> headerFieldsClasses) {
        logger.debug("Entering");

        // <editor-fold defaultstate="collapsed" desc="COMPARATORS">
        //  used for sorting report 'fields' using their sort index
        Comparator fieldsComparator = new Comparator() {
            public int compare(Object o1, Object o2) {
                OReportField rf1 = (OReportField) o1;
                OReportField rf2 = (OReportField) o2;
                return rf1.getSortIndex().compareTo(rf2.getSortIndex());
            }
        };

        //  used for sorting report 'groups' using their sort index
        Comparator groupsComparator = new Comparator() {
            public int compare(Object o1, Object o2) {
                OReportGroup g1 = (OReportGroup) o1;
                OReportGroup g2 = (OReportGroup) o2;
                return g1.getGroupField().getSortIndex().compareTo(
                        g2.getGroupField().getSortIndex());
            }
        };
        // </editor-fold>

        //  Determine which template to be used (based on page orientation)
        String baseTemplateFile = application.getRealPath("/portrait.jrxml");
        boolean landscape = report.isLandscape();
        if (landscape)
            baseTemplateFile = application.getRealPath("/landscape.jrxml");

        String templateFile = baseTemplateFile;

        try {
            //  Load Base Report Template File
            JasperDesign jasperDesign = JRXmlLoader.load(templateFile);

            //  Current Row
            int row = 0;

            //  Number of Header Field Columns
            //  (depends on page orientation: portrait/landscape)
            int noOfColumns = 2;
//            int noOfColumns = (landscape) ? 3 : 2;

            //  Styles to be used in the header fields of Master-Detail report
            JRDesignStyle headerFieldLabelStyle = (JRDesignStyle) jasperDesign.getStylesMap().get("headerFieldLabelStyle");
            JRDesignStyle headerFieldValueStyle = (JRDesignStyle) jasperDesign.getStylesMap().get("headerFieldValueStyle");

            for (int i=0; i<headerFieldsLabels.size(); i++) {
                //  Determine Current Row
                row = i / noOfColumns;

                //  Left, Middle or Right Column?
                boolean leftColumn = false,
                        middleColumn = false;
//                        rightColumn = false;

                if (i % noOfColumns == 0)
                    leftColumn = true;
                else if (i % noOfColumns == 1)
                    middleColumn = true;
//                else
//                    rightColumn = true;

                //  column offset (X-axis) of current column
                int offset = 0;

                     if (leftColumn)   offset = 0;
                else if (middleColumn) offset = ((landscape) ? 340 : 290);
//                else if (rightColumn)  offset = 550;

                //  Parameter
                JRDesignParameter parameter = new JRDesignParameter();
                parameter.setName("param" + i);
                parameter.setValueClassName(headerFieldsClasses.get(i));

                //  Label
                JRDesignStaticText staticText = new JRDesignStaticText();
                staticText.setX(((landscape) ? 100 : 10) + offset);
                staticText.setY(80 + (row*20));
                staticText.setWidth(((landscape) ? 150 : 125));
                staticText.setHeight(20);
                staticText.setStyle(headerFieldLabelStyle);
                staticText.setText(headerFieldsLabels.get(i) + ":");

                //  Text Field
                JRDesignTextField textField = new JRDesignTextField();
                textField.setX(((landscape) ? 250 : 135) + offset);
                textField.setY(80 + (row*20));
                textField.setWidth(150);
                textField.setHeight(20);
                textField.setStyle(headerFieldValueStyle);
                if(headerFieldsClasses.get(i).equals("java.util.Date"))
                    textField.setPattern("MMMMM dd, yyyy");
                else if(headerFieldsClasses.get(i).equals("java.sql.Timestamp"))
                    textField.setPattern("dd/MM/yyyy HH.mm.ss");


                JRDesignExpression expression = new JRDesignExpression();
                expression.setText("$P{param" + i + "}");
                expression.setValueClassName(headerFieldsClasses.get(i));
                textField.setExpression(expression);

                //  Add to Template
                jasperDesign.addParameter(parameter);
                jasperDesign.getPageHeader().getChildren().add(staticText);
                jasperDesign.getPageHeader().getChildren().add(textField);
            }

            //  Add Empty Label for Extra Vertical Spacing below Header Fields
            JRDesignStaticText staticText = new JRDesignStaticText();
            staticText.setX(120);
            staticText.setY(80 + ((row+1)*20));
            staticText.setWidth(110);
            staticText.setHeight(20);
            staticText.setText("");
            jasperDesign.getPageHeader().getChildren().add(staticText);
            ((JRDesignBand)jasperDesign.getPageHeader()).setHeight(80 + ((row+1)*20) + 20);

            //  Overall Header
            JRDesignStyle overallHeaderStyle = (JRDesignStyle) jasperDesign.getStylesMap().get("overallHeaderStyle");
            List<OReportField> reportFields = new ArrayList<OReportField>();
            reportFields.addAll(report.getFields());
            Collections.sort(reportFields, fieldsComparator);

            double actualToSpecifiedRatio = getActualToSpecifiedRatio(jasperDesign.getColumnWidth());

            JRDesignStaticText overallHeaderStaticText = null;
            int x = 0, width = 0, cN = 1;
            String previousOverallHeader = "";
            for (OReportField reportFld : reportFields) {
                //  if it is a crosstab field...
                if (reportFld instanceof CrossTabField)
                    //  get number of columns in the crosstab field
                    cN = crossTabFlds.get(reportFld.getFieldExpression());
                //  if it is a regular field...
                else
                    //  then it is just one column
                    cN = 1;

                //  if an overall header is specified for the report field...
                if(reportFld.getOverallHeader()!= null && !"".equals(reportFld.getOverallHeader())) {
                    //
                    if(!previousOverallHeader.equals(reportFld.getOverallHeader())) {
                        //  if there 'was' overall header label in progress...
                        if(overallHeaderStaticText != null) {
                            width = (int) (((double)width) * actualToSpecifiedRatio);
                            overallHeaderStaticText.setWidth(width);

                            width = 0;
                            jasperDesign.getPageHeader().getChildren().add(overallHeaderStaticText);
                            overallHeaderStaticText = null;
                        }

                        overallHeaderStaticText = new JRDesignStaticText();
                        overallHeaderStaticText.setY(100 + ((row + 1) * 20));
                        overallHeaderStaticText.setX(x);
                        overallHeaderStaticText.setHeight(20);
                        overallHeaderStaticText.setStyle(overallHeaderStyle);
                        previousOverallHeader = reportFld.getOverallHeader();
                        overallHeaderStaticText.setText(previousOverallHeader);
                        width += reportFld.getWidth() * cN;
                    }

                    else
                        width += reportFld.getWidth() * cN;
                }

                else{
                    //  if there 'is' overall header label in progress...
                    if(overallHeaderStaticText != null) {
                        width = (int) (((double)width) * actualToSpecifiedRatio);
                        overallHeaderStaticText.setWidth(width);

                        width = 0;
                        jasperDesign.getPageHeader().getChildren().add(overallHeaderStaticText);
                        overallHeaderStaticText = null;
                    }
                }

                x+= (int) (((double)(reportFld.getWidth() * cN)) * actualToSpecifiedRatio);
            }

            if (overallHeaderStaticText != null) {
                width = (int) (((double)width) * actualToSpecifiedRatio);
                overallHeaderStaticText.setWidth(width);
                jasperDesign.getPageHeader().getChildren().add(overallHeaderStaticText);
            }

            ((JRDesignBand)jasperDesign.getPageHeader()).setHeight(100 + ((row+1)*20) + 20);

            //  Create Template File
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            templateFile = "/report/report" + new Date().getTime() + ".jrxml";
            templateFile = application.getRealPath(templateFile);
            DynamicJasperHelper.generateJRXML(jasperReport, "UTF-8", templateFile);
        }
        catch (JRException ex) {
            // <editor-fold defaultstate="collapsed" desc="LOG">
            logger.error("Exception thrown: ",ex);
            // </editor-fold>

            //  Use 'baseTemplateFile' as is. (i.e. without the header fields)
            templateFile = baseTemplateFile;
        }
logger.debug("Returning");
        return templateFile;
    }

    /**
     * Exports the report in the requested format
     *
     * @param jasperPrint
     *      The JasperPrint object created during report creation.
     * @param dynamicReport
     *      The DynamicReport object created during report creation.
     * @param outputFormat
     *      The requested format for exporting the report.
     * @param request
     *      To be used for passing an attribute for the Flash Viewer mode.
     * @param response
     *      To be used for exporting the report to, except for the
     *      Flash Viewer mode.
     */
    public void exportReport (JasperPrint jasperPrint,
                              DynamicReport dynamicReport,
                              String outputFormat,
                              HttpServletRequest request,
                              HttpServletResponse response) {
        logger.debug("Entering");
        try {
            if (jasperPrint != null) {
                /*  SWF  */
                if (outputFormat.equals("swf")) {
                    request.getSession().setAttribute(BaseHttpServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jasperPrint);
                    request.getRequestDispatcher("/swf.html").include(request, response);
                }

                /*  XLS  */
                else if (outputFormat.equals("xls")) {
                    response.setContentType("application/vnd.ms-excel");
                    String disposition = "attachment; fileName=" + jasperPrint.getName().replace(' ', '_') + ".xls";
                    response.setHeader("Content-Disposition", disposition);
                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    JRXlsExporter exporter = new JRXlsExporter();
                    exporter.setParameter(
                            JRExporterParameter.JASPER_PRINT,
                            jasperPrint);
                    exporter.setParameter(
                            JRExporterParameter.OUTPUT_STREAM,
                            servletOutputStream);
                    exporter.exportReport();
                    servletOutputStream.close();
                }
                
                /*  CSV  */
                else if (outputFormat.equals("csv")) {
                    response.setContentType("text/csv");
                    String disposition = "attachment; fileName=" + jasperPrint.getName().replace(' ', '_') + ".csv";
                    response.setHeader("Content-Disposition", disposition);
                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    JRCsvExporter exporter = new JRCsvExporter();

                    exporter.setParameter(
                            JRCsvExporterParameter.JASPER_PRINT,
                            jasperPrint);
                    exporter.setParameter(
                            JRCsvExporterParameter.OUTPUT_STREAM,
                            servletOutputStream);
                    exporter.exportReport();
                    servletOutputStream.close();
                }

                /*  JRXML  */
                else if (outputFormat.equals("jrxml")) {
                    //response.setContentType("text/xml");
                    response.setContentType("application/download");
                    String disposition = "attachment; fileName='" + jasperPrint.getName() + ".jrxml'";
                    response.setHeader("Content-Disposition", disposition);
                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    DynamicJasperHelper.generateJRXML(dynamicReport, new ClassicLayoutManager(), new HashMap(), "UTF-8", servletOutputStream);
                    servletOutputStream.close();
                }

                /*  PDF  */
                else {
                    response.setContentType("application/pdf");
                    String disposition = "inline; fileName='" + jasperPrint.getName() + ".pdf'";
                    response.setHeader("Content-Disposition", disposition);
                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    
                    JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
                    servletOutputStream.close();
                }
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="LOG">
            logger.error("Exception thrown: ",ex);
             logger.trace("Report Name: {}", jasperPrint.getName());
            // </editor-fold>
        }
        logger.debug("Returning");
    }

        public void exportBirtReport (JasperPrint jasperPrint,
                              DynamicReport dynamicReport,
                              String outputFormat,
                              HttpServletRequest request,
                              HttpServletResponse response) {
        try {
            if (jasperPrint != null) {
                
                    response.setContentType("application/pdf");
                    String disposition = "inline; fileName='" + jasperPrint.getName() + ".pdf'";
                    response.setHeader("Content-Disposition", disposition);
                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    
                    JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
                    servletOutputStream.close();
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="LOG">
            logger.error("Exception thrown: ",ex);
            logger.trace("Report Name: {}", jasperPrint.getName());
            // </editor-fold>
        }
    }  
    /**
     * Gets the columns' widths as specified in the DB, and returns their total
     *
     * @return specifiedColumnsWidth
     *      The total of the columns' widths
     */
    public int getSpecifiedColumnsWidth (OReport report) {
        //  to hold the sum of the columns' widths as retrieved from the DB
        int specifiedColumnsWidth = 0;

        for (OReportField field : report.getFields()) {
            //  if it is a crosstab field...
            if (field instanceof CrossTabField) {
                //  get the number of columns in the crosstab field
                int noOfColumns = crossTabFlds.get(field.getFieldExpression());
                specifiedColumnsWidth += noOfColumns * field.getWidth();
            }
            //  if it is a regular field...
            else
                //  then it is just one column
                specifiedColumnsWidth += field.getWidth();
        }

        //  return the total columns' width
        return specifiedColumnsWidth;
    }

    /**
     * Computes the ratio between the actual columns width (from the JRXML
     * template) and the specified columns width (from the DB)
     *
     * @param actualColumnsWidth
     *      The actual columns width (or page width) as retrieved from the
     *      the JRXML template used in constructing the report
     *
     * @return actualToSpecifiedRatio
     *      The ratio between the actual columns width and the specified
     *      columns width as retrieved from the DB
     */
    public double getActualToSpecifiedRatio (int actualColumnsWidth) {
        double actualToSpecifiedRatio = 0.0;
        int specifiedColumnsWidth = getSpecifiedColumnsWidth(report);
        actualToSpecifiedRatio = ((double) actualColumnsWidth) / ((double)specifiedColumnsWidth);
        return actualToSpecifiedRatio;
    }

    private HashMap<String, Integer> crossTabFlds =  new HashMap<String, Integer>();
    public void addCrossTabField(String fldExp, int width){
        crossTabFlds.put(fldExp, width);
    }
}
