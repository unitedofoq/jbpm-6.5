/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.central.OTenant;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ComputedField;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.multitanantuser.UserTenantRoleTempEntity;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.uiframework.screen.TabularScreen;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.ejb.EJB;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.el.ValueExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.persistence.Transient;
import org.primefaces.component.ajaxstatus.AjaxStatus;
import org.primefaces.component.celleditor.CellEditor;
import org.primefaces.component.column.Column;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.graphicimage.GraphicImage;
import org.primefaces.component.panel.Panel;
import org.primefaces.component.roweditor.RowEditor;
import org.primefaces.component.tooltip.Tooltip;
import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author MEA
 */
@ManagedBean(name = "UserTenantRoleAdminBean")
@ViewScoped
public class UserTenantRoleAdminBean extends TabularBean {

    protected OFunctionResult oFR = new OFunctionResult();
    
    protected UIFrameworkServiceRemote uiFrameWorkFacade = OEJB.lookup(UIFrameworkServiceRemote.class);
    
    OCentralEntityManagerRemote oCentralEntityManager = OEJB.lookup(OCentralEntityManagerRemote.class);
    protected List listProvider = new ArrayList();
    DataTable dataTable1 = null;
    protected int pageSize = 7;
    protected Panel collapsiblePanel;

    @Override
    protected String getBeanName() {
        return "UserTenantRoleAdminBean";
    }

    /**
     *
     */
    @Override
    public void init() {
        super.init();
        try {
            prepareTenantAndLoginNameDropDownFilterString();
            listProvider = oCentralEntityManager.getUserTenantRolesData(loggedUser.getTenant().getPersistenceUnitName());
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
            messagesPanel.clearAndDisplayMessages(oFR);
        }
    }

    @Override
    public HtmlPanelGrid buildInnerScreen() {

        HtmlPanelGrid grid = new HtmlPanelGrid();
        grid.setColumns(1);
        grid.setDir(htmlDir);
        grid.setStyle("width:100%;border:hidden");

        if (messagesPanel == null) {
            messagesPanel = new UserMessagePanel(exFactory, elContext, getBeanName(), ddService, this, loggedUser);
        }

        Panel middlePanel = new Panel();
        if (screenLanguage.equalsIgnoreCase("Arabic")) {
            middlePanel.setStyle("position:relative;text-align:right;border:hidden;padding:0px;");
        } else {
            middlePanel.setStyle("position:relative;border:hidden;padding:0px;");
        }

        grid.getChildren().add(messagesPanel.createMessagePanel());
        List<ScreenField> screenFields = getCurrentScreen().getScreenFields();
        HtmlPanelGrid generalbuttonsGrid = new HtmlPanelGrid();
        generalbuttonsGrid.setId(getFacesContext().getViewRoot().createUniqueId() + "_GeneralButtonsGrid");
        generalbuttonsGrid.setStyleClass("buttonsGeneralPanel");

        generalbuttonsGrid.setId(getFacesContext().getViewRoot().createUniqueId() + "_bgImg");

        HtmlPanelGrid buttonsGrid = new HtmlPanelGrid();
        buttonsGrid.setId(getFacesContext().getViewRoot().createUniqueId() + "_ButtonsGrid");

        buttonsGrid.setColumns(9 + getCurrentScreen().getScreenActions().size());
        HtmlPanelGrid loadingIconPanel = new HtmlPanelGrid();
        loadingIconPanel.setId(getFacesContext().getViewRoot().createUniqueId() + "_load");
        loadingIconPanel.setStyleClass("loading-icon");
        AjaxStatus loadingState = new AjaxStatus();
        loadingState.setId(getFacesContext().getViewRoot().createUniqueId() + "_loadAjax");
        GraphicImage loadingImage = new GraphicImage();
        loadingImage.setId(getFacesContext().getViewRoot().createUniqueId() + "_loadImg");
        loadingImage.setStyleClass("loading-active");

        loadingState.getFacets().put("start", loadingImage);
        HtmlOutputText outputText = new HtmlOutputText();
        outputText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                + "_CompleteLBL");
        loadingState.getFacets().put("complete", outputText);

        loadingIconPanel.getChildren().add(loadingState);
        buttonsGrid.getChildren().add(loadingIconPanel);
        if (isScreenOpenedForLookup()) {
            buttonsGrid.getChildren().add(buildLookupBtnPanel("setLookupData"));
            createButtons(buttonsGrid);
        } else {
            if (!processMode) {
                createScreenActionsButtons(buttonsGrid);
            }
        }
        generalbuttonsGrid.getChildren().add(buttonsGrid);
        grid.getChildren().add(generalbuttonsGrid);

        // <editor-fold defaultstate="collapsed" desc="GetScreenFields & Build Table">
        dataTable1 = new DataTable();
        dataTable1.setResizableColumns(true);
        dataTable1.setStyleClass("dt-table");
        dataTable1.setFilterEvent("enter");
        dataTable1.setId(getCurrentScreen().getName() + "_dataTable1");
        dataTable1.setRows(pageSize);
        dataTable1.setEditable(true);
        dataTable1.setValueExpression("value",
                exFactory.createValueExpression(elContext, "#{" + getBeanName() + ".listProvider}", ArrayList.class));

        dataTable1.setVar("currentRow");
        dataTable1.setPaginatorAlwaysVisible(true);
        //For Paginator
        dataTable1.setPaginatorPosition("bottom");
        dataTable1.setPaginator(true);
        dataTable1.setPaginatorTemplate("{CurrentPageReport}  {FirstPageLink} {PreviousPageLink} {PageLinks} "
                + "{NextPageLink} {LastPageLink} {RowsPerPageDropdown}");

        FacesContext fc = FacesContext.getCurrentInstance();
        ExpressionFactory ef = fc.getApplication().getExpressionFactory();
        // <editor-fold defaultstate="collapsed" desc="Define Table and Its Properties">

        FABSAjaxBehavior rowEdit = new FABSAjaxBehavior();
        MethodExpression rowEditMe = ef.createMethodExpression(
                fc.getELContext(), "#{" + getBeanName() + ".editRow}",
                null, new Class[]{RowEditEvent.class});
        rowEdit.setListener(rowEditMe);
        rowEdit.setImmediate(false);

        rowEdit.setOnstart("validateMandaroryFields();");
        rowEdit.setOncomplete("KeepEditable()");

        FABSAjaxBehavior rowCancelEdit = new FABSAjaxBehavior();
        MethodExpression rowCancelEditMe = ef.createMethodExpression(
                fc.getELContext(), "#{" + getBeanName() + ".cancelEditRow}",
                null, new Class[]{RowEditEvent.class});
        rowCancelEdit.setListener(rowCancelEditMe);
        dataTable1.addClientBehavior("rowEditCancel", rowCancelEdit);
        dataTable1.addClientBehavior("rowEdit", rowEdit);
        //</editor-fold>

        createTableColumns(dataTable1, true, "currentRow", "updateRow");
        //</editor-fold>
        middlePanel.getChildren().add(dataTable1);
        grid.getChildren().add(middlePanel);
        logger.debug("Returning");
        return grid;
    }
    // </editor-fold>

    public List getListProvider() {
        return listProvider;
    }

    public void setListProvider(List listProvider) {
        this.listProvider = listProvider;
    }

    public void createTableColumns(DataTable table, boolean editable,
            String tableVariableExpression, String valueChangeMethod) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Iterate Screen Fields and Create Them">

        createActionColumns(table, true);

        for (int screenFieldIndex = 0; screenFieldIndex < screenFields.size();
                screenFieldIndex++) {
            ScreenField screenField = (ScreenField) screenFields.get(screenFieldIndex);
            Column column = new Column();
            column.setId("column" + screenFieldIndex);

            Field field = screenField.getExpInfos(getOactOnEntity().getEntityClassPath()).
                    get(screenField.
                            getExpInfos(getOactOnEntity().getEntityClassPath()).size() - 1).field;
            if (field.getAnnotation(Transient.class) == null
                    || (field.getAnnotation(ComputedField.class) != null
                    || field.getAnnotation(Translation.class) != null)) {
                ValueExpression valueExpression = exFactory.createValueExpression(
                        elContext, "#{" + tableVariableExpression + "." + screenField.getFieldExpression() + "}",
                        String.class);
                column.setValueExpression("sortBy", valueExpression);
                if (screenField.getDd().getControlType().getDbid() != DD.CT_CALENDAR
                        && screenField.getDd().getControlType().getDbid() != DD.CT_CHECKBOX) {
                    column.setValueExpression("filterBy", valueExpression);
                    column.setFilterMatchMode("contains");

                }
            }

            DD headerDD = screenField.getDd();
            try {
                headerDD = ddService.getDD(headerDD.getName(), loggedUser);
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }

            HtmlOutputLabel label = new HtmlOutputLabel();
            label.setId("columnHeader" + screenFieldIndex);

            /**
             * ************************************
             */
            if (fABSSetupLocal.loadKeySetup("EnableInlineHelp", loggedUser).isBvalue()) {
                HtmlGraphicImage helpImg = new HtmlGraphicImage();
                helpImg.setId("helpImg" + screenFieldIndex);
                helpImg.setUrl("/resources/help.png");
                helpImg.setStyleClass("hlp-img");

                Tooltip toolTip = new Tooltip();
                toolTip.setId("toolTip" + screenFieldIndex);
                toolTip.setFor(label.getId());
                String toolTipData = ddService.getDDLabel("InlineHelpTitle", loggedUser)
                        + " " + screenField.getDd().getInlineHelpTranslated();
                toolTip.setValue(toolTipData);
                toolTip.setShowEffect("slide");
                toolTip.setHideEffect("slide");
                toolTip.setStyleClass("tool-tip");

                label.setValueExpression("rendered", exFactory.createValueExpression(elContext, "#{" + getBeanName()
                        + ".toolTipRender}", boolean.class));
                label.getChildren().add(toolTip);

            }
            /**
             * ************************************
             */
            //Check Mandatory Field to add astrix
            if (screenField.isMandatory()) {
                HtmlGraphicImage mandatoryImg = new HtmlGraphicImage();
                mandatoryImg.setId("mandatory_Img" + screenFieldIndex);
                mandatoryImg.setUrl("/resources/mandatory.gif");
                mandatoryImg.setStyleClass("mandatory-img");
                label.getChildren().add(mandatoryImg);
            }
            if (screenField.getDdTitleOverrideTranslated() != null
                    && !"".equals(screenField.getDdTitleOverrideTranslated())) {
                label.setValue(screenField.getDdTitleOverrideTranslated());
                column.setHeader(label);
            } else if (null != screenField.getDdTitleOverride()
                    && !screenField.getDdTitleOverride().equals("")) {
                label.setValue(screenField.getDdTitleOverride());
                column.setHeader(label);
            } else {
                label.setValue(headerDD.getHeaderTranslated());
                column.setHeader(label);
            }

            UIComponent outputText = createScreenControl(CNTRL_OUTPUTMODE,
                    screenField, tableVariableExpression, screenFieldIndex, valueChangeMethod);

            if (editable) {
                CellEditor cellEditor = new CellEditor();
                cellEditor.setId("cell" + screenFieldIndex);
                UIComponent scrCtrlComp = createScreenControl(CNTRL_INPUTMODE, screenField, tableVariableExpression, screenFieldIndex,
                        valueChangeMethod);
                cellEditor.getFacets().put("input", scrCtrlComp);
                cellEditor.getFacets().put("output", outputText);
                column.getChildren().add(cellEditor);
            } else {
                column.getChildren().add(outputText);
            }
            column.setStyleClass("column-business");
            table.getChildren().add(column);
        }
        logger.debug("Returning");
        //</editor-fold>
    }

    private void createActionColumns(DataTable table, boolean editable) {
        logger.trace("Entering");
        //<editor-fold defaultstate="collapsed" desc="Row Editor Column">
        Column editorColumn = new Column();
        HtmlPanelGrid editRowPanel = new HtmlPanelGrid();
        int width = 20;
        editorColumn.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_column_editor");
        editRowPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_editRowPanel");
        editRowPanel.setStyleClass("editor-panel");
        editorColumn.setHeaderText("");

        editorColumn.setResizable(false);

        if (editable) {
            width += 20;
            RowEditor rowEditor = new RowEditor();
            rowEditor.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                    + "_rowEditor");
            editorColumn.getChildren().add(rowEditor);
        }
        if (((TabularScreen) getCurrentScreen()).isRemovable()) {
            CommandButton deleteButton = createDeleteButton(editRowPanel);
            HtmlOutputLabel outputLabel = new HtmlOutputLabel();
            outputLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                    + "cellEditorLBLOUT");
            if (editable) {
                CellEditor cellEditor = new CellEditor();
                cellEditor.setId("cellEditor_deleteColumn");
                HtmlOutputLabel inputLabel = new HtmlOutputLabel();
                inputLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                        + "cellEditorLBLIN");
                cellEditor.getFacets().put("output", deleteButton != null ? deleteButton : outputLabel);
                cellEditor.getFacets().put("input", inputLabel);
                editorColumn.getChildren().add(cellEditor);
            } else {
                editorColumn.getChildren().add(deleteButton != null ? deleteButton : outputLabel);
            }
        }
        if (showActions) {
            CommandButton duplicateButton = createDuplicateButton(editRowPanel);
            HtmlOutputLabel outputLabel = new HtmlOutputLabel();
            outputLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                    + "cellEditorLBLOUTAct");
            if (duplicateButton != null && ((TabularScreen) getCurrentScreen()).isRemovable()) {
                width += 20;
            }
            if (editable) {
                CellEditor cellEditor = new CellEditor();
                cellEditor.setId("cellEditor_duplicateColumn");
                HtmlOutputLabel inputLabel = new HtmlOutputLabel();
                inputLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                        + "cellEditorLBLINAct");
                cellEditor.getFacets().put("output", duplicateButton != null ? duplicateButton : outputLabel);
                cellEditor.getFacets().put("input", inputLabel);
                editorColumn.getChildren().add(cellEditor);
            } else {
                editorColumn.getChildren().add(duplicateButton != null ? duplicateButton : outputLabel);
            }
        }
        editorColumn.setWidth(width);
        editorColumn.setStyleClass("editor-column");
        editorColumn.setStyle("padding:0px!important;width:" + width + "px;");
        table.getChildren().add(editorColumn);
        logger.trace("Returning");
        //</editor-fold>
    }

    @Override
    public String addRowAction() {
        logger.debug("Entering");
        try {
            oem.getEM(loggedUser);
            messagesPanel.clearMessages();

            if (currentScreen.getScreenFilter() != null
                    && !currentScreen.getScreenFilter().isInActive()
                    && currentScreen.getScreenFilter().isShowInsideScreen()) {
                updateFilterFields(filterfields);
            }

            BaseEntity newEntity = createNewInstance();
            if (newEntity == null) {
                // Assuming createNewInstance displayed corresponding error message
                logger.debug("Returning with empty string");
                return "";
            }
            newEntity.generateInstID();

            listProvider.add(0, newEntity);

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex, true);
        } finally {
            oem.closeEM(loggedUser);
        }
        if (getScreenHeight() == null || getScreenHeight().equals("")) {
            RequestContext.getCurrentInstance().execute(
                    "top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');");
        } else {
            RequestContext.getCurrentInstance().execute(
                    "top.FABS.Portlet.updateIframeSizeWithHeight({iframeName:'" + getPortletInsId() + "Frame'"
                    + ",screenHeight:'" + getScreenHeight() + "'});");
        }

        for (String id : fieldIDs) {
            RequestContext.getCurrentInstance().update("tf:" + id);
        }
        logger.debug("Returning with empty string");
        return "";
    }

    @Override
    public void deleteRowObjectAction(ActionEvent event) {
        try {
            String value = event.getComponent().getParent().getParent().getClientId();
            value = value.substring(0, value.lastIndexOf(":"));
            value = value.substring(value.lastIndexOf(":") + 1);
            int currentInstanceID = (Integer.parseInt(value) % listProvider.size());
            if (currentInstanceID == -1) {
                return;
            }
            oCentralEntityManager.deleteUserTenantRoleEntity((UserTenantRoleTempEntity) listProvider.remove(currentInstanceID));
            RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
            RequestContext.getCurrentInstance().update(dataTable1.getClientId());
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
    }

    @Override
    public void updateRow(ValueChangeEvent vce) {
    }

    @Override
    public void editRow(RowEditEvent event) {
        logger.debug("Entering");
        OFunctionResult ofr = new OFunctionResult();
        BaseEntity entity = ((BaseEntity) event.getObject());
        try {
            if (entity.getDbid() == 0) {
                oCentralEntityManager.saveUserTenantRoleEntity((UserTenantRoleTempEntity) entity, loggedUser);
            } else {
                oCentralEntityManager.updateUserTenantRoleEntity((UserTenantRoleTempEntity) entity);
            }

            ofr.addSuccess(usrMsgService.getUserMessage("DataSaved",
                    Collections.singletonList(getOactOnEntity().getTitle()), systemUser));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().validationFailed();
            ofr.addError(usrMsgService.getUserMessage("ErrorInSaving",
                    Collections.singletonList(getOactOnEntity().getTitle()), systemUser));
        } finally {
            int currentRow = -1;
            if (ofr.getErrors() != null && !ofr.getErrors().isEmpty()) {
                currentRow = getObjectRecordID(entity.getDbid());
                RequestContext.getCurrentInstance().execute("setSelectedRow(" + currentRow + ")");
            } else {
                RequestContext.getCurrentInstance().execute("setSelectedRow(" + -10 + ")");
            }
            messagesPanel.clearAndDisplayMessages(ofr);
            RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
            logger.debug("Returning");
        }
    }

    private void prepareTenantAndLoginNameDropDownFilterString() {
        logger.trace("Entering");
        String tenantLookUpFilterStr = "";
        String loginNameLookupFilterStr = "";

        for (Object object : oCentralEntityManager.getTenantsList()) {
            tenantLookUpFilterStr += ((OTenant) object).getPersistenceUnitName() + "," + ((OTenant) object).getPersistenceUnitName() + ";";
        }
        tenantLookUpFilterStr = tenantLookUpFilterStr.substring(0, tenantLookUpFilterStr.length() - 1);

        for (String loginName : uiFrameWorkFacade.getLoginNamesForAllUsersInTenant(loggedUser)) {
            loginNameLookupFilterStr += loginName + "," + loginName + ";";
        }
        loginNameLookupFilterStr = loginNameLookupFilterStr.substring(0, loginNameLookupFilterStr.length() - 1);

        for (ScreenField field : getCurrentScreen().getScreenFields()) {
            if (field.getFieldExpression().contains("tenant")) {
                field.getDd().setLookupFilter(tenantLookUpFilterStr);
//                field.getDd().setLookupFilterTranslated(tenantLookUpFilterStr);
            } else if (field.getFieldExpression().contains("user")) {
                field.getDd().setLookupFilter(loginNameLookupFilterStr);
//                field.getDd().setLookupFilterTranslated(loginNameLookupFilterStr);
            }
        }
        logger.trace("Returning");
    }
}
