/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;
import javax.faces.event.MethodExpressionValueChangeListener;
import javax.faces.event.ValueChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mmohamed
 */
public class TableColumnHeader {

    final static Logger logger = LoggerFactory.getLogger(TableColumnHeader.class);

    public static enum ColumnSort {

        ASC, DESC, NONE
    }

    private HtmlCommandLink headerLink;
    private HtmlGraphicImage image;

    private ExpressionFactory exFactory;
    private ELContext elContext;
    private OEntityManagerRemote oem;
    @EJB
    private DDServiceRemote ddService;
    private ScreenField screenField;
    public static int chkRequiredHeight;
    public static int txtRequiredHeight;

    private SingleEntityBean backBean;

    public SingleEntityBean getBackBean() {
        return backBean;
    }

    public void setBackBean(SingleEntityBean backBean) {
        this.backBean = backBean;
    }

    public ScreenField getScreenField() {
        return screenField;
    }

    public void setScreenField(ScreenField screenField) {
        this.screenField = screenField;
    }

    public TableColumnHeader(ExpressionFactory exFactory, ELContext elContext,
            ScreenField screenField, String beanName, String id, OUser loggedUser,
            OEntityManagerRemote oem) {
        this.exFactory = exFactory;
        this.elContext = elContext;
        this.screenField = screenField;
        this.beanName = beanName;
        this.id = id;
        this.loggedUser = loggedUser;
        this.oem = oem;
    }

    private String beanName;
    private String id;
    private OUser loggedUser;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public OUser getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(OUser loggedUser) {
        this.loggedUser = loggedUser;
    }

    public HtmlCommandLink getHeaderLink() {
        return headerLink;
    }

    public void setHeaderLink(HtmlCommandLink headerLink) {
        this.headerLink = headerLink;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public ELContext getElContext() {
        return elContext;
    }

    public void setElContext(ELContext elContext) {
        this.elContext = elContext;
    }

    public ExpressionFactory getExFactory() {
        return exFactory;
    }

    public void setExFactory(ExpressionFactory exFactory) {
        this.exFactory = exFactory;
    }

    public HtmlGraphicImage getImage() {
        return image;
    }

    public void setImage(HtmlGraphicImage image) {
        this.image = image;
    }

    public HtmlPanelGrid createHeader(SingleEntityBean backBean,
            boolean isLookup, int maxCheckBoxSpc, int maxTextFldSpc) {
        logger.debug("Entering");
        setBackBean(backBean);
        String requiredHieght = "";
        try {
            String fieldExpression = screenField.getFieldExpression();
            DD fieldDD = screenField.getDd();
            fieldDD = ddService.getDD(fieldDD.getName(), loggedUser);
            String headerValue = (screenField.getDdTitleOverride() != null && !screenField.getDdTitleOverride().equals(""))
                    ? screenField.getDdTitleOverride() : fieldDD.getHeaderTranslated();

            String astrick = screenField.isMandatory() ? "*" : "";
            HtmlPanelGrid headerGrid = new HtmlPanelGrid();

            // dots in the id is not valid...
            id = id.replaceAll("\\.", "_");
            headerGrid.setId(id + "headerGrid");

            headerLink = new HtmlCommandLink();

            headerLink.setId(id + "_sort");
            headerLink.setValue(headerValue);
            Class[] parms2 = new Class[]{ActionEvent.class};
            MethodExpression methodBinding = FacesContext.getCurrentInstance().getApplication().getExpressionFactory().
                    createMethodExpression(elContext, "#{" + getBeanName() + ".sort}", null, parms2);

            //TODO: Enable Checkbox sort when fix the bug
            if (fieldDD.getControlType().getDbid() != DD.CT_CHECKBOX && !isLookup) {
                headerLink.addActionListener(new MethodExpressionActionListener(methodBinding));
            }
            headerLink.setStyleClass("tableheaderTxt");

            FramworkUtilities.setControlFldExp(headerLink, screenField.getFieldExpression());

            image = new HtmlGraphicImage();
            image.setId(id + "_sort_img");
            image.setStyle("border:0px;top:2px;position:relative");
            image.setUrl("/resources/trans.gif");

            String bindingExpression = "#{" + getBeanName() + ".columnsHeaders['" + screenField.getFieldExpression() + "'].image}";

            image.setValueExpression(
                    "binding",
                    exFactory.createValueExpression(
                            elContext,
                            bindingExpression,
                            String.class)
            );

            if (backBean.getDataSort() != null) {
                for (int i = 0; i < backBean.getDataSort().size(); i++) {
                    String sortExpTmp = backBean.getDataSort().get(i).trim().replaceAll(" ", "");
                    if (sortExpTmp.equalsIgnoreCase(screenField.getFieldExpression() + "asc")) {
                        image.setUrl("/resources/AscArrow.gif");
                        break;
                    } else if (sortExpTmp.equalsIgnoreCase(screenField.getFieldExpression() + "desc")) {
                        image.setUrl("/resources/DescArrow.gif");
                        break;
                    }
                }
            }

            HtmlInputHidden fieldExpHidden = new HtmlInputHidden();
            fieldExpHidden.setId(id + "_sort_hdn");
            fieldExpHidden.setValue(fieldExpression);

            HtmlOutputLabel astriskLabel = new HtmlOutputLabel();
            astriskLabel.setValue(astrick);
            astriskLabel.setStyleClass("headerAsterisk");

            HtmlPanelGrid headerLabelGrid = new HtmlPanelGrid();
            headerLabelGrid.setColumns(2);
            headerLabelGrid.setStyle("position:relative; width:100% ;text-align:center; vertical-align: top;height:10px;top:-9px;");
            requiredHieght = "-28px";
            headerLabelGrid.setCellpadding("0");
            headerLabelGrid.setCellspacing("0");
            headerLink.getChildren().add(astriskLabel);
            headerLink.getChildren().add(image);
            headerLink.getChildren().add(fieldExpHidden);
            headerLabelGrid.getChildren().add(headerLink);

            headerGrid.setStyle("width:" + fieldDD.getControlSize() + "px;");
            headerGrid.setCellpadding("0");
            headerGrid.setCellspacing("0");
            headerGrid.getChildren().add(createFilterControl(backBean, id, screenField, requiredHieght, isLookup));
            headerGrid.getChildren().add(headerLabelGrid);

            backBean.getColumnsHeaders().put(screenField.getFieldExpression(), this);

            headerGrid.setStyle("width:100%;");
            if (backBean.fABSSetupLocal.loadKeySetup("EnableInlineHelp", loggedUser).isBvalue()) {
                HtmlGraphicImage helpImg = new HtmlGraphicImage();
                helpImg.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
                helpImg.setUrl("/resources/help.png");

                HtmlPanelGroup group = new HtmlPanelGroup();
                group.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
                group.getChildren().add(helpImg);
            }
            logger.debug("Returning");
            return headerGrid;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning");
            return new HtmlPanelGrid();
        }
    }

    public UIComponent createFilterControl(final SingleEntityBean backBean,
            String id, ScreenField screenField, String requiredHeight,
            boolean isLookup) {
        logger.debug("Entering");
        int controlSize;
        long filterControlType;
        controlSize = screenField.getControlSize();
        if (controlSize == 0) {
            controlSize = screenField.getDd().getControlSize();
        }
        filterControlType = screenField.getDd().getControlType().getDbid();
        if (filterControlType == DD.CT_CALENDAR) {
            org.primefaces.component.calendar.Calendar inputDate = new org.primefaces.component.calendar.Calendar();
            inputDate.setId(id + "_filter");
            inputDate.setSize(controlSize);
            inputDate.setDisabled(true);//TODO: To be modified later. // Replaced with the following line
            logger.trace("Filter Control Added");
            inputDate.setStyle("position:relative; top:" + requiredHeight + "; width:99%;");
            backBean.getFilterControls().add(inputDate);
            return inputDate;
        } else if (filterControlType == DD.CT_CHECKBOX) {
            HtmlCommandButton checkButton = new HtmlCommandButton();
            checkButton.setImmediate(true);
            checkButton.setId(id + "_filter");
            checkButton.setImage("/resources/partialchecked.GIF");
            int filterControlsSize = backBean.getFilterControls() != null ? backBean.getFilterControls().size() : 0;
            checkButton.setStyleClass(String.valueOf(filterControlsSize));

            if (isLookup) {
                Class[] parms = new Class[]{ActionEvent.class};
                MethodExpression methodExpr = exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                        "#{" + beanName + ".lookupFilterActionListener}", null, parms);
                MethodExpressionActionListener checkBTNActionListener = new MethodExpressionActionListener(methodExpr);

                checkButton.addActionListener(checkBTNActionListener);
                checkButton.setStyle("position:relative; top:-25;");
            } else {
                checkButton.setStyle("position:relative; top:" + requiredHeight + ";");
                Class[] parms2 = new Class[]{ActionEvent.class};
                ExpressionFactory ef = FacesContext.getCurrentInstance().getApplication().getExpressionFactory();
                MethodExpression mb = ef.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                        "#{" + getBeanName() + ".changeState}", null, parms2);
                MethodExpressionActionListener al = new MethodExpressionActionListener(mb);
                checkButton.addActionListener(al);
                backBean.getFilterControls().add(checkButton);
                logger.trace("Filter Control Added");
                // </editor-fold>
            }
            logger.debug("Returning");
            return checkButton;
        } else {
            HtmlInputText inputTextFilter = new HtmlInputText();
            inputTextFilter.setId(id + "_filter");
            inputTextFilter.setImmediate(true);
            inputTextFilter.setSize(controlSize);
            int filterControlsSize = backBean.getFilterControls() != null ? backBean.getFilterControls().size() : 0;
            inputTextFilter.setStyleClass(String.valueOf(filterControlsSize));
            //Whe filter text field loses its focus return false so the interface still responding.
            //Commenting this line will make the system irresposive.
            inputTextFilter.setOnblur("return false;");
            if (isLookup) {
                Class[] parms = new Class[]{ActionEvent.class};
                ExpressionFactory ef = FacesContext.getCurrentInstance().getApplication().getExpressionFactory();
                MethodExpression methodExpr = ef.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                        "#{" + beanName + ".lookupFilterChangeListener}", null, parms);
                MethodExpressionActionListener al = new MethodExpressionActionListener(methodExpr);

                Class[] changeParms = new Class[]{ValueChangeEvent.class};
                MethodExpression methodBinding = FacesContext.getCurrentInstance().getApplication().getExpressionFactory().
                        createMethodExpression(elContext, "#{" + beanName + ".lookupChangeFilterValue}", null, changeParms);
                inputTextFilter.addValueChangeListener(new MethodExpressionValueChangeListener(methodBinding));

                inputTextFilter.setStyle("position:relative; top:-24px; width:99%;");

            } else {
                Class[] parms2 = new Class[]{ActionEvent.class};
                ExpressionFactory ef = FacesContext.getCurrentInstance().getApplication().getExpressionFactory();
                MethodExpression mb = ef.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                        "#{" + getBeanName() + ".filterAction}", Void.class, parms2);
                MethodExpressionActionListener al = new MethodExpressionActionListener(mb);
                Class[] changeParms = new Class[]{ValueChangeEvent.class};
                MethodExpression methodBinding = FacesContext.getCurrentInstance().getApplication().getExpressionFactory().
                        createMethodExpression(elContext, "#{" + beanName + ".filterChangeValue}", null, changeParms);
                inputTextFilter.addValueChangeListener(new MethodExpressionValueChangeListener(methodBinding));

                inputTextFilter.setStyle("position:relative; top:" + requiredHeight + "; width:99%;");
                backBean.getFilterControls().add(inputTextFilter);
            }
            logger.trace("Filter Control Added");
            logger.debug("Returning");
            return inputTextFilter;
        }
    }

    public void changeImage(ColumnSort sort) {
        if (sort == ColumnSort.ASC) {
            this.getImage().setUrl("/resources/AscArrow.gif");
        } else if (sort == ColumnSort.DESC) {
            this.getImage().setUrl("/resources/DescArrow.gif");
        } else if (sort == ColumnSort.NONE) {
            this.getImage().setUrl("/resources/trans.gif");
        }
    }
}
