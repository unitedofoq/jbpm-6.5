/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.bpm.model.Task;
import com.unitedofoq.fabs.bpm.model.status.TaskStatus;
import com.unitedofoq.fabs.core.cash.CacheServiceLocal;
import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.process.HumanTaskServiceRemote;
import com.unitedofoq.fabs.core.process.OProcess;
import com.unitedofoq.fabs.core.process.ProcessButton;
import com.unitedofoq.fabs.core.process.ProcessButtonJavaFunction;
import com.unitedofoq.fabs.core.security.user.Inbox;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import com.unitedofoq.fabs.core.uiframework.screen.FormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.TabularScreen;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.commandlink.CommandLink;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ianwar
 */
public class TaskOptionProcess {

    final static Logger logger = LoggerFactory.getLogger(TaskOptionProcess.class);

    private OBackBean backBean;
    private OUser loggedUser;
    private Task processingTask;
    @EJB
    private DDServiceRemote ddService;
    @EJB
    private HumanTaskServiceRemote humanTaskService;
    @EJB
    private CacheServiceLocal cacheService;
    @EJB
    private DataTypeServiceRemote dataTypeService;

    private boolean disabled = false;
    private ELContext elContext;
    private ExpressionFactory exFactory;
    protected OFunctionServiceRemote functionServiceRemote;

    public TaskOptionProcess(OBackBean backBean, OUser loggedUser, Task processingTask, ELContext elContext, ExpressionFactory exFactory) {

        this.backBean = backBean;
        this.loggedUser = loggedUser;
        this.processingTask = processingTask;
        this.elContext = elContext;
        this.exFactory = exFactory;
        this.functionServiceRemote = OEJB.lookup(OFunctionServiceRemote.class);
        this.ddService = OEJB.lookup(DDServiceRemote.class);
        this.humanTaskService = OEJB.lookup(HumanTaskServiceRemote.class);
        this.cacheService = OEJB.lookup(CacheServiceLocal.class);
        this.dataTypeService = OEJB.lookup(DataTypeServiceRemote.class);

    }

    public String taskActionButtons(ActionEvent ae, Inbox processingInbox, String portletInsId, String screenInstId, OScreen currentScreen) {

        logger.debug("Entering");
        if (disabled) {
            logger.debug("Returning with empty string");
            return "";
        }
        processingTask = humanTaskService.getTaskById(processingTask.getId());
        TaskStatus status = processingTask.getStatus();
        if (!status.equals(TaskStatus.INPROGRESS)) {
            if (status.equals(TaskStatus.READY)) {
                humanTaskService.claimAndStartTask(processingTask, loggedUser);
                processingTask = humanTaskService.getTaskById(processingTask.getId());
            } else if (status.equals(TaskStatus.RESERVED)) {
                humanTaskService.startTask(processingTask, loggedUser);
                processingTask = humanTaskService.getTaskById(processingTask.getId());
            } else {
                OPortalUtil.removePortlet(portletInsId, screenInstId);
                logger.debug("Returning with empty string");
                return "";
            }
        }
        disabled = true;
        try {
            String option = ae.getComponent().getId();
            String[] parts = option.split("_");
            option = parts[parts.length - 1];
            logger.trace("Option Process Action Started");
            ODataMessage out = backBean.saveProcessRecords();
            if (out == null || out.getData() == null || out.getData().isEmpty() || !(out.getData().get(0) instanceof BaseEntity)) {
                out = new ODataMessage();
                List<Object> data = new ArrayList<Object>();
                data.add(((SingleEntityBean) backBean).getLoadedEntity());
                data.add(0, option);
                out.setData(data);
                out.setODataType(dataTypeService.loadODataType("VoidDT", loggedUser));
            }

            OFunctionResult functionResult = new OFunctionResult();

            String processId = processingTask.getProcessId();

            OProcess oProcess = humanTaskService.getBusinessOProcess(processId.substring(processId.indexOf(".") + 1), processId.substring(0, processId.indexOf(".")), loggedUser);

            List<ProcessButton> processButtons = oProcess.getProcessButtons();

            if (processButtons != null) {

                for (ProcessButton processButton : processButtons) {

                    if (processButton.getProcessButtonJavaFunction() != null && processButton.getButtonName() != null && processButton.getButtonName().equalsIgnoreCase(option)) {

                        for (ProcessButtonJavaFunction processButtonJavaFunction : processButton.getProcessButtonJavaFunction()) {

                            functionResult.append(functionServiceRemote.runFunction(processButtonJavaFunction.getJavaFunction(), out, new OFunctionParms(), loggedUser));

                        }
                    }
                }

            }

            if (functionResult.getErrors().size() > 0) {

                backBean.getMessagesPanel().clearAndDisplayMessages(functionResult);
                RequestContext.getCurrentInstance().update(backBean.getMessagesPanel().panelC.getClientId());
                backBean.setDisabled(false);
                return "";

            }
            out.getData().set(0, option);
            if (loggedUser.getLoginName().trim().equalsIgnoreCase(processingTask.getActualOwnerId().trim())) {
                humanTaskService.completeTask(processingTask, out, loggedUser);
            }
            humanTaskService.createOutbox(processingInbox, out, loggedUser);
            String inboxID = (String) cacheService.getFromCache("inboxID", loggedUser);
            backBean.setProcessMode(false);
            try {
                ODataMessage odm1 = new ODataMessage(dataTypeService.loadODataType("VoidDT", loggedUser), new ArrayList<Object>());
                OPortalUtil.removeProcessPortlet(screenInstId, inboxID, portletInsId);
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
            OPortalUtil.renderInbox(currentScreen, inboxID, out);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning with empty string");
        return "";
    }

    public HtmlPanelGrid createProcessPanel(String outboxProcessId, String portletInsId, String beanName, String screenLanguage, boolean invalidInputMode, OScreen currentScreen) {
        logger.debug("Entering");
        HtmlPanelGrid processPanel = new HtmlPanelGrid();
        processPanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "processPanel");
        if (!backBean.isProcessMode()) {
            if ((outboxProcessId != null && !outboxProcessId.equals(""))) {
                HtmlOutputLabel lbl = new HtmlOutputLabel();
                lbl.setStyle("position:absolute;right:20px; top:30px;");
                lbl.setStyleClass("labelTxt");
                if (outboxProcessId != null && !outboxProcessId.equals("")) {
                    lbl.setValue(ddService.getDDLabel("OutBox_ProcessID", loggedUser) + " " + outboxProcessId);
                }
                processPanel.getChildren().add(lbl);
                logger.debug("Returning");
                return processPanel;
            }
            logger.debug("Returning with Null");
            return null;
        }
        String taskActions = "";
        String[] taskActionsValues = new String[]{};
        if (!processingTask.getInputs().isEmpty() && processingTask.getInputs().containsKey("taskActions")) {
            taskActions = (String) processingTask.getInputs().get("taskActions");
            taskActionsValues = taskActions.split("_");
        }
        if (processingTask.isRequestStatus()) {
        } else {
            Class[] parms3 = new Class[]{ActionEvent.class};
            TaskStatus status = humanTaskService.getTaskStatus(processingTask.getId());
            if (taskActionsValues.length > 0) {
                if (status.equals(TaskStatus.READY)) {
                    processPanel.setColumns(taskActionsValues.length * 2 + 2);
                    for (String value : taskActionsValues) {
                        addButtonToPanelGroup(value, value, "taskActionButtons",
                                processPanel, parms3, portletInsId, beanName, screenLanguage, invalidInputMode, currentScreen);
                    }
                    backBean.addClaimButtonToPanelGroup("claim", ddService.getDD("claimButton", loggedUser).getLabelTranslated(), "claimAction",
                            processPanel, parms3, portletInsId, beanName, screenLanguage, invalidInputMode, currentScreen);
                } else {
                    processPanel.setColumns(taskActionsValues.length * 2);
                    for (String value : taskActionsValues) {
                        addButtonToPanelGroup(value, value, "taskActionButtons",
                                processPanel, parms3, portletInsId, beanName, screenLanguage, invalidInputMode, currentScreen);
                    }
                }
            }
        }
        processPanel.setStyle("position:relative");
        String id = Long.toString(processingTask.getProcessInstanceId());

        if (id != null && !id.equals("")) {
            HtmlOutputLabel lbl = new HtmlOutputLabel();
            lbl.setStyleClass("labelTxt");
            lbl.setValue(ddService.getDDLabel("OutBox_ProcessID", loggedUser) + " " + id);
            processPanel.getChildren().add(lbl);
        }
        processPanel.setStyle("100%");
        logger.debug("Returning");
        return processPanel;
    }

    public void addButtonToPanelGroup(String buttonID, String buttonLabel,
            String actionMethodName, HtmlPanelGrid panelLayout, Class[] params, String portletInsId, String beanName, String screenLanguage, boolean invalidInputMode, OScreen currentScreen) {
        logger.debug("Entering");
        CommandButton button = new CommandButton();
        button.setTitle(buttonLabel);
        if (screenLanguage.equalsIgnoreCase("Arabic")) {
            button.setStyle("position:relative;text-align:right;");
        } else {
            button.setStyle("position:relative");
        }
        button.setDir((loggedUser.isRtl()) ? "rtl" : "ltr");//set the direction of the button text
        button.setIcon("none");
        button.setValue(buttonLabel);
        button.setTitle(buttonLabel);
        button.setStyleClass("general_btn");
        if (actionMethodName != null) {
            if (null != params) {
                MethodExpression mb = exFactory.createMethodExpression(elContext,
                        "#{" + beanName + "." + actionMethodName + "}", null, params);
                MethodExpressionActionListener al = new MethodExpressionActionListener(mb);
                button.addActionListener(al);
            } else {
                button.setActionExpression(
                        exFactory.createMethodExpression(
                                elContext,
                                "#{" + beanName + "." + actionMethodName + "}",
                                String.class,
                                new Class[0]));
            }
        }

        String buttonsId = backBean.getFacesContext().getViewRoot().createUniqueId();

        HtmlPanelGrid btnPanel = new HtmlPanelGrid();
        btnPanel.setStyle("position:relative;margin:0px;border:0px;padding:0px;");
        btnPanel.setId(buttonsId + "_butPanel");

        HtmlPanelGrid buttonTiltlePanel = new HtmlPanelGrid();
        buttonTiltlePanel.setId(buttonsId + "_butTitlePanel");
        buttonTiltlePanel.setStyleClass("buttonTitleLinkPanel");

        CommandLink buttonTitleLink = new CommandLink();//buttonTitleLink
        buttonTitleLink.setId(buttonsId + "_butTitle");
        buttonTitleLink.setStyleClass("buttonTitleLink");
        buttonTitleLink.setStyle("display:none");
        buttonTitleLink.setValue(buttonLabel);
        buttonTitleLink.setOnclick("top.FABS.Portlet.buttonTitleAction(this.id,'" + portletInsId + "');return false;");

        buttonTitleLink.getChildren().add(buttonTitleLink);
        buttonTitleLink.setStyleClass("buttonTitleLink");

        button.setId(buttonsId + "_button_" + buttonID);

        buttonTiltlePanel.getChildren().add(buttonTitleLink);

        btnPanel.getChildren().add(button);
        btnPanel.getChildren().add(buttonTiltlePanel);

        HtmlPanelGrid grid = new HtmlPanelGrid();
        grid.setColumns(1);
        grid.setId(buttonsId + "_butPanelGrid");
        grid.getChildren().add(btnPanel);
        panelLayout.getChildren().add(button);
        if (invalidInputMode) {
            button.setDisabled(true);
        }

        button.setAjax(true);

        if (currentScreen instanceof TabularScreen) {
            button.setUpdate("tf:generalPanel");
        } else if (currentScreen instanceof FormScreen) {
            button.setUpdate("frmScrForm:middlePanel");
        }
        logger.debug("Returning");
    }

    public void addButtonToPanelGroup(String buttonID, String buttonLabel,
            String actionMethodName, HtmlPanelGrid panelLayout, String portletInsId, String beanName, String screenLanguage, boolean invalidInputMode, OScreen currentScreen) {
        addButtonToPanelGroup(buttonID, buttonLabel, actionMethodName,
                panelLayout, new Class[]{}, portletInsId, beanName, screenLanguage, invalidInputMode, currentScreen);
    }
}
