/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.listener;

import java.util.Iterator;
import javax.faces.FacesException;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author arezk
 */
public class ViewExpiredExceptionHandler extends ExceptionHandlerWrapper {

    private ExceptionHandler wrapped;
    final static Logger logger = LoggerFactory.getLogger(ViewExpiredExceptionHandler.class);

    ViewExpiredExceptionHandler(ExceptionHandler exception) {
        this.wrapped = exception;
    }

    @Override
    public ExceptionHandler getWrapped() {
        return wrapped;
    }

    @Override
    public void handle() throws FacesException {
        logger.debug("Entering");
        final Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents().iterator();
        while (i.hasNext()) {
            ExceptionQueuedEvent event = i.next();
            ExceptionQueuedEventContext context
                    = (ExceptionQueuedEventContext) event.getSource();
            // get the exception from context
            Throwable t = context.getException();
            logger.debug("Start ViewExpiredExceptionHandler Exception");
            if (null != t) {
                logger.debug(t.getMessage());
            }
            logger.debug("End ViewExpiredExceptionHandler Exception");

            if (t instanceof ViewExpiredException) {

                final FacesContext facesContext = FacesContext.getCurrentInstance();
                Flash flash = facesContext.getExternalContext().getFlash();
                flash.put("errorMessage", "Session Time Out : Please Reload the Current Page . ");

                //here you do what ever you want with exception
                try {
//                    // Kill Liferay Session
//                    RequestContext requestContext = RequestContext.getCurrentInstance();
//                    requestContext.execute("top.FABS.Portlet.killLiferaySession();");
//                    //redirect error page
                    String errorPageLocation = "/errorPage.iface";
                    facesContext.setViewRoot(facesContext.getApplication().getViewHandler().createView(facesContext, errorPageLocation));
                    facesContext.getPartialViewContext().setRenderAll(true);
                    facesContext.renderResponse();
                } finally {
                    //remove it from queue
                    i.remove();
                }
            }
        }
        try {
            //parent hanle
            getWrapped().handle();
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
    }
}
