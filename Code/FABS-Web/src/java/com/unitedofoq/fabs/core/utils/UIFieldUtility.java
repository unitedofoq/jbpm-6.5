/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.utils;

import com.unitedofoq.fabs.core.dd.DD;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mohamed
 */
public final class UIFieldUtility {

    final static org.slf4j.Logger logger = LoggerFactory.getLogger(UIFieldUtility.class);

    private UIFieldUtility() {

    }

    /**
     * Handle null sub-entity state which needed for building JSF screens.
     * example: render entity "ScreenField" with field expression
     * "#{ScreenField.changeFunction.name}" which has null "changeFunction"
     * should be #{(ScreenField.changeFunction == null) ? '' :
     * ScreenField.changeFunction.name}.
     *
     * @param fieldExp
     * @param rebuild
     * @return
     */
    public static String buildScreenFieldExp(String fieldExp, boolean rebuild) {
        if (fieldExp != null && !fieldExp.trim().equals("") && rebuild) {
            boolean fieldContainJSFTags = fieldExp.contains("#");
            if (fieldContainJSFTags) {
                fieldExp = fieldExp.replace("#{", "").replace("}", "");
            }
            if (fieldExp.contains("valuesMap")) {
                int index = fieldExp.lastIndexOf("].");
                return fieldExp.substring(0, index);
            }
            String[] fieldExpComponents = fieldExp.split("\\.");
            String resultExp;
            if (fieldExpComponents.length > 2) {
                String currentField = fieldExpComponents[0];
                String currentExp = "";
                for (int i = 1; i < fieldExpComponents.length - 1; i++) {
                    if (i != 1) {
                        currentExp += " || ";
                    }
                    currentField += ("." + fieldExpComponents[i]);
                    currentExp += currentField + " == null";
                }
                resultExp = "(" + currentExp + ") ? null : " + fieldExp;
            } else {
                resultExp = fieldExp;
            }
            if (fieldContainJSFTags) {
                resultExp = "#{" + resultExp + "}";
            }
            return resultExp;
        } else {
            return fieldExp;
        }
    }

    public static String buildFieldExpForDropDown(String fieldExpression, DD fieldDD) {
        StringBuilder resultExp = new StringBuilder();
        try {
            String fieldExp = fieldExpression.replace("#{", "").replace("}", "");
            String lookupFilterDD = fieldDD.getLookupFilter();
            String[] DDExpression = lookupFilterDD.split(";");
            for (int i = 0; i < DDExpression.length; i++) {
                String DDExpressionSplit = DDExpression[i];
                String[] DDExpressionValue = DDExpressionSplit.split(",");
                resultExp.append(fieldExp).append(" ==  '").append(DDExpressionValue[1]).append("'  ?  '").append(DDExpressionValue[0]).
                        append("' ").append(" : ");
                if (i == DDExpression.length - 1) {
                    resultExp.append("'------'");
                }
            }
        } catch (Exception e) {
            logger.error("error when bulid FieldExp For DropDown", e);
        }
        return "#{" + resultExp.toString() + "}";
    }

}
