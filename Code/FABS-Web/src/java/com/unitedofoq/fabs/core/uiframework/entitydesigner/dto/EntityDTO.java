/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.entitydesigner.dto;

import java.util.List;

/**
 *
 * @author Mostafa
 */
public class EntityDTO {
    private int entityIndex;

   
    private String entityName;
    private List<FieldDTO> fields;
    private int xPostion ;
    private int yPostion ;

    /**
     * @return the entityName
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * @param entityName the entityName to set
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    /**
     * @return the fields
     */
    public List<FieldDTO> getFields() {
        return fields;
    }

    /**
     * @param fields the fields to set
     */
    public void setFields(List<FieldDTO> fields) {
        this.fields = fields;
    }

    /**
     * @return the xPostion
     */
    public int getxPostion() {
        return xPostion;
    }

    /**
     * @param xPostion the xPostion to set
     */
    public void setxPostion(int xPostion) {
        this.xPostion = xPostion;
    }

    /**
     * @return the yPostion
     */
    public int getyPostion() {
        return yPostion;
    }

    /**
     * @param yPostion the yPostion to set
     */
    public void setyPostion(int yPostion) {
        this.yPostion = yPostion;
    }
    
     public int getEntityIndex() {
        return entityIndex;
    }

    public void setEntityIndex(int entityIndex) {
        this.entityIndex = entityIndex;
    }
            
}
