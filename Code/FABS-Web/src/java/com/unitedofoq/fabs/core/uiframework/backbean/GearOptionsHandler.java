/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.function.OMenu;
import com.unitedofoq.fabs.core.report.OReport;
import com.unitedofoq.fabs.core.security.SecurityServiceRemote;
import com.unitedofoq.fabs.core.security.user.UserPrivilege.ConstantUserPrivilege;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalPage;
import com.unitedofoq.fabs.core.uiframework.screen.ChartScreen;
import com.unitedofoq.fabs.core.uiframework.screen.FormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.M2MRelationScreen;
import com.unitedofoq.fabs.core.uiframework.screen.MultiLevelScreen;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.TabularScreen;
import com.unitedofoq.fabs.core.uiframework.screen.wizard.Wizard;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.primefaces.component.menubutton.MenuButton;
import org.primefaces.component.menuitem.MenuItem;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mahmed
 */
public class GearOptionsHandler {

    final static org.slf4j.Logger logger = LoggerFactory.getLogger(TabularBean.class);
    public OScreen currentScreen;
    public ExpressionFactory exFactory;

    InitialContext ctx;
    SecurityServiceRemote securityService;
    EntitySetupServiceRemote entitySetupService;
    DDServiceRemote ddService;
    private OUser loggedUser;

    public GearOptionsHandler(OUser loggedUser) {
        this.loggedUser = loggedUser;
        try {
            ctx = new InitialContext();
            securityService = (SecurityServiceRemote) ctx.lookup("java:global/ofoq/com.unitedofoq.fabs.core.security.SecurityServiceRemote");
            entitySetupService = (EntitySetupServiceRemote) ctx.lookup("java:global/ofoq/com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote");
            ddService = (DDServiceRemote) ctx.lookup("java:global/ofoq/com.unitedofoq.fabs.core.dd.DDServiceRemote");

        } catch (NamingException ex) {
            logger.error("Exception thrown", ex);
        }

    }

    private void prepareMenuItem(MenuItem menuItem, OBackBean obackBean) {
        menuItem.addActionListener(prepareActionListener(obackBean));
        menuItem.setAjax(true);
        menuItem.setPartialSubmit(true);
        menuItem.setImmediate(true);
    }

    private MethodExpressionActionListener prepareActionListener(OBackBean obackBean) {
        Class[] parms2 = new Class[]{ActionEvent.class};
        ExpressionFactory ef = FacesContext.getCurrentInstance().getApplication().getExpressionFactory();
        MethodExpression mb = ef.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                "#{" + obackBean.getBeanName() + ".screenMenuAction}", null, parms2);
        MethodExpressionActionListener al = new MethodExpressionActionListener(mb);
        return al;
    }

    public UIComponent addGearToPortletOptions(OBackBean obackBean) {
        logger.debug("Entering");
            MenuButton button = new MenuButton();
            String optionsTitle = ddService.getDDLabel("OptionsTitle", obackBean.getLoggedUser());
            button.setValue(optionsTitle);
            button.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_Options");
            String align = loggedUser.isRtl() ? " left:20px; " : " right:20px;";
            String position = "fixed";
            button.setStyle(
                    "position:" + position + ";  "
                    + align
                    + "top :5px; "
                    + "font-size : 12px !important; "
                    + "z-index:1000000;"
                    + " color:gray !important;"
                    + "background : none !important"
            );

            //<editor-fold defaultstate="collapsed" desc="help">
            MenuItem help = new MenuItem();
            help.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_Help");
            prepareMenuItem(help, obackBean);
            // Current Screen DDs
            String currentScreenhelp = ddService.getDDLabel("currentHelpDDGear", loggedUser);
            help.setValue(currentScreenhelp);
            button.getChildren().add(help);
            //</editor-fold>

            
            
            MenuItem screenFLDsMenuItem = obackBean.createScreenFLDMenuItem(prepareActionListener(obackBean));
                if (screenFLDsMenuItem != null && (securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_button, loggedUser)
                        || securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_screenfldsmenuitem, loggedUser))) {
                    button.getChildren().add(screenFLDsMenuItem);
                }
            //<editor-fold defaultstate="collapsed" desc="ExportExcel">
            if (obackBean instanceof TabularBean) {

                if (securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_button, loggedUser)
                        || securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_exportExcel, loggedUser)) {

                    MenuItem exportExcel = new MenuItem();
                    exportExcel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "ExportExcel");
                    prepareMenuItem(exportExcel, obackBean);
                    // Current Screen DDs
                    String currentExportExcel = ddService.getDDLabel("exportExcelDD", loggedUser);
                    exportExcel.setValue(currentExportExcel);
                    button.getChildren().add(exportExcel);
                }
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="ExportCsv">

                if (securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_button, loggedUser)
                        || securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_exportCsv, loggedUser)) {

                    MenuItem exportCsv = new MenuItem();
                    exportCsv.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "ExportCsv");
                    prepareMenuItem(exportCsv, obackBean);
                    // Current Screen DDs
                    String currentExportCsv = ddService.getDDLabel("exportCsvDD", loggedUser);
                    exportCsv.setValue(currentExportCsv);
                    button.getChildren().add(exportCsv);
                }
                //</editor-fold>        
                //<editor-fold defaultstate="collapsed" desc="ExportPdf">

                if (securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_button, loggedUser)
                        || securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_exportPdf, loggedUser)) {

                    MenuItem exportPdf = new MenuItem();

                    exportPdf.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "ExportPdf");
                    prepareMenuItem(exportPdf, obackBean);
                    // Current Screen DDs
                    String currentExportPdf = ddService.getDDLabel("exportPdfDD", loggedUser);
                    exportPdf.setValue(currentExportPdf);
                    button.getChildren().add(exportPdf);
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="Excel Template">
                if (securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_button, loggedUser)
                        || securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_exceltemplate, loggedUser)) {
                    MenuItem excelTemplate = new MenuItem();
                    excelTemplate.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_ExcelTemplate");
                    prepareMenuItem(excelTemplate, obackBean);
                    // Current Screen DDs
                    String currentScreenexcelTemplate = ddService.getDDLabel("currentexcelTemplateGear", loggedUser);
                    excelTemplate.setValue(currentScreenexcelTemplate);
                    button.getChildren().add(excelTemplate);
                }
                //</editor-fold>
               
            }

            //<editor-fold defaultstate="collapsed" desc="AttachmentScreen">
            if (obackBean instanceof FormBean) {
                if (securityService.checkPrivilegeAuthority(ConstantUserPrivilege.Attachment_PreSave, loggedUser)) {

                    MenuItem exportPdf = new MenuItem();
                    exportPdf.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "AttachmentScreen");
                    prepareMenuItem(exportPdf, obackBean);
                    // Current Screen DDs
                    String currentExportPdf = ddService.getDDLabel("attachmentScreenDD", loggedUser);
                    exportPdf.setValue(currentExportPdf);
                    button.getChildren().add(exportPdf);
                }

                //</editor-fold>
                
                //<editor-fold defaultstate="collapsed" desc="Reload">
            MenuItem reloadMenuItem = new MenuItem();
            String reloadGear = ddService.getDDLabel("reloadGear", loggedUser);
            reloadMenuItem.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_reloadGear");
            reloadMenuItem.setValue(reloadGear);
            prepareMenuItem(reloadMenuItem, obackBean);
            button.getChildren().add(reloadMenuItem);

            //</editor-fold>
            }
            if (obackBean.isShowManagePortlePage()) {
                //<editor-fold defaultstate="collapsed" desc="managePagePortlets">
                if (securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_button, loggedUser)
                        || securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_managepageportlets, loggedUser)) {
                    MenuItem mngPageMenuItem = new MenuItem();
                    mngPageMenuItem.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "managePagePortlets");
                    prepareMenuItem(mngPageMenuItem, obackBean);
                    // Manage Portal Page Portlets
                    String managePagePortletsGear = ddService.getDDLabel("managePagePortletsGear", loggedUser);
                    mngPageMenuItem.setValue(managePagePortletsGear);
                    button.getChildren().add(mngPageMenuItem);
                }
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="exportPageMenuItem">
                if (securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_button, loggedUser)
                        || securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_exportcurrentpage, loggedUser)) {
                    MenuItem exportPageMenuItem = new MenuItem();
                    exportPageMenuItem.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "ExportCurrentPage");

                    prepareMenuItem(exportPageMenuItem, obackBean);
                    // Manage Portal Page Portlets
                    String exportPageGear = ddService.getDDLabel("exportPageGear", loggedUser);
                    exportPageMenuItem.setValue(exportPageGear);
                    button.getChildren().add(exportPageMenuItem);
                }
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="printPageMenuItem">
                if (securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_button, loggedUser)
                        || securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_printpage, loggedUser)) {
                    MenuItem printPageMenuItem = new MenuItem();
                    printPageMenuItem.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "PrintPage");
                    prepareMenuItem(printPageMenuItem, obackBean);
                    // Manage Portal Page Portlets
                    String printPageGear = ddService.getDDLabel("printPageGear", loggedUser);
                    printPageMenuItem.setValue(printPageGear);
                    button.getChildren().add(printPageMenuItem);
                }
                //</editor-fold>
            }

            if (securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_button, loggedUser)
                    || securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_entityfiltermenuitem, loggedUser)) {
                MenuItem screenEntityFilterMenuItem = obackBean.createEntityFilterMenuItem(prepareActionListener(obackBean), "screenEntityFilterMenuItem");
                MenuItem entityFiltersMenuItem = obackBean.createEntityFilterMenuItem(prepareActionListener(obackBean), "entityFiltersMenuItem");
                button.getChildren().add(screenEntityFilterMenuItem);
                button.getChildren().add(entityFiltersMenuItem);
            }
            //<editor-fold defaultstate="collapsed" desc="exportCurrentScreenGear">
            if (securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_button, loggedUser)
                    || securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_exportscreenmetadata, loggedUser)) {
                MenuItem exportScreenMetaMenuItem = new MenuItem();
                String exportScreenMetaMenuItemDD = ddService.getDDLabel("exportCurrentScreen", loggedUser);
                exportScreenMetaMenuItem.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "ExportScreenMetaData");
                exportScreenMetaMenuItem.setValue(exportScreenMetaMenuItemDD);
                prepareMenuItem(exportScreenMetaMenuItem, obackBean);
                button.getChildren().add(exportScreenMetaMenuItem);
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="exportScreenData">
            currentScreen = obackBean.getCurrentScreen();
            String entityName = entitySetupService.
                    getOScreenEntityDTO(currentScreen.getDbid(), loggedUser).
                    getEntityClassName();
            if ((entityName.equals(FormScreen.class.getSimpleName()) || (!obackBean.isShowManagePortlePage())
                    || entityName.equals(TabularScreen.class.getSimpleName())
                    || entityName.equals(M2MRelationScreen.class.getSimpleName())
                    || entityName.equals(MultiLevelScreen.class.getSimpleName())
                    || entityName.equals(ChartScreen.class.getSimpleName())
                    || entityName.equals(OPortalPage.class.getSimpleName())
                    || entityName.equals(OMenu.class.getSimpleName())
                    || entityName.equals(OEntity.class.getSimpleName())
                    || entityName.equals(Wizard.class.getSimpleName())
                    || entityName.equals(OReport.class.getSimpleName())
                    || "ORepTemplDS".equals(entityName)
                    || "OTemplRep".equals(entityName))
                    && (securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_button, loggedUser)
                    || securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_exportscreendata, loggedUser))) {

                MenuItem exportScreendataMenuItem = new MenuItem();
                String exportScreenDataMenuItemDD = ddService.getDDLabel("exportData", loggedUser);
                exportScreendataMenuItem.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "ExportScreenData");
                exportScreendataMenuItem.setValue(exportScreenDataMenuItemDD);
                prepareMenuItem(exportScreendataMenuItem, obackBean);
                button.getChildren().add(exportScreendataMenuItem);
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="Screen Translation">
            if (loggedUser.getFirstLanguage().getDbid() != OUser.ARABIC) {
                MenuItem screenTransMenuItem = new MenuItem();
                prepareMenuItem(screenTransMenuItem, obackBean);
                String screenTransMenuItemDD = ddService.getDDLabel("screenTransMenuItem", loggedUser);
                screenTransMenuItem.setId(FacesContext.getCurrentInstance().getViewRoot()
                        .createUniqueId() + "ScreenTransMenuItem");
                MethodExpression transMB = obackBean.exFactory.createMethodExpression(
                        FacesContext.getCurrentInstance().getELContext(),
                        "#{" + obackBean.getBeanName() + ".screenTranslationMenuAction}", null,
                        new Class[]{ActionEvent.class});
                MethodExpressionActionListener transAl = new MethodExpressionActionListener(transMB);
                screenTransMenuItem.addActionListener(transAl);
                screenTransMenuItem.setValue(screenTransMenuItemDD);
                if (screenTransMenuItem != null && (securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_button, loggedUser)
                        || securityService.checkPrivilegeAuthority(ConstantUserPrivilege.option_screentransmenuitem, loggedUser))) {
                    button.getChildren().add(screenTransMenuItem);
                }
            }
            //</editor-fold>

            return button;
    }

}
