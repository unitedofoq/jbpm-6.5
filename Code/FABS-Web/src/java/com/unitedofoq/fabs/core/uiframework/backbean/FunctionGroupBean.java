/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.function.FunctionGroup;
import com.unitedofoq.fabs.core.function.FunctionGroupFunctions;
import com.unitedofoq.fabs.core.function.FunctionGroupScreen;
import com.unitedofoq.fabs.core.function.FunctionGroupScreenGroup;
import com.unitedofoq.fabs.core.function.JavaFunction;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.ServerJob;
import com.unitedofoq.fabs.core.function.WebServiceFunction;

import com.unitedofoq.fabs.core.report.ReportFunction;
import com.unitedofoq.fabs.core.uiframework.backbean.wizard.WizardController;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalPage;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import com.unitedofoq.fabs.core.uiframework.portal.PortalPageFunction;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenFunction;
import com.unitedofoq.fabs.core.uiframework.screen.UDCScreenFunction;
import com.unitedofoq.fabs.core.uiframework.screen.wizard.Wizard;
import com.unitedofoq.fabs.core.uiframework.screen.wizard.WizardFunction;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.el.MethodExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;
import java.util.Collections;
import org.primefaces.component.accordionpanel.AccordionPanel;
import org.primefaces.component.commandlink.CommandLink;
import org.primefaces.component.tabview.Tab;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author arezk
 */
@ManagedBean(name = "FunctionGroupBean")
@ViewScoped
public class FunctionGroupBean extends FormBean {
final static Logger logger = LoggerFactory.getLogger(FunctionGroupBean.class);
    
    DataTypeServiceRemote dtService = OEJB.lookup(DataTypeServiceRemote.class);

    public FunctionGroupBean() {
    }

    @Override
    protected boolean onInit(Map<String, String> requestParams) {
        if (!super.onInit(requestParams)) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public String refresh() {
        super.refresh();
        return null;
    }

    @Override
    protected String getBeanName() {
        return "FunctionGroupBean";
    }
    private HtmlPanelGrid mainGrid;

    public void setMainGrid(HtmlPanelGrid mainGrid) {
        this.mainGrid = mainGrid;
    }

    public HtmlPanelGrid getMainGrid() {
        logger.debug("Entering");
        try {
            logger.debug("Returning");
            return buildGroupInnerScreen();
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            //FIXME: display user message
            logger.debug("Returning with Null");
            return null;
        }
    }

    private HtmlPanelGrid buildGroupInnerScreen() {
        logger.trace("Entering");
        HtmlPanelGrid grid = new HtmlPanelGrid();
        messagesPanel = new UserMessagePanel(exFactory, elContext, getBeanName(), ddService, this, loggedUser);
        processInput();
        grid.getChildren().add(messagesPanel.createMessagePanel());

        try {
            AccordionPanel accordionPanel = new AccordionPanel();
            accordionPanel.setId("MainAccordion_" + FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
            // multiple opened tabs
            FunctionGroupScreen functionGroupScreen
                    = (FunctionGroupScreen) loadedObject;
            ArrayList<FunctionGroupScreenGroup> functionGroupScreenGroups
                    = (ArrayList<FunctionGroupScreenGroup>) oem.loadEntityList("FunctionGroupScreenGroup",
                            Collections.singletonList("functionGroupScreen.dbid = "
                                    + functionGroupScreen.getDbid()), null, null, loggedUser);

            if (functionGroupScreenGroups == null) {
                logger.trace("Returning");
                return grid;
            }

            ArrayList<FunctionGroup> functionGroups = new ArrayList<FunctionGroup>();
            Comparator functionGroupScreenGroupsComparator = new Comparator() {
                @Override
                public int compare(Object o1, Object o2) {
                    FunctionGroupScreenGroup group1 = (FunctionGroupScreenGroup) o1;
                    FunctionGroupScreenGroup group2 = (FunctionGroupScreenGroup) o2;
                    Integer sortIndex = group1.getSortIndex();
                    Integer level2index = group2.getSortIndex();
                    logger.trace("Returning");
                    return sortIndex.compareTo(level2index);
                }
            };

            Collections.sort(functionGroupScreenGroups, functionGroupScreenGroupsComparator);

            for (FunctionGroupScreenGroup functionGroupScreenGroup : functionGroupScreenGroups) {
                FunctionGroup functionGroup = functionGroupScreenGroup.getFunctionGroup();
                if (functionGroup.isInActive()) {
                    continue;
                }
                functionGroups.add(functionGroup);
            }

            for (FunctionGroup functionGroup : functionGroups) {
                Tab tab = new Tab();
                tab.setId("tab_" + FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
                tab.setTitle(functionGroup.getName());
                List<FunctionGroupFunctions> FunctionGroupFunctions = oem.loadEntityList("FunctionGroupFunctions", Collections.singletonList(
                        "functionGroup.dbid = " + functionGroup.getDbid()), null, null, loggedUser);

                Comparator functionGroupFunctionsComparator = new Comparator() {
                    @Override
                    public int compare(Object o1, Object o2) {
                        FunctionGroupFunctions group1 = (FunctionGroupFunctions) o1;
                        FunctionGroupFunctions group2 = (FunctionGroupFunctions) o2;
                        Integer sortIndex = group1.getSortIndex();
                        Integer level2index = group2.getSortIndex();
                        logger.trace("Returning");
                        return sortIndex.compareTo(level2index);
                    }
                };

                Collections.sort(FunctionGroupFunctions, functionGroupFunctionsComparator);
                for (FunctionGroupFunctions functionGroupFunction : FunctionGroupFunctions) {
                    OFunction ofunction = functionGroupFunction.getOfunction();
                    if (ofunction.isInActive()) {
                        continue;
                    }
                    HtmlPanelGrid panelGrid = new HtmlPanelGrid();
                    panelGrid.setId("functionsGrid_" + FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
                    panelGrid.setColumns(2);
                    panelGrid.setCellpadding("10");
                    CommandLink functionLink = new CommandLink();
                    long generatedID = 10 + (int) (Math.random() * (99 - 10) + 1);
                    if (generatedID < 10 || generatedID > 99) {
                        logger.warn("Error in generating the menu code");
                    }
                    String ID = "ID" + generatedID + "_" + ofunction.getDbid();
                    functionLink.setId(ID);
                    functionLink.setValue(ofunction.getName());
                    Class[] parms = new Class[]{ActionEvent.class};
                    MethodExpression functionMethodExpr = exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                            "#{" + getBeanName() + ".functionActionListener}", null, parms);
                    MethodExpressionActionListener functionActionListener = new MethodExpressionActionListener(functionMethodExpr);
                    functionLink.addActionListener(functionActionListener);
                    panelGrid.getChildren().add(functionLink);
                    tab.getChildren().add(panelGrid);
                }
                accordionPanel.getChildren().add(tab);
            }
            grid.getChildren().add(accordionPanel);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            UserMessage errorMsg = usrMsgService.getUserMessage(
                    ex.getMessage(), systemUser);
            errorMsg.setMessageType(UserMessage.TYPE_ERROR);
            if (errorMsg != null) {
                //getMessages().add(errorMsg);
                messagesPanel.addMessages(errorMsg);
                logger.trace("Returning");
                return grid;
            }
        }
        dataSort = new ArrayList<String>();
        logger.trace("Returning");
        return grid;
    }

    @Override
    public boolean processInput() {
        try {
            loadedObject = (FunctionGroupScreen) getCurrentScreen();
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        return true;
    }

    public void functionActionListener(ActionEvent event) {
        logger.debug("Entering");
        try {
            oem.getEM(loggedUser);
            String scrInstId = "";
            // 'substring(5)' is used to get the dbid of the function, by substracting
            // the first 5 characters, which are: "ID" + 2 random numbers + "_"
            long DBID = Long.valueOf(((HtmlCommandLink) event.getComponent()).getId().substring(5));
            OFunction ofunction = null;
            ofunction = functionServiceRemote.getOFunction(DBID, loggedUser);
            if (ofunction instanceof UDCScreenFunction) {
                UDCScreenFunction screenFunction = (UDCScreenFunction) ofunction;
                ODataType parentDT = dataTypeService.loadODataType("ParentDBID", systemUser);
                //FIXME: performance, load this ODataType once static (ondemand) in the class
                ODataMessage parentDataMessage = new ODataMessage();
                List dataList = new ArrayList();
                dataList.add(screenFunction.getType().getDbid());
                parentDataMessage.setData(dataList);
                parentDataMessage.setODataType(parentDT);

                //call construct screenInstId
                scrInstId = generateScreenInstId(screenFunction.getOscreen().getName());
                if (screenFunction.getScreenMode() != null) {
                    SessionManager.setScreenSessionAttribute(
                            scrInstId, MODE_VAR, screenFunction.getScreenMode().getDbid());
                }
                SessionManager.setScreenSessionAttribute(
                        scrInstId, OPortalUtil.MESSAGE, parentDataMessage);

                SessionManager.setScreenSessionAttribute(
                        scrInstId, OBackBean.WIZARD_VAR, false);

                openScreenCheckFilter(screenFunction.getOscreen(), parentDataMessage, scrInstId);
            } else if (ofunction instanceof ScreenFunction) {
                ScreenFunction screenFunction = (ScreenFunction) ofunction;
                logger.trace("Opening ScreenFunction");
                scrInstId = generateScreenInstId(screenFunction.getOscreen().getName());
                ODataType voidDataType = dataTypeService.loadODataType("VoidDT", systemUser);
                //FIXME: performance, load this ODataType once static (ondemand) in the class
                ODataMessage voidDataMessage = new ODataMessage();
                voidDataMessage.setData(new ArrayList<Object>());
                voidDataMessage.setODataType(voidDataType);

                if (screenFunction.getScreenMode() != null) {
                    SessionManager.setScreenSessionAttribute(
                            scrInstId, MODE_VAR, screenFunction.getScreenMode().getDbid());
                }
                SessionManager.setScreenSessionAttribute(
                        scrInstId, OPortalUtil.MESSAGE, voidDataMessage);

                SessionManager.setScreenSessionAttribute(
                        scrInstId, OBackBean.WIZARD_VAR, false);

                openScreenCheckFilter(screenFunction.getOscreen(), voidDataMessage, scrInstId);

            } else if (ofunction instanceof PortalPageFunction) {
                PortalPageFunction portalPageFunction = (PortalPageFunction) ofunction;
                ArrayList<String> fieldExpressions = new ArrayList<String>();
                fieldExpressions.add("portlets");
                OPortalPage oPortalPage = (OPortalPage) oem.loadEntity(
                        "OPortalPage", portalPageFunction.getPortalPage().getDbid(),
                        null, fieldExpressions, getLoggedUser());

                List<Long> portalPageData = (List<Long>) functionServiceRemote.getPortalPageData(portalPageFunction.getDbid(), loggedUser);
                OPortalUtil.addOPortletPage(portalPageData, oem, dataTypeService, loggedUser, null,
                        uiFrameWorkFacade, uDCService);

            } else if (ofunction instanceof ReportFunction) {
                ReportFunction reportFunction = (ReportFunction) ofunction;
                openReportCheckFilter(reportFunction.getReport(), null);
            } else if (ofunction instanceof WizardFunction) {
                ODataType voidDataType = dataTypeService.loadODataType("VoidDT", systemUser);
                //FIXME: performance, load this ODataType once static (ondemand) in the class
                ODataMessage voidDataMessage = new ODataMessage();
                voidDataMessage.setData(new ArrayList<Object>());
                voidDataMessage.setODataType(voidDataType);
                Wizard wizard = ((WizardFunction) ofunction).getWizard();
                textTranslationServiceRemote.loadEntityTranslation(wizard, loggedUser);
                WizardController wizardController = new WizardController(oem,
                        wizard, loggedUser, usrMsgService);
                wizardController.openWizard(voidDataMessage);

            } else if (ofunction instanceof WebServiceFunction) {
                //FIXME: add any log
            } else if (ofunction instanceof JavaFunction) {
                ODataMessage oDM = new ODataMessage();
                List<Object> data = new ArrayList();
                data.add(getLoggedUser());
                oDM.setData(data);
                OFunctionResult oFR = functionServiceRemote.runFunction(ofunction, oDM, null, getLoggedUser());
                String code = (String) oFR.getReturnValues().get(0);
                if (code != null) {
                    // Should be server job DBID of server job function if so
                    OScreen addEditServerJobScreen = (OScreen) oem.loadEntity("FormScreen",
                            java.util.Collections.singletonList("name='AddEditServerJob'"), null,
                            oem.getSystemUser(loggedUser));
                    ServerJob serverJob = (ServerJob) oem.loadEntity("ServerJob",
                            java.util.Collections.singletonList("dbid=" + code), null,
                            oem.getSystemUser(loggedUser));
                    ODataType serviceJobDT = dtService.loadODataType("ServerJob",
                            oem.getSystemUser(loggedUser));
                    ODataMessage screenDM = new ODataMessage();
                    screenDM.setData(new ArrayList<Object>());
                    screenDM.getData().add(serverJob);
                    screenDM.setODataType(serviceJobDT);
                    scrInstId = generateScreenInstId(addEditServerJobScreen.getName());
                    SessionManager.setScreenSessionAttribute(scrInstId,
                            OPortalUtil.MESSAGE, screenDM);
                    SessionManager.setScreenSessionAttribute(scrInstId, MODE_VAR, "18");

                    openScreenCheckFilter(addEditServerJobScreen, screenDM, scrInstId);
                }
            } else {
                ODataMessage oDM = new ODataMessage();
                List<Object> data = new ArrayList();
                data.add(getLoggedUser());
                oDM.setData(data);
                OFunctionResult oFR = functionServiceRemote.runFunction(ofunction, oDM, null, getLoggedUser());
            }
            logger.trace("Exit processAction");
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
        }
    }
}
