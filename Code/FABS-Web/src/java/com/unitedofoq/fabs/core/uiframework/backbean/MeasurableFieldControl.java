/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.filter.OFilterServiceLocal;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nkhalil
 */
public class MeasurableFieldControl {

    final static Logger logger = LoggerFactory.getLogger(MeasurableFieldControl.class);
    
    private OFilterServiceLocal filterService = OEJB.lookup(OFilterServiceLocal.class);
    
    protected UserMessageServiceRemote usrMsgService = OEJB.lookup(UserMessageServiceRemote.class);
    
    public EntitySetupServiceRemote entitySetupService = OEJB.lookup(EntitySetupServiceRemote.class);
    private String measurableUnitFldExp, ownerScreenExpression;
    private int screenFieldIndex;
    private long mode;
    private ScreenField measurableField;
    private ExpressionFactory exFactory;
    private ELContext elContext;
    private Class entityCls;
    private DDServiceRemote ddService;
    private OUser loggedUser;
    private OEntityManagerRemote oEntityManagerRemote;
    private List dropdownList;
    private FacesContext facesContext;
    private String backName;
    private HtmlSelectOneMenu unitsDropDown;
    private List<SelectItem> selectItems;
    private FABSDropDownConverter converter;
    private String htmlDir;

    public FABSDropDownConverter getConverter() {
        return converter;
    }

    public void setConverter(FABSDropDownConverter converter) {
        this.converter = converter;
    }

    public List<SelectItem> getSelectItems() {
        return selectItems;
    }

    public void setSelectItems(List<SelectItem> selectItems) {
        this.selectItems = selectItems;
    }

    public HtmlSelectOneMenu getDropDownMenu() {
        return unitsDropDown;
    }

    public void setDropDownMenu(HtmlSelectOneMenu dropDownMenu) {
        this.unitsDropDown = dropDownMenu;
    }

    public MeasurableFieldControl(String htmlDir, ScreenField measurableField,
            String measurableUnitFldExp, int screenFieldIndex, long mode,
            String ownerScreenExpression, Class entityCls, DDServiceRemote ddService,
            OUser loggedUser, OEntityManagerRemote oEntityManagerRemote,
            OFilterServiceLocal filterService, String backName,
            ExpressionFactory exFactory, ELContext eLContext, FacesContext facesContext,
            UserMessageServiceRemote usrMsgService, EntitySetupServiceRemote entitySetupService) {
        this.measurableField = measurableField;
        this.measurableUnitFldExp = measurableUnitFldExp;
        this.screenFieldIndex = screenFieldIndex;
        this.mode = mode;
        this.entityCls = entityCls;
        this.ddService = ddService;
        this.loggedUser = loggedUser;
        this.backName = backName;
        this.exFactory = exFactory;
        this.elContext = eLContext;
        this.facesContext = facesContext;
        this.oEntityManagerRemote = oEntityManagerRemote;
        this.filterService = filterService;
        this.ownerScreenExpression = ownerScreenExpression;
        this.htmlDir = htmlDir;
        this.usrMsgService = usrMsgService;
        this.entitySetupService = entitySetupService;
    }

    public HtmlPanelGrid createMeasurableControl() {
        boolean screenFieldIsMandatroy = measurableField.isMandatory();
        HtmlPanelGrid grid = new HtmlPanelGrid();
        grid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "measurgrid" + measurableField.getDbid());
        grid.setColumns(2);
        grid.getChildren().add(createMeasurableFieldTextField(screenFieldIsMandatroy));
        grid.getChildren().add(createMeasurableUnitDropDown());
        grid.setDir(htmlDir);
        return grid;
    }

    private UIComponent createMeasurableFieldTextField(boolean screenFieldIsMandatroy) {
        logger.trace("Entering");
        HtmlInputText measurableFldTXT = new HtmlInputText();
        measurableFldTXT.setId(FacesContext.getCurrentInstance().getViewRoot().
                createUniqueId() + "measurableFldTXT" + measurableField.getDbid());
        if (measurableField.getControlSize() == 0) {
            measurableFldTXT.setSize(measurableField.getDd().getControlSize());
        } else {
            measurableFldTXT.setSize(measurableField.getControlSize());
        }
        measurableFldTXT.setImmediate(true);
        measurableFldTXT.setStyleClass("OTMSTextBox");
        measurableFldTXT.setTabindex(Integer.toString(screenFieldIndex));
        if (mode == OScreen.SM_ADD) {
            measurableFldTXT.setValue(measurableField.getDd().getDefaultValue());
        }
        measurableFldTXT.setDisabled(!measurableField.isEditable());
        measurableFldTXT.setValueExpression(
                "value",
                exFactory.createValueExpression(
                        elContext,
                        "#{" + ownerScreenExpression + "." + measurableField.getFieldExpression() + "}",
                        String.class));

        if (screenFieldIsMandatroy) {
            measurableFldTXT.setRequired(screenFieldIsMandatroy);

            HtmlPanelGrid panelGrid = new HtmlPanelGrid();
            panelGrid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "measurableTXTGrid" + measurableField.getDbid());
            panelGrid.setColumns(1);
            panelGrid.getChildren().add(measurableFldTXT);
            UserMessage userMessage = null;
            try {
                userMessage = usrMsgService.getUserMessage("MandatoryField", loggedUser);
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
            HtmlMessage message = new HtmlMessage();
            message.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                    + "measurableTXTMSG" + measurableField.getDbid());
            message.setTitle(userMessage.getMessageTitleTranslated());
            measurableFldTXT.setRequiredMessage(userMessage.getMessageTextTranslated());
            message.setFor(measurableField.getFieldExpression() + "_inputText"
                    + screenFieldIndex);
            panelGrid.getChildren().add(message);
            logger.trace("Returning");
            return panelGrid;
        }
        logger.trace("Returning");
        return measurableFldTXT;
    }

    private HtmlSelectOneMenu createMeasurableUnitDropDown() {
        logger.trace("Entering");
        String fieldDDString = BaseEntity.getFieldDDName(entityCls,
                measurableUnitFldExp.substring(0, measurableUnitFldExp.indexOf(".")));
        DD fieldDD = ddService.getDD(fieldDDString, loggedUser);
        String dropDownDisplayField = fieldDD.getLookupDropDownDisplayField();
        final String dropDownDisplayFieldSimpleClassName
                = dropDownDisplayField.substring(0, dropDownDisplayField.indexOf("."));
        String colName
                = dropDownDisplayField.substring(dropDownDisplayField.indexOf(".") + 1);
        List<String> conditions = new ArrayList<String>();
        conditions.add("entityClassPath LIKE '%." + dropDownDisplayFieldSimpleClassName + "'");
        Class fldClass = null;
        try {
            OEntityDTO fldOEntity = entitySetupService.loadOEntityDTO(
                    dropDownDisplayFieldSimpleClassName, loggedUser);
            fldClass = Class.forName(fldOEntity.getEntityClassPath());
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Returning with Null");
            return null;
        }
        // <editor-fold defaultstate="collapsed" desc="Create & Fill dropdownList">
        dropdownList = new ArrayList();
        try {
            //  the WHERE clause to be used to filter
            //  the query
            List<String> filterConditions = new ArrayList();
            filterConditions = filterService.parseDDLookupFilter(fieldDD.getLookupFilter(), loggedUser);
            List<String> fieldExprsn = new ArrayList<String>();
            fieldExprsn.add(colName);

            //  execute Query
            dropdownList = oEntityManagerRemote.loadEntityList(
                    fldClass.getSimpleName(), filterConditions, fieldExprsn, null, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Returning with Null");
            return null;
        }
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Create & Set Value & Binding dropDownMenu">
        //  Binding...
        //  the name of the column from which the combo box will be filled with 'values'
        unitsDropDown = new HtmlSelectOneMenu();
        //unitsDropDown.setPartialSubmit(true);
        unitsDropDown.setImmediate(true);
        String fieldName = measurableUnitFldExp.substring(0, measurableUnitFldExp.lastIndexOf("."));
        String valueExpression
                = "#{" + ownerScreenExpression + "." + fieldName + "}";
        unitsDropDown.setValueExpression(
                "value",
                exFactory.createValueExpression(
                        facesContext.getELContext(),
                        valueExpression,
                        fldClass)
        );
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Fill selectItems & Bind uiSelectItems">
        unitsDropDown.setId("MeasureDD_" + measurableUnitFldExp.substring(0, measurableUnitFldExp.indexOf(".")));
        selectItems = new ArrayList<SelectItem>();

        //  Add a 'Not Set' option
        SelectItem selectItem = new SelectItem();
        selectItem.setLabel(Lookup.EMPTY_ITEM);
        selectItem.setValue(null);
        selectItem.setNoSelectionOption(true);
        selectItems.add(selectItem);
        for (int itemIndex = 0; itemIndex < dropdownList.size(); itemIndex++) {
            try {
                String currentItemLabel = "";
                currentItemLabel = (String) ((BaseEntity) dropdownList.get(itemIndex)).invokeGetter(colName);
                selectItem = new SelectItem(dropdownList.get(itemIndex), currentItemLabel);
                selectItems.add(selectItem);
            } catch (NoSuchMethodException ex) {
                logger.error("Exception thrown", ex);
            }
        }
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Create uiSelectItems">
        UISelectItems uiSelectItems = new UISelectItems();
        uiSelectItems.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                + "muiSelectItems" + measurableField.getDbid());
        uiSelectItems.setValueExpression(
                "value",
                exFactory.createValueExpression(
                        facesContext.getELContext(),
                        "#{" + backName + ".selectItemsList['"
                        + unitsDropDown.getId()
                        + "']}",
                        Object.class)
        );
        //</editor-fold>

        unitsDropDown.getChildren().add(uiSelectItems);

        // <editor-fold defaultstate="collapsed" desc="dropDownMenu Converters(), needs selectItems">
        final List<SelectItem> SelectItems = selectItems;
        converter = new FABSDropDownConverter(SelectItems);
        unitsDropDown.setConverter(converter);
        //</editor-fold>
        logger.trace("Returning");
        return unitsDropDown;
    }
}
