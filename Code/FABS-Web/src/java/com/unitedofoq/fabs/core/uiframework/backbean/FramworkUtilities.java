/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.inputtextarea.InputTextarea;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nkhalil
 */
public class FramworkUtilities {

    final static org.slf4j.Logger logger = LoggerFactory.getLogger(FramworkUtilities.class);

    static public void setValueInEntity(Object entity, String expression, Object value) {
        setValueInEntity(entity, expression, value, false);
    }

    static public void setValueInEntity(Object entity, String expression, Object value, Boolean setChanged) {
        logger.debug("Entering");
        if (null == entity) {
            logger.debug("Returning with Null");
            return;
        }
        List<ExpressionFieldInfo> infos = BaseEntity.parseFieldExpression(entity.getClass(), expression);

        String actualField = null;
        for (int i = infos.size() - 1; i >= 0; i--) {
            if (infos.get(i).field.getType().isInstance(value)) {
                actualField = infos.get(i).fieldName;
                break;
            }
            logger.debug("Returning");
        }

        String lookupObjExpression = expression;

        if (lookupObjExpression.contains(".") && actualField != null) {
            lookupObjExpression = lookupObjExpression.substring(0, lookupObjExpression.lastIndexOf(actualField) + actualField.length());
        }
        if (lookupObjExpression.contains(".")) {
            String exp = lookupObjExpression.substring(0, lookupObjExpression.indexOf("."));
            lookupObjExpression = lookupObjExpression.substring(lookupObjExpression.lastIndexOf(".") + 1, lookupObjExpression.length());
            Object entityTMP = null;
            try {
                entityTMP = getValueFromEntity(entity, exp);
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
            // Set the First Object in the relation
            // If the expressionis something like that jobapplicant.personalInfo.name, and we need to set JobApplicant
            if (entityTMP != null && (value != null && entityTMP.getClass().getName().equals(value.getClass().getName()))) {
                lookupObjExpression = exp;
            } else if (entityTMP != null) {
                entity = entityTMP;
            }
            /**
             * The following code is commented because when the expression
             * contains more than one dot, it was generating error, because
             * after parsing the expression it was set the value in a wrong
             * entity. Example: ======= set value in entity(EdsEmployee, new
             * Employee) for field expression
             * (edsEmployee.empoyee.personalInfo.name) was leading to set
             * employee in entity employee which is not correct, so commenting
             * the code will set value correctly.
             */

        }
        try {
            // use value.getClass instead of field.getType cause the field my be declared in the superclass.
            ((BaseEntity) entity).invokeSetter(lookupObjExpression, value);

            if (setChanged) {
                ((BaseEntity) entity).setChanged(true);
            }
            logger.debug("Returning");
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
    }

    static Object getValueFromEntity(Object entity, String expression)
            throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        logger.debug("Entering");
        String currentExpression = null;
        if (entity == null) {
            logger.debug("Returning with Null");
            return null;
        }
        boolean leaf;
        if (expression.contains(".")) {
            currentExpression = expression.substring(0, expression.indexOf("."));
            expression = expression.substring(expression.indexOf(".") + 1);
            leaf = false;
        } else {
            currentExpression = expression;
            leaf = true;
        }

        String methodName;

        Field currentField = BaseEntity.getClassField(entity.getClass(), currentExpression);//entity.getClass().getDeclaredField(currentExpression);

        if (currentField != null) {
            if (currentField.getType() == boolean.class) {
                methodName = "is" + Character.toString(currentExpression.charAt(0)).toUpperCase() + currentExpression.substring(1);
            } else {
                methodName = "get" + Character.toString(currentExpression.charAt(0)).toUpperCase() + currentExpression.substring(1);
            }
        } else {
            methodName = "get" + Character.toString(currentExpression.charAt(0)).toUpperCase() + currentExpression.substring(1);
        }

        Object obj = entity.getClass().getMethod(methodName).invoke(entity);
        if (leaf) {
            logger.debug("Returning");
            return obj;
        } else {
            logger.debug("Returning");
            return getValueFromEntity(obj, expression);
        }

    }

    static public boolean isNumber(String str) {
        logger.debug("Entering");
        logger.debug("Returning");
        return str != null && str.matches("\\d*\\.+\\d+|\\d+");
    }

    static public boolean isFloat(String str) {
        logger.debug("Entering");
        //It can't contain only numbers if it's null or empty...
        if (str == null || str.length() == 0) {
            logger.debug("Returning with False");
            return false;
        }
        str = str.trim();
        for (int i = 0; i < str.length(); i++) {
            //If we find a non-digit character we return false.
            if (!Character.isDigit(str.charAt(i)) && !Character.valueOf(str.charAt(i)).equals('.')) {
                logger.debug("Returning with False");
                return false;
            }
        }
        logger.debug("Returning with True");
        return true;
    }

    static public boolean isFieldNumber(String str, Class actOnEntityCls) {
        logger.debug("Entering");
        try {
            Field field = BaseEntity.getClassField(actOnEntityCls, str);
            if (field != null && (field.getType().isAssignableFrom(Long.class)
                    || field.getType().isAssignableFrom(Integer.class)
                    || field.getType().isAssignableFrom(Short.class)
                    || field.getType().isAssignableFrom(Float.class))) {
                logger.debug("Returning with True");
                return true;
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning with False");
        return false;
    }

    static public List<String> parseFilterString(String filterString) {
        logger.debug("Entering");
        if (filterString != null && !filterString.equals("")) {
            //  to hold the parsed conditions
            List<String> conditions = new ArrayList();

            //  while the conditions delimiter still exists in 'filterString'...
            while (filterString.indexOf(";") != -1) {
                int delimiterIndex = filterString.indexOf(";");
                String condition = filterString.substring(0, delimiterIndex);
                conditions.add(condition);
                filterString = filterString.substring(delimiterIndex + 1);
            }

            //  Add the last condition that was in 'filterString'
            conditions.add(filterString);

            //  return the ArrayList containing the parsed conditions
            logger.debug("Returning");
            return conditions;
        } else {
            logger.debug("Returning");
            return new ArrayList();
        }
    }

    static public Object loadEJBClass(String fClassName) {
        logger.debug("Entering");
        Context context;
        Object retObj;
        try {
            context = new InitialContext();
            retObj = context.lookup("java:global/ofoq/" + fClassName);
            logger.debug("Returning");
            return retObj;
        } catch (NamingException ex) {
            logger.error("Exception thrown", ex);
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        }
    }

    static public String getID(ScreenField field, DD screenFieldDD, int screenFieldIndex) {
        String fldExp = field.getFieldExpression();
        if (fldExp.contains(".")) {
            fldExp = fldExp.replace(".", "_");
        }
        return (fldExp + "_"
                + screenFieldDD.getControlType().getValue().replace(" ", "") + "_"
                + screenFieldIndex);
    }

    static public int getObjectIndexInTabularScreen(UIComponent component, FacesContext context) {
        String clientID = component.getClientId(context);
        String notJavaID = clientID.substring(0,
                clientID.lastIndexOf(":" + component.getId()));
        return Integer.parseInt(notJavaID.substring(notJavaID.lastIndexOf(":") + 1));
    }
    // <editor-fold defaultstate="collapsed" desc="Control Field Expression">
    static final String UICOMP_FLD_ATTR_KEY = "screenFieldExpression";

    static public void setControlFldExp(UIComponent uiComp, String fieldExp) {
        uiComp.getAttributes().put(UICOMP_FLD_ATTR_KEY, fieldExp);
    }

    static public String getControlFldExp(UIComponent uiComp) {
        return (String) uiComp.getAttributes().get(UICOMP_FLD_ATTR_KEY);
    }

    /**
     *
     * @param vce
     * @return
     */
    static public Boolean isComponentValueChanged(ValueChangeEvent vce) {
        logger.debug("Entering");
        Object oldValueObj = vce.getOldValue();
        Object newValueObj = vce.getNewValue();
        Boolean oldValueObjIsNull = (oldValueObj == null ? true : false);
        Boolean newValueObjIsNull = (newValueObj == null ? true : false);

        if (oldValueObjIsNull && newValueObjIsNull) {
            // Both old & new values are null
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.debug("Both old & new values are null, Value Not Changed");
            logger.debug("Returning with False");
            return false;
        }
        if (oldValueObjIsNull
                && (vce.getComponent() instanceof SelectOneMenu
                || vce.getComponent() instanceof org.primefaces.component.calendar.Calendar)) {
            // Old value is different than the new value, as change of both HtmlSelectOneMenu
            // and SelectInputDate will set new value
            logger.debug("Value Changed from: {} To : {}", oldValueObj, newValueObj);
            logger.debug("Returning with true");
            return true;
        }
        if (!oldValueObjIsNull && "".toString().equals(oldValueObj.toString())
                && newValueObjIsNull) {
            logger.debug("Value Changed from: {} To : {}", oldValueObj, newValueObj);
            logger.debug("Returning with False");
            return false;
        }
        if ((oldValueObjIsNull && !newValueObjIsNull)
                || (!oldValueObjIsNull && newValueObjIsNull)) {
            logger.debug("Value Changed from: {} To : {}", oldValueObj, newValueObj);
            logger.debug("Returning with True");
            return true;
        }

        // Niether old nor new values are null
        // Get the values and compare them
        // Compare Boolean CheckBox if so
        if (vce.getComponent() instanceof SelectBooleanCheckbox) {
            // Field is boolean (check box)
            // Get boolean values and check
            boolean oldValue = Boolean.parseBoolean(oldValueObj.toString());
            boolean newValue = Boolean.parseBoolean(newValueObj.toString());
            if (oldValue == newValue) {
                logger.debug("Value Changed from: {} To : {}", oldValueObj, newValueObj);
                logger.debug("Returning with False");
                return false;
            } else {
                logger.debug("Value Changed from: {} To : {}", oldValueObj, newValueObj);
                logger.debug("Returning with True");
                return true;
            }
        }

        // Compare Other Components
        if (vce.getComponent() instanceof InputText
                || vce.getComponent() instanceof SelectOneMenu
                || vce.getComponent() instanceof org.primefaces.component.calendar.Calendar
                || vce.getComponent() instanceof InputTextarea) {
            if (oldValueObj.toString().equals(newValueObj.toString())) {
                logger.debug("Value Changed from: {} To : {}", oldValueObj, newValueObj);
                logger.debug("Returning with False");
                return false;
            } else {
                logger.debug("Value Changed from: {} To : {}", oldValueObj, newValueObj);
                logger.debug("Returning with True");
                return true;
            }
        }
        logger.debug("Value Changed from: {} To : {}", oldValueObj, newValueObj);
        logger.debug("Returning with True");
        return true;
    }

    /**
     * Returns the count of occurances of 'searchFor' string in 'base' string
     */
    public static int countOccurancesOf(String base, String searchFor) {
        int len = searchFor.length();
        int result = 0;
        if (len > 0) {
            int start = base.indexOf(searchFor);
            while (start != -1) {
                result++;
                start = base.indexOf(searchFor, start + len);
            }
        }
        return result;
    }
}
