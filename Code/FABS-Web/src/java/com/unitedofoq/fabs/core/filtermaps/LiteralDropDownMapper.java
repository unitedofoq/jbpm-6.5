/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.filtermaps;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import javax.ejb.EJB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.unitedofoq.fabs.core.uiframework.backbean.SessionManager;
import java.util.List;
/**
 *
 * @author mostafan
 */
public class LiteralDropDownMapper implements ValueMapper{
    private Field field = null;
    final static Logger logger = LoggerFactory.getLogger(LiteralDropDownMapper.class);
    private DDServiceRemote ddService;
    private OUser loggedUser;        
    public LiteralDropDownMapper(){
        loggedUser = (OUser)SessionManager.getSessionAttribute("loggedUser");
        
    
    }
    
    public void setDDService(DDServiceRemote ddService){
        this.ddService = ddService;
    }
    private DD getDDFromField(Field field){
        try {
                
                java.lang.reflect.Method method = field.getDeclaringClass().getMethod("get"+field.getName().substring(0, 1).toUpperCase()+field.getName().substring(1)+"DD");
                return ddService.getDD((String)(method.invoke(field.getDeclaringClass().newInstance())), loggedUser);
            } catch (InstantiationException ex) {
                logger.error("Exception thrown", ex);            
            } catch (IllegalAccessException ex) {
                logger.error("Exception thrown", ex);            
            } catch (NoSuchMethodException ex) {
                logger.error("Exception thrown", ex);            
            } catch (SecurityException ex) {
                logger.error("Exception thrown", ex);            
            } catch (IllegalArgumentException ex) {
                logger.error("Exception thrown", ex);            
            } catch (InvocationTargetException ex) {
                logger.error("Exception thrown", ex);            
            }
            
        
        return null;
    }

    @Override
    public List<String> mapValue(String value) {
        ArrayList<String> mappedValues = new ArrayList<String>();
         String [] lookupTokens = this.getDDFromField(field).getLookupFilter().split(";");
            //ArrayList<String[]> lookupMap = new ArrayList<String[]>();//each array will have two elements only
            for(int i = 0 ; i < lookupTokens.length ; i++){
                String[] lookupToken = lookupTokens[i].split(",");//lookupMap.add();
                if(lookupToken[0].toLowerCase().startsWith(value.toLowerCase())){
                    mappedValues.add(lookupToken[1]);
                }
            }
            for(int i = 0 ; i < lookupTokens.length ; i++){
                String[] lookupToken = lookupTokens[i].split(",");//lookupMap.add();
                if(lookupToken[0].contains(value.toLowerCase())){
                    mappedValues.add(lookupToken[1]);
                }
            }
            return mappedValues;
    }

    @Override
    public boolean isValidMapperForField(Field field) {
        this.field = field;
        return field.getType().equals(String.class)&& this.getDDFromField(field) != null && this.getDDFromField(field).getControlType().getValue().equals("Literal Drop-down");    }
    
    
  }
