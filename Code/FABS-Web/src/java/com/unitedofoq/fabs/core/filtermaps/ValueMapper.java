/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.filtermaps;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author moatafan
 */
public interface ValueMapper {
    public List<String> mapValue(String value);
    public boolean isValidMapperForField(Field field);
}
