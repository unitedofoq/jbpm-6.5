///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
package com.unitedofoq.fabs.core.uiframework.backbean;
//
//

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;

import com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenInputDTO;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import java.util.Calendar;
import java.util.List;

import javax.el.MethodExpression;
import javax.el.ValueExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import org.primefaces.component.column.Column;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.tree.Tree;
import org.primefaces.component.tree.UITreeNode;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
//
///**
// *
// * @author nkhalil
// */
@ManagedBean(name = "TreeBrowserBean")
@ViewScoped
public class TreeBrowserBean extends SingleEntityBean {

    @Override
    public void init() {
        super.init();
        passedEntity = (OEntityDTO) dataMessage.getData().get(0);
    }
    private OEntityDTO passedEntity;
    private TreeBrowser treeBrowser;

    public void refreshNodeActionListener() {
        try {
        } catch (Exception e) {
            UserMessage errorMsg = usrMsgService.getUserMessage("SystemInternalError", systemUser);
            errorMsg.setException(e);
            messagesPanel.clearAndDisplayMessages(errorMsg);
        }
    }

    public void nodeActionListener(NodeSelectEvent event) {
        try {
            treeBrowser.nodeActionListener(event);
        } catch (Exception e) {
            UserMessage errorMsg = usrMsgService.getUserMessage("SystemInternalError", systemUser);
            errorMsg.setException(e);
            messagesPanel.clearAndDisplayMessages(errorMsg);
        }
    }

    public void nodeExpandListener(NodeExpandEvent event) {
        try {
            treeBrowser.nodeExpandListener(event);
        } catch (Exception e) {
            UserMessage errorMsg = usrMsgService.getUserMessage("SystemInternalError", systemUser);
            errorMsg.setException(e);
            messagesPanel.clearAndDisplayMessages(errorMsg);
        }
    }

    @Override
    protected HtmlPanelGrid buildInnerScreen() {
        logger.trace("Entering");
        Long timeBeforeEM = (Calendar.getInstance()).getTimeInMillis();
        htmlPanelGrid = new HtmlPanelGrid();
        processInput();
        if (isScreenOpenedForLookup()) {
            htmlPanelGrid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_lkpTreePanel");
            htmlPanelGrid.getChildren().add(buildLookupBtnPanel("returnTreeSelectedExpression"));
            entityFieldsTreeBrowser = treeBrowser;

        }

        Tree tree = new Tree();
        tree.setVar("node");
        tree.setDynamic(true);
        tree.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "mytree");
        //Set the value
        tree.setValueExpression("value",
                exFactory.createValueExpression(elContext, "#{" + getBeanName() + ".filedsTreeRoot}", DefaultTreeNode.class));

        //Set Node Expand Event
        Class[] expandEvent = new Class[]{NodeExpandEvent.class};
        MethodExpression expandMB = exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                "#{" + getBeanName() + ".nodeExpandListener}", null, expandEvent);

        FABSAjaxBehavior expandBehavior = new FABSAjaxBehavior();
        expandBehavior.setListener(expandMB);
        tree.addClientBehavior("expand", expandBehavior);

        //Set Node Selection Event
        Class[] selectionEvent = new Class[]{NodeSelectEvent.class};
        MethodExpression selectMB = exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                "#{" + getBeanName() + ".nodeActionListener}", null, selectionEvent);
        FABSAjaxBehavior selectionBehavior = new FABSAjaxBehavior();
        selectionBehavior.setListener(selectMB);
        tree.addClientBehavior("select", selectionBehavior);

        //Selection Properties
        ValueExpression selectedNodeVe = exFactory.
                createValueExpression(elContext, "#{" + getBeanName() + ".dynamicSelectedNode}", DefaultTreeNode.class);
        tree.setSelectionMode("single");
        tree.setValueExpression("selection", selectedNodeVe);

        /**
         * ***********************************************
         * Define Node properties
         * ***********************************************
         */
        UITreeNode treeNodeFolder = new UITreeNode();

        treeNodeFolder.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                + "_nodeFolder");
        ValueExpression nodeFieldName = exFactory.
                createValueExpression(elContext, "#{node.nodeField}", String.class);
        ValueExpression nodeFieldStyle = exFactory.
                createValueExpression(elContext, "#{node.customStyleClass}", String.class);

        treeNodeFolder.setExpandedIcon("tree_node_folder_expand");
        treeNodeFolder.setCollapsedIcon("tree_node_folder_collaps");
        treeNodeFolder.setType("folder");

        /**
         * ***********************************************
         * Define the display label
         * **************2*********************************
         */
        HtmlOutputText nodeText = new HtmlOutputText();
        nodeText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                + "_nodeTxt");
        nodeText.setValueExpression("value", nodeFieldName);
        nodeText.setValueExpression("styleClass", nodeFieldStyle);
        treeNodeFolder.getChildren().add(nodeText);

        tree.getChildren().add(treeNodeFolder);

        /* Prepare description section*/
        HtmlPanelGrid descriptionGrid = new HtmlPanelGrid();

        descriptionGrid.setId("descriptionGrid");
        DataTable selectedNodeProperties = new DataTable();
        selectedNodeProperties.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "selectedNodeP");
        selectedNodeProperties.setVar("currentRow");
        selectedNodeProperties.setValueExpression("value",
                exFactory.createValueExpression(elContext, "#{node.nodeProperties}", List.class));

        Column property = new Column();
        property.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "propCol");
        HtmlOutputLabel propertyLBL = new HtmlOutputLabel();
        propertyLBL.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "propLBL");
        propertyLBL.setValueExpression("value",
                exFactory.createValueExpression(elContext, "#{currentRow.property}", String.class));
        property.getChildren().add(propertyLBL);

        Column value = new Column();
        value.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "valCol");
        HtmlOutputLabel valueLBL = new HtmlOutputLabel();
        valueLBL.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "valLBL");
        valueLBL.setValueExpression("value",
                exFactory.createValueExpression(elContext, "#{currentRow.value}", String.class));
        value.getChildren().add(valueLBL);

        selectedNodeProperties.getChildren().add(property);
        selectedNodeProperties.getChildren().add(value);

        descriptionGrid.getChildren().add(selectedNodeProperties);

        /* return generated HtmlPanelGrid */
        htmlPanelGrid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_TreePanel");
        htmlPanelGrid.getChildren().add(tree);
        onloadJSStr += "top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');";
        logger.trace("Returning");
        return htmlPanelGrid;
    }
    @Override
    protected void buildScreenExpression(UIComponent component) {
    }

    @Override
    protected String getBeanName() {
        return TreeBrowserBean.class.getSimpleName();
    }

    @Override
    public ODataMessage saveProcessRecords() {
        return null;
    }

    @Override
    protected BaseEntity getObjectForLookup(long dbid) {
        return null;
    }

    @Override
    public void loadAttribute(String expression) {
    }
    private DefaultTreeNode filedsTreeRoot;

    @Override
    protected boolean processInput() {
        logger.trace("Entering");
        if (!super.processInput()) {
            logger.trace("Returning with False");
            return false;
        }

        String rootClassName;
        try {
            OEntityDTO entity = entitySetupService.loadOEntityDTOForTree(passedEntity.getEntityClassName(), loggedUser);
            rootClassName = entity.getEntityClassPath();
            Class cls = Class.forName(rootClassName);
            treeBrowser = new TreeBrowser(cls, loggedUser, exFactory,
                    FacesContext.getCurrentInstance().getELContext(), this, entitySetupService, ddService,
                    oem);
            setFiledsTreeRoot(new DefaultTreeNode("folder", "Root", null));
            treeBrowser.createRootNode(getFiledsTreeRoot());
            getFiledsTreeRoot().setExpanded(true);
            treeBrowser.addChildren();
            setDynamicSelectedNode(treeBrowser.getRoot());
            logger.trace("Returning with True");
            return true;

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            logger.trace("Returning with False");
            return false;
        }
    }

    @Override
    protected List<ScreenInputDTO> getValidScreenInputs() {
        logger.trace("Entering");
        List<ScreenInputDTO> screenInputs = null;
        try {
            screenInputs = super.getValidScreenInputs();

            // If it's lookup, then support VoidDT Input
            if (isScreenOpenedForLookup()) {
                // Add VoidDT as a natively supported input
                ScreenInputDTO voidInput = uiFrameWorkFacade.
                        getVoidAsScreenInput(screenInputs, loggedUser);
                if (voidInput != null) // Not found in screenInputs and got successfully
                // Support it
                {
                    screenInputs.add(voidInput);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        logger.trace("Returning");
        return screenInputs;
    }
    
    @Override
    protected List<BaseEntity> getLoadedEntity() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * @return the filedsTreeRoot
     */
    public DefaultTreeNode getFiledsTreeRoot() {
        return filedsTreeRoot;
    }

    /**
     * @param filedsTreeRoot the filedsTreeRoot to set
     */
    public void setFiledsTreeRoot(DefaultTreeNode filedsTreeRoot) {
        this.filedsTreeRoot = filedsTreeRoot;
    }

    @Override
    public void addClaimButtonToPanelGroup(String buttonID, String buttonLabel, String actionMethodName, HtmlPanelGrid panelLayout, Class[] params, String portletInsId, String beanName, String screenLanguage, boolean invalidInputMode, OScreen currentScreen) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
