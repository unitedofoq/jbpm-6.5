/*
 * OutboxPage.java
 *
 * Created on Dec 25, 2008, 5:51:38 PM
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.dd.DD;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.Outbox;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import com.unitedofoq.fabs.core.uiframework.screen.FormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenFunction;
import com.unitedofoq.fabs.core.uiframework.screen.TabularScreen;

import java.util.*;
import javax.annotation.PostConstruct;
import javax.el.MethodExpression;
import javax.el.ValueExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;
import org.primefaces.component.column.Column;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.commandlink.CommandLink;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.graphicimage.GraphicImage;
import org.primefaces.component.panel.Panel;
import org.primefaces.context.RequestContext;

/**
 * <p>Page bean that corresponds to a similarly named JSP page.  This
 * class contains component definitions (and initialization code) for
 * all components that you have defined on this page, as well as
 * lifecycle methods and event handlers where you may add behavior
 * to respond to incoming events.</p>
 *
 * @author mmohamed
 */
@ManagedBean(name = "OutboxPage")
@ViewScoped
public class OutboxPage extends TabularBean {

    private List listProvider = new ArrayList();

    private String selectedTask;

    public OutboxPage() {
    }

    public String getSelectedTask() {
        return selectedTask;
    }

    public void setSelectedTask(String selectedTask) {
        this.selectedTask = selectedTask;
    }

    @Override
    public boolean onInit(Map<String, String> requestParams) {
        logger.trace("Entering");
        try {
            if (!super.onInit(requestParams)) {
                logger.trace("Returning with False");
                return false;
            }
            refresh_action(null);
            return  true;
        }catch (Exception e){
            logger.trace("Exception Initializing Outbox", e);
        }
        return false;
    }

    @Override
    protected String getBeanName() {
        return "OutboxPage";
    }

    public void filltable() {
        // <editor-fold defaultstate="collapsed" desc="Log">
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>
        listProvider.clear();
        listProvider.addAll(processService.getUserStartedProcesses(loggedUser,((TabularScreen) getCurrentScreen()).getSortExpression()));
    }

    public void refresh_action(ActionEvent ae) {
        filltable();
        if (table != null) {
            RequestContext.getCurrentInstance().update(table.getClientId());
        }
    }
    private String refreshOutbox_TXT = "Refresh";

    public String getRefreshOutbox_TXT() {
        return "Refresh";
    }

    public void setRefreshOutbox_TXT(String refreshOutbox_TXT) {
        this.refreshOutbox_TXT = refreshOutbox_TXT;
    }

    public List getListProvider() {
        return listProvider;
    }

    public void setListProvider(List listProvider) {
        this.listProvider = listProvider;
    }
    private boolean refreshOutbox;

    public boolean isRefreshOutbox() {
        System.out.println("Refreshing Outbox");
        refresh_action(null);
        return refreshOutbox;
    }

    public void setRefreshOutbox(boolean refreshOutbox) {
        this.refreshOutbox = refreshOutbox;
    }
    private DataTable table;

    public DataTable getTable() {
        return table;
    }

    public void setTable(DataTable table) {
        this.table = table;
    }
    // <editor-fold defaultstate="collapsed" desc="Build Outbox Panel">
    private HtmlPanelGrid outboxPanel;

    public HtmlPanelGrid getOutboxPanel() {
        HtmlPanelGrid OutboxPanel = new HtmlPanelGrid();
        htmlPanelGrid = buildInnerScreen();
        outboxPanel = htmlPanelGrid;
        outboxPanel.setDir(htmlDir);
        return outboxPanel;
    }

    public void setOutboxPanel(HtmlPanelGrid outboxPanel) {
        this.outboxPanel = outboxPanel;
    }
    // </editor-fold>
    int pageSize = 10;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public HtmlPanelGrid buildInnerScreen() {
        HtmlPanelGrid grid = new HtmlPanelGrid();
        grid.setColumns(1);
        grid.setWidth("100%");
        if (messagesPanel == null) {
            messagesPanel = new UserMessagePanel(exFactory, elContext, getBeanName(), ddService, this, loggedUser);
        }
        grid.getChildren().add(messagesPanel.createMessagePanel());
        List<ScreenField> screenFields = getCurrentScreen().getScreenFields();
        Panel middlePanel = new Panel();
        middlePanel.setStyle("position:relative");

        // <editor-fold defaultstate="collapsed" desc="GetScreenFields & Build Table">
        table = new DataTable();
        table.setStyle("position:relative; min-width:600px");
        table.setId(getCurrentScreen().getName() + "_inboxTable");
        table.setRows(pageSize);//For Paginator
        table.setValueExpression("value",
                exFactory.createValueExpression(elContext, "#{" + getBeanName() + ".listProvider}", ArrayList.class));
        table.setValueExpression("sortAscending",
                exFactory.createValueExpression(elContext, "#{" + getBeanName() + ".descending}", Boolean.class));
        table.setVar("currentRow");
        DD inboxNoResocrdsMsgDD = ddService.getDD("inboxNoResocrdsMsg", loggedUser);
        table.setEmptyMessage(inboxNoResocrdsMsgDD.getLabelTranslated());
        table.setPaginatorAlwaysVisible(false);


        Column refreshColumn = new Column();
        refreshColumn.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "refershColumn");
        CommandButton refreshActionButton = new CommandButton();
        refreshActionButton.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "refershIcon");

        refreshActionButton.setStyleClass("refresh_btn");
        refreshActionButton.setTitle(ddService.getDDLabel("refreshButton", loggedUser));
        Class[] parms = {ActionEvent.class};

        MethodExpression refreshMethodExpression = exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                "#{" + getBeanName() + ".refresh_action}", null, parms);
        MethodExpressionActionListener refreshActionListener = new MethodExpressionActionListener(refreshMethodExpression);

        refreshActionButton.addActionListener(refreshActionListener);
        refreshColumn.setHeader(refreshActionButton);
        table.getChildren().add(refreshColumn);
        MethodExpression taskActionListenerMethodExpression = exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                "#{" + getBeanName() + ".taskActionListener}", null, parms);

        MethodExpressionActionListener taskActionListener = new MethodExpressionActionListener(taskActionListenerMethodExpression);

        for (int scrFldExpIndx = 0; scrFldExpIndx < screenFields.size(); scrFldExpIndx++) {

            ScreenField screenField = (ScreenField) screenFields.get(scrFldExpIndx);
            Column column = new Column();
            column.setId("column" + scrFldExpIndx);

            ValueExpression valueExpression = exFactory.createValueExpression(
                    elContext, "#{currentRow." + screenField.getFieldExpression() + "}",
                    String.class);
            column.setValueExpression("sortBy", valueExpression);
            column.setValueExpression("filterBy", valueExpression);
            column.setFilterMatchMode("contains");

            DD headerDD = screenField.getDd();
            try {
                headerDD = (DD) oem.loadEntity(DD.class.getSimpleName(), headerDD.getDbid(), null, null, loggedUser);
            } catch (Exception ex) {
                logger.error("Exception thrown",ex);
            }

            if (screenFields.get(scrFldExpIndx).getDdTitleOverride() == null || screenFields.get(scrFldExpIndx).getDdTitleOverride().equals("")) {
                column.setHeaderText(headerDD.getHeaderTranslated());
            } else {
                column.setHeaderText(screenFields.get(scrFldExpIndx).getDdTitleOverride());
            }

            if ("state".equals(screenField.getFieldExpression())) {
                GraphicImage graphicImage = new GraphicImage();
                graphicImage.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "state");
                graphicImage.setStyleClass("inbox-state-icon");
                graphicImage.setValueExpression("url",
                        exFactory.createValueExpression(elContext, "/resources/process_state/#{currentRow['state']}.gif", String.class));
                column.getChildren().add(graphicImage);
            } else if ("title".equals(screenField.getFieldExpression())) {
                CommandButton commandButton = new CommandButton();
                commandButton.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_TD_Link");
                commandButton.setType("image");
                commandButton.setValueExpression("title",
                        exFactory.createValueExpression(elContext, "TD:#{currentRow['title']}", String.class));
                commandButton.setStyleClass("info_btn");
                commandButton.setValueExpression("rendered",
                        exFactory.createValueExpression(elContext, "#{currentRow['process']}", String.class));
                commandButton.addActionListener(taskActionListener);

                CommandLink commandLink = new CommandLink();
                commandLink.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_Task_Link");
                commandLink.setValueExpression("value",
                        exFactory.createValueExpression(elContext, "#{currentRow['title']}", String.class));
                commandLink.setValueExpression("title",
                        exFactory.createValueExpression(elContext, "#{currentRow['title']}", String.class));
                commandLink.addActionListener(taskActionListener);
                column.getChildren().add(commandButton);
                column.getChildren().add(commandLink);
            } else {
                // <editor-fold defaultstate="collapsed" desc="Add Static Text Field">

                HtmlOutputText outputText = new HtmlOutputText();
                if (screenField.getFieldExpression().contains(".")) {
                    String exp = screenField.getFieldExpression().substring(
                            screenField.getFieldExpression().lastIndexOf(".")
                            + 1);
                    outputText.setId(exp + "_outputText" + scrFldExpIndx);
                } else {
                    outputText.setId(screenField.getFieldExpression() + "_outputText" + scrFldExpIndx);
                }
                if (screenField.getFieldExpression().contains(".")) {
                    System.out.println("Screen Field Expression : " + screenField.getFieldExpression());
                    String expression1 = screenField.getFieldExpression().substring(0, screenField.getFieldExpression().indexOf("."));
                    String expression2 = screenField.getFieldExpression().substring(screenField.getFieldExpression().indexOf("."));
                    System.out.println("THE EXPRESSION ISSSSSSSS " + "#{currentRow['" + expression1 + "']" + expression2 + "}");
                    outputText.setValueExpression("value",
                            exFactory.createValueExpression(elContext,
                            "#{currentRow['" + expression1 + "']" + expression2 + "}", String.class));
                } else {
                    outputText.setValueExpression("value",
                            exFactory.createValueExpression(elContext, "#{currentRow['" + screenField.getFieldExpression() + "']}", String.class));
                }

                outputText.setStyleClass("OTMSTextBox");
                outputText.setStyle("text-align: left; border: none; background-color: inherit;");
                column.getChildren().add(outputText);
                //</editor-fold>
            }
            table.getChildren().add(column);
        }

        //</editor-fold>

        //For Paginator
        table.setPaginatorPosition("bottom");
        table.setPaginator(true);

        table.setValueExpression("binding",
                exFactory.createValueExpression(elContext, "#{" + getBeanName() + ".table}", DataTable.class));

        table.setPaginatorTemplate("{CurrentPageReport}  {FirstPageLink} {PreviousPageLink} {PageLinks} "
                + "{NextPageLink} {LastPageLink} {RowsPerPageDropdown}");
        middlePanel.getChildren().add(table);
        grid.getChildren().add(middlePanel);
        return grid;
    }
    public FormScreen getFormScreen(Outbox currentRowOut,OUser user) throws UserNotAuthorizedException{
        
        FormScreen screen =  (FormScreen) oem.loadEntity(FormScreen.class.getSimpleName(),
                        new ArrayList<String>(Arrays.asList("oactOnEntity.entityClassPath like '%."
                        + currentRowOut.getEntityName() + "'",
                        "outbox=1")),
                        null, user);
        if(screen == null){
            screen = (FormScreen) oem.loadEntity(FormScreen.class.getSimpleName(),
                        new ArrayList<String>(Arrays.asList("oactOnEntity.entityClassPath like '%."
                        + currentRowOut.getEntityName() + "'",
                        "basicDetail=1")),
                        null, user);
        }
        return screen;
    }
    public void taskActionListener(ActionEvent ae) {
        logger.debug("Entering");
        UIComponent contolComponent = ae.getComponent();

        String compFieldExp = FramworkUtilities.getControlFldExp(contolComponent);
        String clientID = contolComponent.getClientId(getContext());
        String notJavaID = clientID.substring(0,
                clientID.lastIndexOf(":" + contolComponent.getId()));
        int currentRow = (Integer.parseInt(notJavaID.substring(notJavaID.lastIndexOf(":") + 1))) % pageSize;
        
        
        if (currentRow >= ((ArrayList<Outbox>)table.getValue()).size()) {
            logger.warn("Current Row index is greater than the loaded List Size");
            logger.debug("Returning");
            return;
        }

        OScreen screen = null;

        int currentPageNo = table.getPage();

        Outbox currentRowOut = ((Outbox) ((ArrayList<Outbox>)table.getValue()).get((currentPageNo * pageSize) + currentRow));
        
        String requesterLoginName =  currentRowOut.getInitiatorLoginName();
               long reqTenantID = 0;
               if(requesterLoginName!=null && !requesterLoginName.equals("")){
                    reqTenantID = centralOEM.getUserTenant(requesterLoginName).getId(); 
               }
               
               if(reqTenantID != loggedUser.getTenant().getId() && !corporate){
                   logger.debug("Returning");
                   return;
               }
               
        if (currentRowOut.getFunctionDBID() > 0) {
            try {
                screen = ((ScreenFunction) oem.loadEntity(ScreenFunction.class.getSimpleName(),
                        currentRowOut.getFunctionDBID(),
                        null, null, loggedUser)).getOscreen();
            } catch (Exception ex) {
                logger.error("Exception thrown",ex);
            }

        } else {
            try {
                OUser systemuUser = oem.getSystemUser(loggedUser);
                if(corporate){
                  systemuUser.setWorkingTenantID(reqTenantID);  
                }
                
                screen = getFormScreen(currentRowOut, systemuUser);
                
                if(screen == null){
                    systemuUser.setWorkingTenantID(0);
                    screen = getFormScreen(currentRowOut, systemuUser);
                }
                
            } catch (Exception ex) {
                logger.error("Exception thrown",ex);
            }
        }

        if (screen == null) {
            logger.warn("Cannot Open Screen From OutBox; No Screen Found");
            logger.debug("Returning");
            return;
        }

        ODataMessage message = new ODataMessage();
        ODataType oDataType = dataTypeService.loadODataType("DBID", loggedUser);
        message.setODataType(oDataType);
        List data = new ArrayList();
        data.add(Long.valueOf(currentRowOut.getEntityDBID()));
        message.setData(data);

        String scrInstId = generateScreenInstId(screen.getName());

        SessionManager.setScreenSessionAttribute(
                scrInstId, MODE_VAR, OScreen.SM_VIEW);

        SessionManager.setScreenSessionAttribute(
                scrInstId, OPortalUtil.MESSAGE, message);

        SessionManager.setScreenSessionAttribute(
                scrInstId, OBackBean.WIZARD_VAR, false);
        
        SessionManager.setScreenSessionAttribute(
                scrInstId, OUTBOX_PROCESS_INS, currentRowOut.getProcessInstanceId());
        
        SessionManager.setScreenSessionAttribute(
                scrInstId, OUTBOX_TASK_INS, currentRowOut.getTaskID());
        
        SessionManager.setScreenSessionAttribute(
                scrInstId, Requester_Tenant_ID, reqTenantID);
                
       openScreen(screen.getName(), screen.getViewPage(), screen.getHeaderTranslated(), scrInstId, false);

    }
}
