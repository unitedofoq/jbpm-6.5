///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
package com.unitedofoq.fabs.core.uiframework.backbean;
//
//

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO;
import com.unitedofoq.fabs.core.uiframework.screen.MultiLevelScreen;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenInputDTO;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import java.util.Calendar;
import java.util.List;
import javax.el.MethodExpression;
import javax.el.ValueExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import org.primefaces.component.contextmenu.ContextMenu;
import org.primefaces.component.menuitem.MenuItem;
import org.primefaces.component.tree.Tree;
import org.primefaces.component.tree.UITreeNode;
import org.primefaces.context.RequestContext;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import javax.swing.tree.DefaultMutableTreeNode;
//import javax.swing.tree.DefaultTreeModel;
//
///**
// *
// * @author nkhalil
// */n
@ManagedBean(name = "MultiLevelBean")
@ViewScoped
public class MultiLevelBean extends SingleEntityBean {
    final static Logger logger = LoggerFactory.getLogger(MultiLevelBean.class);
    public LevelsTree getLevelsTree() {
        return levelsTree;
    }

    public void setLevelsTree(LevelsTree levelsTree) {
        this.levelsTree = levelsTree;
    }  
    
    private LevelsTree levelsTree;
    private List<String> conditions ;
    public void nodeExpandListener(NodeExpandEvent event) {
            int count=count(liferayUsers.get(loggedUser.getLoginName()));
        if(count==0){
            logout();
            return  ;
        }
        try {
            levelsTree.nodeExpandListener(event);       
            RequestContext.getCurrentInstance().execute("top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');");
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            UserMessage errorMsg = usrMsgService.getUserMessage("SystemInternalError", systemUser);
            errorMsg.setException(ex);
            messagesPanel.clearAndDisplayMessages(errorMsg);
        }
    }

    public void editNodeActionListener() {
        try {
            levelsTree.editNodeActionListener(selectedNodes[0]);
        } catch (Exception e) {
            UserMessage errorMsg = usrMsgService.getUserMessage("SystemInternalError", systemUser);
            errorMsg.setException(e);
            messagesPanel.clearAndDisplayMessages(errorMsg);
        }
    }

    public void editNodeInfoActionMenuListener() {
        try {
            //levelsTree.editNodeInfoActionMenuListener(event);//DataSaved            
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            UserMessage errorMsg = usrMsgService.getUserMessage("SystemInternalError", systemUser);
            errorMsg.setException(ex);
            messagesPanel.clearAndDisplayMessages(errorMsg);
        }
    }

    public void newNodeActionMenuListener() {
        try {
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            UserMessage errorMsg = usrMsgService.getUserMessage("SystemInternalError", systemUser);
            errorMsg.setException(ex);
            messagesPanel.clearAndDisplayMessages(errorMsg);
        }
    }

    public void newNodeActionListener() {
        try {
            levelsTree.newNodeActionListener(selectedNodes[0]);
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            UserMessage errorMsg = usrMsgService.getUserMessage("SystemInternalError", systemUser);
            errorMsg.setException(ex);
            messagesPanel.clearAndDisplayMessages(errorMsg);
        }
    }
     public void newChildNodeActionListener() {
         try {
            UserMessage successMsg = usrMsgService.getUserMessage("DataSaved", systemUser);
            messagesPanel.clearAndDisplayMessages(successMsg);
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            UserMessage errorMsg = usrMsgService.getUserMessage("SystemInternalError", systemUser);
            errorMsg.setException(ex);
            messagesPanel.clearAndDisplayMessages(errorMsg);
        }
     }

    public void refreshNodeActionListener() {
        try {
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            UserMessage errorMsg = usrMsgService.getUserMessage("SystemInternalError", systemUser);
            errorMsg.setException(ex);
            messagesPanel.clearAndDisplayMessages(errorMsg);
        }
    }

    public void deleteNodeActionListener() {
        try {
            levelsTree.deleteNodeActionListener(selectedNodes[0]);
            UserMessage deleteMsg = usrMsgService.getUserMessage("DataRemoved", systemUser);
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            UserMessage errorMsg = usrMsgService.getUserMessage("SystemInternalError", systemUser);
            errorMsg.setException(ex);
        }
    } 
    
    public void editNodeInfoActionListener() {
        try {
            UserMessage successMsg = usrMsgService.getUserMessage("DataSaved", systemUser);
            successMsg.setMessageType(UserMessage.TYPE_SUCCESS);
            messagesPanel.addMessages(successMsg);

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            UserMessage errorMsg = usrMsgService.getUserMessage("SystemInternalError", systemUser);
            errorMsg.setException(ex);
            messagesPanel.clearAndDisplayMessages(errorMsg);
        }
    }
    
    private LevelNode root;

    public LevelNode getRoot() {
        return root;
    }

    public void setRoot(LevelNode root) {
        this.root = root;
    }

    @Override
    public void nodeActionListener(NodeSelectEvent event) {
        try {
            levelsTree.nodeActionListener(event);
        } catch (Exception e) {
            UserMessage errorMsg = usrMsgService.getUserMessage("SystemInternalError", systemUser);
            errorMsg.setException(e);
            messagesPanel.clearAndDisplayMessages(errorMsg);
        }
    }
    
    @Override
    protected HtmlPanelGrid buildInnerScreen() 
    {
        Long timeBeforeEM = (Calendar.getInstance()).getTimeInMillis() ;
        htmlPanelGrid = new HtmlPanelGrid();   
        messagesPanel = new UserMessagePanel(exFactory, elContext, getBeanName(), ddService, this, loggedUser);
        htmlPanelGrid.getChildren().add(messagesPanel.createMessagePanel());
        try {
            if (isScreenOpenedForLookup()) {
              try {
                    List<String> lookupConditions = (List<String>) SessionManager.getSessionAttribute("lookupConditions");
                    if (lookupConditions != null)
                        conditions = lookupConditions;
                    else {
                    // Error    
                        logger.warn("lookupConditions Not Passed In Session, It's Null");
                    }
                } catch (Exception ex) {
                    logger.error("Exception thrown",ex);
                    messagesPanel.addErrorMessage(
                            usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
                }
            }
            validateInputSetMapping();          
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            messagesPanel.addErrorMessage(
                    usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
        }
        /*************************************************
         * Define tree context menu
         *************************************************/
        ContextMenu menu = new ContextMenu();
        menu.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()+
                "_TreeMenu");
        //New
        menu.setStyleClass("main_context_menu");
        MenuItem newItem = new MenuItem();
        newItem.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_newItem");
        newItem.setValue(ddService.getDD("newItem", loggedUser).getLabelTranslated());
        newItem.setStyleClass("context_menu_new_item");
        newItem.setIcon("context_menu_new_item_icon");
        MethodExpression mbNewmethodExpression =
        getContext().getApplication().getExpressionFactory().createMethodExpression(
        getElContext(),
         "#{" + getBeanName() + ".newNodeActionListener}",
        null,
        new Class[] {});
        newItem.setActionExpression(mbNewmethodExpression);
        newItem.setUpdate("frmScrForm:multiLevelTree");
        
        //Edit
        MenuItem editItem = new MenuItem();
        editItem.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_edit");
        editItem.setValue(ddService.getDD("editItem", loggedUser).getLabelTranslated());
        editItem.setStyleClass("context_menu_edit_item");
        editItem.setIcon("context_menu_edit_item_icon");
        MethodExpression mbEditmethodExpression =
        getContext().getApplication().getExpressionFactory().createMethodExpression(
         getElContext(),
         "#{" + getBeanName() + ".editNodeActionListener}",
         null,
         new Class[] {});

        editItem.setActionExpression(mbEditmethodExpression);
        editItem.setUpdate("frmScrForm:multiLevelTree");
        
        //Delete
        MenuItem deleteItem = new MenuItem();
        deleteItem.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_delete");
        deleteItem.setValue(ddService.getDD("deleteItem", loggedUser).getLabelTranslated());
        deleteItem.setStyleClass("context_menu_delete_item");
        deleteItem.setIcon("context_menu_delete_item_icon");
        MethodExpression deleteItemmethodExpression =
        getContext().getApplication().getExpressionFactory().createMethodExpression(
        getElContext(),
         "#{" + getBeanName() + ".deleteNodeActionListener}",
        null,
        new Class[] {});
        deleteItem.setActionExpression(deleteItemmethodExpression);
        deleteItem.setUpdate("frmScrForm:multiLevelTree");
        
        //Open in another tree
        MenuItem openItem = new MenuItem();
        openItem.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_Open");
        openItem.setValue(ddService.getDD("openItem", loggedUser).getLabelTranslated());
        openItem.setStyleClass("context_menu_open_item");
        openItem.setIcon("context_menu_open_item_icon");
       
        //Cut and Paste
        MenuItem cutAndPastItem = new MenuItem();
        cutAndPastItem.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_cutAndPast");
        cutAndPastItem.setValue(ddService.getDD("cutAndPastItem", loggedUser).getLabelTranslated());
        cutAndPastItem.setStyleClass("context_menu_cut_item");
        cutAndPastItem.setIcon("context_menu_cut_item_icon");
       
        //Refresh
        MenuItem refreshItem = new MenuItem();
        refreshItem.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_refreshNode");
        refreshItem.setValue(ddService.getDD("refreshItem", loggedUser).getLabelTranslated());
        refreshItem.setStyleClass("context_menu_refresh_item");
        refreshItem.setIcon("context_menu_refresh_item_icon");
       
        MethodExpression refreshItemmethodExpression =
        getContext().getApplication().getExpressionFactory().createMethodExpression(
        getElContext(),
         "#{" + getBeanName() + ".refreshNodeActionListener}",
        null,
        new Class[] {});
        refreshItem.setActionExpression(refreshItemmethodExpression);
        
        //EditNodeInfo
        MenuItem editNodeInfItem = new MenuItem();
        editNodeInfItem.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_editNodeInf");
        editNodeInfItem.setValue(ddService.getDD("editNodeInfItem", loggedUser).getLabelTranslated());
        editNodeInfItem.setStyleClass("context_menu_inf_item");
        editNodeInfItem.setIcon("context_menu_inf_item_icon");
       
        MethodExpression editNodeInfItemmethodExpression =
        getContext().getApplication().getExpressionFactory().createMethodExpression(
        getElContext(),
         "#{" + getBeanName() + ".editNodeInfoActionMenuListener}",
        null,
        new Class[] {});
        editNodeInfItem.setActionExpression(editNodeInfItemmethodExpression);
        
        //NewNode
        MenuItem newNodeItem = new MenuItem();
        newNodeItem.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_newNode");
        newNodeItem.setValue(ddService.getDD("newNodeItem", loggedUser).getLabelTranslated());
        newNodeItem.setStyleClass("context_menu_newnode_item");
        newNodeItem.setIcon("context_menu_newnode_item_icon");
       
        MethodExpression newNodeItemmmethodExpression =
                getContext().getApplication().getExpressionFactory().createMethodExpression(
                getElContext(),
                "#{" + getBeanName() + ".newNodeActionMenuListener}",
                null,
                new Class[]{});
        newNodeItem.setActionExpression(newNodeItemmmethodExpression);
        
        menu.getChildren().add(newItem);
        menu.getChildren().add(editItem);
        menu.getChildren().add(deleteItem);
        menu.getChildren().add(openItem);
        menu.getChildren().add(cutAndPastItem);
        menu.getChildren().add(refreshItem);
        menu.getChildren().add(newNodeItem);
        menu.getChildren().add(editNodeInfItem);
        menu.setFor("multiLevelTree");
        htmlPanelGrid.getChildren().add(menu);
        /*************************************************
         * Define tree properties
         *************************************************/
        
        processInput();        
        if (isScreenOpenedForLookup()) {            
            htmlPanelGrid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_lkpTreePanel");
            htmlPanelGrid.getChildren().add(buildLookupBtnPanel("returnTreeSelectedNode"));
            entityTreeBrowser = levelsTree;
            
        }
        Tree tree = new Tree();
        tree.setVar("item");
        tree.setId("multiLevelTree");
        //Set the value
        tree.setValueExpression("value",
                exFactory.createValueExpression(elContext, "#{" + getBeanName() + ".root}", LevelNode.class));
        //Set Node Expand Event
        Class[] expandEvent = new Class[]{NodeExpandEvent.class};
        MethodExpression expandMB = exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                "#{" + getBeanName() + ".nodeExpandListener}", null, expandEvent);
        
        FABSAjaxBehavior expandBehavior = new FABSAjaxBehavior();
        expandBehavior.setListener(expandMB);        
        tree.addClientBehavior("expand", expandBehavior);
        //Set Node Selection Event
        Class[] selectionEvent = new Class[]{NodeSelectEvent.class};
        MethodExpression selectMB = exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                "#{" + getBeanName() + ".nodeActionListener}", null, selectionEvent);
        FABSAjaxBehavior selectionBehavior = new FABSAjaxBehavior();
        selectionBehavior.setListener(selectMB);
        tree.addClientBehavior("select", selectionBehavior);
        tree.setDynamic(true);
        //Selection Properties
        ValueExpression selectedNodesVe = exFactory.
            createValueExpression(elContext, "#{" + getBeanName() + ".selectedNodes}", TreeNode[].class);
        tree.setSelectionMode("multiple");
        tree.setValueExpression("selection", selectedNodesVe);
        tree.setStyleClass("tree_main_style");       
        /*************************************************
         * Define Node properties
         *************************************************/
        
        UITreeNode treeNodeFolder = new UITreeNode();
        treeNodeFolder.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()+
                "_nodeFolder");
        ValueExpression nodeTxtVe = exFactory.
            createValueExpression(elContext, "#{item}", String.class);

        treeNodeFolder.setExpandedIcon("tree_node_folder_expand");
        treeNodeFolder.setCollapsedIcon("tree_node_folder_collaps");
        
        
        treeNodeFolder.setType("folder");
        
        UITreeNode treeNodeDocument = new UITreeNode();
        treeNodeDocument.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()+
                "_nodeDocument");        
        treeNodeDocument.setStyleClass("tree_node_doc");
        treeNodeDocument.setIcon("tree_node_doc_icon");
        treeNodeDocument.setType("document");
        
        
        //treeNode.setType("document");
        /*************************************************
         * Define the display label
         *************************************************/        
        HtmlOutputText nodeText = new HtmlOutputText();
        HtmlOutputText nodeText2 = new HtmlOutputText();
        nodeText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()+
                "_nodeTxt");        
        nodeText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()+
                "_nodeTxt2");    
        nodeText.setValueExpression("value", nodeTxtVe);
        nodeText2.setValueExpression("value", nodeTxtVe);
        nodeText.setStyleClass("tree_node_folder_text");
        nodeText2.setStyleClass("tree_node_doc_text");
        treeNodeFolder.getChildren().add(nodeText);
        treeNodeDocument.getChildren().add(nodeText2);
        
        tree.getChildren().add(treeNodeFolder);
        tree.getChildren().add(treeNodeDocument);
        htmlPanelGrid.getChildren().add(tree);
        onloadJSStr+="top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');";
        return htmlPanelGrid;
    }
    @Override
    protected void buildScreenExpression(UIComponent component) {
    }

    @Override
    protected String getBeanName() {
        return MultiLevelBean.class.getSimpleName();
    }

    @Override
    public ODataMessage saveProcessRecords() {
        return null;
    }

    @Override
    protected BaseEntity getObjectForLookup(long dbid) {
        return null;
    }

    @Override
    public void loadAttribute(String expression) {
    }
    
    @Override
    protected boolean processInput() {
        logger.trace("Entering");
        if (!super.processInput())
            return false ;
        levelsTree = new LevelsTree((MultiLevelScreen) getCurrentScreen(), loggedUser,
                this, entitySetupService, oem, entityServiceLocal, uiFrameWorkFacade,filterServiceBean);
        root = new LevelNode("folder","Root", null);
         try {
                    currentScreen = (OScreen) textTranslationServiceRemote.loadEntityTranslation(getCurrentScreen(), loggedUser);
                } catch (Exception ex) {
                    logger.error("Exception thrown",ex);
                }
        LevelNode mainLevelNode = new LevelNode("folder",getCurrentScreen().getHeaderTranslated(), root);
        levelsTree.setRoot(mainLevelNode);
        mainLevelNode.setExpanded(true);
        levelsTree.addChildren(conditions,mainLevelNode);
        logger.trace("Returning with True");
        return true ;
    } 
//    /**
//     * Support VoidDT as an input in case of lookup.
//     * <br>sCalls {@link OBackBean#getValidScreenInputs()} and includes its returns
//     * @return 
//     */
    @Override
    protected List<ScreenInputDTO> getValidScreenInputs() {
        logger.trace("Entering");
        List<ScreenInputDTO> screenInputs =  null ;
        try {
            screenInputs = super.getValidScreenInputs() ;

            // If it's lookup, then support VoidDT Input
            if (isScreenOpenedForLookup()) 
            {
                // Add VoidDT as a natively supported input
                ScreenInputDTO voidInput = uiFrameWorkFacade.
                        getVoidAsScreenInput(screenInputs, loggedUser) ;
                if (voidInput != null)
                // Not found in screenInputs and got successfully
                    // Support it
                    screenInputs.add(voidInput);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        logger.trace("Returning");
        return screenInputs ;
    }

    @Override
    protected List<BaseEntity> getLoadedEntity() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void addClaimButtonToPanelGroup(String buttonID, String buttonLabel, String actionMethodName, HtmlPanelGrid panelLayout, Class[] params, String portletInsId, String beanName, String screenLanguage, boolean invalidInputMode, OScreen currentScreen) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
  
    }
