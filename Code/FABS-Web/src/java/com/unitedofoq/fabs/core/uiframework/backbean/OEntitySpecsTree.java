/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.udc.UDCType;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import javax.faces.component.html.HtmlPanelGrid;
import javax.persistence.CascadeType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mrashad
 */
public class OEntitySpecsTree {
final static Logger logger = LoggerFactory.getLogger(OEntitySpecsTree.class);
    private OUser loggedUser;
    private OEntityManagerRemote oem;
    private EntitySetupServiceRemote entitySetupService;
    private Class rootClass;
    private DDServiceRemote ddService;
    // the root tree node
    private OEntitySpecsTreeNode root;
    //  list of objects that are used in the TreeNodes
    private List<OEntitySpecsTreeNode> nodeObjects;
    // object reference to expand node
    private OEntitySpecsTreeNode selectedNodeObject = null;

    public OEntitySpecsTree(Class rootClass, OUser loggedUser, EntitySetupServiceRemote entitySetupService,
            DDServiceRemote ddService, OEntityManagerRemote oem) {
        this.rootClass = rootClass;
        this.loggedUser = loggedUser;
        this.oem = oem;
        this.entitySetupService = entitySetupService;
        this.ddService = ddService;
    }

    public HtmlPanelGrid buildTree() {
        return null;
    }

    public void nodeExpandListener(NodeExpandEvent event) {
        try {
            OEntitySpecsTreeNode oentityNode = (OEntitySpecsTreeNode) event.getTreeNode();
            if (!oentityNode.getChildren().isEmpty()
                    && oentityNode.getChildren().get(0).getData().toString().equals("")) {
                oentityNode.getChildren().remove(0);
            }
            setSelectedNodeObject(oentityNode);
            addChildren();

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            //FIXME: how to display error to user, e.g. User Message
        } finally {
            oem.closeEM(loggedUser);
        }

    }

    public void nodeActionListener(NodeSelectEvent event) {
        try {

            OEntitySpecsTreeNode oentityNode = (OEntitySpecsTreeNode) event.getTreeNode();
            if (!oentityNode.getChildren().isEmpty()
                    && oentityNode.getChildren().get(0).getData().toString().equals("")) {
                oentityNode.getChildren().remove(0);
            }
            setSelectedNodeObject(oentityNode);
            getSelectedNodeObject().setExpanded(true);
            //OEntitySpecsTreeNode newNode = new OEntitySpecsTreeNode("hello", event.getTreeNode());
            addChildren();
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            //FIXME: how to display error to user, e.g. User Message
        } finally {
        }
    }

    public void createRootNode(OEntitySpecsTreeNode node) {

        //  to contain the text of the root node of the tree
        String rootNodeText = "";

        //  load root class's corresponding OEntity
        OEntityDTO entity = null;

        try {
            entity = entitySetupService.loadOEntityDTO(rootClass.getName(), loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }

        //  if corresponding OEntity exists in DB...
        if (entity != null) //  create root node of tree using OEntity's title
        {
            rootNodeText = entity.getTitle();
        } //  if corresponding OEntity does NOT exist in DB...
        else //  create root node of tree using simple name of 'cls'
        {
            rootNodeText = rootClass.getSimpleName();
        }


        OEntitySpecsTreeNode mainOEntityTreeNode = new OEntitySpecsTreeNode(rootNodeText, node);
        setRoot(mainOEntityTreeNode);
        root.setExpanded(true);
        mainOEntityTreeNode.setExpanded(true);

        //  set class of root node to be name of root class
        getRoot().setNodeClass(rootClass.getName());

        //  set expression of root node as blank
        getRoot().setNodeExpression(rootNodeText);

        //  set nodeType of root node 
        getRoot().setNodeType("Root");

        //  add 'object' to 'nodeObjects' for future reference
        nodeObjects = new ArrayList<OEntitySpecsTreeNode>();
        nodeObjects.add(getRoot());
    }

    public void addChildren() {
        //  if node is selected...
        if (selectedNodeObject != null) {
            //  to indicate whether corresponding class can be expanded or not
            //  (i.e. should retrieval of underlying field be attempted?)
            boolean isExpandable = false;

            //  if it is an FABS class...
            if (selectedNodeObject.getNodeClass() != null) {
                if (selectedNodeObject.getNodeClass().contains("unitedofoq")) //  then it is expandable
                {
                    isExpandable = true;
                } //  if it is of type 'List'...
                else if (selectedNodeObject.getNodeClass().endsWith("List")) {
                    //  if generic type is an OTMS class
                    if (selectedNodeObject.getNodeClass().contains("unitedofoq")) //  then it is expandable
                    {
                        isExpandable = true;
                    }
                }
            } else if (selectedNodeObject.getNodeType().equals("FixedNode")) {
                isExpandable = true;
            }

            //  if corresponding is expandable...
            if (isExpandable) {

                //  to hold the class to be expanded
                BaseEntity baseEntity = null;
                Class cls = null;

                /* EXPAND */
                try {
                    if (selectedNodeObject.getNodeClass() != null) {
                        // null when node is one of fixed nodes "Children,Parent ..."
                        //  if corresponding class is NOT of type 'List'...
                        if (!selectedNodeObject.getNodeClass().endsWith("List")) {
                            cls = Class.forName(selectedNodeObject.getNodeClass());
                            baseEntity = (BaseEntity) cls.newInstance();
                        } 
                    } else { // set class with parent class of fixed node
                        cls = selectedNodeObject.getParentClass();
                        if (Modifier.toString(cls.getModifiers()).contains("abstract")) {
                            addEntityTypes(cls);
                            return;
                        }
                        baseEntity = (BaseEntity) cls.newInstance();
                    }
                    if (baseEntity != null) {
                        if (selectedNodeObject.getNodeType().equals("Root")
                                || selectedNodeObject.getNodeType().equals("Child")
                                || selectedNodeObject.getNodeType().equals("Parent")) {
                            addFixedNode(cls, "Children");
                            addFixedNode(cls, "Parents");
                            addFixedNode(cls, "Fields");
                            addFixedNode(cls, "Type");
                        } else if (selectedNodeObject.getNodeType().equals("FixedNode")) {
                            if (selectedNodeObject.getData().equals("Children")) {
                                addEntityChildren(cls);
                            } else if (selectedNodeObject.getData().equals("Parents")) {
                                addEntityParents(cls);
                            } else if (selectedNodeObject.getData().equals("Fields")) {
                                addEntityFields(cls, baseEntity);
                            } else if (selectedNodeObject.getData().equals("Type")) {
                                addEntityTypes(cls);
                            }
                        } else if (selectedNodeObject.getNodeType().equals("Data")) {
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Exception thrown",ex);
                }
            }
        }
    }

    private void addFixedNode(Class parentCls, String nodeText) {
        logger.trace("Entering");
        //  create child node
        //DefaultMutableTreeNode childNode = new DefaultMutableTreeNode();
        OEntitySpecsTreeNode childObject = new OEntitySpecsTreeNode(nodeText, selectedNodeObject);
        childObject.setNodeType("FixedNode");
        childObject.setNodeClass(null);
        childObject.setParentClass(parentCls);
        //  set child node expression
        childObject.setNodeExpression(selectedNodeObject.getNodeExpression());
        
        nodeObjects.add(childObject);
        logger.trace("Returning");
    }

    public void addEntityChildren(Class parentCls) {
        logger.trace("Entering");
        try {
            List<String> entityChildren = BaseEntity.getChildEntityFieldName(parentCls.getName());
            if (entityChildren != null && !entityChildren.isEmpty()) {
                for (String childName : entityChildren) {
                    boolean isList = false;

                    //  create child node
                    //DefaultMutableTreeNode childNode = new DefaultMutableTreeNode();
                    OEntitySpecsTreeNode childObject = new OEntitySpecsTreeNode("", selectedNodeObject);
                    Field childField = BaseEntity.getClassField(parentCls, childName);
                    if (childField == null) {
                        if(parentCls!=null)
                        logger.warn("Field: {} doesn't found in entity: {}",childName,parentCls.getName());
                        continue;
                    }
                    if (childField.getType().getName().endsWith("List")) {
                        ParameterizedType listType = (ParameterizedType) childField.getGenericType();
                        Class<?> listClass = (Class<?>) listType.getActualTypeArguments()[0];
                        childObject.setNodeClass(listClass.getName());
                        childObject.setData(childName + " (" + listClass.getSimpleName() + ") : (*)");
                        isList = true;
                    } else {
                        childObject.setNodeClass(childField.getType().getName());
                        childObject.setData(childName + " (" + Class.forName(childObject.getNodeClass()).getSimpleName() + ")");
                    }

                    // format node text if cascade
                    if (isCascadeField(childField)) {
                        if (isList) {
                            childObject.setData(childObject.getData() + ", Cascade");
                        } else {
                            childObject.setData(childObject.getData() + " : Cascade");
                        }
                    }

                    childObject.setParentClass(parentCls);

                    //  'expression' of parent node is used to generate
                    //  the 'expression' of the child node

                    //  get parent expression
                    String parentNodeExpression = selectedNodeObject.getNodeExpression();

                    String nodeExpression = parentNodeExpression + "." + childField.getName();

                    //  set child node expression
                    childObject.setNodeExpression(nodeExpression);

                    //  set child node as 'leaf' because it has
                    //  no children yet
//                    childObject.setLeaf(true);

                    //  set child node type
                    childObject.setNodeType("Child");

                    //  set child node user object
//                    childNode.setUserObject(childObject);

                    //  add 'childObject' to 'nodeObjects' for future reference
                    nodeObjects.add(childObject);

                    //  add child node to parent
//                    selectedNode.add(childNode);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        finally{
            logger.trace("Returning");
        }

    }

    public void addEntityParents(Class parentCls) {
        logger.debug("Entering");
        try {
            List<String> entityParents = BaseEntity.getParentEntityFieldName(parentCls.getName());
            if (entityParents != null && !entityParents.isEmpty()) {
                for (String parentName : entityParents) {
                    //  create child node
                    boolean isList = false;
                    OEntitySpecsTreeNode childObject = new OEntitySpecsTreeNode("", selectedNodeObject);
                    Field parentField = BaseEntity.getClassField(parentCls, parentName);
                    if (parentField == null) {
                        logger.warn("Field: {} doesn't found in entity", parentName);
                        continue;
                    }
                    if (parentField.getType().getName().endsWith("List")) {
                        ParameterizedType listType = (ParameterizedType) parentField.getGenericType();
                        Class<?> listClass = (Class<?>) listType.getActualTypeArguments()[0];
                        childObject.setNodeClass(listClass.getName());
                        childObject.setData(parentName + " (" + listClass.getSimpleName() + ") : (*)");
                        isList = true;
                    } else {
                        childObject.setNodeClass(parentField.getType().getName());
                        childObject.setData(parentName + " (" + Class.forName(childObject.getNodeClass()).getSimpleName() + ")");
                    }

                    // format node text if cascade
                    if (isCascadeField(parentField)) {
                        if (isList) {
                            childObject.setData(childObject.getData() + ", Cascade");
                        } else {
                            childObject.setData(childObject.getData() + " : Cascade");
                        }
                    }

                    childObject.setParentClass(parentCls);
                    //  'expression' of parent node is used to generate
                    //  the 'expression' of the child node

                    //  get parent expression
                    String parentNodeExpression = selectedNodeObject.getNodeExpression();

                    String nodeExpression = parentNodeExpression + "." + parentField.getName();

                    //  set child node expression
                    childObject.setNodeExpression(nodeExpression);
                    //  set child node as 'leaf' because it has
                    //  no children yet

                    //  set child node type
                    childObject.setNodeType("Parent");

                    //  set child node user object

                    //  add 'childObject' to 'nodeObjects' for future reference
                    nodeObjects.add(childObject);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        finally{
            logger.debug("Returning");
        }

    }

    public void addEntityFields(Class parentCls, BaseEntity baseEntity) {
        logger.debug("Entering");
        try {
            //  get fields of 'baseEntity'
            List<Field> fields = BaseEntity.getClassFields(baseEntity.getClass());
            //  loop over fields of 'baseEntity', and add them as
            //  children to corresponding TreeNode if they have DDs
            for (Field fld : fields) {
                String DDname = null;
                boolean isList = false;

                // <editor-fold defaultstate="collapsed" desc="DD concept">
                //Check if the field is CC
                if (fld.getType().equals(UDC.class) && fld.getName().contains("cc")) {
                    OEntityDTO entity = entitySetupService.loadOEntityDTO(baseEntity.getClassName(), loggedUser);
                    UDCType cType = (UDCType) BaseEntity.getValueFromEntity(entity,
                            (fld.getName().substring(0, fld.getName().length() - 1) + "Type"
                            + fld.getName().substring(fld.getName().length() - 1)));
                    if (cType == null) {
                        continue;
                    }
                    DDname = cType.getCode();
                } else {

                    //  if 'fld' does not have a corresponding DD...
                    DDname = BaseEntity.getFieldDDName(baseEntity, fld.getName());
                }

                //  to hold the value to be displayed as the child
                //  node's name
                String DDlabel = "";

                //  to indicate if the DD exists for this field
                boolean DDexists = false;

                //  if there is no DD name to display the item by...
                if (DDname == null || DDname.equals("")) {
                    logger.trace("DD name is empty or equals Null");
                    continue;
                } else {
                    try {
                        //  get DD
                        DD dd = ddService.getDD(DDname, loggedUser);

                        //  if DD retrieved successfully
                        if (dd != null) {
                            DDexists = true;
                            DDlabel = dd.getLabel();
                        } //  if DD does not exist, or there was problem
                        //  in retrieval...
                        else {
                            DDlabel = DDname;
                        }
                    } catch (Exception ex) {
                        logger.error("Exception thrown",ex);
                        DDlabel = fld.getName();
                    }
                }
                // </editor-fold>

                //get node type
                String fieldType = "";
                if (fld.getType().getName().contains("List")) {
                    ParameterizedType listType = (ParameterizedType) fld.getGenericType();
                    Class<?> listClass = (Class<?>) listType.getActualTypeArguments()[0];
                    fieldType = listClass.getSimpleName();
                    isList = true;
                } else {
                    fieldType = fld.getType().getSimpleName();
                }

                if (fieldType.contains("Java")) {
                    String delim = ".";
                    String[] token = fieldType.split(delim);
                    if (token != null && token.length != 0) {
                        int lastIndex = token.length - 1;
                        fieldType = token[lastIndex];
                    }
                }

                //  create child node
                OEntitySpecsTreeNode childObject = new OEntitySpecsTreeNode("", selectedNodeObject);
                if (fld.getAnnotation(Transient.class) != null
                        && fld.getName().contains("Trans")) {
                    childObject.setData(DDlabel + " (" + fieldType + ") : (T)");
                } else {
                    childObject.setData(DDlabel + " (" + fieldType + ")");
                }
                childObject.setNodeClass(fld.getType().getName());
                childObject.setParentClass(parentCls);

                //  'expression' of parent node is used to generate
                //  the 'expression' of the child node

                //  get parent expression
                String parentNodeExpression = selectedNodeObject.getNodeExpression();

                //  to hold node expression
                String nodeExpression = "";

                //  if the parent has empty expression...
                //  (i.e. it is the ROOT node of the tree)
                if (parentNodeExpression.equals("")) //  expression of child node is field's name
                {
                    nodeExpression = fld.getName();
                } else //  add expression of parent node to fld's name
                {
                    nodeExpression = parentNodeExpression + "." + fld.getName();
                }

                //  set child node expression
                childObject.setNodeExpression(nodeExpression);

                //  if 'fld' is of type 'List'...
                if (fld.getType().getName().endsWith("List")) {
                    //  get the generic type of the list
                    //  set child node generic type
                    childObject.setData(DDlabel + " (" + fieldType + ") : (*)");
                }
                //  if corresponding DD does not exist in the database...
                if (!DDexists) {
                    childObject.setData(DDlabel + " (" + fieldType + ") : (?)");
                }

                //  set child node type
                childObject.setNodeType("Data");
                //if field is mandatory set node color with red
                if (BaseEntity.isFieldMandatory(fld)) {
                    childObject.setNodeColor("red");
                }
                // format node text if cascade
                if (isCascadeField(fld)) {
                    if (isList) {
                        childObject.setData(childObject.getData() + ", Cascade");
                    } else {
                        childObject.setData(childObject.getData() + " : Cascade");
                    }
                }
                //  add 'childObject' to 'nodeObjects' for future reference
                nodeObjects.add(childObject);
            }

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
logger.debug("Returning");
    }

    public void addEntityTypes(Class parentCls) {
        logger.debug("Entering");
        if (Modifier.toString(parentCls.getModifiers()).contains("abstract")) {
            OEntitySpecsTreeNode childObject = new OEntitySpecsTreeNode("Abstract", selectedNodeObject);
            childObject.setNodeType("Data");
            childObject.setNodeClass(null);
            childObject.setParentClass(parentCls);
            //  set child node expression
            childObject.setNodeExpression("");
            //  set child node as 'leaf' because it has
            //  no children yet
            //  set child node user object
//            childNode.setUserObject(childObject);
            //  add 'childObject' to 'nodeObjects' for future reference
            nodeObjects.add(childObject);
            //selectedNode.add(childNode);
        }
        Annotation[] clsAnnotations = parentCls.getAnnotations();
        for (Annotation annotaion : clsAnnotations) {
            if (annotaion.annotationType() != MappedSuperclass.class) {
                continue;
            }
            OEntitySpecsTreeNode childObject = new OEntitySpecsTreeNode("MappedSuperClass", selectedNodeObject);
            childObject.setNodeType("Data");
            childObject.setNodeClass(null);
            childObject.setParentClass(parentCls);
            //  set child node expression
            childObject.setNodeExpression("");
            //  add 'childObject' to 'nodeObjects' for future reference
            nodeObjects.add(childObject);
        }
    }

    private boolean isCascadeField(Field fld) {
        logger.trace("Entering");
        if (fld.getAnnotation(OneToOne.class) != null) {
            OneToOne oneTOneAnnot = fld.getAnnotation(OneToOne.class);
            for (int i = 0; i < oneTOneAnnot.cascade().length; i++) {
                CascadeType type = oneTOneAnnot.cascade()[i];
                if (type == CascadeType.ALL || type == CascadeType.PERSIST) {
                    logger.trace("Returning with True");
                    return true;
                }
            }
        }
        logger.trace("Returning with False");
        return false;
    }

    public void retrieveSelectedObject(TreeNode treeNode) {
        logger.debug("Entering");
        for (OEntitySpecsTreeNode object : nodeObjects) {
            if (object.getData().equals(treeNode.getData())) {
                setSelectedNodeObject(object);
                logger.debug("Returning");
                return;
            }
        }
    }

    public OEntitySpecsTreeNode getSelectedNodeObject() {
        return selectedNodeObject;
    }

    public void setSelectedNodeObject(OEntitySpecsTreeNode selectedNodeObject) {
        this.selectedNodeObject = selectedNodeObject;
    }

    /**
     * @return the root
     */
    public OEntitySpecsTreeNode getRoot() {
        return root;
    }

    /**
     * @param root the root to set
     */
    public void setRoot(OEntitySpecsTreeNode root) {
        this.root = root;
    }
}
