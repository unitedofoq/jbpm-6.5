/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import java.io.Serializable;
import javax.el.MethodExpression;

/**
 *
 * @author bassem
 */
public class FABSAjaxBehaviorStateHolder implements Serializable
{
   private static final long serialVersionUID = -1420465850872729133L;
   
   private String update;
   private String process;
   private String onComplete;
   private String onError;
   private String onSuccess;
   private String onStart;
   private MethodExpression listener;
   private Object superState;
     
   
   /**
    * @return
    */
   public String getUpdate()
   {
      return update;
   }
   
   /**
    * @param update
    */
   public void setUpdate(String update)
   {
      this.update = update;
   }
   
   
   /**
    * @return
    */
   public String getProcess()
   {
      return process;
   }
   
   /**
    * @param process
    */
   public void setProcess(String process)
   {
      this.process = process;
   }
   
   
   /**
    * @return
    */
   public String getOnComplete()
   {
      return onComplete;
   }
   
   /**
    * @param onComplete
    */
   public void setOnComplete(String onComplete)
   {
      this.onComplete = onComplete;
   }
   
   
   /**
    * @return
    */
   public String getOnError()
   {
      return onError;
   }
   
   /**
    * @param onError
    */
   public void setOnError(String onError)
   {
      this.onError = onError;
   }
   
   
   /**
    * @return
    */
   public String getOnSuccess()
   {
      return onSuccess;
   }
   
   /**
    * @param onSuccess
    */
   public void setOnSuccess(String onSuccess)
   {
      this.onSuccess = onSuccess;
   }
   
   
   /**
    * @return
    */
   public String getOnStart()
   {
      return onStart;
   }
   
   /**
    * @param onStart
    */
   public void setOnStart(String onStart)
   {
      this.onStart = onStart;
   }
   
   
   /**
    * @return
    */
   public MethodExpression getListener()
   {
      return listener;
   }
   
   /**
    * @param listener
    */
   public void setListener(MethodExpression listener)
   {
        this.listener = listener;
    }

    /**
     * @return the superState
     */
    public Object getSuperState() {
        return superState;
    }

    /**
     * @param superState the superState to set
     */
    public void setSuperState(Object superState) {
        this.superState = superState;
    }
}
