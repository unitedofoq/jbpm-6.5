/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.entitydesigner;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIForm;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.component.api.UIColumn;
import org.primefaces.component.column.Column;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.dnd.Draggable;
import org.primefaces.component.panel.Panel;
import org.primefaces.component.panelgrid.PanelGrid;
import org.primefaces.model.mindmap.DefaultMindmapNode;
import org.primefaces.model.mindmap.MindmapNode;

import com.unitedofoq.fabs.core.uiframework.entitydesigner.dto.EntityDTO;
import com.unitedofoq.fabs.core.uiframework.entitydesigner.dto.FieldDTO;
import com.unitedofoq.fabs.core.uiframework.entitydesigner.dto.RelationDTO;

/**
 *
 * @author Mostafa
 */
@ManagedBean(name = "jpaBean")
@ViewScoped
public class JPABean implements Serializable {

    private List<EntityDTO> entities = new LinkedList<EntityDTO>();
    private List<FieldDTO> fields = new LinkedList<FieldDTO>();
    private Map<Integer, RelationDTO> currentRelations = new HashMap<Integer, RelationDTO>();
    private List<FieldDTO> newEntityFields = new LinkedList<FieldDTO>();
    private List<EntityDTO> newEntitiesRelation = new LinkedList<EntityDTO>();
    private Map<Integer, EntityDTO> currentEntites = new HashMap<Integer, EntityDTO>();
    private RelationDTO newRelationDTO = new RelationDTO();
    private MindmapNode root;
    private UIComponent workBench = new UIForm();
    private String entityName = "";
    private int ENTITY_INDEX;
    private int RELATION_INDEX;
    private String movedEntityId;
    private String movedEntityX;
    private String movedEntityY;
    private List<FieldDTO> fields2 = new LinkedList<FieldDTO>();
    private String relations = "";
    private List<FieldDTO> fields1 = new LinkedList<FieldDTO>();
    //    private SesssionBean sb ;
    private String entityR1 = "";
    private String entityR2 = "";
    private String editEntityId = "";
    private List<String> typeOfRelation = new ArrayList<String>();
    private String field1 = "";
    private String editRelationId;
    private List<String> fieldTypes;
    private String relationName;

    public String getRelationName() {
        return relationName;
    }

    public void setRelationName(String relationName) {
        this.relationName = relationName;
    }

    public List<String> getFieldTypes() {
        return fieldTypes;
    }

    public void setFieldTypes(List<String> fieldTypes) {
        this.fieldTypes = fieldTypes;
    }

    public Map<Integer, RelationDTO> getCurrentRelations() {
        return currentRelations;
    }

    public void setCurrentRelations(Map<Integer, RelationDTO> currentRelations) {
        this.currentRelations = currentRelations;
    }

    public String getEditRelationId() {
        return editRelationId;
    }

    public void setEditRelationId(String editRelationId) {
        this.editRelationId = editRelationId;
    }
    private String field2 = "";
    private String typeOfRelation1 = "";
    private String typeOfRelation2 = "";

    /**
     * Creates a new instance of Person
     */
    public JPABean() {


        fieldTypes = new ArrayList<String>();
        fieldTypes.add("Byte");
        fieldTypes.add("Short");
        fieldTypes.add("Integer");
        fieldTypes.add("Float");
        fieldTypes.add("Double");
        fieldTypes.add("Character");
        fieldTypes.add("Boolean");
        typeOfRelation.add("1");
        typeOfRelation.add("M");
        newEntitiesRelation.add(new EntityDTO());
        newEntitiesRelation.add(new EntityDTO());
        root = new DefaultMindmapNode("goo\\nom", "Google WebSite", "FFCC00", false);
        MindmapNode root1 = new DefaultMindmapNode("root1", "Google WebSite", "FFCC00", false);
        MindmapNode root2 = new DefaultMindmapNode("root2", "Google WebSite", "FFCC00", false);
        MindmapNode root3 = new DefaultMindmapNode("root3", "Google WebSite", "FFCC00", false);
        MindmapNode root4 = new DefaultMindmapNode("root4", "Google WebSite", "FFCC00", false);
        root.addNode(root1);
        root1.addNode(root2);


    }

    public void updateFields1(ActionEvent e) {
        if (entityR1 != null && currentEntites.get(Integer.valueOf(entityR1)) != null) {
            fields1 = currentEntites.get(Integer.valueOf(entityR1)).getFields();
        }
    }

    public void updateFields2(ActionEvent e) {
        if (entityR2 != null && currentEntites.get(Integer.valueOf(entityR2)) != null) {
            fields2 = currentEntites.get(Integer.valueOf(entityR2)).getFields();
        }
    }

    public void clearAll() {
        currentEntites = new HashMap<Integer, EntityDTO>();
        currentRelations = new HashMap<Integer, RelationDTO>();
        saveToSession();
        getWorkBench().getChildren().clear();
        ENTITY_INDEX = 0;
        RELATION_INDEX = 0;
        reDraw();
    }

    public void clearAll(ActionEvent e) {
        clearAll();
    }

    public void addField(ActionEvent e) {
        FieldDTO fieldDTO = new FieldDTO();
        fieldDTO.setFieldName("");
        fieldDTO.setFieldType("");
        newEntityFields.add(fieldDTO);
    }

    public void removeField(ActionEvent e, FieldDTO fieldDTO) {

        newEntityFields.remove(fieldDTO);
    }

    public void getEntityInfo(ActionEvent e) {

        clearAllDlg();
        EntityDTO entityDTO = currentEntites.get(Integer.valueOf(editEntityId));
        entityName = entityDTO.getEntityName();
        newEntityFields = new ArrayList<FieldDTO>();
        for (int i = 0; i < entityDTO.getFields().size(); i++) {
            FieldDTO fieldDTO = new FieldDTO(entityDTO.getFields().get(i));
            newEntityFields.add(fieldDTO);
        }
       // newEntityFields = new ArrayList<FieldDTO>(entityDTO.getFields());
    }

    public void getRelationInfo(ActionEvent e) {

        clearAllDlg();
        RelationDTO relationDTO = currentRelations.get(Integer.valueOf(Integer.valueOf(editRelationId)));
        relationName = relationDTO.getRelationName();
        entityR2 = String.valueOf(relationDTO.getDestination().getEntityIndex());
        entityR1 = String.valueOf(relationDTO.getSource().getEntityIndex());
        fields1 = relationDTO.getSource().getFields();
        fields2 = relationDTO.getDestination().getFields();
        if (relationDTO.getSourceField() != null) {
            field1 = relationDTO.getSourceField().getFieldName();
        }
        if (relationDTO.getDestinationField() != null) {
            field2 = relationDTO.getDestinationField().getFieldName();
        }
        typeOfRelation1 = relationDTO.getSourceRelationType();
        typeOfRelation2 = relationDTO.getDestinationRelationType();
    }

    public void updateEntityPosition(ActionEvent e) {


        currentEntites.get(Integer.valueOf(movedEntityId.split("_")[1])).setxPostion(Integer.valueOf(movedEntityX));
        currentEntites.get(Integer.valueOf(movedEntityId.split("_")[1])).setyPostion(Integer.valueOf(movedEntityY));
        saveToSession();
    }

    public void saveToSession() {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("entities", currentEntites);
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("relations", currentRelations);
        relations = convertToString(currentRelations);
    }

    public void clearAllDlg(ActionEvent a) {
        clearAllDlg();
    }

    public void clearAllDlg() {
        typeOfRelation1 = "";
        typeOfRelation2 = "";
        relationName = "";
        entityName = "";
        newEntityFields = new LinkedList<FieldDTO>();
        entityR1 = "";
        entityR2 = "";
        field1 = "";
        field2 = "";
        fields1 = new ArrayList<FieldDTO>();
        fields2 = new ArrayList<FieldDTO>();
    }

    public FieldDTO getFieldNyName(EntityDTO entityDTO, String fieldName) {
        if (entityDTO != null) {
            List<FieldDTO> fieldDTOs = entityDTO.getFields();
            if (fieldDTOs != null) {
                for (int i = 0; i < fieldDTOs.size(); i++) {
                    if (fieldName.equalsIgnoreCase(fieldDTOs.get(i).getFieldName())) {
                        return fieldDTOs.get(i);
                    }
                }
            }
        }

        return null;
    }

    public void addRelation(ActionEvent e) {
        if (entityR1 != null && entityR2 != null && !entityR1.isEmpty() && !entityR2.isEmpty()
                && field1 != null && field2 != null && !field1.isEmpty() && !field2.isEmpty()
                && typeOfRelation1 != null && typeOfRelation2 != null && !typeOfRelation1.isEmpty() && !typeOfRelation2.isEmpty()) {

            RelationDTO relationDTO = new RelationDTO();
            EntityDTO entityDTO1 = new EntityDTO();
            EntityDTO entityDTO2 = new EntityDTO();
            relationDTO.setRelationId(RELATION_INDEX);
            entityDTO1 = currentEntites.get(Integer.valueOf(entityR1));
            entityDTO2 = currentEntites.get(Integer.valueOf(entityR2));
            relationDTO.setSource(entityDTO1);
            relationDTO.setDestination(entityDTO2);
            relationDTO.setSourceRelationType(typeOfRelation1);
            relationDTO.setSourceField(getFieldNyName(entityDTO1, field1));
            relationDTO.setDestinationField(getFieldNyName(entityDTO2, field2));
            relationDTO.setDestinationRelationType(typeOfRelation2);
            relationDTO.setRelationName(relationName);
            if (currentRelations == null) {
                currentRelations = new HashMap<Integer, RelationDTO>();
            }
            currentRelations.put(RELATION_INDEX, relationDTO);
            RELATION_INDEX++;
            saveToSession();
        }
        reDraw();
    }

    public EntityDTO getEntityByName(String name) {
        if (currentEntites != null && currentEntites.size() > 0) {
            int i = 0;
            Object[] keySet = currentEntites.keySet().toArray();
            for (i = 0; i < keySet.length; i++) {
                EntityDTO entityDTO = currentEntites.get((Integer) keySet[i]);

                if (entityDTO != null && entityDTO.getEntityName().equalsIgnoreCase(name)) {
                    return entityDTO;
                }
            }
        }
        return null;
    }

    public EntityDTO getEntityById(Integer id) {
        if (id == null) {
            return null;
        }
        return currentEntites.get(id);
    }

    public void addItem(ActionEvent e) {
        ENTITY_INDEX++;

        if (getEntityById(ENTITY_INDEX) == null && getEntityByName(entityName) == null) {
            EntityDTO entityDTO = new EntityDTO();
            entityDTO.setEntityIndex(ENTITY_INDEX);
            entityDTO.setEntityName(entityName);
            List<FieldDTO> newfields = new ArrayList<FieldDTO>();
            for (int i = 0; i < newEntityFields.size(); i++) {
                FieldDTO entityField = newEntityFields.get(i);
                if (entityField.getFieldName() != null && entityField.getFieldType() != null && !entityField.getFieldName().isEmpty() && !entityField.getFieldType().isEmpty()) {
                    newfields.add(entityField);
                }
            }
            entityDTO.setFields(newfields);
            entityDTO.setxPostion(20);
            entityDTO.setyPostion(20);
            currentEntites.put(ENTITY_INDEX, entityDTO);
            reDraw();
        }
    }

    public void deleteRelation(ActionEvent e) {
        RelationDTO relationDTO = currentRelations.get(Integer.valueOf(editRelationId));
        if (relationDTO != null) {
            currentRelations.remove(Integer.valueOf(editRelationId));
        }
        reDraw();
    }

    public void deleteItem(ActionEvent e) {
         EntityDTO entityDTO = currentEntites.get(Integer.valueOf(editEntityId));
        if (entityDTO != null) {
            Object[] arr = (Object[]) currentRelations.keySet().toArray();
            for (int i = 0; i < arr.length; i++) {
                EntityDTO entityDTO1 = currentRelations.get((Integer)arr[i]).getDestination();
                EntityDTO entityDTO2 = currentRelations.get((Integer)arr[i]).getSource();
                if (entityDTO.getEntityIndex() == entityDTO2.getEntityIndex() || entityDTO.getEntityIndex() == entityDTO1.getEntityIndex()) {
                    currentRelations.remove((Integer)arr[i]);
                }
            }
            currentEntites.remove(Integer.valueOf(editEntityId));
            
            reDraw();
        }


    }

    public void updateItem(ActionEvent e) {
        EntityDTO entityDTO = currentEntites.get(Integer.valueOf(editEntityId));


        if (entityDTO != null) {

            // EntityDTO entityDTO = new EntityDTO();
            //entityDTO.setEntityIndex(ENTITY_INDEX);
            entityDTO.setEntityName(entityName);
            List<FieldDTO> newfields = new ArrayList<FieldDTO>();
            for (int i = 0; i < newEntityFields.size(); i++) {
                FieldDTO entityField = newEntityFields.get(i);
                if (entityField.getFieldName() != null && entityField.getFieldType() != null && !entityField.getFieldName().isEmpty() && !entityField.getFieldType().isEmpty()) {
                    newfields.add(entityField);
                }
            }
            entityDTO.setFields(newfields);
            //entityDTO.setxPostion(20);
            //entityDTO.setyPostion(20);
            //currentEntites.put(ENTITY_INDEX, entityDTO);
            reDraw();
        }
    }

    public void updateRelation(ActionEvent e) {
        RelationDTO relationDTO = currentRelations.get(Integer.valueOf(editRelationId));


        if (relationDTO != null) {

            if (entityR1 != null && entityR2 != null && !entityR1.isEmpty() && !entityR2.isEmpty()) {
                EntityDTO entityDTO1 = new EntityDTO();
                EntityDTO entityDTO2 = new EntityDTO();
                //relationDTO.setRelationId(RELATION_INDEX);
                entityDTO1 = currentEntites.get(Integer.valueOf(entityR1));
                entityDTO2 = currentEntites.get(Integer.valueOf(entityR2));
                relationDTO.setSource(entityDTO1);
                relationDTO.setDestination(entityDTO2);
                relationDTO.setDestinationRelationType(typeOfRelation1);
                relationDTO.setSourceRelationType(typeOfRelation2);
                if (currentRelations == null) {
                    currentRelations = new HashMap<Integer, RelationDTO>();
                }
                //currentRelations.put(RELATION_INDEX, relationDTO);
                //RELATION_INDEX++;
                saveToSession();
            }
        }
        reDraw();
    }

    public void reDraw() {

        getWorkBench().getChildren().clear();
        for (int i = 1; i <= ENTITY_INDEX; i++) {
            if (currentEntites.get(i) != null) {
                addEntityComponent(currentEntites.get(i));
            }
        }
        saveToSession();

        entityName = "";
        newEntityFields = new LinkedList<FieldDTO>();
    }

    public UIComponent getWorkBench() {

        return workBench;
    }

    public void setWorkBench(UIComponent workBench) {

        this.workBench = workBench;
    }

    private void addEntityComponent(EntityDTO entityDTO) {


        root = new DefaultMindmapNode("google.com", "Google WebSite", "FFCC00", false);
        Panel entityUIComponent = new Panel();




        entityUIComponent.setHeader("-" + entityDTO.getEntityName() + "-");
        entityUIComponent.setId("Entity_" + entityDTO.getEntityIndex());
        entityUIComponent.setToggleable(true);
        entityUIComponent.setStyle("width:18%;position: absolute;left:" + entityDTO.getxPostion() + "px;top:" + entityDTO.getyPostion() + "px;");
        entityUIComponent.setStyleClass("Entity");
        Draggable draggable = new Draggable();
        draggable.setId("Draggable_" + entityDTO.getEntityIndex());
        draggable.setFor(entityUIComponent.getId());
        getWorkBench().getChildren().add(draggable);
        getWorkBench().getChildren().add(entityUIComponent);

        UIComponent entityFieldsComponent = getEntityFieldsComponent(entityDTO.getFields(), entityDTO.getEntityIndex());
        entityUIComponent.getChildren().add(entityFieldsComponent);


    }

    private UIComponent getEntityFieldsComponent(List<FieldDTO> entityFields, int entityIndex) {

        List<FieldDTO> newfields = new ArrayList<FieldDTO>();
        for (int i = 0; i < entityFields.size(); i++) {
            FieldDTO entityField = entityFields.get(i);
            if (entityField.getFieldName() != null && entityField.getFieldType() != null && !entityField.getFieldName().isEmpty() && !entityField.getFieldType().isEmpty()) {
                newfields.add(entityField);
            }
        }
        DataTable dataTable = new DataTable();
        dataTable.setId("dataTable_" + entityIndex);
        dataTable.setStyle("width:100%");
        Column column1 = new Column();
        column1.setId("column1_" + entityIndex);
        column1.setHeaderText("Type");
        Column column2 = new Column();
        column2.setId("column2_" + entityIndex);
        column2.setHeaderText("Name");

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ELContext elContext = facesContext.getELContext();
        ExpressionFactory elFactory = facesContext.getApplication().getExpressionFactory();
        ValueExpression valueExpresion2 = elFactory.createValueExpression(elContext, "#{field.fieldName}", String.class);
        ValueExpression valueExpresion1 = elFactory.createValueExpression(elContext, "#{field.fieldType}", String.class);


        UIOutput output2 = new UIOutput();

        output2.setValueExpression("value", valueExpresion2);
        UIOutput output1 = new UIOutput();
        output1.setValueExpression("value", valueExpresion1);

        column1.getChildren().add(output1);
        column2.getChildren().add(output2);



        LinkedList<UIColumn> columns = new LinkedList<UIColumn>();
        columns.add(column1);
        columns.add(column2);
        dataTable.setColumns(columns);
        dataTable.setValue(newfields);
        dataTable.setVar("field");

        PanelGrid panelGrid = new PanelGrid();
        panelGrid.setId("PanelGrid_" + entityIndex);
        panelGrid.setColumns(1);
        panelGrid.setStyle("width:100%;");
        panelGrid.getChildren().add(dataTable);
        return panelGrid;
    }

    public void deletField(ActionEvent e) {
    }

    /**
     * @return the entityName
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * @param entityName the entityName to set
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    /**
     * @return the entities
     */
    public List<EntityDTO> getEntities() {
        return entities;
    }

    /**
     * @return the newEntityFields
     */
    public List<FieldDTO> getNewEntityFields() {
        return newEntityFields;
    }

    public RelationDTO getNewRelationDTO() {
        return newRelationDTO;
    }

    public void setNewRelationDTO(RelationDTO newRelationDTO) {
        this.newRelationDTO = newRelationDTO;
    }

    public List<EntityDTO> getNewEntitiesRelation() {
        return newEntitiesRelation;
    }

    public void setNewEntitiesRelation(List<EntityDTO> newEntitiesRelation) {
        this.newEntitiesRelation = newEntitiesRelation;
    }

    public MindmapNode getRoot() {
        return root;
    }

    public void setRoot(MindmapNode root) {
        this.root = root;
    }

    public List<FieldDTO> getFields() {
        return fields;
    }

    public void setFields(List<FieldDTO> fields) {
        this.fields = fields;
    }

    public void retrieveFromSession() {
        currentEntites = (HashMap<Integer, EntityDTO>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("entities");
        currentRelations = (Map<Integer, RelationDTO>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("relations");
        relations = convertToString(currentRelations);
    }

    @PostConstruct
    public void PostConstruct() {

        retrieveFromSession();

        int max = 0;
        if (currentEntites != null && currentEntites.size() > 0) {
            int i = 0;
            Object[] keySet = currentEntites.keySet().toArray();
            for (i = 0; i < keySet.length; i++) {

                if (currentEntites.get((Integer) keySet[i]) != null) {
                    addEntityComponent(currentEntites.get((Integer) keySet[i]));
                    if ((Integer) keySet[i] > max) {
                        max = (Integer) keySet[i];
                    }

                }
            }
            ENTITY_INDEX = max + 1;
        } else {
            currentEntites = new HashMap<Integer, EntityDTO>();
        }
        max = 0;
        if (currentRelations != null && currentRelations.size() > 0) {
            int i = 0;
            Object[] keySet = currentRelations.keySet().toArray();
            for (i = 0; i < keySet.length; i++) {

                if (currentRelations.get((Integer) keySet[i]) != null) {
                    if ((Integer) keySet[i] > max) {
                        max = (Integer) keySet[i];
                    }
                }
            }
            RELATION_INDEX = max + 1;
        } else {
            currentRelations = new HashMap<Integer, RelationDTO>();
        }


    }

    public String getMovedEntityId() {
        return movedEntityId;
    }

    public void setMovedEntityId(String movedEntityId) {
        this.movedEntityId = movedEntityId;
    }

    public String getMovedEntityX() {
        return movedEntityX;
    }

    public void setMovedEntityX(String movedEntityX) {
        this.movedEntityX = movedEntityX;
    }

    public String getMovedEntityY() {
        return movedEntityY;
    }

    public void setMovedEntityY(String movedEntityY) {
        this.movedEntityY = movedEntityY;
    }

    public Map<Integer, EntityDTO> getCurrentEntites() {
        return currentEntites;
    }

    public void setCurrentEntites(Map<Integer, EntityDTO> currentEntites) {
        this.currentEntites = currentEntites;
    }

    public List<FieldDTO> getFields1() {
        return fields1;
    }

    public void setFields1(List<FieldDTO> fields1) {
        this.fields1 = fields1;
    }

    public List<FieldDTO> getFields2() {
        return fields2;
    }

    public void setFields2(List<FieldDTO> fields2) {
        this.fields2 = fields2;
    }

    public String getRelations() {
        return relations;
    }

    public void setRelations(String relations) {
        this.relations = relations;
    }

    public String getEntityR1() {
        return entityR1;
    }

    public void setEntityR1(String entityR1) {
        this.entityR1 = entityR1;
    }

    public String getEntityR2() {
        return entityR2;
    }

    public void setEntityR2(String entityR2) {
        this.entityR2 = entityR2;
    }

    private String convertToString(Map<Integer, RelationDTO> currentRelations) {


        String relations = "";

        if (currentRelations != null && currentRelations.size() > 0) {
            Object[] keySet = currentRelations.keySet().toArray();

            for (int i = 0; i < currentRelations.size(); i++) {
                RelationDTO relationDTO = currentRelations.get((Integer) keySet[i]);
                relations += "--" + relationDTO.getRelationId() + ":" + relationDTO.getSource().getEntityIndex() + ":" + relationDTO.getDestination().getEntityIndex() + ":" + relationDTO.getSourceRelationType() + ":" + relationDTO.getDestinationRelationType();
            }
        }
        return relations;
    }

    public String getEditEntityId() {
        return editEntityId;
    }

    public void setEditEntityId(String editEntityId) {
        this.editEntityId = editEntityId;
    }

    public List<String> getTypeOfRelation() {
        return typeOfRelation;
    }

    public void setTypeOfRelation(List<String> typeOfRelation) {
        this.typeOfRelation = typeOfRelation;
    }

    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public String getField2() {
        return field2;
    }

    public void setField2(String field2) {
        this.field2 = field2;
    }

    public String getTypeOfRelation1() {
        return typeOfRelation1;
    }

    public void setTypeOfRelation1(String typeOfRelation1) {
        this.typeOfRelation1 = typeOfRelation1;
    }

    public String getTypeOfRelation2() {
        return typeOfRelation2;
    }

    public void setTypeOfRelation2(String typeOfRelation2) {
        this.typeOfRelation2 = typeOfRelation2;
    }
}
