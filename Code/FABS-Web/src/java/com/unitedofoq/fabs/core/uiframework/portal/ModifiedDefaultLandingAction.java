/*
 * Add this line in portal-ext (this line only)
 * login.events.post=com.liferay.portal.events.LoginPostAction,com.unitedofoq.otms.core.uiframework.portal.ModifiedDefaultLandingAction
 */
package com.unitedofoq.fabs.core.uiframework.portal;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.exception.PortalException;

import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Layout;
import com.liferay.portal.model.LayoutTypePortlet;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.struts.LastPath;
import com.liferay.portal.util.PortalUtil;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.backbean.OBackBean;
import com.unitedofoq.fabs.core.uiframework.screen.FormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.TabularScreen;
import java.util.HashMap;
import java.util.List;


import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author ahussien
 */
public class ModifiedDefaultLandingAction { //extends Action {
    
    @EJB
    protected UIFrameworkServiceRemote uiFrameWorkFacade;

    final static Logger logger = LoggerFactory.getLogger(ModifiedDefaultLandingAction.class);
    //@Override
    public void run(HttpServletRequest request, HttpServletResponse response) throws ActionException {
        logger.debug("Entering");

        PortalServiceRemote portalService = null;
        try {
            Context context = new InitialContext();
            portalService = (PortalServiceRemote)context.lookup(PortalServiceRemote.class.getName());
        } catch (NamingException ex) {
            logger.error("Exception thrown: ",ex);
        }


        String mainPageName = null;
        String userScreenName = null;
        String screenInstId=null;

        long groupId = 0;
        long userId = 0;
        Layout anyUserLayout = null;

        try {

            groupId = UserLocalServiceUtil.getUser(PortalUtil.getUserId(request)).getGroup().getGroupId();
            userId = PortalUtil.getUserId(request);

            //get any page layout
            anyUserLayout = LayoutLocalServiceUtil.getLayouts(groupId, true).get(0);
            mainPageName = anyUserLayout.getName("");

            userScreenName = UserLocalServiceUtil.getUser(PortalUtil.getUserId(request)).getScreenName();
//            List<String> conditions = new ArrayList<String>();
//            conditions.add("user.loginName = '" + userScreenName + "'");
//
//            List<String> fieldExpression = new ArrayList<String>();
//            fieldExpression.add("userPortlets");

//            OUser systemUser = oEntityManagerRemote.getSystemUser();
//            System.out.println("systemUser dbid: " + systemUser.getDbid());
//
//            List<OUserPortalPage> userPortalPages = oEntityManagerRemote.loadEntityList(
//                    OUserPortalPage.class.getSimpleName(), conditions, fieldExpression, null, systemUser);

            List<OUserPortalPage> userPortalPages = portalService.getUserPortalPages(userScreenName);

            for(OUserPortalPage userPortalPage : userPortalPages)
            {
                Layout layout = null;
                try
                {
                    layout = LayoutLocalServiceUtil.getLayout(userPortalPage.getPortalPageID());
                }
                catch (PortalException ex)//If not found then add a new one
                {
                    layout = LayoutLocalServiceUtil.addLayout(userId, groupId, true,
                            anyUserLayout.getParentLayoutId(), userPortalPage.getName(), userPortalPage.getName(), "", "portlet", false, "", null);
                    userPortalPage.setPortalPageID(layout.getPlid());
                    portalService.updateUserPortalPageID(userPortalPage.getDbid(), layout.getPlid());
                    //oEntityManagerRemote.saveEntity(userPortalPage, oEntityManagerRemote.getSystemUser());
                }
                
                LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
                
                //Remove All portlets layout
                List<String> portletIds = layoutTypePortlet.getPortletIds();
                for(String portletId : portletIds)
                {
                    layoutTypePortlet.removePortletId(userId, portletId);
                }

                //Set template
                layoutTypePortlet.setLayoutTemplateId(userId, userPortalPage.getLayoutType());
                
                List<OUserPortalPagePortlet> userPortalPagePortlets = userPortalPage.getUserPortlets();
                for(OUserPortalPagePortlet userPortalPagePortlet : userPortalPagePortlets)
                {
                    if(userPortalPagePortlet.getPortletId().contains("OTMSPortlet_WAR_FABSPortal"))//if OTMS Page (OTMSPortlet_WAR_FABSPortal)
                    {
                        String portletIdInc = layoutTypePortlet.addPortletId(userId, "OTMSPortlet_WAR_FABSPortal");
                        System.out.println("portletIdInc: " + portletIdInc);
                        layoutTypePortlet.movePortletId(userId, portletIdInc,
                                userPortalPagePortlet.getColumn(), userPortalPagePortlet.getPosition());
                        
                        String viewPage = userPortalPagePortlet.getScreen().getViewPage();
                        if (viewPage == null || "".equals(viewPage)){
                            if (userPortalPagePortlet.getScreen() instanceof TabularScreen) {
                                viewPage = "TabularScreen.iface";
                            } else if (userPortalPagePortlet.getScreen() instanceof FormScreen) {
                                viewPage = "FormScreen.iface";
                            }
                        }
                        screenInstId= OBackBean.generateScreenInstId(userPortalPagePortlet.getScreen().getName());
                        OPortalUtil.setPortletPreferences(portletIdInc, userPortalPagePortlet.getScreen().getName(),screenInstId,
                            userPortalPagePortlet.getScreen().getHeaderTranslated(), viewPage, "", "", null, layout, "false");
                    }
                    else
                    {
                        layoutTypePortlet.movePortletId(userId,
                            userPortalPagePortlet.getPortletId(), userPortalPagePortlet.getColumn(), userPortalPagePortlet.getPosition());
                    }
                }
                
                LayoutLocalServiceUtil.updateLayout(layout.getGroupId(), layout.isPrivateLayout(), layout.getLayoutId(), layout.getTypeSettings());

            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        }

        try {
            //http://localhost:9086/user/ahmed/welcome
            //String path = "/user/" + userScreenName + "/" + mainPageName;
            String path = "/user/" + userScreenName;
            if (Validator.isNotNull(path)) {
                LastPath lastPath = new LastPath(StringPool.BLANK, path, new HashMap<String, String[]>());
                HttpSession session = request.getSession();
                session.setAttribute("LAST_PATH", lastPath);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        }


    }
}
