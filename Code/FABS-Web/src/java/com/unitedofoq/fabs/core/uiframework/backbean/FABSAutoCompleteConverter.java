/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

/**
 *
 * @author lap
 */
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.faces.model.SelectItem;
import org.primefaces.component.selectonemenu.SelectOneMenu;

@FacesConverter(value = "FABSAutoCompleteConverter")
public class FABSAutoCompleteConverter implements Converter, Serializable {

    List<String> conditions = new ArrayList<String>();
    List<SelectItem> SelectItems = null;

    public FABSAutoCompleteConverter(List<SelectItem> selectItems) {
        this.SelectItems = selectItems;
    }

    public FABSAutoCompleteConverter() {
        SelectItems = new ArrayList<SelectItem>();
    }

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
//        if (value != null && value.trim().length() > 0) {
//            try {
//                SingleEntityBean service = (SingleEntityBean) fc.getExternalContext().getApplicationMap().get("autoCompleteList");
//                return service.getAutoCompleteList().get(Integer.parseInt(value));
//            } catch (NumberFormatException e) {
//                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid Entity."));
//            }
//        } else {
//            return null;
//        }
        
        for (SelectItem currentSelectItem : SelectItems) {
            if (currentSelectItem.getLabel().equals(value)) {
                return currentSelectItem.getValue();
            }
        }
        // if corresponding object was not found...
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {

            for (SelectItem currentSelectItem : SelectItems) {
                if (currentSelectItem.getValue() != null) {
                    if (currentSelectItem.getValue().equals(object)) {
                        return currentSelectItem.getLabel();
                    }
                }
            }
            // if corresponding label was not found...
            return "";
        } else {
            return null;
        }
    }
}
