package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.function.OFunctionResult;

import com.unitedofoq.fabs.core.multimedia.OIMageServiceLocal;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

@ManagedBean(name = "UploadImageBean")
@ViewScoped
public class UploadImageBean extends FormBean {

    
    private OIMageServiceLocal imageService = OEJB.lookup(OIMageServiceLocal.class);
    
    private UserMessageServiceRemote userMessageService = OEJB.lookup(UserMessageServiceRemote.class);

    @Override
    protected String getBeanName() {
        return "UploadImageBean";
    }
    OUser viewLoggedUser = loggedUser;
    Map<String, String> requestParams = FacesContext.getCurrentInstance().
            getExternalContext().getRequestParameterMap();

    @Override
    public void init() {

        if (requestParams.get("UD") != null) {
            super.init();
            setLoggedUserLocal(Long.valueOf(loggedUser.getDbid()).toString());
            setUdLocal(requestParams.get("UD"));
        } else {
            setLoggedUserLocal(requestParams.get("loggedUserLocalParam"));
            setUdLocal(requestParams.get("udLocalParam"));
        }
    }

    // <editor-fold defaultstate="collapsed" desc="uploadedImage In advanced mode">
    public void uploadImage(FileUploadEvent event) {
        logger.debug("Entering");
        OutputStream out = null;
        UploadedFile uploadedFileAdv;
        File file = null;
        try {
            uploadedFileAdv = (UploadedFile) event.getFile();
            file = new File(uploadedFileAdv.getFileName());
            InputStream in = uploadedFileAdv.getInputstream();
            out = new FileOutputStream(file);
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            OFunctionResult oFR = new OFunctionResult();
            oFR.append(imageService.saveImage(new File(file.getAbsolutePath()),
                    uploadedFileAdv.getFileName(), uploadedFileAdv.getContentType(), viewLoggedUser));
            setFileName(file.getName());
            if (oFR.getErrors().size() > 0) {
                logger.warn("Error Saving File");
                oFR.addError(userMessageService.getUserMessage(
                        "SystemInternalError", viewLoggedUser));
            } else {
                logger.trace("Successfully saved uploaded file into DB");
            }
            in.close();
            out.flush();
            out.close();
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        } finally {
            try {
                out.close();
            } catch (IOException ex) {
                logger.error("Exception thrown",ex);
            }
            file.deleteOnExit();
            logger.debug("Returning");
        }
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="request parameters management">
    private String screenNameLocal = "UploadImage";
    private String screenInstIdLocal = "UploadImage_5";
    private String portletIdLocal = "UploadImage";
    private String udLocal = "10169";
    private String lPLIDLocal = "10160";
    private String portletInsIdLocal = "OTMSPortlet_WAR_FABSPortal_INSTANCE_tN8T";
    private boolean portalModeLocal = true;
    private boolean selectFRowLocal = false;
    private boolean EnableC2ALocal = false;
    private boolean mainLocal = false;
    private String loggedUserLocal;

    /**
     * @return the screenNameLocal
     */
    public String getScreenNameLocal() {
        return screenNameLocal;
    }

    /**
     * @param screenNameLocal the screenNameLocal to set
     */
    public void setScreenNameLocal(String screenNameLocal) {
        this.screenNameLocal = screenNameLocal;
    }

    /**
     * @return the screenInstIdLocal
     */
    public String getScreenInstIdLocal() {
        return screenInstIdLocal;
    }

    /**
     * @param screenInstIdLocal the screenInstIdLocal to set
     */
    public void setScreenInstIdLocal(String screenInstIdLocal) {
        this.screenInstIdLocal = screenInstIdLocal;
    }

    /**
     * @return the portletIdLocal
     */
    public String getPortletIdLocal() {
        return portletIdLocal;
    }

    /**
     * @param portletIdLocal the portletIdLocal to set
     */
    public void setPortletIdLocal(String portletIdLocal) {
        this.portletIdLocal = portletIdLocal;
    }

    /**
     * @return the udLocal
     */
    public String getUdLocal() {
        return udLocal;
    }

    /**
     * @param udLocal the UDLocal to set
     */
    public void setUdLocal(String udLocal) {
        this.udLocal = udLocal;
    }

    /**
     * @return the lPLIDLocal
     */
    public String getlPLIDLocal() {
        return lPLIDLocal;
    }

    /**
     * @param lPLIDLocal the lPLIDLocal to set
     */
    public void setlPLIDLocal(String lPLIDLocal) {
        this.lPLIDLocal = lPLIDLocal;
    }

    /**
     * @return the portletInsIdLocal
     */
    public String getPortletInsIdLocal() {
        return portletInsIdLocal;
    }

    /**
     * @param portletInsIdLocal the portletInsIdLocal to set
     */
    public void setPortletInsIdLocal(String portletInsIdLocal) {
        this.portletInsIdLocal = portletInsIdLocal;
    }

    /**
     * @return the portalModeLocal
     */
    public boolean isPortalModeLocal() {
        return portalModeLocal;
    }

    /**
     * @param portalModeLocal the portalModeLocal to set
     */
    public void setPortalModeLocal(boolean portalModeLocal) {
        this.portalModeLocal = portalModeLocal;
    }

    /**
     * @return the selectFRowLocal
     */
    public boolean isSelectFRowLocal() {
        return selectFRowLocal;
    }

    /**
     * @param selectFRowLocal the selectFRowLocal to set
     */
    public void setSelectFRowLocal(boolean selectFRowLocal) {
        this.selectFRowLocal = selectFRowLocal;
    }

    /**
     * @return the EnableC2ALocal
     */
    public boolean isEnableC2ALocal() {
        return EnableC2ALocal;
    }

    /**
     * @param EnableC2ALocal the EnableC2ALocal to set
     */
    public void setEnableC2ALocal(boolean EnableC2ALocal) {
        this.EnableC2ALocal = EnableC2ALocal;
    }

    /**
     * @return the mainLocal
     */
    public boolean isMainLocal() {
        return mainLocal;
    }

    /**
     * @param mainLocal the mainLocal to set
     */
    public void setMainLocal(boolean mainLocal) {
        this.mainLocal = mainLocal;
    }

    /**
     * @return the loggedUserLocal
     */
    public String getLoggedUserLocal() {
        return loggedUserLocal;
    }

    /**
     * @param loggedUserLocal the loggedUserLocal to set
     */
    public void setLoggedUserLocal(String loggedUserLocal) {
        this.loggedUserLocal = loggedUserLocal;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="uploadedImage In Simple mode">
    public void uploadImageSimple() {
        logger.debug("Entering");
        OutputStream out = null;
        try {
            File file = new File(getUploadedFile().getFileName());
            InputStream in = getUploadedFile().getInputstream();
            out = new FileOutputStream(file);
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            OFunctionResult oFR = new OFunctionResult();
            oFR.append(imageService.saveImage(new File(file.getAbsolutePath()),
                    getUploadedFile().getFileName(), getUploadedFile().getContentType(), loggedUser));
            setFileName(file.getName());
            if (oFR.getErrors().size() > 0) {
                logger.warn("Error Saving File");
                oFR.addError(userMessageService.getUserMessage(
                        "SystemInternalError", viewLoggedUser));
            } else {
                logger.trace("Successfully saved uploaded file into DB");
            }
            in.close();
            out.flush();
            out.close();
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        } finally {
            try {
                out.close();
            } catch (IOException ex) {
                logger.error("Exception thrown",ex);
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="uploadedFile">
    private UploadedFile uploadedFile;

    /**
     * @return the uploadedFile
     */
    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    /**
     * @param uploadedFile the uploadedFile to set
     */
    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }
    // </editor-fold>
    // </editor-fold>
}
