
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.bpm.model.Task;
import com.unitedofoq.fabs.bpm.model.status.TaskStatus;
import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.central.OTenant;
import com.unitedofoq.fabs.core.alert.services.AlertServiceLocal;
import com.unitedofoq.fabs.core.cash.CacheServiceLocal;
import com.unitedofoq.fabs.core.comunication.EMailSenderLocal;
import com.unitedofoq.fabs.core.content.AttachmentDTO;
import com.unitedofoq.fabs.core.content.EntityAttachmentServiceLocal;
import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.datatype.ODataTypeAttribute;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.delegation.DelegationServiceRemote;
import com.unitedofoq.fabs.core.encryption.DataEncryptorServiceLocal;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.EntityBaseServiceRemote;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntityServiceLocal;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntityAction;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.export.DataExportRemote;
import com.unitedofoq.fabs.core.filter.OFilter.FilterType;
import com.unitedofoq.fabs.core.filter.OFilterServiceLocal;
import com.unitedofoq.fabs.core.filter.UserSessionInfo;
import com.unitedofoq.fabs.core.function.JavaFunction;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.function.URLFunction;
import com.unitedofoq.fabs.core.function.WebServiceFunction;
import com.unitedofoq.fabs.core.i18n.TextTranslationServiceRemote;
import com.unitedofoq.fabs.core.module.ModuleServiceRemote;
import com.unitedofoq.fabs.core.multimedia.OIMageServiceLocal;
import com.unitedofoq.fabs.core.process.HumanTaskServiceRemote;
import com.unitedofoq.fabs.core.process.ProcessServiceRemote;
import com.unitedofoq.fabs.core.process.TaskFunction;
import com.unitedofoq.fabs.core.report.ComparisonCrossTabReport;
import com.unitedofoq.fabs.core.report.CrossTabReport;
import com.unitedofoq.fabs.core.report.OReport;
import com.unitedofoq.fabs.core.report.ReportFunction;
import com.unitedofoq.fabs.core.report.customReports.CustomReport;
import com.unitedofoq.fabs.core.report.customReports.CustomReportDynamicFilter;
import com.unitedofoq.fabs.core.report.customReports.CustomReportFilter;
import com.unitedofoq.fabs.core.routing.ORoutingBeanLocal;
import com.unitedofoq.fabs.core.routing.ORoutingTask;
import com.unitedofoq.fabs.core.routing.ORoutingTaskAction;
import com.unitedofoq.fabs.core.routing.ORoutingTaskInstance;
import com.unitedofoq.fabs.core.security.SecurityServiceRemote;
import com.unitedofoq.fabs.core.security.user.Inbox;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.security.user.UserServiceRemote;
import com.unitedofoq.fabs.core.session.ScreenSessionInfo;
import com.unitedofoq.fabs.core.setup.FABSSetup;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import com.unitedofoq.fabs.core.timezone.TimeZoneServiceLocal;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.udc.UDCServiceRemote;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;

import static com.unitedofoq.fabs.core.uiframework.backbean.TabularBean.logger;

import com.unitedofoq.fabs.core.uiframework.backbean.filter.RunFilterBean;
import com.unitedofoq.fabs.core.uiframework.backbean.wizard.ScreenWizardInformation;
import com.unitedofoq.fabs.core.uiframework.backbean.wizard.WizardController;
import com.unitedofoq.fabs.core.uiframework.backbean.wizard.WizardPanel;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.navigationmenu.NMDataLoader;
import com.unitedofoq.fabs.core.uiframework.orgchart.OrgChartFunction;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalPage;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalPageLite;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import com.unitedofoq.fabs.core.uiframework.portal.PortalPageFunction;
import com.unitedofoq.fabs.core.uiframework.portal.PortalPagePortlet;
import com.unitedofoq.fabs.core.uiframework.portal.ScreenRenderInfo;
import com.unitedofoq.fabs.core.uiframework.screen.EntityFilterScreenAction;
import com.unitedofoq.fabs.core.uiframework.screen.FormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.InputNotSupportedException;
import com.unitedofoq.fabs.core.uiframework.screen.OBackBeanException;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.OutputNotSupportedException;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenDTMapping;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenDTMappingAttr;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenFilter;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenFunction;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenInput;
import com.unitedofoq.fabs.core.uiframework.screen.TabularScreen;
import com.unitedofoq.fabs.core.uiframework.screen.UDCScreenFunction;
import com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenDTMappingAttrDTO;
import com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenInputDTO;
import com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenOutputDTO;
import com.unitedofoq.fabs.core.uiframework.screen.wizard.Wizard;
import com.unitedofoq.fabs.core.uiframework.screen.wizard.WizardFunction;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.fabs.core.webservice.OWebService;
import java.io.FileInputStream;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;
import javax.faces.validator.MethodExpressionValidator;
import javax.servlet.http.HttpServletRequest;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.commandlink.CommandLink;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.inputtextarea.InputTextarea;
import org.primefaces.component.menuitem.MenuItem;
import org.primefaces.component.overlaypanel.OverlayPanel;
import org.primefaces.component.panel.Panel;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.component.separator.Separator;
import org.primefaces.context.RequestContext;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mibrahim
 */
public abstract class OBackBean extends FacesBean {

    final static Logger logger = LoggerFactory.getLogger(OBackBean.class);

    NMDataLoader nMDataLoader = OEJB.lookup(NMDataLoader.class);

    protected ORoutingBeanLocal routingBeanLocal = OEJB.lookup(ORoutingBeanLocal.class);

    UDCServiceRemote uDCService = OEJB.lookup(UDCServiceRemote.class);

    EMailSenderLocal mailSender = OEJB.lookup(EMailSenderLocal.class);

    DataExportRemote exportService = OEJB.lookup(DataExportRemote.class);

    DataEncryptorServiceLocal dataEncryptorService = OEJB.lookup(DataEncryptorServiceLocal.class);

    EntityAttachmentServiceLocal attachmentService = OEJB.lookup(EntityAttachmentServiceLocal.class);

    protected CacheServiceLocal cacheService = OEJB.lookup(CacheServiceLocal.class);

    LevelsTree entityTreeBrowser = null;
    TreeBrowser fieldsTreeBrowser = null;
    private DefaultTreeNode dynamicSelectedNode;
    //public DynamicNodeUserObject filedsTreeRoot;
    protected String onloadJSStr = "";
    protected boolean invalidInputMode = false;
    private CommandButton addRowBtn = null;
    private boolean disabled = false;
    boolean corporate = false;
    protected StreamedContent exportedFile;
    private StreamedContent exportExcelTemplate;
    private long requesterTenantID;
    protected final String Requester_Tenant_ID = "RequesterTenantID";
    protected HashMap<String, UIComponent> allScreenControls = new HashMap<String, UIComponent>();
    boolean lookupMode = false;

    TaskOptionProcess taskOptionProcess;

    public StreamedContent getExportExcelTemplate() {
        return exportExcelTemplate;
    }

    public void setExportExcelTemplate(StreamedContent exportExcelTemplate) {
        this.exportExcelTemplate = exportExcelTemplate;
    }

    public boolean isLookupMode() {
        return lookupMode;
    }

    public void setLookupMode(boolean lookupMode) {
        this.lookupMode = lookupMode;
    }

    public HashMap<String, UIComponent> getAllScreenControls() {
        return allScreenControls;
    }

    public void setAllScreenControls(HashMap<String, UIComponent> allScreenControls) {
        this.allScreenControls = allScreenControls;
    }

    public long getRequesterTenantID() {
        return requesterTenantID;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getOnloadJSStr() {
        return onloadJSStr;
    }

    public void setOnloadJSStr(String onloadJSStr) {
        this.onloadJSStr = onloadJSStr;
    }

    public UserSessionInfo getUserSessionInfo() {
        logger.debug("Entering");
        UserSessionInfo userSesssionInfo = new UserSessionInfo();
        try {
            userSesssionInfo.setC2A(c2AMode);
            ScreenInput currentScrInput = (ScreenInput) oem.loadEntity(ScreenInput.class.getSimpleName(),
                    Collections.singletonList("dbid=" + currentScreenInput.getDbid()), null, loggedUser);
            userSesssionInfo.setCurrentInput(currentScrInput); // not using dto
            userSesssionInfo.setDataMessage(dataMessage);
            userSesssionInfo.setLoggedUser(loggedUser);
            String entityClassPath = entitySetupService.getOScreenEntityDTO(
                    currentScreen.getDbid(), loggedUser).getEntityClassPath();
            userSesssionInfo.setOactOnEntity(entitySetupService.loadOEntity(entityClassPath, loggedUser));
            userSesssionInfo.setSessionAttributes(getSessionMap());
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return userSesssionInfo;
    }
    private boolean toolTipRender = true;

    abstract protected List<BaseEntity> getLoadedEntity();

    public boolean isToolTipRender() {
        return toolTipRender;
    }

    public void setToolTipRender(boolean toolTipRender) {
        this.toolTipRender = toolTipRender;
    }
    //<editor-fold defaultstate="collapsed" desc="Static Fields!">
    final static String PASSEDPARAMETER_VAR = "passedParameter";
    final static String LOOKUP_VAR = "looupParam";
    final static String LOOKUP_SCREEN_NAME = "lookupScreenName";
    final static String LOOKUP_CALLER_SCREEN_NAME = "lookupCallerScreenName";
    public final static int CNTRL_INPUTMODE = 0;
    public final static int CNTRL_OUTPUTMODE = 1;
    public final static int POPUP_LOOKUPTYPE = 0;
    public final static int POPUP_BALLOONTYPE = 1;
    public final static int POPUP_SCREENTYPE = 2;
    public final static String MODE_VAR = "ScreenModeVar";
    public final static String WIZARD_VAR = "WizardVar";
    public final static String WIZARD_CRTL_VAR = "WizardControlVar";
    public final static String WIZARD_INFO_VAR = "WizardInfoVar";
    final static String SCREENNAME_VAR = "screenName";
    //RDO
    public final static String PORTAL_PAGE = "PortalPage";
    public final static int ARABIC_VAR = 24;
    public final static int ENGLISH_VAR = 23;
    final static String LOOKUP_ENTITY_VAR = "LOOKUP_ENTITY_VAR";
    public final static String FILTER_VAR = "FILTER_VAR";
    /**
     * List of screen names of screens that have standalone portlet, and not
     * displayed in the common portlet (OTMSPortlet). By default don't take
     * message input.
     */
    public final static List standalonePortletScreens
            = Arrays.asList("OTMSMenu", "Inbox", "UDCType", "DocMenu", "EmployeeViewr");
    /**
     * Counts of calling {@link #init() function
     */
    protected long initCallCount = 0;

    public String getHtmlDir() {
        return htmlDir;
    }

    public void setHtmlDir(String htmlDir) {
        this.htmlDir = htmlDir;
    }

    public String getUserLanguage() {
        return screenLanguage;
    }

    public void setUserLanguage(String userLanguage) {
        this.screenLanguage = userLanguage;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Protected Fields which is Seen by Children Classes">
    protected boolean isFileSelected;
    protected boolean exit = true;
    protected boolean prsnlzMode;
    /**
     * Screen Mode, values are got from {@link OScreen}, initialized with
     * {@link OScreen#SM_NOMODE}
     */
    protected long mode = OScreen.SM_NOMODE;
    protected boolean supportedInput;
    protected List<UIComponent> filterControls;
    public boolean wizardMode;
    protected WizardController wizardCtrl;
    //The name of this control is used in the bindings in the WizardPanel class
    protected ScreenWizardInformation wizardInfo;
    protected WizardPanel wizardPanel;
    //RDO
    protected OPortalPageLite myPage;
    protected boolean showSaveExitButton = true;
    protected boolean showSaveButton = true;
    long prevThread = 0;
    // <editor-fold defaultstate="collapsed" desc="messagesPanel & functions">
    /**
     * The name of this control is used in the binding of the UserMessage class
     * <br> To change its name you have to go to the UserMessage class and
     * change the binding also
     */
    protected UserMessagePanel messagesPanel;

    /**
     * Return {@link #messagesPanel}
     */
    public UserMessagePanel getMessagesPanel() {
        // This work-arround is here because this getter gets called twice with
        // every action with the screen, which causes the messages log to appear twice
        // FIXME: Replace this workarround with another one because sometimes
        // the System uses the same Thread again
        if (Thread.currentThread().getId() == prevThread) {
            return messagesPanel;
        }
        prevThread = Thread.currentThread().getId();
        // Get the messages list and pass it to OLog.logOFR function,
        // because i cannot pass a UserMessagePanel object to OLog
        ArrayList<UserMessage> messagesList = (ArrayList<UserMessage>) messagesPanel.getMessages();
        return messagesPanel;
    }

    /**
     * Set {@link #messagesPanel}
     */
    public void setMessagesPanel(UserMessagePanel messagesPanel) {
        this.messagesPanel = messagesPanel;
    }
    // </editor-fold>

    public List<UIComponent> getFilterControls() {
        return filterControls;
    }

    public void setFilterControls(List<UIComponent> filterControls) {
        this.filterControls = filterControls;
    }
    final static String HAT_VAR = "HATVar";

    protected FABSSetupLocal fABSSetupLocal = OEJB.lookup(FABSSetupLocal.class);

    protected UIFrameworkServiceRemote uiFrameWorkFacade = OEJB.lookup(UIFrameworkServiceRemote.class);

    protected OIMageServiceLocal oimageServiceLocal = OEJB.lookup(OIMageServiceLocal.class);

    protected OEntityManagerRemote oem = OEJB.lookup(OEntityManagerRemote.class);

    protected EntitySetupServiceRemote entitySetupService = OEJB.lookup(EntitySetupServiceRemote.class);

    protected SecurityServiceRemote securityService = OEJB.lookup(SecurityServiceRemote.class);

    protected HumanTaskServiceRemote humanTaskService = OEJB.lookup(HumanTaskServiceRemote.class);

    protected ProcessServiceRemote processService = OEJB.lookup(ProcessServiceRemote.class);

    protected DelegationServiceRemote delegationService = OEJB.lookup(DelegationServiceRemote.class);

    protected ORoutingBeanLocal routingService = OEJB.lookup(ORoutingBeanLocal.class);

    protected DataTypeServiceRemote dataTypeService = OEJB.lookup(DataTypeServiceRemote.class);

    protected DDServiceRemote ddService = OEJB.lookup(DDServiceRemote.class);

    protected EntityServiceLocal entityServiceLocal = OEJB.lookup(EntityServiceLocal.class);

    protected ModuleServiceRemote moduleService = OEJB.lookup(ModuleServiceRemote.class);

    protected UserMessageServiceRemote usrMsgService = OEJB.lookup(UserMessageServiceRemote.class);

    protected EntityBaseServiceRemote entityBaseService = OEJB.lookup(EntityBaseServiceRemote.class);
    //This is falg to indicate if the screen in portal mode or not
    protected boolean portalMode;
    protected boolean openedAsPopup;
    protected boolean processMode;
    protected boolean routingMode;
    // protected Task Information Variable
    //C2A Variables
    protected boolean c2AMode;
    // This field is used only in case I'm working on the C2AMode
    List<ScreenRenderInfo> toBeRenderScreens;

    public boolean isWizardMode() {
        return wizardMode;
    }

    public WizardController getWizardCtrl() {
        return wizardCtrl;
    }

    public void setWizardCtrl(WizardController wizardCtrl) {
        this.wizardCtrl = wizardCtrl;
    }

    public ScreenWizardInformation getWizardInfo() {
        return wizardInfo;
    }

    public void setWizardInfo(ScreenWizardInformation wizardInfo) {
        this.wizardInfo = wizardInfo;
    }

    public void setWizardMode(boolean wizardMode) {
        this.wizardMode = wizardMode;
    }

    public boolean isC2AMode() {
        return c2AMode;
    }

    public void setC2AMode(boolean c2AMode) {
        this.c2AMode = c2AMode;
    }
    protected Task processingTask;
    protected Inbox currentInbox;
    protected ArrayList<String> errorList;
    protected ArrayList<String> warnList;
    protected ArrayList<String> fatalList;
    protected ArrayList<String> infoList;
    protected ArrayList<String> successList;
    protected List<ORoutingTaskAction> routingTaskActions;
    protected ORoutingTask routingTask;
    protected String routingTaskInstID;
    protected String portletId;
    protected OUser loggedUser;
    protected String screenLanguage;
    protected String htmlDir;
    protected OUser systemUser = null;
    protected OutboxPage outboxPage;
    // This variable will contain object of the screen
    protected OScreen currentScreen;
    protected String passedParameter;
    protected UDC screenMode;
    protected long hat;
    private String portletInsId;
    // This variable has the name of the screen instance
    protected String screenInstId;
    private String screenHeight;
    protected String nextRequestStatus;
    protected String routingTaskComment;

    public long getHat() {
        return hat;
    }

    public void setHat(long hat) {
        this.hat = hat;
    }
    TreeBrowser entityFieldsTreeBrowser = null;
    //LevelsTree entityTreeBrowser = null;
    // object reference to expand node
    protected HashMap<String, Object> screenParamsMap = null;
    protected String layoutPlid = null;
    protected String pageId = null;
    protected boolean rerender = false;
    protected boolean restorePDataFromSessoion = false;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Class Private Fields!">
    protected Map<String, String> uniqueFieldsQueries = new HashMap<String, String>();
    protected Map<String, ExpressionFieldInfo> uniqueFields = new HashMap<String, ExpressionFieldInfo>();
    protected List<String> lookupMandatoryFields = new ArrayList<String>();
    protected Lookup fieldLookup;
    protected List<String> dataSort;

    public List<String> getDataSort() {
        return dataSort;
    }

    public void setDataSort(List<String> dataSort) {
        this.dataSort = dataSort;
    }
    private Panel collapsiblePanel;

    public Panel getCollapsiblePanel() {
        return collapsiblePanel;
    }

    public void setCollapsiblePanel(Panel collapsiblePanel) {
        this.collapsiblePanel = collapsiblePanel;
    }
    private HtmlInputHidden recordIDhidden;

    public HtmlInputHidden getRecordIDhidden() {
        return recordIDhidden;
    }

    public void setRecordIDhidden(HtmlInputHidden recordIDhidden) {
        this.recordIDhidden = recordIDhidden;
    }
    private HtmlInputHidden hidden; //TODO: set to meaningful name

    public HtmlInputHidden getHidden() {
        return hidden;
    }

    public void setHidden(HtmlInputHidden hidden) {
        this.hidden = hidden;
    }
    private HashMap selectItemsList = new HashMap();
    List<String> myGroups = new ArrayList<String>();
    protected String statusButtons;
    protected List<UserMessage> messages;
    public ExpressionFactory exFactory;
    public ELContext elContext;
    // <editor-fold defaultstate="collapsed" desc="balloons">
    HashMap<String, Balloon> balloons = new HashMap<String, Balloon>();

    public HashMap<String, Balloon> getBalloons() {
        return balloons;
    }

    public void setBalloons(HashMap<String, Balloon> balloons) {
        this.balloons = balloons;
    }
    // </editor-fold>

    public ELContext getElContext() {
        return elContext;
    }

    public void setElContext(ELContext elContext) {
        this.elContext = elContext;
    }

    public ExpressionFactory getExFactory() {
        return exFactory;
    }

    public void setExFactory(ExpressionFactory exFactory) {
        this.exFactory = exFactory;
    }

    public List<UserMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<UserMessage> messages) {
        this.messages = messages;
    }

    public String getStatusButtons() {
        return statusButtons;
    }

    public void setStatusButtons(String statusButtons) {
        this.statusButtons = statusButtons;
    }

    //</editor-fold>
    public OBackBean() {
        // <editor-fold defaultstate="collapsed" desc="Log">
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>

        screenParamsMap = new HashMap<String, Object>(8);
        exFactory = getApplication().getExpressionFactory();
        elContext = getFacesContext().getELContext();
        messages = new ArrayList<UserMessage>();
    }

    protected OCentralEntityManagerRemote centralOEM = OEJB.lookup(OCentralEntityManagerRemote.class);

    protected OFunctionServiceRemote functionServiceRemote = OEJB.lookup(OFunctionServiceRemote.class);

    protected OFilterServiceLocal filterServiceBean = OEJB.lookup(OFilterServiceLocal.class);

    protected TextTranslationServiceRemote textTranslationServiceRemote = OEJB.lookup(TextTranslationServiceRemote.class);

    protected TimeZoneServiceLocal timeZoneService = OEJB.lookup(TimeZoneServiceLocal.class);

    // <editor-fold defaultstate="collapsed" desc="init() functions">
    /**
     * Backbean Life cycle function. First thing called by
     * {@link OBackBean#init()}. Returning false means suppress further
     * initialization. Override it to do your validations.
     *
     * @return OBackBean function does nothing and returns true
     */
    protected boolean preInit(Map<String, String> requestParams) {
        return true;
    }
    final ScreenSessionInfo info = new ScreenSessionInfo();

    /**
     * Backbean Life cycle function. Called by {@link OBackBean#init()} after
     * calling {@link OBackBean#preInit()} successfully. Returning false means
     * suppress further initialization. Override it to do your initializations.
     *
     * @return
     *
     */
    protected static Map<String, String> liferayUsers = new HashMap<String, String>();

    public static int count(String userId) {
        // Bypassing function, due to FABS-268
//        Set<String> sessions = (Set) MultiVMPoolUtil.getCache("customAuthCache").get("sessionsCache" + userId);
//        int count = sessions == null ? 0 : sessions.size();
//        return count;
        return 1;
    }

    protected boolean onInit(Map<String, String> requestParams) {
        logger.trace("Entering");
        String userId = requestParams.get("UD");
        if (userId == null) {
            userId = SessionManager.getSessionAttribute("USERID").toString();
        }
        if (userId == null || "".equals(userId)) {
            logger.warn("Invalid UD Request Parameter");
            logger.trace("Returning with False");
            return false;
        }
        if (!OPortalUtil.isUserLoggedIn(Long.valueOf(userId)) || OPortalUtil.getCurrentLayout() == null) {
            return false;
        }
        HttpServletRequest request = (HttpServletRequest) SessionManager.getExternalContext().getRequest();
        if (!OPortalUtil.isUserAuthorized(request, userId)) {
            return false;
        }
        layoutPlid = (String) requestParams.get("lPLID");
        pageId = OPortalUtil.portalPageID;
        if (layoutPlid != null) {
            SessionManager.addSessionAttribute("layoutPLID", layoutPlid);
        }

        String screenName = (String) requestParams.get("screenName");
        int singleQuoteIndex = screenName.indexOf("\'");
        if (singleQuoteIndex != -1) {
            screenName = screenName.substring(0, singleQuoteIndex);
        }
        screenInstId = ((String) requestParams.get("screenInstId"));
        setScreenHeight((String) requestParams.get("screenHeight"));

        if (screenInstId == null) {
            logger.warn("Null screenInstanceID");
            logger.trace("Returning with False");
            return false;
        }
        String loginName = OPortalUtil.getUserLoginName(Long.parseLong(userId));
        if (!liferayUsers.containsKey(loginName)) {
            liferayUsers.put(loginName, userId);
        }
        if (loginName == null) {
            logger.warn("Can Not Initialize With No User, Please Sign On, This Error in ObackBean Class");
            logger.trace("Returning with False");
            return false;
        }
        SessionManager.addSessionAttribute("userID", loginName);
        OTenant loginNameTenant = null;
        try {
            loginNameTenant = centralOEM.getUserTenant(loginName);
            if (loginNameTenant == null) {
                // FIXME: display corresponding user error message
                logger.trace("Returning with False");
                return false;
            }
            OUser tenantGrantedUser = centralOEM.getGrantedUser(loginNameTenant);
            tenantGrantedUser.setLoginName(loginName);
            try {
                oem.getEM(tenantGrantedUser);
                loggedUser = oem.getUser(loginName, loginNameTenant);
                if (loggedUser == null) {
                    logger.warn("Null Logged User");
                    logger.trace("Returning with False");
                    return false;
                }
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
                logger.trace("Returning with False");
                return false;
            } finally {
                logger.trace("Returning");
                oem.closeEM(tenantGrantedUser);
            }
            screenLanguage = loggedUser.getFirstLanguage().getValue();
            htmlDir = (loggedUser.isRtl()) ? "rtl" : "ltr";
            oem.closeEM(tenantGrantedUser);

            if (loggedUser == null) {
                // FIXME: display corresponding user error message
                logger.trace("Returning with False as logged user equals Null");
                return false;
            }

            systemUser = oem.getSystemUser(loggedUser);
            if (systemUser == null) {
                logger.warn("Error Getting Granted User, systemUser in Null");
            }

        } catch (Exception ex) {
            // Probably Database connection error
            logger.error("Exception thrown", ex);
            // FIXME: display corresponding user error message
            logger.trace("Returning with False");
            return false;
        }

        messagesPanel = new UserMessagePanel(exFactory, elContext, getBeanName(), ddService, this, loggedUser);
        if (requestParams.get("EnableC2A") != null) {
            c2AMode = "true".equalsIgnoreCase(requestParams.get("EnableC2A"));
        }
        if (requestParams.get("main") != null) {
            showManagePortlePage = "true".equalsIgnoreCase(requestParams.get("main"));
        }
        if (requestParams.get("portletId") != null) {
            portletId = requestParams.get("portletId").toString();
        }
        if (requestParams.get("portletInsId") != null) {
            portletInsId = (String) requestParams.get("portletInsId".toString());
            int percentageIndex = portletInsId.indexOf("%");
            if (percentageIndex != -1) {
                portletInsId = portletInsId.substring(0, percentageIndex);
            }
            int spaceIndex = portletInsId.indexOf(" ");
            if (spaceIndex != -1) {
                portletInsId = portletInsId.substring(0, spaceIndex);
            }
            portletInsId = portletInsId.replaceAll("[^a-zA-Z0-9_]", "");
        }
        if (requestParams.get("portalMode") != null) {
            portalMode = "true".equalsIgnoreCase(requestParams.get("portalMode"));
        }
        if (requestParams.get("isPopup") != null) {
            openedAsPopup = "true".equalsIgnoreCase(requestParams.get("isPopup"));
        }
        if (requestParams.get("reloadFromSession") != null) {
            restorePDataFromSessoion = true;
        }

        if (screenName == null) {
            screenName = portletId;
        }

        try {
            oem.getEM(systemUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }

        if (screenName.equals("DockMenu")) {
            logger.trace("Returning with False");
            return false;
        }

        FABSSetup corporateEnabled = fABSSetupLocal.loadKeySetup("EnableCorporate", loggedUser);
        if (corporateEnabled != null
                && (corporateEnabled.getSvalue().equalsIgnoreCase("t")
                || corporateEnabled.getSvalue().equalsIgnoreCase("true")
                || corporateEnabled.getSvalue().equalsIgnoreCase("on")
                || corporateEnabled.getSvalue().equalsIgnoreCase("1"))) {
            //Used In Inbox
            corporate = true;
        }

        //FIXME location to be revisited
        if (getScreenParam(Requester_Tenant_ID) != null && corporate) {
            requesterTenantID = Long.parseLong(getScreenParam(Requester_Tenant_ID).toString());
            loggedUser.setWorkingTenantID(requesterTenantID);
            systemUser.setWorkingTenantID(requesterTenantID);
        }

        currentScreen = getScreenObject(screenName);

        //RDO
        myPage = (OPortalPageLite) getScreenParam(PORTAL_PAGE);
        prsnlzMode = currentScreen.isPrsnlzd();

        if (getScreenParam("inboxID") != null) {
            inboxID = (String) getScreenParam("inboxID");
        }

        if (getScreenParam("PassedParameter") != null) {
            passedParameter = (String) getScreenParam("PassedParameter");
        }

        // Check if current screen is opened via balloon/Menu,
        // or opened in a portal page
        if (getScreenParam("showSaveExit") == null) {
            showSaveExitButton = !c2AMode;
        } else {
            showSaveExitButton = (Boolean) getScreenParam("showSaveExit");
        }

        // IF in C2A Mode get the list of screens to be rendered.
        if (c2AMode) {
            toBeRenderScreens = (List<ScreenRenderInfo>) getScreenParam("PortletPagePortlet");
            if (null != toBeRenderScreens) {
                SessionManager.setScreenSessionAttribute(getPortletInsId(),
                        OPortalUtil.TOBERENDREDSCREENS, toBeRenderScreens);
            } else {
                HashMap<String, Object> sessionAttribute
                        = (HashMap<String, Object>) SessionManager.getSessionAttribute(currentScreen.getName());

                if (null == sessionAttribute || sessionAttribute.isEmpty()) {
                    sessionAttribute = (HashMap<String, Object>) nMDataLoader.getScreensSessionInfo().get(currentScreen.getName());
                }
                if (sessionAttribute != null) {
                    screenParamsMap.putAll(sessionAttribute);
                    toBeRenderScreens = (List<ScreenRenderInfo>) screenParamsMap.get("PortletPagePortlet");
                }
                if (null == toBeRenderScreens) {
                    HashMap<String, Object> screenParamMap = SessionManager.getScreenSessionAttribute(getPortletInsId());
                    toBeRenderScreens = (null == screenParamMap) ? null
                            : (List<ScreenRenderInfo>) screenParamMap.get(OPortalUtil.TOBERENDREDSCREENS);
                }
            }
        }

        if (requestParams.get("isLookup") != null
                && requestParams.get("isLookup").toString().equals("true")) {
            screenParamsMap.put("isLookup", Boolean.TRUE);
            currentScreen.setShowHeader(false);
            lookupMode = true;
            logger.trace("Enforced Header Hiding For Lookup");

        } else {
            screenParamsMap.put("isLookup", Boolean.FALSE);
        }

        if (getScreenParam(FILTER_VAR) != null) {
            getCurrentScreen().setScreenFilter((ScreenFilter) getScreenParam(FILTER_VAR));
        }

        for (int i = 0; i < myGroups.size(); i++) {
            if (myGroups != null) {
                logger.trace("My Group: {}", myGroups.get(i));
            }
            if (getScreenParam(myGroups.get(i)) != null) {
                groupName = myGroups.get(i).toString();
            }
        }

        if (groupName != null && getScreenParam(groupName) != null) {
            ODataMessage message = (ODataMessage) getScreenParam(groupName);
            dataMessage = message;
            rerender = true;
        } else {
            List<String> OrgChartscreensInPage = (List<String>) SessionManager.getSessionAttribute("OrgChartScreensInPage");
            if (OrgChartscreensInPage != null && !OrgChartscreensInPage.isEmpty()) {
                for (String screenInst : OrgChartscreensInPage) {
                    if (screenInst.contains(screenName)) {
                        screenInstId = screenInst;
                        break;
                    }
                }
            }
            ODataMessage message = (ODataMessage) getScreenParam(OPortalUtil.MESSAGE);
            if (message != null) {
                if (message.getODataType().getDbid() == 5 || message.getODataType().getDbid() == 4) {
                    dataMessage = message;
                } else {
                    dataMessage = message;
                    rerender = false;
                }
            } else // dataMessage is null
            {
                if (!standalonePortletScreens.contains(currentScreen.getName())) {
                    logger.warn("No Message Passed In: {}", OPortalUtil.MESSAGE);
                }
            }
        }
        boolean isOrg = false;
        if (null != requestParams.get("isOrg")) {
            isOrg = "true".equalsIgnoreCase(requestParams.get("isOrg"));
            if (isOrg) {
                String portletId = ((String) requestParams.get("portletId"));

                String entityDBID = ((String) requestParams.get("entityDBID"));

                String funId = ((String) requestParams.get("funDBID"));

                List data = new ArrayList();

                OFunction function = null;
                try {
                    function = (OFunction) this.oem.loadEntity(OFunction.class.getSimpleName(), Long.parseLong(funId), null, null, loggedUser);
                } catch (Exception e) {

                }
                ODataType dataType = null;
                if ((function instanceof ScreenFunction)) {
                    ScreenFunction sf = (ScreenFunction) function;
                    dataType = sf.getOdataType();
                    if ((dataType.getDbid() == 3L) || (dataType.getDbid() == 8L) || (dataType.getDbid() == 1L)) {
                        data.add(Long.valueOf(Long.parseLong(entityDBID)));
                        data.add(loggedUser);
                        data.add("OrgChart");
                        dataMessage = new ODataMessage(dataType, data);
                        SessionManager.setScreenSessionAttribute(screenInstId, "SCREEN_INPUT", dataMessage);
                        SessionManager.setScreenSessionAttribute(screenInstId, "ScreenModeVar", Long.valueOf(sf.getScreenMode().getDbid()));
                    } else {
                        try {
                            String entityName = dataType.getName();
                            BaseEntity ent = this.oem.loadEntity(entityName, Long.parseLong(entityDBID), null, null, loggedUser);
                            List odmData = new ArrayList();
                            odmData.add(ent);
                            odmData.add(loggedUser);
                            dataMessage = new ODataMessage(dataType, odmData);
                            SessionManager.setScreenSessionAttribute(screenInstId, "SCREEN_INPUT", dataMessage);
                            SessionManager.setScreenSessionAttribute(screenInstId, "ScreenModeVar", Long.valueOf(sf.getScreenMode().getDbid()));
                            mode = Long.valueOf(sf.getScreenMode().getDbid());
                        } catch (Exception ex) {
                        }
                    }

                }
            }

            logger.trace("currentScreen: {}, dataMessage: {}", currentScreen, dataMessage);

        }
        // </editor-fold>

        currentInbox = (Inbox) cacheService.getFromCache("currentInbox", loggedUser);

        String screenProcessMode = (String) cacheService.getFromCache("screenProcessMode", loggedUser);
        if (currentInbox != null && screenProcessMode != null) {

            Task task = currentInbox.getTask();
            TaskFunction taskFunction = currentInbox.getFunction();

            if (!screenProcessMode.equals(currentScreen.getName())) {
                processMode = false;
            } else if (task != null) {

                logger.trace("Screen Run In Portal Mode, Task: {}", task.getNameDD());
                processMode = true;

                processingTask = task;

                if (taskFunction != null) {
                    mode = taskFunction.getScreenMode();
                }
                SessionManager.setScreenSessionAttribute(screenInstId, MODE_VAR, mode);

                dataMessage = humanTaskService.getTaskInputs(task, loggedUser, taskFunction.getDataTypeName());

                cacheService.cache(MODE_VAR, mode, loggedUser);
                cacheService.cache(OPortalUtil.MESSAGE, dataMessage, loggedUser);
                // here manage M2M Input in Process Mode
                //TO-DO
                SessionManager.setScreenSessionAttribute(screenInstId, OPortalUtil.MESSAGE, dataMessage);
            }
        } else {
            processMode = false;
        }

        if (getScreenParam(OUTBOX_PROCESS_INS) != null) {
            outboxProcessId = (String) getScreenParam(OUTBOX_PROCESS_INS);
        }
        if (getScreenParam(OUTBOX_TASK_INS) != null) {
            outboxTaskId = (String) getScreenParam(OUTBOX_TASK_INS);
        }

        // <editor-fold defaultstate="collapsed" desc="routing Mode">
        if (getScreenParam("routingTaskInstID") != null && getScreenParam("screenRoutingMode") != null) {
            String screenRoutingMode = (String) getScreenParam("screenRoutingMode");
            routingTaskInstID = (String) getScreenParam("routingTaskInstID");
            if (!screenRoutingMode.equals(currentScreen.getName())) {
                routingMode = false;
            } else {
                try {
                    logger.trace("Screen Run In Routing Mode, routingTaskInstID: {}", routingTaskInstID);
                    routingMode = true;

                    ORoutingTaskInstance taskInstance = (ORoutingTaskInstance) oem.loadEntity(ORoutingTaskInstance.class.getSimpleName(),
                            Long.parseLong(routingTaskInstID), null, null, loggedUser);
                    ODataMessage taskInfoMsg = new ODataMessage();
                    List routingData = new ArrayList();

                    routingData.add(routingTaskInstID);
                    taskInfoMsg.setData(routingData);
                    // ofr has routing task and list of routing actions
                    OFunctionResult routingOFR = routingService.getRoutingTaskInfo(taskInfoMsg, null, loggedUser);
                    if (routingOFR.getReturnValues() != null && !routingOFR.getReturnValues().isEmpty()) {
                        routingTask = ((ORoutingTaskInstance) routingOFR.getReturnValues().get(0)).getOroutingTask();
                        routingTaskActions = (List<ORoutingTaskAction>) routingOFR.getReturnValues().get(1);
                    }
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }
            }
        } else {
            routingMode = false;
        }
        // </editor-fold>

        if (getScreenParam(WIZARD_VAR) != null) {
            wizardMode = Boolean.parseBoolean(getScreenParam(WIZARD_VAR).toString());
            SessionManager.setScreenSessionAttribute(screenInstId, WIZARD_VAR, wizardMode);
        }

        if (wizardMode) {
            wizardCtrl = (WizardController) getScreenParam(WIZARD_CRTL_VAR);
            wizardInfo = (ScreenWizardInformation) getScreenParam(WIZARD_INFO_VAR);
            dataMessage = wizardCtrl.getStepDataMessage();
            SessionManager.setScreenSessionAttribute(screenInstId, WIZARD_CRTL_VAR, wizardCtrl);
            SessionManager.setScreenSessionAttribute(screenInstId, WIZARD_INFO_VAR, wizardInfo);
            SessionManager.setScreenSessionAttribute(screenInstId, "SCREEN_INPUT", dataMessage);

        }

        // <editor-fold defaultstate="collapsed" desc="ScreenSessionInfo Management">
        if (dataMessage == null) {
            List<ScreenSessionInfo> sessionInfos = uiFrameWorkFacade.getScreenSessionInfo(
                    loggedUser.getDbid(), screenInstId, loggedUser);
            if (sessionInfos != null && sessionInfos.size() > 0) {
                try {
                    dataMessage = dataTypeService.deSerializeDataMessage(sessionInfos.get(sessionInfos.size() - 1).getDataMessage(), loggedUser);
                    // deSerializeDataMessage reloads Base Entities to be Managed (so any getter will work properly)

                    mode = sessionInfos.get(sessionInfos.size() - 1).getScreenMode();
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                    // FIXME: User message?
                }
            }
        } else {
            // dataMessage is not null
            // Save ScreenSessionInfo
            logger.trace("Saving ScreenSessionInfo");
            //TODO: pass all the parameters to saveScreenSessionInfo instead
            // of initializing info here, to isolate this code from the OBackBean

            info.setOpenDate(timeZoneService.getUserCDT(loggedUser));
            info.setOpenedScreen(currentScreen);
            info.setLoggedUser(loggedUser);
            info.setDataMessage(dataTypeService.serializeDataMessage(dataMessage, loggedUser));
            info.setScreenMode(mode);
            info.setScreenInstId(screenInstId);
            try {
                new Thread() {
                    @Override
                    public void run() {
                        uiFrameWorkFacade.saveScreenSessionInfo(info, loggedUser);
                    }
                }.start();

            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
        }
        setPrintable(currentScreen.isPrintable());
        initCurrentScreenInput();
        initCompositeInputInfo();

        oem.closeEM(systemUser);
        logger.trace("Returning with True");
        return true;
    }

    /**
     * Backbean Life cycle function. Called by {@link OBackBean#init()}. After
     * all initializations are done by {@link #onInit(java.util.Map)}. Override
     * it to do your post initialization code and before exiting JSF
     *
     * @PostConstruct event
     *
     * OBackBean function does nothing
     */
    protected void postInit(Map<String, String> requestParams) {
    }
    /**
     * Implements JSF
     *
     * @PostConstruct
     */
    String inboxID;

    @PostConstruct
    public void init() {
        // <editor-fold defaultstate="collapsed" desc="Log">
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>

        initCallCount++;
        if (initCallCount == 2) // init is called twice for no clear reason and repeats all the same
        // functions, so, no stop second call processing to save time
        {
            return;
        }

        // Get the requestParameters
        Map<String, String> requestParameters = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
//requesParam.setValue(Jsoup.clean(requesParam.getValue().toString(), Whitelist.basic()));
        //Clean the parameter map
        Map<String, String> requestParams = new HashMap<String, String>();
        for (Map.Entry requesParam : requestParameters.entrySet()) {
            requestParams.put(requesParam.getKey().toString(), Jsoup.clean(requesParam.getValue().toString(), Whitelist.basic()));
        }

        // Call preInit() and validate its return
        if (!preInit(requestParams)) {
            return;
        }

        // Call onInit() and validate its return
        if (!onInit(requestParams)) {
            return;
        }

        // Call postInit()
        postInit(requestParams);

        taskOptionProcess = new TaskOptionProcess(this, loggedUser, processingTask, getElContext(), getExFactory());
    }
    // </editor-fold>

    protected UserServiceRemote userServiceRemote = OEJB.lookup(UserServiceRemote.class);
    // <editor-fold defaultstate="collapsed" desc="currentScreenInput">
    /**
     * <br>Is set in {@link OBackBean#validateInputSetMapping()}. <br>Is not
     * guaranteed to be of
     */
    protected ScreenInputDTO currentScreenInput;

    public ScreenInputDTO getCurrentScreenInput() {
        return currentScreenInput;
    }

    public void setCurrentScreenInput(ScreenInputDTO currentScreenInput) {
        this.currentScreenInput = currentScreenInput;
    }
    // </editor-fold>
    ODataMessage dataMessage;
    String groupName;

    public ODataMessage getDataMessage() {
        return dataMessage;
    }

    public void setDataMessage(ODataMessage dataMessage) {
        this.dataMessage = dataMessage;
    }

    public HtmlPanelGrid createRoutingPanel() {
        logger.debug("Entering");
        HtmlPanelGrid routingPanel = new HtmlPanelGrid();

        routingPanel.setId("routingPanel");

        routingPanel.setStyle("position:relative");
        //<editor-fold defaultstate="collapsed" desc="buttons">
        HtmlPanelGrid buttonsGrid = new HtmlPanelGrid();
        buttonsGrid.setColumns(routingTaskActions.size());
        for (ORoutingTaskAction action : routingTaskActions) {

            //draw buttons binded with SingleEnityBean **routingTaskButtonAction**
            //method and set button value with action name, set id according to method retrieving
            Class[] parms3 = new Class[]{ActionEvent.class};
            taskOptionProcess.addButtonToPanelGroup("routingButtonAC_" + action.getDbid(),
                    action.getName(), "routingTaskButtonAction", buttonsGrid,
                    parms3, getPortletInsId(), getBeanName(), getScreenLanguage(), isInvalidInputMode(), getCurrentScreen());
        }
        routingPanel.getChildren().add(buttonsGrid);
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="comment">
        if (routingTask.isShowComment()) {
            //create comment textarea
            addTextAreaToPanelGroup(routingPanel);
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="separator">
        addSeparatorToPanelGroup(routingPanel);
        //</editor-fold>
        logger.debug("Returning");
        return routingPanel;
    }

    protected void addSeparatorToPanelGroup(HtmlPanelGrid routingPanel) {
        HtmlPanelGrid separatorGrid = new HtmlPanelGrid();
        separatorGrid.setId(getFacesContext().getViewRoot().createUniqueId() + "alertSeparator");
        Separator separator = new Separator();
        separatorGrid.getChildren().add(separator);
        routingPanel.getChildren().add(separatorGrid);
    }

    public String getRoutingTaskComment() {
        return routingTaskComment;
    }

    public void setRoutingTaskComment(String routingTaskComment) {
        this.routingTaskComment = routingTaskComment;
    }

    protected void addTextAreaToPanelGroup(HtmlPanelGrid panelLayout) {

        HtmlPanelGrid commentGrid = new HtmlPanelGrid();
        commentGrid.setColumns(2);
        //Create Comment Label
        HtmlOutputLabel commentLBL = new HtmlOutputLabel();
        commentLBL.setStyleClass("labelTxt");
        commentLBL.setStyle("color:black; width:50%");
        commentLBL.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_commentLBL");
        commentLBL.setValue("Comment");
        commentGrid.getChildren().add(commentLBL);
        //Create Comment Label Value
        InputTextarea commentTextarea = new InputTextarea();
        commentTextarea.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_commentLBLVal");
        commentTextarea.setImmediate(true);
        commentTextarea.setStyleClass("OTMSTextAreaBox");
        commentTextarea.setValueExpression("value",
                exFactory.createValueExpression(elContext,
                        "#{" + getBeanName() + ".routingTaskComment }", String.class));
        commentTextarea.setValueExpression("binding", exFactory.createValueExpression(
                elContext, "#{" + getBeanName() + ".routingTaskComment}", String.class));
        commentGrid.getChildren().add(commentLBL);
        commentGrid.getChildren().add(commentTextarea);

        panelLayout.getChildren().add(commentGrid);

        //</editor-fold>
    }

    protected OScreen getScreenObject(String screenName) {
        logger.trace("Entering");
        try {
            OScreen oScreen;
            oScreen = uiFrameWorkFacade.getScreenObject(screenName, systemUser);
            return oScreen;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Returning with Null");
            return null;
        }
    }

    private HtmlPanelGrid processPanelGrid;

    public HtmlPanelGrid getProcessPanelGrid() {
        logger.debug("Entering");
        //   Add Process Panel
        //   BEGIN IF processMode IS true
        HtmlPanelGrid grid = new HtmlPanelGrid();
        if (processMode) {
            //   ADD createProcessPanel To generalPanel
            logger.trace("Creating process panel for Tabular screen");
            HtmlPanelGrid processPanel = taskOptionProcess.createProcessPanel(outboxProcessId, getPortletInsId(), getBeanName(), getScreenLanguage(), isInvalidInputMode(), getCurrentScreen());
            grid.getChildren().add(processPanel);
            //   END IF
        }
        logger.debug("Returning");
        return grid;
    }

    public void setProcessPanelGrid(HtmlPanelGrid processPanelGrid) {
        this.processPanelGrid = processPanelGrid;
    }
    //protected BaseEntity parentObject; replaced by headerObject
    // <editor-fold defaultstate="collapsed" desc="headerObject, headerPanel, headerPanelLayout & Functions">
    /**
     *
     */
    protected BaseEntity headerObject;

    public BaseEntity getHeaderObject() {
        return headerObject;
    }

    public void setHeaderObject(BaseEntity headerObject) {
        this.headerObject = headerObject;
    }
    protected HeaderPanel headerPanel;

    public HeaderPanel getHeaderPanel() {
        return headerPanel;
    }

    public void setHeaderPanel(HeaderPanel headerPanel) {
        this.headerPanel = headerPanel;
    }

    /**
     * Creates {@link #headerPanel} and set {@link #headerPanelLayout} if
     * succeeded
     *
     * @param headerEntityOEntity
     * @return {@link #headerPanelLayout}
     */
    public HtmlPanelGrid createHeaderPanelLayout(OEntityDTO headerEntityOEntity) {
        String headerPanelId = getCurrentScreen().getName() + "_headerPanel";
        headerPanel = new HeaderPanel(this, exFactory, getFacesContext(),
                headerEntityOEntity, headerPanelId, entitySetupService, oem, ddService,
                loggedUser);
        headerPanelLayout = headerPanel.createHeaderPanel();
        return headerPanelLayout;
    }
    protected HtmlPanelGrid headerPanelLayout = null;

    public HtmlPanelGrid getHeaderPanelLayout() {
        return headerPanelLayout;
    }

    public void setHeaderPanelLayout(HtmlPanelGrid headerPanelLayout) {
        this.headerPanelLayout = headerPanelLayout;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="OpenScreen">
    public String openScreen(String screenName, String viewPage, String title, String screenInstId, boolean scrnC2AMode) {
        logger.debug("Entering");
        if (portalMode) {
            logger.trace("Portal Mode: Adding New Protlet with Title: {}", title);
            // Setting enableC2A:'false' will make the portlets in Portal Mode open
            // with Save and Exit button
            if (viewPage.equals("EmployeeViewr.jspx")) {
                OrgChartFunction chartFunction;// = null;
                try {
                    chartFunction = (OrgChartFunction) oem.loadEntity(OrgChartFunction.class.getSimpleName(),
                            Collections.singletonList("code = 'empchart'"), null, loggedUser);

                    if (chartFunction != null) {
                        FABSSetup fabsOrgChartWSDL = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                                Collections.singletonList("setupKey = 'FABSOrgChartWSDL'"), null, loggedUser);

                        FABSSetup otmsOrgChartWSDL = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                                Collections.singletonList("setupKey = 'OTMSOrgChartWSDL'"), null, loggedUser);

                        FABSSetup otmsOrgChartURL = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                                Collections.singletonList("setupKey = 'OTMSOrgChartURL'"), null, loggedUser);

                        String call = "top.FABS.Portlet.openOrgChart({viewPage:'" + chartFunction.getViewPage().getCode() + ".jspx"
                                + "',screenInstId:'" + generateScreenInstId("Employee Viewer")
                                + "',htmlDir:'" + htmlDir
                                + "',portletHeight:'" + chartFunction.getPortletHeight()
                                + "',userName:'" + loggedUser.getLoginName()
                                + "',fabsWSDLURL:'" + fabsOrgChartWSDL.getSvalue()
                                + "',otmsWSDLURL:'" + otmsOrgChartWSDL.getSvalue()
                                + "',otmsORGURL:'" + otmsOrgChartWSDL.getSvalue()
                                + "',portletTitle:'Employee Viewer'});";
                        RequestContext context = RequestContext.getCurrentInstance();
                        context.execute(call);
                    }
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }

            } else {
                if (title != null && title.toLowerCase().equals("filter screen")) {
                    title = ddService.getDD("custom_report_header", loggedUser).getHeaderTranslated();
                    title = title + " - " + SessionManager.getSessionAttribute("reportName");
                }
                String call = "top.FABS.Portlet.open({screenName:'" + screenName + "',screenInstId:'" + screenInstId
                        + "',viewPage:'" + viewPage + "',portletTitle:'" + title.replace("\\", "\\\\").replace("\'", "\\'") + "',screenHeight:'"
                        + "" + "',enableC2A:'" + scrnC2AMode + "'});";
                RequestContext context = RequestContext.getCurrentInstance();
                OPortalUtil.reqContext = context;
                context.execute(call);
            }
            logger.debug("Returning with Null");
            return null;
        } else {
            logger.debug("Returning with screenName: {}", screenName);
            return screenName;
        }
    }

    public String openScreen(OScreen oScreen) {
        String openedScreenInstId = generateScreenInstId(oScreen.getName());
        return openScreen(oScreen, openedScreenInstId);
    }

    public String openScreen(OScreen oScreen, String openedScreenInstId) {
        try {
            logger.debug("Entering");
            if (c2AMode) {
                addScreenToMyPortalPage(openedScreenInstId, oScreen);
                oScreen = (OScreen) textTranslationServiceRemote.loadEntityTranslation(oScreen, loggedUser);
                logger.debug("Returning");
                return openScreen(oScreen.getName(), oScreen.getViewPage(), oScreen.getHeaderTranslated(), openedScreenInstId, true);
            } else {
                oScreen = (OScreen) textTranslationServiceRemote.loadEntityTranslation(oScreen, loggedUser);
                logger.debug("Returning");
                return openScreen(oScreen.getName(), oScreen.getViewPage(), oScreen.getHeaderTranslated(), openedScreenInstId, false);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    public String openScreen(String screenName) {
        OScreen screen;// = null;
        screen = uiFrameWorkFacade.getScreenObject(screenName, loggedUser);
        return openScreen(screen);
    }

    public String openScreen(String screenName, String passedScreenInstId) {
        OScreen screen;// = null;
        screen = uiFrameWorkFacade.getScreenObject(screenName, loggedUser);
        return openScreen(screen, passedScreenInstId);
    }

    /**
     * /**
     * Loop for all opened screens in my page and add the screen that will be
     * opened to screen instances list
     */
    private void addScreenToMyPortalPage(String mapKey, OScreen mapValue) {
//        myPage.getPageScreenInstancesList().put(mapKey, mapValue);
        SessionManager.setScreenSessionAttribute(mapKey, PORTAL_PAGE, myPage);
    }

    public OFunctionResult openScreenCheckFilter(
            OScreen screen, ODataMessage dataMessage, String toBOpenedScreenInstId) {
        logger.debug("Entering");
        OFunctionResult ofr = new OFunctionResult();
        try {
            boolean nullData = false;
            boolean runFilter = false;
            screen = (OScreen) oem.loadEntity(OScreen.class.getSimpleName(), screen.getDbid(),
                    null, null, oem.getSystemUser(loggedUser));
            if (screen.getScreenFilter() != null
                    && !screen.getScreenFilter().isInActive()) {
                for (int i = 0; i < screen.getScreenFilter().getFilterFields().size(); i++) {
                    if (screen.getScreenFilter().getFilterFields().get(i).isInActive()) {
                        continue;
                    }

                    if (screen.getScreenFilter().getFilterFields().get(i).getFieldValue() == null
                            || screen.getScreenFilter().getFilterFields().get(i).getFieldValue().equals("")) {
                        nullData = true;
                    }
                }
                runFilter = screen.getScreenFilter().isAlwaysShow() || (nullData && !screen.getScreenFilter().isShowInsideScreen());
            }
            if (runFilter) {
                openFilter(screen, dataMessage, FilterType.SCREEN_FILTER, toBOpenedScreenInstId);
            } else {
                openScreen(screen, toBOpenedScreenInstId);
            }
            return ofr;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            ofr.addError(
                    usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        logger.debug("Returning");
        return ofr;
    }

    //</editor-fold>
    public String openScreenInPopup(OScreen oScreen, int popupType) {
        String openedScreenInstId = generateScreenInstId(oScreen.getName());
        try {
            oScreen = (OScreen) textTranslationServiceRemote.loadEntityTranslation(oScreen, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        return openScreenInPopup(oScreen.getName(), oScreen.getViewPage(), oScreen.getHeaderTranslated(), openedScreenInstId, popupType);
    }

    public String openScreenInPopup(OScreen oScreen, String openedScreenInstId, int popupType) {
        logger.debug("Entering");
        try {
            oScreen = (OScreen) textTranslationServiceRemote.loadEntityTranslation(oScreen, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return openScreenInPopup(oScreen.getName(), oScreen.getViewPage(), oScreen.getHeaderTranslated(), openedScreenInstId, popupType);
    }

    public String openScreenInPopup(String screenName, int popupType) {
        OScreen screen = uiFrameWorkFacade.getScreenObject(screenName, loggedUser);
        return openScreenInPopup(screen, popupType);
    }

    public String openScreenInPopup(String screenName, String passedScreenInstId, int popupType) {
        OScreen screen;//= null;
        screen = uiFrameWorkFacade.getScreenObject(screenName, loggedUser);
        return openScreenInPopup(screen, passedScreenInstId, popupType);
    }

    public String openScreenInPopup(String screenName, String viewPage, String title, String openedScreenInstId, int popUpType) {
        logger.debug("Entering");
        if (portalMode) {
            logger.trace("opened id poral mode");
            logger.trace("Portal Mode: Adding New Protlet with Title: {}", title);
            // Setting enableC2A:'false' will make the portlets in Portal Mode open
            // with Save and Exit button

            // get popup screen width according to type
            if (viewPage.equals("EmployeeViewr.jspx")) {
                OrgChartFunction chartFunction;// = null;
                try {
                    chartFunction = (OrgChartFunction) oem.loadEntity(OrgChartFunction.class.getSimpleName(),
                            Collections.singletonList("code = 'empchart'"), null, loggedUser);

                    if (chartFunction != null) {
                        FABSSetup fabsOrgChartWSDL = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                                Collections.singletonList("setupKey = 'FABSOrgChartWSDL'"), null, loggedUser);

                        FABSSetup otmsOrgChartWSDL = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                                Collections.singletonList("setupKey = 'OTMSOrgChartWSDL'"), null, loggedUser);

                        String call = "top.FABS.Portlet.openOrgChart({viewPage:'" + chartFunction.getViewPage().getCode() + ".jspx"
                                + "',screenInstId:'" + generateScreenInstId("Employee Viewer")
                                + "',htmlDir:'" + htmlDir
                                + "',portletHeight:'" + chartFunction.getPortletHeight()
                                + "',userName:'" + loggedUser.getLoginName()
                                + "',fabsWSDLURL:'" + fabsOrgChartWSDL.getSvalue()
                                + "',otmsWSDLURL:'" + otmsOrgChartWSDL.getSvalue()
                                + "',portletTitle:'Employee Viewer'});";
                        logger.trace("Calling String is: {}", call);
                        RequestContext context = RequestContext.getCurrentInstance();
                        context.execute(call);
                    }
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }

            } else {
                int width = 0;
                if (popUpType == POPUP_BALLOONTYPE) {
                    width = 500;
                } else if (popUpType == POPUP_LOOKUPTYPE) {
                    width = 475;
                } else if (popUpType == POPUP_SCREENTYPE) {
                    width = 650;
                }
                Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();
                RequestContext context = RequestContext.getCurrentInstance();
                if (popUpType == POPUP_LOOKUPTYPE) {
                    String lookupDD = ddService.getDDLabel("lookupDD", loggedUser);
                    //title = lookupDD + " - " + title;
                    context.execute(
                            "try { top.FABS.Portlet.openPopup({screenName:'" + screenName
                            + "',screenInstId:'" + openedScreenInstId
                            + "',portletInstId:'" + getPortletInsId()
                            + "',viewPage:'" + viewPage
                            + "',width:'" + width
                            + "',isLookup:'true',title:'" + title.replace("\\", "\\\\").replace("\'", "\\'") + "'" + "});} catch(err){}");
                } else if (popUpType == POPUP_BALLOONTYPE) {
                    context.execute(
                            "try { top.FABS.Portlet.openPopup({screenName:'" + screenName
                            + "',screenInstId:'" + openedScreenInstId
                            + "',portletInstId:'" + getPortletInsId()
                            + "',viewPage:'" + viewPage
                            + "',width:'" + width
                            + "',height:'" + 200
                            + "',title:''" + "});} catch(err){}");
                } else {
                    context.execute(
                            "try { top.FABS.Portlet.openPopup({screenName:'" + screenName
                            + "',screenInstId:'" + openedScreenInstId
                            + "',portletInstId:'" + getPortletInsId()
                            + "',viewPage:'" + viewPage
                            + "',width:'" + width
                            + "',title:'" + title.replace("\\", "\\\\").replace("\'", "\\'") + "'" + "});} catch(err){}");
                }
            }
            SessionManager.addSessionAttribute("openedScreenInstId", openedScreenInstId);
            logger.debug("Returning with Null");
            return null;
        } else {
            logger.debug("Returning with screenName: {}", screenName);
            return screenName;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="OpenReport">
    public OFunctionResult openReportCheckFilter(OReport report, Long dbid) {
        logger.debug("Entering");
        try {
            boolean runFilter = false;
            if (loggedUser == null) {
                loggedUser = (OUser) SessionManager.getSessionAttribute("LoggedUser");
            }
            report = (OReport) oem.loadEntity(OReport.class.getSimpleName(), report.getDbid(), null, null, loggedUser);
            if (report.getName().equals("PayslipReport")) {
                SessionManager.addSessionAttribute(REPORT_VAR, report);
                openScreen("PayslipFilter");
                logger.debug("Returning with Null");
                return null;
            } else if (report.getName().equals("PayrollSummeryReport")) {
                SessionManager.addSessionAttribute(REPORT_VAR, report);
                openScreen("PayrollSummeryFilter");
                logger.debug("Returning with Null");
                return null;
            } else if (report.getReportFilter() != null
                    && !report.getReportFilter().isInActive()) {
                for (int i = 0; i < report.getReportFilter().getFilterFields().size(); i++) {
                    if (report.getReportFilter().getFilterFields().get(i).isInActive()) {
                        continue;
                    }

                    if (report.getReportFilter().getFilterFields().get(i).getFieldValue() == null || report.getReportFilter().getFilterFields().get(i).getFieldValue().equals("")) {
                        runFilter = true;
                    }
                }
                runFilter |= report.getReportFilter().isAlwaysShow();
            } else if (report.getCustomFilterScreen() != null) {
                SessionManager.addSessionAttribute(REPORT_VAR, report);
                openScreen(report.getCustomFilterScreen());
                logger.debug("Returning with Null");
                return null;
            }
            if (runFilter) {
                if (dbid != null) {
                    SessionManager.addSessionAttribute("headerObjectDBID", dbid.toString());
                }
                openFilter(report, null, FilterType.REPORT_FILTER, null/*not preset*/);
            } else if (report instanceof ComparisonCrossTabReport) {
                SessionManager.addSessionAttribute(REPORT_VAR, report);
                openScreen("ComparisonCalculatedPeriodsScreen");
            } else if (report instanceof CrossTabReport) {
                SessionManager.addSessionAttribute(REPORT_VAR, report);
                openScreen("CrosstabCalculatedPeriodScreen");
            } else if (dbid != null) {
                openReport(report, dbid);
            } else {
                openReport(report);
            }
            logger.debug("Returning with Null");
            return null;
        } catch (UserNotAuthorizedException ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning with Null");
        return null;
    }

    public String openReport(OReport report) {
        logger.debug("Entering");
        logger.trace("Openinging Report: {}", report);
        String viewPage = "report.jsp";
        if (report.getName().equals("IRS_Insurance_Form")) {
            viewPage = "Report";
        }
        SessionManager.addSessionAttribute("loggedUser", loggedUser);
        SessionManager.addSessionAttribute(REPORT_VAR, report);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute(
                "top.FABS.Portlet.openReport({reportName:'" + report.getName()
                + "',reportFormat:'HTML',reportTitle:'" + report.getTitleTranslated() + "', viewPage:'" + viewPage + "'});");
        logger.debug("Returning with Null");
        return null;
    }

    public String runWebServiceCheckFilter(OWebService service, ODataMessage dm) {
        logger.debug("Entering");
        try {
            boolean runFilter = false;
            service = (OWebService) oem.loadEntity(OWebService.class.getSimpleName(),
                    service.getDbid(), null, null, loggedUser);
            if (service.getServiceFilter() != null
                    && !service.getServiceFilter().isInActive()) {
                for (int i = 0; i < service.getServiceFilter().getFilterFields().size(); i++) {
                    if (service.getServiceFilter().getFilterFields().get(i).getFieldValue() == null
                            || service.getServiceFilter().getFilterFields().get(i).getFieldValue().equals("")) {
                        runFilter = true;
                    }
                }
                runFilter |= service.getServiceFilter().isAlwaysShow();
            }
            if (runFilter) {
                openFilter(service, dm, FilterType.WEBSERVICE_FILTER, null/*not preset*/);
            } else {
                runWebService(service, dm);
            }
            logger.debug("Returning with Null");
            return null;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning with Null");
        return null;
    }

    public String runWebService(OWebService service, ODataMessage dm) {
        OFunctionResult result = functionServiceRemote.runWebServiceFunction(service, dm, new OFunctionParms(), loggedUser);
        messagesPanel.addMessages(result);
        return null;
    }
    public static String REPORT_VAR = "REPORT";
    public static String OUTBOX_TASK_INS = "OUTBOX_TASK_INS";
    public static String OUTBOX_PROCESS_INS = "OUTBOX_PROCESS_INS";
    String outboxTaskId, outboxProcessId;

    public OFunctionResult openReport(OReport report, Long dbid) {
        logger.debug("Entering");
        OFunctionResult ofr = new OFunctionResult();
        try {
            String viewPage = "report.jsp";
            if (report.getName().equals("IRS_Insurance_Form")) {
                viewPage = "Report";
            }
            SessionManager.addSessionAttribute(REPORT_VAR, report);
            SessionManager.addSessionAttribute("headerObjectDBID", String.valueOf(dbid));
            SessionManager.addSessionAttribute("loggedUser", loggedUser);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(
                    "top.FABS.Portlet.openReport({reportName:'" + report.getName()
                    + "',reportFormat:'HTML',reportTitle:'" + report.getName() + "', viewPage:'" + viewPage + "'});");
            return ofr;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            ofr.addError(
                    usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
            logger.debug("Returning");
            return ofr;
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Open Custom Report">
    public OFunctionResult openCustomReport(CustomReport customReport, AttachmentDTO attachment) {
        logger.debug("Entering");
        OFunctionResult ofr = new OFunctionResult();
        String openedScreenInstId = generateScreenInstId("RunFilterForCustomReport");
        try {
            SessionManager.addSessionAttribute("reportName", customReport.getReportName());
            if (customReport.getJavaFunctionUserExit() != null) {
                SessionManager.addSessionAttribute("UserExitJavaFunction", customReport.getJavaFunctionUserExit());
            }
            if (customReport.getCustomReportFilter() != null
                    && customReport.getCustomReportFilter().isShowBeforRun()
                    && customReport.getCustomReportFilter().getCustomreportfilterfields() != null
                    && !customReport.getCustomReportFilter().getCustomreportfilterfields().isEmpty()
                    || (customReport.getCustomReportParameters() != null
                    && !customReport.getCustomReportParameters().isEmpty())
                    || customReport.isViewEntityFilter()) {

                ODataMessage message = new ODataMessage();
                ODataType oDataType = dataTypeService.loadODataType(CustomReportFilter.class.getSimpleName(), loggedUser);
                message.setODataType(oDataType);
                List data = new ArrayList();
                data.add(customReport.getCustomReportFilter());
                data.add(customReport);
                data.add(attachment);
                message.setData(data);
                SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.MESSAGE,
                        message);
                openScreen("RunFilterForCustomReport", openedScreenInstId);

            } else {
                logger.debug("Returning");
                return openCustomReport(null, null, customReport, attachment);
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            ofr.addError(
                    usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
            logger.debug("Returning");
            return ofr;
        }
        logger.debug("Returning");
        return ofr;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Open Custom Report With Filters And Or Parameters">
    public OFunctionResult openCustomReport(String whereCondition,
            Map<String, Object> parametersMap,
            CustomReport customReport,
            AttachmentDTO attachment) {
        logger.debug("Entering");
        OFunctionResult ofr = new OFunctionResult();
        try {
            String viewPage = "birtReportWithExcel.jsp";//"CustomReportServlet";
            String reportName = customReport.getReportName();
            SessionManager.addSessionAttribute("UserExitJavaFunction", customReport.getJavaFunctionUserExit());
            SessionManager.addSessionAttribute("reportName", reportName);
            SessionManager.addSessionAttribute("Filters", whereCondition);
            SessionManager.addSessionAttribute("EntityAttachment", attachment);
            SessionManager.addSessionAttribute("parametersMap", parametersMap);
            SessionManager.addSessionAttribute("loggedUser", loggedUser);
            SessionManager.addSessionAttribute("localized", customReport.isLocalized());
            SessionManager.addSessionAttribute("revertLang", customReport.isRevertLang());
            SessionManager.addSessionAttribute("applyingFilter", customReport.isApplyingFilter());
            SessionManager.addSessionAttribute("fitPage", customReport.isFitPage());
            //EnableEncryption Check and set it in session
            ArrayList<String> encCondition = new ArrayList<String>();
            encCondition.add("setupKey = 'enableEncryption'");
            List<FABSSetup> fABSSetup = oem.loadEntityList("FABSSetup", encCondition, null, null, loggedUser);
            if (fABSSetup != null && !fABSSetup.isEmpty() && fABSSetup.get(0).getSvalue().equals("true")) {
                SessionManager.addSessionAttribute("enableEncryption", true);
            } else {
                SessionManager.addSessionAttribute("enableEncryption", false);
            }
            String symmetricKey = dataEncryptorService.getSymmetricKey();
            String ivkey = dataEncryptorService.getIvKey();
            if (null != symmetricKey) {
                SessionManager.addSessionAttribute("symmetricKey", symmetricKey);
            }
            if (null != ivkey) {
                SessionManager.addSessionAttribute("ivkey", ivkey);
            }//End

            RequestContext context = RequestContext.getCurrentInstance();
            String x = "top.FABS.Portlet.openReport({reportName:'" + customReport.getReportName()
                    + "',reportFormat:'HTML',reportTitle:'" + customReport.getReportName() + "', viewPage:'" + viewPage + "'});";

            context.execute(x);
            logger.debug("Returning");
            return ofr;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            ofr.addError(
                    usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
            logger.debug("Returning");
            return ofr;
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="View the attached files">
    public OFunctionResult viewAttachment(AttachmentDTO attachment) {
        logger.debug("Entering");
        OFunctionResult ofr = new OFunctionResult();
        try {
            String viewPage = "ViewAttachmentServlet";
            SessionManager.addSessionAttribute("EntityAttachment", attachment);
            SessionManager.addSessionAttribute("loggedUser", loggedUser);
            RequestContext context = RequestContext.getCurrentInstance();
            String x = "top.FABS.Portlet.openReport({reportName:'" + attachment.getName()
                    + "',reportFormat:'HTML',reportTitle:'" + attachment.getName() + "', viewPage:'" + viewPage + "'});";
            context.execute(x);
            logger.debug("Returning");
            return ofr;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            ofr.addError(
                    usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
            logger.debug("Returning");
            return ofr;
        }
    }
    //</editor-fold>

    protected abstract String getBeanName();

    /**
     * Returns a balloon panel of the passed 'oEntity'
     *
     * @param balloonOEntity
     * @param id
     * @param righToLeft
     * @param ownerEntityExpression
     * @param expressionFieldInfos If 'null', then balloon will be created for
     * ownerEntity
     * @return
     */
    public OverlayPanel createBalloonPanel(
            OEntityDTO balloonOEntity, String id, boolean righToLeft,
            String ownerEntityExpression, List<ExpressionFieldInfo> expressionFieldInfos) {
        logger.trace("Entering");
        logger.trace("Creating Balloon Panel");
        // </editor-fold>
        if (balloonOEntity == null) {
            logger.trace("Returning with Null");
            return null;
        }
        try {
            OverlayPanel generalPanel = null;
            Balloon balloon = null;
            String ownerFieldBackBeanExpression = null;
            if (expressionFieldInfos != null) {
                // Balloon for a field
                int balloonFieldIndex = uiFrameWorkFacade.getBalloonFieldIndex(
                        Class.forName(balloonOEntity.getEntityClassPath()),
                        expressionFieldInfos);
                switch (balloonFieldIndex) {
                    case -2:
                        // Error
                        logger.trace("Returning with Null");
                        return null;
                    case -1:
                        // Owner Entity itself
                        ownerFieldBackBeanExpression = ownerEntityExpression;
                    default:
                        // A field in the expressionFieldInfos
                        ownerFieldBackBeanExpression = ownerEntityExpression;
                        for (int fieldIndex = 0; fieldIndex <= balloonFieldIndex; fieldIndex++) {
                            ownerFieldBackBeanExpression
                                    += "." + expressionFieldInfos.get(fieldIndex).fieldName;
                        }
                }
            }
            balloon = new Balloon(this,
                    ownerEntityExpression, getBeanName(), id,
                    balloonOEntity.getDbid(), balloonOEntity.getEntityClassPath(),
                    balloonOEntity.getTitle(), currentScreen.getName(),
                    exFactory, elContext, uiFrameWorkFacade, securityService,
                    loggedUser);

            generalPanel = balloon.create(ownerFieldBackBeanExpression);

            balloon.setOwnerFieldBackBeanExpression(ownerFieldBackBeanExpression);
            balloons.put(id, balloon);
            logger.trace("Returning");
            return generalPanel;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Returning");
            return null;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Simple Setters & Getters">
    public OScreen getCurrentScreen() {
        return currentScreen;
    }

    public void setCurrentScreen(OScreen currentScreen) {
        this.currentScreen = currentScreen;
    }

    public OUser getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(OUser loggedUser) {
        this.loggedUser = loggedUser;
    }

    public ArrayList<String> getErrorList() {
        return errorList;
    }

    public void setErrorList(ArrayList<String> errorList) {
        this.errorList = errorList;
    }

    public ArrayList<String> getFatalList() {
        return fatalList;
    }

    public void setFatalList(ArrayList<String> fatalList) {
        this.fatalList = fatalList;
    }

    public ArrayList<String> getInfoList() {
        return infoList;
    }

    public void setInfoList(ArrayList<String> infoList) {
        this.infoList = infoList;
    }

    public ArrayList<String> getSuccessList() {
        return successList;
    }

    public void setSuccessList(ArrayList<String> successList) {
        this.successList = successList;
    }

    public ArrayList<String> getWarnList() {
        return warnList;
    }

    public void setWarnList(ArrayList<String> warnList) {
        this.warnList = warnList;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Simple Super-Function Override">
    @Override
    public void error(String summary) {
        super.error(summary);
        getErrorList().add(summary);
    }

    @Override
    public void error(UIComponent comp, String summary) {
        super.error(comp, summary);
        getErrorList().add(summary);
    }

    @Override
    public void warn(String summary) {
        super.warn(summary);
        getWarnList().add(summary);
    }

    @Override
    public void warn(UIComponent comp, String summary) {
        super.warn(comp, summary);
        getWarnList().add(summary);
    }

    @Override
    public void fatal(String summary) {
        super.fatal(summary);
        getFatalList().add(summary);
    }

    @Override
    public void fatal(UIComponent comp, String summary) {
        super.fatal(comp, summary);
        getFatalList().add(summary);
    }

    @Override
    public void info(String summary) {
        super.info(summary);
        getInfoList().add(summary);
    }

    @Override
    public void info(UIComponent comp, String summary) {
        super.info(comp, summary);
        getInfoList().add(summary);
    }

    public void success(String summary) {
        getSuccessList().add(summary);
    }

    public void success(UIComponent comp, String summary) {
        getSuccessList().add(summary);
    }
    // </editor-fold>

    public abstract ODataMessage saveProcessRecords();

    // <editor-fold defaultstate="collapsed" desc="Process Task Actions & Functions">
    public String exitTask() {
        logger.debug("Entering");
        if (isDisabled()) {
            return "";
        }
        String inboxID = (String) getScreenParam("inboxID");
        if (c2AMode && processMode) {
            String call = "top.FABS.Portlet.closeBrowserTab();";
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(call);
        } else if (processMode) {
            OPortalUtil.removePortlet(getPortletInsId(), screenInstId);
        }
        logger.debug("Returning with Null");
        return null;
    }

    public String saveTask() {
        logger.debug("Entering");
        ODataMessage outputDataMessage = saveProcessRecords();
        setProcessMode(true);
        String inboxID = (String) getScreenParam("inboxID");
        OPortalUtil.renderInbox(currentScreen, inboxID, new ODataMessage(
                dataTypeService.loadODataType("VoidDT", loggedUser), new ArrayList<Object>()));
        OPortalUtil.removePortlet(getPortletInsId(), screenInstId);
        logger.debug("Returning with Null");
        return null;
    }

    public String taskActionButtons(ActionEvent ae) {
        OFunctionResult oFR = new OFunctionResult();
        if(this instanceof FormBean){
            ((FormBean)this).checkLookupMandatoryFields(oFR);
        }
        if(oFR.getErrors() != null && oFR.getErrors().size() != 0){
            messagesPanel.clearMessages();
            messagesPanel.addMessages(oFR);
            return"";
        }
        return taskOptionProcess.taskActionButtons(ae, currentInbox, getPortletInsId(), getScreenInstId(), getCurrentScreen());
    }

    // </editor-fold>
    protected Object getValueFromEntity(Object entity, String expression) {
        logger.trace("Entering");
        try {
            if (expression.equalsIgnoreCase("this")) {
                logger.trace("Returning");
                return entity;
            } else if (expression.equalsIgnoreCase("user")) {
                logger.trace("Returning");
                return loggedUser;
            } else if (expression.equalsIgnoreCase("parent")) {
                logger.trace("Returning");
                return headerObject;
            } else if (expression.equalsIgnoreCase("ScreenDataList")) {
                logger.trace("Returning");
                return new ArrayList();
            } else if (expression.equalsIgnoreCase("requestStatusAttr")) {
                logger.trace("Returning");
                return nextRequestStatus;
            }
            return FramworkUtilities.getValueFromEntity(entity, expression);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Returning with Null");
            return null;
        }
    }

    public boolean isProcessMode() {
        return processMode;
    }

    public void setProcessMode(boolean processMode) {
        this.processMode = processMode;
    }

    // <editor-fold defaultstate="collapsed" desc="Run Various Functions">
    /**
     * Called by {@link #runFunctionForEntity(OFunction, BaseEntity) }
     * immediately before running screen function & after all screen session
     * attributes are set. Override it to add your customization, you can
     * directly display user messages in {@link #messagesPanel}
     *
     * @param ofunction
     * @param entity
     * @param passedDataMessage
     * @return Error message if encountered. Nothing returnDataMessage in or
     * returnedValues
     */
    protected OFunctionResult preRunScreenFunction(
            OFunction ofunction, BaseEntity entity, ODataMessage passedDataMessage) {
        OFunctionResult oFR = new OFunctionResult();
        return oFR;
    }

    /**
     * Called to run a {@link OFunction} on any {@link BaseEntity} driven
     * entity. Calls preRun actions, which can be overridden to do your
     * validations and customizations, including
     * {@link #preRunScreenFunction(OFunction, BaseEntity, ODataMessage)}
     *
     * @param ofunction
     * @param entity
     * @return Error message if encountered. Nothing returnDataMessage in or
     * returnedValues
     */
    public OFunctionResult runFunctionForEntity(OFunction ofunction, BaseEntity entity, String actionName) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        String newscreenInstId = "";
        try {
            if (ofunction instanceof UDCScreenFunction) {
                // <editor-fold defaultstate="collapsed" desc="UDCScreenFunction">
                UDCScreenFunction screenFunction = (UDCScreenFunction) ofunction;
                ODataType parentDT = dataTypeService.loadODataType("ParentDBID", systemUser);

                ODataMessage parentDataMessage = new ODataMessage();
                List dataList = new ArrayList();
                dataList.add(screenFunction.getType().getDbid());
                parentDataMessage.setData(dataList);
                parentDataMessage.setODataType(parentDT);
                // construct screen instance id to use in session
                newscreenInstId = generateScreenInstId(screenFunction.getOscreen().getName());
                SessionManager.setScreenSessionAttribute(
                        newscreenInstId, OBackBean.WIZARD_VAR, false);
                SessionManager.setScreenSessionAttribute(
                        newscreenInstId, OPortalUtil.MESSAGE, parentDataMessage);
                oFR.append(openScreenCheckFilter(
                        screenFunction.getOscreen(), parentDataMessage, newscreenInstId));
                // </editor-fold>
                logger.debug("Returning");
                return oFR;

            } else if (ofunction instanceof ScreenFunction) {
                ScreenFunction sfunction = (ScreenFunction) ofunction;
                if (sfunction.getScreenMode() == null) {
                    logger.warn("Screen Has No Mode");
                    oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser));
                }
                if (sfunction.getOdataType() == null) {
                    logger.warn("Called Function Must Have ODataType");
                    oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser));
                }
                if (!oFR.getErrors().isEmpty()) {
                    logger.debug("Returning");
                    return oFR;
                }

                ODataMessage message = new ODataMessage();
                if (ofunction.getDbid() == 1430304
                        || ofunction.getDbid() == 1429683) {
                    ODataType oDataType = dataTypeService.loadODataType("VoidDT", loggedUser);
                    message.setODataType(oDataType);
                    List data = new ArrayList();
                    if (entity != null) {
                        data.add(entity.getClass().getName());
                        data.add(entity.getDbid());
                        data.add(entity);
                    }
                    if (ofunction.getDbid() == 1429683) {
                        data.add("General Action");
                    }
                    message.setData(data);
                } else if (sfunction.getDbid() == 1448530) { //MailMergePreview
                    List data = new ArrayList();
                    ODataType entityDT = dataTypeService.loadODataType(entity.getClass().getSimpleName(), systemUser);
                    data.add(dataMessage.getData().get(1));
                    data.add(entity);
                    data.add(loggedUser);
                    message.setData(data);
                    message.setODataType(entityDT);
                    oFR.setReturnedDataMessage(message);
                } else {
                    message = (ODataMessage) constructOutput(
                            sfunction.getOdataType(), entity).getReturnedDataMessage();
                }

                if (entity instanceof CustomReportDynamicFilter) {
                    SessionManager.addSessionAttribute("CustomReportDynamicFilterENTITY", ((CustomReportDynamicFilter) entity));
                }
                // <editor-fold defaultstate="collapsed" desc="run ScreenFunction">

                // construct screen instance id to use in session
                newscreenInstId = generateScreenInstId(sfunction.getOscreen().getName());
                SessionManager.setScreenSessionAttribute(newscreenInstId,
                        OPortalUtil.MESSAGE, message);
                SessionManager.setScreenSessionAttribute(newscreenInstId,
                        MODE_VAR, String.valueOf(sfunction.getScreenMode().getDbid()));
                SessionManager.setScreenSessionAttribute(newscreenInstId,
                        "showSaveExit", Boolean.TRUE);
                SessionManager.setScreenSessionAttribute(newscreenInstId,
                        OBackBean.WIZARD_VAR, false);
                OFunctionResult tempFR = preRunScreenFunction(ofunction, entity, message);
                if (!tempFR.getErrors().isEmpty()) {
                    oFR.append(tempFR);
                    logger.debug("Returning");
                    return oFR;
                }
                if (sfunction.isRunInPopup()) {
                    //FIXME: openScreenInPopup must return oFR
                    openScreenInPopup(sfunction.getOscreen(), newscreenInstId, POPUP_SCREENTYPE);
                } else {
                    oFR.append(openScreenCheckFilter(sfunction.getOscreen(), message, newscreenInstId));
                }
                // </editor-fold>
                return oFR;

            } else if (ofunction instanceof PortalPageFunction) {
                // <editor-fold defaultstate="collapsed" desc="Open PortalPage">
                PortalPageFunction portalPageFunction = (PortalPageFunction) ofunction;
                HashMap<String, Object> params = new HashMap<String, Object>();
                OPortalPage portalPage = (OPortalPage) oem.loadEntity(OPortalPage.class.getSimpleName(),
                        portalPageFunction.getPortalPage().getDbid(),
                        null, null, loggedUser);
                ODataMessage message = null;
                if (portalPageFunction.getOdataType() == null) {
                    // <editor-fold defaultstate="collapsed" desc="Log">

                    // </editor-fold>
                    // Send Void DBID by default
                    ODataType voidDataType = dataTypeService.loadODataType("VoidDT", systemUser);
                    message = new ODataMessage();
                    message.setData(new ArrayList<Object>());
                    message.setODataType(voidDataType);
                } else {
                    message = (ODataMessage) constructOutput(
                            portalPageFunction.getOdataType(), entity).getReturnedDataMessage();
                }
                if (portalPageFunction.getScreenMode() != null) {
                    params.put(MODE_VAR, String.valueOf(portalPageFunction.getScreenMode().getDbid()));
                }
                params.put(OPortalUtil.MESSAGE, message);

                List<Long> portalPageData = (List<Long>) functionServiceRemote.getPortalPageData(portalPageFunction.getDbid(), loggedUser);
                OPortalUtil.addOPortletPage(portalPageData, oem, dataTypeService, loggedUser, params,
                        uiFrameWorkFacade, uDCService);
                // </editor-fold>
                logger.debug("Returning");
                return oFR;

            } else if (ofunction instanceof ReportFunction) {
                // <editor-fold defaultstate="collapsed" desc="run ReportFunction">
                ReportFunction rfunction = (ReportFunction) ofunction;
                oFR.append(openReportCheckFilter(rfunction.getReport(), entity.getDbid()));
                // </editor-fold>
                logger.debug("Returning");
                return oFR;

            } else if (ofunction instanceof WizardFunction) {
                // <editor-fold defaultstate="collapsed" desc="run WizardFunction">
                Wizard wizard = ((WizardFunction) ofunction).getWizard();
                textTranslationServiceRemote.loadEntityTranslation(wizard, loggedUser);
                WizardController wizardController = new WizardController(oem,
                        wizard, loggedUser, usrMsgService);
                if (wizardController.getScreensWizardInfo().size() > 0) {
                    ODataMessage wizardDM = (ODataMessage) constructOutput(
                            wizardController.getScreensWizardInfo().get(0).getStep().getStepDT(),
                            entity).getReturnedDataMessage();
                    oFR.append(wizardController.openWizard(wizardDM));
                }
                // </editor-fold>
                logger.debug("Returning");
                return oFR;

            } else if (ofunction instanceof JavaFunction) {
                // <editor-fold defaultstate="collapsed" desc="run JavaFunction">
                ODataType entityDT = dataTypeService.loadODataType(entity.getClass().getSimpleName(), systemUser);
                if (entityDT == null) {
                    logger.warn("Can't Load Entity Related DataType");
                    oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser));
                    logger.debug("Returning");
                    return oFR;
                }
                ODataMessage odm = (ODataMessage) constructOutput(entityDT, entity).getReturnedDataMessage();
                OFunctionParms functionParms = new OFunctionParms();
                functionParms.getParams().put("ScreenName", getCurrentScreen().getName());
                functionParms.getParams().put("ScreenDT", getDataMessage());
                oFR = functionServiceRemote.runFunction(ofunction, odm, functionParms, loggedUser);
                // </editor-fold>
                logger.debug("Returning");
                return oFR;

            } else if (ofunction instanceof URLFunction) {
                ODataMessage odm = new ODataMessage();
                oFR = functionServiceRemote.runFunction(ofunction, odm, new OFunctionParms(), loggedUser);
            } else if (ofunction instanceof WebServiceFunction) {
                // <editor-fold defaultstate="collapsed" desc="run runWebServiceCheckFilter">
                ODataType entityDT = dataTypeService.loadODataType(entity.getClass().getSimpleName(), systemUser);
                if (entityDT == null) {
                    logger.warn("Can't Load Entity Related DataType");
                    oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser));
                    logger.debug("Returning");
                    return oFR;
                }
                ODataMessage odm = (ODataMessage) constructOutput(entityDT, entity).getReturnedDataMessage();
                //FIXME: runWebServiceCheckFilter should return OFR
                runWebServiceCheckFilter(((WebServiceFunction) ofunction).getWebService(), odm);
                // </editor-fold>
                logger.debug("Returning");
                return oFR;

            } else if (ofunction.getCode().equals("RunCScreen")) {
                // <editor-fold defaultstate="collapsed" desc="run Screen">
                if (!(entity instanceof OScreen)) {
                    logger.warn("This Function Is Reserved Only For OScreen Instances");
                    oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser));
                    return oFR;
                }
                OScreen scrn = (OScreen) entity;
                ODataType voidDT = dataTypeService.loadODataType("VoidDT", systemUser);
                ODataMessage message = new ODataMessage(voidDT, new ArrayList());
                //construct screen instance id to use in saving session info
                newscreenInstId = generateScreenInstId(scrn.getName());
                SessionManager.setScreenSessionAttribute(newscreenInstId,
                        OPortalUtil.MESSAGE, message);
                long scrnMode = scrn instanceof FormScreen ? OScreen.SM_ADD : OScreen.SM_MANAGED;
                SessionManager.setScreenSessionAttribute(
                        newscreenInstId,
                        MODE_VAR,
                        scrnMode);
                SessionManager.setScreenSessionAttribute(newscreenInstId, "showSaveExit", Boolean.TRUE);
                SessionManager.setScreenSessionAttribute(newscreenInstId, OBackBean.WIZARD_VAR, false);
                oFR.append(openScreenCheckFilter(scrn, message, newscreenInstId));
                // </editor-fold>
                logger.debug("Returning");
                return oFR;

            } else if (ofunction.getCode().equals("GCR")) {
                CustomReport rprt = (CustomReport) entity;
                AttachmentDTO attachmentDTO = attachmentService.get(rprt.getDbid(), rprt.getClassName(), loggedUser).get(0);

                openCustomReport(rprt, attachmentDTO);
            } else if (ofunction.getCode().equals("ViewAttachment")) {
                AttachmentDTO attachmentDTO = attachmentService.get(entity.getDbid(), entity.getClassName(), loggedUser).get(0);
                viewAttachment(attachmentDTO);

            } else if (ofunction.getCode().equals("RunCReport")) {
                // <editor-fold defaultstate="collapsed" desc="run Report">
                if (!(entity instanceof OReport)) {
                    logger.warn("This Function Is Reserved Only For OReportInstances");
                    oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser));
                    logger.debug("Returning");
                    return oFR;
                }
                OReport rprt = (OReport) entity;
                oFR.append(openReportCheckFilter(rprt, entity.getDbid()));
                // </editor-fold>
                logger.debug("Returning");
                return oFR;

            } else if (ofunction instanceof OrgChartFunction) {
                OrgChartFunction chartFunction = (OrgChartFunction) ofunction;
                OPortalPage orgPage = new OPortalPage();
                orgPage.setName("OrgChart");
                orgPage.setHeader("OrgChart");
                orgPage.setOmodule(moduleService.loadOModule("System", systemUser));
                PortalPagePortlet test = new PortalPagePortlet();
                orgPage.setPortlets(new ArrayList<PortalPagePortlet>());
                OPortalUtil.addOPortletPageForOrg(orgPage, chartFunction, entity,
                        oem, dataTypeService, loggedUser, null, actionName);
            } else {
                logger.warn("Function Not Supported To Run From Balloon Yet");
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser));
                logger.debug("Returning");
                return oFR;
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
            logger.debug("Returning");
            return oFR;

        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    public OFunctionResult runActionForEntity(OEntityActionDTO action, BaseEntity entity) {
        String ofunctiontype = action.getOfunctionType();
        if ("JAVA".equals(ofunctiontype)) {
            EntityFilterScreenAction efsa = null;
            ODataMessage dm = new ODataMessage();
            List<String> filterConds = null;
            try {
                efsa = (EntityFilterScreenAction) oem.loadEntity(EntityFilterScreenAction.class.getSimpleName(),
                        Collections.singletonList("screen.dbid=" + currentScreen.getDbid()), null, loggedUser);
                if (efsa != null && efsa.getEntityFilter() != null) {
                    filterConds = filterServiceBean.getFilterConditions(efsa.getEntityFilter(), getUserSessionInfo(), loggedUser);
                }
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
            if (filterConds != null) {
                dm = entityServiceLocal.constructEntityListODataMessage(entity, filterConds, loggedUser);
            } else {
                dm = entityServiceLocal.constructEntityODataMessage(entity, loggedUser);
            }
            OFunctionParms functionParms = new OFunctionParms();
            functionParms.getParams().put("ScreenName", getCurrentScreen().getName());
            functionParms.getParams().put("ScreenDT", getDataMessage());
            if (this instanceof TabularBean) {
                functionParms.getParams().put("ScreenFilter", ((TabularBean) this).getDataFilter());
            }
            return entitySetupService.executeAction(action, dm, functionParms, loggedUser);
        }
        OFunction ofunction = null;
        try {
            ofunction = functionServiceRemote.getOFunction(action.getOfuntionDBID(), loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        return runFunctionForEntity(ofunction, entity, action.getName());
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="balloon functions">
    /**
     * Backbean Life cycle function. Called by {@link OBackBean#balloonAction(ActionEvent)
     * }. after validating & constructing all inputs and immediately before
     * running the balloon action function. Do your validations, and return
     * false if you want to suppress the call; or true to continue
     *
     * @param ae
     * @param entity
     * @param ofunction
     *
     * @return OBackBean function does nothing and returns true
     */
    protected boolean preRunBalloonFunction(ActionEvent ae, OFunction ofunction, BaseEntity entity) {
        if (!ae.getComponent().getId().contains(Balloon.WINDOWLINKPOSTFIX)) {
            if (ofunction instanceof ScreenFunction) {
                ((ScreenFunction) ofunction).setRunInPopup(true);
            }
        }
        return true;
    }

    /**
     * Backbean Life cycle function. Called by {@link OBackBean#balloonAction(ActionEvent)
     * }
     * after running the balloon action function
     *
     * OBackBean function does nothing.
     *
     * @param ae
     * @param entity
     * @param ofunction
     * @param ofrRunFunctionResult
     */
    protected void postRunBalloonFunction(ActionEvent ae, OFunction ofunction,
            BaseEntity entity, OFunctionResult ofrRunFunctionResult) {
    }
//    //FIXME: handle all the types of functions, ProcessFunction. etc...
//    //FIXME: exclude ScreenFunction as it is handled client side
    HtmlPanelGrid importPanelPopup = new HtmlPanelGrid();
    // <editor-fold defaultstate="collapsed" desc="Lookup vars, functions">
    /*
     * is the lookup grid that holds the lookup screen and cannot be in lookup class.
     */
    HtmlPanelGrid lookupPanelPopup = new HtmlPanelGrid();
    /*
     * is binded to *.jspx file to control lookup screen visibility therefore cannot be moved to Lookup class.
     */
    boolean lookupPanelRendered = false;
    protected boolean importDataPanelRendered = false;
    String recordID = "", lookupHeaderStr = "";

    public boolean isImportDataPanelRendered() {
        return importDataPanelRendered;
    }

    public void setImportDataPanelRendered(boolean importDataPanelRendered) {
        this.importDataPanelRendered = importDataPanelRendered;
    }

    public String getLookupHeaderStr() {
        return " Lookup For " + lookupHeaderStr + " ";
    }

    public void setImportPanelPopup(HtmlPanelGrid importPanelPopup) {
        this.importPanelPopup = importPanelPopup;
    }

    public List<String> removeEntryFromList(String elemKey, List<String> list) {
        logger.debug("Entering");
        if (list == null || list.isEmpty()) {
            logger.debug("Returning list which Empty");
            return list;
        }
        List<String> tmpList = new ArrayList<String>();
        tmpList.addAll(list);
        Iterator iterator = tmpList.iterator();
        while (iterator.hasNext()) {
            String string = (String) iterator.next();
            if (string.contains(elemKey)) {
                list.remove(string);
            }
        }
        logger.debug("Returning");
        return list;
    }

    public String returnImportData() {
        logger.debug("Entering");
        importDataPanelRendered = false;
        if (!isFileSelected) {
            UserMessage message = new UserMessage();
            message.setMessageText("You Should Upload Valid XML File.");
            message.setMessageTitle("Invalid File");
            message.setMessageType(UserMessage.TYPE_ERROR);
            messagesPanel.clearAndDisplayMessages(message);
        }
        logger.debug("Returning with Null");
        return null;
    }

    public String exitImportData() {
        importDataPanelRendered = false;
        return null;
    }

    public void logout() {
        RequestContext.getCurrentInstance().execute("top.FABS.Portlet.logOut()");
        RequestContext.getCurrentInstance().execute("window.top.location.href = '/c/portal/logout';");
    }

    public void screenMenuAction(ActionEvent actionEvent) {
        logger.debug("Entering");
        int count = count(liferayUsers.get(loggedUser.getLoginName()));
        if (count == 0) {
            logout();
            logger.debug("Returning");
            return;
        }
        logger.debug("Screen Menu Action Started");
        try {
            oem.getEM(loggedUser);
            if (actionEvent.getComponent().getId().contains("ScreenFLDMenuItem")) {
                ODataType oDataType = dataTypeService.loadODataType("ParentDBID", systemUser);
                if (oDataType != null) {
                    ODataMessage message = new ODataMessage();
                    message.setODataType(oDataType);
                    List data = new ArrayList();
                    data.add(currentScreen.getDbid());
                    message.setData(data);
                    String openedScreenInstId = generateScreenInstId("ScreenFieldsEntry");
                    SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.MESSAGE, message);
                    openScreenInPopup("ScreenFieldsEntry", openedScreenInstId, POPUP_SCREENTYPE);
                } else {
                    logger.debug("Returning");
                    return;
                }
                logger.debug("Returning");
                return;
            } else if (actionEvent.getComponent().getId().contains("ImportData")) {
                importDataPanelRendered = true;
                String openedScreenInstId = generateScreenInstId("ImportDataBean");
                SessionManager.addSessionAttribute("ImportDataCurrentScreen", getPortletInsId());
                openScreenInPopup("ImportDataBean", openedScreenInstId, POPUP_SCREENTYPE);
            } else if (actionEvent.getComponent().getId().contains("CurrentScreenDDs")) {
                String openedScreenInstId = generateScreenInstId("CurrentScreenDDs");
                ODataMessage message = new ODataMessage();
                ODataType oDataType = dataTypeService.loadODataType("OScreen", loggedUser);
                message.setODataType(oDataType);
                List data = new ArrayList();
                data.add(getCurrentScreen());
                message.setData(data);
                SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.MESSAGE,
                        message);
                openScreenInPopup("CurrentScreenDDs", openedScreenInstId, POPUP_SCREENTYPE);
            } else if (actionEvent.getComponent().getId().contains("toggleTipsMenuItem")) {
                toolTipRender = !toolTipRender;
            } else if (actionEvent.getComponent().getId().contains("managePagePortlets")) {
                portletPageAction();
            } else if (actionEvent.getComponent().getId().contains("screenPrsnlzMenuItem")
                    || actionEvent.getComponent().getId().contains("ScreenPrsnlzUpdateMenuItem")) {
                prsnlzMode = true;
                String openedScreenInstId = "";
                if (currentScreen instanceof FormScreen) {
                    ODataType odataType = dataTypeService.loadODataType("FormScreen", systemUser);
                    ODataMessage message = new ODataMessage();
                    if (odataType != null) {
                        message.setODataType(odataType);
                        List data = new ArrayList();
                        data.add(currentScreen.getDbid());
                        message.setData(data);
                    }
                    openedScreenInstId = generateScreenInstId("PRSNLZ_FormScreen");
                    SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.MESSAGE, message);
                    openScreenInPopup("PRSNLZ_FormScreen", openedScreenInstId, POPUP_SCREENTYPE);
                } else if (currentScreen instanceof TabularScreen) {
                    ODataType odataType = dataTypeService.loadODataType("TabularScreen", systemUser);
                    ODataMessage message = new ODataMessage();
                    if (odataType != null) {
                        message.setODataType(odataType);
                        List data = new ArrayList();
                        data.add(currentScreen.getDbid());
                        message.setData(data);
                    }
                    openedScreenInstId = generateScreenInstId("PRSNLZ_TabularScreen");
                    SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.MESSAGE, message);
                    openScreenInPopup("PRSNLZ_TabularScreen", openedScreenInstId, POPUP_SCREENTYPE);
                }
            } else if (actionEvent.getComponent().getId().contains("screenPrsnlzFLDSMenuItem")) {
                ODataType odataType = null;
                prsnlzMode = true;
                if (currentScreen instanceof FormScreen) {
                    odataType = dataTypeService.loadODataType("FormScreen", systemUser);
                } else if (currentScreen instanceof TabularScreen) {
                    odataType = dataTypeService.loadODataType("TabularScreen", systemUser);
                }
                if (odataType != null) {
                    ODataMessage message = new ODataMessage();
                    message.setODataType(odataType);
                    List data = new ArrayList();
                    data.add(currentScreen);
                    message.setData(data);
                    String openedScreenInstId = generateScreenInstId("PRSNLZ_ScreenFields");
                    SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.MESSAGE, message);
                    openScreenInPopup("PRSNLZ_ScreenFields", openedScreenInstId, POPUP_SCREENTYPE);
                } else {
                    logger.debug("Returning");
                    return;
                }
                logger.debug("Returning");
                return;

            } else if (actionEvent.getComponent().getId().contains("screenUndoPrsnlzMenuItem")) {
                UserMessage message = new UserMessage();
                OScreen copyScrn = getCurrentScreenCopy();
                if (copyScrn != null) {
                    OFunctionResult ofr = entitySetupService.callEntityDeleteAction(copyScrn, loggedUser);
                    if (ofr.getErrors() != null && !(ofr.getErrors().isEmpty())) {
                        message.setMessageText("Can not Undo Screen Personalization  ");
                        message.setMessageTitle("Deletion Error");
                        message.setMessageType(UserMessage.TYPE_ERROR);
                        messagesPanel.clearAndDisplayMessages(message);
                    } else {
                        RequestContext.getCurrentInstance().execute(
                                "top.FABS.Portlet.setSrc({screenName:'" + this.getCurrentScreen().getName()
                                + "',screenInstId:'" + this.getScreenInstId()
                                + "',screenHeight:'" + this.getScreenHeight()
                                + "',portletId:'" + this.getPortletInsId()
                                + "',viewPage:'" + this.getCurrentScreen().getViewPage()
                                + "',enableC2A:'" + c2AMode + "'});");
                    }
                }
            } else if (actionEvent.getComponent().getId().contains("ExportScreenMetaData")) {

                InputStream exportedStream = exportService.getEntitySqlAsInputStream(currentScreen, loggedUser);
                exportedFile = new DefaultStreamedContent(exportedStream, "text/html", "exported_file.sql");//text/html;charset=UTF-8//"image/jpg", "downloaded_optimus.jpg");
                RequestContext.getCurrentInstance().execute("simulateDownloadBtnClick();");

            } else if (actionEvent.getComponent().getId().contains("ExportScreenData")) {
                Object toBeExported;
                if (getCurrentScreen() instanceof TabularScreen) {
                    toBeExported = ((TabularBean) this).getAllEntitiesWithCurrentConds();
                } else {
                    toBeExported = getLoadedEntity();
                }
                if (toBeExported instanceof BaseEntity) {
                    InputStream exportedStream = exportService.getEntitySqlAsInputStream((BaseEntity) toBeExported, loggedUser);
                    exportedFile = new DefaultStreamedContent(exportedStream, "text/html", "exported_file.sql");//text/html;charset=UTF-8//"image/jpg", "downloaded_optimus.jpg");
                    RequestContext.getCurrentInstance().execute("simulateDownloadBtnClick();");
                } else if (toBeExported instanceof List) {
                    InputStream exportedStream = exportService.getEntitySqlAsInputStream((List) toBeExported, loggedUser);
                    exportedFile = new DefaultStreamedContent(exportedStream, "text/html", "exported_file.sql");
                    RequestContext.getCurrentInstance().execute("simulateDownloadBtnClick();");
                }
            } else if (actionEvent.getComponent().getId().contains("ExportCurrentPage")) {
                OPortalPage currentPage = uiFrameWorkFacade.getOportalPage(myPage.getId(), loggedUser);
                InputStream exportedStream = exportService.getEntitySqlAsInputStream(currentPage, loggedUser);
                exportedFile = new DefaultStreamedContent(exportedStream, "text/html", "exported_file.sql");//text/html;charset=UTF-8//"image/jpg", "downloaded_optimus.jpg");
                RequestContext.getCurrentInstance().execute("simulateDownloadBtnClick();");
            } else if (actionEvent.getComponent().getId().contains("PrintPage")) {
                RequestContext.getCurrentInstance().execute("top.FABS.Portlet.printPage();");
            } else if (actionEvent.getComponent().getId().contains("ExportExcel")) {
                RequestContext.getCurrentInstance().execute("DownloadDocument('XLS');");
            } else if (actionEvent.getComponent().getId().contains("ExportCsv")) {
                RequestContext.getCurrentInstance().execute("DownloadDocument('CSV');");
            } else if (actionEvent.getComponent().getId().contains("ExportPdf")) {
                RequestContext.getCurrentInstance().execute("DownloadDocument('PDF');");
            } else if (actionEvent.getComponent().getId().contains("AttachmentScreen")) {
                // this open to add dataMessage && open attachmentScreen
                if (this instanceof FormBean) {
                    String functionID = "1430304";
                    OFunction ofunction;
                    ofunction = functionServiceRemote.getOFunction(Long.parseLong(functionID), loggedUser);
                    if (ofunction == null) {
                        logger.warn("Error Loading Function ID");
                        messagesPanel.addErrorMessage(
                                usrMsgService.getUserMessage("SystemInternalError", systemUser));
                        logger.debug("Returning");
                        return;
                    }
                    if (ofunction instanceof ScreenFunction) {
                        ScreenFunction sfunction = (ScreenFunction) ofunction;
                        sfunction.setRunInPopup(true);

                    }
                    portalMode = true;
                    OFunctionResult oFR = new OFunctionResult();
                    BaseEntity entity = (BaseEntity) ((FormBean) this).getLoadedObject();
                    if (entity.getInstID() == null) {
                        entity.generateInstID();
                    }
                    entity.setPreSavedEntity(true);
                    oFR.append(runFunctionForEntity(ofunction, entity, ""));
                }
            } else if (actionEvent.getComponent().getId().contains("reload")) {
                refresh();
            } else if (actionEvent.getComponent().getId().contains("ExcelTemplate")) {
                exportExcelTemplate = ((TabularBean) this).createExcelTemplate();
                RequestContext.getCurrentInstance().execute("simulateDownloadBtnClickExportExcel();");
            } else if (actionEvent.getComponent().getId().contains("Help")) {
                String helpUrl = this.getCurrentScreen().getHelpUrl();
                if (null != helpUrl && !"".equals(helpUrl)) {
                    RequestContext.getCurrentInstance().execute("top.FABS.Portlet.helpPopup({screenName:'" + this.getCurrentScreen().getName()
                            + "',url:'" + helpUrl + "',title:'Help'});");
                } else {
                    // Need to handle empty url case.
                    UserMessage noHelpFoundMessage = usrMsgService.getUserMessage("noHelpFoundMessage", systemUser);
                    noHelpFoundMessage.setMessageType(UserMessage.TYPE_ERROR);
                    messagesPanel.addErrorMessage(noHelpFoundMessage);
                    RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
                }
            } else if (actionEvent.getComponent().getId().contains("screenEntityFilterMenuItem")) {
                openScreenInPopupAction("ManageEntityFilterScreenActions_1");
                logger.debug("Returning");
                return;
            } else if (actionEvent.getComponent().getId().contains("entityFiltersMenuItem")) {
                openScreenInPopupAction("ManageEntityFilters");
                logger.debug("Returning");
                return;
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
        }
    }

    private void openScreenInPopupAction(String screenName) {
        ODataType oDataType = dataTypeService.loadODataType("ParentDBID", systemUser);
        if (oDataType != null) {
            ODataMessage message = new ODataMessage();
            message.setODataType(oDataType);
            SessionManager.addSessionAttribute("ParentScreenFromMenuItem", currentScreen);
            List data = new ArrayList();
            data.add(entitySetupService.getOScreenEntityDTO(currentScreen.getDbid(), loggedUser).getDbid());
            message.setData(data);
            String openedScreenInstId = generateScreenInstId(screenName);
            SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.MESSAGE, message);
            openScreenInPopup(screenName, openedScreenInstId, POPUP_SCREENTYPE);
        }
    }

    AlertServiceLocal alertServiceLocal = OEJB.lookup(AlertServiceLocal.class);

    protected void throwInputNotSupported(ODataType oDT)
            throws InputNotSupportedException {
        logger.warn("Input Data Not Supported");
        InputNotSupportedException insex = new InputNotSupportedException("Input Data not supported");
        insex.setODataType(oDT);
        insex.setOScreen(currentScreen);
        throw insex;
    }

    protected void throwOutputNotSupported(ODataType oDT)
            throws OutputNotSupportedException {
        logger.warn("Ouput Data Not Supported");
        OutputNotSupportedException onsex = new OutputNotSupportedException("Output Data not supported");
        onsex.setODataType(oDT);
        onsex.setOScreen(currentScreen);
        throw onsex;
    }

    /**
     * Return the screen key value from the screen session attribute
     *
     * @param Key
     * @return
     */
    protected Object getScreenParam(String Key) {
        logger.trace("Entering");
        String screenSessionKeyName = screenInstId /*+ "_" + layoutPlid */;
        HashMap<String, Object> sessionAttribute
                = (HashMap<String, Object>) SessionManager.getSessionAttribute(screenSessionKeyName);
        if (null == sessionAttribute || sessionAttribute.isEmpty()) {
            sessionAttribute = (HashMap<String, Object>) nMDataLoader.getScreensSessionInfo().get(screenSessionKeyName);
        }
        if (sessionAttribute != null) {
            screenParamsMap.putAll(sessionAttribute);
            return screenParamsMap.get(Key);
        } else {
            if (currentScreen != null && !standalonePortletScreens.contains(currentScreen.getName())) {
                logger.warn("Screen Session Attribute Not Found");
            }
            logger.trace("Returning with Null");
            return null;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Screen Data Backup, and Restore">
    public String backupDataAction() {
        logger.debug("Entering");
        SessionManager.addSessionAttribute(portletId + "_" + layoutPlid, screenParamsMap);
        backupScreenData();
        logger.debug("Returning with Null");
        return null;
    }

    protected void backupObject(String key, Object value) {
        logger.debug("Entering");
        SessionManager.addSessionAttribute(portletId + "_" + layoutPlid + "_" + key, value);
        logger.debug("Returning");
    }

    public void backupScreenData() {
    }

    protected Object restoreObject(String key) {
        Object obj = SessionManager.getSessionAttribute(portletId + "_" + layoutPlid + "_" + key);
        SessionManager.addSessionAttribute(portletId + "_" + layoutPlid + "_" + key, null);
        return obj;
    }
    // </editor-fold>

    /**
     * <br>You can override this function (and don't call it) in your screen to
     * support inputs that need complex validation and not just using
     * {@link OBackBean#getValidScreenInputs()}; and call the following lines at
     * the end of your function if (currentScreenInput == null &&
     * !restorePDataFromSessoion) { throw new
     * OBackBeanException("InvalidInput"); }
     *
     * @throws OBackBeanException
     */
    protected void validateInputSetMapping()
            throws OBackBeanException {
        logger.debug("Entering");
        if (dataMessage == null) {
            throw new OBackBeanException("InvalidInput");
        }
        // FIXME: need to validate datamessage data is consistent with the data,
        // a function in DataTypeService

        if (currentScreenInput != null) {
            // Found a match in currentScreen.screenInputs, work on it
            try {
                // Fill the
                List<ScreenDTMappingAttrDTO> mapAttrs = currentScreenInput.getAttributesMapping();
                if (mapAttrs.isEmpty()) {
                    // Input has no attribute mapping
                    // Do Nothing
                    logger.debug("Returning");
                    return;
                }
                // Input has attribute mappings

                // FIXME:
                // The follwing code is left for backward compatibility, but needs
                // strong revision, why do we allow null DTAttr in Attribute Mapping
                // and why do we fill them manually and by what logic?
                // <editor-fold defaultstate="collapsed" desc="??? Code">
                // Load DT Attributes from Database, as they may not be already loaded, and
                // to work only on active  ones
                List<ODataTypeAttribute> odtAttrs = oem.loadEntityList(ODataTypeAttribute.class.getSimpleName(),
                        Collections.singletonList("oDataType.dbid=" + dataMessage.getODataType().getDbid()),
                        null, null, oem.getSystemUser(loggedUser));

                if (!odtAttrs.isEmpty()) {
                    for (int i = 0; i < mapAttrs.size(); i++) {
                        if (mapAttrs.get(i).getOdataTypeAttributeDBID() == 0) {
                            logger.warn("Code Needs Tuning");
                            if (odtAttrs.size() > i) {
                                mapAttrs.get(i).setOdataTypeAttributeDBID(odtAttrs.get(i).getDbid());
                            } else if (odtAttrs.size() > 0) {
                                mapAttrs.get(i).setOdataTypeAttributeDBID(odtAttrs.get(odtAttrs.size() - 1).getDbid());
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
            logger.debug("Returning");
            // </editor-fold>
        }

        // Validation On Supported Input
        if (currentScreenInput == null && !restorePDataFromSessoion) {
            throw new OBackBeanException("InvalidInput");
        }
    }

    public DynamicNodeUserObject getSelectedNodeObject() {
        return entityFieldsTreeBrowser.getSelectedNodeAsDynamic();
    }

    public void nodeActionListener(NodeSelectEvent event) {
        try {
            entityFieldsTreeBrowser.nodeActionListener(event);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Tree ExpandListener For Lookup">
    public void lookupExpandListener(ActionEvent actionEvent) {
        try {
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
    }
    //</editor-fold>
    protected boolean showManagePortlePage = false;

    public boolean isShowManagePortlePage() {
        return showManagePortlePage;
    }

    public void setShowManagePortlePage(boolean showManagePortlePage) {
        this.showManagePortlePage = showManagePortlePage;
    }

    public String portletPageAction() {
        logger.debug("Entering");
        ODataType entityDataType = null;
        String openedScreenInstId = "";
        try {
            entityDataType = dataTypeService.loadODataType("ParentDBID", systemUser);

//            List<String> pConditions = new ArrayList<String>();
//            pConditions.add("portletPageID = '" + pageId + "'");//layoutPlid
////            pConditions.add("name = '" + getCurrentScreen().getViewPage() + "'");
//            OPortalPage oPortalPage = (OPortalPage) oem.loadEntity(OPortalPage.class.getSimpleName(),
//                    pConditions, null, loggedUser);
            if (entityDataType != null) {
                ODataMessage outPutMessage = new ODataMessage();
                List data = new ArrayList();
                data.add(pageId);
                outPutMessage.setODataType(entityDataType);
                outPutMessage.setData(data);

                OScreen screen = uiFrameWorkFacade.getScreenObject("PortalPagePortlet", loggedUser);
                //construct screen instance id to use in saving session info
                openedScreenInstId = generateScreenInstId("PortalPagePortlet");
                SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.MESSAGE, outPutMessage);
                SessionManager.setScreenSessionAttribute(openedScreenInstId, MODE_VAR, "20");
                SessionManager.setScreenSessionAttribute(openedScreenInstId, OBackBean.WIZARD_VAR, false);
                SessionManager.setScreenSessionAttribute(openedScreenInstId, "showSaveExit", Boolean.TRUE);

                // generate screenInstanceId variable
                RequestContext context = RequestContext.getCurrentInstance();
                try {
                    screen = (OScreen) textTranslationServiceRemote.loadEntityTranslation(screen, loggedUser);
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }

                openScreenInPopup(screen.getName(),
                        screen.getViewPage(),
                        screen.getHeaderTranslated(),
                        openedScreenInstId, POPUP_SCREENTYPE);
//                context.execute(
//                        "top.FABS.Portlet.open({screenName:'" + screen.getName()
//                        + "',screenInstId:'" + openedScreenInstId
//                        + "',viewPage:'" + screen.getViewPage()
//                        + "',portletTitle:'" + screen.getHeaderTranslated()
//                        + "',enableC2A:'false'});");
                logger.debug("Returning with Null");
                return null;
            } else {
                logger.debug("Returning with Null");
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning with Null");
        return null;
    }

    public void openFilter(BaseEntity entity, ODataMessage message, FilterType type, String toBOpenedScreenInstId) {
        try {
            String screenName = "RunFilter";
            OScreen filterScreen = (OScreen) oem.loadEntity(OScreen.class.getSimpleName(),
                    Collections.singletonList("name = '" + screenName + "'"), null, systemUser);
            String filterScreenInstId = generateScreenInstId(filterScreen.getName());

            SessionManager.setScreenSessionAttribute(filterScreenInstId, RunFilterBean.PASSED_VAR, entity);
            SessionManager.setScreenSessionAttribute(filterScreenInstId, RunFilterBean.FILTER_TYPE_VAR, type);
            SessionManager.setScreenSessionAttribute(filterScreenInstId, RunFilterBean.FILTER_MESSAGE_VAR, message);
            SessionManager.setScreenSessionAttribute(filterScreenInstId,
                    RunFilterBean.TO_B_OPENED_SCREEN_INST_ID_VAR, toBOpenedScreenInstId);

            openScreen(filterScreen, filterScreenInstId);
        } catch (UserNotAuthorizedException ex) {
            logger.error("Exception thrown", ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Wizard Buttons Action">
    //This Method to be overridden in the TabularBean & FormBean to save the data and send the data message
    public String wizardNextAction() {
        return "";
    }

    public String wizardNextAction(ODataMessage wizardMessage) {
        wizardPanel.disbleButtons();
        wizardCtrl.next(getPortletInsId(), wizardMessage);
        return "";
    }

    public String wizardBackAction() {
        wizardPanel.disbleButtons();
        wizardCtrl.back(getPortletInsId());
        return "";
    }

    public String wizardSkipAction() {
        wizardPanel.disbleButtons();
        wizardCtrl.skip(getPortletInsId(), screenInstId);
        return "";
    }

    //This Method to be overridden in the TabularBean & FormBean to save the data
    public String wizardFinishAction() {
        wizardCtrl.finish(getPortletInsId(), screenInstId);
        return "";
    }

    // </editor-fold>
    protected UIComponent createTextFieldControl(int controlSize, String id, String fieldExpression,
            boolean editable,
            DD screenFieldDD, int screenFieldIndex, boolean isMandatory, boolean isUnique, boolean specialchar) {
        logger.trace("Entering");
        InputText inputText = new InputText();
        inputText.setSize(controlSize);
        FramworkUtilities.setControlFldExp(inputText, fieldExpression);
        //That id is used in the manadatory message
        buildScreenExpression(inputText);
        inputText.setId(id);
        inputText.setStyleClass("OTMSTextBox");
        inputText.setTabindex(Integer.toString(screenFieldIndex));

        inputText.setValueExpression(
                "value",
                exFactory.createValueExpression(
                        elContext,
                        fieldExpression,
                        String.class));
        inputText.setDisabled(!editable);
        if (isUnique) {
            Class[] parms = new Class[]{FacesContext.class, UIComponent.class, Object.class};
            MethodExpression mb1 = getFacesContext().getApplication().getExpressionFactory()
                    .createMethodExpression(elContext, "#{" + getBeanName()
                            + ".validateUniquness}", null, parms);
            inputText.addValidator(new MethodExpressionValidator(mb1));
            UserMessage userMessage = usrMsgService.getUserMessage("UniqueValidation", systemUser);
            inputText.setValidatorMessage(userMessage.getMessageTextTranslated());
        }

        if (specialchar) {
            Class[] parms = new Class[]{FacesContext.class, UIComponent.class, Object.class};
            MethodExpression mb1 = getFacesContext().getApplication().getExpressionFactory()
                    .createMethodExpression(elContext, "#{" + getBeanName()
                            + ".validateSpecialChar}", null, parms);
            inputText.addValidator(new MethodExpressionValidator(mb1));
            UserMessage userMessage = usrMsgService.getUserMessage("SpecialChar", systemUser);
            inputText.setValidatorMessage(userMessage.getMessageTextTranslated());
        }

        if (isMandatory) {
            inputText.setRequired(isMandatory);

            HtmlPanelGrid panelGrid = new HtmlPanelGrid();
            panelGrid.setColumns(1);
            panelGrid.getChildren().add(inputText);
            HtmlMessage message = new HtmlMessage();
            UserMessage userMessage = usrMsgService.getUserMessage("MandatoryField", systemUser);
            inputText.setRequiredMessage(screenFieldDD.getLabel() + " "
                    + userMessage.getMessageTextTranslated());
            message.setTitle(userMessage.getMessageTitleTranslated());
            message.setFor(id);
            panelGrid.getChildren().add(message);
            logger.trace("Returning");
            return panelGrid;
        }
        logger.trace("Returning");
        return inputText;
    }

    protected UIComponent createCalendarControl(String id, String fieldExpression,
            int sortIndex, boolean editable,
            DD screenFieldDD, int screenFieldIndex, boolean isMandatory) {
        return createCalendarControlGeneric(id, fieldExpression, sortIndex, editable, screenFieldDD, screenFieldIndex, isMandatory);

    }

    protected final UIComponent createCalendarControlGeneric(String id, String fieldExpression,
            int sortIndex, boolean editable,
            DD screenFieldDD, int screenFieldIndex, boolean isMandatory) {
        logger.trace("Entering");
        org.primefaces.component.calendar.Calendar inputDate = new org.primefaces.component.calendar.Calendar();
        inputDate.setStyle("width:" + screenFieldDD.getControlSize());
        FramworkUtilities.setControlFldExp(inputDate, fieldExpression);
        if (mode == OScreen.SM_ADD) {
            inputDate.setValue(timeZoneService.getUserCDT(loggedUser));
        }
        buildScreenExpression(inputDate);
        inputDate.setPattern("MMM dd, yyyy");
        inputDate.setEffect("slideDown");
        inputDate.setReadonlyInput(true);
        if (htmlDir.equals("rtl")) {
            inputDate.setStyle("text-align:right");
        }
        inputDate.setValueExpression(
                "value",
                exFactory.createValueExpression(
                        elContext,
                        fieldExpression,
                        Date.class));

        inputDate.setNavigator(true);
        inputDate.setDisabled(!editable);
        if (isMandatory) {
            HtmlPanelGrid panelGrid = new HtmlPanelGrid();
            panelGrid.setColumns(1);
            panelGrid.getChildren().add(inputDate);
            panelGrid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_calendar_");
            HtmlMessage message = new HtmlMessage();
            UserMessage userMessage = usrMsgService.getUserMessage("MandatoryField", systemUser);
            inputDate.setRequiredMessage("this field is mandatory");
            message.setTitle(userMessage.getMessageTitleTranslated());
            message.setFor(id + "_inputText"
                    + screenFieldIndex);
            message.setStyle("color:red;font-size:15px;");
            panelGrid.getChildren().add(message);
            logger.trace("Returning");
            return panelGrid;
        }
        logger.trace("Returning");
        return inputDate;
    }

    protected UIComponent createCheckBoxControl(String id, String fieldExpression,
            boolean editable,
            DD screenFieldDD, int screenFieldIndex, boolean isMandatory) {
        logger.trace("Entering");
        SelectBooleanCheckbox checkbox = new SelectBooleanCheckbox();
        checkbox.setId(id);
        checkbox.setImmediate(true);
        FramworkUtilities.setControlFldExp(checkbox, fieldExpression);
        buildScreenExpression(checkbox);
        checkbox.setStyleClass("OTMSCheckBox");
        checkbox.setTabindex(Integer.toString(screenFieldIndex));
        if (screenFieldDD != null && screenFieldDD.getDefaultValue() != null
                && (screenFieldDD.getDefaultValue().equals("1")
                || screenFieldDD.getDefaultValue().equalsIgnoreCase("true")) && mode == OScreen.SM_ADD) {
            checkbox.setValue(true);
        }
        checkbox.setDisabled(!editable);
        checkbox.setValueExpression(
                "value",
                exFactory.createValueExpression(
                        elContext,
                        fieldExpression,
                        Boolean.class));

        if (isMandatory) {
            checkbox.setRequired(isMandatory);
            HtmlPanelGrid panelGrid = new HtmlPanelGrid();
            panelGrid.setColumns(1);
            panelGrid.getChildren().add(checkbox);
            HtmlMessage message = new HtmlMessage();
            UserMessage userMessage = usrMsgService.getUserMessage("MandatoryField", loggedUser);
            checkbox.setRequiredMessage(screenFieldDD.getLabel() + " "
                    + userMessage.getMessageTextTranslated());
            message.setTitle(userMessage.getMessageTitleTranslated());
            message.setFor(fieldExpression + "_inputText"
                    + screenFieldIndex);
            panelGrid.getChildren().add(message);
            logger.trace("Returning");
            return panelGrid;
        }
        logger.trace("Returning");
        return checkbox;
    }

    protected UIComponent createLabelControl(String fieldExpression,
            DD screenFieldDD, int screenFieldIndex) {
        logger.trace("Entering");
        HtmlOutputLabel outputLabel = new HtmlOutputLabel();
        outputLabel.setId(FacesContext.getCurrentInstance().getViewRoot().
                createUniqueId() + "_OTMSLBLControl");
        outputLabel.setStyleClass("OTMSLabel");
        outputLabel.setValueExpression(
                "value",
                exFactory.createValueExpression(
                        elContext,
                        fieldExpression,
                        String.class));
        FramworkUtilities.setControlFldExp(outputLabel, fieldExpression);
        outputLabel.setTabindex(Integer.toString(screenFieldIndex));
        if (mode == OScreen.SM_ADD) {
            outputLabel.setValue(screenFieldDD.getDefaultValue());
        }
        logger.trace("Returning");
        return outputLabel;
    }

    protected UIComponent createTextAreaControl(String id, String fieldExpression,
            boolean editable,
            DD screenFieldDD, int screenFieldIndex, boolean isMandatory) {
        logger.trace("Entering");
        InputTextarea inputTextarea = new InputTextarea();
        inputTextarea.setId(id);
        inputTextarea.setImmediate(true);
        FramworkUtilities.setControlFldExp(inputTextarea, fieldExpression);
        buildScreenExpression(inputTextarea);
        inputTextarea.setStyleClass("OTMSTextAreaBox");
        inputTextarea.setStyle("width:280px;height:50px;overflow: auto;");
        inputTextarea.setTabindex(Integer.toString(screenFieldIndex));
        if (mode == OScreen.SM_ADD) {
            inputTextarea.setValue(screenFieldDD.getDefaultValue());
        }
        inputTextarea.setDisabled(!editable);
        if (isMandatory) {
            inputTextarea.setRequired(isMandatory);
            HtmlPanelGrid panelGrid = new HtmlPanelGrid();
            panelGrid.setColumns(1);
            panelGrid.getChildren().add(inputTextarea);
            HtmlMessage message = new HtmlMessage();
            UserMessage userMessage = usrMsgService.getUserMessage("MandatoryField", loggedUser);
            inputTextarea.setRequiredMessage(screenFieldDD.getLabel() + " "
                    + userMessage.getMessageTextTranslated());
            message.setTitle(userMessage.getMessageTitleTranslated());
            message.setFor(fieldExpression + "_inputText"
                    + screenFieldIndex);
            panelGrid.getChildren().add(message);
            logger.trace("Returning");
            return panelGrid;
        }
        logger.trace("Returning");
        return inputTextarea;
    }

    protected UIComponent createFile(String fieldExpression, DD screenFieldDD, int screenFieldIndex) {
        logger.trace("Entering");
        CommandLink fileLink = new CommandLink();
        fileLink.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        fieldExpression = fieldExpression + ".name";
        FramworkUtilities.setControlFldExp(fileLink, fieldExpression);

        fileLink.setPartialSubmit(true);
        fileLink.setImmediate(true);
        fileLink.setTabindex(Integer.toString(screenFieldIndex));
        buildScreenExpression(fileLink);

        Class[] parms4 = new Class[]{ActionEvent.class};
        MethodExpression mb1 = getFacesContext().getApplication().getExpressionFactory().
                createMethodExpression(elContext, "#{" + getBeanName()
                        + ".downloadFile}", null, parms4);
        fileLink.addActionListener(new MethodExpressionActionListener(mb1));
        logger.trace("Returning with File Link");
        return fileLink;
    }

    protected abstract void buildScreenExpression(UIComponent component);

    //<editor-fold defaultstate="collapsed" desc="Gear Box on Portlet Header">
    /**
     * Creates menu item for screen fields of the screen. User authority is
     * checked. Calls javascript createPortletOption(). Action is got from al
     * <br>Function doesn't try catch
     *
     * @param al
     * @return menu item if user is authorized and no error found, else, null
     */
    protected MenuItem createScreenFLDMenuItem(MethodExpressionActionListener al) {
        logger.trace("Entering");
        if (!entitySetupService.checkPrivilegeAuthorithy("ScreenFieldsOnSpot", loggedUser)) {
            logger.trace("Returning with Null");
            return null;
        }

        String screenFLDsMenuItemDD = ddService.getDDLabel("screenFLDsMenuItem", loggedUser);
        MenuItem screenFLDsMenuItem = new MenuItem();

        screenFLDsMenuItem.setId(FacesContext.getCurrentInstance().getViewRoot()
                .createUniqueId() + "ScreenFLDMenuItem");
        screenFLDsMenuItem.setImmediate(true);
        screenFLDsMenuItem.setPartialSubmit(true);
        screenFLDsMenuItem.setAjax(true);
        screenFLDsMenuItem.addActionListener(al);
        screenFLDsMenuItem.setValue(screenFLDsMenuItemDD);
        if (currentScreen instanceof FormScreen) {
            screenFLDsMenuItem.setUpdate("frmScrForm:middlePanel");
        } else if (currentScreen instanceof TabularScreen) {
            screenFLDsMenuItem.setUpdate("tf:generalPanel");
        }
        logger.trace("Returning");
        return screenFLDsMenuItem;
    }

    protected MenuItem createEntityFilterMenuItem(MethodExpressionActionListener al, String ddLabel) {
        logger.trace("Entering");
        String entityFilterMenuItemDD = ddService.getDDLabel(ddLabel, loggedUser);
        MenuItem entityFilterMenuItem = new MenuItem();
        entityFilterMenuItem.setId(FacesContext.getCurrentInstance().getViewRoot()
                .createUniqueId() + ddLabel);
        entityFilterMenuItem.setImmediate(true);
        entityFilterMenuItem.setPartialSubmit(true);
        entityFilterMenuItem.setAjax(true);
        entityFilterMenuItem.addActionListener(al);
        entityFilterMenuItem.setValue(entityFilterMenuItemDD);
        if (currentScreen instanceof FormScreen) {
            entityFilterMenuItem.setUpdate("frmScrForm:middlePanel");
        } else if (currentScreen instanceof TabularScreen) {
            entityFilterMenuItem.setUpdate("tf:generalPanel");
        }
        logger.trace("Returning");
        return entityFilterMenuItem;
    }

    /**
     * Action of TranslationMenuItem created by {@link #createScreenTransMenuItem()
     * }. Action Must Be Implemented in Screen SubClass Code
     *
     * @param actionEvent
     */
    public void screenTranslationMenuAction(ActionEvent actionEvent) {
        logger.trace("Action Must Be Implemented in Screen Code");
    }

    /**
     * Creates menu item for screen translation of the screen. User authority is
     * checked. Calls javascript createPortletOption(). Action is
     * screenTranslationMenuAction. Override {@link #screenTranslationMenuAction(javax.faces.event.ActionEvent)
     * } to implement your function
     *
     * <br>Function doesn't try catch
     *
     * @param al
     * @return menu item if user is authorized and no error found, else, null
     */
    protected CommandLink createScreenTransMenuItem() {
        logger.trace("Entering");
        if (!entitySetupService.checkPrivilegeAuthorithy("ScreenTranslationOnSpot", loggedUser)) {
            logger.trace("Returning with Null");
            return null;
        }
        CommandLink screenTransMenuItem = new CommandLink();
        screenTransMenuItem.setImmediate(false);
        String screenTransMenuItemDD = ddService.getDDLabel("screenTransMenuItem", loggedUser);
        screenTransMenuItem.setId(FacesContext.getCurrentInstance().getViewRoot()
                .createUniqueId() + "ScreenTransMenuItem");
        MethodExpression transMB = exFactory.createMethodExpression(
                FacesContext.getCurrentInstance().getELContext(),
                "#{" + getBeanName() + ".screenTranslationMenuAction}", null,
                new Class[]{ActionEvent.class});
        MethodExpressionActionListener transAl = new MethodExpressionActionListener(transMB);
        screenTransMenuItem.addActionListener(transAl);
        screenTransMenuItem.setValue("");
        onloadJSStr += "top.FABS.Portlet.createPortletOption({portletId:'" + getPortletInsId()
                + "', optionsTXT:'" + screenTransMenuItemDD
                + "', actionName:'ScreenTransMenuItem"
                + "', subMenu:'GENERAL'});";
        logger.trace("Returning");
        return screenTransMenuItem;
    }

    public boolean isScreenOpenedForLookup() {
        return (screenParamsMap.get("isLookup") != null
                && screenParamsMap.get("isLookup").toString().equals("true"));
    }

    // <editor-fold defaultstate="collapsed" desc="Screen Inputs/Outputs Handling ">
    /**
     * Returns the valid "Supported" & "Natively Supported" screen inputs to be
     * validated and used in the screen <br> <br>{@link OBackBean#currentScreen}
     * should be set first <br>Override it to add your screen-specific inputs
     * and/or alter original screen inputs
     *
     * @return List: Filled with {@link #currentScreen} inputs, empty if there
     * are no inputs <br>null: error
     */
    protected List<ScreenInputDTO> getValidScreenInputs() {
        logger.trace("Entering");
        try {
            List<ScreenInputDTO> screenInputs = new ArrayList<ScreenInputDTO>();
            // Add inputs to a new list to avoid changing the original one
            screenInputs.addAll(uiFrameWorkFacade.getScreenInputDTOs(currentScreen.getDbid(), loggedUser));//
            logger.trace("Returning");
            return screenInputs;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Returning with Null");
            return null;
        }
    }

    /**
     * Initializes {@link OBackBean#currentScreenInput} to the supported input
     * corresponding to the passed {@link OBackBean#dataMessage}. Valid Screen
     * Inputs are got using {@link OBackBean#getValidScreenInputs()}. <br>
     * <br>You can override this function in your screen bean to support inputs
     * that need complex ScreenInput generation and not just overriding
     * {@link OBackBean#getValidScreenInputs()}
     *
     * <br>Precondition: {@link OBackBean#dataMessage} is set <br>Postcondition:
     * {@link #currentScreenInput} is null if no valid input found, otherwise,
     * it's assigned with the input
     */
    protected void initCurrentScreenInput() {
        try {
            currentScreenInput = null;  // Initialize it to null for refresh case

            if (dataMessage == null) // No data message passed, usually in menu
            // Do nothing, leave input Null
            {
                return;
            }

            // Set currentScreenInput to matched ScreenInput if found
            // Search for ScreenInput matched with dataMessage, and set currentScreenInput
            // to it if found
            List<ScreenInputDTO> oscreenInputDTOs = getValidScreenInputs();
            for (ScreenInputDTO oscreenInput : oscreenInputDTOs) {
                if (oscreenInput.isInActive()) // Ignore not active ones
                {
                    continue;
                }

                if (oscreenInput.getOdataTypeDBID()
                        != dataMessage.getODataType().getDbid()) // Mismatched
                // Ignore it
                {
                    continue;
                }

                // Not a deleted screenInput & found matched with datamessage
                if (currentScreenInput != null) {
                    // More than two inputs found maching
                    // Don't break or return, we take the last one found for
                    // backward compatibility
                    logger.warn("More Than One ScreenInput Matched With Input DataMessage");
                }
                // Set it
                //FIXME: should we break here? If not, then it's the last
                // matching one that's got

                currentScreenInput = oscreenInput;
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(
                    usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
        }
    }

    /**
     * Sets {@link #compositeInputRequired}s & fills {@link #compositeInputs}.
     * It must be consistent wcurrentScreenInputith filling odm in
     * {@link #processInput()}. Gets inputs using {@link #getValidScreenInputs()
     * }
     */
    protected boolean initCompositeInputInfo() {
        logger.trace("Entering");
        try {
            // Init variables
            compositeInputRequired = false;
            compositeInputs = new ArrayList<OScreenCompositeInput>();

            List<ScreenInputDTO> screenInputs = getValidScreenInputs();
            if (screenInputs == null) // No input needed for screen
            {
                logger.trace("Returning with True as screenInputs equals Null");
                return true;   // Do nothing, not an error
            }
            for (ScreenInputDTO screenInputDTO : screenInputs) {
                ScreenInput input = null;
                if (screenInputDTO.getDbid() != 0) {
                    input = (ScreenInput) oem.loadEntity(ScreenInput.class.getSimpleName(),
                            Collections.singletonList("dbid=" + screenInputDTO.getDbid()),
                            Collections.singletonList("attributesMapping"), loggedUser);
                } else {
                    input = uiFrameWorkFacade.getScreenInputIfDataTypeDBIDOrVoid(screenInputDTO.getDbid(), loggedUser);
                }
                for (ScreenDTMappingAttr attr : input.getAttributesMapping()) {
                    if (attr.isPartOfCompKey()) {
                        compositeInputRequired = true;
                        OScreenCompositeInput compInput = new OScreenCompositeInput();
                        compInput.input = input;
                        compInput.attr = attr;
                        compositeInputs.add(compInput);
                        //TODO: validate attribute is ready for that, e.g. has initialization
                        // field expression, ???
                        if (attr.getFieldExpression() == null) {
                            logger.warn("Composite Input Must Have fieldExpression");
                            //FIXE: use informative message
                            messagesPanel.addErrorMessage(
                                    usrMsgService.getUserMessage("SystemInternalError", systemUser));

                            HashMap<String, Object> compInputMap = new HashMap<String, Object>();
                            compInputMap.put("test", compInput);
                            compInputMap.remove("test");
                        }
                    }
                }
            }
            logger.trace("Returning with True");
            return true;

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(
                    usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
            logger.trace("Returning with False");
            return false;
        }
    }

    /**
     * Validates input, no actual processing done. If input is invalid, a user
     * message will be displayed, and false is returned <br>Validations: <br>1.
     * {@link OBackBean#dataMessage}, {@link OBackBean#currentScreenInput}, are
     * not null and {@link {@link ODataMessag#isValid() }
     * <br>It also initializes {@link #compositeInputs } if {@link #compositeInputRequired}
     * is true, add ODM if its {@link ODataMessage#oDataType} is one of the
     * {@link #compositeInputs} attributes ODT, keeps one ODM per ODT. This
     * must be consistent with logic in {@link #initCompositeInputInfo() }
     *
     * <br>Preconditions: {@link OBackBean#dataMessage} & {@link OBackBean#currentScreenInput}
     * are set, otherwise, it will always return false
     *
     * @return
     * true: input is valid
     * <br>false: invalid input
     */
    protected boolean processInput() {
        logger.trace("Entering");
        try {
            if (dataMessage == null || currentScreenInput == null) {
                logger.warn("processInput Error, data Message or Screen Input equals Null");
                return false;
            }
            if (!dataMessage.getODataType().getName().equals("VoidDT")
                    && (dataMessage.getData() == null || dataMessage.getData().isEmpty()
                    || (dataMessage.getData().get(0) instanceof BaseEntity
                    && ((BaseEntity) dataMessage.getData().get(0)).getDbid() == 0))) {
                invalidInputMode = true;
                logger.trace("Returning with False");
                return false;
            }
            // <editor-fold defaultstate="collapsed" desc="Update compositeInputs with the input if relevant">
            if (compositeInputRequired) {
                //compositeInputs= new ArrayList<OScreenCompositeInput>();
                HashMap<String, Object> screenParamMap = (HashMap<String, Object>) SessionManager.getSessionMap().get(screenInstId.substring(0, screenInstId.lastIndexOf("_")));
                HashMap<String, Object> passedInputsMap = new HashMap<String, Object>();
                if (null != screenParamMap) {
                    passedInputsMap = (HashMap<String, Object>) screenParamMap.get(OPortalUtil.SCREENPASSEDINPUT);
                }

                // Update composite key with values in passedInputMaps
                if (passedInputsMap.isEmpty()) {
                    logger.warn("Processing Composite Input for Empty Passed Parameters");
                } else {
                    // Passed inputs map is not empty
                    for (OScreenCompositeInput compInput : compositeInputs) {
                        for (Map.Entry<String, Object> passedEntry : passedInputsMap.entrySet()) {
                            if (compInput.attr.getOdataTypeAttribute().getODataType().getDbid()
                                    == Long.valueOf(passedEntry.getKey())) {
                                compInput.odm = (ODataMessage) passedEntry.getValue();
                                break;
                            }
                        }
                        if (null == compInput.odm
                                && passedInputsMap.size() == compositeInputs.size()) {
                            logger.warn("omposite Input Not Found In Passed Parameters");
                        }
                    }
                }
            }
            // </editor-fold>
            logger.trace("Returning with True");
            return true;

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(
                    usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
            logger.trace("Returning with False");
            return false;
        }
    }
    /**
     * Is true if one or more of the valid screen inputs attributes have
     * {@link ScreenDTMappingAttr#partOfCompKey} = true. Is set in {@link #initCompositeInputInfo()
     * }. If it's true, then, screen should wait until all composite key
     * attributes are processed (in {@link #prcessInput()} to add new row,
     * otherwise, it will display error message.
     */
    protected boolean compositeInputRequired = false;
    /**
     * Is maintained in {@link #processInput() }
     */
    protected List<OScreenCompositeInput> compositeInputs = new ArrayList<OScreenCompositeInput>();

    /**
     * Return true if all input attributes of {@link #compositeInput} have
     * corresponding inputs ODM in {@link #compositeInputs}, or, if
     * {@link #compositeInputRequired} is false; otherwise, returns false
     */
    public boolean areAllCompositeInputsAvailable() {
        logger.debug("Entering");
        try {
            if (!compositeInputRequired) {
                logger.debug("Returning with True");
                return true;
            }

            for (OScreenCompositeInput compInput : compositeInputs) {
                if (compInput.odm == null) {
                    logger.warn("OData Message Missing For Composite Input");
                    logger.debug("Returning with False");
                    return false;
                }
                if (null == compInput.attr) {
                    logger.warn("Attribute is null while OData Message is not null");
                    logger.debug("Returning with False");
                    return false;
                }
                int x = 0; // Dummy for debugger which misleadingly steps

            }
            logger.debug("Returning with True");
            return true;

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(
                    usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
            logger.debug("Returning with False");
            return false;
        }
    }

    /**
     * Initialize new entity instance with the composite input values. You need
     * to check first {@link #areAllCompositeInputsAvailable()} to save time,
     * otherwise, an error will be returned if .
     *
     * @param entity
     * @return
     */
    public OFunctionResult initEntityWithComposedInput(BaseEntity entity) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            for (OScreenCompositeInput compInput : compositeInputs) {
                if (compInput.odm == null || compInput.attr == null) {
                    logger.warn("Invalid Composite Input , equals Null");
                    // FIXME: put relevant error message
                    oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser));
                    logger.debug("Returning");
                    return oFR;
                }
                if (!compInput.attr.isForInitialization()) // Not for intialization
                {
                    continue;
                }

                // Valid odm & attr for initialization
                // Initialize the corresponding field
                entity.NewFieldsInstances(
                        Collections.singletonList(compInput.attr.getFieldExpression()));
                FramworkUtilities.setValueInEntity(
                        entity,
                        compInput.attr.getFieldExpression(),
                        compInput.odm.getData().get(0),
                        true);
                entityValueIntializationListener(
                        entity,
                        compInput.attr.getFieldExpression(),
                        compInput.odm.getData().get(0));
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);

        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    /**
     * Override it for additional initializations. Called in
     * {@link #initEntityWithComposedInput(BaseEntity)}.
     *
     * @param entity
     * @param fieldExpression
     * @param value
     */
    protected OFunctionResult entityValueIntializationListener(
            BaseEntity entity, String fieldExpression, Object value) {
        // Nothinig to be done, created for overloading
        return new OFunctionResult();
    }

    /**
     * Returns the valid "Supported" & "Natively Supported" screen outputs to be
     * validated and used in the screen <br> <br>{@link OBackBean#currentScreen}
     * should be set first <br>Override it to add your screen-specific outputs
     * and/or alter original screen outputs
     *
     * @return List: Filled with {@link #currentScreen} outputs, empty if there
     * are no outputs <br>null: error
     */
    protected List<ScreenOutputDTO> getValidScreenOutputs() {
        logger.trace("Entering");
        try {
            List<ScreenOutputDTO> screenOutputs = new ArrayList<ScreenOutputDTO>();
            // Add inputs to a new list to avoid changing the original one
            //screenOutputs.addAll(currentScreen.getScreenOutputs());
            screenOutputs.addAll(uiFrameWorkFacade.getScreenOuputDTOs(currentScreen.getDbid(), loggedUser));
            return screenOutputs;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Returning with Null");
            return null;
        }
    }

    /**
     * Constructs {@link ODataMessage} from both oDT & entity. Supported cases
     * are as following: <br>. oDT doesn't match any of
     * {@link #getValidScreenOutputs()}, and entity type (entity is not null) is
     * same like oDT (Native Construction), oDM is returned with entity & oDT
     * (not considered error), and {@link #loggedUser} is second element in
     * {@link ODataMessage#data}. <br>. oDT doesn't match any of
     * {@link #getValidScreenOutputs()}, and entity type is NOT same like oDT
     * (or entity is null), Case Not Supported, then null
     * {@link OFunctionResult#returnedDataMessage} is returned. <br>. Matched
     * output is found, with no attributes in it, then: If oDT type is
     * {@link ODataType#DTDBID_DBID}, oDM is returned with entity DBID & oDT;
     * else, oDM is returned with entity & oDT. Matched output is set in
     * {@link OFunctionResult#returnValues} element(0). <br>. Matched output is
     * found, with attributes in it. If
     * {@link ScreenDTMappingAttr#fieldExpression} is same like oDT name, then
     * an oDM is returned with entity & oDT; else, the related entity field is
     * returned in oDM with its corresponding data type. Matched output is set
     * in {@link OFunctionResult#returnValues} element(0). <br>. Null oDM with
     * no errors indicates it's "Case Not Supported". Subclasses should cover
     * those cases if supported. Matched output is set in
     * {@link OFunctionResult#returnValues} element(0).
     *
     * Override it (and recommended to call it) to add custom construction.
     *
     * @param oDT
     * @param entity
     * @return oFR as described above in case of success; otherwise, oFR with
     * errors in case of error
     */
    public OFunctionResult constructOutput(ODataType oDT, BaseEntity entity) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            List<ScreenOutputDTO> validOutputs = getValidScreenOutputs();
            // Don't check the following to generate error. As, sometimes,
            // the function is called for natively supported outputs

            ODataMessage odm = new ODataMessage();
            List odmData = new ArrayList();
            ScreenOutputDTO matchedOutput = null;
            // <editor-fold defaultstate="collapsed" desc="Get & check the matched output; Check Native Construction">
            for (ScreenOutputDTO validOutput : validOutputs) {
                if (validOutput.getOdataTypeDBID() == oDT.getDbid()) {
                    matchedOutput = validOutput;
                    break;
                }
            }
            if (matchedOutput == null) {
                if (oDT.getDbid() == 1) {
                    odm.setData(odmData);
                    odm.setODataType(oDT);
                    oFR.setReturnedDataMessage(odm);
                }
                // No matched output found in getValidScreenOutputs
                if (entity == null) // Entity type is NOT same like oDT
                {
                    logger.debug("Returning");
                    return oFR;    // null returnedDataMessage
                }
                if (!entity.getClassName().equals(oDT.getName())) // Entity type is NOT same like oDT
                {
                    logger.debug("Returning");
                    return oFR;    // null returnedDataMessage
                }
                // Entity type IS same like oDT, Native Construction
                // Build odm of both entity & oDT
                odmData.add(entity);
                odmData.add(loggedUser);
                odm.setData(odmData);
                odm.setODataType(oDT);
                oFR.setReturnedDataMessage(odm);
                logger.debug("Returning");
                return oFR;
            }
            // </editor-fold>
            // matchedOutput is found
            oFR.setReturnValues(Collections.singletonList(matchedOutput));

            // Get output attribute mappings
            List<ScreenDTMappingAttrDTO> outputAttrMaps = matchedOutput.getAttributesMapping();

            // <editor-fold defaultstate="collapsed" desc="Native Construction for no attributes found, supporting DTDBID_DBID">
            if (outputAttrMaps == null || outputAttrMaps.isEmpty()) {
                // No Attribute Mapping found in matche output
                // Build odm of both entity & oDT
                if (entity == null && !entity.getClassName().equals(oDT.getName())) {
                    logger.warn("Strange Case, Entity NOT Same Like ODT But Matched Output Found");
                    // </editor-fold>
                }
                if (oDT.getDbid() == ODataType.DTDBID_DBID) {
                    odmData.add(entity.getDbid());
                } else {
                    odmData.add(entity);
                    odmData.add(loggedUser);
                }
                odm.setData(odmData);
                odm.setODataType(oDT);
                oFR.setReturnedDataMessage(odm);
                logger.debug("Returning");
                return oFR;
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Attributes found, return attribute related field">
            for (ScreenDTMappingAttrDTO attr : outputAttrMaps) {
                String attFieldExp = attr.getFieldExpression();
                if (attFieldExp == null || attFieldExp.equals("")) {
                    logger.warn("Output Mapping MUST Have Valid Field Expression");
                    continue;  // Ignore & check the next. No reason for that.
                    // should we generate error?
                }
                if (attFieldExp.equals(oDT.getName())) {
                    // Backword compatibility, where field expression is of the same oDT
                    if (entity != null && !oDT.getName().equals(entity.getClassName())) {
                        logger.warn("Output Mapping Unknown Case, Field Expression Same Like ODT But Not Of Entity Class");
                    } else {
                        odmData.add(entity);
                        if (oDT.getODataTypeAttribute().size() > 1) {
                            odmData.add(loggedUser);
                            if (oDT.getODataTypeAttribute().size() > 2) {
                                logger.warn("2 Data Type Attributes, Case Not Supported");
                                // </editor-fold>
                            }
                        }
                        odm.setData(odmData);
                        odm.setODataType(oDT);
                        break;
                    }
                }
                // field expression is NOT of the same oDT

                // Get the field value
                Object fieldValue = getValueFromEntity(entity, attFieldExp);

                // Set ODataType
                // Get the field class to get the odatatype
                Class fieldClass = null;
                if (fieldValue != null) {
                    fieldClass = fieldValue.getClass();
                } else {
                    fieldClass = BaseEntity.getFieldObjectType(
                            BaseEntity.getClassField(entity.getClass(), attFieldExp));
                }
                odm.setODataType(dataTypeService.loadODataType(
                        fieldClass.getSimpleName(), oem.getSystemUser(loggedUser)));

                // Set the data
                odmData.add(fieldValue);
                if (oDT.getODataTypeAttribute().size() > 1) //FIXME: hardcoded for user, assuming it's the second object
                {
                    odmData.add(loggedUser);
                }
                odm.setData(odmData);
                oFR.setReturnedDataMessage(odm);
                logger.debug("Returning");
                return oFR;
                // Only one attribute is supported. When supporting it,
                // decide which ODT will be sent in the odm
            }
            // </editor-fold>

            // No supported, return null oDM
            logger.debug("Returning");
            return oFR;

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
            logger.debug("Returning");
            return oFR;
        }
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="htmlPanelGrid & buildInnerScreen">
    /**
     * Sets {@link OBackBean#htmlPanelGrid} and return it. Called by {@link #getHtmlPanelGrid()
     * }
     */
    private boolean printable = false;

    public boolean isPrintable() {
        return printable;
    }

    public void setPrintable(boolean printable) {
        this.printable = printable;
    }

    abstract protected HtmlPanelGrid buildInnerScreen();
    /**
     * Called by renderer to build the screen, assuming the JSP has line similar
     * to: ice:panelGrid id="middlePanel" binding="#{FormBean.htmlPanelGrid}"
     */
    protected HtmlPanelGrid htmlPanelGrid;

    public void setHtmlPanelGrid(HtmlPanelGrid htmlPanelGrid) {
        this.htmlPanelGrid = htmlPanelGrid;
    }

    /**
     * Calls {@link OBackBean#buildInnerScreen() } and returns
     * {@link OBackBean#htmlPanelGrid}, adds error user message in case of
     * error. In the life cycle, it's called after {@link #init()}
     */
    public HtmlPanelGrid getHtmlPanelGrid() {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>
        try {
            oem.getEM(loggedUser);
            htmlPanelGrid = buildInnerScreen();
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(
                    usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
        } finally {
            oem.closeEM(loggedUser);
        }
        logger.debug("Returning");
        return htmlPanelGrid;
    }
    // </editor-fold>

    /**
     * Refresh action based on xhtml tag like: <font size="3" face="courier"
     * color="blue"> ice:commandLink partialSubmit="true" id="c2aLink"
     * immediate="true" action="#{TabularBean.refresh}" </font>. <br>It does the
     * following: <br>1. Update {@link #dataMessage} with screen parameter
     * {@link OPortalUtil#MESSAGE} <br>2. Calls {@link #validateInputSetMapping() } & {@link #processInput()
     * }
     *
     * @return
     */
    public String refresh() {
        try {
            logger.debug("Entering");
            oem.getEM(loggedUser);
            messagesPanel.clearMessages();
            dataMessage = (ODataMessage) getScreenParam(OPortalUtil.MESSAGE);
            if (dataMessage == null) {
                logger.warn("data Message: {} is Null", OPortalUtil.MESSAGE);
                messagesPanel.addErrorMessage(
                        usrMsgService.getUserMessage("InvalidInput", oem.getSystemUser(loggedUser)));
                logger.debug("Returning with empty string");
                return "";
            }
            if (compositeInputRequired) // If "sender" not parent input dataMessage, we need to reset
            // currentScreenInput to reflect it, as init() is not called
            // in this case
            {
                initCurrentScreenInput();
            }

            validateInputSetMapping();
            processInput();

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(
                    usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning with Null");
            return null;
        }
    }
    // <editor-fold defaultstate="collapsed" desc="expandMessagePanel">
    /**
     * Is true when we need to expand the {@link #messagePanel}. Is set in
     * {@link UserMessagePanel#addMessages(List)}
     */
    private boolean expandMessagePanel = false;

    public boolean isExpandMessagePanel() {
        return expandMessagePanel;
    }

    public void setExpandMessagePanel(boolean expandMessagePanel) {
        this.expandMessagePanel = expandMessagePanel;
    }

    // </editor-fold>
    /**
     * Return the {@link ODataMessage} in the
     * {@link OFunctionResult#returnValues} first element
     *
     * @param sourceEntity
     * @param odt
     * @param screenIOMapping
     * @return
     */
    public OFunctionResult constructODMFromDTMapping(
            BaseEntity sourceEntity, ODataType odt, ScreenDTMapping screenIOMapping) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            ODataMessage odm = new ODataMessage();
            List odmData = new ArrayList();
            List<ScreenDTMappingAttr> attributeMappings = screenIOMapping.getAttributesMapping();

            if (attributeMappings == null || attributeMappings.isEmpty()) // No attributes in the screen data type mapping
            // For backward compatibility & for simplicity
            {
                // Assuming it's equivalent to sending the entity itself
                logger.warn("Output Mapping Has No Attribute, Assumed To Construct Sent Entity ODM, If So, Then Better To Remove It");
                odm.setODataType(odt);
                odmData.add(sourceEntity);
                odm.setData(odmData);
                oFR.setReturnValues(Collections.singletonList(odm));
                logger.debug("Returning");
                return oFR;
            }

            for (ScreenDTMappingAttr attr : attributeMappings) {
                // Check Field Expression
                String attFieldExp = attr.getFieldExpression();
                if (attFieldExp == null || attFieldExp.equals("")) {
                    logger.warn("Output Mapping MUST Have Valid Field Expression");
                    continue;  // Ignore & check the next. No reason for that.
                }
                if (attFieldExp.equals(odt.getName())) {
                    // Backword compatibility, where field expression is of the same object
                    if (!odt.getName().equals(sourceEntity.getClassName())) {
                        logger.warn("Output Mapping Unknown Case, Field Expression Same Like ODT But Not Of Entity Class");
                    } else {
                        odmData.add(sourceEntity);
                        if (odt.getODataTypeAttribute().size() > 1) {
                            odmData.add(loggedUser);
                            if (odt.getODataTypeAttribute().size() > 2) {
                                logger.warn("2 Data Type Attributes, Case Not Supported,"
                                        + " Attribtue Mapping: {}", screenIOMapping);
                            }
                        }
                        odm.setData(odmData);
                        odm.setODataType(odt);
                        break;
                    }
                }
                // Get the field value
                Object fieldValue = getValueFromEntity(sourceEntity, attFieldExp);

                // Set ODataType
                // Get the field class to get the odatatype
                Class fieldClass = null;
                if (fieldValue != null) {
                    fieldClass = fieldValue.getClass();
                } else {
                    fieldClass = BaseEntity.getFieldObjectType(
                            BaseEntity.getClassField(sourceEntity.getClass(), attFieldExp));
                }
                odm.setODataType(dataTypeService.loadODataType(
                        fieldClass.getSimpleName(), oem.getSystemUser(loggedUser)));

                // Set the data
                odmData.add(fieldValue);
                if (odt.getODataTypeAttribute().size() > 1) //FIXME: hardcoded for user, assuming it's the second object
                {
                    odmData.add(loggedUser);
                }
                odm.setData(odmData);
                break; // Only one attribute is supported. When supporting it,
                // decide which ODT will be sent in the odm
            }
            if (attributeMappings.size() > 1) {
                logger.warn("Screen DataType Mapping Doesn't Support More Than One Attribute");
            }
            oFR.setReturnValues(Collections.singletonList(odm));

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    /**
     * @deprecated use {@link #constructOutput(ODataType, BaseEntity) }
     */
    public OFunctionResult constructODataMessage(ODataType oDataType, BaseEntity baseEntity) {
        return constructOutput(oDataType, baseEntity);
    }

//     /**
//     * Parse Screen name expression to screen name:
//     * It does the following:
//     * 1. Split the screenName expression with hash sign {#}
//     * 2. Return the first element in the result which has the screen name
//     */
    /**
     * @return the screenInstId
     */
    public String getScreenInstId() {
        return screenInstId;
    }

    /**
     * @param screenInstId the screenInstId to set
     */
    public void setScreenInstId(String screenInstId) {
        this.screenInstId = screenInstId;
    }
    /**
     * Counts of opened screens
     */
    protected static int SCREENS_COUNTER = 0;

    /**
     * /**
     * Construct Screen name expression: It does the following: 1. Generate
     * Screen name expression as the following format: //
     * screenName_SCREENS_COUNTER 2. Increment the
     * {@link OBackBean SCREENS_COUNTER} variable 3. Return the generated screen
     * name expression
     */
    public static synchronized String generateScreenInstId(String scrName) {
        String screenInstId = scrName + "_" + UUID.randomUUID().toString();
        SCREENS_COUNTER++;
        return screenInstId;
    }

    public OScreen getCurrentScreenCopy() {
        logger.debug("Entering");
        if (currentScreen.isPrsnlzd()) {
            List<String> conditions = new ArrayList<String>();
            //reload copy
            conditions.add("prsnlzdOriginal_DBID = " + currentScreen.getDbid());
            conditions.add("prsnlzdUser_DBID = " + loggedUser.getDbid());
            OScreen copyScrn = null;
            try {
                if (currentScreen instanceof TabularScreen) {
                    copyScrn = (OScreen) oem.loadEntity(TabularScreen.class.getSimpleName(), conditions, null, loggedUser);
                } else if (currentScreen instanceof FormScreen) {
                    copyScrn = (OScreen) oem.loadEntity(FormScreen.class.getSimpleName(), conditions, null, loggedUser);
                }
                logger.debug("Returning");
                return copyScrn;
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
                logger.debug("Returning with Null");
                return null;
            }
        } else {
            logger.debug("Returning with Null");
            return null;
        }

    }

    /**
     * @return the dynamicSelectedNode
     */
    public DefaultTreeNode getDynamicSelectedNode() {
        return dynamicSelectedNode;
    }

    /**
     * @param dynamicSelectedNode the dynamicSelectedNode to set
     */
    public void setDynamicSelectedNode(DefaultTreeNode dynamicSelectedNode) {
        this.dynamicSelectedNode = dynamicSelectedNode;
    }

    /**
     * @return the fieldsTreeBrowser
     */
    public TreeBrowser getFieldsTreeBrowser() {
        return fieldsTreeBrowser;
    }

    /**
     * @param fieldsTreeBrowser the fieldsTreeBrowser to set
     */
    public void setFieldsTreeBrowser(TreeBrowser fieldsTreeBrowser) {
        this.fieldsTreeBrowser = fieldsTreeBrowser;
    }

    public void updateOpenedScreenOfSameActOnEntity() {
    }

    //FIXME: handle all the types of functions, ProcessFunction. etc...
    //FIXME: exclude ScreenFunction as it is handled client side
    public void balloonAction(ActionEvent ae) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            logger.debug("Entering");

            oem.getEM(systemUser);
            messagesPanel.clearMessages();
            String actionID = ae.getComponent().getId();
            String balloonID = actionID.substring(actionID.indexOf("_"));
            // _____________________________________
            // Get the Balloon of the clicked action
            //
            String functionID = null;

            if (balloonID.contains("actionLink_Window")) {
                String[] balloonIDSplitted = balloonID.split("_");//replace("_actionLink_Window", "");
                balloonID = "_" + balloonIDSplitted[1];
                functionID = balloonIDSplitted[balloonIDSplitted.length - 1];
            } else {
                String[] balloonIDSplitted = balloonID.split("_");
                functionID = balloonID.substring(balloonID.lastIndexOf("_") + 1);
                balloonID = "_" + balloonIDSplitted[balloonIDSplitted.length - 2];

            }

            Balloon clickedBalloon = balloons.get(balloonID);

            if (clickedBalloon == null) {
                logger.warn("No Balloon Got For Action");
                messagesPanel.addErrorMessage(
                        usrMsgService.getUserMessage("SystemInternalError", systemUser));
                logger.debug("Returning");
                return;
            }
            // ____________________________________
            // Load & Validate the OFunction to run
            //
            OFunction ofunction;
            ofunction = functionServiceRemote.getOFunction(Long.parseLong(functionID), loggedUser);
            if (ofunction == null) {
                logger.warn("Error Loading Function ID");
                messagesPanel.addErrorMessage(
                        usrMsgService.getUserMessage("SystemInternalError", systemUser));
                logger.debug("Returning");
                return;
            }

            Long actionEntityDBID = clickedBalloon.getEntityDBID();
            //  will used in attachment before save
//            if (actionEntityDBID == 0) {
//                logger.warn("Entity DBID Is Empty");
//                messagesPanel.addErrorMessage(
//                        usrMsgService.getUserMessage("SystemInternalError", systemUser));
//                return;
//            }

            // _____________________________________
            // Get the entity name & load it
            //
            String actionEntityClassPath = clickedBalloon.getEntityClassPath();
            BaseEntity entity;// = null;
            String entityName = actionEntityClassPath.substring(
                    actionEntityClassPath.lastIndexOf(".") + 1);
            // add row balloon
            if (actionEntityDBID == 0) {
                entity = (BaseEntity) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("entityForAttachment" + getCurrentScreen().getDbid());
                entity.setPreSavedEntity(true);
            } else {
                //TODO: get it from the screen data already loaded, performance
                entity = (BaseEntity) oem.loadEntity(entityName, actionEntityDBID,
                        Collections.singletonList(OEntityManager.CONFCOND_GET_ACTIVEANDINACTIVE), null,
                        systemUser);
            }

            List conditions = new ArrayList();
            conditions.add("dbid = '" + actionID.split("_")[1] + "'");
            conditions.add("##ACTIVEANDINACTIVE");
            OEntityAction action = (OEntityAction) oem.loadEntity(OEntityAction.class.getSimpleName(),
                    conditions, null, loggedUser);
            // Special handling for ScreenFunction Link
            if (ofunction instanceof ScreenFunction) {
                ScreenFunction sfunction = (ScreenFunction) ofunction;
                if (!ae.getComponent().getId().contains(Balloon.WINDOWLINKPOSTFIX)) {
                    sfunction.setRunInPopup(true);
                }
            } else if (ofunction instanceof OrgChartFunction) {
                OrgChartFunction chartFunction = (OrgChartFunction) ofunction;

                OPortalPage orgPage = new OPortalPage();
                orgPage.setName("OrgChart");
                orgPage.setHeader("OrgChart");
                orgPage.setOmodule(moduleService.loadOModule("System", systemUser));
                orgPage.setPortlets(new ArrayList<PortalPagePortlet>());
                OPortalUtil.addOPortletPageForOrg(orgPage, chartFunction, entity, oem, dataTypeService,
                        loggedUser, null, action.getNameTranslated());
                return;
            } else if (ofunction instanceof JavaFunction) {
                // <editor-fold defaultstate="collapsed" desc="run JavaFunction">
                ODataType entityDT = dataTypeService.loadODataType(entity.getClass().getSimpleName(), systemUser);
                if (entityDT == null) {
                    logger.warn("Can't Load Entity Related DataType");
                    oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser));
                    logger.debug("Returning");
                    return;
                }
                ODataMessage odm = new ODataMessage();
                List odmData = new ArrayList();
                /*
                 * MailMerge Generate
                 */
                if (ofunction.getName().equals("generateTemplate")) {
                    odmData.add(dataMessage.getData().get(1));
                }
                // Entity type IS same like oDT, Native Construction
                // Build odm of both entity & oDT
                odmData.add(entity);
                odmData.add(loggedUser);
                odm.setData(odmData);
                odm.setODataType(entityDT);
                oFR.setReturnedDataMessage(odm);
                OFunctionParms functionParms = new OFunctionParms();
                functionParms.getParams().put("ScreenName", getCurrentScreen().getName());
                functionParms.getParams().put("ScreenDT", getDataMessage());
                //Rehab: excute all post actions with action java function
//                oFR = functionServiceRemote.runFunction(ofunction, odm, functionParms, loggedUser);
                oFR.append(entitySetupService.executeAction(action, odm, functionParms, loggedUser));
                if (oFR.getReturnValues() != null && !oFR.getReturnValues().isEmpty()
                        && oFR.getReturnValues().get(0) instanceof InputStream) {
                    exportedFile = downloadFiles(oFR);//text/html;charset=UTF-8//"image/jpg", "downloaded_optimus.jpg");
                    RequestContext.getCurrentInstance().execute("simulateDownloadBtnClick();");
                }
                //Rehab : to update data by balloon action
//                RequestContext.getCurrentInstance().execute("location.reload();");
//                recordID = String.valueOf(((TabularBean)this).getObjectRecordID(entity.getDbid()));
                RequestContext.getCurrentInstance().update("tf:generalPanel");
                messagesPanel.clearAndDisplayMessages(oFR);
                RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
//                RequestContext.getCurrentInstance().update(":tf:tf:"+getCurrentScreen().getName()
//                        +"_generalTable:"+ recordID +":balloonOverLay" + clickedBalloon.getId());
                // </editor-fold>
                logger.debug("Returning");
                return;

            } else if (ofunction instanceof URLFunction) {
                ODataMessage odm = new ODataMessage();
                String mainUrl = ((URLFunction) ofunction).getUrl() + "?entityDBID=" + entity.getDbid();
                ((URLFunction) ofunction).setUrl(mainUrl);
                oFR.append(functionServiceRemote.runFunction(ofunction, odm, new OFunctionParms(), loggedUser));
                logger.debug("Returning");
                return;
            }

            // _____________________________________
            // Run the function
            //
            portalMode = true;
            if (preRunBalloonFunction(ae, ofunction, entity)) {
                oFR.append(runFunctionForEntity(ofunction, entity, ""));
                postRunBalloonFunction(ae, ofunction, entity, oFR);
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            if (oFR.getErrors().size() > 0 || oFR.getWarnings().size() > 0) {
                messagesPanel.clearAndDisplayMessages(oFR);
            }
            oem.closeEM(systemUser);
            logger.debug("Returning");
            return;
        }
    }

    /**
     * @return the screenHeight
     */
    public String getScreenHeight() {
        return screenHeight;
    }

    /**
     * @param screenHeight the screenHeight to set
     */
    public void setScreenHeight(String screenHeight) {
        this.screenHeight = screenHeight;
    }

    public void routingTaskButtonAction(ActionEvent even) {
    }

    /**
     * @return the portletInsId
     */
    public String getPortletInsId() {
        return portletInsId;
    }

    /**
     * @param portletInsId the portletInsId to set
     */
    public void setPortletInsId(String portletInsId) {
        this.portletInsId = portletInsId;
    }

    /**
     * @return the addRowBtn
     */
    public CommandButton getAddRowBtn() {
        return addRowBtn;
    }

    /**
     * @param addRowBtn the addRowBtn to set
     */
    public void setAddRowBtn(CommandButton addRowBtn) {
        this.addRowBtn = addRowBtn;
    }

    /**
     * @return the exportedFile
     */
    public StreamedContent getExportedFile() {
        return exportedFile;
    }

    /**
     * @param exportedFile the exportedFile to set
     */
    public void setExportedFile(StreamedContent exportedFile) {
        this.exportedFile = exportedFile;
    }

    public String dummyRedirection() {
        logger.debug("Entering");
        logger.trace("Screen Instance: {}", this.getScreenInstId());
        logger.trace("Retuening with: /ViewExpired.iface");
        return "/ViewExpired.iface";
    }

    @PreDestroy
    public void beforeBeanDestry() {
        logger.debug("Entering");
        logger.trace("Bean is destroyed!");
        logger.debug("Returning");
    }

    public abstract void addClaimButtonToPanelGroup(String buttonID, String buttonLabel,
            String actionMethodName, HtmlPanelGrid panelLayout, Class[] params, String portletInsId, String beanName, String screenLanguage, boolean invalidInputMode, OScreen currentScreen);

    public String getScreenLanguage() {
        return screenLanguage;
    }

    public boolean isInvalidInputMode() {
        return invalidInputMode;
    }

    public StreamedContent downloadFiles(OFunctionResult oFR) {
        String fileName = "exported_file.sql";
        String fileType = "text/html";
        Object customFileName = oFR.getReturnValues().size() < 2 ? null : oFR.getReturnValues().get(1);
        Object customFileType = oFR.getReturnValues().size() < 3 ? null : oFR.getReturnValues().get(2);
        if (customFileName != null && customFileName instanceof String && !customFileName.toString().equals("")) {
            fileName = customFileName.toString();
        }
        if (customFileType != null && customFileType instanceof String && !customFileType.toString().equals("")) {
            fileType = customFileType.toString();
        }
        return new DefaultStreamedContent((InputStream) oFR.getReturnValues().get(0), fileType, fileName);
    }
                    }
