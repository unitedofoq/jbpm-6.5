/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.uiframework.screen.MultiLevelScreenLevel;
import javax.faces.component.html.HtmlPanelGrid;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;



/**
 *
 * @author nkhalil
 */
public class LevelNode extends DefaultTreeNode{
    private String nodeImage;
    private BaseEntity nodeBaseEntity;
    private LevelsTree levelsTree;
    private MultiLevelScreenLevel screenLevel;
    private String parentField;
    private int levelOrder;
    private long id = 0;   // BaseEntity.instID, and is 0 for root node
    private HtmlPanelGrid balloonGrid;
    private byte[] image;
    private boolean edit=false;
    private String editableValue;
    private String newValue;
    private boolean newVal = false;
    private String highligh="";
    private boolean loadedBefore;

    public boolean isLoadedBefore() {
        return loadedBefore;
    }

    public void setLoadedBefore(boolean loadedBefore) {
        this.loadedBefore = loadedBefore;
    }
    

    public LevelNode(Object data, TreeNode parent) {
        super(data, parent);
    }

    public LevelNode(String type, Object data, TreeNode parent) {
        super(type, data, parent);
    }
    
    
    
    public String getHighligh() {
        return highligh;
    }

    public void setHighligh(String highligh) {
        this.highligh = highligh;
    }
    

    public boolean isNewVal() {
        return newVal;
    }

    public void setNewVal(boolean newVal) {
        this.newVal = newVal;
    }

    
    public String getNewValue() {       
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }
    

    public String getEditableValue() {
        return editableValue;
    }

    public void setEditableValue(String editableValue) {
        this.editableValue = editableValue;
    }
    

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }
    

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
    

    public String getNodeImage() {
        return nodeImage;
    }

    public void setNodeImage(String nodeImage) {
        this.nodeImage = nodeImage;
    }

    public HtmlPanelGrid getBalloonGrid() {
        return balloonGrid;
    }

    public void setBalloonGrid(HtmlPanelGrid balloonGrid) {
        this.balloonGrid = balloonGrid;
    }
    

    public MultiLevelScreenLevel getScreenLevel() {
        return screenLevel;
    }

    public void setScreenLevel(MultiLevelScreenLevel screenLevel) {
        this.screenLevel = screenLevel;
    }

    

    public long getId() {
        return (nodeBaseEntity == null)? 0 : nodeBaseEntity.getInstID();
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getLevelOrder() {
        return levelOrder;
    }

    public void setLevelOrder(int levelOrder) {
        this.levelOrder = levelOrder;
    }
    

    public String getParentField() {
        return parentField;
    }

    public void setParentField(String parentField) {
        this.parentField = parentField;
    }
   
    


    public BaseEntity getNodeBaseEntity() {
        return nodeBaseEntity;
    }

    public void setNodeBaseEntity(BaseEntity nodeBaseEntity) {
        this.nodeBaseEntity = nodeBaseEntity;
        if (nodeBaseEntity != null)
            if(nodeBaseEntity.getInstID() != null)
                id = nodeBaseEntity.getInstID();
        else
            //FIXME: log error
            ;
    }

    /**
     * Registers a user click with this object and updates the selected node in the ObjectAttributeBrowserBean.
     *
     * @param event that fired this method
     */
    public void nodeClicked() {
    }
}
