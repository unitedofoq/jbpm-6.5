/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.mailmerge.MailMergeServiceLocal;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
//import com.unitedofoq.fabs.core.uiframework.report.birt.BirtEngine;
import com.unitedofoq.fabs.core.report.customReports.BirtEngine;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.eclipse.birt.core.framework.IPlatformContext;
import org.eclipse.birt.core.framework.PlatformServletContext;
import org.eclipse.birt.report.engine.api.EngineException;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.engine.api.PDFRenderOption;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MEA
 */
@ManagedBean(name = "MailMergePreviewBean")
@ViewScoped
public class MailMergePreviewBean extends FormBean {
    //<editor-fold defaultstate="collapsed" desc="Variable Section">
final static org.slf4j.Logger logger = LoggerFactory.getLogger(MailMergePreviewBean.class);
    
    private MailMergeServiceLocal mailMergeService = OEJB.lookup(MailMergeServiceLocal.class);
    private String fileType;
    private String templateContent;
    private String templateName;
    private StreamedContent exportedFiles;

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public StreamedContent getExportedFiles() {
        return exportedFiles;
    }

    public void setExportedFiles(StreamedContent exportedFiles) {
        this.exportedFiles = exportedFiles;
    }

    @Override
    protected String getBeanName() {
        return "MailMergePreviewBean";
    }

    public String getTemplateContent() {
        return templateContent;
    }

    public void setTemplateContent(String templateContent) {
        this.templateContent = templateContent;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }
    //</editor-fold>

    public void saveFile() {
        try {
            ServletContext servletContext =
                    (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();

            File reportDir = new File(servletContext.getRealPath("/"));//--/report
            reportDir.mkdir();

            String name = getTemplateName();
            String content = getTemplateContent();

            if ("word".equalsIgnoreCase(fileType)) {
                createWord(content, name);
            } else if ("HTML".equalsIgnoreCase(fileType)) {
                createHTML(content, name);
            } else if ("PDF".equalsIgnoreCase(fileType)) {
                createPDF(content, name, loggedUser);
            } else {
                createWord(content, name);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="HTML File">

    private void createHTML(String templateContent, String templateName) {
        String path = "";
        String htmlPath = "";
        try {

            ServletContext servletContext =
                    (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();

            //<editor-fold defaultstate="collapsed" desc="Generate HTML File">
            String appendedString1="<html><head>"
                    + "<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">"
                    + "<head/><body>";
            String appendedString2="<body/><html/>";
            htmlPath = servletContext.getRealPath(
                    "/report/" + templateName + ".html");
            File htmlFile = new File(htmlPath);
            Writer out = new OutputStreamWriter(new FileOutputStream(htmlPath), "UTF-8");
            FileWriter fw = new FileWriter(htmlFile);
            BufferedWriter bw = new BufferedWriter(out);
            //adding UTF-8 formate
            StringBuilder totalString =new StringBuilder(appendedString1);
            totalString.append(templateContent);
            totalString.append(appendedString2);
            
            bw.write(totalString.toString());
            bw.close();
            InputStream exportedStream = new FileInputStream(htmlPath);
            exportedFiles = new DefaultStreamedContent(exportedStream, "application/html", templateName + ".html");
            RequestContext.getCurrentInstance().execute("simulateDownloadBtnClick()");
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        } finally {
            File toBeDeletedFiel = new File(htmlPath);
            if (toBeDeletedFiel.exists()) {
                toBeDeletedFiel.delete();
            }
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Word File">

    private void createWord(String templateContent, String templateName) {
logger.trace("Entering");
        String path = "";
        String htmlPath="";
        try {
            ServletContext servletContext =(ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            //edited code to generate word
            htmlPath = servletContext.getRealPath("/");
            htmlPath= htmlPath + templateName + ".doc";
            File htmlFile = new File(htmlPath);
            String appendedString1="<html><head>"
                    + "<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">"
                    + "<head/><body>";
            String appendedString2="<body/><html/>";
            Writer out = new OutputStreamWriter(new FileOutputStream(htmlPath), "UTF-8");
            FileWriter fw = new FileWriter(htmlFile);
            BufferedWriter bw = new BufferedWriter(out);
            StringBuilder totalString =new StringBuilder(appendedString1);
            totalString.append(templateContent);
            totalString.append(appendedString2);
            String clear=new String(totalString);
            clear = clear.replace( "<span dir=\"RTL\">","");
            clear = clear.replace( "</span>","");
            bw.write(clear);
            bw.close();
            InputStream exportedStream = new FileInputStream(htmlPath);
            exportedFiles = new DefaultStreamedContent(exportedStream, "application/html", templateName + ".doc");
            RequestContext.getCurrentInstance().execute("simulateDownloadBtnClick()");
           
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        } finally {
            File toBeDeletedFiel = new File(path);
            if (toBeDeletedFiel.exists()) {
                toBeDeletedFiel.delete();
                logger.trace("Returning");
            }
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="PDF File">
    private void createPDF(String templateContent, String templateName, OUser loggedUser) {
logger.trace("Entering");
        String html = templateContent;
        html = html.replaceAll("&nbsp;", " ");

        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();

        String inputFileName = servletContext.getRealPath("/mailMergePDF.rptdesign");
        if (loggedUser.getFirstLanguage().getDbid() == 24) {
            inputFileName = servletContext.getRealPath("/mailMergeArabicPDF.rptdesign");
        }

        String outFileName = servletContext.getRealPath("/");
        long date = new Date().getTime();
        outFileName = outFileName  + date + ".pdf";
        IReportRunnable design = null;
        IReportEngine birtReportEngine;
        IPlatformContext context = new PlatformServletContext(servletContext);
        birtReportEngine = BirtEngine.getBirtEngine(context);

        try {
            design = birtReportEngine.openReportDesign(inputFileName);
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        //create task to run and render report
        IRunAndRenderTask task = birtReportEngine.createRunAndRenderTask(design);

        task.setParameterValue("templateContent", html);

        //<editor-fold defaultstate="collapsed" desc="PDF Generator">
        PDFRenderOption renderOption = new PDFRenderOption();
        renderOption.setOutputFileName(outFileName);
        renderOption.setOutputFormat(PDFRenderOption.OUTPUT_FORMAT_PDF);
        task.setRenderOption(renderOption);
        try {
            task.run();
        } catch (EngineException ex) {
            logger.error("Exception thrown",ex);
        }
        task.close();
        //</editor-fold>
        try {

            InputStream exportedStream = new FileInputStream(outFileName);
            exportedFiles = new DefaultStreamedContent(exportedStream, "application/pdf", templateName + ".pdf");
            RequestContext.getCurrentInstance().execute("simulateDownloadBtnClick()");

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        } finally {
            File toBeDeletedFiel = new File(outFileName);
            if (toBeDeletedFiel.exists()) {
                toBeDeletedFiel.delete();
            }
            
                logger.trace("Returning");
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="onInit">
    @Override
    protected boolean onInit(Map<String, String> requestParams) {
        logger.trace("Entering");
        super.onInit(requestParams);
        OFunctionResult ofr = mailMergeService.generateTemplate(dataMessage, null, loggedUser);
        List<String> returnedDate = new ArrayList<String>();

        returnedDate = ofr.getReturnValues();
        if (!returnedDate.isEmpty()) {
            String contentFromService = returnedDate.get(0);
            setTemplateContent(contentFromService);
        }
        logger.trace("Returning with True");
        return true;
    }
    //</editor-fold>
}
