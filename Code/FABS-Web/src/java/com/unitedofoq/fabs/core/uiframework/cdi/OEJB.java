package com.unitedofoq.fabs.core.uiframework.cdi;

import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * @author Hamada
 * 
 * Utility class to load EJBs by JNDI name in JSF Managed Beans. 
 * CDI doesn't work in JSF Beans when deployed to Wildfly.
 */
public final class OEJB {

	private OEJB() {}

	/**
	 * Lookup EJB class by JNDI context. E.g::
	 * OEntityManagerRemote oem = OEJB.lookup(OEntityManagerRemote.class);
	 */
	@SuppressWarnings("unchecked")
	public static <T> T lookup(Class<T> ejbInterfaceClass) {
		String ejbJNDIName = "java:global/ofoq/" + ejbInterfaceClass.getName();
		try {
			return (T) new InitialContext().lookup(ejbJNDIName);
		} catch (NamingException exception) {
			throw new IllegalArgumentException(String.format("Cannot Lookup EJB class %s with JNDI %s", ejbInterfaceClass, ejbJNDIName),
					exception);
		}
	}

}