/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.websocket;

import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author mmasoud
 */
@ApplicationScoped
@ServerEndpoint("/notificationWebSocket")
public class NotificationWebSocket {

    @Inject
    private NotificationHandler notificationHandler;

    @OnOpen
    public void open(Session session) {
        //notificationHandler.addSession(session);
    }

    @OnClose
    public void close(Session session) {
        notificationHandler.removeSession(session);
    }

    @OnError
    public void onError(Throwable error) {
        Logger.getLogger(NotificationWebSocket.class.getName()).log(Level.SEVERE, null, error);
    }

    @OnMessage
    public void handleMessage(String message, Session session) {
        notificationHandler.addSession(new WebSocketSession(session, message));
    }
}
