package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;

import com.unitedofoq.fabs.core.security.user.OUser;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReportDS implements JRDataSource {

    final static Logger logger = LoggerFactory.getLogger(ReportDS.class);
    private OEntityManagerRemote oem;

    private List<BaseEntity> rslist;
    private List<BaseEntity> rsDetaillist;
    private List<BaseEntity> rsAssociatelist;
    private Iterator<BaseEntity> beIterator;
    private Iterator<BaseEntity> abeIterator;
    private String groupByEntityAttribute;
    private OUser loggedUser;

    private BaseEntity baseEntity = null;
    private BaseEntity associateBaseEntity = null;

    public ReportDS(List<BaseEntity> rslist,
            OEntityManagerRemote oem,
            OUser loggedUser) {
        this.rslist = rslist;
        this.beIterator = rslist.iterator();

        this.oem = oem;
        this.loggedUser = loggedUser;
    }

    public ReportDS(List<BaseEntity> rslist,
            List<BaseEntity> rsAssociateList,
            OEntityManagerRemote oem,
            OUser loggedUser) {
        this.rslist = rslist;
        this.beIterator = rslist.iterator();

        this.rsAssociatelist = rsAssociateList;
        this.abeIterator = rsAssociateList.iterator();

        this.oem = oem;
        this.loggedUser = loggedUser;
    }

    public ReportDS(List<BaseEntity> rslist,
            List<BaseEntity> rsDetaillist,
            List<BaseEntity> rsAssociateList,
            String groupByEntityAttribute,
            OEntityManagerRemote oem,
            OUser loggedUser) {
        this.rslist = rslist;
        this.beIterator = rslist.iterator();

        this.rsAssociatelist = rsAssociateList;
        this.abeIterator = rsAssociateList.iterator();

        this.rsDetaillist = rsDetaillist;
        this.groupByEntityAttribute = groupByEntityAttribute;

        this.oem = oem;
        this.loggedUser = loggedUser;
    }

    public List<BaseEntity> getDetailList() {
        return rslist;
    }

    public boolean next() throws JRException {

        boolean hasNext;

        //  if it is NOT a crosstab report...
        if (rsAssociatelist == null) {
            hasNext = beIterator.hasNext();

            //  if there is a 'next' in 'rslist'...
            if (hasNext) //  retrieve the 'next' in 'rslist'
            {
                baseEntity = beIterator.next();
            }
        } //  if it is a crosstab report...
        else {
            hasNext = abeIterator.hasNext();

            //  if there is a 'next' in 'rsAssociatelist'...
            if (hasNext) {
                //  retrieve the 'next' in 'rsAssociatelist'
                associateBaseEntity = abeIterator.next();

                //  First next() for 'beIterator'
                //  if the first 'next' in 'rslist' hasn't been retrieved yet...
                if (baseEntity == null) {
                    hasNext = beIterator.hasNext();

                    //  if there is a 'next' in 'rslist'...
                    if (hasNext) //  retrieve the 'next' in 'rslist'
                    {
                        baseEntity = beIterator.next();
                    }
                }
            } //  if there is NOT a 'next' in 'rsAssociatelist'...
            else {
                hasNext = beIterator.hasNext();

                //  if there is a 'next' in 'rslist'...
                if (hasNext) {
                    //  retrieve the 'next' in 'rslist'
                    baseEntity = beIterator.next();

                    //  reset the iterator for 'rsAssociatelist'
                    abeIterator = rsAssociatelist.iterator();

                    //  retrieve the 'next' in 'rsAssociatelist'
                    associateBaseEntity = abeIterator.next();
                }
            }
        }

        return hasNext;
    }

    public Object getFieldValue(JRField field) throws JRException {
        logger.debug("Entering");

        Object fieldValue = null;   //  value to be returned
        String fieldDescription = field.getDescription();
        if (fieldDescription == null) {
            fieldDescription = "";
        }

        try {

            // <editor-fold defaultstate="collapsed" desc="CROSSTAB">
            //  if the JRField belongs to a crosstab field...
            if (fieldDescription.startsWith("CROSSTAB")) {
                if (fieldDescription.length() > 8) {
                    fieldDescription = fieldDescription.substring(8);
                } else {
                    fieldDescription = "";
                }

                StringTokenizer fieldNameTokenizer = new StringTokenizer(field.getName(), "_");
                String relEntityAttribute = fieldNameTokenizer.nextToken();     //  periodicSalaryElements
                String crossEntityAttribute = fieldNameTokenizer.nextToken();   //  salaryElement
                String fldExpr = fieldNameTokenizer.nextToken();                //  basicAmount (relative to 'relEntityAttribute')
                String dbid = fieldNameTokenizer.nextToken();                   //  dbid (of 'crossEntityAttribute')
                String associateField = fieldNameTokenizer.nextToken();         //  payCalculatedPeriod  (relative to 'relEntityAttribute')

                List<BaseEntity> relEntities = getRelEntities(relEntityAttribute);

                BigDecimal acummulatedValue = new BigDecimal(0);
                if (relEntities != null) {
                    for (BaseEntity relEntity : relEntities) {
                        BaseEntity crossEntity = (BaseEntity) relEntity.invokeGetter(crossEntityAttribute);   //  salaryElement
                        if (dbid.equalsIgnoreCase(String.valueOf(crossEntity.getDbid()))) {
                            if (rsAssociatelist == null) {
                                acummulatedValue.add((BigDecimal) relEntity.invokeGetter(fldExpr));
                            } else if (relEntity.invokeGetter(associateField).equals(associateBaseEntity)) //  payCalculatedPeriod
                            {
                                acummulatedValue.add((BigDecimal) relEntity.invokeGetter(fldExpr));
                            }
                        }
                    }
                    fieldValue = acummulatedValue;
                }
            } // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="ASSOCIATE">
            else if (fieldDescription.startsWith("ASSOCIATE")) {
                if (fieldDescription.length() > 9) {
                    fieldDescription = fieldDescription.substring(9);
                } else {
                    fieldDescription = "";
                }

                fieldValue = associateBaseEntity.invokeGetter(fieldDescription);
                fieldDescription = "";
            } // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="LIST">
            //  if the JRField is a list...
            else if (fieldDescription.startsWith("LIST")) {
                if (fieldDescription.length() > 4) {
                    fieldDescription = fieldDescription.substring(4);
                } else {
                    fieldDescription = "";
                }

                /**
                 * ****************
                 */
                /*  Retrieve List  */
                /**
                 * ****************
                 */

                /* NOTES:
                 *
                 * 1]
                 * The code below assumes that lists will always contain
                 * entities (i.e. objects of type "BaseEntity").
                 *
                 * 2]
                 * The expression passed in the JRField's name will always
                 * point to the field inside the entities in the list
                 * (i.e. The passed list contains entities. The expression
                 * points to a certain field in those entities.
                 *
                 * 3]
                 * JRField expression = 'firstItem.item.item.  ....  .item'
                 *
                 * Other than 'firstItem', if one of the items in the JRField
                 * expression is a list, the code below will produce unexpected
                 * behavior and will probably CRASH!
                 *
                 * Currently, there is no support for having lists as items
                 * in the JRField expression except if a list is the first item
                 * in the list.
                 *
                 */
                //  get JRField expression
                String listExpr = field.getName();

                //  if the JRField expression contains "."...
                if (field.getName().indexOf(".") != -1) //  get all but the last item in the JRField expression
                //  <list expression> = <first item in JRField expression>
                {
                    listExpr = listExpr.substring(0, listExpr.indexOf("."));
                }

                List list = (List) baseEntity.invokeGetter(listExpr);

                /**
                 * *******************************
                 */
                /*  Create result to be returned  */
                /**
                 * *******************************
                 */
                //  for iterating over the list's elements
                Iterator listIterator = list.iterator();

                //  Since the fields to be used for filling 'buffer' are
                //  directly under the entities contained in the passed list,
                //  <field expression> = <all but the first item in JRField expression>
                String listVal = field.getName().substring(field.getName().indexOf(".") + 1);

                //  will contain the result of iterating over the list's
                //  elements, and to be returned
                String buffer = "";

                //  to prevent the newline character "\n" from being added to
                //  'buffer' before the first item
                boolean isFirstItem = true;

                //  iterate over list's elements, and add to 'buffer'
                while (listIterator.hasNext()) {
                    //  get current entity in list
                    BaseEntity baseEntity1 = (BaseEntity) listIterator.next();

                    //  add value of the field specified in the
                    //  JRField expression
                    if (isFirstItem) {
                        isFirstItem = false;
                    } else //  if it is not the first item...
                    {
                        buffer += "\n";     //  add newline "\n"
                    }
                    buffer += baseEntity1.invokeGetter(listVal);
                }

                //  set value of 'fieldValue' to be returned
                fieldValue = buffer;
            } // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="TOTAL">
            //  if the JRField is a 'Totals' column...
            else if (fieldDescription.startsWith("TOTAL")) {
                if (fieldDescription.length() > 5) {
                    fieldDescription = fieldDescription.substring(5);
                } else {
                    fieldDescription = "";
                    return fieldValue;
                }

                /**
                 * The columns to be used for computing the total are included
                 * in 'fieldDescription' (delimited by "|") after the word
                 * "TOTAL".
                 *
                 * The values from these columns are to be summed up and set to
                 * 'fieldValue', which is eventually returned by this function
                 * at its end.
                 */
                //  to carry the summed-up value
                Double total = new Double(0);

                //  loop until the columns to be summed up are finished
                while (true) {
                    //  current column
                    String colName = "";

                    //  to indicate whether or not this is the last column
                    //  in this loop
                    boolean lastLoop = false;

                    if (fieldDescription.indexOf("|") != -1) {
                        colName = fieldDescription.substring(0, fieldDescription.indexOf("|"));
                        fieldDescription = fieldDescription.substring(fieldDescription.indexOf("|") + 1);
                    } else {
                        colName = fieldDescription;
                        lastLoop = true;
                    }

                    StringTokenizer fieldNameTokenizer = new StringTokenizer(colName, "_");
                    String relEntityAttribute = fieldNameTokenizer.nextToken();     //  periodicSalaryElements
                    String crossEntityAttribute = fieldNameTokenizer.nextToken();   //  salaryElement
                    String fldExpr = fieldNameTokenizer.nextToken();                //  basicAmount (relative to 'relEntityAttribute')
                    String dbid = fieldNameTokenizer.nextToken();                   //  dbid (of 'crossEntityAttribute')
                    String associateField = fieldNameTokenizer.nextToken();         //  payCalculatedPeriod  (relative to 'relEntityAttribute')

                    List<BaseEntity> crossRelEntities = (List<BaseEntity>) baseEntity.invokeGetter(relEntityAttribute);

                    if (crossRelEntities != null) {
                        for (BaseEntity relEntity : crossRelEntities) {
                            BaseEntity crossEntity = (BaseEntity) relEntity.invokeGetter(crossEntityAttribute);  //  salaryElement
                            if (dbid.equalsIgnoreCase(String.valueOf(crossEntity.getDbid()))) {
                                if (rsAssociatelist == null) {
                                    BigDecimal val = (BigDecimal) relEntity.invokeGetter(fldExpr);               //  basicAmount
                                    total += val.doubleValue();
                                    break;
                                } else if (relEntity.invokeGetter(associateField).equals(associateBaseEntity)) {   //  payCalculatedPeriod
                                    BigDecimal val = (BigDecimal) relEntity.invokeGetter(fldExpr);               //  basicAmount
                                    total += val.doubleValue();
                                    break;
                                }
                            }
                        }
                    }

                    //  if it is the last column...
                    if (lastLoop) {
                        break;
                    }
                }

                fieldDescription = "";
                fieldValue = total;
            } // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="GENERAL">
            //  if the JRField is NEITHER a list NOR a cross tab field...
            else //  get value of field
            {
                fieldValue = baseEntity.invokeGetter(field.getName());
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="SELF == EMPLOYEE">
            //  Self == Employee (for IDPReport)
            if (field.getName() != null
                    && field.getName().equals("accountabilities.accountability.type.value")
                    && fieldValue != null
                    && ((String) fieldValue).trim().equals("Self")) {
                fieldValue = "Employee";
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="FIELD DISPLAY EXPRESSION">
            if (!fieldDescription.equals("")) {
                int fieldIndex = fieldDescription.indexOf("$F");
                if (fieldIndex != -1) {

                    if (fieldIndex == 0) {
                        fieldValue = fieldValue.toString()
                                + fieldDescription.substring(2);
                    } else if (fieldIndex == fieldDescription.length() - 2) {
                        fieldValue = fieldDescription.substring(0, fieldIndex)
                                + fieldValue.toString();
                    } else {
                        fieldValue = fieldDescription.substring(0, fieldIndex)
                                + fieldValue.toString()
                                + fieldDescription.substring(fieldIndex + 2);
                    }
                } else {
                    fieldValue = fieldDescription;
                }
            }
            // </editor-fold>

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }

        // <editor-fold defaultstate="collapsed" desc="FIELD DISPLAY EXPRESSION">
        if (fieldValue != null) {
            if (fieldValue.getClass() == BigDecimal.class) {
                fieldValue = ((BigDecimal) fieldValue).doubleValue();
            }
        }
        // </editor-fold>
        logger.debug("Returning");
        return fieldValue;
    }

    private List<BaseEntity> getRelEntities(String relEntityAttribute) {
        logger.debug("Entering");
        List<BaseEntity> relEntities = new ArrayList<BaseEntity>();

        if (rsDetaillist == null) {
            try {
                logger.trace("Detail list equals Null");
                relEntities = (List<BaseEntity>) baseEntity.invokeGetter(relEntityAttribute);
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
        } else {
            for (BaseEntity detailEntity : rsDetaillist) {
                try {
                    if (baseEntity.equals((BaseEntity) detailEntity.invokeGetter(groupByEntityAttribute))) {
                        List<BaseEntity> auxRelEntities = (List<BaseEntity>) detailEntity.invokeGetter(relEntityAttribute);
                        for (BaseEntity auxRelEntity : auxRelEntities) {
                            if (!relEntities.contains(auxRelEntity)) {
                                relEntities.add(auxRelEntity);
                            }
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }
            }
        }
        logger.debug("Returning");
        return relEntities;
    }
}
