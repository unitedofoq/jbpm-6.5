package com.unitedofoq.fabs.core.uiframework.backbean;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lap
 */
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import org.primefaces.component.autocomplete.AutoComplete;

@ManagedBean(name = "themeService", eager = true)
@ApplicationScoped
public class ThemeService {

//    @EJB
//    private AutoCompleteView  autoCompleteView;
    private Theme theme;
    private List<Theme> themes;
    protected AutoComplete autoComplete;
    protected HtmlPanelGrid htmlPanelGrid;
    public ExpressionFactory exFactory;
    public ELContext elContext;

    @PostConstruct
    public void init() {
        exFactory = FacesContext.getCurrentInstance().getApplication().getExpressionFactory();
        elContext = FacesContext.getCurrentInstance().getELContext();
        themes = new ArrayList<Theme>();
        themes.add(new Theme(0, "Afterdark", "afterdark"));
        themes.add(new Theme(1, "Afternoon", "afternoon"));
        themes.add(new Theme(2, "Afterwork", "afterwork"));
        themes.add(new Theme(3, "Aristo", "aristo"));
        themes.add(new Theme(4, "Black-Tie", "black-tie"));
        themes.add(new Theme(5, "Blitzer", "blitzer"));
        themes.add(new Theme(6, "Bluesky", "bluesky"));
        themes.add(new Theme(7, "Bootstrap", "bootstrap"));
        themes.add(new Theme(8, "Casablanca", "casablanca"));
        themes.add(new Theme(9, "Cupertino", "cupertino"));
        themes.add(new Theme(10, "Cruze", "cruze"));
        themes.add(new Theme(11, "Dark-Hive", "dark-hive"));
        themes.add(new Theme(12, "Delta", "delta"));
        themes.add(new Theme(13, "Dot-Luv", "dot-luv"));
        themes.add(new Theme(14, "Eggplant", "eggplant"));
        themes.add(new Theme(15, "Excite-Bike", "excite-bike"));
        themes.add(new Theme(16, "Flick", "flick"));
        themes.add(new Theme(17, "Glass-X", "glass-x"));
        themes.add(new Theme(18, "Home", "home"));
        themes.add(new Theme(19, "Hot-Sneaks", "hot-sneaks"));
        themes.add(new Theme(20, "Humanity", "humanity"));
        themes.add(new Theme(21, "Le-Frog", "le-frog"));
        themes.add(new Theme(22, "Midnight", "midnight"));
        themes.add(new Theme(23, "Mint-Choc", "mint-choc"));
        themes.add(new Theme(24, "Overcast", "overcast"));
        themes.add(new Theme(25, "Pepper-Grinder", "pepper-grinder"));
        themes.add(new Theme(26, "Redmond", "redmond"));
        themes.add(new Theme(27, "Rocket", "rocket"));
        themes.add(new Theme(28, "Sam", "sam"));
        themes.add(new Theme(29, "Smoothness", "smoothness"));
        themes.add(new Theme(30, "South-Street", "south-street"));
        themes.add(new Theme(31, "Start", "start"));
        themes.add(new Theme(32, "Sunny", "sunny"));
        themes.add(new Theme(33, "Swanky-Purse", "swanky-purse"));
        themes.add(new Theme(34, "Trontastic", "trontastic"));
        themes.add(new Theme(35, "UI-Darkness", "ui-darkness"));
        themes.add(new Theme(36, "UI-Lightness", "ui-lightness"));
        themes.add(new Theme(37, "Vader", "vader"));
    }

    public List<Theme> getThemes() {
        return themes;
    }

    public AutoComplete getAutoComplete() {
        autoComplete = new AutoComplete();
        autoComplete = (AutoComplete) FacesContext.getCurrentInstance().getApplication().createComponent(AutoComplete.COMPONENT_TYPE);
        autoComplete.setId("yy");
        autoComplete.setValueExpression("value", exFactory.createValueExpression(elContext,
                                "#{autoCompleteView.theme1}",  Theme.class));//" + autoCompleteView.getTheme1() + "
        autoComplete.setForceSelection(true);
        autoComplete.setVar("theme");
        autoComplete.setCompleteMethod(exFactory.createMethodExpression(elContext,
                                "#{autoCompleteView.completeTheme}",List.class,new Class[]{String.class}));
        autoComplete.setValueExpression("itemLabel", exFactory.createValueExpression(elContext,
                                "#{theme.displayName}",  String.class));//" + theme.getDisplayName() + "
        autoComplete.setValueExpression("itemValue", exFactory.createValueExpression(elContext,
                                "#{theme}",  String.class));

        return autoComplete;
    }

    public void setAutoComplete(AutoComplete autoComplete) {
        this.autoComplete = autoComplete;
    }
    
    public void setHtmlPanelGrid(HtmlPanelGrid htmlPanelGrid) {
        this.htmlPanelGrid = htmlPanelGrid;
    }
    
    public HtmlPanelGrid getHtmlPanelGrid() {
        htmlPanelGrid = new HtmlPanelGrid();
        htmlPanelGrid.setId("j");
        htmlPanelGrid.getChildren().add(getAutoComplete());
        return htmlPanelGrid;
    }

}
