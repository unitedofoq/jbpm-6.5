/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.util.List;

/**
 *
 * @author nkhalil
 */
public class SelfRelationTreeElement{
    private List<BaseEntity> childrenElemnt;
     private BaseEntity relationElement;
    private boolean hasLevels;
    private BaseEntity parentElement;
    private String image;

    public String getImage() {
        return image;//)?"/resources/tree_nav_top_open_no_siblings.gif":"/resources/tree_line_blank.gif";
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<BaseEntity> getChildrenElemnt() {
        return childrenElemnt;
    }

    public void setChildrenElemnt(List<BaseEntity> childrenElemnt) {
        this.childrenElemnt = childrenElemnt;
    }

    public BaseEntity getParentElement() {
        return parentElement;
    }

    public void setParentElement(BaseEntity parentElement) {
        this.parentElement = parentElement;
    }  
    

    public boolean isHasLevels() {
        return hasLevels;
    }

    public void setHasLevels(boolean hasLevels) {
        this.hasLevels = hasLevels;
    }

    public BaseEntity getRelationElement() {
        return relationElement;
    }

    public void setRelationElement(BaseEntity relationElement) {
        this.relationElement = relationElement;
    }
    
}
