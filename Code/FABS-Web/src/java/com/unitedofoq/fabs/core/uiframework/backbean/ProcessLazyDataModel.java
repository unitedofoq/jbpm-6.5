/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.process.HumanTaskServiceRemote;
import com.unitedofoq.fabs.core.process.ProcessServiceRemote;
import com.unitedofoq.fabs.core.process.jBPM.ProcessInstance;
import com.unitedofoq.fabs.core.security.user.Inbox;
import com.unitedofoq.fabs.core.security.user.OUser;
import static com.unitedofoq.fabs.core.uiframework.backbean.InboxLazyDataModel.LOGGER;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.naming.NamingException;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mmasoud
 */
public class ProcessLazyDataModel extends LazyDataModel<ProcessInstance> {

    final static Logger LOGGER = LoggerFactory.getLogger(LazyBaseEntityModel.class);

    private ProcessServiceRemote processService;
    private final OUser loggedUser;
    private List<ProcessInstance> processInstances;

    public ProcessLazyDataModel(OUser loggedUser) {
        this.loggedUser = loggedUser;
        try {
            processService = (ProcessServiceRemote) (new javax.naming.InitialContext()).lookup("java:global/ofoq/com.unitedofoq.fabs.core.process.ProcessServiceRemote");
        } catch (NamingException ex) {
            java.util.logging.Logger.getLogger(ProcessLazyDataModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<ProcessInstance> load(int first, int pageSize, String sortedField, SortOrder sortOrder, Map<String, String> filterMap) {

        LOGGER.debug("Entering");
        String fieldToSort = null;
        String order = sortOrder.name().substring(0, sortOrder.name().indexOf("ENDING"));
        Map<String, String> filters = null;
        if (sortedField == null) {
            fieldToSort = "p.startDate";
            order = "DESC";
        } else {
            fieldToSort = adaptFieldParameter(sortedField);
        }
        int intValue = -1;
        if (filterMap != null) {
            filters = new HashMap<>();
            for (Map.Entry filter : filterMap.entrySet()) {
                String key = (String) filter.getKey();
                String value = (String) filter.getValue();
                if (key.equalsIgnoreCase("status")) {
                    intValue = 0;
                    if ("active".contains(value.trim().toLowerCase())) {
                        intValue += 1;
                    }
                    if ("completed".contains(value.trim().toLowerCase())) {
                        intValue += 2;
                    }
                    switch (intValue) {
                        case 1:
                            filters.put(adaptFieldParameter(key), "1");
                            break;
                        case 2:
                            filters.put(adaptFieldParameter(key), "2");
                            break;
                        default:
                    }
                } else {
                    filters.put(adaptFieldParameter(key), value);
                }
            }
        }
        if (intValue == 0) {
            this.setRowCount(0);
            return new ArrayList<ProcessInstance>();
        } else {
            Map.Entry<Long, List<ProcessInstance>> entry = processService.
                    getAllProcessInstances(loggedUser, first / pageSize, pageSize, fieldToSort, order, filters)
                    .entrySet().iterator().next();
            this.setRowCount(entry.getKey().intValue());
            return entry.getValue();
        }
    }

    private String adaptFieldParameter(String fieldParameter) {
        switch (fieldParameter) {
            case "name":
                fieldParameter = "p.processName";
                break;
            case "initiator":
                fieldParameter = "v.value";
                break;
            default:
                fieldParameter = "p." + fieldParameter;
        }
        return fieldParameter;
    }
}
