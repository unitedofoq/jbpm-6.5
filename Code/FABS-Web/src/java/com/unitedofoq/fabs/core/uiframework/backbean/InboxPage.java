/*
 * InboxPage.java
 *
 * Created on Dec 25, 2008, 5:51:38 PM
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.bpm.model.Task;
import com.unitedofoq.fabs.bpm.model.status.TaskStatus;
import com.unitedofoq.fabs.core.alert.entities.FiredAlert;
import com.unitedofoq.fabs.core.cash.CacheServiceLocal;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.process.TaskFunction;
import com.unitedofoq.fabs.core.routing.ORoutingTaskInstance;
import com.unitedofoq.fabs.core.security.user.Inbox;
import static com.unitedofoq.fabs.core.uiframework.backbean.OBackBean.generateScreenInstId;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import com.unitedofoq.fabs.core.uiframework.screen.FormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.el.MethodExpression;
import javax.el.ValueExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;
import org.primefaces.component.ajaxstatus.AjaxStatus;
import org.primefaces.component.column.Column;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.commandlink.CommandLink;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.graphicimage.GraphicImage;
import org.primefaces.component.panel.Panel;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Page bean that corresponds to a similarly named JSP page. This class contains
 * component definitions (and initialization code) for all components that you
 * have defined on this page, as well as lifecycle methods and event handlers
 * where you may add behavior to respond to incoming events.</p>
 *
 * @author mmohamed
 */
@ManagedBean(name = "InboxPage")
@ViewScoped
public class InboxPage extends SingleEntityBean {

    final static org.slf4j.Logger logger = LoggerFactory.getLogger(InboxPage.class);

    private CacheServiceLocal cacheService = OEJB.lookup(CacheServiceLocal.class);

    private List<Inbox> tasks = new ArrayList<>();

    @Override
    protected boolean onInit(Map<String, String> requestParams) {
        logger.debug("Entering");
        if (!super.onInit(requestParams)) {
            logger.debug("Returning with false");
            return false;
        }

        onloadJSStr = "";

        inboxLazyDataModel = new InboxLazyDataModel(loggedUser, tasks);
        return true;
    }

    public void refreshAction(ActionEvent ae) {
        refresh();
    }

    @Override
    protected String getBeanName() {
        return "InboxPage";
    }

    public void taskActionListener(ActionEvent ae) {
        logger.debug("Entering");
        String taskID = null;
        if (ae.getComponent() instanceof CommandButton) {
            taskID = ((CommandButton) ae.getComponent()).getTitle();
        } else if (ae.getComponent() instanceof CommandLink) {
            taskID = ((CommandLink) ae.getComponent()).getTitle();

            if (taskID == null
                    && (CommandLink) ae.getComponent() != null
                    && (HtmlOutputText) ((CommandLink) ae.getComponent()).getChildren().get(0) != null) {
                taskID = ((HtmlOutputText) ((CommandLink) ae.getComponent()).getChildren().get(0)).getTitle();
            }
        }

        logger.trace("Selected Task ID is: {}", taskID);
        String htScreen;
        if (taskID.startsWith("A:")) {
            taskID = taskID.substring(2);
            try {
                ODataMessage message = new ODataMessage();
                ODataType oDataType = dataTypeService.loadODataType("DBID", loggedUser);
                message.setODataType(oDataType);
                List data = new ArrayList();
                data.add(Long.valueOf(taskID));
                message.setData(data);
                String screenInstanceID = generateScreenInstId("SimpleAlert");
                SessionManager.setScreenSessionAttribute(screenInstanceID,
                        OPortalUtil.MESSAGE, message);
                SessionManager.setScreenSessionAttribute(screenInstanceID,
                        MODE_VAR, OScreen.SM_EDIT);
                openScreen("SimpleAlert", screenInstanceID);
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
        } else if (taskID.startsWith("RM:")) {
            try {
                //Routing Task Screen
                taskID = taskID.substring(3).trim();
                //Load task instance to get routing entity
                ORoutingTaskInstance taskInstance = (ORoutingTaskInstance) oem.loadEntity(ORoutingTaskInstance.class.getSimpleName(),
                        Collections.singletonList("dbid = " + taskID), null, loggedUser);
                ODataMessage taskInfoMsg = new ODataMessage();
                taskInfoMsg.setODataType(dataMessage.getODataType());
                List routingData = new ArrayList();
                String routingScreen;// = null;
                routingData.add(taskID);
                taskInfoMsg.setData(routingData);

                //session datamessage
                ODataMessage screenODM = new ODataMessage();
                ODataType oDataType = dataTypeService.loadODataType("DBID", loggedUser);
                screenODM.setODataType(oDataType);
                List screenData = new ArrayList();
                screenData.add(taskInstance.getRoutingEntityDBID());
                screenODM.setData(screenData);

                OFunctionResult oFR = routingService.getRoutingTaskInfo(taskInfoMsg, null, getLoggedUser());
                if (oFR != null && oFR.getReturnValues() != null && !oFR.getReturnValues().isEmpty()) {
                    routingScreen = ((ORoutingTaskInstance) oFR.getReturnValues().get(0)).
                            getOroutingTask().getTaskScreen().getName();
                    String screenInstanceID = generateScreenInstId(routingScreen);
                    SessionManager.setScreenSessionAttribute(screenInstanceID, "routingTaskInstID", taskID);
                    SessionManager.setScreenSessionAttribute(screenInstanceID, "screenRoutingMode", routingScreen);
                    SessionManager.setScreenSessionAttribute(screenInstanceID, MODE_VAR, OScreen.SM_EDIT);
                    SessionManager.setScreenSessionAttribute(screenInstanceID, OPortalUtil.MESSAGE, screenODM);
                    logger.debug("opening screen: " + routingScreen);
                    openScreen(routingScreen, screenInstanceID);
                }
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
        } else if (taskID.startsWith("TD:")) {
            //TTaskInfo task = ((InboxLazyDataModel) inboxLazyDataModel).getTaskById(Long.valueOf(taskID)).getTaskInfo();
            //SessionManager.addSessionAttribute("taskID", task);
            //openScreen("TaskProperties");
        } else if (taskID.startsWith("FA:")) {
            try {
                FiredAlert alert = (FiredAlert) oem.loadEntity(FiredAlert.class.getSimpleName(),
                        Long.valueOf(taskID.substring(3)).longValue(), null, null, loggedUser);

                if (null == alert.getActOnEntity()) {
                    String openedScreenInstId = generateScreenInstId("FiredAlert");
                    ODataMessage message = new ODataMessage();
                    ODataType oDataType = dataTypeService.loadODataType(FiredAlert.class.getSimpleName(), loggedUser);
                    message.setODataType(oDataType);
                    List data = new ArrayList();
                    data.add(alert);
                    message.setData(data);
                    SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.MESSAGE,
                            message);
                    openScreen("FiredAlert", openedScreenInstId);
                } else {
                    List<String> conditions = new ArrayList<String>();
                    conditions.add("oactOnEntity.dbid=" + alert.getActOnEntity().getDbid());
                    conditions.add("basicDetail=1");
                    FormScreen basicDetailScreen = (FormScreen) oem.loadEntity("FormScreen", conditions,
                            null, oem.getSystemUser(loggedUser));

                    BaseEntity alertDate = (BaseEntity) oem.loadEntity(alert.getActOnEntity().getEntityClassName(), alert.getEntityDBID(), null, null, loggedUser);

                    String openedScreenInstId = generateScreenInstId(basicDetailScreen.getName());
                    ODataMessage message = new ODataMessage();
                    ODataType oDataType = dataTypeService.loadODataType(alert.getActOnEntity().getEntityClassName(), loggedUser);
                    message.setODataType(oDataType);
                    List data = new ArrayList();
                    data.add(alertDate);
                    message.setData(data);
                    SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.MESSAGE,
                            message);
                    openScreen(basicDetailScreen.getName(), openedScreenInstId);

                }

                alert.setInActive(true);
                oem.saveEntity(alert, loggedUser);

                refresh();
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
        } else {
            doActionOnTask(taskID);
        }
    }

    /**
     * ****************************************************************************
     * Method signature :- private void doActionOnTask(String taskId)
     *
     * @param :- String taskId
     * @return :- void
     *
     * Description :- this method receive taskId and get inbox by taskId from
     * myTasks and then get a Task from inbox then check status of Task if
     * status COMPLETED ,remove a task from listProveder and refresh inbox and
     * exit. if status is READY claim and start a task if status is RESERVED
     * start a task then put setScreenSessionAttribute on a session and open a
     * task screen for complete this task.
     * @author hahmed
     *
     * CreationDate:- 29-11-2017 5:15 pm
     *
     * LastModificationDate:- ModifiedBy:-
     *
     *
     *
     *****************************************************************************
     */
    private void doActionOnTask(String taskId) {
        logger.debug("start tackeActionOnTask  :" + taskId);
        Inbox currentInbox = ((InboxLazyDataModel) inboxLazyDataModel).getTaskById(Long.valueOf(taskId));
        Task task = null;
        String htScreen;

        if (currentInbox != null) {
            task = currentInbox.getTask();
        }
        if (task == null) {
            logger.debug("Task : " + taskId + " is null");
            logger.debug("Returning");
            return;
        }
        String requesterLoginName = (String) currentInbox.getTask().getInputs().get("actorid");
        long reqTenantID = 0;
        logger.debug("Get reqTenantID of : " + requesterLoginName);
        if (requesterLoginName != null && !requesterLoginName.equals("")) {
            logger.debug("Get reqTenantID of : " + requesterLoginName);
            reqTenantID = centralOEM.getUserTenant(requesterLoginName).getId();
            logger.debug("reqTenantID of : " + requesterLoginName + "is : " + reqTenantID);
        }

        TaskStatus status = humanTaskService.getTaskStatus(Long.parseLong(taskId));
        TaskFunction function = currentInbox.getFunction();
        if (function == null || function.getScreenName() == null) {
            logger.warn("Task doesn't have a valid Screen Function");
            logger.debug("Returning");
            return;
        }
        if (status.equals(TaskStatus.COMPLETED)) {
            logger.debug("Status of " + taskId + " is : " + status);
            refresh();
            logger.debug("Returning with refreshing");
            return;
        } else if (status.equals(TaskStatus.READY) && task.getCreatorId().equals(loggedUser.getLoginName())) {
            logger.debug("Start of claim and start of " + taskId);
            humanTaskService.claimAndStartTask(task, loggedUser);
            logger.debug("End of claim and start of " + taskId);
        } else if (status.equals(TaskStatus.RESERVED) && oem.getUserForLoginName(task.getCreatorId()) != null) {
            // i think this case can not happen
            logger.debug("Start of start of " + taskId);
            humanTaskService.startTask(task, loggedUser);
            logger.debug("End of start of " + taskId);
        }
        htScreen = function.getScreenName();
        currentInbox.setState("CLAIMED");
        if (status.equals(TaskStatus.READY)) {
            updateView();
        }

        if (htScreen != null) {

            String screenInstanceID = getScreenIdForTask(htScreen, taskId);
            logger.debug("set ScreenSessionAttributes for  " + htScreen);
            SessionManager.setScreenSessionAttribute(screenInstanceID, "inboxID", getScreenInstId());
            SessionManager.setScreenSessionAttribute(screenInstanceID, "taskID", task);
            SessionManager.setScreenSessionAttribute(screenInstanceID, Requester_Tenant_ID, reqTenantID);
            SessionManager.setScreenSessionAttribute(screenInstanceID, "screenProcessMode", htScreen);
            SessionManager.setScreenSessionAttribute(screenInstanceID, "taskFunction", function);

            // cacheService.cache(taskID, task, loggedUser);
            cacheService.cache("inboxID", getScreenInstId(), loggedUser);
            cacheService.cache("currentInbox", currentInbox, loggedUser);
            cacheService.cache(Requester_Tenant_ID, reqTenantID, loggedUser);
            cacheService.cache("screenProcessMode", htScreen, loggedUser);
            //cacheService.cache("taskFunction",function,loggedUser);

            if (!OPortalUtil.isPortletAdded(screenInstanceID)) {
                logger.debug("open screen for   " + htScreen);
                openScreen(function.getScreenName(),
                        function.getScreenViewPage(),
                        function.getScreenHeader(),
                        screenInstanceID, c2AMode);
            }
            logger.debug("Returning");
            return;
        }
    }

    private String getScreenIdForTask(String screenName, String taskID) {
        return screenName + "_" + taskID;
    }

    @Override
    public String refresh() {
        logger.debug("Entering");

        messagesPanel.clearMessages();

        inboxTable.reset();
        inboxTable.setValueExpression("sortBy", null);

        updateView();

        return null;
    }

    public void updateView() {
        RequestContext.getCurrentInstance().update(inboxTable.getClientId());

        if (getScreenHeight() == null || getScreenHeight().equals("")) {
            RequestContext.getCurrentInstance().execute(
                    "top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');");
        } else {
            RequestContext.getCurrentInstance().execute(
                    "top.FABS.Portlet.updateIframeSizeWithHeight({iframeName:'" + getPortletInsId() + "Frame'"
                    + ",screenHeight:'" + getScreenHeight() + "'});");
        }

        changePortletInboxTitle();

    }
    // <editor-fold defaultstate="collapsed" desc="Build Inbox Panel">
    private HtmlPanelGrid inboxPanel;

    public HtmlPanelGrid getInboxPanel() {
        htmlPanelGrid = buildInnerScreen();
        inboxPanel = htmlPanelGrid;
        inboxPanel.setDir(htmlDir);
        return inboxPanel;
    }

    public void setInboxPanel(HtmlPanelGrid inboxPanel) {
        this.inboxPanel = inboxPanel;
    }
    DataTable inboxTable = null;
    private int pageSize = 10;

    private LazyDataModel inboxLazyDataModel;

    @Override
    protected HtmlPanelGrid buildInnerScreen() {
        HtmlPanelGrid grid = new HtmlPanelGrid();
        grid.setColumns(1);
        grid.setWidth("100%");
        if (messagesPanel == null) {
            messagesPanel = new UserMessagePanel(exFactory, elContext, getBeanName(), ddService, this, loggedUser);
        }
        grid.getChildren().add(messagesPanel.createMessagePanel());
        String colorOne = "#FFAFAF", colorTwo = "#FFDFDF";
        List<ScreenField> screenFields = getCurrentScreen().getScreenFields();
        Panel middlePanel = new Panel();
        middlePanel.setStyle("position:relative");

        // <editor-fold defaultstate="collapsed" desc="GetScreenFields & Build Table">
        inboxTable = new DataTable();
        inboxTable.setWidgetVar("inboxTableWidgetVar");
        inboxTable.setStyle("position:relative; min-width:600px");
        inboxTable.setId(getCurrentScreen().getName() + "_inboxTable");
        inboxTable.setValueExpression("value",
                exFactory.createValueExpression(elContext, "#{" + getBeanName() + ".listProvider}", ArrayList.class));
        inboxTable.setVar("currentRow");
        DD inboxNoResocrdsMsgDD = ddService.getDD("inboxNoResocrdsMsg", loggedUser);
        inboxTable.setEmptyMessage(inboxNoResocrdsMsgDD.getLabelTranslated());

        // added by Belal and Masoud
        // lazy loading
        inboxTable.setLazy(true);
        inboxTable.setValue(inboxLazyDataModel);

        // paginator
        inboxTable.setRows(pageSize);
        inboxTable.setPaginatorPosition("bottom");
        inboxTable.setPaginator(true);
        inboxTable.setPaginatorTemplate("{CurrentPageReport}  {FirstPageLink} {PreviousPageLink} {PageLinks} "
                + "{NextPageLink} {LastPageLink} {JumpToPageDropdown}");
        inboxTable.setPaginatorAlwaysVisible(false);

        inboxTable.setFilterEvent("enter");

        Column refreshColumn = new Column();
        refreshColumn.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "refershColumn");
        CommandButton refreshActionButton = new CommandButton();
        refreshActionButton.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "refershIcon");
        refreshActionButton.setOnclick("inboxTableWidgetVar.clearFilters(); inboxTableWidgetVar.clearSelection();");
        refreshActionButton.setStyleClass("refresh_btn");
        refreshActionButton.setTitle(ddService.getDDLabel("refreshButton", loggedUser));
        Class[] parms = {ActionEvent.class};

        MethodExpression refreshMethodExpression = exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                "#{" + getBeanName() + ".refreshAction}", null, parms);
        MethodExpressionActionListener refreshActionListener = new MethodExpressionActionListener(refreshMethodExpression);

        refreshActionButton.addActionListener(refreshActionListener);
        refreshColumn.setHeader(refreshActionButton);
        refreshColumn.setValueExpression("style",
                exFactory.createValueExpression(elContext,
                        "#{ currentRow.colored == 1 ? 'background-color:" + colorOne
                        + ";': (currentRow.colored == 2 ? 'background-color:" + colorTwo
                        + ";' : '')}", String.class));

        inboxTable.getChildren().add(refreshColumn);
        HtmlPanelGrid loadingIconPanel = new HtmlPanelGrid();
        loadingIconPanel.setId(getFacesContext().getViewRoot().createUniqueId() + "_load");
        loadingIconPanel.setStyleClass("loading-icon");
        AjaxStatus loadingState = new AjaxStatus();
        loadingState.setId(getFacesContext().getViewRoot().createUniqueId() + "_loadAjax");
        GraphicImage loadingImage = new GraphicImage();
        loadingImage.setId(getFacesContext().getViewRoot().createUniqueId() + "_loadImg");
        loadingImage.setStyleClass("loading-active");
        loadingState.getFacets().put("start", loadingImage);
        HtmlOutputText outputTextComplete = new HtmlOutputText();
        outputTextComplete.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                + "_CompleteLBL");
        loadingState.getFacets().put("complete", outputTextComplete);
        loadingIconPanel.getChildren().add(loadingState);
        inboxTable.getChildren().add(loadingIconPanel);
        MethodExpression taskActionListenerMethodExpression = exFactory.
                createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                        "#{" + getBeanName() + ".taskActionListener}", null, parms);

        MethodExpressionActionListener taskActionListener = new MethodExpressionActionListener(taskActionListenerMethodExpression);

        for (int scrFldExpIndx = 0; scrFldExpIndx < screenFields.size(); scrFldExpIndx++) {

            ScreenField screenField = (ScreenField) screenFields.get(scrFldExpIndx);
            Column column = new Column();
            column.setId("column" + scrFldExpIndx);

            ValueExpression valueExpression = exFactory.createValueExpression(
                    elContext, "#{currentRow." + screenField.getFieldExpression() + "}",
                    String.class);
            column.setValueExpression("sortBy", valueExpression);
            column.setValueExpression("filterBy", valueExpression);
            column.setFilterMatchMode("contains");

            DD headerDD = screenField.getDd();
            try {
                headerDD = (DD) oem.loadEntity(DD.class.getSimpleName(), headerDD.getDbid(), null, null, loggedUser);
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }

            if (screenFields.get(scrFldExpIndx).getDdTitleOverride() == null || screenFields.get(scrFldExpIndx).getDdTitleOverride().equals("")) {
                column.setHeaderText(headerDD.getHeaderTranslated());
            } else {
                column.setHeaderText(screenFields.get(scrFldExpIndx).getDdTitleOverride());
            }

            if ("state".equals(screenField.getFieldExpression())) {
                GraphicImage graphicImage = new GraphicImage();
                graphicImage.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "state");
                graphicImage.setStyleClass("inbox-state-icon");
                graphicImage.setValueExpression("url",
                        exFactory.createValueExpression(elContext, "/resources/process_state/#{currentRow['state']}.gif", String.class));
                column.getChildren().add(graphicImage);
            } else if ("title".equals(screenField.getFieldExpression())) {
                CommandButton commandButton = new CommandButton();
                commandButton.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_TD_Link");
                commandButton.setType("image");
                commandButton.setValueExpression("title",
                        exFactory.createValueExpression(elContext, "TD:#{currentRow['text']}", String.class));
                commandButton.setStyleClass("info_btn");
                commandButton.setValueExpression("rendered",
                        exFactory.createValueExpression(elContext, "#{currentRow['process']}", String.class));
                commandButton.addActionListener(taskActionListener);

                CommandLink commandLink = new CommandLink();
                commandLink.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_Task_Link");

                //<editor-fold defaultstate="collapsed" desc="outputText Child for commandLink">
                HtmlOutputText hTMLOutputText = new HtmlOutputText();
                hTMLOutputText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_Out_Put_Text");
                hTMLOutputText.setValueExpression(htmlDir, valueExpression);
                hTMLOutputText.setValueExpression("value",
                        exFactory.createValueExpression(elContext, "#{currentRow['title']}", String.class));
                hTMLOutputText.setValueExpression("title",
                        exFactory.createValueExpression(elContext, "#{currentRow['text']}", String.class));
                hTMLOutputText.setEscape(false);
                //</editor-fold>

                commandLink.getChildren().add(hTMLOutputText);

                commandLink.addActionListener(taskActionListener);
                column.getChildren().add(commandButton);
                column.getChildren().add(commandLink);
            } else {
                // <editor-fold defaultstate="collapsed" desc="Add Static Text Field">

                HtmlOutputText outputText = new HtmlOutputText();
                if (screenField.getFieldExpression().contains(".")) {
                    String exp = screenField.getFieldExpression().substring(
                            screenField.getFieldExpression().lastIndexOf(".")
                            + 1);
                    outputText.setId(exp + "_outputText" + scrFldExpIndx);
                } else {
                    outputText.setId(screenField.getFieldExpression() + "_outputText" + scrFldExpIndx);
                }
                if (screenField.getFieldExpression().contains(".")) {
                    logger.debug("Screen Field Expression : " + screenField.getFieldExpression());
                    String expression1 = screenField.getFieldExpression().substring(0, screenField.getFieldExpression().indexOf("."));
                    String expression2 = screenField.getFieldExpression().substring(screenField.getFieldExpression().indexOf("."));
                    logger.debug("THE EXPRESSION ISSSSSSSS " + "#{currentRow['" + expression1 + "']" + expression2 + "}");
                    outputText.setValueExpression("value",
                            exFactory.createValueExpression(elContext,
                                    "#{currentRow['" + expression1 + "']" + expression2 + "}", String.class));
                } else {
                    outputText.setValueExpression("value",
                            exFactory.createValueExpression(elContext, "#{currentRow['" + screenField.getFieldExpression() + "']}", String.class));
                }

                outputText.setStyleClass("OTMSTextBox");
                outputText.setStyle("text-align: left; border: none; background-color: inherit;");
                column.getChildren().add(outputText);
                //</editor-fold>
            }
            column.setValueExpression("style",
                    exFactory.createValueExpression(elContext,
                            "#{ currentRow.colored == 1 ? 'background-color:" + colorOne
                            + ";': (currentRow.colored == 2 ? 'background-color:" + colorTwo
                            + ";' : '')}", String.class));
            inboxTable.getChildren().add(column);
        }

        //</editor-fold>
        //For Paginator
        inboxTable.setPaginatorPosition("bottom");
        inboxTable.setPaginator(true);
        inboxTable.setLazy(true);
        inboxTable.setPaginatorTemplate("{CurrentPageReport}  {FirstPageLink} {PreviousPageLink} {PageLinks} "
                + "{NextPageLink} {LastPageLink} {RowsPerPageDropdown}");
        middlePanel.getChildren().add(inboxTable);
        grid.getChildren().add(middlePanel);
        return grid;
    }
    // </editor-fold>

    public boolean containString(String ContainString, String childString) {
        if ("*".equals(childString)) {
            return true;
        }
        if (ContainString == null) {
            return false;
        }
        ContainString = ContainString.toLowerCase();
        if (childString != null) {
            childString = childString.toLowerCase();
        }
        if (ContainString.contains(childString)) {
            return true;
        }
        return false;
    }

    @Override
    public void loadAttribute(String expression) {
    }

    @Override
    protected List<BaseEntity> getLoadedEntity() {
        return null;
    }

    @Override
    protected void buildScreenExpression(UIComponent component) {
    }

    @Override
    protected BaseEntity getObjectForLookup(long dbid) {
        return null;
    }

    @Override
    public ODataMessage saveProcessRecords() {
        return null;
    }

    @Override
    public void addClaimButtonToPanelGroup(String buttonID, String buttonLabel, String actionMethodName, HtmlPanelGrid panelLayout, Class[] params, String portletInsId, String beanName, String screenLanguage, boolean invalidInputMode, OScreen currentScreen) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private int refreshIntervalPeriod;

    public int getRefreshIntervalPeriod() {

        return Integer.parseInt(fABSSetupLocal.getKeyValue("INBOX_REFRESH_INTERVAL", loggedUser));
    }

    public void setRefreshIntervalPeriod(int refreshIntervalPeriod) {

    }

    private void changePortletInboxTitle() {

        logger.trace("Entering");

        int unread = 0;
        int allTasks = 0;

        try {

            for (Inbox task : tasks) {

                unread += task.getState().equals("READY") ? 1 : 0;
            }

            allTasks = tasks.size();

        } catch (Exception ex) {
            logger.warn("No Inbox Items Found");
            logger.error("Exception thrown", ex);
        }
        RequestContext context = RequestContext.getCurrentInstance();

        String title;
        DD inboxTitle;
        if (allTasks == 0) {
            inboxTitle = ddService.getDD("NoInboxTitle", loggedUser);
        } else {
            inboxTitle = ddService.getDD("InboxTitle", loggedUser);
        }
        if (inboxTitle == null) {
            title = currentScreen.getHeaderTranslated() + " " + (allTasks == 0 ? ""
                    : " (" + unread + " Out of " + allTasks + ")");
        } else {
            title = currentScreen.getHeaderTranslated() + " " + inboxTitle.getHeaderTranslated();
        }
        title = title.replaceAll("UNREAD", unread + "").replaceAll("READ", (allTasks - unread) + "")
                .replaceAll("ALL", allTasks + "");
        context.execute("top.FABS.Portlet.setTitle({title:'" + title
                + "',portletId:'" + getPortletInsId() + "'});");
        if (onloadJSStr.isEmpty()) {
            onloadJSStr += "top.FABS.Portlet.setTitle({title:'" + title
                    + "',portletId:'" + getPortletInsId() + "'});";
        }

    }

}
