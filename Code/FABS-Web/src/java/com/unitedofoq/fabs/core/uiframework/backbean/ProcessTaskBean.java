/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.process.jBPM.TaskInstance;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import java.util.ArrayList;
import java.util.List;
import javax.el.ValueExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.component.column.Column;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.panel.Panel;

/**
 *
 * @author mostafa
 */
@ManagedBean(name = "ProcessTaskBean")
@ViewScoped
public class ProcessTaskBean extends TabularBean {

    @Override
    public void init() {
        super.init();
        filltable();
    }
    
    private List<TaskInstance> tasks =  new ArrayList<TaskInstance>();
    
    @Override
    public HtmlPanelGrid buildInnerScreen() {
        logger.debug("Entering");
        HtmlPanelGrid grid = new HtmlPanelGrid();
        grid.setColumns(1);
        grid.setWidth("100%");
        List<ScreenField> screenFields = getCurrentScreen().getScreenFields();
        Panel middlePanel = new Panel();
        middlePanel.setStyle("position:relative");

        // <editor-fold defaultstate="collapsed" desc="GetScreenFields & Build Table">
        DataTable ProcessTaskInstsTable = new DataTable();
        ProcessTaskInstsTable.setResizableColumns(true);
        ProcessTaskInstsTable.setStyleClass("dt-table");
        ProcessTaskInstsTable.setId(getCurrentScreen().getName() + "_inboxTable");
        ProcessTaskInstsTable.setRows(7);//For Paginator
        ProcessTaskInstsTable.setVar("currentRow");
        ProcessTaskInstsTable.setPaginatorAlwaysVisible(false);
        //For Paginator
        ProcessTaskInstsTable.setPaginatorPosition("bottom");
        ProcessTaskInstsTable.setPaginator(true);
        ProcessTaskInstsTable.setPaginatorTemplate("{CurrentPageReport}  {FirstPageLink} {PreviousPageLink} {PageLinks} "
                + "{NextPageLink} {LastPageLink} {RowsPerPageDropdown}");
        
        
        ProcessTaskInstsTable.setValueExpression("value",
                exFactory.createValueExpression(elContext, "#{" + getBeanName() + ".tasks}", ArrayList.class));
//        //fields
        for (int scrFldExpIndx = 0; scrFldExpIndx < screenFields.size(); scrFldExpIndx++) {

            ScreenField screenField = (ScreenField) screenFields.get(scrFldExpIndx);
            Column column = new Column();
            column.setId("column" + scrFldExpIndx);
            column.setStyleClass("column-business");

            ValueExpression valueExpression = exFactory.createValueExpression(
                    elContext, "#{currentRow." + screenField.getFieldExpression() + "}",
                    String.class);
            column.setValueExpression("sortBy", valueExpression);
            column.setValueExpression("filterBy", valueExpression);
            column.setFilterMatchMode("contains");

            DD headerDD = screenField.getDd();
            try {
                headerDD = (DD) oem.loadEntity(DD.class.getSimpleName(), headerDD.getDbid(), null, null, loggedUser);
            } catch (Exception ex) {
                logger.error("Exception thrown",ex);
            }

            if (screenFields.get(scrFldExpIndx).getDdTitleOverride() == null || screenFields.get(scrFldExpIndx).getDdTitleOverride().equals("")) {
                column.setHeaderText(headerDD.getHeaderTranslated());
            } else {
                column.setHeaderText(screenFields.get(scrFldExpIndx).getDdTitleOverride());
            }

            HtmlOutputText outputText = new HtmlOutputText();
            outputText.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_outputText");
            outputText.setValueExpression("value",
                    exFactory.createValueExpression(elContext,
                    "#{currentRow['" + screenField.getFieldExpression() + "']}", String.class));
            outputText.setStyleClass("OTMSTextBox");
            outputText.setStyle("text-align: left; border: none; background-color: inherit;");

            column.getChildren().add(outputText);

            ProcessTaskInstsTable.getChildren().add(column);

        }

        grid.getChildren().add(ProcessTaskInstsTable);
        logger.debug("Returning");
        return grid;
    }

    @Override
    protected String getBeanName() {
        return "ProcessTaskBean";
    }

    private void filltable() {
        String processInstanceId = getDataMessage().getData().get(1).toString();
        List<TaskInstance> taskList = humanTaskService.getProcessTasks(processInstanceId,loggedUser);
        
        //<editor-fold defaultstate="collapsed" desc="Update For Delegates">
        for (int i = 0; i < taskList.size(); i++) {
            TaskInstance taskInstance = taskList.get(i);
            taskInstance.setAssignee(userServiceRemote.getUserDisplayedName(taskInstance.getAssignee()));
            taskInstance.setFromUser(userServiceRemote.getUserDisplayedName(taskInstance.getFromUser()));
        }
        //</editor-fold>
        
        setTasks(taskList);
    }
    
       
    public void taskActionListener(ActionEvent ae) {
        
    }

    public List<TaskInstance> getTasks() {
        return tasks;
    }

    public void setTasks(List<TaskInstance> tasks) {
        this.tasks = tasks;
    }
}
