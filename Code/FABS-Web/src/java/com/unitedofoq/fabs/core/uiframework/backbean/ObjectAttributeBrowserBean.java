package com.unitedofoq.fabs.core.uiframework.backbean;

/**
 * <p/>
 * The ObjectAttributeBrowserBean class is responsible for store the notion of a selected node.
 * A node becomes selected when a user click on it; the command button that throws
 * the ActionEvent is listened to by one DynamicNodeUserObject Object.  This
 * via a ObjectAttributeBrowserBean call back sets its self as the selected node.
 * </p>
 */
public class ObjectAttributeBrowserBean{

}
