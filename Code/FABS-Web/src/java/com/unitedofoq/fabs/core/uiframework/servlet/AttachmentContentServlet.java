/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.servlet;

import com.unitedofoq.fabs.core.Exception.BasicException;
import com.unitedofoq.fabs.core.content.AttachmentDTO;
import com.unitedofoq.fabs.core.content.AttachmentServiceLocal;
import com.unitedofoq.fabs.core.content.EntityAttachmentServiceLocal;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserServiceRemote;
import com.unitedofoq.fabs.core.uiframework.backbean.SessionManager;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author lap
 */
@WebServlet(name = "AttachmentContentServlet", urlPatterns = {"/AttachmentContentServlet"})
public class AttachmentContentServlet extends HttpServlet {

    @EJB
    EntityAttachmentServiceLocal entityAttachmentService;

    private OUser loggedUser;
    final static Logger logger = LoggerFactory.getLogger(AttachmentContentServlet.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String attachemntId = new String(request.getParameter("attachmentId").getBytes("iso-8859-1"), "UTF-8");
        loggedUser = (OUser) request.getSession().getAttribute("loggedUser");
        AttachmentDTO entityAttachment;
        try {
            entityAttachment = entityAttachmentService.getAttachmentById(attachemntId, loggedUser,"");
            String userId= request.getSession().getAttribute("USERID").toString();
            if(OPortalUtil.isUserLoggedIn(Long.parseLong(userId))&& entityAttachment.getAttachmentUser().equals(loggedUser.getLoginName())){
            viewAttachment(entityAttachment, request, response);
            }
        } catch (BasicException ex) {
            logger.error("Exception thrown", ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void viewAttachment(AttachmentDTO entityAttachment, HttpServletRequest request, HttpServletResponse response) {
        response.setContentType(entityAttachment.getMimeType());
        response.setHeader("Content-Disposition", "inline;filename=" + entityAttachment.getName());
        try {
            response.getOutputStream().write(entityAttachment.getAttachment());
        } catch (IOException ex) {
            logger.error("Exception thrown", ex);
        }

    }

}
