/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.context.RequestContext;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.screen.FormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.PrsnlzdFormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.PrsnlzdTabularScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.uiframework.screen.SingleEntityScreen;
import com.unitedofoq.fabs.core.uiframework.screen.TabularScreen;
import com.unitedofoq.fabs.core.validation.FABSException;

/**
 *
 * @author fabs
 */
@ManagedBean(name = "PrsnlzTabularBean")
@ViewScoped
public class PrsnlzTabularBean extends TabularBean {

    private OScreen passedPrsnlzdOScreen;
    private OScreen copyOfOScreen;
    private BaseEntity copyOfLoadedObject;
    private String originalOScreenDBID;
    OFunctionResult oFR = new OFunctionResult();
    
    protected UIFrameworkServiceRemote uiFrameWorkFacade = OEJB.lookup(UIFrameworkServiceRemote.class);

    @Override
    protected String getBeanName() {
        return "PrsnlzTabularBean";
    }

    @Override
    public void init() {
        super.init();
        originalOScreenDBID = String.valueOf(((OScreen) (dataMessage.getData().get(0))).getDbid());
        if (originalOScreenDBID != null) {
            try {
                loadScreenObjects();
                mode = OScreen.SM_EDIT;
            } catch (Exception ex) {
                logger.error("Exception thrown",ex);
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
                messagesPanel.clearAndDisplayMessages(oFR);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="loadObject">
    private void loadScreenObjects() {
        try {
            passedPrsnlzdOScreen = (OScreen) oem.loadEntity(OScreen.class.getSimpleName(), Collections.singletonList("dbid = '" + originalOScreenDBID + "'"), null, loggedUser);
            if (passedPrsnlzdOScreen.isPrsnlzd()) {
                List<String> conditions = new ArrayList<String>();
                conditions.add("prsnlzdOriginal_DBID = '" + passedPrsnlzdOScreen.getDbid() + "'");
                conditions.add("prsnlzdUser_DBID = '" + loggedUser.getDbid() + "'");
                copyOfOScreen = (OScreen) oem.loadEntity(OScreen.class.getSimpleName(), conditions, null, loggedUser);
            }
        } catch (UserNotAuthorizedException ex) {
            logger.error("Exception thrown",ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
            messagesPanel.clearAndDisplayMessages(oFR);
        }
    }

    // </editor-fold>
    @Override
    public String saveRowObjectListAction() {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            List<BaseEntity> savedTempList = new ArrayList<BaseEntity>();
            boolean save = true;
            List<BaseEntity> lodedListTmp = new ArrayList<BaseEntity>();

            //FIXME : loaded list does not change when using paginator
            lodedListTmp.addAll(loadedList);
            for (BaseEntity entity : lodedListTmp) {
                saveRow(entity, savedTempList);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
            messagesPanel.clearAndDisplayMessages(oFR);
        }
        loadScreenObjects();
        messagesPanel.clearAndDisplayMessages(oFR);
        logger.debug("Returning with Null");
        return null;
    }

    //<editor-fold defaultstate="collapsed" desc="Save Row">
    public OFunctionResult saveRow(BaseEntity entity, List<BaseEntity> savedTempList) {
        logger.debug("Entering");
        try {

            OFunctionResult entityOFR = null;
            boolean save = true;
            //Apply Sequence Initialization
            // save data 
            SingleEntityScreen copyScr = saveScreenCopy();
            if (entity.getDbid() == 0 && save) {
                logger.trace("Entity Should Be Saved");
                //new row
                ((ScreenField) entity).setPrsnlzdOriginal_DBID(-1);
                ((ScreenField) entity).setPrsnlzdUser_DBID(loggedUser.getDbid());
                ((ScreenField) entity).setOScreen(copyScr);
                ((ScreenField) entity).setEditable(true);
                entityOFR = entitySetupService.callEntityCreateAction(entity, loggedUser);
                oFR.append(entityOFR);

            } else if (entity.isChanged() && save) {
                if (((ScreenField) entity).getPrsnlzdOriginal_DBID() == 0) {
                    ((ScreenField) entity).setPrsnlzdOriginal_DBID(entity.getDbid());
                    ((ScreenField) entity).setPrsnlzdUser_DBID(loggedUser.getDbid());
                    if (((ScreenField) entity).getDdTitleOverrideTranslated() != null && !((ScreenField) entity).getDdTitleOverrideTranslated().equalsIgnoreCase("")) {
                        ((ScreenField) entity).setDdTitleOverrideTranslated(((ScreenField) entity).getDdTitleOverride());
                    }
                    entity.setDbid(0);
                    ((ScreenField) entity).setOScreen(copyScr);
                    entityOFR = entitySetupService.callEntityCreateAction(entity, loggedUser);
                    oFR.append(entityOFR);
                } else {
                    List<String> conditions = new ArrayList<String>();
                    conditions.add("prsnlzdOriginal_DBID = " + ((ScreenField) entity).getPrsnlzdOriginal_DBID() + "");
                    if (((ScreenField) entity).getPrsnlzdOriginal_DBID() == -1) {
                        conditions.add("dbid = " + entity.getDbid());
                    }
                    conditions.add("prsnlzdUser_DBID = " + loggedUser.getDbid() + "");
                    FABSEntitySpecs entityFABSSpecs = ScreenField.class.getAnnotation(FABSEntitySpecs.class);
                    String[] prsnlzableFieldsNames = entityFABSSpecs.personalizableFields();
                    ScreenField toBeUpdated = (ScreenField) oem.loadEntity(ScreenField.class.getSimpleName(), conditions, null, loggedUser);
                    if (toBeUpdated != null) {
                        if (entityFABSSpecs != null && prsnlzableFieldsNames != null) {
                            for (String personalizableField : prsnlzableFieldsNames) {
                                FramworkUtilities.setValueInEntity(toBeUpdated, personalizableField, FramworkUtilities.getValueFromEntity(entity, personalizableField));
                            }
                        }
                        toBeUpdated.setPrsnlzdOriginal_DBID(((ScreenField) entity).getPrsnlzdOriginal_DBID());
                        toBeUpdated.setPrsnlzdUser_DBID(loggedUser.getDbid());
                        toBeUpdated.setOScreen(copyScr);
                        if (toBeUpdated.getDdTitleOverrideTranslated() != null && !toBeUpdated.getDdTitleOverrideTranslated().equalsIgnoreCase("")) {
                            toBeUpdated.setDdTitleOverrideTranslated(((ScreenField) entity).getDdTitleOverride());
                        }
                        entityOFR = entitySetupService.callEntityUpdateAction(toBeUpdated, loggedUser);
                        oFR.append(entityOFR);
                    }
                }
            }

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
        } finally {
            messagesPanel.clearAndDisplayMessages(oFR);
            oem.closeEM(loggedUser);
            RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
        }
        logger.debug("Returning");
        return oFR;
    }
    //</editor-fold>

    private SingleEntityScreen saveScreenCopy() {
        logger.trace("Entering");
        try {
            BaseEntity toBeSaved = null;
            if (copyOfOScreen != null) {
                return (SingleEntityScreen) copyOfOScreen;
            }
            List<String> screenFieldExpressions = new ArrayList<String>();
            FABSEntitySpecs entityFABSSpecs = passedPrsnlzdOScreen.getClass().getAnnotation(FABSEntitySpecs.class);
            String[] prsnlzableFieldsNames = entityFABSSpecs.personalizableFields();
            for (int fieldsCounter = 0; fieldsCounter < prsnlzableFieldsNames.length; fieldsCounter++) {
                screenFieldExpressions.add(prsnlzableFieldsNames[fieldsCounter]);
            }
            if (passedPrsnlzdOScreen instanceof TabularScreen) {
                toBeSaved = new PrsnlzdTabularScreen();
            } else if (passedPrsnlzdOScreen instanceof FormScreen) {
                toBeSaved = new PrsnlzdFormScreen();
            }
            toBeSaved.NewFieldsInstances(screenFieldExpressions);

            if (entityFABSSpecs != null && prsnlzableFieldsNames != null) {
                for (String personalizableField : prsnlzableFieldsNames) {
                    Field currentField = BaseEntity.getClassField(toBeSaved.getClass(), personalizableField);
                    if (currentField.getType() == boolean.class) {
                        FramworkUtilities.setValueInEntity(toBeSaved, personalizableField, 2);
                    } else {
                        FramworkUtilities.setValueInEntity(toBeSaved, personalizableField, null);
                    }
                }
            }
            toBeSaved.invokeSetter("name", passedPrsnlzdOScreen.getName() + "_" + loggedUser.getDbid());
            toBeSaved.invokeSetter("omodule", passedPrsnlzdOScreen.getOmodule());            
            toBeSaved.invokeSetter("prsnlzdOriginal_DBID", passedPrsnlzdOScreen.getDbid());
            toBeSaved.invokeSetter("prsnlzdUser_DBID", loggedUser.getDbid());
            toBeSaved.invokeSetter("oactOnEntity", passedPrsnlzdOScreen.invokeGetter("oactOnEntity"));
            if (passedPrsnlzdOScreen instanceof FormScreen) {
                toBeSaved.invokeSetter("columnsNo", passedPrsnlzdOScreen.invokeGetter("columnsNo"));
            }

            oem.saveEntity(toBeSaved, loggedUser);
            List<String> conditions = new ArrayList<String>();
            conditions.add("prsnlzdOriginal_DBID = '" + passedPrsnlzdOScreen.getDbid() + "'");
            conditions.add("prsnlzdUser_DBID = '" + loggedUser.getDbid() + "'");
            toBeSaved = (SingleEntityScreen) oem.loadEntity(OScreen.class.getSimpleName(), conditions, null, loggedUser);

            passedPrsnlzdOScreen.setPrsnlzd(true);
            oem.saveEntity(passedPrsnlzdOScreen, loggedUser);
            loadScreenObjects();
            return (SingleEntityScreen) toBeSaved;

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            if (ex instanceof FABSException) {
                oFR.addError(usrMsgService.getUserMessage("ModuleError", loggedUser), ex);
            } else {
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
            }
            messagesPanel.clearAndDisplayMessages(oFR);
            logger.trace("Returning with Null");
            return null;
        }
    }

    @Override
    protected List<BaseEntity> loadList(
            String entityName, List<String> conditions, List<String> sortConditions) {
        logger.trace("Entering");
        List<BaseEntity> toBeReturnedList = new ArrayList<BaseEntity>();
        List<BaseEntity> toBeReturnedFieldsList = new ArrayList<BaseEntity>();
        List<BaseEntity> copyReturnedFieldsList = new ArrayList<BaseEntity>();
        try {
            toBeReturnedFieldsList = super.loadList(entityName, conditions, sortConditions);
            if (passedPrsnlzdOScreen.isPrsnlzd() && copyOfOScreen != null) {
                List<String> copyConditions = new ArrayList<String>();
                //reload copy
                copyConditions.add("oScreen.dbid = " + copyOfOScreen.getDbid());
                conditions.add("prsnlzdUser_DBID = " + loggedUser.getDbid());

                copyReturnedFieldsList = oem.loadEntityList(entityName, copyConditions, null, null, loggedUser);
                if (!copyReturnedFieldsList.isEmpty() || copyReturnedFieldsList != null) {
                    toBeReturnedList = determinePrsnlzdOScreenFieldsList(toBeReturnedFieldsList, copyReturnedFieldsList);
                } else {
                    logger.trace("Returning");
                    return toBeReturnedFieldsList;
                }
            } else {
                logger.trace("Returning");
                return toBeReturnedFieldsList;
            }
            logger.trace("Returning");
            return toBeReturnedList;

        } catch (UserNotAuthorizedException ex) {
            logger.error("Exception thrown",ex);
            messagesPanel.clearAndDisplayMessages(
                    usrMsgService.getUserMessage("UserNotAuthorized", systemUser));
            //TODO: why is that? How to display the message?
            logger.trace("Returning with new ArrayList");
            return new ArrayList<BaseEntity>();

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            messagesPanel.addErrorMessage(usrMsgService.
                    getUserMessage("SystemInternalError", systemUser), ex, true);
            logger.trace("Returning with Null");
            return null;
        }
    }

    public List<BaseEntity> determinePrsnlzdOScreenFieldsList(List<BaseEntity> originalScreenFields, List<BaseEntity> copiedScreenFields) {
        logger.debug("Entering");
        try {
            List<BaseEntity> returnedScreenFields = new ArrayList<BaseEntity>();
            List<BaseEntity> updatedCopiedScreenFields = new ArrayList<BaseEntity>();
            boolean fieldFound;

            for (BaseEntity copiedField : copiedScreenFields) {
                if (((ScreenField) copiedField).getPrsnlzdOriginal_DBID() != -1) {
                    updatedCopiedScreenFields.add(copiedField);
                    continue;
                }
                returnedScreenFields.add(copiedField);
            }

            for (BaseEntity originalField : originalScreenFields) {
                fieldFound = false;
                for (BaseEntity fieldFromCopy : updatedCopiedScreenFields) {
                    if (originalField.getDbid() == ((ScreenField) fieldFromCopy).getPrsnlzdOriginal_DBID()) {
                        fieldFound = true;
                        break;
                    }
                }
                if (fieldFound) {
                    for (BaseEntity copiedField : updatedCopiedScreenFields) {
                        if (!(((ScreenField) copiedField).getFieldExpression().equals(((ScreenField) originalField).getFieldExpression()))) {
                            continue;
                        }
                        FABSEntitySpecs entityFABSSpecs = originalField.getClass().getAnnotation(FABSEntitySpecs.class);
                        String[] prsnlzableFieldsNames = entityFABSSpecs.personalizableFields();
                        if (entityFABSSpecs != null && prsnlzableFieldsNames != null) {
                            for (String personalizableField : prsnlzableFieldsNames) {
                                if (personalizableField.equals("controlSize")) {
                                    ((ScreenField) originalField).getDd().setControlSize(Integer.valueOf((FramworkUtilities.getValueFromEntity(copiedField, personalizableField)).toString()));
                                } else {
                                    FramworkUtilities.setValueInEntity(originalField, personalizableField, FramworkUtilities.getValueFromEntity(copiedField, personalizableField));
                                }
                            }
                        }
                        ((ScreenField) originalField).setPrsnlzdOriginal_DBID(((ScreenField) copiedField).getPrsnlzdOriginal_DBID());
                        ((ScreenField) originalField).setPrsnlzdUser_DBID(((ScreenField) copiedField).getPrsnlzdUser_DBID());
                        break;
                    }
                }
                returnedScreenFields.add(originalField);
            }
            logger.debug("Returning");
            return returnedScreenFields;
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
            messagesPanel.clearAndDisplayMessages(oFR);
            logger.debug("Returning");
            return new ArrayList<BaseEntity>();
        }
    }

    @Override
    public void deleteRowObjectAction(ActionEvent event) {
        logger.debug("Entering");
        OFunctionResult entityOFR = null;
        try {
            SingleEntityScreen copyScr = copyScr = saveScreenCopy();
            for (BaseEntity entity : selectedEntities) {
                if (entity.getDbid() != 0) {
                    if (((ScreenField) entity).getPrsnlzdOriginal_DBID() == 0) {
                        ((ScreenField) entity).setPrsnlzdOriginal_DBID(entity.getDbid());
                        ((ScreenField) entity).setPrsnlzdUser_DBID(loggedUser.getDbid());
                        entity.setDbid(0);
                        ((ScreenField) entity).setPrsnlzdRemoved(true);
                        ((ScreenField) entity).setOScreen(copyScr);
                        entityOFR = entitySetupService.callEntityCreateAction(entity, loggedUser);
                        oFR.append(entityOFR);
                    } else {
                        List<String> conditions = new ArrayList<String>();
                        conditions.add("prsnlzdOriginal_DBID = " + ((ScreenField) entity).getPrsnlzdOriginal_DBID() + "");
                        conditions.add("prsnlzdUser_DBID = " + loggedUser.getDbid() + "");
                        FABSEntitySpecs entityFABSSpecs = ScreenField.class.getAnnotation(FABSEntitySpecs.class);
                        String[] prsnlzableFieldsNames = entityFABSSpecs.personalizableFields();
                        ScreenField toBeUpdated = (ScreenField) oem.loadEntity(ScreenField.class.getSimpleName(), conditions, null, loggedUser);
                        if (toBeUpdated != null) {
                            if (entityFABSSpecs != null && prsnlzableFieldsNames != null) {
                                for (String personalizableField : prsnlzableFieldsNames) {
                                    FramworkUtilities.setValueInEntity(toBeUpdated, personalizableField, FramworkUtilities.getValueFromEntity(entity, personalizableField));
                                }
                            }
                            toBeUpdated.setPrsnlzdOriginal_DBID(((ScreenField) entity).getPrsnlzdOriginal_DBID());
                            toBeUpdated.setPrsnlzdUser_DBID(loggedUser.getDbid());
                            toBeUpdated.setOScreen(copyScr);
                            ((ScreenField) entity).setPrsnlzdRemoved(true);
                            entityOFR = entitySetupService.callEntityUpdateAction(toBeUpdated, loggedUser);
                            oFR.append(entityOFR);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
            messagesPanel.clearAndDisplayMessages(oFR);
        } finally {
            logger.debug("Returning");
            messagesPanel.clearAndDisplayMessages(oFR);
        }        
    }
}
