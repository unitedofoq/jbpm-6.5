/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.filtermaps;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mostafan
 */
public class BooleanValueMapper implements ValueMapper{

    @Override
    public List<String> mapValue(String value) {
        ArrayList<String>mappedValues = new ArrayList<String>(); 
        if("true".contains(value.toLowerCase())){
            mappedValues.add("1");
            
        }
        else if("false".contains(value.toLowerCase())){
           mappedValues.add("0");
            
        }
        else{
            mappedValues.add(value);
        }
        return mappedValues;
    }

    @Override
    public boolean isValidMapperForField(Field field) {
        return field.getType().equals(boolean.class);
    }
    
    
}
