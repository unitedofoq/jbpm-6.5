/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import javax.el.ELContext;
import javax.el.MethodExpression;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.BehaviorEvent;
import org.primefaces.component.behavior.ajax.AjaxBehavior;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FABSAjaxBehavior extends AjaxBehavior {
final static Logger logger = LoggerFactory.getLogger(OEntityManager.class);
   @Override
   public Object saveState(FacesContext context)
   {
       logger.debug("Entering");
       final FABSAjaxBehaviorStateHolder stateHolder = new FABSAjaxBehaviorStateHolder();
       stateHolder.setSuperState(super.saveState(context));
       stateHolder.setUpdate(getUpdate());
       stateHolder.setProcess(getProcess());
       stateHolder.setOnComplete(getOncomplete());
       stateHolder.setOnError(getOnerror());
       stateHolder.setOnSuccess(getOnsuccess());
       stateHolder.setOnStart(getOnstart());
       stateHolder.setListener(getListener());
       if (initialStateMarked()) {
           if (stateHolder.getSuperState() == null) {
               logger.debug("Returning with Null");
               return null;
           }
       }
       logger.debug("Returning");
      return UIComponentBase.saveAttachedState(context, stateHolder);
   }
   
   
   @Override
   public void restoreState(FacesContext context, Object state)
   {
      if (state != null && context != null)
      {
         Object obj = UIComponentBase.restoreAttachedState(context, state);
         if (obj instanceof FABSAjaxBehaviorStateHolder)
         {
            FABSAjaxBehaviorStateHolder stateHolder = (FABSAjaxBehaviorStateHolder) obj;
            super.restoreState(context, stateHolder.getSuperState());
            setUpdate(stateHolder.getUpdate());
            setProcess(stateHolder.getProcess());
            setOncomplete(stateHolder.getOnComplete());
            setOnerror(stateHolder.getOnError());
            setOnsuccess(stateHolder.getOnSuccess());
            setOnstart(stateHolder.getOnStart());
            setListener(stateHolder.getListener());
         }  
      }
   }
   
   
   @Override
   public void broadcast(BehaviorEvent event) throws AbortProcessingException
   {
      super.broadcast(event);
      ELContext eLContext = FacesContext.getCurrentInstance().getELContext();
      // Backward compatible implementation of listener invocation
      MethodExpression listener = getListener();
      if (listener != null)
      {
         try {
            listener.invoke(eLContext, new Object[] { event });
         } catch(IllegalArgumentException ex) {
             logger.error("Exception thrown",ex);
            listener.invoke(eLContext, new Object[0]);
         }
      }
   }
}
