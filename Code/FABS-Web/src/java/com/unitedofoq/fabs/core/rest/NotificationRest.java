/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.rest;

import com.unitedofoq.fabs.core.notification.DockBarNotification;
import com.unitedofoq.fabs.core.notification.DockBarNotificationAction;
import com.unitedofoq.fabs.core.notification.Notification;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.websocket.NotificationHandler;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author mmasoud
 */
@Path("notificationRest")
public class NotificationRest {

    @Inject
    private NotificationHandler notificationHandler;
    
    @Inject
    private DockBarNotificationAction notificationAction;

    @POST
    @Path("/notifyUser")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response notifyUser(Notification notification) {
        OUser oUser = new OUser();
        notificationHandler.sendNotification(notification);
        return Response.status(201).build();
    }
    
    @POST
    @Path("/notifyLiferayUser")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response notifyUser(DockBarNotification notification) {
        notificationAction.addNotificationEvent(notification);
        return Response.status(201).build();
    }
}
