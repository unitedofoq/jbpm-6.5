/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.i18n.Translation;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.udc.UDCType;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.persistence.Transient;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nkhalil
 */
public class TreeBrowser {
final static Logger logger = LoggerFactory.getLogger(TreeBrowser.class);
    private Class rootClass;
    private OUser loggedUser;
    private ExpressionFactory exFactory;
    private ELContext elContext;
    private String beanName;
    private SingleEntityBean bean;
    private EntitySetupServiceRemote entitySetupService;
    private DDServiceRemote ddService;
    private OEntityManagerRemote oem;
    //  list of objects that are used in the TreeNodes
    private List<DefaultTreeNode> nodeObjects;
    // the root tree node
    private DefaultTreeNode root;
    // object reference to expand node
    private DefaultTreeNode selectedNodeObject = null;

    public TreeBrowser(Class rootClass, OUser loggedUser, ExpressionFactory exFactory,
            ELContext elContext, SingleEntityBean obackBean, EntitySetupServiceRemote entitySetupService,
            DDServiceRemote ddService, OEntityManagerRemote oem) {
        this.rootClass = rootClass;
        this.loggedUser = loggedUser;
        this.exFactory = exFactory;
        this.elContext = elContext;
        this.beanName = obackBean.getBeanName();
        this.bean = obackBean;
        this.entitySetupService = entitySetupService;
        this.ddService = ddService;
        this.oem = oem;
    }

    public void nodeExpandListener(NodeExpandEvent event) {
        try {
            retrieveSelectedObject(event.getTreeNode());
            addChildren();
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            //FIXME: how to display error to user, e.g. User Message
        } finally {
            oem.closeEM(loggedUser);
        }

    }

    public void nodeActionListener(NodeSelectEvent event) {

        // just set selected node
        retrieveSelectedObject(event.getTreeNode());
    }

    public void createRootNode(DefaultTreeNode rootNode) {
        //  to contain the text of the root node of the tree
        String rootNodeText;// = "";

        //  load root class's corresponding OEntity
        OEntityDTO entity = null;

        try {
            entity = entitySetupService.loadOEntityDTOForTree(
                    rootClass.getName(), loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }

        //  if corresponding OEntity exists in DB...
        if (entity != null) {
            rootNodeText = entity.getTitle();
        } //  if corresponding OEntity does NOT exist in DB...
        else {
            rootNodeText = rootClass.getSimpleName();
        }
        DynamicNodeUserObject mainDynamicNode = new DynamicNodeUserObject(rootNodeText, "ui_tree_node_blue");
        mainDynamicNode.setNodeField(rootClass.getSimpleName());
        //  set class of root node to be name of root class
        mainDynamicNode.setNodeClass(rootClass.getName());
        mainDynamicNode.setNodeToolTip("");
        mainDynamicNode.setNodeExpression("");
        mainDynamicNode.setNodeProperties(new ArrayList<DynamicNodeUserObject.NodeProperty>());
        root = new DefaultTreeNode("folder", mainDynamicNode, rootNode);
        root.setExpanded(true);
 
        setSelectedNodeObject(root);
        nodeObjects = new ArrayList<DefaultTreeNode>();
        nodeObjects.add(getRoot());
    }

    public void addChildren() {
        logger.debug("Entering");
        //  if node is selected...
        if (selectedNodeObject != null) {
            selectedNodeObject.getChildren().clear();
            //  to indicate whether corresponding class can be expanded or not
            //  (i.e. should retrieval of underlying field be attempted?)
            boolean isExpandable = false;

            //  if it is an OTMS class...
            if (getSelectedNodeAsDynamic().getNodeClass().contains("unitedofoq")) {
                isExpandable = true;
            } //  if it is of type 'List'...
            else if (getSelectedNodeAsDynamic().getNodeClass().endsWith("List")) {
                //  if generic type is an OTMS class
                if (getSelectedNodeAsDynamic().getNodeGenericType().contains("unitedofoq")) {
                    isExpandable = true;
                }
            }

            //  if corresponding is expandable...
            if (isExpandable) {
                //  to hold the class to be expanded
                BaseEntity baseEntity = null;

                /* EXPAND */
                try {
                    Class cls = null;
                    //  if corresponding class is NOT of type 'List'...
                    if (!getSelectedNodeAsDynamic().getNodeClass().endsWith("List")) {
                        cls = Class.forName(getSelectedNodeAsDynamic().getNodeClass());
                        baseEntity = (BaseEntity) cls.newInstance();
                    } //  if corresponding class is of type 'List'...
                    else {
                        cls = Class.forName(getSelectedNodeAsDynamic().getNodeGenericType());
                        baseEntity = (BaseEntity) cls.newInstance();
                    }

                    //  if 'baseEntity' is retrieved successfully...
                    if (baseEntity != null) {
                        //  set corresponding TreeNode as NOT a 'leaf' since
                        //  children will be added to it
                        //  selectedNodeObject.setLeaf(false);

                        //  get fields of 'baseEntity'
                        List<Field> fields = BaseEntity.getClassFields(baseEntity.getClass());

                        List<String> ignoredFieldNames = new ArrayList<String>();
                        if (cls.getAnnotation(FABSEntitySpecs.class) != null) {
                            String[] ignoredFields = ((FABSEntitySpecs) cls.getAnnotation(FABSEntitySpecs.class)).hideInheritedFields();
                            ignoredFieldNames = Arrays.asList(ignoredFields);
                        }

                        //  loop over fields of 'baseEntity', and add them as
                        //  children to corresponding TreeNode if they have DDs
                        for (Field fld : fields) {
                            if (ignoredFieldNames.contains(fld.getName())) {
                                continue;
                            }
                            String DDname = null;
                            //Check if the field is CC
                            if (fld.getType().equals(UDC.class) && (fld.getName().contains("cc1")
                                    || fld.getName().contains("cc2")
                                    || fld.getName().contains("cc3")
                                    || fld.getName().contains("cc4")
                                    || fld.getName().contains("cc5"))) {
                                OEntityDTO entity = entitySetupService.
                                        loadOEntityDTOForTree(baseEntity.getClassName(), loggedUser);
                                try {
                                long cType = Long.valueOf(BaseEntity.getValueFromEntity(entity,
                                        (fld.getName().substring(0, fld.getName().length() - 1) + "Type"
                                        + fld.getName().substring(fld.getName().length() - 1)) + "Dbid").toString());
                                if (cType == 0) {
                                    continue;
                                }
                                UDCType ccType = (UDCType) oem.loadEntity(UDCType.class.getSimpleName(),
                                        Collections.singletonList("dbid = " + cType), null, loggedUser);
                                DDname = ccType.getCode();
                                } catch (Exception ex) {
                                    logger.error("Exception thrown",ex);
                                    continue;
                                }
                            } else {

                                //  if 'fld' does not have a corresponding DD...
                                DDname = BaseEntity.getFieldDDName(baseEntity, fld.getName());
                            }

                            //  to hold the value to be displayed as the child
                            //  node's name
                            String DDlabel = "";

                            //  to indicate if the DD exists for this field
                            boolean DDexists = false;

                            //  if there is no DD name to display the item by...
                            if (DDname == null || DDname.equals("")) {
                                continue;
                            } else {
                                try {
                                    //  get DD
                                    DD dd = ddService.getDD(DDname, loggedUser);

                                    //  if DD retrieved successfully
                                    if (dd != null) {
                                        DDexists = true;
                                        DDlabel = dd.getLabel();
                                    } //  if DD does not exist, or there was problem
                                    //  in retrieval...
                                    else {
                                        DDlabel = DDname;
                                    }
                                } catch (Exception ex) {
                                    logger.error("Exception thrown",ex);
                                    DDlabel = fld.getName();
                                }
                            }

                            //  create child node
                            //DefaultMutableTreeNode childNode = new DefaultMutableTreeNode();                            
                            String nodeTXT;
                            if (fld.getAnnotation(Transient.class) != null
                                    && fld.getName().contains("Trans")) {
                                nodeTXT = DDlabel + "(T)";
                            } else {
                                nodeTXT = DDlabel;
                            }
                            DynamicNodeUserObject childObject = new DynamicNodeUserObject(fld.getName(), "ui_tree_node_blue");
                            childObject.setNodeField(fld.getName());
                            childObject.setNodeClass(fld.getType().getName());
                            childObject.setParentClass(cls);

                            //  'expression' of parent node is used to generate
                            //  the 'expression' of the child node

                            //  get parent expression
                            String parentNodeExpression = getSelectedNodeAsDynamic().getNodeExpression();

                            //  to hold node expression
                            String nodeExpression = "";

                            //  if the parent has empty expression...
                            //  (i.e. it is the ROOT node of the tree)
                            if (parentNodeExpression.equals("")) {
                                nodeExpression = fld.getName();
                            } else {
                                nodeExpression = parentNodeExpression + "." + fld.getName();
                            }

                            //  set child node expression
                            childObject.setNodeExpression(nodeExpression);

                            //  if 'fld' is of type 'List'...
                            if (fld.getType().getName().endsWith("List")) {
                                //  get the generic type of the list
                                //  (i.e. get the type of list's elements)
                                ParameterizedType pt = (ParameterizedType) fld.getGenericType();

                                //  e.g.
                                //  pt.getActualTypeArguments()[0].toString() = "class com.unitedofoq.otms.core.udc.UDC"
                                //  pt.getActualTypeArguments()[0].toString().substring(6) = "com.unitedofoq.otms.core.udc.UDC"
                                String clsName = pt.getActualTypeArguments()[0].toString().substring(6);

                                //  set child node generic type
                                childObject.setNodeGenericType(clsName);

                                // childObject.setData(DDlabel + " (*)");
                            } else {
                                childObject.setNodeGenericType("");
                            }

                            //  set child node tool tip to be field's name
                            childObject.setNodeToolTip(nodeExpression);

                            //  if corresponding DD does not exist in the database...
                            if (!DDexists) {
                                childObject.setCustomStyleClass("ui_tree_node_green");

                            }

                            //  if field is mandatory (i.e. not nullable)...
                            if (BaseEntity.isFieldMandatory(fld)
                                    || (fld.getAnnotation(Translation.class) != null
                                    && BaseEntity.isFieldMandatory(BaseEntity.getClassField(fld.getDeclaringClass(),
                                    fld.getAnnotation(Translation.class).originalField())))) {
                                //  if corresponding DD exists in the database...
                                if (DDexists) {
                                    childObject.setCustomStyleClass("ui_tree_node_red");//red
                                } else {
                                    childObject.setCustomStyleClass("ui_tree_node_orange");//orange
                                }
                            }
                            //  set child node as 'leaf' because it has
                            //  no children yet

                            //  set child node user object

                            //  add 'childObject' to 'nodeObjects' for future reference

                            DefaultTreeNode addedNode = new DefaultTreeNode("folder", childObject, selectedNodeObject);
                            
                            DefaultTreeNode childEmptyNode = new DefaultTreeNode("folder", null, addedNode);
                            addedNode.setExpanded(false);
                            
                            nodeObjects.add(addedNode);

                        }
                        logger.debug("Returning");
                        return;
                    }
                } catch (Exception ex) {
                    logger.error("Exception thrown",ex);
                }

                if (baseEntity == null) {
                    System.out.println("baseEntity == null!");
                }
            }
        }
        logger.debug("Returning");
    }

    public void retrieveSelectedObject(TreeNode treeNode) {
logger.debug("Entering");
        for (DefaultTreeNode fieldNode : nodeObjects) {
            if (((DynamicNodeUserObject)fieldNode.getData()).getNodeExpression().equals(
                    ((DynamicNodeUserObject)treeNode.getData()).getNodeExpression())) {
                setSelectedNodeObject(fieldNode);
                bean.setDynamicSelectedNode(fieldNode);
                logger.debug("Returning");
                return;
            }
        }
    }

    public DefaultTreeNode getSelectedNodeObject() {
        return selectedNodeObject;
    }

    public void setSelectedNodeObject(DefaultTreeNode selectedNodeObject) {
        this.selectedNodeObject = selectedNodeObject;
    }
    
    DynamicNodeUserObject getSelectedNodeAsDynamic(){
        DynamicNodeUserObject returnedObject = (DynamicNodeUserObject) selectedNodeObject.getData();
        return  returnedObject;
    }

    /**
     * @return the bean
     */
    public OBackBean getBean() {
        return bean;
    }

    /**
     * @param bean the bean to set
     */
    public void setBean(SingleEntityBean bean) {
        this.bean = bean;
    }

    /**
     * @return the root
     */
    public DefaultTreeNode getRoot() {
        return root;
    }

    /**
     * @param root the root to set
     */
    public void setRoot(DefaultTreeNode root) {
        this.root = root;
    }
}
