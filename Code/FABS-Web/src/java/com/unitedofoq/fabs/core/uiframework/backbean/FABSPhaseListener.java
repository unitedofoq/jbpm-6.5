/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import org.primefaces.util.Constants;

/**
 *
 * @author mibrahim
 */
public class FABSPhaseListener implements PhaseListener {

    @Override
    public void afterPhase(PhaseEvent event) {
//        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void beforePhase(PhaseEvent event) {
        System.out.println("beforePhase:");
        FacesContext fc = event.getFacesContext();
        ExternalContext ec = fc.getExternalContext();
        if (event.getPhaseId() == PhaseId.RESTORE_VIEW) {
            if (ec.getRequestParameterMap().containsKey(Constants.PARTIAL_PROCESS_PARAM)
                    && !ec.getRequestParameterMap().get(Constants.PARTIAL_PROCESS_PARAM).equals("@all")) {
            }
            if (null != fc.getViewRoot()) {
                System.out.println("View ID:" + fc.getViewRoot().getViewId());
            } else {
                System.out.println("View is null!!");
            }
        }
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }
}
