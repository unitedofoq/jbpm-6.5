/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.backbean;

import javax.faces.application.ViewHandler;
import javax.faces.application.ViewHandlerWrapper;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

/**
 *
 * @author mibrahim
 */
public class SampleViewHandler extends ViewHandlerWrapper {

    private ViewHandler handler;
    @Override public UIViewRoot restoreView(FacesContext facesContext, String viewId) {
	/**
	 * {@link javax.faces.application.ViewExpiredException}. This happens only  when we try to logout from timed out pages.
	 */
	UIViewRoot root =null; 
	root = handler.restoreView(facesContext, viewId);
	if(root == null) {			
		root = createView(facesContext, viewId);
	}
	return root;
 }
    public SampleViewHandler(ViewHandler handler){
        super();
        this.handler = handler;
    }
   
    /* (non-Javadoc)
     * @see javax.faces.application.ViewHandlerWrapper#getWrapped()
     */
    @Override
    public ViewHandler getWrapped() {
        return handler;
    }

}
