package com.unitedofoq.fabs.core.uiframework.backbean;

import com.unitedofoq.fabs.bpm.model.status.TaskStatus;
import com.unitedofoq.fabs.core.content.AttachmentDTO;
import com.unitedofoq.fabs.core.content.EntityAttachmentServiceLocal;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.datatype.ODataTypeAttribute;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.EntityRequestStatusHistory;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import com.unitedofoq.fabs.core.entitysetup.EntityRequestStatus;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupService;
import com.unitedofoq.fabs.core.entitysetup.OEntityAction;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.filter.FilterDDRequiredException;
import com.unitedofoq.fabs.core.filter.FilterField;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.report.OReport;
import com.unitedofoq.fabs.core.report.customReports.CustomReport;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import static com.unitedofoq.fabs.core.uiframework.backbean.OBackBean.count;
import static com.unitedofoq.fabs.core.uiframework.backbean.OBackBean.liferayUsers;
import com.unitedofoq.fabs.core.uiframework.backbean.wizard.WizardPanel;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import com.unitedofoq.fabs.core.uiframework.portal.ScreenRenderInfo;
import com.unitedofoq.fabs.core.uiframework.screen.DimmedField;
import com.unitedofoq.fabs.core.uiframework.screen.FormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.OBackBeanException;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenAction;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenDTMappingAttr;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenFieldGroup;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenFieldValue;
import com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenDTMappingAttrDTO;
import com.unitedofoq.fabs.core.uiframework.screen.dto.TabularScreenDTO;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.validation.FABSException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EventObject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.el.MethodExpression;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlOutputLink;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.el.MethodBinding;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;
import javax.faces.event.ValueChangeEvent;
import javax.persistence.CascadeType;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.OneToOne;
import javax.persistence.OptimisticLockException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.component.ajaxstatus.AjaxStatus;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.commandlink.CommandLink;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.graphicimage.GraphicImage;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.inputtextarea.InputTextarea;
import org.primefaces.component.menuitem.MenuItem;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.overlaypanel.OverlayPanel;
import org.primefaces.component.panel.Panel;
import org.primefaces.component.password.Password;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.component.selectoneradio.SelectOneRadio;
import org.primefaces.component.separator.Separator;
import org.primefaces.component.spacer.Spacer;
import org.primefaces.component.tooltip.Tooltip;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.slf4j.LoggerFactory;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author arezk
 */
//Overview: This is the control class that is responsible for building form screens
@ManagedBean(name = "FormBean")
@ViewScoped
public class FormBean extends SingleEntityBean {

    final static org.slf4j.Logger logger = LoggerFactory.getLogger(FormBean.class);
    private OFunctionResult globalOFunctionResult = new OFunctionResult();

    EntityAttachmentServiceLocal entityAttachmentService = OEJB.lookup(EntityAttachmentServiceLocal.class);

    protected CommandButton claimButton;

    //this class was added by melad to help sorting screenFields in the group
    class ComparatorForFormScreenGroup implements Comparator<ScreenField> {

        @Override
        public int compare(ScreenField s1, ScreenField s2) {
            return s1.getSortIndex() - s2.getSortIndex();
        }
    }
    //this class was added by melad to help sorting ControlsGroup in the Form screen

    class ComparatorForFormControlsGroup implements Comparator<ScreenFieldGroup> {

        @Override
        public int compare(ScreenFieldGroup s1, ScreenFieldGroup s2) {
            return s1.getSortIndex() - s2.getSortIndex();
        }
    }
    List<String> fieldExpressions = new ArrayList<String>();
    protected BaseEntity loadedObject;
    private String alookupText;
    private List<BaseEntity> formLoadedList;
    private HtmlPanelGrid messageGrid;
    private boolean showTransFlds;
    private boolean isFormReady = false;

    public boolean isShowTransFlds() {
        return showTransFlds;
    }

    public void setShowTransFlds(boolean showTransFlds) {
        this.showTransFlds = showTransFlds;
    }

    public FormBean() {
    }

    boolean hasAttachments;

    public boolean isHasAttachments() {
        return hasAttachments;
    }

    public void setHasAttachments(boolean hasAttachments) {
        this.hasAttachments = hasAttachments;
    }
    int noOfAttachments;

    public int getNoOfAttachments() {
        return noOfAttachments;
    }

    public void setNoOfAttachments(int noOfAttachments) {
        this.noOfAttachments = noOfAttachments;
    }

    @Override
    protected boolean onInit(Map<String, String> requestParams) {
        logger.debug("Entering");
        if (!super.onInit(requestParams)) {
            logger.debug("Returning with false");
            return false;
        }
        try {
            if (dataMessage != null && dataMessage.getODataType() != null) // DataMessage is not null. It's allowed to have null datamessage for
            // inherited screens
            {
                if (dataMessage.getODataType().getDbid() == ODataType.DTDBID_VOID
                        && mode == OScreen.SM_NOMODE // mode not set yet
                        ) {
                    mode = OScreen.SM_ADD;
                }
            }
            logger.debug("Returning with true");
            return true;

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning with False");
            return false;

        } finally {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="Simple Fields, Setters & Getters ">
    public HtmlPanelGrid getMessageGrid() {
        return buildMessagePanel();
    }

    public void setMessageGrid(HtmlPanelGrid messageGrid) {
        this.messageGrid = messageGrid;
    }

    public List<BaseEntity> getLoadedList() {
        return formLoadedList;
    }

    public void setLoadedList(List<BaseEntity> loadedList) {
        this.formLoadedList = loadedList;
    }
    HtmlPanelGrid entityRequestPanelGrid;

    public void setEntityRequestPanelGrid(HtmlPanelGrid entityRequestPanelGrid) {
        this.entityRequestPanelGrid = entityRequestPanelGrid;
    }
    SelectOneMenu menu;

    public SelectOneMenu getMenu() {
        return menu;
    }

    public void setMenu(SelectOneMenu menu) {
        this.menu = menu;
    }
    private String selected;

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public BaseEntity getLoadedObject() {
        return loadedObject;
    }

    public void setLoadedObject(BaseEntity loadedObject) {
        this.loadedObject = loadedObject;
    }
    // </editor-fold>

    /**
     * Creates a new instance of the {@link SingleEntityBean#getOactOnEntity() }
     * entity, and {@link #applyHardCodedInitialization(BaseEntity) }
     *
     * @param screenFieldExpressions
     * @return true, successful <br>false, in case of exception (logged)
     */
    protected boolean createNewLoadedObject(List<String> screenFieldExpressions) {
        try {
            loadedObject = (BaseEntity) Class.forName(getOactOnEntity().getEntityClassPath()).newInstance();

            // <editor-fold defaultstate="collapsed" desc="fill fieldExpressions, initialize using DD Default Values">
            for (ScreenField screenField : getCurrentScreen().getScreenFields()) {
                String fieldExp = screenField.getFieldExpression();

                if (screenField.getDd().getDefaultValue() != null
                        && !"".equals(screenField.getDd().getDefaultValue())) // DD Default Value initialization is required
                {
                    String defaultValue = screenField.getDd().getDefaultValue();

                    if (screenField.getDd().getControlType().getDbid() == DD.CT_TEXTFIELD
                            || screenField.getDd().getControlType().getDbid() == DD.CT_TEXTAREA) // Field is text
                    {
                        if (FramworkUtilities.isFloat(defaultValue)) {
                            FramworkUtilities.setValueInEntity(loadedObject, fieldExp,
                                    BigDecimal.valueOf(Float.valueOf(defaultValue.toString())));
                        } else {
                            // Default value is not number, so, it's text
                            FramworkUtilities.setValueInEntity(loadedObject, fieldExp,
                                    defaultValue);
                        }
                    } else if (screenField.getDd().getControlType().getDbid() == DD.CT_CALENDAR) { // Field is calendar
                        if (defaultValue != null && !defaultValue.equals("") && defaultValue.equalsIgnoreCase("today")) {
                            FramworkUtilities.setValueInEntity(loadedObject, fieldExp, timeZoneService.getUserCDT(loggedUser));
                        } else if (defaultValue != null && !defaultValue.equals("")) {
                            if (isValidDateFormat(defaultValue, screenField.getDd().getCalendarPattern().getValueTranslated())) {
                                Date date = getDefaultDate(screenField.getDd().getCalendarPattern().getValueTranslated(), defaultValue);
                                FramworkUtilities.setValueInEntity(loadedObject, fieldExp, date);
                            } else {
                                FramworkUtilities.setValueInEntity(loadedObject, fieldExp, null);
                            }
                        } else {
                            FramworkUtilities.setValueInEntity(loadedObject, fieldExp, null);
                        }
                    } else if (screenField.getDd().getControlType().getDbid() == DD.CT_CHECKBOX
                            && ("1".equals(defaultValue)
                            || "true".equals(defaultValue))) // Field is checkbox with true value
                    {
                        FramworkUtilities.setValueInEntity(loadedObject, fieldExp, true);
                    }
                } else // DD Default Value initialization is NOT required
                {
                    fieldExpressions.add(fieldExp);
                }
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Intialize new instance using fields expression">
            loadedObject.NewFieldsInstances(fieldExpressions);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Intialize new instance using ScreenInput forInitialization">
            if (currentScreenInput != null && dataMessage != null) {
                for (ScreenDTMappingAttrDTO mapAttr : currentScreenInput.getAttributesMapping()) {
                    // Note that, ODataType.DTDBID_PARENTDBID natively supported input
                    if (!mapAttr.isForInitialization()) {
                        continue;
                    }
                    loadedObject.NewFieldsInstances(Collections.singletonList(mapAttr.getFieldExpression()));
                    FramworkUtilities.setValueInEntity(loadedObject, mapAttr.getFieldExpression(),
                            dataMessage.getData().get(0), true);
                    //FIXME: check return if has errors
                    entityValueIntializationListener(loadedObject, mapAttr.getFieldExpression(),
                            dataMessage.getData().get(0));
                }
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Intialize new instance if parentDBID passed">
            if ((dataMessage.getODataType().getDbid() == ODataType.DTDBID_PARENTDBID
                    && headerObject != null)
                    || (dataMessage.getData().size() == 3
                    && String.valueOf(dataMessage.getData().get(2)).equals("OrgChart"))) {
                String parentFieldName = null;
                // <editor-fold defaultstate="collapsed" desc="Fill parentFieldName, condier multiparents">
                List<String> parentFieldNames = BaseEntity.
                        getParentEntityFieldName(getOactOnEntity().getEntityClassPath());
                if (parentFieldNames.size() == 1) {
                    // Only one parent found
                    parentFieldName = parentFieldNames.get(0);

                } else if (parentFieldNames.size() > 1) {
                    // More than one parent found
                    // Decide the type of the passed parent
                    OFunctionResult parentOFR;// = null;
                    if (dataMessage.getData().size() == 3
                            && String.valueOf(dataMessage.getData().get(2)).equals("OrgChart")) {
                        parentOFR = uiFrameWorkFacade
                                .getParentFromParentDBIDDTO(getOactOnEntity(),
                                        (Long) dataMessage.getData().get(0), loggedUser);
                    } else {
                        parentOFR = uiFrameWorkFacade
                                .getParentFromParentDBIDDTO(getOactOnEntity(),
                                        (Long) dataMessage.getData().get(0), loggedUser);
                    }
                    if (!parentOFR.getErrors().isEmpty()) {
                        messagesPanel.addMessages(parentOFR);
                        return false;
                    }
                    if (parentOFR.getReturnValues().isEmpty()) {
                        // No parent found matches the DBID
                        logger.warn("Parent DBID Not Found In Any Of The Parents Tables");
                        // FIXME: meaningful error message
                        messagesPanel.addErrorMessage(
                                usrMsgService.getUserMessage("SystemInternalError", systemUser));
                        return false;
                    }
                    parentFieldName = (String) parentOFR.getReturnValues().get(1);  //parent field name

                }
                // </editor-fold>
                if (dataMessage.getData().size() == 3
                        && String.valueOf(dataMessage.getData().get(2)).equals("OrgChart")) {
                    BaseEntity orgEmployee = oem.loadEntity("Employee",
                            Collections.singletonList("dbid = " + dataMessage.getData().get(0)), null, loggedUser);

                    loadedObject.NewFieldsInstances(Collections.singletonList(parentFieldName));
                    FramworkUtilities.setValueInEntity(loadedObject, parentFieldName, orgEmployee, true);
                    //FIXME: check return if has errors
                    entityValueIntializationListener(loadedObject, parentFieldName, orgEmployee);
                } else {
                    FramworkUtilities.setValueInEntity(loadedObject, parentFieldName, headerObject);
                    applyDepencyInitialization(loadedObject, headerObject, parentFieldName);
                    //FIXME: check return if has errors
                    entityValueIntializationListener(loadedObject, parentFieldName, headerObject);
                }
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Apply Screen Filter Initialization">
            if (getCurrentScreen().getScreenFilter() != null
                    && !getCurrentScreen().getScreenFilter().isInActive()
                    && getCurrentScreen().getScreenFilter().isInitialize()) {
                List<FilterField> filterFields = getCurrentScreen().getScreenFilter().getFilterFields();
                for (FilterField filterField : filterFields) {
                    if (filterField.isInActive()) {
                        continue;
                    }

                    if (filterField.isFromTo()) // Not supported
                    {
                        continue;
                    }

                    String value = filterField.getFieldValue();
                    if (filterField.getFieldDD().getControlType().getDbid() == DD.CT_DROPDOWN
                            || filterField.getFieldDD().getControlType().getDbid() == DD.CT_LOOKUPSCREEN) {
                        try {
                            BaseEntity currentObject = null;

                            String objectFieldExpression = filterField.getFieldExpression().
                                    substring(0, filterField.getFieldExpression()
                                            .indexOf(getLookupFldExpression(filterField.getFieldExpression())) - 1);

                            List<String> fldExps = new ArrayList<String>();

                            for (int i = 0; i < fieldExpressions.size(); i++) {
                                if (fieldExpressions.get(i).startsWith(objectFieldExpression + ".")) {
                                    fldExps.add(fieldExpressions.get(i).substring(objectFieldExpression.length() + 1));
                                }
                            }

                            currentObject = getLookupBaseEntity(filterField.getFieldExpression(), value, fldExps);

                            BaseEntity.setValueInEntity(loadedObject, objectFieldExpression, currentObject);

                        } catch (Exception ex) {
                            logger.error("Exception thrown", ex);
                        }

                    } else if (filterField.getFieldDD().getControlType().getDbid() == DD.CT_CALENDAR) {
                        if (value != null && !value.equals("")) {
                            try {
                                BaseEntity.setValueInEntity(loadedObject,
                                        getLookupFldExpression(filterField.getFieldExpression()),
                                        FilterField.dateFormat.parse(value));
                            } catch (Exception ex) {
                                logger.error("Exception thrown", ex);
                            }
                        }
                    } else {
                        BaseEntity.setValueInEntity(loadedObject,
                                getLookupFldExpression(filterField.getFieldExpression()), value);
                    }
                }
            }
            // </editor-fold>

            applyFunctionInitialization(loadedObject);
            applyHardCodedInitialization(loadedObject);
            logger.trace("Returning with True");
            return true;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Returning with False");
            return false;
        }
    }

    @Override
    protected boolean processInput() {
        logger.trace("Entering");
        invalidInputMode = false;
        if (!super.processInput()) {
            if (invalidInputMode) {
                List<String> screenFieldExpressions = new ArrayList<String>();
                // <editor-fold defaultstate="collapsed" desc="Fill screenFieldExpressions from currentScreen">
                Iterator screenFieldsIterator = getCurrentScreen().getScreenFields().iterator();
                while (screenFieldsIterator.hasNext()) {
                    screenFieldExpressions.add(
                            ((ScreenField) screenFieldsIterator.next()).getFieldExpression());
                }
                // </editor-fold>
                logger.warn("Screen Input Null Value");
                dataFilter.clear();
                try {
                    currentScreen = (OScreen) textTranslationServiceRemote.loadEntityTranslation(getCurrentScreen(), loggedUser);
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }
                onloadJSStr += "top.FABS.Portlet.setTitle({portletId:'" + getPortletInsId()
                        + "', title:'" + getCurrentScreen().getHeaderTranslated().replace("\\", "\\\\").replace("\'", "\\'") + "'});";
            }
            logger.trace("Returning with False");
            return false;
        }
        OFunctionResult oFR = new OFunctionResult();
        List<String> screenFieldExpressions = new ArrayList<String>();
        // <editor-fold defaultstate="collapsed" desc="Fill screenFieldExpressions from currentScreen">
        Iterator screenFieldsIterator = getCurrentScreen().getScreenFields().iterator();
        while (screenFieldsIterator.hasNext()) {
            screenFieldExpressions.add(
                    ((ScreenField) screenFieldsIterator.next()).getFieldExpression());
        }
        // </editor-fold>

        if (currentScreenInput == null || dataMessage == null || dataMessage.getData() == null) {
            // Screen Input Null Value
            logger.warn("Screen Input Null Value");
            createNewLoadedObject(screenFieldExpressions);
            logger.trace("Returning with True");
            return true;
        } else {
            // Screen Input is valid
            if (portalMode && showManagePortlePage) {
                mode = OScreen.SM_ADD;
            } else if (c2AMode && getScreenParam(MODE_VAR) == null) {
                // In C2A Mode with no MODE_VAR passed
                //TODO: comment, what is the case of having MODE_VAR null?
                // Set to edit by default
                mode = OScreen.SM_EDIT;
            }
        }
        dataFilter.clear();

        List<ScreenDTMappingAttrDTO> scrInMapAttrs = null;  // currentScreenInput mapping attributes
        List<ODataTypeAttribute> odmDTAttrs = null;         // dataMessage DataType Attributes
        // <editor-fold defaultstate="collapsed" desc="Fill curInputMapAttrs & odmDTAttrs">
        try {
            scrInMapAttrs = currentScreenInput.getAttributesMapping();   // No need
            // to check inActive records as they are supposed to be checked
            // in getScreenObject
            odmDTAttrs = oem.loadEntityList(ODataTypeAttribute.class.getSimpleName(),
                    Collections.singletonList("oDataType.dbid=" + dataMessage.getODataType().getDbid()),
                    null, null, oem.getSystemUser(loggedUser));
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        // </editor-fold>

        if (loadedObject == null && c2AMode && !dataMessage.getData().isEmpty()
                && dataMessage.getData().get(0) instanceof BaseEntity
                && ((BaseEntity) dataMessage.getData().get(0)).getDbid() == 0) {
            invalidInputMode = true;
        } else if (loadedObject == null && c2AMode && dataMessage.getData().isEmpty()
                && !dataMessage.getODataType().getName().equalsIgnoreCase("voiddt")) {
            invalidInputMode = true;
        }
        if (invalidInputMode) {
            logger.trace("Returning with True");
            return true;
        }
        int counter = 0;
        for (int idx = 0; idx < scrInMapAttrs.size(); idx++) {
            if (scrInMapAttrs.get(idx).getFieldExpression()
                    .equalsIgnoreCase("this")) {
                counter++;
                try {
                    Object value = null;
                    // If no data type attribute found, continue normally
                    if (scrInMapAttrs.get(idx).getOdataTypeAttributeDBID() == 0
                            || (!odmDTAttrs.isEmpty()
                            && scrInMapAttrs.get(idx).getOdataTypeAttributeDBID() == odmDTAttrs.get(idx).getDbid())) {
                        String attributeExpression = scrInMapAttrs.get(idx).getAttributeExpression();
                        if (attributeExpression == null || attributeExpression.equalsIgnoreCase("")
                                || attributeExpression.equalsIgnoreCase("this")) {
                            value = dataMessage.getData().get(idx);
                        } else {
                            value = getValueFromEntity(dataMessage.getData().get(idx), attributeExpression);
                        }
                    } else {
                        if (counter == scrInMapAttrs.size()) {
                            oFR.addError(usrMsgService.getUserMessage("InvalidScrInDTAttr",
                                    Collections.singletonList(scrInMapAttrs.get(idx).getOdataTypeAttributeDescription()),
                                    systemUser));
                        }
                    }
                    ArrayList<String> conditions = new ArrayList<String>();
                    conditions.add("dbid = " + ((BaseEntity) value).getDbid());
                    loadedObject = loadObject(getOactOnEntity().getEntityClassName(),
                            conditions,
                            screenFieldExpressions, loggedUser);
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                    oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
                    messagesPanel.clearAndDisplayMessages(oFR);
                }
                break;

            } else if (scrInMapAttrs.get(idx).getFieldExpression()
                    .equalsIgnoreCase("requested")) {
                setStatusButtons(dataMessage.getData().get(idx).toString());
            }
            if (scrInMapAttrs.get(idx).isForFilter()) {
                if (!dataMessage.getData().isEmpty()) {
                    Object value = null;
                    int k;
                    int size;
                    if (odmDTAttrs.isEmpty()) {
                        size = 1;
                    } else {
                        size = odmDTAttrs.size();
                    }
                    counter = 0;
                    for (k = 0; k < size; k++) {
                        counter++;
                        if (scrInMapAttrs.get(idx).getOdataTypeAttributeDBID() == 0
                                || scrInMapAttrs.get(idx).getOdataTypeAttributeDBID() == odmDTAttrs.get(k).getDbid()) {
                            String attributeExpression = scrInMapAttrs.get(idx).getAttributeExpression();
                            if (attributeExpression == null || attributeExpression.equalsIgnoreCase("")
                                    || attributeExpression.equalsIgnoreCase("this")) {
                                value = dataMessage.getData().get(k);
                            } else {
                                value = getValueFromEntity(dataMessage.getData().get(k), attributeExpression);
                            }
                            break;
                        } else {
                            if (counter == size) {
                                oFR.addError(usrMsgService.getUserMessage("InvalidScrInDTAttr",
                                        Collections.singletonList(scrInMapAttrs.get(idx).getOdataTypeAttributeDescription()),
                                        systemUser));
                            }
                        }
                    }

                    if (value instanceof BaseEntity) {
                        dataFilter.add(scrInMapAttrs.get(idx).getFieldExpression()
                                + ".dbid = " + ((BaseEntity) value).getDbid());

                    } else if (value instanceof String) {
                        dataFilter.add(scrInMapAttrs.get(idx).getFieldExpression()
                                + " = '" + value + "'");
                    } else {
                        dataFilter.add(scrInMapAttrs.get(idx).getFieldExpression()
                                + " = " + value);
                    }
                }
            }
        }

        if (compositeInputRequired) {
            for (OScreenCompositeInput compInput : compositeInputs) {
                String generatedFilterExp = generateFilterExpression(compInput.attr, compInput.odm);
                if (generatedFilterExp != null) {
                    dataFilter.add(generatedFilterExp);
                }
            }
        }

        // Load or create entity
        if (getCurrentScreen().getScreenFilter() != null
                && !getCurrentScreen().getScreenFilter().isInActive() && !processMode) {
            try {
                dataFilter.addAll(filterServiceBean.getFilterConditions(getCurrentScreen().getScreenFilter(),
                        getUserSessionInfo(), loggedUser));
            } catch (FilterDDRequiredException ex) {
                logger.error("Exception thrown", ex);
            }
        }

        //TODO need tunning - put inside vlidateInputSetMapping
        if (processMode && dataMessage.getODataType().getName().equalsIgnoreCase("dbid")) {
            dataFilter.clear();
            dataFilter.add("dbid = " + dataMessage.getData().get(0));
        }
        for (int idx = 0; idx < scrInMapAttrs.size(); idx++) {
            if (!scrInMapAttrs.get(idx).getFieldExpression()
                    .equalsIgnoreCase("this")) {
                // object not loaded (not found)
                if (mode == OScreen.SM_ADD) {
                    createNewLoadedObject(screenFieldExpressions);
                } else {
                    // <editor-fold defaultstate="collapsed" desc="loadObject with dataFilter">
                    try {
                        loadedObject = loadObject(getOactOnEntity().getEntityClassName(),
                                dataFilter, screenFieldExpressions, getLoggedUser());

                    } catch (Exception ex) {

                        if (ex.getCause() instanceof NonUniqueResultException) {
                            oFR.addError(usrMsgService.getUserMessage("NonUniqueResultFound", systemUser));
                            // <editor-fold defaultstate="collapsed" desc="Get the first element into loadedObject">
                            try {
                                List entityList;
                                entityList = oem.loadEntityList(getOactOnEntity().getEntityClassName(),
                                        dataFilter, screenFieldExpressions, null, getLoggedUser());
                                if (entityList != null && !entityList.isEmpty()) {
                                    loadedObject = (BaseEntity) entityList.get(0);
                                }
                            } catch (Exception ex1) {
                                logger.error("Exception thrown", ex1);
                                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex1);
                            }
                            // </editor-fold>
                        } else {
                            logger.error("Exception thrown", ex);
                        }
                        messagesPanel.clearAndDisplayMessages(oFR);
                    }
                    // </editor-fold>
                }
            }
        }

        if (loadedObject == null && !dataFilter.isEmpty() && (mode == OScreen.SM_EDIT || processMode)) {
            try {
                List entityList;
                entityList = oem.loadEntityList(getOactOnEntity().getEntityClassName(),
                        dataFilter, screenFieldExpressions, null, getLoggedUser());
                if (entityList != null && !entityList.isEmpty()) {
                    loadedObject = (BaseEntity) entityList.get(0);
                }
                if (entityList.size() > 1) {
                    logger.warn("Form Data More Than One Record");
                }
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
        }

        if (c2AMode
                && mode == OScreen.SM_EDIT
                && loadedObject == null) {
            // Screen in Edit Mode for C2A and object not loaded (not found)
            if (createNewLoadedObject(screenFieldExpressions)) {
                mode = OScreen.SM_ADD;
            }
        }

        if (mode == OScreen.SM_ADD
                && headerObject != null // Header object passed
                && loadedObject.getDbid() == 0 // Object for add new, FIXME: why checking if mode = ADD
                ) // Screen in add mode with headerObject passed
        {
            // <editor-fold defaultstate="collapsed" desc="Init parent value in object, applyDepencyInitialization">
            try {
                // Decide the type of the passed parent if in headerObject
                OFunctionResult parentOFR = uiFrameWorkFacade
                        .getParentFromParentDBIDDTO(getOactOnEntity(), headerObject.getDbid(), loggedUser);
                if (!parentOFR.getErrors().isEmpty()) {
                    // FIXME: logError & user message
                } else if (parentOFR.getReturnValues().isEmpty()) {
                    // No parent found matches the DBID
                } else {
                    // Parent found, get its field name
                    String parentFieldName = (String) parentOFR.getReturnValues().get(1);
                    if (BaseEntity.getValueFromEntity(loadedObject, parentFieldName) != null) {
                        FramworkUtilities.setValueInEntity(loadedObject, parentFieldName, headerObject);
                        applyDepencyInitialization(loadedObject, headerObject, parentFieldName);
                    }
                }
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
            // </editor-fold>
        }

        if (mode == OScreen.SM_ADD || currentScreenInput != null) {
            for (int idx = 0; idx < scrInMapAttrs.size(); idx++) {
                if (scrInMapAttrs.get(idx).isForInitialization()) {
                    if (scrInMapAttrs.get(idx).isForInitialization()) {
                        try {
                            if (null != BaseEntity.getValueFromEntity(loadedObject,
                                    scrInMapAttrs.get(idx).getFieldExpression())) {
                                continue;
                            }
                        } catch (Exception ex) {
                            logger.error("Exception thrown", ex);
                        }
                    }
                    loadedObject.NewFieldsInstances(
                            Collections.singletonList(
                                    scrInMapAttrs.get(idx).getFieldExpression()));

                    FramworkUtilities.setValueInEntity(loadedObject,
                            scrInMapAttrs.get(idx).getFieldExpression(),
                            dataMessage.getData().get(idx), true);

                    for (String fldExp : fieldExpressions) {
                        if (fldExp.contains(".") && fldExp.contains("Translated")) {
                            try {
                                String entityToTransFldExpr = fldExp.substring(0, fldExp.lastIndexOf("."));
                                BaseEntity toBeTranslated = (BaseEntity) BaseEntity.getValueFromEntity(loadedObject, entityToTransFldExpr);
                                textTranslationServiceRemote.loadEntityTranslation(toBeTranslated, loggedUser);
                            } catch (Exception ex) {
                                logger.error("Exception thrown", ex);
                            }
                        }
                    }

                    applyDepencyInitialization(loadedObject, dataMessage.getData().get(idx),
                            scrInMapAttrs.get(idx).getFieldExpression());
                }
            }

            if (compositeInputRequired) {
                if (areAllCompositeInputsAvailable()) {
                    initEntityWithComposedInput(loadedObject);
                }
            }
        }

        if (mode == OScreen.SM_ADD && loadedObject == null) {
            createNewLoadedObject(screenFieldExpressions);
        }

        if (loadedObject != null) {
            for (String fldExp : fieldExpressions) {
                if (fldExp.contains(".")) {
                    loadAttribute(fldExp);
                }
            }
            if (loadedObject.getDbid() == 0) {
                applyHardCodedInitialization(loadedObject);
            }

        } else {
            isFormReady = true;

        }

        logger.trace("Returning with True");
        return true;

        // apply hard coded initialization if the loadedObject is new one
    }

    protected String generateFilterExpression(ScreenDTMappingAttr attr, ODataMessage odm) {
        logger.debug("Entering");
        List<ODataTypeAttribute> dTAttrs = null;
        try {
            dTAttrs = oem.loadEntityList(ODataTypeAttribute.class.getSimpleName(),
                    Collections.singletonList("oDataType.dbid=" + odm.getODataType().getDbid()),
                    null, null, oem.getSystemUser(loggedUser));
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        if (attr.isForFilter()) {
            if (!odm.getData().isEmpty()) {
                Object value = null;
                if (attr.getOdataTypeAttribute() != null) {
                    int k;
                    for (k = 0; k < dTAttrs.size(); k++) {
                        if (attr.getOdataTypeAttribute().getDbid() == dTAttrs.get(k).getDbid()) {
                            if (attr.getAttributeExpression() == null || attr.getAttributeExpression().equalsIgnoreCase("")
                                    || attr.getAttributeExpression().equalsIgnoreCase("this")) {
                                value = odm.getData().get(k);
                            } else {
                                value = getValueFromEntity(odm.getData().get(k), attr.getAttributeExpression());
                            }
                            break;
                        }
                    }
                } else {
                    value = odm.getData().get(0);
                }
                if (value instanceof BaseEntity) {
                    logger.debug("Returning");
                    return attr.getFieldExpression()
                            + ".dbid = " + ((BaseEntity) value).getDbid();
                } else if (value instanceof String) {
                    logger.debug("Returning");
                    return attr.getFieldExpression()
                            + " = '" + value + "'";
                } else {
                    logger.debug("Returning");
                    return attr.getFieldExpression()
                            + " = " + value;
                }
            }
        }
        logger.debug("Returning with Null");
        return null;
    }

    //<editor-fold defaultstate = "collapsed" desc="Entity Request Panel">
    public HtmlPanelGrid getEntityRequestPanelGrid() {
        // <editor-fold defaultstate="collapsed" desc="Log">
        Long timeBefore = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>
        HtmlPanelGrid requestPanel = null;
        requestPanel = buildEntityRequestPanel();
        return requestPanel;
    }

    private HtmlPanelGrid buildEntityRequestPanel() {
        logger.debug("Entering");
        if (!processMode) {
            logger.debug("Returning");
            return new HtmlPanelGrid();
        }
        // if (!taskInfo.getAeTTask().isRequestStatus()) {
        if (!processingTask.isRequestStatus()) {
            logger.debug("Returning");
            return new HtmlPanelGrid();
        }
        DataTable requestDataTable = new DataTable();
        requestDataTable.setId("requestTable");
        if (loadedObject != null) {
            currentStatus = loadedObject.getEntityStatus();
        }
        DD currentStatusName = ddService.getDD("$$RequestForm_currentStatus", getLoggedUser());
        DD currentStatusCreateor = ddService.getDD("$$RequestForm_currentStatusCreateor", getLoggedUser());
        DD currentStatusCreationTime = ddService.getDD("$$RequestForm_currentStatusCreationTime", getLoggedUser());
        DD currentStatusNote = ddService.getDD("$$RequestForm_currentStatusNote", getLoggedUser());
        DD nextStatusNote = ddService.getDD("$$RequestForm_nextStatusNote", getLoggedUser());

        HtmlPanelGrid entityRequestGrid = new HtmlPanelGrid();

        HtmlPanelGrid historyRequestGrid = new HtmlPanelGrid();
        HtmlPanelGrid creatorGrid = new HtmlPanelGrid();
        HtmlPanelGrid creationTimeGrid = new HtmlPanelGrid();
        HtmlPanelGrid currentNoteGrid = new HtmlPanelGrid();
        HtmlPanelGrid nextNoteGrid = new HtmlPanelGrid();

        historyRequestGrid.setId("histortGrid_" + getCurrentScreen().getName());
        creatorGrid.setId("creatorGrid_" + getCurrentScreen().getName());
        creationTimeGrid.setId("creationTimeGrid_" + getCurrentScreen().getName());
        currentNoteGrid.setId("currentNoteGrid_" + getCurrentScreen().getName());
        nextNoteGrid.setId("nextNoteGrid_" + getCurrentScreen().getName());

        historyRequestGrid.setColumns(3);
        creatorGrid.setColumns(2);
        creationTimeGrid.setColumns(2);
        currentNoteGrid.setColumns(2);
        nextNoteGrid.setColumns(2);

        entityRequestGrid.setId("entityRequestGrid" + getCurrentScreen().getName());

        Separator separator = new Separator();
        separator.setId(getFacesContext().getViewRoot().createUniqueId() + "EntityStatusSeparator");
        separator.setStyle("width:100%;height:1px");
        entityRequestGrid.getChildren().add(separator);

        if (currentStatus != null) {

            CommandButton browsHistory = new CommandButton();
            browsHistory.setImage("/resources/browse.ico");
            browsHistory.setId("browsHistoryBTN");
            browsHistory.setActionExpression(exFactory.createMethodExpression(elContext,
                    "#{" + getBeanName() + ".browsFormRequestHistory}", String.class, new Class[0]));

            //Create Current Status Label
            OutputLabel currentStautsLBL = new OutputLabel();
            currentStautsLBL.setStyleClass("OTMSlabel");
            currentStautsLBL.setStyle("color:blue");
            currentStautsLBL.setId(getCurrentScreen().getName() + "_currentStatusLBL");
            currentStautsLBL.setValue(currentStatusName.getLabel());

            //Create Current Status Label Value
            OutputLabel currentStautsLBLVal = new OutputLabel();
            currentStautsLBLVal.setStyleClass("OTMSlabel");
            currentStautsLBLVal.setStyle("color:blue");
            currentStautsLBLVal.setId(getCurrentScreen().getName() + "_currentStatusValueLBL");
            currentStautsLBLVal.setValue(currentStatus.getName());

            historyRequestGrid.getChildren().add(currentStautsLBL);
            historyRequestGrid.getChildren().add(currentStautsLBLVal);
            historyRequestGrid.getChildren().add(browsHistory);
            entityRequestGrid.getChildren().add(historyRequestGrid);

            //Create Current Status Creator Label
            OutputLabel creatorLBL = new OutputLabel();
            creatorLBL.setStyleClass("OTMSlabel");
            creatorLBL.setStyle("color:blue");
            creatorLBL.setId(getCurrentScreen().getName() + "_creatorLBL");
            creatorLBL.setValue(currentStatusCreateor.getLabel());
            creatorGrid.getChildren().add(creatorLBL);

            List<String> conditions = new ArrayList<String>();
            conditions.add("entityClassPath like '%" + getOactOnEntity().getEntityClassPath() + "%'");
            conditions.add("entityDBID = " + loadedObject.getDbid());
            conditions.add("newStatus like '%" + currentStatus.getName() + "%'");
            EntityRequestStatusHistory requestStatusHistory = null;
            try {
                requestStatusHistory = (EntityRequestStatusHistory) oem.loadEntity(
                        "EntityRequestStatusHistory", conditions, null, getLoggedUser());
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }

            //Create Current Status Creator Value Label
            OutputLabel creatorLBLVal = new OutputLabel();
            creatorLBLVal.setStyleClass("OTMSlabel");
            creatorLBLVal.setStyle("color:blue");
            creatorLBLVal.setId(getCurrentScreen().getName() + "_currentStatusCreatorValueLBL");
            creatorLBLVal.setValue((requestStatusHistory != null)
                    ? requestStatusHistory.getRequester().getLoginName() : "");
            creatorGrid.getChildren().add(creatorLBLVal);
            entityRequestGrid.getChildren().add(creatorGrid);

            //Create Current Status Creation Label
            OutputLabel creationTimeLBL = new OutputLabel();
            creationTimeLBL.setStyleClass("OTMSlabel");
            creationTimeLBL.setStyle("color:blue");
            creationTimeLBL.setId(getCurrentScreen().getName() + "_currentStatusCreationTimeLBL");
            creationTimeLBL.setValue(currentStatusCreationTime.getLabel());

            //Create Current Status Creation Label Value
            OutputLabel creationTimeLBLVal = new OutputLabel();
            creationTimeLBLVal.setStyleClass("OTMSlabel");
            creationTimeLBLVal.setStyle("color:blue");
            creationTimeLBLVal.setId(getCurrentScreen().getName() + "_currentStatusCreationTimeValueLBL");
            creationTimeLBLVal.setValue(requestStatusHistory.getActionTime().toLocaleString());

            creationTimeGrid.getChildren().add(creationTimeLBL);
            creationTimeGrid.getChildren().add(creationTimeLBLVal);
            entityRequestGrid.getChildren().add(creationTimeGrid);

            //Create Current Status Note Label
            OutputLabel currentNoteLBL = new OutputLabel();
            currentNoteLBL.setStyleClass("OTMSlabel");
            currentNoteLBL.setStyle("color:blue");
            currentNoteLBL.setId(getCurrentScreen().getName() + "_currentStatusNoteLBL");
            currentNoteLBL.setValue(currentStatusNote.getLabel());
            currentNoteGrid.getChildren().add(currentNoteLBL);

            //Create Current Status Note Label Value
            OutputLabel currentNoteLBLVal = new OutputLabel();
            currentNoteLBLVal.setStyleClass("OTMSlabel");
            currentNoteLBLVal.setStyle("color:blue");
            currentNoteLBLVal.setId(getCurrentScreen().getName() + "_currentStatusNoteValueLBL");
            currentNoteLBLVal.setValue(loadedObject.getRequestNote());
            currentNoteGrid.getChildren().add(currentNoteLBLVal);
            entityRequestGrid.getChildren().add(currentNoteGrid);

        }
        //Create Next Status Note Label
        OutputLabel nextNoteLBL = new OutputLabel();
        nextNoteLBL.setStyleClass("OTMSlabel");
        nextNoteLBL.setStyle("color:blue");
        nextNoteLBL.setId(getCurrentScreen().getName() + "_nextStatusNoteLBL");
        nextNoteLBL.setValue(nextStatusNote.getLabel());
        nextNoteGrid.getChildren().add(nextNoteLBL);

        //create Next Status Note Control
        InputTextarea inputTextarea = new InputTextarea();
        inputTextarea.setStyle("width:500;height:40px");
        inputTextarea.setRows(2);
        inputTextarea.setStyleClass("OTMSTextAreaBox");
        if (loadedObject != null) {
            loadedObject.setRequestNote("");
        }
        inputTextarea.setValueExpression("value",
                exFactory.createValueExpression(elContext, "#{" + getBeanName() + ".loadedObject != null?"
                        + getBeanName() + ".loadedObject.requestNote:''"
                        + "}", String.class));

        nextNoteGrid.getChildren().add(inputTextarea);
        entityRequestGrid.getChildren().add(nextNoteGrid);
        logger.debug("Returning");
        return entityRequestGrid;
    }

    public String browsFormRequestHistory() {
        logger.debug("Entering");
        List<String> conditions = new ArrayList<String>();
        conditions.add("name = 'DBID'");
        ODataType objectDataType;
        ODataMessage objectDataMessage = null;
        try {
            objectDataType = dataTypeService.loadODataType("DBID", loggedUser);
            objectDataMessage = (ODataMessage) constructOutput(objectDataType,
                    loadedObject).getReturnedDataMessage();
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        String scrnInstId = generateScreenInstId("EntityRequestHistory");
        SessionManager.setScreenSessionAttribute(scrnInstId, OPortalUtil.MESSAGE, objectDataMessage);
        openScreen("EntityRequestHistory", scrnInstId);
        logger.debug("Entering with Null");
        return null;
    }
    //</editor-fold>

    @Override
    public HtmlPanelGrid buildInnerScreen() {
        logger.debug("Entering");
        htmlPanelGrid = new HtmlPanelGrid();
        htmlPanelGrid.setStyleClass("mainTable");
        try {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.trace("Building Form Screen");
            Long entryTime = (Calendar.getInstance()).getTimeInMillis();
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Method Fields">
            HtmlPanelGrid buttonsPanel = new HtmlPanelGrid();

            htmlPanelGrid.setId(getFacesContext().getViewRoot().createUniqueId() + portletId + "FormGeneralPanel");
            buttonsPanel.setId(getFacesContext().getViewRoot().createUniqueId() + portletId + "FormButtonsPanel");

            htmlPanelGrid.setColumns(1);
            buttonsPanel.setColumns(4 + getCurrentScreen().getScreenActions().size());
            htmlPanelGrid.setCellpadding("0px");
            buttonsPanel.setCellpadding("0px");
            htmlPanelGrid.setCellspacing("0px");
            buttonsPanel.setCellspacing("0px");
            htmlPanelGrid.getChildren().add(messagesPanel.createMessagePanel());

            try {
                validateInputSetMapping();
            } catch (OBackBeanException ex) {
                logger.error("Exception thrown", ex);
                UserMessage errorMsg = usrMsgService.getUserMessage(
                        ex.getMessage(), loggedUser);
                errorMsg.setMessageType(UserMessage.TYPE_ERROR);
                logger.error("Exception thrown: ", ex);
                //TODO: usage of ex.getMessage is not guaranteed at all; fix that
                if (errorMsg != null) {
                    messagesPanel.addMessages(errorMsg);
                }
                //return generalPanel;
            }
            CommandButton saveButton = new CommandButton();
            CommandButton saveExitButton = new CommandButton();
            mode = (mode == 0) ? OScreen.SM_ADD : mode;
            boolean saveButtonFlag = false;
            List<ScreenField> screenFields;// = new ArrayList<ScreenField>();
            List<ScreenFieldGroup> controlsGroups = getCurrentScreen().getScreenControlsGroups();

            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Loading Icon">
            HtmlPanelGrid loadingIconPanel = new HtmlPanelGrid();
            loadingIconPanel.setId(getFacesContext().getViewRoot().createUniqueId() + "_load");
            loadingIconPanel.setStyleClass("loading-icon");
            AjaxStatus loadingState = new AjaxStatus();
            loadingState.setId(getFacesContext().getViewRoot().createUniqueId() + "_loadAjax");
            GraphicImage loadingImage = new GraphicImage();
            loadingImage.setId(getFacesContext().getViewRoot().createUniqueId() + "_loadImg");
            loadingImage.setStyleClass("loading-active");
            loadingState.getFacets().put("start", loadingImage);
            loadingState.getFacets().put("complete", new HtmlOutputText());
            loadingIconPanel.getChildren().add(loadingState);
            buttonsPanel.getChildren().add(loadingIconPanel);
            //</editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Create Header">
            //Adding Process Panel
//            HtmlPanelGrid processPanel = new HtmlPanelGrid();
//            if (processMode
//                    || (outboxProcessId != null && !outboxProcessId.equals(""))
//                    || (outboxTaskId != null && outboxTaskId.equals(""))) {
//
//                processPanel = this.createProcessPanel();
//                htmlPanelGrid.getChildren().add(processPanel);
//            }
            //Adding Routing Panel
            if (routingMode) {
                htmlPanelGrid.getChildren().add(this.createRoutingPanel());
            }

            HtmlPanelGrid middlePanel = new HtmlPanelGrid();
            middlePanel.setId(getFacesContext().getViewRoot().createUniqueId() + "MDPanel" + getCurrentScreen().getName());
            middlePanel.setColumns(((FormScreen) getCurrentScreen()).getColumnsNo() * 2);

            if (getLoggedUser().getFirstLanguage().getDbid() == 24) {
                middlePanel.setStyle("text-align:right;");
            }
            middlePanel.setCellpadding("0");
            middlePanel.setCellspacing("2");

            middlePanel.setStyleClass("ctrlTable");

            if (getScreenParam(MODE_VAR) != null) {
                mode = Long.parseLong(String.valueOf(getScreenParam(MODE_VAR)));
            }

            if (restorePDataFromSessoion) {
                setLoadedObject((BaseEntity) restoreObject("loadedObject"));
            } else {

                processInput();
                if (isFormReady) {
                    isFormReady = false;
                    messagesPanel.addMessages(usrMsgService.getUserMessage("DuplicateSubmission", loggedUser));

                    return htmlPanelGrid;

                }
            }
            int primaryFieldsNo = 0;
            if (headerPanelLayout != null && getCurrentScreen().isShowHeader()) {
                htmlPanelGrid.getChildren().add(headerPanelLayout);
            }
            if (primaryFieldsNo != 0 && primaryFieldsNo % 2 == 1) {
                middlePanel.getChildren().add(new OutputLabel());
                middlePanel.getChildren().add(new OutputLabel());
            }
            //</editor-fold>

            saveButtonFlag = (mode == OScreen.SM_EDIT || mode == OScreen.SM_VIEW);

            if (mode == OScreen.SM_ADD) {
                if (loadedObject == null) {
                }
                saveButtonFlag = true;
            }

            //<editor-fold defaultstate="collapsed" desc="Build Screen!">
            ScreenField screenField;// = null;
            //arezk: fix the loadedObject being null in next while loop
            if (loadedObject == null) {
                try {
                    loadedObject = (BaseEntity) Class.forName(getOactOnEntity().
                            getEntityClassPath()).newInstance();
                    applyHardCodedInitialization(loadedObject);
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }
            }
            //attachment Number
            if (this.getLoadedObject().getDbid() != 0) {
                List<AttachmentDTO> attachmentDTOs = entityAttachmentService.get(this.getLoadedObject().getDbid(), this.getLoadedObject().getClassName(), loggedUser);
                if (attachmentDTOs != null && !attachmentDTOs.isEmpty()) {
                    setHasAttachments(true);
                    setNoOfAttachments(attachmentDTOs.size());
                }
            }
            int controlsInGroup = 0;
            if (controlsGroups != null && !controlsGroups.isEmpty()) {
                middlePanel.setColumns(1);

                int screenFieldIndex = 0;   // It's one index for all groups,
                // if it's an index PER group, then controls ids may be repeated
                // causing exceptions like
                // SEVERE: JSF1007: Duplicate component ID frmScrForm:PanelToolTip1 found in view.

                //this code was added by melad to sort controlsGroups in the formScreen with helping of ComparatorForFormControlsGroup class
                ComparatorForFormControlsGroup comparatorForFormControlsGroup = new ComparatorForFormControlsGroup();
                Collections.sort(controlsGroups, comparatorForFormControlsGroup);

                for (ScreenFieldGroup screenControlsGroup : controlsGroups) {
                    Panel collapsible = new Panel();
                    collapsible.setToggleable(true);
                    collapsible.setToggleSpeed(500);
                    collapsible.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_Grop"
                            + screenControlsGroup.getDbid());
                    textTranslationServiceRemote.loadEntityTranslation(screenControlsGroup, loggedUser);
                    collapsible.setHeader(screenControlsGroup.getHeaderTranslated());

                    HtmlPanelGrid collasibleMiddlePanel = new HtmlPanelGrid();
                    collasibleMiddlePanel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_GropPnl"
                            + screenControlsGroup.getDbid());
                    collasibleMiddlePanel.setColumns(screenControlsGroup.getNumberOfColumns() * 2);
                    screenFields = screenControlsGroup.getGroupFields();
                    String msg = securityService.getEntityFieldsTagMsg(actOnEntityCls,
                            screenFields, loggedUser);
                    if (null != msg) {
                        setHasSecuirtyTags(true);
                        setSecurityTagMsg(msg);
                    }

                    //this code was added by melad to sort screenFields in the group with helping of ComparatorForFormScreenGroup class
                    ComparatorForFormScreenGroup comparatorForFormScreenGroup = new ComparatorForFormScreenGroup();
                    Collections.sort(screenFields, comparatorForFormScreenGroup);

                    Iterator screenFieldsIterator = screenFields.iterator();
                    while (screenFieldsIterator.hasNext()) {
                        fieldExpressions.add(((ScreenField) screenFieldsIterator.next()).getFieldExpression());
                    }
                    screenFieldsIterator = screenFields.iterator();
                    int screenFieldIndexInSection = 0;
                    while (screenFieldsIterator.hasNext()) {
                        screenField = (ScreenField) screenFieldsIterator.next();
                        if (screenField.isInActive()) {
                            continue;
                        }
                        screenFieldIndex++;
                        screenFieldIndexInSection++;
                        try {
                            //screenField = (ScreenField) screenFieldsIterator.next();
                            createFormScreenFieldControl(collasibleMiddlePanel, screenField, screenFieldIndex, screenFieldIndexInSection);
                        } catch (Exception ex) {
                            logger.error("Exception thrown", ex);
                        }
                        controlsInGroup++;
                    }
                    collapsible.setCollapsed(!screenControlsGroup.isExpandable());
                    collapsible.setToggleable(!screenControlsGroup.isExpandable());
                    collapsible.getChildren().add(collasibleMiddlePanel);
                    middlePanel.getChildren().add(collapsible);

                }
                middlePanel.setStyle("width:100%;");

            } else {
                int screenFieldIndex = 0;
                screenFields = ((FormScreen) currentScreen).getScreenFields();
                String msg = securityService.getEntityFieldsTagMsg(actOnEntityCls,
                        screenFields, loggedUser);
                if (null != msg) {
                    setHasSecuirtyTags(true);
                    setSecurityTagMsg(msg);
                }
                ComparatorForFormScreenGroup comparatorForFormScreenGroup = new ComparatorForFormScreenGroup();
                Collections.sort(screenFields, comparatorForFormScreenGroup);
                Iterator screenFieldsIterator = screenFields.iterator();
                while (screenFieldsIterator.hasNext()) {
                    fieldExpressions.add(((ScreenField) screenFieldsIterator.next()).getFieldExpression());
                }
                screenFieldsIterator = screenFields.iterator();
                while (screenFieldsIterator.hasNext()) {
                    screenFieldIndex++;
                    try {
                        screenField = (ScreenField) screenFieldsIterator.next();
                        createFormScreenFieldControl(middlePanel, screenField, screenFieldIndex, 0);

                    } catch (Exception ex) {
                        logger.error("Exception thrown", ex);
                    }
                }
            }
            if (controlsInGroup != 0 && controlsInGroup != getCurrentScreen().getScreenFields().size()) {
                UserMessage errorMsg = usrMsgService.getUserMessage(
                        "ScreenFieldGroup", loggedUser);
                errorMsg.setMessageType(UserMessage.TYPE_ERROR);
                messagesPanel.clearAndDisplayMessages(errorMsg);
            }

            //<editor-fold defaultstate="collapsed" desc="Save Button and Category Code">
            if (saveButtonFlag && showSaveButton) {

                //DD saveButtonDD = ddService.getDD("saveButton", loggedUser);
                saveButton = createButton(buttonsPanel,
                        ddService.getDDLabel("saveButton", loggedUser), "save", null, getBeanName());
                saveButton.setStyleClass("save_btn");
                saveButton.setIcon("save_btn_icon");

                saveButton.setAjax(true);
                saveButton.setUpdate("frmScrForm:middlePanel");
                saveButton.setValueExpression("visible",
                        exFactory.createValueExpression(elContext, "#{!" + getBeanName() + ".processMode}", boolean.class));

            }

            if (saveButtonFlag && showSaveExitButton) {
                saveExitButton = createButton(buttonsPanel,
                        ddService.getDDLabel("saveExitButton", loggedUser), "saveExit", null, getBeanName());
                saveExitButton.setStyleClass("save_exit_btn");
                saveExitButton.setDir(htmlDir);
                saveExitButton.setIcon("save_exit_btn_icon");
                saveExitButton.setAjax(true);
                saveExitButton.setUpdate("frmScrForm:middlePanel");
                saveExitButton.setValueExpression("visible",
                        exFactory.createValueExpression(elContext, "#{!" + getBeanName() + ".processMode}", boolean.class));
            }

            //</editor-fold>
            //   ADD middlePanel TO generalPanel
            if (showSaveButton) {
                saveButton.setValue(" ");
            }

            if (showSaveExitButton) {
                saveExitButton.setValue(" ");
            }
            //Rehab: add action buttons to process pnl in case processMde
            HtmlPanelGrid processPanel = new HtmlPanelGrid();
            buttonsPanel.setStyleClass("buttonsGeneralPanel");
            if (processMode
                    || (outboxProcessId != null && !outboxProcessId.equals(""))
                    || (outboxTaskId != null && outboxTaskId.equals(""))) {
                processPanel = taskOptionProcess.createProcessPanel(outboxProcessId, getPortletInsId(), getBeanName(), getScreenLanguage(), isInvalidInputMode(), getCurrentScreen());
                createScreenActionsButtons(processPanel, "frmScrForm:middlePanel");
                htmlPanelGrid.getChildren().add(processPanel);
            } else {
                createScreenActionsButtons(buttonsPanel, "frmScrForm:middlePanel");
            }
            createMoreActionsMenu(buttonsPanel);

            htmlPanelGrid.getChildren().add(middlePanel);
            //Hide the 'Save' and 'Save & Exit' buttons in process mode:
            if (getScreenParam("taskID") == null
                    && getScreenParam("Alert") == null
                    && mode != OScreen.SM_VIEW) {
                htmlPanelGrid.getChildren().
                        add(buttonsPanel);
            }

            if (wizardMode) {
                wizardPanel = new WizardPanel();
                Separator wizardSeparator = new Separator();
                wizardSeparator.setId(getFacesContext().getViewRoot().createUniqueId() + "wzSep");
                wizardSeparator.setStyleClass("wizard_separator");
                Spacer wizardDivider = new Spacer();
                wizardDivider.setId(getFacesContext().getViewRoot().createUniqueId() + "wzDivid");
                wizardSeparator.setStyleClass("wizard_driver");
                htmlPanelGrid.getChildren().add(wizardSeparator);
                htmlPanelGrid.getChildren().add(
                        wizardPanel.createWizardPanel(getBeanName(), exFactory, elContext, wizardInfo, false, ddService, loggedUser));
            }

            restorePDataFromSessoion = false;
            if (getScreenHeight() == null || getScreenHeight().equals("")) {
                onloadJSStr += "top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');";
            } else {
                onloadJSStr += "top.FABS.Portlet.updateIframeSizeWithHeight({iframeName:'" + getPortletInsId() + "Frame'"
                        + ",screenHeight:'" + getScreenHeight() + "'});";
            }

            GearOptionsHandler gearOptionsUtil = new GearOptionsHandler(loggedUser);
            UIComponent mbutton = (UIComponent) gearOptionsUtil.addGearToPortletOptions(this);

            htmlPanelGrid.getChildren().add(0, mbutton);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addMessages(functionServiceRemote.constructOFunctionResultFromError(
                    "SystemInternalError", ex, loggedUser));
            return htmlPanelGrid;
        }
        // <editor-fold defaultstate="collapsed" desc="Test Code for Open Report">
        if (processMode && c2AMode) {
            try {
                OReport report = (OReport) oem.loadEntity(OReport.class.getSimpleName(), Collections.singletonList("dbid=2"), null, loggedUser);
                openReport(report);
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
        }
        // </editor-fold>
        return htmlPanelGrid;
    }

    @Override
    public String wizardFinishAction() {
        logger.debug("Entering");
        try {
            save();
            if (!exit) {
                //TODO : ADD ERROR MESSAGE
                return "";
            }
            return super.wizardFinishAction();
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(usrMsgService.
                    getUserMessage("SystemInternalError", systemUser), ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    @Override
    public String wizardNextAction() {
        try {
            save();
            ODataMessage wizardDataMessage = null;
            if (!exit) {
                //TODO : ADD ERROR MESSAGE
                return "";
            }

            if (wizardInfo.getStep().isMustSelect()) {
                wizardDataMessage = entityServiceLocal.constructEntityODataMessage(loadedObject, loggedUser);
            }

            return super.wizardNextAction(wizardDataMessage);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(usrMsgService.
                    getUserMessage("SystemInternalError", systemUser), ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    @Override
    protected String getBeanName() {
        return "FormBean";
    }

    //TODO: Mustafa El-Zaher
    @Override
    public ODataMessage saveProcessRecords() {
        save();
        //  ODataType outputDataType = getTaskInfo().getAeTTask().getoOutputDataType();
        String output = (String) processingTask.getInputs().get("outputType");
        ODataType outputDataType = humanTaskService.getDataType(output, loggedUser);
        return (ODataMessage) constructOutput(outputDataType, getLoadedObject()).getReturnedDataMessage();
    }

    public HtmlPanelGrid createLabel(String id, ScreenField screenField) {
        logger.debug("Entering");
        try {
            HtmlPanelGrid headerLabelGrid = null;
            if (headerLabelGrid != null) {
                return headerLabelGrid;
            }
            headerLabelGrid = new HtmlPanelGrid();

            DD fieldDD = (DD) textTranslationServiceRemote.loadEntityTranslation(
                    screenField.getDd(), loggedUser);
            String fieldDDLBL = fieldDD.getLabelTranslated();

            screenField = (ScreenField) textTranslationServiceRemote.loadEntityTranslation(
                    screenField, loggedUser);
            String controlLabel = (screenField.getDdTitleOverrideTranslated() != null
                    && !screenField.getDdTitleOverrideTranslated().equals(""))
                            ? screenField.getDdTitleOverrideTranslated() : fieldDDLBL;
            String astrick = (screenField.isMandatory()) ? "*" : " ";
            HtmlOutputLabel outputLabel = new HtmlOutputLabel();
            if (id.contains(".")) {
                outputLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_"
                        + id.substring(id.lastIndexOf(".") + 1) + "_Label");
            } else {
                outputLabel.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_"
                        + id + "_Label");
            }
            outputLabel.setStyleClass("labelTxt");
            outputLabel.setValue(controlLabel);
            //This is a work around for alignment ... the user to add spaces before label
            //TOFIX: adjust the allignment for all panels
            int spacesCount = 0;
            if (controlLabel.startsWith(" ")) {
                try {
                    spacesCount = StringUtils.countMatches(
                            controlLabel.substring(
                                    0,
                                    controlLabel.indexOf(controlLabel.trim().concat(" ").substring(0, controlLabel.trim().indexOf(" ") + 2))), " ");
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }
            }

            if (spacesCount > 0) {
                headerLabelGrid.setStyle("margin-left : " + spacesCount + "px");
            }

            HtmlOutputLabel astriskLabel = new HtmlOutputLabel();
            astriskLabel.setId(getFacesContext().getViewRoot().createUniqueId() + "astriskLBL" + screenField.getDbid());
            if (screenField.isMandatory()) {
                astriskLabel.setValue("*");
            } else {
                astriskLabel.setStyle("padding-right:5px");
            }
            astriskLabel.setStyleClass("labelAsterisk");

            headerLabelGrid.setId(getFacesContext().getViewRoot().createUniqueId() + "headerLabelGrid" + screenField.getDbid());

            headerLabelGrid.setColumns(3);
            headerLabelGrid.setCellpadding("0");
            headerLabelGrid.setCellspacing("0");
            headerLabelGrid.getChildren().add(outputLabel);
            headerLabelGrid.setStyleClass("labelContainer");
            headerLabelGrid.getChildren().add(astriskLabel);
            return headerLabelGrid;
        } catch (Exception ex) {
            logger.warn("Error Creating Header");
            logger.error("Exception thrown", ex);
            logger.debug("Returning");
            return new HtmlPanelGrid();
        }
    }

    public void getMessages(ActionEvent actionEvent) {
        setMessageGrid(buildMessagePanel());
    }

    @Override
    protected BaseEntity getObjectForLookup(long dbid) {
        return loadedObject;
    }
    private String lookUpButtonID;

    public void lookupListener(ActionEvent ae) {
        logger.trace("Lookup Listener Triggered");
        lookUpButtonID = ae.getComponent().getId();
        if (Character.isDigit(lookUpButtonID.charAt(lookUpButtonID.length() - 1))) {
            lookUpButtonID = lookUpButtonID.substring(0, lookUpButtonID.lastIndexOf("_"));
        }
    }

    public String lookup() {
        logger.debug("Entering");
        try {
            oem.getEM(loggedUser);
            String openedScreenInstId = "";
            if (fieldLookup.getFieldDD().getControlType().getDbid() != DD.CT_LOOKUPATTRIBUTEBROWSER) {
                //lookupFilter = fieldLookup.getLookupFilter();
                lookupHeaderStr = fieldLookup.getLookupHeader();
                fieldLookup.setLookupFilter(new ArrayList<String>());
            }
            fieldLookup.setLookupFilter(new ArrayList<String>());
            fieldLookup = ((Lookup) getLookupHashMap().get(lookUpButtonID));
            lookupHeaderStr = fieldLookup.getLookupHeader();
            recordID = fieldLookup.getEntityDBID_H().getValue().toString();
            RequestContext context = RequestContext.getCurrentInstance();
            if (fieldLookup.getFieldDD().getControlType().getDbid() == DD.CT_LOOKUPATTRIBUTEBROWSER) {
                lookupPanelRendered = true;
                ODataType voidDataType = dataTypeService.loadODataType("VoidDT", systemUser);
                ODataMessage voidDataMessage = new ODataMessage();
                List data = new ArrayList();
                data.add(fieldLookup.getFieldExpressionTreeOEntity(headerObject,
                        getOactOnEntity().getEntityClassName(),
                        ddService, entitySetupService, entityServiceLocal,
                        uiFrameWorkFacade));
                voidDataMessage.setData(data);
                voidDataMessage.setODataType(voidDataType);
                openedScreenInstId = generateScreenInstId("FieldsTreeBrowser");
                SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.MESSAGE, voidDataMessage);
                SessionManager.setScreenSessionAttribute(openedScreenInstId, "isLookup", true);
                SessionManager.addSessionAttribute("lookupORScreen", getPortletInsId());
                openScreenInPopup("FieldsTreeBrowser", openedScreenInstId, POPUP_LOOKUPTYPE);
                logger.debug("Returning with Null");
                return null;
            }
            try {
                ODataType voidDataType = dataTypeService.loadODataType("VoidDT", systemUser);

                dataMessage = new ODataMessage(voidDataType, new ArrayList());
                // generate screenInstanceId variable
                openedScreenInstId = generateScreenInstId("OTMSPortletPopup");
                SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.MESSAGE, dataMessage);
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
            fieldLookup.buildLookupFilter();
            SessionManager.addSessionAttribute("lookupORScreen", getPortletInsId());
            Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();

            if (fieldLookup.getOScreen().getName().equals("ManageCustomReportDynamicFilters")) {

                CustomReport customReport = ((CustomReportFilterBean) this).getCustomReport();
                if (customReport != null && customReport.getCustomReportDynamicFilter() != null) {
                    SessionManager.addSessionAttribute("EntityFiltersForCustomReport", customReport);
                }
            }
            TabularScreenDTO.createLookupMainScr(getCurrentScreen().getInstID(), fieldLookup.getOScreen().getName() + "_" + fieldLookup.getFieldExpression());
            setLookupMainScrInstID(fieldLookup.getOScreen().getName(), getCurrentScreen().getInstID());

            context.execute(
                    "try { top.FABS.Portlet.openPopup({screenName:'"
                    + fieldLookup.getOScreen().getName() + "',screenInstId:'" + openedScreenInstId
                    + "',viewPage:'" + fieldLookup.getOScreen().getViewPage()
                    + "',title:'" + lookupHeaderStr.replace("\\", "\\\\").replace("\'", "\\'") + "',isLookup:'true'});} catch(err){}");
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(
                    usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
        } finally {
            oem.closeEM(loggedUser);
        }
        logger.debug("Returning with Null");
        return null;
    }

    @Override
    public String getLookupConditionValue(String conditionExpression) {
        return String.valueOf(getValueFromEntity(loadedObject, conditionExpression));
    }

    public String getAlookupText() {
        return (String) SessionManager.getSessionAttribute(LOOKUP_VAR);
    }

    public void setAlookupText(String alookupText) {
        this.alookupText = alookupText;
    }
    //</editor-fold >

    //<editor-fold defaultstate="collapsed" desc="Save Actions ">
    public void saveExit() {
        save();

        if (exit) {
            if (openedAsPopup) {

                String param = currentScreen.getName();
                RequestContext.getCurrentInstance().execute("top.FABS.Portlet.closePopupNoID('" + param + "');");
                logger.trace("Calling JS closePopupNoID with PortletInsId: {}", getPortletInsId());
            } else {
                OPortalUtil.removePortlet(getPortletInsId(), screenInstId);
            }
        }
    }
    private EntityRequestStatus currentStatus;
    private EntityRequestStatus nextStatus;

    public EntityRequestStatus getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(EntityRequestStatus currentStatus) {
        this.currentStatus = currentStatus;
    }

    public EntityRequestStatus getNextStatus() {
        return nextStatus;
    }

    public void setNextStatus(EntityRequestStatus nextStatus) {
        this.nextStatus = nextStatus;
    }

    public String saveRequestAction(ActionEvent event) {
        logger.debug("Entering");
        try {
            OEntityDTO entity = getOactOnEntity();
            if (!entity.isRequestable()) {
                UserMessage message = new UserMessage();    //TODO: use Database UserMessage
                message.setMessageTitle("Error:");
                message.setMessageText("The Entity " + entity.getEntityClassName()
                        + " is not Requestable.");
                getMessages().add(message);
                buildMessagePanel();
                logger.debug("Returning with Null");
                return null;
            }
            OFunctionResult functionResult = new OFunctionResult();
            String status = ((CommandButton) event.getComponent()).getLabel();
            ArrayList<String> filedExpressions = new ArrayList<String>();
            ArrayList<String> conditions = new ArrayList<String>();
            nextStatus = null;
            conditions.add("oentityStatus.dbid=" + entity.getDbid());
            conditions.add("name like '" + status + "'");
            nextStatus = (EntityRequestStatus) oem.loadEntity("EntityRequestStatus", conditions, null,
                    getLoggedUser());
            if (nextStatus == null) {
                if (null != entity) {
                    logger.warn("The entity: {} has undefined Next status", entity.getEntityClassName());
                }
                UserMessage message = new UserMessage(); //TODO: use Database UserMessage
                message.setMessageTitle("Error:");
                message.setMessageText("The Entity "
                        + entity.getEntityClassName()
                        + " has Undefined Next Status.");
                getMessages().add(message);
                buildMessagePanel();
                logger.debug("Returning with Null");
                return null;
            }
            loadedObject.setEntityStatus(nextStatus);
            OEntityAction statusAction = nextStatus.getExecutedActionOnSet();
            filedExpressions.add("posts");
            filedExpressions.add("validations");
            filedExpressions.add("ofunction");
            statusAction = (OEntityAction) oem.loadEntityDetail(
                    statusAction, null, filedExpressions, getLoggedUser());
            filedExpressions.clear();
            filedExpressions.add("entityRequestStatus");
            ODataType entityDataType = dataTypeService.loadODataType(getOactOnEntity().
                    getEntityClassName(), systemUser);
            OFunctionParms parm = new OFunctionParms();
            List<EntityRequestStatus> entityStatuses = new ArrayList<EntityRequestStatus>();
            entityStatuses.addAll(entitySetupService.loadOEntity(
                    entity.getEntityClassPath(), loggedUser).getEntityRequestStatus());
            parm.getParams().put(EntitySetupService.VALIDOENTITYSTATUSMAPKEY, entityStatuses);
            ODataMessage entityDataMessage = (ODataMessage) constructOutput(
                    entityDataType, loadedObject).getReturnedDataMessage();
            OFunctionResult ofr = entitySetupService.executeAction(statusAction,
                    entityDataMessage, parm, getLoggedUser());
            functionResult.append(ofr);
            EntityRequestStatusHistory entityRequestStatusHistory
                    = new EntityRequestStatusHistory();
            entityRequestStatusHistory.setActionTime(timeZoneService.getUserCDT(loggedUser));
            if (currentStatus != null) {
                entityRequestStatusHistory.setOldStatus(currentStatus.getName());
            }
            entityRequestStatusHistory.setNewStatus(status);
            entityRequestStatusHistory.setEntityClassPath(entity.getEntityClassPath());
            entityRequestStatusHistory.setEntityDBID(loadedObject.getDbid());
            entityRequestStatusHistory.setRequester(getLoggedUser());
            entityRequestStatusHistory.setRequestNote(loadedObject.getRequestNote());
            oem.saveEntity(entityRequestStatusHistory,
                    getLoggedUser());
            if (ofr.getReturnValues().size() > 0) {
                loadedObject = (BaseEntity) ofr.getReturnValues().get(0);
            }
            nextRequestStatus = status;
            List<Object> requestOutputMessage = new ArrayList<Object>();
            requestOutputMessage.add(loadedObject.getDbid());
            requestOutputMessage.add(nextRequestStatus);
            List<String> dtRequestConditions = new ArrayList<String>();
            dtRequestConditions.add("name = 'requestStatus'");
            ODataType outputRequestType = dataTypeService.loadODataType("requestStatus", systemUser);
            entityDataMessage = (ODataMessage) constructOutput(outputRequestType, loadedObject).getReturnedDataMessage();
            getMessages().addAll(functionResult.getSuccesses());
            getMessages().addAll(functionResult.getErrors());
            getMessages().addAll(functionResult.getWarnings());
            setMessageGrid(buildMessagePanel());
            logger.debug("Returning with Null");
            return null;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning with Null");
        return null;

    }

    protected void changeMode(long newMode) {
        mode = newMode;
    }

    public String save() {
        logger.debug("Entering");
        int count = count(liferayUsers.get(loggedUser.getLoginName()));
        if (count == 0) {
            logout();
            logger.debug("Returning with Null");
            return null;
        }
        OFunctionResult oFR = globalOFunctionResult;
        globalOFunctionResult = new OFunctionResult();
        try {
            oem.getEM(loggedUser);
            OEntityActionDTO parentPreSave = entitySetupService.
                    getOEntityDTOActionDTO(getOactOnEntity().getDbid(),
                            OEntityActionDTO.ATDBID_ParentPreSave, loggedUser);

            if (parentPreSave != null) {
                oFR.append(runParentValidation(parentPreSave));
            }
            if (oFR.getErrors().size() > 0) {
                logger.debug("Returning with Null");
                return null;
            }

            ODataType objectDataType = dataTypeService.loadODataType(getOactOnEntity().getEntityClassName(), loggedUser);
            //When there is lookup it relation one-to-one it reset to null and do not get saved when it is a new field.
            //So; we have to check of the look up field is a one-to-one first.

            boolean save = true;
            for (int currentField = 0; currentField < lookupMandatoryFields.size(); currentField++) {
                //FIXME : Execlude Cascade Persist Object
                if (getValueFromEntity(loadedObject, lookupMandatoryFields.get(currentField)) == null
                        || getValueFromEntity(loadedObject, lookupMandatoryFields.get(currentField)).toString().equals("")) {
                    try {
                        String lookupID = lookupMandatoryFields.get(currentField).replace(".", "_") + "_Lookup_";

                        String label = lookupMandatoryFields.get(currentField);
                        if (getLookupHashMap().get(lookupID) != null) {
                            label = ((Lookup) getLookupHashMap().get(lookupID)).getFieldDD().getHeaderTranslated();
                        }
                        UserMessage message = usrMsgService.getUserMessage("LookupFieldValidation",
                                Collections.singletonList(label),
                                loggedUser);
                        message.setMessageText(message.getMessageText() + label);
                        exit = false;
                        RequestContext.getCurrentInstance().execute("scroll(0,0)");
                        RequestContext.getCurrentInstance().execute("topOfPage();");
                        oFR.addError(message);
                    } catch (Exception ex) {
                        logger.warn("Lookup Validation Message Error");
                        logger.error("Exception thrown", ex);
                    }
                    save = false;
                }
            }

            if (!save) {
                logger.debug("Returning with Null");
                return null;
            }
            for (int currentField = 0; currentField < lookupFldExps.size(); currentField++) {

                BaseEntity baseEntity = (BaseEntity) getValueFromEntity(loadedObject, lookupFldExps.get(currentField));
                Field fld = BaseEntity.getClassField(actOnEntityCls, lookupFldExps.get(currentField));
                boolean cascade = false;

                if (fld.getAnnotation(OneToOne.class) != null) {
                    OneToOne oneTOneAnnot = fld.getAnnotation(OneToOne.class);
                    for (int i = 0; i < oneTOneAnnot.cascade().length; i++) {
                        CascadeType type = oneTOneAnnot.cascade()[i];
                        if (type == CascadeType.ALL || type == CascadeType.PERSIST) {
                            cascade = true;
                        }
                    }
                }

                FABSEntitySpecs entityFABSSpecs = loadedObject.getClass().getAnnotation(FABSEntitySpecs.class);

                if (entityFABSSpecs != null && entityFABSSpecs.customCascadeFields() != null) {
                    for (String childFieldName : entityFABSSpecs.customCascadeFields()) {
                        if (lookupFldExps.get(currentField).startsWith(childFieldName)) {
                            cascade = true;
                        }
                    }
                }

                if (baseEntity != null && baseEntity.getDbid() == 0 && !cascade) {
                    FramworkUtilities.setValueInEntity(loadedObject, lookupFldExps.get(currentField), null);
                    // </editor-fold>
                }
            }

            //Apply Sequence Initialization
            applySequenceInitialization(loadedObject);

            ODataMessage objectDataMessage = (ODataMessage) constructOutput(objectDataType,
                    loadedObject).getReturnedDataMessage();

            OEntityActionDTO action = null;
            if (mode == OScreen.SM_ADD) {
                action = entitySetupService.
                        getOEntityDTOActionDTO(getOactOnEntity().getDbid(),
                                OEntityActionDTO.ATDBID_CREATE, loggedUser);
            } else if (mode == OScreen.SM_EDIT) {
                action = entitySetupService.
                        getOEntityDTOActionDTO(getOactOnEntity().getDbid(),
                                OEntityActionDTO.ATDBID_UPDATE, loggedUser);
            } else if (mode == OScreen.SM_DELETE) {
                action = entitySetupService.
                        getOEntityDTOActionDTO(getOactOnEntity().getDbid(),
                                OEntityActionDTO.ATDBID_DELETE, loggedUser);
            } else if (mode == OScreen.SM_VIEW) {
                action = entitySetupService.
                        getOEntityDTOActionDTO(getOactOnEntity().getDbid(),
                                OEntityActionDTO.ATDBID_RETRIEVE, loggedUser);
            } else {
                logger.warn("Invalid Screen Mode");
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
            }

            try {
                OFunctionParms functionParms = new OFunctionParms();
                functionParms.getParams().put("ScreenName", getCurrentScreen().getName());
                functionParms.getParams().put("ScreenDT", getDataMessage());
                if (action != null) {

                    oFR.append(entitySetupService.executeAction(action,
                            objectDataMessage, functionParms, getLoggedUser()));
                }
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
            }
            if (oFR.getErrors().size() > 0) {
                exit = false;
                RequestContext.getCurrentInstance().execute("scroll(0,0)");
                RequestContext.getCurrentInstance().execute("topOfPage();");
            } else {
                exit = true;
            }

            //RDO :This code to update all opened screens in the page that act on the same entity
            if (oFR.getErrors() == null || oFR.getErrors().isEmpty()) {
                updateOpenedScreenOfSameActOnEntity();
                if (mode == OScreen.SM_ADD) {
                    ODataMessage routingDataMessage = new ODataMessage();
                    routingDataMessage.setODataType(objectDataType);
                    List routingMsgData = new ArrayList();
                    routingMsgData.add(currentScreen);
                    routingMsgData.add(loadedObject);
                    routingMsgData.add(loggedUser);
                    routingDataMessage.setData(routingMsgData);
                    //routingBeanLocal.startRoutingInstance(routingDataMessage, new OFunctionParms(), loggedUser);
                }
            }

            if (oFR.getReturnedDataMessage() != null) {
                loadedObject = (BaseEntity) oFR.getReturnedDataMessage().getData().get(0);
            }
            // <editor-fold defaultstate="collapsed" desc="This Code to Send DataMessage to all the its childern incase the screen is in portal mode">
            if (c2AMode && toBeRenderScreens != null && toBeRenderScreens.size() > 0) {
                ODataMessage outputMessage = null;
                outputMessage = (ODataMessage) constructOutput(objectDataType, loadedObject).getReturnedDataMessage();
                if (outputMessage != null) {
                    for (ScreenRenderInfo screenRenderInfo : toBeRenderScreens) {
                        OPortalUtil.renderScreen(currentScreen, screenRenderInfo.screenInstId, dataMessage);
                    }

                }
            }
            // </editor-fold>

            if (mode == OScreen.SM_ADD && loadedObject.getDbid() != 0) {
                mode = OScreen.SM_EDIT;
            }

            if (loadedObject != null) {
                List<String> fieldsToReInitialize = new ArrayList<String>();
                for (String fldExp : fieldExpressions) {
                    if (fldExp.contains(".")) {
                        Object value = BaseEntity.getValueFromEntity(loadedObject, fldExp);
                        if (value == null) {
                            fieldsToReInitialize.add(fldExp);
                        }
                    }
                }
                if (!fieldsToReInitialize.isEmpty()) {
                    loadedObject.NewFieldsInstances(fieldsToReInitialize);
                }
            }

            if (loadedObject.isPreSavedEntity()) {
                //get attahment before save
                BaseEntity entity = (BaseEntity) loadedObject;
                List<AttachmentDTO> attachments = null;
                attachments = entityAttachmentService.getPreSave(entity, loggedUser);
                if (attachments != null) {

                    if (attachments != null && attachments.size() > 0) {
                        for (AttachmentDTO attachment : attachments) {
                            entityAttachmentService.put(attachment, loadedObject.getClass().getName(), loadedObject.getDbid(), loadedObject, loggedUser);
                        }
                    }
                }
                entityAttachmentService.clearAttachmentListBeforeSave(entity, loggedUser);
            }
        } catch (Exception ex) {
            if (ex instanceof FABSException) {
                oFR.addError(usrMsgService.getUserMessage("ModuleError", loggedUser));
            } else {
                UserMessage errorMsg = usrMsgService.getUserMessage("SystemInternalError", systemUser);
                oFR.addError(errorMsg, ex);
            }
            logger.error("Exception thrown", ex);
        } finally {
            oem.closeEM(loggedUser);
            messagesPanel.clearAndDisplayMessages(oFR);
            RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());

            if (screenInstId.contains("ChangeUserPassword")
                    && oFR.getSuccesses() != null && !oFR.getSuccesses().isEmpty()) {
                OPortalUtil.updateUserPassWord(((OUser) loadedObject).getLoginName(), ((OUser) loadedObject).getNewPass(), ((OUser) loadedObject).getConfirmPass());
            }
            // Render all partc groups
            if (myGroups != null) {
                SessionManager.addSessionAttribute(
                        OPortalUtil.C2A_VAR, "TIME_IS_" + System.currentTimeMillis());
                renderSession();
            }
            for (int currentField = 0; currentField < lookupFldExps.size(); currentField++) {
                loadAttribute(lookupCtrlsExps.get(currentField));
                break;
            }

            HashMap<String, Object> screenParamMap = SessionManager.getScreenSessionAttribute(screenInstId);
            String basicScreenInstID = ((screenParamMap != null) ? ((String) screenParamMap.get(OPortalUtil.BASICSCREENID)) : null);
            if (null != basicScreenInstID
                    && !"".equals(basicScreenInstID)
                    && oFR.getErrors().isEmpty()) {
                ODataMessage basicDataMessage = (ODataMessage) screenParamMap.get(OPortalUtil.BASICSCREENMSG);
                SessionManager.setScreenSessionAttribute(basicScreenInstID,
                        OPortalUtil.BASICSCREENMSG, basicDataMessage);
                SessionManager.setScreenSessionAttribute(basicScreenInstID,
                        OPortalUtil.ISBACKFROMDETAIL, "true");
                SessionManager.setScreenSessionAttribute(basicScreenInstID,
                        OPortalUtil.BACKFROMDETAILOBJECT, loadedObject);
                RequestContext.getCurrentInstance().execute("try {top.FABS.Portlet.renderMyBasic({screenInstId:'" + basicScreenInstID
                        + "'});} catch(err){top.FABS.Portlet.renderByPortletId('" + basicScreenInstID + "');};");
            }
            logger.debug("Returning with Null");
            return null;
        }

    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Screen Data Backup ">
    @Override
    public void backupScreenData() {
        backupObject("loadedObject", getLoadedObject());
    }

    //</editor-fold>
    @Override
    protected void buildScreenExpression(UIComponent component) {
        Class[] parms = new Class[]{ValueChangeEvent.class};
        MethodBinding methodBinding = getFacesContext().getApplication().
                createMethodBinding("#{" + getBeanName() + ".updateRow}", parms);
        if (component instanceof InputText) {
            ((InputText) component).setValueChangeListener(methodBinding);
        } else if (component instanceof org.primefaces.component.calendar.Calendar) {
            ((org.primefaces.component.calendar.Calendar) component).setValueChangeListener(methodBinding);
        } else if (component instanceof Password) {
            ((Password) component).setValueChangeListener(methodBinding);
        } else if (component instanceof InputTextarea) {
            ((InputTextarea) component).setValueChangeListener(methodBinding);
        } else if (component instanceof SelectOneRadio) {
            ((SelectOneRadio) component).setValueChangeListener(methodBinding);
        } else if (component instanceof SelectBooleanCheckbox) {
            ((SelectBooleanCheckbox) component).setValueChangeListener(methodBinding);
        }
    }

    public void handleDateSelect(SelectEvent event) {
        logger.debug("Entering");
        try {
            if (loadedObject == null) {
                logger.warn("Loaded Object is Null");
                logger.debug("Returning");
                return;
            }

            String compFieldExp = FramworkUtilities.getControlFldExp(event.getComponent());
            List<ScreenField> screenFields = getCurrentScreen().getScreenFields();
            ScreenField changedScrFld = null;

            // <editor-fold defaultstate="collapsed" desc="Get Component ScreenField">
            for (int idx = 0; idx < screenFields.size(); idx++) {
                if (screenFields.get(idx).getFieldExpression().equals(compFieldExp)) {
                    changedScrFld = screenFields.get(idx);
                    break;
                }
            }
            // </editor-fold>

            if (changedScrFld == null) {
                logger.warn("Field Expression Not Found For Component");
                OFunctionResult oFR = new OFunctionResult();
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser));
                messagesPanel.clearAndDisplayMessages(oFR);
                logger.debug("Returning");
                return;
            }

            // <editor-fold defaultstate="collapsed" desc="Change Function">
            if (changedScrFld.getChangeFunction() != null) {
                ODataType changeDT = dataTypeService.loadODataType("EntityChangeField", loggedUser);
                List changeData = new ArrayList();

                changeData.add(loadedObject);
                changeData.add(compFieldExp);
                changeData.add(event.getObject());
                changeData.add(screenFields);

                ODataMessage changeODM = new ODataMessage(changeDT, changeData);

                OFunctionResult ofr = functionServiceRemote.runFunction(changedScrFld.getChangeFunction(), changeODM, null, loggedUser);

                if (ofr.getReturnedDataMessage() != null
                        && ofr.getReturnedDataMessage().getODataType() != null
                        && !ofr.getReturnedDataMessage().getData().isEmpty()
                        && ofr.getReturnedDataMessage().getData().get(0).getClass().getName().
                        equals(getOactOnEntity().getEntityClassPath())) {
                    loadedObject = (BaseEntity) ofr.getReturnedDataMessage().getData().get(0);
                    RequestContext.getCurrentInstance().update("frmScrForm");

                    if (loadedObject.getDbid() != 0 && mode == OScreen.SM_ADD) {
                        mode = OScreen.SM_EDIT;
                    }
                }
            }
            // </editor-fold>

            // Value Changed
            loadedObject.setChanged(true);
            try {
                if (compFieldExp.contains(".")) {
                    BaseEntity entity = (BaseEntity) BaseEntity.getValueFromEntity(loadedObject,
                            compFieldExp.substring(0, compFieldExp.lastIndexOf(".")));
                    if (entity.getDbid() == 0) {
                        entity.setChanged(true);
                    }
                }
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);

            OFunctionResult oFR = new OFunctionResult();
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
            messagesPanel.clearAndDisplayMessages(oFR);
        }

    }

    public void updateRow(ValueChangeEvent vce) {
        logger.debug("Entering");
        logger.trace("Value Changed Action");
        try {
            if (loadedObject == null) {
                logger.warn("Loaded Object is Null");
                logger.debug("Returning");
                return;
            }
            boolean anyValueChanged = false;

            String compFieldExp = FramworkUtilities.getControlFldExp(vce.getComponent());
            List<ScreenField> fields = getCurrentScreen().getScreenFields();
            ScreenField changedScrFld = null;

            // <editor-fold defaultstate="collapsed" desc="Get Component ScreenField">
            for (int idx = 0; idx < fields.size(); idx++) {
                if (fields.get(idx).getFieldExpression().equals(compFieldExp)) {
                    changedScrFld = fields.get(idx);
                    break;
                }
            }
            // </editor-fold>

            if (changedScrFld == null) {
                logger.warn("Field Expression Not Found For Component");
                OFunctionResult oFR = new OFunctionResult();
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser));
                messagesPanel.clearAndDisplayMessages(oFR);
                logger.debug("Returning");
                return;
            }

            if (changedScrFld.isToggleDim()) {
                //sorting with sort index considering screen groups
                List<ScreenField> sortedFields = new ArrayList<ScreenField>();
                List<ScreenFieldGroup> fieldGroups = ((FormScreen) getCurrentScreen()).getScreenControlsGroups();
                if (fieldGroups == null || fieldGroups.isEmpty()) {
                    sortedFields.addAll(fields);
                } else {
                    for (int j = 0; j < fieldGroups.size(); j++) {
                        List<ScreenField> screenFields = fieldGroups.get(j).getGroupFields();
                        List<ScreenField> tmp = new ArrayList<ScreenField>();
                        for (int i = 0; i < screenFields.size(); i++) {
                            if (screenFields.get(i).isInActive()) {
                                tmp.add(screenFields.get(i));
                            }
                        }
                        screenFields.removeAll(tmp);
                        Collections.sort(screenFields, new Comparator<ScreenField>() {
                            public int compare(ScreenField sf1, ScreenField sf2) {
                                int sortIn1 = sf1.getSortIndex();
                                int sortIn2 = sf2.getSortIndex();
                                /*For ascending order*/
                                return sortIn1 - sortIn2;
                            }
                        });
                        sortedFields.addAll(screenFields);
                    }
                }
                Set<String> dimmedFields = new HashSet<String>();
                for (ScreenFieldValue fieldValue : changedScrFld.getScreenFieldValues()) {
                    Object o = vce.getNewValue();
                    long id = changedScrFld.getDd().getControlType().getDbid();
                    String fieldVal = "";
                    if (id == DD.CT_DROPDOWN) {
                        String[] fieldExps = changedScrFld.getFieldExpression().split("\\.");
                        fieldVal = ((BaseEntity) o).invokeGetter(fieldExps[fieldExps.length - 1]).toString();
                    } else if (id == DD.CT_LOOKUPSCREEN) {
                        fieldVal = (String) ((BaseEntity) o).invokeGetter(changedScrFld.getFieldExpression());
                    } else {
                        fieldVal = o.toString();
                    }
                    for (DimmedField dimmedField : fieldValue.getDimmedFields()) {
                        String fldName = dimmedField.getScreenField();
                        for (int i = 0; i < sortedFields.size(); i++) {
                            if (sortedFields.get(i).getFieldExpression().equals(fldName)) {
                                long DDdbid = sortedFields.get(i).getDd().getControlType().getDbid();
                                String fieldId = fieldIDs.get(i);
                                if (DDdbid == DD.CT_DROPDOWN || DDdbid == DD.CT_CHECKBOX) {
                                    if (fieldValue.getValue().trim().equals(fieldVal.trim())) {
                                        RequestContext.getCurrentInstance().execute("dimDropDown('frmScrForm:" + fieldId + "')");
                                        dimmedFields.add(fieldId);
                                    } else {
                                        if (!dimmedFields.contains(fieldId)) {
                                            RequestContext.getCurrentInstance().execute("undimDropDown('frmScrForm:" + fieldId + "')");
                                        }
                                    }
                                } else if (DDdbid == DD.CT_LOOKUPSCREEN
                                        || DDdbid == DD.CT_LOOKUPTREE) {
                                    fieldId = sortedFields.get(i).getFieldExpression().replace(".", "_") + "_Lookup__";
                                    if (fieldValue.getValue().trim().equals(fieldVal.trim())) {
                                        RequestContext.getCurrentInstance().execute("dimlookup('frmScrForm:" + fieldId + "')");
                                        dimmedFields.add(fieldId);
                                    } else {
                                        if (!dimmedFields.contains(fieldId)) {
                                            RequestContext.getCurrentInstance().execute("undimlookup('frmScrForm:" + fieldId + "')");
                                        }
                                    }
                                } else {
                                    if (DDdbid == DD.CT_CALENDAR) {
                                        fieldId = fieldId + "_input";
                                    }
                                    if (fieldValue.getValue().trim().equals(fieldVal.trim())) {
                                        RequestContext.getCurrentInstance().execute("dimfield('frmScrForm:" + fieldId + "')");
                                        dimmedFields.add(fieldId);
                                    } else {
                                        if (!dimmedFields.contains(fieldId)) {
                                            RequestContext.getCurrentInstance().execute("undimfield('frmScrForm:" + fieldId + "')");
                                        }
                                    }
                                }
                                RequestContext.getCurrentInstance().update("frmScrForm:" + fieldId);
                                break;
                            }
                        }
                    }
                }
            }

            // <editor-fold defaultstate="collapsed" desc="Change Function">
            if (changedScrFld.getChangeFunction() != null) {
                ODataType changeDT = dataTypeService.loadODataType("EntityChangeField", loggedUser);
                List changeData = new ArrayList();

                changeData.add(loadedObject);
                changeData.add(compFieldExp);
                changeData.add(vce.getNewValue());

                ODataMessage changeODM = new ODataMessage(changeDT, changeData);

                globalOFunctionResult = functionServiceRemote.runFunction(changedScrFld.getChangeFunction(), changeODM, null, loggedUser);
                OFunctionResult ofr = globalOFunctionResult;
                anyValueChanged = true;
                if (ofr.getReturnedDataMessage() != null
                        && ofr.getReturnedDataMessage().getODataType() != null
                        && !ofr.getReturnedDataMessage().getData().isEmpty()
                        && ofr.getReturnedDataMessage().getData().get(0).getClass().getName().
                        equals(getOactOnEntity().getEntityClassPath())) {
                    messagesPanel.clearAndDisplayMessages(ofr);
                    RequestContext.getCurrentInstance().update(messagesPanel.getPanelC().getClientId());
                    loadedObject = (BaseEntity) ofr.getReturnedDataMessage().getData().get(0);
                }
            }
            // </editor-fold>

            Object oldValueObj = vce.getOldValue();
            Object newValueObj = vce.getNewValue();
            applyDepencyInitialization(loadedObject, newValueObj, compFieldExp);

            if (FramworkUtilities.isComponentValueChanged(vce)) {
                // Value Changed
                loadedObject.setChanged(true);
                try {
                    if (compFieldExp.contains(".")) {
                        BaseEntity entity = (BaseEntity) BaseEntity.getValueFromEntity(loadedObject,
                                compFieldExp.substring(0, compFieldExp.lastIndexOf(".")), true);
                        if (entity.getDbid() == 0) {
                            entity.setChanged(true);
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }
            }
            if (anyValueChanged) {
                updateFieldsIDs(vce.getComponent().getId());
            }
            if (loadedObject.getDbid() != 0 && mode == OScreen.SM_ADD) {
                mode = OScreen.SM_EDIT;
            }
            logger.trace("Row Changed Values");

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            OFunctionResult oFR = new OFunctionResult();
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);
            messagesPanel.clearAndDisplayMessages(oFR);
        }
    }

    public void validateUniquness(FacesContext context, UIComponent validate, Object value) {
        logger.debug("Entering");
        try {
            oem.getEM(loggedUser);
            Object oldValue = getValueFromEntity(loadedObject, ((ExpressionFieldInfo) uniqueFields.get(validate.getId())).fieldParsedExpression);
            if ((((BaseEntity) loadedObject).getDbid() != 0 && oldValue != null && !value.equals(oldValue))
                    || ((BaseEntity) loadedObject).getDbid() == 0) {
                if (oem.executeEntityQuery(uniqueFieldsQueries.get(validate.getId())
                        + "'" + value + "'",
                        getLoggedUser()) != null) {
                    ((HtmlInputText) validate).setValid(false);
                    UserMessage userMessage = usrMsgService.getUserMessage("UniqueValidation", loggedUser);
                    HtmlMessage message = new HtmlMessage();
                    message.setTitle(userMessage.getMessageTitleTranslated());
                    message.setStyleClass("inlineErrorMsgTitle");
                    message.setFor(validate.getClientId(context));

                    FacesMessage msg = new FacesMessage(userMessage.getMessageTextTranslated());
                    context.addMessage(validate.getClientId(context), msg);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            //FIXME: how to display error to user, e.g. User Message
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
        }
    }
    List<String> lookupFldExps = new ArrayList<String>();
    List<String> lookupCtrlsExps = new ArrayList<String>();

    @Override
    public void loadAttribute(String expression) {
        logger.debug("Entering");
        lookupCtrlsExps.add(expression);
        if (expression.contains(".")) {
            lookupFldExps.add(expression.substring(0, expression.indexOf(".")));
        }

        List<ExpressionFieldInfo> expressionFldsInfo = null;
        Object frmParentObject = null; // Shouldn't be BaseEntity to be ready for any attribute type
        int expressionFieldsCount = -1;
        int fieldIndex = -1;

        try {
            // Parse field expression in all cases (having "." or not) for code
            // simplicity
            expressionFldsInfo = loadedObject.parseFieldExpression(expression);
            if (expressionFldsInfo == null) {
                logger.warn("Null Expression Field Info");
                logger.debug("Returning");
                return;
            }
            if (expressionFldsInfo.size() < 0) {
                logger.warn("Empty Expression Field Info List");
                logger.debug("Returning");
                return;
            }
            // Check if primitive, no additional processing needed
            if (expressionFldsInfo.get(0).field.getType().isPrimitive()) {
                logger.debug("Returning");
                return;
            }

            // Initialize loop variables
            frmParentObject = loadedObject;
            expressionFieldsCount = expressionFldsInfo.size();
            boolean fieldIsList = false;
            Field expField = null;

            // Loop on the expression fields
            for (fieldIndex = 0; fieldIndex < expressionFieldsCount; fieldIndex++) {
                expField = expressionFldsInfo.get(fieldIndex).field;
                fieldIsList = BaseEntity.getFieldType(expField).getSimpleName().equals("List");
                if (fieldIsList) {
                    logger.debug("Returning");
                    return;
                } else {
                    // Field is not list
                    // No need to load it, as it should be already loaded
                    // $$ OneToX Association Fetch is by default EAGER, take
                    // action if it's LAZY

                    // Set loop variables for next expression field
                    if (fieldIndex == (expressionFieldsCount - 1)) {
                        // Last field in expression
                        // No need for loop variables setting
                        // End the function
                        break;
                    }
                    try {
                        Object fieldValue = ((BaseEntity) frmParentObject).invokeGetter(expField);
                        if (fieldValue == null) {
                            // Probably, a child field is required by JSF
                            /**
                             * This conde is commented due the following
                             * Exeption "java.lang.IllegalStateException: During
                             * synchronization a new object was found through a
                             * relationship that was not marked cascade PERSIST:
                             * OEntity[dbid=0, InstanceID=null]."
                             */
                            ((BaseEntity) frmParentObject).forceFieldEmptyInitialization(
                                    expField.getName());
//                             No need to load its child fields, end the function
                            break;
                        } else {
                            // Field Value Is Not Null
                            // Set the field to be a parent of the next field in
                            // the expression
                            frmParentObject = fieldValue;
                        }
                    } catch (NoSuchMethodException ex) {
                        // No getter for the field, return
                        // <editor-fold defaultstate="collapsed" desc="Log">
                        logger.warn("Field Has No Getter");
                        logger.error("Exception thrown", ex);
                        logger.debug("Returning");
                        return;
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }

    }

    public void validateLookup(ValueChangeEvent vce) {
        logger.debug("Entering");
        if (vce.getNewValue().equals(vce.getOldValue())) {
            logger.debug("Returning");
            return;
        }
        String query = "";
        try {
            List<ExpressionFieldInfo> infos = BaseEntity.parseFieldExpression(
                    Class.forName(getOactOnEntity().getEntityClassPath()), FramworkUtilities.getControlFldExp(vce.getComponent()));
            ExpressionFieldInfo info = infos.get(infos.size() - 1);
            if (null != vce.getNewValue() && "".equals(vce.getNewValue())) {
                FramworkUtilities.setValueInEntity(loadedObject, infos.get(0).fieldName, info.fieldClass.newInstance());
                FramworkUtilities.setValueInEntity(loadedObject, "changed", true);
                logger.debug("Returning");
                return;
            } else {
                query = "SELECT o FROM " + info.fieldClass.getSimpleName() + " o"
                        + " WHERE o." + info.fieldName
                        + " ='" + vce.getNewValue() + "'";
                logger.trace("Query: {}", query);
                BaseEntity entity = (BaseEntity) oem.executeEntityQuery(query, getLoggedUser());
                if (entity == null) {
                    ((HtmlInputText) vce.getComponent()).setValid(false);
                    UserMessage userMessage = usrMsgService.getUserMessage("InvalidValue", loggedUser);
                    HtmlMessage message = new HtmlMessage();
                    message.setTitle(userMessage.getMessageTitleTranslated());
                    message.setStyleClass("inlineErrorMsgTitle");
                    message.setFor(vce.getComponent().getClientId(getContext()));
                    FacesMessage msg = new FacesMessage(userMessage.getMessageTextTranslated());
                    getContext().addMessage(vce.getComponent().getClientId(getContext()), msg);

                } else {
                    FramworkUtilities.setValueInEntity(loadedObject, infos.get(0).fieldName, entity);
                    FramworkUtilities.setValueInEntity(loadedObject, "changed", true);
                }

            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
    }
    private String fileName;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    private int fileProgress;

    public int getFileProgress() {
        return fileProgress;
    }

    public void setFileProgress(int fileProgress) {
        this.fileProgress = fileProgress;
    }

    /**
     * <p>
     * This method is bound to the inputFile component and is executed multiple
     * times during the file upload process. Every call allows the user to finds
     * out what percentage of the file has been uploaded. This progress
     * information can then be used with a progressBar component for user
     * feedback on the file upload progress. </p>
     *
     * @param event holds a InputFile object in its source which can be probed
     * for the file upload percentage complete.
     */
    public void fileUploadProgress(EventObject event) {
    }

    @Override
    public void screenTranslationMenuAction(ActionEvent actionEvent) {
        logger.debug("Entering");
        try {
            oem.getEM(loggedUser);
            ODataType loadedObjectDT = dataTypeService.loadODataType(loadedObject.getClass().getSimpleName(), loggedUser);
            if (loadedObjectDT != null) {
                ODataMessage message = new ODataMessage();
                message.setODataType(loadedObjectDT);
                List data = new ArrayList();
                data.add(loadedObject);
                message.setData(data);
                String openedScreenInstId = generateScreenInstId("FormScreenTranslationSetup");
                SessionManager.setScreenSessionAttribute(openedScreenInstId, OPortalUtil.MESSAGE, message);
                openScreen("FormScreenTranslationSetup", openedScreenInstId);
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
        }

    }

    public void validateNumeric(FacesContext context, UIComponent validate, Object value) {
        boolean isNumeric = (value == null) ? false : FramworkUtilities.isNumber(value.toString());
        if (!isNumeric) {
            ((HtmlInputText) validate).setValid(false);
            UserMessage userMessage = usrMsgService.getUserMessage("NumericValidation", loggedUser);
            HtmlMessage message = new HtmlMessage();
            message.setTitle(userMessage.getMessageTextTranslated());
            message.setStyleClass("inlineErrorMsgTitle");
            message.setFor(validate.getClientId(context));

            FacesMessage msg = new FacesMessage(userMessage.getMessageTitleTranslated());
            context.addMessage(validate.getClientId(context), msg);
        }
    }

    private void createFormScreenFieldControl(HtmlPanelGrid middlePanel,
            ScreenField screenField, int screenFieldIndex, int screenFieldIndexInSection) {
        logger.trace("Entering");
        if (null != screenField.getDd().getDefaultValue()
                && screenField.getDd().getDefaultValue().equalsIgnoreCase("today")) {
            Object FieldValue;
            try {
                FieldValue = BaseEntity.getValueFromEntity(loadedObject, screenField.getFieldExpression());
                if (FieldValue == null) {
                    BaseEntity.setValueInEntity(loadedObject, screenField.getFieldExpression(), timeZoneService.getUserCDT(loggedUser));
                }
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
        }
        HtmlPanelGrid labelGrid = createLabel(screenField.getFieldExpression() + screenFieldIndex, screenField);
        middlePanel.getChildren().add(labelGrid);

        if (invalidInputMode) {
            HtmlPanelGrid grid = new HtmlPanelGrid();

            grid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_grid");
            grid.setCellpadding("0px");
            grid.setCellspacing("0px");
            //<editor-fold defaultstate="collapsed" desc="Tool Tip">
            int i = -1;
            boolean isImage = false;
            if (screenField.getDd().getControlType().getDbid() == DD.CT_IMAGE
                    || screenField.getDd().getControlType().getDbid() == DD.CT_BARCODEIMAGE) {
                isImage = true;
                i = 6;
            }
            if (!(isImage && i % 2 != 0 && i > 0)) {
                if (fABSSetupLocal.loadKeySetup("EnableInlineHelp", loggedUser).isBvalue()
                        && screenField.getDd().isEnableInlineHelp()) {
                    /**
                     * ************************************
                     */
                    HtmlOutputLink outputLink = new HtmlOutputLink();
                    labelGrid.setId("slide" + screenFieldIndex);
                    outputLink.setValue("#");

                    HtmlGraphicImage helpImg = new HtmlGraphicImage();
                    helpImg.setId("helpImg" + screenFieldIndex);
                    helpImg.setUrl("/resources/help.png");

                    outputLink.getChildren().add(helpImg);
                    Tooltip toolTip = new Tooltip();
                    toolTip.setId("toolTip" + screenFieldIndex);
                    toolTip.setFor(labelGrid.getId());
                    String toolTipData
                            = //ddService.getDDLabel("InlineHelpTitle", loggedUser) + " " + " " +
                            screenField.getDd().getInlineHelpTranslated();
                    toolTip.setValue(toolTipData);
                    toolTip.setShowEffect("slide");
                    //toolTip.setHideEffect("slide");
                    toolTip.setStyleClass("tool-tip");

                    labelGrid.setValueExpression("rendered", exFactory.createValueExpression(elContext, "#{" + getBeanName()
                            + ".toolTipRender}", boolean.class));
                    labelGrid.getChildren().add(outputLink);
                    labelGrid.getChildren().add(toolTip);
                }
            }
            //</editor-fold>
            middlePanel.getChildren().add(grid);
            logger.trace("Returning");
            return;
        }

        String measureUnit = BaseEntity.getMeasuarableFieldUnit(screenField.getFieldExpression(), actOnEntityCls);
        if (measureUnit != null) {
            MeasurableFieldControl control = new MeasurableFieldControl(htmlDir,
                    screenField, measureUnit, screenFieldIndex, mode,
                    getBeanName() + ".loadedObject", actOnEntityCls, ddService, getLoggedUser(),
                    oem, filterServiceBean, getBeanName(),
                    exFactory, elContext, FacesContext.getCurrentInstance(), usrMsgService,
                    entitySetupService);

            HtmlPanelGrid measurableCtrl = control.createMeasurableControl();
            measurableCtrl.setCellpadding("0px");
            measurableCtrl.setCellspacing("0px");

            middlePanel.getChildren().add(measurableCtrl);

            getSelectItemsList().put(control.getDropDownMenu().getId(), control.getSelectItems());
        } else {
            int i = -1;
            boolean isImage = false;
            HtmlPanelGrid grid = new HtmlPanelGrid();
            grid.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_grid");
            if (screenField.getDd().getControlType().getDbid() == DD.CT_IMAGE) {
                isImage = true;
                i = 6;
            }
            if (isImage && i % 2 != 0 && i > 0) {
                middlePanel.getChildren().add(new HtmlInputHidden());
                i--;
            } else {
                grid = (HtmlPanelGrid) createScreenControl(CNTRL_INPUTMODE, screenField, getBeanName()
                        + ".loadedObject", screenFieldIndex, "updateRow");
                grid.setCellpadding("0px");
                grid.setCellspacing("0px");
                grid.setColumns(2);
                if (fABSSetupLocal.loadKeySetup("EnableInlineHelp", loggedUser).isBvalue()
                        && screenField.getDd().isEnableInlineHelp()) {
                    grid.setColumns(2);
                    HtmlOutputLink outputLink = new HtmlOutputLink();
                    labelGrid.setId("slide" + screenFieldIndex);
                    outputLink.setValue("#");

                    HtmlGraphicImage helpImg = new HtmlGraphicImage();
                    helpImg.setId("helpImg" + screenFieldIndex);
                    helpImg.setUrl("/resources/help.png");
                    outputLink.getChildren().add(helpImg);
                    Tooltip toolTip = new Tooltip();
                    toolTip.setId("toolTip" + screenFieldIndex);
                    toolTip.setFor(labelGrid.getId());
                    String toolTipData
                            = //ddService.getDDLabel("InlineHelpTitle", loggedUser) + " " +
                            screenField.getDd().getInlineHelpTranslated();
                    toolTip.setValue(toolTipData);
                    toolTip.setShowEffect("slide");
                    //toolTip.setHideEffect("slide");
                    toolTip.setStyleClass("tool-tip");
                    labelGrid.setValueExpression("rendered", exFactory.createValueExpression(elContext, "#{" + getBeanName()
                            + ".toolTipRender}", boolean.class));
                    labelGrid.getChildren().add(outputLink);
                    labelGrid.getChildren().add(toolTip);
                }
            }
            if (screenField.getDd().isBalloon()) {
                CommandButton button = new CommandButton();
                button.setId("openButton_" + screenFieldIndex + "_" + CNTRL_INPUTMODE);
                button.setValue("..");
                button.setImage("/resources/icon.png");
                button.setStyleClass("bln-btn-frm");
                int controlSequence = screenField.getSortIndex();
                if (getCurrentScreen().getScreenControlsGroups() != null && !getCurrentScreen().getScreenControlsGroups().isEmpty()) {
                    controlSequence = screenFieldIndexInSection;
                }
                grid.setStyleClass("bln-grid-panel");
                grid.getChildren().add(button);
                // Don't display the button if the Entity has no DBID, i.e. new row
                String ownerFieldBackBeanExpression = null;
                OEntityDTO balloonEntity = uiFrameWorkFacade.
                        getFieldBallonOEntityDTO(
                                getOactOnEntity(), screenField.getExpInfos(
                                        getOactOnEntity().getEntityClassPath()), loggedUser);
                if (screenField.getExpInfos(getOactOnEntity().getEntityClassPath()) != null) {
                    // Balloon for a field
                    int balloonFieldIndex = 0;
                    try {
                        balloonFieldIndex = uiFrameWorkFacade.getBalloonFieldIndex(
                                Class.forName(balloonEntity.getEntityClassPath()),
                                screenField.getExpInfos(getOactOnEntity().getEntityClassPath()));
                    } catch (ClassNotFoundException ex) {
                        logger.error("Exception thrown", ex);
                    }
                    switch (balloonFieldIndex) {
                        case -2:
                        case -1:
                            // Owner Entity itself
                            ownerFieldBackBeanExpression = getBeanName() + ".loadedObject";
                        default:
                            // A field in the expressionFieldInfos
                            ownerFieldBackBeanExpression = getBeanName() + ".loadedObject";
                            for (int fieldIndex = 0; fieldIndex <= balloonFieldIndex; fieldIndex++) {
                                ownerFieldBackBeanExpression
                                        += "." + screenField.getExpInfos(getOactOnEntity().getEntityClassPath()).get(fieldIndex).fieldName;
                            }
                    }
                }

                OverlayPanel balloonOverLay = new OverlayPanel();
                balloonOverLay = createBalloonPanel(balloonEntity,
                        "_" + screenFieldIndex,
                        false,
                        getBeanName() + ".loadedObject",
                        screenField.getExpInfos(getOactOnEntity().getEntityClassPath()));
                balloonOverLay.setFor(button.getId());
                balloonOverLay.setStyle("display:none;position:absolute;");
                balloonOverLay.setOnShow("hideBreifSeCtion(this.id);");
                balloonOverLay.setAppendToBody(false);
                balloonOverLay.setDynamic(true);
                balloonOverLay.setHideEffect("fade");
                balloonOverLay.setShowEffect("fade");
                balloonOverLay.setId("balloonOverLay" + "_" + screenFieldIndex + "_" + CNTRL_INPUTMODE);
                balloonOverLay.setStyleClass("balloonOverLay");
                grid.getChildren().add(balloonOverLay);
                String renderExpression = "#{"
                        + ownerFieldBackBeanExpression + "==null?'false':"
                        + ownerFieldBackBeanExpression + ".dbid != 0}";
                button.setValueExpression(
                        "rendered",
                        exFactory.createValueExpression(
                                elContext,
                                renderExpression,
                                String.class));

            }
            middlePanel.getChildren().add(grid);

        }
    }

    @Override
    public String refresh() {
        logger.debug("Entering");
        if (currentScreen.isBlockOnLoad()) {
            RequestContext.getCurrentInstance().execute("blockScreen()");
        }
        OScreen screen = getCurrentScreen();
        String portletID = this.getPortletInsId();
        String detail = "";

        try {
            messagesPanel.clearMessages();
            dataMessage = (ODataMessage) getScreenParam(OPortalUtil.MESSAGE);
            validateInputSetMapping();
            processInput();

            for (String id : fieldIDs) {
                if (id.contains("Image")) {
                    int fieldIndex = Integer.parseInt(id.substring(id.lastIndexOf("_") + 1));
                    ScreenField screenField = getCurrentScreen().getScreenFields().get(fieldIndex - 1);
                    setImage((byte[]) getFileFromEntity(getLoadedEntity(), screenField.getFieldExpression()));
                    String imgNameExp = screenField.getFieldExpression().substring(0,
                            screenField.getFieldExpression().lastIndexOf(".")) + "." + "name";
                    String imageName = (String) getFileFromEntity(getLoadedEntity(), imgNameExp);
                    String imagePath = (null == getImage()) ? ""
                            : oimageServiceLocal.getFilePath(getImage(), imageName, loggedUser);
                    if (imagePath.equals("")) {
                        getGraphicImage().setUrl("/resources/default.png");
                    } else {
                        getGraphicImage().setUrl(imagePath);
                    }
                } else {
                    RequestContext.getCurrentInstance().update("frmScrForm:" + id);
                }
            }
            if (headerObject != null && masterField != null) {
                detail = (String) BaseEntity.getValueFromEntity(headerObject, masterField.getFieldExpression());
                if (null != detail
                        && !detail.equals("")) {
                    detail = " - " + detail;
                }
            }
            String title = screen.getHeaderTranslated() + detail;
            RequestContext.getCurrentInstance().execute(
                    "top.FABS.Portlet.setTitle({title:'" + title.replace("\\", "\\\\").replace("\'", "\\'")
                    + "',portletId:'" + portletID + "'});");
            RequestContext.getCurrentInstance().execute(
                    "top.FABS.Portlet.reloadPortlet({portletId:'" + portletID + "'});");

            if (compositeInputRequired) {
                initCurrentScreenInput();
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning with Null");
        return null;
    }

    @Override
    protected List<BaseEntity> getLoadedEntity() {
        return Collections.singletonList(loadedObject);
    }

    protected BaseEntity loadObject(String entityName, ArrayList<String> conditions,
            List<String> fieldExpressions, OUser loggedUser) throws EntityExistsException,
            EntityNotFoundException, NonUniqueResultException, NoResultException,
            OptimisticLockException, RollbackException, TransactionRequiredException, UserNotAuthorizedException {
        BaseEntity returnedEntity;
        conditions.add(OEntityManager.CONFCOND_GET_ACTIVEANDINACTIVE);
        returnedEntity = oem.loadEntity(entityName, conditions, fieldExpressions, loggedUser);

        return returnedEntity;
    }

    @Override
    public void customScreenActionsAction(ActionEvent event) {
        logger.debug("Entering");
        try {
            oem.getEM(loggedUser);
            messagesPanel.clearMessages();
            OFunctionResult oFR = new OFunctionResult();
            checkLookupMandatoryFields(oFR);
            if(oFR.getErrors() != null && oFR.getErrors().size() != 0){
                messagesPanel.addMessages(oFR);
                return;
            }
            
            if (!(event.getComponent() instanceof MenuItem
                    || event.getComponent() instanceof CommandButton)) {
                logger.warn("Component Not Supported In Action");
                messagesPanel.addErrorMessage(
                        usrMsgService.getUserMessage("SystemInternalError", systemUser));
                logger.debug("Returning");
                return;
            }
            UIComponent comp = event.getComponent();
            if (comp.getChildren().isEmpty()) {
                logger.warn("BaseEntity DBID Not Found In Component");
                messagesPanel.addErrorMessage(
                        usrMsgService.getUserMessage("SystemInternalError", systemUser));
                logger.debug("Returning");
                return;
            }
            String dbidAction = ((HtmlInputHidden) comp.getChildren().get(0)).getValue().toString();

            String actionNameFromEvent = event.getComponent().getAttributes().get("actionName").toString();
            List<ScreenAction> screenActions = getCurrentScreen().getScreenActions();
            ScreenAction sa = new ScreenAction();
            if (screenActions != null) {
                for (ScreenAction screenAction : screenActions) {
                    if (screenAction.getAction().getName().equals(actionNameFromEvent)) {
                        sa = screenAction;
                        if (dbidAction.equals("")) {
                            dbidAction = screenAction.getAction().getDbid() + "";
                            break;
                        }
                    }
                }
            }
            if (dbidAction != null && !dbidAction.equals("")) {
                long actionDBID = Long.parseLong(dbidAction);
                OEntityActionDTO action = entitySetupService.getActionDTO(actionDBID, loggedUser);
                if (action != null) {
                    oFR = runActionForEntity(action, loadedObject);
                    messagesPanel.addMessages(oFR);

                    if (getScreenHeight() == null || getScreenHeight().equals("")) {
                        RequestContext.getCurrentInstance().execute(
                                "top.FABS.Portlet.updateIframeSize('" + getPortletInsId() + "Frame');");
                    } else {
                        RequestContext.getCurrentInstance().execute(
                                "top.FABS.Portlet.updateIframeSizeWithHeight({iframeName:'" + getPortletInsId() + "Frame'"
                                + ",screenHeight:'" + getScreenHeight() + "'});");
                    }
                    RequestContext.getCurrentInstance().update(messagesPanel.panelC.getClientId());
                    if (sa != null && sa.isExitAfterActionButton() && oFR.getErrors() != null && oFR.getErrors().isEmpty()) {
                        exit();
                    }
                }
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(
                    usrMsgService.getUserMessage("SystemInternalError", systemUser), ex);

        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
        }

    }

    public void checkLookupMandatoryFields(OFunctionResult oFR) {
        //for manadatory fields
        for (int currentField = 0; currentField < lookupMandatoryFields.size(); currentField++) {
            //FIXME : Execlude Cascade Persist Object
            if (getValueFromEntity(loadedObject, lookupMandatoryFields.get(currentField)) == null
                    || getValueFromEntity(loadedObject, lookupMandatoryFields.get(currentField)).toString().equals("")) {
                try {
                    String lookupID = lookupMandatoryFields.get(currentField).replace(".", "_") + "_Lookup_";
                    String label = lookupMandatoryFields.get(currentField);
                    if (getLookupHashMap().get(lookupID) != null) {
                        label = ((Lookup) getLookupHashMap().get(lookupID)).getFieldDD().getHeaderTranslated();
                    }
                    UserMessage message = usrMsgService.getUserMessage("LookupFieldValidation",
                            Collections.singletonList(label),
                            loggedUser);
                    message.setMessageText(message.getMessageText() + label);
                    exit = false;
                    RequestContext.getCurrentInstance().execute("scroll(0,0)");
                    RequestContext.getCurrentInstance().execute("topOfPage();");
                    oFR.addError(message);
                } catch (Exception ex) {
                    logger.warn("Lookup Validation Message Error");
                    logger.error("Exception thrown", ex);
                }
            }
        }
    }

    //RDO
    public String ReloadDataOnly() {
        String call = "top.FABS.Portlet.setSrc({screenName:'" + this.getCurrentScreen().getName()
                + "',screenInstId:'" + this.getScreenInstId()
                + "',portletId:'" + this.getPortletInsId()
                + "',viewPage:'" + this.getCurrentScreen().getViewPage()
                + "',enableC2A:'" + c2AMode + "'});";
        RequestContext.getCurrentInstance().execute(call);
        return null;
    }
    public String customedDynamicBalloonStyle;

    /**
     * @return the customedDynamicBalloonStyle
     */
    public String getCustomedDynamicBalloonStyle() {
        return customedDynamicBalloonStyle;
    }

    /**
     * @param customedDynamicBalloonStyle the customedDynamicBalloonStyle to set
     */
    public void setCustomedDynamicBalloonStyle(String customedDynamicBalloonStyle) {
        this.customedDynamicBalloonStyle = customedDynamicBalloonStyle;
    }

    @Override
    public void updateFieldsIDs(String updaterFieldID) {
        for (String id : fieldIDs) {
            if (!updaterFieldID.equals(id)) {
                RequestContext.getCurrentInstance().update("frmScrForm:" + id);
            }
        }
    }

    public String openAttachmentScreen() {
        logger.debug("Returning");
        try {
            oem.getEM(loggedUser);
            List conditions = new ArrayList();
            conditions.add("name='AttachmentScreen'");
            FormScreen formScreen = (FormScreen) oem.loadEntity("OScreen", conditions, null, loggedUser);

            if (formScreen == null) {
                logger.warn("Error Getting Basic Detail Screen");
                logger.debug("Returning with Null");
                return null;
            }
            if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("AttachmentForForm" + getCurrentScreen().getInstID()) == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("AttachmentForForm" + getCurrentScreen().getInstID(), getCurrentScreen());
            }
            String openedScreenInstId = formScreen.getInstID() + "";

            // Open the new portlet or screen
            return openScreenInPopup(formScreen, openedScreenInstId, POPUP_SCREENTYPE);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            messagesPanel.addErrorMessage(usrMsgService.getUserMessage("SystemInternalError", systemUser), ex, true);
            logger.debug("Returning with Null");
            return null;
        } finally {
            oem.closeEM(loggedUser);
        }
    }

    public void addClaimButtonToPanelGroup(String buttonID, String buttonLabel,
            String actionMethodName, HtmlPanelGrid panelLayout, Class[] params, String portletInsId, String beanName, String screenLanguage, boolean invalidInputMode, OScreen currentScreen) {
        logger.debug("Entering");
        claimButton = new CommandButton();
        claimButton.setTitle(buttonLabel);
        if (screenLanguage.equalsIgnoreCase("Arabic")) {
            claimButton.setStyle("position:relative;text-align:right;");
        } else {
            claimButton.setStyle("position:relative");
        }
        claimButton.setUpdate("@this");
        claimButton.setDir(htmlDir);
        claimButton.setIcon("none");
        claimButton.setValue(buttonLabel);
        claimButton.setTitle(buttonLabel);
        claimButton.setStyleClass("general_btn");
        if (actionMethodName != null) {
            if (null != params) {
                MethodExpression mb = exFactory.createMethodExpression(elContext,
                        "#{" + getBeanName() + "." + actionMethodName + "}", null, params);
                MethodExpressionActionListener al = new MethodExpressionActionListener(mb);
                claimButton.addActionListener(al);
            } else {
                claimButton.setActionExpression(
                        exFactory.createMethodExpression(
                                elContext,
                                "#{" + getBeanName() + "." + actionMethodName + "}",
                                String.class,
                                new Class[0]));
            }
        }

        String buttonsId = getFacesContext().getViewRoot().createUniqueId();

        HtmlPanelGrid btnPanel = new HtmlPanelGrid();
        btnPanel.setStyle("position:relative;margin:0px;border:0px;padding:0px;");
        btnPanel.setId(buttonsId + "_butPanel");

        HtmlPanelGrid buttonTiltlePanel = new HtmlPanelGrid();
        buttonTiltlePanel.setId(buttonsId + "_butTitlePanel");
        buttonTiltlePanel.setStyleClass("buttonTitleLinkPanel");

        CommandLink buttonTitleLink = new CommandLink();//buttonTitleLink
        buttonTitleLink.setId(buttonsId + "_butTitle");
        buttonTitleLink.setStyleClass("buttonTitleLink");
        buttonTitleLink.setStyle("display:none");
        buttonTitleLink.setValue(buttonLabel);
        buttonTitleLink.setOnclick("top.FABS.Portlet.buttonTitleAction(this.id,'" + getPortletInsId() + "');return false;");

        buttonTitleLink.getChildren().add(buttonTitleLink);
        buttonTitleLink.setStyleClass("buttonTitleLink");

        claimButton.setId(buttonsId + "_button_" + buttonID);

        buttonTiltlePanel.getChildren().add(buttonTitleLink);

        btnPanel.getChildren().add(claimButton);
        btnPanel.getChildren().add(buttonTiltlePanel);

        HtmlPanelGrid grid = new HtmlPanelGrid();
        grid.setColumns(1);
        grid.setId(buttonsId + "_butPanelGrid");
        grid.getChildren().add(btnPanel);
        panelLayout.getChildren().add(claimButton);
        claimButton.setAjax(true);
        logger.debug("Returning");
    }

    public String claimAction(ActionEvent ae) {
        logger.debug("Entering");
        TaskStatus status = humanTaskService.getTaskStatus(processingTask.getId());

        if (!(status.equals(TaskStatus.READY) || status.equals(TaskStatus.RESERVED))) {
            OPortalUtil.removePortlet(getPortletInsId(), screenInstId);
            logger.debug("Returning with empty string");
            return "";
        }
        if (status.equals(TaskStatus.READY)) {
            humanTaskService.claimAndStartTask(processingTask, loggedUser);
        } else if (status.equals(TaskStatus.RESERVED)) {
            humanTaskService.startTask(processingTask, loggedUser);
        }
        ODataMessage out = saveProcessRecords();
        if (out == null || out.getData() == null || out.getData().isEmpty() || !(out.getData().get(0) instanceof BaseEntity)) {
            out = new ODataMessage();
            List<Object> data = new ArrayList<Object>();
            data.add(((SingleEntityBean) this).getLoadedEntity());
            out.setODataType(dataTypeService.loadODataType("VoidDT", loggedUser));
        }
        claimButton.setRendered(false);
        String inboxID = (String) cacheService.getFromCache("inboxID", loggedUser);
        OPortalUtil.renderInbox(getCurrentScreen(), inboxID, out);
//            OPortalUtil.renderScreen(currentScreen, screenInstId, out);
        logger.debug("Returning with empty string");
        return "";
    }

    private boolean isValidDateFormat(String date, String dateFormat) {
        try {
            DateFormat df = new SimpleDateFormat(dateFormat);
            df.setLenient(false);
            df.parse(date);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private Date getDefaultDate(String pattern, String dateValue) {
        Date date = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            date = simpleDateFormat.parse(dateValue);
        } catch (Exception e) {
            return null;
        }
        return date;
    }
}
