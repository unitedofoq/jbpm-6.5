/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.backbean.wizard;


import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.filter.OFilter.FilterType;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.i18n.TextTranslationServiceRemote;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.backbean.OBackBean;
import com.unitedofoq.fabs.core.uiframework.backbean.SessionManager;
import com.unitedofoq.fabs.core.uiframework.backbean.filter.RunFilterBean;
import com.unitedofoq.fabs.core.uiframework.cdi.OEJB;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalUtil;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.SingleEntityScreen;
import com.unitedofoq.fabs.core.uiframework.screen.wizard.Wizard;
import com.unitedofoq.fabs.core.uiframework.screen.wizard.WizardStep;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mmohamed
 */
public class WizardController {
    final static Logger logger = LoggerFactory.getLogger(WizardController.class);
    private OEntityManagerRemote oem;
    private Wizard wizard;
    private List<ScreenWizardInformation> screensWizardInfo;
    private UserMessageServiceRemote userMessageService ;
    
    TextTranslationServiceRemote textTranslationServiceRemote = OEJB.lookup(TextTranslationServiceRemote.class);
    
    private UIFrameworkServiceRemote uiFrameWorkFacade = OEJB.lookup(UIFrameworkServiceRemote.class);
    

    public UserMessageServiceRemote getUserMessageService() {
        return userMessageService;
    }

    public void setUserMessageService(UserMessageServiceRemote userMessageService) {
        this.userMessageService = userMessageService;
    }


    //The Key is the step DBID; and for the einitial DM the key is 0. The value is the output value of every step
    private HashMap<Long,ODataMessage> wizardDMs;

    //no setter, to prevent change from outside
    private int stepIndex; //=0

    public HashMap<Long, ODataMessage> getWizardDMs() {
        return wizardDMs;
    }

    public void setWizardDMs(HashMap<Long, ODataMessage> wizardDMs) {
        this.wizardDMs = wizardDMs;
    }

    public int getStepIndex() {
        return stepIndex;
    }

    private OUser loggedUser ;
    public WizardController(OEntityManagerRemote oem, Wizard wizard, OUser loggedUser, UserMessageServiceRemote userMessageService){
        logger.debug("Entering");
        this.userMessageService = userMessageService;
        this.oem = oem;
        this.wizard = wizard;
        this.loggedUser = loggedUser ;

        wizardDMs = new HashMap<Long, ODataMessage>();       

        screensWizardInfo = new ArrayList<ScreenWizardInformation>();

        // Set the conditions
            List<String> conditions = new ArrayList<String>() ;
            conditions.add("wizard.dbid = " + wizard.getDbid());

            // Set the sorts
            List<String> sorts = new ArrayList<String>() ;
            sorts.add("stepIndex") ;
        try {
            List<WizardStep> wizardSteps = oem.loadEntityList
                    (WizardStep.class.getSimpleName(),
                    conditions,
                    null,
                    sorts,
                    oem.getSystemUser(loggedUser));
            
            boolean finishButtonEnabled = true;


            //The loop is inversed to set the skippable flag
            for (int i = wizardSteps.size()-1 ; i >= 0 ; i--) {
                ScreenWizardInformation information = new ScreenWizardInformation();
                information.setStep(wizardSteps.get(i));
                screensWizardInfo.add(0,information);

                information.setEnableFinish(finishButtonEnabled);
                 
                if(finishButtonEnabled){
                    finishButtonEnabled = wizardSteps.get(i).isSkippable();
                }
                
                information.setEnableSkip(wizardSteps.get(i).isSkippable());

                information.setEnableBack(i!=0);
                if(i == wizardSteps.size()-1 ){
                    information.setEnableNext(false);
                }else{
                    information.setEnableNext(true);
                }
            }
           
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }       
        finally{
            logger.debug("Returning");
        }
    }

    
    private void openWizardStep(WizardStep step,String portalInsID ,int index){
        logger.trace("Entering");
        String screenName = step.getStepScreen().getName();
        long screenMode = step.getStepMode()!=null? step.getStepMode().getDbid() : null;
        String viewPage = step.getStepScreen().getViewPage();
        String newScreenInstId = OBackBean.generateScreenInstId(screenName)+"_wizard";
        //Clear the data of every step
        logger.trace("Clear the data of every step");
        if(getWizardDMs().get(step.getDbid()) != null){
            getWizardDMs().remove(step.getDbid());
        }

        SessionManager.setScreenSessionAttribute(newScreenInstId, OBackBean.MODE_VAR, screenMode);
        SessionManager.setScreenSessionAttribute(newScreenInstId, OBackBean.WIZARD_VAR, true);        
        SessionManager.setScreenSessionAttribute(newScreenInstId, OBackBean.WIZARD_INFO_VAR, getScreensWizardInfo().get(index));
        SessionManager.setScreenSessionAttribute(newScreenInstId, OBackBean.WIZARD_CRTL_VAR, this);


        boolean runFilter = false;
            OScreen screen = step.getStepScreen();
            if (screen.getScreenFilter() != null 
                    && !screen.getScreenFilter().isInActive()) {
                for (int i = 0; i < screen.getScreenFilter().getFilterFields().size(); i++) {
                    if(screen.getScreenFilter().getFilterFields().get(i).isInActive())
                        continue;

                    if (screen.getScreenFilter().getFilterFields().get(i).getFieldValue() == null ||
                            screen.getScreenFilter().getFilterFields().get(i).getFieldValue().equals("")) {
                        runFilter = true;
                    }
                }
                runFilter |= screen.getScreenFilter().isAlwaysShow();
            }

            
            if (runFilter) {
            try{
                String filterScreenName = "RunFilter";
                OScreen filterScreen = (OScreen) oem.loadEntity(OScreen.class.getSimpleName(), Collections.singletonList("name = '" + filterScreenName+"'"), null, loggedUser);
                String filterScreenInstId= OBackBean.generateScreenInstId(filterScreen.getName());

                if(screen instanceof SingleEntityScreen)
                SessionManager.setScreenSessionAttribute(filterScreenInstId, RunFilterBean.PASSED_VAR, ((SingleEntityScreen)screen));
                SessionManager.setScreenSessionAttribute(filterScreenInstId, RunFilterBean.FILTER_TYPE_VAR, FilterType.SCREEN_FILTER);
                SessionManager.setScreenSessionAttribute(filterScreenInstId, RunFilterBean.TO_B_OPENED_SCREEN_INST_ID_VAR, newScreenInstId);
                SessionManager.setScreenSessionAttribute(filterScreenInstId, OBackBean.WIZARD_VAR, true);
                SessionManager.setScreenSessionAttribute(filterScreenInstId, OBackBean.WIZARD_INFO_VAR, getScreensWizardInfo().get(index));
                SessionManager.setScreenSessionAttribute(filterScreenInstId, OBackBean.WIZARD_CRTL_VAR, this);

                try {
                   filterScreen = (OScreen) textTranslationServiceRemote.loadEntityTranslation(filterScreen, loggedUser);
                } catch (Exception ex) {
                    logger.error("Exception thrown: ", ex);
                }
                logger.trace("Calling JS setSrc");
                RequestContext.getCurrentInstance().execute(
                "top.FABS.Portlet.setSrc({screenName:'"+filterScreen.getName() 
                        + "',screenInstId:'" + filterScreenInstId
                        + "',portletId:'" + portalInsID
                        +"',viewPage:'"+ filterScreen.getViewPage() 
                        + "',portletTitle:'"+ filterScreen.getHeaderTranslated() 
                        + "',enableC2A:'false'});");
                }catch(Exception ex){
                    logger.error("Exception thrown: ", ex);
                }
            } else {
                logger.trace("Calling JS setSrc");
                RequestContext.getCurrentInstance().execute(
                "top.FABS.Portlet.setSrc({screenName:'" + screenName
                        + "',screenInstId:'" + newScreenInstId
                        + "',portletId:'" + portalInsID
                        + "',viewPage:'" + viewPage
                        + "',enableC2A:'fasle'});");
            }  
            logger.trace("Returning");
    }

    private void incrementStepIndex(){
        stepIndex++;
    }

    private void decrementStepIndex(){
        stepIndex--;
    }

    public void next(String portalInsID ,ODataMessage addedEntityDM){
        
        WizardStep currentStep = getScreensWizardInfo().get(stepIndex).getStep();

        WizardStep nextStep = getScreensWizardInfo().get(stepIndex+1).getStep();

        incrementStepIndex();

        getWizardDMs().put(currentStep.getDbid(), addedEntityDM);

        openWizardStep(nextStep,portalInsID,stepIndex);
        
    }

    public void back(String portalInsID ){        

        WizardStep previuosStep = getScreensWizardInfo().get(stepIndex-1).getStep();

        decrementStepIndex();

        openWizardStep(previuosStep,portalInsID,stepIndex);

    }

    public void skip(String portalInsID,String screenInstId){

        //FIXME : IMP: is there a step skippable & must select at the same time ... this will leads to send a datamessage when skip
        //The assumbtion is that no step is skippable and mustSelect

        WizardStep currentStep = getScreensWizardInfo().get(stepIndex).getStep();

        if(stepIndex == getScreensWizardInfo().size() -1){
            OPortalUtil.removePortlet(portalInsID, screenInstId);
        }else{
            WizardStep nextStep = getScreensWizardInfo().get(stepIndex+1).getStep();

            incrementStepIndex();

            openWizardStep(nextStep,portalInsID,stepIndex);
        }        
    }

    public void finish(String portalInsID, String screenInstId){
        WizardStep currentStep = getScreensWizardInfo().get(stepIndex).getStep();
        OPortalUtil.removePortlet(portalInsID, screenInstId);
    }

    public List<ScreenWizardInformation> getScreensWizardInfo() {
        return screensWizardInfo;
    }

    public void setScreensWizardInfo(List<ScreenWizardInformation> screenWizardInfo) {
        this.screensWizardInfo = screenWizardInfo;
    }

    public Wizard getWizard() {
        return wizard;
    }

    public void setWizard(Wizard wizard) {
        this.wizard = wizard;
    }

    public ODataMessage getStepDataMessage(){
        logger.debug("Entering");
        if(stepIndex == 0 && getWizardDMs().get(0L) != null){
            logger.debug("Returning");
            return getWizardDMs().get(0L);
        }else{
            ODataType dataType = getScreensWizardInfo().get(stepIndex).getStep().getStepDT();
            Object dMPool [] =  getWizardDMs().values().toArray();
            for (int i = 0; i < dMPool.length; i++) {
                if(dMPool[i]!=null && ((ODataMessage)dMPool[i]).getODataType().getDbid() == dataType.getDbid()){
                    logger.debug("Returning");
                    return (ODataMessage) dMPool[i];
                }
            }
            
        }
        logger.debug("Returning with NULL");
        return null;
    }

    public OFunctionResult openWizard(ODataMessage initDM){
        logger.debug("Entering");
        OFunctionResult ofr = new OFunctionResult();
        try{
        if( getScreensWizardInfo().isEmpty())
        {
            logger.debug("Returning");
            return ofr;
        }

        if(initDM != null){
            getWizardDMs().put(0L, initDM);
        }

        WizardStep firstStep = getScreensWizardInfo().get(0).getStep();

        String screenName = firstStep.getStepScreen().getName();
        long screenMode = firstStep.getStepMode()!=null? firstStep.getStepMode().getDbid() : null;        
        String screenInstId=OBackBean.generateScreenInstId(screenName)+"_wizard";
        
        SessionManager.setScreenSessionAttribute(screenInstId, OBackBean.MODE_VAR, screenMode);
        SessionManager.setScreenSessionAttribute(screenInstId, OBackBean.WIZARD_VAR, true);
        SessionManager.setScreenSessionAttribute(screenInstId, OBackBean.WIZARD_INFO_VAR, getScreensWizardInfo().get(0));
        SessionManager.setScreenSessionAttribute(screenInstId, OBackBean.WIZARD_CRTL_VAR, this);
        logger.trace("Calling JS setSrc");
        RequestContext.getCurrentInstance().execute(
                "top.FABS.Portlet.open({screenName:'"+ firstStep.getStepScreen().getName()
                    + "',screenInstId:'"+ screenInstId
                    + "',viewPage:'"+ firstStep.getStepScreen().getViewPage()
                    + "',portletTitle:'"+ wizard.getWizardTitleTranslated()
                    + "',enableC2A:'false'});");
        }catch (Exception ex) {
            logger.error("Exception thrown",ex);
            ofr.addError(userMessageService.getUserMessage(
                "SystemInternalError", loggedUser), ex);
        }
        logger.debug("Returning");
        return ofr ;
    }
}
