/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.notification;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.UserNotificationEventLocalServiceUtil;
import com.unitedofoq.fabs.core.notification.DockBarNotification;
import com.unitedofoq.fabs.core.notification.DockBarUserNotificationHandler;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author mmasoud
 */
@ApplicationScoped
public class DockBarNotificationAction {

    public void addNotificationEvent(DockBarNotification notification) {
        try {
            ServiceContext serviceContext = new ServiceContext();
            List<User> users = UserLocalServiceUtil.getUsers(0,
                    UserLocalServiceUtil.getUsersCount());
            String notificationText = notification.getNotificationText();
            String userName = notification.getUserName();
            for (User user : users) {
                if (user.getScreenName().equals(userName)) {
                    com.liferay.portal.kernel.json.JSONObject payloadJSON = JSONFactoryUtil.createJSONObject();
                    payloadJSON.put("userId", user.getUserId());
                    payloadJSON.put("notificationText", notificationText);
                    UserNotificationEventLocalServiceUtil.addUserNotificationEvent(
                            user.getUserId(),
                            DockBarUserNotificationHandler.PORTLET_ID,
                            (new Date()).getTime(), user.getUserId(),
                            payloadJSON.toString(), false, serviceContext);
                }
            }
        } catch (PortalException | SystemException ex) {
            Logger.getLogger(DockBarNotificationAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
