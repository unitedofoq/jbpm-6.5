<%@page import="javax.naming.InitialContext"%>
<%@page import="com.unitedofoq.fabs.core.setup.FABSPropertiesServiceLocal"%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%
        //   Report Output Format
        String outputFormat = (String) request.getParameter("output");
        if (outputFormat == null)
            outputFormat = "";
        outputFormat = outputFormat.toLowerCase();

        //  Report Name
        String reportName = (String) request.getParameter("report");
        if (reportName == null)
            reportName = "";
        
        // ContextName
        String contextName = ((FABSPropertiesServiceLocal)(new InitialContext())
                .lookup("java:global/ofoq/com.unitedofoq.fabs.core.setup.FABSPropertiesServiceLocal"))
                .getContextName(); 
%>

<html>

  <head>
    <title><%=reportName%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>

  <body>
     
      <a href='<%=contextName%>Report?output=xls&report=<%=reportName%>' title='Export the report to Excel'>
          <img width='30' height='30' src='<%=contextName%>resources/icons/excel-icon.png' alt='Excel' border='0' />
      </a>
      <a href='<%=contextName%>Report?output=csv&report=<%=reportName%>' title='Export the report to CSV'>
          <img width='30' height='30' src='<%=contextName%>resources/icons/csv-icon.png' alt='CSV' border='0' />
      </a>
      <iframe width='100%' height='800px' src='<%=contextName%>Report?output=<%=outputFormat%>&report=<%=reportName%>' />


    </body>

</html>
