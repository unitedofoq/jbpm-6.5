<%@page import="javax.naming.InitialContext"%>
<%@page import="com.unitedofoq.fabs.core.setup.FABSPropertiesServiceLocal"%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%
    //   Report Output Format
    String outputFormat = (String) request.getParameter("output");
    if (outputFormat == null) {
        outputFormat = "";
    }
    outputFormat = outputFormat.toLowerCase();

    //  Report Name
    String reportName = (String) request.getParameter("report");
    if (reportName == null) {
        reportName = "";
    }

    // ContextName
    String contextName = ((FABSPropertiesServiceLocal) (new InitialContext())
            .lookup("java:global/ofoq/com.unitedofoq.fabs.core.setup.FABSPropertiesServiceLocal"))
            .getContextName();
%>

<html>
    <head>
        <title><%=reportName%></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <iframe id="iframe" width='100%' height='800px' style="overflow:hidden " src='<%=contextName%><%=request.getSession().getAttribute("reportPath")%>' />
    </body>
</html>
