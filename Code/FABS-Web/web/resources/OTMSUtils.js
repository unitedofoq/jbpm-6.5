/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery.noConflict();
jQuery(document).ready(function(){
    //balloon code
    var openbuttonID = 'openBaloonButton';
    var closebuttonID = 'closeBaloonButton';
    var baloonDivId = 'popDiv'

    var openButton = jQuery('[id$='+openbuttonID+']');
    var closeButton = jQuery('[id$='+closebuttonID+']');
    var balloon = jQuery('[id$='+baloonDivId+']');
    balloon.hide();
    var divID
    openButton.click(function(event){
        var currentopenbuttonID = this.id;
        divID = currentopenbuttonID.substr(0, currentopenbuttonID.length - 16) + "popDiv";
        var balloonDiv = jQuery('[id$='+divID+']');
        balloon.hide();
        //style="display: none"
        balloonDiv.show();
        balloonDiv.css("position", "absolute");
        deselectBalloontable(":actionsDataTable")
        return false;
    });

    closeButton.click(function(event){
        var balloonDiv = jQuery('[id$='+divID+']');
        balloon.hide();
        balloonDiv.hide();
        return false;
    });

    //General Calendar
    //alert(' just in ');
    jQuery(":input:text[id*='calendar']").datepicker({
        dateFormat: 'dd/mm/yy',
        yearRange: '1980:2040'
    });
    //Birth Date Calendar
    jQuery(":input:text[id*='birthCalendar']").datepicker({
        dateFormat: 'dd/mm/yy',
        yearRange: '1920:1994'
    });

    
    //adding new row code for textfield
    var rowIndex = 0;
    var pattern = 'first_row';
    var comp = jQuery('[id$='+pattern+']');
    comp.each(function(i,o){
        var id = o.id;
        id = id.substring(0,id.lastIndexOf(':'));
        id = id.substring(0,id.lastIndexOf(':'));
        var trgIdIndex = id;
        id = id.substring(0,id.lastIndexOf(':'));
        var trgId = id;
        id = id.substring(0,id.lastIndexOf(':'));
        var tabId = id + ':_table';
        if(trgIdIndex == trgId +':0'){
            jQuery(o).keydown(
                function(event){
                    var lastRowAct  = jQuery('[id$='+trgIdIndex+']');
                    var newRow = jQuery('[id$='+trgIdIndex+']').clone(true);
                    var drdComps =new Array();
                    var drdValues =new Array();
                    var z = 0;
                    
                    lastRowAct.contents().find(":input").each(function(x,y){
                        var componentID = y.id;
                        var componentType = y.type;
                        if(componentType.match('select-one')){
                            drdComps[z] = y.id;
                            drdValues[z] = y.value;
                            z++;
                        }
                    });

                    newRow.contents().find(":input").attr('id', function(arr){
                        var oldId = this.id;
                        var newRowId;
                        if(oldId.match('_JSCS_')){
                            newRowId = oldId.substr(0, oldId.length - 1) + rowIndex
                        }else{
                            newRowId = oldId + "_JSCS_" + rowIndex;
                        }
                        return newRowId;
                    });
                    newRow.contents().find(":input").attr('name', function(arr){
                        var oldId = this.id;
                        var newRowId;
                        if(oldId.match('_JSCS_')){
                            newRowId = oldId.substr(0, oldId.length - 1) + rowIndex
                        }else{
                            newRowId = oldId + "_JSCS_" + rowIndex;
                        }
                        return newRowId;
                    });
                    newRow.contents().find(":input").attr('value', "");
                    
                    lastRowAct.contents().find(":input").unbind('keydown');
                    lastRowAct.contents().unbind('keydown',function(e){});
                    lastRowAct.contents().find(":input").unbind('focus');
                    
                    lastRowAct.attr("id", function(arr){
                        return arr + "_JSCS_";
                    });

                    
                    jQuery('[id$='+tabId+']').append(newRow);
                    lastRowAct.before(newRow);
                    jQuery(this).focus();
                    if(rowIndex == 0){
                        for(var r in drdComps){
                            var tmpCompID = drdComps[r];
                            var tmpCompVal = drdValues[r];
                            jQuery('[id$='+tmpCompID+']').attr("value", tmpCompVal);
                        }
                    }
                    //For adding the calendar code for the second time you have to remove the pre assigned class first
                    jQuery("[id*='calendar']").removeClass('hasDatepicker').datepicker({
                        dateFormat: 'dd/mm/yy',
                        yearRange: '1980:2040'
                    });
                    jQuery("[id*='calendar']").removeClass('hasDatepicker').datepicker({
                        dateFormat: 'dd/mm/yy',
                        yearRange: '1920:1994'
                    });
                    rowIndex++;
                });
        }
    });


    //adding new row code for dropdown
    var rowIndex_drd = 0;
    var pattern_drd = 'first_row_drd';
    var comp_drd = jQuery('[id$='+pattern_drd+']');
    comp_drd.each(function(a,b){
        var id_drd = b.id;
        id_drd = id_drd.substring(0,id_drd.lastIndexOf(':'));        
        id_drd = id_drd.substring(0,id_drd.lastIndexOf(':'));
        var trgIdIndex_drd = id_drd;
        id_drd = id_drd.substring(0,id_drd.lastIndexOf(':'));       
        var trgId_drd = id_drd;
        id_drd = id_drd.substring(0,id_drd.lastIndexOf(':'));
        var tabId_drd = id_drd + ':_table';
        if(trgIdIndex_drd == trgId_drd +':0'){
            jQuery(b).change(
                function(event){
                    var lastRowAct_drd  = jQuery('[id$='+trgIdIndex_drd+']');
                    var newRow_drd = jQuery('[id$='+trgIdIndex_drd+']').clone(true);
                    var drdComps =new Array();
                    var drdValues =new Array();
                    var z = 0;
                    lastRowAct_drd.contents().find(":input").each(function(x,y){
                        var componentID = y.id;
                        var componentType = y.type;
                        if(componentType.match('select-one')){
                            drdComps[z] = y.id;
                            drdValues[z] = y.value;
                            z++;
                        }
                    });
                    newRow_drd.contents().find(":input").attr('id', function(arr){
                        var oldId = this.id;
                        var newRowId;
                        if(oldId.match('_JSCS_')){
                            newRowId = oldId.substr(0, oldId.length - 1) + rowIndex_drd
                        }else{
                            newRowId = oldId + "_JSCS_" + rowIndex_drd;
                        }
                        return newRowId;
                    });
                    newRow_drd.contents().find(":input").attr('name', function(arr){
                        var oldId = this.id;
                        var newRowId;
                        if(oldId.match('_JSCS_')){
                            newRowId = oldId.substr(0, oldId.length - 1) + rowIndex_drd
                        }else{
                            newRowId = oldId + "_JSCS_" + rowIndex_drd;
                        }
                        return newRowId;
                    });

                    lastRowAct_drd.attr("id", function(arr){
                        return arr + "_JSCS_";
                    });
                    jQuery('[id$='+tabId_drd+']').append(newRow_drd);
                    lastRowAct_drd.before(newRow_drd);
                    jQuery(this).focus();
                    if(rowIndex_drd == 0){
                        for(var r in drdComps){
                            var tmpCompID = drdComps[r];
                            var tmpCompVal = drdValues[r];
                            jQuery('[id$='+tmpCompID+']').attr("value", tmpCompVal);
                        }
                    }
                    rowIndex_drd++;
                    jQuery("[id*='calendar']").removeClass('hasDatepicker').datepicker({
                        dateFormat: 'dd/mm/yy',
                        yearRange: '1980:2040'
                    });
                    jQuery("[id*='calendar']").removeClass('hasDatepicker').datepicker({
                        dateFormat: 'dd/mm/yy',
                        yearRange: '1920:1994'
                    });
                    jQuery(this).unbind('change');
                    jQuery(b).unbind('change');
                });
        }
    });



                        
// alert("Index:"+lsndex);
//  alert("tableName after:"+tableName);
    
});





function submitPage(id)
{
    var lst=id.lastIndexOf("tableColumn");
    var buttonId=id.substr(0,lst);
    var button=document.getElementById(buttonId+'tableColumn1:SubmitButton');
    var temp=buttonId.substr(0, buttonId.lastIndexOf(":"));
    temp=temp.substr(buttonId.lastIndexOf(":")-1,temp.length);
    if(temp!=0)
    {
        if(button.getProps().value!=0)
        {
            button.click();
        }
    }

}
// Function to deselect Balloon action table
function deselectBalloontable(tableID){
    var tbl = jQuery('[id$='+tableID+']');
    var tblRows = tbl[0].rows;
    var index=0;
    for(index=0;index<tblRows.length;index++)
    {
        var tblCells=tblRows[index].cells;
        var idx=0;
        for(idx=0;idx<tblCells.length;idx++){

            tblCells[idx].style.backgroundColor="#FFFFFF";

        }
    }
}
//Function to change Style of selected row
function changeStyleOfSelectedRow(id,selectedrow)
{
    var lst=id.lastIndexOf(":tableColumn");
    var cId=id.substr(0,lst);
    var button=document.getElementById(cId+':tableColumn1:SubmitButton');
    cId=cId.substr(cId.lastIndexOf(":")+1, cId.length);
    cId=parseInt(cId, 10) +3;
    id=id.substr(0, id.lastIndexOf(":tableRowGroup1"))+":_table";
    if(button.getProps().value!=0)
    {
        if(selectedrow==-1)
        {
            selectedrow=cId;
            var temp=button.getProps().value;
            selectedrow=parseInt(temp,10)+3;
        }
    }
    if(selectedrow!=-1)
    {
        if(selectedrow!=cId)
        {
            deselectRowInTable(id,selectedrow);
        }
    }
    selectRowInTable(id, cId);
    selectedrow=cId;
    return selectedrow;
}
//Function to deselect Specific row in table
function deselectRowInTable(tableID,rowIndx){
    var tbl = document.getElementById(tableID);
    var tblCells=tbl.rows[rowIndx].cells;
    var idx=0;
    for(idx=0;idx<tblCells.length;idx++){

        tblCells[idx].style.backgroundColor="#FFFFFF";
    }

}
//Function to select Specific row in table
function selectRowInTable(tableID,rowIndx){
    var tbl = document.getElementById(tableID);
    var tblCells=tbl.rows[rowIndx].cells;
    var idx=0;
    for(idx=0;idx<tblCells.length;idx++){
        tblCells[idx].style.backgroundColor="#90B7D0";
    }
}
function closeBallon(){
    var baloonDivId = 'popDiv';
    var balloonDiv = jQuery('[id$='+baloonDivId+']');
    // balloon.hide();
    balloonDiv.hide();

    return false;
}
    //    alert("I am in first Row: "+theFirstRow+ " actionID:"+actionID);
        //    alert("innnnn");
