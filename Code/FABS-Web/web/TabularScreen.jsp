<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Document   : Page1
    Created on : Jun 20, 2010, 12:18:21 PM
    Author     : nsaleh
-->
<jsp:root version="2.0" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:ice="http://www.icesoft.com/icefaces/component" xmlns:jsp="http://java.sun.com/JSP/Page">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>
        <ice:outputStyle href="./resources/stylesheet.css" id="outputStyle1"/>
        <ice:outputStyle href="./xmlhttp/css/xp/xp.css" id="outputStyle2"/>
        <ice:outputStyle href="/resources/OTMSstyles.css" id="otmsStyle"/>
        <ice:portlet>
            <ice:form id="form1">
                <table width="100%">
                    <tr>
                        <td>
                            <ice:panelLayout id="leftPanel"/>
                        </td>
                        <td>
                            <ice:panelGrid id="generalPanel"/>
                        </td>
                        <td>
                            <ice:panelLayout id="rightPanel"/>
                        </td>
                    </tr>
                </table>
            </ice:form>
        </ice:portlet>
    </f:view>
</jsp:root>
