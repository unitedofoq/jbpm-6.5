<%@page import="java.io.Serializable"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="com.unitedofoq.fabs.core.setup.FABSPropertiesServiceLocal"%>
<%@page import="com.unitedofoq.fabs.core.utils.ObjectEncoder"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%
    //   Report Output Format
    String outputFormat = (String) request.getParameter("output");
    if (outputFormat == null) {
        outputFormat = "";
    }
    outputFormat = outputFormat.toLowerCase();

    //  Report Name
    String reportName = (String) request.getParameter("report");
    if (reportName == null) {
        reportName = "";
    }

    // ContextName
    String contextName = ((FABSPropertiesServiceLocal) (new InitialContext())
            .lookup("java:global/ofoq/com.unitedofoq.fabs.core.setup.FABSPropertiesServiceLocal"))
            .getContextName();
    String inputFilePath=request.getParameter("inputFilePath");
    request.getSession().setAttribute("inputFilePath",inputFilePath);
%>

<html>
    <head>
        <title><%=reportName%></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <form method="POST" action="CustomReportServlet">
            <input type="submit" name="Export" value="Excel" />
            <input type="submit" name="Export" value="CSV" />
            <input type="submit" name="Export" value="PDF" />
            <input type="submit" name="Export" value="Text" />
            <input type="hidden" name="inputFilePath" value="<%=request.getParameter("inputFilePath")%>" />
            <input type="hidden" name="reportName" value="<%=ObjectEncoder.encodeObjectToBase64((Serializable)session.getAttribute("reportName"))%>" />
            <input type="hidden" name="EntityAttachment" value="<%=ObjectEncoder.encodeObjectToBase64((Serializable)session.getAttribute("EntityAttachment"))%>">
            <input type="hidden" name="parametersMap" value="<%=ObjectEncoder.encodeObjectToBase64((Serializable)session.getAttribute("parametersMap"))%>">
            <input type="hidden" name="Filters" value="<%=ObjectEncoder.encodeObjectToBase64((Serializable)session.getAttribute("Filters"))%>">
            <input type="hidden" name="localized" value="<%= ObjectEncoder.encodeObjectToBase64((session.getAttribute("localized")!= null)?(Serializable)session.getAttribute("localized").toString():"false")%>"/>
            <input type="hidden" name="revertLang"value="<%= ObjectEncoder.encodeObjectToBase64((session.getAttribute("revertLang")!= null)?(Serializable)session.getAttribute("revertLang").toString():"false")%>"/>
            <input type="hidden" name="applyingFilter"value="<%= ObjectEncoder.encodeObjectToBase64((session.getAttribute("applyingFilter")!= null)?(Serializable)session.getAttribute("applyingFilter").toString():"false")%>"/>
            <input type="hidden" name="fitPage"value="<%= ObjectEncoder.encodeObjectToBase64((session.getAttribute("fitPage")!= null)?(Serializable)session.getAttribute("fitPage").toString():"false")%>"/>
            <input type="hidden" name="<%="UserExitJavaFunction"%>"value="<%=ObjectEncoder.encodeObjectToBase64((Serializable)session.getAttribute("UserExitJavaFunction"))%>"/>
            
            <iframe id="iframe" width='100%' height='800px' style="overflow:hidden " src='CustomReportServlet?reportName=<%=reportName%>' />
            <!--<iframe id="iframe" width='100%' height='800px' style="overflow:hidden " src='<%=contextName%><%=request.getSession().getAttribute("reportPath")%>' />-->
        </form>
    </body>
</html>
