<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%
        session.setAttribute("userID", request.getAttribute("userID"));
        session.setAttribute("userPassword", request.getAttribute("userPassword"));
        session.setAttribute("pILayout", request.getAttribute("pILayout"));
        session.setAttribute("layoutPLID", request.getAttribute("layoutPLID"));
        String screenHeight="150px";
        String screenWidth ="100%";
        String onloadFunction = request.getAttribute("screenName") + "resizeIframe()";
        if (request.getAttribute("screenHeight") != null) {
            screenHeight = (String) request.getAttribute("screenHeight")+"px";
            onloadFunction = "";
        }
        String screenName = (String) request.getAttribute("screenName");
        String iframeName = "iframe" + screenName;
        String iframeNameSrc = "iframe" + screenName+"src";
%>
<div id="portletDIV">
    <iframe id="iframe<%=request.getAttribute("screenName")%>" name="iframe<%=request.getAttribute("screenName")%>" marginheight="1"  marginwidth="1" frameborder="0" width="<%=screenWidth%>" height="<%=screenHeight%>" onload="<%=onloadFunction%>">
    </iframe>
    
</div>
<script type="text/javascript">
    /*<![CDATA[*/
    function closeOTMSPortlet(portletDivID){
        var portletDiv = document.getElementById(portletDivID);
        var portletOptions = null;
        Liferay.Portlet.close(portletDiv, true, portletOptions);
    }
    function openOTMSPortlet(screenName, viewPage, pTitle){
        //console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
        //console.log('Before Adding Portlet');
        //console.log(screenName);
        //console.log(viewPage);
        //console.log(pTitle);
        //console.log('End Vars');
        var sortColumns = Liferay.Layout.Columns.sortColumns;
        var plid = themeDisplay.getPlid();
        var targetColumn = '1';
        var placeHolder = jQuery('<div class=\"loading-animation\" />');

        var onComplete = function(data) {
                try{
                    //console.log('RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR');
                    //console.dir(data);
                    //console.log('RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR');
                    jQuery.post( '/OTMS-Demo/test.iface',
                    {"ajaxaction":"setPortletPre","portletId":data.portletId,
                        "screenName":screenName,"viewPage":viewPage, "pTitle": pTitle},
                            function(responseText){
                                Liferay.Portlet.refresh(data);
                            },"html"
                        );                     
                }catch(err){
                    alert(err);
                }
            };
        var beforePortletLoaded = null;

        sortColumns.filter(':eq(' + targetColumn + ')').prepend(placeHolder);

        var portletOptions = {
            beforePortletLoaded: beforePortletLoaded,
            onComplete: onComplete,
            plid: plid,
            portletId: 'OTMSPortlet_WAR_OTMS-Demo',
            placeHolder: placeHolder
        };
        var portletInsID = Liferay.Portlet.add(portletOptions);
    }

    Liferay.bind('closePortlet', function(event, data){
        var portletID = data.portletId;
        if (portletID.indexOf('OTMS') != -1){
            removePortletFromServer(portletID);
        }
    });
    
    function removePortletFromServer(portletId){
        jQuery.post( '/OTMS-Demo/test.iface', 
            {"ajaxaction":"removePortlet","portletId":portletId},
            function(responseText){},"html");
    }

    function <%=request.getAttribute("screenName")%>maximizeIframe(iframe) {
		var winHeight = 0;
		if (typeof(window.innerWidth) == 'number') {
			winHeight = window.innerHeight;
		}
		else if ((document.documentElement) &&
				 (document.documentElement.clientWidth || document.documentElement.clientHeight)) {

			winHeight = document.documentElement.clientHeight;
		}
		else if ((document.body) &&
				 (document.body.clientWidth || document.body.clientHeight)) {
			winHeight = document.body.clientHeight;
		}
		iframe.height = (winHeight - 139);
	}

	function <%=request.getAttribute("screenName")%>resizeIframe() {
		<%if (request.getAttribute("screenHeight") == null) {%>
        var iframe = document.getElementById('iframe<%=request.getAttribute("screenName")%>');

		var height = null;
		var pHeight = 560;

		try {
            
			if (iframe.contentWindow.document.body.scrollHeight < pHeight)
				height = iframe.contentWindow.document.body.scrollHeight;
			else
				height = pHeight;
		}
		catch (e) {
			if (themeDisplay.isStateMaximized()) {
				<%=request.getAttribute("screenName")%>maximizeIframe(iframe);
			}
			else {
				iframe.height = pHeight;
			}

			return true;
		}
		iframe.height = height + 20;
        <%}%>
		return true;
	}

	function <%=request.getAttribute("screenName")%>updateIframeSize() {
        <%if (request.getAttribute("screenHeight") == null) {%>
		var iframe = document.getElementById('iframe<%=request.getAttribute("screenName")%>');
        iframe.height = 150;
        <%=request.getAttribute("screenName")%>resizeIframe();
        <%}%>
		return true;
	}
    var <%=request.getAttribute("screenName")+"DataBackedUp"%> = false;
    function gotoURL(url){
        var currentURL = themeDisplay.getLayoutURL();
        currentURL = currentURL.substr(0, currentURL.lastIndexOf("/"));
        currentURL = currentURL + url;
        window.location='http://localhost:9088'+currentURL;
    }
    function <%=request.getAttribute("screenName")%>DataRestored(){
        <%=request.getAttribute("screenName")%>DataBackedUp = false;
    }

	function <%=request.getAttribute("screenName")%>BackupData() {
        try{var frame = jQuery('#iframe<%=request.getAttribute("screenName")%>');
        var reB = frame.contents().find('.backupLink');
        reB.click();
        }catch(e){}
    }
    var <%=iframeNameSrc%> = '/OTMS-Demo/<%=request.getAttribute("viewPage")%>?screenName=<%=request.getAttribute("screenName")%>&portletId=<%=request.getAttribute("portletId")%>&portletInsId=<%=request.getAttribute("portletInsId")%>&portalMode=<%=request.getAttribute("portalMode")%>&selectFRow=<%=request.getAttribute("selectFRow")%>';
    var <%=iframeName%> = jQuery('#<%=iframeName%>');
    <%=iframeName%>.attr('src', <%=iframeNameSrc%>);
    jQuery(document).ready(function() {
        jQuery('#p_p_id_<%=request.getAttribute("screenName")%>_WAR_OTMSDemo_').contents().find('.portlet-topper').mousedown(function(event){
            <%=request.getAttribute("screenName")%>DataBackedUp = true;
            <%=request.getAttribute("screenName")%>BackupData();
         });
         var sortColumns = Liferay.Layout.Columns.sortColumns;
         sortColumns.bind( "sortstop.sortable", function(event, ui) {
             if (ui.item[0].portletId.indexOf('<%=request.getAttribute("screenName")%>')>-1 ){
                 if (<%=request.getAttribute("screenName")%>DataBackedUp == true){
                     var <%=iframeName%> = jQuery('#<%=iframeName%>');
                     var iFsrc =<%=iframeName%>.attr('src') + '&reloadFromSession=true';
                     <%=iframeName%>.attr('src',iFsrc );
                 }
             }
         });

    });
    <%if (request.getAttribute("removeHeader") != null){%>
        jQuery(".portlet-topper").remove();
    <%}%>
    /*]]>*/
</script>


