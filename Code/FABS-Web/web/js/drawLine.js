
var hDiv ;
var vDiv ;

var hDivM;
var vDivM;

var drag = false;
var activeEntity = null;

var entityFormMode = "new";

var graphicsDiv; 

window.onload = function()
{
   
    drawAllLines();
    addListeners(); 
    
}
var y = document.getElementsByClassName('ui-layout-unit-content');
    
graphicsDiv = y[1];
graphicsDiv.onmousemove = getMouseXY;
graphicsDiv.style = "overflow:hidden";



var mouseX = 0, mouseY = 0;

//Mousedown event handler for circle

function openAddItem()
{
    entityFormMode = "new";
    document.getElementById("addEntityForm:Create").style.display = "inline";
    document.getElementById("addEntityForm:Save").style.display = "none";
    document.getElementById("addEntityForm:Delete").style.display = "none";
    
}


function resetEntityFormStatus()
{
    if(entityFormMode == "new")
    {
        document.getElementById("addEntityForm:Create").style.display = "inline";
        document.getElementById("addEntityForm:Save").style.display = "none";
        document.getElementById("addEntityForm:Delete").style.display = "none";
    }
    else  if(entityFormMode == "edit")
    {
        document.getElementById("addEntityForm:Create").style.display = "none";
        document.getElementById("addEntityForm:Save").style.display = "inline";
        document.getElementById("addEntityForm:Delete").style.display = "inline";
    }
    
}

function openEditItem()
{
   entityFormMode = "edit";
    document.getElementById("addEntityForm:Create").style.display = "none";
    document.getElementById("addEntityForm:Save").style.display = "inline";
    document.getElementById("addEntityForm:Delete").style.display = "inline";
}

function openAddRel()
{
    // clearAllDlg();
    document.getElementById("addRelationForm:Create").style.display = "inline";
    document.getElementById("addRelationForm:Save").style.display = "none";
    document.getElementById("addRelationForm:Delete").style.display = "none";
}

function openEditRel()
{
    document.getElementById("addRelationForm:Create").style.display = "none";
    document.getElementById("addRelationForm:Save").style.display = "inline";
    document.getElementById("addRelationForm:Delete").style.display = "inline";
}

function clearAllDlg()
{
    document.getElementById("moveForm:clearAllDlg").click();
}

function reDrawLines(entityId1) {
    
    
    drawAllLines();
    addListeners();
//    var allLines ;
//    allLines = document.getElementsByClassName(entityId1+"Line");
//    var allLines1 = allLines;
//    var ids =[];
//    var k =0;
//    var allLinesLength = allLines1.length;
//    for (var i=0; i < allLinesLength; i++) {
//        var element = allLines1[0];
//        graphicsDiv.removeChild(element);
//        var ids1 = element.id.split("-");
//        for (var j=2; j < ids1.length; j++) {
//            var id = ids1[j];
//            if(id!=entityId1){
//                var flag = false;
//                for (var l=0; l < ids.length; l++) {        
//                    if(ids[l] == id){
//                        flag = true;
//                    }
//                    
//                }
//                if(flag== false){
//                    ids[k] = id;
//                    k++;
//                }
//            }
//        }        
//    }
//    for (var j=0; j < ids.length; j++) {        
//        drawLine(entityId1, ids[j]);
//    }
}

function drawAllLines(){
    
    var allLines = document.getElementsByClassName("Line");
    var allLinesLength = allLines.length;
    for (var i=0; i < allLinesLength; i++) {
        var element = allLines[0];
        graphicsDiv.removeChild(element);
    }
    
    
    var relations = document.getElementById("att:relations").value;
    var relationArray = [];
    relationArray = relations.split("--");
    for(var i = 1; i < relationArray.length;i++)
    {
        var relation = relationArray[i];
        var relationIds = relation.split(":");
        drawLine("form:Entity_"+relationIds[1] , "form:Entity_"+relationIds[2],relationIds[3],relationIds[4],relationIds[0]);
    }
    
}
function drawRelation() {
    var entityId1="form:Entity_"+document.getElementById("addRelationForm:entities1").value;
    var entityId2="form:Entity_"+document.getElementById("addRelationForm:entities2").value;
    drawLine(entityId1, entityId2);
    
}


function drawLine(entityId1, entityId2,M1,M2,relationId) {
   
    var sameX = false;
    var sameY = false;    
    var entityLeft;
    var entityRight;
    var entityHigh;
    var entityLow;
   
    var entity1=document.getElementById(entityId1);   
    var entity2=document.getElementById(entityId2);
   
    if (Math.abs(entity1.offsetTop - entity2.offsetTop) <= (entity1.offsetHeight/2)){        
        sameY = true;
    }
    if (Math.abs(entity1.offsetLeft - entity2.offsetLeft) <= (entity1.offsetWidth/2)){
        sameX = true;
    }
    
    if(entity1.offsetTop>entity2.offsetTop)
    {
        entityHigh = entity2;
        entityLow = entity1;
    }
    else{
        entityHigh = entity1;
        entityLow = entity2;
    }
   
    if(entity1.offsetLeft>entity2.offsetLeft)
    {
        entityLeft = entity2;
        entityRight = entity1;
    }
    else{
        entityLeft = entity1;
        entityRight   = entity2;
    }
   
   
    graphicsDiv.style.position = "relative";
   
    hDiv = document.createElement("div");
    hDivM = document.createElement("div");
    vDivM = document.createElement("div");
    
    hDiv.id = "h"+"-"+relationId+"-"+entity1.id+"-"+entity2.id;
    
    
    
    
        
    if(entityLeft === entityHigh)
    {
        hDiv.style.right =  parseInt(graphicsDiv.offsetWidth)-parseInt(entityRight.offsetLeft)-(parseInt(entityRight.offsetWidth)/2)+"px";
        hDiv.style.left =  parseInt(entityLeft.offsetLeft)+(parseInt(entityLeft.offsetWidth))+"px";        
        hDivM .style.left = hDiv.style.left;
    }
    
    else
    {
        hDiv.style.right =  parseInt(graphicsDiv.offsetWidth)-parseInt(entityRight.offsetLeft)+"px";
        hDiv.style.left =  parseInt(entityLeft.offsetLeft)+(parseInt(entityLeft.offsetWidth)/2)+"px";
        hDivM .style.right = addPixels(hDiv.style.right,+10);
    }
    
    hDiv.style.top = parseInt(entityHigh.offsetTop)+parseInt(entityHigh.offsetHeight)/2+"px"; 
    
    
    
    if(sameY){
        hDiv.style.left =  parseInt(entityLeft.offsetLeft)+(parseInt(entityLeft.offsetWidth))+"px";
        hDiv.style.right =  parseInt(graphicsDiv.offsetWidth)-parseInt(entityRight.offsetLeft)+"px";         
        hDiv.style.top = parseInt(entityHigh.offsetTop)+parseInt(entityHigh.offsetHeight)/2+((parseInt(entityLow.offsetTop)-parseInt(entityHigh.offsetTop))/2)+"px"; 
    }


    hDiv.style.position = "absolute";
    hDiv.className +="horizontalLine";
    
    hDiv.className += " " +entity1.id+"Line";
    hDiv.className += " " +entity2.id+"Line";
    hDiv.className += " " +"Line";
     
    hDivM.style.top = addPixels(hDiv.style.top,-20);
    hDivM.style.position = "absolute";
    hDivM.className += " " +"Line";
    hDivM.className += " " +entity1.id+"Line";
    hDivM.className += " " +entity2.id+"Line";
    hDivM.className += " " +"many";
    
    if(entityHigh === entity1)
    {
        hDivM.innerHTML = M1;
        vDivM.innerHTML = M2;
    }
    
    else    
    {
        vDivM.innerHTML = M1;
        hDivM.innerHTML = M2;
    }
    
    if(!sameX)
        graphicsDiv.appendChild(hDiv);
    
    if(sameY&sameX){
        graphicsDiv.appendChild(hDiv);
    }
    else{
        graphicsDiv.appendChild(hDivM);
    }
    
    
    vDiv = document.createElement("div");
    vDiv.id = "v"+"-"+relationId+"-"+entity1.id+"-"+entity2.id;
         
      
        
        
    vDiv.style.bottom =  parseInt(graphicsDiv.offsetHeight)-parseInt(entityLow.offsetTop)+"px";
    vDiv.style.top =  parseInt(entityHigh.offsetTop)+(parseInt(entityHigh.offsetHeight)/2)+"px";
        
    vDiv.style.left = (parseInt(entityLow.offsetLeft)+parseInt(entityLow.offsetWidth)/2)+"px";
     vDiv.style.position = "absolute";
    vDiv.className +="verticalLine";
        
    vDiv.className += " " +entity1.id+"Line";
    vDiv.className += " " +entity2.id+"Line";
    vDiv.className += " " +"Line";
    
    
    vDivM.style.position = "absolute";
    vDivM.className += " " +entity1.id+"Line";
    vDivM.className += " " +entity2.id+"Line";    
    vDivM.className += " " +"Line";
    vDivM.className += " " +"one";
      
    if(sameX){
        vDiv.style.top =  parseInt(entityHigh.offsetTop)+(parseInt(entityHigh.offsetHeight))+"px";
        vDiv.style.left = parseInt(entityLeft.offsetLeft)+parseInt(entityLeft.offsetWidth)/2+((parseInt(entityRight.offsetLeft)-parseInt(entityLeft.offsetLeft))/2)+"px";  
        hDivM.style.top = addPixels(vDiv.style.top,+5);
        hDivM.style.left = addPixels(vDiv.style.left,+10);
 vDivM.style.left = addPixels(vDiv.style.left,+10);
        vDivM.style.bottom = addPixels(vDiv.style.bottom,+10);        
}

    else if(sameY)
    {
        vDivM.style.top = addPixels(hDiv.style.top,-20);
       if(entityLeft === entityHigh)
       {
          vDivM.style.right = addPixels(hDiv.style.right,+10);            
       }
       else
           vDivM.style.left = hDiv.style.left;  
    }
    
    else
      {
          vDivM.style.left = addPixels(vDiv.style.left,+10);
        vDivM.style.bottom = addPixels(vDiv.style.bottom,+10);
      }
   
    
    
    if(!sameY)
        graphicsDiv.appendChild(vDiv);
   
   
    graphicsDiv.appendChild(vDivM);
   
}

function addPixels(position,diff)
{
    return parseInt(position.split("px")[0])+diff+"px";
}

function circleMouseDbClick(evt,obj) {    
    
    openEditItem();
    drag = false;
    openEditMode(this.id);
    addDlg.show();
}

function openEditMode(entityId)
{
    document.getElementById("addEntityForm:editEntityId").value = entityId.split("_")[1];
    document.getElementById("addEntityForm:getEntity").click();
}

function relationMouseDbClick()
{
    document.getElementById("addRelationForm:editRelationId").value = this.id.split("-")[1] ;
    document.getElementById("addRelationForm:getRelation").click();
    addRelDlg.show();
}

function circleMouseDown(evt,obj) {
    drag = true;
    activeEntity = this;
}

//Mouseup event handler for circle
function circleMouseUp() {
    drag = false;
    
    activeEntity = null;
    document.getElementById("moveForm:movedEntityX").value = this.offsetLeft;
    document.getElementById("moveForm:movedEntityY").value = this.offsetTop;
    document.getElementById("moveForm:movedEntityId").value = this.id;
    document.getElementById("moveForm:updateEntityPosition").click();
}

function getMouseXY(e) {
    
  
    //Redraw the curve with the changed point
    if (drag) {      
        
        reDrawLines(activeEntity.id);
   
    
    }
    return true;
}

function addListeners()
{
    var allEntities ;
    allEntities = document.getElementsByClassName("Entity");
    var allEntitiesLength = allEntities.length;
    for (var i=0; i < allEntitiesLength; i++) {
        var element = allEntities[i];
        element.addEventListener('mousedown', circleMouseDown);
        element.addEventListener('mouseup', circleMouseUp);
        element.addEventListener('dblclick', circleMouseDbClick);
    }
    
    var allRelations;
    allRelations = document.getElementsByClassName("Line");
    var allRelationsLength = allRelations.length;
    for (var i=0; i < allRelationsLength; i++) {
        var element = allRelations[i];
        element.addEventListener('dblclick', relationMouseDbClick);
    }
    
   
}
