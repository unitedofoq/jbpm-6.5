window.onload = init;
var socket = new WebSocket("wss://" + window.location.host + "/OTMS-Web/notificationWebSocket");
socket.onmessage = onMessage;
socket.onopen = onOpen;
var userLoginName = "";

function init() {
    userLoginName = document.getElementById("InboxForm:userLoginName").value;
}

function onOpen(event) {
  socket.send(userLoginName); 
}

function onMessage(event) {
    var notification = JSON.parse(event.data);
    //update the UI
    webSocketNotificationWidgetVar.renderMessage({"summary": notification.summary,
        "detail": notification.detail,
        "severity": notification.serverity});
}