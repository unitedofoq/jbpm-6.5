var preventEnterKeyFunction = function() {
  $(window).keydown(function(event){
    if(event.keyCode === 13) {
      event.preventDefault();
      return false;
    }
  });
};

$(document).ready(preventEnterKeyFunction);

function deleteRow(rowIndex) {
    try {
        jQuery('.deletewithoutconf_btn')[rowIndex].id = 'deletethisrow';
        jQuery('#deletethisrow').trigger('click');
    } catch (error) {
        console.log(error);
    }
}

function editRowList(lastRowIndex) {
    try {
        for (var i = 0; i <= lastRowIndex; i++) {
            editRow(i);
        }
    } catch (error) {
        console.log(error);
    }
}

function lookupSelectionListener() {
    var selected = '';
    jQuery('tr.ui-widget-content.ui-state-highlight').each(function () {
        selected += this.rowIndex + ',';
    });
    selected = selected.substring(0, selected.length - 1);
    passSelectedIndexes([{
            name:'selected',
            value: selected
        }]);
}

PrimeFaces.widget.DataTable.prototype.selectRowsInRange = function(row) {
    
	var rows = this.tbody.children(),
    _self = this;
    
	//unselect previously selected rows with shift
    if(this.cursorIndex) {
    	var oldCursorIndex = this.cursorIndex,
        
        rowsToUnselect = oldCursorIndex > this.originRowIndex 
        	? rows.slice(this.originRowIndex, oldCursorIndex + 1) 
        	: rows.slice(oldCursorIndex, this.originRowIndex + 1);

        rowsToUnselect.each(function(i, item) {
            _self.unselectRow($(item), true);
        });
    }

    //select rows between cursor and origin
    this.cursorIndex = row.index();

    var rowsToSelect = this.cursorIndex > this.originRowIndex 
    	? rows.slice(this.originRowIndex, this.cursorIndex + 1) 
    	: rows.slice(this.cursorIndex, this.originRowIndex + 1);
    
    //var i=0;
    rowsToSelect.each(
    	function(i, item) {
	        if (!(i >= rowsToSelect.length-1)) {
	            _self.selectRow($(item), true);
	        } else {
	            _self.selectRow($(item), false);
	        }
	        i++;
        }
    );
};