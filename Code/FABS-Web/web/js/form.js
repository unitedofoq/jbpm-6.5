var preventEnterKeyFunction = function() {
  $(window).keydown(function(event){
    var targetComp = $(event.target);
    if (event.keyCode === 13 && targetComp.is("input")) {
      event.preventDefault();
      return false;
    }
  });
};
$(document).ready(preventEnterKeyFunction);
