package com.unitedofoq;

import com.liferay.portal.kernel.util.HtmlUtil;
import org.omg.PortableInterceptor.PolicyFactory;

import java.util.logging.Logger;

/**
 * Created by amaroof on 26/09/18.
 */
public class XssChecker {

    private static final Logger logger = Logger.getLogger(XssChecker.class.getName());

    public boolean check (String value)
    {

        String xssPreventedValue = value;
        xssPreventedValue= HtmlUtil.escape(xssPreventedValue);

        logger.info(">>> value after escaping"+ xssPreventedValue);
        if(value.equals(xssPreventedValue)){

            return true;
        }else
        {
            return false;
        }

    }
}
