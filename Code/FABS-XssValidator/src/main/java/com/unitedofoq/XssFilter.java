package com.unitedofoq;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by amaroof on 17/09/18.
 */
public class XssFilter implements Filter {

    private static final Logger logger = Logger.getLogger(XssFilter.class.getName());
    XssChecker xssChecker = new XssChecker();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        logger.info(">>> filter Xss is on ");

        //check path
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String path = httpServletRequest.getRequestURI().toString();
        logger.info(">>> path requested is : " + path);

        if (httpServletRequest.getMethod().equals("GET")) {
            filterChain.doFilter(servletRequest, servletResponse);
        }
        else
        {
            // getting paramter
            Map<String, String[]> paramMap = servletRequest.getParameterMap();

            if(paramMap.containsKey("title"))
            {
                customDoFilter("title" , paramMap ,servletRequest,servletResponse,filterChain);
            }
            else if(paramMap.containsKey("name"))
            {
                customDoFilter("name" , paramMap ,servletRequest,servletResponse,filterChain);
            }else
            {
                    logger.info(">>> not in this request");
                    filterChain.doFilter(servletRequest, servletResponse);
            }
        }
    }
    @Override
    public void destroy() {

    }

    public void customDoFilter (String paramName,Map<String, String[]> paramMap ,ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain ) throws IOException, ServletException
    {
        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
        String [] values ;
        values= paramMap.get(paramName);
        logger.info(">>> found this value under "+paramName+" : " + values[0]);
        Boolean checkResult = xssChecker.check(values[0]);
        logger.info(">>> boalean CheckResult : " +checkResult );
        if(checkResult) //case of passing filter
        {
            logger.info(">>> pass from filter");
            filterChain.doFilter(servletRequest, servletResponse);
        }
        else
        {       // fail to pass filter
                logger.info(">>> fail in filter");
                httpResponse.setStatus(404);
                HttpSession session = ((HttpServletRequest) servletRequest).getSession();
                if (session != null)
                session.invalidate();
                //httpResponse.sendRedirect("/c/portal/logout");
        }

    }
}
