package com.unitedofoq.fabs.plugins.hooks;

import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.security.auth.PrincipalThreadLocal;
import com.liferay.portal.security.ldap.PortalLDAPImporterUtil;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.security.permission.PermissionThreadLocal;
import com.liferay.portal.service.UserLocalServiceUtil;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import ws.ADBusinessManager;
import ws.ActiveDirectoryRemote;

/**
 * @author Hamada
 *
 */
public class FABSUtil {

    /**
     * local variables
     *
     */
    private ActiveDirectoryRemote iADRemote;

    private ADBusinessManager adBusinessManager;

    private User lfUser;
    private long loggedUserDBID;

    public User getLfUser() {
        return lfUser;
    }

    public void setLfUser(User lfUser) {
        this.lfUser = lfUser;
    }

    private Context context;

    private static final Logger logger = Logger.getLogger(FABSUtil.class.getName());

    public FABSUtil(long userId) {
        init(userId);
    }

    /**
     * - Retrieve Liferay user, Init Context, and load FABS AD Remote interface
     *
     */
    private void init(long lfUsrId) {

        try {
            logger.log(Level.FINE, "Loading Liferay User Object wz ID::" + lfUsrId);
            lfUser = UserLocalServiceUtil.getUser(lfUsrId);
            logger.log(Level.FINE, ":::Liferay User ScreenName :::" + lfUser.getScreenName());
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error Getting Liferay User" + lfUsrId + ", Exit!!", e);
            return;
        }
        try {
            logger.log(Level.INFO, "Initialize Context");
            context = new InitialContext();
            iADRemote = (ActiveDirectoryRemote) loadEJBClass(ActiveDirectoryRemote.class.getName());
            Object o = loadEJBClass(ADBusinessManager.class.getName());
            if (o != null) // in-case no implementation found
            {
                adBusinessManager = (ADBusinessManager) o;
            }
        } catch (NamingException ex) {
            logger.log(Level.SEVERE, "Exception Loading EJb Classes", ex);
            logger.log(Level.SEVERE, "Exit !!");
            return;
        }
    }

    /**
     * Sync User Liferay/AD groups to FABS
     *
     */
    @SuppressWarnings("unchecked")
    public void syncUserRoles() {
        logger.log(Level.ALL, "Sync LF Roles to FABS Roles for User:::" + lfUser.getScreenName());
        try {
            logger.log(Level.FINE, "::: Cleaning existing user roles");
            iADRemote.executeEntityUpdateNativeQuery("DELETE FROM RoleUser where ouser_dbid = " + loggedUserDBID);

            logger.log(Level.FINE, "::: retrieving all user roles");
            // Load all defined Roles, to match against user AD groups
            List<Object[]> roles = iADRemote.executeEntityListNativeQuery("Select DBID, name from ORole");

            // MAP<roleName, roleDBID> ToBe used for Comparison wz LF/AD groups
            HashMap<String, String> rolesMap = new HashMap<String, String>();
            for (Object[] oRole : roles) {
                rolesMap.put(String.valueOf(oRole[1]).trim().toLowerCase(), String.valueOf(oRole[0]));
            }
            logger.log(Level.FINE, "::: Adding User Roles based on Liferay/AD groups :::");
            // Building A list of user Roles
            for (UserGroup group : lfUser.getUserGroups()) {
                String groupName = group.getName().trim().toLowerCase();

                String oRoleDBID = rolesMap.get(groupName);
                if (oRoleDBID != null && !"".equalsIgnoreCase(oRoleDBID)) {
                    logger.log(Level.FINE,
                            " >> Adding Role  " + groupName.trim().toLowerCase() + " wz DBID " + oRoleDBID);

                    long dbid = iADRemote.generateDBID("RoleUser");

                    iADRemote.executeEntityUpdateNativeQuery(
                            "INSERT INTO ROLEUSER (dbid , ouser_dbid, orole_dbid, deleted) " + " VALUES(" + dbid + ","
                            + loggedUserDBID + "," + oRoleDBID + ",0)");
                } else {
                    logger.log(Level.FINE,
                            "No Matching Role Found in FABS for Group ::: " + groupName.trim().toLowerCase());
                }
            }
            logger.log(Level.INFO, "::: Done Copying User Roles From Liferay/AD groups to FABS :::");
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "::: Exception thrown", ex);
        }
    }

    /**
     * - Authenticate User against LDAP (AD) by ScreenName, - ReLoad User
     * Information from LDAP to enforce user info update
     *
     */
    public static int authenticateUserByScreenName(long companyId, String screenName) {
        try {
            logger.log(Level.FINE, "::: FABSUtil.authenticateUserByScreenName");

            User tmpLFUser = PortalLDAPImporterUtil.importLDAPUserByScreenName(companyId, screenName);

            if (tmpLFUser != null) {
                UserLocalServiceUtil.updateUser(tmpLFUser);
                return 0;
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "::: Exception while authenticating User By ScreenName", e);
        }
        return -1;
    }

    /**
     * - Authenticate User against LDAP (AD) by EmailAddress, - ReLoad User
     * Information from LDAP to enforce user info update
     *
     */
    public static int authenticateByEmailAddress(long companyId, String emailAddress) {
        try {
            logger.log(Level.FINE, "::: FABSUtil.authenticateUserByEmail");

            User tUser = UserLocalServiceUtil.getUserByEmailAddress(companyId, emailAddress);

            if (tUser == null) {
                return -1;
            }

            User tmpLFUser = PortalLDAPImporterUtil.importLDAPUser(companyId, emailAddress, tUser.getScreenName());

            if (tmpLFUser != null) {
                UserLocalServiceUtil.updateUser(tmpLFUser);
                return 0;
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "::: Exception while authenticating User By Email", e);
        }
        return -1;
    }

    /**
     * - Authenticate User against LDAP (AD) by UserId, - ReLoad User
     * Information from LDAP to enforce user info update
     *
     */
    public static int authenticateUserByUserId(long companyId, long userId) {
        try {
            logger.log(Level.FINE, "::: FABSUtil.authenticateUserByUserId");

            User tUser = UserLocalServiceUtil.getUser(userId);
            if (tUser == null) {
                return -1;
            }

            User tmpLFUser = PortalLDAPImporterUtil.importLDAPUser(companyId, tUser.getEmailAddress(),
                    tUser.getScreenName());

            if (tmpLFUser != null) {
                UserLocalServiceUtil.updateUser(tmpLFUser);
                return 0;
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "::: Exception while authenticateUserByUserId", e);
        }
        return -1;
    }

    /**
     * Check if the user is in the DB, update the existing BankBranch attribute,
     * Otherwise add new record
     *
     */
    public void addUpdateUser() {
        try {
            String tenant = getUserCustomField("tenant");
            String eligibletenants = getUserCustomField("eligibletenants");
            // in-case no tenant defined for user, use tenant 1 as a default tenant.
            tenant = (tenant == null || "".equalsIgnoreCase(tenant.trim())) ? "1" : tenant;
            eligibletenants = (eligibletenants == null || "".equalsIgnoreCase(eligibletenants.trim())) ? tenant : eligibletenants;
            // Create/Update CentralUser
            iADRemote.addUpdateCentUser(lfUser.getScreenName(), Long.valueOf(tenant), eligibletenants);

            logger.log(Level.INFO, " >> Search The DB for OUser wz screenName" + lfUser.getScreenName());

            Object o1 = iADRemote.executeEntityNativeQuery(
                    "SELECT DBID FROM OUSER WHERE loginName = '" + lfUser.getScreenName() + "'");
            // Load User
            if (o1 == null) {// No record for User in FABS, create new User
                // Generate new DBID
                Long dbid = iADRemote.generateDBID("OUser");

                logger.log(Level.INFO, " >> Adding OUser Object wz DBID " + dbid);

                // IMP: Ouser added to System OModule (DBID=1)
                iADRemote.executeEntityUpdateNativeQuery(
                        "INSERT INTO OUser (DBID, UTYPE, loginName, password, displayName, EMAIL, firstLanguage_dbid, "
                        + "secondlanguage_dbid, TRACKMETADATA, TRACKBUSINESSDATA, rtl, OMODULE_DBID, active, deleted) "
                        + " VALUES(" + String.valueOf(dbid) + ", 'USER' , '" + lfUser.getScreenName() + "', 'AD', '"
                        + lfUser.getFullName() + "', '" + lfUser.getEmailAddress() + "', 23,"
                        + " 24, 1 , 1 , 0, 1, 1 , 0) ");

                loggedUserDBID = dbid;

                if (adBusinessManager != null) {
                    adBusinessManager.mapBusinessUser(dbid, lfUser.getScreenName());
                }

            } else {
                loggedUserDBID = Long.valueOf(String.valueOf(o1));
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, " ::: Exception Add/Update User!!", e);
        }
    }

    /**
     * Loads Liferay User location UDC value.
     *
     */
    public String getUserCustomField(String fldName) {
        logger.log(Level.FINE, "::: Loading User location Attribute");
        String fldValue = null;
        try {
            // LF Init PermissionChecker to access custom Fields
            PrincipalThreadLocal.setName(lfUser.getUserId());
            PermissionChecker permissionChecker = PermissionCheckerFactoryUtil.create(lfUser);
            PermissionThreadLocal.setPermissionChecker(permissionChecker);
            // Get User Custom Field Value
            Object fldValueObj = lfUser.getExpandoBridge().getAttribute(fldName);
            fldValue = fldValueObj == null ? null :String.valueOf(fldValueObj);
            logger.log(Level.INFO, ":: User Custom Field : " + fldName + " has a value of ::: " + fldValue);

        } catch (Exception e) {
            logger.log(Level.SEVERE, " ::: Exception While Loading User Custom Field" + fldName + "!!", e);
        }
        return fldValue;
    }

    public void assignUserLanguage() {

        logger.log(Level.FINE, "::: User Language ");

        String firstLang = null;
        String secondLang = null;
        String rtl = null;

        if ("en_US".equalsIgnoreCase(getLfUser().getLocale().getDisplayLanguage())) {
            firstLang = "23"; // English
            secondLang = "24"; // Arabic
            rtl = "0";
        } else {
            firstLang = "24"; //Arabic
            secondLang = "23"; // English
            rtl = "1";
        }
        if (firstLang != null && secondLang != null && rtl != null) {
            iADRemote.executeEntityUpdateNativeQuery(
                    "UPDATE OUser SET firstLanguage_dbid = " + firstLang
                    + ",secondlanguage_dbid=" + secondLang + ",rtl= " + rtl + " WHERE DBID =" + String.valueOf(loggedUserDBID));
        }
    }

    /**
     * Load FABS Ejb Class
     *
     */
    private Object loadEJBClass(String fClassName) {
        Object retObj;
        try {
            logger.log(Level.INFO, "Loading EJB Class", fClassName);
            retObj = context.lookup("java:global/ofoq/" + fClassName);
            logger.log(Level.INFO, "Finished Loading EJB Class", fClassName);
            return retObj;
        } catch (NamingException ex) {
            logger.log(Level.SEVERE, "Exception thrown", ex);
            return null;
        }
    }

    public boolean isDisableRolesMapping() {
        logger.log(Level.INFO, "::: execute query Check if DisableRolesMapping :::");
        Object fabsParam = iADRemote.executeEntityNativeQuery(
                "SELECT SVALUE FROM FABSSETUP WHERE SKEY = 'DisableRolesMapping'");
        if (fabsParam != null && String.valueOf(fabsParam).toLowerCase().equals("true")) {
            return true;
        } else {
            return false;
        }
    }

}
