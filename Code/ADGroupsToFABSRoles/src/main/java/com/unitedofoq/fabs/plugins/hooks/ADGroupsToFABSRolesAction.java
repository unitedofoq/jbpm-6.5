package com.unitedofoq.fabs.plugins.hooks;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Hamada
 *
 */
public class ADGroupsToFABSRolesAction extends Action {

	
    private static final Logger LOGGER = Logger.getLogger(ADGroupsToFABSRolesAction.class.getName());


	public ADGroupsToFABSRolesAction() {
		super();
	}

	public void run(HttpServletRequest request, HttpServletResponse response)
			throws ActionException {
		LOGGER.log(Level.INFO, "::: Post Login Active Directory Groups to Roles Hook :::");
		Long userId= Long.parseLong(request.getRemoteUser());
		LOGGER.log(Level.INFO, ":::Liferay User Id :::" + userId);
		FABSUtil util = new FABSUtil(userId);
		if(util.getLfUser() ==null || util.getLfUser().getScreenName().equalsIgnoreCase("test")){
		  return;
		}
		LOGGER.log(Level.INFO, "::: Add/Update FABS user:::");
		util.addUpdateUser();
		if(!util.isDisableRolesMapping()){
			LOGGER.log(Level.INFO, "::: Copy Liferay/AD groups to FABS :::");
			LOGGER.log(Level.INFO,"::: Call syncUserRoles :::");
			util.syncUserRoles();
		}
		LOGGER.log(Level.INFO, "::: End Active Directory Sync Action :::");
		//util.assignUserLanguage();
		LOGGER.log(Level.INFO, "::: End User Language :::");
	}

}
