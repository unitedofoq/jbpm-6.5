package com.unitedofoq.fabs.plugins.hooks;



import java.util.Map;
 
import com.liferay.portal.security.auth.AuthException;
import com.liferay.portal.security.auth.Authenticator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Hamada
 * 
 * FABS Custom authentication, called after LDAPAuth in the auth pipline. 
 * to refresh User info from LDAP, and revoke access if needed.
 * This to fix Liferay Issue with existing users, Liferay doesn't reimport the users for some reason.
 * TODO Report Issue on Liferay jira, once it is fixed or other solution exists, remove this class
 */ 
public class FABSADAuth implements Authenticator {
 
    @Override
    public int authenticateByEmailAddress(long companyId, String emailAddress,
            String password, Map<String, String[]> headerMap,
            Map<String, String[]> parameterMap) throws AuthException {
        logger.log(Level.INFO, "::: FABSADAuth.authenticateByEmailAddress");
        
    	int result = FABSUtil.authenticateByEmailAddress(companyId, emailAddress);
    	if (result == 0)
    		return SUCCESS;    	
        return DNE;
    }
 
    @Override
    public int authenticateByScreenName(long companyId, String screenName,
            String password, Map<String, String[]> headerMap,
            Map<String, String[]> parameterMap) throws AuthException {
        
        logger.log(Level.INFO, "::: FABSADAuth.authenticateByEmailAddress");
        
    	int result = FABSUtil.authenticateUserByScreenName(companyId, screenName);
    	if (result == 0)
    		return SUCCESS;    	
        return DNE;
    }
 
    @Override
    public int authenticateByUserId(long companyId, long userId,
            String password, Map<String, String[]> headerMap,
            Map<String, String[]> parameterMap) throws AuthException {
        
        logger.log(Level.INFO, "::: FABSADAuth.authenticateByEmailAddress");
        
    	int result = FABSUtil.authenticateUserByUserId(companyId, userId);
    	if (result == 0)
    		return SUCCESS;    	
    	
        return DNE;
    }
    
    private static Logger logger = Logger.getLogger(FABSADAuth.class.getName());
 
}