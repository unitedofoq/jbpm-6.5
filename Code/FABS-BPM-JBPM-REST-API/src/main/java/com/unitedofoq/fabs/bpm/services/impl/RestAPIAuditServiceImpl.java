package com.unitedofoq.fabs.bpm.services.impl;

import com.unitedofoq.fabs.bpm.BPMEngine;
import javax.ejb.Stateless;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import org.kie.api.runtime.manager.audit.NodeInstanceLog;
import com.unitedofoq.fabs.bpm.services.AuditService;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.bpm.services.impl.RestAPIAuditService", beanInterface = AuditService.class)
@LocalBean
public class RestAPIAuditServiceImpl extends AbstractBPMService implements AuditService {

	@EJB
	private BPMEngine engine;

	@Override
	public long getTaskId(long processInstanceId, long nodeInstanceId) {
		long taskId = -1;
		List<? extends NodeInstanceLog> nodeInstancesLogList;
		nodeInstancesLogList = engine.getAuditService().findNodeInstances(
				processInstanceId);
		for (NodeInstanceLog nodeInstanceLog : nodeInstancesLogList) {
			Long workItemId = nodeInstanceLog.getWorkItemId();
			if (workItemId != null
					&& nodeInstanceLog.getNodeInstanceId().equals(
							Long.toString(nodeInstanceId))) {
				taskId = workItemId;
				break;
			}
		}
		return taskId;
	}

}