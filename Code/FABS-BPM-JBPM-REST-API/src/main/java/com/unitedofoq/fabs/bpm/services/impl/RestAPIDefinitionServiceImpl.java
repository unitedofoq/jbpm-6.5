/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.bpm.services.impl;

import com.unitedofoq.fabs.bpm.BPMServicesBuilder;
import com.unitedofoq.fabs.bpm.model.Task;
import com.unitedofoq.fabs.bpm.services.DefinitionService;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author bgalal
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.bpm.services.impl.RestAPIDefinitionServiceImpl", beanInterface = DefinitionService.class)
@LocalBean
public class RestAPIDefinitionServiceImpl extends AbstractBPMService implements DefinitionService {

	private CloseableHttpClient client;

	private CloseableHttpResponse response;

	private BPMServicesBuilder bpmServicesBuilder;

	@PostConstruct
	public void initialize() {
		bpmServicesBuilder = new BPMServicesBuilder();
	}

	@Override
	public Map<String, String> retrieveProcessVariables(String processDefId) {
		Map<String, String> processVariables = new HashMap<>();
		try {
			client = HttpClientBuilder.create().build();

			client = HttpClientBuilder.create()
					.setDefaultCredentialsProvider(provider()).build();

			response = client.execute(new HttpGet(bpmServicesBuilder
					.getEngineUrl()
					+ "/rest/runtime/"
					+ bpmServicesBuilder.getDeploymentId()
					+ "/process/"
					+ processDefId));

			final int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == HttpStatus.SC_OK) {
				System.out.println("OK");
			} else {
				System.out.println("KO");
			}

			processVariables = extractProcessVariableMap(response.getEntity()
					.getContent(), processDefId);
			closeClientAndResponse();
		} catch (IOException ex) {
			Logger.getLogger(RestAPIDefinitionServiceImpl.class.getName()).log(
					Level.SEVERE, null, ex);
		}
		return processVariables;
	}

	private CredentialsProvider provider() {
		final CredentialsProvider provider = new BasicCredentialsProvider();
		final UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(
				bpmServicesBuilder.getValue("PEngine.Username"),
				bpmServicesBuilder.getValue("PEngine.Password"));
		provider.setCredentials(AuthScope.ANY, credentials);
		return provider;
	}

	private void closeClientAndResponse() {
		if (response == null && client == null) {
			return;
		}

		try {
			if (response != null) {
				try {
					final HttpEntity entity = response.getEntity();
					if (entity != null) {
						entity.getContent().close();
					}
				} finally {
					response.close();
				}
			}
			if (client != null) {
				client.close();
			}
		} catch (IOException ex) {
			Logger.getLogger(RestAPIDefinitionServiceImpl.class.getName()).log(
					Level.SEVERE, null, ex);
		}
	}

	private Map<String, String> extractProcessVariableMap(
			InputStream inputStream, String processDefId) {
		Map<String, String> processVariables = new HashMap<String, String>();
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document dom = db.parse(inputStream);
			Element processDefinitionListElement = dom.getDocumentElement();
			NodeList processDefinitionNodeList = processDefinitionListElement
					.getChildNodes();
			if (processDefinitionNodeList != null) {
				int length = processDefinitionNodeList.getLength();
				for (int i = 0; i < processDefinitionNodeList.getLength(); i++) {
					Node dataNode = processDefinitionNodeList.item(i);
					if (dataNode.getNodeName().equals("variables")) {
						NodeList variablesNodeList = dataNode.getChildNodes();
						for (int x = 0; x < variablesNodeList.getLength(); x++) {
							Element element = (Element) variablesNodeList
									.item(x);
							if (element.getNodeName().equals("entry")) {
								String name = element
										.getElementsByTagName("key").item(0)
										.getTextContent();
								String value = element
										.getElementsByTagName("value").item(0)
										.getTextContent();
								processVariables.put(name, value);
								System.out.println("name: " + name + ", value"
										+ value);
							}
						}
					}
				}
			}
		} catch (ParserConfigurationException ex) {
			Logger.getLogger(RestAPIDefinitionServiceImpl.class.getName()).log(
					Level.SEVERE, null, ex);
		} catch (SAXException ex) {
			Logger.getLogger(RestAPIDefinitionServiceImpl.class.getName()).log(
					Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(RestAPIDefinitionServiceImpl.class.getName()).log(
					Level.SEVERE, null, ex);
		}

		return processVariables;
	}

    @Override
    public List<String> retrieveTaskOutputVariables(Task task) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
