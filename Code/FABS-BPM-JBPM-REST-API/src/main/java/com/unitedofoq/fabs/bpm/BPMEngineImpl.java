package com.unitedofoq.fabs.bpm;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Startup;
import javax.ejb.Stateless;

import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.manager.audit.AuditService;
import org.kie.api.task.TaskService;

@Startup
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.bpm.BPMEngine", beanInterface = BPMEngine.class)
public class BPMEngineImpl implements BPMEngine {

	private RuntimeEngine engine = null;
	private KieSession ksession = null;
	private TaskService taskService = null;
	private AuditService auditService = null;

	@PostConstruct
	public void initialize() {
		BPMServicesBuilder bPMServicesBuilder = new BPMServicesBuilder();
		engine = bPMServicesBuilder.buildRestConnection();
		taskService = engine.getTaskService();
		ksession = engine.getKieSession();
		auditService = engine.getAuditService();
	}

	@Override
	public KieSession getKieSession() {
		return ksession;
	}

	@Override
	public TaskService getTaskService() {
		return taskService;
	}

	@Override
	public AuditService getAuditService() {
		return auditService;
	}
}
