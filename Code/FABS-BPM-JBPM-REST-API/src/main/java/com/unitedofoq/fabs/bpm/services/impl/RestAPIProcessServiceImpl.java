package com.unitedofoq.fabs.bpm.services.impl;

import com.unitedofoq.fabs.bpm.BPMEngine;
import com.unitedofoq.fabs.bpm.model.ProcessInstance;
import com.unitedofoq.fabs.bpm.common.model.ProcessRequestData;
import com.unitedofoq.fabs.bpm.services.ProcessService;
import com.unitedofoq.fabs.bpm.model.status.ProcessState;
import java.util.*;

import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.bpm.services.impl.RestAPIProcessService", beanInterface = ProcessService.class)
public class RestAPIProcessServiceImpl extends AbstractBPMService implements ProcessService {

	@EJB
	private BPMEngine engine;

	/**
	 * @param processId
	 * @param parameters
	 * @return
	 */
	@Override
	public ProcessInstance createProcessInstance(String processId,
			Map<String, Object> parameters) {
		return mapJBPMProcessInstanceToProcessInstance(engine.getKieSession()
				.createProcessInstance(processId, parameters));
	}

	/**
	 * @param processInstanceId
	 * @return
	 */
	@Override
	public ProcessInstance startProcessInstance(long processInstanceId) {
		return mapJBPMProcessInstanceToProcessInstance(engine.getKieSession()
				.startProcessInstance(processInstanceId));
	}

	/**
	 * @param processInstanceId
	 */
	@Override
	public void abortProcessInstance(long processInstanceId) {
		engine.getKieSession().abortProcessInstance(processInstanceId);
	}

	/**
	 * @param processId
	 * @param parameters
	 * @return
	 */
	@Override
	public ProcessInstance startProcess(String processId,
			Map<String, Object> parameters) {
		return mapJBPMProcessInstanceToProcessInstance(engine.getKieSession()
				.startProcess(processId, parameters));
	}

	/**
	 * @param processInstanceId
	 * @return
	 */
	@Override
	public ProcessInstance retrieveActiveProcessInstance(long processInstanceId) {
		return mapJBPMProcessInstanceToProcessInstance(engine.getKieSession()
				.getProcessInstance(processInstanceId));
	}

	/**
	 * @return
	 */
	@Override
	public Collection<ProcessInstance> retrieveActiveProcessInstances() {
		return mapJBPMProcessInstanceListToProcessInstanceList(engine
				.getKieSession().getProcessInstances());
	}

	private List<ProcessInstance> mapJBPMProcessInstanceListToProcessInstanceList(
			Collection<org.kie.api.runtime.process.ProcessInstance> jbpmProcessInstances) {
		List<ProcessInstance> processInstances = new ArrayList<ProcessInstance>();
		for (org.kie.api.runtime.process.ProcessInstance jbpmProcessInstance : jbpmProcessInstances) {
			processInstances
					.add(mapJBPMProcessInstanceToProcessInstance(jbpmProcessInstance));
		}
		return processInstances;
	}

	private ProcessInstance mapJBPMProcessInstanceToProcessInstance(
			org.kie.api.runtime.process.ProcessInstance jbpmProcessInstance) {
		ProcessInstance processInstance = new ProcessInstance();
		processInstance.setId(jbpmProcessInstance.getId());
		int status = jbpmProcessInstance.getState();

		switch (status) {
		case org.kie.api.runtime.process.ProcessInstance.STATE_PENDING:
			processInstance.setState(ProcessState.PENDING);
			break;
		case org.kie.api.runtime.process.ProcessInstance.STATE_ACTIVE:
			processInstance.setState(ProcessState.ACTIVE);
			break;
		case org.kie.api.runtime.process.ProcessInstance.STATE_COMPLETED:
			processInstance.setState(ProcessState.COMPLETED);
			break;
		case org.kie.api.runtime.process.ProcessInstance.STATE_ABORTED:
			processInstance.setState(ProcessState.ABORTED);
			break;
		case org.kie.api.runtime.process.ProcessInstance.STATE_SUSPENDED:
			processInstance.setState(ProcessState.SUSPENDED);
			break;
		}
		processInstance.setProcessId(jbpmProcessInstance.getProcessId());
		// processInstance.setProcessName(jbpmProcessInstance.getProcessName());
		return processInstance;
	}

    @Override
    public String retrieveProcessInitiator(long insId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<Long, List<ProcessInstance>> retrieveAllProcessInstances(ProcessRequestData data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
