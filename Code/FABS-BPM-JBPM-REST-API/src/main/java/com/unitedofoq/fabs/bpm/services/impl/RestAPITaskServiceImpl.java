package com.unitedofoq.fabs.bpm.services.impl;

import com.unitedofoq.fabs.bpm.BPMEngine;
import com.unitedofoq.fabs.bpm.common.model.TasksRequestData;
import com.unitedofoq.fabs.bpm.model.Task;
import com.unitedofoq.fabs.bpm.model.status.TaskStatus;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.kie.api.task.model.Status;
import org.kie.api.task.model.TaskData;
import org.kie.api.task.model.TaskSummary;
import com.unitedofoq.fabs.bpm.services.TaskService;
import org.kie.api.task.model.User;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.bpm.services.impl.RestAPITaskService", beanInterface = TaskService.class)
public class RestAPITaskServiceImpl extends AbstractBPMService implements TaskService {

	@EJB
	private BPMEngine engine;

	/**
	 * @param userId
	 * @param language
	 * @return
	 */
	@Override
	public List<Task> getTasksAssignedAsPotentialOwner(String userId) {
		List<TaskSummary> taskSummaries = engine.getTaskService()
				.getTasksAssignedAsPotentialOwner(userId, "en-UK");
		return mapTaskSummaryListToTaskList(taskSummaries);
	}

	/**
	 * @param userId
	 * @param language
	 * @param processId
	 * @return
	 */
	@Override
	public List<Task> getTasksAssignedAsPotentialOwnerByProcessId(
			String userId, String processId) {
		List<TaskSummary> taskSummaries = engine.getTaskService()
				.getTasksAssignedAsPotentialOwnerByProcessId(userId, processId);
		return mapTaskSummaryListToTaskList(taskSummaries);
	}

	@Override
	public List<Task> getTasksOwned(String userId) {
		List<TaskSummary> taskSummaries = engine.getTaskService()
				.getTasksOwned(userId, "en-UK");
		return mapTaskSummaryListToTaskList(taskSummaries);
	}

	@Override
	public List<Task> getTasksOwnedByStatus(String userId, List<TaskStatus> taskStatuses) {
		List<Status> statuses = mapTaskStatusListToStatusList(taskStatuses);
		List<TaskSummary> taskSummaries = engine.getTaskService()
				.getTasksOwnedByStatus(userId, statuses, "en-UK");
		return mapTaskSummaryListToTaskList(taskSummaries);
	}

	// @Override
	// public Task getTaskById(long taskId) {
	// org.kie.api.task.model.Task task =
	// engine.getTaskService().getTaskById(taskId);
	// return
	// }

	/**
	 * @param taskId
	 * @param userId
	 */
	@Override
	public void claim(long taskId, String userId, String password) {
		engine.getTaskService().claim(taskId, userId);
	}

	/**
	 * @param taskId
	 * @param userId
	 * @param data
	 */
	@Override
	public void complete(long taskId, String userId, String password, Map<String, Object> data) {
		engine.getTaskService().complete(taskId, userId, data);
	}

	/**
	 * @param taskId
	 * @param userId
	 * @param delegateUserId
	 */
	@Override
	public void delegate(long taskId, String userId, String delegateUserId) {
		engine.getTaskService().delegate(taskId, userId, delegateUserId);
	}

	@Override
	public void release(long taskId, String userId) {
		engine.getTaskService().release(taskId, userId);
	}

        @Override
	public Map<String, Object> getTaskInputs(long taskId) {
		return engine.getTaskService().getTaskContent(taskId);
	}

	private List<Task> mapTaskSummaryListToTaskList(
			List<TaskSummary> taskSummaries) {
		List<Task> tasks = new ArrayList<Task>();
		for (TaskSummary taskSummary : taskSummaries) {
			Task task = new Task();
			task.setId(taskSummary.getId());
			task.setProcessInstanceId(taskSummary.getProcessInstanceId());
			task.setProcessId(taskSummary.getProcessId());
			task.setName(taskSummary.getName());
			task.setDescription(taskSummary.getDescription());
			task.setSubject(taskSummary.getSubject());
			Status taskStatus = taskSummary.getStatus();
			switch (taskStatus) {
			case Created:
				task.setStatus(TaskStatus.CREATED);
				break;
			case Ready:
				task.setStatus(TaskStatus.READY);
				break;
			case Reserved:
				task.setStatus(TaskStatus.RESERVED);
				break;
			case InProgress:
				task.setStatus(TaskStatus.INPROGRESS);
				break;
			case Suspended:
				task.setStatus(TaskStatus.SUSPENDED);
				break;
			case Completed:
				task.setStatus(TaskStatus.COMPLETED);
				break;
			case Failed:
				task.setStatus(TaskStatus.FAILED);
				break;
			case Error:
				task.setStatus(TaskStatus.ERROR);
				break;
			case Exited:
				task.setStatus(TaskStatus.EXITED);
				break;
			case Obsolete:
				task.setStatus(TaskStatus.OBSOLETE);
				break;
			}
			User actualOwner = taskSummary.getActualOwner();
			if (actualOwner != null) {
				task.setActualOwnerId(actualOwner.getId());
			}
			task.setCreatorId(taskSummary.getCreatedById());
			task.setCreationTime(taskSummary.getCreatedOn().toString());
			task.setActivationTime(taskSummary.getActivationTime());
			task.setExpirationTime(taskSummary.getExpirationTime());
			task.setPeriority(taskSummary.getPriority());
			task.setInputs(getTaskInputs(task.getId()));
			tasks.add(task);
		}
		return tasks;
	}

	private Task mapTaskSummaryListToTaskList(
			org.kie.api.task.model.Task taskSummary) {
		Task task = new Task();
		task.setId(taskSummary.getId());
		task.setProcessInstanceId(taskSummary.getTaskData()
				.getProcessInstanceId());
		task.setProcessId(taskSummary.getTaskData().getProcessId());
		task.setName(taskSummary.getName());
		task.setDescription(taskSummary.getDescription());
		task.setSubject(taskSummary.getSubject());
		Status taskStatus = taskSummary.getTaskData().getStatus();
		switch (taskStatus) {
		case Created:
			task.setStatus(TaskStatus.CREATED);
			break;
		case Ready:
			task.setStatus(TaskStatus.READY);
			break;
		case Reserved:
			task.setStatus(TaskStatus.RESERVED);
			break;
		case InProgress:
			task.setStatus(TaskStatus.INPROGRESS);
			break;
		case Suspended:
			task.setStatus(TaskStatus.SUSPENDED);
			break;
		case Completed:
			task.setStatus(TaskStatus.COMPLETED);
			break;
		case Failed:
			task.setStatus(TaskStatus.FAILED);
			break;
		case Error:
			task.setStatus(TaskStatus.ERROR);
			break;
		case Exited:
			task.setStatus(TaskStatus.EXITED);
			break;
		case Obsolete:
			task.setStatus(TaskStatus.OBSOLETE);
			break;
		}
		User actualOwner = taskSummary.getTaskData().getActualOwner();
		if (actualOwner != null) {
			task.setActualOwnerId(actualOwner.getId());
		}
		task.setCreatorId(taskSummary.getTaskData().getCreatedBy().getId());
		task.setCreationTime(taskSummary.getTaskData().getCreatedOn().toString());
		task.setActivationTime(taskSummary.getTaskData().getActivationTime());
		task.setExpirationTime(taskSummary.getTaskData().getExpirationTime());
		task.setPeriority(taskSummary.getPriority());
		task.setInputs(getTaskInputs(task.getId()));
		return task;
	}

	private List<Status> mapTaskStatusListToStatusList(
			List<TaskStatus> taskStatuses) {
		List<Status> statuses = new ArrayList<Status>();
		for (TaskStatus taskStatus : taskStatuses) {
			switch (taskStatus) {
			case CREATED:
				statuses.add(Status.Created);
				break;
			case READY:
				statuses.add(Status.Ready);
				break;
			case RESERVED:
				statuses.add(Status.Reserved);
				break;
			case INPROGRESS:
				statuses.add(Status.InProgress);
				break;
			case SUSPENDED:
				statuses.add(Status.Suspended);
				break;
			case COMPLETED:
				statuses.add(Status.Completed);
				break;
			case FAILED:
				statuses.add(Status.Failed);
				break;
			case ERROR:
				statuses.add(Status.Error);
				break;
			case EXITED:
				statuses.add(Status.Exited);
				break;
			case OBSOLETE:
				statuses.add(Status.Obsolete);
				break;
			}
		}
		return statuses;
	}

	public void start(long taskId, String userId, String password) {
		engine.getTaskService().start(taskId, userId);

	}

	@Override
	public Task getTaskById(long taskId) {
		org.kie.api.task.model.Task task = engine.getTaskService().getTaskById(
				taskId);
		return mapTaskSummaryListToTaskList(task);
	}

	/******************************************************************************
	 * Method signature :- public TaskStatus getTaskStatusById(long taskId)
	 * 
	 * @param :- long taskId
	 * @return :- com.unitedofoq.fabs.bpm.model.status.TaskStatus
	 * 
	 *         Description :- this method receive taskId and (call jbpm API)pass
	 *         it to engine.getTaskService method to get TaskStatus.
	 * 
	 * @author hahmed
	 * 
	 *         CreationDate:- 30-11-2017 2:25 pm
	 *
	 *         LastModificationDate:- ModifiedBy:-
	 * 
	 * 
	 *
	 ******************************************************************************/
	public TaskStatus getTaskStatusById(long taskId) {

		org.kie.api.task.model.Task task = engine.getTaskService().getTaskById(
				taskId);
		TaskData taskData = task.getTaskData();
		Status status = taskData.getStatus();
		TaskStatus taskStatus = mapStatusToTaskStatus(status);
		return taskStatus;
	}

	/******************************************************************************
	 * Method signature :- private TaskStatus mapStatusToTaskStatus(Status
	 * taskStatus)
	 * 
	 * @param :- org.kie.api.task.model.Status
	 * @return :- com.unitedofoq.fabs.bpm.model.status.TaskStatus
	 * 
	 *         Description :- this method receive
	 *         (org.kie.api.task.model.Status) and map it to
	 *         com.unitedofoq.fabs.bpm.model.status.TaskStatus and return it.
	 * 
	 * @author hahmed
	 * 
	 *         CreationDate:- 30-11-2017 2:35 pm
	 *
	 *         LastModificationDate:- ModifiedBy:-
	 * 
	 * 
	 *
	 ******************************************************************************/
	private TaskStatus mapStatusToTaskStatus(Status taskStatus) {

		TaskStatus status = null;
		switch (taskStatus) {
		case Created:
			status = TaskStatus.CREATED;
			break;
		case Ready:
			status = TaskStatus.READY;
			break;
		case Reserved:
			status = TaskStatus.RESERVED;
			break;
		case InProgress:
			status = TaskStatus.INPROGRESS;
			break;
		case Suspended:
			status = TaskStatus.SUSPENDED;
			break;
		case Completed:
			status = TaskStatus.COMPLETED;
			break;
		case Failed:
			status = TaskStatus.FAILED;
			break;
		case Error:
			status = TaskStatus.ERROR;
			break;
		case Exited:
			status = TaskStatus.EXITED;
			break;
		case Obsolete:
			status = TaskStatus.OBSOLETE;
			break;
		}
		return status;

	}

    @Override
    public List<Task> getProcessInstanceTasks(String processInstanceId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<Long, List<Task>> getTasksAssignedAsPotentialOwner(TasksRequestData data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<String> getTaskPotentialOwnersOfTypeRole(long taskId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
