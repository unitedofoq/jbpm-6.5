package com.unitedofoq.fabs.bpm.services.impl;

import com.unitedofoq.fabs.bpm.services.BPMService;

/**
 *
 */
public abstract class AbstractBPMService implements BPMService {

    @Override
    public String getType() {
        return "jbpm_rest_api";
    }
}
