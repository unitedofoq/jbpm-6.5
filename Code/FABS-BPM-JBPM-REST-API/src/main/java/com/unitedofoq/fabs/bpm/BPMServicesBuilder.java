package com.unitedofoq.fabs.bpm;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.remote.client.api.RemoteRuntimeEngineFactory;

public class BPMServicesBuilder {

	private RuntimeEngine engine = null;
	private String ip = null;
	private String port = null;
	private String deploymentId = null;
	private Properties prop = new Properties();
	private StringBuilder engineUrl;

	/**
	 * Default constructor
	 */
	public BPMServicesBuilder() {
		loadProperties();

		ip = getValue("pengine.ip");
		port = getValue("pengine.port");
		deploymentId = getValue("pengine.deploymentid");
		engineUrl = new StringBuilder("http://").append(ip).append(":")
				.append(port).append("/jbpm-console");
	}

	public RuntimeEngine buildRestConnection() {
		try {
			engine = RemoteRuntimeEngineFactory.newRestBuilder()
					.addUrl(new URL(engineUrl.toString())).addTimeout(5)
					.addDeploymentId(deploymentId)
					.addUserName(getValue("pengine.username"))
					.addPassword(getValue("pengine.password"))
					.disableTaskSecurity().build();
			return engine;
		} catch (MalformedURLException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	/**
	 * *************************************************************************
	 * *** Method signature :- private void loadProperties()
	 *
	 * @param :-
	 * @return :- void
	 *
	 *         Description :- this method load process Engine properties from
	 *         Process_Engine_Config.preperties file that located in home
	 *         directory (EX:- ip and port for process Engine server and
	 *         deploymentId)
	 *
	 * @author hahmed
	 *
	 *         CreationDate:- 6-12-2017 2:15 pm
	 *
	 *         LastModificationDate:- ModifiedBy:-
	 * 
	 * 
	 *
	 ******************************************************************************/
	private void loadProperties() {
		InputStream input = null;
		String fileName = "process_engine_config.properties";
		String filePath = System.getProperty("fabsPropertyPath");
		StringBuilder file = new StringBuilder(filePath).append(File.separator)
				.append(fileName);
		try {
			input = new FileInputStream(file.toString());
			// load a properties from a file
			prop.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * *************************************************************************
	 * *** Method signature :- public String getValue(String key)
	 *
	 * @param :- String
	 * @return :- String
	 *
	 *         Description :- this method receive the String key and return the
	 *         value in this property list with the specified key value (EX:- ip
	 *         )
	 *
	 * @author hahmed
	 *
	 *         CreationDate:- 7-12-2017 2:15 pm
	 *
	 *         LastModificationDate:- ModifiedBy:-
	 *
	 *
	 *
	 *****************************************************************************
	 */
	public String getValue(String key) {
		return prop.getProperty(key);
	}

	// getters
	public String getDeploymentId() {
		return deploymentId;
	}

	public String getEngineUrl() {
		return engineUrl.toString();
	}
}
