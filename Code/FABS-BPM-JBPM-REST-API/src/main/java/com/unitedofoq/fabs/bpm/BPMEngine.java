/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.bpm;

import javax.ejb.Local;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.audit.AuditService;
import org.kie.api.task.TaskService;

/**
 *
 * @author mahmed
 */
@Local
public interface BPMEngine {

	public KieSession getKieSession();

	public TaskService getTaskService();

	public AuditService getAuditService();

}
