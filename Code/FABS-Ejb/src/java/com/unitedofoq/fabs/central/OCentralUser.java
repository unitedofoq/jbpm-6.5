/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.central;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author bassem
 */
@Entity
public class OCentralUser implements Serializable{
    private static final long serialVersionUID = 1L;

    // <editor-fold defaultstate="collapsed" desc="Id">
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Serialization Code">
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OCentralUser)) {
            return false;
        }
        OCentralUser other = (OCentralUser) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return (getClass().getSimpleName()
                + "[id=" + id + "]") ;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="loginName">
    @Column(nullable=false)
    private String loginName ;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="active">
    private Boolean active ;

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="active">
//    private Boolean loggedIn ;
//
//    public String getLoggedInDD() {
//        return "OCentralUser_loggedIn";
//    }
//    
//    public Boolean getLoggedIn() {
//        return loggedIn;
//    }
//
//    public void setLoggedIn(Boolean loggedIn) {
//        this.loggedIn = loggedIn;
//    }

    
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="adminUser">
    private boolean adminUser;

    public boolean isAdminUser() {
        return adminUser;
    }

    public void setAdminUser(Boolean adminUser) {
        this.adminUser = adminUser;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="tenant">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private OTenant tenant ;
    public OTenant getTenant() {
        return tenant;
    }
    public void setTenant(OTenant tenant) {
        this.tenant = tenant;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="loggedIn">
    private boolean loggedIn;

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
    // </editor-fold>

    //<editor-fold desc="Eligible tenants">
    @OneToMany
    @JoinTable(name = "centuser_tenants")
    @OrderBy("name ASC")
    private List<OTenant> eligibleTenants;

    public List<OTenant> getEligibleTenants() {
        return eligibleTenants;
    }

    public void setEligibleTenants(List<OTenant> eligibleTenants) {
        this.eligibleTenants = eligibleTenants;
    }
    //</editor-fold>
}
