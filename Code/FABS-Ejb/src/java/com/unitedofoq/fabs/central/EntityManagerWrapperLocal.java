/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.central;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author arezk
 */
public interface EntityManagerWrapperLocal extends EntityManager {

    public Query createQuery(String query, OUser loggedUser);

    public Query createNamedQuery(String string, OUser loggedUser);

    public BaseEntity merge(BaseEntity baseEntity, OUser loggedUser);

    public void persist(BaseEntity baseEntity, OUser loggedUser);

    public Query createNativeQuery(String query, OUser loggedUser);

    public void clear(OUser loggedUser);

    public void refresh(BaseEntity entity, OUser loggedUser);

    public void detach(Object entity, OUser loggedUser);

    public void detachList(List entityList, OUser loggedUser);

    public void close(OUser loggedUser);

    public void flush(OUser loggedUser);

    public EntityManager getMultiTenantEntityManager(long tenantID);

}
