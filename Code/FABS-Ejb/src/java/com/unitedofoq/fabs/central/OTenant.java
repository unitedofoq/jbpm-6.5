/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.central;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author bassem
 */
@Entity
public class OTenant implements Serializable {
    private static final long serialVersionUID = 1L;

    // <editor-fold defaultstate="collapsed" desc="Id">
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Serialization Code">
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OTenant)) {
            return false;
        }
        OTenant other = (OTenant) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return (getClass().getSimpleName()
                + "[id=" + id + "]") ;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="connDriver">
    @Column(nullable=false)
    private String connDriver ;

    public String getConnDriver() {
        return connDriver;
    }

    public void setConnDriver(String connDriver) {
        this.connDriver = connDriver;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="connURL">
    @Column(nullable=false)
    private String connURL ;

    public String getConnURL() {
        return connURL;
    }

    public void setConnURL(String connURL) {
        this.connURL = connURL;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="connUser">
    @Column(nullable=false)
    private String connUser ;

    public String getConnUser() {
        return connUser;
    }

    public void setConnUser(String connUser) {
        this.connUser = connUser;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="connPassword">
    @Column(nullable=false)
    private String connPassword ;
    public String getConnPassword() {
        return connPassword;
    }

    public void setConnPassword(String connPassword) {
        this.connPassword = connPassword;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="name">
    @Column(nullable=false)
    private String name ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="active">
    private Boolean active ;

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="persistenceUnitName">
    @Column(nullable=false, unique=true)
    private String persistenceUnitName ;

    public String getPersistenceUnitName() {
        return persistenceUnitName;
    }

    public String getPersistenceUnitNameDD() {
        return "OTenant_persistenceUnitName";
    }

    public void setPersistenceUnitName(String persistenceUnitName) {
        this.persistenceUnitName = persistenceUnitName;
    }
    // </editor-fold>

     // <editor-fold defaultstate="collapsed" desc="companyName">
    @Column( name = "companyname" , unique = true)
    private String companyName ;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    // </editor-fold>

    @Override
    public OTenant clone() {
        OTenant clonedTenant = new OTenant() ;
        clonedTenant.setId(getId());
        clonedTenant.setActive(active);
        clonedTenant.setConnDriver(connDriver);
        clonedTenant.setConnPassword(connPassword);
        clonedTenant.setConnURL(connURL);
        clonedTenant.setConnUser(connUser);
        clonedTenant.setPersistenceUnitName(persistenceUnitName);
        clonedTenant.setName(name);
        clonedTenant.setCompanyName(companyName);
        return clonedTenant ;
    }
}
