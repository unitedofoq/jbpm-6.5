/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.central;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.Entity;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.FlushModeType;
import javax.persistence.LockModeType;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.metamodel.Metamodel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author bassem
 */
@Stateless
public class EntityManagerWrapper implements EntityManagerWrapperLocal {

    private static final String JNDI_ENV = "java:comp/env/persistence/";
    @Resource
    SessionContext context;
    @EJB
    OCentralEntityManagerRemote centralEntityManagerRemote;

    static Map<String, EntityManager> createdTenantEntityManagers = new HashMap<>();

    final static Logger logger = LoggerFactory.getLogger(EntityManagerWrapper.class);

    /**
     * Any call to EntityManager functions must pass this method first. It
     * Returns EntityManager that corresponds to the passed loggedUser Tenant.
     * The tenant is used to obtain the JNDI name of its Persistence Context
     *
     * @param loggedUser
     * @return EntityManager
     */
    private EntityManager getMultiTenantEntityManager(OUser loggedUser) {
        logger.trace("Entering");
        if (loggedUser != null) {
            logger.trace("loggedUser {}", loggedUser.getLoginName());
        } else {
            logger.trace("logged user equals Null");
        }
        String tenantName = null;
        if (null != loggedUser.getTenant().getId()
                && loggedUser.getTenant().getId() != loggedUser.getWorkingTenantID()
                && loggedUser.getWorkingTenantID() != 0) {
            OTenant tenant = centralEntityManagerRemote.getUserTenant(loggedUser.getWorkingTenantID());
            if (null != tenant) {
                tenantName = tenant.getPersistenceUnitName();
            } else {
                tenantName = loggedUser.getTenant().getPersistenceUnitName();
            }
            logger.trace("tenantName:{}", tenantName);
        } else {
            tenantName = loggedUser.getTenant().getPersistenceUnitName();
        }

        if (tenantName == null && loggedUser != null) {
            logger.warn("Unknown Tenant loggedUser.getLoginName(): {}", loggedUser.getLoginName());
            throw new RuntimeException("Unknown Tenant");
        }

        String jndiName = new StringBuffer(JNDI_ENV).append(tenantName).toString();
        //Lookup the entity manager
        EntityManager em = createdTenantEntityManagers.get(jndiName);
        if (em == null) {
            em = (EntityManager) context.lookup(jndiName);
            createdTenantEntityManagers.put(jndiName, em);
        }

        if (em == null) {
            logger.warn("em == null, throwing exception");
            throw new RuntimeException("Unknown Tenant");
        }

        logger.trace("Returning");
        return em;
    }

    @Override
    public EntityManager getMultiTenantEntityManager(long tenantID) {

        String tenantName = null;
        OTenant tenant = centralEntityManagerRemote.getUserTenant(tenantID);
        if (null != tenant) {
            tenantName = tenant.getPersistenceUnitName();
            logger.trace("tenantName:{}", tenantName);
        }

        String jndiName = new StringBuffer(JNDI_ENV).append(tenantName).toString();
        //Lookup the entity manager

        EntityManager em = createdTenantEntityManagers.get(jndiName);
        if (em == null) {
            em = (EntityManager) context.lookup(jndiName);
            createdTenantEntityManagers.put(jndiName, em);
        }

        if (em == null) {
            logger.warn("em == null, throwing exception");
            throw new RuntimeException("Unknown Tenant");
        }
        logger.trace("Returning");
        return em;
    }

    @Override
    public void persist(BaseEntity entity, OUser loggedUser) {
        logger.debug("Entering");
        getMultiTenantEntityManager(loggedUser).persist(entity);
        logger.debug("Returning");
    }

    @Override
    public void flush(OUser loggedUser) {
        logger.debug("Entering");
        getMultiTenantEntityManager(loggedUser).flush();
        logger.debug("Returning");
    }

    @Override
    public BaseEntity merge(BaseEntity baseEntity, OUser loggedUser) {
        return getMultiTenantEntityManager(loggedUser).merge(baseEntity);
    }

    @Override
    public void refresh(BaseEntity entity, OUser loggedUser) {
        logger.debug("Entering");
        getMultiTenantEntityManager(loggedUser).refresh(entity);
        logger.debug("Returning");
    }

    @Override
    public void clear(OUser loggedUser) {
        logger.debug("Entering");
        getMultiTenantEntityManager(loggedUser).clear();
        logger.debug("Returning");
    }

    @Override
    public void detach(Object entity, OUser loggedUser) {
        logger.debug("Entering");
        if (entity != null && entity.getClass().isAnnotationPresent(Entity.class)) {
            getMultiTenantEntityManager(loggedUser).detach(entity);
        }
        logger.debug("Returning");
    }

    @Override
    public void detachList(List entityList, OUser loggedUser) {
        logger.debug("Entering");
        if (entityList != null && !entityList.isEmpty()
                && entityList.get(0).getClass().isAnnotationPresent(Entity.class)) {
            for (Object entity : entityList) {
                detach(entity, loggedUser);
            }
        }
        logger.debug("Returning");
    }

    @Override
    public Query createQuery(String string, OUser loggedUser) {
        return getMultiTenantEntityManager(loggedUser).createQuery(string);
    }

    @Override
    public Query createNamedQuery(String string, OUser loggedUser) {
        return getMultiTenantEntityManager(loggedUser).createNamedQuery(string);
    }

    @Override
    public Query createNativeQuery(String query, OUser loggedUser) {
        return getMultiTenantEntityManager(loggedUser).createNativeQuery(query);
    }

    @Override
    public void close(OUser loggedUser) {
        logger.debug("Entering");
        getMultiTenantEntityManager(loggedUser).close();
        logger.debug("Returning");
    }

    // <editor-fold defaultstate="collapsed" desc="Non-Overriden javax.persistence.EntityManager Methods">
    @Override
    public void flush() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T> T merge(T t) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void persist(Object o) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void remove(Object o) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T> T find(Class<T> type, Object o) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T> T find(Class<T> type, Object o, Map<String, Object> map) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T> T find(Class<T> type, Object o, LockModeType lmt) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T> T find(Class<T> type, Object o, LockModeType lmt, Map<String, Object> map) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T> T getReference(Class<T> type, Object o) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setFlushMode(FlushModeType fmt) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public FlushModeType getFlushMode() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void lock(Object o, LockModeType lmt) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void lock(Object o, LockModeType lmt, Map<String, Object> map) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void refresh(Object o) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void refresh(Object o, Map<String, Object> map) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void refresh(Object o, LockModeType lmt) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void refresh(Object o, LockModeType lmt, Map<String, Object> map) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void detach(Object o) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public LockModeType getLockMode(Object o) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setProperty(String string, Object o) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Map<String, Object> getProperties() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Query createQuery(String string) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T> TypedQuery<T> createQuery(CriteriaQuery<T> cq) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T> TypedQuery<T> createQuery(String string, Class<T> type) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Query createNamedQuery(String string) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T> TypedQuery<T> createNamedQuery(String string, Class<T> type) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Query createNativeQuery(String string) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Query createNativeQuery(String string, Class type) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Query createNativeQuery(String string, String string1) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void joinTransaction() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T> T unwrap(Class<T> type) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object getDelegate() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void close() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isOpen() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public EntityTransaction getTransaction() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public EntityManagerFactory getEntityManagerFactory() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public CriteriaBuilder getCriteriaBuilder() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Metamodel getMetamodel() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    // </editor-fold>

    public Query createQuery(CriteriaUpdate updateQuery) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Query createQuery(CriteriaDelete deleteQuery) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public StoredProcedureQuery createNamedStoredProcedureQuery(String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public StoredProcedureQuery createStoredProcedureQuery(String procedureName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public StoredProcedureQuery createStoredProcedureQuery(String procedureName, Class... resultClasses) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public StoredProcedureQuery createStoredProcedureQuery(String procedureName, String... resultSetMappings) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean isJoinedToTransaction() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public <T> EntityGraph<T> createEntityGraph(Class<T> rootType) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public EntityGraph<?> createEntityGraph(String graphName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public EntityGraph<?> getEntityGraph(String graphName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public <T> List<EntityGraph<? super T>> getEntityGraphs(Class<T> entityClass) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
