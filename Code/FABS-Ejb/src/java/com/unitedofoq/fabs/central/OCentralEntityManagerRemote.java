/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.central;

import com.unitedofoq.fabs.core.multitanantuser.UserTenantRoleTempEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;
import java.util.List;

/**
 *
 * @author bassem
 */
@Local
public interface OCentralEntityManagerRemote {

    /**
     * {@link OCentralEntityManager#getUserTenant(java.lang.String) }
     */
    public OTenant getUserTenant(String loginName);
     /**
     *
     * @param tenantDBID
     * @return
     */
    public OTenant getUserTenant(long tenantDBID);


    public List getTenantsList();

    /**
     * {@link OCentralEntityManager#getGrantedUser(com.unitedofoq.fabs.central.OTenant)
     * }
     */
    public OUser getGrantedUser(OTenant tenant);

    /**
     * {@link OCentralEntityManager#isSystemUser(com.unitedofoq.fabs.core.security.user.OUser)
     * }
     */
    public Boolean isSystemUser(OUser user);

    /**
     * {@link OCentralEntityManager#addOCentralUser(com.unitedofoq.fabs.core.security.user.OUser)
     * }
     */
    public boolean addOCentralUser(OUser user);

    public boolean updateOCentralUserTenant(String loginName, long defaultTenant, String eligibleTenants);

    public boolean removeOCentralUser(OUser user);

    public boolean updateOCentralUserDefaultTenant(String loginName, long defaultTenant);
    
    public boolean updateTenantCompanyName(Long tenantId , String companyName);
    
    public java.util.List getAdminUsers();
    public java.util.List getAllUsers();

    public List<OTenant> getUserEligibleTenants(String loginName);

    public List<OTenant> getAllTenants();
    public List getUserTenantRolesData(String persistenceUnitname);

    public void deleteUserTenantRoleEntity(UserTenantRoleTempEntity userTenantRole);

    public void updateUserTenantRoleEntity(UserTenantRoleTempEntity userTenantRole);

   public void saveUserTenantRoleEntity(UserTenantRoleTempEntity userTenantRole ,OUser loggeduser)  ;

    public String getUserTanantName(String loginName);
    @javax.ejb.TransactionAttribute(value = javax.ejb.TransactionAttributeType.REQUIRES_NEW)
    public void clearUserLogin(java.lang.String userName);
    public void killUserSession(long id);
}
