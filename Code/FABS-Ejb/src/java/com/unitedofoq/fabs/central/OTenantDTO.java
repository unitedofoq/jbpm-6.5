/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.central;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author MEA
 */

public class OTenantDTO {
    
    // <editor-fold defaultstate="collapsed" desc="Id">
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="connDriver">
    @Column(nullable=false)
    private String connDriver ;

    public String getConnDriver() {
        return connDriver;
    }

    public void setConnDriver(String connDriver) {
        this.connDriver = connDriver;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="connURL">
    @Column(nullable=false)
    private String connURL ;

    public String getConnURL() {
        return connURL;
    }

    public void setConnURL(String connURL) {
        this.connURL = connURL;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="connUser">
    @Column(nullable=false)
    private String connUser ;

    public String getConnUser() {
        return connUser;
    }

    public void setConnUser(String connUser) {
        this.connUser = connUser;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="connPassword">
    @Column(nullable=false)
    private String connPassword ;
    public String getConnPassword() {
        return connPassword;
    }

    public void setConnPassword(String connPassword) {
        this.connPassword = connPassword;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="name">
    @Column(nullable=false)
    private String name ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="active">
    private Boolean active ;

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="persistenceUnitName">
    @Column(nullable=false, unique=true)
    private String persistenceUnitName ;

    public String getPersistenceUnitName() {
        return persistenceUnitName;
    }

    public String getPersistenceUnitNameDD() {
        return "OTenant_persistenceUnitName";
    }

    public void setPersistenceUnitName(String persistenceUnitName) {
        this.persistenceUnitName = persistenceUnitName;
    }
    // </editor-fold>
    
}
