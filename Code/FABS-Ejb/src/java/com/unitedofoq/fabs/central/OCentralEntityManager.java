/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.central;

import com.unitedofoq.fabs.core.multitanantuser.UserTenantRoleTempEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Used mainly to manage the FABSCentral Persistence Context
 *
 * @author bassem
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.central.OCentralEntityManagerRemote",
        beanInterface = OCentralEntityManagerRemote.class)
public class OCentralEntityManager implements OCentralEntityManagerRemote {

    @PersistenceContext(unitName = "FABSCentral")
    private EntityManager tenantCentralEM;
    final static Logger logger = LoggerFactory.getLogger(OCentralEntityManager.class);

    //final static Logger logger = LoggerFactory.getLogger();
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public OTenant getUserTenant(String loginName) {
        logger.debug("Entering");
        String query = null;
        try {
            query = " SELECT OCU.tenant FROM OCentralUser OCU "
                    + " WHERE OCU.loginName = '" + loginName + "' ";
            logger.trace("Executing query: {}", query);
            OTenant ot = (OTenant) tenantCentralEM.createQuery(query).getSingleResult();
            if (ot == null) {
                logger.warn("No Tenant found for user:{}", loginName);
                logger.debug("Returning");
                return null;
            }
            OTenant clonedTenant = ot.clone();
            //TODO: check if User or Tenant is not active
            logger.debug("Returning");
            return clonedTenant;
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception while processing Query", ex);
            logger.debug("login name: {}", loginName);
            // </editor-fold>
        }
        logger.debug("Returning with Null");
        return null;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List getTenantsList() {
        logger.debug("Entering");
        try {
            String query = "Select entity From OTenant entity ";
            logger.trace("Executing query: {}", query);
            logger.debug("Returning");
            return tenantCentralEM.createQuery(query).getResultList();
        } catch (Exception ex) {
            logger.error("Exception while processing Query", ex);
            logger.debug("Returning with new Arraylist");
            return new ArrayList();
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void clearUserLogin(String userName) {
        logger.debug("Entering");
        try {
            String query = "Update ocentraluser"
                    + " set loggedin = false where loginname = ' "
                    + userName + "'";
            logger.trace("Executing query: {}", query);
            tenantCentralEM.createQuery(query).executeUpdate();
        } catch (Exception ex) {
            logger.error("Exception while processing Query", ex);
        }
        logger.debug("Returning ");
    }

    //<editor-fold defaultstate="collapsed" desc="UserTenantRole Code">
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<UserTenantRoleTempEntity> getUserTenantRolesData(String persistenceUnitName) {
        try {
            Vector<Object[]> v = new Vector<Object[]>();
            UserTenantRoleTempEntity tempUserTenantRole = null;
            List<UserTenantRoleTempEntity> userTenantRoles = new ArrayList<UserTenantRoleTempEntity>();
            if (null != persistenceUnitName && !"".equals(persistenceUnitName)) {
                v = (Vector) tenantCentralEM.createNativeQuery(
                        "Select * From usertenantrole where TENANT='" + persistenceUnitName + "'").getResultList();

            } else {
                v = (Vector) tenantCentralEM.createNativeQuery("Select * From usertenantrole").getResultList();
            }

            for (Object[] e : v) {
                tempUserTenantRole = new UserTenantRoleTempEntity();
                tempUserTenantRole.setCustomDbid((Long) e[0]);
                tempUserTenantRole.setActive((Integer) e[1] == 0 ? false : true);
                tempUserTenantRole.setUser((String) e[2]);
                tempUserTenantRole.setTenant((String) e[3]);
                tempUserTenantRole.setRoles((String) e[4]);

                userTenantRoles.add(tempUserTenantRole);
            }

            return userTenantRoles;
        } catch (Exception ex) {
            logger.error("Exception thrown ", ex);
            return new ArrayList<UserTenantRoleTempEntity>();
        }
    }

    public void deleteUserTenantRoleEntity(UserTenantRoleTempEntity userTenantRole) {
        try {
            tenantCentralEM.createNativeQuery("DELETE From usertenantrole "
                    + "WHERE DBID=" + userTenantRole.getDbid()).executeUpdate();
        } catch (Exception ex) {
            logger.error("Exception thrown ", ex);
        }

    }

    public void updateUserTenantRoleEntity(UserTenantRoleTempEntity userTenantRole) {
        try {
            tenantCentralEM.createNativeQuery(
                    "UPDATE usertenantrole SET user='" + userTenantRole.getUser() + "',"
                    + "active=" + (userTenantRole.isActive() == true ? 1 : 0) + ","
                    + "tenant='" + userTenantRole.getTenant() + "',"
                    + "roles='" + userTenantRole.getRoles()
                    + "' WHERE DBID=" + userTenantRole.getDbid()).executeUpdate();
            System.out.println("ss");
        } catch (Exception ex) {
            logger.error("Exception thrown ", ex);
        }

    }

    public void saveUserTenantRoleEntity(UserTenantRoleTempEntity userTenantRole, OUser loggeduser) {
        try {
            tenantCentralEM.createNativeQuery(
                    "INSERT INTO usertenantrole (active,user,tenant,roles)"
                    + "VALUES (" + (userTenantRole.isActive() == true ? 1 : 0) + ",'"
                    + userTenantRole.getUser() + "','"
                    + userTenantRole.getTenant() + "','"
                    + userTenantRole.getRoles() + "')").executeUpdate();

            System.out.println("ss");
        } catch (Exception ex) {
            logger.error("Exception thrown ", ex);
        }

    }
    //</editor-fold>

    /**
     * Return Granted system user with tenant set.
     *
     * @param tenant Tenant for which a granted system user is required.
     * <br>Must not be null
     * @return Successful: Granted system user with tenant set <br>null: error
     */
    public OUser getGrantedUser(OTenant tenant) {
        logger.debug("Entering");
        if (tenant == null) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Null Tenant Passed");
            logger.debug("Returning");
            return null;
        }
        OUser user = new OUser();
        user.setDbid(0);
        user.setTenant(tenant);
        UDC englishLang = new UDC();
        englishLang.setDbid(OUser.ENGLISH);
        user.setFirstLanguage(englishLang);
        user.getFirstLanguage().setDbid(OUser.ENGLISH);
        user.setLoginName(user.getLoginName());
        logger.debug("Returning");
        return user;
    }

    public Boolean isSystemUser(OUser user) {
        logger.debug("Entering");
        if (user == null) {
            logger.warn("Null User Passed");
            logger.debug("Returning");
            return false;
        }
        OTenant tenant = user.getTenant();
        if (tenant == null) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("No Tenant Assigned To User, User MUST Have A Tenant. User: {}", user);
            logger.debug("Returning");
            // </editor-fold>
            return false;
        }
        OUser grantedUser = getGrantedUser(tenant);
        grantedUser.setLoginName(user.getLoginName());
        logger.debug("Returning");
        return user.equals(grantedUser);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public boolean addOCentralUser(OUser user) {
        logger.debug("Entering");
        OCentralUser centralUser = new OCentralUser();
        try {
            long newId = 0l;
            OTenant tenant = tenantCentralEM.find(OTenant.class, user.getTenant().getId());

            // Manually create a new Id, because 'AUTO' generation doesn't work,
            // It assigns already existing Id
            for (long index = 1;; index++) {
                if (tenantCentralEM.find(OCentralUser.class, index) == null) {
                    newId = index;
                    break;
                }
            }

            centralUser.setId(newId);
            centralUser.setLoginName(user.getLoginName());
            centralUser.setTenant(tenant);
            centralUser.setActive(true);
            tenantCentralEM.merge(centralUser);
            tenantCentralEM.persist(centralUser);
            logger.debug("Returning with true");
            return true;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("User: {}", user);
            logger.debug("Returning with false");
            return false;
        }
    }

    /**
     * This function updates central user eligible tenants
     *
     * @param loginName Central User loginName
     * @param defaultTenant Default Tenant Id
     * @param eligibleTenants comma separated tenant IDs
     * @return true if successful, false otherwise
     */
    @Override
    public boolean updateOCentralUserTenant(String loginName, long defaultTenant, String eligibleTenants) {
        try {
            // get userId
            Object[] result = (Object[]) tenantCentralEM.createNativeQuery("SELECT id,TENANT_ID FROM OCentralUser WHERE loginName like '" + loginName + "'").getSingleResult();
            String userId = String.valueOf(result[0]);
            String currentDefaultTenantId = String.valueOf(result[1]);
            // update user default tenant if current default tenant is removed for the list of eligibleTenants.
            if (!eligibleTenants.contains(String.valueOf(currentDefaultTenantId))) {
                tenantCentralEM.createNativeQuery("UPDATE ocentraluser SET TENANT_ID= " + defaultTenant + " WHERE id=" + userId).executeUpdate();
            }
            // clean any existing records.
            tenantCentralEM.createNativeQuery("DELETE FROM centuser_tenants WHERE OCENTRALUSER_ID=" + userId).executeUpdate();
            for (String tenantId : eligibleTenants.split(",")) {
                tenantCentralEM.createNativeQuery("INSERT INTO centuser_tenants(OCENTRALUSER_ID, ELIGIBLETENANTS_ID) "
                        + " VALUES (" + userId + "," + tenantId + ")").executeUpdate();
            }
            return true;
        } catch (Exception e) {
            logger.error("Exception while updating user tenant", e);
        }

        return false;
    }

    /**
     * Retrieve user eligible tenants
     *
     * @param loginName user login name
     * @return list of eligible tenants for user
     */
    @Override
    public List<OTenant> getUserEligibleTenants(String loginName) {
        List<OTenant> tenants = null;
        try {
            String query = "select eligibleTenants_ID from centuser_tenants where  OCENTRALUSER_ID = ";
            tenants = tenantCentralEM.createQuery("select o.eligibleTenants from OCentralUser o WHERE o.loginName like '" + loginName + "'").getResultList();
        } catch (Exception e) {
            logger.error("Exception while retriving user tenants", e);
        }
        return tenants;
    }

    /**
     * Update User default Tenant
     *
     * @param loginName user login name
     * @param defaultTenant tenant Id
     * @return true if successful, false otherwise
     */
    @Override
    public boolean updateOCentralUserDefaultTenant(String loginName, long defaultTenant) {
        try {
            tenantCentralEM.createNativeQuery("UPDATE ocentraluser SET TENANT_ID=" + defaultTenant + " WHERE LOGINNAME LIKE '" + loginName + "'").executeUpdate();
            return true;
        } catch (Exception e) {
            logger.error("Exception while updating user default tenant", e);
        }
        return false;
    }

    @Override
    public List getAdminUsers() {
        logger.debug("Entering");
        try {
            String query = "Select entity From OCentralUser entity "
                    + "Where entity.adminUser=1";
            logger.trace("Query: {}", query);
            logger.debug("Returning");
            return tenantCentralEM.createQuery(query).getResultList();
        } catch (Exception ex) {
            logger.error("Exception while processing Query", ex);
            logger.debug("Returning with new Arraylist");
            return new ArrayList();
        }
    }

    @Override
    public List getAllUsers() {
        logger.debug("Entering");
        try {
            String query = "Select entity From OCentralUser entity where entity.loggedIn = 1";
            logger.trace("Query: {}", query);
            logger.debug("Returning");
            return tenantCentralEM.createQuery(query).getResultList();
        } catch (Exception ex) {
            logger.error("Exception while processing Query", ex);
            logger.debug("Returning");
            return new ArrayList();
        }
    }

    public void killUserSession(long id) {
        logger.debug("Entering");
        try {
            String query = "Select entity From OCentralUser entity  Where entity.id = " + id;
            logger.trace("Query: {}", query);
            List<OCentralUser> users = (List<OCentralUser>) tenantCentralEM.createQuery(query).getResultList();
            users.get(0).setLoggedIn(false);
            tenantCentralEM.merge(users.get(0));

        } catch (Exception ex) {
            logger.error("Exception while processing Query", ex);
        }
        logger.debug("Returning");
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public boolean removeOCentralUser(OUser user) {
        logger.debug("Entering");
        OCentralUser centralUser = new OCentralUser();
        String loginName = user.getLoginName();
        try {
            String query = " SELECT OCU.tenant FROM OCentralUser OCU "
                    + " WHERE OCU.loginName = '" + loginName + "' ";
            logger.trace("Query: {}", query);
            OTenant ot = (OTenant) tenantCentralEM.createQuery(query).getSingleResult();
            OTenant tenant = tenantCentralEM.find(OTenant.class, ot.getId());
            tenantCentralEM.remove(tenant);
            tenantCentralEM.merge(centralUser);
            tenantCentralEM.persist(centralUser);
            logger.debug("Returning with true");
            return true;
        } catch (Exception ex) {
            logger.error("Exception while processing Query", ex);
            logger.debug("User: {}", loginName);
            logger.debug("Returning with false");
            return false;
        }
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public OTenant getUserTenant(long tenantDBID) {
        logger.debug("Entering");
        String query = null;
        try {
            query = " SELECT tenant FROM OTenant tenant "
                    + " WHERE tenant.id= " + tenantDBID;
            logger.trace("Query: {}", query);
            OTenant ot = (OTenant) tenantCentralEM.createQuery(query).getResultList().get(0);
            if (ot == null) {
                logger.warn("No Tenant found for tenantDBID:{}", tenantDBID);
                logger.debug("Returning");
                return null;
            }
            OTenant clonedTenant = ot.clone();
            //TODO: check if User or Tenant is not active
            logger.debug("Returning");
            return clonedTenant;
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception while processing Query", ex);
            logger.debug("Returning");
            // </editor-fold>
        }
        logger.debug("Returning with Null");
        return null;
    }

    @Override
    public boolean updateTenantCompanyName(Long tenantId, String companyName) {
        tenantCentralEM.createNativeQuery("UPDATE otenant SET companyname='" + companyName + "' WHERE ID =" + tenantId).executeUpdate();
        return true;
    }

    @Override
    public List<OTenant> getAllTenants() {

        List<OTenant> tenants = null;
        try {
            tenants = tenantCentralEM.createQuery("select o FROM OTenant o ").getResultList();
        } catch (Exception e) {
            logger.error("Exception while retriving user tenants", e);
        }
        return tenants;
    }

    @Override
    public String getUserTanantName(String loginName) {
        OTenant UserOTenant = getUserTenant(loginName);
        String tenantName = UserOTenant.getCompanyName();
        logger.info("user log in with name " + loginName + ",his tenant name is " + tenantName);
        return tenantName;
    }
}
