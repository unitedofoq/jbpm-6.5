/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.content;

import com.unitedofoq.fabs.core.Exception.BasicException;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;

/**
 *
 * @author lap
 */
public interface AttachmentServiceLocal {

    public String put(AttachmentDTO attachment, long tenantID, BaseEntity entity) throws BasicException;

    public AttachmentDTO get(long tenantID, String attachmentID) throws BasicException;

    public void delete(String attachmentID, long tenantID) throws BasicException;

    public void update(AttachmentDTO attachment, long tenantID) throws BasicException;   
}
