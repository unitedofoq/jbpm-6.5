/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.comunication;

import java.util.List;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.alert.entites.dto.AlertDTO;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author melsayed
 */
@Local
public interface EMailSenderLocal {

    public boolean sendEMail(String from, String to, String subject, String body);

    public boolean sendEMail(String from, AlertDTO alertDTO, OUser loggedUser);

    public boolean sendHTMLEMail(String from, String to, String subject, String body, OUser loggedUser);

    public List<String> sendEMailToAll(String from, List<String> to, String subject, String body);

    public boolean sendEMail(String from, String to, String subject, String body, String userEmail, String userPassword, String portNumber, String SMTP);

    public List<String> sendEMailToAll(String from, List<String> to, String subject, String body, String userEmail, String userPassword, String portNumber, String SMTP);

    public boolean sendEMailWithAttachment(String from, String to, String subject, String body, String userEmail, String userPassword, String portNumber, String SMTP, String filePath);

    public boolean sendHTMLCustomEMail(java.lang.String from, java.lang.String to, java.lang.String subject, java.lang.String body, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public void persistFiredAlert(String alertType,
            String subject,
            String body,
            String toEmail,
            OEntity actOnEntity,
            long entityDBID,
            OUser loogedUser);
}
