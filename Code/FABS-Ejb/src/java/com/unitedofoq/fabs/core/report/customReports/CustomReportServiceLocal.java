package com.unitedofoq.fabs.core.report.customReports;

import java.io.ByteArrayInputStream;
import javax.ejb.Local;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author bgalal
 */
@Local
public interface CustomReportServiceLocal {

    ByteArrayInputStream generateReport(AbstractReportGenerator reportGenerator);
    
    String reportUserExitJavaFunction(ReportDTO reportDTO);
}
