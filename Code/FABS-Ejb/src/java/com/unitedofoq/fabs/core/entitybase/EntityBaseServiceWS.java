/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitybase;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;

import com.unitedofoq.fabs.core.security.user.OUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.logging.Level;

/**
 *
 * @author mustafa
 */
@Stateless
public class EntityBaseServiceWS implements EntityBaseServiceWSLocal {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @EJB
    private EntitySetupServiceRemote entitySetupService;

    @EJB
    private OEntityManagerRemote oem;

    OUser loggedUser = null;

    final static Logger logger = LoggerFactory.getLogger(EntityBaseServiceWS.class);

    @Override
    public String setValueInEntity(String dbid, String entityName, String fieldExpression, String value, String loginName) {
        logger.debug("Entering");
        if (dbid == null || dbid.equals("") || entityName == null || entityName.equals("") || fieldExpression == null || fieldExpression.equals("")) {
            return "-1";
        }

        try {
            loggedUser = oem.getUserForLoginName(loginName);

            if (fieldExpression.contains(".")) {
                logger.warn("Multilevel Exps. Not Supported Now");
                logger.debug("Returning");
                return "-1";
            }

            if (loggedUser == null) {
                logger.debug("Returning");
                return "-1";
            }

            BaseEntity entity = oem.loadEntity(entityName, Long.parseLong(dbid), null, null, loggedUser);

            entity = (BaseEntity) setValueInProcessEntity(entity, fieldExpression, value);
            entitySetupService.callEntityUpdateAction(entity, loggedUser);

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning");
            return "-1";
        }
        logger.debug("Returning");
        return "1";
    }

    public Object setValueInProcessEntity(Object entity, String expression, Object value) {
        logger.debug("Entering");
        BaseEntity baseEntity = (BaseEntity) entity;
        Object entityTMP = null;
        String lookupObjExpression = expression;
        String exp = "";
        Boolean editInRelation = false;
        //update the relation between two objects
        if (expression.contains("dbid")) {
            editInRelation = true;
        }
        //update the inner object
        while (lookupObjExpression.contains("__")) {
            int expressionLength = lookupObjExpression.split("\\__").length;
            lookupObjExpression = lookupObjExpression.substring(2, lookupObjExpression.length());
            if (lookupObjExpression.contains("__")) {
                exp = lookupObjExpression.substring(0, lookupObjExpression.indexOf("__"));
                lookupObjExpression = lookupObjExpression.substring(exp.length(), lookupObjExpression.length());

                
                try {
                    entityTMP = BaseEntity.getValueFromEntity(entity, exp);
                } catch (Exception ex) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.error("Exception thrown: ", ex);
                    // </editor-fold>
                }
            }
            if (expressionLength > 3) {
                if (entityTMP != null) {
                    entity = entityTMP;
                }
                baseEntity = (BaseEntity) entity;
            }
        }
        //in case of updating the relation between two objects
        if (editInRelation) {
            try {
                Field field = BaseEntity.getClassField(baseEntity.getClass(), exp);
                BaseEntity loadedEntity = oem.loadEntity(BaseEntity.getFieldType(field).getName(), Long.parseLong(value.toString()), null, null, loggedUser);
                baseEntity.invokeSetter(exp, loadedEntity);
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown: ", ex);
                // </editor-fold>
            }
        } else {
            //in case of uppdating the inner object or normal update
            try {
                ((BaseEntity) entity).invokeSetter(lookupObjExpression, value);
            } catch (NoSuchMethodException ex) {
                java.util.logging.Logger.getLogger(EntityBaseServiceWS.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        logger.debug("Returning");
        return entity;
    }
}
