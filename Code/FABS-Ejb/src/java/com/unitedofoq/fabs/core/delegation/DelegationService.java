/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.delegation;

import com.unitedofoq.fabs.bpm.common.model.DelegationData;
import com.unitedofoq.fabs.bpm.model.Task;
import com.unitedofoq.fabs.bpm.services.TaskService;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Stack;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.process.ProcessServiceRemote;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserServiceRemote;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author aelzaher
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.delegation.DelegationServiceRemote",
    beanInterface = DelegationServiceRemote.class)
public class DelegationService implements DelegationServiceRemote {
    
    @EJB
    OFunctionServiceRemote oFunctionService;
    @EJB
    UserMessageServiceRemote userMessageService;
    @EJB
    OEntityManagerRemote oem;
    @EJB
    private ProcessServiceRemote processService;
    @EJB
    private TaskService taskService;
    @EJB
    private UserServiceRemote userService;
    final static Logger logger = LoggerFactory.getLogger(DelegationService.class);
    
    @Override
    public OFunctionResult validateTaskDelegation (ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        logger.debug("Entering: validateTaskDelegation");
        OFunctionResult oFR = new OFunctionResult();
        
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = oFunctionService.validateJavaFunctionODM(
                odm, loggedUser, TaskDelegation.class);
        if (inputValidationErrors != null) {
            logger.debug("Returning from: validateTaskDelegation");
            return inputValidationErrors;
        }
        // </editor-fold>

        try {
            TaskDelegation newDelegation = (TaskDelegation) odm.getData().get(0);
            if (newDelegation.getOwner() == null) {
                newDelegation.setOwner(loggedUser);
            }
            if (newDelegation.getDelegate().isInActive()) {
                oFR.addError(userMessageService.getUserMessage("InActiveUser", loggedUser));
                logger.debug("Returning from: validateTaskDelegation");
                return oFR;
            }
            if (newDelegation.getOwner().getDbid() == newDelegation.getDelegate().getDbid()) {
                oFR.addError(userMessageService.getUserMessage("InvalidDelegation", loggedUser));
                logger.debug("Returning from: validateTaskDelegation");
                return oFR;
            }
            Calendar currentcal = Calendar.getInstance();
            Calendar fromcal = Calendar.getInstance();
            fromcal.setTime(newDelegation.getFromDate());
            fromcal.set(fromcal.get(Calendar.YEAR), fromcal.get(Calendar.MONTH)
                    , fromcal.get(Calendar.DAY_OF_MONTH), 0, 0);
            if (fromcal.before(currentcal)) {
                if (currentcal.get(Calendar.YEAR) == fromcal.get(Calendar.YEAR)
                        && currentcal.get(Calendar.MONTH) == fromcal.get(Calendar.MONTH)
                        && currentcal.get(Calendar.DAY_OF_MONTH) == fromcal.get(Calendar.DAY_OF_MONTH)) {
                    fromcal = currentcal;
                    fromcal.set(Calendar.MINUTE, fromcal.get(Calendar.MINUTE)-1);
                } else {
                    oFR.addError(userMessageService.getUserMessage("FromDateError", loggedUser));
                    logger.debug("Returning from: validateTaskDelegation");
                    return oFR;
                }
            }
            Calendar tocal = Calendar.getInstance();
            tocal.setTime(newDelegation.getToDate());
            tocal.set(tocal.get(Calendar.YEAR), tocal.get(Calendar.MONTH), tocal.get(Calendar.DAY_OF_MONTH), 23, 59);
            newDelegation.setFromDate(fromcal.getTime());
            newDelegation.setToDate(tocal.getTime());
            if (newDelegation.getFromDate().after(newDelegation.getToDate())) {
                oFR.addError(userMessageService.getUserMessage("DateFromToError", loggedUser));
                logger.debug("Returning from: validateTaskDelegation");
                return oFR;
            }
//            if (oem.getUserForLoginName(newDelegation.getOwner().getLoginName()).getTenant().getId() 
//                    != oem.getUserForLoginName(newDelegation.getDelegate().getLoginName()).getTenant().getId()) {
//                oFR.addError(userMessageService.getUserMessage("OutrangedDelegation", loggedUser));
//                logger.debug("Returning from: validateTaskDelegation");
//                return oFR;
//            }
            List<String> cond = new ArrayList<String>();
            cond.add("owner.dbid=" + newDelegation.getDelegate().getDbid());
            List<TaskDelegation> delegations = oem.loadEntityList("TaskDelegation", cond, null,
                    null, oem.getSystemUser(loggedUser));
            cond.clear();
            cond.add("delegate.dbid = " + newDelegation.getOwner().getDbid());
            delegations.addAll(oem.loadEntityList("TaskDelegation", cond, null,
                    null, oem.getSystemUser(loggedUser)));
            
            for (int i = 0; i < delegations.size(); i++) {
                TaskDelegation delegation = delegations.get(i);
                if (delegation.getDbid() == newDelegation.getDbid()) {
                    continue;
                }
                Date f1 = newDelegation.getFromDate(), f2 = delegation.getFromDate(), 
                        e1 = newDelegation.getToDate(), e2 = delegation.getToDate();
                if (isDateBetweenInclusive(f2, f1, e1) || isDateBetweenInclusive(e2, f1, e1)
                        || isDateBetweenInclusive(f1, f2, e2) || isDateBetweenInclusive(e1, f2, e2)) {
                    oFR.addError(userMessageService.getUserMessage("OverlapedDelegation", loggedUser));
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            oFR.addError(userMessageService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        logger.debug("Returning from: validateTaskDelegation");
        return oFR;
    }
    
    private boolean isDateBetweenInclusive (Date dateToCheck, Date from, Date to) {
        logger.debug("Entering: isDateBetweenInclusive");
        logger.debug("Returning from: isDateBetweenInclusive");
        return dateToCheck.equals(to) || dateToCheck.equals(from) || (from.before(dateToCheck) && to.after(dateToCheck)) ;
    }
    
    @Override
    public String getDelegate(String loginName, OUser loggedUser) {
        logger.debug("Entering: getDelegate");
        List<TaskDelegation> delegations = null;
        Stack<String> userNames = new Stack<String>();
        userNames.add(loginName);
        String result = "";
        List<String> names = new ArrayList<String>();
        String currentDateTime = "'" + new Timestamp(Calendar.getInstance().getTime().getTime()).toString() + "'";
        while (!userNames.isEmpty()) {
            String currentLoginName = userNames.pop();
            try {
                String query="SELECT entity FROM TaskDelegation entity WHERE "
                        + "entity.owner.dbid = (select user.dbid from OUser user where user.loginName= '" 
                        + currentLoginName + "') AND NOT entity.fromDate > " + currentDateTime 
                        + " AND NOT entity.toDate < " + currentDateTime;
                logger.debug("Query :{}",query);
                delegations = oem.executeEntityListQuery(query , loggedUser);
            } catch (Exception ex) {
                logger.error("Exception thrown while executing query",ex);
            }
            if (delegations == null || delegations.isEmpty()) {
                if (!names.contains(currentLoginName)) {
                    names.add(currentLoginName);
                }
                continue;
            }
            for (int i = 0; i < delegations.size(); i++) {
                TaskDelegation taskDelegation = delegations.get(i);
                userNames.push(taskDelegation.getDelegate().getLoginName());
            }
        }
        for (int i = 0; i < names.size(); i++) {
            result += (i == 0 ? "" : ",") + names.get(i);
        }
        logger.debug("Returning from: getDelegate");
        return result;
    }
    
    

    @Override
    public List<TaskDelegation> getDelegationsToUser(Long groupID, OUser loggedUser) {
        List<TaskDelegation> delegations = new ArrayList<TaskDelegation>();
        try {
            List<String> cond = new ArrayList<String>();

        cond.add("delegate.dbid=" + loggedUser.getDbid());
        if (groupID != 0) {            
            cond.add("group.dbid=" + groupID);
            
            //General Delegations (With No Groups) 
            delegations.addAll(oem.loadEntityList("TaskDelegation", 
                    Arrays.asList("delegate.dbid=" + loggedUser.getDbid() , "group is null" ), null,
                    null, oem.getSystemUser(loggedUser)));
        }
            //get delegations on certain group
            delegations.addAll(oem.loadEntityList("TaskDelegation", cond, null,
                    null, oem.getSystemUser(loggedUser)));
            
            
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.warn("Couldn't Load Process For Group ID : " + groupID);            
        }
        return delegations;
    }
    
    @Override
    public List<TaskDelegation> getDelegationsFromUser(Long groupID, OUser loggedUser) {
        List<TaskDelegation> delegations = new ArrayList<TaskDelegation>();
        try {
            List<String> cond = new ArrayList<String>();

        cond.add("owner.dbid=" + loggedUser.getDbid());
        if (groupID != 0) {            
            cond.add("group.dbid=" + groupID);
            
            //General Delegations (With No Groups) 
            delegations.addAll(oem.loadEntityList("TaskDelegation", 
                    Arrays.asList("owner.dbid=" + loggedUser.getDbid() , "group is null" ), null,
                    null, oem.getSystemUser(loggedUser)));
        }
            //get delegations on certain group
            delegations.addAll(oem.loadEntityList("TaskDelegation", cond, null,
                    null, oem.getSystemUser(loggedUser)));
            
            
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.warn("Couldn't Load Process For Group ID : " + groupID);            
        }
        return delegations;
    }

    @Override
    public List<DelegationData> getDelegationDataList(List<TaskDelegation> delegations, OUser loggedUser) {
        List<DelegationData> delegationDataList = new ArrayList<>();
        for(Delegation delegation : delegations){
            DelegationData delegationData = new DelegationData();
            delegationData.setOwner(delegation.getOwner().getLoginName());
            delegationData.setDelegate(delegation.getDelegate().getLoginName());
            delegationData.setFromTime(delegation.getFromDate());
            delegationData.setToTime(delegation.getToDate());
            Long groupId = delegation.getGroup() == null ? 0 : delegation.getGroup().getDbid();
            delegationData.setProcessIds(processService.getGroupProcesses(groupId, loggedUser));
            delegationDataList.add(delegationData);
        }
        return delegationDataList;
    }

}
