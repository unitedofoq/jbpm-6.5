/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.mailmerge;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;

import com.unitedofoq.fabs.core.security.user.OUser;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MEA
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.mailmerge.MailMergeServiceLocal",
        beanInterface = MailMergeServiceLocal.class)
public class MailMergeService implements MailMergeServiceLocal {

    @EJB
    private OEntityManagerRemote oem;
    final static Logger logger = LoggerFactory.getLogger(MailMergeService.class);
    @Override
    public OFunctionResult generateTemplate(ODataMessage oDM, OFunctionParms funcParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {

            MailMergeContent mailMergeContent = ((MailMergeContent) oem.loadEntity(MailMergeContent.class.getSimpleName(),
                    Collections.singletonList("mailMergeBasic.dbid = "
                    + ((MailMergeBasic) oDM.getData().get(1)).getDbid()), null, loggedUser));

            BaseEntity entity = (BaseEntity) oDM.getData().get(0);

            String contents = mailMergeContent.getTemplateContent();
            String tempContents = "";
            int lastMatchingIndex = 0;

            String patternStr = "\\[\\[(.*?)\\]\\]";
            Pattern pattern = Pattern.compile(patternStr);
            Matcher matcher = pattern.matcher(contents);
            List<String> values = new ArrayList<String>();

            List<String> fieldExps = new ArrayList<String>();
            while (matcher.find()) {
                fieldExps.add(matcher.group().substring(2, matcher.group().length() - 2));
            }

            pattern = Pattern.compile(patternStr);
            matcher = pattern.matcher(contents);

            entity = oem.loadEntity(entity.getClass().getSimpleName(), entity.getDbid(), null, fieldExps, loggedUser);

            while (matcher.find()) {
                tempContents += contents.substring(lastMatchingIndex, matcher.start());
                String filedEXPR = matcher.group().substring(2, matcher.group().length() - 2);

                List<ExpressionFieldInfo> infos = BaseEntity.parseFieldExpression(entity.getClass(), filedEXPR);

                Object obj = BaseEntity.getValueFromEntity(entity, filedEXPR);
                if (null != obj && infos.get(infos.size() - 1).field.getType().equals(Date.class)) {
                    String datePattern = "dd/MM/yyyy";
                    if (loggedUser.getFirstLanguage().getDbid() == 24) {
                        datePattern = "yyyy/MM/dd";
                    }

                    SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);
                    obj = dateFormat.format((Date) obj);
                }

                if (obj == null) {
                    obj = matcher.group();
                }
                tempContents += obj;
                lastMatchingIndex = matcher.end();
            }
            tempContents += contents.substring(lastMatchingIndex);

            values.add(tempContents);
            oFR.setReturnValues(values);
            logger.debug("Returning");
            return oFR;
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown",ex);
            // </editor-fold>
        }
        logger.debug("Returning");
        return oFR;
    }
}
