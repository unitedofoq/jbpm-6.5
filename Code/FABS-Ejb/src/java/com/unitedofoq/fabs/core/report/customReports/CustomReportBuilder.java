/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.report.customReports;

import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.eclipse.birt.core.framework.IPlatformContext;
import org.eclipse.birt.core.framework.PlatformFileContext;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.model.api.OdaDataSetHandle;
import org.eclipse.birt.report.model.api.ReportDesignHandle;
import org.eclipse.birt.report.model.api.activity.SemanticException;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author bgalal
 */
public final class CustomReportBuilder {

    private final static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(CustomReportBuilder.class);

    private IReportEngine birtReportEngine;
    private IReportRunnable reportDesign;
    private IRunAndRenderTask runAndRenderTask;
    private ReportDesignHandle reportDesignHandle;
    private final ReportDTO reportDTO;

    public CustomReportBuilder(ReportDTO reportDTO) {
        this.reportDTO = reportDTO;
        byte[] data;
        if (reportDTO.isFitPage()) {
            data = fitPage(reportDTO.getEntityAttachment().getAttachment());

        } else {
            data = reportDTO.getEntityAttachment().getAttachment();
        }
        init(data);
    }

    private void init(byte[] data) {
        IPlatformContext context = new PlatformFileContext();
        birtReportEngine = BirtEngine.getBirtEngine(context);
        try {
            reportDesign = birtReportEngine.openReportDesign(new ByteArrayInputStream(data));
        } catch (Exception ex) {
            LOGGER.error("Exception thrown: ", ex);
        }
        if (null != reportDesign) {
            runAndRenderTask = birtReportEngine.createRunAndRenderTask(reportDesign);
            reportDesignHandle = (ReportDesignHandle) reportDesign.getDesignHandle();
        }
        addReportDataSets(reportDTO);

    }

    public void addReportDataSets(ReportDTO reportDTO) {
        //int numberOfDataSets = getReportDesignHandle().getAllDataSets().size();
        List DataSets = getReportDesignHandle().getAllDataSets();
        for (int i = 0; i < DataSets.size(); i++) {
            String DataSetName = ((OdaDataSetHandle) DataSets.get(i)).getProperty("name").toString();//"Data Set"+((t==0)?"":t+"");//((OdaDataSetHandle) getReportDesignHandle().findDataSet("Data Set")).getStringProperty(OdaDataSetHandle.QUERY_TEXT_PROP);
            reportDTO.addDataSetQuery(DataSetName, ((OdaDataSetHandle) getReportDesignHandle().findDataSet(DataSetName)).getStringProperty(OdaDataSetHandle.QUERY_TEXT_PROP));

        }

    }

    public IRunAndRenderTask getRunAndRenderTask() {
        return runAndRenderTask;
    }

    public ReportDesignHandle getReportDesignHandle() {
        return reportDesignHandle;
    }

    public String getDataSetName() {
        return ((OdaDataSetHandle) getReportDesignHandle().findDataSet("Data Set")).getName();
    }

    public CustomReportBuilder buildReportQuery(String whereConditions, String reportName, ODataType screenDataLoadingUEDT) {
        String tempQuery;
        //String oldQuery = reportDTO.getDataSetQuery("Data Set");
        for (Map.Entry<String, String> entry : reportDTO.getDataSetIterator()) {
            String oldQuery = entry.getValue();
            String dataSetName = entry.getKey();
            int[] wordsAfterWhereIndexes = new int[7];
            int tempIndex = 10000000;

            if (null != whereConditions) {
                wordsAfterWhereIndexes[0] = oldQuery.toLowerCase().indexOf("having");
                wordsAfterWhereIndexes[1] = oldQuery.toLowerCase().indexOf("order by");
                wordsAfterWhereIndexes[2] = oldQuery.toLowerCase().indexOf("group by");
                wordsAfterWhereIndexes[3] = oldQuery.toLowerCase().indexOf("limit");
                wordsAfterWhereIndexes[4] = oldQuery.toLowerCase().indexOf("procedure");
                wordsAfterWhereIndexes[5] = oldQuery.toLowerCase().indexOf("into outfile");
                wordsAfterWhereIndexes[6] = oldQuery.toLowerCase().indexOf("for update");
                for (int i = 0; i < wordsAfterWhereIndexes.length; i++) {
                    if (wordsAfterWhereIndexes[i] != -1 && wordsAfterWhereIndexes[i] < tempIndex) {
                        tempIndex = wordsAfterWhereIndexes[i];
                    }
                }
                if (dataSetName.contains("Data Set") &&(reportDTO.isApplyingFilter() || dataSetName.equals("Data Set"))) {
                    if (!oldQuery.toLowerCase().contains("where")) {
                        if (tempIndex != 10000000) {
                            tempQuery = oldQuery.substring(0, tempIndex - 1) + " " + whereConditions + " " + oldQuery.substring(tempIndex);
                        } else if (oldQuery.toLowerCase().contains("oimage") || oldQuery.toLowerCase().contains("logo")) {
                            tempQuery = oldQuery;
                        } else {
                            tempQuery = oldQuery + " " + whereConditions;
                        }
                    } else if (oldQuery.toLowerCase().contains("oimage") || oldQuery.toLowerCase().contains("logo")) {
                        tempQuery = oldQuery;
                    } else {
                        String whereConditionalString = whereConditions + " and ";
                        tempQuery = (oldQuery.contains("where".toUpperCase())) ? oldQuery.replace("where".toUpperCase(), whereConditionalString) : oldQuery.replace("where", whereConditionalString);
                    }
                } else {
                    tempQuery = oldQuery;
                }

                reportDTO.addDataSetQuery(dataSetName, tempQuery);

            }
        }
        return this;
    }

    public Connection getConnectionOnDataSource(OUser loggedUser) {

        DataSource datasource;
        Connection connection = null;

        try {

            Context initContext = new InitialContext();
            datasource = (DataSource) initContext.lookup("jdbc/"
                    + loggedUser.getTenant().getPersistenceUnitName());
            connection = datasource.getConnection();

        } catch (NamingException | SQLException ex) {
            Logger.getLogger(CustomReportBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }

        return connection;
    }

    public CustomReportBuilder buildDataSetHandle() {

        boolean thereIsAtLeastOneDataSet = false;
        for (Map.Entry<String, String> entry : reportDTO.getDataSetIterator()) {

            OdaDataSetHandle dataSetHandle = (OdaDataSetHandle) getReportDesignHandle().findDataSet(entry.getKey());
            try {

                if (dataSetHandle != null) {

                    thereIsAtLeastOneDataSet = true;
                    dataSetHandle.clearProperty(OdaDataSetHandle.QUERY_TEXT_PROP);
                    dataSetHandle.setProperty(OdaDataSetHandle.QUERY_TEXT_PROP, entry.getValue());
                }
            } catch (SemanticException ex) {

                LOGGER.error("Exception thrown: ", ex);
            }
        }
        if (thereIsAtLeastOneDataSet) {

            runAndRenderTask.getAppContext().put("OdaJDBCDriverPassInConnection", getConnectionOnDataSource(reportDTO.getLoggedUser()));
        }
        return this;
    }

    public CustomReportBuilder buildReportOrientation(boolean localized, boolean revertLang) {
        if (localized) {
            try {
                reportDesignHandle.setBidiOrientation(reportDTO.getLoggedUser().isRtl() ? "rtl" : "ltr");
            } catch (Exception ex) {
                LOGGER.error("Exception thrown: ", ex);
            }
            if (null == reportDTO.getParametersMap() || reportDTO.getParametersMap().isEmpty()) {
                reportDTO.setParametersMap(new HashMap<String, Object>());
            }
            reportDTO.getParametersMap().put("lang#_#decimal", reportDTO.getLoggedUser().getFirstLanguage().getDbid());
        }
        if (revertLang) {
            try {
                reportDesignHandle.setBidiOrientation(reportDTO.getLoggedUser().getFirstLanguage().getDbid() != 23 ? "ltr" : "rtl");
            } catch (Exception ex) {
                LOGGER.error("Exception thrown", ex);
            }
            if (null == reportDTO.getParametersMap() || reportDTO.getParametersMap().isEmpty()) {
                reportDTO.setParametersMap(new HashMap<String, Object>());
            }
            reportDTO.getParametersMap().put("lang#_#decimal", reportDTO.getLoggedUser().getSecondLanguage().getDbid());
        }
        return this;
    }

    public CustomReportBuilder buildEncryptionKeyInParametersMap() {
        if (reportDTO.isEncryptionEnabled()) {
            if (null == reportDTO.getParametersMap() || reportDTO.getParametersMap().isEmpty()) {
                reportDTO.setParametersMap(new HashMap<String, Object>());
            }
            reportDTO.getParametersMap().put("symmetricKey#_#String", reportDTO.getSymmetricKey());
            reportDTO.getParametersMap().put("ivkey#_#String", reportDTO.getIvKey());
        }
        return this;
    }

    public CustomReportBuilder buildParametersValue() throws NumberFormatException {

        if (reportDTO.getParametersMap() != null && !reportDTO.getParametersMap().isEmpty()) {
            String paramName;
            String paramType;
            for (Map.Entry<String, Object> entry : reportDTO.getParametersMap().entrySet()) {
                paramName = entry.getKey().substring(0, entry.getKey().indexOf("#_#"));
                paramType = entry.getKey().substring(entry.getKey().indexOf("#_#") + 3);
                //if parameters aren't be set
                if (null == entry.getValue() || entry.getValue().equals("")) {
                    if (entry.getKey().contains("symmetricKey")) {
                        LOGGER.warn("No symmetricKey");

                    }
                    continue;
                }
                if (paramType.equalsIgnoreCase("integer")) {
                    runAndRenderTask.setParameterValue(paramName, Integer.parseInt(entry.getValue().toString()));
                } else if (paramType.equalsIgnoreCase("decimal") || paramType.equalsIgnoreCase("float")) {
                    if (entry.getValue() instanceof BaseEntity) {
                        String value = Long.toString(((BaseEntity) entry.getValue()).getDbid());
                        runAndRenderTask.setParameterValue(paramName, value);
                    } else if (entry.getValue() instanceof Long) {
                        String value = entry.getValue().toString();
                        runAndRenderTask.setParameterValue(paramName, value);
                    } else {
                        runAndRenderTask.setParameterValue(paramName, Double.parseDouble(entry.getValue().toString()));
                    }
                } else if (paramType.equalsIgnoreCase("datetime")) {
                    Date d = (Date) entry.getValue();
                    runAndRenderTask.setParameterValue(paramName, d);
                } else if (paramType.equalsIgnoreCase("date")) {
                    Date dd = (Date) entry.getValue();
                    java.sql.Date d = new java.sql.Date(dd.getTime());
                    runAndRenderTask.setParameterValue(paramName, d);
                } else if (paramType.equalsIgnoreCase("time")) {
                    Date dd = (Date) entry.getValue();
                    Time t = new Time(dd.getTime());
                    runAndRenderTask.setParameterValue(paramName, t);
                } else if (paramType.equalsIgnoreCase("boolean")) {
                    runAndRenderTask.setParameterValue(paramName, Boolean.parseBoolean(entry.getValue().toString()));
                } else {
                    runAndRenderTask.setParameterValue(paramName, entry.getValue().toString());
                }
            }
        }
        return this;
    }

    private String calculate(String factor, String nodeTextContent) {
        factor = factor.replace("mm", "");
        Double pageWidthValue = Double.parseDouble(factor);
        Double columnWidthValue = null;
        if (nodeTextContent.endsWith("mm")) {
            nodeTextContent = nodeTextContent.replace("mm", "");
            columnWidthValue = Double.parseDouble(nodeTextContent);
        } else if (nodeTextContent.endsWith("in")) {
            nodeTextContent = nodeTextContent.replace("in", "");
            columnWidthValue = Double.parseDouble(nodeTextContent) * 25.4;
        } else if (nodeTextContent.endsWith("px")) {
            nodeTextContent = nodeTextContent.replace("px", "");
            columnWidthValue = Double.parseDouble(nodeTextContent) * 0.264583333;
        }
        columnWidthValue = columnWidthValue / pageWidthValue * 100;
        nodeTextContent = Double.toString(columnWidthValue) + "%";
        return nodeTextContent;
    }

    private byte[] fitPage(byte[] data) {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new ByteArrayInputStream(data));
            setLayOut(doc.getElementsByTagName("report").item(0));
            Node masterPage = doc.getElementsByTagName("simple-master-page").item(0);
            String masterPageWidth = getMasterPageWidth(masterPage);
            if (masterPageWidth == null) {
                return data;
            }
            NodeList nodeList = doc.getElementsByTagName("extended-item");
            List<Node> neededNodeList = new ArrayList<>();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                String value = node.getAttributes().getNamedItem("extensionName").getTextContent();
                if (value.equalsIgnoreCase("CrosstabView")) {
                    neededNodeList.addAll(lookForNode(node, "extended-item", "extensionName", "LevelView"));
                }
            }
            setPercentageValues(neededNodeList, masterPageWidth);
            NodeList propertyNodeList = doc.getElementsByTagName("property");
            List<Node> fontSizeNodes = getFontSizeNodes(propertyNodeList);
            setFontSize(fontSizeNodes);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            StreamResult result = new StreamResult(byteArrayOutputStream);
            transformer.transform(source, result);
            data = byteArrayOutputStream.toByteArray();
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            LOGGER.error("Exception thrown", ex);
        } catch (TransformerConfigurationException ex) {
            LOGGER.error("Exception thrown", ex);
        } catch (TransformerException ex) {
            LOGGER.error("Exception thrown", ex);
        }
        return data;
    }

    private List<Node> lookForNode(Node node, String nodeName, String attributeName, String attributeValue) {
        List<Node> neededNodeList = new ArrayList<>();
        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node currentNode = nodeList.item(i);
            if (currentNode.getNodeName().equalsIgnoreCase(nodeName)) {
                Node attribute = currentNode.getAttributes().getNamedItem(attributeName);
                if (attribute != null) {
                    if (attribute.getTextContent().equalsIgnoreCase(attributeValue) && attribute.getTextContent().equalsIgnoreCase("width")) {
                        neededNodeList.add(currentNode);
                    } else if (attribute.getTextContent().equalsIgnoreCase(attributeValue)) {
                        neededNodeList.addAll(lookForNode(currentNode, "property", "name", "width"));
                    } else {
                        neededNodeList.addAll(lookForNode(currentNode, nodeName, attributeName, attributeValue));
                    }
                } else {
                    neededNodeList.addAll(lookForNode(currentNode, nodeName, attributeName, attributeValue));
                }
            } else {
                neededNodeList.addAll(lookForNode(currentNode, nodeName, attributeName, attributeValue));
            }
        }
        return neededNodeList;
    }

    private void setPercentageValues(List<Node> nodeList, String factor) {
        for (Node node : nodeList) {
            node.setTextContent(calculate(factor, node.getTextContent()));
        }
    }

    private String getMasterPageWidth(Node masterPage) {
        NodeList masterPageProperties = masterPage.getChildNodes();
        String width = null;
        boolean flag1 = false;
        boolean flag2 = false;
        for (int i = 0; i < masterPageProperties.getLength(); i++) {
            Node node = masterPageProperties.item(i);
            String nameAttributeValue = null;
            if (node.getNodeName().equalsIgnoreCase("property")) {
                nameAttributeValue = node.getAttributes().getNamedItem("name").getTextContent();
            }
            if (nameAttributeValue != null && nameAttributeValue.equalsIgnoreCase("width")) {
                node.setTextContent("1500mm");
                width = node.getTextContent();
                flag1 = true;
            } else if (nameAttributeValue != null && nameAttributeValue.equalsIgnoreCase("height")) {
                node.setTextContent("1000mm");
                flag2 = true;
            }
            if (flag1 && flag2) {
                break;
            }
        }
        return width;
    }

    private void setLayOut(Node parentNode) {
        NodeList reportProperties = parentNode.getChildNodes();
        Node layOut = null;
        for (int i = 0; i < reportProperties.getLength(); i++) {
            Node currentNode = reportProperties.item(i);
            if (currentNode.getNodeName().equalsIgnoreCase("property")
                    && currentNode.getAttributes().getNamedItem("name")
                    .getTextContent().equalsIgnoreCase("layoutPreference")) {
                layOut = currentNode;
                break;
            }
        }
        if (layOut != null) {
            layOut.setTextContent("auto layout");
        }
    }

    private void setFontSize(List<Node> fontSizeNodes) {
        for (Node node : fontSizeNodes) {
            String currentNodeValue = node.getTextContent();
            String digitsString = "";
            String suffix = "";
            Pattern pattern = Pattern.compile("-?\\d+");
            Matcher matcher = pattern.matcher(currentNodeValue);
            while (matcher.find()) {
                digitsString += matcher.group();
            }
            suffix = currentNodeValue.replace(digitsString, "");
            Double fontSize = Double.parseDouble(digitsString) * 1.5;
            Integer fontSizeIntValue = fontSize.intValue();
            String fontSizeStringValue = fontSizeIntValue.toString() + suffix;
            node.setTextContent(fontSizeStringValue);
        }
    }

    private List<Node> getFontSizeNodes(NodeList propertyNodes) {
        List<Node> fontSizeNodes = new ArrayList<>();
        for (int i = 0; i < propertyNodes.getLength(); i++) {
            Node node = propertyNodes.item(i);
            Node nameAttribute = node.getAttributes().getNamedItem("name");
            if (nameAttribute != null && nameAttribute.getTextContent().equalsIgnoreCase("fontSize")) {
                fontSizeNodes.add(node);
            }
        }
        return fontSizeNodes;
    }
}
