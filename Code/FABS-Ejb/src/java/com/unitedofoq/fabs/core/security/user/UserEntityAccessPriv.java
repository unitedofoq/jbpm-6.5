/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.security.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.eclipse.persistence.annotations.ReadOnly;

import com.unitedofoq.fabs.core.entitysetup.OEntity;

/**
 *
 * @author tarzy
 */
@Entity
@ReadOnly
public class UserEntityAccessPriv implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="oentityDBID">
    @Column(name="OEntity_DBID")
    private long oentityDBID;
    public long getOentityDBID() {
        return oentityDBID;
    }

    public void setOentityDBID(long oentityDBID) {
        this.oentityDBID = oentityDBID;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="oentity">
    @JoinColumn(name="OEntity_DBID", updatable=false, insertable=false)
    @ManyToOne(fetch= FetchType.LAZY)
    private OEntity oentity;

    public OEntity getOentity() {
        return oentity;
    }

    public void setOentity(OEntity oentity) {
        this.oentity = oentity;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="ouserDBID">
    @Id
    @Column(name="OUser_DBID")
    private long ouserDBID ;

    public long getOuserDBID() {
        return ouserDBID;
    }

    public void setOuserDBID(long ouserDBID) {
        this.ouserDBID = ouserDBID;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="granted">
    private boolean granted ;

    public boolean isGranted() {
        return granted;
    }

    public void setGranted(boolean granted) {
        this.granted = granted;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="entityClassPath">
    @Column(name="entityClassPath")
    private String entityClassPath ;

    public String getEntityClassPath() {
        return entityClassPath;
    }

    public void setEntityClassPath(String entityClassPath) {
        this.entityClassPath = entityClassPath;
    }
    // </editor-fold>
}
