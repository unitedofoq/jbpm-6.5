/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.function;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.udc.UDC;

/**
 *
 * @author mibrahim
 */
@Entity
@ParentEntity(fields = {"javaFunction"})
public class ScheduleExpression extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="javaFunction">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private JavaFunction javaFunction;

    public String getJavaFunctionDD() {
        return "ScheduleExpression_javaFunction";
    }

    public JavaFunction getJavaFunction() {
        return javaFunction;
    }

    public void setJavaFunction(JavaFunction javaFunction) {
        this.javaFunction = javaFunction;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="actOnEntity">
    //Entity that dynamic expressions get their values from
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OEntity actOnEntity;

    public String getActOnEntityDD() {
        return "ScheduleExpression_actOnEntity";
    }

    public OEntity getActOnEntity() {
        return actOnEntity;
    }

    public void setActOnEntity(OEntity actOnEntity) {
        this.actOnEntity = actOnEntity;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="recurUnitExpression">
    //Dynamic recurrence expression came from act on entity
    private String recurUnitExpression;

    public String getRecurUnitExpressionDD() {
        return "ScheduleExpression_recurUnitExpression";
    }

    public String getRecurUnitExpression() {
        return recurUnitExpression;
    }

    public void setRecurUnitExpression(String recurUnitExpression) {
        this.recurUnitExpression = recurUnitExpression;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="plannedDateExpression">
    //Expression to start running job on it
    private String plannedStartDateExpression;

    public String getPlannedStartDateExpression() {
        return plannedStartDateExpression;
    }

    public String getPlannedStartDateExpressionDD() {
        return "ScheduleExpression_plannedStartDateExpression";
    }

    public void setPlannedStartDateExpression(String plannedStartDateExpression) {
        this.plannedStartDateExpression = plannedStartDateExpression;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="plannedDateExpression">
    //Expression to end running job on it
    private String plannedEndDateExpression;

    public String getPlannedEndDateExpressionDD() {
        return "ScheduleExpression_plannedEndDateExpression";
    }

    public String getPlannedEndDateExpression() {
        return plannedEndDateExpression;
    }

    public void setPlannedEndDateExpression(String plannedEndDateExpression) {
        this.plannedEndDateExpression = plannedEndDateExpression;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="recurUnit">
    //Static recurrence  date units i.e Daily, Weekly, Monthly ..etc
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC recurUnit;

    public String getRecurUnitDD() {
        return "ScheduleExpression_recurUnit";
    }

    public UDC getRecurUnit() {
        return recurUnit;
    }

    public void setRecurUnit(UDC recurUnit) {
        this.recurUnit = recurUnit;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="plannedDateExpression">
    //Fixed start job time
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date plannedStartDate;

    public String getPlannedStartDateDD() {
        return "ScheduleExpression_plannedStartDate";
    }

    public Date getPlannedStartDate() {
        return plannedStartDate;
    }

    public void setPlannedStartDate(Date plannedStartDate) {
        this.plannedStartDate = plannedStartDate;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="plannedDateExpression">
    //Fixed end job running time
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date plannedEndDate;

    public String getPlannedEndDateDD() {
        return "ScheduleExpression_plannedEndDate";
    }

    public Date getPlannedEndDate() {
        return plannedEndDate;
    }

    public void setPlannedEndDate(Date plannedEndDate) {
        this.plannedEndDate = plannedEndDate;
    }
    // </editor-fold>
}
