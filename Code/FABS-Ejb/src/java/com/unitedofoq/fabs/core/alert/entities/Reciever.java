/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.alert.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;

/**
 *
 * @author root
 */
@Entity
@ParentEntity(fields = {"alert"})
public class Reciever extends BaseEntity {
    @Column(name = "bulkdata")
    private boolean bulk;
    private String email;
    private String emailExpression;
    private String sms;
    private String smsExpression;
    @ManyToOne
    private UDC recieverType;
    @ManyToOne
    private Alert alert;
    

    public String getSmsDD() {
        return "Reciever_sms";
    }

    public String getSms() {
        return sms;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    public String getSmsExpressionDD() {
        return "Reciever_smsExpression";
    }

    public String getSmsExpression() {
        return smsExpression;
    }

    public void setSmsExpression(String smsExpression) {
        this.smsExpression = smsExpression;
    }

    public String getBulkDD() {
        return "Reciever_bulk";
    }

    public boolean isBulk() {
        return bulk;
    }

    public void setBulk(boolean bulk) {
        this.bulk = bulk;
    }

    public String getEmailDD() {
        return "Reciever_email";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailExpressionDD() {
        return "Reciever_emailExpression";
    }

    public String getEmailExpression() {
        return emailExpression;
    }

    public void setEmailExpression(String emailExpression) {
        this.emailExpression = emailExpression;
    }
    
    public String getRecieverTypeDD() {
        return "Reciever_recieverType";
    }

    public UDC getRecieverType() {
        return recieverType;
    }

    public void setRecieverType(UDC recieverType) {
        this.recieverType = recieverType;
    }

    public String getAlertDD() {
        return "Reciever_alert";
    }

    public Alert getAlert() {
        return alert;
    }

    public void setAlert(Alert alert) {
        this.alert = alert;
    }
}
