/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitysetup;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;

/**
 *
 * @author mmohamed
 */
@Entity
@ParentEntity(fields={"entityAction"})
@VersionControlSpecs(omoduleFieldExpression="entityAction.oownerEntity.omodule")
public class EntityActionValidation extends BaseEntity {
    private String fieldExpression;

    public String getFieldExpression() {
        return fieldExpression;
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }

    private String name;
    private boolean stopOnError;

    public boolean isStopOnError() {
        return stopOnError;
    }

    public void setStopOnError(boolean stopOnError) {
        this.stopOnError = stopOnError;
    }
    
    @Translation(originalField="description")
    @Transient
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    private int sortIndex;
    @Translatable(translationField="descriptionTranslated")
    private String description;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    OEntityAction entityAction;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    OFunction function;

    public String getNameDD()           {   return "EntityActionValidation_name";  }
    public String getStopOnErrorDD()    {   return "EntityActionValidation_stopOnError";  }
    public String getSortIndexDD()      {   return "EntityActionValidation_sortIndex";  }
    public String getDescriptionTranslatedDD()    {   return "EntityActionValidation_description";  }
    public String getEntityActionDD()   {   return "EntityActionValidation_entityAction";  }
    public String getFunctionDD()       {   return "EntityActionValidation_function";  }
    public String getFieldExpressionDD()       {   return "EntityActionValidation_fieldExpression";  }
    
    public OEntityAction getEntityAction() {
        return entityAction;
    }

    public void setEntityAction(OEntityAction entityAction) {
        this.entityAction = entityAction;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public OFunction getFunction() {
        return function;
    }

    public void setFunction(OFunction function) {
        this.function = function;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }
}
