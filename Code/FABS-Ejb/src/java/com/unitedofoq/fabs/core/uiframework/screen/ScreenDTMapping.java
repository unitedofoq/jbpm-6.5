/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.screen;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;

/**
 *
 * @author nsaleh
 */
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="MTYPE")
@ChildEntity(fields={"attributesMapping"})
@VersionControlSpecs(omoduleFieldExpression="odataType.omodule")
public abstract class ScreenDTMapping extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="odataType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    protected ODataType odataType;
    public ODataType getOdataType() {
        return odataType;
    }
    public void setOdataType(ODataType odataType) {
        this.odataType = odataType;
    }
    public String getOdataTypeDD() {
        return "ScreenDTMapping_odataType";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="attributesMapping">
    @OneToMany(mappedBy="screenDTMapping")
    protected List<ScreenDTMappingAttr> attributesMapping = new ArrayList<ScreenDTMappingAttr>();
    public List<ScreenDTMappingAttr> getAttributesMapping() {
        return attributesMapping;
    }
    public void setAttributesMapping(List<ScreenDTMappingAttr> attributesMapping) {
        this.attributesMapping = attributesMapping;
    }
    // </editor-fold>
}
