/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.comunication;

import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.central.OCentralUser;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.setup.FABSSetup;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import java.util.List;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import org.slf4j.LoggerFactory;
import javax.mail.Session;

/**
 *
 * @author Rehab
 */
@Singleton
@Startup
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.comunication.EmailConnectionManagerLocal",
        beanInterface = EmailConnectionManagerLocal.class)
public class EmailConnectionManager implements EmailConnectionManagerLocal {

    final static org.slf4j.Logger logger = LoggerFactory.getLogger(EmailNotificationMDB.class);
    private String hostName = "smtp.gmail.com";
    private String authUser = "ofoqfabs@gmail.com";
    private String authPassword = "fabsofoq";
    private String portNumber = "587";
    private String mailSSL = "true";
    private String starttle = "true";
    private String auth = "true";
    private String ntlm = "false";
    private String domain = "";
    private String protocol = "smtp";
    private static String serverHTTPPort="8080";
    private static final String SMTP_HOST_NAME_KEY = "SMTP_HOST_NAME";
    private static final String SMTP_AUTH_USER_KEY = "SMTP_AUTH_USER";
    private static final String SMTP_AUTH_PWD_KEY = "SMTP_AUTH_PWD";
    private static final String SMTP_PORT_NUMBER_KEY = "SMTP_PORT_NUMBER";
    private static final String SMTP_USE_NTLM = "SMTP_USE_NTLM";
    private static final String SMTP_NTLM_DOMAIN = "SMTP_NTLM_DOMAIN";
    private static final String MAIL_SSL_KEY = "MAIL_SSL";
    private static final String MAIL_SMTP_STARTTLE = "MAIL_SMTP_STARTTLE";
    private static final String MAIL_SMTP_AUTH = "MAIL_SMTP_AUTH";
    private static final String MAIL_SENDER_PROTOCOL = "MAIL_SENDER_PROTOCOL";
    private static final String SERVER_HTTP_PORT = "SERVER_HTTP_PORT";
    @EJB
    FABSSetupLocal fABSSetupLocal;
    @EJB
    private OCentralEntityManagerRemote centralOEM;
    @EJB
    OEntityManagerRemote oem;
    private OUser adminUser = null;
    private static Session mailSession = null;
    private static Transport transport = null;

    public static String getServerHTTPPort() {
        return serverHTTPPort;
    }

    @PostConstruct
    public void initiateMailConnection() {
        try {
            getMailConnection();
        } catch (Exception ex) {
            logger.error("Exception ", ex);
        }
    }

    public static Session getMailSession() {
        return mailSession;
    }

    public static Transport getTransport() {
        return transport;
    }

    public Transport Reconnect() throws Exception {
        return getMailConnection();
    }

    private Transport getMailConnection() throws Exception {
        if (adminUser == null) {
            logger.debug("Getting admin user");
            List<OCentralUser> users = centralOEM.getAdminUsers();
            adminUser = oem.getUser(users.get(0).getLoginName(),
                    users.get(0).getTenant());
            logger.debug("Admin user is {}", users.get(0).getLoginName());
        }
        setParameters(adminUser);
        Properties props = setEmailProperties();
        Authenticator auth = new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(authUser, authPassword);
            }
        };
        mailSession = Session.getInstance(props, auth);
        transport = mailSession.getTransport();
        transport.connect();
        return transport;
    }

    private Properties setEmailProperties() {
        logger.trace("Entering");
        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", protocol);
        props.setProperty("mail.smtp.host", hostName);
        props.setProperty("mail.smtp.auth", auth);
        props.setProperty("mail.smtp.starttls.enable", starttle);
        props.put("mail.smtp.port", portNumber);
        //props.put("mail.smtp.socketFactory.port", soketFactoryPort);
        if (isTrue(mailSSL)) {
            props.put("mail.smtp.socketFactory.class",
                    "javax.net.ssl.SSLSocketFactory");
        }
        if (isTrue(ntlm)) {
            props.put("mail.smtp.auth.mechanisms", "NTLM");
            props.put("mail.smtp.auth.ntlm.domain", domain);
        }
        logger.debug(props.toString());
        logger.trace("Returning");
        return props;
    }

    private void setParameters(OUser loggedUser) {
        logger.trace("Entering");
        if (loggedUser != null) {
            hostName = fABSSetupLocal.loadKeySetup(SMTP_HOST_NAME_KEY, loggedUser).getSvalue();
            authUser = fABSSetupLocal.loadKeySetup(SMTP_AUTH_USER_KEY, loggedUser).getSvalue();
            authPassword = fABSSetupLocal.loadKeySetup(SMTP_AUTH_PWD_KEY, loggedUser).getSvalue();
            portNumber = fABSSetupLocal.loadKeySetup(SMTP_PORT_NUMBER_KEY, loggedUser).getSvalue();
            mailSSL = fABSSetupLocal.loadKeySetup(MAIL_SSL_KEY, loggedUser).getSvalue();
            starttle = fABSSetupLocal.loadKeySetup(MAIL_SMTP_STARTTLE, loggedUser).getSvalue();
            auth = fABSSetupLocal.loadKeySetup(MAIL_SMTP_AUTH, loggedUser).getSvalue();

            FABSSetup ntlmSetup = fABSSetupLocal.loadKeySetup(SMTP_USE_NTLM, loggedUser);
            if (ntlmSetup != null) {
                ntlm = ntlmSetup.getSvalue();
            }

            FABSSetup domainSetup = fABSSetupLocal.loadKeySetup(SMTP_NTLM_DOMAIN, loggedUser);
            if (domainSetup != null) {
                domain = domainSetup.getSvalue();
            }

            FABSSetup protocolSetup = fABSSetupLocal.loadKeySetup(MAIL_SENDER_PROTOCOL, loggedUser);
            if (protocolSetup != null) {
                protocol = protocolSetup.getSvalue();
            }

            FABSSetup serverPort = fABSSetupLocal.loadKeySetup(SERVER_HTTP_PORT, loggedUser);
            if (serverPort != null) {
                serverHTTPPort = serverPort.getSvalue();
            }
        }
        logger.trace("Returning");
    }

    private boolean isTrue(String value) {
        return value.toLowerCase().equals("true") || value.toLowerCase().equals("y") || value.toLowerCase().equals("yes");
    }

//    @PreDestroy
//    public void closeMailConnection() {
//        try {
//            transport.isConnected();
//            transport.close();
//        } catch (MessagingException ex) {
//            logger.error("Exception ", ex);
//        }
//    }

}
