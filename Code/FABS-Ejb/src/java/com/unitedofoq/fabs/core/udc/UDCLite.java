/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.udc;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author mibrahim
 */
@Entity
@ReadOnly
@NamedQueries(value={
    @NamedQuery(name="getUDC",
        query = " SELECT    udcLite " +
                " FROM      UDCLite udcLite" +
                " WHERE     udcLite.dbid = :dbid" +
                " AND       udcLite.langid=:langid"),
   @NamedQuery(name="getUDCs",
        query = " SELECT    udcLite " +
                " FROM      UDCLite udcLite" +
                " WHERE     udcLite.dbid = :dbid" +
                " AND       udcLite.langid=:langid"+
                " AND       udcLite.parentdbid=:parentdbid")})
public class UDCLite implements Serializable {
    @Id
    private long dbid;
    private long parentdbid;
    private long langid;
    private String code;
    private String udcValue;

    public long getDbid() {
        return dbid;
    }

    public void setDbid(long dbid) {
        this.dbid = dbid;
    }

    public long getParentdbid() {
        return parentdbid;
    }

    public void setParentdbid(long parentdbid) {
        this.parentdbid = parentdbid;
    }

    public long getLangid() {
        return langid;
    }

    public void setLangid(long langid) {
        this.langid = langid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUdcValue() {
        return udcValue;
    }

    public void setUdcValue(String udcValue) {
        this.udcValue = udcValue;
    }    
    
}
