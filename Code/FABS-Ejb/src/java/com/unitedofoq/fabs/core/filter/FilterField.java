/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.filter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;

/**
 *
 * @author mmohamed
 */
@Entity
@ParentEntity(fields={"filter"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class FilterField extends BaseEntity {
    @Column(nullable=false)
    String fieldExpression;

    @Transient
    public static DateFormat dateFormat =new SimpleDateFormat("yyyy-MM-dd");

    public OFilter getFilter() {
        return filter;
    }
    
    public String getFilterDD() {
        return "FilterField_filter";
    }

    public void setFilter(OFilter filter) {
        this.filter = filter;
    }
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    DD fieldDD;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    OFilter filter;

    String fieldValue;

    public String getFieldValueTo() {
        return fieldValueTo;
    }

    public void setFieldValueTo(String fieldValueTo) {
        this.fieldValueTo = fieldValueTo;
    }
    
    public String getFieldValueToDD() {
        return "FilterField_fieldValueTo";
    }
    
    String fieldValueTo;

    boolean fromTo;

    int fieldIndex;

    public DD getFieldDD() {
        return fieldDD;
    }
    
    public String getFieldDDDD() {
        return "FilterField_fieldDD";
    }

    public void setFieldDD(DD fieldDD) {
        this.fieldDD = fieldDD;
    }

    public String getFieldExpression() {
        return fieldExpression;
    }

    public String getFieldExpressionDD() {
        return "FilterField_fieldExpression";
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }

    public int getFieldIndex() {
        return fieldIndex;
    }
    
    public String getFieldIndexDD() {
        return "FilterField_fieldIndex";
    }

    public void setFieldIndex(int fieldIndex) {
        this.fieldIndex = fieldIndex;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public String getFieldValueDD() {
        return "FilterField_fieldValue";
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public boolean isFromTo() {
        return fromTo;
    }

    public String getFromToDD() {
        return "FilterField_fromTo";
    }

    public void setFromTo(boolean fromTo) {
        this.fromTo = fromTo;
    }

    //<editor-fold defaultstate="collapsed" desc="operator">
    @Column
    private String operator;
    
    public String getOperator() {
        return operator;
    }

    public String getOperatorDD() {
        return "FilterField_operator";
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
    
    //</editor-fold>
    
}
