/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.report;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.filter.OFilter;

/**
 *
 * @author mmohamed
 */
@Entity
@DiscriminatorValue(value = "REPORT")
@ParentEntity(fields = {"report"})
@ChildEntity(fields = {"filterFields"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class ReportFilter extends OFilter {

    @OneToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    OReport report;

    public OReport getReport() {
        return report;
    }

    public void setReport(OReport report) {
        this.report = report;
    }

    public String getReportDD() {
        return "ReportFilter_report";
    }
}
