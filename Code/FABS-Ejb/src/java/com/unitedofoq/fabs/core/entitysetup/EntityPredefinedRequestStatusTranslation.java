package com.unitedofoq.fabs.core.entitysetup;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
@Table(name = "EntityPredefinedRequestStatusi18n")
public class EntityPredefinedRequestStatusTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    // </editor-fold>
}
