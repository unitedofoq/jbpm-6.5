/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.function;

/**
 *
 * @author MEA
 */
public class OMenuFunctionDTO {

    private long dbid;
    private long oFunctionDBID;
    private long omenuDBID;
    private int sortIndex;
    private String oFunctionName;
    private boolean isOFunctionServerJobFunction;
    private String oFunctionCode;
    private String oFunctionDescription;
    private byte[] oFunctionImageData;
    private String oFunctionImageType;
    private String oFunctionImageName;
    private long oFunctionImageDBID;
    private boolean isOFunctionReserved;

    public OMenuFunctionDTO() {
    }

    public OMenuFunctionDTO(long dbid,
            long oFunctionDBID,
            long omenuDBID,
            int sortIndex,
            String oFunctionName,
            boolean isOFunctionServerJobFunction,
            String oFunctionCode,
            String oFunctionDescription,
            byte[] oFunctionImageData,
            String oFunctionImageType,
            String oFunctionImageName,
            long oFunctionImageDBID,
            boolean isOFunctionReserved) {
        this.dbid = dbid;
        this.oFunctionDBID = oFunctionDBID;
        this.omenuDBID = omenuDBID;
        this.sortIndex = sortIndex;
        this.oFunctionName = oFunctionName;
        this.isOFunctionServerJobFunction = isOFunctionServerJobFunction;
        this.oFunctionCode = oFunctionCode;
        this.oFunctionDescription = oFunctionDescription;
        this.oFunctionImageData = oFunctionImageData;
        this.oFunctionImageType = oFunctionImageType;
        this.oFunctionImageName = oFunctionImageName;
        this.oFunctionImageDBID = oFunctionImageDBID;
        this.isOFunctionReserved = isOFunctionReserved;
    }

    public long getDbid() {
        return dbid;
    }

    public void setDbid(long dbid) {
        this.dbid = dbid;
    }

    public long getoFunctionDBID() {
        return oFunctionDBID;
    }

    public void setoFunctionDBID(long oFunctionDBID) {
        this.oFunctionDBID = oFunctionDBID;
    }

    public long getOmenuDBID() {
        return omenuDBID;
    }

    public void setOmenuDBID(long omenuDBID) {
        this.omenuDBID = omenuDBID;
    }

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getoFunctionName() {
        return oFunctionName;
    }

    public void setoFunctionName(String oFunctionName) {
        this.oFunctionName = oFunctionName;
    }

    public boolean isIsOFunctionServerJobFunction() {
        return isOFunctionServerJobFunction;
    }

    public void setIsOFunctionServerJobFunction(boolean isOFunctionServerJobFunction) {
        this.isOFunctionServerJobFunction = isOFunctionServerJobFunction;
    }

    public String getoFunctionCode() {
        return oFunctionCode;
    }

    public void setoFunctionCode(String oFunctionCode) {
        this.oFunctionCode = oFunctionCode;
    }

    public String getoFunctionDescription() {
        return oFunctionDescription;
    }

    public void setoFunctionDescription(String oFunctionDescription) {
        this.oFunctionDescription = oFunctionDescription;
    }

    public byte[] getoFunctionImageData() {
        return oFunctionImageData;
    }

    public void setoFunctionImageData(byte[] oFunctionImageData) {
        this.oFunctionImageData = oFunctionImageData;
    }

    public String getoFunctionImageType() {
        return oFunctionImageType;
    }

    public void setoFunctionImageType(String oFunctionImageType) {
        this.oFunctionImageType = oFunctionImageType;
    }

    public String getoFunctionImageName() {
        return oFunctionImageName;
    }

    public void setoFunctionImageName(String oFunctionImageName) {
        this.oFunctionImageName = oFunctionImageName;
    }

    public boolean isIsOFunctionReserved() {
        return isOFunctionReserved;
    }

    public void setIsOFunctionReserved(boolean isOFunctionReserved) {
        this.isOFunctionReserved = isOFunctionReserved;
    }

    public long getoFunctionImageDBID() {
        return oFunctionImageDBID;
    }

    public void setoFunctionImageDBID(long oFunctionImageDBID) {
        this.oFunctionImageDBID = oFunctionImageDBID;
    }
}