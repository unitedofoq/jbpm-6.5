package com.unitedofoq.fabs.core.report;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name="FTYPE", discriminatorType=DiscriminatorType.STRING)
@DiscriminatorValue(value="MASTER")
@ParentEntity(fields={"oreport"})
@FABSEntitySpecs(indicatorFieldExp="fieldExpression")
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class OReportField extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="oreport">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OReport oreport;

    public OReport getOreport() {
        return oreport;
    }

    public String getOreportDD() {
        return "OReportField_oreport";
    }

    public void setOreport(OReport oreport) {
        this.oreport = oreport;

//        if (oreport instanceof CrossTabReport && this instanceof CrossTabField)
//            ((CrossTabReport) this.oreport).setCrossTabField((CrossTabField) this);
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="ddTitleOverride">
    @Translatable(translationField="ddTitleOverrideTranslated")
    private String ddTitleOverride;
    
    @Translation(originalField="ddTitleOverride")
    @Transient
    private String ddTitleOverrideTranslated;

    public String getDdTitleOverrideTranslated() {
        return ddTitleOverrideTranslated;
    }

    public void setDdTitleOverrideTranslated(String ddTitleOverrideTranslated) {
        this.ddTitleOverrideTranslated = ddTitleOverrideTranslated;
    }
    

    public String getDdTitleOverride() {
        return ddTitleOverride;
    }

    public String getDdTitleOverrideTranslatedDD() {
        return "OReportField_ddTitleOverride";
    }

    public void setDdTitleOverride(String ddTitleOverride) {
        this.ddTitleOverride = ddTitleOverride;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="overallHeader">
    private String overallHeader;

    public String getOverallHeaderDD() {
        return "OReportField_overallHeader";
    }

    public String getOverallHeader() {
        return overallHeader;
    }

    public void setOverallHeader(String overallHeader) {
        this.overallHeader = overallHeader;
    }

    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="fieldExpression">
    @Column(nullable=false)
    private String fieldExpression;

    public String getFieldExpression() {
        return fieldExpression;
    }

    public String getFieldExpressionDD() {
        return "OReportField_fieldExpression";
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="fieldDisplayExpression">
    private String fieldDisplayExpression;

    public String getFieldDisplayExpression() {
        return fieldDisplayExpression;
    }

    public String getFieldDisplayExpressionDD() {
        return "OReportField_fieldDisplayExpression";
    }

    public void setFieldDisplayExpression(String fieldDisplayExpression) {
        this.fieldDisplayExpression = fieldDisplayExpression;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="sortIndex">
    @Column(nullable=false)
    private Integer sortIndex;

    public Integer getSortIndex() {
        return sortIndex;
    }

    public String getSortIndexDD() {
        return "OReportField_sortIndex";
    }

    public void setSortIndex(Integer sortIndex) {
        this.sortIndex = sortIndex;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="width">
    private Integer width;

    public Integer getWidth() {
        return width;
    }

    public String getWidthDD() {
        return "OReportField_width";
    }

    public void setWidth(Integer width) {
        this.width = width;
    }
    //</editor-fold>

}