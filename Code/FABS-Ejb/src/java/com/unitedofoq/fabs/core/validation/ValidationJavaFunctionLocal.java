/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.validation;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author nkhalil
 */
@Local
public interface ValidationJavaFunctionLocal {
    public OFunctionResult validatePositivePercent(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validatePercentLTZero(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validatePercent(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateDecimal(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateDecimalLTZero(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validatePositiveDecimal(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateMonthNumber(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateWeekDayNumber(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateHoursPerDay(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateDaysPerMonth(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateNumber(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validatePositiveNumber(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateNumberLTZero(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateAlphaNumericName(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult fieldRequired(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult fieldUniqueOptional(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult fieldRequiredForNonBlankField(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult fieldRequiredForFieldValue(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateAlphanumericCommaSeparated(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateFieldRequiredForFieldValue(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateYearsNumber(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateFieldHasNoSpaces(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateNumberLessThan10(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateEmail(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateName(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateScreenFilterNumber(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateDecimalMaskFieldDD(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validatePassword(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateCalendarDD(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateSpecialChar(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateHelpLength(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateScreenGroup(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateUniqueUdc(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
}
