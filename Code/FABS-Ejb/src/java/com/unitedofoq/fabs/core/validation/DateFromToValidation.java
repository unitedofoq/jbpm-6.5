package com.unitedofoq.fabs.core.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface DateFromToValidation {

    //  IMPORTANT NOTE:
    //  ---------------
    //  Any "from" field name in the 'from' String array should have a
    //  corresponding "to" field name in the 'from' String array at the
    //  same exact index.

    /**
     * the field that represents the "FROM" date field
     */
    public String[] from();

    /**
     * the field that represents the "TO" date field
     */
    public String[] to() ;
}