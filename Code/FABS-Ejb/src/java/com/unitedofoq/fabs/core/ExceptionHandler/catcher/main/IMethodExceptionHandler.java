/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.ExceptionHandler.catcher.main;

import java.lang.reflect.Method;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author mostafa
 */
@Local
public interface IMethodExceptionHandler {
    public void handle(Class classOfException,Method methodOfException, String exceptionMsg,
            String msgCode, OUser loggedUser);
}
