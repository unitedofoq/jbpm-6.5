/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.entitybase;

import java.lang.reflect.Field;

/**
 *
 * @author Bassem
 * Mainly used by BaseEntity.parseFieldExpression()
 */
public class ExpressionFieldInfo {
    /**
     * Main expression parsed to get this field
     */
    public String fieldParsedExpression ;
    /**
     * field name out of the parsed expression
     */
    public String fieldName ;
    /**
     * Field Object
     */
    public Field field ;
    /**
     * Field Class used in the expression
     * If field is defined in ancestor class then this value will be different
     * than field.getDeclaringClass
     */
    public Class fieldClass ;


    /**
     * Original Field, If Field is Translatable
     */
    public Field originalField ;

    @Override
    public String toString() {
        return      "Expression: " + fieldParsedExpression + ", "
                +   "Field Name: " + fieldName + ", "
                +   "Field: " + field + ", "
                +   "Field Class: " + fieldClass.getName();
    }
}
