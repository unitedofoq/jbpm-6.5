/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.comunication;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author aelzaher
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.function.SMSSenderLocal",
        beanInterface = SMSSenderLocal.class)
public class SMSSender implements SMSSenderLocal {

    @EJB
    OEntityManagerRemote oem;
    @EJB
    FABSSetupLocal fabsSetup;
    @EJB
    UserMessageServiceRemote userMessageServiceRemote;
    @EJB
    DataTypeServiceRemote dataTypeService;
    @EJB
    private EMailSenderLocal eMailSender;

    final static Logger logger = LoggerFactory.getLogger(SMSSender.class);
    
    @Override
    public boolean sendSMS(String message, String recipient, String reciverEMail, String sender, OEntity actOnEntity, long entityDBDI, OUser loggedUser) {
        logger.info("Entering: sendSMS, First");
        //<editor-fold defaultstate="collapsed" desc="log fired Alerts">
        eMailSender.persistFiredAlert("SMS-Alert", message, message, reciverEMail, actOnEntity, entityDBDI, loggedUser);
        //</editor-fold>

        boolean sent = false;
        String requestUrl = "";
        String SMS_ENGINE_DEFAULT_SENDER = "SYSTEM";
        String SMS_ENGINE_IP = "tekegy.org";
        String SMS_ENGINE_PORT = "";
        String SMS_ENGINE_APP = "";
        String SMS_ENGINE_USER_NAME = "ofoq";
        String SMS_ENGINE_PASSWORD = "ofoq";
        HttpURLConnection connection = null;
        try {
            requestUrl = "http://" + SMS_ENGINE_IP + (SMS_ENGINE_PORT.isEmpty() ? "" : ":") + SMS_ENGINE_PORT
                    + "/" + SMS_ENGINE_APP + "?" + "Username=" + URLEncoder.encode(SMS_ENGINE_USER_NAME, "UTF-8")
                    + "&Password=" + URLEncoder.encode(SMS_ENGINE_PASSWORD, "UTF-8")
                    + "&Sender=" + URLEncoder.encode(sender == null || sender.isEmpty()
                    ? SMS_ENGINE_DEFAULT_SENDER : sender, "UTF-8")
                    + "&MessageData=" + URLEncoder.encode(message, "Cp1256")
                    + "&Recipients=" + URLEncoder.encode(recipient, "UTF-8");
            URL url = new URL(requestUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Length", String.valueOf(requestUrl.length()
                    - requestUrl.indexOf("?") - 1));
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Accept", "*/*");
            connection.setRequestMethod("POST");
            PrintWriter pw = new PrintWriter(new OutputStreamWriter(connection.getOutputStream()), true);
            pw.print(requestUrl.substring(requestUrl.indexOf("?") + 1));
            pw.flush();
            pw.close();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder msgResponse = new StringBuilder(10000);
            while (br.ready()) {
                msgResponse.append(br.readLine()).append("\n");
            }
            if (msgResponse.indexOf("OK;") != -1) {
                logger.info("Message Sent: {}", message);
                sent = true;
            } else {
                String response = msgResponse.toString();
                if(response!=null){
                logger.warn("Message Sending Failed: {}", response.substring(
                        response.indexOf(">") + 1, response.lastIndexOf("<")));
                }
                else
                    logger.warn("response equlas Null");
            }
            int httpResponseCode = connection.getResponseCode();
            if (httpResponseCode != 200 || httpResponseCode != 302) {
                logger.info("Response Code =  {}" , httpResponseCode );
            }
            br.close();
        } catch (Exception ex) {
            logger.info("requestUrl: ",requestUrl);
            logger.error("Exception thrown", ex);
        } 
        finally {

            try {
                connection.disconnect();
            } catch (Exception ex) {
                logger.info(ex.getMessage());
            }
        }
        logger.debug("Returning from: sendSMS");
        return sent;
    }

}
