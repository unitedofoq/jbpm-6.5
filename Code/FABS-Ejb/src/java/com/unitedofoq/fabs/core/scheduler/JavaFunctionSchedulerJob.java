/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.scheduler;

import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.ServerJob;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.uiframework.serverjob.resources.ServerJobDTO;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.Calendar;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Hammad
 */
public class JavaFunctionSchedulerJob implements Job {
final static org.slf4j.Logger logger = LoggerFactory.getLogger(JavaFunctionSchedulerJob.class);
    public void updateJobStatus(ServerJob serverJob, boolean isStarted, UDC startedUDC,
            UDC completedUDC, OUser loggedUser) {

        serverJob.setLoggedUser(loggedUser);
        Calendar calendar = Calendar.getInstance();
        Timestamp currentTimestamp = new Timestamp(calendar.getTime().getTime());

        if (isStarted) {
            serverJob.setStartTime(currentTimestamp);
            if (startedUDC != null) {
                serverJob.setStatus(startedUDC);
            }
        } else {
            serverJob.setFinishTime(currentTimestamp);
            if (completedUDC != null) {
                serverJob.setStatus(completedUDC);
            }
        }
    }

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        logger.debug("Entering");
        if(null!=jec &&  jec.getJobDetail() != null && jec.getJobDetail().getKey() != null)
        logger.trace("Time now to execute job: {}", jec.getJobDetail().getKey().getName());

        JobDataMap jobDataMap = jec.getJobDetail().getJobDataMap();
        OUser loggedUser = (OUser) jobDataMap.get("oUser");
        EntitySetupServiceRemote entitySetupService = (EntitySetupServiceRemote) jobDataMap.get("entitySetupService");
        Method functionMethod = (Method) jobDataMap.get("functionMethod");
        Object javaFunctionClsObj = jobDataMap.get("javaFunctionClassObject");
        Object[] args = (Object[]) jobDataMap.get("args");
        UDC startedUDC = (UDC) jobDataMap.get("startedUDC");
        UDC completedUDC = (UDC) jobDataMap.get("completedUDC");

        ServerJobDTO serverJobDTO = (ServerJobDTO) jobDataMap.get("serverJobDTO");
        long notificationTypeDBID = serverJobDTO.getNotificationTypeDBID();
        long moduleDBID = serverJobDTO.getoModuleDBID();

        ServerJob serverJob = new ServerJob();

        serverJob.setFunctionName(serverJobDTO.getFunctionName());
        serverJob.setCode(serverJobDTO.getCode());

        updateJobStatus(serverJob, true, startedUDC, completedUDC, loggedUser);

        entitySetupService.runServerJob(
                moduleDBID, notificationTypeDBID, serverJob,
                functionMethod, javaFunctionClsObj, loggedUser,
                args);

        updateJobStatus(serverJob, false, startedUDC, completedUDC, loggedUser);
    }
}
