package com.unitedofoq.fabs.core.dd;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class DDReport implements Serializable {
    
    @Column
    private String label;
    @Column
    @Id
    private String name;
    @Column
    private Long langDbid;
  
    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the langDbid
     */
    public Long getLangDbid() {
        return langDbid;
    }

    /**
     * @param langDbid the langDbid to set
     */
    public void setLangDbid(Long langDbid) {
        this.langDbid = langDbid;
    }
}
