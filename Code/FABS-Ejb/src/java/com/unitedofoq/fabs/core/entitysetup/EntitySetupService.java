/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitysetup;

import com.unitedofoq.fabs.central.EntityManagerWrapperLocal;
import com.unitedofoq.fabs.core.comunication.EMailSenderLocal;
import com.unitedofoq.fabs.core.comunication.SMSSenderLocal;
import com.unitedofoq.fabs.core.comunication.ServerJobInbox;
import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.EntityBaseServiceRemote;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.function.ServerJob;
import com.unitedofoq.fabs.core.function.ServerJobMessage;
import com.unitedofoq.fabs.core.function.ValidationJavaFunction;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.process.ProcessFunction;
import com.unitedofoq.fabs.core.security.SecurityServiceRemote;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.fabs.core.validation.FABSException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.NoResultException;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author melsayed
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote",
        beanInterface = EntitySetupServiceRemote.class)
public class EntitySetupService implements EntitySetupServiceRemote {

    @EJB
    private OEntityManagerRemote oem;
    @EJB
    OFunctionServiceRemote oFunctionService;
    @EJB
    SecurityServiceRemote securityService;
    @EJB
    DDServiceRemote ddService;
    @EJB
    EntityServiceLocal entityService;
    @EJB
    EMailSenderLocal emailSenderLocal;
    @EJB
    SMSSenderLocal smsSenderLocal;
    @EJB
    UserMessageServiceRemote usrMsgService;
    @EJB
    private DataTypeServiceRemote dtService;
    @EJB
    private OFunctionServiceRemote functionService;
    @EJB
    private EntityBaseServiceRemote entityBaseService;
    public static Hashtable<String, OEntity> cachedOEntity = new Hashtable<String, OEntity>();
    public static Hashtable<String, OEntityDTO> cachedOEntityDTO = new Hashtable<String, OEntityDTO>();
    final static Logger logger = LoggerFactory.getLogger(EntitySetupService.class);

    /**
     * Loads the OEntity Object, including all its direct children fields
     *
     * @param entityClassPath Entity full qualified class path
     * @param loggedUser
     * @return OEntity Object
     * <br> null: OEntity not found, or hidden for authority
     * @throws com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException
     * User not authorized on OEntity access or Retrieve action
     */
    @Override
    public OEntity loadOEntity(String entityClassPath, OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.debug("Entering");
        String entityClassName = entityClassPath.substring(
                entityClassPath.lastIndexOf(".") + 1,
                entityClassPath.length());
        logger.debug("Returning");
        return loadOEntityByClassName(entityClassName, loggedUser);
    }

    @Override
    public OEntityDTO loadOEntityDTO(String entityClassPath, OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.debug("Entering");
        String entityClassName = entityClassPath.substring(
                entityClassPath.lastIndexOf(".") + 1,
                entityClassPath.length());
        logger.debug("Returning");
        return loadOEntityDTOByClassName(entityClassName, loggedUser);
    }

    @Override
    public OEntityDTO loadOEntityDTOForTree(String entityClassPath, OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.debug("Entering");
        String entityClassName = entityClassPath.substring(
                entityClassPath.lastIndexOf(".") + 1,
                entityClassPath.length());
        OEntityDTO entityDTO = clearCashAndLoadEntityDTO(entityClassName, loggedUser);
        getCCExpressions(entityDTO, true, loggedUser);
        logger.debug("Returning");
        return entityDTO;
    }

    private OEntityDTO clearCashAndLoadEntityDTO(String entityClassName, OUser loggedUser) {
        cachedOEntityDTO.clear();
        OEntityDTO entityDTO = null;
        StringBuilder sbQuery = new StringBuilder(50);
        String query = sbQuery.append("SELECT NEW com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO(")
                .append(" e.dbid, e.entityClassPath, e.requestable, e.title)")
                .append(" FROM OEntity e").append(" WHERE e.entityClassPath LIKE '%.").append(entityClassName).append("'")
                .append(" AND e.inActive = false")
                .toString();

        try {
            entityDTO = (OEntityDTO) oem.executeEntityQuery(query, loggedUser);
            if (!loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).
                    getSvalue()) && null != entityDTO) {
                String title = (String) oem.executeEntityNativeQuery(new StringBuilder().append("select title from")
                        .append(" oentitytranslation trans where trans.entityDBID = ")
                        .append(entityDTO.getDbid())
                        .append(" and trans.language_dbid = ")
                        .append(loggedUser.getFirstLanguage().getDbid()).toString(), loggedUser);
                entityDTO.setTitle(title);
            }
            if (entityDTO != null) {
                cachedOEntityDTO.put(loggedUser.getTenant().getId() + "_"
                        + entityClassName, entityDTO);
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.trace("OEnity Not Found In Cache & Loaded. EntityClassName: {}", entityClassName);
                // </editor-fold>
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Query: {}", query);
        }
        return entityDTO;
    }

    @Override
    public OEntityDTO getOScreenEntityDTO(long screenDBID, OUser loggedUser) {
        logger.debug("Entering");
        long dbid;
        String query = null;
        OEntityDTO entityDTO = null;
        try {
            dbid = (Long) oem.executeEntityQuery("SELECT o.oactOnEntity.dbid"
                    + " FROM SingleEntityScreen o"
                    + " WHERE o.dbid = " + screenDBID, loggedUser);
            query = "SELECT NEW com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO("
                    + " e.dbid, e.entityClassPath)"
                    + " FROM OEntity e"
                    + " WHERE e.dbid =" + dbid;
            entityDTO = (OEntityDTO) oem.executeEntityQuery(query, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Query: {}", query);
        }
        if (null != entityDTO) {
            logger.debug("Returning with: ", entityDTO.getEntityClassName());
        }
        return entityDTO;
    }

    @Override
    public OEntityDTO getOReportEntityDTO(long reportDBID, OUser loggedUser) {
        logger.debug("Entering");
        long dbid;
        String query = null;
        OEntityDTO entityDTO = null;
        try {
            dbid = (Long) oem.executeEntityQuery("SELECT o.oactOnEntity.dbid"
                    + " FROM OReport o"
                    + " WHERE o.dbid = " + reportDBID, loggedUser);
            query = "SELECT NEW com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO("
                    + " e.dbid, e.entityClassPath)"
                    + " FROM OEntity e"
                    + " WHERE e.dbid =" + dbid;
            entityDTO = (OEntityDTO) oem.executeEntityQuery(query, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Query: {}", query);
        }
        if (null != entityDTO) {
            logger.debug("Returning with: ", entityDTO.getEntityClassName());
        }
        return entityDTO;
    }

    @Override
    public OEntityDTO getWebServiceEntityDTO(long websrvDBID, OUser loggedUser) {
        logger.debug("Entering");
        long dbid;
        String query = null;
        OEntityDTO entityDTO = null;
        try {
            dbid = (Long) oem.executeEntityQuery("SELECT o.oactOnEntity.dbid"
                    + " FROM OWebService o"
                    + " WHERE o.dbid = " + websrvDBID, loggedUser);
            query = "SELECT NEW com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO("
                    + " e.dbid, e.entityClassPath)"
                    + " FROM OEntity e"
                    + " WHERE e.dbid =" + dbid;
            entityDTO = (OEntityDTO) oem.executeEntityQuery(query, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Query: {}", query);
        }
        if (null != entityDTO) {
            logger.debug("Returning with: ", entityDTO.getEntityClassName());
        }
        return entityDTO;
    }

    @Override
    public OEntityDTO getRoutingEntityDTO(long routingDBID, OUser loggedUser) {
        logger.debug("Entering");
        long dbid;
        String query = null;
        OEntityDTO entityDTO = null;
        try {
            dbid = (Long) oem.executeEntityQuery("SELECT o.routingOEntity.dbid"
                    + " FROM ORoutingTaskInstance o"
                    + " WHERE o.dbid = " + routingDBID, loggedUser);
            query = "SELECT NEW com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO("
                    + " e.dbid, e.entityClassPath, e.title)"
                    + " FROM OEntity e"
                    + " WHERE e.dbid =" + dbid;
            entityDTO = (OEntityDTO) oem.executeEntityQuery(query, loggedUser);
            if (!loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).
                    getSvalue()) && null != entityDTO) {
                String title = (String) oem.executeEntityNativeQuery("select title from"
                        + " oentitytranslation trans where trans.entityDBID = "
                        + dbid
                        + " and trans.language_dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                entityDTO.setTitle(title);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Query: {}", query);
        }
        if (null != entityDTO) {
            logger.debug("Returning with: ", entityDTO.getEntityClassName());
        }
        return entityDTO;
    }

    @Override
    public OEntityDTO getTreeLevelEntityDTO(long treeLevelDBID, OUser loggedUser) {
        logger.debug("Entering");
        long dbid;
        String query = null;
        OEntityDTO entityDTO = null;
        try {
            dbid = (Long) oem.executeEntityQuery("SELECT o.actOnEntity.dbid"
                    + " FROM MultiLevelScreenLevel o"
                    + " WHERE o.dbid = " + treeLevelDBID, loggedUser);
            query = "SELECT NEW com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO("
                    + " e.dbid, e.entityClassPath, e.title)"
                    + " FROM OEntity e"
                    + " WHERE e.dbid =" + dbid;
            entityDTO = (OEntityDTO) oem.executeEntityQuery(query, loggedUser);
            if (!loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).
                    getSvalue()) && null != entityDTO) {
                String title = (String) oem.executeEntityNativeQuery("select title from"
                        + " oentitytranslation trans where trans.entityDBID = "
                        + dbid
                        + " and trans.language_dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                entityDTO.setTitle(title);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Query: {}", query);
        }
        if (null != entityDTO) {
            logger.debug("Returning with: ", entityDTO.getEntityClassName());
        }
        return entityDTO;
    }

    /**
     *
     * @param routingDBID
     * @param loggedUser
     * @return
     */
    @Override
    public OEntityDTO getRoutingParentEntityDTO(long routingDBID, OUser loggedUser) {
        logger.debug("Entering");
        long dbid;
        String query = null;
        OEntityDTO entityDTO = null;
        try {
            dbid = (Long) oem.executeEntityQuery("SELECT o.parentOactOnEntity.dbid"
                    + " FROM MORoutingTaskInstance o"
                    + " WHERE o.dbid = " + routingDBID, loggedUser);
            query = "SELECT NEW com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO("
                    + " e.dbid, e.entityClassPath, e.title)"
                    + " FROM OEntity e"
                    + " WHERE e.dbid =" + dbid;
            entityDTO = (OEntityDTO) oem.executeEntityQuery(query, loggedUser);
            if (!loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).
                    getSvalue()) && null != entityDTO) {
                String title = (String) oem.executeEntityNativeQuery("select title from"
                        + " oentitytranslation trans where trans.entityDBID = "
                        + dbid
                        + " and trans.language_dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                entityDTO.setTitle(title);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Query:{}", query);
        }
        if (null != entityDTO) {
            logger.debug("Returning with: {}", entityDTO.getEntityClassName());
        }
        return entityDTO;
    }

    @Override
    public OEntityDTO getScheduleExpressionEntityDTO(long schexpDBID, OUser loggedUser) {
        logger.debug("Entering");
        long dbid;
        String query = null;
        OEntityDTO entityDTO = null;
        try {
            dbid = (Long) oem.executeEntityQuery("SELECT o.actOnEntity.dbid"
                    + " FROM ScheduleExpression o"
                    + " WHERE o.dbid = " + schexpDBID, loggedUser);
            query = "SELECT NEW com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO("
                    + " e.dbid, e.entityClassPath, e.title)"
                    + " FROM OEntity e"
                    + " WHERE e.dbid =" + dbid;
            entityDTO = (OEntityDTO) oem.executeEntityQuery(query, loggedUser);
            if (!loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).
                    getSvalue()) && null != entityDTO) {
                String title = (String) oem.executeEntityNativeQuery("select title from"
                        + " oentitytranslation trans where trans.entityDBID = "
                        + dbid
                        + " and trans.language_dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                entityDTO.setTitle(title);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Query: {}", query);
        }
        if (null != entityDTO) {
            logger.debug("Returning with: {}", entityDTO.getEntityClassName());
        }
        return entityDTO;
    }

    @Override
    public OEntityAction getOEntityDTOAction(long oentityDBID, long actionType, OUser loggedUser) {
        logger.debug("Entering");
        OEntityAction entityAction = null;
        try {
            entityAction = (OEntityAction) oem.executeEntityQuery("SELECT oa"
                    + " FROM OEntityAction oa"
                    + " WHERE oa.oownerEntity.dbid = " + oentityDBID
                    + " AND oa.actionType.dbid = " + actionType, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("oentityDBID: {}, actionType: {}", oentityDBID, actionType);
        }
        if (null != entityAction) {
            logger.debug("Returning with: {}", entityAction.getClassName());
        }
        return entityAction;
    }

    @Override
    public OEntityDTO loadOEntityDTOByClassName(String entityClassName, OUser loggedUser) {
        logger.debug("Entering");
        OEntityDTO entityDTO = clearCashAndLoadEntityDTO(entityClassName, loggedUser);
        getCCExpressions(entityDTO, false, loggedUser);
        return entityDTO;
    }

    @Override
    public OEntityDTO loadOEntityDTOByDBID(long dbid, OUser loggedUser) {
        logger.debug("Entering");
        String query = "SELECT NEW com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO("
                + " e.dbid, e.entityClassPath, e.requestable, e.title)"
                + " FROM OEntity e"
                + " WHERE e.dbid = " + dbid;
        OEntityDTO entityDTO = null;
        try {
            entityDTO = (OEntityDTO) oem.executeEntityQuery(query, loggedUser);

            if (!loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).
                    getSvalue()) && null != entityDTO) {
                String title = (String) oem.executeEntityNativeQuery("select title from"
                        + " oentitytranslation trans where trans.entityDBID = "
                        + entityDTO.getDbid()
                        + " and trans.language_dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                entityDTO.setTitle(title);
            }
            if (entityDTO != null) {
                cachedOEntityDTO.put(loggedUser.getTenant().getId() + "_"
                        + entityDTO.getEntityClassName(), entityDTO);
                // <editor-fold defaultstate="collapsed" desc="Log">
                if (null != entityDTO) {
                    logger.trace("OEnity Not Found In Cache & Loaded. EntityClassName: {}", entityDTO.getEntityClassName());
                }
                // </editor-fold>
            } else {
                // <editor-fold defaultstate="collapsed" desc="Log">
                if (null != entityDTO) {
                    logger.trace("OEnity is null. EntityClassName: {}", entityDTO.getEntityClassName());
                }
                // </editor-fold>
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Query: {}", query);
        }
        if (null != entityDTO) {
            logger.debug("Returning with: {}", entityDTO.getEntityClassName());
        }
        return entityDTO;
    }

    @Override
    public OEntityDTO loadOEntityDTOByNameForAuditing(String entityClassName, OUser loggedUser) {
        logger.debug("Entering");
        String query = "SELECT NEW com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO("
                + " e.dbid, e.entityClassPath,e.auditable, e.systemAuditable)"
                + " FROM OEntity e"
                + " WHERE e.entityClassPath LIKE '%." + entityClassName + "'";
        OEntityDTO entityDTO = null;
        try {
            entityDTO = (OEntityDTO) oem.executeEntityQuery(query, loggedUser);
        } catch (Exception ex) {
            logger.trace("Query: {}", query);
            logger.error("Exception thrown", ex);
        }
        if (null != entityDTO) {
            logger.debug("Returning with: {}", entityDTO.getEntityClassName());
        }
        return entityDTO;
    }

    @Override
    public ArrayList<String> getCCExpressions(OEntityDTO entityDTO, boolean loadFromDB, OUser loggedUser) {
        logger.debug("Entering");
        ArrayList<String> ccExpressions = new ArrayList<String>();
        try {
            OEntity entity = new OEntity();
            if (loadFromDB) {
                String entityClassName = entityDTO.getEntityClassPath().substring(
                        entityDTO.getEntityClassPath().lastIndexOf(".") + 1,
                        entityDTO.getEntityClassPath().length());
                entity = loadOEntityByClassNameForTree(entityClassName, loggedUser);
            } else {
                entity = loadOEntity(entityDTO.getEntityClassPath(), loggedUser);
            }
            if (entity.getCcType1() != null) {
                entityDTO.setCcType1Dbid(entity.getCcType1().getDbid());
                ccExpressions.add("ccType1.value");
            } else {
                ccExpressions.add("");
            }

            if (entity.getCcType2() != null) {
                entityDTO.setCcType2Dbid(entity.getCcType2().getDbid());
                ccExpressions.add("ccType2.value");
            } else {
                ccExpressions.add("");
            }

            if (entity.getCcType3() != null) {
                entityDTO.setCcType3Dbid(entity.getCcType3().getDbid());
                ccExpressions.add("ccType3.value");
            } else {
                ccExpressions.add("");
            }
            if (entity.getCcType4() != null) {
                entityDTO.setCcType4Dbid(entity.getCcType4().getDbid());
                ccExpressions.add("ccType4.value");
            } else {
                ccExpressions.add("");
            }

            if (entity.getCcType5() != null) {
                entityDTO.setCcType5Dbid(entity.getCcType5().getDbid());
                ccExpressions.add("ccType5.value");
            } else {
                ccExpressions.add("");
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("entityDTO: {}", entityDTO);
        }
        if (ccExpressions != null) {
            logger.debug("Returning with ccExpressions of size: {}", ccExpressions.size());
        }
        return ccExpressions;

    }

    @Override
    public OEntity loadOEntityByClassNameForTree(String entityClassName, OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.debug("Entering");
        cachedOEntity.remove(loggedUser.getTenant().getId() + "_" + entityClassName);
        OEntity oentity = loadOEntityByClassName(entityClassName, loggedUser);
        return oentity;
    }

    /**
     *
     * @param entityClassName
     * @param loggedUser
     * @return
     * @throws com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException
     */
    @Override
    public OEntity loadOEntityByClassName(String entityClassName, OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.debug("Entering");
        OEntity oentity = cachedOEntity.get(loggedUser.getTenant().getId() + "_" + entityClassName);
        if (oentity == null) {
            List<String> fieldExpressions = new ArrayList<String>();
            fieldExpressions.addAll(BaseEntity.getChildEntityFieldName(OEntity.class.getName()));
            try {
                oentity = (OEntity) oem.loadEntity("OEntity",
                        Collections.singletonList("entityClassPath like '%." + entityClassName + "'"),
                        fieldExpressions, oem.getSystemUser(loggedUser));
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown", ex);
                // </editor-fold>
            } finally {
            }
            if (oentity != null) {
                cachedOEntity.put(loggedUser.getTenant().getId() + "_" + entityClassName, oentity);
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.trace("OEnity Not Found In Cache & Loaded. EntityClassName: {}", entityClassName);
                // </editor-fold>
            } else {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.trace("OEnity is null. EntityClassName: {}", entityClassName);
                // </editor-fold>
            }
        } else {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.trace("OEnity Got From Cache. EntityClassName: {}", entityClassName);
            // </editor-fold>
        }
        if (null != oentity) {
            logger.debug("Returning with: {}", oentity.getClassName());
        }
        return oentity;
    }

    public List<OEntityAction> getEntityActions(OEntity entity, OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.debug("Entering");
        List<String> cond = new ArrayList<String>();
        cond.add("displayed = true");
        cond.add("inActive = false");
        cond.add("oownerEntity.dbid = " + entity.getDbid());
        List<OEntityAction> entityActions = oem.loadEntityList("OEntityAction", cond, null,
                Collections.singletonList("sortIndex"), oem.getSystemUser(loggedUser));

        if (entityActions.getClass().getName().contains("IndirectList")) {

            // <editor-fold defaultstate="collapsed" desc="Log">
            if (null != entity) {
                logger.trace("EntityActions Initializing IndirectList. Entity: {}", entity.getClassName());
            }
            // </editor-fold>
            logger.debug("Returning with new ArrayList");
            return new ArrayList<OEntityAction>();
        }
        if (entityActions != null) {
            logger.debug("Returning with entityActions of size: {}", entityActions.size());
        }

        return entityActions;
    }
    @EJB
    FABSSetupLocal fABSSetupLocal;

    @Override
    public OEntityActionDTO getOEntityDTOActionDTO(long oentityDBID,
            long actionType, OUser loggedUser) {
        logger.debug("Entering");
        OEntityActionDTO entityAction = null;
        String query = "SELECT NEW com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO("
                + " a.dbid, a.name, fn.dbid, fn.name, a.oownerEntity.dbid,"
                + " a.oownerEntity.entityClassPath, eap.dbid, eap.name, a.sortIndex,"
                + " a.displayed, pv.dbid, pv.name, at.dbid ,a.displayCondition) "
                + " FROM OEntityAction a "
                + " LEFT JOIN a.ofunction fn"
                + " LEFT JOIN a.entityActionPrivilege eap"
                + " LEFT JOIN a.parentValidation pv"
                + " LEFT JOIN a.actionType at"
                + " WHERE at.dbid = " + actionType
                + " AND a.inActive = false"
                + " AND a.oownerEntity.dbid = " + oentityDBID
                + " ORDER BY  a.sortIndex";
        try {
            entityAction = (OEntityActionDTO) oem.executeEntityQuery(query, loggedUser);
            if (null != entityAction && entityAction.getOfuntionDBID() > 0) {
                String ftype = (String) oem.executeEntityNativeQuery("select FTYPE from ofunction where dbid = "
                        + entityAction.getOfuntionDBID(), loggedUser);
                entityAction.setOfunctionType(ftype);
                if ("SCREEN".equalsIgnoreCase(ftype)) {
                    String screenName = (String) oem.executeEntityQuery("SELECT sf.oscreen.name"
                            + " FROM ScreenFunction sf"
                            + " WHERE sf.dbid = " + entityAction.getOfuntionDBID(), loggedUser);
                    long screenMode = (Long) oem.executeEntityQuery("SELECT sf.screenMode.dbid"
                            + " FROM ScreenFunction sf"
                            + " WHERE sf.dbid = " + entityAction.getOfuntionDBID(), loggedUser);
                    entityAction.setOfunctionScreenName(screenName);
                    entityAction.setScreenMode(screenMode);
                }
            }
            if (!loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).getSvalue())
                    && null != entityAction) {
                String actionName = (String) oem.executeEntityNativeQuery("select name from"
                        + " OEntityActiontranslation entityAcTrans where entityAcTrans.entityDBID = "
                        + entityAction.getDbid()
                        + " and entityAcTrans.language_dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                if (null != actionName && !"".equals(actionName)) {
                    entityAction.setName(actionName);
                }
                if (0 < entityAction.getOfuntionDBID()) {
                    String functionName = (String) oem.executeEntityNativeQuery("select name from"
                            + " Ofunctiontranslation ofunTrans where ofunTrans.entityDBID = "
                            + entityAction.getOfuntionDBID()
                            + " and ofunTrans.language_dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                    if (null != functionName && !"".equals(functionName)) {
                        entityAction.setOfunctionName(functionName);
                    }
                }

            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Query: {}", query);
        }
        //TODO: Add security Check by using method checkPrivilegeAuthorithy
        if (null != entityAction) {
            logger.debug("Returning with: {}", entityAction.getName());
        }
        return entityAction;
    }

    @Override
    public OEntityActionDTO getActionDTO(long dbid, OUser loggedUser) {
        logger.debug("Entering");
        OEntityActionDTO entityAction = null;
        String query = "SELECT NEW com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO("
                + " a.dbid, a.name, fn.dbid, fn.name, a.oownerEntity.dbid,"
                + " a.oownerEntity.entityClassPath, eap.dbid, eap.name, a.sortIndex,"
                + " a.displayed, pv.dbid, pv.name, at.dbid, a.displayCondition) "
                + " FROM OEntityAction a "
                + " LEFT JOIN a.ofunction fn"
                + " LEFT JOIN a.entityActionPrivilege eap"
                + " LEFT JOIN a.parentValidation pv"
                + " LEFT JOIN a.actionType at"
                + " WHERE a.inActive = false"
                + " AND a.dbid = " + dbid
                + " ORDER BY  a.sortIndex";
        try {
            entityAction = (OEntityActionDTO) oem.executeEntityQuery(query, loggedUser);
            if (entityAction.getOfuntionDBID() > 0) {
                String ftype = (String) oem.executeEntityNativeQuery("select FTYPE from ofunction where dbid = "
                        + entityAction.getOfuntionDBID(), loggedUser);
                entityAction.setOfunctionType(ftype);
                if ("SCREEN".equalsIgnoreCase(ftype)) {
                    String screenName = (String) oem.executeEntityQuery("SELECT sf.oscreen.name"
                            + " FROM ScreenFunction sf"
                            + " WHERE sf.dbid = " + entityAction.getOfuntionDBID(), loggedUser);
                    long screenMode = (Long) oem.executeEntityQuery("SELECT sf.screenMode.dbid"
                            + " FROM ScreenFunction sf"
                            + " WHERE sf.dbid = " + entityAction.getOfuntionDBID(), loggedUser);
                    entityAction.setOfunctionScreenName(screenName);
                    entityAction.setScreenMode(screenMode);
                }
            }
            if (!loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).getSvalue())
                    && null != entityAction) {
                String actionName = (String) oem.executeEntityNativeQuery("select name from"
                        + " OEntityActiontranslation entityAcTrans where entityAcTrans.entityDBID = "
                        + entityAction.getDbid()
                        + " and entityAcTrans.language_dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                if (null != actionName && !"".equals(actionName)) {
                    entityAction.setName(actionName);
                }
                if (0 < entityAction.getOfuntionDBID()) {
                    String functionName = (String) oem.executeEntityNativeQuery("select name from"
                            + " Ofunctiontranslation ofunTrans where ofunTrans.entityDBID = "
                            + entityAction.getOfuntionDBID()
                            + " and ofunTrans.language_dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                    if (null != functionName && !"".equals(functionName)) {
                        entityAction.setOfunctionName(functionName);
                    }
                }

            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Query: {}", query);
        }
        //TODO: Add security Check by using method checkPrivilegeAuthorithy
        if (null != entityAction) {
            logger.debug("Returning with: {}", entityAction.getName());
        }
        return entityAction;
    }

    @Override
    public List<OEntityActionDTO> getDisplayedEntityActionsDTO(OEntityDTO entity, OUser loggedUser) {
        logger.debug("Entering");
        ArrayList<OEntityActionDTO> displayedActions = null;
        Vector<OEntityActionDTO> actionDTOs;
        String query;
        query = "SELECT NEW com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO("
                + "a.dbid,a.oownerEntity.dbid, a.name,fn.dbid, fn.name, a.sortIndex,"
                + "a.displayed) "
                + " FROM OEntityAction a left join a.ofunction fn"
                + " WHERE a.displayed = true"
                + " AND a.inActive = false"
                + " AND a.oownerEntity.dbid = " + entity.getDbid()
                + " ORDER BY  a.sortIndex";
        try {
            actionDTOs = (Vector<OEntityActionDTO>) oem.executeEntityListQuery(query, loggedUser);
            displayedActions = new ArrayList<OEntityActionDTO>(actionDTOs);
            for (OEntityActionDTO oEntityActionDTO : displayedActions) {
                if (oEntityActionDTO.getOfuntionDBID() > 0) {
                    String ftype = (String) oem.executeEntityNativeQuery("select FTYPE from ofunction where dbid = "
                            + oEntityActionDTO.getOfuntionDBID(), loggedUser);
                    oEntityActionDTO.setOfunctionType(ftype);
                    if ("SCREEN".equalsIgnoreCase(ftype)) {
                        String screenName = (String) oem.executeEntityQuery("SELECT sf.oscreen.name"
                                + " FROM ScreenFunction sf"
                                + " WHERE sf.dbid = " + oEntityActionDTO.getOfuntionDBID(), loggedUser);
                        long screenMode = (Long) oem.executeEntityQuery("SELECT sf.screenMode.dbid"
                                + " FROM ScreenFunction sf"
                                + " WHERE sf.dbid = " + oEntityActionDTO.getOfuntionDBID(), loggedUser);
                        oEntityActionDTO.setOfunctionScreenName(screenName);
                        oEntityActionDTO.setScreenMode(screenMode);
                    }
                }
            }
            if (!loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).getSvalue()) && null != displayedActions
                    && !displayedActions.isEmpty()) {
                for (OEntityActionDTO oEntityActionDTO : displayedActions) {
                    String actionName = (String) oem.executeEntityNativeQuery("select name from"
                            + " OEntityActiontranslation entityAcTrans where entityAcTrans.entityDBID = "
                            + oEntityActionDTO.getDbid()
                            + " and entityAcTrans.language_dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                    if (null != actionName && !"".equals(actionName)) {
                        oEntityActionDTO.setName(actionName);
                    }
                    if (0 < oEntityActionDTO.getOfuntionDBID()) {
                        String functionName = (String) oem.executeEntityNativeQuery("select name from"
                                + " Ofunctiontranslation ofunTrans where ofunTrans.entityDBID = "
                                + oEntityActionDTO.getOfuntionDBID()
                                + " and ofunTrans.language_dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                        if (null != functionName && !"".equals(functionName)) {
                            oEntityActionDTO.setOfunctionName(functionName);
                        }
                    }
                }

            }
            ArrayList<OEntityActionDTO> availableActions = new ArrayList<OEntityActionDTO>();
            for (Iterator<OEntityActionDTO> it = displayedActions.iterator(); it.hasNext();) {
                OEntityActionDTO oea = it.next();
                try {
                    securityService.checkActionOnlyAuthority(
                            oea, loggedUser, oem.getSystemUser(loggedUser));
                    availableActions.add(oea);
                } catch (Exception ex) {
                    System.out.println("");
                }
            }
            displayedActions = availableActions;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Query: {}", query);
        }
        if (displayedActions != null) {
            logger.debug("Returning with displayedActions of size: {}", displayedActions.size());
        }
        //TODO: Add security Check by using method checkPrivilegeAuthorithy
        return displayedActions;
    }

    @Override
    public List<OEntityActionDTO> getEntityActionsDTO(OEntityDTO entity, OUser loggedUser) {
        logger.debug("Entering");
        ArrayList<OEntityActionDTO> displayedActions = null;
        Vector<OEntityActionDTO> actionDTOs;
        String query;
        query = "SELECT NEW com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO("
                + "a.dbid,a.oownerEntity.dbid, a.name,fn.dbid, fn.name, a.sortIndex,"
                + "a.displayed,a.displayCondition) "///////
                + " FROM OEntityAction a left join a.ofunction fn"
                + " WHERE a.inActive = false"
                + " AND a.oownerEntity.dbid = " + entity.getDbid()
                + " ORDER BY  a.sortIndex";
        try {
            actionDTOs = (Vector<OEntityActionDTO>) oem.executeEntityListQuery(query, loggedUser);
            displayedActions = new ArrayList<OEntityActionDTO>(actionDTOs);
            for (OEntityActionDTO oEntityActionDTO : displayedActions) {
                if (oEntityActionDTO.getOfuntionDBID() > 0) {
                    String ftype = (String) oem.executeEntityNativeQuery("select FTYPE from ofunction where dbid = "
                            + oEntityActionDTO.getOfuntionDBID(), loggedUser);
                    oEntityActionDTO.setOfunctionType(ftype);
                    if ("SCREEN".equalsIgnoreCase(ftype)) {
                        String screenName = (String) oem.executeEntityQuery("SELECT sf.oscreen.name"
                                + " FROM ScreenFunction sf"
                                + " WHERE sf.dbid = " + oEntityActionDTO.getOfuntionDBID(), loggedUser);
                        long screenMode = (Long) oem.executeEntityQuery("SELECT sf.screenMode.dbid"
                                + " FROM ScreenFunction sf"
                                + " WHERE sf.dbid = " + oEntityActionDTO.getOfuntionDBID(), loggedUser);
                        oEntityActionDTO.setOfunctionScreenName(screenName);
                        oEntityActionDTO.setScreenMode(screenMode);
                    }
                }
            }
            if (!loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).getSvalue()) && null != displayedActions
                    && !displayedActions.isEmpty()) {
                for (OEntityActionDTO oEntityActionDTO : displayedActions) {
                    String actionName = (String) oem.executeEntityNativeQuery("select name from"
                            + " OEntityActiontranslation entityAcTrans where entityAcTrans.entityDBID = "
                            + oEntityActionDTO.getDbid()
                            + " and entityAcTrans.language_dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                    if (null != actionName && !"".equals(actionName)) {
                        oEntityActionDTO.setName(actionName);
                    }
                    if (0 < oEntityActionDTO.getOfuntionDBID()) {
                        String functionName = (String) oem.executeEntityNativeQuery("select name from"
                                + " Ofunctiontranslation ofunTrans where ofunTrans.entityDBID = "
                                + oEntityActionDTO.getOfuntionDBID()
                                + " and ofunTrans.language_dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                        if (null != functionName && !"".equals(functionName)) {
                            oEntityActionDTO.setOfunctionName(functionName);
                        }
                    }
                }

            }
            ArrayList<OEntityActionDTO> availableActions = new ArrayList<OEntityActionDTO>();
            for (Iterator<OEntityActionDTO> it = displayedActions.iterator(); it.hasNext();) {
                OEntityActionDTO oea = it.next();
                try {
                    securityService.checkActionOnlyAuthority(
                            oea, loggedUser, oem.getSystemUser(loggedUser));
                    availableActions.add(oea);
                } catch (Exception ex) {
                    System.out.println("");
                }
            }
            displayedActions = availableActions;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Query: {}", query);
        }
        //TODO: Add security Check by using method checkPrivilegeAuthorithy
        logger.debug("Returning");
        return displayedActions;
    }

    @Override
    public List<OObjectBriefInfoField> getObjectBriefInfoFields(OEntity entity, OUser loggedUser)
            throws Exception, UserNotAuthorizedException {
        //$$ load the briefinfofields directly similar to getEntityActions, without
        // loading the entity itself, load DD in expresssion
        logger.debug("Entering");
        List<String> fieldExpressions = new ArrayList<String>();
        fieldExpressions.add("oObjectBriefInfoField");
        OEntity oentity = null;
        try {
            oentity = (OEntity) oem.loadEntity(entity.getEntityClassName(),
                    entity.getDbid(), null, fieldExpressions, loggedUser);
        } catch (UserNotAuthorizedException ex) {
            throw new UserNotAuthorizedException(ex);
        }
        if (null == oentity) {
            logger.debug("Returning with new Arraylist");
            return new ArrayList<OObjectBriefInfoField>();
        }
        List<OObjectBriefInfoField> objectBriefInfoFields = oentity.getOObjectBriefInfoField();
        for (OObjectBriefInfoField objectBriefInfoField : objectBriefInfoFields) {
            objectBriefInfoField.getDd();
        }
        logger.debug("Returning with new ArrayList");
        return new ArrayList<OObjectBriefInfoField>(objectBriefInfoFields);
    }

    @Override
    public List<OObjectBriefInfoField> getObjectDTOBriefInfoFields(OEntityDTO entity, OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.debug("Entering");
        List<OObjectBriefInfoField> objectBriefInfoFields;
        try {
            objectBriefInfoFields = oem.loadEntityList(OObjectBriefInfoField.class.getSimpleName(),
                    Collections.singletonList("oObject.dbid = " + entity.getDbid()), null,
                    null, loggedUser);
        } catch (UserNotAuthorizedException ex) {
            throw new UserNotAuthorizedException(ex);
        }
        logger.debug("Returning");
        return objectBriefInfoFields;
    }

    @Override
    public List<EntityAttributeInit> getObjectDTOEntityAttributeInits(OEntityDTO entity, OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.debug("Entering");
        List<EntityAttributeInit> attributeInits;
        try {
            attributeInits = oem.loadEntityList(EntityAttributeInit.class.getSimpleName(),
                    Collections.singletonList("ownerEntity.dbid = " + entity.getDbid()), null,
                    null, loggedUser);
        } catch (UserNotAuthorizedException ex) {
            throw new UserNotAuthorizedException(ex);
        }
        logger.debug("Returning");
        return attributeInits;
    }

    public OObjectBriefInfoField getObjectMasterField(OEntity entity, OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.debug("Entering");
        OObjectBriefInfoField briefInfoField = null;
        List<String> conditions = new ArrayList<String>();
        Collections.addAll(conditions, "oObject.dbid=" + entity.getDbid(), "primary=true");
        List<String> fieldExpressions = new ArrayList<String>();
        Collections.addAll(fieldExpressions, "dd");
        briefInfoField = (OObjectBriefInfoField) oem.loadEntity("OObjectBriefInfoField",
                conditions, fieldExpressions, loggedUser);
        logger.debug("Returning");
        return briefInfoField;
    }

    public DD getObjectMasterFieldDD(OEntity entity, OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.debug("Entering");
        OObjectBriefInfoField briefInfoField = null;
        List<String> conditions = new ArrayList<String>();
        List<String> fieldExpressions = new ArrayList<String>();
        Collections.addAll(conditions, "primary=true", "oObject.dbid=" + entity.getDbid());
        Collections.addAll(fieldExpressions, "dd");
        briefInfoField = (OObjectBriefInfoField) oem.loadEntity("OObjectBriefInfoField",
                conditions, fieldExpressions, loggedUser);
        logger.debug("Returning");
        return briefInfoField.getDd();
    }

    public List<OEntityTreeDDField> getTreeDDFields(String classPath, int depth) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    protected List<OEntityTreeDDField> getTreeDDFields(String classPath, int depth, OEntityTreeDDField parentDDField) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public OFunctionResult runEntityActionPosts(OEntityAction action,
            ODataMessage oDM, OFunctionParms funcParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            //$$ ToDo: Order the posts
            List<EntityActionPost> actionPosts = action.getPosts();
            for (EntityActionPost post : actionPosts) {
                oFR.append(
                        oFunctionService.runFunction(post.getOfunction(),
                                oDM, funcParms, loggedUser));
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Entity Action: {}, DataMessage:{}", action, oDM);
            oFR.addError(usrMsgService.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public OFunctionResult executeAction(OEntityAction action,
            ODataMessage oDM, OFunctionParms funcParms, OUser loggedUser) {
        logger.debug("Entering");
        logger.debug("Returning");
        return executeAction(action, oDM, funcParms, loggedUser,
                true, true);
    }

    @Override
    public OFunctionResult executeAction(OEntityActionDTO action,
            ODataMessage oDM, OFunctionParms funcParms, OUser loggedUser) {
        logger.debug("Entering");
        logger.debug("Returning");
        return executeAction(action, oDM, funcParms, loggedUser,
                true, true);
    }

    private boolean validateReturnedODM(ODataMessage oldDM, ODataMessage newDM) {
        logger.debug("Entering");
        if (oldDM == null || newDM == null) {
            logger.trace("Returning with false");
            return false;
        }

        if (oldDM.getODataType().getDbid() != newDM.getODataType().getDbid()) {
            logger.trace("Returning with false");
            return false;
        }

        if (oldDM.getData().size() != newDM.getData().size()) {
            logger.trace("Returning with false");
            return false;
        }

        for (int i = 0; i < oldDM.getData().size(); i++) {
            if (oldDM.getData().get(i).getClass() != newDM.getData().get(i).getClass()) {
                logger.trace("Returning with false");
                return false;
            }
        }
        logger.trace("Returning with true");
        return true;

    }

    private List<EntityActionPost> getEntityDTOActionPosts(
            OEntityActionDTO entityActionDTO, OUser loggedUser) {
        logger.debug("Entering");
        List<EntityActionPost> entityActionPosts
                = new ArrayList<EntityActionPost>();
        try {
            String query = "SELECT p FROM EntityActionPost p"
                    + " WHERE p.oentityAction.dbid = "
                    + entityActionDTO.getDbid();
            entityActionPosts = oem.executeEntityListQuery(
                    query,
                    loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        if (entityActionPosts != null) {
            logger.trace("Returning with entityActionValidations for size: {}", entityActionPosts.size());
        }
        return entityActionPosts;
    }

    private List<EntityActionValidation> getEntityDTOActionValidation(
            OEntityActionDTO entityActionDTODTO,
            OUser loggedOUser) {
        logger.trace("Entering");
        List<EntityActionValidation> entityActionValidations
                = new ArrayList<EntityActionValidation>();
        try {
            String query = "SELECT v FROM EntityActionValidation v"
                    + " WHERE v.entityAction.dbid = " + entityActionDTODTO.getDbid();
            logger.trace("Query: {}", query);
            entityActionValidations = oem.executeEntityListQuery(
                    query,
                    loggedOUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        if (entityActionValidations != null) {
            logger.trace("Returning with entityActionValidations of size: {}", entityActionValidations.size());
        }
        return entityActionValidations;
    }
    
    @Override
    public OFunctionResult executeAction(OEntityActionDTO action,
            ODataMessage oDM, OFunctionParms funcParms, OUser loggedUser,
            boolean runPosts, boolean runValidations) {
        //TODO: Cache JavaFunc
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.trace("Executing Action. Action: {},Data Message: {}, Function Parameters: {}", action, oDM, funcParms);
        // </editor-fold>

        if (action == null) {
            logger.warn("Null Action is passed");
        }
        OFunctionResult oFR = new OFunctionResult();
//            OFunctionResult oFRCommit = new OFunctionResult();
        ODataMessage passedDM = oDM;
        boolean saveNewEntity = ((BaseEntity) passedDM.getData().get(0)).getDbid() == 0;
        //$$ ToDo: Order the validations
        OEntityDTO oEntityFromCache = null;
        try {
            oEntityFromCache = loadOEntityDTOByDBID(action.getOownerEntityDBID(),
                    loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            if (action != null) {
                logger.trace("OEntity", action.getOownerEntityDBID());
            }
            //oEntityFromCache = action.getOEntity();
        }
        if (runValidations) {
            ArrayList verifiedValidations = new ArrayList();
            try {
                List<OEntityActionDTO> oActions = getEntityActionsDTO(
                        oEntityFromCache, loggedUser);
                for (OEntityActionDTO oEntityAction : oActions) {
                    if (oEntityAction.getDbid() == action.getDbid()) {
                        action = oEntityAction;
                        break;
                    }
                }

                List<EntityActionValidation> actionValidations
                        = getEntityDTOActionValidation(action, loggedUser);
                for (EntityActionValidation validation : actionValidations) {
                    if (!validation.isInActive()) {
                        verifiedValidations.add(validation);
                    }
                }
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown", ex);
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
                logger.debug("Returning");
                return oFR;
                // </editor-fold>
            }
            if (!verifiedValidations.isEmpty()) {
                OFunctionResult validationFR = null;   // Used to save runFunction
                Comparator valComparator = new Comparator() {
                    @Override
                    public int compare(Object o1, Object o2) {
                        EntityActionValidation val1 = (EntityActionValidation) o1;
                        EntityActionValidation val2 = (EntityActionValidation) o2;
                        Integer level1index = val1.getSortIndex();
                        Integer level2index = val2.getSortIndex();
                        return level1index.compareTo(level2index);
                    }
                };
                Collections.sort(verifiedValidations, valComparator);
                for (Iterator it = verifiedValidations.iterator(); it.hasNext();) {
                    EntityActionValidation validation = (EntityActionValidation) it.next();
                    if (validation.isInActive()) {
                        continue;
                    }
                    if (validation.getFunction() instanceof ValidationJavaFunction) {
                        ODataMessage validationMessage = new ODataMessage();
                        ODataType validationJavaFunctionDT = null;
                        try {
                            validationJavaFunctionDT = dtService.loadODataType("ValidationJavaFunction", loggedUser);
                        } catch (Exception ex) {
                            logger.error("Exception thrown", ex);
                        }
                        List data = new ArrayList();
                        data.add(oDM.getODataType().getName());
                        data.add(oDM.getData().get(0));
                        if (null != validation.getFieldExpression()
                                && !"".equals(validation.getFieldExpression())) {
                            data.add(validation.getFieldExpression());
                        } else {
                            data.add(oDM.getData().get(1));
                        }
                        validationMessage.setODataType(validationJavaFunctionDT);
                        validationMessage.setData(data);

//                            oem.beginTransaction(loggedUser, null);
                        validationFR = oFunctionService.runFunction(
                                validation.getFunction(), validationMessage, funcParms, loggedUser);
//                            oFRCommit = oem.commitTransaction(loggedUser, null);
                        if (validationFR.getErrors().size() > 0) {
                            oFR.append(validationFR);
//                                oFR.append(oem.rollbackTransaction(loggedUser, null));
                            logger.debug("Returning");
                            return oFR;
                        }

                    } //                    else if (validation.getFunction() instanceof BatchJavaFunction) {
                    //
                    //                    }
                    else {
                        validationFR = oFunctionService.runFunction(
                                validation.getFunction(), oDM, funcParms, loggedUser);
                        if (validationFR.getErrors().size() > 0) {
                            oFR.append(validationFR);
                            logger.debug("Returning");
                            return oFR;
                        }
                    }
                    oFR.append(validationFR);
                    if (validationFR.getErrors().size() > 0 && validation.isStopOnError()) {
                        if (oFR.getReturnedDataMessage() == null) {
                            oFR.setReturnedDataMessage(passedDM);
                        }
                        logger.debug("Returning");
                        return oFR;
                    }
                    if (validationFR.getReturnedDataMessage() != null) {
                        if (validateReturnedODM(passedDM, validationFR.getReturnedDataMessage())) {
                            passedDM = validationFR.getReturnedDataMessage();
                        } else {
                            logger.warn("Validation Return DM Should Be Of The Same Passed DM");
                            if (validationFR != null && validation != null) {
                                logger.trace("oDM: {},Input DM: {},Output DM: {},Action Function: {} ", oDM, passedDM, validationFR.getReturnedDataMessage(), validation.getFunction());
                            }

                            oFR.addError(usrMsgService.getUserMessage(
                                    "SystemInternalError", loggedUser));
                            logger.debug("Returning");
                            return oFR;
                        }
                    }
                }
            }
        }

        OFunctionResult mainFunctionResult = null;
        OFunction entityFunction = null;
        try {
            entityFunction = functionService.getOFunction(action.getOfuntionDBID(), loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        mainFunctionResult = oFunctionService.runFunction(
                entityFunction, passedDM, funcParms, loggedUser);
        if (mainFunctionResult.getErrors().size() > 0) {
            oFR.append(mainFunctionResult);
            logger.debug("Returning");
            return oFR;
        }

        oFR.append(mainFunctionResult);

        if (oFR.getReturnedDataMessage() == null) {
            if (mainFunctionResult.getReturnedDataMessage() != null) {
                oFR.setReturnedDataMessage(mainFunctionResult.getReturnedDataMessage());
            } else {
                oFR.setReturnedDataMessage(passedDM);
            }
        }
        if (mainFunctionResult.getReturnedDataMessage() != null) {
            if (validateReturnedODM(passedDM, mainFunctionResult.getReturnedDataMessage())) {
                passedDM = mainFunctionResult.getReturnedDataMessage();
            } else {
                logger.warn("Validation Return DM Should Be Of The Same Passed DM");
                if (mainFunctionResult != null) {
                    logger.trace("oDM: {},Input DM: {},Output DM: {},Action Function: {} ", oDM, passedDM, mainFunctionResult.getReturnedDataMessage(), entityFunction);
                }

                // FIXME: should be uncommented. e.g. Save entity for tabular screen
                //oFR.addError(usrMsgService.getUserMessage(
                //        "SystemInternalError", loggedUser));
                logger.debug("Returning");
                return oFR;
            }
        }
        if (mainFunctionResult.getErrors().size() > 0) // Don't validate errors from oFR which may have non-stop errors from
        // Validations
        {
            // There are as errors and the action is not completed
            logger.debug("Returning");
            return oFR;
        }

        //$$ ToDo: Order the posts
        if (runPosts) {
            ArrayList verifiedPosts = new ArrayList();
//            List<OEntityAction> oActions = oEntityFromCache.getOActions();
//            for (OEntityAction oEntityAction : oActions) {
//                if(oEntityAction.getDbid() == action.getDbid()){
//                    action = oEntityAction;
//                    break;
//                }
//            }
            List<EntityActionPost> actionPosts = getEntityDTOActionPosts(action, loggedUser);
            for (Iterator it = actionPosts.iterator(); it.hasNext();) {
                EntityActionPost post = (EntityActionPost) it.next();
                if (!post.isInActive()) {
                    verifiedPosts.add(post);
                }
            }
            if (verifiedPosts != null && !verifiedPosts.isEmpty()) {
                if (oEntityFromCache != null && action != null) {
                    logger.trace("# Posts for {}  OAction is {} " + oEntityFromCache.getEntityClassName(), action.getName(), verifiedPosts.size());
                }

                OFunctionResult postFR = null;   // Used to save runFunction
                // OFunctionResult to be able to validate it separately from oFR
                // & validate oFR separately from it
                Comparator valComparator = new Comparator() {
                    @Override
                    public int compare(Object o1, Object o2) {
                        EntityActionPost val1 = (EntityActionPost) o1;
                        EntityActionPost val2 = (EntityActionPost) o2;
                        Integer level1index = val1.getSortIndex();
                        Integer level2index = val2.getSortIndex();
                        logger.debug("Returning");
                        return level1index.compareTo(level2index);
                    }
                };
                Collections.sort(verifiedPosts, valComparator);
                for (Iterator it = verifiedPosts.iterator(); it.hasNext();) {
                    EntityActionPost post = (EntityActionPost) it.next();
//                        oem.beginTransaction(loggedUser, null);
                    //oem.flush(loggedUser);
                    postFR = oFunctionService.runFunction(
                            post.getOfunction(), passedDM, funcParms, loggedUser);
//                        oFRCommit = oem.commitTransaction(loggedUser, null);
                    if (postFR.getErrors().size() > 0) {
                        oFR.append(postFR);
                        oFR.getSuccesses().clear();
                        try {
                            if (saveNewEntity) { //&& post.getOfunction().getClass() == ProcessFunction.class
                                oem.deleteCorruptedData((BaseEntity) passedDM.getData().get(0), loggedUser);
                                ((BaseEntity) passedDM.getData().get(0)).setDbid(0);
                            }
                        } catch (Exception ex) {
                            logger.error("exception", ex);
                        }
                        logger.debug("Returning");
                        return oFR;
                    }
                    // we shouldn't use OFucntionResult.append,
                    // cause we don't need the returned value from posts
                    oFR.getErrors().addAll(postFR.getErrors());
                    oFR.getSuccesses().addAll(postFR.getSuccesses());
                    oFR.getWarnings().addAll(postFR.getWarnings());

                    if (postFR.getReturnedDataMessage() != null) {
                        if (validateReturnedODM(passedDM, postFR.getReturnedDataMessage())) {
                            passedDM = postFR.getReturnedDataMessage();
                        } else {
                            logger.warn("Validation Return DM Should Be Of The Same Passed DM");
                            if (post != null && postFR != null) {
                                logger.trace("oDM: {}, Input DM: {}, Output DM: {}, Action Function: {} ", oDM, passedDM, postFR.getReturnedDataMessage(), post.getOfunction());
                            }

                            oFR.addError(usrMsgService.getUserMessage(
                                    "SystemInternalError", loggedUser));
                            logger.debug("Returning");
                            return oFR;
                        }
                    }
                }
            }
        }

        if (oFR.getReturnedDataMessage() == null) {
            oFR.setReturnedDataMessage(passedDM);
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public OFunctionResult executeAction(OEntityAction action,
            ODataMessage oDM, OFunctionParms funcParms, OUser loggedUser,
            boolean runPosts, boolean runValidations) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.trace("Executing Action. Action: {}, Data Message: {}, Function Parameters: {}", action, oDM, funcParms);
        // </editor-fold>

        if (action == null) {
            logger.warn("Null Action is passed");
        }
        OFunctionResult oFR = new OFunctionResult();
//            OFunctionResult oFRCommit = new OFunctionResult();
        ODataMessage passedDM = oDM;

        //$$ ToDo: Order the validations
        OEntity oEntityFromCache = null;
        try {
            oEntityFromCache = loadOEntity(action.getOownerEntity().getEntityClassName(), loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            if (null != action.getOEntity() && action != null) {
                logger.trace("OEntity", action.getOEntity().getEntityClassName());
            }
            oEntityFromCache = action.getOEntity();
        }

        if (runValidations) {
            ArrayList verifiedValidations = new ArrayList();
            try {
                List<OEntityAction> oActions = oEntityFromCache.getOActions();
                for (OEntityAction oEntityAction : oActions) {
                    if (oEntityAction.getDbid() == action.getDbid()) {
                        action = oEntityAction;
                        break;
                    }
                }

                action.getValidations().size();
                for (EntityActionValidation validation : action.getValidations()) {
                    if (!validation.isInActive()) {
                        verifiedValidations.add(validation);
                    }
                }
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown", ex);
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
                logger.debug("Returning");
                return oFR;
                // </editor-fold>
            }
            if (!verifiedValidations.isEmpty()) {
                OFunctionResult validationFR = null;   // Used to save runFunction
                Comparator valComparator = new Comparator() {
                    @Override
                    public int compare(Object o1, Object o2) {
                        EntityActionValidation val1 = (EntityActionValidation) o1;
                        EntityActionValidation val2 = (EntityActionValidation) o2;
                        Integer level1index = val1.getSortIndex();
                        Integer level2index = val2.getSortIndex();
                        logger.debug("Returning");
                        return level1index.compareTo(level2index);
                    }
                };
                Collections.sort(verifiedValidations, valComparator);
                for (Iterator it = verifiedValidations.iterator(); it.hasNext();) {
                    EntityActionValidation validation = (EntityActionValidation) it.next();
                    if (validation.isInActive()) {
                        continue;
                    }
                    if (validation.getFunction() instanceof ValidationJavaFunction) {
                        ODataMessage validationMessage = new ODataMessage();
                        ODataType validationJavaFunctionDT = null;
                        try {
                            validationJavaFunctionDT = dtService.loadODataType("ValidationJavaFunction", loggedUser);
                        } catch (Exception ex) {
                            logger.error("Exception thrown", ex);
                        }
                        List data = new ArrayList();
                        data.add(oDM.getODataType().getName());
                        data.add(oDM.getData().get(0));
                        data.add(validation.getFieldExpression());
                        validationMessage.setODataType(validationJavaFunctionDT);
                        validationMessage.setData(data);

//                            oem.beginTransaction(loggedUser, null);
                        validationFR = oFunctionService.runFunction(
                                validation.getFunction(), validationMessage, funcParms, loggedUser);
//                            oFRCommit = oem.commitTransaction(loggedUser, null);
                        if (validationFR.getErrors().size() > 0) {
                            oFR.append(validationFR);
//                                oFR.append(oem.rollbackTransaction(loggedUser, null));
                            logger.debug("Returning");
                            return oFR;
                        }

                    } //                    else if (validation.getFunction() instanceof BatchJavaFunction) {
                    //
                    //                    }
                    else {
                        validationFR = oFunctionService.runFunction(
                                validation.getFunction(), oDM, funcParms, loggedUser);
                        if (validationFR.getErrors().size() > 0) {
                            oFR.append(validationFR);
                            logger.debug("Returning");
                            return oFR;
                        }
                    }
                    oFR.append(validationFR);
                    if (validationFR.getErrors().size() > 0 && validation.isStopOnError()) {
                        if (oFR.getReturnedDataMessage() == null) {
                            oFR.setReturnedDataMessage(passedDM);
                        }
                        logger.debug("Returning");
                        return oFR;
                    }
                    if (validationFR.getReturnedDataMessage() != null) {
                        if (validateReturnedODM(passedDM, validationFR.getReturnedDataMessage())) {
                            passedDM = validationFR.getReturnedDataMessage();
                        } else {
                            logger.warn("Validation Return DM Should Be Of The Same Passed DM");
                            if (validation != null && validationFR != null) {
                                logger.trace("oDM: {},Input DM: {},Output DM: {},Validation Function: {} ", oDM, passedDM, validationFR.getReturnedDataMessage(), validation.getFunction());
                            }

                            oFR.addError(usrMsgService.getUserMessage(
                                    "SystemInternalError", loggedUser));
                            logger.debug("Returning");
                            return oFR;
                        }
                    }
                }
            }
        }

        OFunctionResult mainFunctionResult = null;
        mainFunctionResult = oFunctionService.runFunction(
                action.getOfunction(), passedDM, funcParms, loggedUser);
        if (mainFunctionResult.getErrors() != null && mainFunctionResult.getErrors().size() > 0) {
            oFR.append(mainFunctionResult);
            logger.debug("Returning");
            return oFR;
        }

        oFR.append(mainFunctionResult);

        if (oFR.getReturnedDataMessage() == null) {
            if (mainFunctionResult.getReturnedDataMessage() != null) {
                oFR.setReturnedDataMessage(mainFunctionResult.getReturnedDataMessage());
            } else {
                oFR.setReturnedDataMessage(passedDM);
            }
        }
        if (mainFunctionResult.getReturnedDataMessage() != null) {
            if (validateReturnedODM(passedDM, mainFunctionResult.getReturnedDataMessage())) {
                passedDM = mainFunctionResult.getReturnedDataMessage();
            } else {
                logger.warn("Validation Return DM Should Be Of The Same Passed DM");
                if (mainFunctionResult != null && action != null) {
                    logger.trace("oDM: {},Input DM: {},Output DM: {},Action Function: {} ", oDM, passedDM, mainFunctionResult.getReturnedDataMessage(), action.getOfunction());
                }

                // FIXME: should be uncommented. e.g. Save entity for tabular screen
                //oFR.addError(usrMsgService.getUserMessage(
                //        "SystemInternalError", loggedUser));
                logger.debug("Returning");
                return oFR;
            }
        }
        if (mainFunctionResult.getErrors() != null && mainFunctionResult.getErrors().size() > 0) // Don't validate errors from oFR which may have non-stop errors from Validations
        {
            // There are as errors and the action is not completed
            logger.debug("Returning");
            return oFR;
        }

        //$$ ToDo: Order the posts
        if (runPosts) {
            ArrayList verifiedPosts = new ArrayList();
            List<OEntityAction> oActions = oEntityFromCache.getOActions();
            for (OEntityAction oEntityAction : oActions) {
                if (oEntityAction.getDbid() == action.getDbid()) {
                    action = oEntityAction;
                    break;
                }
            }

            for (Iterator it = action.getPosts().iterator(); it.hasNext();) {
                EntityActionPost post = (EntityActionPost) it.next();
                if (!post.isInActive()) {
                    verifiedPosts.add(post);
                }
            }
            if (verifiedPosts != null && !verifiedPosts.isEmpty() && oEntityFromCache != null && action != null) {
                logger.trace("# Posts for: {} OAction: {} is {} ", oEntityFromCache.getEntityClassName(), action.getName(), verifiedPosts.size());

                OFunctionResult postFR = null;   // Used to save runFunction
                // OFunctionResult to be able to validate it separately from oFR
                // & validate oFR separately from it
                Comparator valComparator = new Comparator() {
                    @Override
                    public int compare(Object o1, Object o2) {
                        EntityActionPost val1 = (EntityActionPost) o1;
                        EntityActionPost val2 = (EntityActionPost) o2;
                        Integer level1index = val1.getSortIndex();
                        Integer level2index = val2.getSortIndex();
                        logger.debug("Returning");
                        return level1index.compareTo(level2index);
                    }
                };
                Collections.sort(verifiedPosts, valComparator);
                for (Iterator it = verifiedPosts.iterator(); it.hasNext();) {
                    EntityActionPost post = (EntityActionPost) it.next();
//                        oem.beginTransaction(loggedUser, null);
                    //oem.flush(loggedUser);
                    postFR = oFunctionService.runFunction(
                            post.getOfunction(), passedDM, funcParms, loggedUser);
//                        oFRCommit = oem.commitTransaction(loggedUser, null);
                    if (postFR.getErrors().size() > 0) {
                        oFR.append(postFR);
//                            oFR.append(oem.rollbackTransaction(loggedUser, null));
                        logger.debug("Returning");
                        return oFR;
                    }
                    // we shouldn't use OFucntionResult.append,
                    // cause we don't need the returned value from posts
                    oFR.getErrors().addAll(postFR.getErrors());
                    oFR.getSuccesses().addAll(postFR.getSuccesses());
                    oFR.getWarnings().addAll(postFR.getWarnings());

                    if (postFR.getReturnedDataMessage() != null) {
                        if (validateReturnedODM(passedDM, postFR.getReturnedDataMessage())) {
                            passedDM = postFR.getReturnedDataMessage();
                        } else {
                            logger.warn("Validation Return DM Should Be Of The Same Passed DM");
                            oFR.addError(usrMsgService.getUserMessage(
                                    "SystemInternalError", loggedUser));
                            logger.debug("Returning");
                            return oFR;
                        }
                    }
                }
            }
        }

        if (oFR.getReturnedDataMessage() == null) {
            oFR.setReturnedDataMessage(passedDM);
        }
        logger.debug("Returning");
        return oFR;
    }

    /**
     * Loads the parent entity using {@link #loadOEntity(String, OUser) }
     *
     * @param oEntity
     * @param loggedUser
     * @return Return list of {@link #loadOEntity(String, OUser) } parents,
     * empty list if no parents found, null if error
     * @throws com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException
     */
    @Override
    public List loadParentOEntity(OEntity oEntity, OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.debug("Entering");
        List<String> parentClassPath = oEntity.getEntityParentClassPath();
        ArrayList<OEntityDTO> parentOEntities = new ArrayList<OEntityDTO>();
        if (parentClassPath != null) {
            if (parentClassPath.size() == 1) {
                // One parent found
                OEntityDTO parentOEntity = loadOEntityDTO(parentClassPath.get(0), loggedUser);
                if (parentOEntity != null) {
                    parentOEntities.add(parentOEntity);
                }
            } else if (parentClassPath.size() == 2) {
                // Two parents found
                OEntityDTO parentOEntity = loadOEntityDTO(parentClassPath.get(0), loggedUser);
                if (parentOEntity != null) {
                    parentOEntities.add(parentOEntity);
                }
                parentOEntity = loadOEntityDTO(parentClassPath.get(1), loggedUser);
                if (parentOEntity != null) {
                    parentOEntities.add(parentOEntity);
                }
            } else {
                // Either case not supported , or no parent
                // Do nothing, may log error
            }
            logger.debug("Returning");
            return parentOEntities;
        } else {
            logger.debug("Returning with Null");
            return null;
        }
    }

    @Override
    public List loadParentOEntity(OEntityDTO oEntity, OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.debug("Entering");
        List<String> parentClassPath = oEntity.getEntityParentClassPath();
        ArrayList<OEntity> parentOEntities = new ArrayList<OEntity>();
        if (parentClassPath != null) {
            if (parentClassPath.size() == 1) {
                // One parent found
                OEntity parentOEntity = loadOEntity(parentClassPath.get(0), loggedUser);
                if (parentOEntity != null) {
                    parentOEntities.add(parentOEntity);
                }
            } else if (parentClassPath.size() == 2) {
                // Two parents found
                OEntity parentOEntity = loadOEntity(parentClassPath.get(0), loggedUser);
                if (parentOEntity != null) {
                    parentOEntities.add(parentOEntity);
                }
                parentOEntity = loadOEntity(parentClassPath.get(1), loggedUser);
                if (parentOEntity != null) {
                    parentOEntities.add(parentOEntity);
                }
            } else {
                // Either case not supported , or no parent
                // Do nothing, may log error
            }
            logger.debug("Returning");
            return parentOEntities;
        } else {
            logger.debug("Returning with Null");
            return null;
        }
    }

    public List<OEntity> loadChildOEntity(OEntity oEntity, OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.debug("Entering");
        List<OEntity> children = new ArrayList<OEntity>();
        List<String> childClassPath = oEntity.getEntityChildClassPath();
        for (int x = 0; x < childClassPath.size(); x++) {
            children.add(loadOEntity(childClassPath.get(x), loggedUser));
        }
        logger.debug("Returning with children of size: {}", children);
        return children;
    }
    public final static String VALIDOENTITYSTATUSMAPKEY = "VALIDOENTITYSTATUSMAPKEY";

    public OFunctionResult validateOEntityStautsMapping(
            ODataMessage oDM,
            OFunctionParms funcParms, OUser loggedUser) {
        logger.debug("Entering");
        List<EntityRequestStatus> customStatuses = new ArrayList<EntityRequestStatus>();
        customStatuses.addAll((ArrayList<EntityRequestStatus>) funcParms.getParams().get(VALIDOENTITYSTATUSMAPKEY));

        OFunctionResult oFunctionResult = new OFunctionResult();
        UserMessage userMessage = new UserMessage();
        EntityRequestStatus firstStatus = null, lastStatus = null;
        for (EntityRequestStatus entityRequestStatus : customStatuses) {
            if (entityRequestStatus.getMappedStatus() == null) {
                //FIXME: get from user message table
                userMessage.setMessageText(entityRequestStatus.getName()
                        + " Isn't mapped to anny predefined Status");
                oFunctionResult.addError(userMessage);
                continue;
            }

            if (entityRequestStatus.isFirst() && entityRequestStatus.isLast()) {
                //FIXME: get from user message table
                userMessage.setMessageText("Status should be either First or Last");
                oFunctionResult.addError(userMessage);
                continue;
            }

            if (entityRequestStatus.isFirst()) {
                if (firstStatus != null) {
                    //FIXME: get from user message table
                    userMessage.setMessageText("There should be only one first Statust");
                    oFunctionResult.addError(userMessage);
                } else {
                    firstStatus = entityRequestStatus;
                }
                if (entityRequestStatus.getMappedStatus().getPreRequestStatus() != null) {
                    //FIXME: get from user message table
                    userMessage.setMessageText("First status mustn't has prerequisite");
                    oFunctionResult.addError(userMessage);
                }
                continue;
            }

            if (entityRequestStatus.isLast()) {
                if (lastStatus != null) {
                    //FIXME: get from user message table
                    userMessage.setMessageText("There should be only one last Statust");
                    oFunctionResult.addError(userMessage);
                } else {
                    lastStatus = entityRequestStatus;
                }
                continue;
            }
            if (lastStatus != null && entityRequestStatus.getMappedStatus().
                    equals(lastStatus.getMappedStatus().getPreRequestStatus())) {
                //FIXME: get from user message table
                userMessage.setMessageText("Last Status Shouldn't be prerequisite to any status");
                oFunctionResult.addError(userMessage);
            }
            continue;

        }
        logger.debug("Returning");
        return oFunctionResult;
    }

    /**
     * Loads the Action Privilege into the Action Object
     *
     * @param action Action for which the privilege is required to be loaded
     * @param loggedUser
     * @throws com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException
     */
    public void loadEntityActionPrivilege(OEntityAction action, OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.debug("Entering");
        ArrayList<String> fieldExpressions = new ArrayList<String>();
        Collections.addAll(fieldExpressions, "entityActionPrivilege");
        logger.debug("Returning");
        // loadEntityDetail isn't used anymore as we are working today with Local EJBs
        // em.loadEntityDetail(action, null, fieldExpressions, loggedUser) ;
    }

    public OFunctionResult createDefaultBriefInfoFields(OEntity oEntity, OUser loggedUser)
            throws FABSException {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            oem.getEM(loggedUser);
            String entityClassPath = oEntity.getEntityClassPath();
            BaseEntity theEntity = (BaseEntity) Class.forName(entityClassPath).newInstance();
            List<Field> fieldsList = getDefaultBriefInfoFields(oEntity, loggedUser);
            int sortIndex = 0;
            for (Field field : fieldsList) {
                OObjectBriefInfoField briefInfoField = new OObjectBriefInfoField();
                String fieldDDName = BaseEntity.getFieldDDName(theEntity, field.getName());
                if (fieldDDName == null) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    if (null != theEntity) {
                        logger.warn("Can't Get DD Name. entity: {} ,Field: {}", theEntity.getClassName(), field);
                    }
                    // </editor-fold>
                    continue;
                }
                DD fieldDD = ddService.getDD(fieldDDName, loggedUser);
                if (fieldDD != null) {
                    briefInfoField.setDd(fieldDD);
                } else {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.warn("DD Not Found, Brief Info Field Won't Be Created. DD Name: {}", fieldDDName);
                    // </editor-fold>
                    continue;
                }
                briefInfoField.setOObject(oEntity);
                briefInfoField.setFieldExpression(field.getName());
                briefInfoField.setFieldJavaType(field.getDeclaringClass().getName());
                briefInfoField.setSortIndex(++sortIndex);
                // Save the Brief Info Field:
                callEntityCreateAction(briefInfoField, loggedUser);
                // <editor-fold defaultstate="collapsed" desc="Log">
                if (field != null) {
                    logger.debug("Brief Info Field Created. Brief Info Field: {}, Field Expression: {}", briefInfoField, field.getName());
                }
                // </editor-fold>
            }

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.trace("OEntity: {}", oEntity);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    /**
     * Returns list of the oEntity fields that are: . * <br>1. Not unitedofoq
     * entity object
     * <br>2. Not List
     * <br>3. Columns
     * <br>4. Not Nullable
     * <br>5. Not Boolean
     *
     * @param oEntity
     * @param loggedUser
     * @return
     * @throws FABSException
     */
    public List<Field> getDefaultBriefInfoFields(OEntity oEntity, OUser loggedUser)
            throws FABSException {
        logger.debug("Entering");
        try {
            String entityClassPath = oEntity.getEntityClassPath();
            List<Field> foundFieldsList = new ArrayList();
            BaseEntity theEntity = (BaseEntity) Class.forName(entityClassPath).newInstance();
            List<Field> entityFieldsList = theEntity.getAllFields();
            for (Field field : entityFieldsList) {
                String fieldDDName = BaseEntity.getFieldDDName(theEntity, field.getName());
                String fieldClassName = field.getType().getName();
                if (fieldClassName.contains("unitedofoq") || fieldClassName.endsWith("List")) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.debug("Field Exceluded: Because It's Either a List or unitedofoq Object. Field: {}", field);
                    // </editor-fold>
                    continue;
                }
                if (fieldDDName == null) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.debug("Field Exceluded: Because It Has No DD. Field: {}", field);
                    // </editor-fold>
                    continue;
                }
                if ("".equals(fieldDDName)) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.debug("Field Exceluded: Because It Has No DD. Field: {}", field);
                    // </editor-fold>
                    continue;
                }
                if (field.getAnnotation(Column.class) == null) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.debug("Field Exceluded: Beacause Nullable = true. Field: {}", field);
                    // </editor-fold>
                    continue;
                }
                if (field.getAnnotation(Column.class).nullable()) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.debug("Field Exceluded: Beacause Nullable = true. Field: {}", field);
                    // </editor-fold>
                    continue;
                }
                if (field.getDeclaringClass() == boolean.class) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.debug("Field Exceluded: Beacause It's Boolean. Field: {}", field);
                    // </editor-fold>
                    continue;
                }
                foundFieldsList.add(field);
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.debug("Field Included. Field: {}", field);
                // </editor-fold>
            }
            logger.debug("Returning");
            return foundFieldsList;
        } catch (Exception ex) {
            logger.debug("Returning");
            throw new FABSException(loggedUser, null, oEntity, ex);
        }
    }

    @Override
    public List<Field> getDefaultBriefInfoFields(OEntityDTO oEntity, OUser loggedUser)
            throws FABSException {
        logger.debug("Entering");
        try {
            String entityClassPath = oEntity.getEntityClassPath();
            List<Field> foundFieldsList = new ArrayList();
            BaseEntity theEntity = (BaseEntity) Class.forName(entityClassPath).newInstance();
            List<Field> entityFieldsList = theEntity.getAllFields();
            for (Field field : entityFieldsList) {
                String fieldDDName = BaseEntity.getFieldDDName(theEntity, field.getName());
                String fieldClassName = field.getType().getName();
                if (fieldClassName.contains("unitedofoq") || fieldClassName.endsWith("List")) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.debug("Field Exceluded: Because It's Either a List or unitedofoq Object. Field: {}", field);
                    // </editor-fold>
                    continue;
                }
                if (fieldDDName == null) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.debug("Field Exceluded: Because It Has No DD. Field: {}", field);
                    // </editor-fold>
                    continue;
                }
                if ("".equals(fieldDDName)) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.debug("Field Exceluded: Because It Has No DD. Field: {}", field);
                    // </editor-fold>
                    continue;
                }
                if (field.getAnnotation(Column.class) == null) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.debug("Field Exceluded: Beacause Nullable = true. Field: {}", field);
                    // </editor-fold>
                    continue;
                }
                if (field.getAnnotation(Column.class).nullable()) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.debug("Field Exceluded: Beacause Nullable = true. Field: {}", field);
                    // </editor-fold>
                    continue;
                }
                if (field.getDeclaringClass() == boolean.class) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.debug("Field Exceluded: Beacause It's Boolean. Field: {}", field);
                    // </editor-fold>
                    continue;
                }
                foundFieldsList.add(field);
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.debug("Field Included. Field: {}", field);
                // </editor-fold>
            }
            logger.debug("Returning");
            return foundFieldsList;
        } catch (Exception ex) {
            logger.debug("Returning");
            throw new FABSException(loggedUser, ex);
        }
    }

    /**
     * Call the Entity CREATE Action
     *
     * @param entity Entity for which the CREATE Action is called
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult callEntityCreateAction(BaseEntity entity, OUser loggedUser) {
        logger.debug("Entering");
        try {
            String entityName = entity.getClass().getName();
            OEntity oEntity = loadOEntity(entityName, oem.getSystemUser(loggedUser));
            if (oEntity == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("OEntity Missing. Entity Name: {}", entityName);
                // </editor-fold>
                logger.debug("Returning");
                return oFunctionService.constructOFunctionResultFromError("SystemInternalError", loggedUser);
            }
            ODataMessage oDM = entityService.constructEntityODataMessage(entity, oem.getSystemUser(loggedUser));
            if (oDM == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("DataMessage Error. Entity Name: {}", entityName);
                // </editor-fold>
                logger.debug("Returning");
                return oFunctionService.constructOFunctionResultFromError("SystemInternalError", loggedUser);
            }
            OEntityActionDTO action = this.
                    getOEntityDTOActionDTO(oEntity.getDbid(),
                            OEntityActionDTO.ATDBID_CREATE, loggedUser);
            logger.debug("Returning");
            return executeAction(action, oDM, null, loggedUser);

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            logger.debug("Returning");
            return oFunctionService.constructOFunctionResultFromError("SystemInternalError", ex, loggedUser);
        }
    }

    /**
     * Call the CREATE Action for every entity in the entityList
     *
     * @param entityList Entity for which the CREATE Action is called
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult callEntityListCreateAction(List entityList, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        if (entityList == null) {
            logger.debug("Returning");
            return oFR;
        }
        if (entityList.size() == 0) {
            logger.debug("Returning");
            return oFR;
        }
        try {
            oem.getEM(loggedUser);
            String entityName = entityList.get(0).getClass().getName();
            OEntity oEntity = loadOEntity(entityName, oem.getSystemUser(loggedUser));
            if (oEntity == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("OEntity Missing. Entity Name: {}", entityName);
                // </editor-fold>
                logger.debug("Returning");
                return oFunctionService.constructOFunctionResultFromError("SystemInternalError", loggedUser);
            }

            ODataMessage oDM = null;
            for (Object entity : entityList) {
                oDM = entityService.constructEntityODataMessage((BaseEntity) entity, oem.getSystemUser(loggedUser));
                if (oDM == null) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.warn("DataMessage Error. Entity Name: {}", entityName);
                    // </editor-fold>
                    oFR.append(oFunctionService.constructOFunctionResultFromError("SystemInternalError", loggedUser));
                    continue;
                }
                OEntityActionDTO action = this.
                        getOEntityDTOActionDTO(oEntity.getDbid(),
                                OEntityActionDTO.ATDBID_CREATE, loggedUser);
                oFR.append(executeAction(action, oDM, null, loggedUser));
            }
            logger.debug("Returning");
            return oFR;

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            logger.debug("Returning");
            return oFunctionService.constructOFunctionResultFromError("SystemInternalError", loggedUser);

        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
        }
    }

    /**
     * Call the UPDATE Action for the entity
     *
     * @param entity
     * @param loggedUser
     * @return
     */
    public OFunctionResult callEntityUpdateAction(BaseEntity entity, OUser loggedUser) {
        logger.debug("Entering");
        try {
            oem.getEM(loggedUser);
            String entityName = entity.getClass().getName();
            OEntity oEntity = loadOEntity(entityName, oem.getSystemUser(loggedUser));
            if (oEntity == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("OEntity Missing. Entity Name: {}", entityName);
                // </editor-fold>
                logger.debug("Returning");
                return oFunctionService.constructOFunctionResultFromError("SystemInternalError", loggedUser);
            }
            ODataMessage oDM = entityService.constructEntityODataMessage(entity, oem.getSystemUser(loggedUser));
            if (oDM == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("DataMessage Error. Entity Name: {}", entityName);
                // </editor-fold>
                logger.debug("Returning");
                return oFunctionService.constructOFunctionResultFromError("SystemInternalError", loggedUser);
            }
            OEntityActionDTO action = this.
                    getOEntityDTOActionDTO(oEntity.getDbid(),
                            OEntityActionDTO.ATDBID_UPDATE, loggedUser);
            logger.debug("Returning");
            return executeAction(action, oDM, null, loggedUser);

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            logger.debug("Returning");
            return oFunctionService.constructOFunctionResultFromError("SystemInternalError", ex, loggedUser);

        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
        }
    }

    /**
     * Call the UPDATE Action for every entity in the entityList
     *
     * @param entityList
     * @param loggedUser
     * @return
     */
    public OFunctionResult callEntityListUpdateAction(List<BaseEntity> entityList, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        if (entityList == null) {
            logger.debug("Returning");
            return oFR;
        }
        if (entityList.isEmpty()) {
            logger.debug("Returning");
            return oFR;
        }
        try {
            oem.getEM(loggedUser);
            String entityName = entityList.get(0).getClass().getName();
            OEntity oEntity = loadOEntity(entityName, oem.getSystemUser(loggedUser));
            if (oEntity == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("OEntity Missing. Entity Name: {}", entityName);
                // </editor-fold>
                logger.debug("Returning");
                return oFunctionService.constructOFunctionResultFromError("SystemInternalError", loggedUser);
            }

            ODataMessage oDM = null;
            for (BaseEntity entity : entityList) {
                oDM = entityService.constructEntityODataMessage(entity, oem.getSystemUser(loggedUser));
                if (oDM == null) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.warn("DataMessage Error. Entity Name: {}", entityName);
                    // </editor-fold>
                    oFR.append(oFunctionService.constructOFunctionResultFromError("SystemInternalError", loggedUser));
                    continue;
                }
                OEntityActionDTO action = this.
                        getOEntityDTOActionDTO(oEntity.getDbid(),
                                OEntityActionDTO.ATDBID_UPDATE, loggedUser);
                oFR.append(executeAction(action, oDM, null, loggedUser));
            }
            logger.debug("Returning");
            return oFR;

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            logger.debug("Returning");
            return oFunctionService.constructOFunctionResultFromError("SystemInternalError", ex, loggedUser);

        } finally {
            logger.debug("Returning");
            oem.closeEM(loggedUser);
        }
    }

    /**
     * Call the DELETE Action for the entity
     *
     * @param entity
     * @param loggedUser
     * @return
     */
    public OFunctionResult callEntityDeleteAction(BaseEntity entity, OUser loggedUser) {
        logger.debug("Entering");
        try {

            oem.getEM(loggedUser);
            String entityName = entity.getClass().getName();
            OEntity oEntity = loadOEntity(entityName, oem.getSystemUser(loggedUser));
            if (oEntity == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("OEntity Missing. Entity Name: {}", entityName);
                // </editor-fold>
                logger.debug("Returning");
                return oFunctionService.constructOFunctionResultFromError("SystemInternalError", loggedUser);
            }
            ODataMessage oDM = entityService.constructEntityODataMessage(entity, oem.getSystemUser(loggedUser));
            if (oDM == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("DataMessage Error. Entity Name: {}", entityName);
                // </editor-fold>
                logger.debug("Returning");
                return oFunctionService.constructOFunctionResultFromError("SystemInternalError", loggedUser);
            }
            OEntityActionDTO action = this.
                    getOEntityDTOActionDTO(oEntity.getDbid(),
                            OEntityActionDTO.ATDBID_DELETE, loggedUser);
            logger.debug("Returning");
            return executeAction(action, oDM, null, loggedUser);

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            logger.debug("Returning");
            return oFunctionService.constructOFunctionResultFromError("SystemInternalError", ex, loggedUser);

        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
        }
    }

    /**
     * Add, Update {@link OEntityReference}s of the input {@link OEntity}
     *
     * @param odm OEntity
     * @param functionParms Ignored
     * @param loggedUser
     * @return {@link OFunctionResult} with messages & the input odm as returned
     * data message
     */
    @Override
    public OFunctionResult refreshOEntityReferences(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = oFunctionService.validateJavaFunctionODM(
                odm, loggedUser, OEntity.class);
        if (inputValidationErrors != null) {
            logger.debug("Returning");
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        OFunctionResult tempFR = new OFunctionResult();
        OFunctionResult returnFR = new OFunctionResult();
        try {
            OEntity oentity = (OEntity) odm.getData().get(0);

            BaseEntity entityInstance;
            Class entityCls = null;

            // Both Abstract & MappedSuperClasses are invalid owner classes,
            // shouldn't get references for. No chance we have entity instance of
            // our class, so, no chance to have foreign keys values to it
            // Other classes may refer to our entity, but not the vice versa
            // <editor-fold defaultstate="collapsed" desc="Get entity instance & return if invalid class">
            try {
                entityCls = Class.forName(oentity.getEntityClassPath());
                if (Modifier.isAbstract(entityCls.getModifiers())) {
                    // Class is abstract
                    // Don't create references to it
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.warn("Invalid Reference Class - Owner Can't Be Abstract, Shouldn't Have OEntity. Entity Class Path: {}", oentity.getEntityClassPath());
                    // </editor-fold>
                    oFR.addError(usrMsgService.getUserMessage("InvalidRefOEntityClass",
                            Collections.singletonList(oentity.getEntityClassPath()),
                            loggedUser));
                    logger.debug("Returning");
                    return oFR;
                }
                entityInstance = (BaseEntity) entityCls.newInstance();
                if (entityInstance.getClass().getAnnotation(MappedSuperclass.class) != null) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    if (oentity != null) {
                        logger.warn("Invalid Reference Class - Owner Can't Be MappedSuperClass, Shouldn't Have OEntity. Entity Class Path: {}", oentity.getEntityClassPath());
                    }
                    // </editor-fold>
                    oFR.addError(usrMsgService.getUserMessage("InvalidRefOEntityClass",
                            Collections.singletonList(oentity.getEntityClassPath()),
                            loggedUser));
                    logger.debug("Returning");
                    return oFR;
                }
            } catch (Exception ex) {
                // Either class not found, or can't instantiate
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown", ex);
                if (oentity != null) {
                    logger.debug("Entity Class Path: {}", oentity.getEntityClassPath());
                }
                // </editor-fold>
                oFR.addError(usrMsgService.getUserMessage("InvalidRefOEntityClass",
                        Collections.singletonList(oentity.getEntityClassPath()),
                        loggedUser));
                logger.error("Exception thrown", ex);
                return oFR;
            }
            // </editor-fold>

            List<OEntityReference> referencesInDB = null;
            // <editor-fold defaultstate="collapsed" desc="Get all references from DB">
            try {
                referencesInDB = oem.loadEntityList(
                        OEntityReference.class.getSimpleName(),
                        Collections.singletonList("ownerOEntity.dbid = " + oentity.getDbid()),
                        null, null, oem.getSystemUser(loggedUser));
            } catch (Exception ex) {
                if (ex instanceof NoResultException) {
                    // No problem with that
                } else {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.error("Exception thrown", ex);
                    logger.debug("odm: {}", odm);
                    // </editor-fold>
                    oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
                    logger.debug("Returning");
                    return oFR;
                }
            }
            // </editor-fold>

            List<Field> entityRefFields = new ArrayList<Field>();
            // <editor-fold defaultstate="collapsed" desc="Get entity reference fields, including superclasses">
            Class tempEntitySuperCls = entityInstance.getClass();
            do {
                Field declaredFields[] = tempEntitySuperCls.getDeclaredFields();
                for (Field declaredField : declaredFields) {
                    Class fieldObjType = BaseEntity.getFieldObjectType(declaredField);
                    if (fieldObjType.getName().contains("unitedofoq")
                            && declaredField.getAnnotation(Transient.class) == null) {
                        // Field is either an object or list of objects inherited from BaseEntity
                        entityRefFields.add(declaredField);
                    }
                }
                tempEntitySuperCls = tempEntitySuperCls.getSuperclass();
            } while (tempEntitySuperCls != null) // Super Class is Abstract or MappedSuperClass are accepted
                    // As their fields are fields in our class, shouldn't exclude them
                    ;
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Get entity parent field names">
            List<String> entityParentFldNames = BaseEntity.getParentEntityFieldName(
                    entityInstance.getClass().getName());
            List<String> entityChildFldNames = BaseEntity.getChildEntityFieldName(
                    entityInstance.getClass().getName());
            // </editor-fold>

            List<OEntityReference> newReferences = new ArrayList<OEntityReference>();
            // Add new references to newReferences
            // & remove valid references from referencesInDB
            // & remove checked field from entityRefFields
            // <editor-fold defaultstate="collapsed" desc="fill newReferences, filter referencesInDB, entityRefFields">
            // 1. If found in DB: Remove field from entityRefFields & Remove referencesInDB dbRef
            // 2. If not found in DB: Add it to newReferences,
            //    and Remove field from entityRefFields & Remove referencesInDB dbRef
            if (referencesInDB.isEmpty()) {
                // No references previously saved
                for (int fieldIndex = 0; fieldIndex < entityRefFields.size();) {
                    // Create new reference & add it to newReferences
                    Field entityRefField = entityRefFields.get(fieldIndex);
                    OEntity refOEntity = this.loadOEntity(
                            BaseEntity.getFieldObjectType(entityRefField).getName(),
                            oem.getSystemUser(loggedUser));
                    if (refOEntity == null) {
                        // Entity has no OEntity, weired

                        // Remove the field from the entityRefFields
                        entityRefFields.remove(fieldIndex);

                        // Do nothing, and continue
                        continue;
                    }
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    Class refEntityCls = null;
                    try {
                        refEntityCls = Class.forName(refOEntity.getEntityClassPath());
                        if (refEntityCls.getAnnotation(MappedSuperclass.class) != null) {
                            // <editor-fold defaultstate="collapsed" desc="Log">
                            if (refOEntity != null) {
                                logger.warn("Invalid Reference Class - Reference Can't Be MappedSuperClass. Reference Entity Class Path: {}", refOEntity.getEntityClassPath());
                            }
                            // </editor-fold>
                            oFR.addError(usrMsgService.getUserMessage("InvalidRefOEntityClass",
                                    Collections.singletonList(refOEntity.getEntityClassPath()),
                                    loggedUser));
                            logger.debug("Returning");
                            return oFR;
                        }
                    } catch (Exception ex) {
                        // <editor-fold defaultstate="collapsed" desc="Log">
                        logger.error("Exception thrown", ex);
                        if (refOEntity != null) {
                            logger.debug("Reference Entity Class Path: {}", refOEntity.getEntityClassPath());
                        }
                        // </editor-fold>
                        oFR.addError(usrMsgService.getUserMessage("InvalidRefOEntityClass",
                                Collections.singletonList(refOEntity.getEntityClassPath()),
                                loggedUser));
                        logger.debug("Returning");
                        return oFR;
                    }

                    OEntityReference newRef = new OEntityReference();
                    newRef.setOwnerOEntity(oentity);
                    newRef.setOwnerOEntityTableName(entityInstance.getEntityTableName().get(0));
                    newRef.setFieldName(entityRefField.getName());
                    newRef.setFieldColumnName(entityInstance.getFieldColumnName(entityRefField));
                    newRef.setRefOEntity(refOEntity);
                    newRef.setRefOEntityTableName(BaseEntity.getEntityTableName(refEntityCls).get(0));
                    if (entityParentFldNames != null) {
                        newRef.setRefParent(entityParentFldNames.contains(entityRefField.getName()));
                    }
                    if (entityChildFldNames != null) {
                        newRef.setRefChild(entityChildFldNames.contains(entityRefField.getName()));
                    }
                    newRef.setListFld(BaseEntity.getFieldType(entityRefField) == List.class);
                    newRef.setOmodule(oentity.getModule());
                    newReferences.add(newRef);

                    // Remove the field from the entityRefFields
                    entityRefFields.remove(0);
                }
            } else {
                // Found references previously saved

                // Delete refrences which have fields that don't exist in entity class
                for (OEntityReference dbRef : referencesInDB) {
                    Field entityField = BaseEntity.getClassField(entityInstance.getClass(), dbRef.getFieldName());
                    if (entityField == null) {
                        oem.deleteEntity(dbRef, loggedUser);
                        // Success Message
                        ArrayList<String> msgParms = new ArrayList<String>();
                        msgParms.add(oentity.getTitle());
                        msgParms.add(dbRef.getFieldName());
                        msgParms.add(dbRef.getRefOEntity().getTitle());
                        // Add success message to temp FR until guaranteeing it's
                        // successfully commited
                        returnFR.addSuccess(usrMsgService.getUserMessage("OEntityRefDeleted",
                                msgParms, oem.getSystemUser(loggedUser)));
                    }
                }
                // Remove found ones, and add not found ones
                for (int fieldIndex = 0; fieldIndex < entityRefFields.size();) {
                    Field entityRefField = entityRefFields.get(fieldIndex);
                    String fieldName = entityRefField.getName();
                    boolean foundInDB = false;

                    for (OEntityReference dbRef : referencesInDB) {
                        if (!fieldName.equals(dbRef.getFieldName())) // Not same field name
                        {
                            continue;
                        }

                        // Same field name
                        if (isOEntityReferenceChanged(entityInstance, dbRef, entityRefField)) {
                            // Delete the existing one
                            oem.deleteEntity(dbRef, loggedUser);
                            // Remove it from DB, as if it wasn't there
                            referencesInDB.remove(dbRef);
                            break; // Don't set foundInDB = true, so, it's not found in DB
                            // to be added
                        }

                        // Same reference, not changed
                        // Remove it from both lists
                        entityRefFields.remove(fieldIndex);
                        referencesInDB.remove(dbRef);
                        foundInDB = true;
                        break;
                    }
                    if (foundInDB) // Removed from entityRefFields & referencesInDB
                    {
                        continue;
                    }

                    // Create new reference & add it to newReferences
                    OEntity refOEntity = this.loadOEntity(
                            BaseEntity.getFieldObjectType(entityRefField).getName(),
                            oem.getSystemUser(loggedUser));
                    if (refOEntity == null) {
                        // Entity has no OEntity, weired
                        // Remove the field from the entityRefFields
                        entityRefFields.remove(fieldIndex);

                        // Do nothing, and continue
                        continue;
                    }
                    OEntityReference newRef = new OEntityReference();
                    newRef.setOwnerOEntity(oentity);
                    newRef.setOwnerOEntityTableName(entityInstance.getEntityTableName().get(0));
                    newRef.setFieldName(fieldName);
                    newRef.setFieldColumnName(entityInstance.getFieldColumnName(entityRefField));
                    newRef.setRefOEntity(refOEntity);
                    BaseEntity refEntityInstance;
                    try {
                        refEntityInstance = (BaseEntity) Class.forName(
                                refOEntity.getEntityClassPath()).newInstance();
                        if (refEntityInstance.getClass().getAnnotation(MappedSuperclass.class) != null) {
                            // <editor-fold defaultstate="collapsed" desc="Log">
                            if (refOEntity != null) {
                                logger.warn("Invalid Reference Class - Reference Can't Be MappedSuperClass. Reference Entity Class Path: {}", refOEntity.getEntityClassPath());
                            }
                            // </editor-fold>
                            oFR.addError(usrMsgService.getUserMessage("InvalidRefOEntityClass",
                                    Collections.singletonList(refOEntity.getEntityClassPath()),
                                    loggedUser));
                            logger.debug("Returning");
                            return oFR;
                        }
                    } catch (Exception ex) {
                        // <editor-fold defaultstate="collapsed" desc="Log">
                        logger.error("Exception thrown", ex);
                        if (refOEntity != null) {
                            logger.debug("Reference Entity Class Path: {}", refOEntity.getEntityClassPath());
                        }
                        // </editor-fold>
                        oFR.addError(usrMsgService.getUserMessage("InvalidRefOEntityClass",
                                Collections.singletonList(refOEntity.getEntityClassPath()),
                                loggedUser));
                        logger.debug("Returning");
                        return oFR;
                    }
                    newRef.setRefOEntityTableName(refEntityInstance.getEntityTableName().get(0));
                    if (entityParentFldNames != null) {
                        newRef.setRefParent(entityParentFldNames.contains(fieldName));
                    }
                    if (entityChildFldNames != null) {
                        newRef.setRefChild(entityChildFldNames.contains(entityRefField.getName()));
                    }
                    newRef.setListFld(BaseEntity.getFieldType(entityRefField) == List.class);
                    newRef.setOmodule(oentity.getModule());
                    newReferences.add(newRef);

                    // Remove the field from the entityRefFields
                    entityRefFields.remove(fieldIndex);
                }
            }
            // </editor-fold>

            //FIXME: update audit trail
            // <editor-fold defaultstate="collapsed" desc="Create new references in DB (found in newReferences)">
            for (OEntityReference newRef : newReferences) {
                OFunctionResult ofr = this.callEntityCreateAction(newRef, oem.getSystemUser(loggedUser));
                if (!ofr.getErrors().isEmpty()) {
                    returnFR.append(ofr);
                    continue;
                }

                // Success Message
                ArrayList<String> msgParms = new ArrayList<String>();
                msgParms.add(oentity.getTitle());
                msgParms.add(newRef.getFieldName());
                msgParms.add(newRef.getRefOEntity() == null ? "" : newRef.getRefOEntity().getTitle());
                // Add success message to temp FR until guaranteeing it's
                // successfully commited
                returnFR.addSuccess(usrMsgService.getUserMessage("OEntityRefAdded",
                        msgParms, oem.getSystemUser(loggedUser)));
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Delete invalid references (left in referencesInDB)">
            // Remaining references in referencesDB are assumed to be invalid any more
            for (OEntityReference invalidRef : referencesInDB) {
                this.callEntityDeleteAction(invalidRef, oem.getSystemUser(loggedUser));

                // Success Message
                ArrayList<String> msgParms = new ArrayList<String>();
                msgParms.add(oentity.getTitle());
                msgParms.add(invalidRef.getFieldName());
                msgParms.add(invalidRef.getRefOEntity().getTitle());
                // Add success message to temp FR until guaranteeing it's
                // successfully commited
                returnFR.addSuccess(usrMsgService.getUserMessage("OEntityRefDeleted",
                        msgParms, oem.getSystemUser(loggedUser)));
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Add success message to oFR & set odm">
            if (newReferences.isEmpty() && referencesInDB.isEmpty()) {
                // Nothing to be deleted & nothing to be added
                oFR.addSuccess(usrMsgService.getUserMessage("OEntityRefsSame",
                        Collections.singletonList(oentity.getTitle()),
                        oem.getSystemUser(loggedUser)));
            } else {
                oFR.addSuccess(usrMsgService.getUserMessage("OEntityRefRefreshed",
                        Collections.singletonList(oentity.getTitle()),
                        oem.getSystemUser(loggedUser)));
            }
            oFR.append(returnFR);
            oFR.setReturnedDataMessage(odm);
            // </editor-fold>

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.debug("odm: {}", odm);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    public boolean isOEntityReferenceChanged(
            BaseEntity entityInst, OEntityReference oer, Field refField) {
        logger.debug("Entering");
        // Check type
        Class refFieldObjType = BaseEntity.getFieldObjectType(refField);
        if (!refFieldObjType.getName().equals(oer.getRefOEntity().getEntityClassPath())) {
            logger.debug("Returning");
            return true;
        }

        // Check parent
        List<String> entityParentFldNames = BaseEntity.getParentEntityFieldName(
                entityInst.getClass().getName());
        if (entityParentFldNames != null && !entityParentFldNames.isEmpty()) {
            if (oer.isRefParent() && !entityParentFldNames.contains(refField.getName())) {
                logger.debug("Returning");
                return true;
            }
            if (!oer.isRefParent() && entityParentFldNames.contains(refField.getName())) {
                logger.debug("Returning");
                return true;
            }
        }
        List<String> entityChildFldNames = BaseEntity.getChildEntityFieldName(
                entityInst.getClass().getName());
        if (entityChildFldNames != null && !entityChildFldNames.isEmpty()) {
            if (oer.isRefChild() && !entityChildFldNames.contains(refField.getName())) {
                logger.debug("Returning");
                return true;
            }
            if (!oer.isRefChild() && entityChildFldNames.contains(refField.getName())) {
                logger.debug("Returning");
                return true;
            }
        }
        //FIXME: validate also rest of properties, e.g. parent, isList, etc...
        logger.debug("Returning");
        return false;
    }

    /**
     * Calls
     * {@link EntitySetupService#refreshOEntityReferences(ODataMessage, OFunctionParms, OUser)}
     * for all {@link OEntity} found in the database
     *
     * @param odm Ignored
     * @param functionParms
     * @param loggedUser
     * @return Errors, Successes, and Warnings. No returned ODM
     */
    @Override
    public OFunctionResult refreshAllOEntitiesReferences(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            oem.getEM(oem.getSystemUser(loggedUser));
            List<OEntity> allOEntities = (List<OEntity>) oem.loadEntityList(
                    OEntity.class.getSimpleName(), null, null, null, loggedUser);
            for (OEntity oentity : allOEntities) {
                try {
                    // Try catch exception, so, if one OEntity failed, the function
                    // then continues the following OEntities
                    oFR.append(refreshOEntityReferences(
                            dtService.constructEntityDefaultODM(oentity, oem.getSystemUser(loggedUser)),
                            null, loggedUser));
                } catch (Exception ex) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.error("Exception thrown", ex);
                    // </editor-fold>
                    oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
                }
            }
            // to delete oentityrefrence when the oentity does not exist (hard deleted)
            deleteNonExistingOEntitiesRefrences(loggedUser);
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    private OFunctionResult deleteOEntityReference(OEntityReference oer, OUser loggedUser) {
        logger.trace("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            //oem.deleteEntity(oer, loggedUser);
            this.callEntityDeleteAction(oer, loggedUser);

            // Success Message
            ArrayList<String> msgParms = new ArrayList<String>();
            msgParms.add(oer.getFieldName());
            msgParms.add(oer.getRefOEntity().getTitle());
            oFR.addSuccess(usrMsgService.getUserMessage(
                    "OEntityRefDeleted", msgParms, oem.getSystemUser(loggedUser)));

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.trace("OEntityReference", oer);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.trace("Returning");
            return oFR;
        }
    }

    /**
     * Called by
     * {@link EntitySetupService#refreshAllOEntitiesReferences(ODataMessage, OFunctionParms, OUser)}
     *
     * @return Errors, Successes, and Warnings. No returned ODM
     */
    @Override
    public OFunctionResult deleteNonExistingOEntitiesRefrences(OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
//            EntityManager em = oem.getEM(oem.getSystemUser(loggedUser)) ;

            List<OEntityReference> allOERefs = oem.loadEntityList(
                    OEntityReference.class.getSimpleName(), null, null, null, loggedUser);
            for (OEntityReference oer : allOERefs) {
                if (oer.getOwnerOEntity() == null) {
                    oFR.append(deleteOEntityReference(oer, loggedUser));
                    continue;
                }
                if (oer.getRefOEntity() == null) {
                    oFR.append(deleteOEntityReference(oer, loggedUser));
                    continue;
                }
            }

            String query = "SELECT o.dbid from " + OEntity.class.getSimpleName() + " o";
            List< Long> oentityDBIDs = new ArrayList<Long>();
            logger.debug("Query: {}", query);
            oentityDBIDs = oem.executeEntityListNativeQuery(query, loggedUser);
            String allOEntityDBIDs = "";
            for (Iterator<Long> dbidIterator = oentityDBIDs.iterator(); dbidIterator.hasNext();) {
                allOEntityDBIDs = allOEntityDBIDs + dbidIterator.next() + ",";
            }

            allOEntityDBIDs = allOEntityDBIDs.substring(0, allOEntityDBIDs.length() - 1);

            String queryString = "SELECT o from " + OEntityReference.class.getSimpleName() + " o "
                    + " WHERE o.ownerOEntity.dbid NOT IN ( " + allOEntityDBIDs + " )";
            logger.debug("Query: {}", queryString);
            List< OEntityReference> inValidRefrences = new ArrayList<OEntityReference>();
            inValidRefrences = oem.executeEntityListQuery(queryString, loggedUser);

            if (inValidRefrences != null && inValidRefrences.size() > 0) {
                for (OEntityReference invalidRef : inValidRefrences) {
                    oFR.append(deleteOEntityReference(invalidRef, loggedUser));
                }
            }

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    /**
     * Returns the Parent Entity with all brief info fields filled out
     *
     * @param entityOEntity
     * @param parentClassPath
     * @param parentDBID
     * @param loggedUser
     * @return Parent Entity on success
     * <br>null: Error
     *
     * @throws UserNotAuthorizedException
     */
    @Override
    public BaseEntity loadEntityParentForBriefInfoFields(
            OEntity entityOEntity, String parentClassPath, long parentDBID,
            OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.debug("Entering");
        try {
            oem.getEM(loggedUser);
            OUser systemUser = oem.getSystemUser(loggedUser);
            List<String> parentFieldsExpressions = new ArrayList<String>();
            String parentEntityName = parentClassPath.substring(parentClassPath.lastIndexOf(".") + 1);
            List<OObjectBriefInfoField> biFields = null;
            biFields = getObjectBriefInfoFields(loadOEntity(parentClassPath, loggedUser), systemUser);
            for (int i = 0; i < biFields.size(); i++) {
                parentFieldsExpressions.add(biFields.get(i).getFieldExpression());
            }
            logger.debug("Returning");
            return oem.loadEntity(parentEntityName, parentDBID,
                    null, parentFieldsExpressions, loggedUser);
        } catch (UserNotAuthorizedException ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("This Should not Happen, We Used System User", loggedUser);
            // </editor-fold>
            throw new UserNotAuthorizedException(ex);
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
        }
    }

    @Override
    public BaseEntity loadEntityParentForBriefInfoFields(
            OEntityDTO entityOEntity, String parentClassPath, long parentDBID,
            OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.debug("Entering");
        try {
            oem.getEM(loggedUser);
            OUser systemUser = oem.getSystemUser(loggedUser);
            List<String> parentFieldsExpressions = new ArrayList<String>();
            String parentEntityName = parentClassPath.substring(parentClassPath.lastIndexOf(".") + 1);
            List<OObjectBriefInfoField> biFields = null;
            biFields = getObjectBriefInfoFields(loadOEntity(parentClassPath, loggedUser), systemUser);
            for (int i = 0; i < biFields.size(); i++) {
                parentFieldsExpressions.add(biFields.get(i).getFieldExpression());
            }
            logger.debug("Returning");
            return oem.loadEntity(parentEntityName, parentDBID,
                    null, parentFieldsExpressions, loggedUser);
        } catch (UserNotAuthorizedException ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("This Should not Happen, We Used System User");
            // </editor-fold>
            throw new UserNotAuthorizedException(ex);
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Entity Builder">
    //<editor-fold defaultstate="collapsed" desc="Create Java Entity">
    /**
     * call create Entities For All Java Entities
     *
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult createAllJavaEntities(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        ODataMessage dataMessage = null;
        ODataType dataType = null;
        List<Object> data = null;
        try {
            List<OJavaEntity> loadedOEntity = (List<OJavaEntity>) oem.loadEntityList("OJavaEntity", null, null, null, loggedUser);
            dataType = dtService.loadODataType("OJavaEntity", loggedUser);
            for (OJavaEntity oJavaEntity : loadedOEntity) {
                if (oJavaEntity.isInActive() || oJavaEntity.getStatus().equals("built")) {
                    continue;
                }
                data = new ArrayList<Object>();
                data.add(oJavaEntity);
                data.add(loggedUser);
                dataMessage = new ODataMessage(dataType, data);
                createJavaClassFromOJavaEntity(dataMessage, functionParms, loggedUser);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return oFR;
    }

    /**
     * call create JavaClass from OJavaEntity
     *
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult createJavaClassFromOJavaEntity(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            OJavaEntity oJavaEntity = (OJavaEntity) odm.getData().get(0);
            List<OJavaEntityField> oJavaEntityFields = oJavaEntity.getOJavaEntityFields();
            String entityHeader = "";

            //<editor-fold defaultstate="collapsed" desc="packageAndImports">
            entityHeader += "\npackage " + oJavaEntity.getPackageName().getCode() + ";\n";
            String imports = "\nimport java.util.List;\nimport java.util.Date;\nimport javax.persistence.*;";
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="ClassName">
            String className = "public " + (oJavaEntity.isAbstractClass() ? "abstract " : "class ") + oJavaEntity.getName();
            imports += "\nimport java.math.BigDecimal;";
            if (oJavaEntity.getSuperOEntityClass() != null && oJavaEntity.getSuperOJavaEntityClass() != null) {
                if (oJavaEntity.getSuperOEntityClass() != null) {
                    imports += "\nimport " + oJavaEntity.getSuperOEntityClass().getEntityClassPath() + ";";
                    className += " extends " + oJavaEntity.getSuperOEntityClass().getEntityClassName();
                } else if (oJavaEntity.getSuperOJavaEntityClass() != null) {
                    imports += "\nimport " + oJavaEntity.getSuperOJavaEntityClass().getPackageName().getCode() + "." + oJavaEntity.getSuperOJavaEntityClass().getName() + ";";
                    className += " extends " + oJavaEntity.getSuperOJavaEntityClass().getPackageName().getCode() + "." + oJavaEntity.getSuperOJavaEntityClass().getName();
                }
            } else {
                //log no super class found for that entity
                imports += "\nimport com.unitedofoq.fabs.core.entitybase.BaseEntity;";
                className += " extends BaseEntity ";
            }
            className += " {\n";
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="classAnnotations">
            String classAnnotations = "\n@" + oJavaEntity.getEntityType().getCode();
            if (oJavaEntity.getParentField() != null) {
                imports += "\nimport com.unitedofoq.fabs.core.entitybase.ParentEntity;";
                classAnnotations += "\n@ParentEntity(fields={\"" + oJavaEntity.getParentField().getName() + "\"})";
            }
            String childAnnotation = "\n@ChildEntity(fields={";

            //<editor-fold defaultstate="collapsed" desc="inherted">
            if (oJavaEntity.getInheritedType() != null) {
                String inheritance = "\n@Inheritance(strategy=InheritanceType." + oJavaEntity.getInheritedType().getCode() + ")";
                classAnnotations += inheritance;
                if (!oJavaEntity.getInheritedType().getCode().equalsIgnoreCase("TABLE_PER_CLASS")) {
                    String discriminator = "\n@DiscriminatorColumn(name=\"DType\")";
                    classAnnotations += discriminator;
                }
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="inherits">
            if (oJavaEntity.getSuperOJavaEntityClass() != null) {
                if (oJavaEntity.getSuperOJavaEntityClass().getInheritedType() != null) {
                    if (!oJavaEntity.getSuperOJavaEntityClass().getInheritedType().getCode().equals("TABLE_PER_CLASS")) {
                        String discriminator = "\n@DiscriminatorValue(value=\"" + oJavaEntity.getName() + "\")";
                        classAnnotations += discriminator;
                    }
                }
            }
            //</editor-fold>

            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="attributes">
            String attributes = "";

            //<editor-fold defaultstate="collapsed" desc="Parent Attribute">
            if (oJavaEntity.getParentOJavaEntityClass() != null && oJavaEntity.getParentField() == null) {
                imports += "\nimport com.unitedofoq.fabs.core.entitybase.ParentEntity;";
                String parentFieldAnnotation = "", parentField = "", parentSetter = "", parentGetter = "", parentFieldDD = "";

                //<editor-fold defaultstate="collapsed" desc="fieldName">
                String fieldName = "" + oJavaEntity.getParentOJavaEntityClass().getName();
                fieldName = fieldName.substring(0, 2).toLowerCase() + fieldName.substring(2);
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="ParentEntityAnnotation">
                classAnnotations += "\n@ParentEntity(fields={\"" + fieldName + "\"})";
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="imports">
                imports += "\nimport " + oJavaEntity.getParentOJavaEntityClass().getPackageName().getCode()
                        + oJavaEntity.getParentOJavaEntityClass().getName() + ";";
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="parentFieldAnnotation">
                if (!oJavaEntity.isUnityRegardChild()) {
                    parentFieldAnnotation += "\n@ManyToOne";
                } else {
                    parentFieldAnnotation += "\n@OneToOne";
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="parentField">
                parentField += "\nprivate " + oJavaEntity.getParentOJavaEntityClass().getName() + " "
                        + oJavaEntity.getParentField().getName() + ";";
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="setter">
                parentSetter = "\n    public void set"
                        + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1) + "("
                        + oJavaEntity.getParentOJavaEntityClass().getName() + " " + fieldName + ") {\n";
                parentSetter += "        this." + fieldName + " = " + fieldName + ";\n    }\n";
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="getter">
                parentGetter = "\n    public void get" + Character.toUpperCase(fieldName.charAt(0))
                        + fieldName.substring(1) + "() {\n";
                parentGetter += "       return " + fieldName + ";\n    }\n";
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="DDGetter">
                parentFieldDD = "\n    public void get"
                        + Character.toUpperCase(fieldName.charAt(0))
                        + fieldName.substring(1) + "DD() {\n        return \""
                        + oJavaEntity.getName() + "_" + fieldName + "\";\n    }\n";
                //</editor-fold>

//                attributes += "\n    //<editor-fold defaultstate=\"collapsed\" desc=\"" + fieldName + "\">"
//                        + parentFieldAnnotation + parentField + parentSetter + parentGetter + parentFieldDD
//                        + "    // </editor-fold>\n";
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Others">
            if (oJavaEntityFields != null) {
                for (int i = 0; i < oJavaEntityFields.size(); i++) {
                    OJavaEntityField oJavaEntityField = oJavaEntityFields.get(i);
                    //<editor-fold defaultstate="collapsed" desc="fieldName">
                    String fieldName = "" + oJavaEntityField.getName();
                    fieldName = fieldName.substring(0, 2).toLowerCase() + fieldName.substring(2);
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="codeFold">
                    String attribute = "\n    // <editor-fold defaultstate=\"collapsed\" desc=\"" + fieldName + "\">";
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="fieldType & Annotation">
                    String fieldAnnotation = "";
                    String fieldType = oJavaEntityField.getFieldType().getCode();
                    if (fieldType.equalsIgnoreCase("entity")) {

                        //<editor-fold defaultstate="collapsed" desc="Entty Field Types & Imports">
                        if (oJavaEntityField.getFieldEntity() != null) {
                            imports += "\nimport " + oJavaEntityField.getFieldEntity().getEntityClassPath() + ";";
                            fieldType = oJavaEntityField.getFieldEntity().getEntityClassName();
                        } else if (oJavaEntityField.getFieldJavaEntity() != null) {
                            imports += "\nimport " + oJavaEntityField.getFieldJavaEntity().getPackageName().getCode() + "." + oJavaEntityField.getFieldJavaEntity().getName() + ";";
                            fieldType = oJavaEntityField.getFieldJavaEntity().getName();
                        } else {
                            // log that no field entity chosen & set default type
                            imports += "\nimport com.unitedofoq.fabs.core.udc.UDC;";
                            fieldType = "UDC";
                        }
                        //</editor-fold>

                        //<editor-fold defaultstate="collapsed" desc="Relation">
                        if (oJavaEntityField.isChildField()) {
                            if (!childAnnotation.endsWith("{")) {
                                childAnnotation += ",";
                            }
                            childAnnotation += "\"" + oJavaEntityField.getName() + "\"";
                        }
                        if (fieldType.equals("UDC")) {
                            fieldAnnotation += "\n    @ManyToOne";
                        } else {
                            fieldAnnotation += "\n    @" + oJavaEntityField.getFieldRelation().getCode();
                        }
                        if (oJavaEntityField.getMappedBy() != null && !oJavaEntityField.getMappedBy().isEmpty()
                                && oJavaEntityField.getFieldRelation().getCode().startsWith("One")) {
                            fieldAnnotation += "(mappedBy=\"" + oJavaEntityField.getMappedBy() + "\")";
                            //fieldAnnotation += "(mappedBy=\"" + oJavaEntityField.getMappedBy().getName() + "\")";
                        }
                        if (fieldAnnotation.contains("OneToMany")) {
                            fieldType = "List<" + fieldType + ">";
                        }
                        //</editor-fold>

                    } else {

                        //<editor-fold defaultstate="collapsed" desc="Primitive Field Type">
                        fieldType = oJavaEntityField.getFieldType().getCode();
                        fieldAnnotation += "\n    @Column(" + (oJavaEntityField.isMandatoryField() ? "nullable=false" : "") + (oJavaEntityField.isMandatoryField() && oJavaEntityField.isUniqueField() ? "," : "") + (oJavaEntityField.isUniqueField() ? "unique=true" : "") + ")";
                        if (fieldType.equals("Date")) {
                            fieldAnnotation += "\n    @Temporal(javax.persistence.TemporalType.DATE)";
                        } else if (fieldType.equals("TimeStamp")) {
                            imports += "\nimport java.sql.Timestamp;";
                            fieldAnnotation += "\n    @Temporal(javax.persistence.TemporalType.TIMESTAMP)";
                            fieldType = "Date";
                        } else if (fieldType.equals("mText")) {
                            fieldType = "String";
                        } else if (fieldType.equals("lText")) {
                            fieldType = "String";
                        } else if (fieldType.equals("text")) {
                            fieldType = "String";
                        }
                        //</editor-fold>

                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="Join Column">
                    if (oJavaEntityField.isJoinColumn()) {
                        fieldAnnotation += "\n@JoinColumn";
                    }
                    fieldAnnotation = fieldAnnotation.replace("()", "");
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="fieldDeclration">
                    String field = "\n    private " + fieldType + " " + fieldName + ";\n";
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="setter">
                    String setter = "\n    public void set" + Character.toUpperCase(oJavaEntityField.getName().charAt(0))
                            + oJavaEntityField.getName().substring(1) + "(" + fieldType + " " + fieldName
                            + ") {\n";
                    setter += "        this." + fieldName + " = " + fieldName + ";\n    }\n";
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="getter">
                    String getter = "\n    public " + fieldType + " " + (fieldType.equalsIgnoreCase("boolean") ? "is" : "get")
                            + Character.toUpperCase(oJavaEntityField.getName().charAt(0))
                            + oJavaEntityField.getName().substring(1) + "() {\n";
                    getter += "        return " + fieldName + ";\n    }\n";
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="DDGetter">
                    String fieldDD = "\n    public String get" + Character.toUpperCase(oJavaEntityField.getName().charAt(0))
                            + oJavaEntityField.getName().substring(1) + "DD() {\n        return \""
                            + (oJavaEntityField.getDisplayField() != null ? oJavaEntityField.getDisplayField().getName()
                                    : oJavaEntity.getName() + "_" + fieldName) + "\";\n    }\n";
                    //</editor-fold>

                    attribute += fieldAnnotation + field + setter + getter + (!oJavaEntityField.isNotDisplayedField() ? fieldDD : "") + "    // </editor-fold>\n";
                    attributes += "" + attribute;
                    System.out.println("----- Field Created");
                }
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="postChildAnnotation">
            if (!childAnnotation.endsWith("{")) {
                childAnnotation = childAnnotation.substring(0, childAnnotation.length()) + "})";
                imports += "\nimport com.unitedofoq.fabs.core.entitybase.ChildEntity;";
                classAnnotations += childAnnotation;
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Saving Entity">
            String entity = entityHeader + imports + "\n" + classAnnotations + "\n" + className + attributes + "\n}\n";
            System.out.println("INFO:\tEntity " + oJavaEntity.getName() + " Code Is Successfully Created");
            oJavaEntity.setEntityCode(entity);
            oJavaEntity.setStatus("built");
            oem.saveEntity(oJavaEntity, loggedUser);
            //</editor-fold>

            // In Future remove the part that creates files
            //<editor-fold defaultstate="collapsed" desc=".java File Creation">
            String home = System.getProperty("user.home");
            String folder = "" + oJavaEntity.getPackageName().getCode();
            folder = folder.replace('.', '/');
            try {
                new File(home + "/" + folder + "/").mkdirs();
            } catch (Exception ex) {
            }
            File file = new File(home + "/" + folder + "/" + oJavaEntity.getName() + ".java");
            BufferedWriter write = new BufferedWriter(new FileWriter(file));
            write.write(entity);
            write.close();
            file.setExecutable(true);
//            file.setReadOnly();
            //</editor-fold>

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            if (ex instanceof FABSException) {
                oFR.addError(usrMsgService.getUserMessage("ModuleError", loggedUser));
            }
        } finally {
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public OFunctionResult createNewJavaEntities(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        ODataMessage dataMessage = null;
        ODataType dataType = null;
        List<Object> data = null;
        try {
            List<OJavaEntity> loadedOEntity = (List<OJavaEntity>) oem.loadEntityList("OJavaEntity", Collections.singletonList("status != 'built'"), null, null, loggedUser);
            dataType = dtService.loadODataType("OJavaEntity", loggedUser);
            for (OJavaEntity oJavaEntity : loadedOEntity) {
                if (oJavaEntity.isInActive()) {
                    continue;
                }
                data = new ArrayList<Object>();
                data.add(oJavaEntity);
                data.add(loggedUser);
                dataMessage = new ODataMessage(dataType, data);
                createJavaClassFromOJavaEntity(dataMessage, functionParms, loggedUser);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return oFR;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Validations">
    @Override
    public OFunctionResult validateClassName(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = oFunctionService.validateJavaFunctionODM(
                odm, loggedUser, OJavaEntity.class);
        if (inputValidationErrors != null) {
            logger.debug("Returning");
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        String name = null;
        try {
            oem.getEM(loggedUser);
            name = ((OJavaEntity) odm.getData().get(0)).getName();
            name = name.substring(0, 2).toLowerCase() + name.substring(2);
            boolean valid = true;

            //<editor-fold defaultstate="collapsed" desc="isValidName">
            //valid characters, not start with number, _, $
            if (!name.equals("")) {
                for (int i = 0; i < name.length(); i++) {
                    if (name.charAt(i) != '_' && name.charAt(i) != '$' && !Character.isLetter(name.charAt(i))) {
                        oFR.addError(usrMsgService.getUserMessage("InvalidName", loggedUser));
                        valid = false;
                        break;
                    }
                }
            }
            //</editor-fold>

            if (valid) {
                // check if exists in OEntity, OJavaEntity
                OEntity loadedOEntity = loadOEntity(name, loggedUser);
                if (loadedOEntity != null) {
                    OJavaEntity oJavaEntity = (OJavaEntity) oem.loadEntity("OJavaEntity", Collections.singletonList("name like '" + name + "'"), null, loggedUser);
                    if (oJavaEntity != null) {
                        logger.debug("Returning");
                        return oFR;
                    } else {
                        oFR.addError(usrMsgService.getUserMessage("OJavaEntityNameExists", loggedUser));
                    }
                } else {
                    oFR.addError(usrMsgService.getUserMessage("OEntityNameExists", loggedUser));
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Entity: {}", name);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult validateFieldName(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = oFunctionService.validateJavaFunctionODM(
                odm, loggedUser, OJavaEntityField.class);
        if (inputValidationErrors != null) {
            logger.debug("Returning");
            return inputValidationErrors;
        }
        // </editor-fold>
        //valid characters, not start with number, _, $
        OFunctionResult oFR = new OFunctionResult();
        String name = null;
        try {
            OJavaEntityField entityField = (OJavaEntityField) odm.getData().get(0);
            oem.getEM(loggedUser);
            name = entityField.getName();
            name = name.substring(0, 2).toLowerCase() + name.substring(2);
            boolean valid = true;

            //<editor-fold defaultstate="collapsed" desc="isValidName">
            //valid characters, not start with number, _, $
            if (!name.equals("")) {
                for (int i = 0; i < name.length(); i++) {
                    if (name.charAt(i) != '_' && name.charAt(i) != '$' && !Character.isLetter(name.charAt(i))) {
                        oFR.addError(usrMsgService.getUserMessage("InvalidName", loggedUser));
                        valid = false;
                        break;
                    }
                }
            }
            //</editor-fold>

            if (valid) {
                int c = 0;
                List<OJavaEntityField> oJavaEntityFields = entityField.getOJavaEntity().getOJavaEntityFields();
                for (OJavaEntityField oJavaEntityField : oJavaEntityFields) {
                    if (oJavaEntityField.getName().equals(name)) {
                        c++;
                    }
                }
                if (c > 1) {
                    oFR.addError(usrMsgService.getUserMessage("OEntityFieldNameExists", loggedUser));
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Entity: {}", name);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Entering");
            return oFR;
        }
    }

    @Override
    public OFunctionResult preCreateJavaEntity(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            OJavaEntity oJavaEntity = (OJavaEntity) odm.getData().get(0);
            //check that the selected parent is the same type of the parent relation
            if (oJavaEntity.getParentOJavaEntityClass() != null) {
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public OFunctionResult postCreateJavaEntity(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public OFunctionResult preCreateJavaEntityField(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            OJavaEntityField oJavaEntityField = (OJavaEntityField) odm.getData().get(0);
            oJavaEntityField.setOmodule(oJavaEntityField.getOJavaEntity().getOmodule());
            boolean found = false;
            while (oJavaEntityField.getName().length() < 3 || found) {
                oJavaEntityField.setName("o" + oJavaEntityField.getName());
                int c = 0;
                for (int i = 0; i < oJavaEntityField.getOJavaEntity().getOJavaEntityFields().size(); i++) {
                    if (oJavaEntityField.getOJavaEntity().getOJavaEntityFields().get(i).getName().equals(oJavaEntityField.getName())) {
                        c++;
                    }
                }
                if (c > 1) {
                    found = true;
                }
            }
            oJavaEntityField.setName(oJavaEntityField.getName().substring(0, 2).toLowerCase()
                    + oJavaEntityField.getName().substring(2));
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public OFunctionResult postCreateJavaEntityField(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public OFunctionResult preUpdateJavaEntity(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        OJavaEntity oJavaEntity = null;
        try {
            oJavaEntity = (OJavaEntity) odm.getData().get(0);
            OJavaEntity oldJavaEntity = (OJavaEntity) oem.loadEntity("OJavaEntity", oJavaEntity.getDbid(), null, null, loggedUser);

            //<editor-fold defaultstate="collapsed" desc="name or package renaming">
            if (!oJavaEntity.getName().equals(oldJavaEntity.getName())
                    || oJavaEntity.getPackageName().getDbid() != oldJavaEntity.getPackageName().getDbid()) {
                List<OJavaEntityField> loadedOEntity = (List<OJavaEntityField>) oem.loadEntityList("OJavaEntityField",
                        Collections.singletonList("fieldJavaEntity.dbid = " + oJavaEntity.getDbid() + ""), null, null, loggedUser);
                ArrayList<OJavaEntity> useEntity = new ArrayList<OJavaEntity>();
                for (OJavaEntityField oJavaEntityField : loadedOEntity) {
                    if (oJavaEntityField.getFieldJavaEntity() != null) {
                        OJavaEntity temp = oJavaEntityField.getFieldJavaEntity();
                        if (oJavaEntityField.getName().equalsIgnoreCase(oldJavaEntity.getName())) {
                            oJavaEntityField.setName(Character.toLowerCase(oJavaEntity.getName().charAt(0)) + oJavaEntity.getName().substring(1));
                        }
                        oem.saveEntity(temp, loggedUser);
                        if (!useEntity.contains(temp)) {
                            useEntity.add(temp);
                        }
                    }
                }
                for (OJavaEntity oJavaEntity1 : useEntity) {
                    oJavaEntity1.setStatus("new");
                    oem.saveEntity(oJavaEntity1, loggedUser);
                }
            }
            //</editor-fold>

            /**
             * //<editor-fold defaultstate="collapsed" desc="parent-child
             * changes"> if (oJavaEntity.getParentField() != null) { if
             * (oJavaEntity.getParentOJavaEntityClass() != null) {
             *
             * //<editor-fold defaultstate="collapsed" desc="parent field or
             * parent entity changed"> if
             * (oJavaEntity.getParentField().getFieldJavaEntity() == null ||
             * oJavaEntity.getParentField().getFieldJavaEntity().getDbid() !=
             * oJavaEntity.getParentOJavaEntityClass().getDbid()) {
             * OJavaEntityField foundField = null; oem.saveEntity(foundField,
             * loggedUser); oJavaEntity.setParentField(null); // ask for a
             * message or dynamically make it OJavaEntityField parentField =
             * null; parentField = new OJavaEntityField(); String parentName =
             * oJavaEntity.getName(); parentName = parentName.substring(0,
             * 2).toLowerCase() + parentName.substring(2); UDCType fT = null;
             * UDC fieldType = null, oneToOne = null, oneToMany = null;
             *
             * //<editor-fold defaultstate="collapsed" desc="relation">
             * List<String> conditions = new ArrayList<String>();
             * conditions.add("code like 'fieldRelation'"); fT = (UDCType)
             * oem.loadEntity("UDCType", conditions, null, loggedUser);
             * List<UDC> udcs = null; udcs = fT.getChildUDCs(); for (UDC udc :
             * udcs) { if (udc.getCode().equals("OneToOne")) { oneToOne = udc; }
             * if (udc.getCode().equals("OneToMany")) { oneToMany = udc; } }
             * //</editor-fold>
             *
             * //<editor-fold defaultstate="collapsed" desc="fieldType">
             * conditions = new ArrayList<String>(); conditions.add("code like
             * 'fieldType'"); fT = (UDCType) oem.loadEntity("UDCType",
             * conditions, null, loggedUser); udcs = null; udcs =
             * fT.getChildUDCs(); for (UDC udc : udcs) { if
             * (udc.getCode().equals("entity")) { fieldType = udc; break; } }
             * //</editor-fold>
             *
             * parentField.setName(parentName); parentField.setChildField(true);
             * parentField.setFieldJavaEntity(oJavaEntity);
             * parentField.setFieldType(fieldType);
             * parentField.setFieldRelation(oJavaEntity.isMultiplicityRegardParent()
             * ? oneToMany : oneToOne);
             * parentField.setOJavaEntity(oJavaEntity.getParentOJavaEntityClass());
             * parentField.setOmodule(oJavaEntity.getParentOJavaEntityClass().getOmodule());
             * oem.saveEntity(parentField, loggedUser); } //</editor-fold>
             *
             * //<editor-fold defaultstate="collapsed" desc="relaton Checking">
             * else { String
             * code=oJavaEntity.getParentField().getFieldRelation().getCode();
             * if(code.startsWith("One") &&
             * oJavaEntity.isMultiplicityRegardParent()){ UDCType
             * fr=oJavaEntity.getParentField().getFieldRelation().getType();
             * List<UDC> children = fr.getChildUDCs(); for (UDC udc : children)
             * { if(udc.getCode().equals("ManyToOne")){
             * oJavaEntity.getParentField().setFieldRelation(udc); break; } } }
             * oem.saveEntity(oJavaEntity.getParentField(), loggedUser); }
             * //</editor-fold>
             *
             * }
             * //<editor-fold defaultstate="collapsed" desc="parent entity
             * removed"> else { oJavaEntity.setParentField(null); }
             * //</editor-fold> }
             */
            //</editor-fold>
            oJavaEntity.setStatus("new");
        } catch (Exception ex) {
            if (ex instanceof FABSException) {
                oFR.addError(usrMsgService.getUserMessage("ModuleError", loggedUser));
            }
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public OFunctionResult postUpdateJavaEntity(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        OJavaEntity oJavaEntity = null;
        try {
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public OFunctionResult preUpdateJavaEntityField(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            OJavaEntityField oJavaEntityField = (OJavaEntityField) odm.getData().get(0);
            OJavaEntityField oldJavaEntityField = (OJavaEntityField) oem.loadEntity("OJavaEntityField",
                    oJavaEntityField.getDbid(), null, null, loggedUser);
            if (!oldJavaEntityField.getName().equals(oJavaEntityField.getName())) {
                int replace = 0;
                if (Character.isUpperCase(oJavaEntityField.getName().charAt(0))) {
                    replace = 1;
                }
                if (Character.isUpperCase(oJavaEntityField.getName().charAt(1))) {
                    replace = 2;
                }
                if (replace != 0) {
                    oJavaEntityField.setName(oJavaEntityField.getName().substring(0, replace).toLowerCase()
                            + oJavaEntityField.getName().substring(replace));
                }
            }
            // deny update parent field if FieldOJavaEntiy changed or parentOJavaEntityClass
            if (oJavaEntityField.isChildField()) {
                if (oldJavaEntityField.getFieldJavaEntity().getDbid()
                        != oJavaEntityField.getFieldJavaEntity().getDbid()
                        || oJavaEntityField.getFieldType().getDbid()
                        != oldJavaEntityField.getFieldType().getDbid()
                        || oJavaEntityField.getFieldRelation().getDbid()
                        != oldJavaEntityField.getFieldRelation().getDbid()) {
                    oJavaEntityField = oldJavaEntityField;
                }
            }
            if (oJavaEntityField.getMappedBy() != null) {
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public OFunctionResult preRemoveJavaEntityField(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public OFunctionResult postRemoveJavaEntity(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        OJavaEntity oJavaEntity = null;
        try {
            oJavaEntity = (OJavaEntity) odm.getData().get(0);
            List<OJavaEntityField> loadedFields = oJavaEntity.getOJavaEntityFields();
            for (OJavaEntityField oJavaEntityField : loadedFields) {
                if (oJavaEntityField.isChildField()) {
                    OJavaEntity entityToFix = oJavaEntityField.getFieldJavaEntity();
                    if (entityToFix != null) {
                        entityToFix.setParentOJavaEntityClass(null);
                        entityToFix.setParentField(null);
                        oem.saveEntity(entityToFix, loggedUser);
                    }
                }
                oem.deleteEntity(oJavaEntityField, loggedUser);
            }
            loadedFields = (List<OJavaEntityField>) oem.loadEntityList("OJavaEntityField",
                    Collections.singletonList("fieldJavaEntity_dbid = " + oJavaEntity.getDbid()), null, null, loggedUser);
            for (OJavaEntityField oJavaEntityField : loadedFields) {
                oem.deleteEntity(oJavaEntityField, loggedUser);
            }
            List<OJavaEntity> loadedentity = (List<OJavaEntity>) oem.loadEntityList("OJavaEntity",
                    Collections.singletonList("superOJavaEntityClass_dbid = " + oJavaEntity.getDbid()), null, null, loggedUser);
            for (OJavaEntity oJavaEntity1 : loadedentity) {
                oJavaEntity1.setStatus("new");
                oJavaEntity1.setSuperOJavaEntityClass(null);
                oem.saveEntity(oJavaEntity1, loggedUser);
            }
            loadedentity = (List<OJavaEntity>) oem.loadEntityList("OJavaEntity",
                    Collections.singletonList("parentOJavaEntityClass_dbid = " + oJavaEntity.getDbid()), null, null, loggedUser);
            for (OJavaEntity oJavaEntity1 : loadedentity) {
                oJavaEntity1.setStatus("new");
                oJavaEntity1.setParentOJavaEntityClass(null);
                oem.saveEntity(oJavaEntity1, loggedUser);
            }
            oem.deleteEntity(oJavaEntity, loggedUser);
        } catch (Exception ex) {
            if (ex instanceof FABSException) {
                oFR.addError(usrMsgService.getUserMessage("ModuleError", loggedUser));
            }
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public OFunctionResult postRemoveJavaEntityField(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        OJavaEntityField oJavaEntityField = null;
        try {
            oJavaEntityField = (OJavaEntityField) odm.getData().get(0);
            oem.deleteEntity(oJavaEntityField, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return oFR;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Build Create SQL Command">
    //FIXME: Commented from version r18.1.7 as there's no usage
//    @Override
//    public OFunctionResult buildCreateSQLCommand(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
//        logger.debug("Entering");
//        OFunctionResult oFR = new OFunctionResult();
//        try {
//            OJavaEntity entity = (OJavaEntity) odm.getData().get(0);
//            System.out.println("Building creating SQL Command for " + entity.getName());
//            String command = "";
//            command = " CREATE TABLE `" + entity.getName().toLowerCase() + "` (";
//            int dbidLength = DBIDCacheService.MAX_DBID_LENGTH + 1;
//            String columns = "";
//            List<OJavaEntityField> fields = entity.getOJavaEntityFields();
//            for (OJavaEntityField oJavaEntityField : fields) {
//                if (oJavaEntityField.isInActive()) {
//                    continue;
//                }
//                String type = oJavaEntityField.getFieldType().getCode();
//                if (oJavaEntityField.getFieldRelation() != null
//                        && oJavaEntityField.getFieldRelation().getCode().equals("OneToMany")) {
//                    continue;
//                }
//                String fieldName = oJavaEntityField.getName().toUpperCase();
//                if (type.equals("entity")) {
//                    columns += "\n  `" + fieldName + "_DBID` int(" + dbidLength + ") DEFAULT NULL,";
//                } else {
//                    if (type.equals("text")) {
//                        columns += "\n  `" + fieldName + "` TEXT COLLATE utf8_bin DEFAULT NULL,";
//                    } else if (type.equals("boolean")) {
//                        columns += "\n  `" + fieldName + "` smallint(5) DEFAULT NULL,";
//                    } else if (type.equals("Date") || type.equals("TimeStamp")) {
//                        columns += "\n  `" + fieldName + "` timestamp NULL DEFAULT NULL,";
//                    } else if (type.equals("String")) {
//                        columns += "\n  `" + fieldName + "` varchar(255) COLLATE utf8_bin DEFAULT NULL,";
//                    } else if (type.equals("Byte[]")) {
//                        columns += "\n  `" + fieldName + "` BLOB DEFAULT NULL,";
//                    } else if (type.equals("int")) {
//                        columns += "\n  `" + fieldName + "` int(" + dbidLength + ") DEFAULT NULL,";
//                    } else if (type.equals("long")) {
//                        columns += "\n  `" + fieldName + "` int(" + dbidLength + ") DEFAULT NULL,";
//                    } else if (type.equals("BigDecimal")) {
//                        columns += "\n  `" + fieldName + "` DECIMAL DEFAULT NULL,";
//                    } else if (type.equals("float")) {
//                        columns += "\n  `" + fieldName + "` FLOAT DEFAULT NULL,";
//                    } else if (type.equals("double")) {
//                        columns += "\n  `" + fieldName + "` DOUBLE DEFAULT NULL,";
//                    } else if (type.equals("char")) {
//                        columns += "\n  `" + fieldName + "` CHAR DEFAULT NULL,";
//                    } else if (type.equals("mText")) {
//                        columns += "\n  `" + fieldName + "` MEDIUMTEXT COLLATE utf8_bin DEFAULT NULL,";
//                    } else if (type.equals("lText")) {
//                        columns += "\n  `" + fieldName + "` LONGTEXT COLLATE utf8_bin DEFAULT NULL,";
//                    } else {
//                        columns += "\n  `" + fieldName + "` varchar(255) COLLATE utf8_bin DEFAULT NULL,";
//                    }
//                }
//            }
//            if (entity.getSuperOJavaEntityClass() != null) {
//                String fromSuper = entity.getSuperOJavaEntityClass().getCreateSQLCommand();
//                if (fromSuper != null) {
//                    fromSuper = fromSuper.substring(fromSuper.indexOf("(" + 1));
//                    command += columns + fromSuper;
//                } else {
//                    logger.debug("Super Entity Was Not Build");
//                }
//            } else {
//                String addedFromBaseEntity = "\n  `DBID` int(" + dbidLength + ") NOT NULL,"
//                        + "\n  `ACTIVE` smallint(5) DEFAULT NULL,"
//                        + "\n  `DELETED` smallint(5) DEFAULT NULL,"
//                        + "\n  `CC1_DBID` int(" + dbidLength + ") DEFAULT NULL,"
//                        + "\n  `CC2_DBID` int(" + dbidLength + ") DEFAULT NULL,"
//                        + "\n  `CC3_DBID` int(" + dbidLength + ") DEFAULT NULL,"
//                        + "\n  `CC4_DBID` int(" + dbidLength + ") DEFAULT NULL,"
//                        + "\n  `CC5_DBID` int(" + dbidLength + ") DEFAULT NULL,"
//                        + "\n  `LASTMAINTUSER_DBID` int(" + dbidLength + ") DEFAULT NULL,"
//                        + "\n  `LASTMAINTDTIME` timestamp NULL DEFAULT NULL,"
//                        + "\n  `REQUESTNOTE` varchar(255) COLLATE utf8_bin DEFAULT NULL,"
//                        + "\n  `CUSTOM1` varchar(255) COLLATE utf8_bin DEFAULT NULL,"
//                        + "\n  `CUSTOM2` varchar(255) COLLATE utf8_bin DEFAULT NULL,"
//                        + "\n  `CUSTOM3` varchar(255) COLLATE utf8_bin DEFAULT NULL,"
//                        + "\n  `ENTITYSTATUS_DBID` int(" + dbidLength + ") DEFAULT NULL,"
//                        + "\n  PRIMARY KEY (`DBID`),"
//                        + "\n  UNIQUE KEY `OJAVAENTITYREF_PK` (`DBID`)"
//                        + "\n ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;";
//                command += columns + addedFromBaseEntity;
//            }
//            entity.setCreateSQLCommand(command);
//            oem.saveEntity(entity, loggedUser);
//
//            // In Future remove the part that creates files
//            //<editor-fold defaultstate="collapsed" desc=".sql file save">
//            String home = System.getProperty("user.home");
//            String folder = "" + entity.getPackageName().getCode();
//            folder = folder.replace('.', '/');
//            try {
//                new File(home + "/" + folder + "/").mkdirs();
//            } catch (Exception ex) {
//                logger.error("Exception thrown", ex);
//            }
//            File file = new File(home + "/" + folder + "/" + entity.getName() + ".sql");
//            BufferedWriter write = new BufferedWriter(new FileWriter(file));
//            write.write(command);
//            write.close();
//            file.setExecutable(true);
//            //</editor-fold>
//
//        } catch (Exception ex) {
//            logger.error("Exception thrown", ex);
//            if (ex instanceof FABSException) {
//                oFR.addError(usrMsgService.getUserMessage("ModuleError", loggedUser));
//            }
//        }
//        logger.debug("Returning");
//        return oFR;
//    }
    //FIXME: Commented from version r18.1.7 as there's no usage
//    @Override
//    public OFunctionResult buildAllCreateSQLCommands(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
//        logger.debug("Entering");
//        OFunctionResult oFR = new OFunctionResult();
//        ODataMessage dataMessage = null;
//        ODataType dataType = null;
//        List<Object> data = null;
//        try {
//            List<OJavaEntity> loadedOEntity = (List<OJavaEntity>) oem.loadEntityList("OJavaEntity", null, null, null, loggedUser);
//            dataType = dtService.loadODataType("OJavaEntity", loggedUser);
//            for (OJavaEntity oJavaEntity : loadedOEntity) {
//                if (oJavaEntity.isInActive()) {
//                    continue;
//                }
//                data = new ArrayList<Object>();
//                data.add(oJavaEntity);
//                data.add(loggedUser);
//                dataMessage = new ODataMessage(dataType, data);
//                buildCreateSQLCommand(dataMessage, functionParms, loggedUser);
//            }
//        } catch (Exception ex) {
//            logger.error("Exception thrown", ex);
//        }
//        logger.debug("Returning");
//        return oFR;
//    }
    @Override
    public OFunctionResult runCreateSQLCommand(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            OJavaEntity entity = (OJavaEntity) odm.getData().get(0);
            String command = entity.getCreateSQLCommand();
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public OFunctionResult runAllCreateSQLCommand(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        ODataMessage dataMessage = null;
        ODataType dataType = null;
        List<Object> data = null;
        try {
            List<OJavaEntity> loadedOEntity = (List<OJavaEntity>) oem.loadEntityList("OJavaEntity", null, null, null, loggedUser);
            dataType = dtService.loadODataType("OJavaEntity", loggedUser);
            for (OJavaEntity oJavaEntity : loadedOEntity) {
                if (oJavaEntity.isInActive()) {
                    continue;
                }
                data = new ArrayList<Object>();
                data.add(oJavaEntity);
                data.add(loggedUser);
                dataMessage = new ODataMessage(dataType, data);
                runCreateSQLCommand(dataMessage, functionParms, loggedUser);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return oFR;
    }

    //FIXME: Commented from version r18.1.7 as there's no usage
//    @Override
//    public OFunctionResult buildNewCreateSQLCommands(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
//        logger.debug("Entering");
//        OFunctionResult oFR = new OFunctionResult();
//        ODataMessage dataMessage = null;
//        ODataType dataType = null;
//        List<Object> data = null;
//        try {
//            List<OJavaEntity> loadedOEntity = (List<OJavaEntity>) oem.loadEntityList("OJavaEntity", Collections.singletonList("status != 'built'"), null, null, loggedUser);
//            dataType = dtService.loadODataType("OJavaEntity", loggedUser);
//            for (OJavaEntity oJavaEntity : loadedOEntity) {
//                if (oJavaEntity.isInActive()) {
//                    continue;
//                }
//                data = new ArrayList<Object>();
//                data.add(oJavaEntity);
//                data.add(loggedUser);
//                dataMessage = new ODataMessage(dataType, data);
//                buildCreateSQLCommand(dataMessage, functionParms, loggedUser);
//            }
//        } catch (Exception ex) {
//            logger.error("Exception thrown", ex);
//        }
//        logger.debug("Returning");
//        return oFR;
//    }
    @Override
    public OFunctionResult runNewCreateSQLCommand(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        ODataMessage dataMessage = null;
        ODataType dataType = null;
        List<Object> data = null;
        try {
            List<OJavaEntity> loadedOEntity = (List<OJavaEntity>) oem.loadEntityList("OJavaEntity", Collections.singletonList("status != 'built'"), null, null, loggedUser);
            dataType = dtService.loadODataType("OJavaEntity", loggedUser);
            for (OJavaEntity oJavaEntity : loadedOEntity) {
                if (oJavaEntity.isInActive()) {
                    continue;
                }
                data = new ArrayList<Object>();
                data.add(oJavaEntity);
                data.add(loggedUser);
                dataMessage = new ODataMessage(dataType, data);
                runCreateSQLCommand(dataMessage, functionParms, loggedUser);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return oFR;
    }
    //</editor-fold>

    //</editor-fold>
    /**
     * Create OEntity Access Privilege
     *
     * @param odm of OEntity
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult createOEntityAccessPrivilege(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OEntity.class);
        if (inputValidationErrors != null) {
            logger.debug("Returning");
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        OEntity oEntity = (OEntity) odm.getData().get(0);
        try {
            ObjectAccessPrivilege oap = new ObjectAccessPrivilege();
            oap.setAccessedOEntity(oEntity);
            oap.setName(oEntity.getEntityClassName());
            oap.setDescription(oEntity.getTitle() + " Entity Access");
            oFR.append(this.callEntityCreateAction(oap, oem.getSystemUser(loggedUser)));

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Cache">
    @Override
    public OFunctionResult onOEntityCachedUpdate(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            OEntity entity = (OEntity) odm.getData().get(0);
            cachedOEntity.put(loggedUser.getTenant().getId() + "_" + entity.getEntityClassName(), entity);
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.debug("OEnity Updated In Cache. Entity: {}", entity);
            // </editor-fold>
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult onOEntityDTOCachedUpdate(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            OEntity entity = (OEntity) odm.getData().get(0);
            OEntityDTO entityDTO = loadOEntityDTO(entity.getEntityClassPath(), loggedUser);
            cachedOEntityDTO.put(loggedUser.getTenant().getId() + "_"
                    + entityDTO.getEntityClassName(), entityDTO);
            // <editor-fold defaultstate="collapsed" desc="Log">
            if (null != entityDTO) {
                logger.debug("OEnity Updated In Cache. Entity: {}", entityDTO.getEntityClassName());
            }
            // </editor-fold>
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult preOEntityCachedUpdate(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            OEntity entity = (OEntity) odm.getData().get(0);
            String name = ((OEntity) oem.loadEntity("OEntity", entity.getDbid(), null, null, loggedUser)).getEntityClassName();
            if (!name.equals(entity.getEntityClassName())) {
                cachedOEntity.remove(loggedUser.getTenant().getId() + "_" + name);
                // <editor-fold defaultstate="collapsed" desc="Log">
                if (null != entity) {
                    logger.debug("OEnity Removed From Cache. Entity: {}", entity.getClassName());
                }
                // </editor-fold>
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult preOEntityDTOCachedUpdate(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            OEntityDTO entity = (OEntityDTO) loadOEntityDTOByDBID(((OEntity) odm.getData().get(0)).getDbid(),
                    loggedUser);
            String name = ((OEntityDTO) loadOEntityDTOByDBID(entity.getDbid(),
                    loggedUser)).getEntityClassName();
            if (!name.equals(entity.getEntityClassName())) {
                cachedOEntityDTO.remove(loggedUser.getTenant().getId() + "_" + name);
                // <editor-fold defaultstate="collapsed" desc="Log">
                if (null != entity) {
                    logger.debug("OEnity Removed From Cache. Entity: {}", entity.getEntityClassName());
                }
                // </editor-fold>
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult onOEntityCachedRemove(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            OEntity entity = (OEntity) odm.getData().get(0);
            cachedOEntity.remove(loggedUser.getTenant().getId() + "_" + entity.getEntityClassName());
            // <editor-fold defaultstate="collapsed" desc="Log">
            if (null != entity) {
                logger.debug("OEnity Removed From Cache. Entity: {}", entity.getClassName());
            }
            // </editor-fold>
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult onOEntityDTOCachedRemove(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            OEntityDTO entity = (OEntityDTO) odm.getData().get(0);
            cachedOEntityDTO.remove(loggedUser.getTenant().getId() + "_"
                    + entity.getEntityClassName());
            // <editor-fold defaultstate="collapsed" desc="Log">
            if (entity != null) {
                logger.debug("OEnity Removed From Cache. Entity: {}", entity.getClass());
            }
            // </editor-fold>
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    /**
     * Clears {@link #cachedOEntity}
     */
    @Override
    public OFunctionResult clearOEntityCache(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            cachedOEntity.clear();
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.debug("OEnity Cache Cleared");
            // </editor-fold>
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult clearOEntityDTOCache(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            cachedOEntityDTO.clear();
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.debug("OEnity Cache Cleared");
            // </editor-fold>
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }
    //</editor-fold>

    /**
     * Return list of parents tree (class paths) of OEntity in which our OEntity
     * (of 'entityClassPath') is defined as child. Tree is got using {@link EntityBaseService#getEntityParentsTree(java.lang.String)
     * }. Relationship is got using {@link OEntityReference#isRefChild()}
     *
     * @param entityClassPath
     * @param loggedUser
     * @return List of parents tree is returned in
     * {@link OFunctionResult#returnValues} first element as List; or empty in
     * case not found or error (messages added)
     */
    @Override
    public OFunctionResult getParentsFromReference(String entityClassPath, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        oFR.setReturnValues(new ArrayList());
        try {
            if (entityClassPath == null || "".equals(entityClassPath)) {
                logger.debug("Returning");
                return oFR;
            }
            OEntity oentity = loadOEntity(entityClassPath, oem.getSystemUser(loggedUser));
            if (oentity == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("Entity Not Found. Entity Class Path: {}", entityClassPath);
                // </editor-fold>
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
                logger.debug("Entering");
                return oFR;
            }
            List<OEntityReference> referencesInDB = null;
            // <editor-fold defaultstate="collapsed" desc="Get all references, where I'm child">
            try {
                ArrayList<String> conds = new ArrayList<String>();
                conds.add("refOEntity.dbid = " + oentity.getDbid());
                conds.add("refChild=1");
                referencesInDB = oem.loadEntityList(
                        OEntityReference.class.getSimpleName(),
                        conds, null, null, oem.getSystemUser(loggedUser));
            } catch (Exception ex) {
                if (ex instanceof NoResultException) {
                    // No problem with that
                    logger.debug("Entering");
                    return oFR;
                } else {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.error("Exception thrown", ex);
                    logger.trace("EntityClassPath: {}", entityClassPath);
                    // </editor-fold>
                    oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
                    logger.debug("Entering");
                    return oFR;
                }
            }
            // </editor-fold>

            ArrayList<String> parentsTree = new ArrayList<String>();
            for (OEntityReference iamChildRef : referencesInDB) {
                parentsTree.add(iamChildRef.getOwnerOEntity().getEntityClassPath());
                parentsTree.addAll(
                        entityBaseService.getEntityParentsTree(
                                iamChildRef.getOwnerOEntity().getEntityClassPath()));
            }
            oFR.setReturnValues(parentsTree);

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public void runServerJob(OModule module, UDC notificationType, ServerJob serverJob, Method functionMethod, Object javaFunctionClsObj, OUser loggedUser, Object[] args) {
        logger.debug("Entering");
        try {
            // Load ServerJob from DB
            UDC inProgessUDC = (UDC) oem.loadEntity("UDC", Collections.singletonList("code = 'JobInProgress'"), null, loggedUser);
            if (inProgessUDC == null) {
                logger.warn("JobInProgress UDC is not found in DB");
                logger.debug("Returning");
                return;
            }

            serverJob.setStatus(inProgessUDC);
            serverJob = (ServerJob) oem.saveEntity(serverJob, loggedUser);

            // Invoke ServerJob's Function
            OFunctionResult oFR = (OFunctionResult) functionMethod.invoke(javaFunctionClsObj, args);

            // set ServerJob's status to 'Completed'
            UDC completedUDC = (UDC) oem.loadEntity("UDC", Collections.singletonList("code = 'JobCompleted'"), null, loggedUser);

            if (completedUDC == null) {
                logger.warn("completedUDC UDC is not found in DB");
                logger.debug("Returning");
                return;
            }

            serverJob.setStatus(completedUDC);
            Calendar calendar = Calendar.getInstance();
            Timestamp currentTimestamp = new Timestamp(calendar.getTime().getTime());
            serverJob.setFinishTime(currentTimestamp);

            //Prepare to send notification to user with job result
            String notificationBody = "", notificationTitle = "";
            if (null != notificationType) {
                List<String> msgParams = new ArrayList<String>();
                msgParams.add(serverJob.getLoggedUser().getLoginName());
                msgParams.add(serverJob.getCode());
                UserMessage notificationMessage = usrMsgService.getUserMessage(
                        "ServerJobGnrlMsg", msgParams, loggedUser);
                notificationTitle += notificationMessage.getMessageTextTranslated();
                notificationBody = notificationMessage.getMessageTextTranslated() + "\n";
            }
            // Finished processing the function:
            // Set the returned OFunctionsResult in ServerJobMessages
            List<ServerJobMessage> serverJobMessages = new ArrayList<ServerJobMessage>();
            int sequence = 0;

            List<UserMessage> errorMessages = oFR.getErrors();
            if (!errorMessages.isEmpty()) {
                notificationBody += "With the following error messages:" + "\n";
            }
            for (UserMessage errorMsg : errorMessages) {
                sequence++;
                ServerJobMessage serverJobMessage = new ServerJobMessage();
                serverJobMessage.setMessageName(errorMsg.getName());
                serverJobMessage.setMessageTitle(errorMsg.getMessageTitle());
                serverJobMessage.setMessageText(errorMsg.getMessageText());
                serverJobMessage.setSequence(sequence);
                serverJobMessage.setMessageType("Error");
                serverJobMessage.setServerJob(serverJob);
                serverJobMessages.add(serverJobMessage);
                notificationBody += errorMsg.getMessageTitleTranslated() + ": "
                        + errorMsg.getMessageTextTranslated() + "\n";
            }

            List<UserMessage> warningMessages = oFR.getWarnings();
            if (!warningMessages.isEmpty()) {
                notificationBody += "With the following warning messages:" + "\n";
            }
            for (UserMessage warningMsg : warningMessages) {
                sequence++;
                ServerJobMessage serverJobMessage = new ServerJobMessage();
                serverJobMessage.setMessageName(warningMsg.getName());
                serverJobMessage.setMessageTitle(warningMsg.getMessageTitle());
                serverJobMessage.setMessageText(warningMsg.getMessageText());
                serverJobMessage.setSequence(sequence);
                serverJobMessage.setMessageType("Warning");
                serverJobMessage.setServerJob(serverJob);
                serverJobMessages.add(serverJobMessage);
                notificationBody += warningMsg.getMessageTitleTranslated() + ": "
                        + warningMsg.getMessageTextTranslated() + "\n";
            }
            List<UserMessage> successMessages = oFR.getSuccesses();
            if (!successMessages.isEmpty()) {
                notificationBody += "With the following success messages:" + "\n";
            }
            for (UserMessage successMsg : successMessages) {
                sequence++;
                ServerJobMessage serverJobMessage = new ServerJobMessage();
                serverJobMessage.setMessageName(successMsg.getName());
                serverJobMessage.setMessageTitle(successMsg.getMessageTitle());
                serverJobMessage.setMessageText(successMsg.getMessageText());
                serverJobMessage.setSequence(sequence);
                serverJobMessage.setMessageType("Success");
                serverJobMessage.setServerJob(serverJob);
                serverJobMessages.add(serverJobMessage);
                notificationBody += successMsg.getMessageTitleTranslated() + ": "
                        + successMsg.getMessageTextTranslated() + "\n";
            }
            //Send notifications if any
            if (null != notificationType) {
                if (notificationType.getDbid() == ServerJob.INBOX) {
                    ServerJobInbox inbox = new ServerJobInbox();
                    inbox.setDoneDate(DateFormat.getDateTimeInstance(DateFormat.DEFAULT,
                            DateFormat.FULL).format(serverJob.getFinishTime()));
                    inbox.setText(notificationBody);
                    inbox.setTitle(notificationBody);
                    inbox.setUserLoginName(serverJob.getLoggedUser().getLoginName());
                    inbox.setOmodule(module);
                    oem.saveEntity(inbox, loggedUser);
                } else if (notificationType.getDbid() == ServerJob.EMAIL) {
                    emailSenderLocal.sendEMail("ofoqfabs@gmail.com",
                            loggedUser.getEmail(), notificationTitle, notificationBody);
                } else if (notificationType.getDbid() == ServerJob.SMS) {
                    smsSenderLocal.sendSMS(notificationBody, serverJob.getLoggedUser().getLoginName(),
                            null, notificationTitle, null, 0, loggedUser);
                }
            }
            // Sort the messages
            Comparator msgComparator = new Comparator() {
                @Override
                public int compare(Object o1, Object o2) {
                    ServerJobMessage msg1 = (ServerJobMessage) o1;
                    ServerJobMessage msg2 = (ServerJobMessage) o2;
                    Integer level1index = msg1.getSequence();
                    Integer level2index = msg2.getSequence();
                    return level1index.compareTo(level2index);
                }
            };
            Collections.sort(serverJobMessages, msgComparator);
            // Set ServerMessages in ServerJob
            serverJob.setServerJobMessages(serverJobMessages);
            oFR.append(this.callEntityUpdateAction(serverJob, loggedUser));
        } catch (Exception ex) {
            if (serverJob != null) {
                logger.debug("ServerJob Function name: {}", serverJob.getFunctionName());
                logger.error("Exception thrown", ex);
            } else {
                logger.error("Exception thrown", ex);
            }
        }
    }

    @Override
    public void runServerJob(long moduleDBID, long notificationTypeDBID, ServerJob serverJob, Method functionMethod, Object javaFunctionClsObj, OUser loggedUser, Object[] args) {
        logger.debug("Entering");
        try {
            // Load ServerJob from DB
            UDC inProgessUDC = (UDC) oem.loadEntity("UDC", Collections.singletonList("code = 'JobInProgress'"), null, loggedUser);
            if (inProgessUDC == null) {
                logger.warn("JobInProgress UDC is not found in DB");
                logger.debug("Returning");
                return;
            }

            serverJob.setStatus(inProgessUDC);
            serverJob = (ServerJob) oem.saveEntity(serverJob, loggedUser);

            // Invoke ServerJob's Function
            OFunctionResult oFR = (OFunctionResult) functionMethod.invoke(javaFunctionClsObj, args);

            // set ServerJob's status to 'Completed'
            UDC completedUDC = (UDC) oem.loadEntity("UDC", Collections.singletonList("code = 'JobCompleted'"), null, loggedUser);

            if (completedUDC == null) {
                logger.warn("completedUDC UDC is not found in DB");
                logger.debug("Returning");
                return;
            }

            serverJob.setStatus(completedUDC);
            Calendar calendar = Calendar.getInstance();
            Timestamp currentTimestamp = new Timestamp(calendar.getTime().getTime());
            serverJob.setFinishTime(currentTimestamp);

            //Prepare to send notification to user with job result
            String notificationBody = "", notificationTitle = "";
            if (0 != notificationTypeDBID) {
                List<String> msgParams = new ArrayList<String>();
                msgParams.add(serverJob.getLoggedUser().getLoginName());
                msgParams.add(serverJob.getCode());
                UserMessage notificationMessage = usrMsgService.getUserMessage(
                        "ServerJobGnrlMsg", msgParams, loggedUser);
                notificationTitle += notificationMessage.getMessageTextTranslated();
                notificationBody = notificationMessage.getMessageTextTranslated() + "\n";
            }
            // Finished processing the function:
            // Set the returned OFunctionsResult in ServerJobMessages
            List<ServerJobMessage> serverJobMessages = new ArrayList<ServerJobMessage>();
            int sequence = 0;

            List<UserMessage> errorMessages = oFR.getErrors();
            if (!errorMessages.isEmpty()) {
                notificationBody += "With the following error messages:" + "\n";
            }
            for (UserMessage errorMsg : errorMessages) {
                sequence++;
                ServerJobMessage serverJobMessage = new ServerJobMessage();
                serverJobMessage.setMessageName(errorMsg.getName());
                serverJobMessage.setMessageTitle(errorMsg.getMessageTitle());
                serverJobMessage.setMessageText(errorMsg.getMessageText());
                serverJobMessage.setSequence(sequence);
                serverJobMessage.setMessageType("Error");
                serverJobMessage.setServerJob(serverJob);
                serverJobMessages.add(serverJobMessage);
                notificationBody += errorMsg.getMessageTitleTranslated() + ": "
                        + errorMsg.getMessageTextTranslated() + "\n";
            }

            List<UserMessage> warningMessages = oFR.getWarnings();
            if (!warningMessages.isEmpty()) {
                notificationBody += "With the following warning messages:" + "\n";
            }
            for (UserMessage warningMsg : warningMessages) {
                sequence++;
                ServerJobMessage serverJobMessage = new ServerJobMessage();
                serverJobMessage.setMessageName(warningMsg.getName());
                serverJobMessage.setMessageTitle(warningMsg.getMessageTitle());
                serverJobMessage.setMessageText(warningMsg.getMessageText());
                serverJobMessage.setSequence(sequence);
                serverJobMessage.setMessageType("Warning");
                serverJobMessage.setServerJob(serverJob);
                serverJobMessages.add(serverJobMessage);
                notificationBody += warningMsg.getMessageTitleTranslated() + ": "
                        + warningMsg.getMessageTextTranslated() + "\n";
            }
            List<UserMessage> successMessages = oFR.getSuccesses();
            if (!successMessages.isEmpty()) {
                notificationBody += "With the following success messages:" + "\n";
            }
            for (UserMessage successMsg : successMessages) {
                sequence++;
                ServerJobMessage serverJobMessage = new ServerJobMessage();
                serverJobMessage.setMessageName(successMsg.getName());
                serverJobMessage.setMessageTitle(successMsg.getMessageTitle());
                serverJobMessage.setMessageText(successMsg.getMessageText());
                serverJobMessage.setSequence(sequence);
                serverJobMessage.setMessageType("Success");
                serverJobMessage.setServerJob(serverJob);
                serverJobMessages.add(serverJobMessage);
                notificationBody += successMsg.getMessageTitleTranslated() + ": "
                        + successMsg.getMessageTextTranslated() + "\n";
            }
            //Send notifications if any
            if (0 != notificationTypeDBID) {
                if (notificationTypeDBID == ServerJob.INBOX) {
                    ServerJobInbox inbox = new ServerJobInbox();
                    inbox.setDoneDate(DateFormat.getDateTimeInstance(DateFormat.DEFAULT,
                            DateFormat.FULL).format(serverJob.getFinishTime()));
                    inbox.setText(notificationBody);
                    inbox.setTitle(notificationBody);
                    inbox.setUserLoginName(serverJob.getLoggedUser().getLoginName());
                    inbox.setOmodule((OModule) oem.loadEntity(OModule.class.getSimpleName(), moduleDBID, null, null, loggedUser));
                    oem.saveEntity(inbox, loggedUser);
                } else if (notificationTypeDBID == ServerJob.EMAIL) {
                    emailSenderLocal.sendEMail("ofoqfabs@gmail.com",
                            loggedUser.getEmail(), notificationTitle, notificationBody);
                } else if (notificationTypeDBID == ServerJob.SMS) {
                    smsSenderLocal.sendSMS(notificationBody, serverJob.getLoggedUser().getLoginName(),
                            null, notificationTitle, null, 0, loggedUser);
                }
            }
            // Sort the messages
            Comparator msgComparator = new Comparator() {
                @Override
                public int compare(Object o1, Object o2) {
                    ServerJobMessage msg1 = (ServerJobMessage) o1;
                    ServerJobMessage msg2 = (ServerJobMessage) o2;
                    Integer level1index = msg1.getSequence();
                    Integer level2index = msg2.getSequence();
                    logger.debug("Returning with: {} ", level1index.compareTo(level2index));
                    return level1index.compareTo(level2index);
                }
            };
            Collections.sort(serverJobMessages, msgComparator);
            // Set ServerMessages in ServerJob
            serverJob.setServerJobMessages(serverJobMessages);
            oFR.append(this.callEntityUpdateAction(serverJob, loggedUser));
        } catch (Exception ex) {
            if (serverJob != null) {
                logger.debug("ServerJob Function name: {}", serverJob.getFunctionName());
                logger.error("Exception thrown", ex);
            } else {
                logger.error("Exception thrown", ex);
            }
        }
        logger.debug("Returning");
    }

    @Override
    public boolean checkPrivilegeAuthorithy(String privilege, OUser loggedUser) {
        logger.debug("Entering");
        boolean authorized = true;
        try {
            securityService.checkPrivilegeAuthorithy("ScreenTranslationOnSpot", loggedUser,
                    oem.getSystemUser(loggedUser));
        } catch (Exception ex) {
            authorized = false;
        } finally {
        }
        logger.debug("Returning with: {}", authorized);
        return authorized;
    }
}
