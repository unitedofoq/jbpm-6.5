package com.unitedofoq.fabs.core.entitybase;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "testnewentitytbl")
public class TestNewEntity extends BaseEntity {

    private String strField1;

    public String getStrField1() {
        return strField1;
    }

    public String getStrField1DD() {
        return "TestNewEntity_strField1";
    }

    public void setStrField1(String strField1) {
        this.strField1 = strField1;
    }

    private String strField1ch;

    public String getStrField1ch() {
        return strField1ch;
    }

    public String getStrField1chDD() {
        return "TestNewEntity_strField1ch";
    }

    public void setStrField1ch(String strField1ch) {
        this.strField1ch = strField1ch;
    }

    private String strField1init;

    public String getStrField1init() {
        return strField1init;
    }

    public String getStrField1initDD() {
        return "TestNewEntity_strField1init";
    }

    public void setStrField1init(String strField1init) {
        this.strField1init = strField1init;
    }
    private String strField1initFunc;

    public String getStrField1initFunc() {
        return strField1initFunc;
    }

    public String getStrField1initFuncDD() {
        return "TestNewEntity_strField1initFunc";
    }

    public void setStrField1initFunc(String strField1initFunc) {
        this.strField1initFunc = strField1initFunc;
    }

    private String strField1val;

    public String getStrField1val() {
        return strField1val;
    }

    public String getStrField1valDD() {
        return "TestNewEntity_strField1val";
    }

    public void setStrField1val(String strField1val) {
        this.strField1val = strField1val;
    }
    private String strField1post;

    public String getStrField1post() {
        return strField1post;
    }

    public String getStrField1postDD() {
        return "TestNewEntity_strField1post";
    }

    public void setStrField1post(String strField1post) {
        this.strField1post = strField1post;
    }

    private String strField2;

    public String getStrField2() {
        return strField2;
    }

    public String getStrField2DD() {
        return "TestNewEntity_strField2";
    }

    public void setStrField2(String strField2) {
        this.strField2 = strField2;
    }

    // <editor-fold defaultstate="collapsed" desc="translation">
    @Translatable(translationField = "strField1TrTranslated")
    private String strField1Tr;

    public String getStrField1Tr() {
        return strField1Tr;
    }

    public String getStrField1TrDD() {
        return "TestNewEntity_strField1Tr";
    }

    public void setStrField1Tr(String strField1Tr) {
        this.strField1Tr = strField1Tr;
    }

    @Transient
    @Translation(originalField = "strField1Tr")
    private String strField1TrTranslated;

    public String getStrField1TrTranslated() {
        return strField1TrTranslated;
    }

    public void setStrField1TrTranslated(String strField1TrTranslated) {
        this.strField1TrTranslated = strField1TrTranslated;
    }

    @Translatable(translationField = "strField2TrTranslated")
    private String strField2Tr;

    public String getStrField2Tr() {
        return strField2Tr;
    }

    public String getStrField2TrDD() {
        return "TestNewEntity_strField2Tr";
    }

    public void setStrField2Tr(String strField2Tr) {
        this.strField2Tr = strField2Tr;
    }

    @Transient
    @Translation(originalField = "strField2Tr")
    private String strField2TrTranslated;

    public String getStrField2TrTranslated() {
        return strField2TrTranslated;
    }

    public void setStrField2TrTranslated(String strField2TrTranslated) {
        this.strField2TrTranslated = strField2TrTranslated;
    }
    // </editor-fold >

    @Column(name = "strField3Col")
    private String strField3;

    public String getStrField3() {
        return strField3;
    }

    public String getStrField3DD() {
        return "TestNewEntity_strField3";
    }

    public void setStrField3(String strField3) {
        this.strField3 = strField3;
    }

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateField1;

    public Date getDateField1() {
        return dateField1;
    }

    public String getDateField1DD() {
        return "TestNewEntity_dateField1";
    }

    public void setDateField1(Date dateField1) {
        this.dateField1 = dateField1;
    }

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateField2;

    public Date getDateField2() {
        return dateField2;
    }

    public String getDateField2DD() {
        return "TestNewEntity_dateField2";
    }

    public void setDateField2(Date dateField2) {
        this.dateField2 = dateField2;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateField3Col")
    private Date dateField3;

    public Date getDateField3() {
        return dateField3;
    }

    public String getDateField3DD() {
        return "TestNewEntity_dateField3";
    }

    public void setDateField3(Date dateField3) {
        this.dateField3 = dateField3;
    }

    private BigDecimal decField1;

    public BigDecimal getDecField1() {
        return decField1;
    }

    public String getDecField1DD() {
        return "TestNewEntity_decField1";
    }

    public void setDecField1(BigDecimal decField1) {
        this.decField1 = decField1;
    }

    private BigDecimal decField2;

    public BigDecimal getDecField2() {
        return decField2;
    }

    public String getDecField2DD() {
        return "TestNewEntity_decField2";
    }

    public void setDecField2(BigDecimal decField2) {
        this.decField2 = decField2;
    }

    @Column(name = "decField3Col")
    private BigDecimal decField3;

    public BigDecimal getDecField3() {
        return decField3;
    }

    public String getDecField3DD() {
        return "TestNewEntity_decField3";
    }

    public void setDecField3(BigDecimal decField3) {
        this.decField3 = decField3;
    }

    @ManyToOne
    private TestNewEntity selfField1;

    public TestNewEntity getSelfField1() {
        return selfField1;
    }

    public String getSelfField1DD() {
        return "TestNewEntity_selfField1";
    }

    public void setSelfField1(TestNewEntity selfField1) {
        this.selfField1 = selfField1;
    }

    @ManyToOne
    private TestNewEntity selfField2;

    public TestNewEntity getSelfField2() {
        return selfField2;
    }

    public String getSelfField2DD() {
        return "TestNewEntity_selfField2";
    }

    public void setSelfField2(TestNewEntity selfField2) {
        this.selfField2 = selfField2;
    }

    @ManyToOne
    @JoinColumn(name = "selfField3Col_dbid")
    private TestNewEntity selfField3;

    public TestNewEntity getSelfField3() {
        return selfField3;
    }

    public String getSelfField3DD() {
        return "TestNewEntity_selfField3";
    }

    public void setSelfField3(TestNewEntity selfField3) {
        this.selfField3 = selfField3;
    }
    @OneToOne
    private TestNewEntity selfField1onetoone;

    public TestNewEntity getSelfField1onetoone() {
        return selfField1onetoone;
    }

    public String getSelfField1onetooneDD() {
        return "TestNewEntity_selfField1onetoone";
    }

    public void setSelfField1onetoone(TestNewEntity selfField1onetoone) {
        this.selfField1onetoone = selfField1onetoone;
    }

    @OneToOne
    private TestNewEntity selfField2onetoone;

    public TestNewEntity getSelfField2onetoone() {
        return selfField2onetoone;
    }

    public String getSelfField2onetooneDD() {
        return "TestNewEntity_selfField2onetoone";
    }

    public void setSelfField2onetoone(TestNewEntity selfField2onetoone) {
        this.selfField2onetoone = selfField2onetoone;
    }

    @OneToOne
    @JoinColumn(name = "selfField3onetooneCol_dbid")
    private TestNewEntity selfField3onetoone;

    public TestNewEntity getSelfField3onetoone() {
        return selfField3onetoone;
    }

    public String getSelfField3onetooneDD() {
        return "TestNewEntity_selfField3onetoone";
    }

    public void setSelfField3onetoone(TestNewEntity selfField3onetoone) {
        this.selfField3onetoone = selfField3onetoone;
    }

    @OneToMany
    private List<TestNewSubEntity> oneToManyFieldLazy;

    public List<TestNewSubEntity> getOneToManyFieldLazy() {
        return oneToManyFieldLazy;
    }

    public void setOneToManyFieldLazy(List<TestNewSubEntity> oneToManyFieldLazy) {
        this.oneToManyFieldLazy = oneToManyFieldLazy;
    }

}
