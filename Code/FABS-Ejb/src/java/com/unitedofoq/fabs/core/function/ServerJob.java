/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.function;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;

/**
 *
 * @author arezk
 */
@Entity
@FABSEntitySpecs(isAuditTrailed=false)
@ChildEntity(fields = "serverJobMessages")
public class ServerJob extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="Notification Types">
    public final static int INBOX = 1440659;
    public final static int EMAIL = 1440662;
    public final static int SMS = 1440663;
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="serverJobMessages">
    @OneToMany(mappedBy = "serverJob", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<ServerJobMessage> serverJobMessages;

    public List<ServerJobMessage> getServerJobMessages() {
        return serverJobMessages;
    }

    public void setServerJobMessages(List<ServerJobMessage> serverJobMessages) {
        this.serverJobMessages = serverJobMessages;
    }

    public String getServerJobMessagesDD() {
        return "ServerJob_serverJobMessages";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="functionName">
    private String functionName;

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public String getFunctionNameDD() {
        return "ServerJob_functionName";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="status">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC status;

    public UDC getStatus() {
        return status;
    }

    public void setStatus(UDC status) {
        this.status = status;
    }

    public String getStatusDD() {
        return "ServerJob_status";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="loggedUser">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private OUser loggedUser;

    public OUser getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(OUser loggedUser) {
        this.loggedUser = loggedUser;
    }

    public String getLoggedUserDD() {
        return "ServerJob_loggedUser";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "ServerJob_code";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startTime">
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date startTime;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getStartTimeDD() {
        return "ServerJob_startTime";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="finishTime">
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date finishTime;

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public String getFinishTimeDD() {
        return "ServerJob_finishTime";
    }
    // </editor-fold>
}
