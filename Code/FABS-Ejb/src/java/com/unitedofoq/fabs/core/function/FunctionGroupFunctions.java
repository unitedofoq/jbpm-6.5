/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.function;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields={"ofunction", "functionGroup"})
public class FunctionGroupFunctions extends ObjectBaseEntity{
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private OFunction ofunction;

    public OFunction getOfunction() {
        return ofunction;
    }

    public void setOfunction(OFunction OFunction) {
        this.ofunction = OFunction;
    }
    
    public String getOfunctionDD() {
        return "FunctionGroupFunctions_ofunction";
    }
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private FunctionGroup functionGroup;

    public FunctionGroup getFunctionGroup() {
        return functionGroup;
    }

    public void setFunctionGroup(FunctionGroup functionGroup) {
        this.functionGroup = functionGroup;
    }
    
    public String getFunctionGroupDD() {
        return "FunctionGroupFunctions_functionGroup";
    }
    
    private int sortIndex;

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }
    
    public String getSortIndexDD() {
        return "FunctionGroupFunctions_sortIndex";
    }
}
