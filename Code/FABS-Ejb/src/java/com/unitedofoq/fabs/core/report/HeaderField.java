package com.unitedofoq.fabs.core.report;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;

@Entity
@ParentEntity(fields="headerFieldReport")
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class HeaderField extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="headerFieldReport">
    @JoinColumn(nullable=false, name="HeaderFieldReport_DBID")
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OReport headerFieldReport;

    public OReport getHeaderFieldReport() {
        return headerFieldReport;
    }

    public String getHeaderFieldReportDD() {
        return "HeaderField_headerFieldReport";
    }

    public void setHeaderFieldReport(OReport headerFieldReport) {
        this.headerFieldReport = headerFieldReport;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="label">
    @Column(nullable=false)
    private String label;

    public String getLabel() {
        return label;
    }

    public String getLabelDD() {
        return "HeaderField_label";
    }

    public void setLabel(String label) {
        this.label = label;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="headerFieldExpression">
    @Column(nullable=false)
    private String headerFieldExpression;

    public String getHeaderFieldExpression() {
        return headerFieldExpression;
    }

    public String getHeaderFieldExpressionDD() {
        return "HeaderField_headerFieldExpression";
    }

    public void setHeaderFieldExpression(String headerFieldExpression) {
        this.headerFieldExpression = headerFieldExpression;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="sortIndex">
    @Column(nullable=false)
    private Integer sortIndex;

    public Integer getSortIndex() {
        return sortIndex;
    }

    public String getSortIndexDD() {
        return "HeaderField_sortIndex";
    }

    public void setSortIndex(Integer sortIndex) {
        this.sortIndex = sortIndex;
    }
    //</editor-fold>

}
