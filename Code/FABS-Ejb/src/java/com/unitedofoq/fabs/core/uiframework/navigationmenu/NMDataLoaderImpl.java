/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.navigationmenu;

import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.central.OTenant;
import com.unitedofoq.fabs.central.OTenantDTO;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.filter.FilterFieldDTO;
import com.unitedofoq.fabs.core.filter.OFilter;
import com.unitedofoq.fabs.core.i18n.TextTranslationServiceRemote;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;


import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.slf4j.LoggerFactory;

/**
 *
 * @author satef
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.uiframework.navigationmenu.NMDataLoader",
        beanInterface = NMDataLoader.class)
public class NMDataLoaderImpl implements NMDataLoader {
       final static org.slf4j.Logger logger = LoggerFactory.getLogger(NMDataLoaderImpl.class);
    @EJB
    OCentralEntityManagerRemote centralOEM;
    @EJB
    OEntityManagerRemote oem;
    protected static int COUNTER = 10000;
    @EJB
    protected TextTranslationServiceRemote textTranslationServiceRemote;
    private HashMap<String, Map<String, Object>> screensSessionInfo = new HashMap<String, Map<String, Object>>();
    public final static String MODE_VAR = "ScreenModeVar";
    public static final String MESSAGE = "SCREEN_INPUT";
    public final static String WIZARD_VAR = "WizardVar";
    public static String PASSED_VAR = "PASSED_ENTITY";
    public static String FILTER_MESSAGE_VAR = "MESSAGE_VAR";
    public static String FILTER_TYPE_VAR = "FILTER_TYPE";
    public static String TO_B_OPENED_SCREEN_INST_ID_VAR = "TO_B_OPENED_SCREEN_INST_ID";
    public HashMap<String, OTenantDTO> tenants;
    private HashMap<String, ArrayList<String>> role_menu_indexs;
    private HashMap<String, ArrayList<String>> menu_submenu_indexs;
    private HashMap<String, ArrayList<String>> menu_fun_indexs;
    public static int SCREENS_COUNTER = 0;
    
    public NMDataLoaderImpl() {
    }

    /**
     *
     * @return
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public HashMap<String, ConcurrentHashMap> constractDTOCach() {
 logger.debug("Entering ");
        HashMap<String, ConcurrentHashMap> cachedMenus;
        ConcurrentHashMap<String, HashMap> cachedIndexs;
        ConcurrentHashMap<String, HashMap> cachedImages;
        cachedMenus = (HashMap<String, ConcurrentHashMap>) new HashMap();
        cachedIndexs = (ConcurrentHashMap<String, HashMap>) new ConcurrentHashMap();
        cachedImages = (ConcurrentHashMap<String, HashMap>) new ConcurrentHashMap();

        tenants = (HashMap<String, OTenantDTO>) getTenantsDTOList1();
        ArrayList<String> TenantKeys = new ArrayList(tenants.keySet());
        for (int i = 0;
                i < TenantKeys.size();
                i++) {
            try {
                HashMap<String, String> imageMap = cachMenuImages(tenants.get(TenantKeys.get(i)));
                ConcurrentHashMap<Long, ArrayList<NavigationMenuDTO>> role_menus = getListOfMainMenus(tenants.get(TenantKeys.get(i)), imageMap);
                HashMap<Long, ArrayList<OMenuDTO>> menu_submenus = getListOfSubMenus(tenants.get(TenantKeys.get(i)), null, imageMap);
                HashMap<Long, ArrayList<OfunctionDTO>> menu_functions = getListOffunctions(tenants.get(TenantKeys.get(i)), null);
                constractIndexCach(role_menus, menu_submenus, menu_functions);

                cachedImages.put(TenantKeys.get(i), imageMap);
                cachedIndexs.put(TenantKeys.get(i) + "R", role_menu_indexs);
                cachedIndexs.put(TenantKeys.get(i) + "M", menu_submenu_indexs);
                cachedIndexs.put(TenantKeys.get(i) + "F", menu_fun_indexs);

                ArrayList<Long> roles = new ArrayList<Long>(role_menus.keySet());
                ArrayList<Long> parentMenus = new ArrayList<Long>(menu_submenus.keySet());
                ArrayList<Long> menuFunctions = new ArrayList<Long>(menu_functions.keySet());
                if (!roles.isEmpty()) {
                    for (int p = 0; p < roles.size(); p++) {
                        for (int m = 0; m < role_menus.get(roles.get(p)).size(); m++) {
                            Long menuID = role_menus.get(roles.get(p)).get(m).getMenuId();
                            if (parentMenus.contains(menuID)) {
                                role_menus.get(roles.get(p)).get(m).setChildMenues(menu_submenus.get(menuID));
                                Collections.sort(role_menus.get(roles.get(p)).get(m).getChildMenues());
                            }
                            if (menuFunctions.contains(menuID)) {
                                role_menus.get(roles.get(p)).get(m).getFunctions().addAll(menu_functions.get(menuID));
                            }
                        }
                        Collections.sort(role_menus.get(roles.get(p)));
                    }
                    assigneSecondLFuns(role_menus, menu_functions);
                    String id = TenantKeys.get(i);
                    cachedMenus.put(id, role_menus);
                }
            } catch (Exception ex) {
                logger.error("Exception thrown ",ex);
                System.out.println("Error building DTO Cach");
            }
        }

        cachedMenus.put("index", cachedIndexs);
        cachedMenus.put("images", cachedImages);
        logger.debug("Returning ");
        return cachedMenus;
    }

    public HashMap<String, ConcurrentHashMap> ConstractTanentDTOCach(String tanentID) {
logger.debug("Entering ");
        HashMap<String, ConcurrentHashMap> cachedMenus;
        ConcurrentHashMap<String, HashMap> cachedImages;
        cachedMenus = (HashMap<String, ConcurrentHashMap>) new HashMap();
        cachedImages = (ConcurrentHashMap<String, HashMap>) new ConcurrentHashMap();

        tenants = (HashMap<String, OTenantDTO>) getTenantsDTOList1();
        try {
            HashMap<String, String> imageMap = cachMenuImages(tenants.get(tanentID));
            ConcurrentHashMap<Long, ArrayList<NavigationMenuDTO>> role_menus = getListOfMainMenus(tenants.get(tanentID), imageMap);
            HashMap<Long, ArrayList<OMenuDTO>> menu_submenus = getListOfSubMenus(tenants.get(tanentID), null, imageMap);
            HashMap<Long, ArrayList<OfunctionDTO>> menu_functions = getListOffunctions(tenants.get(tanentID), null);
            constractIndexCach(role_menus, menu_submenus, menu_functions);

            cachedImages.put(tanentID, imageMap);

            ArrayList<Long> roles = new ArrayList<Long>(role_menus.keySet());
            ArrayList<Long> parentMenus = new ArrayList<Long>(menu_submenus.keySet());
            ArrayList<Long> menuFunctions = new ArrayList<Long>(menu_functions.keySet());
            if (!roles.isEmpty()) {
                for (int p = 0; p < roles.size(); p++) {
                    for (int m = 0; m < role_menus.get(roles.get(p)).size(); m++) {
                        Long menuID = role_menus.get(roles.get(p)).get(m).getMenuId();
                        if (parentMenus.contains(menuID)) {
                            role_menus.get(roles.get(p)).get(m).setChildMenues(menu_submenus.get(menuID));
                            Collections.sort(role_menus.get(roles.get(p)).get(m).getChildMenues());
                        }
                        if (menuFunctions.contains(menuID)) {
                            role_menus.get(roles.get(p)).get(m).getFunctions().addAll(menu_functions.get(menuID));
                        }
                    }
                    Collections.sort(role_menus.get(roles.get(p)));
                }
                assigneSecondLFuns(role_menus, menu_functions);
                String id = tanentID;
                cachedMenus.put(id, role_menus);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            System.out.println("Error building DTO Cach");
        }
        logger.debug("Returning ");
        return cachedMenus;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void assigneSecondLFuns(ConcurrentHashMap<Long, ArrayList<NavigationMenuDTO>> rm, HashMap<Long, ArrayList<OfunctionDTO>> fun) {
        logger.debug("Returning ");
        ArrayList<OMenuDTO> currentProcessing = new ArrayList<OMenuDTO>();
        Map n = rm;
        ArrayList<ArrayList<NavigationMenuDTO>> mainMenus = new ArrayList(n.values());
        for (int i = 0; i < mainMenus.size(); i++) {
            for (int s = 0; s < mainMenus.get(i).size(); s++) {
                if (mainMenus.get(i).get(s).getChildMenues() != null) {
                    currentProcessing.addAll(mainMenus.get(i).get(s).getChildMenues());
                }
            }
        }

        for (int p = 0; p < currentProcessing.size(); p++) {
            ArrayList<OfunctionDTO> funs = fun.get(currentProcessing.get(p).getDbid());
            if (funs != null) {
                currentProcessing.get(p).setFunctions(funs);
            }
        }
logger.debug("Returning ");
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public ConcurrentHashMap<Long, ArrayList<NavigationMenuDTO>> getListOfMainMenus(OTenantDTO OT, HashMap<String, String> imageMap) {
        logger.debug("Entering ");
        List parent_menus;
        ConcurrentHashMap<Long, ArrayList<NavigationMenuDTO>> role_menu_map = new ConcurrentHashMap();
        try {
            OUser tempUser = new OUser();
            OTenant tempTenant = new OTenant();
            tempTenant.setPersistenceUnitName(OT.getPersistenceUnitName());
            tempUser.setTenant(tempTenant);
            String Query = "SELECT distinct RM.OROLE_DBID,OM.DBID,OM.NAME,OM.SORTINDEX,"
                    + "OM.HIERCODE,OM.LARGEMENUIMAGE_DBID, MT.NAME FROM OMENU OM JOIN ROLEMENU RM ON OM.DBID = RM.OMENU_DBID "
                    + "LEFT JOIN OMENUTRANSLATION MT ON OM.DBID = MT.ENTITYDBID"
                    + " WHERE OM.PARENTMENU_DBID IS NULL AND RM.DELETED = 0 AND OM.DELETED = 0 "
                    + "ORDER BY RM.OROLE_DBID";
            logger.debug("Query: {}",Query);
            parent_menus = (List) oem.executeEntityListNativeQuery(Query, tempUser);
            oem.closeEM(tempUser);
            for (int i = 0; i < parent_menus.size(); i++) {
                System.out.println(i);
                Object[] roleMenu = (Object[]) parent_menus.get(i);
                Long ImageDBID;
                if (roleMenu[5] != null) {
                    String LargeImageDBID = roleMenu[5].toString();
                    ImageDBID = Long.parseLong(LargeImageDBID);
                } else {
                    ImageDBID = new Long("0");
                }
                String RoleDBID = (null == roleMenu[0]) ? "0" : roleMenu[0].toString();
                String MenuDBID = (null == roleMenu[1]) ? "0" : roleMenu[1].toString();
                if (MenuDBID.equals("1000850020758")) {
                    System.out.print("tedd");
                }
                String sortIndex = (null == roleMenu[3]) ? "0" : roleMenu[3].toString();
                if (role_menu_map.containsKey(Long.parseLong(RoleDBID))) {
                    NavigationMenuDTO nv = new NavigationMenuDTO(Long.parseLong(RoleDBID), Long.parseLong(MenuDBID), Integer.parseInt(sortIndex),
                            (String) roleMenu[2], (String) roleMenu[6], (String) roleMenu[4], null, null, ImageDBID);
                    nv.setImageURL(imageMap.get(String.valueOf(nv.getLargeImageDbid())));
                    role_menu_map.get(Long.parseLong(RoleDBID)).add(nv);
                } else {
                    NavigationMenuDTO nv = new NavigationMenuDTO(Long.parseLong(RoleDBID), Long.parseLong(MenuDBID),
                            Integer.parseInt(sortIndex), (String) roleMenu[2], (String) roleMenu[6], (String) roleMenu[4], null, null, ImageDBID);
                    nv.setImageURL(imageMap.get(String.valueOf(nv.getLargeImageDbid())));
                    System.out.println(nv.getMenuName());
                    role_menu_map.put(Long.parseLong(RoleDBID), new ArrayList<NavigationMenuDTO>());
                    role_menu_map.get(Long.parseLong(RoleDBID)).add(nv);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            System.out.println("Error getting parent menus list");
        }
        logger.debug("Returning ");
        return role_menu_map;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public HashMap<Long, ArrayList<OMenuDTO>> getListOfSubMenus(OTenantDTO OT, String parent_menu, HashMap<String, String> imageMap) {
        logger.debug("Entering ");
        List sub_menus;
        HashMap<Long, ArrayList<OMenuDTO>> parent_sub_menu_map = new HashMap();
        try {
            OUser tempUser = new OUser();
            OTenant tempTenant = new OTenant();
            tempTenant.setPersistenceUnitName(OT.getPersistenceUnitName());
            tempUser.setTenant(tempTenant);
            String Query;
            if (parent_menu == null) {
                Query = "SELECT M.DBID,M.PARENTMENU_DBID,M.NAME,M.SORTINDEX,M.HIERCODE,M.LARGEMENUIMAGE_DBID,MT.NAME FROM OMENU M left join OMENUTRANSLATION MT "
                        + "on M.DBID = MT.ENTITYDBID WHERE M.PARENTMENU_DBID IS NOT NULL"
                        + " AND M.DELETED =0 AND M.ACTIVE = 1";
                logger.debug("Query: {}",Query);
            } else {
                Query = "SELECT M.DBID,M.PARENTMENU_DBID,M.NAME,M.SORTINDEX,M.HIERCODE,MT.NAME FROM OMENU M left JOIN OMENUTRANSLATION MT "
                        + "ON M.DBID = MT.ENTITYDBID WHERE M.PARENTMENU_DBID LIKE '" + parent_menu + "'"
                        + " AND M.DELETED = 0";
                logger.debug("Query: {}",Query);
            }

            sub_menus = (List) oem.executeEntityListNativeQuery(Query, tempUser);
            oem.closeEM(tempUser);
            parent_sub_menu_map = new HashMap();
            for (int i = 0; i < sub_menus.size(); i++) {
                System.out.println(i);

                Object[] subMenu = (Object[]) sub_menus.get(i);
                Long ImageDBID;

                if (subMenu[5] != null) {
                    String largeImageDBID = subMenu[5].toString();
                    ImageDBID = Long.parseLong(largeImageDBID);
                } else {
                    ImageDBID = new Long(0);
                }
                String dbid = (null == subMenu[0]) ? "0" : subMenu[0].toString();
                String sortIndex = (null == subMenu[3]) ? "0" : subMenu[3].toString();
                String parentMenuDBID = subMenu[1].toString();
                OMenuDTO om = new OMenuDTO(Long.parseLong(dbid), (String) subMenu[2], (String) subMenu[6], null,
                        Integer.parseInt(sortIndex), (String) subMenu[4], null, ImageDBID);
                om.setImageURL(imageMap.get(String.valueOf(om.getLargeImageDbid())));
                if (parent_sub_menu_map.containsKey(Long.parseLong(parentMenuDBID))) {
                    parent_sub_menu_map.get(Long.parseLong(parentMenuDBID)).add(om);
                } else {
                    parent_sub_menu_map.put(Long.parseLong(parentMenuDBID), new ArrayList<OMenuDTO>());
                    parent_sub_menu_map.get(Long.parseLong(parentMenuDBID)).add(om);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            System.out.println("Error getting sub menus list");
        }
        logger.debug("Returning ");
        return parent_sub_menu_map;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public HashMap<Long, ArrayList<OfunctionDTO>> getListOffunctions(OTenantDTO OT, ArrayList<String> menusList) {
        logger.debug("Entering");
        List functions;
        HashMap<Long, ArrayList<OfunctionDTO>> function_menu_map = new HashMap();
        try {
            OUser tempUser = new OUser();
            OTenant tempTenant = new OTenant();
            tempTenant.setPersistenceUnitName(OT.getPersistenceUnitName());
            tempUser.setTenant(tempTenant);
            String Query;
            if (menusList == null) {
                Query = "SELECT distinct menufunction.* from menufunction, omenu"
                        + " where omenu.dbid = menufunction.menudbid AND OMENU.DELETED =0 ";
                logger.debug("Query: {}",Query);
            } else {
                String InCondition = "(";
                for (int i = 0; i < menusList.size(); i++) {
                    InCondition += menusList.get(i) + ",";
                }
                InCondition = InCondition.substring(0, InCondition.lastIndexOf(",")) + ")";
                Query = "Select distinct menufunction.* from menufunction join omenu "
                        + "on omenu.dbid = menufunction.menudbid"
                        + " where omenu.dbid in " + InCondition + " AND OMENU.DELETED =0 ";
                logger.debug("Query: {}",Query);
            }
            functions = (List) oem.executeEntityListNativeQuery(Query, tempUser);
            oem.closeEM(tempUser);
            for (int i = 0; i < functions.size(); i++) {
                OfunctionDTO fun;
                Object[] funMenu = (Object[]) functions.get(i);
                String x5 = funMenu[5].toString();
                String x1 = funMenu[1].toString();
                String sortIndex = (null == funMenu[0]) ? "0" : funMenu[0].toString();
                if (function_menu_map.containsKey(Long.parseLong(x5))) {
                    Long x = Long.parseLong(funMenu[3].toString());
                    fun =
                            new OfunctionDTO(Long.parseLong(x1), (String) funMenu[2],
                            (String) funMenu[4], Long.parseLong(x5), x.intValue(), Integer.parseInt(sortIndex));
                    function_menu_map.get(Long.parseLong(x5)).add(fun);
                } else {
                    Long x = Long.parseLong(funMenu[3].toString());
                    fun =
                            new OfunctionDTO(Long.parseLong(x1), (String) funMenu[2],
                            (String) funMenu[4], Long.parseLong(x5), x.intValue(), Integer.parseInt(sortIndex));
                    function_menu_map.put(Long.parseLong(x5), new ArrayList<OfunctionDTO>());
                    function_menu_map.get(Long.parseLong(x5)).add(fun);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            System.out.println("Error getting functions list");
        }
        logger.debug("Returning");
        return function_menu_map;
    }
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public HashMap<String, String> cachMenuImages(OTenantDTO OT) {
        logger.debug("Entering");
        HashMap<String, String> ImagesMap = new HashMap();
        try {
            OUser tempUser = new OUser();
            OTenant tempTenant = new OTenant();
            tempTenant.setPersistenceUnitName(OT.getPersistenceUnitName());
            tempUser.setTenant(tempTenant);
            List images;
            String Query = "SELECT DBID,IMAGE,NAME FROM OIMAGE WHERE DELETED = 0";
            images = (List) oem.executeEntityListNativeQuery(Query, tempUser);
            oem.closeEM(tempUser);
            for (int i = 0; i < images.size(); i++) {
                Object[] image = (Object[]) images.get(i);
                String dbid = image[0].toString();
                OImageDTO imageDTO = new OImageDTO(Long.parseLong(dbid), (String) image[2], (byte[]) image[1]);
                String imagePath = getImagePath(imageDTO.getName(), imageDTO);
                ImagesMap.put(imageDTO.getDbid().toString(), imagePath);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        }
        logger.debug("Returning");
        return ImagesMap;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public String getImagePath(String fileName, OImageDTO imageDTO) {
        logger.debug("Entering");
        try {

            String realPath =
                    ((ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext())
                    .getRealPath("/");

            File reportDir = new File(realPath);

            if (!reportDir.exists()) {
                reportDir.mkdir();
            }
            String separator = "/";
            if (realPath.lastIndexOf("\\") != -1) {
                separator = "\\";
            }
            String imageFilePath = realPath + separator + fileName;

            OutputStream outputStream = new FileOutputStream(imageFilePath);
            outputStream.write(imageDTO.getImage());
            outputStream.close();
            File file = new File(imageFilePath);

            if (file.exists()) {
                return imageFilePath;//.substring(realPath.indexOf("/resources/cached/"));
            } else {
                logger.debug("File Not Found");
                 logger.debug("Returning with empty string");
                return "";
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            logger.debug("Returning with empty string");
            return "";
        }
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public HashMap<String, OTenantDTO> getTenantsDTOList1() {
        logger.debug("Entering");
        List<OTenant> tenants = centralOEM.getTenantsList();
        HashMap<String, OTenantDTO> tenantDTOList = new HashMap();
        try {
            OTenantDTO obj = null;
            for (int i = 0; i < tenants.size(); i++) {
                obj = new OTenantDTO();
                obj.setActive(((OTenant) tenants.get(i)).getActive());
                obj.setConnDriver(((OTenant) tenants.get(i)).getConnDriver());
                obj.setConnPassword(((OTenant) tenants.get(i)).getConnPassword());
                obj.setConnURL(((OTenant) tenants.get(i)).getConnURL());
                obj.setId(((OTenant) tenants.get(i)).getId());
                obj.setName(((OTenant) tenants.get(i)).getName());
                obj.setPersistenceUnitName(((OTenant) tenants.get(i)).getPersistenceUnitName());
                tenantDTOList.put(obj.getId().toString(), obj);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        }
        if(tenantDTOList!=null)
         logger.debug("Returning with tenantDTOList of size: {}",tenantDTOList.size());
        return tenantDTOList;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ORoleDTO> getRolesDTOList(OTenantDTO oTenantDTO) {
        logger.debug("Entering");
        OUser tempUser = new OUser();
        OTenant tempTenant = new OTenant();
        tempTenant.setPersistenceUnitName(oTenantDTO.getPersistenceUnitName());
        tempUser.setTenant(tempTenant);

        List<ORoleDTO> roleListDTO = null;
        String query = "SELECT NEW com.unitedofoq.fabs.core.uiframework.navigationmenu.ORoleDTO("
                + " oModule.dbid,"
                + " entity.name,"
                + " entity.description,"
                + " entity.reserved)"
                + " FROM ORole entity "
                + " LEFT JOIN entity.omodule oModule";
        logger.debug("Query: {}",query);
        try {
            roleListDTO = new ArrayList<ORoleDTO>((Vector<ORoleDTO>) oem.executeEntityListQuery(query, tempUser));
            oem.closeEM(tempUser);
            System.out.println("");
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        } finally {
            logger.debug("Returning");
            return roleListDTO;
        }
    }

    public HashMap<String, OTenantDTO> getTenants() {
        return tenants;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public HashMap<String, ScreenFunctionDTO> getScreenFunctionListDTO(OUser loggedUser) {
        logger.debug("Entering");
        HashMap<String, ScreenFunctionDTO> screenFunctions = new HashMap();
        List<ScreenFunctionDTO> screenFunction = null;
        List<FilterFieldDTO> filterFields = null;
        String query = " SELECT NEW com.unitedofoq.fabs.core.uiframework.navigationmenu.ScreenFunctionDTO("
                + " screenFunction.dbid,"
                + " oScreen.name,"
                + " oScreen.dbid,"
                + " screenMode.dbid,"
                + " filter.dbid,"
                + " filter.active,"
                + " filter.alwaysShow,"
                + " filter.showInsideScreen"
                + " )"
                + " FROM ScreenFunction screenFunction"
                + " LEFT JOIN screenFunction.screenMode screenMode"
                + " LEFT JOIN screenFunction.oscreen oScreen"
                + " LEFT JOIN oScreen.screenFilter filter"
                + " WHERE screenFunction.active=true "
                + " AND   screenFunction.inActive=false";
        logger.debug("Query: {}",query);
        try {
            screenFunction = new ArrayList<ScreenFunctionDTO>((Vector<ScreenFunctionDTO>) oem.executeEntityListQuery(query, loggedUser));
            for (ScreenFunctionDTO screenFunctionDTO : screenFunction) {
                if (screenFunctionDTO.getoScreenFilterDBID() == 0) {
                    Long DBID = screenFunctionDTO.getDbid();
                    screenFunctions.put(DBID.toString(), screenFunctionDTO);
                    continue;
                }
                query = " SELECT NEW com.unitedofoq.fabs.core.filter.FilterFieldDTO("
                        + " filterField.dbid,"
                        + " filterField.filter.dbid,"
                        + " filterField.fieldValue,"
                        + " filterField.fieldExpression,"
                        + " filterField.fromTo,"
                        + " filterField.fieldIndex,"
                        + " filterField.fieldValueTo,"
                        + " filterField.operator,"
                        + " fieldDD.dbid"
                        + " )"
                        + " FROM FilterField filterField"
                        + " LEFT JOIN  filterField.fieldDD fieldDD"
                        + " WHERE  filterField.inActive=false"
                        + " AND filterField.filter.dbid=" + screenFunctionDTO.getoScreenFilterDBID();
                logger.debug("Query: {}",query);
                filterFields = new ArrayList<FilterFieldDTO>((Vector<FilterFieldDTO>) oem.executeEntityListQuery(query, loggedUser));
                screenFunctionDTO.setoScreenFilterFilelds(filterFields);
                Long DBID = screenFunctionDTO.getDbid();
                screenFunctions.put(DBID.toString(), screenFunctionDTO);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        } finally {
            logger.debug("Returning");
            return screenFunctions;
        }
    }

    @Override
    public void constractIndexCach(ConcurrentHashMap<Long, ArrayList<NavigationMenuDTO>> role_menu,
            HashMap<Long, ArrayList<OMenuDTO>> menu_submenus, HashMap<Long, ArrayList<OfunctionDTO>> menu_functions) {
        logger.debug("Entering");
        role_menu_indexs = new HashMap<String, ArrayList<String>>();
        menu_submenu_indexs = new HashMap<String, ArrayList<String>>();
        menu_fun_indexs = new HashMap<String, ArrayList<String>>();
        ArrayList<Long> Keys = new ArrayList<Long>(role_menu.keySet());

        for (int i = 0; i < Keys.size(); i++) {
            role_menu_indexs.put(Keys.get(i).toString(), new ArrayList<String>());
            ArrayList<NavigationMenuDTO> mainMenus = role_menu.get(Keys.get(i));
            for (int k = 0; k < mainMenus.size(); k++) {
                role_menu_indexs.get(Keys.get(i).toString()).add(mainMenus.get(k).getMenuId().toString());
            }
        }

        Keys = new ArrayList<Long>(menu_submenus.keySet());
        for (int i = 0; i < Keys.size(); i++) {
            menu_submenu_indexs.put(Keys.get(i).toString(), new ArrayList<String>());
            ArrayList<OMenuDTO> submenus = menu_submenus.get(Keys.get(i));
            for (int k = 0; k < submenus.size(); k++) {
                menu_submenu_indexs.get(Keys.get(i).toString()).add(submenus.get(k).getDbid().toString());
            }
        }

        Keys = new ArrayList<Long>(menu_functions.keySet());
        for (int i = 0; i < Keys.size(); i++) {
            menu_fun_indexs.put(Keys.get(i).toString(), new ArrayList<String>());
            ArrayList<OfunctionDTO> funs = menu_functions.get(Keys.get(i));
            for (int k = 0; k < funs.size(); k++) {
                menu_fun_indexs.get(Keys.get(i).toString()).add(String.valueOf(funs.get(k).getFunctiondbid()));
            }
        }
        Keys = null;
        logger.debug("Returning");
    }

    /**
     * @return the role_menu_indexs
     */
    @Override
    public HashMap<String, ArrayList<String>> getRole_menu_indexs() {
        return role_menu_indexs;
    }

    /**
     * @param role_menu_indexs the role_menu_indexs to set
     */
    public void setRole_menu_indexs(HashMap<String, ArrayList<String>> role_menu_indexs) {
        this.role_menu_indexs = role_menu_indexs;
    }

    /**
     * @return the menu_submenu_indexs
     */
    @Override
    public HashMap<String, ArrayList<String>> getMenu_submenu_indexs() {
        return menu_submenu_indexs;
    }

    /**
     * @param menu_submenu_indexs the menu_submenu_indexs to set
     */
    public void setMenu_submenu_indexs(HashMap<String, ArrayList<String>> menu_submenu_indexs) {
        this.menu_submenu_indexs = menu_submenu_indexs;
    }

    @Override
    public HashMap<String, ArrayList<String>> getMenu_fun_indexs() {
        return menu_fun_indexs;
    }

    public void setMenu_fun_indexs(HashMap<String, ArrayList<String>> menu_fun_indexs) {
        this.menu_fun_indexs = menu_fun_indexs;
    }

    @Override
    public HashMap<String, Map<String, Object>> getScreensSessionInfo() {
        return screensSessionInfo;
    }

    @Override
    public String constructScreenFunctionsLinks(OfunctionDTO ofunction, OUser loggedUser) {

        //<editor-fold defaultstate="collapsed" desc="get ScreenFunctionDTO object">
        ScreenFunctionDTO screenFunctionDTO =
                getScreenFunctionDTO(loggedUser, ofunction.getFunctiondbid());
        //</editor-fold>

        ODataType voidDataType = null;
        try {
            //<editor-fold defaultstate="collapsed" desc="Prepare DataMessage Object">
            voidDataType = (ODataType) oem.loadEntity(ODataType.class.getSimpleName(),
                    Collections.singletonList("name = 'VoidDT'"), null, loggedUser);

            ODataMessage voidDataMessage = new ODataMessage();
            voidDataMessage.setData(new ArrayList<Object>());
            voidDataMessage.setODataType(voidDataType);
            //</editor-fold>

            String openedScreenInstId = screenFunctionDTO.getoScreenName() + "_" + COUNTER++;

            Map<String, Object> screenInfo = new HashMap<String, Object>();
            Map<String, Object> filterScreenInfo = new HashMap<String, Object>();

            if (screenFunctionDTO.getoScreenModeDBID() != 0) {
                screenInfo.put(MODE_VAR, screenFunctionDTO.getoScreenModeDBID());

            }
            screenInfo.put(MESSAGE, voidDataMessage);

            screenInfo.put(WIZARD_VAR, false);

            boolean nullData = false;
            boolean runFilter = false;

            OScreen screen = (OScreen) oem.loadEntity(OScreen.class.getSimpleName(),
                    screenFunctionDTO.getoScreenDBID(), null, null, loggedUser);

            if (screen.getScreenFilter() != null
                    && !screen.getScreenFilter().isInActive()) {
                for (int i = 0; i < screen.getScreenFilter().getFilterFields().size(); i++) {
                    logger.trace("Screen Fields Loop: {}",i);
                    if (screen.getScreenFilter().getFilterFields().get(i).isInActive()) {
                        logger.trace("{} Filter Field is inActive",screen.getScreenFilter().getFilterFields().get(i));
                        continue;
                    }

                    if (screen.getScreenFilter().getFilterFields().get(i).getFieldValue() == null
                            || screen.getScreenFilter().getFilterFields().get(i).getFieldValue().equals("")) {
                        nullData = true;
                    }
                }
                runFilter = screen.getScreenFilter().isAlwaysShow() || (nullData && !screen.getScreenFilter().isShowInsideScreen());
            }

            screensSessionInfo.put(openedScreenInstId, screenInfo);

            if (runFilter) {
logger.trace("run Filter is True");
                String screenName = "RunFilter";
                OScreen filterScreen = (OScreen) oem.loadEntity(OScreen.class.getSimpleName(),
                        Collections.singletonList("name = '" + screenName + "'"), null, loggedUser);
                String filterScreenInstId = filterScreen.getName() + "_" + COUNTER++;

                filterScreenInfo.put(PASSED_VAR, (BaseEntity) screen);
                filterScreenInfo.put(FILTER_TYPE_VAR, OFilter.FilterType.SCREEN_FILTER);
                filterScreenInfo.put(FILTER_MESSAGE_VAR, voidDataMessage);

                filterScreenInfo.put(TO_B_OPENED_SCREEN_INST_ID_VAR, openedScreenInstId);

                screensSessionInfo.put(filterScreenInstId, filterScreenInfo);

                filterScreen = (OScreen) textTranslationServiceRemote
                        .loadEntityTranslation(filterScreen, loggedUser);

                String call = "top.FABS.Portlet.open({screenName:'" + screenName + "',"
                        + "screenInstId:'" + filterScreenInstId + "'"
                        + ",viewPage:'" + filterScreen.getViewPage()
                        + "',portletTitle:'" + filterScreen.getHeaderTranslated()
                        + "',screenHeight:'',enableC2A:'" + true + "'});";
logger.trace("Call: {}",call);
                return call;

            } else {
                screen = (OScreen) textTranslationServiceRemote
                        .loadEntityTranslation(screen, loggedUser);

                String call = "top.FABS.Portlet.open({screenName:'" + screen.getName() + "',"
                        + "screenInstId:'" + openedScreenInstId + "'"
                        + ",viewPage:'" + screen.getViewPage()
                        + "',portletTitle:'" + screen.getHeaderTranslated()
                        + "',screenHeight:'',enableC2A:'" + true + "'});";
                logger.trace("Call: {}", call);
                return call;
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        }
        logger.debug("Returning");
        return "";
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public ScreenFunctionDTO getScreenFunctionDTO(OUser loggedUser, long dbid) {
        logger.debug("Entering");
        ScreenFunctionDTO screenFunction = null;
        List<FilterFieldDTO> filterFields = null;
        String query = " SELECT NEW com.unitedofoq.fabs.core.uiframework.navigationmenu.ScreenFunctionDTO("
                + " screenFunction.dbid,"
                + " oScreen.name,"
                + " oScreen.dbid,"
                + " screenMode.dbid,"
                + " filter.dbid,"
                + " filter.active,"
                + " filter.alwaysShow,"
                + " filter.showInsideScreen"
                + " )"
                + " FROM ScreenFunction screenFunction"
                + " LEFT JOIN screenFunction.screenMode screenMode"
                + " LEFT JOIN screenFunction.oscreen oScreen"
                + " LEFT JOIN oScreen.screenFilter filter"
                + " WHERE screenFunction.active=true "
                + " AND   screenFunction.inActive=false "
                + " AND   screenFunction.dbid=" + dbid;
        logger.debug("Query: {}",query);
        try {
            screenFunction = (ScreenFunctionDTO) oem.executeEntityQuery(query, loggedUser);

            if (screenFunction.getoScreenFilterDBID() != 0) {
                query = " SELECT NEW com.unitedofoq.fabs.core.filter.FilterFieldDTO("
                        + " filterField.dbid,"
                        + " filterField.filter.dbid,"
                        + " filterField.fieldValue,"
                        + " filterField.fieldExpression,"
                        + " filterField.fromTo,"
                        + " filterField.fieldIndex,"
                        + " filterField.fieldValueTo,"
                        + " filterField.operator,"
                        + " fieldDD.dbid"
                        + " )"
                        + " FROM FilterField filterField"
                        + " LEFT JOIN  filterField.fieldDD fieldDD"
                        + " WHERE  filterField.inActive=false"
                        + " AND filterField.filter.dbid=" + screenFunction.getoScreenFilterDBID();
                logger.debug("Query: {}",query);
                filterFields = new ArrayList<FilterFieldDTO>((Vector<FilterFieldDTO>) oem.executeEntityListQuery(query, loggedUser));
                screenFunction.setoScreenFilterFilelds(filterFields);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        } finally {
            logger.debug("Returning");
            return screenFunction;
        }
    }
}
