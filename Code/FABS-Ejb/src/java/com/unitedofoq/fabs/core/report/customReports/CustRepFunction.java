/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.report.customReports;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.unitedofoq.fabs.core.function.OFunction;

/**
 *
 * @author Melad
 */
@Entity
public class CustRepFunction extends OFunction {

    // <editor-fold defaultstate="collapsed" desc="customReport">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private CustomReport customReport;

    public CustomReport getCustomReport() {
        return customReport;
    }

    public void setCustomReport(CustomReport customReport) {
        this.customReport = customReport;
    }

    public String getCustomReportDD() {
        return "CustomReportFunction_customReport";
    }
    // </editor-fold >
}
