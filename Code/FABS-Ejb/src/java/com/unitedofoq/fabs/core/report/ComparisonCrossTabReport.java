package com.unitedofoq.fabs.core.report;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;

@Entity
@VersionControlSpecs(omoduleFieldExpression = "omodule")
@DiscriminatorValue(value = "COMPARISON")
public class ComparisonCrossTabReport extends CrossTabReport {

    @Transient
    // <editor-fold defaultstate="collapsed" desc="comparisonValue1">
    private BaseEntity comparisonValue1;

    public BaseEntity getComparisonValue1() {
        return comparisonValue1;
    }

//    public String getComparisonValue1DD() {
//        return "ComparisonCrossTabReport_comparisonValue1";
//    }

    public void setComparisonValue1(BaseEntity comparisonValue1) {
        this.comparisonValue1 = comparisonValue1;
    }
    //</editor-fold>

    @Transient
    // <editor-fold defaultstate="collapsed" desc="comparisonValue2">
    private BaseEntity comparisonValue2;

//    public String getComparisonValue2DD() {
//        return "ComparisonCrossTabReport_comparisonValue2";
//    }

    public BaseEntity getComparisonValue2() {
        return comparisonValue2;
    }

    public void setComparisonValue2(BaseEntity comparisonValue2) {
        this.comparisonValue2 = comparisonValue2;
    }
    //</editor-fold>

}
