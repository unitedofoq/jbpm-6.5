/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.portal;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author mibrahim
 */
@Entity
@ReadOnly
public class PortalPageFunctionLite implements Serializable{
    @Id
    private long functionDBID;
    private long portalPageDBID;
    private long odataTypeDBID;
    private long screenModeDBID;

    public long getFunctionDBID() {
        return functionDBID;
    }

    public void setFunctionDBID(long functionDBID) {
        this.functionDBID = functionDBID;
    }

    public long getPortalPageDBID() {
        return portalPageDBID;
    }

    public void setPortalPageDBID(long portalPageDBID) {
        this.portalPageDBID = portalPageDBID;
    }

    public long getOdataTypeDBID() {
        return odataTypeDBID;
    }

    public void setOdataTypeDBID(long odataTypeDBID) {
        this.odataTypeDBID = odataTypeDBID;
    }

    public long getScreenModeDBID() {
        return screenModeDBID;
    }

    public void setScreenModeDBID(long screenModeDBID) {
        this.screenModeDBID = screenModeDBID;
    }
    
    
}
