package com.unitedofoq.fabs.core.report.customReports;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Entity;

@Entity
public class CustomReportTranslation extends BaseEntityTranslation {

    private String reportName;

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

}
