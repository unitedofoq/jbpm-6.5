/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.portal;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.udc.UDC;

/**
 *
 * @author htawab
 */
@Entity
@ParentEntity(fields={"portalPage"})
@DiscriminatorValue(value = "PortalPage")
@ChildEntity(fields={"functionImage", "menuFunction"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class PortalPageFunction extends OFunction 
{
    // <editor-fold defaultstate="collapsed" desc="portalPage">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    OPortalPage portalPage;
    public String getPortalPageDD() {
        return "PortalPageFunction_portalPage";
    }
    public OPortalPage getPortalPage() {
        return portalPage;
    }

    public void setPortalPage(OPortalPage portalPage) {
        this.portalPage = portalPage;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="ODataPage">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ODataType odataType=null;

    public String getOdataTypeDD()   {   return "PortalPageFunction_odataType";  }

    public ODataType getOdataType() {
        return odataType;
    }

    public void setOdataType(ODataType odataType) {
        this.odataType = odataType;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="ScreenMode">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC screenMode = null;

    public UDC getScreenMode() {
        return screenMode;
    }

    public String getScreenModeDD() {
        return "PortalPageFunction_screenMode";
    }

    public void setScreenMode(UDC screenMode) {
        this.screenMode = screenMode;
    }

    // </editor-fold>
}
