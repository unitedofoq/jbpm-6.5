/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.comunication;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.alert.entites.dto.AlertDTO;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author aelzaher
 */
@Local
public interface SMSSenderLocal {

    public boolean sendSMS(String message, String recipient, String reciverEMail, String sender, OEntity actOnEntity, long entityDBDI, OUser loggedUser);

}
