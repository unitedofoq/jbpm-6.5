/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.function;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserMenu;
import com.unitedofoq.fabs.core.webservice.OWebService;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author melsayed
 */
@Local
public interface OFunctionServiceRemote {

    public boolean isUserExit();
    
    public void setUserExit(boolean userExit);
    
    public BaseEntity getCurrentEntityObject();
    
    public void setCurrentEntityObject(BaseEntity currentEntityObject);
    
    public OFunction getOFunction(Long dbid, OUser loggedUser);

    public OFunctionResult runFunction(
            OFunction function, ODataMessage oDM, OFunctionParms params, OUser loggedUser);

    public OFunctionResult clearAllCaches(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public List<OFunction> getMenuFunctions(String menuID, OUser user);

    public List<OMenu> getMenuSubMenus(String menuID, OUser user);

    public List<UserMenu> getUserMenus(OUser user);

    public com.unitedofoq.fabs.core.function.OFunctionResult runWebServiceFunction(OWebService webService, com.unitedofoq.fabs.core.datatype.ODataMessage oDM, com.unitedofoq.fabs.core.function.OFunctionParms params, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    /**
     * Refer to {@link OFunctionService#validateJavaFunctionODM(ODataMessage, OUser, java.lang.Class[])
     * }
     */
    public OFunctionResult validateJavaFunctionODM(
            ODataMessage odm, OUser loggedUser, Class... dataElementsClasses);

    /**
     * Refer to {@link OFunctionService#validateJavaFunctionODM(ODataMessage, OUser, boolean, java.lang.Class[])
     * }
     */
    public OFunctionResult validateJavaFunctionODM(
            ODataMessage odm, OUser loggedUser, boolean allowNulls, Class... dataElementsClasses);

    public OFunctionResult validateValidationJavaFunctionODM(
            ODataMessage odm, OUser loggedUser, Class... dataElementsClasses);

    public OFunctionResult constructOFunctionResultFromError(
            String errorUserMessageName, OUser loggedUser);

    public OFunctionResult constructOFunctionResultFromError(String errorUserMessageName, Exception ex, OUser loggedUser);

    public String getOFunctionImagePath(OFunction function, OUser loggedUser);

    public String getOMenuImagePath(OMenu menu, OUser loggedUser);

    public List<OMenuFunction> getUserMenuFunctions(OUser user, OMenu parentMenu, int displayLevel, OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult validateCreateOMenu(com.unitedofoq.fabs.core.datatype.ODataMessage oDM, com.unitedofoq.fabs.core.function.OFunctionParms params, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult validateUpdateOMenu(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms params, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult constructHierCodeForExistingMenus(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public java.util.List<com.unitedofoq.fabs.core.function.OMenu> getUserMenuSubMenus(com.unitedofoq.fabs.core.security.user.OUser user, com.unitedofoq.fabs.core.function.OMenu parentMenu, int displayLevel, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public String getOMenuLargeImagePath(OMenu menu, OUser loggedUser);

    public List<Object> getUserMenuFunctions(long menuDBID, OUser loggedUser);

    public java.lang.String getFunctionName(long functionDBID, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

//    public void openConnection(com.unitedofoq.fabs.core.security.user.OUser loggedUser);
//
//
//    public void closeConnection(com.unitedofoq.fabs.core.security.user.OUser loggedUser);
    public java.lang.String getFunctionsType(long functionDBID, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public Object getPortalPageData(long functionDBID, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

}
