package com.unitedofoq.fabs.core.dd;

import java.lang.reflect.Field;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;

@Local
public interface DDServiceRemote {

    /**
     *
     * @param ddName
     * @param loggedUser
     * @return
     */
    public DD getDD(String ddName, OUser loggedUser);

    public String getDDName(BaseEntity entity, String fieldExpression);

    public OFunctionResult validateDD (ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult ddDataValidation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult validateDDnotInEntity (ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult validateDDnotUsed(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult createFieldDefaultDD(String ddName, Field field, OEntity oEntity,
            OUser loggedUser);

    public OFunctionResult clearDDCache(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    
    public OFunctionResult clearMyDDCache(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult onDDCachedUpdate(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult onDDCachedRemove(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult preDDCachedUpdate(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public java.lang.String getDDLabel(java.lang.String name, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public java.lang.String getDDReportLabel(java.lang.String name, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult createFieldDefaultDD(java.lang.String ddName, java.lang.reflect.Field field, com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO oEntity, com.unitedofoq.fabs.core.security.user.OUser loggedUser);
    
    public OFunctionResult checkIfHelpTextNull(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

}
