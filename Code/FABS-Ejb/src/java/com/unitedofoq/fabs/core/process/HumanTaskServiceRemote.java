/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.process;

import com.unitedofoq.fabs.bpm.model.Task;
import com.unitedofoq.fabs.bpm.model.status.TaskStatus;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.process.jBPM.TaskInstance;
import com.unitedofoq.fabs.core.security.user.Inbox;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

/**
 *
 * @author melsayed
 * @author Abu Bakr
 */
@Local
public interface HumanTaskServiceRemote {

    public void claimTask(Task task, OUser loggedUser);

    public Boolean sendEMailForTask(Task task, OUser loggedUser);

    Map<Long, List<Inbox>> getUserInboxOnLoad(OUser loggedUser, int page, int pageSize, String sortedField, String sortingOrder, Map<String, String> filters);

    public void releaseAndClaimTask(Task task, OUser loggedUser);

    public List<TaskInstance> getProcessTasks(String processInstanceId, OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult clearProcessCache(com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public ODataMessage getTaskInputs(Task task, OUser loggedUser, String functionDataTypeName);

    public void completeTask(Task task, ODataMessage output, OUser loggedUser);

    public ODataType getDataType(String dataTypeName, OUser loggedUser);

    public void startTask(Task task, OUser loggedUser);

    public Task getTaskForCurrentNodeInstance(long processInstanceId, long nodeInstanceId);

    public void claimAndStartTask(Task task, OUser loggedUser);

    public TaskStatus getTaskStatus(long taskId);

    public void createOutbox(Inbox inbox, ODataMessage output, OUser loggedUser);

    public Task getTaskById(long id);

    public List<String> getTaskPotentialOwnersOfTypeRole(long taskId);

    public OProcess getBusinessOProcess(String processName, String packageName, OUser loggedUser);

}
