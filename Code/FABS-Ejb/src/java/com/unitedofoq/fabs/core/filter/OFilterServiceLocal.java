/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.filter;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import java.util.List;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author mmohamed
 */
@Local
public interface OFilterServiceLocal {

    public List<String> getFilterConditions(OFilter filter, UserSessionInfo userSesssionInfo, OUser loggedUser) 
            throws FilterDDRequiredException;
    
    public List<String> getFilterConditionsForProcess(OFilter filter, OUser loggedUser);
    
    public List<String> parseDDLookupFilter(String filterValue, OUser loggedUser);
    
    public OFunctionResult validateOneEntityFilterOnScreen(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    
}
