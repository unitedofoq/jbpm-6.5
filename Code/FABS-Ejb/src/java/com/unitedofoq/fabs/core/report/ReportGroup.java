/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.report;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.module.OModule;

/**
 *
 * @author melsayed
 */
@Entity
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class ReportGroup extends ObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OModule omodule;

    public String getOmoduleDD(){
        return "ReportGroup_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="name">
    @Column(nullable=false, unique=true)
    private String name;
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    public String getNameDD() {
        return "ReportGroup_name";
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="reports">
    @OneToMany(mappedBy = "reportGroup")
    private List<OReport> reports;
    /**
     * @return the reports
     */
    public List<OReport> getReports() {
        return reports;
    }
    public String getReportsDD() {
        return "ReportGroup_reports";
    }

    /**
     * @param reports the reports to set
     */
    public void setReports(List<OReport> reports) {
        this.reports = reports;
    }
    //</editor-fold>
    
}
