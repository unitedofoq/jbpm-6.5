/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.entitysetup;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.List;

/**
 *
 * @author arezk
 */
@Local
public interface EntityServiceLocal {
//    public ODataMessage constructODataMessage(ODataType oDataType, BaseEntity baseEntity) ;

    ODataMessage constructEntityODataMessage(BaseEntity entity, OUser user);
    ODataMessage constructEntityListODataMessage(BaseEntity entity, List<String> filterConds, OUser loggedUser); 
}
