/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author rsamir
 */
@Entity
@ParentEntity(fields = {"screenField"})
//@ChildEntity(fields = {"dimmedFields"})
public class ScreenFieldValue extends BaseEntity{

    private String value;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private ScreenField screenField;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    public String getValueDD() {
        return "ScreenFieldValue_value";
    }

    public ScreenField getScreenField() {
        return screenField;
    }

    public void setScreenField(ScreenField screenField) {
        this.screenField = screenField;
    }
    
    public String getScreenFieldDD() {
        return "ScreenFieldValue_screenField";
    }
    
    @OneToMany(mappedBy="screenFieldValue", fetch= FetchType.LAZY)
    protected List<DimmedField> dimmedFields = null;

    public List<DimmedField> getDimmedFields() {
        return dimmedFields;
    }

    public void setDimmedFields(List<DimmedField> dimmedFields) {
        this.dimmedFields = dimmedFields;
    }
    
}
