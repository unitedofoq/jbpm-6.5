/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.export;

import com.unitedofoq.fabs.core.content.EntityAttachmentServiceLocal;
import com.unitedofoq.fabs.core.content.EntityAttachments;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import javax.xml.bind.DatatypeConverter;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntityAttributeInit;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OMenuFunction;
import com.unitedofoq.fabs.core.report.customReports.CustomReport;
import com.unitedofoq.fabs.core.report.customReports.CustRepFunction;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.portal.PortalPagePortlet;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.uiframework.screen.wizard.WizardStep;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.unitedofoq.fabs.core.entitybase.NotExportable;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 *
 * @author aelzaher
 */
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.export.DataExportRemote",
        beanInterface = DataExportRemote.class)
@Stateless
public class DataExport implements DataExportRemote {

    //<editor-fold defaultstate="collapsed" desc="Data Export">
    @EJB
    OEntityManagerRemote oem;
    @EJB
    EntityAttachmentServiceLocal entityAttachmentService;
    final static Logger logger = LoggerFactory.getLogger(DataExport.class);
    public static Hashtable<String, List<String>> specialEntitiesFieldExp;


    private final static HashMap<String, Boolean> isTenantSQLServer = new HashMap<String, Boolean>();

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public boolean isOracle(OUser loggedUser) {
        String tenantName = loggedUser.getTenant().getName();
        Boolean isSQLServer = isTenantSQLServer.get(tenantName);

        Connection connection = null;
        
        if (isSQLServer == null) {
            try {
                DataSource dataSource = ((DataSource) new InitialContext().lookup("jdbc/"
                        + loggedUser.getTenant().getPersistenceUnitName()));
                connection = dataSource.getConnection();

                if (connection.getMetaData().getDatabaseProductName().toLowerCase().contains("sql server")) {
                    isSQLServer = true;
                } else {
                    isSQLServer = false;
                }
                isTenantSQLServer.put(tenantName, isSQLServer);
            } catch (Exception e) {
                logger.error("Exception thrown", e);
                isSQLServer = false;
            }
            finally {

                try {
                    connection.close();
                } catch (Exception exception) {
                    logger.info(exception.getMessage());
                }
            }
        }
        return !isSQLServer;
    }



    public List<String> getEntitySpecialFieldObjects(BaseEntity entity) {
        if (specialEntitiesFieldExp == null || specialEntitiesFieldExp.isEmpty()) {
            specialEntitiesFieldExp = new Hashtable<String, List<String>>();
            specialEntitiesFieldExp.put(ScreenField.class.getSimpleName(), Arrays.asList(new String[]{"dd"}));
            specialEntitiesFieldExp.put(WizardStep.class.getSimpleName(), Arrays.asList(new String[]{"stepScreen"}));
            specialEntitiesFieldExp.put(PortalPagePortlet.class.getSimpleName(), Arrays.asList(new String[]{"screen"}));
            specialEntitiesFieldExp.put("OReport",
                    Arrays.asList(new String[]{"userFilters", "reportGroup", "omodule",
                        "oactOnEntity", "logo", "layoutType", "inputs", "customFilterScreen"}));
            specialEntitiesFieldExp.put("ReportFunction", Arrays.asList(new String[]{"odataType"}));

            specialEntitiesFieldExp.put("ORepTemplDS",
                    Arrays.asList(new String[]{"fields", "fieldGroup", "dsEntity"}));
            specialEntitiesFieldExp.put("ORepTemplDSField",
                    Arrays.asList(new String[]{"udcType", "oactOnEntity", "dd", "calendarFormat"}));

            specialEntitiesFieldExp.put("OTemplRep",
                    Arrays.asList(new String[]{"fields", "orepTemplDS", "orepTempl"}));
            specialEntitiesFieldExp.put("ORepTemplDS",
                    Arrays.asList(new String[]{"fields", "fieldGroup", "dsEntity"}));
            specialEntitiesFieldExp.put("ORepTemplDSField",
                    Arrays.asList(new String[]{"udcType", "oactOnEntity", "dd", "calendarFormat"}));

            specialEntitiesFieldExp.put("ObjectAccessPrivilege", Arrays.asList(new String[]{"rolePrivileges"}));
            specialEntitiesFieldExp.put("OEntity", Arrays.asList(new String[]{"ODataType"}));
            specialEntitiesFieldExp.put(CustomReport.class.getSimpleName(),
                    Arrays.asList(new String[]{"EntityAttachments", "customReportFilter", "CustomReportFunction", "OMenuFunction"}));
        }
        return specialEntitiesFieldExp.get(entity.getClassName());
    }

    @Override
    public String getEntitySql(BaseEntity entity, OUser loggedUser) {
        logger.debug("Entering");
        StringBuilder sql = new StringBuilder();
        Stack<BaseEntity> stack = new Stack<BaseEntity>();
        stack.push(entity);
        exportEntityDD(entity, sql, loggedUser);
        exportEntityIntializations(entity, sql, loggedUser);
        while (!stack.empty()) {

            BaseEntity currentEntity = stack.pop();

            //<editor-fold defaultstate="collapsed" desc="get Entity SQL">
            Class entityClass = currentEntity.getClass();
            Hashtable<String, List<Field>> tableFields = BaseEntity.getClassFieldsInSameTable(entityClass);
            List<String> discriminator = BaseEntity.getDiscrimnatorFieldTableData(entityClass);
            discriminator = discriminator.isEmpty() ? null : discriminator;
            for (Map.Entry<String, List<Field>> entry : tableFields.entrySet()) {
                String tableName = entry.getKey();
                List<Field> fields = entry.getValue();
                sql.append(getRemoveCode(tableName, fields, currentEntity)).append("\n");
                sql.append(getCreateCode(tableName, fields, currentEntity, discriminator, loggedUser)).append("\n");
            }
            try {
                if (currentEntity.getTranslatableFields() != null && !currentEntity.getTranslatableFields().isEmpty()) {
                    Class translationClass = null;
                    try {
                        translationClass = Class.forName(currentEntity.getClass().getName() + "Translation");
                    } catch (Exception ex) {
                        logger.error("Exception thrown",ex);
                    }
                    if (translationClass != null) {
                        String query = " SELECT entity FROM " + currentEntity.getClass().getSimpleName() + "Translation "
                                + "entity WHERE entity.entityDBID=" + currentEntity.getDbid() + "L";
                        logger.trace("Query: {}", query);
                        Object result = oem.executeEntityListQuery(query, oem.getSystemUser(loggedUser));
                        if (result != null) {
                            List<BaseEntityTranslation> entityTranslation = (List<BaseEntityTranslation>) result;
                            if (!entityTranslation.isEmpty()) {
                                tableFields = BaseEntity.
                                        getClassFieldsInSameTable(entityTranslation.get(0).getClass());
                                for (Map.Entry<String, List<Field>> entry : tableFields.entrySet()) {
                                    String tableName = entry.getKey();
                                    List<Field> fields = entry.getValue();
                                    sql.append(getRemoveCode(tableName, fields, entityTranslation).replace(delimiter, ""));
                                    sql.append(getCreateCode(tableName, fields, entityTranslation,
                                            discriminator, loggedUser).replace(delimiter, ""));
                                }
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Add Children">
            List<String> childFields = BaseEntity.getChildEntityFieldName(currentEntity.getClass().getName());
            for (int i = 0; i < childFields.size(); i++) {
                try {
                    Object objectToAdd = currentEntity.invokeGetter(childFields.get(i));
                    if (objectToAdd != null) {
                        if (objectToAdd instanceof List) {
                            List objectsToAdd = (List) objectToAdd;
                            for (int j = 0; j < objectsToAdd.size(); j++) {
                                Object object = objectsToAdd.get(j);
                                if(!(object instanceof NotExportable)){
                                    stack.push((BaseEntity) object);
                                }
                            }
                        } else {
                            if(!(objectToAdd instanceof NotExportable)){
                                
                                stack.push((BaseEntity) objectToAdd);
                            }
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Add Special Entity">
            List<String> fieldExps = getEntitySpecialFieldObjects(currentEntity);
            CustRepFunction tempFunction = null;
            if (fieldExps != null) {
                for (int i = 0; i < fieldExps.size(); i++) {
                    String fieldExp = fieldExps.get(i);
                    try {
                        Object objectToAdd = currentEntity.invokeGetter(fieldExp);
                        if (objectToAdd != null) {
                            if (objectToAdd instanceof List) {
                                List objectsToAdd = (List) objectToAdd;
                                for (int j = 0; j < objectsToAdd.size(); j++) {
                                    Object object = objectsToAdd.get(j);
                                    stack.push((BaseEntity) object);
                                }
                            } else {
                                stack.push((BaseEntity) objectToAdd);
                            }
                        } else if (fieldExp.equals("ODataType")
                                && currentEntity.getClass().getSimpleName().equals("OEntity")) {
                            String entityName = ((OEntity) currentEntity).getEntityClassName();
                            BaseEntity entityOdataType = oem.loadEntity("ODataType",
                                    Collections.singletonList("name = '" + entityName + "'"), null, loggedUser);
                            stack.push((BaseEntity) entityOdataType);
                        } else if (currentEntity.getClass().getSimpleName().equals("CustomReport")) {
                            if (fieldExp.equals("EntityAttachments")) {
                                List<String> conditions = new ArrayList<String>();
                                conditions.add("EntityDBID = '" + currentEntity.getDbid() + "'");
                                List<EntityAttachments> eodt = (List<EntityAttachments>) (EntityAttachments) oem.loadEntityList("EntityAttachments", conditions, null,null, loggedUser);
                                
                                EntityAttachments attachment = new EntityAttachments();
                                if(eodt != null && !eodt.isEmpty()){
                                    attachment = eodt.get(0);
                                } 
                                stack.push((BaseEntity) eodt);
                            } else if (fieldExp.equals("CustomReportFunction")) {
                                BaseEntity entityOdataType = oem.loadEntity(CustRepFunction.class.getSimpleName(),
                                        Collections.singletonList("customReport.dbid= '" + currentEntity.getDbid() + "'"), null, loggedUser);
                                stack.push((BaseEntity) entityOdataType);
                                tempFunction = (CustRepFunction) entityOdataType;
                            } else if (fieldExp.equals("OMenuFunction")) {
                                BaseEntity entityOdataType = oem.loadEntity(OMenuFunction.class.getSimpleName(),
                                        Collections.singletonList("ofunction.dbid= '" + tempFunction.getDbid() + "'"), null, loggedUser);
                                stack.push((BaseEntity) entityOdataType);
                            }
                        }
                    } catch (Exception ex) {
                        logger.error("Exception thrown", ex);

                    }
                }
            }
            //</editor-fold>

        }
        logger.debug("Returning with: {}",sql.toString());
        return sql.toString();
    }


    @Override
    public InputStream getEntitySqlAsInputStream(BaseEntity entity, OUser loggedUser) {
        logger.debug("Entering");
        InputStream inputStream = null;
        try {
              inputStream = new ByteArrayInputStream( getEntitySql(entity, loggedUser).getBytes());
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        logger.debug("Returning");
        return inputStream;
    }

    @Override
    public String getEntitySql(List<BaseEntity> entity, OUser loggedUser) {
        logger.debug("Entering");
        StringBuilder sql = new StringBuilder();
        Stack<BaseEntity> stack = new Stack<BaseEntity>();
        for (int i = 0; i < entity.size(); i++) {
            stack.push(entity.get(i));
        }
        while (!stack.empty()) {

            BaseEntity currentEntity = stack.pop();

            //<editor-fold defaultstate="collapsed" desc="get Entity SQL">
            Class entityClass = currentEntity.getClass();
            Hashtable<String, List<Field>> tableFields = BaseEntity.getClassFieldsInSameTable(entityClass);
            List<String> discriminator = BaseEntity.getDiscrimnatorFieldTableData(entityClass);
            discriminator = discriminator.isEmpty() ? null : discriminator;
            for (Map.Entry<String, List<Field>> entry : tableFields.entrySet()) {
                String tableName = entry.getKey();
                List<Field> fields = entry.getValue();
                sql.append(getRemoveCode(tableName, fields, currentEntity)).append("\n");
                sql.append(getCreateCode(tableName, fields, currentEntity, discriminator, loggedUser)).append("\n");
            }
            try {
                if (currentEntity.getTranslatableFields() != null && !currentEntity.getTranslatableFields().isEmpty()) {
                    Class translationClass = null;
                    try {
                        translationClass = Class.forName(currentEntity.getClass().getName() + "Translation");
                    } catch (Exception ex) {
                        logger.error("Exception thrown",ex);
                    }
                    if (translationClass != null) {
                        String query = " SELECT entity FROM " + currentEntity.getClass().getSimpleName() + "Translation "
                                + "entity WHERE entity.entityDBID=" + currentEntity.getDbid() + "L";
                        logger.trace("Query: {}", query);
                        Object result = oem.executeEntityListQuery(query, oem.getSystemUser(loggedUser));
                        if (result != null) {
                            List<BaseEntityTranslation> entityTranslation = (List<BaseEntityTranslation>) result;
                            if (!entityTranslation.isEmpty()) {
                                tableFields = BaseEntity.
                                        getClassFieldsInSameTable(entityTranslation.get(0).getClass());
                                for (Map.Entry<String, List<Field>> entry : tableFields.entrySet()) {
                                    String tableName = entry.getKey();
                                    List<Field> fields = entry.getValue();
                                    sql.append(getRemoveCode(tableName, fields, entityTranslation).replace(delimiter, ""));
                                    sql.append(getCreateCode(tableName, fields, entityTranslation,
                                            discriminator, loggedUser).replace(delimiter, ""));
                                }
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                logger.error("Exception thrown",ex);
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Add Children">
            List<String> childFields = BaseEntity.getChildEntityFieldName(currentEntity.getClass().getName());
            for (int i = 0; i < childFields.size(); i++) {
                try {
                    Object objectToAdd = currentEntity.invokeGetter(childFields.get(i));
                    if (objectToAdd != null) {
                        if (objectToAdd instanceof List) {
                            List objectsToAdd = (List) objectToAdd;
                            for (int j = 0; j < objectsToAdd.size(); j++) {
                                Object object = objectsToAdd.get(j);
                                stack.push((BaseEntity) object);
                            }
                        } else {
                            stack.push((BaseEntity) objectToAdd);
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Exception thrown",ex);
                }
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Add Special Entity">
            List<String> fieldExps = getEntitySpecialFieldObjects(currentEntity);
            if (fieldExps != null) {
                for (int i = 0; i < fieldExps.size(); i++) {
                    String fieldExp = fieldExps.get(i);
                    try {
                        Object objectToAdd = currentEntity.invokeGetter(fieldExp);
                        if (objectToAdd != null) {
                            if (objectToAdd instanceof List) {
                                List objectsToAdd = (List) objectToAdd;
                                for (int j = 0; j < objectsToAdd.size(); j++) {
                                    Object object = objectsToAdd.get(j);
                                    stack.push((BaseEntity) object);
                                }
                            } else {
                                stack.push((BaseEntity) objectToAdd);
                            }
                        } else if (fieldExp.equals("ODataType")
                                && currentEntity.getClass().getSimpleName().equals("OEntity")) {
                            String entityName = ((OEntity) currentEntity).getEntityClassName();
                            BaseEntity entityOdataType = oem.loadEntity("ODataType",
                                    Collections.singletonList("name = '" + entityName + "'"), null, loggedUser);
                            stack.push((BaseEntity) entityOdataType);
                        }
                    } catch (Exception ex) {
                        logger.error("Exception thrown",ex);
                    }
                }
            }
            //</editor-fold>

        }
        logger.debug("Returning with: {}",sql.toString());
        return sql.toString();
    }

   

    @Override
    public InputStream getEntitySqlAsInputStream(List<BaseEntity> entity, OUser loggedUser) {
        logger.debug("Entering");
        InputStream inputStream = null;
        try {
            inputStream = new ByteArrayInputStream( getEntitySql(entity, loggedUser).getBytes());

        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        logger.debug("Returning");
        return inputStream;
    }
    //</editor-fold>
    // For regex do not use in delimiter <([{\^-=$!|]})?*+.>
    public static final String delimiter = "/#@#/";

    //<editor-fold defaultstate="collapsed" desc="Convert To SQL Code">
    @Override
    public String getUpdateCode(String tableName, List<Field> fields, List entities, OUser loggedUser) {
        logger.debug("Entering");
        if (entities.isEmpty()) {
            logger.debug("Returning with \" \" as entities is empty");
            return "";
        }
        StringBuilder code = new StringBuilder();
        String updateCode;
        for (int i = 0; i < entities.size(); i++) {
            updateCode = "UPDATE " + tableName + " SET ";
            for (int j = 0; j < fields.size(); j++) {
                if (j != 0) {
                    updateCode += ",";
                }
                String value = "NULL";
                try {
                    if (entities.get(i) instanceof BaseEntity) {
                        value = getSQLValue(((BaseEntity) entities.get(i)).invokeGetter(fields.get(j)), loggedUser);
                    } else if (entities.get(i) instanceof BaseEntityTranslation) {
                        value = getSQLValue(((BaseEntityTranslation) entities.get(i)).invokeGetter(fields.get(j)), loggedUser);
                    }
                } catch (Exception ex) {
                    logger.error("Exception thrown: ",ex);
                    value = "NULL";
                }

                String fieldName = BaseEntity.getFieldColumnName(fields.get(j));
                if (fieldName == null || fieldName.isEmpty()) {
                    fieldName = fields.get(j).getName();
                }
                updateCode += fieldName + "="
                        + value + " ";
            }
            code.append(updateCode);
            if (entities.get(i) instanceof BaseEntity) {
                code.append(" WHERE DBID='").append(((BaseEntity) entities.get(i)).getDbid());
            } else if (entities.get(i) instanceof BaseEntityTranslation) {
                BaseEntityTranslation entity = (BaseEntityTranslation) entities.get(i);
                code.append(" WHERE ENTITYDBID='").append(entity.getEntityDBID());
                code.append("' AND LANGUAGE_DBID='").append(entity.getLanguage().getDbid());
            }
            code.append("';" + delimiter + "\n");
        }
        logger.debug("Returning with: {}",code.toString());
        return code.toString();
    }

    @Override
    public String getRemoveCode(String tableName, List<Field> fields, List entities) {
        logger.debug("Entering");
        if (entities.isEmpty()) {
            logger.debug("Returning with \" \" as entities is empty");
            return "";
        }
        if (true) {
            String code = "";
            for (int i = 0; i < entities.size(); i++) {
                code += getRemoveCode(tableName, fields, entities.get(i)) + "\n";
            }
            return code;
        }
        String deleteCode;
        deleteCode = "DELETE FROM " + tableName + " WHERE ";
        if (entities.get(0) instanceof BaseEntity) {
            deleteCode += "DBID";
        } else if (entities.get(0) instanceof BaseEntityTranslation) {
            deleteCode += "CONCAT(ENTITYDBID,' ',LANGUAGE_DBID)";
        }
        deleteCode += " IN (";
        for (int i = 0; i < entities.size(); i++) {
            if (i != 0) {
                deleteCode += ",";
            }
            if (entities.get(i) instanceof BaseEntity) {
                deleteCode += ((BaseEntity) entities.get(i)).getDbid();
            } else if (entities.get(i) instanceof BaseEntityTranslation) {
                BaseEntityTranslation entity = (BaseEntityTranslation) entities.get(i);
                deleteCode += "CONCAT(" + entity.getEntityDBID() + ",' ', "
                        + entity.getLanguage().getDbid() + ")";
            }
        }
        deleteCode += ");" + delimiter + "\n";
        logger.debug("Returning with: ",deleteCode);
        return deleteCode;
    }

    @Override
    public String getCreateCode(String tableName, List<Field> fields, List entities,
            List<String> discriminatorData, OUser loggedUser) {
        logger.debug("Entering");
        if (entities.isEmpty()) {
            logger.debug("Returning with \" \" as entities is empty");
            return "";
        }
        StringBuilder code = new StringBuilder();
        String createCode;
        createCode = "INSERT INTO " + tableName + " (";
        if (discriminatorData != null && !discriminatorData.isEmpty()
                && discriminatorData.get(0).equalsIgnoreCase(tableName)) {
            createCode += "" + discriminatorData.get(1) + ",";
        }
        for (int i = 0; i < fields.size(); i++) {
            if (i != 0) {
                createCode += ",";
            }
            String fieldName = BaseEntity.getFieldColumnName(fields.get(i));
            if (fieldName == null || fieldName.isEmpty()) {
                fieldName = fields.get(i).getName();
            }
            createCode += fieldName;
        }
        createCode += ") VALUES (";
        if (discriminatorData != null && !discriminatorData.isEmpty()
                && discriminatorData.get(0).equalsIgnoreCase(tableName)) {
            createCode += "'" + discriminatorData.get(2) + "',";
        }
        for (int j = 0; j < entities.size(); j++) {
            code.append(createCode).append(getFieldSQLValue(entities.get(j), fields, loggedUser))
                    .append(");" + delimiter + "\n");
        }
        logger.debug("Returning with: {}",code.toString());
        return code.toString();
    }

    @Override
    public String getUpdateCode(String tableName, List<Field> fields, Object entity, OUser loggedUser) {
        logger.debug("Entering");
        if (entity == null) {
            logger.debug("Returning with \" \" as entities is empty");
            return "";
        }
        StringBuilder code = new StringBuilder();
        String updateCode;
        updateCode = "UPDATE " + tableName + " SET ";
        for (int j = 0; j < fields.size(); j++) {
            if (j != 0) {
                updateCode += ",";
            }
            String value = "NULL";
            try {
                if (entity instanceof BaseEntity) {
                    value = getSQLValue(((BaseEntity) entity).invokeGetter(fields.get(j)), loggedUser);
                } else if (entity instanceof BaseEntityTranslation) {
                    value = getSQLValue(((BaseEntityTranslation) entity).invokeGetter(fields.get(j)), loggedUser);
                }
            } catch (Exception ex) {
                value = "NULL";
            }

            String fieldName = BaseEntity.getFieldColumnName(fields.get(j));
            if (fieldName == null || fieldName.isEmpty()) {
                fieldName = fields.get(j).getName();
            }
            updateCode += fieldName + "="
                    + value + " ";
        }
        code.append(updateCode);
        if (entity instanceof BaseEntity) {
            code.append(" WHERE DBID='").append(((BaseEntity) entity).getDbid());
        } else if (entity instanceof BaseEntityTranslation) {
            BaseEntityTranslation entityTranslation = (BaseEntityTranslation) entity;
            code.append(" WHERE ENTITYDBID='").append(entityTranslation.getEntityDBID());
            code.append("' AND LANGUAGE_DBID='").append(entityTranslation.getLanguage().getDbid());
        }
        code.append("';");
        logger.debug("Returning with: ",code.toString());
        return code.toString();
    }

    @Override
    public String getRemoveCode(String tableName, List<Field> fields, Object entity) {
        logger.debug("Entering");
        if (entity == null) {
            logger.debug("Returning with \" \" as entities is empty");
            return "";
        }
        String deleteCode;
        deleteCode = "DELETE FROM " + tableName + " WHERE ";
        if (entity instanceof BaseEntity) {
            deleteCode += "DBID = " + ((BaseEntity) entity).getDbid();
        } else if (entity instanceof BaseEntityTranslation) {
            BaseEntityTranslation entityTranslation = (BaseEntityTranslation) entity;
            deleteCode += "ENTITYDBID = " + entityTranslation.getEntityDBID()
                    + " AND LANGUAGE_DBID = " + entityTranslation.getLanguage().getDbid();
        }
        deleteCode += ";";
        logger.debug("Returning with: ",deleteCode);
        return deleteCode;
    }

    @Override
    public String getCreateCode(String tableName, List<Field> fields, Object entity,
            List<String> discriminatorData, OUser loggedUser) {
        logger.debug("Entering");
        if (entity == null) {
            logger.debug("Returning with \" \" as entities is empty");
            return "";
        }
        StringBuilder code = new StringBuilder();
        String createCode;
        createCode = "INSERT INTO " + tableName + " (";
        if (discriminatorData != null && !discriminatorData.isEmpty()
                && discriminatorData.get(0).equalsIgnoreCase(tableName)) {
            createCode += "" + discriminatorData.get(1) + ",";
        }
        for (int i = 0; i < fields.size(); i++) {
            if (i != 0) {
                createCode += ",";
            }
            String fieldName = BaseEntity.getFieldColumnName(fields.get(i));
            if (fieldName == null || fieldName.isEmpty()) {
                fieldName = fields.get(i).getName();
            }
            createCode += fieldName;
        }
        createCode += ") VALUES (";
        if (discriminatorData != null && !discriminatorData.isEmpty()
                && discriminatorData.get(0).equalsIgnoreCase(tableName)) {
            createCode += "'" + discriminatorData.get(2) + "',";
        }
        code.append(createCode).append(getFieldSQLValue(entity, fields, loggedUser))
                .append(");");
        logger.debug("Returning with: {}",code.toString());
        return code.toString();
    }

    @Override
    public String getFieldSQLValue(Object entity, List<Field> fields, OUser loggedUser) {
        logger.debug("Entering");
        String values = "";
        for (int i = 0; i < fields.size(); i++) {
            if (i != 0) {
                values += ",";
            }
            try {
                if (entity instanceof BaseEntity) {
                    values += getSQLValue(((BaseEntity) entity).invokeGetter(fields.get(i)), loggedUser);
                } else if (entity instanceof BaseEntityTranslation) {
                    values += getSQLValue(((BaseEntityTranslation) entity).invokeGetter(fields.get(i)), loggedUser);
                }
            } catch (Exception ex) {
                values += "NULL";
                logger.error("Exception thrown",ex);
                if(fields!=null && fields.get(i)!=null)
                logger.warn("Value Of Field {} Could Not Found" + fields.get(i).getName());
            }
        }
        logger.debug("Returning with: {}",values);
        return values;
    }

    @Override
    public Object getObjectFromSQLValue(Object value, Field field) {
        logger.debug("Entering");
        String fieldTypeName = field.getType().getName();
        if (fieldTypeName.contains("Boolean") || fieldTypeName.contains("boolean")) {
            logger.debug("Returning with: {}",("" + value).equals("1"));
            return ("" + value).equals("1");
        }
        if (fieldTypeName.contains("byte")) {
            logger.debug("Returning with: {}",value.toString().getBytes());
            return value.toString().getBytes();
        }
        logger.debug("Returning with: {}",value);
        return value;
    }

    @Override
    public String getSQLValue(Object object, OUser loggedUser) {
        logger.debug("Entering");
        if (object == null) {
            logger.debug("Returning with Null as object equals Null");
            return "NULL";
        }
        if (object instanceof Boolean) {
            logger.debug("Returning with: {}",(Boolean) object ? "'1'" : "'0'");
            return (Boolean) object ? "'1'" : "'0'";
        }
        if (object instanceof BaseEntity) {
            logger.debug("Returning with: {}","'" + ((BaseEntity) object).getDbid() + "'");
            return "'" + ((BaseEntity) object).getDbid() + "'";
        }
        if (object instanceof Date) {
            if (isOracle(loggedUser)) {
                logger.debug("returning with: {}","TIMESTAMP '" + new Timestamp(((Date) object).getTime()).toString() + "'");
                return "TIMESTAMP '" + new Timestamp(((Date) object).getTime()).toString() + "'";
            } else {
                logger.debug("Returning with: {}","'" + new Timestamp(((Date) object).getTime()).toString() + "'");
                return "'" + new Timestamp(((Date) object).getTime()).toString() + "'";
            }
        }
        if (object instanceof byte[]) {
            try {
                logger.debug("Returning with: {}","'0x" + DatatypeConverter.printHexBinary((byte[]) object) + "'");
                return "'0x" + DatatypeConverter.printHexBinary((byte[]) object) + "'";
            } catch (Exception ex) {
                logger.error("Exception thrown",ex);
                return "NULL";
            }
        }
        return "'" + object.toString().replace("'", "''") + "'";
    }

    //</editor-fold>
    @Override
    public OFunctionResult exportEntityData(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            if (odm.getData() != null && !odm.getData().isEmpty() && odm.getData().get(0) instanceof BaseEntity) {
                oFR.getReturnValues().add(getEntitySqlAsInputStream((BaseEntity) odm.getData().get(0), loggedUser));
            }
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
        logger.debug("Returning");
        return oFR;
    }

    public int runBatch(String batch, OUser loggedUser) {
        return runStatements(splitToStatements(batch, loggedUser), loggedUser);
    }

    public int runStatements(List<String> statements, OUser loggedUser) {
        logger.debug("Entering");
        int count = 0;
        for (int i = 0; i < statements.size(); i++) {
            try {
                count += oem.executeEntityUpdateNativeQuery(statements.get(i), loggedUser) ? 1 : 0;
            } catch (Exception ex) {
                logger.error("Exception thrown",ex);
            }
        }
        logger.debug("Returning");
        return count;
    }

    public List<String> splitToStatements(String batch, OUser loggedUser) {
        logger.debug("Entering");
        List<String> statements = new ArrayList<String>();
        int first = 0;
        boolean currentIsText = false;
        for (int i = 0; i < batch.length(); i++) {
            if (batch.charAt(i) == ';' && !currentIsText) {
                statements.add(batch.substring(first, i + 1));
                first = i + 1;
            } else if (batch.charAt(i) == '\'') {
                currentIsText = !currentIsText;
            }
        }
        logger.debug("Returning");
        return statements;
    }
    public void exportEntityDD(BaseEntity entity, StringBuilder sql, OUser loggedUser) {
        try {
            String entityName = ((OEntity) entity).getEntityClassPath().substring(((OEntity) entity).getEntityClassPath().lastIndexOf(".") + 1);
            List<DD> ddList = oem.executeEntityListQuery("SELECT d from DD d JOIN FETCH d.controlType where d.name like '" + entityName + "/_%' ESCAPE '/'", loggedUser);
            if (ddList != null) {
                //generate delete statement
                String delete = "";
                String insertQuery = "";
                for (int i = 0; i < ddList.size(); i++) {
                    DD dd = ddList.get(i);
                    if (dd != null) {
                        delete += (delete.equals("")) ? "DELETE FROM DD where DBID in (" : ",";
                        delete += dd.getDbid();
                        insertQuery += "INSERT INTO DD (omodule_dbid,inlinehelp,calendarpattern_dbid,multiselection,name,label,dropdown,header,"
                                + "controltype_dbid,controlsize,defaultvalue,lookupscreen_dbid,balloon,spcharpattern,patternmessage_dbid"
                                + ",helpmsglength,lookupfilter,lookupdropdowndisplayfield,usedinreport,enableinlinehelp,active,dbid,deleted"
                                + ",custom1,custom2,custom3) VALUES(";
                        insertQuery += "'" + ((dd.getOmodule() == null) ? "0" : dd.getOmodule().getDbid()) 
                                + "'," + ((dd.getInlineHelp() == null) ? "null" : "'" + dd.getInlineHelp() + "'") 
                                + ",'" + ((dd.getCalendarPattern() == null) ? "0" : dd.getCalendarPattern().getDbid()) 
                                + "','" + ((dd.isMultiSelection()) ? "1" : "0") + "'," + ((dd.getName() == null) ? "null" : "'" +dd.getName()+"'") + "," 
                                + ((dd.getLabel() == null) ? "null" : "'" +dd.getLabel()+"'") + ",'" + ((dd.isDropdown()) ? "1" : "0") + "'," + ((dd.getHeader() == null) ? "null" : "'" +dd.getHeader()+"'") + ",'";
                        insertQuery += ((dd.getControlType() == null) ? "0" : dd.getControlType().getDbid()) 
                                + "','" + dd.getControlSize() + "'," + ((dd.getDefaultValue() == null) ? "null" : "'" +dd.getDefaultValue()+"'") + ",'";
                        insertQuery += ((dd.getLookupScreen() == null) ? "0" : dd.getLookupScreen().getDbid()) 
                                + "','" + ((dd.isBalloon()) ? "1" : "0") + "'," + ((dd.getSpCharPattern() == null) ? "null" : "'" +dd.getSpCharPattern()+"'") 
                                + ",'" + ((dd.getPatternMessage() == null) ? "0" : dd.getPatternMessage().getDbid()) + "',";
                        insertQuery += "'" + dd.getHelpMsgLength() + "'," + ((dd.getLookupFilter() == null) ? "null" : "'" + dd.getLookupFilter().replace("'", "''") + "'")
                                + "," + ((dd.getLookupDropDownDisplayField() == null) ? "null" : "'" + dd.getLookupDropDownDisplayField() + "'")
                                + ",'" + ((dd.getUsedInReport() == null) ? "0" : ((dd.getUsedInReport()) ? "1" : "0"));
                        insertQuery += "','" + ((dd.isEnableInlineHelp()) ? "1" : "0") + "','" + ((dd.isActive()) ? "1" : "0") 
                                + "','" + dd.getDbid() + "','0',"
                                + ((dd.getCustom1() == null) ? "null" : "'" + dd.getCustom1() + "'") 
                                + "," + ((dd.getCustom2() == null) ? "null" : "'" + dd.getCustom2() + "'")
                                + "," + ((dd.getCustom3() == null) ? "null" : "'" + dd.getCustom3() + "'") + ") ; \n";
                    }
                }
                delete += (delete.equals("")) ? "" : ") ;";
                sql.append(delete).append("\n");
                sql.append(insertQuery).append("\n");
                
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }

    }
    
    public void exportEntityIntializations(BaseEntity entity, StringBuilder sql, OUser loggedUser) {
         try {
            long entityId = ((OEntity) entity).getDbid();
            List<EntityAttributeInit> attributesList=oem.executeEntityListQuery("SELECT E FROM EntityAttributeInit E WHERE E.ownerEntity.dbid="+entityId, loggedUser);
            if (attributesList != null && attributesList.size()>0 ) {
                //generate delete && insert statement
                StringBuilder delete = new StringBuilder();
                StringBuilder insertQuery = new StringBuilder();
                for (EntityAttributeInit attributeInit : attributesList) {
                    
                    if (attributeInit != null) {
                        delete.append( (delete.toString().equals("")) ? "DELETE FROM EntityAttributeInit where DBID in (" : ",");
                        delete.append(attributeInit.getDbid());
                        insertQuery.append( "INSERT INTO EntityAttributeInit (DBID,CUSTOM1,CUSTOM2,CUSTOM3,FIELDEXPRESSION,HARDCODEDVALUE,INITIALIZATIONEXPRESSION"
                                + ",REQUESTNOTE,SEQUENCE,ENTITYSTATUS_DBID,OWNERENTITY_DBID,FUNCTION_DBID) VALUES(");
                        insertQuery.append( "'" +attributeInit.getDbid()+ "'," + ((attributeInit.getCustom1() == null) ? "null" : "'" + attributeInit.getCustom1() + "'")
                                + "," + ((attributeInit.getCustom2() == null) ? "null" : "'" + attributeInit.getCustom2() + "'") 
                                + "," + ((attributeInit.getCustom3() == null) ? "null" : "'" + attributeInit.getCustom3() + "'") 
                                + "," + ((attributeInit.getFieldExpression()== null) ? "null" : "'"+attributeInit.getFieldExpression()+"'") + "," );
                        insertQuery.append( ((attributeInit.getHardCodedValue()== null) ? "null" : "'"+attributeInit.getHardCodedValue()+"'") 
                                + "," +((attributeInit.getInitializationExpression()== null) ? "null" : "'"+attributeInit.getInitializationExpression()+"'") + ",");
                        insertQuery.append( ((attributeInit.getRequestNote()== null) ? "null" : "'"+attributeInit.getRequestNote()+"'") 
                                + ",'" + ((attributeInit.isSequence()) ? "1" : "0") + "','" + ((attributeInit.getEntityStatus() == null) ? "0" : attributeInit.getEntityStatus().getDbid()) + "','");
                        insertQuery.append( ((attributeInit.getOwnerEntity() == null) ? "0" : attributeInit.getOwnerEntity().getDbid()) + "','" + ((attributeInit.getFunction()== null) ? "0" : attributeInit.getFunction().getDbid()) + "');\n");
                    }
                }
                delete.append((delete.toString().equals("")) ? "" : ");");
                sql.append(delete).append("\n");
                sql.append(insertQuery).append("\n");
                
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
    }
}
