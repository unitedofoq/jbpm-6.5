/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.audittrail.persistant.entity;

import com.unitedofoq.fabs.core.dd.DD;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.NotExportable;

/**
 *
 * @author MEA
 */
@Entity
@ParentEntity(fields = {"auditTrailTransaction"})
public class AuditTrailTransactionDetail extends BaseEntity implements NotExportable {
    //<editor-fold defaultstate="collapsed" desc="Transaction">

    @ManyToOne
    @JoinColumn(name = "auditTrailTransaction_dbid")
    AuditTrailTransaction auditTrailTransaction;

    public AuditTrailTransaction getAuditTrailTransaction() {
        return auditTrailTransaction;
    }

    public void setAuditTrailTransaction(AuditTrailTransaction auditTrailTransaction) {
        this.auditTrailTransaction = auditTrailTransaction;
    }

    public String getAuditTrailTransactionDD() {
        return "AuditTrailTransactionDetail_auditTrailTransaction";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Filed Expression">
    String fieldExpression;

    public String getFieldExpression() {
        return fieldExpression;
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }

    public String getFieldExpressionDD() {
        return "AuditTrailTransactionDetail_fieldExpression";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="oldValue">
    String oldValue;

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getOldValueDD() {
        return "AuditTrailTransactionDetail_oldValue";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="newValue">
    String newValue;

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getNewValueDD() {
        return "AuditTrailTransactionDetail_newValue";
    }
    //</editor-fold>
    @ManyToOne
    private DD fieldDD;

    public DD getFieldDD() {
        return fieldDD;
    }
    
    public String getFieldDDDD() {
        return "AuditTrailTransactionDetail_fieldDD";
    }

    public void setFieldDD(DD fieldDD) {
        this.fieldDD = fieldDD;
    }
    
    
}