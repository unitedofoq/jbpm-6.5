
package com.unitedofoq.fabs.core.process.jBPM;

import java.util.List;
import java.util.Map;

import org.kie.api.runtime.process.NodeInstance;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;

/**
 *
 * @author mostafa
 */
public class ProcessInstance extends BaseEntity {

    private String id;
    private String outcome;
    private ProcessDefinition processDefinition;
    private String startDate;
    private boolean active;
    private String status;
    private String name;
    private String initiator;
    private List<String> activeNodes;
    private List<NodeInstance> nodeInstances;
    private Map<String, Object> variables;
    private String currentExecutors;
    private String description;

    public String getDescription() {
        return description;
    }
    
    public String getDescriptionDD() {
        return "ProcessInstance_description";
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getId() {
        return id;
    }

    public String getIdDD() {
        return "ProcessInstance_id";
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProcessDefinition getProcessDefinition() {
        return processDefinition;
    }

    public void setProcessDefinition(ProcessDefinition processDefinition) {
        this.processDefinition = processDefinition;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getStartDateDD() {
        return "ProcessInstance_startDate";
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public boolean isActive() {
        return active;
    }

    public String getActiveDD() {
        return "ProcessInstance_active";
    }
    
    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        String info = "Belongs to Process = " + getName()
                + ", It's ID = " + getId() + ", Started On " + getStartDate().toString();
        return info;
    }

    public String getOutcome() {
        return outcome;
    }

    public String getOutcomeDD() {
        return "ProcessInstance_outcome";
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public String getStatus() {
        return status;
    }

    public String getStatusDD() {
        return "ProcessInstance_status";
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public String getNameDD() {
        return "ProcessInstance_name";
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getActiveNodes() {
        return activeNodes;
    }

    public void setActiveNodes(List<String> activeNodes) {
        this.activeNodes = activeNodes;
    }

    public List<NodeInstance> getNodeInstances() {
        return nodeInstances;
    }

    public void setNodeInstances(List<NodeInstance> nodeInstances) {
        this.nodeInstances = nodeInstances;
    }

    public Map<String, Object> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, Object> variables) {
        this.variables = variables;
    }

    public String getCurrentExecutors() {
        return currentExecutors;
    }
    
    public String getCurrentExecutorsDD() {
        return "ProcessInstance_currentExecutors";
    }

    public void setCurrentExecutors(String currentExecutors) {
        this.currentExecutors = currentExecutors;
    }

    public String getInitiator() {
        return initiator;
    }
    
    public String getInitiatorDD() {
        return "ProcessInstance_initiator";
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }
}
