/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.report.customReports;

import com.itextpdf.text.pdf.PdfDocument;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.ByteArrayOutputStream;
import org.eclipse.birt.report.engine.api.EngineException;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.engine.api.PDFRenderOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author bgalal
 */
public class PDFReportGenerator extends AbstractReportGenerator {
    
    private final static Logger LOGGER = LoggerFactory.getLogger(PDFReportGenerator.class);
    
    public PDFReportGenerator(ReportDTO reportDTO) {
        super(reportDTO);
    }
    
    @Override
    public ByteArrayOutputStream generateReport(CustomReportBuilder customReportBuilder) {
        LOGGER.debug("Entering");
        IRunAndRenderTask task = prepareReportDesignHandle(customReportBuilder);
        if(task == null)
            return null;
        //<editor-fold defaultstate="collapsed" desc="PDF Generator">
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        PDFRenderOption renderOption = new PDFRenderOption();
        renderOption.setOutputFormat(PDFRenderOption.OUTPUT_FORMAT_PDF);
        try {
            PdfWriter.getInstance(new PdfDocument(), outStream);
            renderOption.setOutputStream(outStream);
        } catch (Exception ex) {
            LOGGER.error("Exception thrown: ", ex);
        }
        task.setRenderOption(renderOption);
        runReport(task);
        //</editor-fold>
        LOGGER.trace("Returning");
        return outStream;
    }
}
