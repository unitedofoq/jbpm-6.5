/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.alert.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import javax.persistence.DiscriminatorValue;

/** 
 *
 * @author root
 */
@Entity
@DiscriminatorValue(value = "BASIC")
@ChildEntity(fields={"alertConditions", "recievers","alertParams"})
public class Alert extends BaseAlert {
    @OneToMany(mappedBy = "alert")
    private List<AlertCondition> alertConditions;
    @OneToMany(mappedBy = "alert")
    private List<Reciever> recievers;
    @OneToMany(mappedBy = "alert")
    private List<AlertParam> alertParams;
    @OneToOne
    private OEntity actOnEntity;
    
    public List<AlertCondition> getAlertConditions() {
        return alertConditions;
    }

    public void setAlertConditions(List<AlertCondition> alertConditions) {
        this.alertConditions = alertConditions;
    }

    public List<Reciever> getRecievers() {
        return recievers;
    }

    public void setRecievers(List<Reciever> recievers) {
        this.recievers = recievers;
    }

    public List<AlertParam> getAlertParams() {
        return alertParams;
    }

    public void setAlertParams(List<AlertParam> alertParams) {
        this.alertParams = alertParams;
    }

    public String getScheduleDD() {
        return "Alert_schedule";
    }

    public String getActOnEntityDD() {
        return "Alert_actOnEntity";
    }
    
    public OEntity getActOnEntity() {
        return actOnEntity;
    }

    public void setActOnEntity(OEntity actOnEntity) {
        this.actOnEntity = actOnEntity;
    }

   
}
