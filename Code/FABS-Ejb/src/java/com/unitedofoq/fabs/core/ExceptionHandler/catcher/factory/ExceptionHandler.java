/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.ExceptionHandler.catcher.factory;

import java.lang.reflect.Method;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.naming.InitialContext;

import com.unitedofoq.fabs.core.Exception.BasicException;
import com.unitedofoq.fabs.core.ExceptionHandler.catcher.main.IMethodExceptionHandler;

/**
 *
 * @author mostafa
 */
@Interceptor
public class ExceptionHandler {
    @AroundInvoke
    public Object handleException(InvocationContext ctx) throws Exception{
        Method methodOfException = ctx.getMethod();
        Class classOfException = ctx.getClass();
        IMethodExceptionHandler exceptionHandler = null;
        try {
            InitialContext ic = new InitialContext();
            exceptionHandler = 
                    (IMethodExceptionHandler) ic.lookup("java:global/ofoq/"
                    + "com.unitedofoq.fabs.core.ExceptionHandler.catcher.main.IMethodExceptionHandler");
            return ctx.proceed();
        }catch(BasicException e){
            exceptionHandler.handle(classOfException, methodOfException,
                    e.getExceptionMessage(), e.getMessageCode(), e.getLoggedUser());
            return null;
        }
    }
}
