
package com.unitedofoq.fabs.core.uiframework.screen;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class FABSSetupScreenTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="header">
    @Column
    private String header;

    public void setHeader(String header) {
        this.header = header;
    }

    public String getHeader() {
        return header;
    }
    // </editor-fold>

}
