/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields = {"person"})
public class PersonPhoto extends BaseEntity {

    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false, unique = true)
    private Person person;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getPersonDD() {
        return "PersonPhoto_person";
    }
    @Column(columnDefinition = "BLOB(2097152)") // 2MB size
    private byte[] photoData;

    public byte[] getPhotoData() {
        return photoData;
    }

    public void setPhotoData(byte[] photoData) {
        this.photoData = photoData;
    }

    public String getPhotoDataDD() {
        return "PersonPhoto_photoData";
    }
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameDD() {
        return "PersonPhoto_name";
    }
//    @Transient
//    private GraphicImage personelPhotoData;
//
//    /**
//     * @return the personelPhotoData
//     */
//    public GraphicImage getPersonelPhotoData() {
//        GraphicImage image = new GraphicImage();
//        // call creation of byte array to image file
//        return image;
//    }
//
//    /**
//     * @param personelPhotoData the personelPhotoData to set
//     */
//    public void setPersonelPhotoData(GraphicImage personelPhotoData) {
//        this.personelPhotoData = personelPhotoData;
//    }
}
