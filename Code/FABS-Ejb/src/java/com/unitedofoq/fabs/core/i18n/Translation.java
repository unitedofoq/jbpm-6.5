/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.i18n;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author mmohamed
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public abstract @interface Translation{
    String originalField();
}
