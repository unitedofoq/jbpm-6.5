/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.validation;

import com.liferay.portal.UserPasswordException;
import com.liferay.portal.kernel.util.MethodKey;
import com.liferay.portal.kernel.util.PortalClassInvoker;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.model.PasswordPolicy;
import com.liferay.portal.service.PasswordPolicyLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntityActionValidation;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.uiframework.screen.FormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenFilter;
import com.unitedofoq.fabs.core.uiframework.screen.SingleEntityScreen;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nkhalil
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.validation.ValidationJavaFunctionLocal",
        beanInterface = ValidationJavaFunctionLocal.class)
public class ValidationJavaFunctionBean implements ValidationJavaFunctionLocal {

    @EJB
    EntitySetupServiceRemote entitySetupServiceRemote;
    @EJB
    private UserMessageServiceRemote userMessageServiceRemote;
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private OFunctionServiceRemote functionService;
    final static Logger logger = LoggerFactory.getLogger(ValidationJavaFunctionBean.class);

    public Class[] getSingleFieldValidationMethodParmTypes() {
        Class[] types = new Class[3];
        types[0] = Object.class;
        types[1] = String.class;
        types[2] = OUser.class;
        return types;
    }

    public OFunctionResult callSingleFieldValidationMethod(Method method, ODataMessage odm, OUser loggedUser) {
        if (method != null && odm != null) {
            logger.debug("Entering: callSingleFieldValidationMethod with Params Method:{}, ODataMessage:{}", method, odm);
        }
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            oem.getEM(systemUser);
            Object entity = odm.getData().get(1);
            String fieldExpression = (String) odm.getData().get(2);
            StringTokenizer st = new StringTokenizer(fieldExpression, ",");
            while (st.hasMoreTokens()) {
                oFR.append((OFunctionResult) method.invoke(this, entity, st.nextToken(), loggedUser));
            }
        } catch (Exception ex) {
            logger.error("Exception Thrown:", ex);
        } finally {
            oem.closeEM(systemUser);
        }
        logger.debug("Returning: callSingleFieldValidationMethod");
        return oFR;
    }

    public OFunctionResult validatePositivePercent(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validatePositivePercent with Params ODataMessage:{}, OFunctionParms:{}", odm, functionParms);
        Method callBackMethod = null;
        try {
            callBackMethod = getClass().getDeclaredMethod("validatePositivePercent",
                    getSingleFieldValidationMethodParmTypes());
            return callSingleFieldValidationMethod(callBackMethod, odm, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception Thrown:", ex);
            return new OFunctionResult();
        }
    }

    public OFunctionResult validatePositivePercent(Object entity, String fieldName, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            logger.debug("Entering: validatePositivePercent with Params fieldName:{}", fieldName);
            oem.getEM(systemUser);
            int intData = Integer.parseInt(
                    BaseEntity.getValueFromEntity(
                            entity,
                            fieldName).
                            toString());
            if (intData < 0) {
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "InvalidPositivePercent", Collections.singletonList(fieldName), systemUser));
            }
        } catch (Exception ex) {
            if (ex.getClass() == NullPointerException.class) {
                logger.debug("Returning");
                return oFR;
            }
            logger.error("Exception Thrown:", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "InvalidPositivePercent", Collections.singletonList(fieldName), systemUser));
        } finally {
            oem.closeEM(systemUser);
        }
        logger.debug("Returning");
        return oFR;
    }

    public OFunctionResult validatePercentLTZero(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        Method callBackMethod = null;
        try {
            logger.debug("Entering: validatePercentLTZero with Params ODataMessage:{}, OFunctionParms:{}", odm, functionParms);
            callBackMethod = getClass().getDeclaredMethod("validatePercentLTZero",
                    getSingleFieldValidationMethodParmTypes());
            return callSingleFieldValidationMethod(callBackMethod, odm, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown while validating Percent LTZero:{}", ex);
            return new OFunctionResult();
        }
    }

    public OFunctionResult validatePercentLTZero(Object entity, String fieldName, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        int intData = 0;
        try {
            logger.debug("Entering: validatePercentLTZero with Params fieldName:{}", fieldName);
            oem.getEM(systemUser);
            intData = Integer.parseInt(
                    BaseEntity.getValueFromEntity(
                            entity,
                            fieldName).
                            toString());
            if (intData > 0) {
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "InvalidPercentLTZero", Collections.singletonList(fieldName), systemUser));
            }
        } catch (Exception ex) {
            if (ex.getClass() == NullPointerException.class) {
                logger.error("Exception while validating Percent LTZero with Null Pointer Exception:{}", ex);
                return oFR;
            }
            logger.error("Exception while validating Percent LTZero with params entity:{}, fieldName:{}", entity, fieldName);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "InvalidPercentLTZero", Collections.singletonList(fieldName), systemUser));
        } finally {
            oem.closeEM(systemUser);
        }
        logger.debug("Returning: validatePercentLTZero");
        return oFR;
    }

    public OFunctionResult validatePercent(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        Method callBackMethod = null;
        try {
            logger.debug("Entering: validatePercent with Params ODataMessage:{}, OFunctionParms:{}", odm, functionParms);
            callBackMethod = getClass().getDeclaredMethod("validatePercent",
                    getSingleFieldValidationMethodParmTypes());
            logger.debug("Returning: validatePercent");
            return callSingleFieldValidationMethod(callBackMethod, odm, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception while validating Percent:{}", ex);
            return new OFunctionResult();
        }
    }

    public OFunctionResult validatePercent(Object entity, String fieldName, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            logger.debug("Entering: validatePercent with Params fieldName:{}", fieldName);
            oem.getEM(systemUser);
        } catch (Exception ex) {
            if (ex.getClass() == NullPointerException.class) {
                logger.debug("Returning");
                return oFR;
            }
            logger.error("Exception Thrown:", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "InvalidPercent", Collections.singletonList(fieldName), systemUser));
        } finally {
            oem.closeEM(systemUser);
        }
        logger.debug("Returning: validatePercent");
        return oFR;
    }

    public OFunctionResult validateDecimal(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validating Decimal with Params ODataMessage:{}, OFunctionParms:{}", odm, functionParms);
        Method callBackMethod = null;
        try {
            callBackMethod = getClass().getDeclaredMethod("validateDecimal",
                    getSingleFieldValidationMethodParmTypes());
            logger.debug("Returning: validateDecimal");
            return callSingleFieldValidationMethod(callBackMethod, odm, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception Thrown:", ex);
            logger.debug("Returning: validateDecimal");
            return new OFunctionResult();
        }

    }

    public OFunctionResult validateDecimal(Object entity, String fieldName, OUser loggedUser) {
        logger.debug("Entering: validateDecimal with Params fieldName:{}", fieldName);
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            oem.getEM(systemUser);
        } catch (Exception ex) {
            if (ex.getClass() == NullPointerException.class) {
                logger.debug("Returning");
                return oFR;
            }
            logger.error("Exception Thrown:", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "InvalidDecimal", Collections.singletonList(fieldName), systemUser));
        } finally {
            oem.closeEM(systemUser);
        }
        logger.debug("Returning: validateDecimal");
        return oFR;
    }

    public OFunctionResult validateDecimalLTZero(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateDecimalLTZero with Params ODataMessage:{}, OFunctionParms:{}", odm, functionParms);
        Method callBackMethod = null;
        try {
            callBackMethod = getClass().getDeclaredMethod("validateDecimalLTZero",
                    getSingleFieldValidationMethodParmTypes());
            logger.debug("Returning");
            return callSingleFieldValidationMethod(callBackMethod, odm, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception Thrown:", ex);
            return new OFunctionResult();
        } finally {
            logger.debug("Returning: validateDecimalLTZero");
        }
    }

    public OFunctionResult validateDecimalLTZero(Object entity, String fieldName, OUser loggedUser) {
        logger.debug("Entering: validateDecimalLTZero with Params fieldName:{}", fieldName);
        OFunctionResult oFR = new OFunctionResult();
        float decimalData = 0;
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            oem.getEM(systemUser);
            decimalData = Float.parseFloat(
                    BaseEntity.getValueFromEntity(
                            entity,
                            fieldName).
                            toString());
            if (decimalData > 0) {
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "InvalidDecimalLTZero", Collections.singletonList(fieldName), systemUser));
            }

        } catch (Exception ex) {
            if (ex.getClass() == NullPointerException.class) {
                logger.debug("Returning");
                return oFR;
            }
            logger.error("Exception thrown:", ex);
        } finally {
            oem.closeEM(systemUser);
        }
        logger.debug("Returning: validateDecimalLTZero");
        return oFR;
    }

    public OFunctionResult validatePositiveDecimal(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validatePositiveDecimal with Params ODataMessage:{}, functionParms:{}", odm, functionParms);
        Method callBackMethod = null;
        try {
            callBackMethod = getClass().getDeclaredMethod("validatePositiveDecimal",
                    getSingleFieldValidationMethodParmTypes());
            logger.debug("Returning");
            return callSingleFieldValidationMethod(callBackMethod, odm, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown:", ex);
            return new OFunctionResult();
        }
    }

    public OFunctionResult validatePositiveDecimal(Object entity, String fieldName, OUser loggedUser) {
        logger.debug("Entering: validatePositiveDecimal with Params fieldName:{}", fieldName);
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        float decimalData = 0;
        try {
            oem.getEM(systemUser);
            decimalData = Float.parseFloat(
                    BaseEntity.getValueFromEntity(
                            entity,
                            fieldName).
                            toString());
            if (decimalData < 0) {
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "InvalidPositiveDecimal", Collections.singletonList(fieldName), systemUser));
            }
        } catch (Exception ex) {
            if (ex.getClass() == NullPointerException.class) {
                logger.debug("Returning");
                return oFR;
            }
            logger.error("Exception thrown:", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "InvalidPositiveDecimal", Collections.singletonList(fieldName), systemUser));
        } finally {
            oem.closeEM(systemUser);
        }
        logger.debug("Returning from: validatePositiveDecimal");
        return oFR;
    }

    public OFunctionResult validateMonthNumber(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateMonthNumber");
        Method callBackMethod = null;
        try {
            callBackMethod = getClass().getDeclaredMethod("validateMonthNumber",
                    getSingleFieldValidationMethodParmTypes());
            logger.debug("Returning: validateMonthNumber");
            return callSingleFieldValidationMethod(callBackMethod, odm, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown:", ex);
            logger.debug("Returning: validateMonthNumber");
            return new OFunctionResult();
        }
    }

    public OFunctionResult validateMonthNumber(Object entity, String fieldName, OUser loggedUser) {
        logger.debug("Entering: validateMonthNumber");
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        int intData = 0;
        try {
            oem.getEM(systemUser);
            intData = Integer.parseInt(
                    BaseEntity.getValueFromEntity(
                            entity,
                            fieldName).
                            toString());
            if (intData < 0 || intData > 12) {
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "InvalidMonthNumber", Collections.singletonList(fieldName), systemUser));
            }
        } catch (Exception ex) {
            if (ex.getClass() == NullPointerException.class) {
                logger.debug("Returning");
                return oFR;
            }
            logger.error("Exception thrown:", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "InvalidMonthNumber", Collections.singletonList(fieldName), systemUser));
        } finally {
            oem.closeEM(systemUser);
        }
        logger.debug("Returning: validateMonthNumber");
        return oFR;
    }

    public OFunctionResult validateWeekDayNumber(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateWeekDayNumber");
        Method callBackMethod = null;
        try {
            callBackMethod = getClass().getDeclaredMethod("validateWeekDayNumber",
                    getSingleFieldValidationMethodParmTypes());
            logger.debug("Returning: validateWeekDayNumber");
            return callSingleFieldValidationMethod(callBackMethod, odm, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown:", ex);
            logger.debug("Returning: validateWeekDayNumber");
            return new OFunctionResult();
        }
    }

    public OFunctionResult validateWeekDayNumber(Object entity, String fieldName, OUser loggedUser) {
        logger.debug("Entering: validateWeekDayNumber");
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        int intData = 0;
        try {
            oem.getEM(systemUser);
            intData = Integer.parseInt(
                    BaseEntity.getValueFromEntity(
                            entity,
                            fieldName).
                            toString());
            if (intData < 0 || intData > 7) {
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "InvalidWeekDayNumber", Collections.singletonList(fieldName), systemUser));
            }
        } catch (Exception ex) {
            if (ex.getClass() == NullPointerException.class) {
                logger.debug("Returning");
                return oFR;
            }
            logger.error("Exception thrown:", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "InvalidWeekDayNumber", Collections.singletonList(fieldName), systemUser));
        } finally {
            oem.closeEM(systemUser);
        }
        logger.debug("Returning: validateWeekDayNumber");
        return oFR;
    }

    public OFunctionResult validateHoursPerDay(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateHoursPerDay");
        Method callBackMethod = null;
        try {
            callBackMethod = getClass().getDeclaredMethod("validateHoursPerDay",
                    getSingleFieldValidationMethodParmTypes());
            logger.debug("Returning: validateHoursPerDay");
            return callSingleFieldValidationMethod(callBackMethod, odm, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown:", ex);
            return new OFunctionResult();
        }
    }

    public OFunctionResult validateHoursPerDay(Object entity, String fieldName, OUser loggedUser) {
        logger.debug("Entering: validateHoursPerDay");
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        int intData = 0;
        try {
            oem.getEM(systemUser);
            intData = Integer.parseInt(
                    BaseEntity.getValueFromEntity(
                            entity,
                            fieldName).
                            toString());
            if (intData < 0 || intData > 23) {
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "InvalidHoursPerDay", Collections.singletonList(fieldName), systemUser));
            }
        } catch (Exception ex) {
            if (ex.getClass() == NullPointerException.class) {
                logger.debug("Returning");
                return oFR;
            }
            logger.error("Exception thrown:", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "InvalidHoursPerDay", Collections.singletonList(fieldName), systemUser));
        } finally {
            oem.closeEM(systemUser);
        }
        logger.debug("Returning: validateHoursPerDay");
        return oFR;
    }

    public OFunctionResult validateDaysPerMonth(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateDaysPerMonth");
        Method callBackMethod = null;
        try {
            callBackMethod = getClass().getDeclaredMethod("validateDaysPerMonth",
                    getSingleFieldValidationMethodParmTypes());
            logger.debug("Returning: validateDaysPerMonth");
            return callSingleFieldValidationMethod(callBackMethod, odm, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown:", ex);
            logger.debug("Returning: validateDaysPerMonth");
            return new OFunctionResult();
        }
    }

    public OFunctionResult validateDaysPerMonth(Object entity, String fieldName, OUser loggedUser) {
        logger.debug("Entering: validateDaysPerMonth");
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        int intData = 0;
        try {
            oem.getEM(systemUser);
            intData = Integer.parseInt(
                    BaseEntity.getValueFromEntity(
                            entity,
                            fieldName).
                            toString());
            if (intData < 0 || intData > 31) {
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "InvalidWeekDayNumber", Collections.singletonList(fieldName), systemUser));
            }
        } catch (Exception ex) {
            if (ex.getClass() == NullPointerException.class) {
                logger.debug("Returning");
                return oFR;
            }
            logger.error("Exception thrown:", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "InvalidWeekDayNumber", Collections.singletonList(fieldName), systemUser));
        } finally {
            oem.closeEM(systemUser);
        }
        logger.debug("Returning: validateDaysPerMonth");
        return oFR;
    }

    @Override
    public OFunctionResult validateNumberLessThan10(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateNumberLessThan10");
        Method callBackMethod = null;
        try {
            callBackMethod = getClass().getDeclaredMethod("validateNumberLessThan10",
                    getSingleFieldValidationMethodParmTypes());
            logger.debug("Returning: validateNumberLessThan10");
            return callSingleFieldValidationMethod(callBackMethod, odm, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown:", ex);
            logger.debug("Returning: validateNumberLessThan10");
            return new OFunctionResult();
        }
    }

    public OFunctionResult validateNumberLessThan10(Object entity, String fieldName, OUser loggedUser) {
        logger.debug("Entering: validateNumberLessThan10");
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        int intData = 0;
        try {
            oem.getEM(systemUser);
            intData = Integer.parseInt(
                    BaseEntity.getValueFromEntity(
                            entity,
                            fieldName).
                            toString());
            if (intData > 9) {
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "NumberGreaterThan9", Collections.singletonList(fieldName), systemUser));
            }
            return oFR;
        } catch (Exception ex) {
            if (ex.getClass() == NullPointerException.class) {
                logger.debug("Returning");
                return oFR;
            }
            logger.error("Exception thrown:", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "InvalidNumber", Collections.singletonList(fieldName), systemUser));
        } finally {
            oem.closeEM(systemUser);
        }
        logger.debug("Returning: validateNumberLessThan10");
        return oFR;
    }

    public OFunctionResult validateNumber(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateNumber");
        Method callBackMethod = null;
        try {
            callBackMethod = getClass().getDeclaredMethod("validateNumber",
                    getSingleFieldValidationMethodParmTypes());
            logger.debug("Returning: validateNumber");
            return callSingleFieldValidationMethod(callBackMethod, odm, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown:", ex);
            logger.debug("Returning: validateNumber");
            return new OFunctionResult();
        }
    }

    public OFunctionResult validateNumber(Object entity, String fieldName, OUser loggedUser) {
        logger.debug("Entering: validateNumber");
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            oem.getEM(systemUser);

        } catch (Exception ex) {
            if (ex.getClass() == NullPointerException.class) {
                return oFR;
            }
            logger.error("Exception thrown:", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "InvalidNumber", Collections.singletonList(fieldName), systemUser));
        } finally {
            oem.closeEM(systemUser);
        }
        logger.debug("Entering: validateNumber");
        return oFR;
    }

    public OFunctionResult validatePositiveNumber(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validatePositiveNumber, First");
        Method callBackMethod = null;
        try {
            callBackMethod = getClass().getDeclaredMethod("validatePositiveNumber",
                    getSingleFieldValidationMethodParmTypes());
            logger.debug("Returning: validatePositiveNumber");
            return callSingleFieldValidationMethod(callBackMethod, odm, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown:", ex);
            logger.debug("Returning: validatePositiveNumber, First");
            return new OFunctionResult();
        }
    }

    public OFunctionResult validatePositiveNumber(Object entity, String fieldName, OUser loggedUser) {
        logger.debug("Entering: validatePositiveNumber, Second");
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        int intData = 0;
        try {
            oem.getEM(systemUser);
            intData = Integer.parseInt(BaseEntity.getValueFromEntity(
                    entity, fieldName).toString());
            if (intData < 0) {
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "InvalidPositiveNumber", Collections.singletonList(fieldName), systemUser));
            }
        } catch (Exception ex) {
            if (ex.getClass() == NullPointerException.class) {
                logger.debug("Returning");
                return oFR;
            }
            logger.error("Exception thrown:", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "InvalidPositiveNumber", Collections.singletonList(fieldName), systemUser));
        } finally {
            oem.closeEM(systemUser);
        }
        logger.debug("Returning: validatePositiveNumber, Second");
        return oFR;
    }

    public OFunctionResult validateNumberLTZero(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateNumberLTZero, First");
        Method callBackMethod = null;
        try {
            callBackMethod = getClass().getDeclaredMethod("validateNumberLTZero",
                    getSingleFieldValidationMethodParmTypes());
            logger.debug("Returning: validateNumberLTZero, First");
            return callSingleFieldValidationMethod(callBackMethod, odm, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown:", ex);
            logger.debug("Returning: validateNumberLTZero");
            return new OFunctionResult();
        }
    }

    public OFunctionResult validateNumberLTZero(Object entity, String fieldName, OUser loggedUser) {
        logger.debug("Entering: validateNumberLTZero, Second");
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        int intData = 0;
        try {
            oem.getEM(systemUser);
            intData = Integer.parseInt(
                    BaseEntity.getValueFromEntity(entity, fieldName).toString());
            if (intData > 0) {
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "InvalidNumberLTZero", Collections.singletonList(fieldName), systemUser));
            }
        } catch (Exception ex) {
            if (ex.getClass() == NullPointerException.class) {
                logger.debug("Returning");
                return oFR;
            }
            logger.error("Exception thrown:", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "InvalidNumberLTZero", Collections.singletonList(fieldName), systemUser));
        } finally {
            oem.closeEM(systemUser);
        }
        logger.debug("Returning: validateNumberLTZero, Second");
        return oFR;
    }

    public OFunctionResult validateAlphaNumericName(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateAlphaNumericName, First");
        Method callBackMethod = null;
        try {
            callBackMethod = getClass().getDeclaredMethod("validateAlphaNumericName",
                    getSingleFieldValidationMethodParmTypes());
            logger.debug("Returning: validateAlphaNumericName");
            return callSingleFieldValidationMethod(callBackMethod, odm, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown:", ex);
            logger.debug("Returning: validateAlphaNumericName, First");
            return new OFunctionResult();
        }
    }

    public OFunctionResult validateAlphaNumericName(Object entity, String fieldExpression, OUser loggedUser) {
        logger.debug("Entering: validateAlphaNumericName, Second");
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            oem.getEM(systemUser);
            String fieldValue = BaseEntity.getValueFromEntity(entity, fieldExpression).toString();
            StringCharacterIterator iterator = new StringCharacterIterator(fieldValue);
            Character character = iterator.current();
            while (character != CharacterIterator.DONE) {
                if (Character.isLetterOrDigit(character) == false) {
                    oFR.addError(userMessageServiceRemote.getUserMessage(
                            "SpecialCharacterOrWhiteSpaceFound", Collections.singletonList(fieldExpression), systemUser));
                    break;
                }
                character = iterator.next();
            }
        } catch (Exception ex) {
            if (ex.getClass() == NullPointerException.class) {
                logger.debug("Returning");
                return oFR;
            }
            logger.error("Exception thrown:", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser), ex);
        } finally {
            oem.closeEM(systemUser);
        }
        logger.debug("Returning: validateAlphaNumericName, Second");
        return oFR;
    }

    public OFunctionResult fieldRequired(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: fieldRequired, First");
        Method callBackMethod = null;
        try {
            callBackMethod = getClass().getDeclaredMethod("fieldRequired",
                    getSingleFieldValidationMethodParmTypes());
            logger.debug("Returning: fieldRequired, First");
            return callSingleFieldValidationMethod(callBackMethod, odm, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown:", ex);
            logger.debug("Returning: fieldRequired, First");
            return new OFunctionResult();
        }
    }

    public OFunctionResult fieldRequired(Object entity, String fieldExpression, OUser loggedUser) {
        logger.debug("Entering: fieldRequired, Second");
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            StringTokenizer tokenizer = new StringTokenizer(fieldExpression, ",");
            while (tokenizer.hasMoreTokens()) {
                String nextToken = tokenizer.nextToken();
                List<ExpressionFieldInfo> expFldsInfo = BaseEntity.parseFieldExpression(entity.getClass(), fieldExpression);
                if (expFldsInfo == null) {
                    oFR.addError(userMessageServiceRemote.getUserMessage("FieldExpressionNotFound",
                            userMessageServiceRemote.constructListFromStrings(fieldExpression,
                                    ((BaseEntity) entity).getClassName()), loggedUser));
                    logger.debug("Returning");
                    return oFR;
                }
                if (BaseEntity.getValueFromEntity(entity, nextToken).toString().length() == 0) {
                    oFR.addError(userMessageServiceRemote.getUserMessage(
                            "fieldRequired", Collections.singletonList(nextToken), loggedUser));
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown:", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser), ex);
        } finally {
            oem.closeEM(loggedUser);
        }
        logger.debug("Entering: fieldRequired, Second");
        return oFR;
    }

    public OFunctionResult fieldUniqueOptional(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: fieldUniqueOptional");
        // We shouldn't call callback function for peformance enhancement
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateValidationJavaFunctionODM(
                odm, loggedUser, BaseEntity.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>
        BaseEntity entity = (BaseEntity) odm.getData().get(1);
        String fieldExpression = (String) odm.getData().get(2);
        OFunctionResult oFR = new OFunctionResult();
        if (!entity.isChanged()) {
            return oFR;
        }
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            oem.getEM(loggedUser);
            List<String> loadConditions = new ArrayList();

            // Check that at least one field is not NULL for peroformance enhacnement
            StringTokenizer tokenizer = new StringTokenizer(fieldExpression, ",");
            boolean allFieldsAreNull = true;
            ArrayList<String> nonEmptyFields = new ArrayList();
            while (tokenizer.hasMoreElements()) {
                String fieldName = tokenizer.nextToken();
                String fieldValue = entity.invokeGetter(fieldName).toString();
                if (fieldValue.length() != 0) {
                    List<ExpressionFieldInfo> fieldExpInfos = BaseEntity.parseFieldExpression(
                            entity.getClass(), fieldName);
                    Class fieldCls = fieldExpInfos.get(fieldExpInfos.size() - 1).field.getType();
                    if (fieldCls.equals(String.class)) {
                        nonEmptyFields.add(fieldName);
                        if (fieldName.endsWith("Translated")) {
                            String fieldNameWithoutTrans = fieldName.substring(0, fieldName.lastIndexOf("Translated"));
                            loadConditions.add(fieldNameWithoutTrans + "='" + fieldValue + "'");
                        } else {
                            loadConditions.add(fieldName + "='" + fieldValue + "'");
                        }
                    } else if (fieldCls.equals(Integer.class)
                            || fieldCls.equals(int.class)
                            || fieldCls.equals(Long.class)
                            || fieldCls.equals(long.class)
                            || fieldCls.equals(Double.class)
                            || fieldCls.equals(double.class)
                            || fieldCls.equals(Float.class)
                            || fieldCls.equals(float.class)) {
                        nonEmptyFields.add(fieldName);
                        loadConditions.add(fieldName + "=" + fieldValue);
                    } else {
                        logger.error("Field:{} with value:{} is not Supported", fieldName, fieldValue);
                        oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
                    }
                    allFieldsAreNull = false;
                }
            }
            if (allFieldsAreNull) {
                logger.warn("all Fields are Null");
                return oFR;
            }

            // Get all entities that are already existed in DB
            ArrayList<BaseEntity> entitiesFromDB = new ArrayList<BaseEntity>();
            for (String condition : loadConditions) {
                if (condition.endsWith("Translated")) {
                    condition = condition.substring(0, condition.lastIndexOf("Translated"));
                }
                String temp = condition.substring(0, condition.indexOf("="));
                if (temp.contains(".")) {
                    temp = condition.substring(0, condition.indexOf("."));
                }
                // This workaround is for loading all the objects of the same SuperClass
                // in order to validate the objects of all subclasses of the same SuperClass
                Field field = BaseEntity.getClassField(entity.getClass(), temp);
                String entityName = field.getDeclaringClass().getSimpleName(); //= entity.getClass().getSimpleName();
                List entityFromDB = oem.loadEntityList(
                        entityName,
                        Collections.singletonList(condition), null, null, loggedUser);// i don't need translated
                if (entityFromDB.size() > 0) {
                    entitiesFromDB.add((BaseEntity) entityFromDB.get(0));
                }
            }

            for (String nonEmptyField : nonEmptyFields) {
                String fieldValue = "";
                fieldValue = BaseEntity.getValueFromEntity(entity, nonEmptyField).toString();// i need translated
                for (BaseEntity nextEntity : entitiesFromDB) {
                    if (nonEmptyField.endsWith("Translated")) {
                        nonEmptyField = nonEmptyField.substring(0, nonEmptyField.lastIndexOf("Translated"));
                    }
                    Object valueFromEntity = nextEntity.invokeGetter(nonEmptyField);// i don't need translated
                    if (valueFromEntity.toString().equals(fieldValue) && nextEntity.getDbid() != entity.getDbid()) {
                        oFR.addError(userMessageServiceRemote.getUserMessage("fieldNotUnique",
                                Collections.singletonList(nonEmptyField), loggedUser));
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Eception thrown: ", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser), ex);
        } finally {
            oem.closeEM(loggedUser);
        }
        logger.debug("Returning: fieldUniqueOptional");
        return oFR;
    }

    public OFunctionResult fieldRequiredForNonBlankField(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: fieldRequiredForNonBlankField");

        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateValidationJavaFunctionODM(
                odm, loggedUser, BaseEntity.class);
        if (inputValidationErrors != null) {
            logger.warn("there are Validation Errors");
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        BaseEntity entity = (BaseEntity) odm.getData().get(1);
        String fieldExpression = (String) odm.getData().get(2);
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            oem.getEM(loggedUser);
            StringTokenizer tokenizer = new StringTokenizer(fieldExpression, ",");
            // Verify fieldName contains the three required fields
            if (tokenizer.countTokens() <= 1) {
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "CorruptedDataInField", Collections.singletonList(
                                fieldExpression), loggedUser));
                logger.debug("Returning");
                return oFR;
            }
            String fieldName = tokenizer.nextToken();
            String fieldValue = BaseEntity.getValueFromEntity(entity, fieldName).toString();
            // If First token is not empty, verify next tokens are all not blank
            if (fieldValue.length() != 0) {
                while (tokenizer.hasMoreElements()) {
                    fieldName = tokenizer.nextToken();
                    fieldValue = BaseEntity.getValueFromEntity(entity, fieldName).toString();
                    if (fieldValue.length() == 0) {
                        oFR.addError(userMessageServiceRemote.getUserMessage(
                                "fieldRequired", Collections.singletonList(fieldName), loggedUser));
                    }
                }
            }
            logger.debug("Returning");
            return oFR;

        } catch (Exception ex) {
            logger.error("Exception thrown:", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser), ex);
            logger.debug("Returning");
            return oFR;

        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning: fieldRequiredForNonBlankField");
        }
    }

    public OFunctionResult fieldRequiredForFieldValue(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: fieldRequiredForFieldValue");

        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateValidationJavaFunctionODM(
                odm, loggedUser, BaseEntity.class);
        if (inputValidationErrors != null) {
            logger.warn("there are Validation Errors");
            return inputValidationErrors;
        }
        // </editor-fold>

        BaseEntity entity = (BaseEntity) odm.getData().get(1);
        String fieldExpression = (String) odm.getData().get(2);
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            oem.getEM(loggedUser);
            StringTokenizer tokenizer = new StringTokenizer(fieldExpression, ",");
            // Verify fieldName contains the two required fields
            if (tokenizer.countTokens() != 2) {
                oFR.addError(userMessageServiceRemote.getUserMessage("CorruptedDataInField",
                        Collections.singletonList(fieldExpression), loggedUser));
                logger.debug("Returning");
                return oFR;
            }

            String fieldName = tokenizer.nextToken();
            if (fieldName.length() != 0) {
                int indexOfEquals = fieldName.indexOf("=");
                if (indexOfEquals == 0) {
                    oFR.addError(userMessageServiceRemote.getUserMessage("CorruptedDataInField",
                            Collections.singletonList(fieldName), loggedUser));
                    logger.debug("Returning");
                    return oFR;
                }
                String firstFieldName = fieldName.substring(0, indexOfEquals);
                String firstFieldValue = fieldName.substring(indexOfEquals + 1);
                //If no value after '=', then return
                if (firstFieldValue.length() == 0) {
                    logger.debug("Returning");
                    return oFR;
                }
                // Skip if doesn't contain '='
                if (fieldName.indexOf("=") != -1) {
                    // Skip if doesn't contain value after '='
                    if (fieldName.substring(fieldName.indexOf("=")).length() != 0) {
                        // If first field value in validation fieldExpression equals the value of the
                        // validated field, it should have a value
                        if (BaseEntity.getValueFromEntity(entity, firstFieldName).toString().equals(firstFieldValue)) {
                            String secondFieldValue = tokenizer.nextToken();
                            if (BaseEntity.getValueFromEntity(entity, secondFieldValue) == null) {
                                oFR.addError(userMessageServiceRemote.getUserMessage(
                                        "fieldRequired", Collections.singletonList(secondFieldValue), loggedUser));
                                logger.debug("Returning");
                                return oFR;
                            }
                            if (BaseEntity.getValueFromEntity(entity, secondFieldValue).equals("")) {
                                oFR.addError(userMessageServiceRemote.getUserMessage(
                                        "fieldRequired", Collections.singletonList(secondFieldValue), loggedUser));
                                logger.debug("Returning");
                                return oFR;
                            }
                        }
                    }
                }
            }
            logger.debug("Returning");
            return oFR;

        } catch (Exception ex) {
            logger.error("Exception thown: ", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser), ex);
            logger.debug("Returning");
            return oFR;

        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returinig: fieldRequiredForFieldValue");
        }
    }

    public OFunctionResult validateAlphanumericCommaSeparated(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateAlphanumericCommaSeparated");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateValidationJavaFunctionODM(
                odm, loggedUser, EntityActionValidation.class);
        if (inputValidationErrors != null) {
            logger.warn("there are Validation Errors");
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        EntityActionValidation validation = (EntityActionValidation) odm.getData().get(1);
        if (validation.getFunction().getName().equals("fieldRequiredForFieldValue")) {
            logger.debug("Returning");
            return oFR;
        }
        String fieldExpression = (String) odm.getData().get(2);
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            oem.getEM(loggedUser);
            String fieldValue = (String) BaseEntity.getValueFromEntity(validation, fieldExpression);
            if (fieldValue.length() == 0) {
                logger.debug("Returning");
                return oFR;
            }
            StringTokenizer tokenizer = new StringTokenizer(fieldValue, ",");
            // Loop on all tokens, and validate that every token is alphanumeric
            while (tokenizer.hasMoreTokens()) {
                String nextFieldExpression = tokenizer.nextToken();
                StringCharacterIterator iterator = new StringCharacterIterator(nextFieldExpression);
                Character character = iterator.first();
                while (character != CharacterIterator.DONE) {
                    if (Character.isLetterOrDigit(character) == false) {
                        // Tokenizer gets characters between commas, so commas will never occure
                        if (character.equals('.')) {
                            character = iterator.next();
                            continue;
                        }
                        oFR.addError(userMessageServiceRemote.getUserMessage(
                                "InvalidFormat", Collections.
                                        singletonList(fieldValue), systemUser));
                        break;
                    }
                    character = iterator.next();
                }
            }
            return oFR;
        } catch (Exception ex) {
            logger.error("Exception thown: ", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser), ex);
            logger.debug("Returning");
            return oFR;
        } finally {
            logger.debug("Returinig: validateAlphanumericCommaSeparated");
            oem.closeEM(loggedUser);
        }
    }

    public OFunctionResult validateFieldRequiredForFieldValue(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {

        logger.debug("Entering: validateFieldRequiredForFieldValue");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateValidationJavaFunctionODM(
                odm, loggedUser, EntityActionValidation.class);
        if (inputValidationErrors != null) {
            logger.warn("there are Validation Errors");
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        EntityActionValidation validation = (EntityActionValidation) odm.getData().get(1);
        try {
            if (!validation.getFunction().getName().equals("fieldRequiredForFieldValue")) {
                return oFR;
            }
            String fieldExpression = (String) odm.getData().get(2);
            String fieldValue = BaseEntity.getValueFromEntity(validation, fieldExpression).toString();
            if (fieldExpression.length() == 0) {
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "fieldRequired", Collections.singletonList(fieldExpression), loggedUser));
                logger.debug("Returning");
                return oFR;
            }
            oem.getEM(loggedUser);
            // Verify fieldName contains at least 2 fields
            StringTokenizer tokenizer = new StringTokenizer(fieldValue, ",");
            if (tokenizer.countTokens() < 2) {
                oFR.addError(userMessageServiceRemote.getUserMessage("CorruptedDataInField",
                        Collections.singletonList(fieldValue), loggedUser));
                logger.debug("Returning");
                return oFR;
            }
            String firstFieldName = tokenizer.nextToken();
            int indexOfEquals = firstFieldName.indexOf("=");
            // Return if doesn't contain '=', or '=' is at the beginning of the fieldExpression
            if (indexOfEquals < 1) {
                oFR.addError(userMessageServiceRemote.getUserMessage("CorruptedDataInField",
                        Collections.singletonList(firstFieldName), loggedUser));
                logger.debug("Returning");
                return oFR;
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser), ex);
            logger.debug("Returning");
            return oFR;
        } finally {
            oem.closeEM(loggedUser);
        }
        logger.debug("Returning: validateFieldRequiredForFieldValue");
        return oFR;
    }

    public OFunctionResult validateYearsNumber(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateYearsNumber");
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        int intData = 0;
        try {
            oem.getEM(systemUser);
            Object valueFromEntity = BaseEntity.getValueFromEntity(
                    odm.getData().get(1),
                    (String) odm.getData().get(2));
            if (valueFromEntity != null) {
                intData = Integer.parseInt(
                        valueFromEntity.toString());
                if (intData < 0 || intData > 99) {
                    oFR.addError(userMessageServiceRemote.getUserMessage(
                            "InvalidYearsNumber", Collections.singletonList(String.valueOf(intData)), systemUser));
                }
            }
        } catch (Exception ex) {
            if (ex.getClass() == NullPointerException.class) {
                logger.debug("Returning");
                return oFR;
            }
            logger.error("Exception thrown: ", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "InvalidYearsNumber", Collections.singletonList(String.valueOf(intData)), systemUser));
        } finally {
            oem.closeEM(systemUser);
        }
        logger.debug("Returning: validateYearsNumber");
        return oFR;
    }

    public OFunctionResult validateFieldHasNoSpaces(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateFieldHasNoSpaces");
        Method callBackMethod = null;
        try {
            callBackMethod = getClass().getDeclaredMethod("validateFieldHasNoSpaces",
                    getSingleFieldValidationMethodParmTypes());
            logger.debug("Returning: validateFieldHasNoSpaces");
            return callSingleFieldValidationMethod(callBackMethod, odm, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown:", ex);
            logger.debug("Returning: validateFieldHasNoSpaces");
            return new OFunctionResult();
        }
    }

    public OFunctionResult validateFieldHasNoSpaces(Object entity, String fieldExpression, OUser loggedUser) {
        logger.debug("Entering: validateFieldHasNoSpaces");
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            oem.getEM(systemUser);
            String fieldValue = BaseEntity.getValueFromEntity(entity, fieldExpression).toString();
            StringCharacterIterator iterator = new StringCharacterIterator(fieldValue);
            Character character = iterator.current();
            while (character != CharacterIterator.DONE) {
                if (Character.isSpaceChar(character)) {
                    oFR.addError(userMessageServiceRemote.getUserMessage(
                            "FieldCannotHaveSpaces", Collections.singletonList(fieldExpression), systemUser));
                    break;
                }
                character = iterator.next();
            }
        } catch (Exception ex) {
            if (ex.getClass() == NullPointerException.class) {
                logger.debug("Returning");
                return oFR;
            }
            logger.error("Exception trown:", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser), ex);
        } finally {
            oem.closeEM(systemUser);
        }
        logger.debug("Returning: validateFieldHasNoSpaces");
        return oFR;
    }

    //<editor-fold defaultstate="collapsed" desc="validateName">
    @Override
    public OFunctionResult validateName(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateName");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateValidationJavaFunctionODM(
                odm, loggedUser, BaseEntity.class);
        if (inputValidationErrors != null) {
            logger.warn("There is name Valdation Errors");
            logger.debug("Returning with Validation Errors: {}", inputValidationErrors.getReturnedDataMessage());
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        BaseEntity entity = (BaseEntity) odm.getData().get(1);
        String fieldExpression = (String) odm.getData().get(2);
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            oem.getEM(loggedUser);
            StringTokenizer tokenizer = new StringTokenizer(fieldExpression, ",");
            if (tokenizer.countTokens() < 1) {
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "CorruptedDataInField", Collections.singletonList(
                                fieldExpression), loggedUser));
                logger.debug("Returning");
                return oFR;
            }
            while (tokenizer.hasMoreElements()) {
                String fieldName = tokenizer.nextToken();
                String fieldValue = BaseEntity.getValueFromEntity(entity, fieldName).toString();
                boolean invalid = false;
                if (fieldValue.length() < 3 || fieldValue.length() > 16 || !Character.isLetter(fieldValue.charAt(0))
                        || !Character.isLetterOrDigit(fieldValue.charAt(fieldValue.length() - 1))) {
                    invalid = true;
                } else {
                    for (int i = 0; i < fieldValue.length(); i++) {
                        if (!Character.isLetterOrDigit(fieldValue.charAt(i)) && fieldValue.charAt(i) != '-'
                                && fieldValue.charAt(i) != '-' && fieldValue.charAt(i) != '.') {
                            invalid = true;
                            break;
                        }
                    }
                }
                if (invalid) {
                    oFR.addError(userMessageServiceRemote.getUserMessage(
                            "InvalidName", Collections.singletonList(fieldValue), loggedUser));
                } else {
                    //<editor-fold defaultstate="collapsed" desc="Temporary to change all login names to lower case">
                    fieldValue = fieldValue.toLowerCase();
                    BaseEntity.setValueInEntity(entity, fieldName, fieldValue);
                    oFR.setReturnValues(odm.getData());
                    //</editor-fold>
                }
            }
            return oFR;
        } catch (Exception ex) {
            logger.error("Exception thown: ", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser), ex);
            logger.debug("Returning");
            return oFR;
        } finally {
            logger.debug("Returning: validateName");
            oem.closeEM(loggedUser);
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="validateEmail">
    @Override
    public OFunctionResult validateEmail(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateEmail");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateValidationJavaFunctionODM(
                odm, loggedUser, BaseEntity.class);
        if (inputValidationErrors != null) {
            logger.warn("There are Validation errors");
            return inputValidationErrors;
        }
        // </editor-fold>
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        BaseEntity entity = (BaseEntity) odm.getData().get(1);
        try {
            String fieldExpression = (String) odm.getData().get(2);
            oem.getEM(loggedUser);
            StringTokenizer tokenizer = new StringTokenizer(fieldExpression, ",");
            if (tokenizer.countTokens() < 1) {
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "CorruptedDataInField", Collections.singletonList(
                                fieldExpression), loggedUser));
                logger.debug("Returning");
                return oFR;
            }
            while (tokenizer.hasMoreElements()) {
                String fieldName = tokenizer.nextToken();
                String fieldValue = BaseEntity.getValueFromEntity(entity, fieldName).toString();
                if (fieldValue == null || fieldValue.equals("")) {
                    logger.debug("Returning");
                    return oFR;
                }
                boolean invalid = false;
                invalid = !ValidationJavaFunctionBean.isValid(fieldValue);
                if (invalid) {
                    oFR.addError(userMessageServiceRemote.getUserMessage(
                            "InvalidEmail", Collections.singletonList(fieldValue), loggedUser));
                }
            }
            logger.debug("Returning");
            return oFR;

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser), ex);
            logger.debug("Returning");
            return oFR;

        } finally {
            logger.debug("Returning: validateEmail");
            oem.closeEM(loggedUser);
        }
    }
    //<editor-fold defaultstate="collapsed" desc="RFC 2822 Email Standard">
    private static final boolean ALLOW_DOMAIN_LITERALS = false;
    private static final boolean ALLOW_QUOTED_IDENTIFIERS = false;
    // RFC 2822 2.2.2 Structured Header Field Bodies
    private static final String wsp = "[ \\t]"; //space or tab
    private static final String fwsp = wsp + "*";
    //RFC 2822 3.2.1 Primitive tokens
    private static final String dquote = "\\\"";
    //ASCII Control characters excluding white space:
    private static final String noWsCtl = "\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x7F";
    //all ASCII characters except CR and LF:
    private static final String asciiText = "[\\x01-\\x09\\x0B\\x0C\\x0E-\\x7F]";
    // RFC 2822 3.2.2 Quoted characters:
    //single backslash followed by a text char
    private static final String quotedPair = "(\\\\" + asciiText + ")";
    //RFC 2822 3.2.4 Atom:
    private static final String atext = "[a-zA-Z0-9\\!\\#\\$\\%\\&\\'\\*\\+\\-\\/\\=\\?\\^\\_\\`\\{\\|\\}\\~]";
    private static final String atom = fwsp + atext + "+" + fwsp;
    private static final String dotAtomText = atext + "+" + "(" + "\\." + atext + "+)*";
    private static final String dotAtom = fwsp + "(" + dotAtomText + ")" + fwsp;
    //RFC 2822 3.2.5 Quoted strings:
    //noWsCtl and the rest of ASCII except the doublequote and backslash characters:
    private static final String qtext = "[" + noWsCtl + "\\x21\\x23-\\x5B\\x5D-\\x7E]";
    private static final String qcontent = "(" + qtext + "|" + quotedPair + ")";
    private static final String quotedString = dquote + "(" + fwsp + qcontent + ")*" + fwsp + dquote;
    //RFC 2822 3.2.6 Miscellaneous tokens
    private static final String word = "((" + atom + ")|(" + quotedString + "))";
    private static final String phrase = word + "+"; //one or more words.
    //RFC 1035 tokens for domain names:
    private static final String letter = "[a-zA-Z]";
    private static final String letDig = "[a-zA-Z0-9]";
    private static final String letDigHyp = "[a-zA-Z0-9-]";
    private static final String rfcLabel = letDig + "(" + letDigHyp + "{0,61}" + letDig + ")?";
    private static final String rfc1035DomainName = rfcLabel + "(\\." + rfcLabel + ")*\\." + letter + "{2,6}";
    //RFC 2822 3.4 Address specification
    //domain text - non white space controls and the rest of ASCII chars not including [, ], or \:
    private static final String dtext = "[" + noWsCtl + "\\x21-\\x5A\\x5E-\\x7E]";
    private static final String dcontent = dtext + "|" + quotedPair;
    private static final String domainLiteral = "\\[" + "(" + fwsp + dcontent + "+)*" + fwsp + "\\]";
    private static final String rfc2822Domain = "(" + dotAtom + "|" + domainLiteral + ")";
    private static final String domain = ALLOW_DOMAIN_LITERALS ? rfc2822Domain : rfc1035DomainName;
    private static final String localPart = "((" + dotAtom + ")|(" + quotedString + "))";
    private static final String addrSpec = localPart + "@" + domain;
    private static final String angleAddr = "<" + addrSpec + ">";
    private static final String nameAddr = "(" + phrase + ")?" + fwsp + angleAddr;
    private static final String mailbox = nameAddr + "|" + addrSpec;
    //now compile a pattern for efficient re-use:
    //if we're allowing quoted identifiers or not:
    private static final String patternString = ALLOW_QUOTED_IDENTIFIERS ? mailbox : addrSpec;
    public static final Pattern VALID_PATTERN = Pattern.compile(patternString);

    public static boolean isValid(String emailToCheck) {
        return VALID_PATTERN.matcher(emailToCheck).matches();
    }
    //</editor-fold>

    //</editor-fold>
    public OFunctionResult validateScreenFilterNumber(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Enternig: validateScreenFilterNumber");
        OFunctionResult ofr = new OFunctionResult();
        try {
            ScreenFilter filter = (ScreenFilter) odm.getData().get(1);

            List<String> conditions = new ArrayList();
            conditions.add("screen.dbid=" + filter.getScreen().getDbid());
            List<ScreenFilter> otherFilters = oem.loadEntityList(
                    ScreenFilter.class.getSimpleName(), conditions, null,
                    null, loggedUser);
            if (otherFilters != null && !otherFilters.isEmpty()) {
                ofr.addError(userMessageServiceRemote.getUserMessage(
                        "ScreenHasAnotherFilter", loggedUser));
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            return null;
        }
        logger.debug("Returning");
        return ofr;
    }

    public OFunctionResult validateDecimalMaskFieldDD(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateDecimalMaskFieldDD");
        OFunctionResult oFR = new OFunctionResult();
        try {
            ScreenField screenField = (ScreenField) odm.getData().get(1);
            DD screenFieldDD = screenField.getDd();

            SingleEntityScreen screen = screenField.getOScreen();
            long screenDBID = screen.getDbid();
            OEntityDTO actOnEntity = entitySetupServiceRemote.getOScreenEntityDTO(screenDBID, loggedUser);

            String actOnEntityClassPath = actOnEntity.getEntityClassPath();
            Class screenEntityClass = null;
            screenEntityClass = Class.forName(actOnEntityClassPath);

            List<ExpressionFieldInfo> infos = BaseEntity.parseFieldExpression(
                    screenEntityClass, screenField.getFieldExpression());

            Field field = infos.get(infos.size() - 1).field;
            if (field.getType() != BigDecimal.class && screenFieldDD != null
                    && (screenFieldDD.getDecimalMask() != null && screenFieldDD.getDecimalMask() != 0)) {
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "FieldIsNotDecimal", loggedUser));
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.debug("Returning with Null");
            return null;
        }
        logger.debug("Returning: validateDecimalMaskFieldDD");
        return oFR;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public OFunctionResult validatePassword(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        OUser user = (OUser) odm.getData().get(0);

        try {
            if (user.getCurrentPass() != null && user.getCurrentPass().equals(user.getNewPass())) {
                UserPasswordException userPasswordException = new UserPasswordException(UserPasswordException.PASSWORD_SAME_AS_CURRENT);
                throw userPasswordException;
            }
            long companyId = PortalUtil.getDefaultCompanyId();
            PasswordPolicy passwordPolicy = null;
            try {
                passwordPolicy = PasswordPolicyLocalServiceUtil.getDefaultPasswordPolicy(companyId);
            } catch (Exception ex) {
                logger.info("no default password policy");
            }
            if (passwordPolicy != null) {
                ClassLoader portalClassLoader = PortalClassLoaderUtil.getClassLoader();
                Class<?> pwdToolkitUtilClass = portalClassLoader.loadClass("com.liferay.portal.security.pwd.PwdToolkitUtil");
                MethodKey methodKey = new MethodKey(pwdToolkitUtilClass, "validate", long.class, long.class, String.class, String.class, PasswordPolicy.class);
                PortalClassInvoker.invoke(true, methodKey, companyId, new Long(0), user.getPassword(), user.getPassword(), passwordPolicy);
            }
        } catch (UserPasswordException upe) {
            logger.debug("Invalid password, error:" + upe.getType(), upe);

            String message = "";
            switch (upe.getType()) {
                case UserPasswordException.PASSWORD_ALREADY_USED:
                    message = "PasswordAlreadyUsed";
                    break;
                case UserPasswordException.PASSWORD_CONTAINS_TRIVIAL_WORDS:
                    message = "PasswordTrivialWords";
                    break;
                case UserPasswordException.PASSWORD_INVALID:
                    message = "PasswordInvalid";
                    break;
                case UserPasswordException.PASSWORD_LENGTH:
                    message = "PasswordLength";
                    break;
                case UserPasswordException.PASSWORD_TOO_TRIVIAL:
                    message = "PasswordTrivial";
                    break;
                case UserPasswordException.PASSWORD_TOO_YOUNG:
                    message = "PasswordYoung";
                    break;
                case UserPasswordException.PASSWORD_SAME_AS_CURRENT:
                    message = "PasswordsameAsCurrent";
                    break;
            }

            oFR.addError(userMessageServiceRemote.getUserMessage(
                    message, loggedUser));
        } catch (Exception e) {
            logger.error("Exception thrown: ", e);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), e);
        }

        logger.debug("returning");
        return oFR;
    }

    @Override
    public OFunctionResult validateCalendarDD(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        DD dd = (DD) odm.getData().get(0);
        try {
            if (dd.getControlType().getValue().equals("Calendar")) {
                if (dd.getCalendarPattern() == null) {
                    oFR.addError(userMessageServiceRemote.getUserMessage("InvalidCalendarPattern", loggedUser));
                }
            }
        } catch (Exception e) {
            logger.error("Exception thrown: ", e);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), e);
        }
        logger.debug("returning");
        return oFR;
    }

    public OFunctionResult validateSpecialChar(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {

        OFunctionResult oFR = new OFunctionResult();
        BaseEntity be = (BaseEntity) odm.getData().get(0);

        String message = "";

        Pattern p = Pattern.compile("[!'%]");
        Matcher m = null;

        List<Field> fields = be.getAllFields();
        String fieldValue = "";

        try {
            for (int i = 0; i < fields.size(); i++) {
                if (fields.get(i).getType() == String.class) {
                    fieldValue = (String) BaseEntity.getValueFromEntity(be,
                            fields.get(i).getName());
                    if (fieldValue != null) {
                        m = p.matcher(fieldValue);

                        if (m.find()) {
                            message = "special characters";
                            oFR.addError(userMessageServiceRemote.getUserMessage(
                                    message, loggedUser));
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    e.getMessage(), loggedUser));
        }
        logger.debug("returning");
        return oFR;
    }

    public OFunctionResult validateHelpLength(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        DD ddScreen = (DD) odm.getData().get(0);
        try {
            if (ddScreen.getHelpMsgLength() != 0) {
                if (ddScreen.getInlineHelp().length() > ddScreen.getHelpMsgLength()) {
                    oFR.addError(userMessageServiceRemote.getUserMessage(
                            "InvalidHelpMessageLength", loggedUser));
                }
            }
        } catch (Exception e) {
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    e.getMessage(), loggedUser));
        }
        logger.debug("returning");
        return oFR;
    }

    /**
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return function to validate Screen Group on Form Screen
     */
    @Override
    public OFunctionResult validateScreenGroup(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        ScreenField screenField = (ScreenField) odm.getData().get(0);

        try {
            if (screenField.getOScreen() instanceof FormScreen) {
                if (!screenField.getOScreen().getScreenControlsGroups().isEmpty()) {
                    if (screenField.getScreenFieldGroup().getGroupFields() == null) {
                        oFR.addError(userMessageServiceRemote.getUserMessage(
                                "InvalidScreenGroup", loggedUser));
                    }
                }
            }
        } catch (Exception e) {
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    e.getMessage(), loggedUser));
        }
        logger.debug("returning");
        return oFR;
    }

    @Override
    public OFunctionResult validateUniqueUdc(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        UDC udc = (UDC) odm.getData().get(0);

        try {
            if (udc != null) {
                List conditions = new ArrayList();
                conditions.add("code='" + udc.getCode() + "'");
                conditions.add("type.code='" + udc.getType().getCode() + "'");
                conditions.add(OEntityManager.CONFCOND_GET_ACTIVEONLY);
                List<UDC> udcList = (List<UDC>) oem.loadEntityList("UDC", conditions, udc.getFieldExpressions(), null, loggedUser);
                if (udcList != null && !udcList.isEmpty()) {
                    if (udcList.size() > 1 || udcList.get(0).getDbid() != udc.getDbid()) {
                        oFR.addError(userMessageServiceRemote.getUserMessage(
                                "ValidateUniqueUdc", loggedUser));
                        return oFR;
                    }
                }
            }
        } catch (Exception e) {
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", loggedUser));
        }
        logger.debug("returning");
        return oFR;
    }
}
