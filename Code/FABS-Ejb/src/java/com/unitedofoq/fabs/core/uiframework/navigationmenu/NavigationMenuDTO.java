/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.navigationmenu;

import java.util.ArrayList;
import java.util.List;

import com.unitedofoq.fabs.core.function.OMenu;
import com.unitedofoq.fabs.core.security.user.RoleMenu;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author M. Nagy
 */
public class NavigationMenuDTO implements Comparable<NavigationMenuDTO> {
    final static Logger logger = LoggerFactory.getLogger(NavigationMenuDTO.class);
    private int tanentNum;
    private Long roleId;
    private List<OfunctionDTO> functions;
    private Long menuId;
    private String menuName;
    private String menuNameTrans;
    private List<OMenuDTO> childMenues;
    private int menuSortIndex;
    private String hierCode;
    private Long largeImageDbid;
    private String imageURL;

    public NavigationMenuDTO(Long roleId, Long menuId, int sortIndex, String name, String nameTrans,String hierCode, List<OMenuDTO> childMenues, List<OfunctionDTO> functions,Long imageDBID) {
        logger.debug("Entering");
        this.largeImageDbid = imageDBID;
        this.roleId = roleId;
        this.menuId = menuId;
        this.childMenues = childMenues;
        this.menuSortIndex = sortIndex;
        this.menuName = name;
        this.menuNameTrans = nameTrans;
        this.hierCode = hierCode;
        if(functions==null){
            this.functions = new ArrayList<OfunctionDTO>();           
        }else{
            this.functions = functions;
        }
        logger.debug("Returning");
    }

    public static NavigationMenuDTO constractNMDTO(Object obj,Long RoleID,ArrayList<OMenuDTO> subs,ArrayList<OfunctionDTO> funs,String imageURL) {
        logger.debug("Entering");
        NavigationMenuDTO NMDTO = null;
        if (obj instanceof RoleMenu) {
            logger.trace("this is a RoleMenu");
            RoleMenu rm = (RoleMenu) obj;
            ArrayList<OMenuDTO> chiledMenus = subs;
            ArrayList<OfunctionDTO> menuFunctions =funs;
            
            Long roleDBID = rm.getOrole().getDbid();
            Long MenuDBID = rm.getOmenu().getDbid();
            
            if (rm.isInActive() == false && rm.getOmenu().isActive() == true) {
                NMDTO = new NavigationMenuDTO(roleDBID, MenuDBID, rm.getOmenu().getSortIndex(),
                    rm.getOmenu().getName(),rm.getOmenu().getNameTranslated(), rm.getOmenu().getHierCode(), chiledMenus, menuFunctions,new Long("0"));
            } else {
                NMDTO = null;
            }
        }else if(obj instanceof OMenu){
            logger.trace("this is an OMenu");
            OMenu M = (OMenu) obj;
            ArrayList<OMenuDTO> chiledMenus = subs;
            ArrayList<OfunctionDTO> menuFunctions =funs;
            if (M.isInActive() == false) {
                Long imagDBID;
                if(M.getLargeMenuImage()==null){
                    imagDBID=new Long(0);
                }else{
                imagDBID = M.getLargeMenuImage().getDbid();
                }
                NMDTO = new NavigationMenuDTO(RoleID, M.getDbid(),M.getSortIndex(),
                    M.getName(),M.getNameTranslated(),M.getHierCode(), chiledMenus, menuFunctions,imagDBID.longValue());
                NMDTO.setImageURL(imageURL);
            } else {
                logger.trace("this is not a RoleMenu an OMenu");
                NMDTO = null;
            }
        }
        logger.debug("Returning");
        return NMDTO;
    }

    
    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public List<OMenuDTO> getChildMenues() {
        return childMenues;
    }

    public void setChildMenues(List<OMenuDTO> childMenues) {
        if (this.childMenues == null) {
            this.childMenues = new ArrayList<OMenuDTO>();
        }
        this.childMenues.addAll(childMenues);
    }

    public int getMenuSortIndex() {
        return menuSortIndex;
    }

    public void setMenuSortIndex(int menuSortIndex) {
        this.menuSortIndex = menuSortIndex;
    }

    public int getTanentNum() {
        return tanentNum;
    }

    public void setTanentNum(int tanentNum) {
        this.tanentNum = tanentNum;
    }

    public List<OfunctionDTO> getFunctions() {
        return functions;
    }

    public void setFunctions(List<OfunctionDTO> functions) {
        this.setFunctions(functions);
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public String getHierCode() {
        return hierCode;
    }

    public void setHierCode(String hierCode) {
        this.hierCode = hierCode;
    }

    public Long getLargeImageDbid() {
        return largeImageDbid;
    }

    public void setLargeImageDbid(Long largeImageDbid) {
        this.largeImageDbid = largeImageDbid;
    }

    public String getImageURL() {
        return imageURL;
    }

    /**
     * @param imageURL the imageURL to set
     */
    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    /**
     * @return the menuNameTrans
     */
    public String getMenuNameTrans() {
        return menuNameTrans;
    }

    /**
     * @param menuNameTrans the menuNameTrans to set
     */
    public void setMenuNameTrans(String menuNameTrans) {
        this.menuNameTrans = menuNameTrans;
    }

    @Override
    public int compareTo(NavigationMenuDTO o) {
        return this.menuSortIndex - o.menuSortIndex;
    }
    
}
