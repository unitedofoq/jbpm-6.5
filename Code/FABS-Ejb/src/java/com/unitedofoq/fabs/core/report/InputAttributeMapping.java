/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.report;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.unitedofoq.fabs.core.datatype.ODataTypeAttribute;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;

/**
 *
 * @author melsayed
 */
@Entity
@VersionControlSpecs(omoduleFieldExpression="oDataTypeAttribute.oDataType.omodule")
public class InputAttributeMapping extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="fieldExpression">
    @Column(nullable=false)
    private String fieldExpression;
    /**
     * @return the fieldExpression
     */
    public String getFieldExpression() {
        return fieldExpression;
    }

    public String getFieldExpressionDD() {
        return "InputAttributeMapping_fieldExpression";
    }

    /**
     * @param fieldExpression the fieldExpression to set
     */
    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="oDataTypeAttribute">
    @JoinColumn(nullable=false, name="ODataTypeAttribute_DBID")
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private ODataTypeAttribute oDataTypeAttribute;
    /**
     * @return the oDataTypeAttribute
     */
    public ODataTypeAttribute getODataTypeAttribute() {
        return oDataTypeAttribute;
    }

    public String getODataTypeAttributeDD() {
        return "InputAttributeMapping_oDataTypeAttribute";
    }

    /**
     * @param oDataTypeAttribute the oDataTypeAttribute to set
     */
    public void setODataTypeAttribute(ODataTypeAttribute oDataTypeAttribute) {
        this.oDataTypeAttribute = oDataTypeAttribute;
    }
    //</editor-fold>
}
