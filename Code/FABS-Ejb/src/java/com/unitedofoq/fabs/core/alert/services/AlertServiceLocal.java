/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.alert.services;

import java.util.List;

import javax.ejb.Local;


import com.unitedofoq.fabs.core.alert.entites.dto.AlertDTO;
import com.unitedofoq.fabs.core.alert.entities.FiredAlert;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author nkhalil
 */
@Local
public interface AlertServiceLocal {
    /**
     *
     * @param oUser
     * @return
     */
    public List<AlertDTO> getAlertJobDTOs(OUser oUser);
    
    /**
     * Exposing this function to allow developers to get a specific alert from DB,
     *  alter its content, then fire it
     * @param oUser
     * @param alertTitle
     * @return 
     */
    public List<AlertDTO> getAlertJobDTOs(OUser oUser, String alertTitle);

    public boolean validateCondition(BaseEntity baseEntity,
            String fieldExpression,
            String operator,
            String conditionValue,
            OUser oUser);

    public OFunctionResult runAlert(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult attachAlertFunction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateAlertSql(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult deleteAlertFunctionAndScheduleExpression(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public List<FiredAlert> getAllFiredAlert(OUser loggedUser);

    /**
     * Exposing this function to allow developers to fire whatever alert they want, 
     * whenever they want it
     * @param alertDTO
     * @param loggedUser
     * @return 
     */
    public boolean fireAlert(AlertDTO alertDTO, OUser loggedUser);
}
