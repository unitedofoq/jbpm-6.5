/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.mailmerge;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;

/**
 *
 * @author 3dly
 */
@Entity
@ParentEntity(fields = "mailMergeBasic")
@ChildEntity(fields = "mailMergeContent")
public class MailMergeField extends BaseEntity {

    //<editor-fold defaultstate="collapsed" desc="index">
    @Column
    private int sortIndex;

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getSortIndexDD() {
        return "MailMergeField_sortIndex";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="mailMergeBasic">
    @ManyToOne
    private MailMergeBasic mailMergeBasic;

    public MailMergeBasic getMailMergeBasic() {
        return mailMergeBasic;
    }

    public void setMailMergeBasic(MailMergeBasic mailMergeBasic) {
        this.mailMergeBasic = mailMergeBasic;
    }

    public String getMailMergeBasicDD() {
        return "MailMergeField_mailMergeBasic";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="fieldExpression">
    private String fieldExpression;

    public String getFieldExpression() {
        return fieldExpression;
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }

    public String getFieldExpressionDD() {
        return "MailMergeField_fieldExpression";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="mailmergeContent">
    @OneToMany
    private List<MailMergeContent> mailMergeContent;

    public List<MailMergeContent> getMailMergeContent() {
        return mailMergeContent;
    }

    public void setMailMergeContent(List<MailMergeContent> mailMergeContent) {
        this.mailMergeContent = mailMergeContent;
    }

    public String getMailMergeContentDD() {
        return "MailMergeBasic_mailMergeContent";
    }
    //</editor-fold>
}
