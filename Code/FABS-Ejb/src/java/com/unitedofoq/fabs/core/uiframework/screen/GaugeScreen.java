/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitysetup.OEntity;

/**
 *
 * @author bassem
 */
@Entity
@DiscriminatorValue(value = "GCHART")
@ChildEntity(fields={"screenInputs", "screenOutputs","screenFilter","screenFunctions"})
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class GaugeScreen extends FormScreen {    
    // <editor-fold defaultstate="collapsed" desc="gaugeScreenValues">
    @OneToMany(mappedBy = "gaugeScreen")
    private List<GaugeScreenValue> gaugeScreenValues;

    public List<GaugeScreenValue> getGaugeScreenValues() {
        return gaugeScreenValues;
    }

    public void setGaugeScreenValues(List<GaugeScreenValue> gaugeScreenValues) {
        this.gaugeScreenValues = gaugeScreenValues;
    }
    
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="label">
    @JoinColumn(name="xaxisTitle_dbid")
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private DD label;
    public DD getLabel() {
        return label;
    }

    public void setLabel(DD label) {
        this.label = label;
    }

    public String getLabeleDD() {
        return "ChartScreen_xaxisTitle";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="gaugeOEntity">
    @JoinColumn(name="oactOnEntity_dbid", insertable=false, updatable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OEntity gaugeOEntity;

    public OEntity getGaugeOEntity() {
        return gaugeOEntity;
    }

    public void setGaugeOEntity(OEntity gaugeOEntity) {
        this.gaugeOEntity = gaugeOEntity;
    }
    public String getGaugeOEntityDD()                {   return "SingleEntityScreen_oActOnEntity";  }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="height">
    @Column(name="height")
    private int gheight;

    public int getGheight() {
        return gheight;
    }

    public void setGheight(int gheight) {
        this.gheight = gheight;
    }    
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="width">
    @Column(name="width")
    private int gwidth;

    public String getGwidthDD() {
        return "ChartScreen_width";
    }
    
    public int getGwidth() {
        return gwidth;
    }

    public void setGwidth(int gwidth) {
        this.gwidth = gwidth;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="showTickLabels">
    private boolean showTickLabels;

    public boolean isShowTickLabels() {
        return showTickLabels;
    }

    public void setShowTickLabels(boolean showTickLabels) {
        this.showTickLabels = showTickLabels;
    }
    public String getShowTickLabelsDD()      {   return "FormScreen_basicDetail";  }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="labelHeightAdjust">
    private int labelHeightAdjust;

    public int getLabelHeightAdjust() {
        return labelHeightAdjust;
    }

    public void setLabelHeightAdjust(int labelHeightAdjust) {
        this.labelHeightAdjust = labelHeightAdjust;
    }
    public String getLabelHeightAdjustDD() {
        return "FormScreen_columnsNo";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="intervalOuterRadius">
    private int intervalOuterRadius;

    public int getIntervalOuterRadius() {
        return intervalOuterRadius;
    }

    public void setIntervalOuterRadius(int intervalOuterRadius) {
        this.intervalOuterRadius = intervalOuterRadius;
    }
    public String getIntervalOuterRadiusDD() {
        return "Gaugcegh_intervalOuterRadius";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="gvalueFieldExp">
    @Column(name="dim3LabelFieldExp")
    private String gvalueFieldExp;

    public String getGvalueFieldExp() {
        return gvalueFieldExp;
    }

    public void setGvalueFieldExp(String gvalueFieldExp) {
        this.gvalueFieldExp = gvalueFieldExp;
    }
    
    public String getGvalueFieldExpExpDD() {
        return "ChartScreen_dim3LabelFieldExp";
    }    
    // </editor-fold>
}
