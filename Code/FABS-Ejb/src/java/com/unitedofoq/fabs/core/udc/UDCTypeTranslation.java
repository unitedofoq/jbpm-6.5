
package com.unitedofoq.fabs.core.udc;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class UDCTypeTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="value">
    @Column
    private String value;

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    // </editor-fold>

}
