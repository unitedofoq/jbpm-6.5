/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.orgchart;

/**
 *
 * @author MEA
 */
public class OrgChartFunctionDTO {
    private long DBID;
    private String name;
    private int portletHeight;
    private String viewPageCode;

    public OrgChartFunctionDTO() {
    }

    public OrgChartFunctionDTO(long DBID, String name, int portletHeight, String viewPageCode) {
        this.DBID = DBID;
        this.name = name;
        this.portletHeight = portletHeight;
        this.viewPageCode = viewPageCode;
    }

    public long getDBID() {
        return DBID;
    }

    public void setDBID(long DBID) {
        this.DBID = DBID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPortletHeight() {
        return portletHeight;
    }

    public void setPortletHeight(int portletHeight) {
        this.portletHeight = portletHeight;
    }

    public String getViewPageCode() {
        return viewPageCode;
    }

    public void setViewPageCode(String viewPageCode) {
        this.viewPageCode = viewPageCode;
    }
    
}
