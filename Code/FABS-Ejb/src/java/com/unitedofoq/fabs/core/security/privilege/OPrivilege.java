package com.unitedofoq.fabs.core.security.privilege;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.security.user.RolePrivilege;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name="PTYPE", discriminatorType=DiscriminatorType.STRING)
@DiscriminatorValue("GENERAL")
public class OPrivilege extends ObjectBaseEntity {

    public enum ActionName {
        ADD, UPDATE, DELETE, VIEW
    }

    @Column(unique=true, nullable=false)
    protected String name;
    public String getNameDD(){
        return "OPrivilege_name";
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    @Translatable(translationField="descriptionTranslated")
    protected String description;
    public String getDescriptionTranslatedDD(){
        return "OPrivilege_description";
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @OneToMany(mappedBy="oprivilege")
    protected List<RolePrivilege> rolePrivileges = null;
    public List<RolePrivilege> getRolePrivileges() {
        return rolePrivileges;
    }
    public void setRolePrivileges(List<RolePrivilege> rolePrivileges) {
        this.rolePrivileges = rolePrivileges;
    }
}
