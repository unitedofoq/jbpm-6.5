/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.ExceptionHandler.thrower.factory;

import com.unitedofoq.fabs.core.Exception.ApplicationExecption;
import com.unitedofoq.fabs.core.Exception.BasicException;
import com.unitedofoq.fabs.core.Exception.BusinessException;
import com.unitedofoq.fabs.core.Exception.SystemExecption;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessage;

/**
 *
 * @author mostafa
 */
public class ExceptionFactory {

    public static void throwException(UserMessage userMessage,
            ExceptionType exceptionType, OUser loggedUser) throws BasicException{
        switch(exceptionType){
            case APPLICATION:
                throw new ApplicationExecption(userMessage, loggedUser);
            case BUSINESS:
                throw new BusinessException(userMessage, loggedUser);
            case SYSTEM:
                throw new SystemExecption(userMessage, loggedUser);
        }
    }
}
