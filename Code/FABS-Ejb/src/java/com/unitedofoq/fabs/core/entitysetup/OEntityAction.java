package com.unitedofoq.fabs.core.entitysetup;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

/**
 *
 * @author arezk
 */
//Overview: Actions displayed in the balloon
@Entity
@NamedQuery(
        name = "getEntityDisplayedActions",
        query = " SELECT    oea "
        + " FROM      OEntityAction oea "
        + " WHERE     oea.displayed = true "
        + " AND       oea.inActive = false "
        + " AND       oea.oownerEntity.dbid = :oEntityDBID "
        + " ORDER BY  oea.sortIndex")
@ParentEntity(fields = {"oownerEntity"})
@ChildEntity(fields = {"validations", "posts", "entityActionPrivilege"})
@VersionControlSpecs(versionControlType = VersionControlType.Parent)
public class OEntityAction extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="name">
    @Transient
    @Translation(originalField = "name")
    private String nameTranslated;

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    @Translatable(translationField = "nameTranslated")
    @Column(nullable = false)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameTranslatedDD() {
        return "OEntityAction_name";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ofunction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private OFunction ofunction;

    public OFunction getOfunction() {
        return ofunction;
    }

    public void setOfunction(OFunction oFunction) {
        this.ofunction = oFunction;
    }

    public String getOfunctionDD() {
        return "OEntityAction_function";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="oOwnerEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private OEntity oownerEntity = null;
    public OEntity getOOwnerEntity() {
        return oownerEntity;
    }
    public void setOOwnerEntity(OEntity oOwnerEntity) {
        this.oownerEntity = oOwnerEntity;
    }

    public String getOownerEntityDD() {
        return "OEntityAction_oownerEntity";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entityActionPrivilege">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, mappedBy = "entityAction")
    private EntityActionPrivilege entityActionPrivilege;

    public EntityActionPrivilege getEntityActionPrivilege() {
        return entityActionPrivilege;
    }

    public void setEntityActionPrivilege(EntityActionPrivilege entityActionPrivilege) {
        this.entityActionPrivilege = entityActionPrivilege;
    }

    public String getEntityActionPrivilegeDD() {
        return "OEntityAction_entityActionPrivilege";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="sortIndex">
    @Column(nullable = false)
    private int sortIndex;

    public String getSortIndexDD() {
        return "OEntityAction_sortIndex";
    }

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="displayed">
    @Column(nullable = false)
    private boolean displayed;

    public boolean isDisplayed() {
        return displayed;
    }

    public void setDisplayed(boolean displayed) {
        this.displayed = displayed;
    }

    public String getDisplayedDD() {
        return "OEntityAction_displayed";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="posts">
    @OneToMany(mappedBy = "oentityAction")
    private List<EntityActionPost> posts;
    @OneToMany(mappedBy = "entityAction")
    private List<EntityActionValidation> validations;

    public List<EntityActionValidation> getValidations() {
        return validations;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="validations">

    public void setValidations(List<EntityActionValidation> validations) {
        this.validations = validations;
    }

    public List<EntityActionPost> getPosts() {
        return posts;
    }

    public void setPosts(List<EntityActionPost> posts) {
        this.posts = posts;
    }

    public String getPostsDD() {
        return "OEntityAction_posts";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="parentValidation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OFunction parentValidation;

    public OFunction getParentValidation() {
        return parentValidation;
    }

    public void setParentValidation(OFunction parentValidation) {
        this.parentValidation = parentValidation;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="actionType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC actionType;

    public UDC getActionType() {
        return actionType;
    }

    public void setActionType(UDC actionType) {
        this.actionType = actionType;
    }

    public String getActionTypeDD() {
        return "OEntityAction_actionType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Action Type Static Fields">
    /**
     * Action Type UDC DBID of "Create" Action
     */
    @Transient
    public static final Long ATDBID_CREATE = 121L;
    /**
     * Action Type UDC DBID of "Retrieve" Action
     */
    @Transient
    public static final Long ATDBID_RETRIEVE = 122L;
    /**
     * Action Type UDC DBID of "Update" Action
     */
    @Transient
    public static final Long ATDBID_UPDATE = 123L;
    /**
     * Action Type UDC DBID of "Delete" Action
     */
    @Transient
    public static final Long ATDBID_ACTIVATION = 124L;
    /**
     * Action Type UDC DBID of "Duplicate" Action
     */
    @Transient
    public static final Long ATDBID_DUPLICATE = 1027861L;
    /**
     * Action Type UDC DBID of "Parent Pre Save" Action
     */
    @Transient
    public static final Long ATDBID_ParentPreSave = 125L;
    /**
     * Action Type UDC DBID of "Hard Remove" Action
     */
    @Transient
    public static final Long ATDBID_DELETE = 126L;

    /**
     * @return the oownerEntity
     */
    public OEntity getOownerEntity() {
        return oownerEntity;
    }

    /**
     * @param oownerEntity the oownerEntity to set
     */
    public void setOownerEntity(OEntity oownerEntity) {
        this.oownerEntity = oownerEntity;
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Display Condition">
    private String displayCondition;

    public String getDisplayCondition() {
        return displayCondition;
    }

    public void setDisplayCondition(String displayCondition) {
        this.displayCondition = displayCondition;
    }

    public String getDisplayConditionDD() {
        return "OEntityAction_displayCondition";
    }
    //</editor-fold>
}
