/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.security.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.security.privilege.OPrivilege;

/**
 *
 * @author ahussien
 */
@Entity
@ParentEntity(fields={"orole"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class RolePrivilege extends BaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="granted">
    @Column(nullable=false)
    private boolean granted = true;

    public boolean isGranted() {
        return granted;
    }

    public void setGranted(boolean granted) {
        this.granted = granted;
    }
    
    public String getGrantedDD(){
        return "RolePrivilege_granted";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="orole">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
    private ORole orole;
    public String getOroleDD(){
        return "RolePrivilege_orole";
    }
    /**
     * @return the orole
     */
    public ORole getOrole() {
        return orole;
    }

    /**
     * @param orole the orole to set
     */
    public void setOrole(ORole orole) {
        this.orole = orole;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="oprivilege">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
    private OPrivilege oprivilege;
    public String getOprivilegeDD(){
        return "RolePrivilege_oPrivilege";
    }
    /**
     * @return the oprivilege
     */
    public OPrivilege getOprivilege() {
        return oprivilege;
    }

    /**
     * @param oprivilege the oprivilege to set
     */
    public void setOprivilege(OPrivilege oprivilege) {
        this.oprivilege = oprivilege;
    }
    // </editor-fold>
}
