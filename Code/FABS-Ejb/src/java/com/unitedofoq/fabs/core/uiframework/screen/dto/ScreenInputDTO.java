/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mostafa
 */
public class ScreenInputDTO {
    
    public ScreenInputDTO(String inputHeader, long odataTypeDBID, long dbid, boolean inActive, String odataTypeName){
        this.inputHeader = inputHeader;
        this.odataTypeDBID = odataTypeDBID;
        this.dbid = dbid;
        this.inActive = inActive;
        this.odataTypeName = odataTypeName;
    }
    
    public ScreenInputDTO(){
        
    }
    
    private String inputHeader;
    private long odataTypeDBID;
    private long dbid;
    private boolean inActive;  
    private String odataTypeName;
    
    private List<ScreenDTMappingAttrDTO> attributesMapping = new ArrayList<ScreenDTMappingAttrDTO>();
    /**
     * @return the inputHeader
     */
    public String getInputHeader() {
        return inputHeader;
    }

    /**
     * @param inputHeader the inputHeader to set
     */
    public void setInputHeader(String inputHeader) {
        this.inputHeader = inputHeader;
    }

    /**
     * @return the odataTypeDBID
     */
    public long getOdataTypeDBID() {
        return odataTypeDBID;
    }

    /**
     * @param odataTypeDBID the odataTypeDBID to set
     */
    public void setOdataTypeDBID(long odataTypeDBID) {
        this.odataTypeDBID = odataTypeDBID;
    }

    /**
     * @return the dbid
     */
    public long getDbid() {
        return dbid;
    }

    /**
     * @param dbid the dbid to set
     */
    public void setDbid(long dbid) {
        this.dbid = dbid;
    }

    /**
     * @return the attributesMapping
     */
    public List<ScreenDTMappingAttrDTO> getAttributesMapping() {
        return attributesMapping;
    }

    /**
     * @param attributesMapping the attributesMapping to set
     */
    public void setAttributesMapping(List<ScreenDTMappingAttrDTO> attributesMapping) {
        this.attributesMapping = attributesMapping;
    }

    /**
     * @return the inActive
     */
    public boolean isInActive() {
        return inActive;
    }

    /**
     * @param inActive the inActive to set
     */
    public void setInActive(boolean inActive) {
        this.inActive = inActive;
    }

    /**
     * @return the odataTypeName
     */
    public String getOdataTypeName() {
        return odataTypeName;
    }

    /**
     * @param odataTypeName the odataTypeName to set
     */
    public void setOdataTypeName(String odataTypeName) {
        this.odataTypeName = odataTypeName;
    }
}
