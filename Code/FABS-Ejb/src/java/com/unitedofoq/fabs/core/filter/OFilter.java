/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.filter;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;

/**
 *
 * @author mmohamed
 */
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name="FTYPE", discriminatorType=DiscriminatorType.STRING)
@ChildEntity(fields={"filterFields"})
public class OFilter extends ObjectBaseEntity{

    public enum FilterType {
        SCREEN_FILTER, REPORT_FILTER, WEBSERVICE_FILTER
    }

    @Column(nullable=false)
    String filterName;
    boolean alwaysShow;

    @OneToMany(mappedBy="filter")
    List<FilterField> filterFields;

    public List<FilterField> getFilterFields() {
        List<FilterField> notDeletedFields = new ArrayList<FilterField>();
        for (FilterField field : filterFields) {
            if (!field.isInActive())
                notDeletedFields.add(field);
        }
        return notDeletedFields;
    }

    public void setFilterFields(List<FilterField> filterFields) {
        this.filterFields = filterFields;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    UDC layout;

    public boolean isAlwaysShow() {
        return alwaysShow;
    }

    public String getAlwaysShowDD() {
        return "OFilter_alwaysShow";
    }

    public void setAlwaysShow(boolean alwaysShow) {
        this.alwaysShow = alwaysShow;
    }

    public String getFilterName() {
        return filterName;
    }

    public String getFilterNameDD() {
        return "OFilter_filterName";
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public UDC getLayout() {
        return layout;
    }
    
    public String getLayoutDD() {
        return "OFilter_layout";
    }

    public void setLayout(UDC layout) {
        this.layout = layout;
    }   
}
