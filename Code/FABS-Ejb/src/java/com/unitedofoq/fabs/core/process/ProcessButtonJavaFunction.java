/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.process;

import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.function.JavaFunction;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author ianwar
 */
@Entity
public class ProcessButtonJavaFunction extends ObjectBaseEntity {

    @Column(name = "validationName")
    private String validationName;

    @ManyToOne
    @JoinColumn(nullable = false, name = "processButton_DBID")
    private ProcessButton processButton;

    @ManyToOne
    @JoinColumn(nullable = false, name = "javaFunction_DBID")
    private JavaFunction javaFunction;

    public String getValidationName() {
        return validationName;
    }

    public String getValidationNameDD() {
        return "ProcessButtonJavaFunction_validationName";
    }

    public void setValidationName(String validationName) {
        this.validationName = validationName;
    }

    public ProcessButton getProcessButton() {
        return processButton;
    }

    public String getProcessButtonDD() {
        return "ProcessButtonJavaFunction_processButton";
    }

    public void setProcessButton(ProcessButton processButton) {
        this.processButton = processButton;
    }

    public JavaFunction getJavaFunction() {
        return javaFunction;
    }

    public String getJavaFunctionDD() {
        return "ProcessButtonJavaFunction_javaFunction";
    }

    public void setJavaFunction(JavaFunction javaFunction) {
        this.javaFunction = javaFunction;
    }

}
