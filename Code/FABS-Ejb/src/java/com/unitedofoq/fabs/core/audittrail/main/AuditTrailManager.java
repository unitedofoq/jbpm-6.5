/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.audittrail.main;

import com.unitedofoq.fabs.central.EntityManagerWrapperLocal;
import com.unitedofoq.fabs.core.audittrail.exception.InvalidMethodSignatureException;
import com.unitedofoq.fabs.core.audittrail.exception.UnreachableMessageQueueException;
import com.unitedofoq.fabs.core.audittrail.main.valueholder.AuditTrailMessageObject;
import com.unitedofoq.fabs.core.audittrail.persistant.entity.TransactionStatus;
import com.unitedofoq.fabs.core.audittrail.persistant.entity.TransactionType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mustafa
 */
@Stateful
@LocalBean
public class AuditTrailManager {

    @EJB
    private EntityManagerWrapperLocal emw;
    @EJB
    private EntitySetupServiceRemote ess;
    @EJB
    private OEntityManagerRemote entityManager;

    final static Logger logger = LoggerFactory.getLogger(AuditTrailManager.class);

    @AroundInvoke
    @TransactionAttribute(value = TransactionAttributeType.NOT_SUPPORTED)
    public Object auditTrailInterceptor(InvocationContext ctx) throws Exception {

        logger.info("Entering");
        Exception caughtException = null;
        Object[] params = ctx.getParameters();
        BaseEntity newObject = null;
        OUser loggedUser = null;
        Object result = null;

        if (params != null
                && params.length > 1
                && params[0] instanceof BaseEntity
                && params[1] instanceof OUser) {

            newObject = (BaseEntity) params[0];
            loggedUser = (OUser) params[1];

        } else {
            try {
                result = ctx.proceed();
            } catch (InvalidMethodSignatureException ex) {
                logger.error("Exception thrown", ex);
                throw new InvalidMethodSignatureException();
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            } finally {
                logger.info("Returning from: auditTrailInterceptor");
                return result;
            }
        }

        //Check Entity For Audit Trail Enabled
        OEntityDTO oEntity = ess.loadOEntityDTOByNameForAuditing(newObject.getClass().getSimpleName(), loggedUser);

        if (oEntity == null || !(oEntity.isAuditable()
                || oEntity.isSystemAuditable())) {
            logger.info("Returning");
            return ctx.proceed();
        }

        if (loggedUser.getDbid() == 0 && oEntity.isAuditable()
                && !oEntity.isSystemAuditable()) {
            logger.info("Returning");
            return ctx.proceed();
        }

        if (loggedUser.getDbid() != 0 && !oEntity.isAuditable()
                && oEntity.isSystemAuditable()) {
            logger.info("Returning");
            return ctx.proceed();
        }

        AuditTrailMessageObject messageObject = new AuditTrailMessageObject();

        try {
            if (ctx.getMethod().getName().contains("saveEntity")) {

                messageObject.setNewObject(newObject);

                //Set Operation Type
                if (newObject.getDbid() == 0) {
                    messageObject.setTransactionType(TransactionType.ADD);
                } else {
                    messageObject.setTransactionType(TransactionType.UPDATE);

                    List<String> fieldExpressions = new ArrayList<String>();
                    Hashtable<String, List<Field>> tableFields = BaseEntity.
                            getClassFieldsInSameTable(newObject.getClass());

                    for (Map.Entry<String, List<Field>> entry : tableFields.entrySet()) {
                        List<Field> fields = entry.getValue();

                        for (Field field : fields) {
                            fieldExpressions.add(field.getName());
                        }
                    }

                    BaseEntity oldObject;//= (BaseEntity) emw.createQuery(query, loggedUser).getSingleResult();
                    oldObject = (BaseEntity) entityManager.loadEntity(newObject.getClass().getSimpleName(), newObject.getDbid(), null, fieldExpressions, loggedUser);

                    messageObject.setOldObject(oldObject);
                }

            } else if (ctx.getMethod().getName().contains("deleteEntity")) {
                messageObject.setTransactionType(TransactionType.DELETE);
                messageObject.setOldObject(newObject);
                messageObject.setNewObject(newObject);
            } else if (ctx.getMethod().getName().contains("activateDeactivateEntity")) {
                messageObject.setTransactionType(TransactionType.ACTIVATE_DEACTIVATE);
                messageObject.setOldObject(newObject);
            } else {
                logger.info("Returning");
                return ctx.proceed();
            }

            messageObject.setoUser(loggedUser);
            messageObject.setUserTenantID(loggedUser.getTenant().getId());
            messageObject.setWorkingOnTenantID(loggedUser.getWorkingTenantID());
            messageObject.setoEntity(oEntity);
            messageObject.setAuditDate(new Date());
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            return result = ctx.proceed();
        }
        try {
            result = ctx.proceed();
            messageObject.setResult(TransactionStatus.Succeeded.toString());
        } catch (Exception ex) {
            messageObject.setResult(TransactionStatus.Failed.toString());
            caughtException = ex;
        } finally {
            try {
                sendMessage(messageObject);
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
            logger.info("Returning");
            if (caughtException instanceof UserNotAuthorizedException) {
                throw caughtException;
            }
            return result;
        }
    }
    @Resource(mappedName = "java:/jms/AuditTrailMSGQueueFactory")
    private ConnectionFactory connectionFactory;
    @Resource(mappedName = "java:/jms/queue/AuditTrailMSGQueue")
    private Queue queue;

    public void sendMessage(AuditTrailMessageObject msg) throws UnreachableMessageQueueException {
        logger.debug("Entering");
        MessageProducer messageProducer;
        Connection connection = null;
        Session session = null;
        try {
            connection = connectionFactory.createConnection();
            session = connection.createSession(false,
                    Session.AUTO_ACKNOWLEDGE);

            ObjectMessage auditTrailMsg = session.createObjectMessage(msg);

            messageProducer = session.createProducer(queue);
            messageProducer.send(auditTrailMsg);

            messageProducer.close();

        } catch (JMSException ex) {
            logger.error("Exception thrown", ex);
            throw new UnreachableMessageQueueException();
        } finally {

            try {
                session.close();
            } catch (Exception ex) {
                logger.info(ex.getMessage());
            }
            try {
                connection.close();
            } catch (Exception ex) {
                logger.info(ex.getMessage());
            }
        }
        logger.debug("Returning");
    }
}
