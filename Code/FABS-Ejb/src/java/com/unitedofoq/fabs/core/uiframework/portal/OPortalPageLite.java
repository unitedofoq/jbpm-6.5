/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.portal;

import java.io.Serializable;
import java.util.HashMap;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.ReadOnly;

import com.unitedofoq.fabs.core.uiframework.screen.OScreenLite;

/**
 *
 * @author mibrahim
 */
@Entity
@ReadOnly
@NamedQueries(value={
    @NamedQuery(name="getPage",
        query = " SELECT    portalPageLite " +
                " FROM      OPortalPageLite portalPageLite" +
                " WHERE     portalPageLite.id = :dbid" +
                " AND       portalPageLite.langid=:langDBID")})
public class OPortalPageLite implements Serializable {
    @Id
    private long id;
    private String portletPageID;
    private String name;
    private long layoutTemplateDBID;
    private String layoutTemplateUDCValue;
    private String header;
    private long langid;

    public long getLangid() {
        return langid;
    }

    public void setLangid(long langid) {
        this.langid = langid;
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPortletPageID() {
        return portletPageID;
    }

    public void setPortletPageID(String portletPageID) {
        this.portletPageID = portletPageID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getLayoutTemplateDBID() {
        return layoutTemplateDBID;
    }

    public void setLayoutTemplateDBID(long layoutTemplateDBID) {
        this.layoutTemplateDBID = layoutTemplateDBID;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    /**
     * @return the layoutTemplateUDCValue
     */
    public String getLayoutTemplateUDCValue() {
        return layoutTemplateUDCValue;
    }

    /**
     * @param layoutTemplateUDCValue the layoutTemplateUDCValue to set
     */
    public void setLayoutTemplateUDCValue(String layoutTemplateUDCValue) {
        this.layoutTemplateUDCValue = layoutTemplateUDCValue;
    }

    //RDO
    // <editor-fold defaultstate="collapsed" desc="Page Screen Instances">
    @Transient
    private HashMap<String,OScreenLite> pageScreenInstancesList;

    /**
     * @return the pageScreenInstancesList
     */
    public HashMap<String,OScreenLite> getPageScreenInstancesList() {
        return pageScreenInstancesList;
    }

    /**
     * @param pageScreenInstancesList the pageScreenInstancesList to set
     */
    public void setPageScreenInstancesList(HashMap<String,OScreenLite> pageScreenInstancesList) {
        this.pageScreenInstancesList = pageScreenInstancesList;
    }
}
