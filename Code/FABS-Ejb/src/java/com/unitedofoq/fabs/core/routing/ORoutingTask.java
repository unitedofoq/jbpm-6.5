/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.routing;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

//import com.unitedofoq.fabs.core.alert.OAlert;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.function.JavaFunction;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.uiframework.screen.SingleEntityScreen;

/**
 *
 * @author lap3
 */
@Entity
@ParentEntity(fields={"routing"})
@ChildEntity(fields = {"taskActions"})
@VersionControlSpecs(versionControlType=VersionControlSpecs.VersionControlType.Parent)
public class ORoutingTask extends BaseEntity {
     // <editor-fold defaultstate="collapsed" desc="firstTask">
    private boolean firstTask;

    public String getFirstTaskDD() {
        return " ORoutingTask_firstTask";
    }
    
    public boolean isFirstTask() {
        return firstTask;
    }

    public void setFirstTask(boolean firstTask) {
        this.firstTask = firstTask;
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="name">

    @Translatable(translationField = "nameTranslated")
    @Column(unique = true, nullable = false)
    private String name;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    @Transient
    @Translation(originalField = "name")
    private String nameTranslated;

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }

    public String getNameTranslatedDD() {
        return "ORoutingTask_name";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    private String description;

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    /**
     * @return the descriptionTranslated
     */
    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    /**
     * @param descriptionTranslated the descriptionTranslated to set
     */
    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getDescriptionTranslatedDD() {
        return "ORoutingTask_description";
    }
    // </editor-fold>    
    
    // <editor-fold defaultstate="collapsed" desc="owner">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OUser owner;

    /**
     * @return the owner
     */
    public OUser getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(OUser owner) {
        this.owner = owner;
    }
    
    /**
     * @return the owner
     */
    public String getOwnerDD() {
        return "ORoutingTask_owner";
    }

    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="ownerJavaFunction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private JavaFunction ownerJavaFunction;
    /**
     * @return the ownerJavaFunction
     */
    public JavaFunction getOwnerJavaFunction() {
        return ownerJavaFunction;
    }

    /**
     * @param ownerJavaFunction the ownerJavaFunction to set
     */
    public void setOwnerJavaFunction(JavaFunction ownerJavaFunction) {
        this.ownerJavaFunction = ownerJavaFunction;
    }
     /**
     * @return the ownerJavaFunction
     */
    public String getOwnerJavaFunctionDD() {
        return "ORoutingTask_ownerJavaFunction";
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="ownerJFParameter">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC ownerJFParameter;

    public String getOwnerJFParameterDD() {
        return "ORoutingTask_ownerJFParameter";
    }
    
    public UDC getOwnerJFParameter() {
        return ownerJFParameter;
    }

    public void setOwnerJFParameter(UDC ownerJFParameter) {
        this.ownerJFParameter = ownerJFParameter;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="showComment">
    private boolean showComment;

    /**
     * @return the showComment
     */
    public boolean isShowComment() {
        return showComment;
    }

    /**
     * @param showComment the showComment to set
     */
    public void setShowComment(boolean showComment) {
        this.showComment = showComment;
    }
    /**
     * @return the showComment
     */
    public String getShowCommentDD() {
        return "ORoutingTask_showComment";
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="notifiable">
    private boolean notifiable;
   /**
     * @return the notifiable
     */
    public boolean isNotifiable() {
        return notifiable;
    }

    /**
     * @param notifiable the notifiable to set
     */
    public void setNotifiable(boolean notifiable) {
        this.notifiable = notifiable;
    }
    /**
     * @return the notifiable
     */
    public String getNotifiableDD() {
        return "ORoutingTask_notifiable";
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="alert">
//    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
//    private OAlert alert;
//
//    /**
//     * @return the alert
//     */
//    public OAlert getAlert() {
//        return alert;
//    }
//
//    /**
//     * @param alert the alert to set
//     */
//    public void setAlert(OAlert alert) {
//        this.alert = alert;
//    }
    /**
     * @return the alert
     */
    public String getAlertDD() {
        return "ORoutingTask_alert";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="taskScreen">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)    
    private SingleEntityScreen taskScreen;

    /**
     * @return the taskScreen
     */
    public SingleEntityScreen getTaskScreen() {
        return taskScreen;
    }

    /**
     * @param taskScreen the taskScreen to set
     */
    public void setTaskScreen(SingleEntityScreen taskScreen) {
        this.taskScreen = taskScreen;
    }
    /**
     * @return the taskScreen
     */
    public String getTaskScreenDD() {
        return "ORoutingTask_taskScreen";
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="routing">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)    
    private ORouting routing;

    /**
     * @return the routing
     */
    public ORouting getRouting() {
        return routing;
    }

    /**
     * @param routing the routing to set
     */
    public void setRouting(ORouting routing) {
        this.routing = routing;
    }
    /**
     * @return the routing
     */
    public String getRoutingDD() {
        return "ORoutingTask_routing";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="taskInstances">
    @OneToMany
    private List<ORoutingTaskInstance> taskInstances;

    /**
     * @return the taskInstances
     */
    public List<ORoutingTaskInstance> getTaskInstances() {
        return taskInstances;
    }
    /**
     * @param taskInstances the taskInstances to set
     */
    public void setTaskInstances(List<ORoutingTaskInstance> taskInstances) {
        this.taskInstances = taskInstances;
    }
    /**
     * @return the taskInstances
     */
    public String getTaskInstancesDD() {
        return "ORoutingTask_taskInstances";
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="taskActions">
    @OneToMany(mappedBy="routingTask")
    private List<ORoutingTaskAction> taskActions;
    /**
     * @return the taskActions
     */
    public List<ORoutingTaskAction> getTaskActions() {
        return taskActions;
    }
    /**
     * @param taskActions the taskActions to set
     */
    public void setTaskActions(List<ORoutingTaskAction> taskActions) {
        this.taskActions = taskActions;
    }
    /**
     * @return the taskActions
     */
    public String getTaskActionsDD() {
        return "ORoutingTask_taskActions";
    }
    // </editor-fold>
    
}
