/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.audittrail.main.valueholder;

import java.io.Serializable;
import java.util.Date;

import com.unitedofoq.fabs.core.audittrail.persistant.entity.TransactionType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author mustafa
 */
public class AuditTrailMessageObject implements Serializable {

    private BaseEntity oldObject;
    private BaseEntity newObject;
    private OEntityDTO oEntity;
    private OUser oUser;
    private long userTenantID;
    private long workingOnTenantID;
    private Date auditDate;
    private TransactionType transactionType;
    private String result;

    public OEntityDTO getoEntity() {
        return oEntity;
    }

    public void setoEntity(OEntityDTO oEntity) {
        this.oEntity = oEntity;
    }

    public BaseEntity getOldObject() {
        return oldObject;
    }

    public void setOldObject(BaseEntity oldObject) {
        this.oldObject = oldObject;
    }

    public BaseEntity getNewObject() {
        return newObject;
    }

    public void setNewObject(BaseEntity newObject) {
        this.newObject = newObject;
    }

    public Date getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(Date auditDate) {
        this.auditDate = auditDate;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public OUser getoUser() {
        return oUser;
    }

    public void setoUser(OUser oUser) {
        this.oUser = oUser;
    }

    public long getUserTenantID() {
        return userTenantID;
    }

    public void setUserTenantID(long userTenantID) {
        this.userTenantID = userTenantID;
    }

    public long getWorkingOnTenantID() {
        return workingOnTenantID;
    }

    public void setWorkingOnTenantID(long workingOnTenantID) {
        this.workingOnTenantID = workingOnTenantID;
    }
}
