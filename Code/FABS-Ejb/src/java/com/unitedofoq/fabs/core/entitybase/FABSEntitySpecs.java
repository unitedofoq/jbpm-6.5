/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.entitybase;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkEntityManager;

/**
 *
 * @author bassem
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface FABSEntitySpecs {
    // History Image
    /**
     * Optional
     * Entity in which the History Image will be saved
     */
    Class  historyImageEntity() default void.class;
    /**
     * Optional
     * <br>Field Name of the Original-Entity DBID
     * <br>Used in the History Image Entity
     */
    String historyImageOriginalEntityField() default "";

    /**
     * Fields names that will be hidden in the User Interface from the super class
     */
    String[] hideInheritedFields() default {};


    /**
     * List of fields that have custom cascade, where they have cascade
     * but not mandatory. Those fields are saved manually in the case of
     * Cascade (Parent)Merge-(Child)Persist 
     * <br>Normally, the field should have cascade={CascadeType.PERSIST, CascadeType.MERGE}
     * in its entity
     * <br>Refer to {@link UIFrameworkEntityManager#saveCustomCascadeEntities(BaseEntity, OUser)}
     */
    String[] customCascadeFields() default {};

    /**
     * Indicator Key Field Name, must be String field.
     */
    String indicatorFieldExp() default "";
    
    /**
     * List of fields that are available to be personalized in the screen
     */
    String[] personalizableFields() default {};
    
    /**
     * boolean value that indicates id the entity handled by audit train or not
     */
    boolean isAuditTrailed() default true;
}
