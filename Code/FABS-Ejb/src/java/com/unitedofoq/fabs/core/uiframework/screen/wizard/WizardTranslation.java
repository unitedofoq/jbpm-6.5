
package com.unitedofoq.fabs.core.uiframework.screen.wizard;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class WizardTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="wizardTitle">
    @Column
    private String wizardTitle;

    public void setWizardTitle(String wizardTitle) {
        this.wizardTitle = wizardTitle;
    }

    public String getWizardTitle() {
        return wizardTitle;
    }
    // </editor-fold>

}
