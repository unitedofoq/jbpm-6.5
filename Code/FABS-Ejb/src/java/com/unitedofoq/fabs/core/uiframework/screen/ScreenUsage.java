/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author nkhalil
 */
@Entity
@ParentEntity(fields = {"screen"})
public class ScreenUsage extends BaseEntity {
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OScreen screen;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OUser user;
    private int usageCounter;
    private boolean enforceShow;

    public String getEnforceShowDD() {
        return "ScreenUsage_enforceShow";
    }

    public boolean isEnforceShow() {
        return enforceShow;
    }

    public void setEnforceShow(boolean enforceShow) {
        this.enforceShow = enforceShow;
    }

    public String getScreenDD() {
        return "ScreenUsage_screen";
    }

    public OScreen getScreen() {
        return screen;
    }

    public void setScreen(OScreen screen) {
        this.screen = screen;
    }

    public String getUsageCounterDD() {
        return "ScreenUsage_usageCounter";
    }

    public int getUsageCounter() {
        return usageCounter;
    }

    public void setUsageCounter(int usageCounter) {
        this.usageCounter = usageCounter;
    }

    public String getUserDD() {
        return "ScreenUsage_user";
    }

    public OUser getUser() {
        return user;
    }

    public void setUser(OUser user) {
        this.user = user;
    }
}
