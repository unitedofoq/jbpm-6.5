/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.process;

import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author ianwar
 */
@Entity
public class ProcessButton extends ObjectBaseEntity {

    @Column(nullable = false, name = "buttonName")
    private String buttonName;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false, name = "oprocess_dbid")
    private OProcess oprocess;

    @OneToMany(mappedBy = "processButton")
    private List<ProcessButtonJavaFunction> processButtonJavaFunction;

    public String getButtonName() {
        return buttonName;
    }

    public void setButtonName(String buttonName) {
        this.buttonName = buttonName;
    }

    public String getButtonNameDD() {
        return "ButtonEntity_buttonName";
    }

    public OProcess getOprocess() {
        return oprocess;
    }

    public String getOprocessDD() {
        return "ProcessButton_oprocess";
    }

    public void setOprocess(OProcess oprocess) {
        this.oprocess = oprocess;
    }

    public List<ProcessButtonJavaFunction> getProcessButtonJavaFunction() {
        return processButtonJavaFunction;
    }

    public void setProcessButtonJavaFunction(List<ProcessButtonJavaFunction> processButtonJavaFunction) {
        this.processButtonJavaFunction = processButtonJavaFunction;
    }

}
