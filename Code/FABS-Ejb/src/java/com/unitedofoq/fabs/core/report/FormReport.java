package com.unitedofoq.fabs.core.report;

import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue(value = "FORM")
@VersionControlSpecs(omoduleFieldExpression = "omodule")
public class FormReport extends OReport {

}
