/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.timezone;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.JavaFunction;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.setup.FABSSetupLite;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author lap3
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.timezone.TimeZoneServiceLocal",
beanInterface = TimeZoneServiceLocal.class)
public class TimeZoneService implements TimeZoneServiceLocal {
        final static Logger logger = LoggerFactory.getLogger(TimeZoneService.class);
    @EJB
    OEntityManagerRemote oem;
    @EJB
    protected OFunctionServiceRemote functionServiceRemote;

    @Override
    public Date getUserCDT(OUser loggedUser) {
        Date returnedDate = null;
        try {
            List<FABSSetupLite> fabsSetup = new ArrayList<FABSSetupLite>();
            fabsSetup = oem.createListNamedQuery("getFABSSetup", loggedUser,
                    "key", "timeZoneFn");
            if (fabsSetup!= null && !fabsSetup.isEmpty() && fabsSetup.get(0) != null && fabsSetup.get(0).getSvalue() != null
                    && !fabsSetup.get(0).getSvalue().equals("")) {
                JavaFunction javaFn = (JavaFunction) oem.loadEntity(JavaFunction.class.getSimpleName(),
                        Collections.singletonList("dbid = " + Long.valueOf(fabsSetup.get(0).getSvalue())), null, loggedUser);

                ODataMessage odm = new ODataMessage();
                List data = new ArrayList();
                data.add(loggedUser);
                odm.setData(data);

                OFunctionResult retOFR = functionServiceRemote.runFunction(javaFn, odm, new OFunctionParms(), loggedUser);
                if (retOFR.getReturnValues() != null && !retOFR.getReturnValues().isEmpty()) {
                    returnedDate = getDateForTimeZone(String.valueOf(retOFR.getReturnValues().get(0)));
                } else {
                    logger.error("Java Function Does not returned Time Zone");
                }
            } else {
                if (loggedUser.getTimeZone() != null && loggedUser.getTimeZone().getDbid() != 0) {
                    returnedDate = getDateForTimeZone(loggedUser.getTimeZone().getValue());
                } else {
                    returnedDate = getSystemCDT();
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        } finally {
            logger.debug("Returning");
            return returnedDate;
        }
    }

    @Override
    public String getSystemCDTAsString() {
        logger.debug("Entering");
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");//dd/MM/yyyy
        String currDate = sdfDate.format(new Date());
        logger.debug("Returning");
        return currDate;    
    }

    @Override
    public Date getSystemCDT() {
        logger.debug("Entering");
        Calendar cal = Calendar.getInstance();
        logger.debug("Returning");
        return cal.getTime();
    }

    private Date getDateForTimeZone(String timeZone) {
        try {
            logger.debug("Entering");
            SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            dateFormatGmt.setTimeZone(TimeZone.getTimeZone(timeZone));
            String timeZoneDate = dateFormatGmt.format(new Date());
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = (Date) formatter.parse(timeZoneDate);
            logger.debug("Returning");
            return date;
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            logger.debug("Returning with Null");
            return null;
        }
    }
    
    @Override
    public OFunctionResult testTimeZOne(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            List returnedData = new ArrayList();
            returnedData.add("America/Antigua");
            oFR.setReturnValues(returnedData);
        } catch (Exception ex) {
            logger.error("exception thrown: ",ex);
            logger.debug("Returning with Null");
            return null;
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }
}