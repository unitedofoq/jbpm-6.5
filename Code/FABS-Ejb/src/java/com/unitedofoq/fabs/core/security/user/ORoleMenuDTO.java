/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.security.user;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Melad
 */
public class ORoleMenuDTO {

    private long dbid;
    private long oRoleDBID;
    private long oModuleDbid;
    private String oRoleName;
    private String oRoleDescription;
    private boolean oRoleReserved;
    private boolean includeSubs;
    private long oMenuDBID;
    private List<ORoleUserDTO> roleUser;

    public ORoleMenuDTO() {
    }

    public ORoleMenuDTO(long dbid, 
            long oRoleDBID, 
            long oModuleDbid, 
            String oRoleName, 
            String oRoleDescription, 
            boolean oRoleReserved, 
            boolean includeSubs, 
            long oMenuDBID) {
        this.dbid = dbid;
        this.oRoleDBID = oRoleDBID;
        this.oModuleDbid = oModuleDbid;
        this.oRoleName = oRoleName;
        this.oRoleDescription = oRoleDescription;
        this.oRoleReserved = oRoleReserved;
        this.includeSubs = includeSubs;
        this.oMenuDBID = oMenuDBID;
        roleUser=new ArrayList<ORoleUserDTO>();
    }

    public long getDbid() {
        return dbid;
    }

    public void setDbid(long dbid) {
        this.dbid = dbid;
    }

    public long getoRoleDBID() {
        return oRoleDBID;
    }

    public void setoRoleDBID(long oRoleDBID) {
        this.oRoleDBID = oRoleDBID;
    }

    public long getoModuleDbid() {
        return oModuleDbid;
    }

    public void setoModuleDbid(long oModuleDbid) {
        this.oModuleDbid = oModuleDbid;
    }

    public String getoRoleName() {
        return oRoleName;
    }

    public void setoRoleName(String oRoleName) {
        this.oRoleName = oRoleName;
    }

    public String getoRoleDescription() {
        return oRoleDescription;
    }

    public void setoRoleDescription(String oRoleDescription) {
        this.oRoleDescription = oRoleDescription;
    }

    public boolean isoRoleReserved() {
        return oRoleReserved;
    }

    public void setoRoleReserved(boolean oRoleReserved) {
        this.oRoleReserved = oRoleReserved;
    }

    public boolean isIncludeSubs() {
        return includeSubs;
    }

    public void setIncludeSubs(boolean includeSubs) {
        this.includeSubs = includeSubs;
    }

    public long getoMenuDBID() {
        return oMenuDBID;
    }

    public void setoMenuDBID(long oMenuDBID) {
        this.oMenuDBID = oMenuDBID;
    }

    public List<ORoleUserDTO> getRoleUser() {
        return roleUser;
    }

    public void setRoleUser(List<ORoleUserDTO> roleUser) {
        this.roleUser = roleUser;
    }
    
}
