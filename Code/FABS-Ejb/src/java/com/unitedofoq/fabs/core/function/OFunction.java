package com.unitedofoq.fabs.core.function;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.multimedia.OImage;
import com.unitedofoq.fabs.core.uiframework.navigationmenu.NMEntityListener;

/**
 *
 * @author arezk
 */
@Entity
@EntityListeners(value={NMEntityListener.class})
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name="FTYPE", discriminatorType=DiscriminatorType.STRING)
@FABSEntitySpecs(indicatorFieldExp="name")
@ChildEntity(fields={"menuFunction", "functionGroupFunctions"})
public class OFunction extends ObjectBaseEntity 
{
    // <editor-fold defaultstate="collapsed" desc="functionImage">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name="functionImage_dbid")
    private OImage functionImage;

    public String getFunctionImageDD(){
        return  "OFunction_functionImage";
    }

    public OImage getFunctionImage() {
        return functionImage;
    }

    public void setFunctionImage(OImage functionImage) {
        this.functionImage = functionImage;
    }
    // </editor-fold> 
    
    // <editor-fold defaultstate="collapsed" desc="name, nameTranslated">
    @Transient
    @Translation(originalField="name")
    private String nameTranslated;
    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    @Translatable(translationField="nameTranslated")
    private String name;
    public String getNameTranslatedDD() {
        return "OFunction_name";
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="description, descriptionTranslated">
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    @Translatable(translationField="descriptionTranslated")
    private String description;
    public String getDescriptionTranslatedDD() {
        return "OFunction_description";
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="serverJob">
    private boolean serverJob;

    public boolean isServerJob() {
        return serverJob;
    }

    public void setServerJob(boolean serverJob) {
        this.serverJob = serverJob;
    }

    public String getServerJobDD(){
        return "OFunction_serverJob";
    }
    // </editor-fold> 

    @Column(unique=true, nullable=false)
    private String code;
    public String getCodeDD() {
        return "OFunction_code";
    }

    @Column(nullable=false)
    private boolean reserved = false;

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }
    
    public String getReservedDD() {
        return "OFunction_reserved";
    }

    @OneToMany(mappedBy="ofunction")
    List<OMenuFunction> menuFunction;

    public List<OMenuFunction> getMenuFunction() {
        return menuFunction;
    }

    public void setMenuFunction(List<OMenuFunction> menuFunction) {
        this.menuFunction = menuFunction;
    }

    /* DD NAMES GETTERS */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @OneToMany(mappedBy="ofunction")
    private List<FunctionGroupFunctions> functionGroupFunctions;

    public List<FunctionGroupFunctions> getFunctionGroupFunctions() {
        return functionGroupFunctions;
    }

    public void setFunctionGroupFunctions(List<FunctionGroupFunctions> functionGroupFunctions) {
        this.functionGroupFunctions = functionGroupFunctions;
    }
    
    public String getFunctionGroupFunctionsDD() {
        return "OFunction_functionGroupFunctions";
    }    
}
