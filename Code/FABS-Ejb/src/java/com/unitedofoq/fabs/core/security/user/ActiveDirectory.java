package com.unitedofoq.fabs.core.security.user;

import com.unitedofoq.fabs.core.usermessage.UserMessage;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;


import javax.naming.Binding;
import javax.naming.Context;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import static javax.naming.directory.SearchControls.SUBTREE_SCOPE;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.InitialLdapContext;

//Imports for changing password
import javax.naming.directory.ModificationItem;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.ldap.StartTlsResponse;
import javax.naming.ldap.StartTlsRequest;
import javax.net.ssl.*;
import org.slf4j.LoggerFactory;

//******************************************************************************
//**  ActiveDirectory
//*****************************************************************************/
/**
 * Provides static methods to authenticate users, change passwords, etc.
 *
 *****************************************************************************
 */
public class ActiveDirectory {
final static org.slf4j.Logger logger = LoggerFactory.getLogger(ActiveDirectory.class);
    private static String[] userAttributes = {
        "distinguishedName",
        "cn",
        "name",
        "uid",
        "sn",
        "givenname", "memberOf", "samaccountname",
        "userPrincipalName", "mail"
    };
    private static String[] groupAttributes = {
        "name", "cn"
    };

    private ActiveDirectory() {
    }

    //**************************************************************************
    //** getConnection
    //*************************************************************************/
    /**
     * Used to authenticate a user given a username/password. Domain name is
     * derived from the fully qualified domain name of the host machine.
     */
    public static LdapContext getConnection(String username, String password) throws NamingException {
        return getConnection(username, password, null, null);
    }

    //**************************************************************************
    //** getConnection
    //*************************************************************************/
    /**
     * Used to authenticate a user given a username/password and domain name.
     */
    public static LdapContext getConnection(String username, String password, String domainName) throws NamingException {
        return getConnection(username, password, domainName, null);
    }

    //**************************************************************************
    //** getConnection
    //*************************************************************************/
    /**
     * Used to authenticate a user given a username/password and domain name.
     * Provides an option to identify a specific a Active Directory server.
     */
    public static LdapContext getConnection(String username, String password, String domainName, String serverName) throws NamingException {
logger.debug("Entering");
        if (domainName == null) {
            try {
                logger.debug("Domain Number is Null, I will get it form host Name");
                String fqdn = java.net.InetAddress.getLocalHost().getCanonicalHostName();
                if (fqdn.split("\\.").length > 1) {
                    domainName = fqdn.substring(fqdn.indexOf(".") + 1);
                }
            } catch (java.net.UnknownHostException ex) {
                logger.error("Exception thrown: ",ex);
            }
        }

        
        if (password != null) {
            password = password.trim();
            if (password.length() == 0) {
                password = null;
            }
        }

        //bind by using the specified username/password
        Hashtable props = new Hashtable();
        String principalName = username + "@" + domainName;
        props.put(Context.SECURITY_PRINCIPAL, principalName);
        if (password != null) {
            props.put(Context.SECURITY_CREDENTIALS, password);
        }


        String ldapURL = "ldap://" + ((serverName == null) ? domainName : serverName + "." + domainName) + '/';
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        ldapURL = "ldap://" + serverName + "/";
        props.put(Context.PROVIDER_URL, ldapURL);
        try {
            logger.debug("Returning");
            return new InitialLdapContext(props, null);
        } catch (javax.naming.CommunicationException ex) {
            logger.error("Exception thrown: ",ex);
            logger.trace("Failed to connect to {} {}" , domainName , ((serverName == null) ? "" : " through " + serverName));
            throw new NamingException("Failed to connect to " + domainName + ((serverName == null) ? "" : " through " + serverName));
        } catch (NamingException ex) {
            logger.error("Exception thrown: ",ex);
            logger.trace("Failed to authenticate {} @ {} {}" , username , domainName , ((serverName == null) ? "" : " through " + serverName));
            throw new NamingException("Failed to authenticate " + username + "@" + domainName + ((serverName == null) ? "" : " through " + serverName));
        }
        
    }

    //**************************************************************************
    //** getUser
    //*************************************************************************/
    /**
     * Used to check whether a username is valid.
     *
     * @param username A username to validate (e.g. "peter", "peter@acme.com",
     * or "ACME\peter").
     */
    public static User getUser(String username, LdapContext context) {
        logger.debug("Entering");
        try {
            String domainName = null;
            if (username.contains("@")) {
                username = username.substring(0, username.indexOf("@"));
                domainName = username.substring(username.indexOf("@") + 1);
            } else if (username.contains("\\")) {
                username = username.substring(0, username.indexOf("\\"));
                domainName = username.substring(username.indexOf("\\") + 1);
            } else {
                String authenticatedUser = (String) context.getEnvironment().get(Context.SECURITY_PRINCIPAL);
                if (authenticatedUser.contains("@")) {
                    domainName = authenticatedUser.substring(authenticatedUser.indexOf("@") + 1);
                }
            }

            if (domainName != null) {
                String principalName = username + "@" + domainName;
                SearchControls controls = new SearchControls();
                controls.setSearchScope(SUBTREE_SCOPE);
                controls.setReturningAttributes(userAttributes);
                NamingEnumeration<SearchResult> answer = context.search(toDC(domainName), "(& (userPrincipalName=" + principalName + ")(objectClass=user))", controls);
                if (answer.hasMore()) {
                    Attributes attr = answer.next().getAttributes();
                    NamingEnumeration<String> ids = attr.getIDs();
                    User user = null;
                    if (attr.get("name") != null) {
                        user = new User(attr);
                    }
                    if (user != null) {
                        logger.debug("Returning with New User");
                        return new User(attr);
                    }
                }
            }
        } catch (NamingException ex) {
            logger.error("Exception thrown: ",ex);
        }
        logger.debug("Returning with Null");
        return null;
    }


    public static List<User> listAllUsers(String searchBase, LdapContext authContext) {
        logger.debug("Entering");
        List<User> foundUsers = new ArrayList<User>();
        try {
            SearchControls constraints = new SearchControls();
            String filter = "(objectClass=user)"; // You might want to limit search to user objects only based on objectClass
            constraints.setReturningAttributes(userAttributes);
            constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
            NamingEnumeration<SearchResult> results = authContext.search(searchBase, filter, constraints);
            while (results.hasMore()) {
                Attributes attr = ((SearchResult) results.next()).getAttributes();
                Attribute user = attr.get("userPrincipalName");
                if (user != null) {
                    foundUsers.add(new User(attr));
                }
            }

        } catch (NamingException ex) {
            logger.error("exception thrown: ",ex);
        }
        logger.debug("Returning");
        return foundUsers;
    }

    //**************************************************************************
    //** getUsers
    //*************************************************************************/
    /**
     * Returns a list of users in the domain.
     */

    private static String toDC(String domainName) {
        logger.trace("Entering");
        StringBuilder buf = new StringBuilder();
        for (String token : domainName.split("\\.")) {
            if (token.length() == 0) {
                continue;   // defensive check
            }
            if (buf.length() > 0) {
                buf.append(",");
            }
            buf.append("DC=").append(token);
        }
        logger.trace("Returning");
        return buf.toString();
    }

    //**************************************************************************
    //** User Class
    //*************************************************************************/
    /**
     * Used to represent a User in Active Directory
     */
    public static class User {
final static org.slf4j.Logger logger = LoggerFactory.getLogger(User.class);
        private String distinguishedName;
        private String commonName;
        private String givenName;
        private String memberof;
        private String mail;
        private String samaccountname;

        public User(Attributes attr) throws javax.naming.NamingException {
            commonName = attr.get("cn") != null ? (String) attr.get("cn").get() : null;
            givenName = attr.get("name") != null ? (String) attr.get("name").get() : null;
            distinguishedName = attr.get("distinguishedName") != null ? (String) attr.get("distinguishedName").get() : null;
            mail = attr.get("mail") != null ? (String) attr.get("mail").get() : null;
            samaccountname = attr.get("samaccountname") != null ? (String) attr.get("samaccountname").get() : givenName;

        }

        public String getCommonName() {
            return commonName;
        }

        public String getDistinguishedName() {
            return distinguishedName;
        }

        public String toString() {
            return getDistinguishedName();
        }

        /**
         * Used to change the user password. Throws an IOException if the Domain
         * Controller is not LDAPS enabled.
         *
         * @param trustAllCerts If true, bypasses all certificate and host name
         * validation. If false, ensure that the LDAPS certificate has been
         * imported into a trust store and sourced before calling this method.
         * Example: String keystore =
         * "/usr/java/jdk1.5.0_01/jre/lib/security/cacerts";
         * System.setProperty("javax.net.ssl.trustStore",keystore);
         */
        public void changePassword(String oldPass, String newPass, boolean trustAllCerts, LdapContext context)
                throws java.io.IOException, NamingException {
            logger.debug("Entering");
            String dn = getDistinguishedName();
            //Switch to SSL/TLS
            StartTlsResponse tls = null;
            try {
                logger.trace("Switching to SSL/TLS");
                tls = (StartTlsResponse) context.extendedOperation(new StartTlsRequest());
            } catch (Exception ex) {
                //"Problem creating object: javax.naming.ServiceUnavailableException: [LDAP: error code 52 - 00000000: LdapErr: DSID-0C090E09, comment: Error initializing SSL/TLS, data 0, v1db0"
                logger.error("exception thrown: ",ex);
                logger.debug("Failed to establish SSL connection to the Domain Controller. Is LDAPS enabled?");
                throw new java.io.IOException("Failed to establish SSL connection to the Domain Controller. Is LDAPS enabled?");
            }
            //Exchange certificates
            if (trustAllCerts) {
                tls.setHostnameVerifier(DO_NOT_VERIFY);
                SSLSocketFactory sf = null;
                try {
                    SSLContext sc = SSLContext.getInstance("TLS");
                    sc.init(null, TRUST_ALL_CERTS, null);
                    sf = sc.getSocketFactory();
                } catch (java.security.NoSuchAlgorithmException ex) {
                    logger.error("exception thrown: ",ex);
                } catch (java.security.KeyManagementException ex) {
                    logger.error("exception thrown: ",ex);
                }
                tls.negotiate(sf);
            } else {
                tls.negotiate();
            }


            //Change password
            try {
                ModificationItem[] modificationItems = new ModificationItem[2];
                modificationItems[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("unicodePwd", getPassword(oldPass)));
                modificationItems[1] = new ModificationItem(DirContext.ADD_ATTRIBUTE, new BasicAttribute("unicodePwd", getPassword(newPass)));
                context.modifyAttributes(dn, modificationItems);
            } catch (javax.naming.directory.InvalidAttributeValueException ex) {
                String error = ex.getMessage().trim();
                if (error.startsWith("[") && error.endsWith("]")) {
                    error = error.substring(1, error.length() - 1);
                }
                System.err.println(error);
                logger.error("exception thrown: ",ex);
                tls.close();
                throw new NamingException(
                        "New password does not meet Active Directory requirements. "
                        + "Please ensure that the new password meets password complexity, "
                        + "length, minimum password age, and password history requirements.");
            } catch (NamingException ex) {
                logger.error("exception thrown: ",ex);
                tls.close();
                throw ex;
            }

            //Close the TLS/SSL session
            logger.trace("Close the TLS/SSL session");
            tls.close();
            logger.debug("Returning");
        }
        private static final HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
        private static TrustManager[] TRUST_ALL_CERTS = new TrustManager[]{
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
            }
        };

        private byte[] getPassword(String newPass) {
            String quotedPassword = "\"" + newPass + "\"";
            //return quotedPassword.getBytes("UTF-16LE");
            char unicodePwd[] = quotedPassword.toCharArray();
            byte pwdArray[] = new byte[unicodePwd.length * 2];
            for (int i = 0; i < unicodePwd.length; i++) {
                pwdArray[i * 2 + 1] = (byte) (unicodePwd[i] >>> 8);
                pwdArray[i * 2 + 0] = (byte) (unicodePwd[i] & 0xff);
            }
            return pwdArray;
        }

        /**
         * @return the givenName
         */
        public String getGivenName() {
            return givenName;
        }

        /**
         * @param givenName the givenName to set
         */
        public void setGivenName(String givenName) {
            this.givenName = givenName;
        }

        /**
         * @param distinguishedName the distinguishedName to set
         */
        public void setDistinguishedName(String distinguishedName) {
            this.distinguishedName = distinguishedName;
        }

        /**
         * @param commonName the commonName to set
         */
        public void setCommonName(String commonName) {
            this.commonName = commonName;
        }

        /**
         * @return the memberof
         */
        public String getMemberof() {
            return memberof;
        }

        /**
         * @param memberof the memberof to set
         */
        public void setMemberof(String memberof) {
            this.memberof = memberof;
        }

        /**
         * @return the mail
         */
        public String getMail() {
            return mail;
        }

        /**
         * @param mail the mail to set
         */
        public void setMail(String mail) {
            this.mail = mail;
        }

        /**
         * @return the samaccountname
         */
        public String getSamaccountname() {
            return samaccountname;
        }

        /**
         * @param samaccountname the samaccountname to set
         */
        public void setSamaccountname(String samaccountname) {
            this.samaccountname = samaccountname;
        }
    }
}