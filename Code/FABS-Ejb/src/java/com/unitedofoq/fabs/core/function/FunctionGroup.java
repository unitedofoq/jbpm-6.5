/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.function;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;

/**
 *
 * @author arezk
 */
@Entity
@ChildEntity(fields={"functionGroupFunctions", "functionGroupScreenGroups"})
public class FunctionGroup extends ObjectBaseEntity{
    @Translatable(translationField="nameTranslated")
    @Column(unique=true)
    private String name;

    @Transient
    @Translation(originalField="name")
    private String nameTranslated;

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getNameTranslatedDD(){
        return "FunctionGroup_name";
    }
    
    @OneToMany(mappedBy="functionGroup")
    private List<FunctionGroupFunctions> functionGroupFunctions;

    public List<FunctionGroupFunctions> getFunctionGroupFunctions() {
        return functionGroupFunctions;
    }

    public void setFunctionGroupFunctions(List<FunctionGroupFunctions> functionGroupFunctions) {
        this.functionGroupFunctions = functionGroupFunctions;
    }
    
    public String getFunctionGroupFunctionsDD() {
        return "FunctionGroup_functionGroupFunctions";
    }
    
    @OneToMany(mappedBy="functionGroup")
    private List<FunctionGroupScreenGroup> functionGroupScreenGroups;

    public List<FunctionGroupScreenGroup> getFunctionGroupScreenGroups() {
        return functionGroupScreenGroups;
    }

    public String getFunctionGroupScreenGroupsDD() {
        return "FunctionGroup_functionGroupScreenGroups";
    }

    public void setFunctionGroupScreenGroups(List<FunctionGroupScreenGroup> functionGroupScreenGroups) {
        this.functionGroupScreenGroups = functionGroupScreenGroups;
    }
}
