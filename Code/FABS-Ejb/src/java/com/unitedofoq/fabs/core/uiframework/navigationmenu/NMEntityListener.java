/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.navigationmenu;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import com.unitedofoq.fabs.core.function.OMenu;
import com.unitedofoq.fabs.core.function.OMenuFunction;
import com.unitedofoq.fabs.core.security.user.ORole;
import com.unitedofoq.fabs.core.security.user.RoleMenu;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;


public class NMEntityListener {

    @PostRemove
    public void onRemove(Object obj) {
        if (obj instanceof RoleMenu) {
            NavMenuManagerImpl.getSingleInstance().onRemove(obj);
        }else if(obj instanceof OMenuFunction){
            NavMenuManagerImpl.getSingleInstance().onRemove(obj);
        }        
    }

    @PostUpdate
    public void onUpdate(Object obj) {
        if (obj instanceof BaseEntityTranslation) {
            NavMenuManagerImpl.getSingleInstance().onUpdateTranslation(obj);
        }else{
            NavMenuManagerImpl.getSingleInstance().onUpdate(obj);
        }
    }

    @PostPersist
    public void onAdd(Object obj) {
        if (obj instanceof RoleMenu) {
            NavMenuManagerImpl.getSingleInstance().onAdd(obj);
        }else if(obj instanceof OMenuFunction){
            NavMenuManagerImpl.getSingleInstance().onAdd(obj);
        }else if(obj instanceof OMenu){
            NavMenuManagerImpl.getSingleInstance().onAdd(obj);
        }else if(obj instanceof ORole){
            NavMenuManagerImpl.getSingleInstance().onAdd(obj);
        }
        
    }
}