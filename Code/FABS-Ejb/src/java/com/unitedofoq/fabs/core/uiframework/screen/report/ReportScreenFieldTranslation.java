/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen.report;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

/**
 *
 * @author mostafa
 */
@Entity
public class ReportScreenFieldTranslation extends BaseEntityTranslation{
    
    // <editor-fold defaultstate="collapsed" desc="ddTitleOverride">
    @Column
    private String ddTitleOverride;

    public void setDdTitleOverride(String ddTitleOverride) {
        this.ddTitleOverride = ddTitleOverride;
    }

    public String getDdTitleOverride() {
        return ddTitleOverride;
    }
    // </editor-fold>

}
