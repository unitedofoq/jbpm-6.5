/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.routing;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;

/**
 *
 * @author mibrahim
 */
@Entity
@ChildEntity(fields={"flowSteps"})
public class FABSRoutingFlow extends BaseEntity {
    //<editor-fold desc="routingFlowName" defaultstate="collapsed">
    @Translatable(translationField= "routingFlowNameTranslated")
    private String routingFlowName;
    @Translation(originalField="routingFlowName")
    @Transient
    private String routingFlowNameTranslated;
    public String getRoutingFlowName() {
        return routingFlowName;
    }

    public void setRoutingFlowName(String routingFlowName) {
        this.routingFlowName = routingFlowName;
    }

    public String getRoutingFlowNameTranslatedDD() {
        return "FABSRoutingFlow_routingFlowName";
    }
    public String getRoutingFlowNameTranslated() {
        return routingFlowNameTranslated;
    }

    public void setRoutingFlowNameTranslated(String routingFlowNameTranslated) {
        this.routingFlowNameTranslated = routingFlowNameTranslated;
    } 
    //</editor-fold>
    //<editor-fold desc="routingFlowDescirption" defaultstate="collapsed">
    @Translatable(translationField= "routingFlowDescirptionTranslated")
    private String routingFlowDescirption;
    @Transient
    @Translation(originalField="routingFlowDescirption")
    private String routingFlowDescirptionTranslated;
    
    public String getRoutingFlowDescirptionTranslatedDD() {
        return "FABSRoutingFlow_routingFlowDescirption";
    }
    public String getRoutingFlowDescirption() {
        return routingFlowDescirption;
    }

    public void setRoutingFlowDescirption(String routingFlowDescirption) {
        this.routingFlowDescirption = routingFlowDescirption;
    }

    public String getRoutingFlowDescirptionTranslated() {
        return routingFlowDescirptionTranslated;
    }

    public void setRoutingFlowDescirptionTranslated(String routingFlowDescirptionTranslated) {
        this.routingFlowDescirptionTranslated = routingFlowDescirptionTranslated;
    }
    
    //</editor-fold>
    //<editor-fold desc="routingFlowOEntity" defaultstate="collapsed">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OEntity routingFlowOEntity;

    public String getRoutingFlowOEntityDD() {
        return "FABSRoutingFlow_routingFlowOEntity";
    }

    public OEntity getRoutingFlowOEntity() {
        return routingFlowOEntity;
    }

    public void setRoutingFlowOEntity(OEntity routingFlowOEntity) {
        this.routingFlowOEntity = routingFlowOEntity;
    }
    //</editor-fold>
    //<editor-fold desc="routingEntity" defaultstate="collapsed">
    @Transient
    private BaseEntity routingEntity;
    public String getRoutingEntityDD() {
        return "FABSRoutingFlow_routingEntity";
    }

    public BaseEntity getRoutingEntity() {
        return routingEntity;
    }

    public void setRoutingEntity(BaseEntity routingEntity) {
        this.routingEntity = routingEntity;
    }
    //</editor-fold>
    //<editor-fold desc="currentFlowStep" defaultstate="collapsed">
   @Transient
    private FABSRoutingFlowStep currentFlowStep;
    public String getCurrentFlowStepDD() {
        return "FABSRoutingFlow_currentFlowStep";
    }

    public FABSRoutingFlowStep getCurrentFlowStep() {
        return currentFlowStep;
    }

    public void setCurrentFlowStep(FABSRoutingFlowStep currentFlowStep) {
        this.currentFlowStep = currentFlowStep;
    }
    //</editor-fold>
    //<editor-fold desc="flowSteps" defaultstate="collapsed">
    @OneToMany(mappedBy = "routingFlow")
    private List<FABSRoutingFlowStep> flowSteps;
    public List<FABSRoutingFlowStep> getFlowSteps() {
        return flowSteps;
    }

    public void setFlowSteps(List<FABSRoutingFlowStep> flowSteps) {
        this.flowSteps = flowSteps;
    }
    //</editor-fold>
}
