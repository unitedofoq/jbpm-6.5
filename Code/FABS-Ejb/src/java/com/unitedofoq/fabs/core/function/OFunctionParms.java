/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.function;

import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author melsayed
 */
public class OFunctionParms implements Serializable{
    HashMap params = new HashMap();

    public HashMap getParams() {
        if (params == null)
            return new HashMap();
        return params;
    }

    public void setParams(HashMap paramets) {
        this.params = paramets;
    }

    public String constructLogMessage() {
        if ( params != null ) {
              return params.toString();
        }
        else
            return "null" ;
    }
    @Override
    public String toString() {
        return constructLogMessage() ;
    }
}
