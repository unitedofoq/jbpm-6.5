/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.function;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.uiframework.screen.SingleEntityScreen;

/**
 *
 * @author arezk
 */
@Entity
@DiscriminatorValue(value="FunctionGroup")
@ParentEntity(fields="functionGroupScreenGroups")
@ChildEntity(fields={"screenInputs", "screenOutputs","screenFilter","screenFunctions"})
@VersionControlSpecs(omoduleFieldExpression = "omodule")
public class FunctionGroupScreen extends SingleEntityScreen {
    
    @OneToMany(mappedBy="functionGroupScreen")
    private List<FunctionGroupScreenGroup> functionGroupScreenGroups;

    public List<FunctionGroupScreenGroup> getFunctionGroupScreenGroups() {
        return functionGroupScreenGroups;
    }

    public void setFunctionGroupScreenGroups(List<FunctionGroupScreenGroup> functionGroupScreenGroups) {
        this.functionGroupScreenGroups = functionGroupScreenGroups;
    }
    
    public String getFunctionGroupScreenGroupsDD() {
        return "FunctionGroupScreen_functionGroupScreenGroups";
    }
    
    @Override
    public void PrePersist() {
        super.PrePersist();
        setViewPage("FunctionGroupScreen.iface");
    }
    
    @Override
    public String getHeaderTranslatedDD(){
        return "FunctionGroupScreen_header";
    }
}
