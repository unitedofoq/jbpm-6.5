package com.unitedofoq.fabs.core.comunication;

import com.unitedofoq.fabs.core.content.AttachmentDTO;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Hammad
 */
@MessageDriven(mappedName = "java:/jms/queue/NotificationQueue", activationConfig = {
    @ActivationConfigProperty(propertyName = "destination", propertyValue = "jms/queue/NotificationQueue"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
    @ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "JMSType='email'"),
    @ActivationConfigProperty(propertyName = "reconnectAttempts", propertyValue = "-1"),
    @ActivationConfigProperty(propertyName = "setupAttempts", propertyValue = "-1")
})
public class EmailNotificationMDB implements MessageListener {

    final static org.slf4j.Logger logger = LoggerFactory.getLogger(EmailNotificationMDB.class);

    @Resource
    private MessageDrivenContext context;
    @Resource(mappedName = "java:/jms/queue/PoisonMessagesQueue")
    private Queue poisonMessagesQueue;
    @Resource(mappedName = "java:/jms/PoisonMessagesConnectionFactory")
    private ConnectionFactory poisonMessagesQueueFactory;
    @EJB
    private EmailConnectionManagerLocal connectionManager;

    public EmailNotificationMDB() {
    }

    @Override
    public void onMessage(Message message) {
        logger.debug("Entering");
        EMailMessage eMailMessage;
        String messageBody = "";

        // Parse JMS message
        try {
            messageBody = message.getBody(String.class);
            logger.debug("Received message with messageBody: {}", messageBody);
            eMailMessage = EMailMessage.fromString(messageBody);
        } catch (JMSException ex) {
            logger.error("JMSException ! Discarding message", ex);
            return;
        } catch (IOException io) {
            logger.warn("Failed to parse message, moving it to poison messages queue... Message body: {}", messageBody);
            sendPoisonMessageToPoisonMessagesQueue(messageBody);
            return;
        }

        // Send email        
        try {
            sendEmail(eMailMessage.getFrom(), eMailMessage.getTos(), eMailMessage.getCcs(), eMailMessage.getSubject(), eMailMessage.getBody(), eMailMessage.getAttachments());
        } catch (javax.mail.AuthenticationFailedException authenticationFailedException) {
            logger.error("Authentication with mail server failed, will return message to queue", authenticationFailedException);
            context.setRollbackOnly();
        } catch (javax.mail.internet.AddressException addressException) {
            logger.warn("Invalid email address TO: {}, CC: {}... Moving message to poison messages queue !", eMailMessage.getTos().toString(), eMailMessage.getCcs().toString());
            logger.error("Exception thrown", addressException);
            sendPoisonMessageToPoisonMessagesQueue(messageBody);
        } catch (javax.mail.MessagingException messagingException){
            logger.error("couldn't connect to SMTP host, will return message to queue", messagingException);
            context.setRollbackOnly();
        } catch (Exception ex) {
            logger.warn("Couldnt send email message for unknown reason... Moving message to poison messages queue ! Message body: {}", messageBody);
            logger.error("Exception thrown", ex);
            sendPoisonMessageToPoisonMessagesQueue(messageBody);
        }
        logger.debug("Returning");
    }

    private void sendPoisonMessageToPoisonMessagesQueue(String message) {
        try {
            JMSSender.sendJMSMessageToQueue(message, poisonMessagesQueue, poisonMessagesQueueFactory, "email");
        } catch (JMSException jMSException) {
            logger.error("Failed to send message follow message to posion message queue: {}", message);
            logger.error("Message will be discarded forever due to JMSException", jMSException);
        }
    }

    private void sendEmail(String from, List<String> tos, List<String> ccs,
            String subject, String body,
            List<AttachmentDTO> attachments) throws Exception {
        logger.trace("Entering");
        MimeMessage message = new MimeMessage(EmailConnectionManager.getMailSession());
        message.setFrom(new InternetAddress(from));
        message.setSubject(subject, "UTF-8");
        for (String to : tos) {
            message.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(to));
        }
        for (String cc : ccs) {
            message.addRecipient(javax.mail.Message.RecipientType.CC, new InternetAddress(cc));
        }
        // create the Multipart and add its parts to it
        Multipart mp = createMultiPart(body);

        if (attachments != null) {
            for (AttachmentDTO attachment : attachments) {
                if (attachment != null) {
                    DataSource source = AttachmentHelper.getDataSource(attachment);
                    MimeBodyPart attachMbp = new MimeBodyPart();
                    // attach the file to the message
                    attachMbp.setDataHandler(new DataHandler(source));
                    attachMbp.setFileName(source.getName());
                    mp.addBodyPart(attachMbp);
                }
            }
        }
        message.setContent(mp);
//        if (!EmailConnectionManager.getTransport().isConnected()) {
//            connectionManager.Reconnect();
//        }
        EmailConnectionManager.getTransport().send(message);
        logger.trace("Message sent successfully");
    }

    private static Multipart createMultiPart(String body) throws MessagingException {
        Multipart mp = new MimeMultipart("related");
        MimeBodyPart bodyPart = new MimeBodyPart();
        mp.addBodyPart(bodyPart);

        int imgTagIndex = -1;

        while ((imgTagIndex = body.indexOf("<img", imgTagIndex + 1)) != -1) {
            int srcAttributeValueStartIndex = body.indexOf("src=\"", imgTagIndex) + "src=\"".length();
            int imageIdValueStartIndex = body.indexOf("imageId=", srcAttributeValueStartIndex) + "imageId=".length();
            int imageIdValueEndIndex = body.indexOf("\"", imageIdValueStartIndex);
            String imageID = body.substring(imageIdValueStartIndex, imageIdValueEndIndex);
            String url = body.substring(srcAttributeValueStartIndex, imageIdValueEndIndex);
            body = body.replace(url, "cid:" + imageID);

            MimeBodyPart imgPart = new MimeBodyPart();
            try {
                URL fullURL = new URL("http://localhost:" + EmailConnectionManager.getServerHTTPPort() + url);
                imgPart.setDataHandler(new DataHandler(fullURL));
                imgPart.setHeader("Content-ID", "<" + imageID + ">");
                imgPart.setHeader("Content-Disposition", "inline");
                mp.addBodyPart(imgPart);
            } catch (IOException iOException) {
                logger.error("Exception thrown", iOException);
            }
        }

        bodyPart.setContent(body, "text/html; charset=utf-8");

        return mp;
    }
    
}
