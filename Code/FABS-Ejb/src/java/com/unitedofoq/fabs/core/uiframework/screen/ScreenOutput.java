/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.screen;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;

/**
 *
 * @author mmohamed
 */
@Entity
@DiscriminatorValue("OUTPUT")
@ParentEntity(fields={"oscreen"})
@ChildEntity(fields={"attributesMapping"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class ScreenOutput extends ScreenDTMapping {
    // <editor-fold defaultstate="collapsed" desc="oscreen">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name="OSCREEN_OUT_DBID", nullable=false)
    // Should set (nullable=false) but it sets the column in DB not nullable
    // which is not applicable for ScreenInput Record for single ScreenDTMapping table
    protected OScreen oscreen ;
    public OScreen getOscreen() {
        return oscreen;
    }
    public void setOscreen(OScreen oscreen) {
        this.oscreen = oscreen;
    }
    public String getOscreenDD() {
        return "ScreenOutput_oscreen";
    }
    // </editor-fold>
}
