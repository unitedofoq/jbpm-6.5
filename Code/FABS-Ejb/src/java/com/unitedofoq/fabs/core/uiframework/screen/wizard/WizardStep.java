/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.screen.wizard;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;

/**
 *
 * @author mmohamed
 */
@Entity
@ParentEntity(fields={"wizard"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class WizardStep extends BaseEntity {
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private Wizard wizard;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OScreen stepScreen;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private ODataType stepDT;

    private Integer stepIndex;

    private boolean skippable;

    private boolean mustSelect;

    public boolean isMustSelect() {
        return mustSelect;
    }

    public void setMustSelect(boolean mustSelect) {
        this.mustSelect = mustSelect;
    }

    public boolean isSkippable() {
        return skippable;
    }

    public void setSkippable(boolean skippable) {
        this.skippable = skippable;
    }
    

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC stepMode = null;



    public String getMustSelectDD() {
        return "WizardStep_mustSelect";
    }

    

    public UDC getStepMode() {
        return stepMode;
    }

    public String getStepModeDD() {
        return "WizardStep_stepMode";
    }

    public void setStepMode(UDC stepMode) {
        this.stepMode = stepMode;
    }

  
    public String getSkippableDD() {
        return "WizardStep_skippable";
    }

  

    public ODataType getStepDT() {
        return stepDT;
    }

    public String getStepDTDD() {
        return "WizardStep_stepDT";
    }

    public void setStepDT(ODataType stepDT) {
        this.stepDT = stepDT;
    }

    public Integer getStepIndex() {
        return stepIndex;
    }

    public String getStepIndexDD() {
        return "WizardStep_stepIndex";
    }

    public void setStepIndex(Integer stepIndex) {
        this.stepIndex = stepIndex;
    }

    public OScreen getStepScreen() {
        return stepScreen;
    }

    public String getStepScreenDD() {
        return "WizardStep_stepScreen";
    }

    public void setStepScreen(OScreen stepScreen) {
        this.stepScreen = stepScreen;
    }

    public Wizard getWizard() {
        return wizard;
    }

    public String getWizardDD() {
        return "WizardStep_wizard";
    }

    public void setWizard(Wizard wizard) {
        this.wizard = wizard;
    }
}
