/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.setup.FABSSetup;

/**
 *
 * @author nsaleh
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.log.OLogSetUpServiceRemote",
beanInterface = OLogSetUpServiceRemote.class)
public class OLogSetUpService implements OLogSetUpServiceRemote {

    @EJB
    public OEntityManagerRemote oem;
    //<editor-fold defaultstate="collapsed" desc="Cache">
    public static HashMap<String, HashMap<String, String>> cachedLogSetup = new HashMap<String, HashMap<String, String>>();

    @Override
    public OFunctionResult loadLogInfoLevel(OUser loggedUser) {
        if (loggedUser == null) {
            return new OFunctionResult();
        }
        OFunctionResult oFR = new OFunctionResult();
        List returnedValues = new ArrayList();
        Level logLevel = null;
        try {
            HashMap fabsSetupMap = cachedLogSetup.get(String.valueOf(loggedUser.getTenant().getId()));
            if (fabsSetupMap != null && fabsSetupMap.get("LogInfoLevel") != null) {
                String logInfoLevelValue = String.valueOf(fabsSetupMap.get("LogInfoLevel"));
                if (logInfoLevelValue.equals("UNDEFINED")) {
                    return oFR;
                }
                logLevel = getLevelFromName(logInfoLevelValue);
                returnedValues.add(logLevel);
                oFR.setReturnValues(returnedValues);
                return oFR;
            } else { // setup key does not exists then load it from db and put it in the cach
                // set undefined in map to stop prevent infinite loop of calling load entity and logging
                if (fabsSetupMap == null) {
                    fabsSetupMap = new HashMap<String, String>();
                }
                fabsSetupMap.put("LogInfoLevel", "UNDEFINED");
                cachedLogSetup.put(String.valueOf(loggedUser.getTenant().getId()), fabsSetupMap);

                FABSSetup LogFabsSetup = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                        Collections.singletonList("setupKey = 'LogInfoLevel'"), null, loggedUser);
                if (LogFabsSetup != null) {
                    fabsSetupMap.put(LogFabsSetup.getSetupKey(), LogFabsSetup.getSvalue());
                    cachedLogSetup.put(String.valueOf(loggedUser.getTenant().getId()), fabsSetupMap);
                    logLevel = getLevelFromName(LogFabsSetup.getSvalue());
                    returnedValues.add(logLevel);
                    oFR.setReturnValues(returnedValues);
                    return oFR;
                }
            }
        } catch (Exception e) {
            System.out.println("*** Internal Exception in logException: "
                    + e.getMessage());
        }
        return oFR;
    }

    @Override
    public OFunctionResult loadLogQueryStatisticsLevel(OUser loggedUser) {
        if (loggedUser == null) {
            return null;
        }
        OFunctionResult oFR = new OFunctionResult();
        List returnedValues = new ArrayList();
        Level logLevel = null;
        try {
            HashMap fabsSetupMap = cachedLogSetup.get(String.valueOf(loggedUser.getTenant().getId()));
            if (fabsSetupMap != null && fabsSetupMap.get("LogQueryStatisticsLevel") != null) {
                String logInfoLevelValue = String.valueOf(fabsSetupMap.get("LogQueryStatisticsLevel"));
                if (logInfoLevelValue.equals("UNDEFINED")) {
                    return oFR;
                }
                logLevel = getLevelFromName(logInfoLevelValue);
                returnedValues.add(logLevel);
                oFR.setReturnValues(returnedValues);
                return oFR;
            } else { // setup key does not exists then load it from db and put it in the cach

                // set undefined in map to stop prevent infinite loop of calling load entity and logging
                if (fabsSetupMap == null) {
                    fabsSetupMap = new HashMap<String, String>();
                }
                fabsSetupMap.put("LogQueryStatisticsLevel", "UNDEFINED");
                cachedLogSetup.put(String.valueOf(loggedUser.getTenant().getId()), fabsSetupMap);

                FABSSetup LogFabsSetup = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                        Collections.singletonList("setupKey = 'LogQueryStatisticsLevel'"), null, loggedUser);
                if (LogFabsSetup != null) {
                    fabsSetupMap.put(LogFabsSetup.getSetupKey(), LogFabsSetup.getSvalue());
                    cachedLogSetup.put(String.valueOf(loggedUser.getTenant().getId()), fabsSetupMap);
                    logLevel = getLevelFromName(LogFabsSetup.getSvalue());
                    returnedValues.add(logLevel);
                    oFR.setReturnValues(returnedValues);
                    return oFR;
                }
            }
        } catch (Exception e) {
            System.out.println("*** Internal Exception in logException: "
                    + e.getMessage());
        }
        return oFR;
    }

    @Override
    public OFunctionResult loadLogLineOption(OUser loggedUser) {
        if (loggedUser == null) {
            return null;
        }
        OFunctionResult oFR = new OFunctionResult();
        List returnedValues = new ArrayList();
        try {
            HashMap fabsSetupMap = cachedLogSetup.get(String.valueOf(loggedUser.getTenant().getId()));
            if (fabsSetupMap != null && fabsSetupMap.get("LogLineOption") != null) {
                String logLineSetupValue = String.valueOf(fabsSetupMap.get("LogLineOption"));
                if (logLineSetupValue.equals("UNDEFINED")) {
                    return oFR;
                }
                returnedValues.add(logLineSetupValue);
                oFR.setReturnValues(returnedValues);
                return oFR;
            } else {
                // set undefined in map to stop prevent infinite loop of calling load entity and logging
                if (fabsSetupMap == null) {
                    fabsSetupMap = new HashMap<String, String>();
                }
                fabsSetupMap.put("LogLineOption", "UNDEFINED");
                cachedLogSetup.put(String.valueOf(loggedUser.getTenant().getId()), fabsSetupMap);

                List<String> condition = new ArrayList<String>();
                condition.add("setupKey = 'LogLineOption'");
                FABSSetup LogFabsSetup = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                        condition, null, loggedUser);
                if (LogFabsSetup != null) {
                    fabsSetupMap.put(LogFabsSetup.getSetupKey(), String.valueOf(LogFabsSetup.isBvalue()));
                    cachedLogSetup.put(String.valueOf(loggedUser.getTenant().getId()), fabsSetupMap);
                    returnedValues.add(LogFabsSetup.getSvalue());
                    oFR.setReturnValues(returnedValues);
                    return oFR;
                }
            }
        } catch (Exception e) {
            System.out.println("*** Internal Exception in logException: "
                    + e.getMessage());
        }
        return oFR;
    }

    @Override
    public OFunctionResult loadLoadEntityOption(OUser loggedUser) {
        if (loggedUser == null) {
            return null;
        }
        OFunctionResult oFR = new OFunctionResult();
        List returnedValues = new ArrayList();
        try {
            HashMap fabsSetupMap = cachedLogSetup.get(String.valueOf(loggedUser.getTenant().getId()));
            if (fabsSetupMap != null && fabsSetupMap.get("LogLoadEntityOption") != null) {
                String logLoadEntitySetupValue = String.valueOf(fabsSetupMap.get("LogLoadEntityOption"));
                //stop prevent infinite loop of calling load entity and logging
                if (logLoadEntitySetupValue.equals("UNDEFINED")) {
                    return oFR;
                }
                returnedValues.add(logLoadEntitySetupValue);
                oFR.setReturnValues(returnedValues);
                return oFR;
            } else {
                // set undefined in map to stop prevent infinite loop of calling load entity and logging
                if (fabsSetupMap == null) {
                    fabsSetupMap = new HashMap<String, String>();
                }
                fabsSetupMap.put("LogLoadEntityOption", "UNDEFINED");
                cachedLogSetup.put(String.valueOf(loggedUser.getTenant().getId()), fabsSetupMap);

                FABSSetup LogFabsSetup = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                        Collections.singletonList("setupKey = 'LogLoadEntityOption'"), null, loggedUser);
                if (LogFabsSetup != null) {
                    fabsSetupMap.put(LogFabsSetup.getSetupKey(), String.valueOf(LogFabsSetup.isBvalue()));
                    cachedLogSetup.put(String.valueOf(loggedUser.getTenant().getId()), fabsSetupMap);
                    returnedValues.add(LogFabsSetup.getSvalue());
                    oFR.setReturnValues(returnedValues);
                    return oFR;
                }
            }
        } catch (Exception e) {
            System.out.println("*** Internal Exception in logException: "
                    + e.getMessage());
        }
        return oFR;
    }

    @Override
    public OFunctionResult clearOLogCache(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        cachedLogSetup.clear();
        return oFR;
    }

    private Level getLevelFromName(String levelName) {
        if (levelName.equals("FINEST")) { // lowest
            return Level.FINEST;
        } else if (levelName.equals("FINER")) {
            return Level.FINER;
        } else if (levelName.equals("FINE")) {
            return Level.FINE;
        } else if (levelName.equals("CONFIG")) {
            return Level.CONFIG;
        } else if (levelName.equals("INFO")) {
            return Level.INFO;
        } else if (levelName.equals("WARNING")) {
            return Level.WARNING;
        } else if (levelName.equals("SEVERE")) {
            return Level.SEVERE;
        } else if (levelName.equals("ALL")) {
            return Level.ALL;
        } else if (levelName.equals("OFF")) {
            return Level.OFF;
        }
        return null;
    }

    @Override
    public OFunctionResult onFabsSetUpUpdate(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            if (cachedLogSetup != null) {
                if (cachedLogSetup.get(String.valueOf(loggedUser.getTenant().getId())) != null) {
                    cachedLogSetup.remove(String.valueOf(loggedUser.getTenant().getId()));
                }
            }
            HashMap<String, String> fabsSetupMap = new HashMap<String, String>();

            FABSSetup LogFabsSetup = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                    Collections.singletonList("setupKey = 'LogInfoLevel'"), null, loggedUser);
            if (LogFabsSetup != null) {
                fabsSetupMap.put(LogFabsSetup.getSetupKey(), LogFabsSetup.getSvalue());
            }
            LogFabsSetup = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                    Collections.singletonList("setupKey = 'LogQueryStatisticsLevel'"), null, loggedUser);
            if (LogFabsSetup != null) {
                fabsSetupMap.put(LogFabsSetup.getSetupKey(), LogFabsSetup.getSvalue());
            }
            LogFabsSetup = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                    Collections.singletonList("setupKey = 'LogLineOption'"), null, loggedUser);
            if (LogFabsSetup != null) {
                fabsSetupMap.put(LogFabsSetup.getSetupKey(), String.valueOf(LogFabsSetup.isBvalue()));
            }
            LogFabsSetup = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                    Collections.singletonList("setupKey = 'LogLoadEntityOption'"), null, loggedUser);
            if (LogFabsSetup != null) {
                fabsSetupMap.put(LogFabsSetup.getSetupKey(), String.valueOf(LogFabsSetup.isBvalue()));
            }
            cachedLogSetup.put(String.valueOf(loggedUser.getTenant().getId()), fabsSetupMap);
        } catch (Exception ex) {
            System.out.println("*** Internal Exception in logException: "
                    + ex.getMessage());
        } finally {
            return oFR;
        }
    }
}
