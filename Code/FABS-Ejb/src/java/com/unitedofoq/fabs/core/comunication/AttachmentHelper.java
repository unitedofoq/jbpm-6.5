/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.comunication;

import com.unitedofoq.fabs.core.content.AttachmentDTO;
import java.util.ArrayList;
import java.util.List;
import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;

/**
 *
 * @author mostafan
 */
public class AttachmentHelper {
    
    
    public static DataSource getDataSource(AttachmentDTO attachment){
        String buf= new StringBuffer(attachment.getName()).reverse().toString();
        int mimeIndex = buf.length() - 1 - buf.indexOf(".");
        String mime = "application/"+attachment.getName().substring(mimeIndex+1);
        ByteArrayDataSource data = new ByteArrayDataSource(attachment.getAttachementBytes(),mime);
        data.setName(attachment.getName());
        return data;
    }
    
    public static List<String> getNamesFromAttachments(List<AttachmentDTO> attachments){
        if(attachments == null)
            return null;
        List<String> attachmentList = new ArrayList<String>();
        for(AttachmentDTO attachemnt: attachments ){
            if(attachemnt != null){
                attachmentList.add(attachemnt.getName());
            }
        }
        return attachmentList;
    
    }
    public static List<String> getAttachmentDTOFromAttachmentListNameAndAttachmentListData(List<AttachmentDTO> attachments){
        if(attachments == null)
            return null;
        List<String> attachmentList = new ArrayList<String>();
        for(AttachmentDTO attachemnt: attachments ){
            if(attachemnt != null){
             attachmentList.add(attachemnt.getAttachementDataEncoded());
           }
        }
        return attachmentList;
    
    }
    public static List<AttachmentDTO> getListOFAttachements(List<String> name , List<String> attachmentsData){
        
        if(name == null && attachmentsData == null){
            return null;
        }
        int nameNumber = name.size();
        
        if(nameNumber != attachmentsData.size()){
            return null;
        }
        List<AttachmentDTO> attachmentList = new ArrayList<AttachmentDTO>();
        for(int i = 0 ; i < nameNumber ; i++){
             attachmentList.add(new AttachmentDTO(name.get(i),attachmentsData.get(i)));
             
        }
        return attachmentList;
    
    }
    
    
}
