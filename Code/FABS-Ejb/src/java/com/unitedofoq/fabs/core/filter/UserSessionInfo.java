/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.filter;

import java.util.Map;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenInput;

/**
 * Class that holds all available data about the user session
 * 
 * @author arezk
 */
public class UserSessionInfo {
    private Map sessionAttributes;

    public Map getSessionAttributes() {
        return sessionAttributes;
    }

    public void setSessionAttributes(Map sessionAttributes) {
        this.sessionAttributes = sessionAttributes;
    }
    
    private ODataMessage dataMessage;

    public ODataMessage getDataMessage() {
        return dataMessage;
    }

    public void setDataMessage(ODataMessage dataMessage) {
        this.dataMessage = dataMessage;
    }
    
    private ScreenInput currentInput;

    public ScreenInput getCurrentInput() {
        return currentInput;
    }

    public void setCurrentInput(ScreenInput currentInput) {
        this.currentInput = currentInput;
    }
    
    private OUser loggedUser;

    public OUser getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(OUser loggedUser) {
        this.loggedUser = loggedUser;
    }
    
    private boolean C2A;

    public boolean isC2A() {
        return C2A;
    }

    public void setC2A(boolean C2A) {
        this.C2A = C2A;
    }
    
    private OEntity oactOnEntity;

    public OEntity getOactOnEntity() {
        return oactOnEntity;
    }

    public void setOactOnEntity(OEntity oactOnEntity) {
        this.oactOnEntity = oactOnEntity;
    }
}
