package com.unitedofoq.fabs.core.uiframework.navigationmenu;

/**
 *
 * @author tarzy
 */
public class OImageDTO {

    private Long dbid;
    private String name;
    private byte[] image;

    public OImageDTO(Long dbid, String name, byte[] image) {
        this.dbid = dbid;
        this.name = name;
        this.image = image;
    }

    
    
    public Long getDbid() {
        return dbid;
    }

    public void setDbid(Long dbid) {
        this.dbid = dbid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
