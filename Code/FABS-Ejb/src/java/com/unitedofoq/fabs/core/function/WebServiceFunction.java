/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.function;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.webservice.OWebService;

/**
 *
 * @author htawab
 */
@Entity
@DiscriminatorValue(value = "WSFunction")
@ParentEntity(fields = {"webService"})
@ChildEntity(fields = {"functionImage", "menuFunction", "webServiceFunctionParamsMappings"})
@VersionControlSpecs(versionControlType = VersionControlType.Parent)
public class WebServiceFunction extends OFunction {
    // <editor-fold defaultstate="collapsed" desc="odataType">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    ODataType odataType;

    public String getOdataTypeDD() {
        return "WebServiceFunction_odataType";
    }

    public ODataType getOdataType() {
        return odataType;
    }

    public void setOdataType(ODataType odataType) {
        this.odataType = odataType;
    }
    // </editor-fold> 
    // <editor-fold defaultstate="collapsed" desc="webService">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    OWebService webService;

    public OWebService getWebService() {
        return webService;
    }

    public void setWebService(OWebService webService) {
        this.webService = webService;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="webServiceFunctionParamsMappings">
    @OneToMany(mappedBy = "webServiceFunction")
    List<WebServiceFunctionParamsMapping> webServiceFunctionParamsMappings;

    public String getWebServiceFunctionParamsMappingsDD() {
        return "WebServiceFunction_webServiceFunctionParamsMappings";
    }

    public List<WebServiceFunctionParamsMapping> getWebServiceFunctionParamsMappings() {
        return webServiceFunctionParamsMappings;
    }

    public void setWebServiceFunctionParamsMappings(List<WebServiceFunctionParamsMapping> webServiceFunctionParamsMappings) {
        this.webServiceFunctionParamsMappings = webServiceFunctionParamsMappings;
    }
    // </editor-fold>
}
