/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.security.user;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.service.PasswordTrackerLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.central.OTenant;
import com.unitedofoq.fabs.core.comunication.EMailSenderLocal;
import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.module.ModuleServiceRemote;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.setup.FABSSetup;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkEntityManagerRemote;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagementType;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NamingException;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nkhalil
 */
@Stateless
@PermitAll
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.security.user.UserServiceRemote",
        beanInterface = UserServiceRemote.class)
@javax.ejb.TransactionManagement(TransactionManagementType.BEAN)
public class UserServiceBean implements UserServiceRemote {

    final static Logger logger = LoggerFactory.getLogger(UserServiceBean.class);
    @EJB
    LdapService ldapService;
    @EJB
    private FABSSetupLocal setupService;
    @EJB
    OEntityManagerRemote oem;
    @EJB
    UIFrameworkServiceRemote uiFrameworkService;
    @EJB
    UIFrameworkEntityManagerRemote uiFrameworkEM;
    @EJB
    EntitySetupServiceRemote entitySetupService;
    @EJB
    UserMessageServiceRemote userMsgService;
    @EJB
    OFunctionServiceRemote functionService;
    @EJB
    OCentralEntityManagerRemote centralEM;
    @EJB
    ModuleServiceRemote moduleService;
    @EJB
    private EMailSenderLocal eMailSender;
    @EJB
    protected DDServiceRemote ddService;
    @EJB
    DataTypeServiceRemote dataTypeServiceRemote;

    private static HashMap<String, OUser> cachedLoggedUsers = new HashMap<>();

    public boolean checkUserRole(long userDBID, long roleDBID, OUser loggedUser) {
        logger.debug("Entering");
        try {
            List<String> conditions = new ArrayList<String>();
            conditions.add("ouser.dbid = " + userDBID);
            conditions.add("oRole.dbid = " + roleDBID);
            if (oem.loadEntity(RoleUser.class.getSimpleName(), conditions, null, loggedUser) != null) {
                logger.debug("Returning with True");
                return true;
            } else {
                logger.debug("Returning with False");
                return false;
            }
        } catch (Exception ex) {
            logger.error("exception thrown: ", ex);
            logger.debug("Returning with false");
            return false;
        }
    }

    /**
     * Connects to the LDAP server using parameters from
     * LDAPConnection.properties
     *
     * @return DirContext Object
     * @throws IOException
     */
    @Override
    public OFunctionResult synchUsersAndRolesWithLDAP(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            oem.getEM(loggedUser);
            OModule systemModule = moduleService.loadOModule("System", loggedUser);
            if (!ldapService.checkLdapConnection(loggedUser)) {
                oFR.addError(userMsgService.getUserMessage("LDAPConnectionError", loggedUser));
                logger.trace("LDAP Connection Error");
                logger.debug("LDAP Connection Error");
                return oFR;
            }
            List< OUser> returnedUsers = ldapService.getLdapUsers(loggedUser);
            Iterator<OUser> iterator = returnedUsers.iterator();
            // LDAP Users
            List<String> ldapUsers = new ArrayList<String>();
            while (iterator.hasNext()) {
                OUser iteratorUser = iterator.next();
                String userName = iteratorUser.getLoginName();
                ldapUsers.add(userName);
                OUser ouser = checkIfUserExists(userName, loggedUser);

                // Get user attributes from LDAP
                String mail = iteratorUser.getEmail();
                String password = iteratorUser.getPassword();
                OUser lDapUser = new OUser();
                // User Exists
                if (ouser != null) {
                    lDapUser.setEmail(mail);
                    lDapUser.setPassword(password);
                    oFR.append(updateUser(ouser, lDapUser, loggedUser));
                } else {
                    lDapUser.setLoginName(userName);
                    lDapUser.setEmail(mail);
                    lDapUser.setPassword(password);
                    oFR.append(addUser(lDapUser, loggedUser, systemModule));
                }

            }
            // __________
            // FABS Users
            List<OUser> fabsUsers = oem.loadEntityList("OUser",
                    Collections.singletonList(OEntityManager.CONFCOND_GET_ACTIVEANDINACTIVE), null, null, loggedUser);

            Iterator<OUser> fabsUserIterator = fabsUsers.iterator();

            while (fabsUserIterator.hasNext()) {
                OUser fabsUser = fabsUserIterator.next();
                // If FABS user not found in LDAP users, soft delete it
                if (!ldapUsers.contains(fabsUser.getLoginName())) {
                    fabsUser.setInActive(true);
                    oFR.append(entitySetupService.callEntityUpdateAction(fabsUser, loggedUser));
                }
            }
            // LDAP Groups
            List< RoleUser> returnedRoleUsers = ldapService.getLdapRoleUsers(loggedUser);
            List<String> ldapGroups = new ArrayList<String>();
            Map<String, List<String>> ldapGroupsMembers = new Hashtable<String, List<String>>();

            Iterator<RoleUser> roleUsersIterator = returnedRoleUsers.iterator();

            while (roleUsersIterator.hasNext()) {
                RoleUser IterRole = roleUsersIterator.next();
                String groupName = IterRole.getOrole().getName();
                ldapGroups.add(groupName);

                List conditions = new ArrayList();
                conditions.add("name = '" + groupName + "'");
                conditions.add(OEntityManager.CONFCOND_GET_ACTIVEANDINACTIVE);
                ORole orole = (ORole) oem.loadEntity("ORole", conditions, null, loggedUser);

                // LDAP Group exists in FABS ORoles
                List<String> groupMembers = new ArrayList();

                String memberName = IterRole.getOuser().getLoginName();
                // Get currentLDAPGroupMembers = currentLDAPGroup.members
                groupMembers.add(memberName);

                // All LDAP group names, and members of each group
                ldapGroupsMembers.put(groupName, groupMembers);

                if (orole != null) {
                    conditions = new ArrayList();
                    conditions.add("orole.name = '" + groupName + "'");
                    conditions.add(OEntityManager.CONFCOND_GET_ACTIVEANDINACTIVE);
                    // Get roleUsersOfCurrentLDAPGroup FROM RoleUsers WHERE ORole.name = currentLDAPGroup.name
                    List<RoleUser> roleUsersOfCurrGroup = oem.loadEntityList("RoleUser", conditions, null, null, loggedUser);

                    // Loop on currentLDAPGroupMembers
                    List<String> roleUsersLoginNames = new ArrayList<String>();
                    Iterator<RoleUser> roleUsersOfCurrGroupIter = roleUsersOfCurrGroup.iterator();
                    while (roleUsersOfCurrGroupIter.hasNext()) {
                        RoleUser roleUser = roleUsersOfCurrGroupIter.next();
                        if (roleUser.getOuser() == null) {
                            logger.debug("RoleUser with NULL or non-existing OUser");
                            continue;
                        }
                        roleUsersLoginNames.add(roleUser.getOuser().getLoginName());
                    }
                    Iterator<String> groupMembersIter = groupMembers.iterator();
                    while (groupMembersIter.hasNext()) {
                        String member = groupMembersIter.next();
                        if (!roleUsersLoginNames.contains(member)) {
                            RoleUser newRoleUser = new RoleUser();
                            OUser currentMemberAsOUser = (OUser) oem.loadEntity("OUser", Collections.singletonList("loginName = '" + member + "'"), null, loggedUser);
                            if (currentMemberAsOUser == null) {
                                logger.trace("LDAP group member cannot be NULL in OUsers table");
                                continue;
                            }
                            newRoleUser.setOuser(currentMemberAsOUser);
                            newRoleUser.setOrole(roleUsersOfCurrGroup.get(0).getOrole());
                            oFR.append(entitySetupService.callEntityUpdateAction(newRoleUser, loggedUser));
                        } else {
                            // Found but deleted <<<<<<<
                            Iterator<RoleUser> roleUsersOfCurrGroupIterator = roleUsersOfCurrGroup.iterator();
                            while (roleUsersOfCurrGroupIterator.hasNext()) {
                                RoleUser roleUser = roleUsersOfCurrGroupIterator.next();
                                if (roleUser.getOuser() == null) {
                                    continue;
                                }
                                if (roleUser.getOuser().getLoginName().equals(member)) {
                                    if (roleUser.isInActive()) {
                                        roleUser.setInActive(false);
                                        oFR.append(entitySetupService.callEntityUpdateAction(roleUser, loggedUser));
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    // currentLDAPGroup NOT exist in ORoles, Create new one
                } else {
                    ORole newRole = new ORole();
                    newRole.setName(groupName);
                    newRole.setOmodule(systemModule);
                    oFR.append(entitySetupService.callEntityCreateAction(newRole, loggedUser));

                    // Loop on currentLDAPGroup members
                    Iterator<String> groupMembersIterator = groupMembers.iterator();
                    while (groupMembersIterator.hasNext()) {
                        String groupMember = groupMembersIterator.next();
                        RoleUser newRoleUser = new RoleUser();
                        newRoleUser.setOrole(newRole);
                        OUser loadedOUser = (OUser) oem.loadEntity("OUser", Collections.singletonList(
                                "loginName = '" + groupMember + "'"), null, loggedUser);
                        newRoleUser.setOuser(loadedOUser);
                        oFR.append(entitySetupService.callEntityCreateAction(newRoleUser, loggedUser));
                    }
                }
            }

            // __________
            // FABS Roles
            List<ORole> fabsRoles = oem.loadEntityList("ORole",
                    Collections.singletonList(OEntityManager.CONFCOND_GET_ACTIVEANDINACTIVE), null, null, loggedUser);
            Iterator<ORole> fabsRolesIterator = fabsRoles.iterator();
            while (fabsRolesIterator.hasNext()) {
                ORole orole = fabsRolesIterator.next();
                if (ldapGroups.contains(orole.getName())) {

                    // Get currentRoleUser_OUsers = currentORole.OUsers
                    List<OUser> currentRoleUser_OUsers = new ArrayList<OUser>();
                    List<RoleUser> currentOroleUsers = orole.getOroleUsers();
                    Iterator<RoleUser> roleUserIterator = currentOroleUsers.iterator();
                    for (; roleUserIterator.hasNext();) {
                        RoleUser roleUser = roleUserIterator.next();
                        // Remove RolesUsers with NULL OUsers
                        if (roleUser.getOuser() == null) {
                            roleUserIterator.remove();
                            continue;
                        }
                        currentRoleUser_OUsers.add(roleUser.getOuser());
                    }
                    // Get LDAPGroupMembersOfCurrentRoleUser FROM groupMembers WHERE group.name = currentRoleUser.name
                    List<String> groupMembers = ldapGroupsMembers.get(orole.getName());
                    Iterator<OUser> currentRoleUser_OUsersIter = currentRoleUser_OUsers.iterator();
                    while (currentRoleUser_OUsersIter.hasNext()) {
                        OUser currentRoleUser_OUser = currentRoleUser_OUsersIter.next();
                        if (!groupMembers.contains(currentRoleUser_OUser.getLoginName())) {
                            // set RoleUser.deleted = 1 <<<<
                            Iterator<RoleUser> roleUserIter = currentOroleUsers.iterator();
                            while (roleUserIter.hasNext()) {
                                RoleUser roleUser = roleUserIter.next();
                                if (roleUser.getOrole().getName().equals(orole.getName())
                                        && roleUser.getOuser().getLoginName().equals(currentRoleUser_OUser.getLoginName())) {
                                    roleUser.setInActive(true);
                                    oFR.append(entitySetupService.callEntityCreateAction(roleUser, loggedUser));
                                }
                            }
                        }
                    }
                } else {
                    // currentORole DOES NOT exist in LDAP groups
                    orole.setInActive(true);
                    oFR.append(entitySetupService.callEntityCreateAction(orole, loggedUser));
                }
            }
        } catch (Exception ex) {
            logger.error("exception thrown: ", ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult addUserToLDAP(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();

        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OUser.class);
        if (inputValidationErrors != null) {
            logger.debug("Returning");
            return inputValidationErrors;
        }
        // </editor-fold>
        OUser user = null;
        try {
            user = (OUser) odm.getData().get(0);
            if (user.getTenant() == null) {
                logger.debug("User Tanenet is Null");
                user.setTenant(loggedUser.getTenant());
            }
            // If User is not found in Cent, Add it:
            //            if (centralEM.getUserTenant(user.getLoginName()) == null) {
            //                centralEM.addOCentralUser(user);
            //            } else {
            //                oFR.addError(userMsgService.getUserMessage("UserAlreadyFoundInCent",
            //                        Collections.singletonList(user.getLoginName()), loggedUser));
            //            }

            if (!ldapService.checkLdapConnection(loggedUser)) {
                oFR.addError(userMsgService.getUserMessage("LDAPConnectionError", loggedUser));
                logger.trace("LDAP Connection Error");
                logger.debug("Returning");
                return oFR;
            }
            if (user.getLoginName().isEmpty()) {
                logger.debug("Login Name is Empty");
                logger.trace("Login Name Required");
                logger.trace("LDAP Add User Error");
                oFR.addError(userMsgService.getUserMessage("LoginNameRequired", loggedUser));
                oFR.addError(userMsgService.getUserMessage("LDAPAddUserError",
                        Collections.singletonList(user.getLoginName()), loggedUser));
                logger.debug("Returning");
                return oFR;
            }
            if (user.getPassword().isEmpty()) {
                logger.debug("Login Password is Empty");
                logger.trace("Password Required");
                logger.trace("LDAP Add User Error");
                oFR.addError(userMsgService.getUserMessage("PasswordRequired", loggedUser));
                oFR.addError(userMsgService.getUserMessage("LDAPAddUserError",
                        Collections.singletonList(user.getLoginName()), loggedUser));
                logger.debug("Returning");
                return oFR;
            }
            if (!ldapService.checkUserExistence(user, loggedUser)) {
                if (ldapService.addUser(user, loggedUser)) {
                    oFR.addSuccess(userMsgService.getUserMessage(
                            "LDAPSavedUser",
                            Collections.singletonList(user.getLoginName()),
                            loggedUser));
                } else {
                    oFR.addError(userMsgService.getUserMessage(
                            "LDAPAddUserError",
                            Collections.singletonList(user.getLoginName()),
                            loggedUser), null);
                    logger.debug("Returning");
                    return oFR;
                }
            } else {
                oFR.addError(userMsgService.getUserMessage("UserAlreadyExistedInLDAP",
                        Collections.singletonList(user.getLoginName()), loggedUser
                ), null);
                logger.debug("Returning");
                return oFR;
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {
            logger.debug("Returning");
            oem.closeEM(loggedUser);
            return oFR;
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Reset User PassWord">
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public OFunctionResult resetUserPassowrd(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser
    ) {
        logger.debug("Entering");
        OUser user;
        OFunctionResult oFR = new OFunctionResult();
        String newPWD = generateNewPassword();

        try {
            user = (OUser) odm.getData().get(0);

            if (!ldapService.checkLdapConnection(loggedUser)) {
                oFR.addError(userMsgService.getUserMessage("LDAPConnectionError", loggedUser));
                logger.trace("LDAP Connection Error");
                logger.debug("Returning");
                return oFR;
            }
            if (!ldapService.checkUserExistence(user, loggedUser)) {
                oFR.addError(userMsgService.getUserMessage("UserNotFoundInLDAP",
                        Collections.singletonList(user.getLoginName()), loggedUser
                ), null);
                logger.debug("Returning");
                return oFR;
            } else {
                if (!ldapService.changeUserPassword(user, newPWD, loggedUser)) {
                    oFR.addError(userMsgService.getUserMessage("LDAPUserPasswordIsNotChanged",
                            Collections.singletonList(user.getLoginName()), loggedUser));
                } else {
                    oFR.addSuccess(userMsgService.getUserMessage("LDAPSavedUser",
                            Collections.singletonList(user.getLoginName()), loggedUser));

                    oFR.addSuccess(userMsgService.getUserMessage("LDAPSavedUser",
                            Collections.singletonList(user.getLoginName()), loggedUser));

                    sendMailWithNewPWD(newPWD, user, loggedUser);

                }
            }

        } catch (Exception ex) {
            logger.error("exception thrown: ", ex);
        }
        logger.debug("Returning");
        return oFR;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Send EMail With The New PassWord">

    private void sendMailWithNewPWD(String newPWD, OUser user, OUser loggedUser) {
        logger.debug("Entering");
        String eMailBody = "";
        String eMailSubject = "";

        try {

            if (user.getFirstLanguage().getDbid() == 23) {
                eMailBody = ((FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                        Collections.singletonList("setupKey='resetPWDEMailBody'"),
                        null,
                        loggedUser)).getSvalue();

                eMailSubject = ((FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                        Collections.singletonList("setupKey='resetPWDEMailSubject'"),
                        null,
                        loggedUser)).getSvalue();
            } else {
                eMailBody = ((FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                        Collections.singletonList("setupKey='resetPWDEMailBodyTranslated'"),
                        null,
                        loggedUser)).getSvalue();

                eMailSubject = ((FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                        Collections.singletonList("setupKey='resetPWDEMailSubjectTranslated'"),
                        null,
                        loggedUser)).getSvalue();
            }

            eMailBody = "<html><head><title></title></head><body>" + eMailBody + "</body></html>";
            eMailBody = eMailBody.replace("[[1]]", user.getDisplayName());
            eMailBody = eMailBody.replace("[[2]]", newPWD);

            eMailSender.sendHTMLEMail(null, user.getEmail(),
                    eMailSubject, eMailBody, loggedUser);
            logger.debug("Returning");
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Generate Random PassWord">

    private String generateNewPassword() {
        logger.debug("Entering");
        int pwdLength = (int) Math.round(8 + Math.random() * (14 - 8));

        String passwordCHRs = "abcdefghijklmnopqrstuvwxyz"
                + "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "01234567890";

        String PWD = "";

        int chracterLocationInList;

        for (int i = 0; i < pwdLength; i++) {

            chracterLocationInList = (int) Math.round(Math.random() * 61);
            PWD += passwordCHRs.charAt(chracterLocationInList);
        }
        logger.debug("Returning");
        return PWD;
    }
    //</editor-fold>

    /*private String getPasswordFromLDAP(OUser loggedUser) throws IOException, NamingException, LDAPNotFoundException{
     DirContext ctx = getLDAPContext(loggedUser);
     if (ctx == null) {
     //oFR.addError(userMsgService.getUserMessage("LDAPConnectionError", loggedUser));
     //logger.debug("Returning as LDAP Connection Error");
     //return oFR;
     throw new LDAPNotFoundException();
     }
     return getPasswordFromLDAP(ctx,loggedUser.getLoginName());
     }*/
    private void updateUserByLiferay(OUser newUser, OUser oldUser, OUser loggedUser) throws IOException, NamingException, LDAPNotFoundException, PortalException, SystemException {
        long companyId = PortalUtil.getDefaultCompanyId();
        long userIdLiferay = UserLocalServiceUtil.getUserIdByScreenName(companyId, oldUser.getLoginName());
        if (newUser.getLatestPassword() != null && !newUser.getLatestPassword().equals("") && !oldUser.getLatestPassword().equals(newUser.getLatestPassword())) {
            UserLocalServiceUtil.updatePassword(userIdLiferay, newUser.getLatestPassword(), newUser.getLatestPassword(), false, true);//the last argument should be false but when ever we edit password will validate
        }

        String oldpass = UserLocalServiceUtil.getUser(userIdLiferay).getPasswordUnencrypted();//doesn't work
        UserLocalServiceUtil.updateEmailAddress(userIdLiferay, oldpass, newUser.getEmail(), newUser.getEmail());

        if (newUser.isInActive()) {
            UserLocalServiceUtil.updateStatus(userIdLiferay, WorkflowConstants.STATUS_INACTIVE);
        } else {
            UserLocalServiceUtil.updateStatus(userIdLiferay, WorkflowConstants.STATUS_APPROVED);
        }

    }

    @Override
    public OFunctionResult unlockLiferayUser(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult ofr = new OFunctionResult();
        try {
            OUser user = (OUser) odm.getData().get(0);
            long companyId = PortalUtil.getDefaultCompanyId();
            long userIdLiferay = UserLocalServiceUtil.getUserIdByScreenName(companyId, user.getLoginName());
            User u = UserLocalServiceUtil.updateLockoutById(userIdLiferay, false);
            if (u == null) {
                ofr.addError(userMsgService.getUserMessage("UserUnlockError", loggedUser));
            } else {
                ofr.addSuccess(userMsgService.getUserMessage("UserUnlockedFromLiferay", loggedUser));
            }
            logger.debug("Returning");
            return ofr;
        } catch (Exception ex) {
            ofr.addError(userMsgService.getUserMessage("UesrUnlockError", loggedUser), ex);
            return ofr;
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public OFunctionResult editUserInLiferay(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OUser.class);
        if (inputValidationErrors != null) {
            logger.debug("Returning");
            return inputValidationErrors;
        }
        // </editor-fold>
        OUser user = null;
        try {
            user = (OUser) odm.getData().get(0);
            OUser userFromDB = (OUser) oem.loadEntity("OUser", user.getDbid(),
                    Collections.singletonList(OEntityManager.CONFCOND_GET_ACTIVEANDINACTIVE),
                    null, loggedUser);

            updateUserByLiferay(user, userFromDB, loggedUser);
        }/*catch(LDAPNotFoundException ex){
         oFR.addError(userMsgService.getUserMessage("LDAPConnectionError", loggedUser));
         logger.debug("Returning as LDAP Connection Error");
         return oFR;
         }*/ catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("exception thrown: ", ex);
            oFR.addError(userMsgService.getUserMessage("LDAPEditUserError",
                    Collections.singletonList(user.getLoginName()), loggedUser));
            // </editor-fold>
        }
        return oFR;
    }

    @Override
    public OFunctionResult editUserInLDAP(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OUser.class);
        if (inputValidationErrors != null) {
            logger.debug("Returning");
            return inputValidationErrors;
        }
        // </editor-fold>
        OUser user = null;
        try {
            user = (OUser) odm.getData().get(0);

            if (user.getPassword().isEmpty()) {
                oFR.addError(userMsgService.getUserMessage("PasswordRequired", loggedUser));
                oFR.addError(userMsgService.getUserMessage("LDAPAddUserError",
                        Collections.singletonList(user.getLoginName()), loggedUser));
                logger.debug("Returning, as Password is Empty");
                return oFR;
            }

            if (!ldapService.checkLdapConnection(loggedUser)) {
                oFR.addError(userMsgService.getUserMessage("LDAPConnectionError", loggedUser));
                logger.trace("LDAP Connection Error");
                logger.debug("Returning");
                return oFR;
            }
            if (!ldapService.checkUserExistence(user, loggedUser)) {
                oFR.addError(userMsgService.getUserMessage("UserNotFoundInLDAP",
                        Collections.singletonList(user.getLoginName()), loggedUser), null);
                logger.debug("Returning");
                return oFR;
            } else {
                if (ldapService.updateUser(user, loggedUser)) {
                    oFR.addSuccess(userMsgService.getUserMessage("LDAPSavedUser",
                            Collections.singletonList(user.getLoginName()), loggedUser));
                } else {
                    oFR.addError(userMsgService.getUserMessage("LDAPEditUserError",
                            Collections.singletonList(user.getLoginName()), loggedUser));
                }
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("exception thrown: ", ex);
            oFR.addError(userMsgService.getUserMessage("LDAPEditUserError",
                    Collections.singletonList(user.getLoginName()), loggedUser));
            // </editor-fold>
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult deleteUserFromLDAP(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser
    ) {
        logger.debug("entering");
        OFunctionResult oFR = new OFunctionResult();
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OUser.class);
        if (inputValidationErrors != null) {
            logger.debug("Returning");
            return inputValidationErrors;
        }
        // </editor-fold>
        OUser user = null;
        try {
            oem.getEM(loggedUser);
            user = (OUser) odm.getData().get(0);

            if (!ldapService.checkLdapConnection(loggedUser)) {
                oFR.addError(userMsgService.getUserMessage(
                        "LDAPConnectionError", loggedUser));
                logger.trace("LDAP Connection Error");
                logger.debug("Returning");
                return oFR;
            }

            if (ldapService.deleteUser(user, loggedUser)) {
                oFR.addSuccess(userMsgService.getUserMessage(
                        "LDAPDeletedUser",
                        Collections.singletonList(user.getLoginName()),
                        loggedUser));
            } else {
                oFR.addError(userMsgService.getUserMessage(
                        "LDAPRemovedUserError",
                        Collections.singletonList(user.getLoginName()),
                        loggedUser));
            }

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            oFR.addError(userMsgService.getUserMessage("LDAPRemovedUserError",
                    Collections.singletonList(user.getLoginName()), loggedUser));
            // </editor-fold>
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override

    public OFunctionResult ORoleCreateAction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, ORole.class
        );
        if (inputValidationErrors
                != null) {
            logger.debug("Returning");
            return inputValidationErrors;
        }
        // </editor-fold>
        ORole role = null;

        try {
            oem.getEM(loggedUser);
            role = (ORole) odm.getData().get(0);

            if (!ldapService.checkLdapConnection(loggedUser)) {
                oFR.addError(userMsgService.getUserMessage("LDAPConnectionError", loggedUser));
                logger.trace("LDAP Connection Error");
                logger.debug("Returning");
                return oFR;
            }
            if (ldapService.checkRoleExistence(role, loggedUser)) {
                oFR.addError(userMsgService.getUserMessage("RoleFoundInLDAP",
                        Collections.singletonList(role.getName()), loggedUser
                ), null);
                logger.debug("Returning");
                return oFR;
            } else {
                try {
                    // logger.error("Exception thrown: ", ex);
                    // Add OUser to FABS DB
                    oFR = uiFrameworkEM.saveEntity(odm, functionParms, loggedUser);
                } catch (Exception ex1) {
                    oFR.addError(userMsgService.getUserMessage("SystemInternalError", loggedUser), ex1);
                    oFR.addError(userMsgService.getUserMessage("RoleNotSaved", loggedUser), ex1);
                    logger.debug("Returning");
                    return oFR;
                }
                if (ldapService.addRole(role, loggedUser)) {
                    oFR.addSuccess(userMsgService.getUserMessage("LDAPSavedRole",
                            Collections.singletonList(role.getName()), loggedUser));
                    logger.debug("Returning");
                    return oFR;
                } else {
                    oFR.addError(userMsgService.getUserMessage("LDAPAddRoleError",
                            Collections.singletonList(role.getName()), loggedUser));
                    oFR.addError(userMsgService.getUserMessage("SystemInternalError", loggedUser), null);
                    // Delete Role
                    entitySetupService.callEntityDeleteAction(role, loggedUser);
                    logger.debug("Returning");
                    return oFR;
                }

            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            if (ex.getCause().getClass().equals(NameAlreadyBoundException.class)) {
                oFR.addError(userMsgService.getUserMessage("LDAPRoleAlreadyFound",
                        Collections.singletonList(role.getName()), loggedUser));
            } else {
                oFR.addError(userMsgService.getUserMessage("LDAPAddRoleError",
                        Collections.singletonList(role.getName()), loggedUser));
            }
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    /**
     * Post action for creating new ORole
     */
    @Override
    public OFunctionResult addRoleToLDAP(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult(); // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(odm,
                loggedUser, ORole.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors; // </editor-fold> ORole role = null;
        }
        ORole role = null;
        try {
            oem.getEM(loggedUser);
            role = (ORole) odm.getData().get(0);
            if (!ldapService.checkLdapConnection(loggedUser)) {
                oFR.addError(userMsgService.getUserMessage("LDAPConnectionError", loggedUser));
                logger.trace("LDAP Connection Error");
                logger.debug("Returning");
                return oFR;
            }

            // Check if role exists in LDAP
            if (ldapService.checkRoleExistence(role, loggedUser)) {
                oFR.addError(userMsgService.getUserMessage("LDAPRoleAlreadyFound",
                        Collections.singletonList(role.getName()), loggedUser));
            } else {
                ldapService.addRole(role, loggedUser);
                oFR.addSuccess(userMsgService.getUserMessage("LDAPSavedRole",
                        Collections.singletonList(role.getName()), loggedUser));
            }
        } catch (Exception ex) { // <editor-fold defaultstate="collapsed" desc="Log">
            oFR.addError(userMsgService.getUserMessage("LDAPAddRoleError",
                    Collections.singletonList(role.getName()), loggedUser));
            logger.debug(ex.getMessage()); // </editor-fold>
        } finally {
            oem.closeEM(loggedUser);
            return oFR;
        }
    }

    @Override
    public OFunctionResult editRoleInLDAP(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser
    ) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, ORole.class);
        if (inputValidationErrors != null) {
            logger.debug("Returning");
            return inputValidationErrors;
        }
        // </editor-fold>
        ORole role = null;
        try {
            oem.getEM(loggedUser);
            role = (ORole) odm.getData().get(0);
            if (!ldapService.checkLdapConnection(loggedUser)) {
                oFR.addError(userMsgService.getUserMessage("LDAPConnectionError", loggedUser));
                logger.trace("LDAP Connection Error");
                logger.debug("Returning");
                return oFR;
            }
            if (!ldapService.checkRoleExistence(role, loggedUser)) {
                oFR.addError(userMsgService.getUserMessage("RoleANotFoundInLDAP",
                        Collections.singletonList(role.getName()), loggedUser), null);
                logger.debug("Returning");
                return oFR;
            } else {
                if (ldapService.updateRole(role, loggedUser)) {
                    oFR.addSuccess(userMsgService.getUserMessage("LDAPSavedRole",
                            Collections.singletonList(role.getName()), loggedUser));
                } else {
                    oFR.addError(userMsgService.getUserMessage("LDAPEditRoleError",
                            Collections.singletonList(role.getName()), loggedUser));
                }
            }

        } catch (Exception ex) {
            // TO-DO: When error in saving the Role, Add handling for the this

            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            oFR.addError(userMsgService.getUserMessage("LDAPEditRoleError",
                    Collections.singletonList(role.getName()), loggedUser));
            // </editor-fold>
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult deleteRoleFromLDAP(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser
    ) {
        logger.debug("entering");
        OFunctionResult oFR = new OFunctionResult();
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, ORole.class);
        if (inputValidationErrors != null) {
            logger.debug("Returning");
            return inputValidationErrors;
        }
        // </editor-fold>
        ORole role = null;
        try {
            oem.getEM(loggedUser);
            role = (ORole) odm.getData().get(0);
            if (!ldapService.checkLdapConnection(loggedUser)) {
                oFR.addError(userMsgService.getUserMessage("LDAPConnectionError", loggedUser));
                logger.trace("LDAP Connection Error");
                logger.debug("Returning");
                return oFR;
            }

            if (!ldapService.checkRoleExistence(role, loggedUser)) {
                oFR.addError(userMsgService.getUserMessage("RoleANotFoundInLDAP",
                        Collections.singletonList(role.getName()), loggedUser
                ), null);
                logger.debug("Returning");
                return oFR;
            }
            if (ldapService.deleteRole(role, loggedUser)) {
                oFR.addSuccess(userMsgService.getUserMessage("LDAPDeletedRole",
                        Collections.singletonList(role.getName()), loggedUser
                ));
            } else {
                oFR.addError(userMsgService.getUserMessage("LDAPRemovedRoleError",
                        Collections.singletonList(role.getName()), loggedUser));

            }
        } catch (Exception ex) {
            // TO-DO: When error in saving the Role, Add handling for the this

            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            oFR.addError(userMsgService.getUserMessage("LDAPRemovedRoleError",
                    Collections.singletonList(role.getName()), loggedUser));
            // </editor-fold>
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult addRoleUserToLDAP(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser
    ) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, RoleUser.class);
        if (inputValidationErrors != null) {
            logger.debug("Returning with: {}", inputValidationErrors);
            return inputValidationErrors;
        }
        // </editor-fold>
        String loginName = null;
        String roleName = null;
        try {
            oem.getEM(loggedUser);
            RoleUser roleUser = (RoleUser) odm.getData().get(0);
            loginName = roleUser.getOuser().getLoginName();
            if (loginName == null) {
                loginName = (String) oem.executeEntityQuery("Select u.loginName From OUser u where u.dbid = "
                        + roleUser.getOuser().getDbid(), loggedUser);
            }
            roleName = roleUser.getOrole().getName();
            if (!ldapService.checkLdapConnection(loggedUser)) {
                oFR.addError(userMsgService.getUserMessage("LDAPConnectionError", loggedUser));
                logger.trace("LDAP Connection Error");
                logger.debug("Returning");
                return oFR;
            }

            if (ldapService.addRoleUser(loginName, roleName, loggedUser)) {
                oFR.addSuccess(userMsgService.getUserMessage("LDAPSavedUserRole",
                        userMsgService.constructListFromStrings(loginName, roleName),
                        loggedUser
                ));
            } else {
                boolean addRole = ldapService.addRole(roleUser.getOrole(), loggedUser);
                boolean addUserRole = false;
                if (addRole) {
                    addUserRole = ldapService.addRoleUser(loginName, roleName, loggedUser);
                }
                if (!(addRole && addUserRole)) {
                    oFR.addError(userMsgService.getUserMessage("LDAPAddUserRoleError",
                            userMsgService.constructListFromStrings(loginName, roleName), loggedUser));
                }

            }
        } catch (Exception ex) {
            // TO-DO: When error in saving the Role, Add handling for the this

            // <editor-fold defaultstate="collapsed" desc="Log">
            oFR.addError(userMsgService.getUserMessage("LDAPAddUserRoleError",
                    userMsgService.constructListFromStrings(loginName, roleName), loggedUser));
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult editRoleUserInLDAP(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser
    ) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, RoleUser.class);
        if (inputValidationErrors != null) {
            logger.debug("Returning: {}", inputValidationErrors);
            return inputValidationErrors;
        }
        // </editor-fold>
        String loginName = null, newLoginName = null, roleName = null, newRoleName = null;
        try {
            oem.getEM(loggedUser);
            RoleUser roleUser = (RoleUser) odm.getData().get(0);
            // load RoleUser from db to know which field is changed, Role or User
            RoleUser roleUserFromDB = (RoleUser) oem.loadEntity("RoleUser", roleUser.getDbid(),
                    null, null, loggedUser);
            if (roleUser.getOrole().getDbid() != roleUserFromDB.getOrole().getDbid()
                    && roleUser.getOuser().getDbid() == roleUserFromDB.getOuser().getDbid()) {
                // Role is changed
                newRoleName = roleUser.getOrole().getName();
                loginName = roleUser.getOuser().getLoginName();
            } else if (roleUser.getOrole().getDbid() == roleUserFromDB.getOrole().getDbid()
                    && roleUser.getOuser().getDbid() != roleUserFromDB.getOuser().getDbid()) {
                // User is changed
                newLoginName = roleUser.getOuser().getLoginName();
                roleName = roleUser.getOrole().getName();
            } else if (roleUser.isInActive() != roleUserFromDB.isInActive()) {
            } else {
                // Weird Case
                logger.debug("Unrecognized case, Only User or Role should have been changed");
                oFR.addError(userMsgService.getUserMessage("SystemInternalError", loggedUser));
                logger.debug("Returning");
                return oFR;
            }

            if (!ldapService.checkLdapConnection(loggedUser)) {
                oFR.addError(userMsgService.getUserMessage("LDAPConnectionError", loggedUser));
                logger.trace("LDAP Connection Error");
                logger.debug("Returning");
                return oFR;
            }
            if (newRoleName != null) {
                if (ldapService.addRoleUser(loginName, newRoleName, loggedUser)) {
                    String oldRolename = roleUserFromDB.getOrole().getName();
                    if (ldapService.deleteRoleUser(loginName, oldRolename, loggedUser)) {
                        oFR.addSuccess(userMsgService.getUserMessage("LDAPSavedUserRole",
                                userMsgService.constructListFromStrings(loginName, roleName), loggedUser));
                    } else {
                        oFR.addError(userMsgService.getUserMessage("DeleteUserFromOldRoleLDAPError", loggedUser));
                    }
                }

            }
        } catch (Exception ex) {
            // TO-DO: When error in saving the Role, Add handling for the this

            // <editor-fold defaultstate="collapsed" desc="Log">
            oFR.addError(userMsgService.getUserMessage("LDAPEditUserRoleError",
                    userMsgService.constructListFromStrings(newLoginName, newRoleName), loggedUser));
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult deleteRoleUserFromLDAP(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser
    ) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, RoleUser.class);
        if (inputValidationErrors != null) {
            logger.debug("Returning with: {}", inputValidationErrors);
            return inputValidationErrors;
        }
        // </editor-fold>
        String loginName = null;
        String roleName = null;
        try {
            oem.getEM(loggedUser);
            RoleUser roleUser = (RoleUser) odm.getData().get(0);
            loginName = roleUser.getOuser().getLoginName();
            roleName = roleUser.getOrole().getName();
            if (!ldapService.checkLdapConnection(loggedUser)) {
                oFR.addError(userMsgService.getUserMessage("LDAPConnectionError", loggedUser));
                logger.trace("LDAP Connection Error");
                logger.debug("Returning");
                return oFR;
            }

            if (ldapService.deleteRoleUser(loginName, roleName, loggedUser)) {
                oFR.addSuccess(userMsgService.getUserMessage("LDAPDeletedUserRole",
                        userMsgService.constructListFromStrings(loginName, roleName),
                        loggedUser));
            } else {
                oFR.addError(userMsgService.getUserMessage("LDAPDeleteUserRoleError",
                        userMsgService.constructListFromStrings(loginName, roleName), loggedUser));
            }

        } catch (Exception ex) {
            // TO-DO: When error in saving the Role, Add handling for the this

            // <editor-fold defaultstate="collapsed" desc="Log">
            oFR.addError(userMsgService.getUserMessage("LDAPDeleteUserRoleError",
                    userMsgService.constructListFromStrings(loginName, roleName), loggedUser));
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    public OFunctionResult validateLoginNameUnchanged(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OUser.class);
        if (inputValidationErrors != null) {
            logger.debug("Returning with: {}", inputValidationErrors);
            return inputValidationErrors;
        }
        // </editor-fold>
        OUser user = null;
        try {
            oem.getEM(loggedUser);
            user = (OUser) odm.getData().get(0);
            OUser loadedUserDB = (OUser) oem.loadEntity("OUser", user.getDbid(),
                    null, null, loggedUser);
            if (!user.getLoginName().equals(loadedUserDB.getLoginName())) {
                oFR.addError(userMsgService.getUserMessage("LoginNameChangedError",
                        Collections.singletonList(loadedUserDB.getLoginName()), loggedUser));
                logger.debug("Returning");
                return oFR;
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: {}", ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult validateRoleNameUnchanged(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, ORole.class);
        if (inputValidationErrors != null) {
            logger.debug("Returning with: {}", inputValidationErrors);
            return inputValidationErrors;
        }
        // </editor-fold>
        ORole role = null;
        try {
            oem.getEM(loggedUser);
            role = (ORole) odm.getData().get(0);
            ORole loadedRoleDB = (ORole) oem.loadEntity("ORole", role.getDbid(),
                    null, null, loggedUser);
            if (!role.getName().equals(loadedRoleDB.getName())) {
                oFR.addError(userMsgService.getUserMessage("RoleNameChangedError",
                        Collections.singletonList(loadedRoleDB.getName()), loggedUser));
                return oFR;
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("returning");
            return oFR;
        }
    }

    private String getPasswordFromLDAP(String loginName, OUser loggedUser) {
        String password = ldapService.getPasswordFromLDAP(loginName, loggedUser);
        return password;
    }

    @Override
    public OFunctionResult userChangePassword(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OUser.class);
        if (inputValidationErrors != null) {
            logger.debug("returning with: {}", inputValidationErrors);
            return inputValidationErrors;
        }
        // </editor-fold>
        OUser user = null;
        try {
            user = (OUser) odm.getData().get(0);
            String currentPass = user.getCurrentPass();
            String newPass = user.getNewPass();
            String confirmPass = user.getConfirmPass();
            if (currentPass == null) {
                logger.debug("Returning as currentPassword equals Null");
                return oFR;
            }
            OUser userFromDB = (OUser) oem.loadEntity("OUser", user.getDbid(),
                    Collections.singletonList(OEntityManager.CONFCOND_GET_ACTIVEANDINACTIVE),
                    null, loggedUser);

            // If new and old don't match, return
            if (!ldapService.checkLdapConnection(loggedUser)) {
                oFR.addError(userMsgService.getUserMessage("LDAPConnectionError", loggedUser));
                logger.trace("LDAP Connection Error");
                logger.debug("Returning");
                return oFR;
            }
            String oldpass = getPasswordFromLDAP(user.getLoginName(), loggedUser);
            if (oldpass == null) {
                oldpass = userFromDB.getPassword();
            }

            if (!isPasswordAsCurrentPasswordInLiferay(user)) {
                oFR.addError(userMsgService.getUserMessage("NewAndCurrentPasswordsNoMatch", loggedUser));
                logger.debug("Returning as New And Current Passwords Not Matched");
                return oFR;
            }

            if (!confirmPass.equals(newPass)) {
                oFR.addError(userMsgService.getUserMessage("NewAndConfirmPasswordsNoMatch", loggedUser));
                logger.debug("Returning as New And Confirm Passwords Not Matched");
                return oFR;
            }

            // Save in FABS db
            user.setPassword(newPass);//(encryptor.encrypt(newPass));
            ODataMessage returnedDataMessage = new ODataMessage();
            List data = new ArrayList();
            data.add(user);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            // <editor-fold defaultstate="collapsed" desc="Log">
            if (ex.getCause().getClass().equals(NameAlreadyBoundException.class)) {
                oFR.addError(userMsgService.getUserMessage("LDAPUserAlreadyFound",
                        Collections.singletonList(user.getLoginName()), loggedUser));
            } else {
                oFR.addError(userMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
            }
            // </editor-fold>
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult validateUserExistsInCent(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OUser.class
        );
        if (inputValidationErrors
                != null) {
            logger.debug("Returning with: {}", inputValidationErrors);
            return inputValidationErrors;
        }
        // </editor-fold>
        OUser user = null;

        try {
            oem.getEM(loggedUser);
            user = (OUser) odm.getData().get(0);

            // If loginname already exists in FABSCent, skip adding it
            if (centralEM.getUserTenant(user.getLoginName()) != null) {
                // It exists
                oFR.addError(userMsgService.getUserMessage("UserExistsInCent",
                        Collections.singletonList(user.getLoginName()), loggedUser));
                logger.debug("Returning");
                return oFR;
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(userMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            oem.closeEM(user);
        }

        logger.debug(
                "Returning");
        return oFR;
    }

    @Override
    public OFunctionResult parseUserExpression(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        String filterFieldExpression = null;
        try {
            String value = params.getParams().get("filterFieldExpression").toString();
            String returnedExpression = "";
            if (value.startsWith("{")) {
                value = value.substring(2, value.length() - 1);
            }
            if (value.startsWith("loggedUser") || value.startsWith("loggeduser")) {
                value = value.substring(value.indexOf(".") + 1);
                returnedExpression = loggedUser.invokeGetter(value).toString();
            }
            ODataMessage returnedDM = new ODataMessage();
            List data = new ArrayList();
            data.add(returnedExpression);
            returnedDM.setData(data);
            oFR.setReturnedDataMessage(returnedDM);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(userMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public String getUserDisplayedName(String loginName) {
        logger.debug("Entering");
        try {
            OUser loggedUser = oem.getUserForLoginName(loginName);
            if (loggedUser != null) {
                logger.debug("Returning");
                return loggedUser.getDisplayName() == null || loggedUser.getDisplayName().trim().equals("") ? loginName : loggedUser.getDisplayName();
            }
        } catch (Exception ex) {
            logger.error("exception thrown: ", ex);
        }
        logger.debug("Returning with Login Name: {}", loginName);
        return loginName;
    }

    @Override
    public OUserDTO getUserDTO(String loginName, OUser loggedUser) {
        logger.debug("Entering");
        OUserDTO udto = new OUserDTO();
        udto.setLoginName(loginName);
        OTenant userTenant = centralEM.getUserTenant(loginName);
        if (userTenant != null) {
            udto.setTenantID(userTenant.getId());
        }
        try {
            Object name;
            if (udto.getTenantID() == loggedUser.getTenant().getId()) {
                name = oem.executeEntityQuery("Select o.displayName From OUser o where o.loginName = '" + loginName + "'", loggedUser);
            } else {
                name = getUserDisplayedName(loginName);
            }
            if (name != null && !name.toString().equals("")) {
                udto.setDisplayName(name.toString());
            } else {
                udto.setDisplayName(loginName);
            }
        } catch (Exception ex) {
            udto.setDisplayName(loginName);
            logger.error("Exception thrown: ", ex);
        }
        logger.debug("Returning");
        return udto;
    }

    public OFunctionResult updateUser(OUser ouser, OUser lDapUser, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        // Update LDAP attributes with OUser
        ouser.setEmail(lDapUser.getEmail());
        ouser.setPassword(lDapUser.getPassword());
        ouser.setInActive(false);
        oFR.append(entitySetupService.callEntityUpdateAction(ouser, loggedUser));
        logger.debug("Returning");
        return oFR;
    }

    public OFunctionResult addUser(OUser ouser, OUser loggedUser, OModule systemModule) {
        logger.debug("Entering");
        // New LDAP user
        OFunctionResult oFR = new OFunctionResult();
        OUser newUser = new OUser();
        newUser.setLoginName(ouser.getLoginName());
        newUser.setEmail(ouser.getEmail());
        newUser.setPassword(ouser.getPassword());
        newUser.setFirstLanguage(loggedUser.getFirstLanguage());
        newUser.setOmodule(systemModule);
        newUser.setUserPrefrence1(loggedUser.getUserPrefrence1());
        newUser.setSecondLanguage(loggedUser.getSecondLanguage());
        oFR.append(entitySetupService.callEntityCreateAction(newUser, loggedUser));
        logger.debug("Returning");

        if (oFR.getErrors().size() == 0) {
            List<RoleUser> roleUsers = ouser.getRoleUsers();
            if (roleUsers != null) {
                for (RoleUser roleUser : roleUsers) {
                    roleUser.setOuser(newUser);
                    oFR.append(entitySetupService.callEntityCreateAction(roleUser, loggedUser));
                }
            }
        }

        return oFR;
    }

    public OUser checkIfUserExists(String userName, OUser loggedUser) throws EntityNotFoundException, RollbackException, EntityExistsException, NonUniqueResultException, UserNotAuthorizedException, NoResultException, TransactionRequiredException, OptimisticLockException {
        logger.debug("Entering");
        List<String> conditions = new ArrayList<String>();
        conditions.add("loginName = '" + userName + "'");
        conditions.add(OEntityManager.CONFCOND_GET_ACTIVEANDINACTIVE);
        OUser ouser = new OUser();
        ouser = (OUser) oem.loadEntity("OUser", conditions, null, loggedUser);
        if (null != ouser) {
            logger.debug("Returning with Ouser: {}", ouser.getLoginName());
        }
        return ouser;
    }

    @EJB
    private OCentralEntityManagerRemote centralOEM;

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    @Override
    @PermitAll
    public OUser getUserfromLoginNameAndTenant(String loginName) {
        logger.debug("Entering");
        OTenant loginNameTenant = null;
        OUser loggedUser = null;
        OUser tenantGrantedUser = null;
        try {
            //loggedUser = cachedLoggedUsers.get(loginName);
            if (loggedUser == null) {
                loginNameTenant = centralOEM.getUserTenant(loginName);
                if (loginNameTenant == null) {
                    // FIXME: display corresponding user error message
                    logger.debug("Returning with Null");
                    return null;
                }
                tenantGrantedUser = centralOEM.getGrantedUser(loginNameTenant);
                tenantGrantedUser.setLoginName(loginName);

                oem.getEM(tenantGrantedUser);
                loggedUser = oem.getUser(loginName, loginNameTenant);
                if (loggedUser == null) {
                    logger.debug("Returning with Null as logged user is Null");
                    return null;
                }
                cachedLoggedUsers.put(loginName, loggedUser);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.debug("Returning with Null");
            return null;
        } finally {
            logger.debug("Returnin with Logged user");
            oem.closeEM(tenantGrantedUser);
            return loggedUser;
        }
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    @Override
    public OUser getLoggedInUser(String loginName) {
        logger.debug("Entering");
        OUser loggedUser = null;
        OTenant loginNameTenant = null;
        OUser tenantGrantedUser = centralOEM.getGrantedUser(loginNameTenant);
        try {
            loginNameTenant = centralOEM.getUserTenant(loginName);
            oem.getEM(tenantGrantedUser);
            loggedUser = oem.getUser(loginName, loginNameTenant);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {
            oem.closeEM(tenantGrantedUser);
            logger.debug("Returnin with Logged user");
            return loggedUser;
        }

    }

    @Override
    public OFunctionResult addUserToCent(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OUser.class
        );
        if (inputValidationErrors
                != null) {
            return inputValidationErrors;
        }
        // </editor-fold>
        OUser user = null;

        try {
            user = (OUser) odm.getData().get(0);
            if (user.getTenant() == null) {
                user.setTenant(loggedUser.getTenant());
            }

            // If User is not found in Cent, Add it:
            if (centralEM.getUserTenant(user.getLoginName()) == null) {
                centralEM.addOCentralUser(user);
            } else {
                oFR.addError(userMsgService.getUserMessage("UserAlreadyFoundInCent",
                        Collections.singletonList(user.getLoginName()), loggedUser));
            }
        } catch (Exception ex) {
            // Rollback FABS and FABSCent
            oFR.addError(userMsgService.getUserMessage("CentAddUserError",
                    Collections.singletonList(user.getLoginName()), loggedUser), ex);
        } finally {
            oem.closeEM(loggedUser);
            return oFR;
        }
    }

    @Override
    public String getPasswordForUserFromLDAP(OUser loggedUser) {
        if (loggedUser != null) {
            return getPasswordFromLDAP(loggedUser.getLoginName(), loggedUser);
        } else {
            return null;
        }
    }

    public List<String> getUserRoles(OUser user) {
        Set<String> userRoles = new HashSet<>();
        List<RoleUser> roleUsers = user.getRoleUsers();
        for (RoleUser roleUser : roleUsers) {
            userRoles.add(roleUser.getOrole().getName());
        }
        return new ArrayList<String>(userRoles);
    }

    @Override
    public OFunctionResult validateUserNameWithRole(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            OUser user = (OUser) odm.getData().get(0);
            String userName = user.getLoginName();
            String sql = "select count(dbid) from ORole where name = '" + userName + "'";
            int count = (int) oem.executeEntityNativeQuery(sql, loggedUser);
            if (count != 0) {
                ofr.addError(userMsgService.getUserMessage("UserName_001", loggedUser));
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

    @Override
    public OFunctionResult validateRoleNameWithUser(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            ORole role = (ORole) odm.getData().get(0);
            String roleName = role.getName();
            String sql = "select count(dbid) from OUser where loginname = '" + roleName + "'";
            int count = (int) oem.executeEntityNativeQuery(sql, loggedUser);
            if (count != 0) {
                ofr.addError(userMsgService.getUserMessage("RoleName_001", loggedUser));
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

    private boolean isPasswordAsCurrentPasswordInLiferay(OUser user) {
        try {
            long _userId = UserLocalServiceUtil.getUserIdByScreenName(PortalUtil.getDefaultCompanyId(), user.getLoginName());
            return PasswordTrackerLocalServiceUtil.isSameAsCurrentPassword(_userId, user.getCurrentPass());
        } catch (PortalException | SystemException ex) {
            logger.error(ex.getLocalizedMessage());
        }

        return false;
    }

}
