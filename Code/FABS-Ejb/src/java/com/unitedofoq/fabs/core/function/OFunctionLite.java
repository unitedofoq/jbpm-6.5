/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.function;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.ReadOnly;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;

/**
 *
 * @author mibrahim
 */
@Entity
@ReadOnly
@Table(name="OFunction")
public class OFunctionLite extends BaseEntity{
    @Column(name="name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
