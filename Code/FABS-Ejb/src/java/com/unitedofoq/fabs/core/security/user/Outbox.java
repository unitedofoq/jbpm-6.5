/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.security.user;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;


/**
 *
 * @author ahussien
 */
@Entity
public class Outbox extends BaseEntity{
    private boolean process;
    private String title;
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date actionDate;
    private String processName;
    private long entityDBID;
    private String processInstanceId;
    private String entityName;
    private String actionTaken;
    private int functionDBID;    
    @ManyToOne
    private OUser user;
    private String initiator;
    private String initiatorLoginName;

    public String getInitiatorLoginName() {
        return initiatorLoginName;
    }

    public String getInitiatorLoginNameDD() {
        return "Outbox_initiatorLoginName";
    }

    
    public void setInitiatorLoginName(String initiatorLoginName) {
        this.initiatorLoginName = initiatorLoginName;
    }

    
    
    public String getInitiator() {
        return initiator;
    }
    
    public String getInitiatorDD() {
        return "Outbox_initiator";
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    private String taskID;

    public String getTaskID() {
        return taskID;
    }

    public void setTaskID(String taskID) {
        this.taskID = taskID;
    }
 
    public boolean isProcess() {
        return process;
    }

    public void setProcess(boolean process) {
        this.process = process;
    }

    public String getProcessDD() {
        return "Outbox_process";
    }

    public String getStartDateDD() {
        return "Outbox_startDate";
    }

    public String getEndDateDD() {
        return "Outbox_endDate";
    }

    public String getStateDD() {
        return "Outbox_state";
    }

    public String getTaskIDDD() {
        return "Outbox_taskID";
    }

    public String getTitleDD() {
        return "Outbox_title";
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }
    
    public String getActionTaken() {
        return actionTaken;
    }
    
    public String getActionTakenDD() {
        return "Outbox_actionTaken";
    }
    
    public long getEntityDBID() {
        return entityDBID;
    }
    
    public void setEntityDBID(long entityDBID) {
        this.entityDBID = entityDBID;
    }
    
    public String getEntityName() {
        return entityName;
    }
    
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }
    
    public int getFunctionDBID() {
        return functionDBID;
    }
    
    public void setFunctionDBID(int functionDBID) {
        this.functionDBID = functionDBID;
    }
    
    public OUser getUser() {
        return user;
    }
    
    public String getUserDD() {
        return "Outbox_user";
    }
    
    public void setUser(OUser user) {
        this.user = user;
    }
    
    public String getProcessName() {
        return processName;
    }
    
    public void setProcessName(String processName) {
        this.processName = processName;
    }
    
    public Date getStartDate() {
        return startDate;
    }
    
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    
    public Date getEndDate() {
        return endDate;
    }
    
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    public String getProcessInstanceIdDD() {
        return "Outbox_processInstanceId";
    }
    
    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }
    
}