/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.filter;

/**
 *
 * @author MEA
 */
public class FilterFieldDTO {
    private long dbid;
    private long filterDBID;
    private String value;
    private String fieldExpression;
    private boolean fromTo;
    private int fieldIndex;
    private String fieldvalueTo;
    private String operator;
    private long fieldDdDBID;

    public FilterFieldDTO() {
    }

    public FilterFieldDTO(long dbid, 
            long filterDBID, 
            String value, 
            String fieldExpression, 
            boolean fromTo, 
            int fieldIndex, 
            String fieldvalueTo, 
            String operator,
            long fieldDdDBID) {
        this.dbid = dbid;
        this.filterDBID = filterDBID;
        this.value = value;
        this.fieldExpression = fieldExpression;
        this.fromTo = fromTo;
        this.fieldIndex = fieldIndex;
        this.fieldvalueTo = fieldvalueTo;
        this.operator = operator;
        this.fieldDdDBID=fieldDdDBID;
    }

    public long getDbid() {
        return dbid;
    }

    public void setDbid(long dbid) {
        this.dbid = dbid;
    }

    public long getFilterDBID() {
        return filterDBID;
    }

    public void setFilterDBID(long filterDBID) {
        this.filterDBID = filterDBID;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getFieldExpression() {
        return fieldExpression;
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }

    public boolean isFromTo() {
        return fromTo;
    }

    public void setFromTo(boolean fromTo) {
        this.fromTo = fromTo;
    }

    public int getFieldIndex() {
        return fieldIndex;
    }

    public void setFieldIndex(int fieldIndex) {
        this.fieldIndex = fieldIndex;
    }

    public String getFieldvalueTo() {
        return fieldvalueTo;
    }

    public void setFieldvalueTo(String fieldvalueTo) {
        this.fieldvalueTo = fieldvalueTo;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public long getFieldDdDBID() {
        return fieldDdDBID;
    }

    public void setFieldDdDBID(long fieldDdDBID) {
        this.fieldDdDBID = fieldDdDBID;
    }

}

