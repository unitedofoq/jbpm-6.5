/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.dd.dto;



/**
 *
 * @author root
 */
public class DDDTO {
    // <editor-fold defaultstate="collapsed" desc="Control Types">
    public final static int CT_LABEL = 5;
    public final static int CT_TEXTFIELD = 6;
    public final static int CT_TEXTAREA = 7;
    public final static int CT_LINK = 133959;
    public final static int CT_CHECKBOX = 8;
    public final static int CT_URLLINK = 1430357;
    public final static int CT_COMMANDLINK = -1;
    public final static int CT_RADIOBUTTON = 9;
    public final static int CT_CALENDAR = 11;
    public final static int CT_FILEBROWSER = 12;
    public final static int CT_LOOKUPSCREEN = 13;
    public final static int CT_LOOKUPMULTILEVEL = 6003;
    public final static int CT_LOOKUPATTRIBUTEBROWSER = 6000;
    public final static int CT_LOOKUPTREE = 1027665;    
    public final static int CT_DROPDOWN = 6001;
    public final static int CT_LITERALDROPDOWN = 6002;
    public final static int CT_TABLE = 28;
    public final static int CT_MULTISELECTIONLIST = 6013;
    public final static int CT_FILEUPLOAD = 132322;
    public final static int CT_FILEDOWNLOAD = 132323;
    public final static int CT_FILE = 132329;
    public final static int CT_RICHTEXT = 1027763;
    public final static int CT_IMAGE = 1028031;
    public final static int CT_MULTISELECTMENU = 1028401;
    public final static int CT_PASSWORD = 1029355;

    public enum DDLookupType {

        DDLT_DRPDWN, DDLT_SCREEN, DDLT_FIELDEXP, DDLT_LTRLDRPDN, DDLT_MLTILVLSCRN, DDLT_NONE, DDLT_MULTISELECTIONLIST,
        DDLT_FLTRBLDR, DDLT_TREE
    };
    // </editor-fold>   
    private long dbid;
    private String defaultValue;
    private long controlTypeDBID;
    private int controlSize;
    private String name;
    private String label;
    private boolean balloon;
    private String lookupDropDownDisplayField;
    private String inlineHelp;
    private int decimalMask;
    private String header;
    private long calendarPatternDBID;
    private String lookupFilter;
    private long lookupScreenDBID;
    private boolean multiSelection;
    private boolean usedInReport;
    private boolean omoduleDBID;

    public long getControlTypeDBID() {
        return controlTypeDBID;
    }

    public void setControlTypeDBID(long controlTypeDBID) {
        this.controlTypeDBID = controlTypeDBID;
    }

    public long getCalendarPatternDBID() {
        return calendarPatternDBID;
    }

    public void setCalendarPatternDBID(long calendarPatternDBID) {
        this.calendarPatternDBID = calendarPatternDBID;
    }

    public boolean isMultiSelection() {
        return multiSelection;
    }

    public void setMultiSelection(boolean multiSelection) {
        this.multiSelection = multiSelection;
    }

    public boolean isUsedInReport() {
        return usedInReport;
    }

    public void setUsedInReport(boolean usedInReport) {
        this.usedInReport = usedInReport;
    }

    public boolean isOmoduleDBID() {
        return omoduleDBID;
    }

    public void setOmoduleDBID(boolean omoduleDBID) {
        this.omoduleDBID = omoduleDBID;
    }

    
    public long getLookupScreenDBID() {
        return lookupScreenDBID;
    }

    public void setLookupScreenDBID(long lookupScreenDBID) {
        this.lookupScreenDBID = lookupScreenDBID;
    }

    public String getLookupFilter() {
        return lookupFilter;
    }

    public void setLookupFilter(String lookupFilter) {
        this.lookupFilter = lookupFilter;
    }    

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public int getDecimalMask() {
        return decimalMask;
    }

    public void setDecimalMask(int decimalMask) {
        this.decimalMask = decimalMask;
    }

    public String getInlineHelp() {
        return inlineHelp;
    }

    public void setInlineHelp(String inlineHelp) {
        this.inlineHelp = inlineHelp;
    }

    public String getLookupDropDownDisplayField() {
        return lookupDropDownDisplayField;
    }

    public void setLookupDropDownDisplayField(String lookupDropDownDisplayField) {
        this.lookupDropDownDisplayField = lookupDropDownDisplayField;
    }

    public long getDbid() {
        return dbid;
    }

    public void setDbid(long dbid) {
        this.dbid = dbid;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }


    public int getControlSize() {
        return controlSize;
    }

    public void setControlSize(int controlSize) {
        this.controlSize = controlSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isBalloon() {
        return balloon;
    }

    public void setBalloon(boolean balloon) {
        this.balloon = balloon;
    }

    public DDLookupType getLookupType() {
        if (controlTypeDBID == CT_LOOKUPSCREEN) {
            return DDLookupType.DDLT_SCREEN;
        }
        if (controlTypeDBID == CT_LOOKUPMULTILEVEL) {
            return DDLookupType.DDLT_MLTILVLSCRN;
        }
        if (controlTypeDBID == CT_DROPDOWN) {
            return DDLookupType.DDLT_DRPDWN;
        }
        if (controlTypeDBID == CT_LITERALDROPDOWN) {
            return DDLookupType.DDLT_LTRLDRPDN;
        }
        if (controlTypeDBID == CT_LOOKUPATTRIBUTEBROWSER) {
            return DDLookupType.DDLT_FIELDEXP;
        }
        if (controlTypeDBID == CT_MULTISELECTIONLIST) {
            return DDLookupType.DDLT_MULTISELECTIONLIST;
        }
        //if(controlType == CT_LOOKUPFILTERBUILDER) return DDLookupType.DDLT_FLTRBLDR;
        if (controlTypeDBID == CT_LOOKUPTREE) {
            return DDLookupType.DDLT_TREE;
        }

        return DDLookupType.DDLT_NONE;
    }
}
