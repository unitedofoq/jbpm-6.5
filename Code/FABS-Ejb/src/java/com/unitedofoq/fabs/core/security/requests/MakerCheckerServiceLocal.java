/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.security.requests;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

/**
 *
 * @author lap
 */
@Local
public interface MakerCheckerServiceLocal {

  public OFunctionResult validateRequest(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

  public OFunctionResult makerCheckerRequestInit(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

  public OFunctionResult deletedRequestAction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

  public OFunctionResult rejectRequest(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

  public OFunctionResult approveRequest(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
  
  public OFunctionResult validateUinqunessEmail(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

  public OFunctionResult validateUinqunessLoginname(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

  public OFunctionResult validateArabicLanguageWithRTL(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

}
