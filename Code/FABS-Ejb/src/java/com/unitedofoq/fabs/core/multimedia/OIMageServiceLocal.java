/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.multimedia;

import java.io.File;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.data.PersonPhoto;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author nkhalil
 */
@Local
public interface OIMageServiceLocal {
    public OFunctionResult saveImage(File file, String fileName, String contentType, OUser loggedUser);
    public OFunctionResult savePersonPhoto(File file, String fileName, String contentType, OUser loggedUser);
    public String getImagePath(String fileName, OUser loggedUser);
    public String getPersonImagePath(PersonPhoto personPhoto, OUser loggedUser);
    public java.lang.String getFilePath(byte[] fileBytes, String fileName, OUser loggedUser);
    
}
