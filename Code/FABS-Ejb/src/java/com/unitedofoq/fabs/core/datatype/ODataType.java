package com.unitedofoq.fabs.core.datatype;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.module.OModule;

@Entity
@ChildEntity(fields = {"oDataTypeAttribute"})
@FABSEntitySpecs(indicatorFieldExp="name")
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class ODataType extends ObjectBaseEntity{
    public static final long DTDBID_VOID =  1 ;
    public static final long DTDBID_DBID =  3 ;
    public static final long DTDBID_PARENTDBID =  8 ;
    public static final long DTDBID_DBIDLIST =  9 ;
    public static final long MAILMERGE_DBID=1448487;

    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OModule omodule;

    public String getOmoduleDD(){
        return "ODataType_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="name">
    @Column(nullable=false, unique=true)
    private String name;
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    public String getNameDD(){
        return "ODataType_name" ;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="oDataTypeAttribute">
    @OneToMany(mappedBy = "oDataType")
    private List<ODataTypeAttribute> oDataTypeAttribute;
    /**
     * @return the oDataTypeAttribute
     */
    public List<ODataTypeAttribute> getODataTypeAttribute() {
        return oDataTypeAttribute;
    }
    /**
     * @param oDataTypeAttribute the oDataTypeAttribute to set
     */
    public void setODataTypeAttribute(List<ODataTypeAttribute> oDataTypeAttribute) {
        this.oDataTypeAttribute = oDataTypeAttribute;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="description">
    @Translation(originalField="description")
    @Transient
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    @Translatable(translationField="descriptionTranslated")
    private String description;
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD(){
        return "ODataType_description";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Static Values">
    static final long DT_VOID_DBID = 1 ;
    static final long DT_DBID = 3 ;
    static final long DT_PARENT_DBID = 8 ;
    static final long DT_DBID_LIST = 9 ;
    // </editor-fold>
}
