/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.multitanantuser;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author MEA
 */
@Entity
public class UserTenantRole implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="DBID">
    @Id
    private long dbid;

    public long getDbid() {
        return dbid;
    }

    public void setDbid(long dbid) {
        this.dbid = dbid;
    }

    public String getDbidDD() {
        return "UserTenantRole_dbid";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="User">
    private String user;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUserDD() {
        return "UserTenantRole_user";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Roles">
    private String roles;

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String getgetRolesDD() {
        return "UserTenantRole_roles";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Tenant">
    private String tenant;

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public String getTenantDD() {
        return "UserTenantRole_tenant";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="inActive">

    protected boolean active = false;

    public String getActiveDD() {
        return "UserTenantRole_active";
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    //</editor-fold>
}