package com.unitedofoq.fabs.core.report.customReports;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = {"customReportFilter"})
public class CustRepFilterFld extends BaseEntity {

    private String fieldName;
    private String operator;
    private String val1;
    private String val2;
    private int sortIndex;
    private String rowOperator;
    private String leftBrackets;
    private String rightBrackets;

    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private DD val1Dd;

    private boolean mandatory;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    @JoinColumn(name = "customreportfilter_DBID")
    private CustomReportFilter customReportFilter;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    @JoinColumn(name = "customreportdynamicfilter_DBID")
    private CustomReportDynamicFilter customReportDynamicFilter;

    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private OEntity val1Oentity;

    private String val1FieldExpression;

    public CustomReportDynamicFilter getCustomReportDynamicFilter() {
        return customReportDynamicFilter;
    }

    public void setCustomReportDynamicFilter(CustomReportDynamicFilter customReportDynamicFilter) {
        this.customReportDynamicFilter = customReportDynamicFilter;
    }

    public String getCustomReportDynamicFilterDD() {
        return "CustomReportFilterField_customReportDynamicFilter";
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldNameDD() {
        return "CustomReportFilterFields_fieldName";
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getOperatorDD() {
        return "CustomReportFilterFields_operator";
    }

    public String getVal1() {
        return val1;
    }

    public void setVal1(String val1) {
        this.val1 = val1;
    }

    public String getVal1DD() {
        return "CustomReportFilterFields_val1";
    }

    public String getVal2() {
        return val2;
    }

    public void setVal2(String val2) {
        this.val2 = val2;
    }

    public String getVal2DD() {
        return "CustomReportFilterFields_val2";
    }

    public DD getVal1Dd() {
        return val1Dd;
    }

    public void setVal1Dd(DD val1Dd) {
        this.val1Dd = val1Dd;
    }

    public String getVal1DdDD() {
        return "CustomReportFilterFields_val1Dd";
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    public String getMandatoryDD() {
        return "CustomReportFilterFields_mandatory";
    }

    public CustomReportFilter getCustomReportFilter() {
        return customReportFilter;
    }

    public void setCustomReportFilter(CustomReportFilter customReportFilter) {
        this.customReportFilter = customReportFilter;
    }

    public String getCustomReportFilterDD() {
        return "CustomReportFilterFields_customReportFilter";
    }

    public OEntity getVal1Oentity() {
        return val1Oentity;
    }

    public void setVal1Oentity(OEntity val1Oentity) {
        this.val1Oentity = val1Oentity;
    }

    public String getVal1OentityDD() {
        return "CustomReportFilterFields_val1Oentity";
    }

    public String getVal1FieldExpression() {
        return val1FieldExpression;
    }

    public void setVal1FieldExpression(String val1FieldExpression) {
        this.val1FieldExpression = val1FieldExpression;
    }

    public String getVal1FieldExpressionDD() {
        return "CustomReportFilterFields_val1FieldExpression";
    }

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getSortIndexDD() {
        return "CustomReportFilterFields_sortIndex";
    }

    @Translatable(translationField = "titleTranslated")
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Transient
    @Translation(originalField = "title")
    private String titleTranslated;

    public String getTitleTranslated() {
        return titleTranslated;
    }

    public void setTitleTranslated(String titleTranslated) {
        this.titleTranslated = titleTranslated;
    }

    public String getTitleTranslatedDD() {
        return "CustomReportFilterFields_title";
    }

    public String getHeader() {
        if (title == null || title.isEmpty()) {
            return fieldName;
        }
        return title;
    }

    public String getRowOperator() {
        return rowOperator;
    }

    public void setRowOperator(String rowOperator) {
        this.rowOperator = rowOperator;
    }

    public String getRowOperatorDD() {
        return "CustomReportFilterField_rowOperator";
    }

    public String getLeftBrackets() {
        return leftBrackets;
    }

    public void setLeftBrackets(String leftBrackets) {
        this.leftBrackets = leftBrackets;
    }

    public String getLeftBracketsDD() {
        return "CustomReportFilterField_leftBrackets";
    }

    public String getRightBrackets() {
        return rightBrackets;
    }

    public void setRightBrackets(String rightBrackets) {
        this.rightBrackets = rightBrackets;
    }

    public String getRightBracketsDD() {
        return "CustomReportFilterField_rightBrackets";
    }
}
