/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen.dto;

/**
 *
 * @author mostafa
 */
public class ScreenDTMappingAttrDTO {

    public ScreenDTMappingAttrDTO(String fieldExpression, String attributeExpression,
            boolean forFilter, boolean forInitialization, String odataTypeAttributeDescription,
            long odataTypeAttributeDBID, boolean inActive, boolean partOfCompKey) {
        this.fieldExpression = fieldExpression;
        this.attributeExpression = attributeExpression;
        this.forFilter = forFilter;
        this.forInitialization = forInitialization;
        this.inActive = inActive;
        this.partOfCompKey = partOfCompKey;
        this.odataTypeAttributeDBID = odataTypeAttributeDBID;
        this.odataTypeAttributeDescription = odataTypeAttributeDescription;
    }

    public ScreenDTMappingAttrDTO(String fieldExpression, String attributeExpression,
            boolean forFilter, boolean forInitialization, boolean inActive, boolean partOfCompKey) {
        this.fieldExpression = fieldExpression;
        this.attributeExpression = attributeExpression;
        this.forFilter = forFilter;
        this.forInitialization = forInitialization;
        this.inActive = inActive;
        this.partOfCompKey = partOfCompKey;
    }

    public ScreenDTMappingAttrDTO(long odataTypeAttributeDBID, String odataTypeAttributeDescription) {
        this.odataTypeAttributeDBID = odataTypeAttributeDBID;
        this.odataTypeAttributeDescription = odataTypeAttributeDescription;
    }

    public ScreenDTMappingAttrDTO(long odataTypeAttributeDBID) {
        this.odataTypeAttributeDBID = odataTypeAttributeDBID;
    }

    public ScreenDTMappingAttrDTO() {
    }
    private boolean forFilter;
    private boolean forInitialization;
    private String fieldExpression;
    private String attributeExpression;
    private long odataTypeAttributeDBID;
    private String odataTypeAttributeDescription;
    private boolean inActive;
    private boolean partOfCompKey;

    /**
     * @return the forFilter
     */
    public boolean isForFilter() {
        return forFilter;
    }

    /**
     * @param forFilter the forFilter to set
     */
    public void setForFilter(boolean forFilter) {
        this.forFilter = forFilter;
    }

    /**
     * @return the forInitialization
     */
    public boolean isForInitialization() {
        return forInitialization;
    }

    /**
     * @param forInitialization the forInitialization to set
     */
    public void setForInitialization(boolean forInitialization) {
        this.forInitialization = forInitialization;
    }

    /**
     * @return the fieldExpression
     */
    public String getFieldExpression() {
        return fieldExpression;
    }

    /**
     * @param fieldExpression the fieldExpression to set
     */
    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }

    /**
     * @return the attributeExpression
     */
    public String getAttributeExpression() {
        return attributeExpression;
    }

    /**
     * @param attributeExpression the attributeExpression to set
     */
    public void setAttributeExpression(String attributeExpression) {
        this.attributeExpression = attributeExpression;
    }

    /**
     * @return the odataTypeAttributeDBID
     */
    public long getOdataTypeAttributeDBID() {
        return odataTypeAttributeDBID;
    }

    /**
     * @param odataTypeAttributeDBID the odataTypeAttributeDBID to set
     */
    public void setOdataTypeAttributeDBID(long odataTypeAttributeDBID) {
        this.odataTypeAttributeDBID = odataTypeAttributeDBID;
    }

    /**
     * @return the odataTypeAttributeDescription
     */
    public String getOdataTypeAttributeDescription() {
        return odataTypeAttributeDescription;
    }

    /**
     * @param odataTypeAttributeDescription the odataTypeAttributeDescription to
     * set
     */
    public void setOdataTypeAttributeDescription(String odataTypeAttributeDescription) {
        this.odataTypeAttributeDescription = odataTypeAttributeDescription;
    }

    /**
     * @return the inActive
     */
    public boolean isInActive() {
        return inActive;
    }

    /**
     * @param inActive the inActive to set
     */
    public void setInActive(boolean inActive) {
        this.inActive = inActive;
    }

    /**
     * @return the partOfCompKey
     */
    public boolean isPartOfCompKey() {
        return partOfCompKey;
    }

    /**
     * @param partOfCompKey the partOfCompKey to set
     */
    public void setPartOfCompKey(boolean partOfCompKey) {
        this.partOfCompKey = partOfCompKey;
    }
}
