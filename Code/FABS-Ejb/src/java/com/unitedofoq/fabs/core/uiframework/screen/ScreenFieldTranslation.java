
package com.unitedofoq.fabs.core.uiframework.screen;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class ScreenFieldTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="ddTitleOverride">
    @Column
    private String ddTitleOverride;

    public void setDdTitleOverride(String ddTitleOverride) {
        this.ddTitleOverride = ddTitleOverride;
    }

    public String getDdTitleOverride() {
        return ddTitleOverride;
    }
    // </editor-fold>

}
