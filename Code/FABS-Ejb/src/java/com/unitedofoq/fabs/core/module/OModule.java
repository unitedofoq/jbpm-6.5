/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.module;

import com.unitedofoq.fabs.core.entitybase.DBIDGeneration;
import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

/**
 *
 * @author arezk
 */
@Entity
public class OModule extends ObjectBaseEntity {

    @Translatable(translationField = "moduleTitleTranslated")
    @Column(nullable = false)
    private String moduleTitle;
    @Translation(originalField = "moduleTitle")
    @Transient
    private String moduleTitleTranslated;

    public String getModuleTitleTranslatedDD() {
        return "OModule_moduleTitle";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private UDC moduleType;

    public UDC getModuleType() {
        return moduleType;
    }

    public void setModuleType(UDC moduleType) {
        this.moduleType = moduleType;
    }

    public String getModuleTypeDD() {
        return "OModule_moduleType";
    }
    @OneToMany(mappedBy = "omodule")
    private List<OEntity> oentity;

    public String getOentityDD() {
        return "OModule_oentity";
    }

    public String getDbidGenerationDD() {
        return "OModule_dbidGeneration";
    }

    public List<OEntity> getOentity() {
        return oentity;
    }

    public void setOentity(List<OEntity> oentity) {
        this.oentity = oentity;
    }

    public String getModuleTitle() {
        return moduleTitle;
    }

    public void setModuleTitle(String moduleTitle) {
        this.moduleTitle = moduleTitle;
    }

    public String getModuleTitleTranslated() {
        return moduleTitleTranslated;
    }

    public void setModuleTitleTranslated(String moduleTitleTranslated) {
        this.moduleTitleTranslated = moduleTitleTranslated;
    }
    @OneToOne(cascade = {CascadeType.ALL}, mappedBy = "module")
    @JoinColumn(nullable = false)
    private DBIDGeneration dbidGeneration;

    public DBIDGeneration getDbidGeneration() {
        return dbidGeneration;
    }

    public void setDbidGeneration(DBIDGeneration dbidGeneration) {
        this.dbidGeneration = dbidGeneration;
    }

    public boolean dbidIsInRange(long dbid) {
        return (dbid >= dbidGeneration.getDbidRangeStart() && dbid < dbidGeneration.getDbidRangeEnd());
    }

    @Override
    public void PrePersist() {
        super.PrePersist();
        if (dbidGeneration != null) {
            if (dbidGeneration.getModule() == null) {
                dbidGeneration.setModule(this);
            }
        }
    }
}
