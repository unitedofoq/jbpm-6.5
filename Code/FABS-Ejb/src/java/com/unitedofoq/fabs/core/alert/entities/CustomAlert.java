/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.alert.entities;

import com.unitedofoq.fabs.core.report.customReports.CustomReport;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 *
 * @author lap
 */
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(value = "CUST")
public class CustomAlert extends BaseAlert {  
    private String filterSQL;
    private String toSQL;
    private String ccSQL;
    private String paramSQL;
    private String attachementSQL;
    private String reportParameterSQL;
    private String reportPasswordSQL;
    private CustomReport customReport;

    public String getFilterSQLDD() {
        return "CustomAlert_filterSQL";
    }    
        
    public String getFilterSQL() {
        return filterSQL;
    }

    public void setFilterSQL(String filterSQL) {
        this.filterSQL = filterSQL;
    }
    
    
    
    public String getToSQLDD() {
        return "CustomAlert_toSQL";
    }

    public String getToSQL() {
        return toSQL;
    }

    public void setToSQL(String toSQL) {
        this.toSQL = toSQL;
    }

    public String getCcSQLDD() {
        return "CustomAlert_ccSQL";
    }
    
    public String getCcSQL() {
        return ccSQL;
    }

    public void setCcSQL(String ccSQL) {
        this.ccSQL = ccSQL;
    }

    public String getParamSQLDD() {
        return "CustomAlert_paramSQL";
    }
    
    public String getParamSQL() {
        return paramSQL;
    }

    public void setParamSQL(String paramSQL) {
        this.paramSQL = paramSQL;
    }

    public String getAttachementSQL() {
        return attachementSQL;
    }

    public void setAttachementSQL(String attachementSQL) {
        this.attachementSQL = attachementSQL;
    }
    
    public String getAttachementSQLDD() {
        return "CustomAlert_attachementSQL";
    }   

    public CustomReport getCustomReport() {
        return customReport;
    }

    public void setCustomReport(CustomReport customReport) {
        this.customReport = customReport;
    }
    
    public String getCustomReportDD() {
        return "CustomAlert_customReport";
    }

    public String getReportParameterSQL() {
        return reportParameterSQL;
    }

    public void setReportParameterSQL(String reportParameterSQL) {
        this.reportParameterSQL = reportParameterSQL;
    }
    
    public String getReportParameterSQLDD() {
        return "CustomAlert_reportParameterSQL";
    }

    public String getReportPasswordSQL() {
        return reportPasswordSQL;
    }

    public void setReportPasswordSQL(String reportPasswordSQL) {
        this.reportPasswordSQL = reportPasswordSQL;
    }
    
    public String getReportPasswordSQLDD() {
        return "CustomAlert_reportPasswordSQL";
    }
}
