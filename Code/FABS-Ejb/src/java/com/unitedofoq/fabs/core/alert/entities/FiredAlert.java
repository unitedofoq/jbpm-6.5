/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.alert.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntity;

/**
 *
 * @author MEA
 */
@Entity
public class FiredAlert extends BaseEntity {

    //<editor-fold defaultstate="collapsed" desc="Alert Title">
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleDD() {
        return "FiredAlert_title";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Alert Date">
    @Temporal(TemporalType.DATE)
    private Date alertDate;

    public Date getAlertDate() {
        return alertDate;
    }

    public void setAlertDate(Date alertDate) {
        this.alertDate = alertDate;
    }

    public String getAlertDateDD() {
        return "FiredAlert_alertDate";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Message Subject">
    private String messageSubject;

    public String getMessageSubject() {
        return messageSubject;
    }

    public void setMessageSubject(String messageSubject) {
        this.messageSubject = messageSubject;
    }

    public String getMessageSubjectDD() {
        return "FiredAler_messageSubject";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Message Body">
    private String messageBody;

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getMessageBodyDD() {
        return "FiredAlert_messageBody";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Reciver EMail">
    private String eMail;

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String geteMailDD() {
        return "FiredAlert_eMail";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Alert Type">
    private String type; //SMS or Email Alerts

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeDD() {
        return "FiredAlert_type";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="OEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.EAGER)
    @JoinColumn(name = "oEntity_dbid")
    OEntity actOnEntity;

    public OEntity getActOnEntity() {
        return actOnEntity;
    }

    public void setActOnEntity(OEntity actOnEntity) {
        this.actOnEntity = actOnEntity;
    }

    public String getActOnEntityDD() {
        return "FiredAlert_actOnEntity";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="entity">
    @Column(name = "entity_dbid")
    long entityDBID;

    public long getEntityDBID() {
        return entityDBID;
    }

    public void setEntityDBID(long entityDBID) {
        this.entityDBID = entityDBID;
    }

    public String getEntityDBIDDD() {
        return "AuditTrailTransaction_entityDBID";
    }
    //</editor-fold>
}
