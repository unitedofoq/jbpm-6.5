/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.content;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.Exception.BasicException;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author lap
 */
@Local
public interface EntityAttachmentServiceLocal {
    public AttachmentDTO putPreSave(AttachmentDTO attachmentDTO, BaseEntity entity, OUser loggedUser);
    public List<AttachmentDTO> getPreSave(BaseEntity entity, OUser loggedUser);
    public void deleteForPresave(BaseEntity entity, AttachmentDTO attachmentDTO, OUser loggedUser);
    public AttachmentDTO put(AttachmentDTO attachmentDTO,String attachmentEntityName,long entityDBID,  BaseEntity entity, OUser loggedUser) throws BasicException;
    public List<AttachmentDTO> get(long entityDBID, String attachmentEntityName,OUser loggedUser) throws BasicException;
    public AttachmentDTO getAttachmentById(String attachmentID, OUser loggedUser,String attachmentEntityName) throws BasicException;
    public void deleteAll(long entityDBID, OUser loggedUser,String attachmentEntityName);
    public void delete(String attachmentID,OUser loggedUser,String attachmentEntityName) throws BasicException;
    public void update(AttachmentDTO attachment, OUser loggedUser,String attachmentEntityName) throws BasicException;
    public boolean hasAttachments(String attachmentName, OUser loggedUser,String attachmentEntityName);
    public AttachmentDTO getByName(String attachmentName, OUser loggedUser,String attachmentEntityName);
    public void clearAttachmentListBeforeSave(BaseEntity entity, OUser loggedUser);
}
