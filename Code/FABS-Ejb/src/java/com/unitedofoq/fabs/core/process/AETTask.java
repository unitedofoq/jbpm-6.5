/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.process;

import java.io.Serializable;

import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.process.jBPM.TaskInstance;

/**
 *
 * @author ahussien
 */
public class AETTask implements Serializable {

//    private Boolean requestable;
//private String oInputDataType;
//private String oOutputDataType;
//private String oRunScreen;
//private String oRunPage;
//private String taskScreenMode;
//private String presentation;
//private String screenMode;
//private boolean requestStatus;

private String processInstanceID;
private Boolean requestable;
private ODataType oOutputDataType;
private long taskScreenModeID;
private String presentation;
private TaskFunction function;
private boolean requestStatus;
private TaskInstance taskInstance;
private Boolean isNotification;

    public void setFunction(TaskFunction function) {
        this.function = function;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    public long getTaskScreenModeID() {
        return taskScreenModeID;
    }

    public void setTaskScreenModeID(long taskScreenModeID) {
        this.taskScreenModeID = taskScreenModeID;
    }        

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the instanceID
     */
    public String getInstanceID() {
        return instanceID;
    }

    /**
     * @param instanceID the instanceID to set
     */
    public void setInstanceID(String instanceID) {
        this.instanceID = instanceID;
    }
    

    private String id;
    private String name;
    private String createdOn;
    private String status;
    private boolean skipable;

    private String createdBy;
    private String initiator;
    private String activationDate;
    private String expirationDate;
    private String description;
    private String type;
    private String owner;
    private String priority;
    private String subject;
    private String instanceID;

    private String claim_release_Type;
    private String suspend_resume_Type;
    private boolean claim_release_Flag;
    private boolean suspend_resume_Flag;
    private boolean skip_Flag;
    private boolean delegate_Flag;

    public boolean isClaim_release_Flag() {
        return claim_release_Flag;
    }

    public void setClaim_release_Flag(boolean claim_release_Flag) {
        this.claim_release_Flag = claim_release_Flag;
    }

    public boolean isDelegate_Flag() {
        return delegate_Flag;
    }

    public void setDelegate_Flag(boolean delegate_Flag) {
        this.delegate_Flag = delegate_Flag;
    }

    public boolean isSkip_Flag() {
        return skip_Flag;
    }

    public void setSkip_Flag(boolean skip_Flag) {
        this.skip_Flag = skip_Flag;
    }

    public boolean isSkipable() {
        return skipable;
    }

    public void setSkipable(boolean skipable) {
        this.skipable = skipable;
    }

    public boolean isSuspend_resume_Flag() {
        return suspend_resume_Flag;
    }

    public void setSuspend_resume_Flag(boolean suspend_resume_Flag) {
        this.suspend_resume_Flag = suspend_resume_Flag;
    }
    
    public String getActivationDateDD() {
        return "AETTask_activationDate";
    }

    public String getActualOwnerDD() {
        return "AETTask_actualOwner";
    }

    public String getCreatedByDD() {
        return "AETTask_createdBy";
    }

    public String getCreatedOnDD() {
        return "AETTask_createdOn";
    }

    public String getDescriptionDD() {
        return "AETTask_description";
    }

    public String getExpirationDateDD() {
        return "AETTask_expirationDate";
    }

    public String getIdDD() {
        return "AETTask_id";
    }

    public String getInitiatorDD() {
        return "AETTask_initiator";
    }

    public String getNameDD() {
        return "AETTask_name";
    }

    public String getOwnerDD() {
        return "AETTask_owner";
    }

    public String getPriorityDD() {
        return "AETTask_priority";
    }

    public String getSkipableDD() {
        return "AETTask_skipable";
    }

    public String getStatusDD() {
        return "AETTask_status";
    }

    public String getTypeDD() {
        return "AETTask_type";
    }

    public String getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(String activationDate) {
        this.activationDate = activationDate;
    }    

    public String getClaim_release_Type() {
        return claim_release_Type;
    }

    public void setClaim_release_Type(String claim_release_Type) {
        this.claim_release_Type = claim_release_Type;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

   

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInitiator() {
        return initiator;
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    

    public String getSuspend_resume_Type() {
        return suspend_resume_Type;
    }

    public void setSuspend_resume_Type(String suspend_resume_Type) {
        this.suspend_resume_Type = suspend_resume_Type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubjectDD() {
        return "AETTask_subject";
    }

    public void setSubjectDD(String task_subject) {
        setSubject(task_subject);
    }

    /**
     * @return the requestable
     */
    public Boolean getRequestable() {
        return requestable;
    }

    /**
     * @param requestable the requestable to set
     */
    public void setRequestable(Boolean requestable) {
        this.requestable = requestable;
    }

    

    /**
     * @return the presentation
     */
    public String getPresentation() {
        return presentation;
    }

    /**
     * @param presentation the presentation to set
     */
    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    
    /**
     * @return the requestStatus
     */
    public boolean isRequestStatus() {
        return requestStatus;
    }

    /**
     * @param requestStatus the requestStatus to set
     */
    public void setRequestStatus(boolean requestStatus) {
        this.requestStatus = requestStatus;
    }

    

    /**
     * @return the oOutputDataType
     */
    public ODataType getoOutputDataType() {
        return oOutputDataType;
    }

    /**
     * @param oOutputDataType the oOutputDataType to set
     */
    public void setoOutputDataType(ODataType oOutputDataType) {
        this.oOutputDataType = oOutputDataType;
    }

    

    /**
     * @return the function
     */
    public TaskFunction getFunction() {
        return function;
    }

    /**
     * @return the isNotification
     */
    public Boolean getIsNotification() {
        return isNotification;
    }

    /**
     * @param isNotification the isNotification to set
     */
    public void setIsNotification(Boolean isNotification) {
        this.isNotification = isNotification;
    }

    /**
     * @return the processInstanceID
     */
    public String getProcessInstanceID() {
        return processInstanceID;
    }

    /**
     * @param processInstanceID the processInstanceID to set
     */
    public void setProcessInstanceID(String processInstanceID) {
        this.processInstanceID = processInstanceID;
    }

    public TaskInstance getTaskInstance() {
        return taskInstance;
    }

    public void setTaskInstance(TaskInstance taskInstance) {
        this.taskInstance = taskInstance;
    }
    
}
