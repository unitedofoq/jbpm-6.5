/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.screen.wizard;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.function.OFunction;


/**
 *
 * @author mmohamed
 */
@Entity
@DiscriminatorValue(value = "WIZARD")
@ParentEntity(fields={"wizard"})
@ChildEntity(fields={"functionImage", "menuFunction"})
@VersionControlSpecs(omoduleFieldExpression="wizard.omodule")
public class WizardFunction extends OFunction {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    Wizard wizard;    

    public Wizard getWizard() {
        return wizard;
    }

    public String getWizardDD() {
        return "WizardFunction_wizard";
    }

    public void setWizard(Wizard wizard) {
        this.wizard = wizard;
    }
    
}
