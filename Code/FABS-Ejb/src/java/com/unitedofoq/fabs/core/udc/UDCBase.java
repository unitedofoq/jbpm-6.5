/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.udc;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;

/**
 *
 * @author htawab
 */
@Entity
@Table(name = "UDC")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "UTYPE")
public class UDCBase extends ObjectBaseEntity
{
    @Translatable(translationField="valueTranslated")
    @Column(nullable=false)
    protected String value;

    @Translation(originalField="value")
    @Transient
    private String valueTranslated;

    public String getValueTranslated() {
        return valueTranslated;
    }

    public void setValueTranslated(String valueTranslated) {
        this.valueTranslated = valueTranslated;
    }


    @Column(nullable = false)
    protected String code;

    protected boolean reserved;

    /* DD NAMES GETTERS */
    public String getValueTranslatedDD()    { return "UDC_value"; }
    public String getCodeDD()     { return "UDC_code"; }
    public String getReservedDD() { return "UDC_reserved"; }

    /*  GETTERS  */
    public String  getValue()   { return value; }
    public String  getCode()    { return code; }
    public boolean isReserved() { return reserved; }

    /*  SETTERS  */
    public void setValue(String value)        { this.value = value; }
    public void setCode(String code)          { this.code = code; }
    public void setReserved(boolean reserved) { this.reserved = reserved; }
}
