/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.process.Intalio;

/**
 *
 * @author melsayed
 */
public class IntalioTask {
    private String taskId;
    private String status;
    private String type;
    private String description;
    private String processId;
    private String creationDate;
    private String roleOwner;
    private String formUrl;
    private String userProcessCompleteSOAPAction;
    private String isChainedBefore;

    /**
     * @return the taskId
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * @param taskId the taskId to set
     */
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the processId
     */
    public String getProcessId() {
        return processId;
    }

    /**
     * @param processId the processId to set
     */
    public void setProcessId(String processId) {
        this.processId = processId;
    }

    /**
     * @return the creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the roleOwner
     */
    public String getRoleOwner() {
        return roleOwner;
    }

    /**
     * @param roleOwner the roleOwner to set
     */
    public void setRoleOwner(String roleOwner) {
        this.roleOwner = roleOwner;
    }

    /**
     * @return the formUrl
     */
    public String getFormUrl() {
        return formUrl;
    }

    /**
     * @param formUrl the formUrl to set
     */
    public void setFormUrl(String formUrl) {
        this.formUrl = formUrl;
    }

    /**
     * @return the userProcessCompleteSOAPAction
     */
    public String getUserProcessCompleteSOAPAction() {
        return userProcessCompleteSOAPAction;
    }

    /**
     * @param userProcessCompleteSOAPAction the userProcessCompleteSOAPAction to set
     */
    public void setUserProcessCompleteSOAPAction(String userProcessCompleteSOAPAction) {
        this.userProcessCompleteSOAPAction = userProcessCompleteSOAPAction;
    }

    /**
     * @return the isChainedBefore
     */
    public String getIsChainedBefore() {
        return isChainedBefore;
    }

    /**
     * @param isChainedBefore the isChainedBefore to set
     */
    public void setIsChainedBefore(String isChainedBefore) {
        this.isChainedBefore = isChainedBefore;
    }
}
