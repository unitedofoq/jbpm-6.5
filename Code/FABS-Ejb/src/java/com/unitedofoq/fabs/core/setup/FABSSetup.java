/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.setup;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.udc.UDC;

/**
 *
 * @author nkhalil
 */
@Entity
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class FABSSetup extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OModule omodule;

    public String getOmoduleDD(){
        return "FABSSetup_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
// </editor-fold>

    private boolean bvalue;

    public String getBvalueDD() {
        return "FABSSetup_bvalue";
    }

    public boolean isBvalue() {
        return bvalue;
    }

    public void setBvalue(boolean bvalue) {
        this.bvalue = bvalue;
    }


    private boolean cached;

    public boolean isCached() {
        return cached;
    }

    public void setCached(boolean cached) {
        this.cached = cached;
    }

    public String getCachedDD(){
        return "FABSSetup_cached";
    }
    private String description;
    public String getDescriptionDD() {
        return "FABSSetup_description";
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @OneToMany(mappedBy = "fabsSetup")
    private List<FABSSetupScreenKey> fABSSetupScreenKeys;

    public List<FABSSetupScreenKey> getFABSSetupScreenKeys() {
        return fABSSetupScreenKeys;
    }

    public void setFABSSetupScreenKeys(List<FABSSetupScreenKey> fABSSetupScreenKeys) {
        this.fABSSetupScreenKeys = fABSSetupScreenKeys;
    }

    private String fieldExpression;

    public String getFieldExpression() {
        return fieldExpression;
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }
    
    @Column(name="skey", unique=true, nullable=false)
    private String setupKey;
    private String svalue;
    //private long lvalue;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC group;
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private DD dd;

    public String getDdDD() {return "FABSSetup_dd";}
    public String getSetupKeyDD() {return "FABSSetup_setupKey";}
    public String getSvalueDD() {return "FABSSetup_svalue";}
    public String getGroupDD() {return "FABSSetup_group";}
    public String getFieldExpressionDD() {return "FABSSetup_fieldExpression";}
    public String getFABSSetupScreenKeysDD() {return "FABSSetup_fABSSetupScreenKeys";}

    public DD getDd() {return dd;}

    public void setDd(DD dd) {
        this.dd = dd;
    }


    public UDC getGroup() {
        return group;
    }

    public void setGroup(UDC group) {
        this.group = group;
    }


    public String getSetupKey() {
        return setupKey;
    }

    public void setSetupKey(String setupKey) {
        this.setupKey = setupKey;
    }

    public String getSvalue() {
        return svalue;
    }

    public void setSvalue(String svalue) {
        this.svalue = svalue;
    }


}
