/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.function;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.uiframework.navigationmenu.NMEntityListener;

/**
 *
 * @author nkhalil
 */
@Entity
@EntityListeners(value={NMEntityListener.class})
@ParentEntity(fields={"omenu", "ofunction"})
@FABSEntitySpecs(indicatorFieldExp="omenu.name")
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class OMenuFunction extends BaseEntity 
{
    // <editor-fold defaultstate="collapsed" desc="ofunction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    OFunction ofunction;
    public OFunction getOfunction() {
        return ofunction;
    }
    public String getOfunctionDD(){
        return "OMenuFunction_ofunction";
    }
    public void setOfunction(OFunction ofunction) {
        this.ofunction = ofunction;
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="omenu">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    OMenu omenu;
    public OMenu getOmenu() {
        return omenu;
    }
    public String getOmenuDD(){
        return "OMenuFunction_omenu";
    }
    public void setOmenu(OMenu omenu) {
        this.omenu = omenu;
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="sortIndex">
    private int sortIndex;

    /**
     * @return the sortIndex
     */
    public int getSortIndex() {
        return sortIndex;
    }

    /**
     * @param sortIndex the sortIndex to set
     */
    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getSortIndexDD(){
        return "OMenuFunction_sortIndex";
    }
    // </editor-fold >
}
