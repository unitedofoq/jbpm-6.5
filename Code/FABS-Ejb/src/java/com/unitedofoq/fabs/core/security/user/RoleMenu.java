/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.security.user;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.function.OMenu;
import com.unitedofoq.fabs.core.uiframework.navigationmenu.NMEntityListener;

/**
 *
 * @author ahussien
 */
@Entity
@EntityListeners(value={NMEntityListener.class})
@ParentEntity(fields={"omenu", "orole"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class RoleMenu extends BaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="includeSubs">
    private boolean includeSubs;
    public boolean isIncludeSubs() {
        return includeSubs;
    }
    public void setIncludeSubs(boolean includeSubs) {
        this.includeSubs = includeSubs;
    }
    public String getIncludeSubsDD(){
        return "RoleMenu_includeSubs";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="omenu">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
    private OMenu omenu;
    public String getOmenuDD(){
        return "RoleMenu_omenu";
    }
    public OMenu getOmenu() {
        return omenu;
    }
    public void setOmenu(OMenu omenu) {
        this.omenu = omenu;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="orole">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
    private ORole orole;
    public String getOroleDD(){
        return "RoleMenu_orole";
    }
    public ORole getOrole() {
        return orole;
    }
    public void setOrole(ORole orole) {
        this.orole = orole;
    }
    // </editor-fold>
}
