package com.unitedofoq.fabs.core.process;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalPage;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;

@Entity
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class OHumanTask extends BaseEntity{

    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OModule omodule;

    public String getOmoduleDD(){
        return "OHumanTask_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
// </editor-fold>

    private boolean requestable;

    public boolean isRequestable() {
        return requestable;
    }

    public void setRequestable(boolean requestable) {
        this.requestable = requestable;
    }

   
    
    @Column(unique=true,nullable=false)
    private String name;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ODataType oInputDataType;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ODataType oOutputDataType;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OScreen oRunScreen;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OPortalPage oRunPage;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC taskScreenMode;

    @Translatable(translationField="presentationTranslated")
    private String presentation;

    @Translation(originalField="presentation")
    @Transient
    private String presentationTranslated;

    public String getPresentationTranslatedDD(){
        return "OHumanTask_presentation";
    }

    public String getPresentationTranslated() {
        return presentationTranslated;
    }

    public void setPresentationTranslated(String presentationTranslated) {
        this.presentationTranslated = presentationTranslated;
    }


    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC screenMode;

    private boolean requestStatus;

    public boolean isRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(boolean requestStatus) {
        this.requestStatus = requestStatus;
    }
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the oInputDataType
     */
    public ODataType getoInputDataType() {
        return oInputDataType;
    }

    /**
     * @param oInputDataType the oInputDataType to set
     */
    public void setoInputDataType(ODataType oInputDataType) {
        this.oInputDataType = oInputDataType;
    }

    /**
     * @return the oOutputDataType
     */
    public ODataType getoOutputDataType() {
        return oOutputDataType;
    }

    /**
     * @param oOutputDataType the oOutputDataType to set
     */
    public void setoOutputDataType(ODataType oOutputDataType) {
        this.oOutputDataType = oOutputDataType;
    }

    /**
     * @return the oRunScreen
     */
    public OScreen getoRunScreen() {
        return getORunScreen();
    }

    /**
     * @param oRunScreen the oRunScreen to set
     */
    public void setoRunScreen(OScreen oRunScreen) {
        this.setORunScreen(oRunScreen);
    }

    /**
     * @return the taskScreenMode
     */
    public UDC getTaskScreenMode() {
        return taskScreenMode;
    }

    /**
     * @param taskScreenMode the taskScreenMode to set
     */
    public void setTaskScreenMode(UDC taskScreenMode) {
        this.taskScreenMode = taskScreenMode;
    }

    /**
     * @return the presentation
     */
    public String getPresentation() {
        return presentation;
    }

    /**
     * @param presentation the presentation to set
     */
    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    /**
     * @return the screenMode
     */
    public UDC getScreenMode() {
        return screenMode;
    }

    /**
     * @param screenMode the screenMode to set
     */
    public void setScreenMode(UDC screenMode) {
        this.screenMode = screenMode;
    }

    /**
     * @return the oRunScreen
     */
    public OScreen getORunScreen() {
        return oRunScreen;
    }

    /**
     * @param oRunScreen the oRunScreen to set
     */
    public void setORunScreen(OScreen oRunScreen) {
        if(oRunPage != null) return;
        this.oRunScreen = oRunScreen;
    }

    /**
     * @return the oRunPage
     */
    public OPortalPage getORunPage() {
        return oRunPage;
    }

    /**
     * @param oRunPage the oRunPage to set
     */
    public void setORunPage(OPortalPage oRunPage) {
        if(oRunScreen != null) return;
        this.oRunPage = oRunPage;
    }



   

}
