/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.encryption;

import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.central.OCentralUser;
import com.unitedofoq.fabs.core.Exception.BasicException;
import com.unitedofoq.fabs.core.content.AttachmentDTO;
import com.unitedofoq.fabs.core.content.EntityAttachmentServiceLocal;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntityField;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.setup.FABSSetup;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Rehab
 */
@Singleton
@Startup
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.encryption.DataEncryptorServiceLocal",
        beanInterface = DataEncryptorServiceLocal.class)
public class DataEncryptorService implements DataEncryptorServiceLocal {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    public static byte[] publicKeyb;
    public static byte[] privateKeyb;
    private String SymmetricKey;
    Cipher cipher = null;
    byte[] ciphertext = null;
    byte[] decryptedSymmetricKey;
    byte[] encSymmetricKey;

    //new algorithm
    private static final String ALGO = "AES/CBC/PKCS5Padding";
    private String ivKey = "66657489213487653421348765432783";
    private byte[] keyValue;
    private static Key key;
    private static Cipher decryptor;
    private static Cipher encryptor;

    @Override
    public byte[] getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(byte[] key) {
        this.keyValue = key;
    }

    @Override
    public String getSymmetricKey() {
        return SymmetricKey;
    }

    public void setSymmetricKey(String key) {
        this.SymmetricKey = SymmetricKey;
    }

    @Override
    public String getIvKey() {
        return ivKey;
    }

    public void setIvKey(String ivKey) {
        this.ivKey = ivKey;
    }

//    private PooledPBEStringEncryptor stringEncryptor;
//    private PooledPBEBigIntegerEncryptor bigIntegerEncryptor;
//    private PooledPBEBigDecimalEncryptor bigDecimalEncryptor;
    public static OUser loggedUser = null;
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private OCentralEntityManagerRemote centralOEM;
    @EJB
    public UserMessageServiceRemote userMessageServiceRemote;
    @EJB
    private EntityAttachmentServiceLocal entityAttachmentService;

    private BaseEntity entity = new BaseEntity();
    final static Logger logger = LoggerFactory.getLogger(DataEncryptorService.class);

    @PostConstruct
    public void init() {
        logger.debug("Entering: init");
        try {
            List<OCentralUser> users = getCentralUser();
            for (int i = 0; i < users.size(); i++) {
                loggedUser = getTanentUser(users.get(i));
                if (loggedUser == null) {
                    continue;
                }
                cipher = Cipher.getInstance("RSA");
                encSymmetricKey = getSymatricKey();
                List<AttachmentDTO> keyPair = getKeyPair();
                if (keyPair != null && keyPair.size() !=0 && encSymmetricKey != null) {
                    if (keyPair.get(0).getName().toLowerCase().equals("publickey.key")) {
                        publicKeyb = keyPair.get(0).getAttachment();
                        privateKeyb = keyPair.get(1).getAttachment();
                    } else {
                        publicKeyb = keyPair.get(1).getAttachment();
                        privateKeyb = keyPair.get(0).getAttachment();
                    }
                    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                    PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyb);
                    PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
                    cipher.init(Cipher.DECRYPT_MODE, privateKey);
                    decryptedSymmetricKey = cipher.doFinal(encSymmetricKey);
                    SymmetricKey = new String(decryptedSymmetricKey, "UTF-8");
                    keyValue = decryptedSymmetricKey;
                    buildEncryptors();
                } else {
                    logger.warn("missing encryption configuration publicKey.key privateKey.key");
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning from: init");
    }

    private void buildEncryptors() throws NoSuchAlgorithmException, NoSuchPaddingException, DecoderException,
            InvalidKeyException, InvalidAlgorithmParameterException {
        logger.trace("Entering: buildEncryptors");
        key = generateKey();
        encryptor = Cipher.getInstance(ALGO);
        IvParameterSpec iv = new IvParameterSpec(Hex.decodeHex(ivKey.toCharArray()));//32 numbers must
        decryptor = Cipher.getInstance(ALGO);
        encryptor.init(Cipher.ENCRYPT_MODE, key, iv);
        decryptor.init(Cipher.DECRYPT_MODE, key, iv);
        logger.trace("Returning from: buildEncryptors");
    }

    @Override
    public String generateSymmetricKey() {
        logger.debug("Entering: generateSymmetricKey");
        int pwdLength = 16;
        String passwordCHRs = "abcdefghijklmnopqrstuvwxyz"
                + "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "01234567890";
        String PWD = "";
        int chracterLocationInList;
        for (int i = 0; i < pwdLength; i++) {
            chracterLocationInList = (int) Math.round(Math.random() * 61);
            PWD += passwordCHRs.charAt(chracterLocationInList);
        }
        logger.debug("Returning from: generateSymmetricKey");
        return PWD;
    }
    
    @Override
    public String textEncryptor(String msg) {
        logger.debug("Entering: textEncryptor");
        String encMsg = null;
        try {
            encMsg = DataEncryptorService.encrypt(msg);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            logger.debug("Returning from: textEncryptor");
            return encMsg;
        }
    }

    @Override
    public String textDecryptor(String encMsg) {
        logger.debug("Entering: textEncryptor");
        String decMsg = null;
        try {
            decMsg = DataEncryptorService.decrypt(encMsg);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            logger.debug("Returning from: textDecryptor");
            return decMsg;
        }
    }

    @Override
    public String bigIntEncryptor(BigInteger Msg) {
        logger.debug("Entering: bigIntEncryptor");
        String enMsg = null;
        try {
            String passwordEnc = DataEncryptorService.encrypt(Msg.toString());
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            logger.debug("Returning from: bigIntEncryptor");
            return enMsg;
        }
    }

    @Override
    public BigInteger bigIntDecryptor(String encMsg) {
        logger.debug("Entering: bigIntDecryptor");
        BigInteger decMsg = null;
        long test;
        try {
            decMsg = new BigInteger(DataEncryptorService.decrypt(encMsg));
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            logger.debug("Returning from: bigIntDecryptor");
            return decMsg;
        }
    }

    @Override
    public String bigDecEncryptor(BigDecimal Msg) {
        logger.debug("Entering: bigDecEncryptor");
        String enMsg = null;
        try {
            enMsg = DataEncryptorService.encrypt(Msg.toString());
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            logger.debug("Returning from: bigDecEncryptor");
            return enMsg;
        }
    }

    @Override
    public BigDecimal bigDecDecryptor(String encMsg) {
        logger.debug("Entering: bigDecDecryptor");
        BigDecimal decMsg = null;
        try {
//            decMsg = bigDecimalEncryptor.decrypt(encMsg);
            decMsg = new BigDecimal(DataEncryptorService.decrypt(encMsg));
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            logger.debug("Returning from: bigDecDecryptor");
            return decMsg;
        }
    }

    public void saveSymmetricKeyInDB(byte[] symmKey, OUser systemUser) {//String symmKey) {
        logger.debug("Entering: saveSymmetricKeyInDB");
        try {
            AttachmentDTO attachmentDTO = entityAttachmentService.getByName("symmetricKey", systemUser, FABSSetup.class.getName());
            if (attachmentDTO != null) {
                attachmentDTO.setAttachment(symmKey);
                entityAttachmentService.update(attachmentDTO, systemUser, FABSSetup.class.getName());
            } else {
                attachmentDTO = new AttachmentDTO();
                attachmentDTO.setName("symmetricKey");
                attachmentDTO.setAttachment(symmKey);
                entityAttachmentService.put(attachmentDTO, FABSSetup.class.getName(), 0, entity,systemUser);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning from: saveSymmetricKeyInDB");
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    private OUser getTanentUser(OCentralUser user) {
        logger.trace("Entering: getTanentUser");
        OUser ologgedUser = null;
        try {
            ologgedUser = oem.getUserEnc(user.getLoginName(),
                    user.getTenant());
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.trace("Returning from: bigDecDecryptor");
        return ologgedUser;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    private List<OCentralUser> getCentralUser() {
        logger.trace("Entering: getCentralUser");
        List<OCentralUser> users = centralOEM.getAdminUsers();
        logger.trace("Returning from: getCentralUser");
        return users;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    private List<AttachmentDTO> getKeyPair() 
            throws NonUniqueResultException, RollbackException,
            TransactionRequiredException, UserNotAuthorizedException,
            OptimisticLockException, NoResultException, EntityNotFoundException,
            EntityExistsException, BasicException {
        logger.trace("Entering: getKeyPair");
        List<String> conditions = new ArrayList<String>();
        conditions.add("setupKey = 'keyPair'");
        FABSSetup fABSSetup = (FABSSetup) oem.loadEntity("FABSSetup", conditions, null, loggedUser);
        List<AttachmentDTO> keys = new ArrayList<AttachmentDTO>();
        if (fABSSetup != null && fABSSetup.getDbid() != 0) {
            keys = entityAttachmentService.get(fABSSetup.getDbid(), FABSSetup.class.getName(), loggedUser);
        }
        logger.trace("Returning from: getKeyPair");
        return keys;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    private byte[] getSymatricKey() throws NonUniqueResultException,
            TransactionRequiredException, NoResultException,
            EntityExistsException, RollbackException, OptimisticLockException,
            EntityNotFoundException, UserNotAuthorizedException {
        logger.trace("Entering: getSymatricKey");
        byte[] encSymmetricKey = null;
        AttachmentDTO adto = entityAttachmentService.getByName("symmetricKey", loggedUser, FABSSetup.class.getName());
        if (adto != null) {
            encSymmetricKey = adto.getAttachment();
        }
        logger.trace("Returning from: getSymatricKey");
        return encSymmetricKey;
    }

    public PublicKey getPublicKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
        logger.debug("Entering: getPublicKey");
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyb);
        PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);
        logger.debug("Returning from: getPublicKey");
        return publicKey;
    }

    public byte[] encryptSymmetricKey(PublicKey key, byte[] symmKey) throws NoSuchAlgorithmException,
            InvalidKeyException, IllegalBlockSizeException,
            BadPaddingException, UnsupportedEncodingException {
        logger.debug("Entering: encryptSymmetricKey");
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        ciphertext = cipher.doFinal(symmKey);
        logger.debug("Returning from: encryptSymmetricKey");
        return ciphertext;
    }

    @Override
    public void changePublicPrivateKeys(AttachmentDTO attachmentDTO,long entityDBID, byte[] firstPublic, OUser systemUser) {
        logger.debug("Entering: changePublicPrivateKeys");
        try {
            //----------set&get symm from file--------
            //get public and encrypt symm
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(firstPublic);
            PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);
            AttachmentDTO adto = entityAttachmentService.getByName("symmetricKey", systemUser, FABSSetup.class.getName());
            //first run
            byte[] encKey = null;
            if (adto == null) {
                keyValue = generateSymmetricKey().getBytes();
                encKey = encryptSymmetricKey(publicKey, keyValue);
            } else {
                encKey = encryptSymmetricKey(publicKey, SymmetricKey.getBytes());
            }
            //save in DB
            saveSymmetricKeyInDB(encKey, systemUser);
            key = generateKey();
            entityAttachmentService.put(attachmentDTO, FABSSetup.class.getName(), entityDBID, entity,systemUser);
            init();
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning from: changePublicPrivateKeys");
    }

    //validation function on create and update case check or uncheck is encrypted

    public OFunctionResult encryptionTableHasData(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: encryptionTableHasData");
        OFunctionResult oFR = new OFunctionResult();
        OEntityField newEntityField = new OEntityField();
        OEntityField oldEntityField = new OEntityField();
        try {
            if (odm.getData().get(0) instanceof OEntityField) {
                newEntityField = (OEntityField) odm.getData().get(0);
                //get entity class name & chech if table is empty
                String entityName = newEntityField.getOentity().getEntityClassName();
                List<BaseEntity> baseEntity = (List<BaseEntity>) oem.loadEntityList(entityName, null, null, null, loggedUser);
                //load entity from DB
                if (newEntityField != null && newEntityField.getDbid() != 0) {
                    List<String> conditions = new ArrayList<String>();
                    conditions.add("dbid=" + newEntityField.getDbid());
                    oldEntityField = (OEntityField) oem.loadEntity("OEntityField", conditions, null, loggedUser);
                }
                if (baseEntity != null && baseEntity.size() != 0 && oldEntityField != null) {
                    if (oldEntityField.isEncrypted() && !newEntityField.isEncrypted()) {
                        oFR.addError(userMessageServiceRemote.getUserMessage("tableHasData", loggedUser));
                    }
                    if (!oldEntityField.isEncrypted() && newEntityField.isEncrypted()) {
                        oFR.addError(userMessageServiceRemote.getUserMessage("tableHasData", loggedUser));
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning from: encryptionTableHasData");
        return oFR;
    }

    //validation function on delete case check or uncheck is encrypted
    public OFunctionResult encryptionTableHasDataDelete(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: encryptionTableHasDataDelete");
        OFunctionResult oFR = new OFunctionResult();
        OEntityField newEntityField = new OEntityField();
        OEntityField oldEntityField = new OEntityField();
        try {
            if (odm.getData().get(0) instanceof OEntityField) {
                newEntityField = (OEntityField) odm.getData().get(0);
                //get entity class name & chech if table is empty
                String entityName = newEntityField.getOentity().getEntityClassName();
                List<BaseEntity> baseEntity = (List<BaseEntity>) oem.loadEntityList(entityName, null, null, null, loggedUser);
                //load entity from DB
                if (newEntityField != null && newEntityField.getDbid() != 0) {
                    List<String> conditions = new ArrayList<String>();
                    conditions.add("dbid=" + newEntityField.getDbid());
                    oldEntityField = (OEntityField) oem.loadEntity("OEntityField", conditions, null, loggedUser);
                }                
                if (baseEntity != null && baseEntity.size() != 0 && oldEntityField != null
                        && oldEntityField.isEncrypted() ) {
                    oFR.addError(userMessageServiceRemote.getUserMessage("tableHasData", loggedUser));
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning from: encryptionTableHasDataDelete");
        return oFR;
    }

    //check if column not found
    public OFunctionResult encryColumnNotFound(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: encryColumnNotFound");
        OFunctionResult oFR = new OFunctionResult();
        OEntityField entityField = new OEntityField();
        try {
            if (odm.getData().get(0) instanceof OEntityField) {
                entityField = (OEntityField) odm.getData().get(0);
                //get entity class name
                String entityName = entityField.getOentity().getEntityClassPath();
                String encFieldName = "";
                Class b = Class.forName(entityName);
                Field field = b.getField(entityField.getFieldExpression());
                if (field.equals("Enc") && entityField.isEncrypted()) {//&&(encFieldName == null || encFieldName.equals(""))){
                    oFR.addError(userMessageServiceRemote.getUserMessage("encryptedColumnNotFound", loggedUser));
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning from: encryColumnNotFound");
        return oFR;
    }

    private Key generateKey() {
        logger.trace("Entering: generateKey");
        key = new SecretKeySpec(keyValue, "AES");
        logger.trace("Entering: generateKey");
        return key;
    }

    public static String encrypt(String Data) throws Exception {
        logger.debug("Entering: encrypt");
        byte[] encVal = encryptor.doFinal(Data.getBytes());
        String encryptedValue = Hex.encodeHexString(encVal);
        logger.debug("Returning from: encrypt");
        return encryptedValue;
    }

    public static String decrypt(String encryptedData) throws Exception {
        logger.debug("Entering: decrypt");
        byte[] decordedValue = Hex.decodeHex(encryptedData.toCharArray());
        byte[] decValue = decryptor.doFinal(decordedValue);
        String decryptedValue = new String(decValue);
        logger.debug("Returning from: decrypt");
        return decryptedValue;
    }
    
    public OFunctionResult encryptOuserPassword(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: encryColumnNotFound");
        OFunctionResult oFR = new OFunctionResult();
        try {
            List<String> conds = new ArrayList<String>();
            conds.add("##ACTIVEANDINACTIVE");
            List<OUser> ousers = oem.loadEntityList(OUser.class.getName(), conds, null, null, loggedUser);
            List<BaseEntity> ousersUpdated = new ArrayList<BaseEntity>();
            for(OUser ouser :ousers){
                ouser.setPasswordEnc(encrypt(ouser.getPassword()));
                ouser.setPassword("");
                ousersUpdated.add((BaseEntity)ouser);
            }
            oem.saveEntityList(ousersUpdated, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning from: encryColumnNotFound");
        return oFR;
    }
}
