/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.security.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author arezk
 */
@Entity
@ReadOnly
@NamedQueries(value={
    @NamedQuery(name="getUserMenus",
        query = " SELECT    userMenu " +
                " FROM      UserMenu userMenu " +
                " WHERE     userMenu.user_DBID = :userDBID " ),

    @NamedQuery(name="getUserRootMenus",
        query = " SELECT    userMenu " +
                " FROM      UserMenu userMenu " +
                " WHERE     userMenu.user_DBID = :userDBID" )})
public class UserMenu implements Serializable {
    @Column(name="hierCode")
    private String hierCode;

    public String getHierCode() {
        return hierCode;
    }

    public void setHierCode(String hierCode) {
        this.hierCode = hierCode;
    }  
 
    
    
    private static final long serialVersionUID = 1L;

    @Column(name="includeSubs")
    private boolean includeSubs;

    public boolean isIncludeSubs() {
        return includeSubs;
    }

    public void setIncludeSubs(boolean includeSubs) {
        this.includeSubs = includeSubs;
    }

    @Column(name="OUSER_DBID")
    private Long user_DBID ;

    public Long getUser_DBID() {
        return user_DBID;
    }

    public void setUser_DBID(Long user_DBID) {
        this.user_DBID = user_DBID;
    }

    @Id
    @Column(name="OMENU_DBID")
    private Long menu_DBID ;
    public Long getMenu_DBID() {
        return menu_DBID;
    }
    public void setMenu_DBID(Long menu_DBID) {
        this.menu_DBID = menu_DBID;
    }   

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserMenu)) {
            return false;
        }
        UserMenu other = (UserMenu) object;
        if ((this.user_DBID == null && other.user_DBID != null) || 
                (this.user_DBID != null && !this.user_DBID.equals(other.user_DBID))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.includeSubs ? 1 : 0);
        hash = 37 * hash + (this.user_DBID != null ? this.user_DBID.hashCode() : 0);
        hash = 37 * hash + (this.menu_DBID != null ? this.menu_DBID.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "com.unitedofoq.fabs.core.security.user.UserMenus[id=" + user_DBID + "]";
    }
}
