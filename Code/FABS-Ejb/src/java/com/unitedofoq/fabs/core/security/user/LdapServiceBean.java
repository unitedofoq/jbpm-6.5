package com.unitedofoq.fabs.core.security.user;

import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.setup.FABSSetup;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.security.user.LdapService", beanInterface = LdapService.class)
public class LdapServiceBean implements LdapService {

    final static Logger logger = LoggerFactory.getLogger(LdapServiceBean.class);
    @EJB
    private OCentralEntityManagerRemote centralOEM;
    @EJB
    private FABSSetupLocal setupService;
    @EJB
    UserMessageServiceRemote userMsgService;
    @EJB
    private OEntityManagerRemote oem;

    private String factoryInitial;
    private String securityAuthentication;
    private String groupFilter;
    private String peopleFilter;
    private String groupBase;
    private String peopleBase;
    private String securityPrincipal;
    private String serverURL;
    private String password;
    private String uniquememberKey;
    private String cnKey;
    private String uidKey;
    private String mailKey;
    private String snKey;
    private String givenNameKey;
    private String userTopValue;
    private String userPersonValue;
    private String userOrganizationalPersonValue;
    private String userInetOrgPersonValue;
    private String userRoleKey;
    private String adminRoleKey;
    private String roleTopKey;
    private String roleGroupOfUniqueNamesKey;
    private String passwordKey;

    private void loadLdapParams(OUser loggedUser) {
        logger.debug("Starting of loading LdapParams");
        FABSSetup factoryInitialKey = setupService.loadKeySetup(
                "factoryInitial", loggedUser);
        factoryInitial = factoryInitialKey.getSvalue();

        FABSSetup securityAuthenticationKey = setupService.loadKeySetup(
                "securityAuthentication", loggedUser);
        securityAuthentication = securityAuthenticationKey.getSvalue();

        FABSSetup groupFilterKey = setupService.loadKeySetup("groupFilter",
                loggedUser);
        groupFilter = groupFilterKey.getSvalue();

        FABSSetup peopleFilterKey = setupService.loadKeySetup("peopleFilter",
                loggedUser);
        peopleFilter = peopleFilterKey.getSvalue();

        FABSSetup groupBaseKey = setupService.loadKeySetup("groupBase",
                loggedUser);
        groupBase = groupBaseKey.getSvalue();

        FABSSetup peopleBaseKey = setupService.loadKeySetup("peopleBase",
                loggedUser);
        peopleBase = peopleBaseKey.getSvalue();

        FABSSetup securityPrincipalKey = setupService.loadKeySetup(
                "securityPrincipal", loggedUser);
        securityPrincipal = securityPrincipalKey.getSvalue();

        FABSSetup serverURLKey = setupService.loadKeySetup("serverURL",
                loggedUser);
        serverURL = serverURLKey.getSvalue();

        FABSSetup ldapPassword = setupService.loadKeySetup("password",
                loggedUser);
        password = ldapPassword.getSvalue();

        FABSSetup ldapUniquememberKey = setupService.loadKeySetup(
                "ldapUniquememberKey", loggedUser);
        uniquememberKey = ldapUniquememberKey.getSvalue();

        FABSSetup ldapRoleCnKey = setupService.loadKeySetup("ldapRoleCn",
                loggedUser);
        cnKey = ldapRoleCnKey.getSvalue();

        FABSSetup ldapUserCnKey = setupService.loadKeySetup("ldapUserCn",
                loggedUser);
        uidKey = ldapUserCnKey.getSvalue();

        FABSSetup ldapUserMailKey = setupService.loadKeySetup("ldapUserMail",
                loggedUser);
        mailKey = ldapUserMailKey.getSvalue();

        FABSSetup ldapUserSnKey = setupService.loadKeySetup("ldapUserSn",
                loggedUser);
        snKey = ldapUserSnKey.getSvalue();

        FABSSetup ldapUserGivenNameKey = setupService.loadKeySetup(
                "ldapUsergivenName", loggedUser);
        givenNameKey = ldapUserGivenNameKey.getSvalue();

        FABSSetup ldapUserRoleKey = setupService.loadKeySetup("ldapUserRole",
                loggedUser);
        userRoleKey = ldapUserRoleKey.getSvalue();

        FABSSetup ldapAdminRoleKey = setupService.loadKeySetup("ldapAdminRole",
                loggedUser);
        adminRoleKey = ldapAdminRoleKey.getSvalue();

        FABSSetup PasswordAttribute = setupService.loadKeySetup(
                "ldapUserPassword", loggedUser);
        passwordKey = PasswordAttribute.getSvalue();

        userTopValue = "top";
        userPersonValue = "person";
        userOrganizationalPersonValue = "organizationalPerson";
        userInetOrgPersonValue = "inetOrgPerson";
        roleTopKey = "top";
        roleGroupOfUniqueNamesKey = "GroupOfUniqueNames";
        logger.info("loading LdapParams completed Successfully");
    }

    private DirContext getLDAPContext(OUser loggedUser) throws IOException {
        logger.debug("Starting of getLDAPContext");
        loadLdapParams(loggedUser);

        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, factoryInitial);
        env.put(Context.PROVIDER_URL, serverURL);
        env.put(Context.SECURITY_AUTHENTICATION, securityAuthentication);
        env.put(Context.SECURITY_PRINCIPAL, securityPrincipal);
        env.put(Context.SECURITY_CREDENTIALS, password);
        env.put("peopleBase", peopleBase);
        env.put("peopleFilter", peopleFilter);
        env.put("groupBase", groupBase);
        env.put("groupFilter", groupFilter);

        DirContext ctx;
        try {
            ctx = new InitialDirContext(env);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            ctx = null;
        }
        logger.info("getLDAPContext completed Successfully");
        return ctx;
    }

    @Override
    public boolean checkLdapConnection(OUser loggedUser) {
        logger.debug("Starting of checkLdapConnection");
        DirContext ldapContext = null;
        try {
            ldapContext = getLDAPContext(loggedUser);
        } catch (IOException e) {
            logger.error("Error : ", e);
        }
        if (ldapContext != null) {
            logger.info("connected to Ldap Successfully");
            return true;
        } else {
            logger.info("Failing to connect to Ldap");
        }
        return false;
    }

    @Override
    public boolean addUser(OUser user, OUser loggedUser) {
        logger.debug("Starting of addUser to Ldap");
        loadLdapParams(loggedUser);
        if (user != null) {
            StringBuilder name = new StringBuilder(uidKey).append("=")
                    .append(user.getLoginName()).append(",").append(peopleBase);
            String givenName = user.getLoginName();
            String sn = ".";

            if (user.getDisplayName() != null
                    && !user.getDisplayName().equals("")) {
                if (user.getDisplayName().contains(" ")) {
                    givenName = user.getDisplayName().substring(0,
                            user.getDisplayName().lastIndexOf(" "));
                    sn = user.getDisplayName().substring(user.getDisplayName().lastIndexOf(" ") + 1);
                } else {
                    givenName = user.getDisplayName();
                }
            }
            Attributes attributes = new BasicAttributes();
            attributes = new BasicAttributes();
            attributes.put(new BasicAttribute("objectclass", userTopValue));
            attributes.put(new BasicAttribute("objectclass", userPersonValue));
            attributes.put(new BasicAttribute("objectclass",
                    userOrganizationalPersonValue));
            attributes.put(new BasicAttribute("objectclass",
                    userInetOrgPersonValue));
            if (uidKey.equals("uid")) {
                if (user.getDisplayName() != null
                        && !user.getDisplayName().equals("")) {
                    attributes.put(new BasicAttribute(cnKey, user
                            .getDisplayName()));
                } else {
                    attributes.put(new BasicAttribute(cnKey, user
                            .getLoginName() + " " + sn));
                }
            }
            attributes.put(new BasicAttribute(uidKey, user.getLoginName()));
            attributes.put(new BasicAttribute(snKey, sn));
            // attributes.put(new BasicAttribute(cnKey, user.getDisplayName()));
            attributes.put(new BasicAttribute(givenNameKey, givenName));
            attributes.put(new BasicAttribute(mailKey, user.getEmail()));
            attributes.put(new BasicAttribute(passwordKey, user.getPassword()));
            // User language:
            if (user.getFirstLanguage() != null
                    && user.getFirstLanguage().getDbid() == 24) {
                attributes.put(new BasicAttribute("l", "ar_SA"));
            }

            Attributes roleUserAttribute = new BasicAttributes();
            StringBuilder ldapAtribute = new StringBuilder(uidKey).append("=")
                    .append(user.getLoginName()).append(",").append(peopleBase);
            roleUserAttribute.put(new BasicAttribute(uniquememberKey,
                    ldapAtribute.toString()));
            InitialDirContext initDirContext = null;
            try {
                initDirContext = (InitialDirContext) getLDAPContext(loggedUser);
                initDirContext
                        .bind(name.toString(), initDirContext, attributes);
                initDirContext.modifyAttributes(userRoleKey,
                        DirContext.ADD_ATTRIBUTE, roleUserAttribute);
                initDirContext.modifyAttributes(adminRoleKey,
                        DirContext.ADD_ATTRIBUTE, roleUserAttribute);
                logger.info("addUser to Ldap completed Successfully");
                return true;
            } catch (Exception nex) {
                logger.error("Error: ", nex);
                return false;
            } finally {
                if (initDirContext != null) {
                    try {
                        logger.debug("Starting of closing Ldap connection");
                        initDirContext.close();
                    } catch (NamingException e) {
                        logger.error("Failing to close Ldap connection ", e);
                    }
                    logger.debug("closing Ldap connection completed Successfully");
                }

            }

        }

        return false;
    }

    @Override
    public boolean checkUserExistence(OUser user, OUser loggedUser) {
        logger.debug("Starting of checkUserExistence in Ldap ");
        loadLdapParams(loggedUser);
        if (user != null) {
            String givenName = user.getLoginName();
            String sn = ".";

            if (user.getDisplayName() != null
                    && !user.getDisplayName().equals("")) {
                if (user.getDisplayName().contains(" ")) {
                    givenName = user.getDisplayName().substring(0,
                            user.getDisplayName().lastIndexOf(" "));
                    sn = user.getDisplayName().substring(user.getDisplayName().lastIndexOf(" ") + 1);
                } else {
                    givenName = user.getDisplayName();
                }
            }
            Attributes attributes = new BasicAttributes();
            attributes.put(new BasicAttribute("objectclass", userTopValue));
            attributes.put(new BasicAttribute("objectclass", userPersonValue));
            attributes.put(new BasicAttribute("objectclass",
                    userOrganizationalPersonValue));
            attributes.put(new BasicAttribute("objectclass",
                    userInetOrgPersonValue));
            attributes.put(new BasicAttribute(uidKey, user.getLoginName()));
            attributes.put(new BasicAttribute(snKey, sn));
            attributes.put(new BasicAttribute(givenNameKey, givenName));
            attributes.put(new BasicAttribute(mailKey, user.getEmail()));
            attributes.put(new BasicAttribute(passwordKey, user.getPassword()));
            // attributes.put(new BasicAttribute(cnKey, user.getDisplayName()));
            if (uidKey.equals("uid")) {
                if (user.getDisplayName() != null
                        && !user.getDisplayName().equals("")) {
                    attributes.put(new BasicAttribute(cnKey, user
                            .getDisplayName()));
                } else {
                    attributes.put(new BasicAttribute(cnKey, user
                            .getLoginName() + " " + sn));
                }
            }
            StringBuilder name = new StringBuilder(uidKey).append("=")
                    .append(user.getLoginName()).append(",").append(peopleBase);
            InitialDirContext initDirContext = null;
            // Check if user exists in LDAP
            try {
                initDirContext = (InitialDirContext) getLDAPContext(loggedUser);
                initDirContext.search(name.toString(), attributes);
                logger.info("checkUserExistence in Ldap completed Successfully");
                return true;
                // User doesn't exist
            } catch (Exception ex) {
                logger.error("Error: " + ex);
                return false;
            } finally {
                if (initDirContext != null) {
                    try {
                        logger.debug("Starting of closing Ldap connection");
                        initDirContext.close();
                    } catch (NamingException e) {
                        logger.error("Failing to close Ldap connection", e);
                    }
                    logger.debug("closing Ldap connection completed Successfully");
                }
            }
        }
        return false;
    }

    @Override
    public boolean checkRoleExistence(ORole role, OUser loggedUser) {
        logger.debug("Starting of checkRoleExistence in Ldap");
        loadLdapParams(loggedUser);
        if (role != null) {
            Attributes attributes = new BasicAttributes();
            attributes.put(new BasicAttribute(cnKey, role.getName()));
            StringBuilder name = new StringBuilder(cnKey).append("=")
                    .append(role.getName()).append(",").append(groupBase);
            InitialDirContext initDirContext = null;
            // Check if user exists in LDAP
            try {
                initDirContext = (InitialDirContext) getLDAPContext(loggedUser);
                initDirContext.search(name.toString(), attributes);
                logger.info("checkRoleExistence in Ldap completed Successfully");
                return true;
                // Role doesn't exist
            } catch (Exception ex) {
                logger.error("Error : ", ex);
                return false;
            } finally {
                if (initDirContext != null) {
                    try {
                        logger.debug("Starting of closing Ldap connection");
                        initDirContext.close();
                    } catch (NamingException e) {
                        logger.error("Failing to close Ldap connection", e);
                    }
                    logger.debug("closing Ldap connection completed Successfully");
                }

            }
        }
        return false;
    }

    @Override
    public boolean addRoleUser(String userName, String roleName, OUser loggedUser) {
        logger.debug("Starting of addRoleUser in Ldap");
        loadLdapParams(loggedUser);
        if (userName != null && roleName != null) {
            StringBuilder uniquememberValue = new StringBuilder(uidKey)
                    .append("=").append(userName).append(",")
                    .append(peopleBase);
            StringBuilder name = new StringBuilder(cnKey).append("=")
                    .append(roleName).append(",").append(groupBase);
            Attributes attributes = new BasicAttributes();
            attributes.put(new BasicAttribute(uniquememberKey,
                    uniquememberValue.toString()));
            InitialDirContext initDirContext = null;
            try {
                initDirContext = (InitialDirContext) getLDAPContext(loggedUser);
                initDirContext.modifyAttributes(name.toString(),
                        DirContext.ADD_ATTRIBUTE, attributes);
                logger.info("addRoleUser in Ldap completed Successfully");
                return true;
            } catch (Exception e) {
                logger.error("Error : ", e);
                return false;
            } finally {
                if (initDirContext != null) {
                    try {
                        logger.debug("Starting of closing Ldap connection");
                        initDirContext.close();
                    } catch (NamingException e) {
                        logger.error("Failing to close Ldap connection", e);
                    }
                    logger.debug("closing Ldap connection  completed Successfully");
                }

            }
        }
        return false;
    }

    @Override
    public boolean addRole(ORole role, OUser loggedUser) {
        logger.debug("Starting of addRole in Ldap");
        loadLdapParams(loggedUser);
        if (role != null) {
            Attributes attributes = new BasicAttributes();
            attributes.put(new BasicAttribute("objectclass", roleTopKey));
            attributes.put(new BasicAttribute("objectclass",
                    roleGroupOfUniqueNamesKey));
            attributes.put(new BasicAttribute(cnKey, role.getName()));
            attributes.put(new BasicAttribute(uniquememberKey,
                    (new StringBuilder(uidKey).append("=").append("admin")
                    .append(",").append(peopleBase)).toString()));
            StringBuilder name = new StringBuilder(cnKey).append("=")
                    .append(role.getName()).append(",").append(groupBase);
            InitialDirContext initDirContext = null;
            try {
                initDirContext = (InitialDirContext) getLDAPContext(loggedUser);
                initDirContext
                        .bind(name.toString(), initDirContext, attributes);
                logger.info("addRole in Ldap completed Successfully");
                return true;
            } catch (Exception e) {
                logger.error("Error : ", e);
                return false;
            } finally {
                if (initDirContext != null) {
                    try {
                        logger.debug("Starting of closing Ldap connection");
                        initDirContext.close();
                    } catch (NamingException e) {
                        logger.error("Failing to close Ldap connection", e);
                    }
                    logger.debug("closing Ldap connection completed Successfully");
                }

            }
        }
        return false;
    }

    @Override
    public boolean updateUser(OUser user, OUser loggedUser) {
        logger.debug("Starting of updateUser in Ldap");
        loadLdapParams(loggedUser);
        if (user != null) {
            String givenName = user.getLoginName();
            String sn = ".";

            if (user.getDisplayName() != null
                    && !user.getDisplayName().equals("")) {
                if (user.getDisplayName().contains(" ")) {
                    givenName = user.getDisplayName().substring(0,
                            user.getDisplayName().lastIndexOf(" "));
                    sn = user.getDisplayName().substring(user.getDisplayName().lastIndexOf(" ") + 1);
                } else {
                    givenName = user.getDisplayName();
                }
            }
            Attributes attributes = new BasicAttributes();
            attributes.put(new BasicAttribute("objectclass", userTopValue));
            attributes.put(new BasicAttribute("objectclass", userPersonValue));
            attributes.put(new BasicAttribute("objectclass",
                    userOrganizationalPersonValue));
            attributes.put(new BasicAttribute("objectclass",
                    userInetOrgPersonValue));
            attributes.put(new BasicAttribute(uidKey, user.getLoginName()));
            attributes.put(new BasicAttribute(snKey, sn));
            attributes.put(new BasicAttribute(givenNameKey, givenName));
            attributes.put(new BasicAttribute(mailKey, user.getEmail()));
            attributes.put(new BasicAttribute(passwordKey, user.getPassword()));
            // attributes.put(new BasicAttribute(cnKey, user.getDisplayName()));
            if (uidKey.equals("uid")) {
                if (user.getDisplayName() != null
                        && !user.getDisplayName().equals("")) {
                    attributes.put(new BasicAttribute(cnKey, user
                            .getDisplayName()));
                } else {
                    attributes.put(new BasicAttribute(cnKey, user
                            .getLoginName() + " " + sn));
                }
            }
            StringBuilder name = new StringBuilder(uidKey).append("=")
                    .append(user.getLoginName()).append(",").append(peopleBase);
            InitialDirContext initDirContext = null;
            try {
                initDirContext = (InitialDirContext) getLDAPContext(loggedUser);
                initDirContext.rebind(name.toString(), initDirContext,
                        attributes);
                logger.info("updateUser in Ldap  completed Successfully");
                return true;
            } catch (Exception e) {
                logger.error("Error : ", e);
                return false;
            } finally {
                if (initDirContext != null) {
                    try {
                        logger.debug("Starting of closing Ldap connection");
                        initDirContext.close();
                    } catch (NamingException e) {
                        logger.error("Failing to close Ldap connection", e);
                    }
                    logger.debug("closing Ldap connection completed Successfully");
                }

            }
        }
        return false;
    }

    @Override
    public boolean updateRoleUser(String userName, String oldRoleName, String newRoleName, OUser loggedUser) {
        logger.debug("Starting of updateRoleUser in Ldap");
        loadLdapParams(loggedUser);
        if (userName != null && oldRoleName != null && newRoleName != null) {
            StringBuilder uniquememberValue = new StringBuilder(uidKey)
                    .append("=").append(userName).append(",")
                    .append(peopleBase);
            StringBuilder oldName = new StringBuilder(cnKey).append("=")
                    .append(oldRoleName).append(",").append(groupBase);
            StringBuilder newName = new StringBuilder(cnKey).append("=")
                    .append(newRoleName).append(",").append(groupBase);
            Attributes attributes = new BasicAttributes();
            attributes.put(new BasicAttribute(uniquememberKey,
                    uniquememberValue.toString()));
            attributes.put(new BasicAttribute(cnKey, oldName.toString()));
            InitialDirContext initDirContext = null;
            try {
                initDirContext = (InitialDirContext) getLDAPContext(loggedUser);
                initDirContext.modifyAttributes(oldName.toString(),
                        DirContext.REMOVE_ATTRIBUTE, attributes);
                attributes.put(new BasicAttribute(cnKey, newName.toString()));
                initDirContext.modifyAttributes(newName.toString(),
                        DirContext.ADD_ATTRIBUTE, attributes);
                logger.info("updateRoleUser in Ldap  completed Successfully");
                return true;
            } catch (Exception e) {
                logger.error("Error : ", e);
                return false;
            } finally {
                if (initDirContext != null) {
                    try {
                        logger.debug("Starting of closing Ldap connection");
                        initDirContext.close();
                    } catch (NamingException e) {
                        logger.error("Failing to close Ldap connection", e);
                    }
                    logger.debug("closing Ldap connection  completed Successfully");
                }

            }
        }
        return false;
    }

    @Override
    public boolean updateRole(ORole role, OUser loggedUser) {
        logger.debug("Starting of updateRole in Ldap");
        loadLdapParams(loggedUser);
        if (role != null) {

            Attributes attributes = new BasicAttributes();
            attributes.put(new BasicAttribute("objectclass", roleTopKey));
            attributes.put(new BasicAttribute("objectclass",
                    roleGroupOfUniqueNamesKey));
            attributes.put(new BasicAttribute(cnKey, role.getName()));
            attributes.put(new BasicAttribute(uniquememberKey,
                    (new StringBuilder(uidKey).append("=").append("admin")
                    .append(",").append(peopleBase)).toString()));
            StringBuilder name = new StringBuilder(cnKey).append("=")
                    .append(role.getName()).append(",").append(groupBase);
            InitialDirContext initDirContext = null;
            try {
                initDirContext = (InitialDirContext) getLDAPContext(loggedUser);
                initDirContext.rebind(name.toString(), initDirContext,
                        attributes);
                logger.info("updateRole in Ldap completed Successfully");
                return true;
            } catch (Exception e) {
                logger.error("Error : ", e);
                return false;
            } finally {
                if (initDirContext != null) {
                    try {
                        logger.debug("Starting of closing Ldap connection");
                        initDirContext.close();
                    } catch (NamingException e) {
                        logger.error("Failing to close Ldap connection", e);
                    }
                    logger.debug("closing Ldap connection  completed Successfully");
                }

            }
        }
        return false;
    }

    @Override
    public boolean deleteRoleUser(String userName, String roleName, OUser loggedUser) {
        logger.debug("Starting of deleteRoleUser from Ldap");
        loadLdapParams(loggedUser);
        if (userName != null && roleName != null) {

            Attributes attributes = new BasicAttributes();
            attributes.put(new BasicAttribute(uniquememberKey,
                    (new StringBuilder(uidKey).append("=").append(userName)
                    .append(",").append(peopleBase)).toString()));
            StringBuilder name = new StringBuilder(cnKey).append("=")
                    .append(roleName).append(",").append(groupBase);
            InitialDirContext initDirContext = null;
            try {
                initDirContext = (InitialDirContext) getLDAPContext(loggedUser);
                initDirContext.modifyAttributes(name.toString(),
                        DirContext.REMOVE_ATTRIBUTE, attributes);
                logger.info("deleteRoleUser from Ldap completed Successfully");
                return true;
            } catch (Exception e) {
                logger.error("Error : ", e);
                return false;
            } finally {
                if (initDirContext != null) {
                    try {
                        logger.debug("Starting of closing Ldap connection");
                        initDirContext.close();
                    } catch (NamingException e) {
                        logger.error("Failing to close Ldap connection", e);
                    }
                    logger.debug("closing Ldap connection completed Successfully");
                }

            }
        }
        return false;
    }

    @Override
    public boolean deleteUser(OUser user, OUser loggedUser) {
        logger.debug("Starting of deleteUser from Ldap");
        loadLdapParams(loggedUser);
        if (user != null) {
            StringBuilder name = new StringBuilder(uidKey).append("=")
                    .append(user.getLoginName()).append(",").append(peopleBase);
            InitialDirContext initDirContext = null;
            try {
                initDirContext = (InitialDirContext) getLDAPContext(loggedUser);
                initDirContext.unbind(name.toString());
                logger.info("deleteUser from Ldap completed Successfully");
                return true;
            } catch (Exception e) {
                logger.error("Error : ", e);
                return false;
            } finally {
                if (initDirContext != null) {
                    try {
                        logger.debug("Starting of closing Ldap connection");
                        initDirContext.close();
                    } catch (NamingException e) {
                        logger.error("Failing to close Ldap connection", e);
                    }
                    logger.debug("closing Ldap connection completed Successfully");
                }

            }
        }
        return false;
    }

    @Override
    public boolean deleteRole(ORole role, OUser loggedUser) {
        logger.debug("Starting of deleteRole from Ldap");
        loadLdapParams(loggedUser);
        if (role != null) {
            StringBuilder name = new StringBuilder(cnKey).append("=")
                    .append(role.getName()).append(",").append(groupBase);
            InitialDirContext initDirContext = null;
            try {
                initDirContext = (InitialDirContext) getLDAPContext(loggedUser);
                initDirContext.unbind(name.toString());
                logger.info("deleteRole from Ldap completed Successfully");
                return true;
            } catch (Exception e) {
                logger.error("Error : ", e);
                return false;
            } finally {
                if (initDirContext != null) {
                    try {
                        logger.debug("Starting of closing Ldap connection");
                        initDirContext.close();
                    } catch (NamingException e) {
                        logger.error("Failing to close Ldap connection", e);
                    }
                    logger.debug("closing Ldap connection completed Successfully");
                }

            }
        }
        return false;
    }

    @Override
    public boolean changeUserPassword(OUser user, String newPassWord, OUser loggedUser) {
        logger.debug("Starting of changeUserPassword in Ldap");
        loadLdapParams(loggedUser);
        if (user != null && newPassWord != null) {
            String givenName = user.getLoginName();
            String sn = ".";

            if (user.getDisplayName() != null
                    && !user.getDisplayName().equals("")) {
                if (user.getDisplayName().contains(" ")) {
                    givenName = user.getDisplayName().substring(0,
                            user.getDisplayName().lastIndexOf(" "));
                    sn = user.getDisplayName().substring(user.getDisplayName().lastIndexOf(" ") + 1);
                } else {
                    givenName = user.getDisplayName();
                }
            }

            Attributes attributes = new BasicAttributes();
            attributes.put(new BasicAttribute("objectclass", userTopValue));
            attributes.put(new BasicAttribute("objectclass", userPersonValue));
            attributes.put(new BasicAttribute("objectclass",
                    userOrganizationalPersonValue));
            attributes.put(new BasicAttribute("objectclass",
                    userInetOrgPersonValue));
            attributes.put(new BasicAttribute(uidKey, user.getLoginName()));
            attributes.put(new BasicAttribute(snKey, sn));
            attributes.put(new BasicAttribute(givenNameKey, givenName));
            attributes.put(new BasicAttribute(mailKey, user.getEmail()));
            // attributes.put(new BasicAttribute(cnKey, user.getDisplayName()));
            // attributes.put(new BasicAttribute(password, user.getPassword()));
            if (uidKey.equals("uid")) {
                if (user.getDisplayName() != null
                        && !user.getDisplayName().equals("")) {
                    attributes.put(new BasicAttribute(cnKey, user
                            .getDisplayName()));
                } else {
                    attributes.put(new BasicAttribute(cnKey, user
                            .getLoginName() + " " + sn));
                }
            }
            attributes.put(new BasicAttribute(passwordKey, newPassWord));
            StringBuilder name = new StringBuilder(uidKey).append("=")
                    .append(user.getLoginName()).append(",").append(peopleBase);
            InitialDirContext initDirContext = null;
            try {
                initDirContext = (InitialDirContext) getLDAPContext(loggedUser);
                initDirContext.rebind(name.toString(), initDirContext,
                        attributes);
                logger.info("changeUserPassword in Ldap  completed Successfully");
                return true;
            } catch (Exception e) {
                logger.error("Error : ", e);
                return false;
            } finally {
                if (initDirContext != null) {
                    try {
                        logger.debug("Starting of closing Ldap connection");
                        initDirContext.close();
                    } catch (NamingException e) {
                        logger.error("Failing to close Ldap connection ", e);
                    }
                    logger.debug("closing Ldap connection completed Successfully");
                }

            }
        }
        return false;
    }

    @Override
    public String getPasswordFromLDAP(String loginName, OUser loggedUser) {
        logger.debug("Starting of getPasswordFromLDAP ");
        loadLdapParams(loggedUser);
        if (loginName != null) {
            SearchControls constraints = new SearchControls();
            constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
            String[] attrIDs = {passwordKey};
            constraints.setReturningAttributes(attrIDs);
            NamingEnumeration answer = null;
            InitialDirContext initDirContext = null;
            try {
                initDirContext = (InitialDirContext) getLDAPContext(loggedUser);
                answer = initDirContext.search(peopleBase, uidKey + "="
                        + loginName.trim(), constraints);
                if (answer != null && answer.hasMore()) {
                    Attributes attrs = ((SearchResult) answer.next())
                            .getAttributes();
                    byte[] value = (byte[]) attrs.get(passwordKey).get();
                    if (value != null) {
                        logger.info("getPasswordFromLDAP completed Successfully");
                    }
                    return new String(value);
                }
            } catch (Exception e) {
                logger.error("Failing to getPasswordFromLDAP ", e);
                return null;
            } finally {
                if (initDirContext != null) {
                    try {
                        logger.debug("Starting of closing Ldap connection");
                        initDirContext.close();
                    } catch (NamingException e) {
                        logger.error("Failing to close Ldap connection ", e);
                    }
                    logger.debug("closing Ldap connection completed Successfully");
                }

            }
        }

        return null;
    }

    @Override
    public List<OUser> getLdapUsers(OUser loggedUser) {
        logger.debug("Starting of getLdapUsers from Ldap");
        loadLdapParams(loggedUser);
        List<OUser> ldapUsers = new ArrayList<OUser>();
        SearchControls pplSearchControls = new SearchControls();
        pplSearchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        pplSearchControls.setReturningAttributes(new String[]{uidKey,
            mailKey, snKey, passwordKey, givenNameKey});
        NamingEnumeration ppleAnswer = null;
        InitialDirContext initDirContext = null;
        try {
            initDirContext = (InitialDirContext) getLDAPContext(loggedUser);
            ppleAnswer = initDirContext.search(peopleBase, peopleFilter,
                    pplSearchControls);

            while (ppleAnswer != null && ppleAnswer.hasMore()) {
                SearchResult pplSearchResult = (SearchResult) ppleAnswer.next();
                Attributes searchResultsAttrs = pplSearchResult.getAttributes();
                String userName = searchResultsAttrs.get(uidKey).get()
                        .toString();
                String userMail = searchResultsAttrs.get(mailKey).get()
                        .toString();
                String userPassword = searchResultsAttrs.get(passwordKey).get()
                        .toString();

                OUser ouser = new OUser();
                ouser.setLoginName(userName);
                ouser.setPassword(userPassword);
                ouser.setEmail(userMail);

                ldapUsers.add(ouser);
            }

        } catch (Exception e) {
            logger.error("Failing to getLdapUsers ", e);
            return null;
        } finally {
            if (initDirContext != null) {
                try {
                    logger.debug("Starting of closing Ldap connection");
                    initDirContext.close();
                } catch (NamingException e) {
                    logger.error("Failing to close Ldap connection ", e);
                }
                logger.debug("closing Ldap connection completed Successfully");
            }

        }
        logger.info("getLdapUsers from Ldap completed Successfully");
        return ldapUsers;
    }

    @Override
    public List<RoleUser> getLdapRoleUsers(OUser loggedUser) {
        logger.debug("Starting of getLdapRoleUsers from Ldap");
        loadLdapParams(loggedUser);
        List<RoleUser> ldapRoleUsers = new ArrayList<RoleUser>();

        SearchControls groupSearchControls = new SearchControls();
        groupSearchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        groupSearchControls.setReturningAttributes(new String[]{cnKey,
            uniquememberKey});
        NamingEnumeration groupAnswer = null;
        InitialDirContext initDirContext = null;
        try {
            initDirContext = (InitialDirContext) getLDAPContext(loggedUser);
            groupAnswer = initDirContext.search(groupBase, groupFilter,
                    groupSearchControls);
            while (groupAnswer != null && groupAnswer.hasMore()) {
                SearchResult groupSearchResult = (SearchResult) groupAnswer
                        .next();
                Attributes searchResultsAttrs = groupSearchResult
                        .getAttributes();
                String groupName = searchResultsAttrs.get(cnKey).get()
                        .toString();

                NamingEnumeration all = searchResultsAttrs.get(uniquememberKey)
                        .getAll();
                while (all != null && all.hasMore()) {
                    String memberInfo = all.next().toString();
                    String memberName = memberInfo.substring(
                            memberInfo.indexOf(uidKey) + 3,
                            memberInfo.indexOf(","));
                    ORole role = new ORole();
                    role.setName(groupName);
                    RoleUser ruleUser = new RoleUser();
                    ruleUser.setOrole(role);
                    OUser user = new OUser();
                    user.setLoginName(memberName);
                    ruleUser.setOuser(user);
                    ldapRoleUsers.add(ruleUser);
                }

            }
        } catch (Exception e) {
            logger.error("Failing to getLdapRoleUsers ", e);
            return null;
        } finally {
            if (initDirContext != null) {
                try {
                    logger.debug("Starting of closing Ldap connection");
                    initDirContext.close();
                } catch (NamingException e) {
                    logger.error("Failing to close Ldap connection ", e);
                }
                logger.debug("closing Ldap connection completed Successfully");
            }

        }
        logger.info("getLdapRoleUsers from Ldap completed Successfully");
        return ldapRoleUsers;
    }

    @Override
    public List<ORole> getLdapRoles(OUser loggedUser) {
        logger.debug("Starting of getLdapRoles from Ldap");
        loadLdapParams(loggedUser);
        List<ORole> ldapRoles = new ArrayList<ORole>();
        SearchControls pplSearchControls = new SearchControls();
        pplSearchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        pplSearchControls.setReturningAttributes(new String[]{cnKey});
        NamingEnumeration ppleAnswer = null;
        InitialDirContext initDirContext = null;
        try {
            initDirContext = (InitialDirContext) getLDAPContext(loggedUser);
            ppleAnswer = initDirContext.search(groupBase, groupFilter,
                    pplSearchControls);

            while (ppleAnswer != null && ppleAnswer.hasMore()) {
                SearchResult pplSearchResult = (SearchResult) ppleAnswer.next();
                Attributes searchResultsAttrs = pplSearchResult.getAttributes();
                String groupName = searchResultsAttrs.get(cnKey).get()
                        .toString();
                ORole role = new ORole();
                role.setName(groupName);
                ldapRoles.add(role);
            }

        } catch (Exception e) {
            logger.error("Failing to getLdapRoles ", e);
            return null;
        } finally {
            if (initDirContext != null) {
                try {
                    logger.debug("Starting of closing Ldap connection");
                    initDirContext.close();
                } catch (NamingException e) {
                    logger.error("Failing to close Ldap connection ", e);
                }
                logger.debug("closing Ldap connection completed Successfully");
            }

        }
        logger.info("getLdapRoles from Ldap completed Successfully");
        return ldapRoles;
    }

    @Override
    public OFunctionResult validateLDAPConnection(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            if (!checkLdapConnection(loggedUser)) {
                ofr.addError(userMsgService.getUserMessage("LDAPConnectionError", loggedUser));
            }
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
        }
        return ofr;
    }

}
