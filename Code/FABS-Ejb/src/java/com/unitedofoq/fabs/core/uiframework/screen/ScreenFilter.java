/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.filter.OFilter;

/**
 *
 * @author mmohamed
 */
@Entity
@DiscriminatorValue(value = "SCREEN")
@ParentEntity(fields = {"screen"})
@ChildEntity(fields = {"filterFields"})
@VersionControlSpecs(versionControlType = VersionControlType.Parent)
public class ScreenFilter extends OFilter {

    @OneToOne(fetch = javax.persistence.FetchType.EAGER, optional = false)
    OScreen screen;

    public OScreen getScreen() {
        return screen;
    }

    public void setScreen(OScreen screen) {
        this.screen = screen;
    }
    private boolean initialize;

    public boolean isInitialize() {
        return initialize;
    }

    public String getInitializeDD() {
        return "ScreenFilter_initialize";
    }

    public void setInitialize(boolean initialize) {
        this.initialize = initialize;
    }
    private boolean showInsideScreen;

    public boolean isShowInsideScreen() {
        return showInsideScreen;
    }

    public String getShowInsideScreenDD() {
        return "ScreenFilter_showInsideScreen";
    }

    public void setShowInsideScreen(boolean showInsideScreen) {
        this.showInsideScreen = showInsideScreen;
    }
}
