
package com.unitedofoq.fabs.core.datatype;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class ODataTypeAttributeTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    // </editor-fold>

}
