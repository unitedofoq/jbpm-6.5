/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitybase;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mostafa
 */
@Singleton
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.entitybase.ScreenInstanceGeneratorLocal",
        beanInterface = ScreenInstanceGeneratorLocal.class)
public class ScreenInstanceGenerator implements ScreenInstanceGeneratorLocal {

    final static Logger logger = LoggerFactory.getLogger(ScreenInstanceGenerator.class);
    private int SCREENS_COUNTER = 0;

    /**
     * /**
     * Construct Screen name expression: It does the following: 1. Generate
     * Screen name expression as the following format: //
     * screenName_SCREENS_COUNTER 2. Increment the
     * {@link OBackBean SCREENS_COUNTER} variable 3. Return the generated screen
     * name expression
     */
    //@Lock(LockType.WRITE)
//    @Override
//    public String generateScreenInstId(String scrName) {
//        String screenInstId = scrName + "_" + SCREENS_COUNTER;
//        SCREENS_COUNTER++;
//        return screenInstId;
//    }
    private int serverNumber;
    private String serverNumberStr;

    @PostConstruct
    public void init() {
        logger.debug("Entering");
        serverNumber = Integer.parseInt(getServerRealIpAddress().substring(getServerRealIpAddress().lastIndexOf(".") + 1));
        serverNumberStr = String.valueOf(serverNumber);
        if (serverNumberStr.length() < 3) {
            for (int i = 0; i < serverNumberStr.length(); i++) {
                serverNumberStr = "0".concat(serverNumberStr);
            }
        }
        logger.debug("Returning");
    }

    private String getServerRealIpAddress() {
        logger.trace("Entering");
        try {
            Enumeration<NetworkInterface> n = NetworkInterface.getNetworkInterfaces();

            for (; n.hasMoreElements();) {

                NetworkInterface e = n.nextElement();
                Enumeration<InetAddress> a = e.getInetAddresses();

                for (; a.hasMoreElements();) {

                    InetAddress addr = a.nextElement();
                    if (addr.isLoopbackAddress() || addr instanceof Inet6Address) {
                        continue;
                    }
                    if (null != addr) {
                        logger.trace("Returning with: {}", addr.getHostAddress());
                    }
                    return addr.getHostAddress();
                }
            }
            logger.trace("Returning with empty string");
            return "";
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Returning with empty string");
            return "";
        }
    }
}
