/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen.report;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;

/**
 *
 * @author mostafa
 */
@Entity
@ParentEntity(fields = {"reportScreenField"})
@VersionControlSpecs(versionControlType = VersionControlSpecs.VersionControlType.Parent)
public class ScreenFieldColor extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="reportScreenField">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ReportScreenField reportScreenField;

    public ReportScreenField getReportScreenField() {
        return reportScreenField;
    }

    public void setReportScreenField(ReportScreenField reportScreenField) {
        this.reportScreenField = reportScreenField;
    }

    public String getReportScreenFieldDD() {
        return "ScreenFieldColor_reportScreenField";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="colorValue">
    @Column(nullable = false)
    private String colorValue;

    /**
     * @return the colorValue
     */
    public String getColorValue() {
        return colorValue;
    }

    /**
     * @param colorValue the colorValue to set
     */
    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    public String getColorValueDD() {
        return "ScreenFieldColor_colorValue";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="colorCondition">
    @Column
    private String colorCondition;

    /**
     * @return the colorCondition
     */
    public String getColorCondition() {
        return colorCondition;
    }

    public String getColorConditionDD() {
        return "ScreenFieldColor_colorCondition";
    }

    /**
     * @param colorCondition the colorCondition to set
     */
    public void setColorCondition(String colorCondition) {
        this.colorCondition = colorCondition;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="colorConditionValue">
    @Column
    private String colorConditionValue;

    /**
     * @return the colorConditionValue
     */
    public String getColorConditionValue() {
        return colorConditionValue;
    }

    /**
     * @param colorConditionValue the colorConditionValue to set
     */
    public void setColorConditionValue(String colorConditionValue) {
        this.colorConditionValue = colorConditionValue;
    }

    public String getColorConditionValueDD() {
        return "ScreenFieldColor_colorConditionValue";
    }
    // </editor-fold>
}
