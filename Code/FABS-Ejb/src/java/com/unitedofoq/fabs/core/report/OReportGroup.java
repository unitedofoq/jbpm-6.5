package com.unitedofoq.fabs.core.report;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;

@Entity
@ParentEntity(fields={"oreport"})
@FABSEntitySpecs(indicatorFieldExp="oreport.name")
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class OReportGroup extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="oreport">
    @JoinColumn(name="OReport_DBID")
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OReport oreport;

    public OReport getOreport() {
        return oreport;
    }

    public String getOreportDD() {
        return "OReportGroup_oreport";
    }

    public void setOreport(OReport oreport) {
        this.oreport = oreport;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="groupField">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private OReportField groupField;

    public OReportField getGroupField() {
        return groupField;
    }

    public String getGroupFieldDD() {
        return "OReportGroup_groupField";
    }

    public void setGroupField(OReportField groupField) {
        this.groupField = groupField;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="aggregateFields">
    @OneToMany
    private List<OReportField> aggregateFields;

    public List<OReportField> getAggregateFields() {
        return aggregateFields;
    }

    public String getAggregateFieldsDD() {
        return "OReportGroup_aggregateFields";
    }
    //</editor-fold>

}
