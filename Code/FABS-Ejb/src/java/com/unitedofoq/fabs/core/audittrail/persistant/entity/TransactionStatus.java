/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.audittrail.persistant.entity;

/**
 *
 * @author mostafa
 */
public enum TransactionStatus {

    Succeeded, Failed;
}
