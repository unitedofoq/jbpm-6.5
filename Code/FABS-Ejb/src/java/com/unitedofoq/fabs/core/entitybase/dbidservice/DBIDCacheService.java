package com.unitedofoq.fabs.core.entitybase.dbidservice;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.entitybase.dbidservice.DBIDCacheServiceLocal",
        beanInterface = DBIDCacheServiceLocal.class)
public class DBIDCacheService implements DBIDCacheServiceLocal {

    ConcurrentHashMap<TableDBIDKey, DBIDNode> tableIDs;
    DBIDNode headNode;
    DBIDNode lastNode;
    final int DEFAULT_DBID_CACHE_SIZE = 50;

    //FIXME: Commented from version r18.1.7 as there's no usage
//    public static final int MAX_DBID_LENGTH = 14;
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private FABSSetupLocal setupService;

    final static Logger logger = LoggerFactory.getLogger(DBIDCacheService.class);

    @PostConstruct
    public void init() {
        if (tableIDs == null) {
            tableIDs = new ConcurrentHashMap<>();
        }
    }

    @Override
    public BaseEntity createNewDbid(BaseEntity entity, OUser loggedUser) {
        Long newDbid = getEntityDbid(entity, loggedUser);
        entity.setNewDbid(newDbid);
        return entity;
    }

    private Long getEntityDbid(BaseEntity entity, OUser loggedUser) {
        boolean applySuffix = isSuffixApplyed(entity, loggedUser);
        return generateNewDbid(entity, applySuffix, loggedUser);
    }

    private Long generateNewDbid(BaseEntity entity, boolean suffixApplied, OUser loggedUser) {
        Long newDbid;
        String tableName = getTableName(entity);
        TableDBIDKey tableIDKey = TableDBIDKey.createKey(tableName, suffixApplied, loggedUser.getTenant().getId());
        if (tableIDs.containsKey(tableIDKey)) {
            DBIDNode entry = tableIDs.get(tableIDKey);
            newDbid = entry.newDbid + 1;
        } else {
            newDbid = getDBIDUsingDB(entity, loggedUser) + 1;
        }
        addNewDbid(tableIDKey, newDbid, loggedUser);
        if (suffixApplied) {
            newDbid *= 1000;
            newDbid += getSuffixValue(loggedUser);
        }
        return newDbid;
    }

    @Override
    public Long generateNewDbid(String tableName, boolean suffixApplied, OUser loggedUser) {
        Long newDbid;
        TableDBIDKey tableIDKey = TableDBIDKey.createKey(tableName, suffixApplied, loggedUser.getTenant().getId());
        if (tableIDs.containsKey(tableIDKey)) {
            DBIDNode entry = tableIDs.get(tableIDKey);
            newDbid = entry.newDbid + 1;
        } else {
            newDbid = getDBIDUsingDB(tableName, suffixApplied, loggedUser) + 1;
        }
        addNewDbid(tableIDKey, newDbid, loggedUser);
        if (suffixApplied) {
            newDbid *= 1000;
            newDbid += getSuffixValue(loggedUser);
        }
        return newDbid;
    }

    private void addNewDbid(TableDBIDKey tableIDKey, Long newDbid, OUser loggedUser) {
        DBIDNode entry;
        if (tableIDs.containsKey(tableIDKey)) {
            entry = tableIDs.get(tableIDKey);
            entry.newDbid = newDbid;
            removeNode(entry);
            addFirst(entry);
        } else {
            entry = new DBIDNode();
            entry.previosNode = null;
            entry.nextNode = null;
            entry.newDbid = newDbid;
            entry.tableIDKey = tableIDKey;
            if (tableIDs.size() > getDBIDCacheSize(loggedUser)) {
                tableIDs.remove(lastNode.tableIDKey);
                removeNode(lastNode);
                addFirst(entry);
            } else {
                addFirst(entry);
            }
        }
        tableIDs.put(tableIDKey, entry);
    }

    private void addFirst(DBIDNode node) {
        node.nextNode = headNode;
        node.previosNode = null;
        if (headNode != null) {
            headNode.previosNode = node;
        }
        headNode = node;
        if (lastNode == null) {
            lastNode = headNode;
        }
    }

    private void removeNode(DBIDNode node) {

        if (node.previosNode != null) {
            node.previosNode.nextNode = node.nextNode;
        } else {
            headNode = node.nextNode;
        }

        if (node.nextNode != null) {
            node.nextNode.previosNode = node.previosNode;
        } else {
            lastNode = node.previosNode;
        }
    }

    private Long getDBIDUsingDB(BaseEntity entity, OUser loggedUser) {
        try {
            String tableName = getTableName(entity);
            return getDBIDUsingDB(tableName, isSuffixApplyed(entity, loggedUser), loggedUser);
        } catch (EntityExistsException |
                EntityNotFoundException | NonUniqueResultException | NoResultException |
                OptimisticLockException | RollbackException | TransactionRequiredException e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    private Long getDBIDUsingDB(String tableName, boolean suffixApplied, OUser loggedUser) {
        Object maxIDObj = 0;
        try {
            String selectSQL = "Select ";
            if (suffixApplied) {
                selectSQL += "max(dbid / 1000)";
            } else {
                selectSQL += "max(dbid)";
            }
            selectSQL += " from " + tableName;
            maxIDObj = oem.executeEntityNativeQuery(selectSQL, loggedUser);
            if (maxIDObj != null) {
                long maxID;
                if (maxIDObj instanceof BigDecimal) {
                    maxID = ((BigDecimal) maxIDObj).longValue();
                } else {
                    maxID = (long) maxIDObj;
                }
                maxIDObj = maxID;
            }
        } catch (UserNotAuthorizedException | EntityExistsException |
                EntityNotFoundException | NonUniqueResultException | NoResultException |
                OptimisticLockException | RollbackException | TransactionRequiredException e) {
            logger.error(e.getMessage());
        }
        return maxIDObj == null ? 0l : Long.valueOf(maxIDObj.toString());
    }

    private String getTableName(BaseEntity baseEntity) {
        return baseEntity.getEntityTableName().get(0);
    }

    private boolean isSuffixApplyed(BaseEntity entity, OUser loggedUser) {
        return !(entity.getVCModuleExpression() == null || entity.getVCModuleExpression().equals("")) && getSuffixValue(loggedUser) != 0;
    }

    private long getSuffixValue(OUser loggedUser) {
        Object suffix = setupService.getKeyValue("DBIDSUFFIX", loggedUser);
        return suffix == null ? 0 : Long.valueOf(suffix.toString());
    }

    private int getDBIDCacheSize(OUser loggedUser) {
        String dbidCacheSizeStr = setupService.getKeyValue("DBID_CACHE_SIZE", loggedUser);
        int dbidCacheSize = DEFAULT_DBID_CACHE_SIZE;
        if (!dbidCacheSizeStr.equals("")) {
            dbidCacheSize = Integer.valueOf(dbidCacheSizeStr);
        }
        return dbidCacheSize;
    }

    @Override
    public OFunctionResult refreshDbidCache(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        try {
            for (Map.Entry<TableDBIDKey, DBIDNode> tableID : tableIDs.entrySet()) {
                TableDBIDKey tableDBIDKey = tableID.getKey();
                DBIDNode dbidNode = tableID.getValue();
                long newDbid = getDBIDUsingDB(tableDBIDKey.tableName, tableDBIDKey.suffixApplied, loggedUser);
                if (newDbid > dbidNode.newDbid) {
                    dbidNode.newDbid = newDbid;
                    tableIDs.put(tableDBIDKey, dbidNode);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return new OFunctionResult();
    }
}

class DBIDNode {

    Long newDbid;
    TableDBIDKey tableIDKey;
    DBIDNode previosNode;
    DBIDNode nextNode;
}

class TableDBIDKey {

    boolean suffixApplied;
    String tableName;
    long tenantId;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.tableName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TableDBIDKey other = (TableDBIDKey) obj;
        if (!Objects.equals(this.tableName, other.tableName)
                && !Objects.equals(this.tenantId, other.tenantId)) {
            return false;
        }
        return true;
    }

    public static TableDBIDKey createKey(String tableName, boolean suffixApplied, long tenantId) {
        TableDBIDKey key = new TableDBIDKey();
        key.tableName = tableName;
        key.suffixApplied = suffixApplied;
        key.tenantId = tenantId;
        return key;
    }
}
