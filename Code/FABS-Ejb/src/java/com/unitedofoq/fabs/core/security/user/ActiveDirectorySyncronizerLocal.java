/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.security.user;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import javax.ejb.Local;

/**
 *
 * @author mostafa
 */
@Local
public interface ActiveDirectorySyncronizerLocal {
     public OFunctionResult SycOnDemad(ODataMessage odm, OFunctionParms params, OUser loggedUser);
     
    
}
