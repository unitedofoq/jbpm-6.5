/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.businessrule;

import org.drools.core.spi.Activation;
import org.kie.api.runtime.rule.AgendaFilter;
import org.kie.api.runtime.rule.Match;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author aelzaher
 */
public class OAgendaFilter implements AgendaFilter {

    String ruleName;
    final static Logger logger = LoggerFactory.getLogger(OAgendaFilter.class);

    public OAgendaFilter(String ruleName) {
        this.ruleName = ruleName;
        logger.debug("Rulename: {}", ruleName);
    }

    @Override
    public boolean accept(Match match) {
        logger.debug("Entering");
        if (ruleName == null || ruleName.isEmpty()) {
            logger.debug("Returning");
            return true;
        }
        if (match.getRule().getName().equalsIgnoreCase(ruleName)) {
            logger.debug("Returning");
            return true;
        }
        logger.debug("Returning");
        return false;
    }
}
