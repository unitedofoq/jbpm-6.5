/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.log.service;


import com.unitedofoq.fabs.core.log.logger.ILogger;

public class LogFactory {

    private static LogFactory instance = null;

    static {
        instance = new LogFactory();
    }

    private LogFactory() {
    }

    public static LogFactory getSingleInstance() {
        return instance;
    }

    public ILogger getLogger(LogSetupConfiguration setupConf) {

        ILogger logger = null;
        try {
            //here we will switch loggers APIs

            Class cls = Class.forName(setupConf.getClassFullName().toString());
           
            Object obj = cls.newInstance();
            logger = (ILogger) obj;
            //logger.setConfigurationFile(setupConf.getConfigurationFileName());

            return logger;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}