/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.cash;

import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

/**
 *
 * @author mmasoud
 */
@Local
public interface CacheServiceLocal {
    
    public void cache(String key, Object value, OUser loggedUser);
    
    public void decache(String key, OUser loggedUser);
    
    public Object getFromCache(String key, OUser loggedUser);
}
