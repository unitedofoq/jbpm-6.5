/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.function;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


import com.unitedofoq.fabs.core.datatype.ODataMessage;

import com.unitedofoq.fabs.core.usermessage.UserMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author melsayed
 */
public class OFunctionResult implements Serializable {

    final static Logger logger = LoggerFactory.getLogger(OFunctionResult.class);
    // <editor-fold defaultstate="collapsed" desc="errors">
    private List<UserMessage> errors;

    public List<UserMessage> getErrors() {
        if (errors == null) {
            errors = new ArrayList<UserMessage>();
        }
        return errors;
    }

    public void setErrors(List<UserMessage> errors) {
        this.errors = errors;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="warnings">
    private List<UserMessage> warnings;

    public List<UserMessage> getWarnings() {
        if (warnings == null) {
            warnings = new ArrayList<UserMessage>();
        }
        return warnings;
    }

    public void setWarnings(List<UserMessage> warnings) {
        this.warnings = warnings;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="successes">
    private List<UserMessage> successes;

    public List<UserMessage> getSuccesses() {
        if (successes == null) {
            successes = new ArrayList<UserMessage>();
        }
        return successes;
    }

    public void setSuccesses(List<UserMessage> successes) {
        this.successes = successes;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="return value(s)">
    List returnValues;

    public List getReturnValues() {
        if (returnValues == null) {
            returnValues = new ArrayList();
        }
        return returnValues;
    }

    public void setReturnValues(List returnValues) {
        this.returnValues = returnValues;
    }

    public void addReturnValue(Object value) {
        getReturnValues().add(value);
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="returned message">
    /**
     * The Data Message to be set in the end of the java function to be returned
     * to the screen
     */
    ODataMessage returnedDataMessage;

    public ODataMessage getReturnedDataMessage() {
        return returnedDataMessage;
    }

    public void setReturnedDataMessage(ODataMessage returnedDataMessage) {
        this.returnedDataMessage = returnedDataMessage;
    }

    // </editor-fold>
    public void addError(UserMessage msg) {
        logger.debug("Entering");
        if (msg == null) {
            logger.debug("Returning");
            return;
        }
        // <editor-fold defaultstate="collapsed" desc="Log">
        if (msg.getDbid() == 0) {
            logger.warn("Need To Load The Message From DB, Not Using HardCoded One");
            logger.trace("Text: {}", msg.getMessageText());
        }
        // </editor-fold>
        getErrors().add(msg);
        logger.debug("Returning");
    }

    public void addError(UserMessage msg, Exception ex) {
        logger.debug("Entering");
        if (msg == null) {
            logger.debug("Returning");
            return;
        }
        // <editor-fold defaultstate="collapsed" desc="Log">
        if (msg.getDbid() == 0) {
            logger.warn("Need To Load The Message From DB, Not Using HardCoded One");
            logger.trace("Text: {}", msg.getMessageText());
        }
        // </editor-fold>
        msg.setException(ex);
        getErrors().add(msg);
        logger.debug("Returning");
    }

    public void addWarning(UserMessage msg) {
        logger.debug("Entering");
        if (msg == null) {
            logger.debug("Returning");
            return;
        }
        // <editor-fold defaultstate="collapsed" desc="Log">
        if (msg.getDbid() == 0) {
            logger.warn("Need To Load The Message From DB, Not Using HardCoded One");
            logger.trace("Text", msg.getMessageText());
        }
        // </editor-fold>
        logger.debug("Returning");
        getWarnings().add(msg);
    }

    public void addSuccess(UserMessage msg) {
        logger.debug("Entering");
        if (msg == null) {
            logger.debug("Returning");
            return;
        }
        // <editor-fold defaultstate="collapsed" desc="Log">
        if (msg.getDbid() == 0) {
            logger.warn("Need To Load The Message From DB, Not Using HardCoded One");
            logger.trace("Text", msg.getMessageText());
        }
        // </editor-fold>
        logger.debug("Returning");
        getSuccesses().add(msg);
    }

    /**
     * Appends the messages & returned values. IT DOES NOT APPEND
     * returnedDataMessage
     *
     * @param oFR
     */
    public void append(OFunctionResult oFR) {
        if (oFR == null) {
            logger.debug("Returning");
            return;
        }
        getErrors().addAll(oFR.getErrors());
        getWarnings().addAll(oFR.getWarnings());
        getSuccesses().addAll(oFR.getSuccesses());
        getReturnValues().addAll(oFR.getReturnValues());
    }

}
