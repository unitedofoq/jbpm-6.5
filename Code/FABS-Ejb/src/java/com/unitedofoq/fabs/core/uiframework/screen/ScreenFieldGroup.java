/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.FetchType;

/**
 *
 * @author bassem
 */
@Entity
@ParentEntity(fields = {"oscreen"})
@VersionControlSpecs(versionControlType = VersionControlType.Parent)
public class ScreenFieldGroup extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="expandable">

    private boolean expandable;

    public String getExpandableDD() {
        return "ScreenFieldGroup_expandable";
    }

    public boolean isExpandable() {
        return expandable;
    }

    public void setExpandable(boolean expandable) {
        this.expandable = expandable;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="header">
    @Translatable(translationField = "headerTranslated")
    private String header;

    public String getHeaderTranslatedDD() {
        return "ScreenFieldGroup_header";
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }
    @Translation(originalField = "header")
    @Transient
    protected String headerTranslated;

    public String getHeaderTranslated() {
        return headerTranslated;
    }

    public void setHeaderTranslated(String headerTranslated) {
        this.headerTranslated = headerTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="expInfos">
    private int numberOfColumns;

    public String getNumberOfColumnsDD() {
        return "ScreenFieldGroup_numberOfColumns";
    }

    public int getNumberOfColumns() {
        return numberOfColumns;
    }

    public void setNumberOfColumns(int numberOfColumns) {
        this.numberOfColumns = numberOfColumns;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="oscreen">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private SingleEntityScreen oscreen;

    public String getOscreenDD() {
        return "ScreenFieldGroup_oscreen";
    }

    public SingleEntityScreen getOscreen() {
        return oscreen;
    }

    public void setOscreen(SingleEntityScreen oscreen) {
        this.oscreen = oscreen;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="groupFields">
    //fetch=FetchType.EAGER added by Rehab to sove java8 issue with JPA
    @OneToMany(mappedBy = "screenFieldGroup", fetch=FetchType.EAGER)
    private List<ScreenField> groupFields;

    public String getGroupFieldsDD() {
        return "ScreenFieldGroup_groupFields";
    }

    public List<ScreenField> getGroupFields() {
        return groupFields;
    }

    public void setGroupFields(List<ScreenField> groupFields) {
        this.groupFields = groupFields;
    }
    // </editor-fold>   
    // <editor-fold defaultstate="collapsed" desc="sortIndex">
    private int sortIndex;

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getSortIndexDD() {
        return "ScreenFieldGroup_sortIndex";
    }
    // </editor-fold>   
}
