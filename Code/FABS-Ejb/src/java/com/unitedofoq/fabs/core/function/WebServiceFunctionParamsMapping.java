/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.function;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;

/**
 *
 * @author htawab
 */
@Entity
@Table(name = "WEBSERVICEFUNPARAMSMAPPING")
@ParentEntity(fields = "webServiceFunction")
@VersionControlSpecs(versionControlType = VersionControlType.Parent)
public class WebServiceFunctionParamsMapping extends BaseEntity {

    String parameterName;
    String fldExpr;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    WebServiceFunction webServiceFunction;

    public String getFldExpr() {
        return fldExpr;
    }

    public String getFldExprDD() {
        return "WebServiceFunctionParamsMapping_fldExpr";
    }

    public String getWebServiceFunctionDD() {
        return "WebServiceFunctionParamsMapping_webServiceFunction";
    }

    public void setFldExpr(String fldExpr) {
        this.fldExpr = fldExpr;
    }

    public WebServiceFunction getWebServiceFunction() {
        return webServiceFunction;
    }

    public void setWebServiceFunction(WebServiceFunction webServiceFunction) {
        this.webServiceFunction = webServiceFunction;
    }

    public String getParameterName() {
        return parameterName;
    }

    public String getParameterNameDD() {
        return "WebServiceFunctionParamsMapping_parameterName";
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    @Override
    public String toString() {
        return "com.unitedofoq.fabs.core.function.WebServiceFunctionParamsMapping[id=" + dbid + "]";
    }
}
