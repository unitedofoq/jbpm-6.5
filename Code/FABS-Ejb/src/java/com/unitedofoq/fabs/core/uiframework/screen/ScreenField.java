package com.unitedofoq.fabs.core.uiframework.screen;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author arezk
 */
//Overview: Represents the data for each field that will be displayed in the screen.
@Entity
@ParentEntity(fields = {"oScreen"})
@DiscriminatorValue(value = "SCREEN")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "SFTYPE", discriminatorType = DiscriminatorType.STRING)
@FABSEntitySpecs(indicatorFieldExp = "fieldExpression", personalizableFields = {"controlSize", "ddTitleOverride", "sortIndex", "prsnlzdRemoved"})
@VersionControlSpecs(versionControlType = VersionControlType.Parent)
public class ScreenField extends BaseEntity {
    final static Logger logger = LoggerFactory.getLogger(ScreenField.class);
    // <editor-fold defaultstate="collapsed" desc="groupFields">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScreenFieldGroup screenFieldGroup;

    public String getScreenFieldGroupDD() {
        return "ScreenField_screenControlsGroup";
    }

    public ScreenFieldGroup getScreenFieldGroup() {
        return screenFieldGroup;
    }

    public void setScreenFieldGroup(ScreenFieldGroup screenFieldGroup) {
        this.screenFieldGroup = screenFieldGroup;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="changeFunction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OFunction changeFunction;

    public OFunction getChangeFunction() {
        return changeFunction;
    }

    public String getChangeFunctionDD() {
        return "ScreenField_changeFunction";
    }

    public void setChangeFunction(OFunction changeFunction) {
        this.changeFunction = changeFunction;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="oScreen">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private SingleEntityScreen oScreen;

    public SingleEntityScreen getOScreen() {
        return oScreen;
    }

    public void setOScreen(SingleEntityScreen oScreen) {
        this.oScreen = oScreen;
    }
    // </editor-fold>
    @Column(nullable = false)
    private String fieldExpression;
//Editable or not
    @Column(nullable = false)
    private boolean editable;
//The sorting index
    @Column(nullable = false)
    private int sortIndex;
//Mandatory in screen or not
    @Column(nullable = false)
    private boolean mandatory;
//Override the title
    @Translatable(translationField = "ddTitleOverrideTranslated")
    private String ddTitleOverride;
    @Translation(originalField = "ddTitleOverride")
    @Transient
    private String ddTitleOverrideTranslated;

    public String getDdTitleOverrideTranslated() {
        return ddTitleOverrideTranslated;
    }

    public void setDdTitleOverrideTranslated(String ddTitleOverrideTranslated) {
        this.ddTitleOverrideTranslated = ddTitleOverrideTranslated;
    }
    private int controlSize;
//Information about the control in the Data Dictionary
//Relation Many-To-One(DD)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private DD dd = null;

    /* DD NAMES GETTERS */
    public String getDdTitleOverrideTranslatedDD() {
        return "ScreenField_ddTitleOverride";
    }

    public String getEditableDD() {
        return "ScreenField_editable";
    }

    public String getFieldExpressionDD() {
        return "ScreenField_fieldExpression";
    }

    public String getMandatoryDD() {
        return "ScreenField_mandatory";
    }

    public String getSortIndexDD() {
        return "ScreenField_sortIndex";
    }

    public String getDdDD() {
        return "ScreenField_dd";
    }

    public String getPrsnlzdRemovedDD() {
        return "ScreenField_prsnlzdRemoved";
    }

    public String getDdTitleOverride() {
        return ddTitleOverride;
    }

    public void setDdTitleOverride(String ddTitleOverride) {
        this.ddTitleOverride = ddTitleOverride;
    }

    public String getFieldExpression() {
        return fieldExpression;
    }

    public void setFieldExpression(String fieldExpression) {
        if (fieldExpression != null && !fieldExpression.equals(this.fieldExpression)) {
            expInfos = null;
        }
        this.fieldExpression = fieldExpression;
    }

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    /**
     * @return the dd
     */
    public DD getDd() {
        return dd;
    }

    /**
     * @param dd the dd to set
     */
    public void setDd(DD dd) {
        this.dd = dd;
    }

    public int getControlSize() {
        return controlSize;
    }

    public void setControlSize(int controlSize) {
        this.controlSize = controlSize;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }
    // <editor-fold defaultstate="collapsed" desc="expInfos">
    /**
     * Filled by {@link #getExpInfos()}
     */
    @Transient
    private List<ExpressionFieldInfo> expInfos = null;

    /**
     * Get the list of parsed fieldExpression using the {@link #oScreen}
     * actOnEntity
     *
     * @return <br> ExpressionFieldInfo List <br> Null: Error
     */
    public List<ExpressionFieldInfo> getExpInfos(String screenActOnEntityClassPath) {
        logger.debug("Entering");
        if (expInfos != null) // Previously Parsed, cached
        // Return it
        {
            logger.debug("Returning");
            return expInfos;
        }

        // ___________
        // Validations
        //
        if (fieldExpression == null) {
            logger.debug("Returning with Null");
            return null;
        }
        Class actOnEntityCls;
        try {
            actOnEntityCls = Class.forName(screenActOnEntityClassPath);
        } catch (Exception ex) {
            logger.error("Exception thown: ",ex);
            logger.debug("Returning with Null");
            return null;
        }

        // _________________________
        // Parse the fieldExpression
        //
        expInfos = BaseEntity.parseFieldExpression(actOnEntityCls, fieldExpression);
        logger.debug("Returning");
        return expInfos;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Personalization Fields : prsnlzdOriginal_DBID, prsnlzdUser_DBID, prsnlzdRemoved">
    @Column(nullable = true)
    private long prsnlzdOriginal_DBID;
    @Column(nullable = true)
    private long prsnlzdUser_DBID;
    @Column(nullable = true)
    private boolean prsnlzdRemoved;

    /**
     * @return the prsnlzdOriginal_DBID
     */
    public long getPrsnlzdOriginal_DBID() {
        return prsnlzdOriginal_DBID;
    }

    /**
     * @param prsnlzdOriginal_DBID the prsnlzdOriginal_DBID to set
     */
    public void setPrsnlzdOriginal_DBID(long prsnlzdOriginal_DBID) {
        this.prsnlzdOriginal_DBID = prsnlzdOriginal_DBID;
    }

    /**
     * @return the prsnlzdUser_DBID
     */
    public long getPrsnlzdUser_DBID() {
        return prsnlzdUser_DBID;
    }

    /**
     * @param prsnlzdUser_DBID the prsnlzdUser_DBID to set
     */
    public void setPrsnlzdUser_DBID(long prsnlzdUser_DBID) {
        this.prsnlzdUser_DBID = prsnlzdUser_DBID;
    }

    /**
     * @return the prsnlzdRemoved
     */
    public boolean isPrsnlzdRemoved() {
        return prsnlzdRemoved;
    }

    /**
     * @param prsnlzdRemoved the prsnlzdRemoved to set
     */
    public void setPrsnlzdRemoved(boolean prsnlzdRemoved) {
        this.prsnlzdRemoved = prsnlzdRemoved;
    }
    // </editor-fold>
    private boolean toggleDim;

    public boolean isToggleDim() {
        return toggleDim;
    }

    public void setToggleDim(boolean toggleDim) {
        this.toggleDim = toggleDim;
    }
    
    public String getToggleDimDD() {
        return "ScreenField_toggleDim";
    }
    
    @OneToMany(mappedBy="screenField", fetch= FetchType.LAZY)
    protected List<ScreenFieldValue> screenFieldValues = null;

    public List<ScreenFieldValue> getScreenFieldValues() {
        return screenFieldValues;
    }

    public void setScreenFieldValues(List<ScreenFieldValue> screenFieldValues) {
        this.screenFieldValues = screenFieldValues;
    }
    
}
