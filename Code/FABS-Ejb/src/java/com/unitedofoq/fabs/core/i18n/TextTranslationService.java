package com.unitedofoq.fabs.core.i18n;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.BaseEntityMetaData;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import com.unitedofoq.fabs.core.entitybase.EntityAuditTrail;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.export.DataExportRemote;
import com.unitedofoq.fabs.core.module.ModuleServiceRemote;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.session.ScreenSessionInfo;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.OneToOne;
import javax.persistence.OptimisticLockException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.i18n.TextTranslationServiceRemote",
        beanInterface = TextTranslationServiceRemote.class)
public class TextTranslationService implements TextTranslationServiceRemote {

    @EJB
    OEntityManagerRemote oem;
    @EJB
    ModuleServiceRemote moduleService;
    @EJB
    FABSSetupLocal fABSSetupLocal;
    @EJB
    DataExportRemote dataExport;
    public final static long SYS_DEFAULT_LANG = OUser.ENGLISH;

    final static Logger logger = LoggerFactory.getLogger(TextTranslationService.class);

    /**
     *
     * @param transEntityName
     * @param langdbid
     * @param filteredField
     * @param filterValue
     * @param loggedUser
     * @return
     */
    @Override
    public List<Long> getFilterEntitiesDBIDs(String transEntityName, List<String> conds,
            List<String> sorts, OUser loggedUser, String fieldClassName) {
        logger.debug("Entering");
        try {

            Set subEntities = null;
            if (fieldClassName != null) {
                Class<?> entity = Class.forName(fieldClassName);
                Reflections reflections = new Reflections();
                subEntities = reflections.getSubTypesOf(entity);
            }

            StringBuilder query = new StringBuilder("");
            if (subEntities == null || subEntities.isEmpty()) {

                query.append(" SELECT entity.entityDBID");
                query.append(" FROM ").append(transEntityName).append(" entity");
                query.append(" WHERE ");
                for (int i = 0; i < conds.size(); i++) {

                    if (i != 0) {

                        query.append(" AND ");
                    }
                    query.append("entity.").append(conds.get(i));
                }
                StringBuilder orderBy = new StringBuilder("");
                if (sorts != null && !sorts.isEmpty()) {

                    for (int i = 0; i < sorts.size(); i++) {

                        orderBy.append(i != 0 ? ",entity." : "entity.").append(sorts.get(i));
                    }
                    query.append(" ORDER BY ").append(orderBy);
                }
            } else {

                int index = 0;
                for (Object subEntity : subEntities) {

                    String entityNameParts[] = subEntity.toString().split("\\.");
                    if (index > 0) {

                        query.append(" UNION");
                    }
                    query.append(" SELECT entity.entityDBID");
                    query.append(" FROM ").append(entityNameParts[entityNameParts.length - 1]).append("Translation entity");
                    query.append(" WHERE ");
                    for (int i = 0; i < conds.size(); i++) {

                        if (i != 0) {

                            query.append(" AND ");
                        }
                        query.append("entity.").append(conds.get(i));
                    }
                    StringBuilder orderBy = new StringBuilder("");
                    if (sorts != null && !sorts.isEmpty()) {

                        for (int i = 0; i < sorts.size(); i++) {

                            orderBy.append(i != 0 ? ",entity." : "entity.").append(sorts.get(i));
                        }
                        query.append(" ORDER BY ").append(orderBy);
                    }
                    index++;
                }
            }
            logger.trace("Query: {}", query);
            logger.debug("Returning");
            return (List<Long>) oem.executeEntityListQuery(query.toString(), loggedUser);
        } catch (ClassNotFoundException | UserNotAuthorizedException | EntityExistsException | EntityNotFoundException | NonUniqueResultException | NoResultException | OptimisticLockException | RollbackException | TransactionRequiredException ex) {  //  ERROR IN RETRIEVING TEXT TRANSLATION ENTITY...
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning with Null");
        return null;
    }

    /**
     * gets the translation for the passed entity field.
     *
     * @param fields The 'java.reflect.Field' variable
     * @param entities The entity instance.
     * @param fieldExpression The expression representing the translatable
     * entity field.
     * @param loggedUser The user requesting the translation.
     *
     * @return The textTranslation for the passed entity field.
     */
    @Override
    public List<BaseEntity> loadEntityListTranslation(List<BaseEntity> entities, OUser loggedUser)
            throws TextTranslationException {
        logger.debug("Entering");
        //  'loggedUser' cannot be NULL because the TextTranslation is
        //  retrieved based on the user's language
        if (loggedUser == null) {
            throw new TextTranslationException("NULL user was passed to "
                    + "TextTranslationService.getFieldTranslation().");
        }
        //  'entity' cannot be NULL because translations cannot be retrieved
        //  for NULL entities
        if (entities == null) {
            throw new TextTranslationException("NULL entity was passed to"
                    + "TextTranslationService.getFieldTranslation().");
        }

        if (entities.isEmpty()) {
            logger.debug("Returning");
            return entities;
        }

        BaseEntity entity = entities.get(0);
        List<Field> entityTranslatableFields = entity.getTranslatableFields();
        if (entity.getTranslatableFields().isEmpty()) {
            logger.debug("Returning");
            return entities;
        }
        try {
            if (loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).getSvalue())) {
                for (int i = 0; i < entityTranslatableFields.size(); i++) {
                    BaseEntity.setValueInEntity(entity, entityTranslatableFields.get(i).getName(),
                            BaseEntity.getValueFromEntity(entity, entityTranslatableFields.get(i).getAnnotation(
                                            Translatable.class).translationField()));
                }
            } else {
                List result = null;
                List<String> cond = new ArrayList<String>();
                String dbids = "";
                for (int i = 0; i < entities.size(); i++) {
                    if (i != 0) {
                        dbids += ",";
                    }
                    dbids += entities.get(i).getDbid();
                }
                cond.add("entityDBID IN (" + dbids + ")");
                cond.add("language.dbid=" + loggedUser.getFirstLanguage().getDbid());

                List<BaseEntityTranslation> translations = null;
                if (result == null) {
                    translations = new ArrayList<BaseEntityTranslation>();
                } else {
                    translations = (List<BaseEntityTranslation>) result;
                }
                for (int i = 0; i < entities.size(); i++) {
                    BaseEntityTranslation translation = null;
                    for (int j = 0; j < translations.size(); j++) {
                        if (translations.get(j).getEntityDBID() == entities.get(i).getDbid()) {
                            translation = translations.get(j);
                        }
                    }
                    if (translation != null) {
                        for (int k = 0; k < entityTranslatableFields.size(); k++) {
                            Object value = BaseEntity.getValueFromEntity(translation,
                                    entityTranslatableFields.get(k).getName());
                            if (value == null) {
                                value = BaseEntity.getValueFromEntity(entity,
                                        entityTranslatableFields.get(k).getName());
                            }
                            BaseEntity.setValueInEntity(entity, entityTranslatableFields.get(k).getAnnotation(
                                    Translatable.class).translationField(), value);
                        }
                    } else {
                        for (int k = 0; k < entityTranslatableFields.size(); k++) {
                            BaseEntity.setValueInEntity(entity, entityTranslatableFields.get(k).getAnnotation(
                                    Translatable.class).translationField(), BaseEntity.getValueFromEntity(
                                            entity, entityTranslatableFields.get(k).getName()));
                        }
                    }
                }

            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return entities;
    }

    @Override
    public boolean entityNeedsTranslation(BaseEntity entity, OUser loggedUser) {
        logger.debug("Entering");
        if (entity == null) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Null Entity Passed");
            // </editor-fold>
            logger.debug("Returning");
            return false;
        }
        if (loggedUser == null) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Null Logged User Passed. Entity: {}", entity);
            // </editor-fold>
            logger.debug("Returning");
            return false;
        }
        if (loggedUser.getFirstLanguage() == null) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Null Logged User Language. Entity: {}", entity);
            // </editor-fold>
            logger.debug("Returning");
            return false;
        }
        List<Field> entityTranslatableFields = entity.getTranslatableFields();
        if (entityTranslatableFields != null) {
            if (entityTranslatableFields.size() > 0) {
                logger.debug("Returning");
                return true;
            }
        }
        logger.debug("Returning");
        return false;
    }

    @Override
    public BaseEntity loadEntityTranslation(BaseEntity entity, OUser loggedUser)
            throws TextTranslationException {
        logger.debug("Entering");
        logger.debug("Returning");
        return loadEntityTranslation(entity, false, loggedUser);
    }

    @Override
    public BaseEntity forceLoadEntityTranslation(BaseEntity entity, OUser loggedUser)
            throws TextTranslationException {
        logger.debug("Entering");
        logger.debug("Returning");
        return loadEntityTranslation(entity, true, loggedUser);
    }

    private BaseEntity loadEntityTranslation(BaseEntity entity, boolean forceTranslation, OUser loggedUser)
            throws TextTranslationException {
        logger.trace("Entering");
        //  'loggedUser' cannot be NULL because the TextTranslation is
        //  retrieved based on the user's language
        if (loggedUser == null) {
            throw new TextTranslationException("NULL user passed to "
                    + "TextTranslationService.loadEntityTranslation().");
        }
        //  'entity' cannot be NULL because translations cannot be retrieved
        //  for NULL entities
        if (entity == null) {
            throw new TextTranslationException("NULL entity was passed to"
                    + "TextTranslationService.loadEntityTranslation().");
        }

        if (entity instanceof EntityAuditTrail || entity instanceof ScreenSessionInfo
                || entity instanceof TextTranslation) {
            logger.trace("Returning");
            return entity;
        }
        /* Get entity fields, and then for each @Translatable field,
         * retrieve the corresponding translation
         */
        try {
            List<Field> entityTranslatableFields = entity.getTranslatableFields();
            if (entity.getTranslatableFields().isEmpty()) {
                logger.trace("Returning");
                return entity;
            }
            if (loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).getSvalue()) && !forceTranslation) {
                for (int i = 0; i < entityTranslatableFields.size(); i++) {
                    BaseEntity.setValueInEntity(entity, entityTranslatableFields.get(i).getAnnotation(
                            Translatable.class).translationField(), BaseEntity.getValueFromEntity(
                                    entity, entityTranslatableFields.get(i).getName()));
                }
            } else {
                List result = null;
                List<String> cond = new ArrayList<String>();
                cond.add("entityDBID=" + entity.getDbid());
                if (forceTranslation) {
                    cond.add("language.dbid=" + loggedUser.getSecondLanguage().getDbid());
                } else {
                    cond.add("language.dbid=" + loggedUser.getFirstLanguage().getDbid());
                }

                result = entityFromLocal(Class.forName(
                        entity.getClass().getName() + "Translation"), cond, null, null, loggedUser);

                if (null != result && !result.isEmpty()) {
                    Hashtable<String, List<Field>> fieldsInSameTable = null;

                    fieldsInSameTable = BaseEntity.getClassFieldsInSameTable(
                            Class.forName(entity.getClass().getName() + "Translation"));

                    List<Field> fields = new ArrayList<Field>();
                    for (Map.Entry<String, List<Field>> entry : fieldsInSameTable.entrySet()) {
                        List<Field> fieldsInTable = entry.getValue();
                        fields.addAll(fieldsInTable);
                    }
                    Object[] translation = (Object[]) result.get(0);
                    for (int i = 0; i < entityTranslatableFields.size(); i++) {
                        Object value = null;
                        for (int j = 0; j < fields.size(); j++) {
                            if (fields.get(j).getName().equals(entityTranslatableFields.get(i).getName())) {
                                value = translation[j];
                                break;
                            }
                        }
                        if (value == null) {
                            value = BaseEntity.getValueFromEntity(entity,
                                    entityTranslatableFields.get(i).getName());
                        }
                        BaseEntity.setValueInEntity(entity, entityTranslatableFields.get(i).getAnnotation(
                                Translatable.class).translationField(), value);
                    }
                } else {
                    for (int i = 0; i < entityTranslatableFields.size(); i++) {
                        BaseEntity.setValueInEntity(entity, entityTranslatableFields.get(i).getAnnotation(
                                Translatable.class).translationField(), BaseEntity.getValueFromEntity(
                                        entity, entityTranslatableFields.get(i).getName()));
                    }
                }
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            if (null != entity) {
                logger.trace("Entity: {}", entity.getClassName());
            }
        }
        //  return entity after being translated
        logger.trace("Returning");
        return entity;
    }

    /**
     * saves the text translations for the fields of the passed entity instance
     * in the desired language.
     *
     * @param entity The entity instance for which the field translations will
     * be saved.
     * @param language The UDC of the desired language.
     * @param loggedUser The user requesting the save.
     */
    @Override
    public BaseEntity preEntityTranslationSave(BaseEntity entity, OUser loggedUser) {
        logger.debug("Entering");
        Field field;
        String translationFieldname;
        String transFldValue;
        try {
            if (entity instanceof EntityAuditTrail || entity instanceof ScreenSessionInfo
                    || entity instanceof TextTranslation) {
                logger.debug("Returning");
                return entity;
            }
            List<String> childFieldNames = BaseEntity.getChildEntityFieldName(entity.getClass().getName());
            for (String childFldName : childFieldNames) {
                Field childFld = BaseEntity.getClassField(entity.getClass(), childFldName);
                if (childFld == null) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.warn("Child Field Not Found. Won't Have DBID Generated Properly. Parent: {}, Child Field Entity: {}", entity, childFldName);
                    // </editor-fold>
                    continue;
                }
                if (childFld.getAnnotation(OneToOne.class) != null) {
                    if (((OneToOne) childFld.getAnnotation(OneToOne.class)).cascade() != null
                            || ((OneToOne) childFld.getAnnotation(OneToOne.class)).cascade().length > 0) {
                        BaseEntity childEntity = (BaseEntity) BaseEntity.getValueFromEntity(entity, childFldName);
                        if (null == childEntity) {
                            continue;
                        }
                        preEntityTranslationSave(childEntity, loggedUser);
                    }
                }
            }
            List<Field> entityTranslatableFields = entity.getTranslatableFields();
            for (int i = 0; i < entityTranslatableFields.size(); i++) {
                field = entityTranslatableFields.get(i);
                translationFieldname = field.getAnnotation(
                        Translatable.class).translationField();
                transFldValue = (String) entity.invokeGetter(translationFieldname);
                if (loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                        fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).getSvalue())) {
                    if (transFldValue != null) {
                        BaseEntity.setValueInEntity(entity, field.getName(), transFldValue);
                    } else {
                        BaseEntity.setValueInEntity(entity, translationFieldname,
                                BaseEntity.getValueFromEntity(entity, field.getName()));
                    }
                } else {
                    if (BaseEntity.getValueFromEntity(entity, field.getName()) == null) {
                        BaseEntity.setValueInEntity(entity, field.getName(), transFldValue);
                    }
                    if (transFldValue == null) {
                        BaseEntity.setValueInEntity(entity, translationFieldname,
                                BaseEntity.getValueFromEntity(entity, field.getName()));
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            if (null != entity) {
                logger.trace("Entity: {}", entity.getClassName());
            }
        }
        logger.debug("Entering");
        return entity;
    }

    @Override
    public void saveEntityTranslation(BaseEntity entity, OUser loggedUser) {
        //  'loggedUser' cannot be NULL because the TextTranslation is
        //  saved based on the user's language
        logger.debug("Entering");
        if (loggedUser == null) {
            logger.warn("NULL user passed to saveEntityTranslation.");
            logger.debug("Returning");
            return;
        }

        if (entity == null || entity instanceof EntityAuditTrail || entity instanceof ScreenSessionInfo
                || entity instanceof TextTranslation) {
            logger.debug("Returning");
            return;
        }

        try {
            List<String> childFieldNames = BaseEntity.getChildEntityFieldName(entity.getClass().getName());
            for (String childFldName : childFieldNames) {
                Field childFld = BaseEntity.getClassField(entity.getClass(), childFldName);
                if (childFld == null) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    if (null != entity) {
                        logger.warn("Child Field Not Found. Won't Have DBID Generated Properly. Parent: {}, Child Field Entity: {}", entity.getClassName(), childFldName);
                    }
                    // </editor-fold>
                    continue;
                }
                if (childFld.getAnnotation(OneToOne.class) != null) {
                    if (((OneToOne) childFld.getAnnotation(OneToOne.class)).cascade() != null
                            || ((OneToOne) childFld.getAnnotation(OneToOne.class)).cascade().length > 0) {
                        BaseEntity childEntity = (BaseEntity) BaseEntity.getValueFromEntity(entity, childFldName);
                        if (null == childEntity) {
                            continue;
                        }
                        saveEntityTranslation(childEntity, loggedUser);
                    }
                }
            }
            List<Field> entityTranslatableFields = entity.getTranslatableFields();
            if (entityTranslatableFields.isEmpty()) {
                logger.debug("Returning");
                return;
            }

            String query = null;
            BaseEntityMetaData baseEntityMetaData = new BaseEntityMetaData();

            Class entityTranslationClass = Class.forName(entity.getClass().getName().concat("Translation"));
//            if(entityTranslation.getAnnotation(Table.class) != null){
//                entityTranslationTable = entityTranslation.getAnnotation(Table.class).toString();
//            }
            String tableName = "";
            if (entityTranslationClass != null) {
                List<String> list = baseEntityMetaData.getEntityTableName(entityTranslationClass);

                if (list != null) {
                    tableName = list.get(0);
                    query = "INSERT INTO " + tableName + " (";
                }
            } else {
                tableName = entity.getClass().getSimpleName().toLowerCase() + "translation";
                query = "INSERT INTO " + tableName + " (";
            }
            String deleteQuery = "delete from " + tableName + " where ENTITYDBID = " + entity.getDbid();

//            String query = "INSERT INTO " + entity.getClass().getSimpleName().toLowerCase() + "translation " + " (";
            query += "ENTITYDBID,LANGUAGE_DBID";
            String values = "'" + entity.getDbid() + "','" + loggedUser.getFirstLanguage().getDbid() + "'";
            for (int i = 0; i < entityTranslatableFields.size(); i++) {
                String fieldColumnName = BaseEntity.getFieldColumnName(entityTranslatableFields.get(i));
                if (fieldColumnName == null || fieldColumnName.isEmpty()) {
                    continue;
                }
                query += ",";
                String fieldName = BaseEntity.getFieldColumnName(entityTranslatableFields.get(i));
                if (fieldName == null || fieldName.isEmpty()) {
                    fieldName = entityTranslatableFields.get(i).getName();
                }
                query += fieldName;
                values += "," + dataExport.getSQLValue(entity.invokeGetter(
                        BaseEntity.getClassField(entity.getClass(), entityTranslatableFields.
                                get(i).getAnnotation(Translatable.class).translationField())), loggedUser);
            }
            query += ") VALUES (" + values + ")";
            if (loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).getSvalue())) {
                query = query.replace(",'" + loggedUser.getFirstLanguage().getDbid() + "',",
                        ",'" + loggedUser.getSecondLanguage().getDbid() + "',");
            }
            logger.trace("Query: {}", query);
            oem.executeEntityUpdateNativeQuery(deleteQuery, loggedUser);
            oem.executeEntityUpdateNativeQuery(query, loggedUser);

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Entity: {}", entity);
        }
        logger.debug("Returning");
    }

    @Override
    public void updateEntityTranslation(BaseEntity entity, boolean forceTranslation, OUser systemUser) {
        //  'loggedUser' cannot be NULL because the TextTranslation is
        //  updated based on the user's language
        logger.debug("Entering");
        if (entity == null) {
            logger.warn("NULL user passed to updateEntityTranslation.");
            logger.debug("Returning");
            return;
        }
        //  if the language of 'loggedUser' is English...

        if (entity == null || entity instanceof EntityAuditTrail || entity instanceof ScreenSessionInfo
                || entity instanceof TextTranslation) {
            logger.debug("Returning");
            return;
        }

        List<Field> entityTranslatableFields = entity.getTranslatableFields();
        try {
            List<String> childFieldNames = BaseEntity.getChildEntityFieldName(entity.getClass().getName());
            for (String childFldName : childFieldNames) {
                Field childFld = BaseEntity.getClassField(entity.getClass(), childFldName);
                if (childFld == null) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    if (null != entity) {
                        logger.warn("Child Field Not Found. Won't Have DBID Generated Properly. Parent: {}, Child Field Entity: {}", entity.getClassName(), childFldName);
                    }
                    // </editor-fold>
                    continue;
                }
                if (childFld.getAnnotation(OneToOne.class) != null) {
                    if (((OneToOne) childFld.getAnnotation(OneToOne.class)).cascade() == null
                            || ((OneToOne) childFld.getAnnotation(OneToOne.class)).cascade().length == 0) {
                        BaseEntity childEntity = (BaseEntity) BaseEntity.getValueFromEntity(entity, childFldName);
                        if (null == childEntity) {
                            continue;
                        }
                        updateEntityTranslation(childEntity, systemUser);
                    }
                }
            }
            if (entity.getTranslatableFields().isEmpty()) {
                logger.debug("Returning");
                return;
            }
            if (systemUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", systemUser).getSvalue()) && !forceTranslation) {
                for (int i = 0; i < entityTranslatableFields.size(); i++) {
                    Object object = BaseEntity.getValueFromEntity(entity, entityTranslatableFields
                            .get(i).getAnnotation(Translatable.class).translationField());
                    if (object != null) {
                        BaseEntity.setValueInEntity(entity, entityTranslatableFields.get(i).getName(), object);
                    }
                }
            } else {
                String query;
                List<String> cond = new ArrayList<String>();
                long userLangDBID;
                if (forceTranslation) {
                    userLangDBID = systemUser.getSecondLanguage().getDbid();
                } else {
                    userLangDBID = systemUser.getFirstLanguage().getDbid();
                }
                cond.add("language.dbid=" + userLangDBID);
                cond.add("entityDBID=" + entity.getDbid());
                List result = entityFromLocal(Class.forName(
                        entity.getClass().getName() + "Translation"), cond, null, null, systemUser);
                if (null != result
                        && result.isEmpty()) {
                    //insert into parent entity direct
                    query = "INSERT INTO " + entity.getClass().getSimpleName().toLowerCase() + "translation " + " (";

                    query += "ENTITYDBID,LANGUAGE_DBID";
                    String values = "'" + entity.getDbid() + "','" + userLangDBID + "'";
                    for (int i = 0; i < entityTranslatableFields.size(); i++) {
                        query += ",";
                        String fieldName = BaseEntity.getFieldColumnName(entityTranslatableFields.get(i));
                        if (fieldName == null || fieldName.isEmpty()) {
                            fieldName = entityTranslatableFields.get(i).getName();
                        }
                        query += fieldName;
                        values += "," + dataExport.getSQLValue(entity.invokeGetter(
                                BaseEntity.getClassField(entity.getClass(), entityTranslatableFields.
                                        get(i).getAnnotation(Translatable.class).translationField())), systemUser);
                    }
                    query += ") VALUES (" + values + ")";
                } else {
                    //updating in translatable table
                    BaseEntityMetaData baseEntityMetaData = new BaseEntityMetaData();
                    Class translationEntityClass = Class.forName(
                            entity.getClass().getName() + "Translation");
                    String translationTableName;
                    List<String> list = baseEntityMetaData.getEntityTableName(translationEntityClass);
                    if (list != null) {
                        translationTableName = list.get(0);
                    } else {
                        translationTableName = entity.getClass().getSimpleName().toLowerCase() + "translation";
                    }
                    query = "UPDATE " + translationTableName + " SET ";
                    for (int i = 0; i < entityTranslatableFields.size(); i++) {
                        if (i != 0) {
                            query += ",";
                        }
                        String fieldName = BaseEntity.getFieldColumnName(entityTranslatableFields.get(i));
                        if (fieldName == null || fieldName.isEmpty()) {
                            fieldName = entityTranslatableFields.get(i).getName();
                        }
                        query += fieldName;
                        query += "=" + dataExport.getSQLValue(entity.invokeGetter(
                                BaseEntity.getClassField(entity.getClass(), entityTranslatableFields.
                                        get(i).getAnnotation(Translatable.class).translationField())), systemUser);
                    }
                    query += " WHERE ";
                    query += "ENTITYDBID='" + entity.getDbid() + "' AND LANGUAGE_DBID='" + userLangDBID + "'";
                }
                logger.trace("Query: {}", query);
                oem.executeEntityUpdateNativeQuery(query, systemUser);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            // Shouldn't happen, we know the function should be there
            logger.warn("This Should Not Happen - Field Getter Not Found");
        }
        logger.debug("Returning");
    }

    @Override
    public void updateEntityTranslation(BaseEntity entity, OUser systemUser) {
        logger.debug("Entering");
        // used in oem.saveEntity
        updateEntityTranslation(entity, false, systemUser);
        logger.debug("Returning");
    }

    @Override
    public void removeEntityTranslation(BaseEntity entity, OUser loggedUser) {
        //  'loggedUser' cannot be NULL because the TextTranslation is
        //  removed using an OUser object
        logger.debug("Entering");
        if (loggedUser == null) {
            logger.warn("NULL user passed to removeEntityTranslation.");
            logger.debug("Returning");
            return;
        }

        if (entity == null || entity instanceof EntityAuditTrail || entity instanceof ScreenSessionInfo) {
            logger.debug("Returning");
            return;
        }

        /*
         * Warning:
         * The following line of code does not consider the possibility
         * that the same entity name might be used for two classes
         * that exist in two different packages.
         *
         * In this case, TextTranslationService must adapt to this
         * possibility in case the need arises.
         *
         * The suggested solution is to use the fully qualified name of
         * the entity's class instead of just the simple name.
         */
        try {
            List<String> childFieldNames = BaseEntity.getChildEntityFieldName(entity.getClass().getName());
            for (String childFldName : childFieldNames) {
                Field childFld = BaseEntity.getClassField(entity.getClass(), childFldName);
                if (childFld == null) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.warn("Child Field Not Found. Won't Have DBID Generated Properly. Parent: {}, Child Field Entity: {}", entity, childFldName);
                    // </editor-fold>
                    continue;
                }
                if (childFld.getAnnotation(OneToOne.class) != null) {
                    if (((OneToOne) childFld.getAnnotation(OneToOne.class)).cascade() != null
                            || ((OneToOne) childFld.getAnnotation(OneToOne.class)).cascade().length > 0) {
                        BaseEntity childEntity = (BaseEntity) BaseEntity.getValueFromEntity(entity, childFldName);
                        if (null == childEntity) {
                            continue;
                        }
                        removeEntityTranslation(childEntity, loggedUser);
                    }
                }
            }
            List<Field> entityTranslatableFields = entity.getTranslatableFields();
            if (entity.getTranslatableFields().isEmpty()) {
                logger.debug("Returning");
                return;
            }
            if (loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).getSvalue())) {
                for (int i = 0; i < entityTranslatableFields.size(); i++) {
                    BaseEntity.setValueInEntity(entity, entityTranslatableFields.get(i).getName(),
                            BaseEntity.getValueFromEntity(entity, entityTranslatableFields.get(i).getAnnotation(
                                            Translatable.class).translationField()));
                }
            } else {
                String query;
                List<String> cond = new ArrayList<String>();
                cond.add("entityDBID=" + entity.getDbid());
                List result = null;//versionControlService.entityFromLocal(Class.forName(
                //entity.getClass().getName() + "Translation"), cond, null, null, loggedUser);
                if (null != result
                        && !result.isEmpty()) {
                    BaseEntityMetaData baseEntityMetaData = new BaseEntityMetaData();
                    List<String> list = baseEntityMetaData.getEntityTableName(entity.getClass());
                    String tablename;
                    if (list != null) {
                        tablename = list.get(0);
                    } else {
                        tablename = entity.getClass().getSimpleName().toLowerCase();
                    }
                    query = "DELETE FROM " + tablename + "translation WHERE "
                            + "ENTITYDBID='" + entity.getDbid() + "'";
                    logger.trace("Query: {}", query);
                    oem.executeEntityUpdateNativeQuery(query, loggedUser);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
    }

    @Override
    public String buildQuery(Class entityClass, List<String> conditions,
            List<String> orderBy, List<String> fieldExpressions, OUser loggedUser) {
        logger.debug("Entering");
        if (conditions == null) {
            conditions = new ArrayList<String>();
        }
        Hashtable<String, List<Field>> fieldsInSameTable = BaseEntity.getClassFieldsInSameTable(
                entityClass);
        List<String> dValues = BaseEntity.getDiscrimnatorFieldTableData(
                entityClass);
        String query = "SELECT ";
        int c = 0;
        String tables = "";
        String where = "";
        String ob = "";
        List<Field> fields = new ArrayList<Field>();
        for (Map.Entry<String, List<Field>> entry : fieldsInSameTable.entrySet()) {
            String table = entry.getKey();
            List<Field> fieldsInTable = entry.getValue();
            fields.addAll(fieldsInTable);
            if (!tables.isEmpty()) {
                tables += ",";
            }
            tables += table + " e" + c;
            if (!dValues.isEmpty()) {
                if (table.equals(dValues.get(0))) {
                    conditions.add("e" + c + "." + dValues.get(1) + "='" + dValues.get(2) + "'");
                }
            }
            for (int i = 0; i < fieldsInTable.size(); i++) {
                String colName = BaseEntity.getFieldColumnName(fieldsInTable.get(i));
                query += (i == 0 && c == 0 ? "" : ",") + "e" + c + "." + colName;
                if (conditions != null && !conditions.isEmpty()) {
                    for (int j = 0; j < conditions.size(); j++) {
                        if (conditions.get(j).startsWith(fieldsInTable.get(i).getName() + ".dbid")) {
                            conditions.set(j, "e" + c + "." + colName + conditions.get(j).substring(
                                    fieldsInTable.get(i).getName().length() + 5));
                        } else if (conditions.get(j).startsWith(fieldsInTable.get(i).getName())) {
                            conditions.set(j, "e" + c + "." + colName + conditions.get(j).substring(
                                    fieldsInTable.get(i).getName().length()));
                        }
                    }
                }
                if (orderBy != null && !orderBy.isEmpty()) {
                    for (int j = 0; j < orderBy.size(); j++) {
                        if (orderBy.get(j).startsWith(fieldsInTable.get(i).getName() + ".dbid")) {
                            orderBy.set(j, "e" + c + "." + colName + orderBy.get(j).substring(
                                    fieldsInTable.get(i).getName().length() + 5));
                        } else if (orderBy.get(j).startsWith(fieldsInTable.get(i).getName())) {
                            orderBy.set(j, "e" + c + "." + colName + orderBy.get(j).substring(
                                    fieldsInTable.get(i).getName().length()));
                        }
                    }
                }
            }
            c++;
        }
        if (c > 1) {
            for (int i = c - 1; i > 0; i--) {
                conditions.add("e0.dbid=e" + i + ".dbid");
            }
        }
        if (conditions != null && !conditions.isEmpty()) {
            where += " WHERE ";
            for (int j = 0; j < conditions.size(); j++) {
                where += (j == 0 ? "" : " AND ") + conditions.get(j);
            }
        }
        if (orderBy != null && !orderBy.isEmpty()) {
            ob += " Order By ";
            for (int j = 0; j < orderBy.size(); j++) {
                ob += (j == 0 ? "" : " , ") + orderBy.get(j);
            }
        }
        query += " FROM " + tables + where + ob;
        logger.debug("Returning with : {}", query);
        return query;
    }

    @Override
    public List entityFromLocal(Class entityClass, List<String> cond, List<String> orderBy,
            List<String> fieldExpressions, OUser loggedUser) {
        logger.debug("Entering");
        String query = null;
        try {
            query = buildQuery(entityClass, cond, orderBy, fieldExpressions, loggedUser);
            logger.trace("Query: {}", query);
            List resultSet = oem.executeEntityListNativeQuery(query, oem.getSystemUser(loggedUser));
            Hashtable<String, List<Field>> fieldsInSameTable = BaseEntity.getClassFieldsInSameTable(
                    entityClass);
            List<Field> fields = new ArrayList<Field>();
            for (Map.Entry<String, List<Field>> entry : fieldsInSameTable.entrySet()) {
                List<Field> fieldsInTable = entry.getValue();
                fields.addAll(fieldsInTable);
            }
            if (resultSet != null) {
                logger.debug("Returning resultSet of size: {}", resultSet.size());
            }

            return resultSet;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning with new ArrayList");
            return new ArrayList();
        }
    }

    @Override
    public BaseEntity getEntityWithTowLevelsOfTranslatedFields(BaseEntity entity, OUser loggedUser) {
        if (entity == null) {
            return entity;
        }
        entity = getEntityWithTranslatedFields(entity, loggedUser);
        Field[] entityFields = entity.getClass().getDeclaredFields();
        for (Field field : entityFields) {
            try {
                Object fieldValue = getEntityFieldValue(entity, field);
                if (fieldValue != null && fieldValue instanceof BaseEntity) {
                    BaseEntity fieldEntity = (BaseEntity) fieldValue;
                    fieldEntity = getEntityWithTranslatedFields(fieldEntity, loggedUser);
                    entity.invokeSetter(field, fieldEntity);
                }
            } catch (IllegalArgumentException ex) {
                logger.debug(null, ex);
            } catch (NoSuchMethodException ex) {
                logger.debug(null, ex);
            }
        }
        return entity;
    }

    private BaseEntity getEntityWithTranslatedFields(BaseEntity entity, OUser loggedUser) {
        if (entity != null) {
            setAllTranslatedFields(entity, loggedUser);
        }
        return entity;
    }

    private void setAllTranslatedFields(BaseEntity entity, OUser loggedUser) {
        String entityName = entity.getClass().getName();
        String entityTranslationName = entityName + "Translation";
        try {
            Class.forName(entityTranslationName);
            List<String> entityTranslatedFields = getAllEntityNullTranslatedFieldsHirarchy(entity);
            if (entityTranslatedFields == null || entityTranslatedFields.isEmpty()) {
                return;
            }
            BaseEntityTranslation entityTranslation = oem.getEntityTranslation(entity.getClassName(), entity.getDbid(), loggedUser);
            if (entityTranslation == null) {
                return;
            }
            for (String translatedField : entityTranslatedFields) {
                Field fieldInOriginalEntity = getFieldFromClassHirarchy(entity.getClass(), translatedField + "Translated");
                if (fieldInOriginalEntity != null) {
                    Object translatedFieldValue = entityTranslation.invokeGetter(translatedField);
                    entity.invokeSetter(fieldInOriginalEntity, translatedFieldValue);
                }
            }
        } catch (ClassNotFoundException | SecurityException | NoSuchMethodException ex) {
            logger.debug(null, ex);
        }
    }

    private List<String> getAllEntityNullTranslatedFieldsHirarchy(Object entity) {
        Class entityClass = entity.getClass();
        List<String> TranslatedFields = new ArrayList<>();
        while (!entityClass.getSimpleName().equals("Object")) {
            Field[] fields = entityClass.getDeclaredFields();
            for (Field field : fields) {
                try {
                    Translation annotation = field.getAnnotation(Translation.class);
                    if (annotation != null && annotation instanceof Translation && ((BaseEntity) entity).invokeGetter(field) == null) {
                        TranslatedFields.add(annotation.originalField());
                    }
                } catch (NoSuchMethodException ex) {
                    logger.debug(null, ex);
                }
            }
            entityClass = entityClass.getSuperclass();
        }
        return TranslatedFields;
    }

    private Field getFieldFromClassHirarchy(Class clazz, String fieldName) {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException ex) {
            return getFieldFromClassHirarchy(clazz.getSuperclass(), fieldName);
        } catch (SecurityException ex) {
            logger.debug(null, ex);
            return null;
        }
    }

    private Object getEntityFieldValue(BaseEntity entity, Field field) {
        try {
            return entity.invokeGetter(field);
        } catch (NoSuchMethodException ex) {
            logger.debug(null, ex);
            return null;
        }
    }
}
