/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.audittrail.messaging.listner;


import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import com.unitedofoq.fabs.core.audittrail.exception.InvalidAuditTrailObjectException;
import com.unitedofoq.fabs.core.audittrail.main.valueholder.AuditTrailMessageObject;
import com.unitedofoq.fabs.core.audittrail.messaging.processor.AuditTrailMessageProcessorLocal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mustafa
 */
@MessageDriven(mappedName = "java:/jms/queue/AuditTrailMSGQueue", activationConfig = {
    @ActivationConfigProperty(propertyName = "destination", propertyValue = "jms/queue/AuditTrailMSGQueue"),
    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
    @ActivationConfigProperty(propertyName = "reconnectAttempts", propertyValue = "-1"),
    @ActivationConfigProperty(propertyName = "setupAttempts", propertyValue = "-1")
})
public class AuditTrailMessageMDB implements MessageListener {

    @EJB
    AuditTrailMessageProcessorLocal auditTrailMessageProcessor;

    final static Logger logger = LoggerFactory.getLogger(AuditTrailMessageMDB.class);
    
    public AuditTrailMessageMDB() {
    }

    @Override
    public void onMessage(Message message) {
        logger.debug("Entering");
        try {

            ObjectMessage o = (ObjectMessage) message;
            AuditTrailMessageObject auditTrailMessageObject = (AuditTrailMessageObject) o.getObject();

            auditTrailMessageProcessor.process(auditTrailMessageObject);

        } catch (JMSException ex) {
            logger.error("Exception thrown", ex);
        } catch (InvalidAuditTrailObjectException ex) {
            logger.error("Exception thrown", ex);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
    }
}
