
package com.unitedofoq.fabs.core.module;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class OModuleTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="moduleTitle">
    @Column
    private String moduleTitle;

    public void setModuleTitle(String moduleTitle) {
        this.moduleTitle = moduleTitle;
    }

    public String getModuleTitle() {
        return moduleTitle;
    }
    // </editor-fold>

}
