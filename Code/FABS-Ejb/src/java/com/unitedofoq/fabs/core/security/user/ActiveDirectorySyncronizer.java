package com.unitedofoq.fabs.core.security.user;

import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;

import com.unitedofoq.fabs.core.module.ModuleServiceRemote;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.security.user.ActiveDirectoryFacade.User;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//
//
@Singleton
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.security.user.ActiveDirectorySyncronizerLocal",
        beanInterface = ActiveDirectorySyncronizerLocal.class)
public class ActiveDirectorySyncronizer implements ActiveDirectorySyncronizerLocal {
final static Logger logger = LoggerFactory.getLogger(ActiveDirectorySyncronizer.class);

    @Resource
    TimerService activeDirectorySyncronizerTimerService;
    @EJB
    private UserServiceRemote userServiceRemote;
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private OCentralEntityManagerRemote centralOEM;
    @EJB
    private ModuleServiceRemote moduleService;
    @EJB
    private FABSSetupLocal fABSSetupLocal;

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    private String getParametersFromFABSSetup(String keySetup,
            OUser loggedUser) throws NoResultException, RollbackException {
        return fABSSetupLocal.loadKeySetup(keySetup, loggedUser).getSvalue();
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    private OModule loadSystemModule(OUser loggedUser)
            throws NoResultException, RollbackException {
        OModule systemModule = moduleService.loadOModule("System", loggedUser);
        return systemModule;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    private OUser loadOUserToCheckifUserExistOrNot(User user, OUser loggedUser)
            throws NonUniqueResultException, RollbackException, UserNotAuthorizedException,
            EntityNotFoundException, EntityExistsException, OptimisticLockException,
            TransactionRequiredException, NoResultException {
        OUser ouser;
        List<String> conditions = new ArrayList<String>();
        conditions.add("loginName = '" + user.getSamaccountname().toLowerCase() + "'");
        conditions.add(OEntityManager.CONFCOND_GET_ACTIVEANDINACTIVE);
        ouser = (OUser) oem.loadEntity("OUser", conditions, null, loggedUser);
        return ouser;
    }
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    private LdapContext getConnectionWithActiveDirectory(String securityAuthentication,
            String domainPassword, String domainName, String serverURL)
            throws NamingException {
        LdapContext ctx;
        ctx = ActiveDirectoryFacade.getConnection(securityAuthentication,
                domainPassword, domainName, serverURL);
        return ctx;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private OFunctionResult addUser(OUser ouser, OUser loggedUser, OModule systemModule) {
        return userServiceRemote.addUser(ouser, loggedUser, systemModule);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private OFunctionResult updateUser(OUser ouser, OUser loggedUser) {
        return userServiceRemote.updateUser(ouser, ouser, loggedUser);
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    private void setNewUserData(OUser ouser, User user, OUser loggedUser, OModule systemModule,
            String defaultMailHost, String defaultPassword) {
        ouser.setLoginName(user.getSamaccountname());
        ouser.setFirstLanguage(loggedUser.getFirstLanguage());
        ouser.setSecondLanguage(loggedUser.getSecondLanguage());
        ouser.setOmodule(systemModule);
        ouser.setEmail(user.getMail() == null ? (user.getSamaccountname()
                + "@" + defaultMailHost) : user.getMail());
        ouser.setPassword(defaultPassword);
        ouser.setInActive(false);
        ouser.setRtl(false);
        ouser.setUserPrefrence1(loggedUser.getUserPrefrence1());
        ouser.setActive(true);
        
        String rolesString = getParametersFromFABSSetup("ADUserRoles", loggedUser);
        
        if ((rolesString != null) && (!"".equals(rolesString))) {
            String[] rolesArray = rolesString.split(",");
            List<RoleUser> roleUserList = new ArrayList<RoleUser>();
            for (String role: rolesArray) {
                try
                {
                ORole oRole = (ORole) oem.loadEntity("ORole",
                    Collections.singletonList("name ='" + role + "'"), null,
                    loggedUser);
                
                RoleUser roleUser = new RoleUser();
                roleUser.setOrole(oRole);
                roleUser.setOuser(ouser);
                roleUserList.add(roleUser);
                } catch (Exception ex) {
                    logger.error("Exception thrown: ",ex);
                }
            }
            ouser.setRoleUsers(roleUserList);
        }
    }

    private void addUserFromActiveDirectory(OUser loggedUser) throws
            OptimisticLockException, EntityNotFoundException, UserNotAuthorizedException,
            NonUniqueResultException, NoResultException, EntityExistsException, TransactionRequiredException,
            RollbackException {

        List<String> ldapUsers = new ArrayList<String>();
        List<String> success = new ArrayList<String>();
        List<String> failure = new ArrayList<String>();
        OFunctionResult oFR = new OFunctionResult();

        OModule systemModule = loadSystemModule(loggedUser);
        String securityAuthentication = getParametersFromFABSSetup("securityAuthenication", loggedUser);
        String serverURL = getParametersFromFABSSetup("ADserverURL", loggedUser);
        String domainName = getParametersFromFABSSetup("domainname", loggedUser);
        String domainPassword = getParametersFromFABSSetup("domainpassword", loggedUser);
        String searchBase = getParametersFromFABSSetup("searchbase", loggedUser);
        String defaultMailHost = getParametersFromFABSSetup("defaultMailHost", loggedUser);
        String defaultPassword = getParametersFromFABSSetup("defaultPassword", loggedUser);

        //<editor-fold defaultstate="collapse" desc="Error with AD Parameter Connection">
        if (systemModule == null || securityAuthentication == null || serverURL == null
                || domainName == null || domainPassword == null || searchBase == null
                || defaultMailHost == null || defaultPassword == null) {
            logger.error("Error in Getting connection parameters for Active Directory");
        }
        //</editor-fold>

        LdapContext ctx = null;
        try {
            ctx = getConnectionWithActiveDirectory(securityAuthentication,
                    domainPassword, domainName, serverURL);
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            logger.trace("failed to connect with Active Directory in ActiveDirectorySyn");

        }
        if (ctx != null) {
            List<ActiveDirectoryFacade.User> users = ActiveDirectoryFacade.
                    listAllUsers(searchBase, ctx);
            OUser ouser;
            for (int itr = 0; itr < users.size(); itr++) {
                try {
                ActiveDirectoryFacade.User user = users.get(itr);
                logger.trace("ADSynchronizer processing AD user: {}", user.getSamaccountname());
                ldapUsers.add(user.getSamaccountname());
                ouser = loadOUserToCheckifUserExistOrNot(user, loggedUser);
                if ((ouser != null) && (!ouser.isActive())) {
                    logger.debug("Ouser Activated");
                    ouser.setActive(true);
                    oFR.append(updateUser(ouser, loggedUser));
                }
                if (ouser == null) {
                    logger.trace("Ouser equals Null");
                    ouser = new OUser();
                    setNewUserData(ouser, user, loggedUser, systemModule, defaultMailHost, defaultPassword);
                    
                    oFR = addUser(ouser, loggedUser, systemModule);
                    if (oFR.getSuccesses() != null) {
                        success.add("User added: " + ouser.getLoginName());
                        logger.debug("User: {} add",ouser.getLoginName());
                    } else {
                        logger.debug("User: {} Failed to be added",ouser.getLoginName());
                        failure.add("User Failed to be added : " + ouser.getLoginName());
                    }
                
                }
                } catch (Exception ex) {
                    logger.debug("Failed to add user, skipping to next");
                    logger.error("Exception thrown: ",ex);
                }
            }
        }
        logger.debug("Returning");
    }

    @Override
    public OFunctionResult SycOnDemad(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult ofr = new OFunctionResult();
        try {
            addUserFromActiveDirectory(loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            logger.trace("Error with Sychronization of Active Directory");
        }
        logger.debug("Returning");
        return ofr;
    }
}
