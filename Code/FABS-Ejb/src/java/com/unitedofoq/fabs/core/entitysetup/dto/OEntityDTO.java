/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitysetup.dto;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author root
 */
public class OEntityDTO implements Serializable {
    private long dbid;
    private String entityClassPath;
    private boolean requestable;
    private long ccType1Dbid;
    private long ccType2Dbid;
    private long ccType3Dbid;
    private long ccType4Dbid;
    private long ccType5Dbid;
    private boolean auditable;
    private boolean systemAuditable;

    public boolean isSystemAuditable() {
        return systemAuditable;
    }

    public void setSystemAuditable(boolean systemAuditable) {
        this.systemAuditable = systemAuditable;
    }
    
    
    public boolean isAuditable() {
        return auditable;
    }

    public void setAuditable(boolean auditable) {
        this.auditable = auditable;
    }

    public boolean isRequestable() {
        return requestable;
    }

    public void setRequestable(boolean requestable) {
        this.requestable = requestable;
    }
    // <editor-fold defaultstate="collapsed" desc="title">
    //Title that gets inherited by all entities (i.e. OObject, OBusinessObject) to be used in the Balloon
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    // </editor-fold>

    public OEntityDTO(long dbid, String entityClassPath) {
        this.dbid = dbid;
        this.entityClassPath = entityClassPath;
    }

    public OEntityDTO(long dbid, String entityClassPath, String title) {
        this.dbid = dbid;
        this.entityClassPath = entityClassPath;
        this.title = title;
    }
    
    public OEntityDTO(long dbid, String entityClassPath, boolean requestable, String title) {
        this.dbid = dbid;
        this.entityClassPath = entityClassPath;
        this.requestable = requestable;
        this.title = title;
    }

    public OEntityDTO(long dbid, String entityClassPath,
            boolean auditable, boolean systemAuditable) {
        this.dbid = dbid;
        this.entityClassPath = entityClassPath;
        this.auditable = auditable;
        this.systemAuditable = systemAuditable;
    }

    public OEntityDTO(long dbid, String entityClassPath, boolean requestable, String title, long ccType1Dbid,
            long ccType2Dbid, long ccType3Dbid, long ccType4Dbid, long ccType5Dbid) {
        this.dbid = dbid;
        this.entityClassPath = entityClassPath;
        this.requestable = requestable;
        this.title = title;

        this.ccType1Dbid = ccType1Dbid;
        this.ccType2Dbid = ccType2Dbid;
        this.ccType3Dbid = ccType3Dbid;
        this.ccType4Dbid = ccType4Dbid;
        this.ccType5Dbid = ccType5Dbid;

    }

    public long getDbid() {
        return dbid;
    }

    public void setDbid(long dbid) {
        this.dbid = dbid;
    }

    public String getEntityClassPath() {
        return entityClassPath;
    }

    public void setEntityClassPath(String entityClassPath) {
        this.entityClassPath = entityClassPath;
    }

    public String getEntityClassName() {
        return getEntityClassPath().substring(
                getEntityClassPath().lastIndexOf(".") + 1,
                getEntityClassPath().length());
    }
    private ArrayList<String> entityParentClassPath = null;

    /**
     * Returns {@link BaseEntity#getParentClassPath(java.lang.String)}, caching
     * it in {@link #entityParentClassPath}
     */
    public List getEntityParentClassPath() {
        if (entityParentClassPath != null) {
            return entityParentClassPath;
        }
        entityParentClassPath = (ArrayList<String>) BaseEntity.getParentClassPath(getEntityClassPath());
        return entityParentClassPath;
    }
    ArrayList<String> entityParentFieldName = null;

    public List getEntityParentFieldName() {
        if (entityParentFieldName != null) {
            return entityParentFieldName;
        }
        List<String> entityParentFieldNames = BaseEntity.getParentEntityFieldName(getEntityClassPath());
        if (entityParentFieldNames != null && !entityParentFieldNames.isEmpty()) {
            entityParentFieldName = (ArrayList<String>) entityParentFieldNames;
        }
        return entityParentFieldName;
    }

    /**
     * @return the ccType1Dbid
     */
    public long getCcType1Dbid() {
        return ccType1Dbid;
    }

    /**
     * @param ccType1Dbid the ccType1Dbid to set
     */
    public void setCcType1Dbid(long ccType1Dbid) {
        this.ccType1Dbid = ccType1Dbid;
    }

    /**
     * @return the ccType2Dbid
     */
    public long getCcType2Dbid() {
        return ccType2Dbid;
    }

    /**
     * @param ccType2Dbid the ccType2Dbid to set
     */
    public void setCcType2Dbid(long ccType2Dbid) {
        this.ccType2Dbid = ccType2Dbid;
    }

    /**
     * @return the ccType3Dbid
     */
    public long getCcType3Dbid() {
        return ccType3Dbid;
    }

    /**
     * @param ccType3Dbid the ccType3Dbid to set
     */
    public void setCcType3Dbid(long ccType3Dbid) {
        this.ccType3Dbid = ccType3Dbid;
    }

    /**
     * @return the ccType4Dbid
     */
    public long getCcType4Dbid() {
        return ccType4Dbid;
    }

    /**
     * @param ccType4Dbid the ccType4Dbid to set
     */
    public void setCcType4Dbid(long ccType4Dbid) {
        this.ccType4Dbid = ccType4Dbid;
    }

    /**
     * @return the ccType5Dbid
     */
    public long getCcType5Dbid() {
        return ccType5Dbid;
    }

    /**
     * @param ccType5Dbid the ccType5Dbid to set
     */
    public void setCcType5Dbid(long ccType5Dbid) {
        this.ccType5Dbid = ccType5Dbid;
    }
}
