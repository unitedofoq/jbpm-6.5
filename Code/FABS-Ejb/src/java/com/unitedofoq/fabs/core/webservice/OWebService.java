/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.webservice;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.module.OModule;

/**
 *
 * @author htawab
 */
@Entity
@ChildEntity(fields={"serviceFilter", "webServiceFunction"})
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class OWebService extends ObjectBaseEntity implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OModule omodule;

    public String getOmoduleDD(){
        return "OWebService_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
// </editor-fold>

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ODataType returnDT;

    public ODataType getReturnDT() {
        return returnDT;
    }

    public void setReturnDT(ODataType returnDT) {
        this.returnDT = returnDT;
    }

    public String getReturnDTDD(ODataType returnDT) {
        return "OWebService_returnDT";
    }

    String nameSpace;
    String serviceName;
    String operationName;
    String endPointAddress;
    @Transient
    String soapAction;
    String portTypeName;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    OFunction preFunction;
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    OFunction postFunction;

    @OneToOne(fetch = javax.persistence.FetchType.LAZY, cascade=CascadeType.ALL,mappedBy="service")
    private WebServiceFilter serviceFilter;

    public OFunction getPostFunction() {
        return postFunction;
    }
    
    public String getPostFunctionDD() {
        return "OWebService_postFunction";
    }

    public void setPostFunction(OFunction postFunction) {
        this.postFunction = postFunction;
    }

    public String getPreFunctionDD() {
        return "OWebService_preFunction";
    }

    public OFunction getPreFunction() {
        return preFunction;
    }

    public void setPreFunction(OFunction preFunction) {
        this.preFunction = preFunction;
    }

    public WebServiceFilter getServiceFilter() {
        return serviceFilter;
    }
    
    public String getServiceFilterDD() {
        return "OWebService_serviceFilter";
    }

    public void setServiceFilter(WebServiceFilter serviceFilter) {
        this.serviceFilter = serviceFilter;
    }

    public String getEndPointAddress() {
        return endPointAddress;
    }
    public String getEndPointAddressDD() {
        return "OWebService_endPointAddress";
    }

    public void setEndPointAddress(String endPointAddress) {
        this.endPointAddress = endPointAddress;
    }

    public String getNameSpace() {
        return nameSpace;
    }
    public String getNameSpaceDD() {
        return "OWebService_nameSpace";
    }

    public void setNameSpace(String nameSpace) {
        this.nameSpace = nameSpace;
    }

    public String getOperationName() {
        return operationName;
    }
    public String getOperationNameDD() {
        return "OWebService_operationName";
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public String getPortTypeName() {
        return portTypeName;
    }
    public String getPortTypeNameDD() {
        return "OWebService_portTypeName";
    }

    public void setPortTypeName(String portTypeName) {
        this.portTypeName = portTypeName;
    }

    public String getServiceName() {
        return serviceName;
    }
    public String getServiceNameDD() {
        return "OWebService_serviceName";
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getSoapAction() {
        return soapAction;
    }

    public void setSoapAction(String soapAction) {
        this.soapAction = soapAction;
    }

    @Override
    public void PostLoad(){
        super.PostLoad();
        soapAction = nameSpace + operationName;
    }

    @Override
    public void PrePersist() {
        super.PrePersist();
        if (serviceFilter != null)
            if (serviceFilter.getService() == null)
                serviceFilter.setService(this);
    }

    // <editor-fold defaultstate="collapsed" desc="webServiceFunction">
//    @JoinColumn
//    @OneToMany(mappedBy="webService")
//    private WebServiceFunction webServiceFunction ;
//
//    public WebServiceFunction getWebServiceFunction() {
//        return webServiceFunction;
//    }
//
//    public void setWebServiceFunction(WebServiceFunction webServiceFunction) {
//        this.webServiceFunction = webServiceFunction;
//    }
    // </editor-fold> 
}
