/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDCBase;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author melsayed
 */
@Stateless
@EJB(name="java:global/ofoq/com.unitedofoq.fabs.core.uiframework.UIFrameworkValidationServiceRemote",
    beanInterface=UIFrameworkValidationServiceRemote.class)
public class UIFrameworkValidationServiceBean implements UIFrameworkValidationServiceRemote {
final static Logger logger = LoggerFactory.getLogger(UIFrameworkValidationServiceBean.class);
    @EJB
    private UserMessageServiceRemote userMessageServiceRemote;
    @EJB
    DDServiceRemote ddService;

    @EJB
    OFunctionServiceRemote functionService;

    public OFunctionResult validateUDC(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser){
        logger.debug("Entering");
        if (odm.getData().size() != 2) {
            UserMessage errorMsg = userMessageServiceRemote.getUserMessage("CorruptedData", loggedUser);
            OFunctionResult functionResult = new OFunctionResult();
            if (errorMsg != null) {
                functionResult.addError(errorMsg);
            }
            logger.debug("Returning");
            return functionResult;
        }
        OFunctionResult functionResult = new OFunctionResult();
        UDCBase uDCBase = (UDCBase) odm.getData().get(0);

        if (uDCBase.isReserved()) {
            functionResult.addError(userMessageServiceRemote.getUserMessage(
                    "udcreservedError", loggedUser));
        }
logger.debug("Returning");
        return functionResult;
    }

    private OFunctionResult getCorruptedDataErrorMessage(OUser loggedUser)
    {
        logger.debug("Returning");
        UserMessage errorMsg = userMessageServiceRemote.getUserMessage("CorruptedData", loggedUser);
        OFunctionResult functionResult = new OFunctionResult();
        if (errorMsg != null)
            functionResult.addError(errorMsg);
        logger.debug("Returning");
        return functionResult;
    }
}