/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.comunication;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 *
 * @author aelzaher
 */
@WebService(serviceName = "MailSender")
@Stateless
public class MailSender {

    @EJB
    private EMailSenderLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "sendEMail")
    public boolean sendEMail(@WebParam(name = "from") String from, @WebParam(name = "to") String to, @WebParam(name = "subject") String subject, @WebParam(name = "body") String body) {
        return ejbRef.sendEMail(from, to, subject, body);
    }

    @WebMethod(operationName = "sendEMailToAll")
    public List<String> sendEMailToAll(@WebParam(name = "from") String from, @WebParam(name = "to") List<String> to, @WebParam(name = "subject") String subject, @WebParam(name = "body") String body) {
        return ejbRef.sendEMailToAll(from, to, subject, body);
    }

    @WebMethod(operationName = "sendEMail_1")
    @RequestWrapper(className = "sendEMail_1")
    @ResponseWrapper(className = "sendEMail_1Response")
    public boolean sendEMail(@WebParam(name = "from") String from, @WebParam(name = "to") String to, @WebParam(name = "subject") String subject, @WebParam(name = "body") String body, @WebParam(name = "userEmail") String userEmail, @WebParam(name = "userPassword") String userPassword, @WebParam(name = "portNumber") String portNumber, @WebParam(name = "SMTP") String SMTP) {
        return ejbRef.sendEMail(from, to, subject, body, userEmail, userPassword, portNumber, SMTP);
    }

    @WebMethod(operationName = "sendEMailToAll_1")
    @RequestWrapper(className = "sendEMailToAll_1")
    @ResponseWrapper(className = "sendEMailToAll_1Response")
    public List<String> sendEMailToAll(@WebParam(name = "from") String from, @WebParam(name = "to") List<String> to, @WebParam(name = "subject") String subject, @WebParam(name = "body") String body, @WebParam(name = "userEmail") String userEmail, @WebParam(name = "userPassword") String userPassword, @WebParam(name = "portNumber") String portNumber, @WebParam(name = "SMTP") String SMTP) {
        return ejbRef.sendEMailToAll(from, to, subject, body, userEmail, userPassword, portNumber, SMTP);
    }

}
