/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.portal;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;

/**
 *
 * @author htawab
 */
@Entity
@ParentEntity(fields = "portalPage")
@ChildEntity(fields = {"receivers"})
@VersionControlSpecs(versionControlType = VersionControlType.Parent)
public class PortalPagePortlet extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="portalPage">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    protected OPortalPage portalPage;

    public OPortalPage getPortalPage() {
        return portalPage;
    }

    public String getPortalPageDD() {
        return "PortalPagePortlet_PortalPage";
    }

    public void setPortalPage(OPortalPage portalPage) {
        this.portalPage = portalPage;
    }
    // </editor-fold>
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "parentPortlet_DBID")
    PortalPagePortlet parentPortlet;
    @OneToMany(mappedBy = "parentPortlet")
    List<PortalPagePortlet> toBeRenderedList;
    protected boolean main;

    public boolean isMain() {
        return main;
    }

    public void setMain(boolean main) {
        this.main = main;
    }

    public String getMainDD() {
        return "PortalPagePortlet_main";
    }
    protected int position;

    public String getPositionDD() {
        return "PortalPagePortlet_position";
    }
    @Column(name = "columnN")
    protected String column;

    public String getColumnDD() {
        return "PortalPagePortlet_column";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    protected OScreen screen;

    public String getScreenDD() {
        return "PortalPagePortlet_screen";
    }

    /**
     * Get the value of screen
     *
     * @return the value of screen
     */
    public OScreen getScreen() {
        return screen;
    }

    /**
     * Set the value of screen
     *
     * @param screen new value of screen
     */
    public void setScreen(OScreen screen) {
        this.screen = screen;
    }

    /**
     * Get the value of position
     *
     * @return the value of position
     */
    public int getPosition() {
        return position;
    }

    /**
     * Set the value of position
     *
     * @param position new value of position
     */
    public void setPosition(int position) {
        this.position = position;
    }

    /**
     * Get the value of column
     *
     * @return the value of column
     */
    public String getColumn() {
        return column;
    }

    /**
     * Set the value of column
     *
     * @param column new value of column
     */
    public void setColumn(String column) {
        this.column = column;
    }

    public PortalPagePortlet getParentPortlet() {
        return parentPortlet;
    }

    public String getParentPortletDD() {
        return "PortalPagePortlet_parentPortlet";
    }

    public void setParentPortlet(PortalPagePortlet parentPortlet) {
        this.parentPortlet = parentPortlet;
    }

    public List<PortalPagePortlet> getToBeRenderedList() {
        return toBeRenderedList;
    }

    public void setToBeRenderedList(List<PortalPagePortlet> toBeRenderedList) {
        this.toBeRenderedList = toBeRenderedList;
    }
    // <editor-fold defaultstate="collapsed" desc="Width, Height">
    private String width;

    public String getWidthDD() {
        return "PortalPagePortlet_width";
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }
    private String height;

    public String getHeightDD() {
        return "PortalPagePortlet_height";
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="defaultState">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC defaultState;

    public UDC getDefaultState() {
        return defaultState;
    }

    public String getDefaultStateDD() {
        return "PortalPagePortlet_defaultState";
    }

    public void setDefaultState(UDC defaultState) {
        this.defaultState = defaultState;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="senders">
    @OneToMany(mappedBy = "receiver")
    List<PortalPagePortletReceiver> senders;

    public List<PortalPagePortletReceiver> getSenders() {
        return senders;
    }

    public void setSenders(List<PortalPagePortletReceiver> senders) {
        this.senders = senders;
    }

    public String getSendersDD() {
        return "PortalPagePortlet_senders";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="receivers">
    @OneToMany(mappedBy = "sender")
    List<PortalPagePortletReceiver> receivers;

    public List<PortalPagePortletReceiver> getReceivers() {
        return receivers;
    }

    public void setReceivers(List<PortalPagePortletReceiver> receivers) {
        this.receivers = receivers;
    }

    public String getReceiversDD() {
        return "PortalPagePortlet_receivers";
    }
    // </editor-fold>
    @Transient
    private String portletScreenInstId;

    /**
     * @return the portletScreenInstId
     */
    public String getPortletScreenInstId() {
        return portletScreenInstId;
    }

    /**
     * @param portletScreenInstId the portletScreenInstId to set
     */
    public void setPortletScreenInstId(String portletScreenInstId) {
        this.portletScreenInstId = portletScreenInstId;
    }
}
