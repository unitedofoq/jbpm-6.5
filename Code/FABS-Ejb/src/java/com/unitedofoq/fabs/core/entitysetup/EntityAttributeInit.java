/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitysetup;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.function.OFunction;

/**
 *
 * @author mmohamed
 */
@Entity
@ParentEntity(fields = {"ownerEntity"})
@VersionControlSpecs(versionControlType = VersionControlType.Parent)
public class EntityAttributeInit extends BaseEntity {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private OEntity ownerEntity;
    private String fieldExpression;
    private String initializationExpression;
    private String hardCodedValue;
    private boolean sequence;
    private boolean sequencePerParent;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OFunction function;

    public OFunction getFunction() {
        return function;
    }

    public String getFunctionDD() {
        return "EntityAttributeInit_function";
    }

    public void setFunction(OFunction function) {
        this.function = function;
    }

    public boolean isSequencePerParent() {
        return sequencePerParent;
    }

    public String getSequencePerParentDD() {
        return "EntityAttributeInit_sequencePerParent";
    }

    public void setSequencePerParent(boolean sequencePerParent) {
        this.sequencePerParent = sequencePerParent;
    }

    public String getFieldExpression() {
        return fieldExpression;
    }

    public String getFieldExpressionDD() {
        return "EntityAttributeInit_fieldExpression";
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }

    public String getHardCodedValue() {
        return hardCodedValue;
    }

    public String getHardCodedValueDD() {
        return "EntityAttributeInit_hardCodedValue";
    }

    public void setHardCodedValue(String hardCodedValue) {
        this.hardCodedValue = hardCodedValue;
    }

    public String getInitializationExpression() {
        return initializationExpression;
    }

    public String getInitializationExpressionDD() {
        return "EntityAttributeInit_initializationExpression";
    }

    public void setInitializationExpression(String initializationExpression) {
        this.initializationExpression = initializationExpression;
    }

    public OEntity getOwnerEntity() {
        return ownerEntity;
    }

    public String getOwnerEntityDD() {
        return "EntityAttributeInit_ownerEntity";
    }

    public void setOwnerEntity(OEntity ownerEntity) {
        this.ownerEntity = ownerEntity;
    }

    public boolean isSequence() {
        return sequence;
    }

    public String getSequenceDD() {
        return "EntityAttributeInit_sequence";
    }

    public void setSequence(boolean sequence) {
        this.sequence = sequence;
    }
}
