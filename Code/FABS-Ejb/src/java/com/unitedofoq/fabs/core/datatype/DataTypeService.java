package com.unitedofoq.fabs.core.datatype;

import java.io.StringReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;


import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Transient;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntityAction;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;

import com.unitedofoq.fabs.core.process.OProcess;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.udc.UDCType;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.fabs.core.validation.FABSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@EJB(name="java:global/ofoq/com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote",
    beanInterface=DataTypeServiceRemote.class)
public class DataTypeService implements DataTypeServiceRemote
{
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private UserMessageServiceRemote usrMsgService;
    @EJB
    private EntitySetupServiceRemote entitySetupService;
    //<editor-fold defaultstate="collapsed" desc="Cache">
    public static Hashtable<String, ODataType> cachedODataType = new Hashtable<String, ODataType>();
    final static Logger logger = LoggerFactory.getLogger(DataTypeService.class);
    @Override
    public OFunctionResult onODataTypeCachedUpdate (
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        logger.debug("Returning");
        OFunctionResult oFR = new OFunctionResult();
        try {
            ODataType dataType = (ODataType) odm.getData().get(0);
            cachedODataType.put(loggedUser.getTenant().getId() + "_" + dataType.getName(), dataType);
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.debug("DataType Updated In Cache. DataType: {}", dataType);
            // </editor-fold>
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ",ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }
    
    @Override
    public OFunctionResult preODataTypeCachedUpdate (
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        logger.debug("Returning");
        OFunctionResult oFR = new OFunctionResult();
        try {
            ODataType dataType = (ODataType) odm.getData().get(0);
            String oldName = ((ODataType) oem.loadEntity("ODataType", dataType.getDbid()
                    , null, null, loggedUser)).getName();
            if (!oldName.equals(dataType.getName())) {
                cachedODataType.remove(loggedUser.getTenant().getId() + "_" + oldName);
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.debug("DataType Removed From Cache. DataType: {}", dataType);
                // </editor-fold>
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ",ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR ;
        }
    }
    
    @Override
    public OFunctionResult onODataTypeCachedRemove (
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            ODataType dataType = (ODataType) odm.getData().get(0);
            cachedODataType.remove(loggedUser.getTenant().getId() + "_" + dataType.getName());
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.debug("DataType Removed From Cache. DataType: {}", dataType);
            // </editor-fold>
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ",ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR ;
        }
    }
    
    /**
     * Clears {@link #cachedODataType}
     */
    @Override
    public OFunctionResult clearODataTypeCache (
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            cachedODataType.clear();
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.debug("DataType Cache Cleared");
            // </editor-fold>
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown",ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR ;
        }
    }
    //</editor-fold>
        
   
    /**
     * generates the XML String for the passed Element
     *
     * @param node
     *      The element to be converted to an XML String
     *
     * @return  The corresponding XML String
     */
    public String generateXML(Element element) {
        logger.debug("Entering: generateXML");
        Document document = element.getOwnerDocument();
        DOMImplementationLS domImplLS = (DOMImplementationLS) document.getImplementation();
        LSSerializer serializer = domImplLS.createLSSerializer();
        logger.debug("Returning from: generateXML");
        return serializer.writeToString(element);
    }

    @Override
    public ODataType loadODataType(String dtName , OUser loggedUser)
    {
        logger.debug("Entering");
        ODataType dataType = cachedODataType.get(loggedUser.getTenant().getId() + "_" + dtName);
        if (dataType != null) {
            logger.debug("DataType Got From Cache: {}",dataType);
            logger.debug("Returning");
            return dataType ;
        }
        
        try {
            dataType = (ODataType) oem.loadEntity(ODataType.class.getSimpleName(),
                    Collections.singletonList("name = '" + dtName + "'"), null, loggedUser);
            if (dataType != null) 
            {
                cachedODataType.put(loggedUser.getTenant().getId() + "_" + dtName, dataType);
                logger.info("DataType Not Found In Cache & Loaded : {}",dataType);
                
            } else {
                logger.warn("DataType Not Found");
                // </editor-fold>
            }
            
        } catch (Exception ex) {
            
            logger.error("Exception thrown",ex);

        } finally {
        }
        logger.info("Returning");
        return dataType;
    }
    /**
     * generates the Element for the passed XML String
     *
     * @param XMLstring
     *      The XML String to be converted an Element
     *
     * @return  The corresponding Element
     */
    public Element generateElement(String XMLstring) {
        logger.debug("Entering");
        try {
            logger.trace("XMLString",XMLstring);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new InputSource(new StringReader(XMLstring)));
            logger.debug("Returning ");
            return doc.getDocumentElement();
        }
        catch (Exception ex) {
            logger.error("Exception thrown",ex);
            logger.debug("Returning");
            return null;
        }
    }


    /**
     * generates the XML string for the passed ODataMessage
     *
     * @param dm
     *      The ODataMessage to be converted to an XML String
     *
     * @return  The corresponding XML string
     */
    public String generateXML(ODataMessage dataMessage) {
        logger.debug("Entering: generateXML");
        //  ODataAttributes of the retrieved ODataType
        //  (needed for the XML creation)
        ODataType dataType = dataMessage.getODataType();
        //  get ODataTypeAttributes
        List<ODataTypeAttribute> attributes = dataType.getODataTypeAttribute();

        //  Attributes need to be sorted to ensure that the data
        //  in the XML is inserted in the correct order
        attributes = sortAttributes(attributes);
        //  It is assumed that the data in the passed ODataMessage are
        //  already in the correct order

        //  get data of passed ODataMessage
        List<Object> data = dataMessage.getData();

        //-----------------------------------------------------------------

        StringBuffer result = new StringBuffer();
        result.append("<" + dataType.getName());
        result.append(" xmlns:ns0=\"" + dataType.getCustom1() + "\"");

        for(ODataTypeAttribute attribute : attributes) {
            if (attribute.getOentity() != null)
                if (attribute.getCustom1() != null || !attribute.getCustom1().equals(""))
                    result.append(" xmlns:ns" + attribute.getSeq() + "=\"" + attribute.getCustom1() + "\"");
        }

        result.append(">");

        //-----------------------------------------------------------------

        for(ODataTypeAttribute attribute : attributes)
        {
            try {
                //  Get sequence of attribute data in the list of data
                int seq = attribute.getSeq();

                //In case of data is empty or size < attributes.size
                if((seq - 1) >= data.size())
                {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.warn("Generating XML from DataMessage Error, But not fatal");
                    logger.debug("dataMessage.getData().size(): {}  attributes.size():{}",
                            dataMessage.getData().size(), attributes.size());
                    // </editor-fold>
                    break;
                }
                //  Retrieve object using acquired sequence
                Object obj = data.get(seq - 1);

                result.append("\t<ns0:" + attribute.getName() + ">");

                //  get OEntity representing the current attribute
                OEntity entity = attribute.getOentity();

                if (obj != null) {
                    //  if the retrieved OEntity is not null...
                    //  (i.e. if the attribute is an OEntity...)
                    if (entity != null) {
                        if (!attribute.getMultiValue()) {
                            //  if the entity (from attribute) matches that of the
                            //  object type...
                            if (entity.getEntityClassPath().equals(obj.getClass().getName())) {
                                //  print entity tag in addition to underlying tags
                                //  (recursion is used to print the underlying tags)
                                result.append(printEntityTag("\t\t", obj, false, attribute.getSeq()));
                            }
                            else {
                                //  skip and print an error message
                                logger.warn("ERROR: attribute entity and object class type do not match.");//FIXME: Use OLog
                            }
                        }
                        //  if it is a List...
                        else {
                            List<Object> objList = (List<Object>) obj;
                            for (Object o: objList) {
                                if (o != null) {
                                    //  if the entity (from attribute) matches that of the
                                    //  object type...
                                    if (entity.getEntityClassPath().equals(o.getClass().getName())) {
                                        //  print entity tag in addition to underlying tags
                                        //  (recursion is used to print the underlying tags)
                                        result.append(printEntityTag("\t\t", o, false, attribute.getSeq()));
                                    }
                                    else {
                                        //  skip and print an error message
                                        logger.error("ERROR: attribute entity and object class type do not match.");
                                    }
                                }
                            }
                        }
                    }
                    //  if it is not an OEntity (i.e. a primitive type)...
                    else {
                        //  If the data items consists of a List...
                        if (obj.getClass().getName().endsWith("List")) {
                            //  YET TO BE HANDLED
                        }
                        else if (obj != null)
                            //  print the value
                            result.append(obj.toString());
                    }
                }

                result.append("</ns0:" + attribute.getName() + ">\n");
            }
            catch (Exception ex) {
                logger.error("Exception thrown",ex);
                logger.debug("Returning from: generateXML");
                return null;
            }
            result.append("\n");
        }

        result.append("</" + dataType.getName() + ">");
        logger.debug("Returning from: generateXML");
        return result.toString();
        
    }



    public SOAPMessage generateProcessSOAPMessage(OProcess process, ODataMessage odm)
    {
        logger.debug("Entering: generateProcessSOAPMessage");
        //  ODataAttributes of the retrieved ODataType
        //  (needed for the XML creation)
        ODataType dataType = odm.getODataType();

        SOAPMessage message = new SOAPMessage();
        message.setFooter(preparefooter());
        
        if(odm == null || odm.getData()==null || odm.getData().isEmpty())
        {
            message.setBody("<void:unused>test</void:unused>");
            message.setHeader(prepareHeader(process.getCustom1()));
        }
        else
        {
            message.setHeader(prepareHeader(process.getCustom1()));
            logger.debug("IS NOT NULL");
            message.setBody(generateSOAPBody(odm, process).getBody());
        }
        logger.debug("Returning from: generateProcessSOAPMessage");
        return message;
    }

    private String prepareHeader(String namespace)
    {
        logger.trace("Entering: prepareHeader");
        String header =
                "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
                "xmlns:DT=\"" + namespace + "\" " +
                "xmlns:otms=\"http://unitedofoq.com/otms\">\n<soap:Header/>\n<soap:Body>\n<request>";
        logger.trace("Returning from: prepareHeader");
        return header;
    }

    private String prepareIntalioHeader(String processNamespace, String datatypeNamespace)
    {
        logger.trace("Entering: prepareIntalioHeader");
        String header = "";
        header = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/s" +
                "oap/envelope/\" xmlns:DT=\"" + processNamespace + "\" xmlns:datatype" +
                "=\"" + datatypeNamespace + "\" xmlns:otms=\"http://unitedofoq.com/otms\"><soapenv:Header/>";
        logger.trace("Returning from: prepareIntalioHeader");
        return header;
    }

    private String prepareIntalioFooter()
    {
        logger.trace("Entering: prepareIntalioFooter");
        logger.trace("Returning from: prepareIntalioFooter");
        return "</soapenv:Envelope>";
    }


    private String preparefooter()
    {
        logger.trace("Entering: preparefooter");
        logger.trace("Returning from: preparefooter");
        return "\n</request>\n</soap:Body>\n</soap:Envelope>";
    }
    /**
     * generates the SOAP message for the passed ODataMessage
     *
     * @param dm
     *      The ODataMessage to be converted to a SOAP message
     *      (The ODataMessage is assumed to be complete with the ODataType,
     *      ODataTypeAttributes and data)
     * @param process
     *      The related OProcess
     *      (The process name will be used in the SOAP message)
     *
     * @return  The corresponding SOAP message
     */
    public String generateSOAPMessage(ODataMessage dm, OProcess proc) {
        logger.debug("Entering: generateSOAPMessage");
        //  Prepare process name that will be used in the SOAPXML message.
        //  Process Name in SOAP message is the same as that of
        //  the passed OProcess, but with the first letter in lowercase.
        
        String processName
                = Character.toString(proc.getName().charAt(0)).toLowerCase()
                + proc.getName().substring(1);

        String SOAP = "";

        //  XML Header
        SOAP +="<soapenv:Envelope "
            + "xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" "
            + "xmlns:otms=\"http://unitedofoq.com/otms\">\n"
            + "<soapenv:Header/>\n"
            + "<soapenv:Body>\n\n";

        //  Process Header
        SOAP += "\t<" + processName + ">\n";

        //  ODataAttributes of the retrieved ODataType
        //  (needed for the SOAP message creation)
        List<ODataTypeAttribute> attributes
                = dm.getODataType().getODataTypeAttribute();

        //  The data corresponding to the retrieved ODataAttributes
        List<Object> data =  dm.getData();

        //  Attributes need to be sorted to ensure that the data
        //  in the SOAP message is inserted in the correct order
        attributes = sortAttributes(attributes);
        //  It is assumed that the data in the passed ODataMessage are
        //  already in the correct order

        //  Loop over the ODataAttributes to write their elements in the
        //  resulting SOAP message
        for (ODataTypeAttribute attribute : attributes) {
            //  Header of current attribute
            SOAP += "\t\t<" + attribute.getName() + ">";

            //  Get sequence of attribute data in the list of data
            int seq = attribute.getSeq();

            //  Retrieve object using acquired sequence
            Object obj = data.get(seq - 1);

            //  get OEntity representing the current attribute
            OEntity entity = attribute.getOentity();

            if (obj != null) {
                //  if the retrieved OEntity is not null...
                //  (i.e. if the attribute is an OEntity...)
                if (entity != null) {
                    //  if the entity (from attribute) matches that of the
                    //  object type...
                    if (entity.getEntityClassPath().equals(obj.getClass().getName())) {
                        SOAP += "\n";
                        //  print entity tag in addition to underlying tags
                        //  (recursion is used to print the underlying tags)
                        SOAP += printEntityTag("\t\t\t", obj, true, attribute.getSeq());
                        SOAP += "\t\t";
                    }
                    else {
                        //  skip and print an error message
                        logger.warn("ERROR: attribute entity and object class type do not match.");
                    }
                }
                //  if it is not an OEntity (i.e. a primitive type)...
                else {
                    //  If the data items consists of a List...
                    if (obj.getClass().getName().endsWith("List")) {
                        //  YET TO BE HANDLED
                    }
                    else
                        //  print the value
                        SOAP += obj.toString();
                }
            }

            //  Footer of current attribute
            SOAP += "</" + attribute.getName() + ">\n\n";
        }

        //  Process Footer
        SOAP += "\t</" + processName + ">\n\n";

        //  XML Footer
        SOAP += "</soapenv:Body>\n";
        SOAP += "</soapenv:Envelope>";

        //  return SOAP message
        logger.debug("Returning from: generateSOAPMessage");
        return SOAP;
    }

    public SOAPMessage generateIntalioSOAPBody(ODataMessage dm, OProcess proc)
    {
        logger.debug("Entering: generateIntalioSOAPBody");
        SOAPMessage message = new SOAPMessage();
        List<ODataTypeAttribute> attributes = new ArrayList<ODataTypeAttribute>();

        String SOAP = "<soapenv:Body><DT:requestRequest>";
        attributes = dm.getODataType().getODataTypeAttribute();
        List<Object> data =  dm.getData();
        if(attributes !=null && attributes.size() > 1)
        {
            attributes = sortAttributes(attributes);
        }
        if(attributes != null)
        {
            for (ODataTypeAttribute attribute : attributes) {
                //  Header of current attribute
                SOAP += "\t\t<datatype:" + attribute.getName() + ">";

                //  Get sequence of attribute data in the list of data
                int seq = attribute.getSeq();

                //  Retrieve object using acquired sequence
                Object obj = data.get(seq - 1);

                //  get OEntity representing the current attribute
                OEntity entity = attribute.getOentity();

                if (obj != null) {
                    //  if the retrieved OEntity is not null...
                    //  (i.e. if the attribute is an OEntity...)
                    if (entity != null) {
                        //  if the entity (from attribute) matches that of the
                        //  object type...

                        SOAP += "\n";
                        //  print entity tag in addition to underlying tags
                        //  (recursion is used to print the underlying tags)
                        SOAP += printEntityTag("\t\t\t", obj, true, attribute.getSeq());
                        SOAP += "\t\t";
                    }
                    //  if it is not an OEntity (i.e. a primitive type)...
                    else {
                        //  If the data items consists of a List...
                        if (obj.getClass().getName().endsWith("List")) {
                            //  YET TO BE HANDLED
                        }
                        else
                            //  print the value
                            SOAP += obj.toString();
                    }
                }

                //  Footer of current attribute
                SOAP += "</datatype:" + attribute.getName() + ">\n\n";
            }
         }
         else
         {
             logger.debug("NO Attributes");
         }
        SOAP += "</DT:requestRequest></soapenv:Body>";
        message.setBody(SOAP);
        logger.debug("Returning from: generateIntalioSOAPBody");
        return message;
    }

     public SOAPMessage generateSOAPBody(ODataMessage dm, OProcess proc) {
         logger.debug("Entering: generateSOAPBody");
        //  Prepare process name that will be used in the SOAPXML message.
        //  Process Name in SOAP message is the same as that of
        //  the passed OProcess, but with the first letter in lowercase.

         List<ODataTypeAttribute> attributes = new ArrayList<ODataTypeAttribute>();
        String SOAP = "";
               attributes = dm.getODataType().getODataTypeAttribute();
        
        logger.debug("Attributes FOUND");
        //  The data corresponding to the retrieved ODataAttributes
        List<Object> data =  dm.getData();
        logger.debug("DATA LOADED");
        //  Attributes need to be sorted to ensure that the data
        //  in the SOAP message is inserted in the correct order
        if(attributes !=null && attributes.size() > 1)
        {
            attributes = sortAttributes(attributes);
        }
        else
        {
            System.out.println("Attributes are null or Only ONE ATTRIBUTE");
        }
         
        //  It is assumed that the data in the passed ODataMessage are
        //  already in the correct order

        //  Loop over the ODataAttributes to write their elements in the
        //  resulting SOAP message
         if(attributes != null)
         {
        for (ODataTypeAttribute attribute : attributes) {
            logger.debug("Processing Attribute ",attribute.getName());
            //  Header of current attribute
            SOAP += "\t\t<DT:" + attribute.getName() + ">";

            //  Get sequence of attribute data in the list of data
            int seq = attribute.getSeq();

            //  Retrieve object using acquired sequence
            Object obj = data.get(seq - 1);

            //  get OEntity representing the current attribute
            OEntity entity = attribute.getOentity();

            if (obj != null) {
                //  if the retrieved OEntity is not null...
                //  (i.e. if the attribute is an OEntity...)
                if (entity != null) {
                    //  if the entity (from attribute) matches that of the
                    //  object type...
                    
                    SOAP += "\n";
                    //  print entity tag in addition to underlying tags
                    //  (recursion is used to print the underlying tags)
                    SOAP += printEntityTag("\t\t\t", obj, true, attribute.getSeq());
                    SOAP += "\t\t";
                }
                //  if it is not an OEntity (i.e. a primitive type)...
                else {
                    //  If the data items consists of a List...
                    if (obj.getClass().getName().endsWith("List")) {
                        //  YET TO BE HANDLED
                    }
                    else
                        //  print the value
                        SOAP += obj.toString();
                }
            }

            //  Footer of current attribute
            SOAP += "</DT:" + attribute.getName() + ">\n\n";
        }
         }
         else
         {
             logger.debug("NO Attributes");
         }

       SOAPMessage message = new SOAPMessage();
       message.setBody(SOAP);
       logger.debug("Returning from: generateSOAPBody");
        //  return SOAP message
        return message;
    }

    /**
     * adds the tag for the passed object (usually an entity) to the passed SOAP
     * String, and returns it
     *
     * @param SOAP
     *      The SOAP string to which the tag of the passed object will be added
     * @param indentation
     *      A String representing the indentation level of the tag to be printed
     * @param obj
     *      The object for which the tag will be added
     *
     * @return  The SOAP string after modifications
     */
    public String printEntityTag(String indentation, Object obj, boolean SOAP, int seq) {
        logger.debug("Entering: printEntityTag");
        String TAG = "";
        for (Field fld : getFields(obj.getClass())) {
            if (fld.getName().contains("_persistence")) continue;
            try {
                //  There are some frequently used constants in OTMS that
                //  have the final modifier

                //  if there is a 'final' modifier associated to the field...
                //  (i.e. if it is one of the frequently used OTMS constants)
                String modifiers = Modifier.toString(fld.getModifiers());
                if (modifiers.indexOf("final") != -1)
                    //  SKIP
                continue;
                //  if there is a 'static' modifier associated to the field...
                //  (i.e. if it is one of the frequently used OTMS constants)
                if (modifiers.indexOf("static") != -1)
                    //  SKIP
                continue;

                //  if it is an @Transient field...
                if (fld.getAnnotation(Transient.class) != null)
                    //  SKIP
                    continue;

//                //  Header of current field
//                TAG += indentation
//                        + "<"
//                        + ((SOAP) ? "" : "otms:")
//                        + fld.getName()
//                        + ">";


                /* Get value of current field by using its getter */
                //  to hold the name of the getter
                String methodName = "";

                //  if the current field is of type boolean
                if (fld.getType().getName().equals("boolean"))
                    //  then its getter starts with "is"
                    methodName = "is";
                else
                    //  otherwise, its getter starts with "get"
                    methodName = "get";

                //  Capitalize first letter of the field's name, then add the
                //  field name to 'methodName'
                methodName += Character.toString(fld.getName().charAt(0)).toUpperCase()
                            + fld.getName().substring(1);

                //  invoke getter to retrieve the current field's value
                Object fldValue
                        = obj.getClass().getMethod(methodName).invoke(obj);
                
                
                /* Value of current field retrieved */


                if (fldValue != null) {
 
                    //  Header of current field
                    TAG += indentation
                        + "<"
                        + ((SOAP) ? "otms:" : "ns" + seq + ":")
                        + fld.getName()
                        + ">";

                    //  if it is an OTMS entity...
                    if (fldValue.getClass().getName().startsWith("com.unitedofoq")) {
                        // Commented By Mostafa El-Zaher as we use lazy loading,
                        //whick means that No Object inside an object will be loaded (null value)
//                        TAG += "\n";
//                        //  print entity tag in addition to underlying tags
//                        //  (recursion is used to print the underlying tags)
//                        TAG += printEntityTag(indentation + "\t", fldValue, SOAP, seq);
//                        TAG += indentation;
                    }
                    else {
                        //  if it is not of type List...
                        if (!fldValue.getClass().getName().endsWith("List"))

                            // Validation for Boolean Values to be 1 or 0 instead of True and False
                            if(fldValue.toString().contains("true"))
                            {
                                TAG += 1;
                            }
                            else if(fldValue.toString().contains("false"))
                            {
                                TAG += 0;
                            }
                            else
                            {
                                TAG += fldValue.toString();
                            }
                        else {
                            //  YET TO BE HANDLED
                        }
                    }

                    //  Footer of current field
                    TAG += "</" 
                    	+ ((SOAP) ? "otms:" : "ns" + seq + ":")
                    	+ fld.getName() 
                    	+ ">\n";
                }

//                //  Footer of current field
//                TAG += "</otms:" + fld.getName() + ">\n";
            }
            catch (NoSuchMethodException ex) {
                 logger.error("Exception thrown",ex);
                TAG += indentation
                        + "<"
                        + ((SOAP) ? "otms:" : "ns" + seq + ":")
                        + fld.getName()
                        + "/>\n";
            }
            catch (IllegalAccessException ex) {
                 logger.error("Exception thrown",ex);
            }
            catch (InvocationTargetException ex) {
                logger.error("Exception thrown",ex);
            }
            
            //  If an exception occurs that is not one of the above exceptions,
            //  it is to be thrown to the caller function.
        }
        logger.debug("Returning from: printEntityTag");
        return TAG;
    }


    /**
     * gets all the fields for the passed class (including inherited fields)
     *
     * @param cls
     *      The Class object
     *
     * @return  An array of type 'Field' containing all fields of the class
     */
    public Field[] getFields (Class cls) {
        logger.debug("Entering: getFields");
        //  Since the getDeclaredFields() functions gets only the fields
        //  declared in 'cls', and not the inherited ones, recursion is needed
        //  to retrieve the inherited fields

        //  will contain all the fields to be returned
        Field [] overallFields;

        //  if 'cls' does not inherit another class...
        if (cls.getSuperclass().getName().equals("java.lang.Object")) {
            //  then only the declared fields of 'cls' are to be returned
            overallFields = cls.getDeclaredFields();
        }
        //  if 'cls' inherits another class...
        else {
            //  declared fields
            Field [] declaredFields = cls.getDeclaredFields();

            //  inherited fields
            Field [] superclassFields = getFields(cls.getSuperclass());

            //  overallFields = declaredFields + superclassFields
            overallFields = new Field[declaredFields.length + superclassFields.length];

            //  loop over 'declaredFields' and 'superclassFields' to add them
            //  to 'overallFields'
            int i = 0;
            for (; i<superclassFields.length; i++) {
                overallFields[i] = superclassFields[i];
            }

            for (int j=0; j<declaredFields.length; j++) {
                overallFields[j+i] = declaredFields[j];
            }
        }
        logger.debug("Returning from: getFields");
        //  return fields of 'cls'
        return overallFields;
    }


    /**
     * generates the ODataMessage object for an Element
     *
     * @param data
     *      The Element containing the data
     * @param inputDataType
     *      The ODataType to be used in the ODataMessage
     *
     * @return
     *      The ODataMessage object
     */
    public ODataMessage generateODataMessage(Element data, ODataType dt) {
        logger.debug("Entering: generateODataMessage");
        //  Since the passed ODataType will often be passed with an empty
        //  ODataAttribute list, the ODataType must be reloaded from the
        //  with the corresponding ODataAttribute list
//        loadEntityDetail isn't used anymore as we are working today with Local EJBs
//        List<String> fieldExpressions = new ArrayList<String>();
//        fieldExpressions.add("oDataTypeAttribute");
//        try {
//            //  Reload ODataType with the corresponding ODataAttribute list
//            dt = (ODataType) oem.loadEntityDetail(dt, null, fieldExpressions, oem.getSystemUser());
//        }
//        catch (Exception ex) {
//            System.out.println("\nException:");
//            ex.printStackTrace();
//            System.out.println();
//        }

        //  Attributes in the passed ODataType 'dt' need to be sorted to ensure
        //  that the data in the ODataMessage is inserted in the correct order
        List<ODataTypeAttribute> attributes = dt.getODataTypeAttribute();
        attributes = sortAttributes(attributes);
        //  It is assumed that the data in the passed element 'data' are
        //  already in the correct order

        //  Get the <request> tag that is directly under
        //  the current <htdt:taskData> tag
        Node node = data.getFirstChild();

        //  The children of the <request> tag should contain the data
        //  to be put in the generated ODataMessage
        NodeList nodelist = node.getChildNodes();

        //  if the number of data items in 'nodelist' is not equal to the
        //  number of ODataTypeAttributes in 'dt'...
        if (nodelist.getLength() != attributes.size()) {
            //  print a warning message
            logger.warn("Number of data items in passed element is not equal to the number of ODataAttributes in the passed ODataType");
        }

        //  Retrieve the data for the ODataMessage
        List<Object> dmData = new ArrayList();
        Object attributeValue;

        //  Loop over the nodes to obtain the data
        for (int i=0; i<nodelist.getLength(); i++) {
            Node child = nodelist.item(i);
            if(child == null) continue;
            if (child.getFirstChild() != null) {
                //  get corresponding attribute
                ODataTypeAttribute attribute = attributes.get(i);

                //  if it is an entity
                if (attribute.getOentity() != null) {
                    try {
                        String className = attribute.getOentity().getEntityClassPath();
                        Object value = generateObject((Element) child, Class.forName(className));
                        dmData.add(value);
                    }
                    catch (Exception ex) {
                        logger.error("Exception thrown",ex);
                        logger.warn(
                                "OEntity Value Could Not Be Added to data list "
                              + "of the ODataMessage\n");

                        dmData.add(null);
                    }
                }

                //  if it is not an entity and not of type Void
                else if (!attribute.getPrimitiveTypeClassPath().equals("java.lang.Void")) {
                    String value = child.getFirstChild().getNodeValue();
                    if (attribute.getPrimitiveTypeClassPath().equalsIgnoreCase("java.lang.Boolean")) {
                        if(value.contains("0"))
                            dmData.add(Boolean.FALSE);
                        else
                            if(value.contains("1"))
                                dmData.add(Boolean.TRUE);
                        else
                            dmData.add(null);
                    }
                    else if(attribute.getPrimitiveTypeClassPath().equalsIgnoreCase("java.lang.Long"))
                    {
                        Long longVal = new Long(0);
                        try
                        {
                            longVal = new Long(value);
                        }
                        catch(Exception ex)
                        {
                            longVal = new Long(0);
                        }
                        finally
                        {
                            dmData.add(longVal);
                        }
                    }
                    else if(attribute.getPrimitiveTypeClassPath().equalsIgnoreCase("java.lang.String"))
                    {
                        dmData.add(value);
                    }
                    else
                    {
                        // TO-DO make sure that all other types (Generic Solution is implemeneted)
                        dmData.add(null);
                    }
                }
            }
        }

        //  Generate ODataMessage
        ODataMessage dm = new ODataMessage();
        dm.setODataType(dt);     //  Set DataType
        dm.setData(dmData);      //  Set Data
        logger.debug("Returning from: generateODataMessage");
        //  return generated ODataMessage
        return dm;
    }


    /**
     * generates the Object for the passed Element
     *
     * @param node
     *      The passed Element
     * @param cls
     *      The Class of the object to be generated
     *
     * @return  The corresponding object
     */
    public Object generateObject(Element node, Class cls) {
        logger.debug("Entering: generateObject");
        try {
            JAXBContext context = JAXBContext.newInstance(cls);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            logger.debug("Returning from: generateObject");
            return unmarshaller.unmarshal(node);
        }
        catch(Exception ex)
        {
            logger.error("Exception thrown",ex);

            try {
                logger.debug("Returning from: generateObject");
                return cls.newInstance();
            }
            catch (Exception exc) {
                logger.error("Exception thrown",ex);
                logger.debug("Returning from: generateObject");
                return null;
            }
        }
    }


    /**
     * generates and saves the ODataType for the passed BaseEntity
     *
     * @param entity
     *      The passed BaseEntity
     * @param loggedUser
     *      The user requesting the generation of the ODataType
     */
    public void generateODataType(BaseEntity entity, OUser loggedUser) {
        logger.debug("Entering: generateODataType");
        ODataTypeAttribute oDataTypeAttribute = new ODataTypeAttribute();
        oDataTypeAttribute.setMultiValue(Boolean.FALSE);
        oDataTypeAttribute.setOentity((OEntity) entity);
        oDataTypeAttribute.setSeq(1);
        oDataTypeAttribute.setName(entity.getClass().getName().substring(0,1).toLowerCase()+entity.getClass().getName().substring(1));
        List<ODataTypeAttribute> dataTypeAttributes = new ArrayList<ODataTypeAttribute>();
        dataTypeAttributes.add(oDataTypeAttribute);
        ODataType oDataType= new ODataType();
        oDataType.setName(entity.getClass().getName().toLowerCase());
        oDataType.setCc1(entity.getCc1());
        oDataType.setCc2(entity.getCc2());
        oDataType.setCc3(entity.getCc3());
        oDataType.setCc4(entity.getCc4());
        oDataType.setCc5(entity.getCc5());
        oDataType.setCustom1(entity.getCustom1());
        oDataType.setCustom2(entity.getCustom2());
        oDataType.setCustom3(entity.getCustom3());
        oDataType.setActive(true);
        oDataType.setInActive(false);
        oDataType.setDescription(entity.getClass().getName().toLowerCase());
        oDataType.setLastMaintDTime(entity.getLastMaintDTime());
        oDataType.setLastMaintUser(loggedUser);
        oDataType.setSelected(entity.isSelected());
        oDataType.setODataTypeAttribute(dataTypeAttributes);
//        OEntityManager oEntityManager = new OEntityManager();

        try {
            oem.saveEntity(oDataType, loggedUser);
        }
        catch (Exception ex) {
             logger.error("Exception thrown",ex);
        }
        logger.debug("Returning from: generateODataType");
    }


    /**
     * generates and saves the ODataType for the passed OEntity
     *
     * @param entity
     *      The passed OEntity
     * @param loggedUser
     *      The user requesting the generation of the ODataType
     */
    public void generateODataType(OEntity oEntity, OUser loggedUser) {
        logger.debug("Entering: generateODataType");
        ODataTypeAttribute oDataTypeAttribute = new ODataTypeAttribute();
        oDataTypeAttribute.setMultiValue(Boolean.FALSE);
        oDataTypeAttribute.setOentity(oEntity);
        oDataTypeAttribute.setSeq(1);
        oDataTypeAttribute.setName(oEntity.getClass().getName().substring(0,1).toLowerCase()+oEntity.getClass().getName().substring(1));
        List<ODataTypeAttribute> dataTypeAttributes = new ArrayList<ODataTypeAttribute>();
        dataTypeAttributes.add(oDataTypeAttribute);
        ODataType oDataType= new ODataType();
        oDataType.setName(oEntity.getClass().getName().toLowerCase());
        oDataType.setCc1(oEntity.getCc1());
        oDataType.setCc2(oEntity.getCc2());
        oDataType.setCc3(oEntity.getCc3());
        oDataType.setCc4(oEntity.getCc4());
        oDataType.setCc5(oEntity.getCc5());
        oDataType.setCustom1(oEntity.getCustom1());
        oDataType.setCustom2(oEntity.getCustom2());
        oDataType.setCustom3(oEntity.getCustom3());
        oDataType.setActive(true);
        oDataType.setInActive(false);
        oDataType.setDescription(oEntity.getClass().getName().toLowerCase());
        oDataType.setLastMaintDTime(oEntity.getLastMaintDTime());
        oDataType.setLastMaintUser(loggedUser);
        oDataType.setSelected(oEntity.isSelected());
        oDataType.setODataTypeAttribute(dataTypeAttributes);
//        OEntityManager oEntityManager = new OEntityManager();
        
        try {
            oem.saveEntity(oDataType, loggedUser);
        }
        catch (Exception ex) {
            logger.error("Exception thrown",ex);
        }
         logger.debug("Returning from: generateODataType");
    }


    /**
     * sorts the passed list of ODataTypeAttributes
     *
     * @param attributes
     *      The passed list of ODataTypeAttributes
     *
     * @return  The list of ODataTypeAttributes after being sorted
     */
    public List<ODataTypeAttribute> sortAttributes(List<ODataTypeAttribute> attributes) {
        /*  INSERTION SORT  */
        logger.debug("Entering: sortAttributes");
        if (attributes != null && attributes.size() > 1) {
            for (int i=0; i<attributes.size(); i++) {
                for (int j=i+1; j<attributes.size(); j++) {
                    int seqI = attributes.get(i).getSeq();
                    int seqJ = attributes.get(j).getSeq();

                    //  Sort by 'Seq'
                    if (seqI > seqJ) {
                        ODataTypeAttribute tmp = attributes.get(i);
                        attributes.set(i, attributes.get(j));
                        attributes.set(j, tmp);
                    }
                }
            }
        }
        logger.debug("Returning from: sortAttributes");
        return attributes;
    }

    public static void main(String[] args)
    {
        logger.debug("Entering: main");
        UDCType udcType = new UDCType();
        udcType.setDbid(0);
        udcType.setCode("UT");
        udcType.setReserved(true);
        udcType.setValue("UDC Type");

        UDC gender = new UDC();
        gender.setDbid(70);
        gender.setCode("G");
        gender.setReserved(true);
        gender.setType(udcType);
        gender.setValue("Gender");

        UDC male = new UDC();
        male.setDbid(71);
        OUser user = new OUser();
        user.setLoginName("aly");
        user.setPassword("pass");
        male.setLastMaintUser(user);
        male.setCode("M");
        male.setReserved(true);
        male.setValue("Male");
//        male.setType(gender);

//        //String s = generateJavaObjectXML(male);
//        String s = "<udc>    <dbid>71</dbid>    <deleted>false</deleted>    <lastMaintUser>        <dbid>0</dbid>        <deleted>false</deleted>        <selected>false</selected>        <active>true</active>        <loginName>aly</loginName>        <password>pass</password>        <reserved>false</reserved>    </lastMaintUser>    <selected>false</selected>    <active>true</active>    <code>M</code>    <reserved>true</reserved>    <type>        <dbid>70</dbid>        <deleted>false</deleted>        <selected>false</selected>        <active>true</active>        <code>G</code>        <reserved>true</reserved>        <type>            <dbid>0</dbid>            <deleted>false</deleted>            <selected>false</selected>            <active>true</active>            <code>UT</code>            <reserved>true</reserved>            <value>UDC Type</value>        </type>        <value>Gender</value>    </type>    <value>Male</value></udc>";
//        System.out.println(s);
//
//
//        BaseEntity output = (BaseEntity) generateObject(s, BaseEntity.class);
//        System.out.println("output.getDbid(): " + output.getDbid());

//        String s = "<udc><dbid>0</dbid><deleted>false</deleted><selected>false</selected><active>true</active><code>UT</code><reserved>true</reserved><value>UDC Type</value></udc>";
//        UDC retrievedObject = (UDC) generateObject(s, UDC.class);
//        System.out.println( output.getCode() );
//        System.out.println( output.getLastMaintUser().getLoginName() );
//        System.out.println( output.getType().getValue() );


//          Prepare Entities
        OEntity udc = new OEntity();
        udc.setConfiguration(true);
        udc.setEntityClassPath("com.unitedofoq.fabs.core.udc.UDC");
        udc.setTitle("my Title");
//        udc.setParentEntity(null);
        udc.setOActions(new ArrayList<OEntityAction>());

        OEntity baseEntity = new OEntity();
        baseEntity.setConfiguration(true);
        baseEntity.setEntityClassPath("com.unitedofoq.fabs.core.entitybase.BaseEntity");
        baseEntity.setTitle("my Other Title");
//        baseEntity.setParentEntity(null);
        baseEntity.setOActions(new ArrayList<OEntityAction>());

        //  Prepare Attributes
        ODataTypeAttribute udcAttribute, intType, baseEntityAttribute ;
        List<ODataTypeAttribute> attributes = new ArrayList<ODataTypeAttribute>();

        udcAttribute = new ODataTypeAttribute();
        udcAttribute.setName("myUDC");
        udcAttribute.setOentity(udc);
        udcAttribute.setMultiValue(Boolean.FALSE);
        udcAttribute.setSeq(1);
        udcAttribute.setPrimitiveTypeClassPath(null);

        intType = new ODataTypeAttribute();
        intType.setMultiValue(Boolean.FALSE);
        intType.setSeq(2);
        intType.setPrimitiveTypeClassPath("java.lang.Integer");

        baseEntityAttribute = new ODataTypeAttribute();
        baseEntityAttribute.setOentity(baseEntity);
        baseEntityAttribute.setMultiValue(Boolean.FALSE);
        baseEntityAttribute.setSeq(3);
        baseEntityAttribute.setPrimitiveTypeClassPath(null);

        attributes.add(udcAttribute);
        //attributes.add(intType);
        //attributes.add(baseEntityAttribute);

        //  Prepare DataType and Data
        ODataType myInput = new ODataType();        //  DataType
        myInput.setODataTypeAttribute(attributes);  //

        UDC sampleUdc = new UDC();
        sampleUdc.setCode("xfx");
        sampleUdc.setReserved(true);
        sampleUdc.setValue("Hello");

        Integer MSCTO = new Integer(3);

        BaseEntity sampleBaseEntity = new BaseEntity();
        sampleBaseEntity.setDbid(9090);
        sampleBaseEntity.setInActive(false);
        sampleBaseEntity.setSelected(false);

        List<Object> data = new ArrayList<Object>();
        data.add(sampleUdc);
        //data.add(MSCTO);
        //data.add(sampleBaseEntity);

        // Preparing Message
        ODataMessage dataMessage = new ODataMessage();
        dataMessage.setODataType(myInput);
        dataMessage.setData(data);

        DataTypeService dataTypeService = new DataTypeService();

        String s = dataTypeService.generateXML(dataMessage);
        logger.debug("s: {}",s);
        logger.debug("Returning from: main");



    }

    public SOAPMessage generateIntalioProcessSOAPMessage(OProcess process, ODataMessage odm) {
        logger.debug("Entering : generateIntalioProcessSOAPMessage");
        SOAPMessage message = new SOAPMessage();
        message.setBody(generateIntalioSOAPBody(odm, process).getBody());
        message.setHeader(prepareIntalioHeader(process.getCustom1(), odm.getODataType().getCustom1()));
        message.setFooter(prepareIntalioFooter());
        logger.debug("Returning from: generateIntalioProcessSOAPMessage");
        return message;
    }

/**
     * Construct and return ODataMessage object from the 'entity' default ODataType
     * (whose name is the entity class simple name.
     * @param entity
     * @param loggedUser
     * @return
     * @throws FABSException
     */
    public ODataMessage constructEntityDefaultODM(BaseEntity entity, OUser loggedUser)
            throws FABSException
    {
        logger.debug("Entering : constructEntityDefaultODM");
        ODataType entityDT = null ;
        try {
            // Validate passed entity
            if (entity == null)
                //FIXME: generate user message error and put in fex
                throw new FABSException(loggedUser,
                        "Null Entity Passed", null) ;

            // Load Entity Default DataType
            entityDT = loadODataType(entity.getClass().getSimpleName(), loggedUser);

            // Validate datatype
            if (entityDT == null ) {
                //FIXME: generate user message error and put in fex
                FABSException fex = new FABSException(loggedUser,
                        "Entity Default Data Type Not Found", entity) ;
                throw fex ;
            }
            // Construct DataMessage
            List odmData = new ArrayList();
            odmData.add(entity);
			odmData.add(loggedUser);
            ODataMessage odm = new ODataMessage(entityDT, odmData);
            logger.debug("Returning from: constructEntityDefaultODM");
            return odm;

        } catch (Exception ex) {
            if (ex instanceof FABSException )
                throw new FABSException((FABSException) ex) ;
            else
                throw new FABSException(loggedUser, "", entity, ex) ;
            
        }
    }


    @Override
    public ODataMessage constructSingleValueObjectDM(Object value, OUser loggedUser)
    {
        logger.debug("Entering : constructSingleValueObjectDM");
        ODataType dataType =  loadODataType("SingleObjectValue", loggedUser);
        ODataMessage message = new ODataMessage();
        message.setODataType(dataType);
        message.setData(Collections.singletonList(value));
        logger.debug("Returning from: constructSingleValueObjectDM");
        return message;
    }


        private final char NUL_DEL = 0x0;
    private final char LIST_SIZE_DEL = 0x1;
    private final char INT_DEL = 0x2;
    private final char LONG_DEL = 0x3;
    private final char DOUBLE_DEL = 0x4;
    private final char BASE_ENTITY_DEL = 0x5;
    private final char BIGINT_DEL = 0x6;
    private final char BIGDECIMAL_DEL = 0x7;
    private final char DATE_DEL = 0x8;
    private final char END_DEL = 0x9;

    private byte[] concatArrays(byte[] array1, byte[] array2){
        logger.trace("Entering : concatArrays");
        if(array2 == null || array2.length == 0){
            return array1;
        }
        byte[] tmpArr = new byte[array1.length + array2.length];
        System.arraycopy(array1, 0, tmpArr, 0, array1.length);
        System.arraycopy(array2, 0, tmpArr, array1.length, array2.length);
        logger.trace("Returning from: concatArrays");
        return tmpArr;
    }

    @Override
    public byte[] serializeDataMessage(ODataMessage odm, OUser loggOUser){
        logger.debug("Entering : serializeDataMessage");
        byte[] serializedODM = new byte[0];

        if(odm == null){
            logger.debug("Returning from: serializeDataMessage");
            return new byte[0];
        }
        byte[] dtDBIDArray = Long.toString(odm.getODataType().getDbid()).getBytes();
        serializedODM = concatArrays(serializedODM, dtDBIDArray);


        if(odm.getData()!=null){
            byte[] sizeDelArray = {(byte)LIST_SIZE_DEL};
            serializedODM = concatArrays(serializedODM, sizeDelArray);

            byte[] sizeInBytes = Integer.toString(odm.getData().size()).getBytes();
            serializedODM = concatArrays(serializedODM, sizeInBytes);

            for (int i = 0; i < odm.getData().size(); i++) {
                byte[] valArray= null;
                byte[] delArray= null;

                if(odm.getData().get(i) == null){
                    delArray = new byte[]{(byte)NUL_DEL};
                }else if(odm.getData().get(i) instanceof Integer){
                    delArray = new byte[]{(byte)INT_DEL};
                    valArray = odm.getData().get(i).toString().getBytes();
                }else if(odm.getData().get(i) instanceof Long){
                    delArray = new byte[]{(byte)LONG_DEL};
                    valArray = odm.getData().get(i).toString().getBytes();
                }else if(odm.getData().get(i) instanceof Double){
                    delArray = new byte[]{(byte)DOUBLE_DEL};
                    valArray = odm.getData().get(i).toString().getBytes();
                }else if(odm.getData().get(i) instanceof BigInteger){
                    delArray = new byte[]{(byte)BIGINT_DEL};
                    valArray = odm.getData().get(i).toString().getBytes();
                }else if(odm.getData().get(i) instanceof BigDecimal){
                    delArray = new byte[]{(byte)BIGDECIMAL_DEL};
                    valArray = odm.getData().get(i).toString().getBytes();
                }else if(odm.getData().get(i) instanceof Date){
                    delArray = new byte[]{(byte)DATE_DEL};
                    valArray = odm.getData().get(i).toString().getBytes();
                }else if(odm.getData().get(i) instanceof BaseEntity){
                    delArray = new byte[]{(byte)BASE_ENTITY_DEL};
                    String baseEntitySer = odm.getData().get(i).getClass().getSimpleName()
                            + "," + ((BaseEntity) odm.getData().get(i)).getDbid();
                    valArray = baseEntitySer.getBytes();
                }else{
                    logger.warn("Unexpected type to be serialized");
                }

                if(delArray!=null && valArray!=null){
                    serializedODM = concatArrays(serializedODM, delArray);
                    serializedODM = concatArrays(serializedODM, valArray);
                }else if(delArray==null ^ valArray==null){
                    logger.warn("Unexpected Serialization Behavior");
                }

            }
        }else{
            byte[] nullDelArray = {(byte)NUL_DEL};
            serializedODM = concatArrays(serializedODM, nullDelArray);
        }

        byte[] endArray = new byte[]{(byte)END_DEL};
        serializedODM = concatArrays(serializedODM, endArray);
        logger.debug("Returning from: serializeDataMessage");
        return serializedODM;
    }


    @Override
    public ODataMessage deSerializeDataMessage(byte[] odmByte , OUser loggOUser){
        logger.debug("Entering : deSerializeDataMessage");
        ODataType dataType = null;
        List data = new ArrayList();

        int firstIndex =0;
        int lastIndex;
        byte del = 0x10;

        int dataSize;

        for (int i = 0; i < odmByte.length; i++) {
            byte b = odmByte[i];

            if(b < 0x1f){
                lastIndex = i;
                if(firstIndex == 0){
                    byte[] dtDBIdByte = Arrays.copyOfRange(odmByte, 0, lastIndex);
                    long dtDBID = Long.parseLong(new String(dtDBIdByte));

                    try{
                        dataType = (ODataType) oem.loadEntity(ODataType.class.getSimpleName(), dtDBID ,null, null, loggOUser);
                    }catch(Exception ex){
                        logger.error("Exception thrown",ex);
                    }

                    if(b == NUL_DEL){
                        data = null;
                        break;
                    }

                }
                else{
                    byte[] listSizeArray = Arrays.copyOfRange(odmByte, firstIndex, lastIndex);
                    Object dataObj = null;

                    if(del == LIST_SIZE_DEL){
                        dataSize = Integer.parseInt(new String(listSizeArray));
                        if(dataSize == 0) break;
                    }else {
                    if(del == INT_DEL){
                        dataObj = Integer.parseInt(new String(listSizeArray));
                    }else if(del == LONG_DEL){
                        dataObj = Long.parseLong(new String(listSizeArray));
                    }else if(del == DOUBLE_DEL){
                        dataObj = Double.parseDouble(new String(listSizeArray));
                    }else if(del == BIGINT_DEL){
                        dataObj = new BigInteger(new String(listSizeArray));
                    }else if(del == BIGDECIMAL_DEL){
                        dataObj = new BigDecimal(new String(listSizeArray));
                    }else if(del == DATE_DEL){
                        dataObj = new Date(new String(listSizeArray));
                    }else if(del == BASE_ENTITY_DEL){
                        String baseEntityString = new String(listSizeArray);
                        String entityName = baseEntityString.substring(0, baseEntityString.indexOf(","));
                        long entityDBID = Long.parseLong(baseEntityString.substring(baseEntityString.indexOf(",")+1));
                        try {
                            dataObj = oem.loadEntity(entityName, entityDBID, null, null, loggOUser);
                        } catch (Exception ex) {
                            logger.error("Exception thrown: ",ex);
                        }

                    }else if (b == NUL_DEL){
                        //Remains Null
                    }else{
                        logger.warn("Error In Data Message Deserialization");
                    }
                        data.add(dataObj);
                    }
                }
                firstIndex = lastIndex+1;
                del = b;
            }

            if(b == END_DEL)
                break;
        }

        if(dataType!=null){
            ODataMessage odm = new ODataMessage(dataType, data);
            logger.debug("Returning from: deSerializeDataMessage");
            return odm;
        }else{
            logger.warn("Deserializarion Error", loggOUser);
            logger.debug("Returning from: deSerializeDataMessage");
            return null;
        }


    }

    @Override
    public boolean isODMValid(ODataMessage odm) {
        logger.debug("Entering : isODMValid");
        try {
            if (odm == null){
                logger.debug("Returning from: isODMValid");
                return false ;
            }
            if (odm.getODataType() == null){
                logger.debug("Returning from: isODMValid");
                return false ;
            }
            if (!odm.dataIsNotEmpty() )
            // Data Is empty 
                if (odm.getODataType().getDbid() != ODataType.DTDBID_VOID){
                // Data Is empty for Non Void DT
                    logger.debug("Returning from: isODMValid");
                    return false ;
                }
            // TODO: validate data objects match datatype attributes
            logger.debug("Returning from: isODMValid");
            return true ;
        } catch (Exception ex) {
            logger.debug("Returning from: isODMValid");
            logger.error("Exception thrown",ex);
            return false ;            
        } finally {
            
        }
    }
    
    @Override
    public OFunctionResult createMissingDataType(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering : createMissingDataType");
        OFunctionResult oFR = new OFunctionResult();
        try {
            List<String> cond = new ArrayList<String>();
            cond.add(OEntityManager.CONFCOND_GET_ACTIVEONLY);
            List<ODataType> dataTypes = oem.loadEntityList("ODataType", cond, null, null, oem.getSystemUser(loggedUser));
            int c = 0;
            for (int i = 0; i < dataTypes.size(); i++) {
                ODataType dataType = dataTypes.get(i);
                if (!dataType.getName().endsWith("List")) {
                    if (loadODataType(dataType.getName() + "List", loggedUser) == null) {
                        ODataType entityListDataType = new ODataType();
                        entityListDataType.setName(dataType.getName() + "List");
                        entityListDataType.setOmodule(dataType.getOmodule());
                        if (dataType.getOmodule() != null) {
                            oFR.append(entitySetupService.
                                    callEntityCreateAction(entityListDataType, loggedUser));
                            c++;
                        }
                    }
                }
            }
            if (oFR.getErrors().isEmpty()) {
                oFR.addSuccess(usrMsgService.getUserMessage("DataSaved", loggedUser));
            }
        } catch (Exception ex) {
            logger.error("Exception thrown",ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        logger.debug("Returning from : createMissingDataType");
        return oFR;
    }
    
}
