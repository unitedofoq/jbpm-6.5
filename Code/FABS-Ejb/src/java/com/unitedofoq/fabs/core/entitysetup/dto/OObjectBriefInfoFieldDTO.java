/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitysetup.dto;

import java.io.Serializable;

/**
 *
 * @author root
 */
public class OObjectBriefInfoFieldDTO implements Serializable{
    private long dbid;
    private String fieldExpression;
    private boolean primary;
    private String fieldJavaType;
    private int sortIndex;
    private boolean masterField;
    private String titleOverride;
    private String ddLabel;
    private long ddDBID;

    public long getDdDBID() {
        return ddDBID;
    }

    public void setDdDBID(long ddDBID) {
        this.ddDBID = ddDBID;
    }
    

    public String getFieldExpression() {
        return fieldExpression;
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }

    public boolean isPrimary() {
        return primary;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    public String getFieldJavaType() {
        return fieldJavaType;
    }

    public void setFieldJavaType(String fieldJavaType) {
        this.fieldJavaType = fieldJavaType;
    }

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public boolean isMasterField() {
        return masterField;
    }

    public void setMasterField(boolean masterField) {
        this.masterField = masterField;
    }

    public String getTitleOverride() {
        return titleOverride;
    }

    public void setTitleOverride(String titleOverride) {
        this.titleOverride = titleOverride;
    }

    public String getDdLabel() {
        return ddLabel;
    }

    public void setDdLabel(String ddLabel) {
        this.ddLabel = ddLabel;
    }

    
    public long getDbid() {
        return dbid;
    }

    public void setDbid(long dbid) {
        this.dbid = dbid;
    }
    

    public OObjectBriefInfoFieldDTO(String fieldExpression, boolean primary, 
            String fieldJavaType, int sortIndex, boolean masterField,
            String titleOverride, String ddLabel, long ddDBID, long dbid) {
        this.fieldExpression = fieldExpression;
        this.primary = primary;
        this.fieldJavaType = fieldJavaType;
        this.sortIndex = sortIndex;
        this.masterField = masterField;
        this.titleOverride = titleOverride;
        this.ddLabel = ddLabel;
        this.ddDBID = ddDBID;
        this.dbid = dbid;
    }
    
}
