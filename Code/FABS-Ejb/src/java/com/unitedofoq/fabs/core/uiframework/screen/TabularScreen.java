/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;

/**
 * 
* @author arezk
 */
//Overview: Represents the tabular screen type. Contains the necessary information to build a tabular screen.
//Discriminator "TabularScreen"
@Entity
@DiscriminatorValue(value = "TABULAR")
@ChildEntity(fields = {"screenInputs", "screenOutputs", "screenFilter", "screenFunctions",
    "screenActions", "screenFields"})
@FABSEntitySpecs(personalizableFields = {"header", "showActiveOnly", "showHeader", "basic", "fastTrack", "recordsInPage", "sideBalloon", "sortExpression"})
@VersionControlSpecs(omoduleFieldExpression = "omodule")
public class TabularScreen extends SingleEntityScreen {
//Sheet or regular Table

    private boolean sheet;

    public String getSheetDD() {
        return "TabularScreen_sheet";
    }

    public boolean isSheet() {
        return sheet;
    }

    public void setSheet(boolean sheet) {
        this.sheet = sheet;
    }
//Whether you can add a new row or not
    @Column(nullable = false)
    private boolean fastTrack;
//Has a side balloon or not
    @Column(nullable = false)
    private boolean sideBalloon;
//This screen is basic or not
    private String sortExpression;
    private int recordsInPage = 7;

    @Column(nullable = false)
    private boolean duplicate;
    
    public int getRecordsInPage() {
        return recordsInPage;
    }

    public String getRecordsInPageDD() {
        return "TabularScreen_recordsInPage";
    }

    public void setRecordsInPage(int recordsInPage) {
        this.recordsInPage = recordsInPage;
    }

    public String getSortExpression() {
        return sortExpression;
    }

    public void setSortExpression(String sortExpression) {
        this.sortExpression = sortExpression;
    }
    //This screen is basic or not
    @Column(nullable = false)
    private boolean basic;

    public boolean isBasic() {
        return basic;
    }

    public void setBasic(boolean basic) {
        this.basic = basic;
    }

    /* DD NAMES GETTERS */
    public String getFastTrackDD() {
        return "TabularScreen_fastTrack";
    }

    public String getSideBalloonDD() {
        return "TabularScreen_sideBalloon";
    }

    public String getBasicDD() {
        return "TabularScreen_basic";
    }

    public String getSortExpressionDD() {
        return "TabularScreen_sortExpression";
    }

    public boolean isFastTrack() {
        return fastTrack;
    }

    public void setFastTrack(boolean fastTrack) {
        this.fastTrack = fastTrack;
    }

    public boolean isSideBalloon() {
        return sideBalloon;
    }

    public void setSideBalloon(boolean sideBalloon) {
        this.sideBalloon = sideBalloon;
    }
    
    public boolean isDuplicate() {
        return duplicate;
    }

    /**
     * @param duplicate the duplicate to set
     */
    public void setDuplicate(boolean duplicate) {
        this.duplicate = duplicate;
    }
    public String getDuplicateDD() {
        return "TabularScreen_duplicate";
    }
}
