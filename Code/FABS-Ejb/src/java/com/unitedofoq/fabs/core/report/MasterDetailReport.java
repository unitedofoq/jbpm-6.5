package com.unitedofoq.fabs.core.report;

import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.unitedofoq.fabs.core.entitysetup.OEntity;

@Entity
@DiscriminatorValue(value = "MASTERDETAIL")
@VersionControlSpecs(omoduleFieldExpression = "omodule")
public class MasterDetailReport extends OReport {

    // <editor-fold defaultstate="collapsed" desc="headerFields">
    @OneToMany(mappedBy="headerFieldReport")
    private List<HeaderField> headerFields;

    public List<HeaderField> getHeaderFields() {
        return headerFields;
    }

    public String getHeaderFieldsDD() {
        return "MasterDetailReport_headerFields";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="headerObjectEntity">
    @JoinColumn(nullable=false)
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private OEntity headerObjectEntity;

    public OEntity getHeaderObjectEntity() {
        return headerObjectEntity;
    }

    public String getHeaderObjectEntityDD() {
        return "MasterDetailReport_headerObjectEntity";
    }

    public void setHeaderObjectEntity(OEntity headerObjectEntity) {
        this.headerObjectEntity = headerObjectEntity;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="detailEntityAttribute">
    @Column(nullable=false)
    private String detailEntityAttribute;

    public String getDetailEntityAttribute() {
        return detailEntityAttribute;
    }

    public String getDetailEntityAttributeDD() {
        return "MasterDetailReport_detailEntityAttribute";
    }

    public void setDetailEntityAttribute(String detailEntityAttribute) {
        this.detailEntityAttribute = detailEntityAttribute;
    }
    //</editor-fold>

}
