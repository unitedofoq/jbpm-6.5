/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.function;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields="serverJob")
public class ServerJobMessage extends BaseEntity{

    // <editor-fold defaultstate="collapsed" desc="serverJob">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private ServerJob serverJob;

    public ServerJob getServerJob() {
        return serverJob;
    }

    public void setServerJob(ServerJob serverJob) {
        this.serverJob = serverJob;
    }

    public String getServerJobDD(){
        return "ServerJobMessage_serverJob";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="sequence">
    private int sequence;

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public String getSequenceDD(){
        return "ServerJobMessage_sequence";
    }
// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="messageType">
    private String messageType;

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessageTypeDD(){
        return "ServerJobMessage_messageType";
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="messageTitle">
    private String messageTitle;

    public String getMessageTitle() {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

    public String getMessageTitleDD(){
        return "ServerJob_messageTitle";
    }
// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="messageText">
    @Column(length=512)
    private String messageText;

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageTextDD(){
        return "ServerJob_messageText";
    }
// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="messageName">
    private String messageName;
    
    public String getMessageName() {
        return messageName;
    }

    public void setMessageName(String messageName) {
        this.messageName = messageName;
    }

    public String getMessageNameDD(){
        return "ServerJob_messageName";
    }
// </editor-fold>

}
