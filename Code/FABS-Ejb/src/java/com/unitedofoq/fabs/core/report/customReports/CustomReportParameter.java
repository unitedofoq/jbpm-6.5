/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.report.customReports;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;

/**
 *
 * @author MEA
 */
@Entity
@ParentEntity(fields = {"customReport"})
public class CustomReportParameter extends BaseEntity {
    //<editor-fold defaultstate="collapsed" desc="title">
    private String title;
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleDD() {
        return "CustomReportParameter_title";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="parameterName">
    private String parameterName;

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterNameDD() {
        return "CustomReportParameter_parameterName";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="parameterDD">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private DD parameterDd;

    public DD getParameterDd() {
        return parameterDd;
    }

    public void setParameterDd(DD parameterDd) {
        this.parameterDd = parameterDd;
    }

    public String getParameterDdDD() {
        return "CustomReportParameter_parameterDd";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="customReport">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name="customreport_DBID")
    private CustomReport customReport;

    public CustomReport getCustomReport() {
        return customReport;
    }

    public void setCustomReport(CustomReport customReport) {
        this.customReport = customReport;
    }

    public String getCustomReportDD() {
        return "CustomReportParameter_customReport";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="fieldEXPR">
    private String fieldEXPR;

    public String getFieldEXPR() {
        return fieldEXPR;
    }

    public void setFieldEXPR(String fieldEXPR) {
        this.fieldEXPR = fieldEXPR;
    }

    public String getFieldEXPRDD() {
        return "CustomReportParameter_fieldEXPR";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="sortIndex">
    private int sortIndex;

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getSortIndexDD() {
        return "CustomReportParameter_sortIndex";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="mandatory">
    private  boolean mandatory;

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }
    
    public String getMandatoryDD() {
        return "CustomReportParameter_mandatory";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Parameter Type">
    private String parameterType;

    public String getParameterType() {
        return parameterType;
    }

    public void setParameterType(String parameterType) {
        this.parameterType = parameterType;
    }
    public String getParameterTypeDD() {
        return "CustomReportParameter_parameterType";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="toString">
    @Override
    public String toString() {
        return "com.unitedofoq.fabs.core.report.customReports.CustomReportParameter[ DBID=" + dbid + " ]";
    }
    public String getHeader(){
        if(title == null || title.isEmpty()){
            return parameterName;
        }
        return title;
    }
    
    //</editor-fold>
}

