/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.report;

import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author melsayed
 */
@Stateless
@EJB(name="java:global/ofoq/com.unitedofoq.fabs.core.report.ReportServiceRemote",
    beanInterface=ReportServiceRemote.class)
public class ReportServiceBean implements ReportServiceRemote {
    
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method" or "Web Service > Add Operation")
 
}
