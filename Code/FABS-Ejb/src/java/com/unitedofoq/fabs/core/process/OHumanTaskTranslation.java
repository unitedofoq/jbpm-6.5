
package com.unitedofoq.fabs.core.process;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class OHumanTaskTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="presentation">
    @Column
    private String presentation;

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public String getPresentation() {
        return presentation;
    }
    // </editor-fold>

}
