/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.content;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Column;
import javax.persistence.Entity;

/**
 *
 * @author lap
 */
@Entity
public class EntityAttachments extends BaseEntity {

    String attachmentID;
    long entityDBID;
    
    @Column(nullable = false)
    String entityClsName;

    @Column
    private String contentManagement;

    private String attachmentUser;

    public String getAttachmentUser() {
        return attachmentUser;
    }

    public void setAttachmentUser(String attachmentUser) {
        this.attachmentUser = attachmentUser;
    }
    
    public String getContentManagement() {
        return contentManagement;
    }

    public void setContentManagement(String contentManagement) {
        this.contentManagement = contentManagement;
    }
    public String getAttachmentID() {
        return attachmentID;
    }

    public void setAttachmentID(String attachmentID) {
        this.attachmentID = attachmentID;
    }

    public long getEntityDBID() {
        return entityDBID;
    }

    public void setEntityDBID(long entityDBID) {
        this.entityDBID = entityDBID;
    }

    public String getEntityClsName() {
        return entityClsName;
    }

    public void setEntityClsName(String entityClsName) {
        this.entityClsName = entityClsName;
    }

    // <editor-fold defaultstate="collapsed" desc="description">
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    // </editor-fold>
}
