/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.function;

/**
 *
 * @author MEA
 */
public class URLFunctionDTO {
    private long dbid;
    private String name;
    private String URL;

    public URLFunctionDTO() {
    }

    public URLFunctionDTO(long dbid, String name, String URL) {
        this.dbid = dbid;
        this.name = name;
        this.URL = URL;
    }

    public long getDbid() {
        return dbid;
    }

    public void setDbid(long dbid) {
        this.dbid = dbid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }
    
}
