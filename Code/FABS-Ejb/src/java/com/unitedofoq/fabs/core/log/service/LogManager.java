/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.log.service;



import com.unitedofoq.fabs.core.log.logger.ILogger;

public class LogManager {

    static private ILogger logger = null;
    static private LogManager logManager = null;
    static private LogSetupConfiguration configObject=null;

    private LogManager() {
        configObject=new LogSetupConfiguration();
        logger=LogFactory.getSingleInstance().getLogger(configObject);
    }

    public static LogManager getInstance() {
        
        //logger=LogFactory.getSingleInstance().getLogger(configObject);
        if (null==logManager) {
            return new LogManager();
        }
        return logManager;
    }

    public void logError(StringBuilder message) {
        logger.logError(message);
    }

    public void logInfo(StringBuilder message) {
        logger.logInfo(message);
    }

    public void logWarn(StringBuilder message) {
        logger.logWarn(message);
    }
}