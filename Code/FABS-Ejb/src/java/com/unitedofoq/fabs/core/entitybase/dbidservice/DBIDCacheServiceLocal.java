package com.unitedofoq.fabs.core.entitybase.dbidservice;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

@Local
public interface DBIDCacheServiceLocal {

    public BaseEntity createNewDbid(BaseEntity entity, OUser loggedUser);

    public Long generateNewDbid(String tableName, boolean suffixApplied, OUser loggedUser);

    public OFunctionResult refreshDbidCache(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
}
