/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.datatype.ODataTypeAttribute;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;

/**
 *
 * @author nsaleh
 */
@Entity
@ParentEntity(fields={"screenDTMapping"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class ScreenDTMappingAttr extends BaseEntity {
    
    // <editor-fold defaultstate="collapsed" desc="forFilter">
    @Column(nullable=false)
    /**
     * Is true when it's required to be used to filter the screen records
     */
    private boolean forFilter;
    /**
     * Return {@link ScreenDTMappingAttr#forFilter}
     */
    public boolean isForFilter() {
        return forFilter;
    }
    /**
     * Set {@link ScreenDTMappingAttr#forFilter}
     */
    public void setForFilter(boolean forFilter) {
        this.forFilter = forFilter;
    }
    public String getForFilterDD() {   
        return "ScreenDTMappingAttr_forFilter";  
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="forInitialization">
    @Column(nullable=false)
    /**
     * Is true when it's required to be set in the newly inserted (by user) row
     */
    private boolean forInitialization;
    /**
     * Return {@link ScreenDTMappingAttr#forInitialization}
     */
    public boolean isForInitialization() {
        return forInitialization;
    }
    /**
     * Set {@link ScreenDTMappingAttr#forInitialization}
     */
    public void setForInitialization(boolean forInitialization) {
        this.forInitialization = forInitialization;
    }
    public String getForInitializationDD() {   
        return "ScreenDTMappingAttr_forInitialization";  
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="fieldExpression">
    private String fieldExpression;
    public String getFieldExpression() {
        return fieldExpression;
    }
    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }
    public String getFieldExpressionDD() {   
        return "ScreenDTMappingAttr_fieldExpression";  
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="odataTypeAttribute">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "DTAtt_DBID")
    private ODataTypeAttribute odataTypeAttribute;
    public ODataTypeAttribute getOdataTypeAttribute() {
        return odataTypeAttribute;
    }
    public void setOdataTypeAttribute(ODataTypeAttribute odataTypeAttribute) {
        this.odataTypeAttribute = odataTypeAttribute;
    }
    public String getOdataTypeAttributeDD() {   
        return "ScreenDTMappingAttr_odataTypeAttribute";  
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="screenDTMapping">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name = "DTMapping_DBID")
    private ScreenDTMapping screenDTMapping;
    public ScreenDTMapping getScreenDTMapping() {
        return screenDTMapping;
    }
    public void setScreenDTMapping(ScreenDTMapping screenDTMapping) {
        this.screenDTMapping = screenDTMapping;
    }
    public String getScreenDTMappingDD() {   
        return "ScreenDTMappingAttr_screenDTMapping";  
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="attributeExpression">
    private String attributeExpression;
    public String getAttributeExpression() {
        return attributeExpression;
    }
    public String getAttributeExpressionDD() {
        return "ScreenDTMappingAttr_attributeExpression";
    }
    public void setAttributeExpression(String attributeExpression) {
        this.attributeExpression = attributeExpression;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="partOfCompKey">
    /**
     * When true, then, it's part of a composite key
     */
    private boolean partOfCompKey;

    public boolean isPartOfCompKey() {
        return partOfCompKey;
    }

    public void setPartOfCompKey(boolean partOfCompKey) {
        this.partOfCompKey = partOfCompKey;
    }
    public String getPartOfCompKeyDD() {
        return "ScreenDTMappingAttr_partOfCompKey";  
    }
    // </editor-fold>
}
