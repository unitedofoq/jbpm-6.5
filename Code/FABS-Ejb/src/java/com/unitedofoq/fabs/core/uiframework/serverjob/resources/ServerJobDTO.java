/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.serverjob.resources;

import com.unitedofoq.fabs.core.usermessage.UserMessageService;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Melad
 */
public class ServerJobDTO {

    private long dbid;
    private long baseAlertDBID;
    private long scheduleExpressionDBID;
    private long scheduleExpressionActOnEntityDBID;
    private String scheduleExpressionActOnEntityClassPath;
    private String scheduleExpressionLannedStartDateExpression;
    private String scheduleExpressionPlannedEndDateExpression;
    private String scheduleExpressionRecurUnitExpression;
    private Date scheduleExpressionPlannedEndDate;
    private Date scheduleExpressionPlannedStartDate;
    private long scheduleExpressionRecurUnitDBID;
    private String scheduleExpressionRecurUnitCode;
    private String functionName;
    private String code;
    private String functionClassPath;
    private long notificationTypeDBID;
    private long oModuleDBID;
final static Logger logger = LoggerFactory.getLogger(ServerJobDTO.class);
    public ServerJobDTO() {
    }

    public ServerJobDTO(long dbid, 
                       long baseAlertDBID,
                        long scheduleExpressionDBID,
                        long scheduleExpressionActOnEntityDBID,
                        String scheduleExpressionActOnEntityClassPath,
                        String scheduleExpressionPlannedEndDateExpression,
                        String scheduleExpressionLannedStartDateExpression,
                        String scheduleExpressionRecurUnitExpression,
                        Date scheduleExpressionPlannedEndDate,
                        Date scheduleExpressionPlannedStartDate,
                        long scheduleExpressionRecurUnitDBID,
                        String scheduleExpressionRecurUnitCode,
                        String functionName, 
                        String code, 
                        String functionClassPath,
                        long notificationTypeDBID,
                        long oModuleDBID) {
        logger.debug("Entering: ServerJobDTO");
        this.dbid = dbid;
        this.baseAlertDBID = baseAlertDBID;
        this.scheduleExpressionDBID=scheduleExpressionDBID;
        this.scheduleExpressionActOnEntityDBID=scheduleExpressionActOnEntityDBID;
        this.scheduleExpressionActOnEntityClassPath=scheduleExpressionActOnEntityClassPath;
        this.scheduleExpressionPlannedEndDateExpression=scheduleExpressionPlannedEndDateExpression;
        this.scheduleExpressionLannedStartDateExpression=scheduleExpressionLannedStartDateExpression;
        this.scheduleExpressionRecurUnitExpression=scheduleExpressionRecurUnitExpression;
        this.scheduleExpressionPlannedEndDate=scheduleExpressionPlannedEndDate;
        this.scheduleExpressionPlannedStartDate=scheduleExpressionPlannedStartDate;
        this.scheduleExpressionRecurUnitDBID=scheduleExpressionRecurUnitDBID;
        this.scheduleExpressionRecurUnitCode=scheduleExpressionRecurUnitCode;
        this.functionName = functionName;
        this.code = code;
        this.functionClassPath = functionClassPath;
        this.notificationTypeDBID=notificationTypeDBID;
        this.oModuleDBID=oModuleDBID;
        logger.debug("Returning: ServerJobDTO");
    }

    public long getBaseAlertDBID() {
        return baseAlertDBID;
    }

    public void setBaseAlertDBID(long baseAlertDBID) {
        this.baseAlertDBID = baseAlertDBID;
    }
    

    public long getDbid() {
        return dbid;
    }

    public long getScheduleExpressionDBID() {
        return scheduleExpressionDBID;
    }

    public long getScheduleExpressionActOnEntityDBID() {
        return scheduleExpressionActOnEntityDBID;
    }

    public String getScheduleExpressionActOnEntityClassPath() {
        return scheduleExpressionActOnEntityClassPath;
    }

    public String getScheduleExpressionLannedStartDateExpression() {
        return scheduleExpressionLannedStartDateExpression;
    }

    public String getScheduleExpressionPlannedEndDateExpression() {
        return scheduleExpressionPlannedEndDateExpression;
    }

    public String getScheduleExpressionRecurUnitExpression() {
        return scheduleExpressionRecurUnitExpression;
    }

    public Date getScheduleExpressionPlannedEndDate() {
        return scheduleExpressionPlannedEndDate;
    }

    public Date getScheduleExpressionPlannedStartDate() {
        return scheduleExpressionPlannedStartDate;
    }

    public long getScheduleExpressionRecurUnitDBID() {
        return scheduleExpressionRecurUnitDBID;
    }

    public String getScheduleExpressionRecurUnitCode() {
        return scheduleExpressionRecurUnitCode;
    }

    public String getFunctionName() {
        return functionName;
    }

    public String getCode() {
        return code;
    }

    public String getFunctionClassPath() {
        return functionClassPath;
    }

    public long getNotificationTypeDBID() {
        return notificationTypeDBID;
    }

    public long getoModuleDBID() {
        return oModuleDBID;
    }

    public void setDbid(long dbid) {
        this.dbid = dbid;
    }

    public void setScheduleExpressionDBID(long scheduleExpressionDBID) {
        this.scheduleExpressionDBID = scheduleExpressionDBID;
    }

    public void setScheduleExpressionActOnEntityDBID(long scheduleExpressionActOnEntityDBID) {
        this.scheduleExpressionActOnEntityDBID = scheduleExpressionActOnEntityDBID;
    }

    public void setScheduleExpressionActOnEntityClassPath(String scheduleExpressionActOnEntityClassPath) {
        this.scheduleExpressionActOnEntityClassPath = scheduleExpressionActOnEntityClassPath;
    }

    public void setScheduleExpressionLannedStartDateExpression(String scheduleExpressionLannedStartDateExpression) {
        this.scheduleExpressionLannedStartDateExpression = scheduleExpressionLannedStartDateExpression;
    }

    public void setScheduleExpressionPlannedEndDateExpression(String scheduleExpressionPlannedEndDateExpression) {
        this.scheduleExpressionPlannedEndDateExpression = scheduleExpressionPlannedEndDateExpression;
    }

    public void setScheduleExpressionRecurUnitExpression(String scheduleExpressionRecurUnitExpression) {
        this.scheduleExpressionRecurUnitExpression = scheduleExpressionRecurUnitExpression;
    }

    public void setScheduleExpressionPlannedEndDate(Date scheduleExpressionPlannedEndDate) {
        this.scheduleExpressionPlannedEndDate = scheduleExpressionPlannedEndDate;
    }

    public void setScheduleExpressionPlannedStartDate(Date scheduleExpressionPlannedStartDate) {
        this.scheduleExpressionPlannedStartDate = scheduleExpressionPlannedStartDate;
    }

    public void setScheduleExpressionRecurUnitDBID(long scheduleExpressionRecurUnitDBID) {
        this.scheduleExpressionRecurUnitDBID = scheduleExpressionRecurUnitDBID;
    }

    public void setScheduleExpressionRecurUnitCode(String scheduleExpressionRecurUnitCode) {
        this.scheduleExpressionRecurUnitCode = scheduleExpressionRecurUnitCode;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setFunctionClassPath(String functionClassPath) {
        this.functionClassPath = functionClassPath;
    }

    public void setNotificationTypeDBID(long notificationTypeDBID) {
        this.notificationTypeDBID = notificationTypeDBID;
    }

    public void setoModuleDBID(long oModuleDBID) {
        this.oModuleDBID = oModuleDBID;
    }
}
