/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.comunication;

import javax.mail.Session;
import javax.mail.Transport;

/**
 *
 * @author Rehab
 */
public interface EmailConnectionManagerLocal {
    
    public Transport Reconnect() throws Exception;
    
}
