package com.unitedofoq.fabs.core.dd;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import javax.persistence.OneToOne;

@Entity
@FABSEntitySpecs(indicatorFieldExp = "name")
@VersionControlSpecs(omoduleFieldExpression = "omodule")
public class DD extends ObjectBaseEntity implements FABSConstants{
    // <editor-fold defaultstate="collapsed" desc="OModule">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private OModule omodule;

    public String getOmoduleDD() {
        return "DD_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Inline Help">
    @Transient
    @Translation(originalField = "inlineHelp")
    private String inlineHelpTranslated;

    public String getInlineHelpTranslated() {
        return inlineHelpTranslated;
    }

    public String getInlineHelpTranslatedDD() {
        return "DD_inlineHelp";
    }

    public void setInlineHelpTranslated(String inlineHelpTranslated) {
        this.inlineHelpTranslated = inlineHelpTranslated;
    }
    @Translatable(translationField = "inlineHelpTranslated")
    @Column
    private String inlineHelp;

    public String getInlineHelpDD() {
        return "DD_inlineHelp";
    }

    public String getInlineHelp() {
        return inlineHelp;
    }

    public void setInlineHelp(String inlineHelp) {
        this.inlineHelp = inlineHelp;
    }
    // </editor-fold>
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC calendarPattern;

    public String getCalendarPatternDD() {
        return "DD_calendarPattern";
    }

    public UDC getCalendarPattern() {
        return calendarPattern;
    }

    public void setCalendarPattern(UDC calendarPattern) {
        this.calendarPattern = calendarPattern;
    }
    private boolean multiSelection;

    public String getMultiSelectionDD() {
        return "DD_multiSelection";
    }

    public boolean isMultiSelection() {
        return multiSelection;
    }

    public void setMultiSelection(boolean multiSelection) {
        this.multiSelection = multiSelection;
    }
    // <editor-fold defaultstate="collapsed" desc="Control Types">
    @Column(nullable = false, unique = true)
    private String name;

    public String getNameDD() {
        return "DD_name";
    }
    // </editor-fold>
    @Translatable(translationField = "labelTranslated")
    private String label;
    @Transient
    @Translation(originalField = "label")
    private String labelTranslated;

    public String getLabelTranslated() {
        return labelTranslated;
    }

    public void setLabelTranslated(String labelTranslated) {
        this.labelTranslated = labelTranslated;
    }

    public String getLabelTranslatedDD() {
        return "DD_label";
    }
    private boolean dropdown;

    public String getDropdownDD() {
        return "DD_dropdown";
    }
    @Translatable(translationField = "headerTranslated")
    private String header;
    @Translation(originalField = "header")
    @Transient
    String headerTranslated;

    public String getHeaderTranslated() {
        return headerTranslated;
    }

    public void setHeaderTranslated(String headerTranslated) {
        this.headerTranslated = headerTranslated;
    }

    public String getHeaderTranslatedDD() {
        return "DD_header";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private UDC controlType;

    public String getControlTypeDD() {
        return "DD_controlType";
    }
    private int controlSize;

    public String getControlSizeDD() {
        return "DD_controlSize";
    }
    private String defaultValue;

    public String getDefaultValueDD() {
        return "DD_defaultValue";
    }
    //@ManyToMany
    //private List<UDC> validations;
//    public String getLookupUDCTypeDD() {
//        return "DD_lookupUDCType";
//    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OScreen lookupScreen;

    public String getLookupScreenDD() {
        return "DD_lookupScreen";
    }
    private boolean balloon;
    private boolean specialChar;
    private String spCharPattern;
    @OneToOne
    private UserMessage patternMessage;

    public String getPatternMessageDD(){
        return "DD_patternMessage";
    }
    public UserMessage getPatternMessage() {
        return patternMessage;
    }

    public void setPatternMessage(UserMessage patternMessage) {
        this.patternMessage = patternMessage;
    }

   

    public String getSpCharPattern() {
        return spCharPattern;
    }

    public void setSpCharPattern(String spCharPattern) {
        this.spCharPattern = spCharPattern;
    }

    public String getSpCharPatternDD(){
        return "DD_spCharPattern";
    }
    public boolean isSpecialChar() {
        return specialChar;
    }

    public void setSpecialChar(boolean specialChar) {
        this.specialChar = specialChar;
    }

    public String getSpecialCharDD(){
        return "DD_specialChar";
    }
    

   
    
    @Column(name = "helpMsgLength")
    private int helpMsgLength;

    public int getHelpMsgLength() {
        return helpMsgLength;
    }
    public String getHelpMsgLengthDD(){
        return "DD_helpMsgLength";
    }
    public void setHelpMsgLength(int helpMsgLength) {
        this.helpMsgLength = helpMsgLength;
    }
    public boolean isBalloon() {
        return balloon;
    }

    public void setBalloon(boolean balloon) {
        this.balloon = balloon;
    }

    public boolean isDropdown() {
        return dropdown;
    }

    public void setDropdown(boolean dropdown) {
        this.dropdown = dropdown;
    }

    public String getBalloonDD() {
        return "DD_balloon";
    }
//    @Translatable(translationField = "lookupFilterTranslated")
    private String lookupFilter;
    private String lookupDropDownDisplayField;
//    @Translation(originalField = "lookupFilter")
//    @Transient
//    private String lookupFilterTranslated;
//
//    /**
//     * @return the lookupFilterTranslated
//     */
//    public String getLookupFilterTranslated() {
//        return lookupFilterTranslated;
//    }
//
//    /**
//     * @param lookupFilterTranslated the lookupFilterTranslated to set
//     */
//    public void setLookupFilterTranslated(String lookupFilterTranslated) {
//        this.lookupFilterTranslated = lookupFilterTranslated;
//    }

    public String getLookupFilterDD() {
        return "DD_lookupFilter";
    }

    public String getLookupDropDownDisplayFieldDD() {
        return "DD_lookupDropDownDisplayField";
    }
    /*  GETTERS  */

    public String getName() {
        return name;
    }

    public String getLabel() {
        return label;
    }

    public String getHeader() {
        return header;
    }

    public UDC getControlType() {
        return controlType;
    }

    public int getControlSize() {
        return controlSize;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    //  public List<UDC> getValidations() {
    //    return validations;
    //}
    public OScreen getLookupScreen() {
        return lookupScreen;
    }

    public String getLookupFilter() {
        return lookupFilter;
    }

    public String getLookupDropDownDisplayField() {
        return lookupDropDownDisplayField;
    }

    /*  SETTERS  */
    public void setName(String name) {
        this.name = name;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public void setControlType(UDC controlType) {
        this.controlType = controlType;
    }

    public void setControlSize(int controlSize) {
        this.controlSize = controlSize;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

//    public void setValidations(List<UDC> validations) {
//        this.validations = validations;
//    }
    public void setLookupScreen(OScreen lookupScreen) {
        this.lookupScreen = lookupScreen;
    }

    public void setLookupFilter(String lookupFilter) {
        this.lookupFilter = lookupFilter;
    }

    public void setLookupDropDownDisplayField(String lookupDropDownDisplayField) {
        this.lookupDropDownDisplayField = lookupDropDownDisplayField;
    }


   
    /**
     * Returns the DD Lookup Type
     * @return 
     */
    public DDLookupType getLookupType() {
        if (controlType.getDbid() == CT_LOOKUPSCREEN) {
            return DDLookupType.DDLT_SCREEN;
        }
        if (controlType.getDbid() == CT_LOOKUPMULTILEVEL) {
            return DDLookupType.DDLT_MLTILVLSCRN;
        }
        if (controlType.getDbid() == CT_DROPDOWN) {
            return DDLookupType.DDLT_DRPDWN;
        }
        if (controlType.getDbid() == CT_LITERALDROPDOWN) {
            return DDLookupType.DDLT_LTRLDRPDN;
        }
        if (controlType.getDbid() == CT_LOOKUPATTRIBUTEBROWSER) {
            return DDLookupType.DDLT_FIELDEXP;
        }
        if (controlType.getDbid() == CT_MULTISELECTIONLIST) {
            return DDLookupType.DDLT_MULTISELECTIONLIST;
        }
        //if(controlType.getDbid() == CT_LOOKUPFILTERBUILDER) return DDLookupType.DDLT_FLTRBLDR;
        if (controlType.getDbid() == CT_LOOKUPTREE) {
            return DDLookupType.DDLT_TREE;
        }
        if (controlType.getDbid() == CT_AUTOCOMPLETE) {
            return DDLookupType.DDLT_AUTOCOMPLETE;
        }

        return DDLookupType.DDLT_NONE;
    }

    /*
     * This method is moved to OFilterServiceBean
     *
     public List<String> parseLookupFilter() {
     if (lookupFilter != null && !lookupFilter.equals("")) {
     //  to hold the parsed conditions
     List<String> conditions = new ArrayList();
     String tmpLookupFilter = lookupFilter;

     //  while the conditions delimiter still exists in 'lookupFilter'...
     while (tmpLookupFilter.indexOf(";") != -1) {
     int delimiterIndex = tmpLookupFilter.indexOf(";");
     String condition = tmpLookupFilter.substring(0, delimiterIndex);
     conditions.add(condition);
     tmpLookupFilter = tmpLookupFilter.substring(delimiterIndex + 1);
     }

     //  Add the last condition that was in 'lookupFilter'
     conditions.add(tmpLookupFilter);

     //  return the ArrayList containing the parsed conditions
     return conditions;
     } else {
     return new ArrayList();
     }
     }
     */
    // <editor-fold defaultstate="collapsed" desc="decimalMask">
    @Column(nullable = true, unique = false)
    private Integer decimalMask;

    /**
     * @return the decimalMask
     */
    public Integer getDecimalMask() {
        return decimalMask;
    }

    /**
     * @param decimalMask the decimalMask to set
     */
    public void setDecimalMask(Integer decimalMask) {
        this.decimalMask = decimalMask;
    }

    /**
     * @return the decimalMaskDD
     */
    public String getDecimalMaskDD() {
        return "DD_decimalMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="usedInReport">
    @Column(nullable = true, unique = false)
    private Boolean usedInReport;

    /**
     * @return the usedInReport
     */
    public Boolean getUsedInReport() {
        return usedInReport;
    }

    /**
     * @param usedInReport the usedInReport to set
     */
    public void setUsedInReport(Boolean usedInReport) {
        this.usedInReport = usedInReport;
    }

    /**
     * @return the usedInReportDD
     */
    public String getUsedInReportDD() {
        return "DD_usedInReport";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="inlineHelp">
    private boolean enableInlineHelp;
    
    public boolean isEnableInlineHelp() {
        return enableInlineHelp;
    }

    public void setEnableInlineHelp(boolean enableInlineHelp) {
        this.enableInlineHelp = enableInlineHelp;
    }
    
    public String getEnableInlineHelpDD() {
        return "DD_enableInlineHelp";
    }
    // </editor-fold>
    
    @Column(nullable = true, unique = false)
    private Boolean htmlAllowed;
    
    public Boolean getHtmlAllowed(){
        return htmlAllowed;
    }
    
    public void setHtmlAllowed(Boolean htmlAllowed){
        this.htmlAllowed = htmlAllowed;
    }
    
    public String getHtmlAllowedDD(){
        return "htmlAllowed_DD";
    }
}
