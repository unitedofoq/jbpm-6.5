package com.unitedofoq.fabs.core.function;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.multimedia.OImage;
import com.unitedofoq.fabs.core.security.user.RoleMenu;
import com.unitedofoq.fabs.core.uiframework.navigationmenu.NMEntityListener;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
@EntityListeners(value={NMEntityListener.class})
@ChildEntity(fields={"menuFunction", "roleMenus"})
@FABSEntitySpecs(indicatorFieldExp="name")
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class OMenu extends ObjectBaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="omodule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
    private OModule omodule;

    public String getOmoduleDD(){
        return "OMenu_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="hierCode">
    private String hierCode;

    public String getHierCode() {
        return hierCode;
    }

    public void setHierCode(String hierCode) {
        this.hierCode = hierCode;
    }

    public String getHierCodeDD(){
        return  "OMenu_hierCode";
    }
    // </editor-fold>
    
    @ManyToOne(fetch = javax.persistence.FetchType.EAGER)
    @JoinColumn(name="menuImage_dbid")
    private OImage menuImage;

    public String getMenuImageDD(){
        return  "OMenu_menuImage";
    }

    public OImage getMenuImage() {
        return menuImage;
    }

    public void setMenuImage(OImage menuImage) {
        this.menuImage = menuImage;
    }


    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    @Transient
    @Translation(originalField="name")
    private String nameTranslated;
    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    @Column(unique=true,nullable=false)
    private String code;
    public String getCodeDD(){
        return "OMenu_code";
    }
    @Translatable(translationField="nameTranslated")
    private String name;

    @Translatable(translationField="descriptionTranslated")
    private String description;
    public String getDescriptionTranslatedDD(){
        return "OMenu_description";
    }
    public String getNameTranslatedDD(){
        return "OMenu_name";
    }
    private boolean reserved = false;

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }

    
    public String getReservedDD(){
        return "OMenu_reserved";
    }

    @OneToMany(mappedBy = "parentMenu")
    private List<OMenu> childMenues;

    public String getChildMenuesDD() {
        return "OMenu_childMenues";
    }

    public List<OMenu> getChildMenues() {
        return childMenues;
    }

    public void setChildMenues(List<OMenu> childMenues) {
        this.childMenues = childMenues;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OMenu parentMenu = null;
    public String getParentMenuDD(){
        return "OMenu_parentMenu";
    }
    @OneToMany(mappedBy="omenu")
    List<OMenuFunction> menuFunction = null;

    public String getMenuFunctionDD() {
        return "menuFunction_menuFunction";
    }

    public List<OMenuFunction> getMenuFunction() {
        return menuFunction;
    }

    public void setMenuFunction(List<OMenuFunction> menuFunction) {
        this.menuFunction = menuFunction;
    }
    
    @OneToMany(mappedBy="omenu")
    protected List<RoleMenu> roleMenus = null;

    public List<RoleMenu> getRoleMenus() {
        return roleMenus;
    }

    public void setRoleMenus(List<RoleMenu> roleMenus) {
        this.roleMenus = roleMenus;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OMenu getParentMenu() {
        return parentMenu;
    }

    public void setParentMenu(OMenu parentMenu) {
        this.parentMenu = parentMenu;
    }

   

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private int sortIndex;

    /**
     * @return the sortIndex
     */
    public int getSortIndex() {
        return sortIndex;
    }

    /**
     * @param sortIndex the sortIndex to set
     */
    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getSortIndexDD(){
        return "OMenu_sortIndex";
    }
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name="largeMenuImage_dbid")
    private OImage largeMenuImage;

    public String getLargeMenuImageDD(){
        return  "OMenu_largeMenuImage";
    }

    public OImage getLargeMenuImage() {
        return largeMenuImage;
    }

    public void setLargeMenuImage(OImage largeMenuImage) {
        this.largeMenuImage = largeMenuImage;
    }
    
}