/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.alert.entities;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;

/**
 *
 * @author root
 */
@Entity
@ParentEntity(fields={"alert"})
public class AlertParam extends BaseEntity{
    private String fieldExpression;
    private int paramIndex;
    @ManyToOne
    private Alert alert;

    public String getFieldExpressionDD() {
        return "AlertParam_fieldExpression";
    }
    
    public String getFieldExpression() {
        return fieldExpression;
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }
    
    public String getParamIndexDD() {
        return "AlertParam_paramIndex";
    }

    public int getParamIndex() {
        return paramIndex;
    }

    public void setParamIndex(int paramIndex) {
        this.paramIndex = paramIndex;
    }
    
    

    public String getAlertDD() {
        return "AlertParam_alert";
    }
    
    public Alert getAlert() {
        return alert;
    }

    public void setAlert(Alert alert) {
        this.alert = alert;
    }    
    
}
