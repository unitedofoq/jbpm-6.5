/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.content.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;

/**
 *
 * @author mibrahim
 */
@Entity
public class DBAttachment extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="description">
    private String description;

    public String getDescriptionDD() {
        return "DBAttachment_description";
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="attachment">
    @Column(columnDefinition = "BLOB(4194304)") // 4MB size
    private byte[] attachment;

    public String getAttachmentDD() {
        return "DBAttachment_attachment";
    }

    public byte[] getAttachment() {
        return attachment;
    }

    public void setAttachment(byte[] attachment) {
        this.attachment = attachment;
    }
    // </editor-fold>  
    // <editor-fold defaultstate="collapsed" desc="attachment mimeType">
    private String mimeType;

    public String getMimeTypeDD() {
        return "DBAttachment_mimeType";
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
    // </editor-fold>  
    // <editor-fold defaultstate="collapsed" desc="attachmentName">
    private String name;

    public String getNameDD() {
        return "DBAttachment_name";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    // </editor-fold> 
}
