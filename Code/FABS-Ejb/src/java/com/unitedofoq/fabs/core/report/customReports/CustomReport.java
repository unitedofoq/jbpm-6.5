/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.report.customReports;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.function.JavaFunction;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author Melad
 */
@Entity
@ChildEntity(fields = {"customReportParameters"})
public class CustomReport extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="reportName">

    @Column(name = "viewEntityFilter")
    private boolean viewEntityFilter;

    @OneToMany(mappedBy = "customReport")
    private List<CustomReportDynamicFilter> customReportDynamicFilter;

    public List<CustomReportDynamicFilter> getCustomReportDynamicFilter() {
        return customReportDynamicFilter;
    }

    public void setCustomReportDynamicFilter(List<CustomReportDynamicFilter> customReportDynamicFilter) {
        this.customReportDynamicFilter = customReportDynamicFilter;
    }

    public String getCustomReportDynamicFilterDD() {
        return "CustomReport_customReportDynamicFilter";
    }

    public boolean isViewEntityFilter() {
        return viewEntityFilter;
    }

    public void setViewEntityFilter(boolean viewEntityFilter) {
        this.viewEntityFilter = viewEntityFilter;
    }

    public String getViewEntityFilterDD() {
        return "CustomReport_viewEntityFilter";
    }

    @Translatable(translationField = "reportNameTranslated")
    private String reportName;

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportName() {
        return reportName;
    }

    @Transient
    @Translation(originalField = "reportName")
    private String reportNameTranslated;

    public String getReportNameTranslated() {
        return reportNameTranslated;
    }

    public void setReportNameTranslated(String reportNameTranslated) {
        this.reportNameTranslated = reportNameTranslated;
    }

    public String getReportNameTranslatedDD() {
        return "CustomReport_reportName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reportFilter">
    @ManyToOne(fetch = javax.persistence.FetchType.EAGER)
    @JoinColumn(name = "customreportfilter_dbid")
    private CustomReportFilter customReportFilter;

    public CustomReportFilter getCustomReportFilter() {
        return customReportFilter;
    }

    public void setCustomReportFilter(CustomReportFilter customReportFilter) {
        this.customReportFilter = customReportFilter;
    }

    public String getCustomReportFilterDD() {
        return "CustomReport_customReportFilter";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ReportParameters">
    @OneToMany(mappedBy = "customReport")
    private List<CustomReportParameter> customReportParameters;

    public List<CustomReportParameter> getCustomReportParameters() {
        return customReportParameters;
    }

    public void setCustomReportParameters(List<CustomReportParameter> customReportParameters) {
        this.customReportParameters = customReportParameters;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="localized">
    private boolean localized;

    public boolean isLocalized() {
        return localized;
    }

    public void setLocalized(boolean localized) {
        this.localized = localized;
    }

    public String getLocalizedDD() {
        return "CustomReport_localized";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="revertLang">
    private boolean revertLang;

    public boolean isRevertLang() {
        return revertLang;
    }

    public void setRevertLang(boolean revertLang) {
        this.revertLang = revertLang;
    }

    public String getRevertLangDD() {
        return "CustomReport_revertLang";
    }
    //</editor-fold>

    @Transient
    private boolean fitPage;

    private boolean applyingFilter = true;

    public boolean isApplyingFilter() {
        return applyingFilter;
    }

    public void setApplyingFilter(boolean applyingFilter) {
        this.applyingFilter = applyingFilter;
    }

    public String getApplyingFilterDD() {
        return "CustomReport_applyingFilter";
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private JavaFunction javaFunctionUserExit;

    public JavaFunction getJavaFunctionUserExit() {
        return javaFunctionUserExit;
    }

    public void setJavaFunctionUserExit(JavaFunction javaFunctionUserExit) {
        this.javaFunctionUserExit = javaFunctionUserExit;
    }

    public String getJavaFunctionUserExitDD() {
        return "CustomReport_javaFunctionUserExit";
    }

    public void setFitPage(boolean fitPage) {
        this.fitPage = fitPage;
    }

    public boolean isFitPage() {
        return fitPage;
    }
}
