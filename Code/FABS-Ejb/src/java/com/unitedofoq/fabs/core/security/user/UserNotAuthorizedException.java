/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.security.user;

import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntityAction;
import com.unitedofoq.fabs.core.security.privilege.OPrivilege;

/**
 *
 * @author ahussien
 */
public class UserNotAuthorizedException extends Exception{

    private String message;

    public UserNotAuthorizedException() {
    }
    @Override
    public String getMessage() {
        return message;
    }

    public UserNotAuthorizedException(UserNotAuthorizedException unaex) {
        this.message = unaex.getMessage() ;
        this.oUser = unaex.oUser ;
        this.unauthOnAction = unaex.unauthOnAction ;
        this.unauthOnEntity = unaex.unauthOnEntity ;
        this.unauthOnPrivilege = unaex.unauthOnPrivilege ;
        this.unauthOnPrivilegeName = unaex.unauthOnPrivilegeName ;
        this.forNA = unaex.forNA ;
    }

    public UserNotAuthorizedException(String message) {
        this.message = message; // to be got from user message
    }

    @Override
    public String toString() {
        return
            (message!=null? "Message: " + message : "")
           +(oUser!=null? ", User: " + oUser.toString() : "")
           +(unauthOnEntity!=null? ", Entity: " + unauthOnEntity.toString() : "")
           +(unauthOnPrivilegeName!=null? ", Privilege Name: " + unauthOnPrivilegeName : "")
           +(unauthOnPrivilege!=null? ", Privilege: " + unauthOnPrivilege.toString() : "")
           +(unauthOnAction!=null? ", Action: " + unauthOnAction : "")
           + ", For N/A: " + forNA
           ;
    }

    private OUser oUser ;

    public OUser getOUser() {
        return oUser;
    }

    public void setOUser(OUser oUser) {
        this.oUser = oUser;
    }
    
    private OEntity unauthOnEntity ;
    public OEntity getUnauthOnEntity() {
        return unauthOnEntity;
    }

    public void setUnauthOnEntity(OEntity unauthOnEntity) {
        this.unauthOnEntity = unauthOnEntity;
    }

    private String unauthOnPrivilegeName ;

    public String getUnauthOnPrivilegeName() {
        return unauthOnPrivilegeName;
    }

    public void setUnauthOnPrivilegeName(String unauthOnPrivilegeName) {
        this.unauthOnPrivilegeName = unauthOnPrivilegeName;
    }
    
    private OPrivilege unauthOnPrivilege ;
    public OPrivilege getUnauthOnPrivilege() {
        return unauthOnPrivilege;
    }

    public void setUnauthOnPrivilege(OPrivilege unauthOnPrivilege) {
        this.unauthOnPrivilege = unauthOnPrivilege;
    }

    private OEntityAction unauthOnAction ;
    public OEntityAction getUnauthOnAction() {
        return unauthOnAction;
    }
    public void setUnauthOnAction(OEntityAction unauthOnAction) {
        this.unauthOnAction = unauthOnAction;
    }
    
    // <editor-fold defaultstate="collapsed" desc="forNA">
    /**
     * Is true if the user is not authorized on the privilege because 
     * there is no {@link RolePrivilege} record found for any role of {@link #oUser}
     * on {@link #unauthOnPrivilege}
     */
    private boolean forNA = false ;

    public boolean isForNA() {
        return forNA;
    }

    public void setForNA(boolean forNA) {
        this.forNA = forNA;
    }
    // </editor-fold>
}
