/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitybase;

import com.unitedofoq.fabs.core.entitybase.FABSDataValidation.FABSDataValidationType;
import com.unitedofoq.fabs.core.entitybase.cloneservice.EntityCloner;
import com.unitedofoq.fabs.core.entitysetup.EntityRequestStatus;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.validation.DateFromToException;
import com.unitedofoq.fabs.core.validation.DateFromToValidation;
import com.unitedofoq.fabs.core.validation.EntityDataValidationException;
import com.unitedofoq.fabs.core.validation.FABSException;
import com.unitedofoq.fabs.core.validation.OEntityDataValidationException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author AbuBakr
 */
@MappedSuperclass
public class BaseEntity implements Serializable {

    private String requestNote;
    final static Logger logger = LoggerFactory.getLogger(BaseEntity.class);
    // <editor-fold defaultstate="collapsed" desc="Instance Management">
    static private long stInstID;
    @Transient
    private Long instID;

    public Long getInstID() {
        return instID;
    }

    public void setInstID(Long instID) {
        this.instID = instID;
    }

    public void generateInstID() {
        if (instID == null) {
            stInstID++;
            instID = stInstID;
        }
    }
    // </editor-fold>

    public void BaseEntity() {
        // Instance Management
        if (instID == null) {
            stInstID++;
            instID = stInstID;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Lifecycle Functions">
    @PrePersist
    protected void PrePersist() {
        logger.debug("Entering");
        // Instance Management
        if (instID == null) {
            stInstID++;
            instID = stInstID;
        }
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.trace("Entity Lifecycle. this: {}", this);
        logger.debug("Returning");
        // </editor-fold>
    }

    @PostPersist
    protected void PostPersist() {
        logger.debug("Entering");
        // moduleInDB = getModule() ; // Highly recommended not to do similar code here
        // for performance concerns
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.trace("Entity Lifecycle. this: {}", this);
        logger.debug("Returning");
        // </editor-fold>
    }

    @PostLoad
    protected void PostLoad() {
        logger.debug("Entering");
        // Instance Management
        if (instID == null) {
            stInstID++;
            instID = stInstID;
        }
        // moduleInDB = getModule() ; // Highly recommended not to do similar code here
        // for performance concerns
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.debug("New Instance Created. InstanceID: {}, DBID: {}", instID, dbid);
        logger.trace("Entity Lifecycle. this: {}", this);
        // </editor-fold>
        logger.debug("Returning");
    }

    @PreUpdate
    protected void PreUpdate() {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.trace("Entity Lifecycle. this: {}", this);
        // </editor-fold>
        logger.debug("Returning");
    }

    @PostUpdate
    protected void PostUpdate() {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.trace("Entity Lifecycle. this: {}", this);
        // </editor-fold>
        logger.debug("Returning");
    }

    @PreRemove
    protected void PreRemove() {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.trace("Entity Lifecycle. this: {}", this);
        // </editor-fold>
        logger.debug("Returning");
    }

    @PostRemove
    protected void PostRemove() {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.trace("Entity Lifecycle. this: {}", this);
        // </editor-fold>
        logger.debug("Returning");
    }
    // </editor-fold>

    public String getRequestNote() {
        return requestNote;
    }

    public void setRequestNote(String requestNote) {
        this.requestNote = requestNote;
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EntityRequestStatus entityStatus;

    public EntityRequestStatus getEntityStatus() {
        return entityStatus;
    }

    public void setEntityStatus(EntityRequestStatus entityStatus) {
        this.entityStatus = entityStatus;
    }
    @Transient
    private boolean changed;

    @Transient
    private boolean preSavedEntity;

    public boolean isPreSavedEntity() {
        return preSavedEntity;
    }

    public void setPreSavedEntity(boolean preSavedEntity) {
        this.preSavedEntity = preSavedEntity;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }
    @Transient
    private boolean encyrpted;

    public boolean isEncyrpted() {
        return encyrpted;
    }

    public void setEncyrpted(boolean encyrpted) {
        this.encyrpted = encyrpted;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected long dbid;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    protected Date lastMaintDTime;
    @ManyToOne
    protected UDC cc1 = null;
    @ManyToOne
    protected UDC cc2 = null;
    @ManyToOne
    protected UDC cc3 = null;
    @ManyToOne
    protected UDC cc4 = null;
    @ManyToOne
    protected UDC cc5 = null;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    protected OUser lastMaintUser = null;
    @Column(nullable = true, name = "deleted")
    protected boolean inActive = false;

    public String getInActiveDD() {
        return "BaseEntity_inActive";
    }

    public boolean isInActive() {
        return inActive;
    }

    public void setInActive(boolean inActive) {
        this.inActive = inActive;
    }
    @Transient
    protected boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    @Transient
    protected String className;
    protected String custom1;
    protected String custom2;
    protected String custom3;
    // <editor-fold defaultstate="collapsed" desc="Force Initialization">
    /**
     * List of entity fields that are forced to be initialized
     * <br>Initialization is mainly done
     *
     * @ loading <br>Fields that are initialized could be "IndirectList"
     */
    @Transient
    private List<String> forcedInitializedFields = new ArrayList<String>();

    protected List<String> getForcedInitializedFields() {
        return forcedInitializedFields;
    }

    /**
     * Set the field value to new() object of its type <br>Supports both list
     * and non-list fields Use
     * {@link #forceFieldInitialization(java.lang.String, java.lang.Object)}
     *
     * @param fieldName The field name, shouldn't have "."
     */
    public void forceFieldEmptyInitialization(String fieldName) {
        logger.debug("Entering");
        String newField = null;
        if (fieldName.contains(".")) {
            newField = fieldName.substring(1 + fieldName.indexOf("."));
            fieldName = fieldName.substring(0, fieldName.length() - newField.length() - 1);
        }
        Field field = BaseEntity.getClassField(this.getClass(), fieldName);
        if (field == null) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Field Not Found. Field Name: {}", fieldName);
            // </editor-fold>
        }
        Class fieldType = BaseEntity.getFieldType(field);
        if (fieldType.equals(List.class)) {
            forceFieldInitialization(fieldName, new ArrayList());
        } else {
            // Not a list
            Class fieldObjectType = BaseEntity.getFieldObjectType(field);
            try {
                Object newInst = fieldObjectType.newInstance();
                if (newInst instanceof BaseEntity) {
                    ((BaseEntity) newInst).generateInstID();
                }
                forceFieldInitialization(fieldName, newInst);
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown: {}", ex);
                logger.trace("Field Name: {}, Field Object Type: {}", fieldName, fieldObjectType);
                // </editor-fold>
            }
        }
        logger.debug("Returning");
    }

    /**
     * Set the field value to 'initializationValue' <br> Add the field to the
     * forced initialized fields list <br> Field can be a single valued or a
     * list <br> Field value is set into forcedFieldsOriginal
     *
     * @param fieldName The field name, shouldn't have "."
     */
    void forceFieldInitialization(String fieldName, Object initializationValue) {
        logger.trace("Entering");
        // ___________
        // Validations
        //
        if (fieldName.contains(".")) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.trace("Field Name Must Not Have '.' Field Name: {}", fieldName);
            // </editor-fold>
            logger.trace("Returning");
            return;
        }
        try {
            // ___________________
            // Set the field value
            //
            invokeSetter(fieldName, initializationValue);
        } catch (NoSuchMethodException ex) {
            logger.error("Exception thrown: ", ex);
            logger.trace("Field: {}", fieldName);
        }

        // ________________________________________________________________
        // Add field name to forcedInitializedFields
        //
        // Validate forcedInitializedFields is created
        if (forcedInitializedFields == null) // forcedInitializedFields is not created yet
        // Create forcedInitializedFields
        {
            forcedInitializedFields = new ArrayList<String>();
        }

        // Add the field to the list
//        forcedInitializedFields.add(fieldName);
        // ________________________________________________________________
        // Add field value to Original Values for later restore if required
        //
        // Validate forcedFieldsOriginal is created
        if (forcedFieldsOriginal == null) // forcedFieldsOriginal is not created yet
        {
            try {
                // Create forcedFieldsOriginal
                forcedFieldsOriginal = (BaseEntity) (this.getClass().newInstance());
                forcedFieldsOriginal.generateInstID();
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown: ", ex);
                logger.trace("Field Name: {}", fieldName);
                // </editor-fold>
                logger.trace("Returning");
                return;
            }
        }
        // Add field value to Original Values for later restore if required
        try {
            forcedFieldsOriginal.invokeSetter(
                    fieldName, invokeGetter(fieldName));
        } catch (NoSuchMethodException ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            logger.trace("Field Name: {}", fieldName);
            // </editor-fold>
        }
        logger.trace("Returning");
    }

    /**
     * Check whether the field initialization was forced or it not.
     *
     * @param fieldName
     * @return <br> true: field initialization is forced, its current value is
     * irrelevant to its value in database <br> false: field initialization is
     * not forced; though, its value is not guaranteed to be loaded from
     * database
     */
    public Boolean isFieldIntializationForced(String fieldName) {
        if (forcedInitializedFields == null) {
            return false;
        }
        if (forcedInitializedFields.indexOf(fieldName) == -1) {
            return false;
        }
        return true;
    }

    /**
     * Removes the field from the forced initialized fields list. <br> Its
     * original value is left AS IS, and not restored in the function
     *
     * @param fieldName The field name, shouldn't have "."
     * @return true: if field is removed successfully <br> false: if list is
     * empty or field not found
     */
    protected boolean removeForcedInitializedField(String fieldName) {
        if (forcedInitializedFields == null) {
            return false;
        }
        return forcedInitializedFields.remove(fieldName);
    }
    @Transient
    /**
     * Holds the original (loaded from DB) values of the forced initialized
     * fields in a copy of the entity. Only the forced fields are set in this
     * copy
     */
    BaseEntity forcedFieldsOriginal;

    public BaseEntity getForcedFieldsOriginal() {
        return forcedFieldsOriginal;
    }

    public void resetForcedInitilizedOriginal() {
        forcedFieldsOriginal = this;
    }

    /**
     * Initialize all the entity IndirectList direct fields to Empty Array List
     * (new ArrayList()) <br> It doesn't initalize fields sub-fields.
     */
    public void forceEmptyListsIntialization() {
        logger.debug("Entering");
        List<Field> entityAllFields = getAllFields();
        //em.clear();
        for (Field field : entityAllFields) {
            if (field.getAnnotation(OneToMany.class) != null
                    || field.getAnnotation(ManyToMany.class) != null) {
                Object list = null;
                try {
                    list = invokeGetter(field);
                } catch (NoSuchMethodException ex) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.error("Exception thrown: ", ex);
                    logger.trace("Entity: {}, Field: {}", this, field);
                    // </editor-fold>
                }
                if (list.getClass().getName().contains("IndirectList")) {
                    forceFieldInitialization(field.getName(), new ArrayList());
                }
            }
        }
        logger.debug("Returning");
    }

    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="getAllFields">
    /**
     * Get a list of ALL class fields Including fields of super classes, public,
     * private, and protected <br> Is used to get the fields Is used instead of:
     * 1- getDeclaredFields: dosn't get the super class fields 2- getAllFields:
     * gets only public fields
     */
    public List<Field> getAllFields() {
        return BaseEntityMetaData.getClassFields(this.getClass());
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="getTranslatableFields">
    /**
     * Get a list of ALL class Translatable fields Including fields of super
     * classes, public, private, and protected
     */
    public List<Field> getTranslatableFields() {
        // Generate the list
        return BaseEntityMetaData.getTranslatableFields(this);
    }
    // </editor-fold >

    public static boolean isFieldUnique(Field field) {
        return BaseEntityMetaData.isFieldUnique(field);
    }

    public List<Field> getUniqueFields() {
        // Generate the list
        return BaseEntityMetaData.getUniqueFields(this);
    }

    // <editor-fold defaultstate="collapsed" desc="getMandatoryFields">
    /**
     * Get a list of ALL class Mandatory fields (nullable = false) Including
     * fields of super classes, public, private, protected, and join columns
     */
    public List<Field> getMandatoryFields() {
        return BaseEntityMetaData.getMandatoryFields(this);
    }
    // </editor-fold >

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the dbid fields are not set
        if (!(object instanceof BaseEntity)) {
            return false;
        }
        BaseEntity baseEntity = (BaseEntity) object;

        return (this.getDbid() == baseEntity.getDbid());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (int) (this.dbid ^ (this.dbid >>> 32));
        return hash;
    }

    @Override
    public String toString() {
        String name = null;
        // <editor-fold defaultstate="collapsed" desc="Get 'name' field value if found">
        try {
            Field nameField = BaseEntity.getClassField(this.getClass(), "name");
            name = (String) invokeGetter(nameField);
        } catch (Exception ex) {
            // Do nothing, it's just a check, name is not expected to be found
            // in all entities anyway
        }
        // </editor-fold>
        String title = null;
        // <editor-fold defaultstate="collapsed" desc="Get 'title' field value if found">
        try {
            Field nameField = BaseEntity.getClassField(this.getClass(), "title");
            title = (String) invokeGetter(nameField);
        } catch (Exception ex) {
            // Do nothing, it's just a check, name is not expected to be found
            // in all entities anyway
        }
        // </editor-fold>
        return (getClass().getSimpleName()
                + "[dbid=" + getDbid()
                + ", InstanceID=" + instID
                + (name == null ? "" : ", Name=" + name)
                + (title == null ? "" : ", Title=" + title)
                + "]");
    }

    /**
     * @return the lastMaintDTime
     */
    public Date getLastMaintDTime() {
        return lastMaintDTime;
    }

    /**
     * @param lastMaintDTime the lastMaintDTime to set
     */
    public void setLastMaintDTime(Date lastMaintDTime) {
        this.lastMaintDTime = lastMaintDTime;
    }

    public long getDbid() {
        return dbid;
    }

    public void setDbid(long dbid) {
        // Entity DBID should only changed once by entity manager when saving entity
//        if (this.dbid != 0) // Following line Fills the log with pages of lines!
//        // OLog.logInformation("Manually changing a none-zero DBID!", lastMaintUser);
//        {
        this.dbid = dbid;
//        }
    }

    public void setNewDbid(long dbid) {
        this.dbid = dbid;
    }

    /**
     * @return the cc1
     */
    public UDC getCc1() {
        return cc1;
    }

    /**
     * @param cc1 the cc1 to set
     */
    public void setCc1(UDC cc1) {
        this.cc1 = cc1;
    }

    /**
     * @return the cc2
     */
    public UDC getCc2() {
        return cc2;
    }

    /**
     * @param cc2 the cc2 to set
     */
    public void setCc2(UDC cc2) {
        this.cc2 = cc2;
    }

    /**
     * @return the cc3
     */
    public UDC getCc3() {
        return cc3;
    }

    /**
     * @param cc3 the cc3 to set
     */
    public void setCc3(UDC cc3) {
        this.cc3 = cc3;
    }

    /**
     * @return the cc4
     */
    public UDC getCc4() {
        return cc4;
    }

    /**
     * @param cc4 the cc4 to set
     */
    public void setCc4(UDC cc4) {
        this.cc4 = cc4;
    }

    /**
     * @return the cc5
     */
    public UDC getCc5() {
        return cc5;
    }

    /**
     * @param cc5 the cc5 to set
     */
    public void setCc5(UDC cc5) {
        this.cc5 = cc5;
    }

    /**
     * @return the lastMaintUser
     */
    public OUser getLastMaintUser() {
        return lastMaintUser;
    }

    /**
     * @param lastMaintUser the lastMaintUser to set
     */
    public void setLastMaintUser(OUser lastMaintUser) {
        this.lastMaintUser = lastMaintUser;
    }

    public String getClassName() {
        return this.getClass().getSimpleName();
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getCustom1() {
        return custom1;
    }

    public void setCustom1(String custom1) {
        this.custom1 = custom1;
    }

    public String getCustom1DD() {
        return "BaseEntity_custom1";
    }

    public String getCustom2() {
        return custom2;
    }

    public void setCustom2(String custom2) {
        this.custom2 = custom2;
    }

    public String getCustom2DD() {
        return "BaseEntity_custom2";
    }

    public String getCustom3() {
        return custom3;
    }

    public void setCustom3(String custom3) {
        this.custom3 = custom3;
    }

    public String getCustom3DD() {
        return "BaseEntity_custom3";
    }

    /**
     * Get Entity Descriminator Value if found
     *
     * @return Descriminator Value if found Otherwise, returns ""
     */
    public String getEntityDiscrminatorValue() {
        DiscriminatorValue discrValue = (DiscriminatorValue) getClass().getAnnotation(DiscriminatorValue.class);
        if (discrValue == null) // Function could be called for any entity, not necessarily has discriminator
        // No need to log error
        {
            return "";
        } else {
            return discrValue.value();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Getter & Setter Invokes Functions">
    /**
     * Invokes the getter of the field (fieldName) which is declared in the
     * entity or one of it's super classes
     *
     * It considers the boolean field
     *
     * @param fieldExp The name of the field which is got. It supports child
     * fields, i.e. "."
     *
     * @return Field Value in case of Success <br>Null in case of field value is
     * null, error (field not found), one of the expression chain is null
     */
    public Object invokeGetter(String fieldExp)
            throws NoSuchMethodException {
        logger.debug("Entering");
        if (fieldExp == null) {
            logger.debug("Returning with Null");
            return null;
        }

        if (fieldExp.contains(".")) {
            // Expression, get the full expression values
            BaseEntity entity = this;
            while (fieldExp.indexOf(".") != -1) {
                String currentField = fieldExp.substring(0, fieldExp.indexOf("."));
                fieldExp = fieldExp.substring(fieldExp.indexOf(".") + 1);
                entity = (BaseEntity) entity.invokeGetter(currentField);
                if (entity == null) {
                    return null;
                }
            }

            //  At this point, 'fieldName' contains the the last field.
            return entity.invokeGetter(fieldExp);
        } else {
            Field field = getClassField(getClass(), fieldExp);
            if (field == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("Field Not Found In Class. Field Name: {}, Class: {}", fieldExp, getClass());
                // </editor-fold>
                logger.debug("Returning");
                return null;
            }
            String methodName;
            if (field.getType().getName().equals("boolean")) {
                methodName = "is";
            } else {
                methodName = "get";
            }

            // Set first letter to uppercase
            methodName += Character.toString(fieldExp.charAt(0)).toUpperCase()
                    + fieldExp.substring(1);

            try {
                Method method = getClass().getMethod(methodName);
                return method.invoke(this);
            } catch (Exception ex) {
                if (ex.getClass().getName().equals("java.lang.NoSuchMethodException")) {
                    throw new NoSuchMethodException(ex.getMessage());
                } else {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.error("Exception thrown : ", ex);
                    logger.trace("Field Name", fieldExp);
                    // </editor-fold>
                }
            }
            logger.debug("Returning with Null");
            return null;
        }
    }

    public Object getFieldValue(Field field)
            throws NoSuchMethodException {
        logger.debug("Entering");
        try {
            Object fieldValue = field.get(this);
            logger.debug("Returning with: {}", fieldValue);
            return fieldValue;
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            if (null != BaseEntity.class.getName()) {
                logger.trace("Class: {}", BaseEntity.class.getName());
            }
            logger.debug("Returning with Null");
            return null;
        }
    }

    /**
     * Invokes the getter of the field, of the BaseEntity
     */
    public Object invokeGetter(Field field)
            throws NoSuchMethodException {
        return invokeGetter(field.getName());
    }

    /**
     * Invokes the setter of the field (fieldName) which is declared in the
     * entity or one of it's super classes
     *
     * It doesn't call em.clear(), so, the set value will be persisted if entity
     * is persisted
     *
     * @param fieldName The name of the field which is set Is a direct field and
     * not a field expression, i.e. shouldn't contain "."
     * @param value Should be of the same type like the field's
     * @param types field setter function parameter types Usually is not needed,
     * even if the field is a list Is passed to getMethod()
     */
    public void invokeSetter(String fieldName, Object value, Class... types)
            throws NoSuchMethodException {
        logger.debug("Entering");
        if (fieldName.contains(".")) {
            System.out.println("Calling setter of "
                    + getClass().getName() + "." + fieldName);
            throw new UnsupportedOperationException(
                    "Nested Field Expression Not Supported Yet");
        }

        String methodName = "set"
                + Character.toString(fieldName.charAt(0)).toUpperCase()
                + fieldName.substring(1);

        if (types.length == 0) {
            // No types passed
            types = new Class[1];
            // Get the types if the field is list
            Field field = getClassField(getClass(), fieldName);
            types[0] = getFieldType(field);
        }

        try {
            Method method = getClass().getMethod(methodName, types);
            if (types[0].getName().toLowerCase().contains("short")) {
                method.invoke(this, new Short(value.toString()));
            } else if (types[0].getName().toLowerCase().contains("long")) {
                method.invoke(this, new Long(value.toString()));
            } else if (types[0].getName().equalsIgnoreCase("java.math.BigDecimal") && value instanceof String) {
                method.invoke(this, new BigDecimal(value.toString()));
            } else if (types[0].getName().equalsIgnoreCase("java.math.BigDecimal") && value instanceof Double) {
                method.invoke(this, new BigDecimal((Double) value));
            } else if (types[0].getName().toLowerCase().contains("int") && value instanceof String) {
                method.invoke(this, new Integer(value.toString()));
            } else if (types[0].getName().toLowerCase().contains("double") && value instanceof String) {
                method.invoke(this, new Double(value.toString()));
            } else if (types[0].getName().toLowerCase().contains("float") && value instanceof String) {
                method.invoke(this, new Float(value.toString()));
            } else if (types[0].getName().toLowerCase().contains("boolean") && value instanceof String) {
                method.invoke(this, Boolean.parseBoolean(value.toString()));
            } else if (types[0].getName().equalsIgnoreCase("java.util.Date") && value instanceof String) {
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                method.invoke(this, formatter.parse(value.toString()));
            } else if (types[0].getName().equalsIgnoreCase("java.sql.Date") && value instanceof String) {
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                java.util.Date date = formatter.parse(value.toString());
                method.invoke(this, new java.sql.Date(date.getTime()));
            } else {
                method.invoke(this, value);
            }
        } catch (Exception ex) {
            if (ex.getClass().getName().equals("java.lang.NoSuchMethodException")) {
                throw new NoSuchMethodException(ex.getMessage());
            } else {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown: ", ex);
                logger.trace("Field Name: {}", fieldName);
                // </editor-fold>
            }
        }
        logger.debug("Returning");
    }

    /**
     * Invokes the setter of the field (fieldName) which is declared in the
     * entity or one of it's super classes. Uses invokeSetter(String fieldName,
     * Object value, Class ... types)
     */
    public void invokeSetter(Field field, Object value) throws NoSuchMethodException {
        invokeSetter(field.getName(), value);
    }
    // </editor-fold>

    /**
     * Initialize the BaseEntity fields in fieldExpressions using
     * Class.newInstance()
     *
     * <br> Initialized Fields Conditions: <br> It only initializes fields that
     * are (all of the following): <br> 1. Parent (i.e. have post "." in the
     * expression) <br> 2. Not already initialized <br> <br> It adds the
     * initialized fields to forcedInitializedFields list <br> <br> - Is mainly
     * called for entity created using Class.newInstance(), and required to
     * initialize specific fields for data binding with UI. This is to avoid
     * binding to (e.g. null.DBID) for new instances <br> - Assuming all the
     * fields objects are of type BaseEntity or Primitive <br> - Take care when
     * using this function with a persisted entity, as if the field has values
     * in database but not loaded (due to lazy load), this function will create
     * it empty, and when saving the entity then the field will be reset in the
     * database <br> - It doesn't initialize IndirectList that do not follow the
     * "Initialized Fields Conditions"
     *
     * @param fieldExpressions List of fields to be initialized, should be of
     * the current entity
     */
    public boolean NewFieldsInstances(List<String> fieldExpressions) {
        logger.debug("Entering");
        int fieldExpCount = fieldExpressions.size();
        int iFieldExp;
        int fieldCount;
        int iField;
        String fieldExp;
        BaseEntity newInst, fieldParent;
        boolean bAllFieldsInstantiated = true;
        ExpressionFieldInfo expFldInfo;

        //TODO: $$ consider public field that has no getters & setters
        if (this.fieldExpressions == null) {
            this.fieldExpressions = new ArrayList<String>();
        }

        if (fieldExpressions != null) {
            for (int i = 0; i < fieldExpressions.size(); i++) {
                if (!this.fieldExpressions.contains(fieldExpressions.get(i))) {
                    this.fieldExpressions.add(fieldExpressions.get(i));
                }
            }
        }

        //this.fieldExpressions = fieldExpressions ;
        for (iFieldExp = 0; iFieldExp < fieldExpCount; iFieldExp++) {
            fieldExp = fieldExpressions.get(iFieldExp);
            if (!fieldExp.contains(".")) // field is direct field on the entity, and not a related entity's
            // field
            // Do nothing, as it doesn't need intitialization
            {
                continue;
            }

            List<ExpressionFieldInfo> expFldsInfo = parseFieldExpression(fieldExp);
            fieldCount = expFldsInfo.size();
            fieldParent = this;    // first field parent is this
            for (iField = 0;
                    iField < fieldCount - 1 /* no need to instantiate last field
                     it can be null with no problem */;
                    iField++) {
                expFldInfo = expFldsInfo.get(iField);
                try {
                    Object fieldValue = fieldParent.invokeGetter(expFldInfo.field);
                    if (fieldValue != null) {
                        // field is not NULL, don't touch it
                        fieldParent = (BaseEntity) fieldValue;
                        continue;
                    }
                } catch (Exception nsmex) {
                    // No getter, ignore it, assuming it has no setter
                    continue;
                }
                try {
                    // Add to forced intialized List
                    fieldParent.forceFieldEmptyInitialization(expFldInfo.field.getName());
                    fieldParent = (BaseEntity) fieldParent.invokeGetter(expFldInfo.field);
                } catch (Exception ex) {
                    logger.error("Eception thrown: ", ex);
                    if (expFldInfo != null) {
                        logger.trace("ExpFieldInfo: {}", expFldInfo.toString());
                    } else {
                        logger.trace("expFldInfo equals Null");
                    }
                    bAllFieldsInstantiated = false;
                }
            }
        }
        logger.debug("Returning with: {}", bAllFieldsInstantiated);
        return bAllFieldsInstantiated;
    }

    /**
     * Return a Field list of the passed field expression, first element is the
     * most left field
     *
     * Calls parseFieldExpression(Class ownerClass, String fieldExpression)
     */
    public List<ExpressionFieldInfo> parseFieldExpression(String fieldExpression) {
        return BaseEntityMetaData.parseFieldExpression(getClass(), fieldExpression);
    }

    /**
     * Returns the field DD name
     *
     * @param fieldName Is a direct field and not a field expression, i.e.
     * shouldn't contain "."
     *
     * @return DD Name "" if field is not found null in case of error
     */
    public String getFieldDDName(String fieldName) {
        return BaseEntityMetaData.getFieldDDName(this, fieldName);
    }

    public void dataNativeValidation(OUser loggedUser)
            throws OEntityDataValidationException, EntityDataValidationException {
        logger.debug("Entering");
        //  CHECK IF "this" IS AN OENTITY OR NOT???
        //  SKIP VALIDATION IF NOT OENTITY???

        /*  Date From-To Validation  */
        DateFromToValidation dateFromToAnnotation
                = getClass().getAnnotation(DateFromToValidation.class);

        if (dateFromToAnnotation != null) {
            String[] from = dateFromToAnnotation.from();
            String[] to = dateFromToAnnotation.to();
            if (from.length != 0 && to.length != 0 && from.length == to.length) {
                for (int i = 0; i < from.length; i++) {
                    Field fromField = getClassField(getClass(), from[i]);
                    Field toField = getClassField(getClass(), to[i]);
                    try {
                        Date fromDate = (Date) this.invokeGetter(fromField);
                        Date toDate = (Date) this.invokeGetter(toField);

                        if (fromDate != null && toDate != null) {
                            if (fromDate.after(toDate)) {
                                throw new DateFromToException(this, from[i], to[i]);
                            }
                        }
                    } catch (NoSuchMethodException ex) {
                        // <editor-fold defaultstate="collapsed" desc="Log">
                        logger.error("Exception thrown: ", ex);
                        logger.trace("Entity: {}", this);
                        // </editor-fold>
                    }
                }
            }
        }

        List<Field> entityFields = getAllFields();
        for (Field field : entityFields) {
            FABSDataValidation dataValidationAnnotation = field.getAnnotation(FABSDataValidation.class);
            if (dataValidationAnnotation != null) {
                String fieldName = field.getName();
                try {
                    FABSDataValidationType validationType = dataValidationAnnotation.validationType();
//                    Object fieldData = getValueFromEntity(this, fieldName);
                    Object fieldData = invokeGetter(fieldName);
                    if (fieldData == null) {
                        continue;
                    }
                    int intData = 0;
                    float decimalData = 0;
                    switch (validationType) {
                        case PercentLTZero:
                            try {
                                intData = Integer.parseInt(fieldData.toString());
                            } catch (NumberFormatException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.PercentLTZero, fieldName,
                                        loggedUser, "InvalidPercentLTZero", this);
                            } catch (ClassCastException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.PercentLTZero, fieldName,
                                        loggedUser, "InvalidPercentLTZero", this);
                            }
                            if (!(intData < 0)) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.PercentLTZero, fieldName,
                                        loggedUser, "InvalidPercentLTZero", this);
                            }
                            break;
                        case Percent:
                            try {
                                decimalData = Float.parseFloat(fieldData.toString());
                            } catch (NumberFormatException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.Percent, fieldName,
                                        loggedUser, "InvalidPercent", this);
                            } catch (ClassCastException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.Percent, fieldName,
                                        loggedUser, "InvalidPercent", this);
                            }
                            break;
                        case PositivePercent:
                            try {
                                intData = Integer.parseInt(fieldData.toString());
                            } catch (NumberFormatException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.PositivePercent, fieldName,
                                        loggedUser, "InvalidPositivePercent", this);
                            } catch (ClassCastException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.PositivePercent, fieldName,
                                        loggedUser, "InvalidPositivePercent", this);
                            }
                            if (intData < 0) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.PositivePercent, fieldName,
                                        loggedUser, "InvalidPositivePercent", this);
                            }
                            break;
                        case Decimal:
                            try {
                                decimalData = Float.parseFloat(fieldData.toString());
                            } catch (NumberFormatException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.Decimal, fieldName,
                                        loggedUser, "InvalidDecimal", this);
                            } catch (ClassCastException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.Decimal, fieldName,
                                        loggedUser, "InvalidDecimal", this);
                            }
                            break;
                        case DecimalLTZero:
                            try {
                                decimalData = Float.parseFloat(fieldData.toString());
                            } catch (NumberFormatException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.DecimalLTZero, fieldName,
                                        loggedUser, "InvalidDecimalLTZero", this);
                            } catch (ClassCastException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.DecimalLTZero, fieldName,
                                        loggedUser, "InvalidDecimalLTZero", this);
                            }
                            if (decimalData > -1) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.DecimalLTZero, fieldName,
                                        loggedUser, "InvalidDecimalLTZero", this);
                            }
                            break;
                        case PositiveDecimal:
                            try {
                                decimalData = Float.parseFloat(fieldData.toString());
                            } catch (NumberFormatException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.PositiveDecimal, fieldName,
                                        loggedUser, "InvalidPositiveDecimal", this);
                            } catch (ClassCastException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.PositiveDecimal, fieldName,
                                        loggedUser, "InvalidPositiveDecimal", this);
                            }
                            if (decimalData < 0) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.PositiveDecimal, fieldName,
                                        loggedUser, "InvalidPositiveDecimal", this);
                            }
                            break;
                        case MonthNumber:
                            try {
                                intData = Integer.parseInt(fieldData.toString());
                            } catch (NumberFormatException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.MonthNumber, fieldName,
                                        loggedUser, "InvalidMonthNumber", this);
                            } catch (ClassCastException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.MonthNumber, fieldName,
                                        loggedUser, "InvalidMonthNumber", this);
                            }
                            if (intData < 1 || intData > 12) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.MonthNumber, fieldName,
                                        loggedUser, "InvalidMonthNumber", this);
                            }
                            break;
                        case WeekDayNumber:
                            try {
                                intData = Integer.parseInt(fieldData.toString());
                            } catch (NumberFormatException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.WeekDayNumber, fieldName,
                                        loggedUser, "InvalidWeekDayNumber", this);
                            } catch (ClassCastException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.WeekDayNumber, fieldName,
                                        loggedUser, "InvalidWeekDayNumber", this);
                            }
                            if (intData < 1 || intData > 7) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.WeekDayNumber, fieldName,
                                        loggedUser, "InvalidWeekDayNumber", this);
                            }
                            break;
                        case HoursPerDay:
                            try {
                                intData = Integer.parseInt(fieldData.toString());
                            } catch (NumberFormatException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.HoursPerDay, fieldName,
                                        loggedUser, "InvalidHoursPerDay", this);
                            } catch (ClassCastException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.HoursPerDay, fieldName,
                                        loggedUser, "InvalidHoursPerDay", this);
                            }
                            if (intData < 0 || intData > 23) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.HoursPerDay, fieldName,
                                        loggedUser, "InvalidHoursPerDay", this);
                            }
                            break;
                        case DaysPerMonth:
                            try {
                                intData = Integer.parseInt(fieldData.toString());
                            } catch (NumberFormatException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.DaysPerMonth, fieldName,
                                        loggedUser, "InvalidDaysPerMonth", this);
                            } catch (ClassCastException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.DaysPerMonth, fieldName,
                                        loggedUser, "InvalidDaysPerMonth", this);
                            }
                            if (intData < 0 || intData > 31) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.DaysPerMonth, fieldName,
                                        loggedUser, "InvalidDaysPerMonth", this);
                            }
                            break;
                        case Years:
                            try {
                                intData = Integer.parseInt(fieldData.toString());
                            } catch (NumberFormatException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.Years, fieldName,
                                        loggedUser, "InvalidYears", this);
                            } catch (ClassCastException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.Years, fieldName,
                                        loggedUser, "InvalidYears", this);
                            }
                            if (intData < 0 || intData > 99) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.Years, fieldName,
                                        loggedUser, "InvalidYears", this);
                            }
                            break;
                        case Number:
                            try {
                                intData = Integer.parseInt(fieldData.toString());
                            } catch (NumberFormatException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.Number, fieldName,
                                        loggedUser, "InvalidNumber", this);
                            } catch (ClassCastException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.Number, fieldName,
                                        loggedUser, "InvalidNumber", this);
                            }
                            break;
                        case PositiveNumber:
                            try {
                                intData = Integer.parseInt(fieldData.toString());
                            } catch (NumberFormatException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.PositiveNumber, fieldName,
                                        loggedUser, "InvalidPositiveNumber", this);
                            } catch (ClassCastException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.PositiveNumber, fieldName,
                                        loggedUser, "InvalidPositiveNumber", this);
                            }
                            if (intData < 0) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.PositiveNumber, fieldName,
                                        loggedUser, "InvalidPositiveNumber", this);
                            }
                            break;
                        case NumberLTZero:
                            try {
                                intData = Integer.parseInt(fieldData.toString());
                            } catch (NumberFormatException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.NumberLTZero, fieldName,
                                        loggedUser, "InvalidNumberLTZero", this);
                            } catch (ClassCastException e) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.NumberLTZero, fieldName,
                                        loggedUser, "InvalidNumberLTZero", this);
                            }
                            if (intData > -1) {
                                throw new EntityDataValidationException(
                                        FABSDataValidationType.NumberLTZero, fieldName,
                                        loggedUser, "InvalidNumberLTZero", this);
                            }
                            break;
                    }
                } catch (IllegalArgumentException ex) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.error("Exception thrown: ", ex);
                    logger.trace("Entity: {}, Field Name: {}", this, fieldName);
                    // </editor-fold>
                } catch (NoSuchMethodException ex) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.error("Exception thrown: ", ex);
                    logger.trace("Entity: {}, Field Name: {}", this, fieldName);
                    // </editor-fold>
                }
            }
        }
        logger.debug("Returning");
    }

    // <editor-fold defaultstate="collapsed" desc="Static Functions">
    /**
     * Return a list of the entity fields that are either of type objectTypeCls
     * or are lists of element type objectTypeCls
     *
     * @param objectTypeCls
     * @return
     */
    public static List<Field> getClassObjectTypeFields(Class cls, Class objectTypeCls) {
        return BaseEntityMetaData.getClassObjectTypeFields(cls, objectTypeCls);
    }

    /**
     * Returns {@link #getFieldDDName(Class, String)}
     */
    public static String getFieldDDName(
            BaseEntity entity, String fieldExpreission) {
        return BaseEntityMetaData.getFieldDDName(entity.getClass(), fieldExpreission);
    }

    public static String getFieldDDName(
            Class cls, String fieldExpreission) {
        return BaseEntityMetaData.getFieldDDName(cls, fieldExpreission);
    }

    /**
     * Return a Field list of the passed field expression, first element is the
     * most left field
     *
     * It supports nested fields (i.e. expression contains ".") It supports
     * fields of all types (including lists) It supports fields of class
     * ancestors First field in the expression should be in the cls
     *
     * @param ownerClass Class to which the first (most left) field in the
     * expression belongs
     * @param fieldExpression the field expression, which allows nested fields
     * (i.e. contains ".")
     * @return List of ExpressionFieldInfo objects <br> null : Error
     */
    public static List<ExpressionFieldInfo> parseFieldExpression(
            Class ownerClass, String fieldExpression) {
        logger.debug("Entering");
        List<ExpressionFieldInfo> fieldHierarchy = new ArrayList<ExpressionFieldInfo>();
        if (fieldExpression == null) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Field Expression is Null. Field Class: {}", ownerClass);
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        }
        if (fieldExpression.contains(".")) {
            StringTokenizer stringT = new StringTokenizer(fieldExpression, ".");
            Class fldClass = ownerClass;
            while (stringT.hasMoreTokens()) {
                ExpressionFieldInfo efi = new ExpressionFieldInfo();
                efi.fieldParsedExpression = fieldExpression;
                efi.fieldName = stringT.nextToken();
                efi.fieldClass = fldClass;
                efi.field = getClassField(efi.fieldClass, efi.fieldName);
                if (efi.field == null) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    // </editor-fold>
                    logger.debug("Returning with Null");
                    return null; // Error: no further processing can be done
                }
                Translation translation = efi.field.getAnnotation(Translation.class);
                if (translation != null) {
                    Field originalField = getClassField(efi.fieldClass, translation.originalField());
                    if (originalField == null) {
                        logger.warn("Original Field For Translated Field Is Not Found in Entity. Entity: {}, Field: {}", ownerClass, fieldExpression);
                    } else {
                        efi.originalField = originalField;
                    }
                }
                fieldHierarchy.add(efi);
                fldClass = getFieldObjectType(efi.field);
            }
        } else {
            // Field expression has no ".", it's direct field name
            ExpressionFieldInfo efi = new ExpressionFieldInfo();
            efi.fieldParsedExpression = fieldExpression;
            efi.fieldName = fieldExpression;
            efi.fieldClass = ownerClass;
            efi.field = getClassField(ownerClass, fieldExpression);
            if (efi.field == null) {
                logger.debug("Returning with Null");
                return null; // Error: no further processing can be done
            }
            Translation translation = efi.field.getAnnotation(Translation.class);
            if (translation != null) {
                Field originalField = getClassField(efi.fieldClass, translation.originalField());
                if (originalField == null) {
                    logger.warn("Original Field For Translated Field Is Not Found in Entity. Entity: {}, Field: {}", ownerClass, fieldExpression);
                } else {
                    efi.originalField = originalField;
                }
            }
            fieldHierarchy.add(efi);
        }
        if (fieldHierarchy != null) {
            logger.debug("Returning with fieldHierarchry of size : {}", fieldHierarchy.size());
        } else {
            logger.debug("fieldHierarchy equals Null");
        }
        return fieldHierarchy;
    }

    /**
     * This function returns the following: - Field type if it's not a list -
     * 'List' if the field is list
     */
    public static Class getFieldType(Field field) {
        if (field.getAnnotation(OneToMany.class) != null
                || field.getAnnotation(ManyToMany.class) != null) {
            return List.class;
        } else {
            return field.getType();
        }
    }

    /**
     * This function returns the following: - Field type if it's not a list -
     * List Element type if the field is list
     */
    public static Class getFieldObjectType(Field field) {
        return BaseEntityMetaData.getFieldObjectType(field);
    }

    /**
     * Returns the Field object corresponding to fieldName
     *
     * Search for the field in the cls and its super classes as well Return null
     * if not found at all
     *
     * @param cls the class to which the fields of fieldName belongs
     * @param fieldName is a direct field and not a field expression, i.e.
     * shouldn't contain "." otherwise it won't be found
     */
    public static Field getClassField(Class cls, String fieldName) {
        return BaseEntityMetaData.getClassField(cls, fieldName);
    }

    /**
     * Get all the fields of the class and its super classes
     *
     * If you have the entity instance, then call BaseEntity.getAllFields()
     * instead, it's the same functionality, but probably of a better
     * performance
     */
    public static List<Field> getClassFields(Class cls) {
        return BaseEntityMetaData.getClassFields(cls);
    }

    public static List<Field> getSelfFields(Class cls) {
        return BaseEntityMetaData.getSelfFields(cls);
    }

    /**
     * Return true if the field is not nullable
     *
     * It considers both the Column & JoinColumn
     *
     * @param field The field being checked
     */
    public static boolean isFieldMandatory(Field field) {
        return BaseEntityMetaData.isFieldMandatory(field);
    }

    /**
     * Get a list of Parent Entities Field Names in & of the entity of
     * entityClassPath. <br>Parents are defined using
     *
     * @ParentEntity annotation <br>No exceptions caught inside, caller should
     * take care of that
     *
     * @param entityClassPath Full class path of the child entity
     *
     * @return null: No parent found, or error List<String>: List of parents
     * found
     */
    public static List getParentEntityFieldName(String entityClassPath) {
        return BaseEntityMetaData.getParentEntityFieldName(entityClassPath);
    }

    /**
     * Get list of Parent Entity Class Paths in & of the entity of
     * entityClassPath. <br> Parent is defined using
     *
     * @ParentEntity annotation
     *
     * @param entityClassPath Full class path of the child entity
     *
     * @return null: No parent found <br>Else the parent found
     */
    public static List getParentClassPath(String entityClassPath) {
        return BaseEntityMetaData.getParentClassPath(entityClassPath);
    }

    /**
     * Returns list of parent class names, based on call to
     * {@link BaseEntity#getParentClassPath(String) }
     */
    public static List getParentClassName(String entityClassPath) {
        return BaseEntityMetaData.getParentClassName(entityClassPath);
    }

    /**
     * Get a list of Child Entities Fields Class Paths in & of the entity of
     * entityClassPath. <br> Children are defined using
     *
     * @ChildEntity annotation <br> If child entity field is a list, then the
     * field element type class path is got
     * @param entityClassPath
     * @return
     */
    public static List getChildEntityClassPath(String entityClassPath) {
        return BaseEntityMetaData.getChildEntityClassPath(entityClassPath);
    }

    /**
     * Get a list of Child Entities Fields Names in & of the entity of
     * entityClassPath. <br> Children are defined using
     *
     * @ChildEntity annotation <br> Super class children are included
     * @param entityClassPath
     * @return <br> List of all children fields names if successful <br> Empty
     * List: if not children found <br> Null: Error
     */
    public static List<String> getChildEntityFieldName(String entityClassPath) {
        return BaseEntityMetaData.getChildEntityFieldName(entityClassPath);
    }

    public static Field getChildFieldInParentEntity(
            BaseEntity childEntity, Field parentFieldInChildEntity, BaseEntity parentEntity) {
        return BaseEntityMetaData.getChildFieldInParentEntity(childEntity, parentFieldInChildEntity, parentEntity);
    }

    public static List getRelatedEntities(Class entityClass) {
        return BaseEntityMetaData.getRelatedEntities(entityClass);
    }

    /**
     * Return a List<Field> of OneToOne & ManyToOne Related Fields
     */
    public static List getXToOneRelatedFields(Class entityClass) {
        return BaseEntityMetaData.getXToOneRelatedFields(entityClass);
    }

    /**
     * Return a List<String> of OneToMany & ManyToMany Related Fields Names
     */
    public static List getXToManyRelatedEntities(Class entityClass) {
        return BaseEntityMetaData.getXToManyRelatedEntities(entityClass);
    }

    public static String getMeasuarableFieldUnit(String fieldExpression, Class entityClass) {
        return BaseEntityMetaData.getMeasuarableFieldUnit(fieldExpression, entityClass);
    }

    public static Boolean isFieldOneToOneChild(Class entityClass, String fieldName) {
        return BaseEntityMetaData.isFieldOneToOneChild(entityClass, fieldName);
    }

    /**
     * Returns list of the unique fields in the class <br> It doesn't return the
     * unique constraint fields
     *
     * @param entityClass
     * @return Empty List if no unqiue fields found or Error, otherwise, filled
     * list
     */
    public static List getUniqueFields(Class entityClass) {
        return BaseEntityMetaData.getUniqueFields(entityClass);
    }

    /**
     * Returns list of the fields names of the first found unique constraint
     *
     * @param entityClass
     * @return Empty List if no unqiue fields found or in case in error,
     * otherwise, filled list
     */
    public static List getUniqueConstraintFieldNames(Class entityClass) {
        return BaseEntityMetaData.getUniqueConstraintFieldNames(entityClass);
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="oEntity">
    /**
     * OEntity object of the entity <br>It's Transient field, and not
     * necessarily loaded, i.e. may be null for not loading the object
     */
    @Transient
    OEntity oEntity;

    /**
     * {@link #oEntity} getter
     *
     * @return
     */
    public OEntity getOEntity() {
        return oEntity;
    }

    /**
     * Package function to set {@link #oEntity} field
     *
     * @param oEntity oEntity of the entity
     */
    void setOEntity(OEntity oEntity) {
        this.oEntity = oEntity;
    }
    // </editor-fold>

    public static Object getValueFromEntity(Object entity, String expression)
            throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if (entity == null) {
            return entity;
        }
        String currentExpression = null;
        boolean leaf;
        if (expression.contains(".")) {
            currentExpression = expression.substring(0, expression.indexOf("."));
            expression = expression.substring(expression.indexOf(".") + 1);
            leaf = false;
        } else {
            currentExpression = expression;
            leaf = true;
        }

        String methodName;

        Field currentField = BaseEntity.getClassField(entity.getClass(), currentExpression);//entity.getClass().getDeclaredField(currentExpression);

        if (currentField != null) {
            if (currentField.getType() == boolean.class) {
                methodName = "is" + Character.toString(currentExpression.charAt(0)).toUpperCase() + currentExpression.substring(1);
            } else {
                methodName = "get" + Character.toString(currentExpression.charAt(0)).toUpperCase() + currentExpression.substring(1);
            }
        } else {
            methodName = "get" + Character.toString(currentExpression.charAt(0)).toUpperCase() + currentExpression.substring(1);
        }
//        if(methodName.equals("getValueTranslated")){
//            methodName="getValue";
//        }
        if (currentField.getType() == List.class) {
            List objects = (List) entity.getClass().getMethod(methodName).invoke(entity);
            if (leaf) {
                return objects;
            } else {
                List resultObjects = new ArrayList();
                for (Object object : objects) {
                    Object o = getValueFromEntity(object, expression);
                    if (o != null) {
                        resultObjects.add(o);
                    }
                }
                return resultObjects;
            }
        } else {
            Object obj = entity.getClass().getMethod(methodName).invoke(entity);
            if (leaf) {
                return obj;
            } else {
                return getValueFromEntity(obj, expression);
            }
        }

    }

    public static Object getValueFromEntity(Object entity, String expression, boolean recreate)
            throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Object obj = getValueFromEntity(entity, expression);
        if (obj == null && recreate) {
            ((BaseEntity) entity).forceFieldEmptyInitialization(expression);
            obj = getValueFromEntity(entity, expression);
        }
        return obj;
    }

    static public void setValueInEntity(Object entity, String expression, Object value) {
        logger.debug("Entering");
        String lookupObjExpression = expression;
        if (lookupObjExpression.contains(".")) {
            lookupObjExpression = lookupObjExpression.substring(0, lookupObjExpression.lastIndexOf("."));
        }
        if (lookupObjExpression.contains(".")) {
            String exp = lookupObjExpression.substring(0, lookupObjExpression.indexOf("."));
            lookupObjExpression = lookupObjExpression.substring(lookupObjExpression.lastIndexOf(".") + 1, lookupObjExpression.length());
            Object entityTMP = null;
            try {
                entityTMP = getValueFromEntity(entity, exp);
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown: ", ex);
                // </editor-fold>
            }
            // Set the First Object in the relation
            // If the expressionis something like that jobapplicant.personalInfo.name, and we need to set JobApplicant
            if (entityTMP != null && entityTMP.getClass().getName().equals(value.getClass().getName())) {
                lookupObjExpression = exp;
            } else if (entityTMP != null) {
                entity = entityTMP;
            }
            /**
             * The following code is commented because when the expression
             * contains more than one dot, it was generating error, because
             * after parsing the expression it was set the value in a wrong
             * entity. Example: set value in entity(EdsEmployee, new Employee)
             * for field expression (edsEmployee.employee.personalInfo.name) was
             * leading to set employee in entity employee which is not correct,
             * so commenting the code will set value correctly.
             */
//            if(((BaseEntity)entity).getDbid() == 0){
//                lookupObjExpression = exp;
//            }
        }
        //String methodName = "set" + Character.toString(lookupObjExpression.charAt(0)).toUpperCase() + lookupObjExpression.substring(1);
        try {
            // use value.getClass instead of field.getType cause the field my be declared in the superclass.
            ((BaseEntity) entity).invokeSetter(lookupObjExpression, value);
            //entity.getClass().getDeclaredMethod(methodName,value.getClass()).invoke(entity, value);
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
        }
        logger.debug("Returning");
    }
    @Transient
    List<String> fieldExpressions;

    public List<String> getFieldExpressions() {
        return fieldExpressions;
    }

    /**
     * Set entities chain (parent, children) in the fieldExp to changed,
     * starting from "this" entity.
     *
     * @param fieldExp Can contain ".", and must be one of the entity fields
     *
     * @return true: successfully changed <br> false: error
     *
     * @throws FABSException In case of internal exception
     */
    public Boolean setChanged(String fieldExp) throws FABSException {
        logger.debug("Entering");
        ArrayList<BaseEntity> fieldAffectedEntityList = new ArrayList<BaseEntity>();
        try {
            if (fieldExp == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("Null Expression Passed. this: {}", this);
                // </editor-fold>
                logger.debug("Entering with false");
                return false;
            }
            fieldAffectedEntityList.add(this);

            // Get children entities if could & found
            List<ExpressionFieldInfo> fieldExpInfos = parseFieldExpression(getClass(), fieldExp);
            BaseEntity parentEntity = this;
            for (ExpressionFieldInfo expressionFldInfo : fieldExpInfos) {
                Object fieldObj = parentEntity.invokeGetter(expressionFldInfo.field);
                if (fieldObj == null) // Field value is null or child is null
                {
                    break;
                }
                if (!(fieldObj instanceof BaseEntity)) // Field is not entity, no need to continue
                {
                    break;
                }

                BaseEntity childEntity = (BaseEntity) fieldObj;
                fieldAffectedEntityList.add(childEntity);
                parentEntity = childEntity;
            }

            // Set all chain to changed
            for (BaseEntity compEntity : fieldAffectedEntityList) {
                if (!compEntity.isChanged()) {
                    compEntity.setChanged(true);
                }
            }

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            logger.trace("this: {}, Field Expression: {}", this, fieldExp);
            // </editor-fold>
            throw new FABSException(null, (FABSException) ex);
        }
        logger.debug("Returning with  true");
        return true;
    }

    /**
     * Return list of the super classes of the class cls. The list stops (and
     * doesn't include) BaseEntity, ObjectBaseEntity, & BusinessObjectBaseEntity
     */
    static public List<Class> getSuperClasses(Class cls) {
        return BaseEntityMetaData.getSuperClasses(cls);
    }

    /**
     * Return list of the super classes of the class cls. The list stops (and
     * doesn't include) BaseEntity, ObjectBaseEntity, & BusinessObjectBaseEntity
     */
    static public List<Class> getSuperEntityClasses(Class cls) {
        return BaseEntityMetaData.getSuperClasses(cls);
    }

    public List<Class> getSuperClasses() {
        return getSuperClasses(getClass());
    }

    /**
     * Return the full expression of the Version Control OModule
     *
     * <br>Example: returns "oscreen.omodule" for ScreenOutput entity
     *
     * @return Expression in case of success <br>"": In Entity has not VC
     * Expression <br>null: error
     *
     * Must Be consistent with {@link EntityBaseService#changeEntityForModuleChange(BaseEntity, OModule, OModule, String, long, OUser)
     * }
     * in multi-parent decision
     */
    public String getVCModuleExpression() {
        return BaseEntityMetaData.getVCModuleExpression(this);
    }

    /**
     * Calls {@link BaseEntity#getVCModuleExpression() }
     */
    public static String getVCModuleExpression(Class entityClass) {
        return BaseEntityMetaData.getVCModuleExpression(entityClass);
    }

    /**
     * Return the {
     *
     * @import OModule} object of the entity if it has one
     * @return OModule Object is entity has a valid one <br>null: in case of
     * entity module is null, or error
     */
    public OModule getModule() {
        logger.debug("Entering");
        try {
            String omoduleFieldExpression = getVCModuleExpression();
            if (omoduleFieldExpression == null) {
                logger.debug("Returning with Null");
                return null;
            }
            logger.debug("Returning");
            return (OModule) invokeGetter(omoduleFieldExpression);
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            logger.trace("this", this);
            // </editor-fold>
            logger.debug("Returning");
            return null;
        }
    }

    /**
     * Calls {@link #getAnnotation(java.lang.Class, java.lang.Class)} using
     * getClass()
     */
    public Annotation getAnnotation(Class annotClass) {
        return BaseEntityMetaData.getAnnotation(getClass(), annotClass);
    }

    /**
     * Returns the annotation if found, checking all super classes as well
     *
     * @param entityClass Class for which annotation is being checked
     * @param annotClass Class for which we search
     * @return Annotation class found, success <br>null: Error or no annotation
     * found
     */
    public static Annotation getAnnotation(Class entityClass, Class annotClass) {
        return BaseEntityMetaData.getAnnotation(entityClass, annotClass);
    }

    /**
     * Return indicator field value of the entity including its parent's as
     * well, referring to {@link FABSEntitySpecs#indicatorFieldExp()}. <br>It
     * checks super classes if not annotation found in the entity class. Note:
     * if FABSEntitySpecs is defined in more than one super classes, then, the
     * first one is used any way, even if the first one has no indicatorFieldExp
     * and its super class has. <br>Parent is checked for only the entity, and
     * not for super classes
     *
     * @return Indicator Field Value in case of found and success <br>Blank: No
     * Value Found, or No Indicator Key Assigned <br>null: Error
     */
    public String getIndicatorFieldValue() {
        logger.debug("Entering");
        try {
            String entityIndFldVal = null;

            // ___________________________________
            // Get Entity Indicator Value if found
            //
            FABSEntitySpecs entityFabsSpecs = (FABSEntitySpecs) getAnnotation(FABSEntitySpecs.class);
            if (entityFabsSpecs == null) {
                // FABSEntitySpecs is not defined for the class (& supers)
                logger.debug("Returning with empty string");
                return ""; // Class Has No Indicator Key Assigned
            } else {
                // FABSEntitySpecs is defined for the class
                // Check the indicatorFieldExp
                String entityIndFldExp = entityFabsSpecs.indicatorFieldExp();
                if ("".equals(entityIndFldExp)) {
                    // indicatorFieldExp is not defined for the entityFabsSpecs
                    logger.debug("Returning with empty string");
                    return ""; // Class Has No Indicator Key Assigned
                } else {
                    // indicatorFieldExp is defined for this class
                    // Get its value
                    entityIndFldVal = (String) invokeGetter(entityIndFldExp);
                }
            }

            // _________________________________________________
            // Get Parent Indicator Value If Found and Append It
            //
            ParentEntity parentEntity = getClass().getAnnotation(ParentEntity.class);
            if (parentEntity == null) {
                logger.debug("Returning with: {}", entityIndFldVal);
                return entityIndFldVal;
            }

            BaseEntity parent = (BaseEntity) invokeGetter((String) parentEntity.fields()[0]);
            if (parent == null) {
                logger.debug("Returning with: {}", entityIndFldVal);
                return entityIndFldVal;
            }

            String parentIndicatorVal = parent.getIndicatorFieldValue();
            if (parentIndicatorVal == null) {
                logger.debug("Returning with: {}", entityIndFldVal);
                return entityIndFldVal;
            }

            if ("".equals(parentIndicatorVal)) {
                logger.debug("Returning with: {}", entityIndFldVal);
                return entityIndFldVal;
            }
            logger.debug("Returning with: {}", parentIndicatorVal + "." + entityIndFldVal);
            return parentIndicatorVal + "." + entityIndFldVal;
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            logger.trace("this: {}", this);
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;   // Error
        }
    }

    /**
     * Get the value of the original field Get the value entered from the screen
     * in the mask field Check if the value of the original field is equal to
     * the value of the screen masked field after approximation of the value of
     * the original field to the number of decimal of the masked field
     *
     * if the value is the same no action will be done to the original field if
     * the value is different set the new value in the original field
     */
    public void updateDecimalValue(String originalDecimalExpression, BigDecimal maskedDecimalValue) {
        logger.debug("Entering");
        BigDecimal originalDecimalValue = null;
        try {
            // Get value of originalDecimal field
            originalDecimalValue = (BigDecimal) invokeGetter(originalDecimalExpression);
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.trace("Entity Name: {}, Original Field Name: {}, Original Field Value: {}, Masked Screen Value :{}", this, originalDecimalExpression, originalDecimalValue, maskedDecimalValue);
            // </editor-fold>
            if (maskedDecimalValue != null && !maskedDecimalValue.toString().equals("")) {
                String str = maskedDecimalValue.toString();
                int decimalDigits = 0;
                if (str.contains(".")) {
                    decimalDigits = str.substring(str.indexOf(".")).length();
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.debug("maskedDecimalValue Contain DOT");
                    // </editor-fold>
                }
                if (originalDecimalValue == null) {
                    originalDecimalValue = new BigDecimal(0.0);
                }
                if (decimalDigits > 0) {
                    decimalDigits -= 1;
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.debug("decimalDigits After DOT: {}", decimalDigits);
                    // </editor-fold>
                }
                DecimalFormat df = new DecimalFormat("#0");
                df.setMaximumFractionDigits(decimalDigits);
                df.setMinimumFractionDigits(decimalDigits);
                if (!df.format(originalDecimalValue).equals(maskedDecimalValue.toString())) {
                    invokeSetter(originalDecimalExpression, maskedDecimalValue);
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.debug("Setting Screen Value In Original Field");
                    // </editor-fold>
                }
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            logger.trace("Entity Name: {}, Original Field Name: {}, Original Field Value: {}", this, originalDecimalExpression, originalDecimalValue);
            // </editor-fold>
        }
        logger.debug("Returning");
    }

    /**
     * Returns entity table(s) name (in lower case) in the database <br>If
     * {@link Table} annotation is found in the entity, then table name is
     * returned <br>If no inheritance strategy found then entity name is
     * returned <br>If found inheritance strategy: <br>1. If it is SINGLE_TABLE,
     * then the "single" table name is returned <br>2. Else, the super entities
     * chain table names are returned, with entity table name in the first
     * element
     *
     * @see #getFieldColumnName(Field)
     */
    public List<String> getEntityTableName() {
        return BaseEntityMetaData.getEntityTableName(getClass());
    }

    public static List<String> getEntityTableName(Class cls) {
        return BaseEntityMetaData.getEntityTableName(cls);
    }

    /**
     * Returns the column name of the field in the database table, in lower
     * case. The column name is concluded from the field specifications. No
     * validation is done that the column really exists in the database.
     *
     * @param field
     * @return null: if error <br>Empty: if list, or Transient
     *
     * @see #getEntityTableName()
     */
    public static String getFieldColumnName(Field field) {
        return BaseEntityMetaData.getFieldColumnName(field);
    }

    public static Hashtable<String, List<Field>> getClassFieldsInSameTable(Class entityClass) {
        return BaseEntityMetaData.getClassFieldsInSameTable(entityClass);
    }

    public static List<String> getDiscrimnatorFieldTableData(Class entityClass) {
        return BaseEntityMetaData.getDiscrimnatorFieldTableData(entityClass);
    }

    public static FABSEntitySpecs getFabsEntitySpecs(BaseEntity entity) {
        return BaseEntityMetaData.getFabsEntitySpecs(entity.getClass());
    }

    public String getDbidDD() {
        return "BaseEntity_dbid";
    }

    public BaseEntity createNewInstance() {
        EntityCloner cloner = new EntityCloner(this);
        BaseEntity newEntity = (BaseEntity) cloner.generateClone();
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ObjectOutputStream outputStrm = new ObjectOutputStream(outputStream);
            outputStrm.writeObject(newEntity);
            ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            ObjectInputStream objInputStream = new ObjectInputStream(inputStream);
            newEntity = (BaseEntity) objInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            logger.error(e.getMessage());
            return null;
        }
        newEntity.generateInstID();
        return newEntity;
    }

}
