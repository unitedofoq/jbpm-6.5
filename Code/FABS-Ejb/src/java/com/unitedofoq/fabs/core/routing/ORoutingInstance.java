/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.routing;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author lap3
 */
@Entity
@ParentEntity(fields={"routing"})
@ChildEntity(fields={"routingTaskInstances"})
public class ORoutingInstance extends BaseEntity{
    
    // <editor-fold defaultstate="collapsed" desc="initiator">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OUser initiator;
    
    /**
     * @return the initiator
     */
    public OUser getInitiator() {
        return initiator;
    }

    /**
     * @param initiator the initiator to set
     */
    public void setInitiator(OUser initiator) {
        this.initiator = initiator;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="routing">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private ORouting routing;
    
    /**
     * @return the routing
     */
    public ORouting getRouting() {
        return routing;
    }

    /**
     * @param routing the routing to set
     */
    public void setRouting(ORouting routing) {
        this.routing = routing;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="routingTaskInstances">
    @OneToMany(mappedBy = "oroutingInst")
    private List<ORoutingTaskInstance> routingTaskInstances;

    public List<ORoutingTaskInstance> getRoutingTaskInstances() {
        return routingTaskInstances;
    }

    public void setRoutingTaskInstances(List<ORoutingTaskInstance> routingTaskInstances) {
        this.routingTaskInstances = routingTaskInstances;
    }
    // </editor-fold>
    
}
