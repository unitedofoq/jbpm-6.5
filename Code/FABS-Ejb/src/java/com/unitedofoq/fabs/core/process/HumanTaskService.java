/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.process;

import com.unitedofoq.fabs.bpm.common.model.TasksRequestData;
import com.unitedofoq.fabs.bpm.connector.BPMServiceFactory;
import com.unitedofoq.fabs.bpm.model.Task;
import com.unitedofoq.fabs.bpm.model.status.TaskMode;
import com.unitedofoq.fabs.bpm.model.status.TaskStatus;
import com.unitedofoq.fabs.bpm.services.AuditService;
import com.unitedofoq.fabs.bpm.services.DefinitionService;
import com.unitedofoq.fabs.bpm.services.TaskService;
import com.unitedofoq.fabs.core.comunication.EMailSenderLocal;
import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.delegation.DelegationServiceRemote;
import com.unitedofoq.fabs.core.delegation.TaskDelegation;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.process.jBPM.TaskInstance;
import com.unitedofoq.fabs.core.security.user.Inbox;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.OUserDTO;
import com.unitedofoq.fabs.core.security.user.Outbox;
import com.unitedofoq.fabs.core.security.user.UserServiceRemote;
import com.unitedofoq.fabs.core.setup.FABSSetup;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalPage;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author melsayed
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.process.HumanTaskServiceRemote",
        beanInterface = HumanTaskServiceRemote.class)
public class HumanTaskService implements HumanTaskServiceRemote {

    @Inject
    private BPMServiceFactory bpmServiceFactory;
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private UserServiceRemote userService;
    @EJB
    private DataTypeServiceRemote dataTypeService;
    @EJB
    private FABSSetupLocal fABSSetupLocal;
    @EJB
    private EntitySetupServiceRemote entitySetupService;
    @EJB
    private EMailSenderLocal mailSender;
    @EJB
    private TaskService taskService;
    @EJB
    private DelegationServiceRemote delegationService;
    @EJB
    private AuditService auditService;
    @EJB
    private DefinitionService definitionService;

    @PostConstruct
    public void initialize() {
//        taskService = bpmServiceFactory.getTaskServiceImpl();
//        auditService = bpmServiceFactory.getAuditServiceImpl();
//        definitionService = bpmServiceFactory.getDefinitionServiceImpl();
    }

    final static Logger logger = LoggerFactory.getLogger(HumanTaskService.class);

    @Override
    public void claimTask(Task task, OUser loggedUser) {
        taskService.claim(task.getId(), loggedUser.getLoginName(), userService.getPasswordForUserFromLDAP(loggedUser));

    }

    @Override
    public void claimAndStartTask(Task task, OUser loggedUser) {
        taskService.claim(task.getId(), loggedUser.getLoginName(), userService.getPasswordForUserFromLDAP(loggedUser));
        taskService.start(task.getId(), loggedUser.getLoginName(), userService.getPasswordForUserFromLDAP(loggedUser));
    }

    @Override
    public void startTask(Task task, OUser loggedUser) {
        taskService.start(task.getId(), loggedUser.getLoginName(), userService.getPasswordForUserFromLDAP(loggedUser));
    }

    @Override
    public void releaseAndClaimTask(Task task, OUser loggedUser) {
        if (loggedUser != null || task != null) {
            taskService.claim(task.getId(), loggedUser.getLoginName(), userService.getPasswordForUserFromLDAP(loggedUser));
        }
    }

    @Override
    public void completeTask(Task task, ODataMessage output, OUser loggedUser) {
        logger.debug("Entering");
        List<String> taskOutputVariables = definitionService.retrieveTaskOutputVariables(task);
        Map taskOutputs = buildTaskOutputs(taskOutputVariables, output, loggedUser);
        Map taskInput = task.getInputs();

        taskService.complete(task.getId(), loggedUser.getLoginName(), userService.getPasswordForUserFromLDAP(loggedUser), taskOutputs);

    }

    @Override
    public void createOutbox(Inbox inbox, ODataMessage output, OUser loggedUser) {

        Task task = null;
        Map taskInputs = null;
        Long dbid = null;

        String actorId = null;

        if (inbox != null) {
            task = inbox.getTask();
        }
        if (task != null) {
            taskInputs = task.getInputs();
        }
        if (taskInputs != null) {
            dbid = Long.valueOf((taskInputs.get("dbid").toString()));
            actorId = (String) taskInputs.get("ActorId");
        }
        Outbox outbox = new Outbox();
        if (dbid != null) {
            outbox.setEntityDBID(dbid);
        }

        outbox.setProcessName(task.getProcessId());
        outbox.setEntityName(inbox.getFunction().getEntityClassName());
        try {
            outbox.setStartDate(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(task.getCreationTime().replace("-", "/")));
        } catch (ParseException ex) {
            logger.error("Exception thrown", ex);
        }
        outbox.setEndDate(new Date());
        outbox.setTitle((String) (taskInputs.get("TaskName")));
        outbox.setActionTaken(output.getData().get(0).toString());
        if (outbox.getActionTaken() != null && !outbox.getActionTaken().trim().equals("")) {
            Calendar actioCal = Calendar.getInstance();
            Date actionDate = actioCal.getTime();
            outbox.setActionDate(actionDate);
        }
        outbox.setTaskID(Long.toString(task.getId()));
        outbox.setProcessInstanceId(Long.toString(task.getProcessInstanceId()));
        outbox.setInitiator(inbox.getInitiator());
        outbox.setInitiatorLoginName(inbox.getInitiator());
        OUser systemuUser = oem.getSystemUser(loggedUser);
        systemuUser.setWorkingTenantID(0);
        if (actorId == null || actorId.trim().equalsIgnoreCase("null")) {
            outbox.setUser(oem.getUserForLoginName(loggedUser.getLoginName()));
        } else {
            outbox.setUser(oem.getUserForLoginName(actorId));
        }
        entitySetupService.callEntityCreateAction(outbox, systemuUser);
        logger.debug("Returning with: {}");
    }

    public ODataMessage getTaskInputs(Task task, OUser loggedUser, String functionDataTypeName) {
        logger.debug("Entering");
        if (task == null || functionDataTypeName == null) {
            logger.debug("Returning with Null");
            return null;
        }
        Map<String, Object> taskInputs = taskService.getTaskInputs(task.getId());
        ODataMessage odm = new ODataMessage();

        ODataType dt = dataTypeService.loadODataType(functionDataTypeName, loggedUser);
        odm.setODataType(dt);
        if (functionDataTypeName.equalsIgnoreCase("VoidDT")) {
            odm.setData(null);

        } else if (functionDataTypeName.equalsIgnoreCase("DBID")) {
            String dbidKey = taskInputs.keySet().toArray()[0].toString();
            String dbidString = taskInputs.get("dbid").toString();
            Long dbid = Long.valueOf(dbidString.trim());
            List<Object> data = new ArrayList<Object>();
            data.add(dbid);
            odm.setData(data);
            if (odm != null) {
                logger.debug("Returing with: {}", odm.getData());
            }
            return odm;
        } else if (functionDataTypeName.equalsIgnoreCase("ParentDBID")) {
            Long dbid = Long.valueOf(taskInputs.keySet().toArray()[0].toString().trim());
            List<Object> data = new ArrayList<Object>();
            data.add(dbid);
            odm.setData(data);
            if (odm != null) {
                logger.debug("Returning with: {}", odm.getData());
            }
            return odm;
        } else if (task != null) {
            logger.warn("DataTypeInput for Task: {} is not Supported in Process", task.getName());
        }
        if (odm != null) {
            logger.debug("Returning with: {}", odm.getData());
        }
        return odm;
    }

    @EJB
    HumanTaskServiceRemote humanTaskService;

    static HashMap<Long, List<String>> processesPerGroup = new HashMap<Long, List<String>>();

    @Override
    public Map<Long, List<Inbox>> getUserInboxOnLoad(OUser loggedUser, int page,
            int pageSize, String sortedField, String sortingOrder, Map<String, String> filters) {
        logger.debug("Entering");
        List<String> roles = userService.getUserRoles(loggedUser);
        if (loggedUser == null) {
            logger.debug("Returning");
            return null;
        }

        List<Inbox> inboxList = new ArrayList<>();
        List<TaskDelegation> delegations = delegationService
                .getDelegationsFromUser(0l, loggedUser);
        delegations.addAll(delegationService.getDelegationsToUser(0l, loggedUser));
        TasksRequestData requestData = getTasksRequestData(page, pageSize,
                delegations, loggedUser, sortedField, sortingOrder, filters, roles);
        Entry<Long, List<Task>> entry = taskService.
                getTasksAssignedAsPotentialOwner(requestData).entrySet().iterator().next();
        Long totalElements = entry.getKey();
        List<Task> tasks = entry.getValue();
        List<Long> taskIDs = new ArrayList<>();
        for (Task task : tasks) {
            Inbox inbox = new Inbox();
            inbox.setProcess(true);
            String fromUser = (String) task.getInputs().get("from");
            inbox.setFromUser(fromUser);
            inbox.setTitle(formatTaskName(task));
            String mode = (String) task.getInputs().get("taskMode");
            if (mode.equals(TaskMode.APPROVE_REJECT.getValue())) {
                task.setTaskMode(TaskMode.APPROVE_REJECT);
            } else if (mode.equals(TaskMode.DISMISS_DONE.getValue())) {
                task.setTaskMode(TaskMode.DISMISS_DONE);
            }
            inbox.setTask(task);
            inbox.setDate(task.getCreationTime());
            String fromMail = oem.getUserForLoginName(fromUser).getDisplayName();
            inbox.setFromMail(fromMail);
            inbox.setId(task.getId());
            taskIDs.add(inbox.getId());
            inbox.setSubject(task.getName());
            inbox.setText(Long.toString(task.getId()));
            inbox.setState(task.getStatus().toString());

            String initiator = (String) task.getInputs().get("actorid");
            String initiatorDN = oem.getUserForLoginName(initiator).getDisplayName();
            inbox.setInitiator(initiatorDN);
            String functionCode = (String) task.getInputs().get("function");
            if (functionCode != null) {
                inbox.setFunction(getOFunction(functionCode, loggedUser, inbox.getInitiator()));
            }
            inboxList.add(inbox);
        }
        Map<Long, List<Inbox>> inboxListWithInfo = new HashMap<>();
        inboxListWithInfo.put(totalElements, inboxList);
        return inboxListWithInfo;
    }

    public List<Outbox> getUserOutbox(OUser loggedUser) {
        if (loggedUser == null) {
            return null;
        }
        // <editor-fold defaultstate="collapsed" desc="Log">
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>

        List<Outbox> outboxList = new ArrayList<Outbox>();
        return outboxList;
    }

    private OPortalPage getPortalPage(String pageName, OUser loggedUser) {
        logger.trace("Entering");
        OPortalPage page = null;
        List<String> cond = new ArrayList<String>();
        cond.add("name = '" + pageName + "'");
        try {
            page = (OPortalPage) oem.loadEntity("OPortalPage", cond, null, loggedUser);
        } catch (Exception ex) {
            logger.warn("No PortalPage for name: {}", pageName);
            logger.error("Exception thrown: ", ex);
        }
        logger.trace("Returning");
        return page;
    }

    public ODataType getDataType(String dataTypeName, OUser loggedUser) {
        logger.trace("Entering");
        ODataType odt = null;
        List<String> cond = new ArrayList<String>();
        cond.add("name = '" + dataTypeName + "'");
        try {
            odt = (ODataType) oem.loadEntity("ODataType", cond, null, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.warn("No DataType for name: {} ", dataTypeName);
        }
        if (null != odt) {
            logger.trace("Returning with : {}", odt.getName());
        }
        return odt;
    }

    static final HashMap<String, Long> taskScreenModes = new HashMap<String, Long>();

    private Long getTaskModeUDC(String udcCode, OUser loggedUser) {
        logger.trace("Entering");
        Long udcId = 0l;

        if (taskScreenModes.get(udcCode) != null && taskScreenModes.get(udcCode) != 0) {
            logger.trace("Returning");
            return taskScreenModes.get(udcCode);
        } else {
            try {
                Object udcID = oem.executeEntityQuery("Select u.dbid from UDC u where u.code = '"
                        + udcCode.toLowerCase() + "' and u.type.code = 'taskscreenmode'", loggedUser);

                if (udcID != null) {
                    udcId = Long.parseLong(udcID.toString());
                    taskScreenModes.put(udcCode, udcId);
                }
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
                logger.warn("No UDC for code {} with type code taskscreenmode", udcCode);
            }
        }
        logger.trace("Entering");
        return udcId;
    }

    static final HashMap<String, TaskFunction> taskFunctions = new HashMap<String, TaskFunction>();

    private TaskFunction getOFunction(String funcCode, OUser loggedUser, String requesterLoginname) {
        logger.trace("Entering");
        TaskFunction func = null;
        OUserDTO requesterDTO = null;
        requesterDTO = getUserDTO(requesterLoginname, loggedUser);
        if (taskFunctions.get(funcCode + "_" + requesterDTO.getTenantID()) != null) {
            logger.trace("Returning");
            return taskFunctions.get(funcCode + "_" + requesterDTO.getTenantID());
        } else {
            func = new TaskFunction();
            try {
                String query = "Select o.name, o.viewPage, u.dbid, o.STYPE , e.entityClassPath , d.name , o.header "
                        + " from ScreenFunction f , oscreen o, oentity e , odatatype d, UDC u, ofunction fun "
                        + " where f.dbid = fun.dbid and f.oscreen_dbid = o.dbid and o.oactonentity_dbid = e.dbid "
                        + " and f.screenmode_dbid = u.dbid and f.odatatype_dbid = d.dbid and fun.code = '" + funcCode + "'";
                logger.debug("Query: {}", query);
                boolean corporate = false;
                FABSSetup corporateEnabled = fABSSetupLocal.loadKeySetup("EnableCorporate", loggedUser);
                if (corporateEnabled != null
                        && (corporateEnabled.getSvalue().equalsIgnoreCase("t")
                        || corporateEnabled.getSvalue().equalsIgnoreCase("true")
                        || corporateEnabled.getSvalue().equalsIgnoreCase("on")
                        || corporateEnabled.getSvalue().equalsIgnoreCase("1"))) {
                    //Used In Inbox
                    corporate = true;
                }

                OUser systemusUser = oem.getSystemUser(loggedUser);

                if (corporate) {
                    systemusUser.setWorkingTenantID(requesterDTO.getTenantID());
                } else {
                    systemusUser.setWorkingTenantID(0);
                }

                if (corporate) {
                    systemusUser.setWorkingTenantID(requesterDTO.getTenantID());
                } else {
                    systemusUser.setWorkingTenantID(0);
                }

                Object functionValue = null;
                logger.trace("Query: {}", query);
                functionValue = oem.executeEntityNativeQuery(query, systemusUser);

                if (functionValue == null) {
                    if (taskFunctions.get(funcCode + "_" + loggedUser.getTenant().getId()) != null) {
                        logger.trace("Returning");
                        return taskFunctions.get(funcCode + "_" + loggedUser.getTenant().getId());
                    } else {
                        systemusUser.setWorkingTenantID(0);
                        functionValue = oem.executeEntityNativeQuery(query, systemusUser);
                    }
                }

                Object[] results = (Object[]) functionValue;
                String screenType = "";

                if (results[0] != null) {
                    func.setScreenName(results[0].toString());
                }
                if (results[1] != null) {
                    func.setScreenViewPage(results[1].toString());
                }
                if (results[2] != null) {
                    func.setScreenMode(Long.parseLong(results[2].toString()));
                }
                if (results[3] != null) {
                    screenType = results[3].toString();
                }
                if (results[4] != null) {
                    func.setEntityClassPath(results[4].toString());
                }
                if (results[5] != null) {
                    func.setDataTypeName(results[5].toString());
                }
                if (results[6] != null) {
                    func.setScreenHeader(results[6].toString());
                }

                func.setFunctionCode(funcCode);

                if (func.getScreenViewPage() == null || func.equals("")) {
                    func.setScreenViewPage(OScreen.getViewPage(screenType));
                }

                taskFunctions.put(funcCode + "_" + requesterDTO.getTenantID(), func);

            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
        }
        logger.trace("Returning");
        return func;
    }

    @Override
    public OFunctionResult clearProcessCache(OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            taskScreenModes.clear();
            taskFunctions.clear();
            processesPerGroup.clear();
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public List<TaskInstance> getProcessTasks(String processInstanceId, OUser loggedUser) {
        List<com.unitedofoq.fabs.bpm.model.Task> tasks = taskService.getProcessInstanceTasks(processInstanceId);
        return mapTasksToTaskInstance(tasks, loggedUser);
    }

    private List<TaskInstance> mapTasksToTaskInstance(List<com.unitedofoq.fabs.bpm.model.Task> tasks, OUser loggedUser) {
        List<TaskInstance> taskInstances = new ArrayList<TaskInstance>();
        for (com.unitedofoq.fabs.bpm.model.Task task : tasks) {
            TaskInstance taskInstance = new TaskInstance();
            taskInstance.setDelegated(task.isDelegated());
            taskInstance.setFunctionCode((task.getTaskFunction() == null) ? null : task.getTaskFunction().getFunctionCode());
            taskInstance.setId(String.valueOf(task.getId()));
            taskInstance.setProcessId(task.getProcessId());
            String taskName = "";
            if (task.getInputs() != null) {
                taskName = formatTaskName(task);
            }
            taskInstance.setName(taskName);//task.getName()
            taskInstance.setAssignee((String) task.getInputs().get("ActorId"));
            taskInstance.setCurrentStateTitle(task.getStatus().toString());
            taskInstance.setCreationDate(task.getCreationTime().toString());
            List<String> conditions = new ArrayList<String>();
            conditions.add("taskID = '" + taskInstance.getId() + "'");
            conditions.add("initiatorLoginName='" + (String) task.getInputs().get("empName") + "'");

            if (task.getActualOwnerId() != null && task.getActualOwnerId() != "") {

                taskInstance.setExecuter(task.getActualOwnerId());

            } else if (task.getInputs().get("ActorId") != null && (!((String) task.getInputs().get("ActorId")).toLowerCase().trim().equals("null"))) {

                taskInstance.setExecuter((String) task.getInputs().get("ActorId"));

            } else {

                List<String> taskPotentialOwners = getTaskPotentialOwnersOfTypeRole(task.getId());
                if (taskPotentialOwners != null) {

                    String currentExecuter = "";
                    for (String value : taskPotentialOwners) {

                        currentExecuter += value + " , ";
                    }
                    taskInstance.setExecuter(currentExecuter);
                }

            }
            Outbox o = new Outbox();
            try {
                o = (Outbox) oem.loadEntity(Outbox.class.getSimpleName(), conditions, null, loggedUser);
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            }
            if (o != null) {
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date actionDate = o.getActionDate();
                if (actionDate != null) {
                    taskInstance.setCompletionDate(formatter.format(o.getActionDate()));
                }
            }
            taskInstance.setTaskOutput((o != null) ? o.getActionTaken() : null);//approve or rejected
            taskInstance.setFromUser((String) task.getInputs().get("from"));
            if (task.getInputs().get("complTime") != null) {
                taskInstance.setCompletionDate(((Date) task.getInputs().get("complTime")).toGMTString());//task.getActivationTime()
            }
            taskInstance.setOwner(task.getActualOwnerId());

            taskInstances.add(taskInstance);
        }
        return taskInstances;
    }

    private String formatTaskName(Task task) {
        String taskName = (String) task.getInputs().get("TaskName");
        if (taskName != null) {
            String regex = "([a-z])([A-Z])";
            String replacement = "$1 $2";
            taskName = taskName.replaceAll(regex, replacement);
//            while (taskName.contains("__")) {
//                String inputName = taskName.substring(
//                        taskName.indexOf("__"), taskName.indexOf(
//                                "__", taskName.indexOf("__") + 1) + 2);
//                taskName = taskName.replace(
//                        inputName, " " + task.getInputs().get(
//                                inputName.replace(
//                                        "__", "").replace(" ", "")));
//            }
            if (task.getActualOwnerId() != null) {
                if (task.getCreatorId() != null && !task.getCreatorId().equals(task.getActualOwnerId())) {
                    OUser creator = oem.getUserForLoginName(task.getCreatorId());
                    if (creator != null) {
                        taskName = taskName + " - Delegated From "
                                + ((creator.getDisplayName() != null && !creator.getDisplayName().equals(""))
                                ? creator.getDisplayName() : creator.getLoginName());
                        task.setDelegated(true);
                    }
                }
            }
        }
        return taskName;
    }

    @Override
    public Boolean sendEMailForTask(Task task, OUser loggedUser) {
        logger.debug("Entering");
        String subject = "", body = "";
        if (null != task && null != loggedUser) {
            logger.debug("Sending HTML Email for Task {} with id = {} To User {} With Email {}", task.getName(), task.getId(), loggedUser.getLoginName(), loggedUser.getEmail());
        }
        if (loggedUser.getLoginName().equals((String) task.getInputs().get("ActorId"))) {
            if (null != task) {
                logger.debug("Sending Mail for Actual Task Owner: {} Task Name {} Task Id {} Task Owner {}", loggedUser, task.getName(), task.getId(), task.getActualOwnerId());
            }
            subject = task.getName();
        } else {
            if (null != task && null != loggedUser) {
                logger.debug("Sending Mail for A Delegate User {} Task Name {} Task Id {} Task Actual Owner {} Delegate User {}", loggedUser, task.getName(), task.getId(), task.getActualOwnerId(), loggedUser.getLoginName());
            }
            subject = "Task Delagated from "
                    + oem.getUserForLoginName((String) task.getInputs().get("ActorId")).getDisplayName() + " \"" + task.getName() + "\"";
        }
        body = generateEMailBodyForTask(task, loggedUser);
        try {
            logger.debug("Returning");
            List<String> conds = new ArrayList<String>();
            conds.add("setupKey = 'SMTP_FROM_EMAIL'");
            String mail = ((FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(), conds, null, loggedUser))
                    .getSvalue();
            return mailSender.sendHTMLEMail(mail, loggedUser.getEmail(), subject, body, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning");
            return false;
        }

    }

    private String generateEMailBodyForTask(Task task, OUser loggedUser) {
        logger.trace("Entering");
        StringBuilder Body = new StringBuilder();
        if (task != null) {
            logger.trace("Preparing HTML Message Body for Task: {} With id: {}", task.getName(), task.getId(), loggedUser);
        }
        Body.append("<html><title>").append(task.getName()).append("</title>");
        Body.append("<body><big> ").append(fABSSetupLocal.loadKeySetup("JBPMEmailBody",
                loggedUser).getSvalue().replace("%Employee%", (loggedUser != null) ? loggedUser.getDisplayName() : "Employee")).append("</big>");
        String ApplicationHTTPSURL_added = fABSSetupLocal.loadKeySetup("ApplicationHTTPSURL_added", loggedUser).getSvalue();
        if (ApplicationHTTPSURL_added.equals("true")) {
            Body.append("<br><a href=").append(fABSSetupLocal.loadKeySetup("ApplicationHTTPSURL",
                    loggedUser).getSvalue()).append(" >Click on this link to process this request</a> </body></html>");
        }
        logger.trace("Email Body: {}", Body.toString());
        logger.debug("Returning with Body: {}", Body.toString());
        return Body.toString();
    }

    @Override
    public Task getTaskForCurrentNodeInstance(long processInstanceId, long nodeInstanceId) {
        Task task = null;
        long taskId = auditService.getTaskId(processInstanceId, nodeInstanceId);
        if (taskId != -1) {
            task = taskService.getTaskById(taskId);
        }
        return task;
    }

    /**
     * ****************************************************************************
     * Method signature :- public TaskStatus getTaskStatus(long taskId)
     *
     * @param :- long taskId
     * @return :- com.unitedofoq.fabs.bpm.model.status.TaskStatus
     *
     * Description :- this method receive taskId and pass it to
     * taskService.getTaskStatusById method to get TaskStatus
     *
     * @author hahmed
     *
     * CreationDate:- 30-11-2017 2:15 pm
     *
     * LastModificationDate:- ModifiedBy:-
     *
     *
     *
     *****************************************************************************
     */
    @Override
    public TaskStatus getTaskStatus(long taskId) {
        return taskService.getTaskStatusById(taskId);
    }

    private TasksRequestData getTasksRequestData(int page, int pageSize,
            List<TaskDelegation> delegations, OUser loggedUser,
            String sortedField, String sortingOrder, Map<String, String> filters, List<String> roles) {
        TasksRequestData tasksRequestData = new TasksRequestData();
        tasksRequestData.setActorId(loggedUser.getLoginName());
        tasksRequestData.setPage(page);
        tasksRequestData.setPageSize(pageSize);
        tasksRequestData.setSortedField(sortedField);
        tasksRequestData.setSortingOrder(sortingOrder);
        tasksRequestData.setFilters(filters);
        tasksRequestData.setDelegations(delegationService.
                getDelegationDataList(delegations, loggedUser));
        tasksRequestData.setRoles(roles);
        return tasksRequestData;
    }

    private static HashMap<String, OUserDTO> users = new HashMap<String, OUserDTO>();

    private OUserDTO getUserDTO(String loginname, OUser loggegUser) {
        logger.debug("Entering");
        if (users.get(loginname) != null) {
            logger.debug("Returning");
            return users.get(loginname);
        } else {
            OUserDTO dto;
            dto = userService.getUserDTO(loginname, loggegUser);
            users.put(loginname, dto);
            logger.debug("Returning");
            return dto;
        }
    }

    private Map<String, Object> buildTaskOutputs(List<String> taskOutputVariables, ODataMessage dataInputs, OUser loggedUser) {
        Map<String, Object> taskOutputs = new HashMap<>();
        for (String taskOutputVariable : taskOutputVariables) {
            if (taskOutputVariable.startsWith("__")) {
                String fieldName = taskOutputVariable.substring(2).replace("__", ".");
                try {
                    if (dataInputs.getData().get(1) instanceof BaseEntity) {
                        Object result = ((BaseEntity) dataInputs.getData().get(1)).invokeGetter(fieldName);
                        if (result != null) {
                            taskOutputs.put(taskOutputVariable, result);
                        }
                    } else if (dataInputs.getData().get(1) instanceof Collection) {
                        Collection<BaseEntity> entities = ((Collection<BaseEntity>) dataInputs.getData().get(1));
                        if (!entities.isEmpty()) {
                            BaseEntity entity = entities.iterator().next();
                            Object result = entity.invokeGetter(fieldName);
                            taskOutputs.put(taskOutputVariable, result);
                        }
                    } else if (null != taskOutputs) {
                        logger.warn("Task Dynamic output format Error. Field name: {}", taskOutputVariable);
                    }
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                    if (dataInputs != null && dataInputs.getData() != null) {
                        logger.warn("Couldn't Get attribute {} from Entity: {}", fieldName, dataInputs.getData().getClass().getSimpleName());
                    }

                    taskOutputs.put(taskOutputVariable, null);
                }
            } else if (taskOutputVariable.equalsIgnoreCase("dbid")) {
                taskOutputs.put(taskOutputVariable, ((BaseEntity) dataInputs.getData().get(0)).getDbid() + "");
            } else if (taskOutputVariable.equals("options")) {
                taskOutputs.put(taskOutputVariable, dataInputs.getData().get(0));
            } else if (taskOutputVariable.equalsIgnoreCase("completedBy")) {
                taskOutputs.put(taskOutputVariable, loggedUser.getLoginName());
            } else if (taskOutputVariable.equalsIgnoreCase("completedByDisplayName")) {
                taskOutputs.put(taskOutputVariable, loggedUser.getDisplayName());
            } else {
                logger.trace("Task OutPut doesn't have corresponding value");
            }
        }
        return taskOutputs;
    }

    @Override
    public Task getTaskById(long id) {
        return taskService.getTaskById(id);
    }

    @Override
    public List<String> getTaskPotentialOwnersOfTypeRole(long taskId) {
        return taskService.getTaskPotentialOwnersOfTypeRole(taskId);
    }

    @Override
    public OProcess getBusinessOProcess(String processName, String packageName, OUser loggedUser) {

        try {

            String query = " SELECT e FROM OProcess e WHERE e.packageName= '" + packageName + "'  AND e.name = '" + processName + "'";
            logger.debug("Query: {}", query);

            return (OProcess) oem.executeEntityQuery(query, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);

        }

        return null;
    }
}
