/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitybase;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author melsayed
 */
@Local
public interface EntityBaseServiceRemote {

    /**
     * {@link EntityBaseService#addEntityAuditTrail(BaseEntity, EntityAuditTrail.ActionType, OUser)
     * }
     */
    public void addEntityAuditTrail(BaseEntity entity,
            EntityAuditTrail.ActionType actionType, OUser actor);

    /**
     * {@link EntityBaseService#duplicateEntity(BaseEntity, OUser, boolean) }
     */
    public OFunctionResult duplicateEntity(
            BaseEntity entity, OUser loggedUser, boolean saveToDB);

    public OFunctionResult duplicateEntityFunction(ODataMessage odm, OFunctionParms funcParms,
            OUser loggedUser);

    //FIXME: Commented from version r18.1.7 as there's no usage
//    /*
//     * Refer to {@link EntityBaseService#changeEntityForModuleChange(ODataMessage, OUser)}
//     */
//    public OFunctionResult changeEntityForModuleChange(ODataMessage odm, OUser loggedUser);
    /**
     * Refer to {@link EntityBaseService#getEntityParentsTree(BaseEntity) }
     */
    public List<String> getEntityParentsTree(String entityClassPath);

    /**
     * Refer to {@link EntityBaseService#loadEntityReferences(BaseEntity) }
     * load non child entity references
     */
    public List<BaseEntity> loadNonChildEntityReferences(BaseEntity entity, OUser loggedUser)
            throws Exception;

    /*
     * Refer to {@link EntityBaseService#xyz(OEntity, OUser)}
     */
    public OFunctionResult checkInvalidEntityFK(ODataMessage odm, OFunctionParms ofp, OUser loggedUser);

    public OFunctionResult duplicateFormScreenFunction(ODataMessage odm, OFunctionParms funcParms, OUser loggedUser);

    public OFunctionResult duplicateFormScreen(
            BaseEntity entity, OUser loggedUser, boolean saveToDB);

}
