/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitybase;

import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.udc.UDC;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 *
 * @author hamada
 */
@Entity
@VersionControlSpecs(omoduleFieldExpression = "module")
public class DBIDGeneration extends BaseEntity implements Serializable {

    @Column(nullable = false)
    private Long dbidRangeStart;
    @Column(nullable = false)
    private Long dbidRangeEnd;
    @Column(nullable = false)
    private Long newDBID;
    @Column(nullable = false, unique = true)
    private String entityName;
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false, unique = true)
    private OModule module;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private UDC scope;

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public Long getNewDBID() {
        return newDBID;
    }

    public void setNewDBID(Long newDBID) {
        this.newDBID = newDBID;
    }

    public OModule getModule() {
        return module;
    }

    public void setModule(OModule module) {
        this.module = module;
    }

    public UDC getScope() {
        return scope;
    }

    public void setScope(UDC scope) {
        this.scope = scope;
    }

    public Long getDbidRangeStart() {
        return dbidRangeStart;
    }

    public void setDbidRangeStart(Long dbidRangeStart) {
        this.dbidRangeStart = dbidRangeStart;
    }

    public Long getDbidRangeEnd() {
        return dbidRangeEnd;
    }

    public void setDbidRangeEnd(Long dbidRangeEnd) {
        this.dbidRangeEnd = dbidRangeEnd;
    }

    
    @Override
    public String toString() {
        return "com.unitedofoq.fabs.core.entitybase.DBIDGeneration[id=" + dbid + "]";
    }

    //DDS
    public String getDbidRangeEndDD() {
        return "DBIDGeneration_dbidRangeEnd";
    }

    public String getDbidRangeStartDD() {
        return "DBIDGeneration_dbidRangeStart";
    }

    public String getModuleDD() {
        return "DBIDGeneration_module";
    }

    public String getNewDBIDDD() {
        return "DBIDGeneration_newDBID";
    }
}
