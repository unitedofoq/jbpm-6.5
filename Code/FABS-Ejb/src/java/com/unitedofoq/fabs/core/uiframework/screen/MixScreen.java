/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.screen;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;

/**
 *
 * @author nkhalil
 */
@DiscriminatorValue(value="Mix")
@Entity
@ChildEntity(fields={"screenInputs", "screenOutputs","screenFilter","screenFunctions"})
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class MixScreen extends SingleEntityScreen {   
   private String loadedListType;
   private String relationField;

    public String getRelationField() {
        return relationField;
    }

    public void setRelationField(String relationField) {
        this.relationField = relationField;
    }


    public String getLoadedListType() {
        return loadedListType;
    }

    public void setLoadedListType(String loadedListType) {
        this.loadedListType = loadedListType;
    }

}
