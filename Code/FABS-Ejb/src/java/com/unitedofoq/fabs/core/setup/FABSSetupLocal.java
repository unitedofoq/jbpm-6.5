/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.setup;

import java.util.List;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitysetup.OEntityAction;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author nkhalil
 */
@Local
public interface FABSSetupLocal {
    
    public List<FABSSetup> loadGroupSetups(List<String> groupNames, OUser loggedUser);
    
    public FABSSetup loadKeySetup(String key, OUser loggedUser);
    
    public OFunctionResult saveSetup(FABSSetup fABSSetup,OEntityAction entityAction, OUser loggedUser);
    
    public String getDriverType(OUser loggedUser);
    
    public OFunctionResult onFABSSetupCachedUpdate (ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    
    public OFunctionResult preFABSSetupCachedUpdate (ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    
    public OFunctionResult onFABSSetupCachedRemove (ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    
    public OFunctionResult clearFABSSetupCache (ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public java.lang.String getKeyValue(java.lang.String key, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult saveSetup(FABSSetup fABSSetup,
            OEntityActionDTO entityAction, OUser loggedUser) ;
}
