package com.unitedofoq.fabs.core.entitybase;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.validation.EntityDataValidationException;
import com.unitedofoq.fabs.core.validation.FABSException;
import com.unitedofoq.fabs.core.validation.OEntityDataValidationException;
import javax.ejb.Local;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;

@Local
public interface OEntityManagerLocal {

    public BaseEntity saveEntityInternal(BaseEntity entity, OUser loggedUser, boolean batch)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException,
            OEntityDataValidationException, EntityDataValidationException, FABSException;
}
