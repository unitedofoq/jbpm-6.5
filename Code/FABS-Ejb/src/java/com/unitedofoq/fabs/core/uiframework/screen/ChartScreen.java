/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import java.util.Date;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.udc.UDC;

/**
 *
 * @author bassem
 */
@Entity
@DiscriminatorValue(value = "CHART")
@ChildEntity(fields={"screenInputs", "screenOutputs","screenFilter","screenFunctions", "chartScreenDim3Fields"})
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class ChartScreen extends OScreen {
    // <editor-fold defaultstate="collapsed" desc="chartScreenDim3Fields">
    @OneToMany(mappedBy = "chartScreen")
    private List<ChartScreenDim3Field> chartScreenDim3Fields;

    public List<ChartScreenDim3Field> getChartScreenDim3Fields() {
        return chartScreenDim3Fields;
    }

    public void setChartScreenDim3Fields(List<ChartScreenDim3Field> chartScreenDim3Fields) {
        this.chartScreenDim3Fields = chartScreenDim3Fields;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="height">
    private int height;

    public String getHeightDD() {
        return "ChartScreen_height";
    }
    
    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="width">
    private int width;

    public String getWidthDD() {
        return "ChartScreen_width";
    }
    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
    
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="chartType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC chartType;

    public String getChartTypeDD() {
        return "ChartScreen_chartType";
    }
    public UDC getChartType() {
        return chartType;
    }

    public void setChartType(UDC chartType) {
        this.chartType = chartType;
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="yaxisTitle">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private DD yaxisTitle;
    
    public String getYaxisTitleDD() {
        return "ChartScreen_yaxisTitle";
    }
    public DD getYaxisTitle() {
        return yaxisTitle;
    }

    public void setYaxisTitle(DD yaxisTitle) {
        this.yaxisTitle = yaxisTitle;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="xaxisTitle">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private DD xaxisTitle;

    public String getXaxisTitleDD() {
        return "ChartScreen_xaxisTitle";
    }
    public DD getXaxisTitle() {
        return xaxisTitle;
    }

    public void setXaxisTitle(DD xaxisTitle) {
        this.xaxisTitle = xaxisTitle;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="aggregateFunction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC aggregateFunction;

    public String getAggregateFunctionDD() {
        return "ChartScreen_aggregateFunction";
    }
    public UDC getAggregateFunction() {
        return aggregateFunction;
    }

    public void setAggregateFunction(UDC aggregateFunction) {
        this.aggregateFunction = aggregateFunction;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dataFilter">
    private String dataFilter;

    public String getDataFilterDD() {
        return "ChartScreen_dataFilter";
    }
    public String getDataFilter() {
        return dataFilter;
    }

    public void setDataFilter(String dataFilter) {
        this.dataFilter = dataFilter;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="yFilter">
    private String yfilter;

    public String getYfilterDD() {
        return "ChartScreen_yfilter";
    }

    public String getYfilter() {
        return yfilter;
    }

    public void setYfilter(String yfilter) {
        this.yfilter = yfilter;
    }
    
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="xFilter">
    private String xfilter;

    public String getXfilterDD() {
        return "ChartScreen_xfilter";
    }

    public String getXfilter() {
        return xfilter;
    }

    public void setXfilter(String xfilter) {
        this.xfilter = xfilter;
    }
    
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dim3Filter">
    private String dim3Filter;

    public String getDim3FilterDD() {
        return "ChartScreen_dim3Filter";
    }
    public String getDim3Filter() {
        return dim3Filter;
    }

    public void setDim3Filter(String dim3Filter) {
        this.dim3Filter = dim3Filter;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dim3OEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OEntity dim3OEntity;

    public String getDim3OEntityDD() {
        return "ChartScreen_dim3OEntity";
    }
    public OEntity getDim3OEntity() {
        return dim3OEntity;
    }

    public void setDim3OEntity(OEntity dim3OEntity) {
        this.dim3OEntity = dim3OEntity;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dim3LabelFieldExp">
    private String dim3LabelFieldExp;

    public String getDim3LabelFieldExpDD() {
        return "ChartScreen_dim3LabelFieldExp";
    }
    public String getDim3LabelFieldExp() {
        return dim3LabelFieldExp;
    }
    public void setDim3LabelFieldExp(String dim3LabelFieldExp) {
        this.dim3LabelFieldExp = dim3LabelFieldExp;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="xaxisCalcFieldExp">
    private String xaxisCalcFieldExp;

    public String getXaxisCalcFieldExpDD() {
        return "ChartScreen_xaxisCalcFieldExp";
    }
    public String getXaxisCalcFieldExp() {
        return xaxisCalcFieldExp;
    }

    public void setXaxisCalcFieldExp(String xaxisCalcFieldExp) {
        this.xaxisCalcFieldExp = xaxisCalcFieldExp;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="xaxisOEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OEntity xaxisOEntity;

    public String getXaxisOEntityDD() {
        return "ChartScreen_xaxisOEntity";
    }
    public OEntity getXaxisOEntity() {
        return xaxisOEntity;
    }

    public void setXaxisOEntity(OEntity xaxisOEntity) {
        this.xaxisOEntity = xaxisOEntity;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="xaxisLabelFieldExp">
    private String xaxisLabelFieldExp;

    public String getXaxisLabelFieldExpDD() {
        return "ChartScreen_xaxisLabelFieldExp";
    }
    public String getXaxisLabelFieldExp() {
        return xaxisLabelFieldExp;
    }

    public void setXaxisLabelFieldExp(String xaxisLabelFieldExp) {
        this.xaxisLabelFieldExp = xaxisLabelFieldExp;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="xaxisDateUnit">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC xaxisDateUnit;

    public String getXaxisDateUnitDD() {
        return "ChartScreen_xaxisDateUnit";
    }
    public UDC getXaxisDateUnit() {
        return xaxisDateUnit;
    }

    public void setXaxisDateUnit(UDC xaxisDateUnit) {
        this.xaxisDateUnit = xaxisDateUnit;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;

    public String getStartDateDD() {
        return "ChartScreen_startDate";
    }
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="endDate">
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endDate;

    public String getEndDateDD() {
        return "ChartScreen_endDate";
    }
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    // </editor-fold>
    
}
