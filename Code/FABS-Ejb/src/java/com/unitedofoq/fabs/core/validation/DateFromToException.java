package com.unitedofoq.fabs.core.validation;

import com.unitedofoq.fabs.central.EntityManagerWrapper;
import java.util.Date;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;

import org.slf4j.LoggerFactory;

public class DateFromToException extends OEntityDataValidationException {

    private String fromFieldName;
    private String toFieldName;
    private Date fromDate;
    private Date toDate;
    private BaseEntity entity;
    final static org.slf4j.Logger logger = LoggerFactory.getLogger(DateFromToException.class);

    public DateFromToException(
            BaseEntity entity,
            String fromFieldName,
            String toFieldName) {

        this.entity = entity;
        this.fromFieldName = fromFieldName;
        this.toFieldName = toFieldName;
    }

    /**
     * @return the fromFieldName
     */
    public String getFromFieldName() {
        return fromFieldName;
    }

    /**
     * @param fromFieldName the fromFieldName to set
     */
    public void setFromFieldName(String fromFieldName) {
        this.fromFieldName = fromFieldName;
    }

    /**
     * @return the toFieldName
     */
    public String getToFieldName() {
        return toFieldName;
    }

    /**
     * @param toFieldName the toFieldName to set
     */
    public void setToFieldName(String toFieldName) {
        this.toFieldName = toFieldName;
    }

    /**
     * @return the fromDate
     */
    public Date getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * @return the toDate
     */
    public Date getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the entity
     */
    public BaseEntity getEntity() {
        return entity;
    }

    /**
     * @param entity the entity to set
     */
    public void setEntity(BaseEntity entity) {
        this.entity = entity;
    }

    @Override
    public String toString() {
        return message;
    }

    @Override
    public String getMessage() {
        logger.debug("Entering: getMessage");
        message = "[" + entity.getClassName() + "|dbid=" + entity.getDbid() + "]: "
                + fromFieldName
                + ((fromDate != null) ? "(value=" + fromDate.toString() + ")" : "")
                + " cannot be greater than "
                + toFieldName
                + ((toDate != null) ? "(value=" + toDate + ")" : "")
                + ".";
        logger.debug("Returning: getMessage with value:{}", message);
        return message;
    }
}
