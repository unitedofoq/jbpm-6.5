/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.usermessage;

import java.io.Serializable;

/**
 *
 * @author bassem
 */
public class UserMessageBrief implements Serializable{

    // <editor-fold defaultstate="collapsed" desc="messagCode">
    private String messagCode ;

    public String getMessagCode() {
        return messagCode;
    }

    public void setMessagCode(String messagCode) {
        this.messagCode = messagCode;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="messageText">
    private String messageText ;

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="messageTitle">
    private String messageTitle ;

    public String getMessageTitle() {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }
    // </editor-fold>

}
