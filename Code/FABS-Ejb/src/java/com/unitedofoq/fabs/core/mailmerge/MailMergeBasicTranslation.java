/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.mailmerge;

import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

/**
 *
 * @author 3dly
 */
@Entity
public class MailMergeBasicTranslation extends BaseEntityTranslation {

    //<editor-fold defaultstate="collapsed" desc="templateName">
    private String templateName;

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateNameDD() {
        return "MailMergeBasicTranslation_templateName";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="templateDescription">
    private String templateDescription;

    public String getTemplateDescription() {
        return templateDescription;
    }

    public void setTemplateDescription(String templateDescription) {
        this.templateDescription = templateDescription;
    }

    public String getTemplateDescriptionDD() {
        return "MailMergeBasicTranslation_templateDescription";
    }
    //</editor-fold>
}
