/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.filter.OFilter;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 *
 * @author rehab
 */
@Entity
@DiscriminatorValue(value = "ENTITY")
@ParentEntity(fields = {"oentity"})
@ChildEntity(fields = {"filterFields"})
@VersionControlSpecs(versionControlType = VersionControlSpecs.VersionControlType.Parent)
public class EntityFilter extends OFilter{
    
    @OneToOne(fetch = javax.persistence.FetchType.EAGER, optional = false)
    private OEntity oentity;
    private boolean showInsideScreen;

    public OEntity getOentity() {
        return oentity;
    }
    public void setOentity(OEntity oentity) {
        this.oentity = oentity;
    }
    
    public String getOentityDD() {
        return "EntityFilter_oentity";
    }

    public boolean isShowInsideScreen() {
        return showInsideScreen;
    }
    public String getShowInsideScreenDD() {
        return "ScreenFilter_showInsideScreen";
    }
    public void setShowInsideScreen(boolean showInsideScreen) {
        this.showInsideScreen = showInsideScreen;
    }

}
