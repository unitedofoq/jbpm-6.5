/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.routing;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;

/**
 *
 * @author lap3
 */
@Entity
@ParentEntity(fields = {"routingTask"})
@VersionControlSpecs(versionControlType = VersionControlSpecs.VersionControlType.Parent)
public class ORoutingTaskAction extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="name">
    @Translatable(translationField = "nameTranslated")
    @Column(nullable = false)
    private String name;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    @Transient
    @Translation(originalField = "name")
    private String nameTranslated;

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }

    public String getNameTranslatedDD() {
        return "ORoutingTaskAction_name";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    private String description;

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    /**
     * @return the descriptionTranslated
     */
    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    /**
     * @param descriptionTranslated the descriptionTranslated to set
     */
    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getDescriptionTranslatedDD() {
        return "ORoutingTaskAction_description";
    }
    // </editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="function">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OFunction function;

    /**
     * @return the function
     */
    public OFunction getFunction() {
        return function;
    }

    /**
     * @param function the function to set
     */
    public void setFunction(OFunction function) {
        this.function = function;
    }

    /**
     * @return the function
     */
    public String getFunctionDD() {
        return "ORoutingTaskAction_function";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="nextTask">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ORoutingTask nextTask;

    /**
     * @return the nextTask
     */
    public ORoutingTask getNextTask() {
        return nextTask;
    }

    /**
     * @param nextTask the nextTask to set
     */
    public void setNextTask(ORoutingTask nextTask) {
        this.nextTask = nextTask;
    }

    /**
     * @return the nextTask
     */
    public String getNextTaskDD() {
        return "ORoutingTaskAction_nextTask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="mapFieldExpression">
    private String mapFieldExpression;

    /**
     * @return the mapFieldExpression
     */
    public String getMapFieldExpression() {
        return mapFieldExpression;
    }

    /**
     * @param mapFieldExpression the mapFieldExpression to set
     */
    public void setMapFieldExpression(String mapFieldExpression) {
        this.mapFieldExpression = mapFieldExpression;
    }

    /**
     * @return the mapFieldExpression
     */
    public String getMapFieldExpressionDD() {
        return "ORoutingTaskAction_mapFieldExpression";
    }
    // </editor-fold>  
    // <editor-fold defaultstate="collapsed" desc="udcValue">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC udcValue;

    /**
     * @return the udcValue
     */
    public UDC getUdcValue() {
        return udcValue;
    }

    /**
     * @param udcValue the udcValue to set
     */
    public void setUdcValue(UDC udcValue) {
        this.udcValue = udcValue;
    }

    /**
     * @return the udcValue
     */
    public String getUdcValueDD() {
        return "ORoutingTaskAction_udcValue";
    }
    // </editor-fold>  
    // <editor-fold defaultstate="collapsed" desc="booleanValue">
    private boolean boolValue;

    /**
     * @return the boolValue
     */
    public boolean isBoolValue() {
        return boolValue;
    }

    /**
     * @param boolValue the boolValue to set
     */
    public void setBoolValue(boolean boolValue) {
        this.boolValue = boolValue;
    }

    /**
     * @return the boolValue
     */
    public String getBoolValueDD() {
        return "ORoutingTaskAction_boolValue";
    }
    // </editor-fold>  
    // <editor-fold defaultstate="collapsed" desc="finalizeRouting">
    private boolean finalizeRouting;

    /**
     * @return the finalizeRouting
     */
    public boolean isFinalizeRouting() {
        return finalizeRouting;
    }

    /**
     * @param finalizeRouting the finalizeRouting to set
     */
    public void setFinalizeRouting(boolean finalizeRouting) {
        this.finalizeRouting = finalizeRouting;
    }

    /**
     * @return the end
     */
    public String getFinalizeRoutingDD() {
        return "ORoutingTaskAction_finalizeRouting";
    }
    // </editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="routingTask">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private ORoutingTask routingTask;

    /**
     * @return the routingTask
     */
    public ORoutingTask getRoutingTask() {
        return routingTask;
    }

    /**
     * @param routingTask the routingTask to set
     */
    public void setRoutingTask(ORoutingTask routingTask) {
        this.routingTask = routingTask;
    }

    /**
     * @return the routingTask
     */
    public String getRoutingTaskDD() {
        return "ORoutingTaskAction_routingTask";
    }
    // </editor-fold>    
}
