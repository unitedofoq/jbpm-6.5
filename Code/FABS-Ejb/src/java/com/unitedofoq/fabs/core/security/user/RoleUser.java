/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.security.user;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;

/**
 *
 * @author ahussien
 */
@Entity
@ParentEntity(fields={"orole", "ouser"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class RoleUser extends BaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="orole">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
    private ORole orole;
    public String getOroleDD(){
        return "RoleUser_orole";
    }
    /**
     * @return the orole
     */
    public ORole getOrole() {
        return orole;
    }

    /**
     * @param orole the orole to set
     */
    public void setOrole(ORole orole) {
        this.orole = orole;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="ouser">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
    private OUser ouser;
    public String getOuserDD(){
        return "RoleUser_ouser";
    }
    public OUser getOuser() {
        return ouser;
    }

    public void setOuser(OUser ouser) {
        this.ouser = ouser;
    }
    // </editor-fold>
}