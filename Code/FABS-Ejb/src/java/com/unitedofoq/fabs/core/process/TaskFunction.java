/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.process;

/**
 *
 * @author mustafa
 */
public class TaskFunction {
    
    private String screenName;
    private String screenHeader;
    private long screenMode;
    private String entityClassPath;
    private String screenViewPage;
    private String dataTypeName;
    private String FunctionCode;

    public String getScreenHeader() {
        return screenHeader;
    }

    public void setScreenHeader(String screenHeader) {
        this.screenHeader = screenHeader;
    }
    
    
    
    public String getFunctionCode() {
        return FunctionCode;
    }

    public void setFunctionCode(String FunctionCode) {
        this.FunctionCode = FunctionCode;
    }        
    
    public String getEntityClassName() {
        return getEntityClassPath().substring(
                getEntityClassPath().lastIndexOf(".") + 1,
                getEntityClassPath().length());
    }

    
    
    public String getDataTypeName() {
        return dataTypeName;
    }

    public void setDataTypeName(String dataTypeName) {
        this.dataTypeName = dataTypeName;
    }   
    
    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public long getScreenMode() {
        return screenMode;
    }

    public void setScreenMode(long screenMode) {
        this.screenMode = screenMode;
    }

    public String getEntityClassPath() {
        return entityClassPath;
    }

    public void setEntityClassPath(String entityClassPath) {
        this.entityClassPath = entityClassPath;
    }

    public String getScreenViewPage() {
        return screenViewPage;
    }

    public void setScreenViewPage(String screenViewPage) {
        this.screenViewPage = screenViewPage;
    }

}
