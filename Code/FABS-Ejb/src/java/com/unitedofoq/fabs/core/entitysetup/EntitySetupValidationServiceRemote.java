/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.entitysetup;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author arezk
 */
@Local
public interface EntitySetupValidationServiceRemote {
    /**
     * Refer to {@link EntitySetupService#validateEntityGeneral(ODataMessage, OFunctionParms, OUser) }
     */
    public OFunctionResult validateEntityGeneral(
            ODataMessage dataMessage, OFunctionParms functionParms, OUser loggedUser);
    
    /**
     * Refer to {@link EntitySetupService#validateAllEntitiesGeneral(ODataMessage, OFunctionParms, OUser) }
     */
    public OFunctionResult validateAllEntitiesGeneral(
            ODataMessage dataMessage, OFunctionParms functionParms, OUser loggedUser);
//
//    /**
//     * Refer to {@link EntitySetupService#validateEntityRecordsModuleIntegrity(ODataMessage, OFunctionParms, OUser) }
//     */
//    public OFunctionResult validateEntityRecordsModuleIntegrity(
//        ODataMessage oDM, OFunctionParms funcParms, OUser loggedUser) ;
//
//    /**
//     * Refer to {@link EntitySetupService#validateSystemEntitiesRecordsModuleIntegrity(
//     *  ODataMessage, OFunctionParms, OUser) }
//     */
//    public OFunctionResult validateSystemEntitiesRecordsModuleIntegrity(
//            ODataMessage oDM, OFunctionParms funcParms, OUser loggedUser) ;
//    public OFunctionResult validateFixedOModule(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
//    public OFunctionResult validateParentFieldExists(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
//    public OFunctionResult validateChildFieldExists(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
//    public OFunctionResult validateEntityFieldsNames(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
//    public OFunctionResult validateOEntityDT(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
//    public OFunctionResult validateEntityDDs(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
//    public OFunctionResult validateEntityTranslation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
//    public OFunctionResult validateEntityFABSEntitySpecs(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
//    public OFunctionResult validateEntityComputedFields(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
//    public OFunctionResult validateEntityMeasurableField(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
//    public OFunctionResult validateVersionControl(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
//    public OFunctionResult validateOneToOneFields(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
//    public OFunctionResult validateEntityAndTable(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
}

