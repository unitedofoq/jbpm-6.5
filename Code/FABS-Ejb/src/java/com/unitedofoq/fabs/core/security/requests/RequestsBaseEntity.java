/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.security.requests;

import com.unitedofoq.fabs.core.entitybase.*;
import javax.persistence.MappedSuperclass;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.Date;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author AbuBakr
 */
@MappedSuperclass
public class RequestsBaseEntity extends BaseEntity {

  @ManyToOne
  private UDC requestType;

  @ManyToOne
  private UDC action;

  private boolean actionTaken;

  @Temporal(javax.persistence.TemporalType.DATE)
  private Date actionDate;

  public UDC getRequestType() {
    return requestType;
  }

  public String getRequestTypeDD() {
    return "RequestsBaseEntity_requestType";
  }

  public void setRequestType(UDC requestType) {
    this.requestType = requestType;
  }

  public UDC getAction() {
    return action;
  }

  public String getActionDD() {
    return "RequestsBaseEntity_action";
  }

  public void setAction(UDC action) {
    this.action = action;
  }

  public boolean isActionTaken() {
    return actionTaken;
  }

  public String getActionTakenDD() {
    return "RequestsBaseEntity_actionTaken";
  }

  public void setActionTaken(boolean actionTaken) {
    this.actionTaken = actionTaken;
  }

  public Date getActionDate() {
    return actionDate;
  }

  public String getActionDateDD() {
    return "RequestsBaseEntity_actionDate";
  }

  public void setActionDate(Date actionDate) {
    this.actionDate = actionDate;
  }
}
