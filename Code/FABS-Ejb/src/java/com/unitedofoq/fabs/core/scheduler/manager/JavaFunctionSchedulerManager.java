/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.scheduler.manager;

import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.core.alert.entities.BaseAlert;
import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.ScheduleExpression;

import com.unitedofoq.fabs.core.scheduler.JavaFunctionScheduler;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserServiceBean;
import com.unitedofoq.fabs.core.timezone.TimeZoneServiceLocal;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.serverjob.resources.ServerJobDTO;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Hammad
 */
@Singleton
@LocalBean
@Startup
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.scheduler.manager.JavaFunctionSchedulerManagerLocal",
        beanInterface = JavaFunctionSchedulerManagerLocal.class)

public class JavaFunctionSchedulerManager implements JavaFunctionSchedulerManagerLocal {
final static org.slf4j.Logger logger = LoggerFactory.getLogger(JavaFunctionSchedulerManager.class);
    @EJB
    protected DataTypeServiceRemote dataTypeService;
    @EJB
    OEntityManagerRemote oem;
    @EJB
    private OCentralEntityManagerRemote centralOEM;
    @EJB
    EntitySetupServiceRemote entitySetupService;
    @EJB
    UserMessageServiceRemote userMessageServiceRemote;
    @EJB
    TimeZoneServiceLocal timeZoneService;
    @EJB
    UIFrameworkServiceRemote uIFrameworkService;

    JavaFunctionScheduler javaFunctionScheduler = null;

    @Override
    public OFunctionResult addSchedule(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        ScheduleExpression scheduleExpression = (ScheduleExpression) odm.getData().get(0);
        if (javaFunctionScheduler != null) {
            ServerJobDTO serverJobDTO = uIFrameworkService.getServerJobDTO(loggedUser, scheduleExpression.getDbid());
            if (serverJobDTO != null) {
                javaFunctionScheduler.addServerJob(serverJobDTO, loggedUser);
            }
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public OFunctionResult deleteSchedule(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        ScheduleExpression scheduleExpression = (ScheduleExpression) odm.getData().get(0);
        if (javaFunctionScheduler != null) {
            javaFunctionScheduler.cancelJob(scheduleExpression.getDbid());
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public OFunctionResult updateSchedule(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();

        deleteSchedule(odm, functionParms, loggedUser);
        addSchedule(odm, functionParms, loggedUser);
logger.debug("Returning");
        return oFR;
    }

    @PostConstruct
    public void init() {
        logger.debug("Entering");
        try {
            javaFunctionScheduler = new JavaFunctionScheduler(dataTypeService, oem, centralOEM, entitySetupService//, timeZoneService
                    , uIFrameworkService);
            javaFunctionScheduler.LoadServerJobs();
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        }
        logger.debug("Returning");
    }

    @PreDestroy
    public void clean() {
        if (javaFunctionScheduler != null) {
            javaFunctionScheduler.cleanup();
        }
    }
    
    @Override
    public OFunctionResult updateCustomAlert(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        BaseAlert alert = (BaseAlert) odm.getData().get(0);
        if (javaFunctionScheduler != null && alert.getJavaFunction() != null && alert.getJavaFunction().getRunningScheduleExpression() != null) {
            javaFunctionScheduler.cancelJob(alert.getJavaFunction().getRunningScheduleExpression().getDbid());
            ServerJobDTO serverJobDTO = uIFrameworkService.getServerJobDTO(loggedUser, alert.getJavaFunction().getRunningScheduleExpression().getDbid());
            if (serverJobDTO != null) {
                javaFunctionScheduler.addServerJob(serverJobDTO, loggedUser);
            }
        }
        logger.debug("Returning");
        return oFR;
    }
}
