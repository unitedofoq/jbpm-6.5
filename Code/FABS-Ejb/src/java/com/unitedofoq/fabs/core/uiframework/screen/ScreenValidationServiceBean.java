/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import static com.unitedofoq.fabs.core.entitybase.BaseEntity.getValueFromEntity;
import com.unitedofoq.fabs.core.entitybase.BaseEntityMetaData;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.setup.FABSSetup;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkValidationServiceRemote;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.lang.reflect.InvocationTargetException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author melsayed
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.uiframework.screen.ScreenValidationServiceRemote",
        beanInterface = ScreenValidationServiceRemote.class)
public class ScreenValidationServiceBean implements ScreenValidationServiceRemote {

    @EJB
    EntitySetupServiceRemote entitySetupServiceRemote;
    @EJB
    private UserMessageServiceRemote usrMsgService;
    @EJB
    OEntityManagerRemote oem;
    @EJB
    OFunctionServiceRemote functionService;
    @EJB
    UIFrameworkValidationServiceRemote uifValidationService;
    @EJB
    DDServiceRemote ddService;
    @EJB
    private FABSSetupLocal fabsSetupService;
    @EJB
    private DataTypeServiceRemote dtService;
final static Logger logger = LoggerFactory.getLogger(ScreenValidationServiceBean.class);
    @Override
    public OFunctionResult validateEntityScreens(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateEntityScreens");
        OFunctionResult oFR = new OFunctionResult();
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OEntity.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>
        try {
            oem.getEM(loggedUser);
            OEntity oEntity = (OEntity) odm.getData().get(0);
            List<SingleEntityScreen> entityScreens = (List<SingleEntityScreen>) oem.loadEntityList("SingleEntityScreen",
                    Collections.singletonList("oactOnEntity.dbid=" + oEntity.getDbid()), null, null, loggedUser);
            for (SingleEntityScreen screen : entityScreens) {
                odm.getData().remove(0);
                odm.getData().add(0, screen);
                oFR.append(validateOScreen(odm, null, loggedUser));
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning: validateEntityScreens");
            return oFR;
        }
    }

    // This Function validate a specific screen for Its Input, Output, Field Expression and Screen Function
    @Override
    public OFunctionResult validateOScreen(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Enetering: validateOScreen");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OScreen.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>
        OFunctionResult oFR = new OFunctionResult();
        OScreen screen = (OScreen) odm.getData().get(0);
        try {
            if (!(screen instanceof SingleEntityScreen)) {
                oFR.addWarning(usrMsgService.getUserMessage(
                        "ScreenNotValidatableForInput", loggedUser));
                return oFR;
            }
            oFR.append(validateOScreenInputs(odm, functionParms, loggedUser));
            oFR.append(validateOScreenOutputs(odm, functionParms, loggedUser));
            oFR.append(validateOScreenScreenFieldExpression(odm, functionParms, loggedUser));
            oFR.append(validateOScreenScreenFunctions(odm, functionParms, loggedUser));
            oFR.append(validateOscreenScreenFields(odm, functionParms, loggedUser));
            oFR.append(validateOscreenBasicDetail(odm, functionParms, loggedUser));
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        } finally {
            logger.debug("Returning: validateOScreen");
            return oFR;
        }
    }

    @Override
    public OFunctionResult validateOScreenInputs(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateOScreenInputs");
        OFunctionResult oFR = new OFunctionResult();
        OScreen screen = null;
        try {
            screen = (OScreen) odm.getData().get(0);
            List<ExpressionFieldInfo> result;
            long screenDBID = screen.getDbid();
            OEntityDTO actOnEntity = entitySetupServiceRemote.getOScreenEntityDTO(screenDBID, loggedUser);
            Class actOnEntityClass = Class.forName(actOnEntity.getEntityClassPath());
            List<ScreenInput> inputs = oem.loadEntityList("ScreenInput", Collections.singletonList(
                    "oscreen.dbid=" + screen.getDbid()), null, null, loggedUser);
            if (inputs != null && inputs.size() > 0) {
                for (int i = 0; i < inputs.size(); i++) {
                    List<ScreenDTMappingAttr> attMapping = inputs.get(i).getAttributesMapping();
                    for (int j = 0; j < attMapping.size(); j++) {
                        String currentFieldExpression = attMapping.get(j).getFieldExpression();
                        // Skip reserved Keywords
                        if (currentFieldExpression.equalsIgnoreCase("this")
                                || currentFieldExpression.equalsIgnoreCase("user")
                                || currentFieldExpression.equalsIgnoreCase("requestStatusAttr")
                                || currentFieldExpression.equalsIgnoreCase("parent")
                                || currentFieldExpression.equalsIgnoreCase("ScreenDataList")) {
                            continue;
                        }
                        result = BaseEntityMetaData.parseFieldExpression(actOnEntityClass,
                                currentFieldExpression);
                        if (result == null) {
                            logger.debug("result is Null");
                            ArrayList<String> params = new ArrayList<String>();
                            params.add(screen.getName());
                            params.add(currentFieldExpression);
                            oFR.addError(usrMsgService.getUserMessage(
                                    "InputMappingError", params, loggedUser));
                        }
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            oFR.addError(usrMsgService.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning: validateOScreenInputs");
            return oFR;
        }
    }   
       
    private OFunctionResult ValidateUniqueOutPutOnFieldExpressionFromRawData(String userMessageValidationError,ODataMessage odm,String fieldExpression,Object value,String databaseValue, OUser loggedUser) {
        FormScreen screen = (FormScreen) odm.getData().get(0);
        OFunctionResult oFR = new OFunctionResult();
       
        try{
            boolean state= validateUniqueOutPutOnFieldExpressionFromScreen(screen,fieldExpression,value,databaseValue,loggedUser);
            if(!state){
                oFR.addError(usrMsgService.getUserMessage(userMessageValidationError, loggedUser));
            }
        }
        catch(NoSuchMethodException ex){
            logger.error("Exception thrown: ",ex);
            oFR.addError(usrMsgService.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
           
        } catch (IllegalAccessException ex) {
            logger.error("Exception thrown: ",ex);
            oFR.addError(usrMsgService.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
        } catch (IllegalArgumentException ex) {
            logger.error("Exception thrown: ",ex);
            oFR.addError(usrMsgService.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
        } catch (InvocationTargetException ex) {
            logger.error("Exception thrown: ",ex);
            oFR.addError(usrMsgService.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
        } catch (UserNotAuthorizedException ex) {
            logger.error("Exception thrown: ",ex);
            oFR.addError(usrMsgService.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
        }
        
        return oFR;
    }
    
 
    private boolean validateUniqueOutPutOnFieldExpressionFromScreen(FormScreen screen,String fieldExpression,Object value,String databaseValue,OUser loggedUser) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, UserNotAuthorizedException{
        logger.debug("Enetering: ValidateUniqueOutPutOnFieldExpression");
        boolean answer = true;
        long oActOnEntityDBID = screen.getOactOnEntity().getDbid();
        // List<String> cond = new ArrayList<String>();
        if (BaseEntity.getValueFromEntity(screen,fieldExpression).equals(value)) {
                    long count = (Long) oem.executeEntityQuery(
                        "SELECT COUNT(s) FROM FormScreen s"
                        + " WHERE s.oactOnEntity.dbid = " + oActOnEntityDBID
                        + " AND s."+fieldExpression+" = "+databaseValue, loggedUser);
                    answer = (count == 0) ;
                    
        }
        logger.debug("leaving: ValidateUniqueOutPutOnFieldExpression");
        
        return answer;

    }

    @Override
    public OFunctionResult validateOscreenBasicDetail(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        return ValidateUniqueOutPutOnFieldExpressionFromRawData("UniqueBasicScreenError",odm,"basicDetail",true,"1",loggedUser);
        
        
    }
    @Override
    public OFunctionResult validateOutBoxUniqness(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        return ValidateUniqueOutPutOnFieldExpressionFromRawData("OneOutbox",odm,"outbox",true,"1",loggedUser);
    }

    private OFunctionResult validateOScreenIO(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.error("Entering: validateOScreenIO");
        OFunctionResult oFR = new OFunctionResult();
        try {
            OScreen screen = (OScreen) odm.getData().get(0);
            List<String> conditions, fieldExpressions;
            conditions = new ArrayList<String>();
            fieldExpressions = new ArrayList<String>();
            fieldExpressions.add("screenInputs.attributesMapping");
            screen = (OScreen) oem.loadEntity(OScreen.class.getName(), screen.getDbid(), conditions, fieldExpressions, loggedUser);
            if (screen != null) {
                conditions = new ArrayList<String>();
                fieldExpressions = new ArrayList<String>();
            } else {
                return oFR;
            }
            // TO-DO:
            // Validate Mapping After Completing the Mapping by mmohammed
//            screenDTMappings = (List<ScreenDTMapping>) oem.loadEntityList(
//                    ScreenDTMapping.class.getName(), conditions, fieldExpressions, null, loggedUser);
        } catch (Exception ex) {
            logger.error("Exceptin thrown: ",ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        logger.error("Returning: validateOScreenIO");
        return oFR;
    }

    @Override
    public OFunctionResult validateOScreenOutputs(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateOScreenOutputs");
        OFunctionResult oFR = new OFunctionResult();
        OScreen screen = null;
        try {
            screen = (OScreen) odm.getData().get(0);
            long screenDBID = screen.getDbid();
            OEntityDTO actOnEntity = entitySetupServiceRemote.
                    getOScreenEntityDTO(screenDBID, loggedUser);
            Class actOnClass = Class.forName(actOnEntity.getEntityClassPath());
            List<ScreenOutput> outputs = screen.getScreenOutputs();
            for (int i = 0; i < outputs.size(); i++) {
                List<ScreenDTMappingAttr> attMapping = outputs.get(i).getAttributesMapping();
                for (int j = 0; j < attMapping.size(); j++) {
                    String currentFieldExpression = attMapping.get(j).getFieldExpression();
                    // Skip reserved Keywords
                    if (currentFieldExpression.equalsIgnoreCase("this")
                            || currentFieldExpression.equalsIgnoreCase("user")
                            || currentFieldExpression.equalsIgnoreCase("requestStatusAttr")
                            || currentFieldExpression.equalsIgnoreCase("parent")
                            || currentFieldExpression.equalsIgnoreCase("ScreenDataList")) {
                        continue;
                    }
                    List<ExpressionFieldInfo> result = BaseEntityMetaData.
                            parseFieldExpression(actOnClass, currentFieldExpression);

                    if (result == null) {
                        oFR.addError(usrMsgService.getUserMessage(
                                "OutputMappingError", usrMsgService.constructListFromStrings(screen.getName(), currentFieldExpression), loggedUser));
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning: validateOScreenOutputs");
            return oFR;
        }
    }

    /**
     * This Function loops on all screens and validate them
     *
     * @param odm Not used
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult validateAllOScreens(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateAllOScreens");
        // No Input Validate required, as we get all screens of IntegrityValidDefModule module
        OFunctionResult oFR = new OFunctionResult();
        try {
            oem.getEM(loggedUser);
            String entityVCModuleExp = BaseEntity.getVCModuleExpression(OScreen.class);
            if (entityVCModuleExp == null) {
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
                return oFR;
            }
            List<String> loadConds = new ArrayList<String>();
            FABSSetup defaultModule = fabsSetupService.loadKeySetup(
                    "IntegrityValidDefModule", oem.getSystemUser(loggedUser));
            if (defaultModule != null
                    && defaultModule.getSvalue() != null && !"".equals(defaultModule.getSvalue())) {
                // Default Integrity Validation Module is set
                loadConds.add(entityVCModuleExp + ".moduleTitle='" + defaultModule.getSvalue() + "'");
            }

            List<OScreen> screens = (List<OScreen>) oem.loadEntityList("OScreen",
                    loadConds, null, null, loggedUser);
            ODataMessage oscreenDM = new ODataMessage();
            oscreenDM.setODataType(dtService.loadODataType("OScreen", loggedUser));
            oscreenDM.setData(new ArrayList<Object>());
            oscreenDM.getData().add(null);  // Empty to be filled in the loop
            oscreenDM.getData().add(null);  // Empty to be filled in the loop
            for (OScreen screen : screens) {
                oscreenDM.getData().set(0, screen);
                oscreenDM.getData().set(1, loggedUser);
                oFR.append(validateOScreen(oscreenDM, functionParms, loggedUser));
            }

        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            oFR.addError(usrMsgService.getUserMessage(
                    "SystemInternalError", loggedUser), ex);

        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning: validateAllOScreens");
            return oFR;
        }
    }

    @Override
    public OFunctionResult validateOScreenScreenFunctions(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateOScreenScreenFunctions");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OScreen.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>
        OFunctionResult oFR = new OFunctionResult();
        OScreen screen = null;
        try {
            screen = (OScreen) odm.getData().get(0);
            List<ScreenFunction> screenFunctions = new ArrayList<ScreenFunction>();
            ArrayList<String> conditions = new ArrayList<String>();
            conditions.add("oscreen.dbid = " + screen.getDbid());
            screenFunctions = (ArrayList<ScreenFunction>) oem.loadEntityList(
                    ScreenFunction.class.getSimpleName(), conditions, null, null, loggedUser);
            if (!(screenFunctions != null && screenFunctions.size() > 0)) {
                oFR.addWarning(usrMsgService.getUserMessage(
                        "ScreenFunctionWarning", Collections.singletonList(screen.getName()), loggedUser));
                return oFR;
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning: validateOScreenScreenFunctions");
            return oFR;
        }
    }

    @Override
    public OFunctionResult validateOScreenScreenFieldExpression(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateOScreenScreenFieldExpression");
        OFunctionResult oFR = new OFunctionResult();
        OScreen screen = null;
        try {
            screen = (OScreen) odm.getData().get(0);
            long screenDBID = screen.getDbid();
            OEntityDTO screenEntityDTO = entitySetupServiceRemote.getOScreenEntityDTO(screenDBID, loggedUser);
            List<ScreenField> screenFields = ((SingleEntityScreen) screen).getScreenFields();
            List<ExpressionFieldInfo> parseResult;
            int screenFieldsSize = screenFields.size();
            if (screenFieldsSize == 0) {
                oFR.addError(usrMsgService.getUserMessage(
                        "ScreenFieldsEmpty", Collections.singletonList(screen.getName()), loggedUser));
                return oFR;
            }
            String screenFieldExpression;
            for (int i = 0; i < screenFieldsSize; i++) {
                screenFieldExpression = screenFields.get(i).getFieldExpression();
                Class actOnEntityClass = null;
                try {
                    actOnEntityClass = Class.forName(screenEntityDTO.getEntityClassPath());
                } catch (ClassNotFoundException e) {
                    oFR.addError(usrMsgService.getUserMessage("EntityNotFound",
                            Collections.singletonList(screenEntityDTO.getEntityClassPath()), loggedUser));
                    return oFR;
                }
                parseResult = BaseEntity.parseFieldExpression(
                        actOnEntityClass, screenFieldExpression);
                if (parseResult == null) {
                    oFR.addError(usrMsgService.getUserMessage(
                            "ScreenFieldError", usrMsgService.constructListFromStrings(
                            screen.getName(), screenFieldExpression), loggedUser));
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            oFR.addError(usrMsgService.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
            return oFR;
        }
        logger.debug("Returning: validateOScreenScreenFieldExpression");
        return oFR;
    }

    /**
     * This Parent-PreSave function validates the ScreenFields which are
     * displayed in the screen, If the ScreenField is edited in a FormScreen, or
     * the screen was opened filtered with anything other than the parent of the
     * ScreenField, the function will get the OScreen object from the
     * ScreenField and validate each ScreenField independent of the current
     * screen object. If the odm has OScreen, it will validate its ScreenFields
     *
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult preSaveValidateOscreenScreenFields(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: preSaveValidateOscreenScreenFields");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, BaseEntity.class, List.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        SingleEntityScreen screen = null;
        try {
            // Np headerObject found, in case of opening a ScreenField in formScreen
            if (odm.getData().get(0).getClass().equals(BaseEntity.class)) {
                List<ScreenField> screenFields = (List<ScreenField>) odm.getData().get(1);
                for (ScreenField screenField : screenFields) {
                    oFR.append(validateEachScreenField(screenField, loggedUser));
                }
                return oFR;
            }

            screen = (SingleEntityScreen) odm.getData().get(0);
            long screenDBID = screen.getDbid();
            OEntityDTO actOnEntity = entitySetupServiceRemote.getOScreenEntityDTO(screenDBID, loggedUser);
            if (actOnEntity == null) {
                logger.error("Screen Has NULL actOnEntity");
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
                return oFR;
            }
            String actOnEntityClassPath = actOnEntity.getEntityClassPath();
            Class screenEntityClass = null;
            try {
                screenEntityClass = Class.forName(actOnEntityClassPath);
            } catch (ClassNotFoundException ex) {
                logger.error("Exception thrown: ",ex);
                oFR.addError(usrMsgService.getUserMessage("EntityNotFound",
                        Collections.singletonList(actOnEntityClassPath), loggedUser));
                return oFR;
            }

            List<ScreenField> screenFieldsInUI = (List<ScreenField>) odm.getData().get(1);
            oFR.append(validateScreenFieldsSequence(screen, screenFieldsInUI, loggedUser));

            // Validate Screen Fields Expression
            if (screen instanceof TabularScreen || screen instanceof FormScreen) {
                for (ScreenField screenField : screenFieldsInUI) {
                    List<ExpressionFieldInfo> infos = BaseEntity.parseFieldExpression(
                            screenEntityClass, screenField.getFieldExpression());
                    if (infos == null) {
                        oFR.addError(usrMsgService.getUserMessage("FieldExpressionNotFound", usrMsgService.
                                constructListFromStrings(
                                screenField.getFieldExpression(),
                                actOnEntity.getEntityClassName()),
                                loggedUser));
                        continue;
                    }
                    Field field = infos.get(infos.size() - 1).field;
                    if (!field.getType().isPrimitive() && field.getType() != String.class
                            && field.getType() != Date.class
                            && field.getType().getSuperclass() != Number.class
                            && field.getType() != Boolean.class
                            && !field.getType().getName().equals("[B")) {
                        oFR.addError(usrMsgService.getUserMessage("ScreenFieldPimitiveType",
                                Collections.singletonList(field.getName()), loggedUser));
                        return oFR;
                    }
                }

                // Validate lookup

                for (ScreenField screenField : screenFieldsInUI) {
                    if (!screenField.isEditable()) {
                        continue;
                    }

                    DD dd = screenField.getDd();
                    if (dd == null || dd.getDbid() == 0) {
                        logger.debug("DD equals Null or its DBID equals zero - User Didnt choose DDs");
                        // User didn't choose DD
                        // Initialize DD to default value from Entity
                        BaseEntity baseEntityTEMP = (BaseEntity) screenEntityClass.newInstance();
                        dd = ddService.getDD(BaseEntity.getFieldDDName(
                                baseEntityTEMP, screenField.getFieldExpression()), loggedUser);

                    }
                    if(screenField.isEditable() && dd==null){
                      logger.error("default dd not found ");
                      oFR.addError(usrMsgService.getUserMessage("defaultDDNotFound", loggedUser)); 
                      return oFR;
                    }
                    if (dd.getControlType().getDbid() != DD.CT_LOOKUPSCREEN
                            && dd.getControlType().getDbid() != DD.CT_LOOKUPMULTILEVEL) {
                        continue;
                    }

                    List<ExpressionFieldInfo> infos = BaseEntity.parseFieldExpression(
                            screenEntityClass, screenField.getFieldExpression());
                    Field field = infos.get(infos.size() - 1).field;
                    if (!BaseEntity.isFieldUnique(field)) {
                        oFR.addError(usrMsgService.getUserMessage("LookupControlError",
                                usrMsgService.constructListFromStrings(screen.getName(), field.getName()), loggedUser));
                        return oFR;
                    }
                }
            }

            BaseEntity entity = (BaseEntity) screenEntityClass.newInstance();
            List<Field> fields = entity.getMandatoryFields();
            for (Field field : fields) {
                if (field.getType() == boolean.class) {
                    if (entity.invokeGetter(field).equals(true)) {
                        continue;
                    }
                }
                String fieldName = field.getName();
                boolean exist = false;
                for (ScreenField screenField : screenFieldsInUI) {
                    if (screenField.getFieldExpression().startsWith(fieldName)) {
                        exist = true;
                        break;
                    }
                }
                if (!exist) {
                    oFR.addWarning(usrMsgService.getUserMessage(
                            "MissingScreenField", Collections.singletonList(fieldName), loggedUser));
                }
            }

            //Validate all or non fields in groups
            //oFR.append(validateAllOrNonFieldsInGroups(odm, functionParms, loggedUser));
            return oFR;

        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning: preSaveValidateOscreenScreenFields");
            return oFR;
        }
    }

    @Override
    public OFunctionResult validateOscreenScreenFields(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateOscreenScreenFields");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OScreen.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        SingleEntityScreen screen = null;
        try {
            // Initialize Variables
            screen = (SingleEntityScreen) odm.getData().get(0);
            long screenDBID = screen.getDbid();
            List<ScreenField> screenFieldsInUI = screen.getScreenFields();
            OEntityDTO actOnEntity = entitySetupServiceRemote.getOScreenEntityDTO(screenDBID, loggedUser);
            if (actOnEntity == null) {
                logger.error("Screen Has NULL actOnEntity");
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
                return oFR;
            }
            String actOnEntityClassPath = actOnEntity.getEntityClassPath();
            Class screenEntityClass = null;
            try {
                screenEntityClass = Class.forName(actOnEntityClassPath);
            } catch (ClassNotFoundException ex) {
                logger.error("Exception thrown:",ex);
                oFR.addError(usrMsgService.getUserMessage("EntityNotFound",
                        Collections.singletonList(actOnEntityClassPath), loggedUser));
                return oFR;
            }
            // Validatating the missing Screen Fields
            oFR.append(validateScreenFieldsSequence(screen, screenFieldsInUI, loggedUser));

            // Validate Screen Fields Expression
            if (screen instanceof TabularScreen || screen instanceof FormScreen) {
                for (ScreenField screenField : screenFieldsInUI) {
                    if (screenField.isInActive()) {
                        continue;
                    }
                    List<ExpressionFieldInfo> infos = BaseEntity.parseFieldExpression(
                            screenEntityClass, screenField.getFieldExpression());
                    if (infos == null) {
                        oFR.addError(usrMsgService.getUserMessage("FieldExpressionNotFound", usrMsgService.
                                constructListFromStrings(screenField.getFieldExpression(),
                                actOnEntity.getEntityClassName()), loggedUser));
                        return oFR;
                    }
                    Field field = infos.get(infos.size() - 1).field;
                    if (!field.getType().isPrimitive() && field.getType() != String.class
                            && field.getType() != Date.class
                            && field.getType().getSuperclass() != Number.class
                            && field.getType() != Boolean.class
                            && !field.getType().getName().equals("[B")) {
                        oFR.addError(usrMsgService.getUserMessage(
                                "ScreenFieldPimitiveType", loggedUser));
                        return oFR;
                    }
                }
                // Validate lookup

                for (ScreenField screenField : screenFieldsInUI) {
                    if (!screenField.isEditable()) {
                        continue;
                    }
                    DD dd;
                    BaseEntity baseEntityTEMP = (BaseEntity) screenEntityClass.newInstance();
                    if (screenField.getDd() == null || screenField.getDd().getDbid() == 0) {
                        dd = ddService.getDD(BaseEntity.getFieldDDName(
                                baseEntityTEMP, screenField.getFieldExpression()), loggedUser);

                    } else {
                        dd = screenField.getDd();
                    }
                    if (dd.getControlType().getDbid() != DD.CT_LOOKUPSCREEN && dd.getControlType().getDbid() != DD.CT_LOOKUPMULTILEVEL) {
                        continue;
                    }
                    List<ExpressionFieldInfo> infos = BaseEntity.parseFieldExpression(
                            screenEntityClass, screenField.getFieldExpression());
                    Field field = infos.get(infos.size() - 1).field;
                    if (!BaseEntity.isFieldUnique(field)) {
                        oFR.addError(usrMsgService.getUserMessage("LookupControlError",
                                usrMsgService.constructListFromStrings(screen.getName(), field.getName()), loggedUser));
                        return oFR;
                    }
                }
            }


            // Validatating the missing Screen Fields
            BaseEntity entity = (BaseEntity) screenEntityClass.newInstance();
            List<Field> fields = entity.getMandatoryFields();
            for (Field field : fields) {
                String fieldName = field.getName();
                boolean exist = false;
                for (ScreenField screenField : screenFieldsInUI) {
                    if (screenField.isInActive()) {
                        continue;
                    }
                    if (screenField.getFieldExpression().startsWith(fieldName)) {
                        exist = true;
                        break;
                    }
                }
                if (!exist) {
                    oFR.addWarning(usrMsgService.getUserMessage(
                            "MissingScreenField", Collections.singletonList(fieldName), loggedUser));
                }
            }
            //FIXME
            //Validate all or non fields in groups
            return oFR;

        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning: validateOscreenScreenFields");
            return oFR;
        }
    }

    @Override
    public OFunctionResult validateAllOrNonFieldsInGroups(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateAllOrNonFieldsInGroups");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OScreen.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        SingleEntityScreen screen = null;
        try {
            screen = (SingleEntityScreen) odm.getData().get(0);
            if (!(screen instanceof FormScreen)) {
                oFR.addError(usrMsgService.getUserMessage("ScreenFieldGroupType", loggedUser));
                return oFR;
            }

            List<ScreenField> screenFields = oem.loadEntityList("ScreenField",
                    Collections.singletonList("oScreen.dbid=" + screen.getDbid()),
                    null, null, loggedUser);

            int screenFieldsInGroup = 0;
            for (ScreenField screenField : screenFields) {
                if (screenField.getScreenFieldGroup() != null) {
                    screenFieldsInGroup++;
                }
            }
            if (screenFieldsInGroup == 0 || screenFieldsInGroup == screenFields.size()) {
                return oFR;
            }

            oFR.addError(usrMsgService.getUserMessage("ScreenFieldGroup", loggedUser));
            return oFR;

        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning: validateAllOrNonFieldsInGroups");
            return oFR;
        }
    }

    /**
     * Validates that the screen fields have no duplicated sequence. Checks both
     * fieldsInDB & fieldsInUI
     * <br>Deleted fields are ignored in both fieldsInDB & fieldsInUI
     *
     * @param fieldsInDB
     * @param fieldsInUI
     * @param loggedUser
     * @return Error message if found. Just messages, no data
     */
    private OFunctionResult validateScreenFieldsSequence(
            OScreen ownerScreen, List<ScreenField> fieldsInUI, OUser loggedUser) {
        logger.debug("Entering: validateScreenFieldsSequence");
        OFunctionResult oFR = new OFunctionResult();
        ArrayList<ScreenField> fields2Compare = new ArrayList<ScreenField>();
        try {
            oem.getEM(loggedUser);
            // Get fields saved in DB
            List<ScreenField> fieldsInDB = oem.loadEntityList("ScreenField",
                    Collections.singletonList("oScreen.dbid=" + ownerScreen.getDbid()),
                    null, null, loggedUser);

            // Add to fields2Compare UI Fields that are displayed active
            for (ScreenField fieldUI : fieldsInUI) {
                if (fieldUI.isInActive()) {
                    // Field is not active
                    // Ignore it
                    continue;
                }
                fields2Compare.add(fieldUI);
            }

            // Add to fields2Compare DB Fields that are displayed active
            // and not in fields2Compare
            for (ScreenField fieldDB : fieldsInDB) {
                if (fieldDB.isInActive()) {
                    // Field is not active
                    // Ignore it
                    continue;
                }
                boolean foundInUI = false;
                for (ScreenField field2Compare : fields2Compare) {
                    if (field2Compare.getDbid() == fieldDB.getDbid()) {
                        foundInUI = true;
                        break;
                    }
                }
                if (!foundInUI) {
                    fields2Compare.add(fieldDB);
                }
            }

            // Compare that UI Field Sequence is not repeated both DB & UI Fields
            for (int startCompIndex = 0; startCompIndex < fields2Compare.size(); startCompIndex++) {
                ScreenField field2Compare = fields2Compare.get(startCompIndex);
                if (field2Compare.getSortIndex() == 0) {
                    oFR.addError(usrMsgService.getUserMessage("ScreenFieldIndexZero",
                            usrMsgService.constructListFromStrings(
                            field2Compare.getOScreen().getName(),
                            field2Compare.getFieldExpression()),
                            loggedUser));

                    // No need to compare its index
                    continue;
                }
                for (int fieldIndex = startCompIndex + 1; // Compare following elements
                        fieldIndex < fields2Compare.size(); fieldIndex++) {
                    if (fields2Compare.get(fieldIndex).getSortIndex()
                            != field2Compare.getSortIndex()) // SortIndex is not repeated
                    {
                        continue;
                    }

                    // SortIndex is repeated
                    oFR.addError(usrMsgService.getUserMessage("ScreenFieldIndexRepeated",
                            usrMsgService.constructListFromStrings(
                            field2Compare.getOScreen().getName(),
                            "" + field2Compare.getSortIndex(),
                            fields2Compare.get(fieldIndex).getFieldExpression()
                            + ","
                            + field2Compare.getFieldExpression()),
                            loggedUser));

                    break;
                }
            }

        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning: validateScreenFieldsSequence");
            oem.closeEM(loggedUser);
            return oFR;
        }
    }

    private OFunctionResult validateEachScreenField(ScreenField screenField, OUser loggedUser) {
        logger.debug("Entering: validateEachScreenField");
        OFunctionResult oFR = new OFunctionResult();
        SingleEntityScreen screen = null;
        try {
            screen = (SingleEntityScreen) screenField.getOScreen();
            long screenDBID = screen.getDbid();
            OEntityDTO actOnEntity = entitySetupServiceRemote.getOScreenEntityDTO(screenDBID, loggedUser);
            if (actOnEntity == null) {
                logger.error("Screen Has NULL actOnEntity");
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
                return oFR;
            }
            String actOnEntityClassPath = actOnEntity.getEntityClassPath();
            Class screenEntityClass = null;
            try {
                screenEntityClass = Class.forName(actOnEntityClassPath);
            } catch (ClassNotFoundException e) {
                oFR.addError(usrMsgService.getUserMessage("EntityNotFound",
                        Collections.singletonList(actOnEntityClassPath), loggedUser));
                return oFR;
            }

            // Validate Screen Fields Expression
            if (screen instanceof TabularScreen || screen instanceof FormScreen) {
                List<ExpressionFieldInfo> infos = BaseEntity.parseFieldExpression(
                        screenEntityClass, screenField.getFieldExpression());
                if (infos == null) {
                    oFR.addError(usrMsgService.getUserMessage(
                            "FieldExpressionNotFound", usrMsgService.
                            constructListFromStrings(
                            screenField.getFieldExpression(),
                            actOnEntity.getEntityClassName()),
                            loggedUser));
                } else {
                    Field field = infos.get(infos.size() - 1).field;
                    if (!field.getType().isPrimitive() && field.getType() != String.class
                            && field.getType() != Date.class
                            && field.getType().getSuperclass() != Number.class
                            && field.getType() != Boolean.class
                            && !field.getType().getName().equals("[B")) {
                        oFR.addError(usrMsgService.getUserMessage("ScreenFieldPimitiveType",
                                Collections.singletonList(field.getName()), loggedUser));
                        return oFR;
                    }
                }

                // Validate lookup

                if (screenField.isEditable()) {
                    DD dd = screenField.getDd();
                    if (dd == null || dd.getDbid() == 0) {
                        // User didn't choose DD
                        // Initialize DD to default value from Entity
                        logger.debug("user didn't choose DDs");
                        BaseEntity baseEntityTEMP = (BaseEntity) screenEntityClass.newInstance();
                        dd = ddService.getDD(BaseEntity.getFieldDDName(
                                baseEntityTEMP, screenField.getFieldExpression()), loggedUser);

                    }
                    if (dd.getControlType().getDbid() == DD.CT_LOOKUPSCREEN
                            && dd.getControlType().getDbid() == DD.CT_LOOKUPMULTILEVEL) {
                        infos = BaseEntity.parseFieldExpression(
                                screenEntityClass, screenField.getFieldExpression());
                        Field field = infos.get(infos.size() - 1).field;
                        if (!BaseEntity.isFieldUnique(field)) {
                            oFR.addError(usrMsgService.getUserMessage("LookupControlError",
                                    usrMsgService.constructListFromStrings(screen.getName(), field.getName()), loggedUser));
                            return oFR;
                        }
                    }
                }
            }

            //Validate all or non fields in groups
            //oFR.append(validateAllOrNonFieldsInGroups(odm, functionParms, loggedUser));
            return oFR;

        } catch (Exception ex) {
            logger.error("Eception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning: validateEachScreenField");
            return oFR;
        }
    }
}
