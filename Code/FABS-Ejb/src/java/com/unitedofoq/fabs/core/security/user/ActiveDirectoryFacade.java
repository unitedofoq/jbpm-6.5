package com.unitedofoq.fabs.core.security.user;

import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.central.OCentralUser;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;

import static com.unitedofoq.fabs.core.security.user.ActiveDirectory.logger;
import com.unitedofoq.fabs.core.setup.FABSSetupBean;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;


import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import static javax.naming.directory.SearchControls.SUBTREE_SCOPE;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.PagedResultsControl;
import javax.naming.ldap.PagedResultsResponseControl;
import javax.naming.ldap.StartTlsRequest;
import javax.naming.ldap.StartTlsResponse;
import javax.net.ssl.*;
import org.slf4j.LoggerFactory;

//******************************************************************************
//**  ActiveDirectoryFacade
//*****************************************************************************/
/**
 * Provides static methods to authenticate users, change passwords, etc.
 *
 *****************************************************************************
 */
//@WebService
//@Singleton
//@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.security.user.ActiveDirectoryFacadeLocal",
//        beanInterface = ActiveDirectoryFacadeLocal.class)
public class ActiveDirectoryFacade implements ActiveDirectoryFacadeLocal {

    private static String[] userAttributes = {
        "distinguishedName",
        "cn",
        "name",
        "uid",
        "sn",
        "givenname", "memberOf", "samaccountname",
        "userPrincipalName", "mail"
    };
    private static String[] groupAttributes = {
        "name", "cn"
    };

//  public ActiveDirectoryFacade() {
    private ActiveDirectoryFacade() {
    }

    //**************************************************************************
    //** getConnection
    //*************************************************************************/
    /**
     * Used to authenticate a user given a username/password. Domain name is
     * derived from the fully qualified domain name of the host machine.
     */
    public static DirContext getConnection(String username, String password) throws NamingException {
        return getConnection(username, password, null, null);
    }

    //**************************************************************************
    //** getConnection
    //*************************************************************************/
    /**
     * Used to authenticate a user given a username/password and domain name.
     */
    public static DirContext getConnection(String username, String password, String domainName) throws NamingException {
        return getConnection(username, password, domainName, null);
    }

    //**************************************************************************
    //** getConnection
    //*************************************************************************/
    /**
     * Used to authenticate a user given a username/password and domain name.
     * Provides an option to identify a specific a Active Directory server.
     */
    public static LdapContext getConnection(String username, String password, String domainName, String serverName) throws NamingException {
logger.debug("Entering");
        if (domainName == null) {
            logger.debug("Domain Number is Null, I will get it form host Name");
            try {
                String fqdn = java.net.InetAddress.getLocalHost().getCanonicalHostName();
                if (fqdn.split("\\.").length > 1) {
                    domainName = fqdn.substring(fqdn.indexOf(".") + 1);
                }
            } catch (java.net.UnknownHostException ex) {
                logger.error("exception thrown: ",ex);
            }
        }

        if (password != null) {
            password = password.trim();
            if (password.length() == 0) {
                password = null;
            }
        }

        //bind by using the specified username/password
        Hashtable props = new Hashtable();
        String principalName = username + "@" + domainName;
        props.put(Context.SECURITY_PRINCIPAL, principalName);
        if (password != null) {
            props.put(Context.SECURITY_CREDENTIALS, password);
        }

        String ldapURL = "ldap://" + ((serverName == null) ? domainName : serverName + "." + domainName) + '/';
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        ldapURL = "ldap://" + serverName + "/";
        props.put(Context.PROVIDER_URL, ldapURL);
        try {
            return new InitialLdapContext(props, null);
        } catch (javax.naming.CommunicationException ex) {
            logger.error("Exception thrown: ",ex);
            throw new NamingException("Failed to connect to " + domainName + ((serverName == null) ? "" : " through " + serverName));
        } catch (NamingException ex) {
            logger.error("Exception thrown: ",ex);
            throw new NamingException("Failed to authenticate " + username + "@" + domainName + ((serverName == null) ? "" : " through " + serverName));
        }
    }

    //**************************************************************************
    //** getUser
    //*************************************************************************/
    /**
     * Used to check whether a username is valid.
     *
     * @param username A username to validate (e.g. "peter", "peter@acme.com",
     * or "ACME\peter").
     */
    public static User getUser(String username, LdapContext context) {
        logger.debug("Entering");
        try {
            String domainName = null;
            if (username.contains("@")) {
                username = username.substring(0, username.indexOf("@"));
                domainName = username.substring(username.indexOf("@") + 1);
            } else if (username.contains("\\")) {
                username = username.substring(0, username.indexOf("\\"));
                domainName = username.substring(username.indexOf("\\") + 1);
            } else {
                String authenticatedUser = (String) context.getEnvironment().get(Context.SECURITY_PRINCIPAL);
                if (authenticatedUser.contains("@")) {
                    domainName = authenticatedUser.substring(authenticatedUser.indexOf("@") + 1);
                }
            }

            if (domainName != null) {
                String principalName = username + "@" + domainName;
                SearchControls controls = new SearchControls();
                controls.setSearchScope(SUBTREE_SCOPE);
                controls.setReturningAttributes(userAttributes);
                NamingEnumeration<SearchResult> answer = context.search(toDC(domainName),
                        "(& (userPrincipalName=" + principalName + ")(objectClass=user))", controls);
                if (answer.hasMore()) {
                    Attributes attr = answer.next().getAttributes();
                    NamingEnumeration<String> ids = attr.getIDs();
                    User user = null;
                    if (attr.get("name") != null) {
                        user = new User(attr);
                    }
                    if (user != null) {
                        logger.debug("Returning with User");
                        return new User(attr);
                    }
                }
            }
        } catch (NamingException ex) {
            logger.error("Exception thrown: ",ex);
        }
        logger.debug("Returning with Null");
        return null;
    }

    public static List<User> listAllUsers(String searchBase, LdapContext authContext) {
        logger.debug("Entering");
        List<User> foundUsers = new ArrayList<User>();
        try {
            int pageSize = 999;
            byte[] cookie = null;
            int total;
            try {
                authContext.setRequestControls(new Control[]{
                    new PagedResultsControl(pageSize, Control.NONCRITICAL)});
            } catch (IOException ex) {
                logger.error("Exception thrown: ",ex);
            }
            do {

                SearchControls constraints = new SearchControls();
                //        String []returnedAttributes = {"mail"};
                String filter = "(objectClass=user)"; // You might want to limit search to user objects only based on objectClass
//            String []filterAttributes = {"test"};

                constraints.setReturningAttributes(userAttributes);
                constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
                NamingEnumeration<SearchResult> results = authContext.search(searchBase, filter, constraints);
                while (results.hasMore()) {
                    Attributes attr = ((SearchResult) results.next()).getAttributes();
                    Attribute user = attr.get("userPrincipalName");
                    if (user != null) {
                        foundUsers.add(new User(attr));
                    }
                }
                Control[] controls = authContext.getResponseControls();
                if (controls != null) {
                    for (int i = 0; i < controls.length; i++) {
                        if (controls[i] instanceof PagedResultsResponseControl) {
                            PagedResultsResponseControl prrc
                                    = (PagedResultsResponseControl) controls[i];
                            total = prrc.getResultSize();
                            if (total != 0) {
                                System.out.println("***************** END-OF-PAGE "
                                        + "(total : " + total
                                        + ") *****************\n");
                            } else {
                                System.out.println("***************** END-OF-PAGE "
                                        + "(total: unknown) ***************\n");
                            }
                            cookie = prrc.getCookie();
                        }
                    }
                } else {
                    System.out.println("No controls were sent from the server");
                }
                // Re-activate paged results
                authContext.setRequestControls(new Control[]{
                    new PagedResultsControl(pageSize, cookie, Control.CRITICAL)});

            } while (cookie != null);

            authContext.close();

        } catch (NamingException e) {
            System.err.println("PagedSearch failed.");
            e.printStackTrace();
        } catch (IOException ie) {
            System.err.println("PagedSearch failed.");
            ie.printStackTrace();
        }
        logger.debug("Returning");
        return foundUsers;
    }

    //**************************************************************************
    //** getUsers
    //*************************************************************************/
    /**
     * Returns a list of users in the domain.
     */
    private static String toDC(String domainName) {
        logger.debug("Entering");
        StringBuilder buf = new StringBuilder();
        for (String token : domainName.split("\\.")) {
            if (token.length() == 0) {
                continue;   // defensive check
            }
            if (buf.length() > 0) {
                buf.append(",");
            }
            buf.append("DC=").append(token);
        }
        return buf.toString();
    }

    //**************************************************************************
    //** User Class
    //*************************************************************************/
    /**
     * Used to represent a User in Active Directory
     */
    public static class User {
final static org.slf4j.Logger logger = LoggerFactory.getLogger(User.class);
        private String distinguishedName;
        private String commonName;
        private String givenName;
        private String memberof;
        private String mail;
        private String samaccountname;

        public User(Attributes attr) throws javax.naming.NamingException {
            commonName = attr.get("cn") != null ? (String) attr.get("cn").get() : null;
            givenName = attr.get("name") != null ? (String) attr.get("name").get() : null;
            distinguishedName = attr.get("distinguishedName") != null ? (String) attr.get("distinguishedName").get() : null;
            mail = attr.get("mail") != null ? (String) attr.get("mail").get() : null;
            samaccountname = attr.get("samaccountname") != null ? (String) attr.get("samaccountname").get() : givenName;

        }

        public String getCommonName() {
            return commonName;
        }

        public String getDistinguishedName() {
            return distinguishedName;
        }

        public String toString() {
            return getDistinguishedName();
        }

        /**
         * Used to change the user password. Throws an IOException if the Domain
         * Controller is not LDAPS enabled.
         *
         * @param trustAllCerts If true, bypasses all certificate and host name
         * validation. If false, ensure that the LDAPS certificate has been
         * imported into a trust store and sourced before calling this method.
         * Example: String keystore =
         * "/usr/java/jdk1.5.0_01/jre/lib/security/cacerts";
         * System.setProperty("javax.net.ssl.trustStore",keystore);
         */
        public void changePassword(String oldPass, String newPass, boolean trustAllCerts, LdapContext context)
                throws java.io.IOException, NamingException {
            String dn = getDistinguishedName();
            logger.trace("Switch to SSL/TLS");
            //Switch to SSL/TLS
            StartTlsResponse tls = null;
            try {
                tls = (StartTlsResponse) context.extendedOperation(new StartTlsRequest());
            } catch (Exception ex) {
                //"Problem creating object: javax.naming.ServiceUnavailableException: [LDAP: error code 52 - 00000000: LdapErr: DSID-0C090E09, comment: Error initializing SSL/TLS, data 0, v1db0"
                logger.error("Exception thrown: ",ex);
                logger.debug("Failed to establish SSL connection to the Domain Controller. Is LDAPS enabled?");
                throw new java.io.IOException("Failed to establish SSL connection to the Domain Controller. Is LDAPS enabled?");
            }
            //Exchange certificates
            if (trustAllCerts) {
                tls.setHostnameVerifier(DO_NOT_VERIFY);
                SSLSocketFactory sf = null;
                try {
                    SSLContext sc = SSLContext.getInstance("TLS");
                    sc.init(null, TRUST_ALL_CERTS, null);
                    sf = sc.getSocketFactory();
                } catch (java.security.NoSuchAlgorithmException ex) {
                    logger.error("exception thrown: ",ex);
                } catch (java.security.KeyManagementException ex) {
                    logger.error("Exception thrown: ",ex);
                }
                tls.negotiate(sf);
            } else {
                tls.negotiate();
            }

            //Change password
            try {
                ModificationItem[] modificationItems = new ModificationItem[2];
                modificationItems[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("unicodePwd", getPassword(oldPass)));
                modificationItems[1] = new ModificationItem(DirContext.ADD_ATTRIBUTE, new BasicAttribute("unicodePwd", getPassword(newPass)));
                context.modifyAttributes(dn, modificationItems);
            } catch (javax.naming.directory.InvalidAttributeValueException ex) {
                String error = ex.getMessage().trim();
                logger.error("Exception thrown: ",ex);
                if (error.startsWith("[") && error.endsWith("]")) {
                    error = error.substring(1, error.length() - 1);
                }
                System.err.println(error);
                tls.close();
                throw new NamingException(
                        "New password does not meet Active Directory requirements. "
                        + "Please ensure that the new password meets password complexity, "
                        + "length, minimum password age, and password history requirements.");
            } catch (NamingException ex) {
                logger.error("Exception thrown: ",ex);
                tls.close();
                throw ex;
            }

            //Close the TLS/SSL session
            logger.debug("Close the TLS/SSL session");
            tls.close();
            logger.debug("Returning");
        }
        private static final HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
        private static TrustManager[] TRUST_ALL_CERTS = new TrustManager[]{
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
            }
        };

        private byte[] getPassword(String newPass) {
            String quotedPassword = "\"" + newPass + "\"";
            //return quotedPassword.getBytes("UTF-16LE");
            char unicodePwd[] = quotedPassword.toCharArray();
            byte pwdArray[] = new byte[unicodePwd.length * 2];
            for (int i = 0; i < unicodePwd.length; i++) {
                pwdArray[i * 2 + 1] = (byte) (unicodePwd[i] >>> 8);
                pwdArray[i * 2 + 0] = (byte) (unicodePwd[i] & 0xff);
            }
            return pwdArray;
        }

        /**
         * @return the givenName
         */
        public String getGivenName() {
            return givenName;
        }

        /**
         * @param givenName the givenName to set
         */
        public void setGivenName(String givenName) {
            this.givenName = givenName;
        }

        /**
         * @param distinguishedName the distinguishedName to set
         */
        public void setDistinguishedName(String distinguishedName) {
            this.distinguishedName = distinguishedName;
        }

        /**
         * @param commonName the commonName to set
         */
        public void setCommonName(String commonName) {
            this.commonName = commonName;
        }

        /**
         * @return the memberof
         */
        public String getMemberof() {
            return memberof;
        }

        /**
         * @param memberof the memberof to set
         */
        public void setMemberof(String memberof) {
            this.memberof = memberof;
        }

        /**
         * @return the mail
         */
        public String getMail() {
            return mail;
        }

        /**
         * @param mail the mail to set
         */
        public void setMail(String mail) {
            this.mail = mail;
        }

        /**
         * @return the samaccountname
         */
        public String getSamaccountname() {
            return samaccountname;
        }

        /**
         * @param samaccountname the samaccountname to set
         */
        public void setSamaccountname(String samaccountname) {
            this.samaccountname = samaccountname;
        }
    }

    /**
     * @desc update User's attributes at Active Directory
     * Uses attributes passed without any mapping
     *
     * @param userLoginName user identifier
     * @param modAttrib Properties object (key pair) that contains the required
     * User's attributes to be mapped to AD schema
     * @return
     */
    public static void updateUserAtrributes(String userLoginName,  Properties modAttrib) {
logger.debug("Entering");
        try {       
            Properties newModAttrib = mapUserAttributesfromOTMStoAD(modAttrib);
            LdapContext ldctx = new InitialLdapContext();
            String securityAuthentication = getParametersFromFABSSetup("securityAuthenication");
            String serverURL = getParametersFromFABSSetup("ADserverURL");
            String domainName = getParametersFromFABSSetup("domainname");
            String domainPassword = getParametersFromFABSSetup("domainpassword");

            ldctx = getConnection(securityAuthentication,
                    domainPassword, domainName, serverURL);

            int size = modAttrib.size();
            // Specify the changes to make
            ModificationItem[] mods = new ModificationItem[size];

            User user = getUser(userLoginName, ldctx);

            if (user == null) {
                return;
            }

            Enumeration enume = modAttrib.propertyNames();
            int i = 0;
            while (enume.hasMoreElements()) {
                String key = (String) enume.nextElement();
                // Replace the "mail" attribute with a new value
                String value = modAttrib.getProperty(key);
                if ("userAccountControl".equals(key)) {
                    int accountControlValue = Integer.parseInt((String) ldctx.getAttributes(user.getDistinguishedName()).get("userAccountControl").get());
                    if ("true".equals(value)) {
                        value = String.valueOf(accountControlValue | 2);
                    } else {
                        value = String.valueOf(accountControlValue & ~2);
                    }
                }
                mods[i] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                        new BasicAttribute(key, value));
                i++;
            }
            // Perform the requested modifications on the named object
            ldctx.modifyAttributes(user.getDistinguishedName(), mods);
            //TODO write your implementation code here:
            ldctx.close();
        } catch (NamingException ex) {
            logger.error("exception thrown: ",ex);
        }
        logger.debug("Returning");
    }
    
    /**
     * @desc update or map User's attributes name to specific name that can be
     * recognized by Active Directory ,using mapped configuration
     * @param modifiedAttribs Properties object (key pair) that contains the
     * required User's attributes and it is value to be changed
     * @return Properties object (key pair)
     */
    private static Properties mapUserAttributesfromOTMStoAD(Properties modifiedAttribs) {
logger.debug("Entering");
        Properties newModifiedAttribs = new Properties();
        //@todo should be loaded from FABS setup
        
        Properties mappedAttributes = new Properties();
        mappedAttributes.setProperty("name", "givenName");
        mappedAttributes.setProperty("Notes", "info");
        mappedAttributes.setProperty("phone", "mobile");
        mappedAttributes.setProperty("disable", "userAccountControl");

        Enumeration enume = modifiedAttribs.propertyNames();

        while (enume.hasMoreElements()) {
            String key = (String) enume.nextElement();
            String value = (String) modifiedAttribs.getProperty(key);
            String mappedKey = mappedAttributes.getProperty(key);
            newModifiedAttribs.setProperty(mappedKey, value);
        }
        logger.debug("Returning");
        return newModifiedAttribs;
    }

    private static String getParametersFromFABSSetup(String sKey) {
        logger.debug("Entering");
      String svalue =null;
        try {
            OUser loggedUser = null;

            InitialContext ctx = new InitialContext();
            FABSSetupLocal fabsSetup = (FABSSetupLocal) ctx.lookup("java:global/ofoq/com.unitedofoq.fabs.core.setup.FABSSetupLocal");
            OCentralEntityManagerRemote centralOEM = (OCentralEntityManagerRemote) ctx.lookup("java:global/ofoq/com.unitedofoq.fabs.central.OCentralEntityManagerRemote");
            OEntityManagerRemote oem = (OEntityManagerRemote) ctx.lookup("java:global/ofoq/com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote");

            List<OCentralUser> OCentralUsers = centralOEM.getAdminUsers();
            loggedUser = oem.getUser(OCentralUsers.get(0).getLoginName(), OCentralUsers.get(0).getTenant());

            svalue = fabsSetup.loadKeySetup(sKey, loggedUser).getSvalue();
        } catch (NamingException ex) {
            logger.error("Exception thrown: ",ex);
        }
        logger.debug("Returning");
        return svalue;
    }

//    @Override
    public static void disableUser(String userId) {
        Properties modifiededAttribute = new Properties();
        modifiededAttribute.setProperty("disable", "true");
        updateUserAtrributes(userId, modifiededAttribute);
    }

//    @Override
    public static void enableUser(String userId) {
        Properties modifiededAttribute = new Properties();
        modifiededAttribute.setProperty("disable", "false");
        updateUserAtrributes(userId, modifiededAttribute);
    }

//    @Override
    public void blockUser(String userId) {
//        Properties modifiededAttribute = new Properties();
//        modifiededAttribute.setProperty("ms-DS-User-Account-Auto-Locked", "true");
//        updateUserAtrributes(userId,modifiededAttribute);
    }
}
