/*
 * ============================================================================
 *                   GNU Lesser General Public License
 * ============================================================================
 *
 * Taylor - The Java Enterprise Application Framework.
 * Copyright (C) 2005 John Gilbert jgilbert01@users.sourceforge.net
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 * 
 * John Gilbert
 * Email: jgilbert01@users.sourceforge.net
 */
package com.unitedofoq.fabs.core.security.user;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Use this annotation to flag properties on entity beans for encryption by the
 * EncyptionCallbackListener.
 * 
 * The properties MUST be of type String.
 * 
 * @author jgilbert
 * 
 */
@Target(METHOD)
@Retention(RUNTIME)
@Documented
public @interface Encrypted {

	/**
	 * Use this attribute to control which Encryptor implementation to use.
	 * 
	 * Another class that implements the Encryptor interface and has a default
	 * constructor can be used. The default implementation is Base64Encryptor.
	 * 
	 * @return
	 */
	Class encryptor() default Base64Encryptor.class;
}
