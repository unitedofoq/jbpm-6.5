/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitysetup;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.ServerJob;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.validation.FABSException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author melsayed
 */
@Local
public interface EntitySetupServiceRemote {

    /**
     * {@link EntitySetupService#loadOEntity(String, OUser)}
     */
    public OEntity loadOEntity(String entityClassPath, OUser loggedUser)
            throws UserNotAuthorizedException;

    public List<OEntityAction> getEntityActions(OEntity entity, OUser loggedUser)
            throws UserNotAuthorizedException;

    public List<OObjectBriefInfoField> getObjectBriefInfoFields(OEntity entity, OUser loggedUser)
            throws Exception, UserNotAuthorizedException;

    public OObjectBriefInfoField getObjectMasterField(OEntity entity, OUser loggedUser)
            throws UserNotAuthorizedException;

    public DD getObjectMasterFieldDD(OEntity entity, OUser loggedUser)
            throws UserNotAuthorizedException;

    public List<OEntityTreeDDField> getTreeDDFields(String classPath, int depth);

    public OFunctionResult runEntityActionPosts(
            OEntityAction action, ODataMessage oDM, OFunctionParms funcParms, OUser loggedUser);

    public OFunctionResult executeAction(
            OEntityAction action, ODataMessage oDM, OFunctionParms funcParms, OUser loggedUser);

    /**
     * {@link EntitySetupService#loadParentOEntity(OEntity, OUser) }
     */
    public List loadParentOEntity(OEntity oEntity, OUser loggedUser)
            throws UserNotAuthorizedException;

    public OFunctionResult validateOEntityStautsMapping(ODataMessage oDM,
            OFunctionParms funcParms, OUser loggedUser);

    /**
     * Refer to
     * {@link EntitySetupService#loadEntityActionPrivilege(OEntityAction, OUser)}
     */
    public void loadEntityActionPrivilege(OEntityAction action, OUser loggedUser)
            throws UserNotAuthorizedException;

    /**
     * Refer to
     * {@link EntitySetupService#createDefaultBriefInfoFields(OEntity, OUser)}
     */
    public OFunctionResult createDefaultBriefInfoFields(OEntity oEntity, OUser loggedUser)
            throws FABSException;

    /**
     * Refer to {@link EntitySetupService#getDefaultBriefInfoFields(OEntity, OUser)
     * }
     */
    public List<Field> getDefaultBriefInfoFields(OEntity oEntity, OUser loggedUser)
            throws FABSException;

    /**
     * Refer to {@link EntitySetupService#callEntityCreateAction(BaseEntity, OUser)
     * }
     */
    public OFunctionResult callEntityCreateAction(BaseEntity entity, OUser loggedUser);

    /**
     * Refer to {@link EntitySetupService#callEntityListCreateAction(BaseEntity, OUser)
     * }
     */
    public OFunctionResult callEntityListCreateAction(List entityList, OUser loggedUser);

    /**
     * Refer to {@link EntitySetupService#callEntityUpdateAction(BaseEntity, OUser)
     * }
     */
    public OFunctionResult callEntityUpdateAction(BaseEntity entity, OUser loggedUser);

    /**
     * Refer to {@link EntitySetupService#callEntityListUpdateAction(BaseEntity, OUser)
     * }
     */
    public OFunctionResult callEntityListUpdateAction(List<BaseEntity> entityList, OUser loggedUser);

    public OEntity loadOEntityByClassNameForTree(String entityClassName, OUser loggedUser) throws UserNotAuthorizedException;

    public OEntityDTO loadOEntityDTOForTree(String entityClassPath, OUser loggedUser) throws UserNotAuthorizedException;

    public com.unitedofoq.fabs.core.entitysetup.OEntity loadOEntityByClassName(java.lang.String entityClassName, com.unitedofoq.fabs.core.security.user.OUser loggedUser) throws com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;

    public OFunctionResult callEntityDeleteAction(BaseEntity entity, OUser loggedUser);

    /**
     * Refer to {@link EntitySetupService#refreshOEntityReferences(ODataMessage, OFunctionParms, OUser)
     * }
     */
    public OFunctionResult refreshOEntityReferences(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    /**
     * Refer to {@link EntitySetupService#refreshAllOEntitiesReferences(ODataMessage, OFunctionParms, OUser)
     * }
     */
    public OFunctionResult refreshAllOEntitiesReferences(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    /**
     * Refer to {@link EntitySetupService#loadEntityParentForBriefInfoFields(OEntity, String, long, OUser)
     * }
     */
    public BaseEntity loadEntityParentForBriefInfoFields(
            OEntity entityOEntity, String parentClassPath, long parentDBID,
            OUser loggedUser)
            throws UserNotAuthorizedException;

    public OFunctionResult createJavaClassFromOJavaEntity(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult validateClassName(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult validateFieldName(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult createAllJavaEntities(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult createNewJavaEntities(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult postCreateJavaEntity(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult preCreateJavaEntityField(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult preUpdateJavaEntity(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult preRemoveJavaEntityField(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult postRemoveJavaEntity(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult preUpdateJavaEntityField(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult postUpdateJavaEntity(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    //FIXME: Commented from version r18.1.7 as there's no usage
//    public OFunctionResult buildCreateSQLCommand(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
//    public OFunctionResult buildAllCreateSQLCommands(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
//    public OFunctionResult buildNewCreateSQLCommands(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult runCreateSQLCommand(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult runAllCreateSQLCommand(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult runNewCreateSQLCommand(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult postCreateJavaEntityField(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult postRemoveJavaEntityField(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult preCreateJavaEntity(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    /**
     * Refer to
     * {@link EntitySetupService#clearOEntityCache(ODataMessage, OFunctionParms, OUser)}
     */
    public OFunctionResult clearOEntityCache(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    /**
     *
     */
    public OFunctionResult createOEntityAccessPrivilege(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult onOEntityCachedUpdate(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult onOEntityCachedRemove(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult preOEntityCachedUpdate(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult deleteNonExistingOEntitiesRefrences(OUser loggedUser);

    /**
     * Refer to
     * {@link EntitySetupService#getParentsFromReference(String, OUser)}
     */
    public OFunctionResult getParentsFromReference(String entityClassPath, OUser loggedUser);

    public void runServerJob(OModule module, UDC notificationType, ServerJob serverJob,
            Method functionMethod, Object javaFunctionClsObj, OUser loggedUser, Object[] args);

    public void runServerJob(long moduleDBID, long notificationTypeDBID, ServerJob serverJob, Method functionMethod, Object javaFunctionClsObj, OUser loggedUser, Object[] args);

    public boolean checkPrivilegeAuthorithy(String privilege, OUser loggedUser);

    public java.util.List<com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO> getDisplayedEntityActionsDTO(com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO entity,
            com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO loadOEntityDTOByClassName(java.lang.String entityClassName, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO loadOEntityDTO(java.lang.String entityClassPath, com.unitedofoq.fabs.core.security.user.OUser loggedUser) throws com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;

    public com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO getOScreenEntityDTO(long screenDBID, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO getOReportEntityDTO(long screenDBID, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO getWebServiceEntityDTO(long screenDBID, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.entitysetup.OEntityAction getOEntityDTOAction(long oentityDBID, long actionType, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public java.util.List<com.unitedofoq.fabs.core.entitysetup.OObjectBriefInfoField> getObjectDTOBriefInfoFields(com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO entity, com.unitedofoq.fabs.core.security.user.OUser loggedUser) throws com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;

    public com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO getRoutingEntityDTO(long routingDBID, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO getTreeLevelEntityDTO(long treeLevelDBID, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO getRoutingParentEntityDTO(long treeLevelDBID, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO loadOEntityDTOByDBID(long dbid, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO getScheduleExpressionEntityDTO(long schexpDBID, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public java.util.List loadParentOEntity(com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO oEntity, com.unitedofoq.fabs.core.security.user.OUser loggedUser) throws com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;

    public java.util.List<com.unitedofoq.fabs.core.entitysetup.EntityAttributeInit> getObjectDTOEntityAttributeInits(com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO entity, com.unitedofoq.fabs.core.security.user.OUser loggedUser) throws com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;

    public java.util.ArrayList<java.lang.String> getCCExpressions(com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO entityDTO, boolean loadFromDB, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult onOEntityDTOCachedUpdate(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult preOEntityDTOCachedUpdate(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult onOEntityDTOCachedRemove(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult clearOEntityDTOCache(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO getOEntityDTOActionDTO(long oentityDBID, long actionType, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult executeAction(com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO action, com.unitedofoq.fabs.core.datatype.ODataMessage oDM, com.unitedofoq.fabs.core.function.OFunctionParms funcParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult executeAction(com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO action, com.unitedofoq.fabs.core.datatype.ODataMessage oDM, com.unitedofoq.fabs.core.function.OFunctionParms funcParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser, boolean runPosts, boolean runValidations);

    public java.util.List<com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO> getEntityActionsDTO(com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO entity, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult executeAction(com.unitedofoq.fabs.core.entitysetup.OEntityAction action, com.unitedofoq.fabs.core.datatype.ODataMessage oDM, com.unitedofoq.fabs.core.function.OFunctionParms funcParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser, boolean runPosts, boolean runValidations);

    public com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO getActionDTO(long dbid, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.entitybase.BaseEntity loadEntityParentForBriefInfoFields(com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO entityOEntity, java.lang.String parentClassPath, long parentDBID, com.unitedofoq.fabs.core.security.user.OUser loggedUser) throws com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;

    public java.util.List<java.lang.reflect.Field> getDefaultBriefInfoFields(com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO oEntity, com.unitedofoq.fabs.core.security.user.OUser loggedUser) throws com.unitedofoq.fabs.core.validation.FABSException;

    public OEntityDTO loadOEntityDTOByNameForAuditing(String entityClassName, OUser loggedUser);

}
