/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.report.customReports;

import java.util.List;

// TODO : Added by Belal. Not used class, potential to be deleted.
/**
 *
 * @author MEA
 */
public class CustomreportFunctionDTO {
    private long dbid;
    private String name;
    private long customReportDBID;
    private String customReportName;
    private byte[] attachment;
    private long customReportFilterDBID;
    private boolean customReportFilterShowBeforRun;
    private List<CustomReportFilterFieldDTO> customReportFilterFields;

    public CustomreportFunctionDTO() {
    }

    public CustomreportFunctionDTO(long dbid,
            String name,
            long customReportDBID, 
            String customReportName, 
            byte[] attachment, 
            long customReportFilterDBID, 
            boolean customReportFilterShowBeforRun) {
        this.dbid = dbid;
        this.name=name;
        this.customReportDBID = customReportDBID;
        this.customReportName = customReportName;
        this.attachment = attachment;
        this.customReportFilterDBID = customReportFilterDBID;
        this.customReportFilterShowBeforRun = customReportFilterShowBeforRun;
    }

    public long getDbid() {
        return dbid;
    }

    public void setDbid(long dbid) {
        this.dbid = dbid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCustomReportDBID() {
        return customReportDBID;
    }

    public void setCustomReportDBID(long customReportDBID) {
        this.customReportDBID = customReportDBID;
    }

    public String getCustomReportName() {
        return customReportName;
    }

    public void setCustomReportName(String customReportName) {
        this.customReportName = customReportName;
    }

    public byte[] getAttachment() {
        return attachment;
    }

    public void setAttachment(byte[] attachment) {
        this.attachment = attachment;
    }

    public long getCustomReportFilterDBID() {
        return customReportFilterDBID;
    }

    public void setCustomReportFilterDBID(long customReportFilterDBID) {
        this.customReportFilterDBID = customReportFilterDBID;
    }

    public boolean isCustomReportFilterShowBeforRun() {
        return customReportFilterShowBeforRun;
    }

    public void setCustomReportFilterShowBeforRun(boolean customReportFilterShowBeforRun) {
        this.customReportFilterShowBeforRun = customReportFilterShowBeforRun;
    }

    public List<CustomReportFilterFieldDTO> getCustomReportFilterFields() {
        return customReportFilterFields;
    }

    public void setCustomReportFilterFields(List<CustomReportFilterFieldDTO> customReportFilterFields) {
        this.customReportFilterFields = customReportFilterFields;
    }
 
}
