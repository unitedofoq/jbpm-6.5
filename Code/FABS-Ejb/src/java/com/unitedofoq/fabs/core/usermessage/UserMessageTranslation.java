
package com.unitedofoq.fabs.core.usermessage;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class UserMessageTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="messageTitle">
    @Column
    private String messageTitle;

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

    public String getMessageTitle() {
        return messageTitle;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="messageText">
    @Column
    private String messageText;

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageText() {
        return messageText;
    }
    // </editor-fold>

}
