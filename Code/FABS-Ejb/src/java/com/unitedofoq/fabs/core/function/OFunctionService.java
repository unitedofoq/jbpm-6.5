/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.function;

import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;

import org.kie.internal.KnowledgeBase;
import org.kie.internal.KnowledgeBaseFactory;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.api.io.ResourceType;
import org.kie.internal.io.ResourceFactory;
import org.drools.core.io.impl.UrlResource;
import org.kie.internal.runtime.StatefulKnowledgeSession;
import org.kie.api.runtime.rule.AgendaFilter;
import org.primefaces.context.RequestContext;
import com.unitedofoq.fabs.core.businessrule.OAgendaFilter;
import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.datatype.ODataTypeAttribute;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.encryption.DataEncryptorServiceLocal;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.i18n.TextTranslationServiceRemote;
import com.unitedofoq.fabs.core.log.OLogSetUpServiceRemote;
import com.unitedofoq.fabs.core.module.ModuleServiceRemote;
import com.unitedofoq.fabs.core.multimedia.OIMageServiceLocal;
import com.unitedofoq.fabs.core.multimedia.OImage;
import com.unitedofoq.fabs.core.process.ProcessFunction;
import com.unitedofoq.fabs.core.process.ProcessServiceRemote;
import com.unitedofoq.fabs.core.report.ReportFunction;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserMenu;
import com.unitedofoq.fabs.core.security.user.UserServiceRemote;
import com.unitedofoq.fabs.core.setup.FABSSetup;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.orgchart.OrgChartFunction;
import com.unitedofoq.fabs.core.uiframework.portal.PortalPageFunction;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenFunction;
import com.unitedofoq.fabs.core.uiframework.screen.wizard.WizardFunction;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.fabs.core.validation.FABSException;
import com.unitedofoq.fabs.core.webservice.OWebService;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import org.drools.*;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author melsayed
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.function.OFunctionServiceRemote",
        beanInterface = OFunctionServiceRemote.class)
public class OFunctionService implements OFunctionServiceRemote {

    @EJB
    private OEntityManagerRemote oem;
    @EJB
    ProcessServiceRemote processServiceRemote;
    @EJB
    UserServiceRemote userService;
    @EJB
    FABSSetupLocal fabsSetup;
    @EJB
    UserMessageServiceRemote userMessageServiceRemote;
    @EJB
    EntitySetupServiceRemote entitySetupService;
    @EJB
    DataTypeServiceRemote dataTypeService;
    @EJB
    OIMageServiceLocal imageService;
    @EJB
    TextTranslationServiceRemote translationServiceRemote;
    @EJB
    DataEncryptorServiceLocal dataEncryptorServiceLocal;
    @EJB
    DDServiceRemote dDService;
    @EJB
    OLogSetUpServiceRemote oLogSetUpService;
    @EJB
    UIFrameworkServiceRemote uIFrameworkService;
    @EJB
    ModuleServiceRemote moduleService;

    BaseEntity currentEntityObject;
    boolean userExit;
    //validateName//validateEmail//fieldUniqueOptional//addUserToLDAP//
    final static Logger logger = LoggerFactory.getLogger(OFunctionService.class);
    static Map<String, OFunction> cachedOfucntions = new HashMap<>();

    @Override
    public OFunction getOFunction(Long dbid, OUser loggedUser) {
        OFunction function = cachedOfucntions.get(loggedUser.getTenant().getId() + "_" + dbid);
        try {
            function = (OFunction) oem.loadEntity(OFunction.class.getSimpleName(), dbid, null, null, loggedUser);
            cachedOfucntions.put(loggedUser.getTenant().getId() + "_" + dbid, function);
        } catch (Exception e) {
            logger.error("Exception thrown: ", e);
        }
        return function;
    }

    @Override//validateLoginNameUnchanged//validateEmail//fieldUniqueOptional//editUserInLDAP//userChangePassword
    public OFunctionResult runFunction(OFunction function, ODataMessage oDM,
            OFunctionParms params, OUser loggedUser) {
        logger.debug("Entering");
        if (function == null) {
            logger.warn("Null Function");
            return new OFunctionResult();
        }
        if (function.isServerJob()) {
            logger.debug("Returning");
            return runServerJob(function, oDM, params, loggedUser);
        }
        if (function.getClass() == ProcessFunction.class) {
            logger.debug("Returning");
            return runProcessFunction(function, oDM, params, loggedUser);
        }
        if (function.getClass() == JavaFunction.class) {
            logger.debug("Returning");
            return runJavaFunction(function, oDM, params, loggedUser);
        }
        if (function.getClass() == ValidationJavaFunction.class) {
            logger.debug("Returning");
            return runJavaFunction(function, oDM, params, loggedUser);
        }
        if (function.getClass() == OrgChartFunction.class) {
            logger.debug("Returning");
            return runJavaFunction(function, oDM, params, loggedUser);
        }
        if (function.getClass() == FieldExpParserFunction.class) {
            logger.debug("Returning");
            return runJavaFunction(function, oDM, params, loggedUser);
        }
        if (function.getClass() == URLFunction.class) {
            logger.debug("Returning");
            return runURLFunction(function, oDM, params, loggedUser);
        }
        if (function.getClass() == ORuleFunction.class) {
            logger.debug("Returning");
            return runRuleFunction(function, oDM, params, loggedUser);
        } else {
            String message = "Run Function doesn't support "
                    + function.getClass().getSimpleName() + " yet";
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.trace("Function: {}, Data Message: {}, Function Parameters: {}", function, oDM, params);
            // </editor-fold>
            logger.debug("Returning");
            return constructOFunctionResultFromError("SystemInternalError", loggedUser);
        }
    }

    @Override
    public List<OFunction> getMenuFunctions(String menuID, OUser loggedUser) {
        logger.debug("Entering");
        List<OFunction> retFunctions = new ArrayList<OFunction>();
        try {
            List<OMenuFunction> OMenuFunctions = null;
            List<BaseEntity> tempFunctions = new ArrayList<BaseEntity>();

            oem.getEM(loggedUser);

            OMenuFunctions = oem.loadEntityList("OMenuFunction",
                    Collections.singletonList("omenu.dbid=" + menuID),
                    /*fieldExpressions*/ null, Collections.singletonList("sortIndex"),
                    oem.getSystemUser(loggedUser));
            for (OMenuFunction menuFunction : OMenuFunctions) {
                OFunction function = menuFunction.getOfunction();
                if (null == function) {
                    // Probably happens when function is found in OFunction table
                    // but not found in its related descrimination table (e.g. ScreenFunction)
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.warn("ofunction of MenuFunction Object Is Null. Menu DBID: {}, MenuFunction Object: {}", menuID, menuFunction);
                    // </editor-fold>
                    continue;
                }
                tempFunctions.add(function);
            }
            // Commented because it is already called in
            tempFunctions = oem.callPostLoadForQueryLoadedList(tempFunctions, null, null, loggedUser);
            for (BaseEntity baseEntity : tempFunctions) {
                retFunctions.add((OFunction) baseEntity);
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.trace("Menu ID: {}", menuID);
            // </editor-fold>
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return retFunctions;
        }
    }

    @Override
    public List<OMenu> getMenuSubMenus(String menuID, OUser loggedUser) {
        logger.debug("Entering");
        List<OMenu> retMenus = null;
        try {
            retMenus = (List<OMenu>) oem.loadEntityList("OMenu",
                    Collections.singletonList("parentMenu.dbid=" + menuID),
                    null, Collections.singletonList("sortIndex"),
                    oem.getSystemUser(loggedUser));
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.trace("Menu ID: {}", menuID);
            // </editor-fold>
        }
        if (retMenus != null) {
            logger.debug("Returning with retMenus of size: {}", retMenus.size());
        }
        return retMenus;
    }

    @Override
    public List<UserMenu> getUserMenus(OUser loggedUser) {
        logger.debug("Entering");
        List tmpmenus = new ArrayList();
        List<UserMenu> userMenus = null;
        List<UserMenu> returnedUserMenus = new ArrayList<UserMenu>();
        try {
            userMenus = oem.
                    createListNamedQuery("getUserMenus", loggedUser, "userDBID", loggedUser.getDbid());
            if (null == userMenus || userMenus.isEmpty()) {
                throw new Exception();
            }

            for (UserMenu userMenu : userMenus) {
                tmpmenus.add(userMenu);
//                oem.callPostLoadForQueryLoadedList(tmpmenus,
//                        null, null, loggedUser);
            }

        } catch (Exception ex) {

            if (loggedUser != null) {
                FacesContext facesContext = FacesContext.getCurrentInstance();

                String errorPageLocation = "/userHaveNoRole.iface";
                facesContext.setViewRoot(facesContext.getApplication().getViewHandler().createView(facesContext, errorPageLocation));
                facesContext.getPartialViewContext().setRenderAll(true);
                facesContext.renderResponse();
            }

            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.trace("User: {}", loggedUser);
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        }

        returnedUserMenus.addAll(tmpmenus);
        if (returnedUserMenus != null) {
            logger.debug("Returning with returnedUserMenus of size: {} ", returnedUserMenus.size());
        }

        return returnedUserMenus;
    }

    @Override
    public List<OMenu> getUserMenuSubMenus(OUser user, OMenu parentMenu,
            int displayLevel, OUser loggedUser) {
        logger.debug("Entering");
        List<OMenu> userMenuSubMenus = new ArrayList<OMenu>();
        try {
            // Sign-on
            if (parentMenu == null) {
                userMenuSubMenus = getUserRootMenus(user);
            } else // Click
            {
                userMenuSubMenus = getUserMenuSubMenusNextLevel(user, parentMenu);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            if (null != user) {
                logger.trace("OUser: {}", user.getLoginName());
            }
        } finally {
            logger.debug("Returning");
            return userMenuSubMenus;
        }
    }

    @Override
    public List<Object> getUserMenuFunctions(long menuDBID, OUser loggedUser) {
        logger.debug("Entering");
        List<Object> dbids = new ArrayList<Object>();
        try {
            String query = "select functiondbid, name "
                    + " from menufunction where menudbid = " + menuDBID
                    + "  and langid = " + loggedUser.getFirstLanguage().getDbid();
            dbids = (List<Object>) oem.executeEntityListNativeQuery(query, loggedUser);
            logger.trace("Query: {}", query);

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("OUser:{}", loggedUser);
        } finally {
            if (dbids != null) {
                logger.debug("Returning with dbids of size: {}", dbids.size());
            }

            return dbids;
        }
    }

    @Override
    public String getFunctionsType(long functionDBID, OUser loggedUser) {
        logger.debug("Entering");
        String type = "";
        try {
            String query = "select distinct ftype"
                    + " from menufunction where functiondbid = " + functionDBID
                    + "  and langid = " + loggedUser.getFirstLanguage().getDbid();

            type = String.valueOf(oem.executeEntityNativeQuery(query, loggedUser));

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            logger.debug("Returning with: {}", type);
            return type;
        }
    }

    /**
     *
     * @param functionDBID
     * @param loggedUser
     * @return
     */
    @Override
    public Object getPortalPageData(long functionDBID, OUser loggedUser) {
        logger.debug("Entering");
        Object portalData = new Object();
        try {
            String query = "select portalPageDBID "
                    + " from PortalPageFunctionLite where functiondbid = " + functionDBID;
            portalData = oem.executeEntityListNativeQuery(query, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            logger.debug("Returning with: {}", portalData);
            return portalData;
        }
    }

    /**
     *
     * @param functionDBID
     * @param loggedUser
     * @return
     */
    @Override
    public String getFunctionName(long functionDBID, OUser loggedUser) {
//        List<MenuFunction> userFunctions = new ArrayList<MenuFunction>();
        logger.debug("Entering");
        String name = "";
        try {

            name = (String) oem.executeEntityNativeQuery("select name "
                    + " from ofunctiontranslation where entityDBID = " + functionDBID
                    + " and language_dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            //em.close();
            oem.closeEM(loggedUser);
            logger.debug("Returning with: {}", name);
            return name;
        }
    }

    @Override
    public List<OMenuFunction> getUserMenuFunctions(OUser user, OMenu parentMenu,
            int displayLevel, OUser loggedUser) {
        logger.debug("Entering");
        List<OMenuFunction> userUndeletedFunctions = new ArrayList<OMenuFunction>();
        List<OMenuFunction> userFunctions;// = new ArrayList<OMenuFunction>();
        try {
            oem.getEM(loggedUser);
            userFunctions = (List<OMenuFunction>) parentMenu.getMenuFunction();
            for (OMenuFunction oMenuFunction : userFunctions) {
                if (oMenuFunction.getOfunction() == null) {
                    continue;
                }
                if (!oMenuFunction.getOfunction().isInActive() && !oMenuFunction.isInActive()) {
                    oMenuFunction = (OMenuFunction) translationServiceRemote.loadEntityTranslation(oMenuFunction, loggedUser);
                    userUndeletedFunctions.add(oMenuFunction);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            if (null != user) {
                logger.trace("OUser: {}", user.getLoginName());
            }
        } finally {
            oem.closeEM(user);
            if (userUndeletedFunctions != null) {
                logger.debug("Returning with userUndeletedFunctions of size: {}", userUndeletedFunctions.size());
            }

            return userUndeletedFunctions;
        }
    }

    public List<OMenu> getUserRootMenus(OUser user) {
        logger.debug("Entering");
        List tmpmenus = new ArrayList();
        List<UserMenu> userMenus;// = null;
        try {
            userMenus = oem.
                    createListNamedQuery("getUserRootMenus", oem.getSystemUser(user), "userDBID", user.getDbid());

            for (UserMenu userMenu : userMenus) {
                OMenu toBeDisplayedMenu = (OMenu) oem.loadEntity("OMenu",
                        Collections.singletonList("hierCode='" + userMenu.getHierCode() + "'"), null,
                        oem.getSystemUser(user));
                tmpmenus.add(toBeDisplayedMenu);
                oem.callPostLoadForQueryLoadedList(tmpmenus,
                        null, null, user);
            }

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.trace("User: {}", user);
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        }
        if (tmpmenus != null) {
            logger.debug("Returning with tmpmenus of size: {}", tmpmenus.size());
        }
        return tmpmenus;
    }

    public List<OMenu> getUserMenuSubMenusNextLevel(OUser user, OMenu parentMenu) {
        logger.debug("Entering");
        List<OMenu> returnedUserMenus = new ArrayList<OMenu>();
        try {
            returnedUserMenus = oem.loadEntityList("OMenu", Collections.singletonList("hierCode LIKE '"
                    + parentMenu.getHierCode() + "___'"), null, null, oem.getSystemUser(user));
            Iterator<OMenu> iterator = returnedUserMenus.iterator();
            for (; iterator.hasNext();) {
                if (iterator.next().getHierCode().equals(parentMenu.getHierCode())) {
                    iterator.remove();
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("User: {}", user);
        } finally {
            if (returnedUserMenus != null) {
                logger.debug("Returning with returnedUserMenus: {}", returnedUserMenus.size());
            }

            return returnedUserMenus;
        }
    }

    public List<UserMenu> getUserMenuSubMenus(OUser user, OMenu parentMenu) {
        logger.debug("Entering");
        List<UserMenu> returnedUserMenus = new ArrayList<UserMenu>();
        List tmpmenus = new ArrayList();
        List<UserMenu> userMenus;// = null;
        try {
            String getSubMenus = " SELECT    userMenu "
                    + " FROM      UserMenu userMenu "
                    + " WHERE     userMenu.user_DBID = " + user.getDbid()
                    + " AND       userMenu.oMenu.hierCode LIKE '" + parentMenu.getHierCode() + "%'";

            userMenus = oem.createViewListQuery(getSubMenus, oem.getSystemUser(user));
            if (userMenus != null) {
                for (UserMenu userMenu : userMenus) {
                    // Skip parentMenu, which is loaded by the query
                    if (userMenu.getHierCode().equals(parentMenu.getHierCode())) {
                        continue;
                    }
                    tmpmenus.add(userMenu);
                    oem.callPostLoadForQueryLoadedList(tmpmenus,
                            null, null, user);
                }
            }

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.trace("User: {}", user);
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        }
        for (Object tempMenu : tmpmenus) {
            returnedUserMenus.add((UserMenu) tempMenu);
        }
        if (returnedUserMenus != null) {
            logger.debug("Returning with returnedUserMenus", returnedUserMenus.size());
        }
        return returnedUserMenus;
    }

    private String generateMenuCode(String parentCode, OUser loggedUser) {
        logger.debug("Entering");
        String newCode = null;
        List<OMenu> menusList = new ArrayList<OMenu>();
        List<OMenu> childrenList = new ArrayList<OMenu>();
        List<Integer> menusCodes = new ArrayList<Integer>();
        try {
            if (parentCode != null) {
                // Get children list
                childrenList = oem.loadEntityList("OMenu", Collections.singletonList(
                        "hierCode LIKE '" + parentCode + "%'"), null, null, loggedUser);

                for (OMenu menu : childrenList) {
                    // Skip parent
                    if (menu.getHierCode().equals(parentCode)) {
                        continue;
                    }
                    // Skip non-direct children
                    if (menu.getHierCode().length() != parentCode.length() + 3) {
                        continue;
                    }
                    menusCodes.add(Integer.valueOf(menu.getHierCode()));
                }

                // If no children in new parent, create first one
                if (menusCodes.isEmpty()) {
                    newCode = parentCode.concat("001");
                } // There are children, add menu to them
                else {
                    int maxCode = 0;
                    for (Integer menuCode : menusCodes) {
                        if (menuCode > maxCode) {
                            maxCode = menuCode;
                        }
                    }
                    newCode = String.valueOf(maxCode + 1);
                }
            } else {
                // Root menu
                // 3 '_'s means exactly 3 characters
                menusList = oem.loadEntityList("OMenu", Collections.singletonList(
                        "hierCode LIKE '___'"), null, null, loggedUser);

                if (menusList.isEmpty()) {
                    newCode = "001";
                } else {
                    for (OMenu menu : menusList) {
                        menusCodes.add(Integer.valueOf(menu.getHierCode()));
                    }

                    int maxCode = 0;
                    for (Integer menuCode : menusCodes) {
                        if (menuCode > maxCode) {
                            maxCode = menuCode;
                        }
                    }
                    newCode = String.valueOf(maxCode + 1);
                }
            }

            // Code must be a multiple of 3 chars
            while ((newCode.length() % 3) != 0) {
                newCode = "0".concat(newCode);
            }
            return newCode;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("OMenu: {}", parentCode);
            logger.debug("Returning with newcode: {}", newCode);
            return newCode;
        }
    }

    @Override
    public OFunctionResult validateCreateOMenu(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        String generatedCode = null;
        try {
            oem.getEM(loggedUser);

            // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
            OFunctionResult inputValidationErrors = validateValidationJavaFunctionODM(
                    odm, loggedUser, OMenu.class);
            if (inputValidationErrors != null) {
                logger.debug("Returning");
                return inputValidationErrors;
            }
            // </editor-fold>

            OMenu newMenu = (OMenu) odm.getData().get(1);
            OMenu parentMenu = newMenu.getParentMenu();
            if (parentMenu == null) {
                generatedCode = generateMenuCode(null, loggedUser);
            } else {
                generatedCode = generateMenuCode(parentMenu.getHierCode(), loggedUser);
            }

            // To apply changes to the entity, after returning from this validation function
            ODataType menuDataType = dataTypeService.loadODataType("OMenu", loggedUser);
            newMenu.setHierCode(generatedCode);
            newMenu.setChanged(true);
            List newData = new ArrayList();
            newData.add(newMenu);
            newData.add(loggedUser);
            odm.setData(newData);
            odm.setODataType(menuDataType);
            oFR.setReturnedDataMessage(odm);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            if (odm != null && odm.getData() != null && odm.getData().size() != 0) {
                logger.trace("OMenu: {}", (OMenu) odm.getData().get(0));
            }
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult validateUpdateOMenu(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            oem.getEM(loggedUser);

            // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
            OFunctionResult inputValidationErrors = validateValidationJavaFunctionODM(
                    odm, loggedUser, OMenu.class);
            if (inputValidationErrors != null) {
                logger.debug("Returning");
                return inputValidationErrors;
            }
            // </editor-fold>

            OMenu updatedMenu = (OMenu) odm.getData().get(1);
            String updatedMenuCode = updatedMenu.getHierCode();
            // Menu has no code, Return
            if (updatedMenuCode == null) {
                oFR.addError(userMessageServiceRemote.getUserMessage("MenuWithNoHierCode",
                        Collections.singletonList(updatedMenu.getName()), loggedUser));
                logger.debug("Returning");
                return oFR;
            }

            OMenu changedParentMenu = updatedMenu.getParentMenu();
            OMenu originalMenu = (OMenu) oem.loadEntity("OMenu",
                    updatedMenu.getDbid(), null, null, loggedUser);

            // If updated parent and original parent are null, nothing is changed, return
            if (originalMenu.getParentMenu() == null && changedParentMenu == null) {
                logger.debug("Returning");
                return oFR;
            } //  updated parent and original parent are not null, compare DBIDs
            else if (originalMenu.getParentMenu() != null && changedParentMenu != null) {
                if (originalMenu.getParentMenu().getDbid() == changedParentMenu.getDbid()) {
                    logger.debug("Returning");
                    return oFR;
                }
            }

            String generatedParentCode = null;
            List<OMenu> childrenTree = new ArrayList<OMenu>();
            if (changedParentMenu == null) {
                generatedParentCode = generateMenuCode(null, loggedUser);
            } else {
                generatedParentCode = generateMenuCode(changedParentMenu.getHierCode(), loggedUser);
            }

            // Update menu
            updatedMenu.setHierCode(generatedParentCode);
            ODataType menuDataType = dataTypeService.loadODataType("OMenu", loggedUser);
            updatedMenu.setChanged(true);
            List newData = new ArrayList();
            newData.add(updatedMenu);
            newData.add(loggedUser);
            newData.add(odm.getData().get(2));
            odm.setData(newData);
            odm.setODataType(menuDataType);
            oFR.setReturnedDataMessage(odm);

            // Get children menus
            childrenTree = oem.loadEntityList("OMenu", Collections.singletonList(
                    "hierCode LIKE '" + updatedMenuCode + "%'"), null, null, loggedUser);

            for (OMenu child : childrenTree) {
                // Skips updatedMenu
                if (child.getHierCode().equals(updatedMenuCode)) {
                    continue;
                }
                String codeWithoutParent = child.getHierCode().substring(updatedMenuCode.length());
                String childNewCode = generatedParentCode.concat(codeWithoutParent);
                child.setHierCode(childNewCode);
                child.setChanged(true);
                // Update child menu
                // This will call validation again
                oFR.append(entitySetupService.callEntityUpdateAction(child, loggedUser));
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            if (odm != null && odm.getData() != null && odm.getData().size() != 0) {
                logger.trace("OMenu: {}", (OMenu) odm.getData().get(0));
            }
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult constructHierCodeForExistingMenus(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            oem.getEM(loggedUser);
            List<String> conds = new ArrayList<String>();
            conds.add("parentMenu is NULL");
            conds.add(OEntityManager.CONFCOND_GET_ACTIVEANDINACTIVE);
            List<OMenu> allMenus = (List<OMenu>) oem.loadEntityList("OMenu", conds, null, null, loggedUser);

            int counter = 0;
            String newCode = null;
            for (OMenu menu : allMenus) {
                newCode = String.valueOf(++counter);
                // Code must be a multiple of 3 chars
                while ((newCode.length() % 3) != 0) {
                    newCode = "0".concat(newCode);
                }
                menu.setHierCode(newCode);
                oem.saveEntity(menu, loggedUser);
                //entitySetupService.callEntityUpdateAction(menu, loggedUser);
                constructHierCodeForMenuTree(menu, loggedUser);
            }

        } catch (Exception ex) {
            if (ex instanceof FABSException) {
                oFR.addError(userMessageServiceRemote.getUserMessage("ModuleError", loggedUser));
            } else {
                oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
            }
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    private void constructHierCodeForMenuTree(OMenu menu, OUser loggedUser) {
        logger.trace("Entering");
        String newCode = null;
        try {
            oem.getEM(loggedUser);
            List<String> conds = new ArrayList<String>();
            conds.add("parentMenu.dbid = " + menu.getDbid());
            conds.add(OEntityManager.CONFCOND_GET_ACTIVEANDINACTIVE);
            //List<OMenu> childList = menu.getChildMenues();
            List<OMenu> childList = oem.loadEntityList("OMenu", conds, null, null, loggedUser);
            // if(childList.isEmpty())return;
            for (int i = 0; i < childList.size(); i++) {
                OMenu childMenu = childList.get(i);
                newCode = String.valueOf(i + 1);
                // Code must be a multiple of 3 chars
                while ((newCode.length() % 3) != 0) {
                    newCode = "0".concat(newCode);
                }
                childMenu.setHierCode(menu.getHierCode().concat(newCode));
                oem.saveEntity(childMenu, loggedUser);
                constructHierCodeForMenuTree(childMenu, loggedUser);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            if (null != menu) {
                logger.trace("menu: {}", menu.getName());
            }
        } finally {
            logger.debug("Returning");
        }
    }

    public OFunctionResult runProcessFunction(
            OFunction function, ODataMessage oDM, OFunctionParms params, OUser loggedUser) {
        logger.debug("Entering");
        if (function.getClass() != ProcessFunction.class) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            if (oDM != null) {
                logger.warn("runProcessFunction Should not be called for non process function. Function: {}, Data Message: {}, Function Parameters: {}", function, oDM.getData(), params);
            }

            // </editor-fold>
            logger.debug("Returning");
            return constructOFunctionResultFromError("SystemInternalError", loggedUser);
        }
        OFunctionResult processStartResult;
        try {
            if (oDM != null) {
                logger.trace("Running Process Function. Function: {}, Data Message: {}, Function Parameters: {}", function, oDM.getData(), params);
            }

            List<Object> data = oDM.getData();
            ProcessFunction processFunction = (ProcessFunction) function;
            processFunction.getOprocessAction();
            processStartResult = processServiceRemote.doProcessAction(
                    processFunction.getOprocessAction(), data, loggedUser);
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.trace("Fucntion: {}, Function Parameters: {}", function, params);
            // </editor-fold>
            logger.debug("Returning");
            return constructOFunctionResultFromError("SystemInternalError", ex, loggedUser);
        }
        logger.debug("Returning");
        return processStartResult;
    }

    public OFunctionResult runWebServiceFunction(
            OWebService webService, ODataMessage oDM, OFunctionParms params, OUser loggedUser) {
        logger.debug("Entering");
        logger.debug("Starting method runWebServiceFunction");
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        OFunctionResult oFR = new OFunctionResult();
        try {
            OFunctionParms functionParms = new OFunctionParms();
            HashMap map = new HashMap();
            map.put("WebService", webService);
            functionParms.setParams(map);

            //Run Pre Function
            if (webService.getPreFunction() != null) {
                oFR.append(runFunction(webService.getPreFunction(), oDM, functionParms, loggedUser));
            }

            try {
                String wsdlURL = webService.getEndPointAddress() + "?wsdl";

                URL oURL = new URL(wsdlURL);
                HttpURLConnection con = (HttpURLConnection) oURL.openConnection();
                con.setDoOutput(true);
                con.setDoInput(true);
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
                con.setRequestProperty("SOAPAction", webService.getNameSpace()
                        + "/" + webService.getOperationName());

                String soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
                        + "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" "
                        + "xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"> "
                        + " <soap:Body>   "
                        + " <" + webService.getOperationName() + " xmlns=\"" + webService.getNameSpace() + "\">     ";

                List<ODataTypeAttribute> dtAttrList = oem.loadEntityList(ODataTypeAttribute.class.getSimpleName(),
                        Collections.singletonList("oDataType.dbid = " + oDM.getODataType().getDbid()), null,
                        Collections.singletonList("seq"), loggedUser);

                if (dtAttrList.size() != oDM.getData().size()) {
                    logger.warn("The Data Type Attribute number is not equal to the number of Data ");
                    logger.debug("Returning");
                    return constructOFunctionResultFromError("SystemInternalError", loggedUser);
                }

                for (int i = 0; i < oDM.getData().size(); i++) {
                    soapMessage += "<" + dtAttrList.get(i).getName() + ">"
                            + oDM.getData().get(i).toString()
                            + "</" + dtAttrList.get(i).getName() + ">";
                }
//                soapMessage += " <wsdbid>" + String.valueOf(webService.getDbid()) + "</wsdbid>  "
//                        + " <userdbid>" + String.valueOf(loggedUser.getDbid()) + "</userdbid>  "
//                        + " <userloginname>" + String.valueOf(loggedUser.getLoginName()) + "</userloginname>  "
//                        + " <userpass>" + String.valueOf(loggedUser.getPassword()) + "</userpass>"
//                        + "  </" + webService.getOperationName() + "> "
//                        + "   </soap:Body>"
//                        + "</soap:Envelope>";

                FABSSetup fabsSetupCustName = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                        Collections.singletonList("setupKey='Customer Name'"), null, loggedUser);
                soapMessage += " <wsdbid>" + String.valueOf(webService.getDbid()) + "</wsdbid>  "
                        + " <userdbid>" + String.valueOf(loggedUser.getDbid()) + "</userdbid>  "
                        + " <userloginname>" + String.valueOf(loggedUser.getLoginName()) + "</userloginname>  "
                        + " <userpass>" + String.valueOf(dataEncryptorServiceLocal.getIvKey()) + "|" + String.valueOf(dataEncryptorServiceLocal.getSymmetricKey()) + "</userpass> ";  //loggedUser.getPassword();
                if (fabsSetupCustName != null) {
                    soapMessage += " <custname>" + fabsSetupCustName.getSvalue() + "</custname> ";
                }
                soapMessage += "  </" + webService.getOperationName() + "> "
                        + "   </soap:Body>"
                        + "</soap:Envelope>";

                OutputStream reqStream = con.getOutputStream();
                System.out.println("Before Invoking WB");
                reqStream.write(soapMessage.getBytes());
                System.out.println("After Invoking WB");
                InputStream resStream = con.getInputStream();

                byte[] byteBuf = new byte[1024];
                int len = resStream.read(byteBuf);
                String in = "";
                while (len > -1) {
                    in += new String(byteBuf, 0, len);
                    len = resStream.read(byteBuf);
                }
                if (webService.getReturnDT() != null) {
                    List data = new ArrayList();
                    for (int i = 0; i < webService.getReturnDT().getODataTypeAttribute().size(); i++) {
                        if (webService.getReturnDT().getODataTypeAttribute().get(i).isInActive()) {
                            continue;
                        }
                        data.add(in.substring(in.indexOf("<" + webService.getReturnDT().getODataTypeAttribute().get(i).getName() + ">")
                                + webService.getReturnDT().getODataTypeAttribute().get(i).getName().length() + 2, in.indexOf("</" + webService.getReturnDT().getODataTypeAttribute().get(i).getName() + ">")));
                    }
                    ODataMessage returnedDM = new ODataMessage(webService.getReturnDT(), data);
                    oFR.setReturnedDataMessage(returnedDM);
                }

                logger.trace("Web Service Response is : {}", in);

                oFR.addSuccess(userMessageServiceRemote.getUserMessage(
                        "WebserviceSuccess", loggedUser));

            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown", ex);
                logger.trace("Function Parameters: {}", params);
                // </editor-fold>
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "SystemInternalError", loggedUser), ex);
                logger.debug("Returning");
                return oFR;
            }

            if (webService.getPostFunction() != null) {
                oFR.append(runFunction(webService.getPostFunction(), oDM, null, loggedUser));
            }

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.trace("Function Parameters: {}", params);
            // </editor-fold>
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
            logger.debug("Returning");
            return oFR;
        } finally {
            logger.debug("WebService END");
        }
        oFR.addSuccess(userMessageServiceRemote.getUserMessage("WebServiceSuccess", loggedUser));
        logger.debug("Returning");
        return oFR;
    }

    private OFunctionResult runJavaFunction(
            OFunction function, ODataMessage oDM, OFunctionParms params, OUser loggedUser) {
        logger.trace("Entering");
        if (oDM != null && currentEntityObject != null && isUserExit()) {
            oDM.getData().add(currentEntityObject);
            setUserExit(false);
        }
        if (function.getClass() != JavaFunction.class
                && function.getClass() != ValidationJavaFunction.class
                && function.getClass() != OrgChartFunction.class
                && function.getClass() != FieldExpParserFunction.class) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            if (null != oDM) {
                logger.warn("runJavaFunction Should not be called for non Java function. Function: {}, Data Message: {}, Function Parameters: {}", function, oDM.getData(), params);
            }
            // </editor-fold>
            logger.trace("Returning");
            return constructOFunctionResultFromError("SystemInternalError", loggedUser);
        }

        OFunctionResult oFR = new OFunctionResult();
        try {
            if (oDM != null) {
                logger.trace("Running Java Function. Function: {}, Data Message: {}, Function Parameters: {}", function, oDM.getData(), params);
            }

            JavaFunction javaFunction = (JavaFunction) function;
            if (null != javaFunction.getBaseAlert() && javaFunction.isServerJob()) {
                logger.trace("Running Java Function for alert: {}", javaFunction.getBaseAlert());
                oDM.getData().add(javaFunction.getBaseAlert());

            }
            Object functionCls;
            InitialContext ic = new InitialContext();
            functionCls = ic.lookup("java:global/ofoq/" + javaFunction.getFunctionClassPath().trim());
            // Get the method
            Class[] parameterTypes = new Class[3];
            Object[] args = new Object[3];

            parameterTypes[0] = ODataMessage.class;
            args[0] = oDM;

            parameterTypes[1] = OFunctionParms.class;
            args[1] = params;

            parameterTypes[2] = OUser.class;
            args[2] = loggedUser;

            Method functionMethod
                    = functionCls.getClass().getMethod(javaFunction.getFunctionName().trim(), parameterTypes);

            // Invoke the method
            oFR = (OFunctionResult) functionMethod.invoke(functionCls, args);
            logger.trace("Returning {}", oFR.toString());
            return oFR;

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            if (oDM != null) {
                logger.trace("Function: {}, Data Message: {}, Function Parameters: {}", function, oDM.getData(), params);
            }

            // </editor-fold>
            logger.trace("Returning");
            return oFR;
        }
    }

    /**
     * Calls {@link #validateJavaFunctionODM(ODataMessage, OUser, boolean, Class[])
     * }
     * with allowNull = false
     */
    @Override
    public OFunctionResult validateJavaFunctionODM(
            ODataMessage odm, OUser loggedUser, Class... dataElementsClasses) {
        return validateJavaFunctionODM(odm, loggedUser, false, dataElementsClasses);
    }

    /**
     *
     * /**
     * Return null if the datamessage is valid <br>Validates the odm & its data
     * for nullability (if allowNull = false) <br>Validates the data size
     * <br>Validates that the data elements is instanceof 'dataElementsClasses'
     * IN ORDER
     *
     * @param odm DataMessage to be validated
     * @param loggedUser loggedUser
     * @param dataElementsClasses Should-Be Class of the data element in the
     * message IN ORDER
     * @return null: odm is valid Otherwise, {@link OFunctionResult} object with
     * the error message
     */
    @Override
    public OFunctionResult validateJavaFunctionODM(
            ODataMessage odm, OUser loggedUser, boolean allowNulls, Class... dataElementsClasses) {
        logger.debug("Entering");
        try {
            if (odm == null) //FIXME: Use more informative message
            {
                logger.debug("Returning");
                return constructOFunctionResultFromError("CorruptedData", loggedUser);
            }

            if (odm.getData() == null) //FIXME: Use more informative message
            {
                logger.debug("Returning");
                return constructOFunctionResultFromError("CorruptedData", loggedUser);
            }

            if (odm.getData().isEmpty()) //FIXME: Use more informative message
            {
                logger.debug("Returning");
                return constructOFunctionResultFromError("CorruptedData", loggedUser);
            }

            if (dataElementsClasses != null) {
                for (int dataElementIndex = 0; dataElementIndex < dataElementsClasses.length; dataElementIndex++) {
                    Object dataElement = odm.getData().get(dataElementIndex);
                    if (dataElement == null) {
                        if (allowNulls) {
                            continue;
                        }
                        logger.debug("Returning");
                        //FIXME: Use more informative message
                        return constructOFunctionResultFromError("CorruptedData", loggedUser);
                    }
                    if (!dataElementsClasses[dataElementIndex].isInstance(dataElement)) //FIXME: Use more informative message
                    {
                        logger.debug("Returning");
                        return constructOFunctionResultFromError("CorruptedData", loggedUser);
                    }
                }
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            if (null != odm) {
                logger.trace("ODM: {}, DataElementClasses: {}", odm.getData(), dataElementsClasses);
            }
            // </editor-fold>
            //FIXME: Use more informative message
            logger.debug("Returning");
            return constructOFunctionResultFromError("CorruptedData", ex, loggedUser);
        }
        logger.debug("Returning with Null");
        return null;   //It's valid
    }

    public OFunctionResult validateValidationJavaFunctionODM(
            ODataMessage odm, OUser loggedUser, Class... dataElementsClasses) {
        logger.debug("Entering");
        try {
            if (odm == null) {
                logger.warn("odm is null.");

                //FIXME: Use more informative message
                logger.debug("Returning");
                return constructOFunctionResultFromError("CorruptedData", loggedUser);
            }

            if (odm.getData() == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("odm data is null. ODM: {}", odm.getData());
                // </editor-fold>
                //FIXME: Use more informative message
                logger.debug("Returning");
                return constructOFunctionResultFromError("CorruptedData", loggedUser);
            }

            if (odm.getData().size() < 3) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("odm data size is not 3. ODM: {}", odm.getData());
                // </editor-fold>
                //FIXME: Use more informative message
                logger.debug("Returning");
                return constructOFunctionResultFromError("CorruptedData", loggedUser);
            }

            if (dataElementsClasses != null) {
                for (int dataElementIndex = 0; dataElementIndex < dataElementsClasses.length; dataElementIndex++) {
                    Object dataElement = odm.getData().get(dataElementIndex);
                    if (dataElement == null) {
                        // <editor-fold defaultstate="collapsed" desc="Log">
                        if (odm != null) {
                            logger.warn("null data element. ODM: {}, dataElementIndex: {}", odm.getData(), dataElementIndex);
                        }
                        // </editor-fold>
                        //FIXME: Use more informative message
                        logger.debug("Returning");
                        return constructOFunctionResultFromError("CorruptedData", loggedUser);
                    }

                    if (!dataElementsClasses[dataElementIndex].isInstance(odm.getData().get(1))) {
                        // <editor-fold defaultstate="collapsed" desc="Log">
                        if (odm != null) {
                            logger.warn("Element is not instance of data type. ODM: {}, element index: {}", odm.getData(), dataElementIndex);
                        }

                        // </editor-fold>
                        //FIXME: Use more informative message
                        logger.debug("Returning");
                        return constructOFunctionResultFromError("CorruptedData", loggedUser);
                    }
                }
                boolean invalid = true;
                String className = (String) odm.getData().get(0);
                Class classInstance = odm.getData().get(1).getClass();
                while (true) {
                    if (classInstance.getSuperclass() == null) {
                        break;
                    }
                    if (className.equals(classInstance.getSimpleName())) {
                        invalid = false;
                        break;
                    }
                    classInstance = classInstance.getSuperclass();
                }
                if (invalid) {
                    logger.debug("Returning");
                    return constructOFunctionResultFromError("CorruptedData", loggedUser);
                }
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            if (null != odm) {
                logger.trace("ODM: {}, DataElementClasses: {}", odm.getData(), dataElementsClasses);
            }
            // </editor-fold>
            //FIXME: Use more informative message
            logger.debug("Returning");
            return constructOFunctionResultFromError("CorruptedData", ex, loggedUser);
        }
        logger.debug("Returning with Null");
        return null;   //It's valid
    }

    public OFunctionResult constructOFunctionResultFromError(String errorUserMessageName, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult functionResult = new OFunctionResult();
        functionResult.addError(userMessageServiceRemote.getUserMessage(
                errorUserMessageName, loggedUser));
        logger.debug("Returning");
        return functionResult;
    }

    public OFunctionResult constructOFunctionResultFromError(String errorUserMessageName, Exception ex, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult functionResult = new OFunctionResult();
        UserMessage errorMsg = userMessageServiceRemote.getUserMessage(
                errorUserMessageName, loggedUser);
        functionResult.addError(errorMsg, ex);
        logger.debug("Returning");
        return functionResult;
    }

    private OFunctionResult runServerJob(OFunction function, ODataMessage oDM, OFunctionParms params, OUser loggedUser) {
        logger.trace("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            oem.getEM(loggedUser);
            // Create a new ServerJob
            ServerJob serverJob = new ServerJob();
            // set status to 'Started'
            UDC startedUDC = (UDC) oem.loadEntity("UDC", Collections.singletonList("code = 'JobStarted'"), null, loggedUser);
            if (startedUDC != null) {
                serverJob.setStatus(startedUDC);
            }
            serverJob.setLoggedUser(loggedUser);

            Calendar calendar = Calendar.getInstance();
            Timestamp currentTimestamp = new Timestamp(calendar.getTime().getTime());

            serverJob.setStartTime(currentTimestamp);
            serverJob.setFunctionName(function.getName());
            //oFR.append(entitySetupService.callEntityCreateAction(serverJob, loggedUser));
            //oFR.append(entitySetupService.callEntityUpdateAction(serverJob, loggedUser));
            serverJob = (ServerJob) oem.saveEntity(serverJob, loggedUser);
            serverJob.setCode(String.valueOf(serverJob.getDbid()));
            serverJob = (ServerJob) oem.saveEntity(serverJob, loggedUser);
            oFR.setReturnValues(Collections.singletonList(String.valueOf(serverJob.getDbid())));
            oFR.addSuccess(userMessageServiceRemote.getUserMessage("ServerJobStarted",
                    Collections.singletonList(serverJob.getCode()), loggedUser));
            if (function.getClass() != JavaFunction.class
                    && function.getClass() != ValidationJavaFunction.class
                    && function.getClass() != OrgChartFunction.class) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                if (null != oDM) {
                    logger.warn("runJavaFunction Should not be called for non Java function. Function: {}, Data Message: {}, Function Parameters: {}", function, oDM.getData(), params);
                }
                // </editor-fold>
                oem.closeEM(loggedUser);
                logger.trace("Returning");
                return constructOFunctionResultFromError("SystemInternalError", loggedUser);
            }

            // <editor-fold defaultstate="collapsed" desc="Log">
            if (null != oDM) {
                logger.trace("Running Server Job. Function: {}, Data Message: {}, Function Parameters: {}", function, oDM.getData(), params);
            }
            // </editor-fold>

            JavaFunction javaFunction = (JavaFunction) function;
            if (null != javaFunction.getBaseAlert()) {
                logger.trace("Running Java Function for alert: {}", javaFunction.getBaseAlert());
                oDM.getData().add(javaFunction.getBaseAlert());
            }
            Object javaFunctionClassObject;
            InitialContext ic = new InitialContext();
            javaFunctionClassObject = ic.lookup("java:global/ofoq/" + javaFunction.getFunctionClassPath().trim());
            // Get the method
            Class[] parameterTypes = new Class[3];
            Object[] args = new Object[3];

            parameterTypes[0] = ODataMessage.class;
            args[0] = oDM;

            parameterTypes[1] = OFunctionParms.class;
            args[1] = params;

            parameterTypes[2] = OUser.class;
            args[2] = loggedUser;

            Method functionMethod
                    = javaFunctionClassObject.getClass().getMethod(javaFunction.getFunctionName().trim(), parameterTypes);

            // Invoke the method
            JavaFunctionThread javaFunctionThread = new JavaFunctionThread(loggedUser,
                    entitySetupService,
                    serverJob,
                    functionMethod,
                    javaFunctionClassObject,
                    args, javaFunction.getNotificationType(), javaFunction.getOmodule());
            (new Thread(javaFunctionThread)).start();
            logger.trace("Returning");
            return oFR;

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            if (null != function) {
                logger.trace("Function: {}", function.getName());
            }
        } finally {
            oem.closeEM(loggedUser);
        }
        logger.trace("Returning");
        return oFR;
    }

    @Override
    public String getOFunctionImagePath(OFunction function, OUser loggedUser) {
        logger.debug("Entering");
        if (function == null) {
            logger.debug("Returning with empty string");
            return "";
        }
        String imagePath = "";
        try {
            oem.getEM(loggedUser);

            OImage functionImage = function.getFunctionImage();
            if (functionImage != null) {
                String imageName = functionImage.getName();
                imagePath = imageService.getImagePath(imageName, loggedUser);
                logger.debug("Returning with: {}", imagePath);
                return imagePath;
            }

            if (ScreenFunction.class.isInstance(function)) {
                imagePath = imageService.getImagePath("ScreenFunction.png", loggedUser);
            } else if (JavaFunction.class.isInstance(function)) {
                imagePath = imageService.getImagePath("JavaFunction.png", loggedUser);
            } else if (OMenuFunction.class.isInstance(function)) {
                imagePath = imageService.getImagePath("OMenuFunction.png", loggedUser);
            } else if (URLFunction.class.isInstance(function)) {
                imagePath = imageService.getImagePath("URLFunction.png", loggedUser);
            } else if (WebServiceFunction.class.isInstance(function)) {
                imagePath = imageService.getImagePath("WebServiceFunction.png", loggedUser);
            } else if (WizardFunction.class.isInstance(function)) {
                imagePath = imageService.getImagePath("WizardFunction.png", loggedUser);
            } else if (ProcessFunction.class.isInstance(function)) {
                imagePath = imageService.getImagePath("ProcessFunction.png", loggedUser);
            } else if (PortalPageFunction.class.isInstance(function)) {
                imagePath = imageService.getImagePath("PortalPageFunction.png", loggedUser);
            } else if (ReportFunction.class.isInstance(function)) {
                imagePath = imageService.getImagePath("ReportFunction.png", loggedUser);
            } else if (ORuleFunction.class.isInstance(function)) {
                imagePath = imageService.getImagePath("ORuleFunction.png", loggedUser);
            }
            logger.debug("Returning with: {}");
            return imagePath;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("OFunction: {}", function);
            logger.debug("Returning with empty string");
            return "";
        } finally {
            oem.closeEM(loggedUser);
        }
    }

    @Override
    public String getOMenuImagePath(OMenu menu, OUser loggedUser) {
        logger.debug("Entering");
        if (menu == null) {
            logger.debug("Returning with empty string");
            return "";
        }
        String imagePath = "";
        try {
            oem.getEM(loggedUser);
            OImage menuImage = menu.getMenuImage();
            if (menuImage != null) {
                String imageName = menuImage.getName();
                imagePath = imageService.getImagePath(imageName, loggedUser);
                logger.debug("Returning with: {}", imagePath);
                return imagePath;
            }
            imagePath = imageService.getImagePath("OMenuFunction.png", loggedUser);
            logger.debug("Returning with: {}", imagePath);
            return imagePath;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            if (menu != null) {
                logger.trace("OMenu DBID: {}", menu.getDbid());
            }

            logger.debug("Returning with empty string");
            return "";
        } finally {
            oem.closeEM(loggedUser);
        }
    }

    @Override
    public String getOMenuLargeImagePath(OMenu menu, OUser loggedUser) {
        logger.debug("Entering");
        if (menu == null) {
            logger.debug("Returning with empty string");
            return "";
        }
        String imagePath = "";
        try {

            OImage menuImage = menu.getLargeMenuImage();
            if (menuImage != null) {
                String imageName = menuImage.getName();
                imagePath = imageService.getImagePath(imageName, loggedUser);
                return imagePath;
            }
            logger.debug("Returning with empty string");
            return "";
        } catch (Exception ex) {

            logger.error("Exception thrown", ex);
            if (menu != null) {
                logger.trace("OMenu DBID: {}", menu.getDbid());
            }
            logger.debug("Returning with empty string");
            return "";
        } finally {
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Business Rules">
    private OFunctionResult runRuleFunction(OFunction function, ODataMessage oDM, OFunctionParms params, OUser loggedUser) {
        logger.trace("Entering");
        OFunctionResult oFR = new OFunctionResult();

        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        if (function.getClass() != ORuleFunction.class) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            if (null != oDM) {
                logger.warn("runRuleFunction Should not be called for non Rule function. Function: {}, Data Message: {}, Function Parameters: {}", function, oDM.getData(), params);
            }
            // </editor-fold>
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
            logger.trace("Returning");
            return oFR;
        }
        ORuleFunction ruleFunction = (ORuleFunction) function;
        if (oDM.getODataType().getDbid() != ruleFunction.getOdataType().getDbid()) {
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
            // <editor-fold defaultstate="collapsed" desc="Log">
            if (null != oDM) {
                logger.warn("Invalid ODataType for ODataMessage of ORuleFunction. Function: {}, Data Message: {}, Function Parameters: {}", function, oDM.getData(), params);
            }
            // </editor-fold>
        }
        // </editor-fold>

        try {
            // <editor-fold defaultstate="collapsed" desc="Log">
            if (null != oDM) {
                logger.trace("Running Rule Function. Function: {}, Data Message: {}, Function Parameters: {}", function, oDM.getData(), params);
            }
            // </editor-fold>
            KnowledgeBuilder builder = KnowledgeBuilderFactory.newKnowledgeBuilder();
            String url = "http://" + fabsSetup.loadKeySetup("RULE_ENGINE_IP", loggedUser).getSvalue() + ":"
                    + fabsSetup.loadKeySetup("RULE_ENGINE_PORT", loggedUser).getSvalue()
                    + fabsSetup.loadKeySetup("RULE_ENGINE_URL", loggedUser).getSvalue()
                    + "/" + ruleFunction.getPackageName()
                    + fabsSetup.loadKeySetup("RULE_ENGINE_LATEST", loggedUser).getSvalue();
            UrlResource urlResource = (UrlResource) ResourceFactory.newUrlResource(url);
            urlResource.setBasicAuthentication("enabled");
            urlResource.setUsername(fabsSetup.loadKeySetup("RULE_ENGINE_NAME", loggedUser).getSvalue());
            urlResource.setPassword(fabsSetup.loadKeySetup("RULE_ENGINE_PASS", loggedUser).getSvalue());
            builder.add(urlResource, ResourceType.PKG);
            if (builder != null && builder.hasErrors()) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("Authentication Failed. {}", builder.getErrors().toString());
                // </editor-fold>
            } else {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.trace("Authentication Success");
                // </editor-fold>
            }
            KnowledgeBase knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
            knowledgeBase.addKnowledgePackages(builder.getKnowledgePackages());
            StatefulKnowledgeSession session = knowledgeBase.newStatefulKnowledgeSession();
            BaseEntity entity = (BaseEntity) oDM.getData().get(0);
            //StatelessKnowledgeSession statelessSession = knowledgeBase.newStatelessKnowledgeSession();
            //statelessSession.execute(entity);
            session.insert(entity);
            OAgendaFilter filter = new OAgendaFilter(ruleFunction.getRuleName());
            int count = session.fireAllRules((AgendaFilter) filter);
            session.dispose();
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.trace("Rule Fired. Count: {}", count);
            // </editor-fold>
            oFR.addSuccess(userMessageServiceRemote.getUserMessage("RuleFired", loggedUser));
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.trace("Fucntion: {} ,Function Parameters: {} ", function, params);
            // </editor-fold>
        }
        logger.trace("Returning");
        return oFR;
    }

    //</editor-fold>
    private OFunctionResult runURLFunction(OFunction function, ODataMessage oDM, OFunctionParms params, OUser loggedUser) {
        logger.trace("Entering");
        OFunctionResult oFR = new OFunctionResult();
        String url = ((URLFunction) function).getUrl();
        if (!(url.startsWith("http://") || url.startsWith("https://"))) {
            if (!url.startsWith("/")) {
                url = "http://" + url;
            } else {
                HttpServletRequest origRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
                String realPath = origRequest.getRequestURL().toString();
                url = realPath.substring(0, realPath.lastIndexOf("/")) + url;
            }
        }
        RequestContext.getCurrentInstance().execute(
                "top.FABS.Portlet.addURLBrowserTab({tabURL:'" + url
                + "', selectTab:'true'});");
        oFR.addSuccess(userMessageServiceRemote.getUserMessage("URL is opened Successfully", loggedUser));
        logger.trace("Returning");
        return oFR;
    }

    @Override
    public OFunctionResult clearAllCaches(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        oFR.append(entitySetupService.clearOEntityCache(odm, functionParms, loggedUser));
        oFR.append(dDService.clearDDCache(odm, functionParms, loggedUser));
        oFR.append(oLogSetUpService.clearOLogCache(odm, functionParms, loggedUser));
        oFR.append(uIFrameworkService.clearScreenCache(odm, functionParms, loggedUser));
        oFR.append(dataTypeService.clearODataTypeCache(odm, functionParms, loggedUser));
        oFR.append(moduleService.clearOModuleCache(odm, functionParms, loggedUser));
        oFR.append(userMessageServiceRemote.clearUserMessageCache(odm, functionParms, loggedUser));
        oFR.append(fabsSetup.clearFABSSetupCache(odm, functionParms, loggedUser));
        return oFR;
    }

    @Override
    public BaseEntity getCurrentEntityObject() {
        return currentEntityObject;
    }

    @Override
    public void setCurrentEntityObject(BaseEntity currentEntityObject) {
        this.currentEntityObject = currentEntityObject;
    }

    @Override
    public boolean isUserExit() {
        return userExit;
    }

    @Override
    public void setUserExit(boolean userExit) {
        this.userExit = userExit;
    }
}
