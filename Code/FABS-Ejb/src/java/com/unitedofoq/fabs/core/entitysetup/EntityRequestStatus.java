/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitysetup;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;

/**
 *
 * @author nkhalil
 */
@Entity
@ParentEntity(fields={"oentityStatus"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class EntityRequestStatus extends BaseEntity {
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EntityPredefinedRequestStatus mappedStatus;
    @Translatable(translationField="descriptionTranslated")
    private String description;

    @Translation(originalField="description")
    @Transient
    private String descriptionTranslated;

    public String getDescriptionTranslatedDD(){
        return "EntityRequestStatus_description";
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    private String name;
    private boolean first;
    private boolean last;

    public boolean isFirst() {
        return first;
    }

    public void setFirst(boolean first) {
        this.first = first;
    }

    public boolean isLast() {
        return last;
    }

    public void setLast(boolean last) {
        this.last = last;
    }
    

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OEntity oentityStatus;
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private OEntityAction executedActionOnSet;

    public EntityPredefinedRequestStatus getMappedStatus() {
        return mappedStatus;
    }

    public void setMappedStatus(EntityPredefinedRequestStatus mappedStatus) {
        this.mappedStatus = mappedStatus;
    }


    public OEntityAction getExecutedActionOnSet() {
        return executedActionOnSet;
    }

    public void setExecutedActionOnSet(OEntityAction executedActionOnSet) {
        this.executedActionOnSet = executedActionOnSet;
    }

    public OEntity getOentityStatus() {
        return oentityStatus;
    }

    public void setOentityStatus(OEntity oentityStatus) {
        this.oentityStatus = oentityStatus;
    }
    

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
