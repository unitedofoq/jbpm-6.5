/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.portal;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;

import com.unitedofoq.fabs.core.uiframework.screen.FormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.uiframework.screen.TabularScreen;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ahussien
 */
@Stateless
@EJB(name="java:global/ofoq/com.unitedofoq.fabs.core.uiframework.portal.PortalServiceRemote",
    beanInterface=PortalServiceRemote.class)
public class PortalServiceBean implements PortalServiceRemote {
    final static Logger logger = LoggerFactory.getLogger(PortalServiceBean.class);
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method" or "Web Service > Add Operation")

    @EJB
    private OEntityManagerRemote oem;
    
    public List<OUserPortalPage> getUserPortalPages(String userLoginName) {
        logger.debug("Entering");
        List<String> conditions = new ArrayList<String>();
        conditions.add("user.loginName = '" + userLoginName + "'");

        List<String> fieldExpression = new ArrayList<String>();
        fieldExpression.add("userPortlets");

        List<OUserPortalPage> userPortalPages = new ArrayList<OUserPortalPage>();
        logger.error("Function Not Supported");
        for(OUserPortalPage userPortalPage : userPortalPages)
        {
            userPortalPage.setUser(null);
            for(OUserPortalPagePortlet userPortalPagePortlet : userPortalPage.getUserPortlets())
            {
                if(userPortalPagePortlet.getScreen() == null)
                    userPortalPagePortlet.setScreen(null);
                else
                {
                    OScreen screenTemp = null;
                    if(userPortalPagePortlet.getScreen() instanceof TabularScreen)
                        screenTemp = new TabularScreen();
                    else
                        if(userPortalPagePortlet.getScreen() instanceof FormScreen)
                            screenTemp = new FormScreen();
                        else
                            screenTemp = new OScreen();

                    screenTemp.setViewPage(userPortalPagePortlet.getScreen().getViewPage());
                    screenTemp.setName(userPortalPagePortlet.getScreen().getName());
                    screenTemp.setHeader(userPortalPagePortlet.getScreen().getHeaderTranslated());

                    userPortalPagePortlet.setScreen(screenTemp);
                }
            }
        }
logger.debug("Returning");
        return userPortalPages;
    }

    public void updateUserPortalPageID(long userPortalPageId, long layoutPlid) {
        logger.debug("Entering");
//Until it's fixed
        try {
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        }
        finally{
        logger.debug("Returning");
    }
        
    }
    
}
