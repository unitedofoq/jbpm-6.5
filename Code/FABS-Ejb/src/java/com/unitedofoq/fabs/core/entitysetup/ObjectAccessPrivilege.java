/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.entitysetup;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.security.privilege.OPrivilege;

/**
 *
 * @author ahussien
 */
@Entity
@DiscriminatorValue("OBJECTACCESS")
@ParentEntity(fields={"accessedOEntity"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class ObjectAccessPrivilege extends OPrivilege 
{
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name="OObject_DBID", nullable=false)
    private OEntity accessedOEntity;

    public OEntity getAccessedOEntity() {
        return accessedOEntity;
    }

    public void setAccessedOEntity(OEntity accessedOEntity) {
        this.accessedOEntity = accessedOEntity;
    }
}
