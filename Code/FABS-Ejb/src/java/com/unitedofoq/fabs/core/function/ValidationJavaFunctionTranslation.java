package com.unitedofoq.fabs.core.function;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import com.unitedofoq.fabs.core.uiframework.navigationmenu.NMEntityListener;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Entity
@Table(name = "ValidationJavaFunctioni18n")
@EntityListeners(value={NMEntityListener.class})
public class ValidationJavaFunctionTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="name">
    @Column
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    // </editor-fold>
}
