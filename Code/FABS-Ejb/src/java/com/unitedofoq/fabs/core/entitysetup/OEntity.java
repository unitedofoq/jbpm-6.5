package com.unitedofoq.fabs.core.entitysetup;

import com.unitedofoq.fabs.core.audittrail.persistant.entity.AuditTrailTransaction;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.function.JavaFunction;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.udc.UDCType;

/**
 * Holds the specifications of an entity, acts as Data Dictionary for the
 * entities.
 *
 * @author arezk
 */
@Entity
@ChildEntity(fields = {"oActions", "oObjectBriefInfoField", "entityRequestStatus",
    "entityHatFields", "accessPrivilege", "oentityReferences", "auditTrailTransaction",
"oentityFields"})
@VersionControlSpecs(omoduleFieldExpression = "omodule")
@FABSEntitySpecs(indicatorFieldExp = "title")
public class OEntity extends ObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="inlineHelp">

    private String inlineHelp;

    public String getInlineHelpDD() {
        return "OEntity_inlineHelp";
    }

    public String getInlineHelp() {
        return inlineHelp;
    }

    public void setInlineHelp(String inlineHelp) {
        this.inlineHelp = inlineHelp;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="requestable">
    private boolean requestable;

    public boolean isRequestable() {
        return requestable;
    }

    public void setRequestable(boolean requestable) {
        this.requestable = requestable;
    }

    public String getRequestableDD() {
        return "OEntity_requestable";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="oentityStatus">
    @OneToMany(mappedBy = "oentityStatus")
    private List<EntityRequestStatus> entityRequestStatus = null;

    public List<EntityRequestStatus> getEntityRequestStatus() {
        return entityRequestStatus;
    }

    public void setEntityRequestStatus(List<EntityRequestStatus> entityRequestStatus) {
        this.entityRequestStatus = entityRequestStatus;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="oObjectBriefInfoField">
    @OneToMany(mappedBy = "oObject")
    private List<OObjectBriefInfoField> oObjectBriefInfoField;
    @OneToMany(mappedBy = "entity")
    private List<OEntityHatField> entityHatFields;

    public List<OEntityHatField> getEntityHatFields() {
        return entityHatFields;
    }

    public void setEntityHatFields(List<OEntityHatField> entityHatFields) {
        this.entityHatFields = entityHatFields;
    }

    public List<OObjectBriefInfoField> getOObjectBriefInfoField() {
        return oObjectBriefInfoField;
    }

    public void setOObjectBriefInfoField(List<OObjectBriefInfoField> oObjectBriefInfoField) {
        this.oObjectBriefInfoField = oObjectBriefInfoField;
    }

    public String getOObjectBriefInfoFieldDD() {
        return "Object_oObjectBriedInfoField";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entityClassPath">
    //Package path
    @Column(nullable = false, unique = true)
    protected String entityClassPath;

    public String getEntityClassPath() {
        return entityClassPath;
    }

    public void setEntityClassPath(String entityClassPath) {
        this.entityClassPath = entityClassPath;
    }

    public String getEntityClassPathDD() {
        return "OEntity_entityClassPath";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="title">
    //Title that gets inherited by all entities (i.e. OObject, OBusinessObject) to be used in the Balloon
    @Translatable(translationField = "titleTranslated")
    protected String title;
    @Translation(originalField = "title")
    @Transient
    protected String titleTranslated;

    public String getTitleTranslated() {
        return titleTranslated;
    }

    public void setTitleTranslated(String titleTranslated) {
        this.titleTranslated = titleTranslated;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleTranslatedDD() {
        return "OEntity_title";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="oActions">
    @OneToMany(mappedBy = "oownerEntity")
    protected List<OEntityAction> oActions = null;

    public List<OEntityAction> getOActions() {
        return oActions;
    }

    public void setOActions(List<OEntityAction> oActions) {
        this.oActions = oActions;
    }

    public String getOActionsDD() {
        return "OEntity_oActions";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="EntityAttributeInit">
    @OneToMany(mappedBy = "ownerEntity")
    protected List<EntityAttributeInit> entityAttrInit = null;

    public List<EntityAttributeInit> getEntityAttrInit() {
        return entityAttrInit;
    }

    public String getEntityAttrInitDD() {
        return "OEntity_entityAttrInit";
    }

    public void setEntityAttrInit(List<EntityAttributeInit> entityAttrInit) {
        this.entityAttrInit = entityAttrInit;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Entity Actions Functions">
    /**
     * Get the Entity Action given its type from oActions field.
     * <br> It doesn't load the action from database, it just searches for it in
     * the oActions field
     *
     * @param actionTypeDBID One of the ATDBID_xyz member fields
     * @return action: if found
     * <br>null: either there is no corresponding action, or it's not loaded
     * from database for any reason (security, LAZY, etc...)
     */
    public OEntityAction getAction(Long actionTypeDBID) {
        if (oActions == null) {
            return null;
        }
        for (OEntityAction action : oActions) {
            UDC actionType = action.getActionType();
            if (actionType != null) // Note it's not mandatory, could be null
            {
                if (actionType.getDbid() == actionTypeDBID) {
                    return action;
                }
            }
        }
        return null;
    }

    /**
     * Calls {@link #getAction(Long)} using "ATDBID_CREATE"
     */
    public OEntityAction getCreateAction() {
        return getAction(OEntityAction.ATDBID_CREATE);
    }

    /**
     * Calls {@link #getAction(Long)} using "ATDBID_RETRIEVE"
     */
    public OEntityAction getRetrieveAction() {
        return getAction(OEntityAction.ATDBID_RETRIEVE);
    }

    /**
     * Calls {@link #getAction(Long)} using "ATDBID_UPDATE"
     */
    public OEntityAction getUpdateAction() {
        return getAction(OEntityAction.ATDBID_UPDATE);
    }

    /**
     * Calls {@link #getAction(Long)} using "ATDBID_DELETE"
     */
    public OEntityAction getDeleteAction() {
        return getAction(OEntityAction.ATDBID_DELETE);
    }

    /**
     * Calls {@link #getAction(Long)} using "ATDBID_DUPLICATE"
     */
    public OEntityAction getDuplicateAction() {
        return getAction(OEntityAction.ATDBID_DUPLICATE);
    }

    public OEntityAction getParenPreSaveAction() {
        return getAction(OEntityAction.ATDBID_ParentPreSave);
    }

    public OEntityAction getActivationAction() {
        return getAction(OEntityAction.ATDBID_ACTIVATION);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Category Code Types">
    //UDC types for this entity
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    protected UDCType ccType1 = null;

    public UDCType getCcType1() {
        return ccType1;
    }

    public void setCcType1(UDCType ccType1) {
        this.ccType1 = ccType1;
    }

    public String getCcType1DD() {
        return "OEntity_ccType1";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    protected UDCType ccType2 = null;

    public UDCType getCcType2() {
        return ccType2;
    }

    public void setCcType2(UDCType ccType2) {
        this.ccType2 = ccType2;
    }

    public String getCcType2DD() {
        return "OEntity_ccType2";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    protected UDCType ccType3 = null;

    public UDCType getCcType3() {
        return ccType3;
    }

    public void setCcType3(UDCType ccType3) {
        this.ccType3 = ccType3;
    }

    public String getCcType3DD() {
        return "OEntity_ccType3";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    protected UDCType ccType4 = null;

    public UDCType getCcType4() {
        return ccType4;
    }

    public void setCcType4(UDCType ccType4) {
        this.ccType4 = ccType4;
    }

    public String getCcType4DD() {
        return "OEntity_ccType4";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    protected UDCType ccType5 = null;

    public UDCType getCcType5() {
        return ccType5;
    }

    public void setCcType5(UDCType ccType5) {
        this.ccType5 = ccType5;
    }

    public String getCcType5DD() {
        return "OEntity_ccType5";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="configuration">
    protected boolean configuration;

    public boolean isConfiguration() {
        return configuration;
    }

    public void setConfiguration(boolean configuration) {
        this.configuration = configuration;
    }

    public String getConfigurationDD() {
        return "OEntity_configuration";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Referenced Entity Functions">
    /**
     * Get the Class "unqualified" name of the Referenced Entity
     */
    public String getEntityClassName() {
        return getEntityClassPath().substring(
                getEntityClassPath().lastIndexOf(".") + 1,
                getEntityClassPath().length());
    }
    /**
     * Filled by {@link #getParentClassPath(java.lang.String)}
     */
    @Transient
    ArrayList<String> entityParentClassPath = null;

    /**
     * Returns {@link BaseEntity#getParentClassPath(java.lang.String)}, caching
     * it in {@link #entityParentClassPath}
     */
    public List getEntityParentClassPath() {
        if (entityParentClassPath != null) {
            return entityParentClassPath;
        }
        entityParentClassPath = (ArrayList<String>) BaseEntity.getParentClassPath(getEntityClassPath());
        return entityParentClassPath;
    }
    /**
     * Filled by {@link #getEntityParentFieldName()}
     */
    @Transient
    ArrayList<String> entityParentFieldName = null;

    /**
     * Return null if
     * {@link BaseEntity#getParentEntityFieldName(java.lang.String)} returned
     * null or empty values; otherwise, returns the values, caching it in
     * {@link #entityParentFieldName}
     */
    public List getEntityParentFieldName() {
        if (entityParentFieldName != null) {
            return entityParentFieldName;
        }
        List<String> entityParentFieldNames = BaseEntity.getParentEntityFieldName(getEntityClassPath());
        if (entityParentFieldNames != null && !entityParentFieldNames.isEmpty()) {
            entityParentFieldName = (ArrayList<String>) entityParentFieldNames;
        }
        return entityParentFieldName;
    }

    public List<String> getEntityChildClassPath() {
        return BaseEntity.getChildEntityClassPath(getEntityClassPath());
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="omodule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private OModule omodule;

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }

    public String getOmoduleDD() {
        return "OEntity_omodule";
    }
    // </editor-fold>

    @Override
    public String getEntityDiscrminatorValue() {
        if (getEntityClassName().equals("OEntity")) {
            return "OBJ";  // = OObject Class Dicrminator Value
        }
        return super.getEntityDiscrminatorValue();
    }

    public String getEntityTypeDD() {
        return "OEntity_entityType";
    }
    // <editor-fold defaultstate="collapsed" desc="oentityReferences">
    @OneToMany(mappedBy = "ownerOEntity")
    private List<OEntityReference> oentityReferences;

    public List<OEntityReference> getOentityReferences() {
        return oentityReferences;
    }

    public void setOentityReferences(List<OEntityReference> oentityReferences) {
        this.oentityReferences = oentityReferences;
    }

    public String getOentityReferencesDD() {
        return "OEntity_oentityReferences";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="accessPrivilege">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, mappedBy = "accessedOEntity")
    private ObjectAccessPrivilege accessPrivilege;

    public String getAccessPrivilegeDD() {
        return "OEntity_accessPrivilege";
    }

    public ObjectAccessPrivilege getAccessPrivilege() {
        return accessPrivilege;
    }

    public void setAccessPrivilege(ObjectAccessPrivilege accessPrivilege) {
        this.accessPrivilege = accessPrivilege;
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="AuditTrail Auditable">
    private boolean auditable;

    public boolean isAuditable() {
        return auditable;
    }

    public void setAuditable(boolean auditable) {
        this.auditable = auditable;
    }

    public String getAuditableDD() {
        return "OEntity_auditable";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="AuditTrail Transaction">
    @OneToMany(mappedBy = "oEntity")
    List<AuditTrailTransaction> auditTrailTransaction;

    public List<AuditTrailTransaction> getAuditTrailTransaction() {
        return auditTrailTransaction;
    }

    public void setAuditTrailTransaction(List<AuditTrailTransaction> auditTrailTransaction) {
        this.auditTrailTransaction = auditTrailTransaction;
    }

    public String getAuditTrailTransactionDD() {
        return "OEntity_auditTrailTransaction";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="AuditTrail System Auditable">
    private boolean systemAuditable;

    public boolean isSystemAuditable() {
        return systemAuditable;
    }

    public void setSystemAuditable(boolean systemAuditable) {
        this.systemAuditable = systemAuditable;
    }

    public String getSystemAuditableDD() {
        return "OEntity_systemAuditable";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="oEntityField">
    @OneToMany(mappedBy = "oentity")
    private List<OEntityField> oentityFields;

    public List<OEntityField> getOentityFields() {
        return oentityFields;
    }

    public void setOentityFields(List<OEntityField> oentityFields) {
        this.oentityFields = oentityFields;
    }
    public String getOentityFieldsDD() {
        return "OEntity_oentityFields";
    }
    
    @ManyToOne
     private JavaFunction dataLoadingUserExit ;

    /**
     * Getter of {@link #dataLoadingUserExit}
     * @return 
     */
    public JavaFunction getDataLoadingUserExit() {
        return dataLoadingUserExit;
    }

    /**
     * Setter of {@link #dataLoadingUserExit}
     */
    public void setDataLoadingUserExit(JavaFunction dataLoadingUserExit) {
        this.dataLoadingUserExit = dataLoadingUserExit;
    }

    public String getDataLoadingUserExitDD() {
        return "OEntity_dataLoadingUserExit";
    }
    
    
    //</editor-fold>
    private String uploadAttachDispCond;
    private String deleteAttachDispCond;

    public String getUploadAttachDispCond() {
        return uploadAttachDispCond;
    }

    public void setUploadAttachDispCond(String uploadAttachDispCond) {
        this.uploadAttachDispCond = uploadAttachDispCond;
    }
    
    public String getUploadAttachDispCondDD() {
        return "OEntity_uploadAttachDispCond";
    }

    public String getDeleteAttachDispCond() {
        return deleteAttachDispCond;
    }

    public void setDeleteAttachDispCond(String deleteAttachDispCond) {
        this.deleteAttachDispCond = deleteAttachDispCond;
    }

    public String getDeleteAttachDispCondDD() {
        return "OEntity_deleteAttachDispCond";
    }

}
