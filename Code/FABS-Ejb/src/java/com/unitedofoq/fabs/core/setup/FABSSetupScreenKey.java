/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.setup;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.uiframework.screen.FABSSetupScreen;

/**
 *
 * @author nkhalil
 */
@Entity
@ParentEntity(fields={"setupScreen"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class FABSSetupScreenKey extends BaseEntity {
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private FABSSetup fabsSetup;
  
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private  FABSSetupScreen setupScreen;
    
    public String getFabsSetupDD() {
        return "FABSSetupScreenKeys_fabsSetup";
    }

    public FABSSetup getFabsSetup() {
        return fabsSetup;
    }

    public void setFabsSetup(FABSSetup fabsSetup) {
        this.fabsSetup = fabsSetup;
    }

    public String getSetupScreenDD() {
        return "FABSSetupScreenKeys_setupScreen";
    }

    public FABSSetupScreen getSetupScreen() {
        return setupScreen;
    }

    public void setSetupScreen(FABSSetupScreen setupScreen) {
        this.setupScreen = setupScreen;
    }

}
