/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.function;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields={"functionGroup", "functionGroupScreen"})
public class FunctionGroupScreenGroup extends ObjectBaseEntity{
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private FunctionGroup functionGroup;

    public FunctionGroup getFunctionGroup() {
        return functionGroup;
    }

    public void setFunctionGroup(FunctionGroup functionGroup) {
        this.functionGroup = functionGroup;
    }
    
    public String getFunctionGroupDD() {
        return "FunctionGroupScreenGroup_functionGroup";
    }
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private FunctionGroupScreen functionGroupScreen;

    public FunctionGroupScreen getFunctionGroupScreen() {
        return functionGroupScreen;
    }

    public void setFunctionGroupScreen(FunctionGroupScreen FunctionGroupScreen) {
        this.functionGroupScreen = FunctionGroupScreen;
    }
    
    public String getFunctionGroupScreenDD() {
        return "FunctionGroup_functionGroupScreen";
    }

    private int sortIndex;

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }
    public String getSortIndexDD() {
        return "FunctionGroupScreenGroup_sortIndex";
    }
}
