/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.navigationmenu;

import java.util.Properties;

import org.primefaces.component.submenu.Submenu;

public class multiLangSubMenu extends Submenu {

    public final static int ARABIC_VAR = 24;
    public final static int ENGLISH_VAR = 23;
    private Properties langs = new Properties();
    private String secondLang;
    private String firstLang;

    public String getSecondLang() {
        return secondLang;
    }

    public void setSecondLang(String secondLang) {
        this.secondLang = secondLang;
    }

    public void setRendered(boolean rendered, String local) {
        if (local.equals(String.valueOf(ARABIC_VAR))) {
            if (secondLang != null) {
                if (!secondLang.equals("")) {
                    super.setLabel(secondLang);
                }
            }
        }else{
            super.setLabel(firstLang);
        }
        super.setRendered(rendered);
    }

    public Properties getLangs() {
        return langs;
    }

    public void setLangs(Properties langs) {
        this.langs = langs;
    }

    public String getFirstLang() {
        return firstLang;
    }

    public void setFirstLang(String firstLang) {
        this.firstLang = firstLang;
    }
}
