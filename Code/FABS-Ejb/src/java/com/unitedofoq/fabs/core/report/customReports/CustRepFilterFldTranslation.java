package com.unitedofoq.fabs.core.report.customReports;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Entity;

@Entity
public class CustRepFilterFldTranslation extends BaseEntityTranslation {

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
