/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.report.customReports;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import com.unitedofoq.fabs.core.uiframework.navigationmenu.NMEntityListener;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;

/**
 *
 * @author Melad
 */
@Entity
@EntityListeners(value = {NMEntityListener.class})
public class CustRepFunctionTranslation extends BaseEntityTranslation {

    //<editor-fold defaultstate="collapsed" desc="NameTranslated">
    @Column
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="DiscriptionTranslated">
    @Column
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    //</editor-fold>
}
