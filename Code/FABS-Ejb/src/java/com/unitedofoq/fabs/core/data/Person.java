/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.data;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;

/**
 *
 * @author arezk
 */
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name="PTYPE")
@ChildEntity(fields={"personPhoto"})
public class Person extends PersonBase {
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, mappedBy="person")
    private PersonPhoto personPhoto;

    public PersonPhoto getPersonPhoto() {
        return personPhoto;
    }

    public void setPersonPhoto(PersonPhoto personPhoto) {
        this.personPhoto = personPhoto;
    }

    public String getPersonPhotoDD(){
        return "Person_personPhoto";
    }
}
