/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.validation;

import com.unitedofoq.fabs.central.EntityManagerWrapper;
import java.util.ArrayList;
import java.util.List;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author bassem
 */
public class FABSException extends Exception
{
    final static Logger logger = LoggerFactory.getLogger(FABSException.class);
    // <editor-fold defaultstate="collapsed" desc="loggedUser">
    /**
     * Message of the exception
     */
    protected String message ;
    @Override
    public String getMessage() {
        return this.message ;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="loggedUser">
    /**
     * The currently logged user if available. 
     * <br> It could be the system user as well.
     * <br> It could be null for internal functions
     */
    protected OUser loggedUser ;
    /**
     * Get {@link  #loggedUser}
     */
    public OUser getLoggedUser() {
        return loggedUser;
    }
    /**
     * Set {@link  #loggedUser}
     */
    public void setLoggedUser(OUser loggedUser) {
        this.loggedUser = loggedUser;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="originatingJavaException">
    /**
     * The Original Java Exception caught upon which this more informative exception 
     * is created and thrown
     * <br> It could be null if no Java Originating Exception
     */
    protected Exception originatingJavaException ;
    /**
     * Get {@link #originatingJavaException}
     */
    public Exception getOriginatingJavaException() {
        return originatingJavaException;
    }
    /**
     * Set {@link #originatingJavaException}
     */
    public void setOriginatingJavaException(Exception originatingJavaException) {
        this.originatingJavaException = originatingJavaException;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="concernedEntity">
    /**
     * Main Entity for which this exception is thrown
     * <br> It could be null if exception is not concerned with entity
     */
    protected BaseEntity concernedEntity ;
    /**
     * Get {@link #concernedEntity}
     */
    public BaseEntity getConcernedEntity() {
        return concernedEntity;
    }
    /**
     * Set {@link #concernedEntity}
     */
    public void setConcernedEntity(BaseEntity concernedEntity) {
        this.concernedEntity = concernedEntity;
    }
    // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="relatedEntity">
    /**
     * Main Entity for which this exception is thrown
     * <br> It could be null if exception is related to reference entity
     */
    protected BaseEntity referenceEntity ;
    /**
     * Get {@link #referenceEntity}
     */
    public BaseEntity getReferenceEntity() {
        return referenceEntity;
    }
    /**
     * Set {@link #referenceEntity}
     */
    public void setReferenceEntity(BaseEntity referenceEntity) {
        this.referenceEntity = referenceEntity;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="relatedObjects">
    /**
     * List of minor objects for which this exception is thrown
     * <br> It could be null if exception is not concerned with object
     */
    protected List<Object> relatedObjects ;
    public List<Object>  getRelatedObjects() {
        if (relatedObjects == null)
            return new ArrayList<Object>() ;
        return relatedObjects;
    }
    public void setRelatedObjects(List<Object> customEntities) {
        this.relatedObjects = customEntities;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="errorUserMessagesInfo">
    /**
     * Error User messages information list
     * <br>It is a list of lists, the inner list is on the following format:
     * <UserMessage Name>, <UserMeesage Parameters>
     *
     * @see #errorUserMessages
     */
    protected List<List<String>> errorUserMessagesInfo;
    /**
     * Get {@link #errorUserMessagesInfo}
     */
    public List<List<String>> getErrorUserMessagesInfo() {
        return errorUserMessagesInfo;
    }
    /**
     * Set {@link #errorUserMessagesInfo}
     */
    public void setErrorUserMessagesInfo(List<List<String>> errorUserMessage) {
        this.errorUserMessagesInfo = errorUserMessage;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="errorUserMessage">
    /**
     * Error User messages list
     * @see #errorUserMessagesInfo
     */
    private List<UserMessage> errorUserMessages ;
    public List<UserMessage> getErrorUserMessages() {
        return errorUserMessages;
    }
    public void setErrorUserMessages(List<UserMessage> errorUserMessages) {
        this.errorUserMessages = errorUserMessages;
    }
    // </editor-fold>

    /**
     * All parameters are optional
     * <Br>Logs itself in constructor
     */
    public FABSException(OUser loggedUser, String message,
            BaseEntity concernedEntity,
            Exception originatingJavaException,
            List relatedObjects,
            List<List<String>> errorUserMessages)
    {
        this.loggedUser = loggedUser;
        this.originatingJavaException = originatingJavaException;
        this.concernedEntity = concernedEntity;
        this.relatedObjects = relatedObjects;
        this.errorUserMessagesInfo = errorUserMessages;
        this.message = message ;

        if (! (originatingJavaException instanceof FABSException))
            logger.error("Exception while FABSException:{}",this);
    }

    /**
     * All parameters are optional
     * <Br>Logs itself in constructor
     */
    public FABSException(OUser loggedUser, String message, 
            BaseEntity concernedEntity,
            Exception originatingJavaException, 
            List<Object> relatedObjects)
    {
        this.message = message;
        this.loggedUser = loggedUser;
        this.originatingJavaException = originatingJavaException;
        this.concernedEntity = concernedEntity;
        this.relatedObjects = relatedObjects;
        
        if (! (originatingJavaException instanceof FABSException))
            logger.error("Exception while FABSException:{}",this);
    }

    /**
     * All parameters are optional
     * <Br>Logs itself in constructor
     */
    public FABSException(OUser loggedUser, String message,
            BaseEntity concernedEntity,
            Exception originatingJavaException)
    {
        this.message = message;
        this.loggedUser = loggedUser;
        this.originatingJavaException = originatingJavaException;
        this.concernedEntity = concernedEntity;

        if (! (originatingJavaException instanceof FABSException))
            logger.debug("Exception while FABSException:{}",this);
    }

    /**
     * All parameters are optional
     * <Br>Logs itself in constructor
     */
    public FABSException(OUser loggedUser, String message,
            BaseEntity concernedEntity)
    {
        this.message = message;
        this.loggedUser = loggedUser;
        this.concernedEntity = concernedEntity;

        logger.debug("Exception while FABSException:{}",this);
    }
    
     /**
     * All parameters are optional
     * <Br>Logs itself in constructor
     */
    public FABSException(OUser loggedUser, String message,
            BaseEntity concernedEntity, BaseEntity referenceEntity)
    {
        this.message = message;
        this.loggedUser = loggedUser;
        this.concernedEntity = concernedEntity;
        this.referenceEntity = referenceEntity;

        logger.debug("Exception while FABSException:{}",this);
    }

    /**
     * Constructor
     * @param loggedUser
     * @param ex
     * Could be FABSException or any other one
     */
    public FABSException(OUser loggedUser, Exception ex)
    {
        this.loggedUser = loggedUser;
        if (ex instanceof FABSException) {
            assign((FABSException)ex);
        } else {
            this.originatingJavaException = ex;
            logger.debug("Exception while FABSException:{}",this);
        }
    }
    
    /**
     * Logs itself in constructor
     */
    public FABSException(FABSException ex) {
        assign(ex) ;
    }

    public void assign(FABSException ex) {
        this.message = ex.message;
        this.loggedUser = ex.loggedUser;
        this.originatingJavaException = ex.originatingJavaException;
        this.concernedEntity = ex.concernedEntity;
        this.relatedObjects = ex.relatedObjects;
        this.errorUserMessagesInfo = ex.errorUserMessagesInfo;
        this.errorUserMessages = ex.errorUserMessages;
    }
    /**
     * Compatible with {@link OLog} format
     */
    public String constructLogMessage() {
        String logMsg = "";
        if ( originatingJavaException != null ) {
            Exception ex = originatingJavaException ;
            logMsg +=   "\n\t* Orig. Exception Type    : " + ex.getClass().getName()
                    + (ex.getMessage() == null ? "" :
                        "\n\t* Orig. Exception Message : " + ex.getMessage() )
                    + (ex.getCause() == null ? "" :
                        "\n\t* Orig. Exception Cause   : " + ex.getCause() )
                    + (ex.toString() == null ? "" :
                        "\n\t* Orig. Exception String  : " + ex.toString() );
        }
        logMsg +=   "\n\t* Message          : " + (message == null ? "" : message);
        logMsg +=   "\n\t* Logged User      : " + (loggedUser == null ? "null" :
            loggedUser.getDbid() + ", " + loggedUser.getLoginName())  ;

        if (concernedEntity != null) {
            logMsg +=   "\n\t* Concerned Entity : " + concernedEntity.toString() ;
        }
        if (relatedObjects != null) {
            for (Object obj : relatedObjects) {
                logMsg +=   "\n\t* Related Object   : " + obj.toString() ;
            }
        }
        if (errorUserMessagesInfo != null) {
            for (int i = 0 ; i < errorUserMessagesInfo.size() ; i ++) {
                List<String> errMessage = errorUserMessagesInfo.get(i) ;
                logMsg +=   "\n\t* Error Message    : " + errMessage.get(0);
            }
        }
        if (this.errorUserMessages != null) {
            for (int i = 0 ; i < errorUserMessages.size() ; i ++) {
                logMsg +=   "\n\t* Error Message    : " + errorUserMessages.get(i) +
                        errorUserMessages.get(i) == null ? "" :
                            ", Name=" + errorUserMessages.get(i).getName();
            }
        }
        logger.debug("Exception while FABSException:{}",logMsg);
        return logMsg ;
    }
}
