/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.process.Intalio;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;



import javax.swing.JOptionPane;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.security.user.Inbox;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import org.slf4j.LoggerFactory;

/**
 *
 * @author melsayed
 */
public class IntalioProcessService implements  IntalioService{
final static org.slf4j.Logger logger = LoggerFactory.getLogger(IntalioProcessService.class);
    public String loadProcessConfiguration(String configName) {
        logger.debug("Entering");
        Properties props = new Properties();
        String configValue = "";
        //try retrieve data from file
        try {
            props.load(this.getClass().getResource("FABSProcess.properties").openStream());
            configValue = props.getProperty(configName);
            logger.debug("Returning");
            return configValue;
        } catch (IOException ex) {
            logger.error("Exception thrown: ",ex);
            logger.debug("Returning with \"\"");
            return "";
        }
    }
    private String getProcessEngineServiceURL()    {
        return loadProcessConfiguration("ProcessEngineURL")+"/axis2/services/";
    }

    private String getTaskManagementURL(){
        return loadProcessConfiguration("ProcessEngineURL")+"/axis2/services/TaskManagementServices?wsdl";
    }
    
    public String authenticate(String userName, String password)    {
        try {
            logger.debug("Entering");
            // TODO add your handling code here:
            String processURL = getProcessEngineServiceURL()+ "TokenService?wsdl";
            URL oURL = new URL(processURL);
            HttpURLConnection con = (HttpURLConnection) oURL.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            con.setRequestProperty("SOAPAction", "authenticateUser");
            String reqXML = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xm" +
                    "lsoap.org/soap/envelope/\" xmlns:tok=\"http://tempo.intalio" +
                    ".org/security/tokenService/\"><soapenv:Header/><soapenv:Bod" +
                    "y><tok:authenticateUser><tok:user>" + userName + "</tok:use" +
                    "r><tok:password>" + password + "</tok:password></tok:authen" +
                    "ticateUser></soapenv:Body></soapenv:Envelope>";
            OutputStream reqStream = con.getOutputStream();
            reqStream.write(reqXML.getBytes());
            InputStream resStream = con.getInputStream();
            byte[] byteBuf = new byte[1024];
            int len = resStream.read(byteBuf);
            String in = "";
            while (len > -1) {
                in += new String(byteBuf, 0, len);
                len = resStream.read(byteBuf);
            }
            reqStream.close();
            resStream.close();
            System.out.println(in);
            in = in.substring(in.indexOf("<tokenws:token>")+14);
            System.out.println(in);
            in = in.intern().substring(1, in.indexOf("</tokenws:token>"));
            System.out.println(in);
            logger.debug("Returning with: {}",in);
            return in;
        } catch (IOException ex) {
            logger.error("Exception thrown: ",ex);
        }
        logger.debug("Returning with \"\"");
        return "";
    }

    public List<IntalioProcessInstance> getProcessInstancesList() {
//        Element element;
//        NodeList nodes;
        List<IntalioProcessInstance> processInstanceses = new ArrayList();
        try {
            // TODO add your handling code here:
            String processURL = loadProcessConfiguration("ProcessEngineURL")+"/ode/processes/InstanceManagement?wsdl";
            URL oURL = new URL(processURL);
            HttpURLConnection con = (HttpURLConnection) oURL.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            con.setRequestProperty("SOAPAction", "listAllInstances");
            String reqXML = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xm" +
                    "lsoap.org/soap/envelope/\" xmlns:pmap=\"http://www.apache.o" +
                    "rg/ode/pmapi\"><soapenv:Header/><soapenv:Body><pmap:listAll" +
                    "Instances/></soapenv:Body></soapenv:Envelope>";
            OutputStream reqStream = con.getOutputStream();
            reqStream.write(reqXML.getBytes());
            InputStream resStream = con.getInputStream();
            byte[] byteBuf = new byte[1024];
            int len = resStream.read(byteBuf);
            String in = "";
            while (len > -1) {
                in += new String(byteBuf, 0, len);
                len = resStream.read(byteBuf);
            }
            reqStream.close();
            resStream.close();
            String[] processList = in.split("<ns:instance-info xmlns:ns=\"http://www.apache.org/ode/pmapi/types/2006/08/02/\">");
            for (int i = 1; i < processList.length; i++) {
                processInstanceses.add(getProcessInstanceFromString(processList[i]));
                logger.debug("Returning with processInstanceses");
                return processInstanceses;
            }
        } catch (IOException ex) {
            logger.error("Exception thrown: ",ex);
        }
        logger.debug("Returning with processInstanceses");
        return processInstanceses;
    }

    public void getProcessInstanceInfo(Long pid)  {
        // TODO add your handling code here:
        logger.debug("Entering");
        try {
            // TODO add your handling code here:
            String processURL = loadProcessConfiguration("ProcessEngineURL")+"/ode/processes/InstanceManagement?wsdl";
            URL oURL = new URL(processURL);
            HttpURLConnection con = (HttpURLConnection) oURL.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            con.setRequestProperty("SOAPAction", "listAllInstances");
            String reqXML = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xm" +
                    "lsoap.org/soap/envelope/\" xmlns:pmap=\"http://www.apache.o" +
                    "rg/ode/pmapi\"><soapenv:Header/><soapenv:Body><pmap:getInst" +
                    "anceInfo><iid>"+pid+"</iid></pmap:getInstanceInfo></soapenv" +
                    ":Body></soapenv:Envelope>";
            OutputStream reqStream = con.getOutputStream();
            reqStream.write(reqXML.getBytes());
            InputStream resStream = con.getInputStream();
            byte[] byteBuf = new byte[1024];
            int len = resStream.read(byteBuf);
            String in = "";
            while (len > -1) {
                in += new String(byteBuf, 0, len);
                len = resStream.read(byteBuf);
            }
            reqStream.close();
            resStream.close();
            String[] processList = in.split("<ns:instance-info xmlns:ns=\"http://www.apache.org/ode/pmapi/types/2006/08/02/\">");
            for (int i = 1; i < processList.length; i++) {
                processList[i] = processList[i].substring(processList[i].indexOf("<ns:iid"));
                processList[i] = processList[i].substring(8, processList[i].indexOf("</ns:iid"));
                System.out.println(processList[i]);
                JOptionPane.showMessageDialog(null, processList[i]);
            }
        } catch (IOException ex) {
            logger.error("Exception thrown: ",ex);
        }
    }

    public void getProcessList() {
        logger.debug("Entering");
        try {
            // TODO add your handling code here:
            String processURL = loadProcessConfiguration("ProcessEngineURL")+"/ode/processes/ProcessManagement?wsdl";
            URL oURL = new URL(processURL);
            HttpURLConnection con = (HttpURLConnection) oURL.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            con.setRequestProperty("SOAPAction", "listAllProcesses");
            String reqXML = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xml" + "soap.org/soap/envelope/\" xmlns:pmap=\"http://www.apache.org" + "/ode/pmapi\">\n<soapenv:Header/>\n<soapenv:Body>\n<pmap:listAllProcesses/>" + "</soapenv:Body>\n</soapenv:Envelope>";
            OutputStream reqStream = con.getOutputStream();
            reqStream.write(reqXML.getBytes());
            InputStream resStream = con.getInputStream();
            byte[] byteBuf = new byte[1024];
            int len = resStream.read(byteBuf);
            String in = "";
            while (len > -1) {
                in += new String(byteBuf, 0, len);
                len = resStream.read(byteBuf);
            }
            reqStream.close();
            resStream.close();
            String[] processList = in.split("<ns:process-info xmlns:ns=\"http://www.apache.org/ode/pmapi/types/2006/08/02/\">");
            for (int i = 1; i < processList.length; i++) {
                processList[i] = processList[i].substring(processList[i].indexOf("<ns:pid"));
                processList[i] = processList[i].substring(8, processList[i].indexOf("</ns:pid"));
                System.out.println(processList[i]);
                JOptionPane.showMessageDialog(null, processList[i]);
            }
        } catch (IOException ex) {
            logger.error("Exception thrown: ",ex);
        }
    }

    public String[] getMyTasks(String userName, String password)    {
        logger.debug("Entering");
        try {
            // TODO add your handling code here:
            String processURL = getTaskManagementURL();
            URL oURL = new URL(processURL);
            HttpURLConnection con = (HttpURLConnection) oURL.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            con.setRequestProperty("SOAPAction", "getTaskList");
            String reqXML = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xm" +
                    "lsoap.org/soap/envelope/\" xmlns:tas=\"http://www.intalio.c" +
                    "om/BPMS/Workflow/TaskManagementServices-20051109/\"><soapen" +
                    "v:Header/><soapenv:Body><tas:getTaskListRequest><tas:partic" +
                    "ipantToken>" + authenticate(userName, password) + "</tas:pa" +
                    "rticipantToken></tas:getTaskListRequest></soapenv:Body></so" +
                    "apenv:Envelope>";
            OutputStream reqStream = con.getOutputStream();
            reqStream.write(reqXML.getBytes());
            InputStream resStream = con.getInputStream();
            byte[] byteBuf = new byte[1024];
            int len = resStream.read(byteBuf);
            String in = "";
            while (len > -1) {
                in += new String(byteBuf, 0, len);
                len = resStream.read(byteBuf);
            }
            reqStream.close();
            resStream.close();
            String[] taskList = in.split("<tms:task>");
            String[] taskIds = new String[taskList.length-1];
            for (int i = 1; i < taskList.length; i++) {
                taskList[i] = taskList[i].substring(taskList[i].indexOf("<tms:taskId>"));
                taskList[i] = taskList[i].substring(12, taskList[i].indexOf("</tms:taskId>"));
                taskIds[i-1] = taskList[i];
                JOptionPane.showMessageDialog(null, taskList[i]);
            }
            logger.debug("Returning taskIds: {}",taskIds);
            return taskIds;
        } catch (IOException ex) {
            logger.error("Exception thrown: ",ex);
            JOptionPane.showMessageDialog(null, ex.toString());
        }
        logger.debug("Returning");
        return new String[0];
    }

    public List<IntalioTask> getMyCompletedTasks(String userName, String password)    {
        List<IntalioTask> myTasks = getTaskWithType(userName, password,"COMPLETED");
        return myTasks;
    }

    public List<IntalioTask> getTaskWithType(String userName, String password, String type)
    {
        logger.debug("Entering");
        try {
            // TODO add your handling code here:
            String processURL = getTaskManagementURL();
            URL oURL = new URL(processURL);
            HttpURLConnection con = (HttpURLConnection) oURL.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            con.setRequestProperty("SOAPAction", "getTaskList");
            String reqXML = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xm" +
                    "lsoap.org/soap/envelope/\" xmlns:tas=\"http://www.intalio.c" +
                    "om/BPMS/Workflow/TaskManagementServices-20051109/\"><soapen" +
                    "v:Header/><soapenv:Body><tas:getTaskListRequest><tas:partic" +
                    "ipantToken>" + authenticate(userName, password) + "</tas:pa" +
                    "rticipantToken></tas:getTaskListRequest></soapenv:Body></so" +
                    "apenv:Envelope>";
            OutputStream reqStream = con.getOutputStream();
            reqStream.write(reqXML.getBytes());
            InputStream resStream = con.getInputStream();
            byte[] byteBuf = new byte[1024];
            int len = resStream.read(byteBuf);
            String in = "";
            while (len > -1) {
                in += new String(byteBuf, 0, len);
                len = resStream.read(byteBuf);
            }
            reqStream.close();
            resStream.close();
            String[] taskList = in.split("<tms:task>");
            List<String> taskIds = new ArrayList<String>();
            for (int i = 1; i < taskList.length; i++) {
                String taskType = taskList[i].substring(taskList[i].indexOf("<tms:taskState>"));
                taskType = taskList[i].substring(12, taskList[i].indexOf("</tms:taskState>"));
                taskList[i] = taskList[i].substring(taskList[i].indexOf("<tms:taskId>"));
                taskList[i] = taskList[i].substring(12, taskList[i].indexOf("</tms:taskId>"));
                if(taskList[i].equalsIgnoreCase(taskType)) taskIds.add(taskList[i]);
            }
            logger.debug("Returning with Null");
            return null;
        } catch (IOException ex) {
            logger.error("Exception thrown: ",ex);
            JOptionPane.showMessageDialog(null, ex.toString());
        }
        logger.debug("Returning with Null");
        return null;
    }
    public String getTaskInput(String taskId, String userName, String password)
    {
        logger.debug("Entering");
        try {
            // TODO add your handling code here:
            String processURL = getTaskManagementURL();
            URL oURL = new URL(processURL);
            HttpURLConnection con = (HttpURLConnection) oURL.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            con.setRequestProperty("SOAPAction", "getTask");
            String reqXML = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xm" +
                    "lsoap.org/soap/envelope/\" xmlns:tas=\"http://www.intalio.c" +
                    "om/BPMS/Workflow/TaskManagementServices-20051109/\"><soapen" +
                    "v:Header/><soapenv:Body><tas:getTaskRequest><tas:taskId>" + taskId +
                    "</tas:taskId><tas:participantToken>" + authenticate(userName, password) +
                    "</tas:participantToken></tas:getTaskRequest></soapenv:Body>" +
                    "</soapenv:Envelope>";
            OutputStream reqStream = con.getOutputStream();
            reqStream.write(reqXML.getBytes());
            InputStream resStream = con.getInputStream();
            byte[] byteBuf = new byte[1024];
            int len = resStream.read(byteBuf);
            String in = "";
            while (len > -1) {
                in += new String(byteBuf, 0, len);
                len = resStream.read(byteBuf);
            }
            reqStream.close();
            resStream.close();
            if(in.contains("<tms:input>"))
            {
                in = in.substring(in.indexOf("<tms:input>")+11);
                in = in.substring(0,in.indexOf("</tms:input>"));
                logger.debug("Returning with: {}",in);
                return in;
            }
        } catch (IOException ex) {
            logger.error("Exception thrown: ",ex);
            JOptionPane.showMessageDialog(null, ex.toString());
        }
        logger.debug("Returning with \"\" ");
        return "";

    }
    

//  Completed needs Integration with FABS
    public void completeTask(String task, String userName, String password) {
        try {
            logger.debug("Entering");
            // TODO add your handling code here:
            String processURL = getTaskManagementURL();
            URL oURL = new URL(processURL);
            HttpURLConnection con = (HttpURLConnection) oURL.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            con.setRequestProperty("SOAPAction", "complete");
            String reqXML = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xm" +
                    "lsoap.org/soap/envelope/\" xmlns:tas=\"http://www.intalio.c" +
                    "om/BPMS/Workflow/TaskManagementServices-20051109/\"><soapen" +
                    "v:Header/><soapenv:Body><tas:completeRequest><tas:taskId>"   +
                    task + "</tas:taskId><tas:participantToken>" + authenticate(userName, password) +
                    "</tas:participantToken></tas:completeRequest></soapenv:Body" +
                    "></soapenv:Envelope>";
            OutputStream reqStream = con.getOutputStream();
            reqStream.write(reqXML.getBytes());
            InputStream resStream = con.getInputStream();
            byte[] byteBuf = new byte[1024];
            int len = resStream.read(byteBuf);
            String in = "";
            while (len > -1) {
                in += new String(byteBuf, 0, len);
                len = resStream.read(byteBuf);
            }
            reqStream.close();
            resStream.close();
            if(in.contains("<tms:okResponse")){
                logger.debug("Returning");
                return ;
            }
                
        } catch (IOException ex) {
            logger.error("Exception thrown: ",ex);
            JOptionPane.showMessageDialog(null, ex.toString());
        }
    }


//  Completed needs Integration with FABS
    private IntalioProcessInstance getProcessInstanceFromString(String ProcessData)
    {
        try
        {
         String id, iid, processName, status, startDate, lastActiveDate;
                iid = ProcessData.substring(ProcessData.indexOf("<ns:iid")+8);
                iid = iid.substring(0, iid.indexOf("</ns:iid"));
                id = ProcessData.substring(ProcessData.indexOf("<ns:pid"));
                id = id.substring(8, id.indexOf("</ns:pid"));
                processName = ProcessData.substring(ProcessData.indexOf("<ns:process-name"));
                processName = processName.substring(processName.indexOf(">")+1);
                processName = processName.substring(0, id.indexOf("</ns:process-name"));
                status = ProcessData.substring(ProcessData.indexOf("<ns:status>")+11);
                status = status.substring(0, status.indexOf("</ns:status>"));
                startDate = ProcessData.substring(ProcessData.indexOf("<ns:dt-started>")+15);
                startDate = startDate.substring(0, startDate.indexOf("</ns:dt-started>"));
                lastActiveDate = ProcessData.substring(ProcessData.indexOf("<ns:dt-last-active>")+19);
                lastActiveDate = lastActiveDate.substring(0, lastActiveDate.indexOf("</ns:dt-last-active>"));
                IntalioProcessInstance processInstance = new IntalioProcessInstance();
                processInstance.setInstanceId(new Long(iid));
                processInstance.setLastActiveDate(lastActiveDate);
                processInstance.setName(processName);
                processInstance.setProcessId(id);
                processInstance.setStartDate(startDate);
                processInstance.setStatus(status);
                return processInstance;
        }
        catch(Exception ex)
        {
            logger.error("Exception thrown: ",ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    private IntalioProcess getProcessFromString(String ProcessData)
    {
        try
        {
         String processId, version, Status, name, activeCount, completedCount,
                 errorCount, failedCount, suspendedCount, terminatedCount;
                processId = ProcessData.substring(ProcessData.indexOf("<ns:pid>")+8);
                processId = processId.substring(0, processId.indexOf("</ns:pid"));
                version = ProcessData.substring(ProcessData.indexOf("<ns:version>")+12);
                version = version.substring(0, version.indexOf("</ns:version>"));
                Status = ProcessData.substring(ProcessData.indexOf("<ns:status>")+11);
                Status = Status.substring(0, Status.indexOf("</ns:status>"));
                name = ProcessData.substring(ProcessData.indexOf("<ns:process-name"));
                name = name.substring(name.indexOf(">"));
                name = name.substring(name.indexOf(":")+1);
                name = name.substring(0, name.indexOf("</ns:process-name>"));
                activeCount = ProcessData.substring(ProcessData.indexOf("<ns:instances state=\"ACTIVE\"")+36);
                activeCount = activeCount.substring(0, activeCount.indexOf("\""));
                completedCount = ProcessData.substring(ProcessData.indexOf("<ns:instances state=\"COMPLETED\"")+39);
                completedCount = completedCount.substring(0, completedCount.indexOf("\""));
                errorCount = ProcessData.substring(ProcessData.indexOf("<ns:instances state=\"ERROR\"")+35);
                errorCount = errorCount.substring(0, errorCount.indexOf("\""));
                failedCount = ProcessData.substring(ProcessData.indexOf("<ns:instances state=\"FAILED\"")+36);
                failedCount = failedCount.substring(0, failedCount.indexOf("\""));
                suspendedCount = ProcessData.substring(ProcessData.indexOf("<ns:instances state=\"SUSPENDED\"")+39);
                suspendedCount = suspendedCount.substring(0, suspendedCount.indexOf("\""));
                terminatedCount = ProcessData.substring(ProcessData.indexOf("<ns:instances state=\"TERMINATED\"")+40);
                terminatedCount = terminatedCount.substring(0, terminatedCount.indexOf("\""));
                IntalioProcess process = new IntalioProcess();
                process.setActiveCount(new Long(activeCount));
                process.setCompletedCount(new Long(completedCount));
                process.setErrorCount(new Long(errorCount));
                process.setFailedCount(new Long(failedCount));
                process.setName(name);
                process.setProcessId(processId);
                process.setStatus(Status);
                process.setSuspendedCount(new Long(suspendedCount));
                process.setTerminatedCount(new Long(terminatedCount));
                process.setVersion(new Long(version));
                return process;
        }
        catch(Exception ex)
        {
            logger.error("Exception thrown: ",ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    private IntalioTask getTaskFromString(String taskData)
    {
        logger.debug("Entering");
        try
        {
            String taskId;
            String status;
            String type;
            String description;
            String processId;
            String creationDate;
            String roleOwner;
            String formUrl;
            String userProcessCompleteSOAPAction;
            String isChainedBefore;
            taskId = taskData.substring(taskData.indexOf("<tms:taskId>")+12);
            taskId = taskId.substring(0, taskId.indexOf("</tms:taskId> "));
            status = taskData.substring(taskData.indexOf("<tms:taskState>")+15);
            status = status.substring(0, status.indexOf("</tms:taskState>"));
            type = taskData.substring(taskData.indexOf("<tms:taskType>")+14);
            type = type.substring(0, type.indexOf("</tms:taskType>"));
            description = taskData.substring(taskData.indexOf("<tms:description>")+17);
            description = description.substring(0, description.indexOf("</tms:description>"));
            processId = taskData.substring(taskData.indexOf("<tms:processId>")+15);
            processId = processId.substring(0, processId.indexOf("</tms:processId>"));
            creationDate = taskData.substring(taskData.indexOf("<tms:creationDate>")+18);
            creationDate = creationDate.substring(0, creationDate.indexOf("</tms:creationDate>"));
            roleOwner = taskData.substring(taskData.indexOf("<tms:roleOwner>")+15);
            roleOwner = roleOwner.substring(0, roleOwner.indexOf("</tms:roleOwner>"));
            formUrl = taskData.substring(taskData.indexOf("<tms:formUrl>")+13);
            formUrl = formUrl.substring(0, formUrl.indexOf("</tms:formUrl>"));
            userProcessCompleteSOAPAction = taskData.substring(taskData.indexOf("<tms:userProcessCompleteSOAPAction>")+35);
            userProcessCompleteSOAPAction = userProcessCompleteSOAPAction.substring(0,
                    userProcessCompleteSOAPAction.indexOf("</tms:userProcessCompleteSOAPAction>"));
            isChainedBefore = taskData.substring(taskData.indexOf("<tms:isChainedBefore>")+21);
            isChainedBefore = isChainedBefore.substring(0, isChainedBefore.indexOf("</tms:isChainedBefore>"));
            IntalioTask task = new IntalioTask();
            task.setCreationDate(creationDate);
            task.setDescription(description);
            task.setFormUrl(formUrl);
            task.setIsChainedBefore(isChainedBefore);
            task.setProcessId(processId);
            task.setRoleOwner(roleOwner);
            task.setStatus(status);
            task.setTaskId(taskId);
            task.setType(type);
            task.setUserProcessCompleteSOAPAction(userProcessCompleteSOAPAction);
            logger.debug("Returning");
            return task;
        }
        catch(Exception ex)
        {
            logger.error("Exception thrown: ",ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    public void skipTask(String task, String userName, String password) {
        try {
            logger.debug("Entering");
            // TODO add your handling code here:
            String processURL = getTaskManagementURL();
            URL oURL = new URL(processURL);
            HttpURLConnection con = (HttpURLConnection) oURL.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            con.setRequestProperty("SOAPAction", "skip");
            String reqXML = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xm" +
                    "lsoap.org/soap/envelope/\" xmlns:tas=\"http://www.intalio.c" +
                    "om/BPMS/Workflow/TaskManagementServices-20051109/\"><soapen" +
                    "v:Header/><soapenv:Body><tas:skipRequest><tas:taskId>" + task + 
                    "</tas:taskId><tas:participantToken>" + authenticate(userName, password) + 
                    "</tas:participantToken></tas:skipRequest></soapenv:Body></s" +
                    "oapenv:Envelope>";
            OutputStream reqStream = con.getOutputStream();
            reqStream.write(reqXML.getBytes());
            InputStream resStream = con.getInputStream();
            byte[] byteBuf = new byte[1024];
            int len = resStream.read(byteBuf);
            String in = "";
            while (len > -1) {
                in += new String(byteBuf, 0, len);
                len = resStream.read(byteBuf);
            }
            reqStream.close();
            resStream.close();
            if(in.contains("<tms:okResponse"))return ;
        } catch (IOException ex) {
            logger.error("Exception thrown: ",ex);
            JOptionPane.showMessageDialog(null, ex.toString());
        }

    }

    public List<IntalioTaskInstance> getUserOutbox(String userName, String password) {
        
logger.debug("Returning with Null");
        return null;
    }

    public List<IntalioTaskInstance> getUserInbox(String userName, String password) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    

    public String getNextHTScreen(String currentTask, String userName, String password) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getUserNextHTScreen(String currentTask, String user, String userName, String password) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<IntalioTaskInstance> getUserCompletedTasks(String selectedUser, String userName, String password) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getStringInstance(String taskID, String userName, String password) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getProcessNameFromURL(String processWSDLURL) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getProcessState(long processId, String userName, String password) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void restartProcessInstance(long processId, String userName, String password) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void resumeProcessInstance(long processId, String userName, String password) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void suspendProcess(long processId, String userName, String password) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void terminateProcess(long processId, String userName, String password) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getProcessEngineURL() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void claimTask(String task, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void completeTask(String task, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void exitTask(String task, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void failTask(String task, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void resumeTask(String task, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void startTask(String task, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void delegateTask(String task, OUser user, String delegatedUser) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void releaseTask(String task, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setTaskOutputAndComplete(String task, String output, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setTaskOutput(String task, String output, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void skipTask(String task, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getTaskInput(String task, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getUserNextHTScreen(String currentTask, String user, OUser loggedUser) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<IntalioTaskInstance> getUserCompletedTasks(String selectedUser, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<IntalioTaskInstance> getUserCompletedTasksInfo(OUser selectedUser, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public OScreen getTaskScreen(String taskInfo, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<String> getUserClaimedTasks(OUser selectedUser, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<String> getUserOpenTasks(OUser selectedUser, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<String> getUserWorkableTasksInfo(OUser selectedUser, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<IntalioTask> getUserWorkableTasks(OUser selectedUser, OUser user) {
        List<IntalioTask> userTasks = new ArrayList<IntalioTask>();
        try {
            // TODO add your handling code here:
            String processURL = getTaskManagementURL();
            URL oURL = new URL(processURL);
            HttpURLConnection con = (HttpURLConnection) oURL.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            con.setRequestProperty("SOAPAction", "getTaskList");
            String reqXML = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xm" +
                    "lsoap.org/soap/envelope/\" xmlns:tas=\"http://www.intalio.c" +
                    "om/BPMS/Workflow/TaskManagementServices-20051109/\"><soapen" +
                    "v:Header/><soapenv:Body><tas:getTaskListRequest><tas:partic" +
                    "ipantToken>examples\\" + authenticate(user.getLoginName(), user.getPassword()) + "</tas:pa" +
                    "rticipantToken></tas:getTaskListRequest></soapenv:Body></so" +
                    "apenv:Envelope>";
            OutputStream reqStream = con.getOutputStream();
            reqStream.write(reqXML.getBytes());
            InputStream resStream = con.getInputStream();
            byte[] byteBuf = new byte[1024];
            int len = resStream.read(byteBuf);
            String in = "";
            while (len > -1) {
                in += new String(byteBuf, 0, len);
                len = resStream.read(byteBuf);
            }
            reqStream.close();
            resStream.close();
            String[] taskList = in.split("<tms:task>");
            for (int i = 1; i < taskList.length; i++) {
                if(taskList[i].contains("<tms:taskState>READY</tms:taskState>") 
                        || taskList[i].contains("<tms:taskState>CLAIMED</tms:taskState>")
                || taskList[i].contains(""))
                {
                    IntalioTask intalioTask = getTaskFromString(taskList[i]);
                    userTasks.add(intalioTask);
                }
            }
            logger.debug("Returning with userTasks: {}",userTasks);
            return userTasks;
        }
        catch(Exception ex)
        {
            logger.error("Exception thrown: ",ex);
            logger.debug("Returning with userTasks: {}",userTasks);
            return userTasks;
        }

    }

    public String getStringInstance(String taskID, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public IntalioTask getTaskInstanceDetails(String taskID, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getProcessState(long processId, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<IntalioTask> getProcessTasks(Long processId, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void restartProcessInstance(long processId, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void resumeProcessInstance(long processId, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public long startProcess(IntalioProcess process, ODataMessage processInput, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void suspendProcess(long processId, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void terminateProcess(long processId, OUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }


    // COMPLETED
    public List<IntalioProcessInstance> getRunningProcessInstances() {
        List<IntalioProcessInstance> processInstances   = new ArrayList<IntalioProcessInstance>();
        List<IntalioProcessInstance> allProcessInstances   = getProcessInstancesList();
        for (Iterator<IntalioProcessInstance> it = allProcessInstances.iterator(); it.hasNext();) {
            IntalioProcessInstance intalioProcessInstance = it.next();
            if(intalioProcessInstance.getStatus().equalsIgnoreCase("ACTIVE")) processInstances.add(intalioProcessInstance);
        }
        logger.debug("Returning with processInstances: {}",processInstances);
        return processInstances;
        
    }

    // COMPLETED
    public IntalioProcessInstance getProcessInstanceByProcessID(Long processID, OUser user) {
        logger.debug("Entering");
        IntalioProcessInstance instance = new IntalioProcessInstance();
        try {
            // TODO add your handling code here:
            String processURL = getTaskManagementURL();
            URL oURL = new URL(processURL);
            HttpURLConnection con = (HttpURLConnection) oURL.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            con.setRequestProperty("SOAPAction", "skip");
            String reqXML = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xm" +
                    "lsoap.org/soap/envelope/\" xmlns:pmap=\"http://www.apache.o" +
                    "rg/ode/pmapi\"><soapenv:Header/><soapenv:Body><pmap:getInst" +
                    "anceInfo><iid>" + processID + "</iid></pmap:getInstanceInfo" +
                    "></soapenv:Body></soapenv:Envelope>";
            OutputStream reqStream = con.getOutputStream();
            reqStream.write(reqXML.getBytes());
            InputStream resStream = con.getInputStream();
            byte[] byteBuf = new byte[1024];
            int len = resStream.read(byteBuf);
            String in = "";
            while (len > -1) {
                in += new String(byteBuf, 0, len);
                len = resStream.read(byteBuf);
            }
            reqStream.close();
            resStream.close();
            instance = getProcessInstanceFromString(in);
            logger.debug("Returning with instance: {}",instance);
            return instance;
        } catch (IOException ex) {
            logger.error("Exception thrown: ",ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    // NOT COMPLETED
    public Long getTaskProcessID(String task, OUser user) {
        logger.debug("Entering");
        IntalioTask intalioTask = getTaskInfo(task, user);
        logger.debug("Returning with Null");
        return null;
    }

    private IntalioTask getTaskInfo(String taskId, OUser user)
    {
        logger.debug("Entering");
        IntalioTask task = new IntalioTask();
        try {
            // TODO add your handling code here:
            String processURL = getTaskManagementURL();
            URL oURL = new URL(processURL);
            HttpURLConnection con = (HttpURLConnection) oURL.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            con.setRequestProperty("SOAPAction", "skip");
            String reqXML = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xm" +
                    "lsoap.org/soap/envelope/\" xmlns:tas=\"http://www.intalio.c" +
                    "om/BPMS/Workflow/TaskManagementServices-20051109/\"><soapen" +
                    "v:Header/><soapenv:Body><tas:getTaskRequest><tas:taskId>" +
                    task + "</tas:taskId><tas:participantToken>" + authenticate(
                    user.getLoginName(), user.getPassword()) + "</tas:participan" +
                    "tToken></tas:getTaskRequest></soapenv:Body></soapenv:Envelope>";
            OutputStream reqStream = con.getOutputStream();
            reqStream.write(reqXML.getBytes());
            InputStream resStream = con.getInputStream();
            byte[] byteBuf = new byte[1024];
            int len = resStream.read(byteBuf);
            String in = "";
            while (len > -1) {
                in += new String(byteBuf, 0, len);
                len = resStream.read(byteBuf);
            }
            reqStream.close();
            resStream.close();
            task = getTaskFromString(in);
            return task;
        } catch (IOException ex) {
            logger.error("Exception thrown: ",ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    public List<Inbox> getUserInbox(OUser loggedUser)
    {
        logger.debug("Entering");
        if(loggedUser == null){
            logger.debug("Returning with Null");
            return null;
        }
            
        
        List<Inbox> inboxList = new ArrayList<Inbox>();
        List<IntalioTask> tasks = new ArrayList<IntalioTask>();
        tasks = getUserWorkableTasks(loggedUser, loggedUser);
        for (IntalioTask taskInfo : tasks) {
                Inbox inbox = new Inbox();
                /////inbox.setFromMail("SYSTEM");
                inbox.setDate(taskInfo.getCreationDate());
                inbox.setTitle(taskInfo.getDescription());
                inbox.setText(taskInfo.getTaskId());
                inbox.setProcess(true);
                inbox.setState(taskInfo.getStatus());
                inboxList.add(inbox);
            }
        logger.debug("Returning");
        return inboxList;
    }
}
