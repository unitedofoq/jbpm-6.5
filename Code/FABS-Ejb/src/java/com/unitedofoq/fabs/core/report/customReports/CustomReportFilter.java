/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.report.customReports;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import java.io.Serializable;

/**
 *
 * @author MEA
 */
@Entity
@ChildEntity(fields = {"customreportfilterfields"})
public class CustomReportFilter extends BaseEntity {

    private String filterName;
    private String discription;
    private boolean showBeforRun;
    private String viewName;
    
    @OneToMany(mappedBy = "customReportFilter")
    private List<CustRepFilterFld> customreportfilterfields;
    
    @OneToMany(mappedBy = "customReportFilter")
    private List<CustomReport> customReports;
    
    @Transient
    CustomReport customReportObject;
    

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public String getFilterNameDD() {
        return "CustomReportFilter_filterName";
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public String getDiscriptionDD() {
        return "CustomReportFilter_discription";
    }

    public List<CustRepFilterFld> getCustomreportfilterfields() {
        return customreportfilterfields;
    }

    public void setCustomreportfilterfields(List<CustRepFilterFld> customreportfilterfields) {
        this.customreportfilterfields = customreportfilterfields;
    }

    public String getCustomreportfilterfieldsDD() {
        return "CustomReportFilter_customreportfilterfields";
    }

    public List<CustomReport> getCustomReports() {
        return customReports;
    }

    public void setCustomReports(List<CustomReport> customReports) {
        this.customReports = customReports;
    }

    public String getCustomReportsDD() {
        return "CustomReportFilter_customReports";
    }

    public boolean isShowBeforRun() {
        return showBeforRun;
    }

    public void setShowBeforRun(boolean showBeforRun) {
        this.showBeforRun = showBeforRun;
    }

    public String getShowBeforRunDD() {
        return "CustomReportFilter_showBeforRun";
    }

    public String getViewName() {
        return viewName;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    public CustomReport getCustomReportObject() {
        return customReportObject;
    }

    public void setCustomReportObject(CustomReport customReportObject) {
        this.customReportObject = customReportObject;
    }
    
    public String getViewNameDD() {
        return "CustomReportFilter_viewName";
    }
    
}
