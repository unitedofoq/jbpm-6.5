/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.alert.entities;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.function.JavaFunction;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;

/**
 *
 * @author lap
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "ATYPE", discriminatorType = DiscriminatorType.STRING)
public class BaseAlert extends BaseEntity {
    private String messageSubject;
    private String messageBody;
    @Column(nullable = false, unique = true)
    private String title;
    private boolean sendViaEmail;
    private boolean sendViaSMS;
    @OneToOne
    private JavaFunction javaFunction;

    public String getJavaFunctionDD() {
        return "BaseAlert_javaFunction";
    }
    
    public JavaFunction getJavaFunction() {
        return javaFunction;
    }

    public void setJavaFunction(JavaFunction javaFunction) {
        this.javaFunction = javaFunction;
    }
    

    public String getSendViaEmailDD() {
        return "Alert_sendViaEmail";
    }

    public boolean isSendViaEmail() {
        return sendViaEmail;
    }

    public void setSendViaEmail(boolean sendViaEmail) {
        this.sendViaEmail = sendViaEmail;
    }

    public String getSendViaSMSDD() {
        return "Alert_sendViaSMS";
    }

    public boolean isSendViaSMS() {
        return sendViaSMS;
    }

    public void setSendViaSMS(boolean sendViaSMS) {
        this.sendViaSMS = sendViaSMS;
    }

    public String getMessageSubjectDD() {
        return "Alert_messageSubject";
    }

    public String getMessageSubject() {
        return messageSubject;
    }

    public void setMessageSubject(String messageSubject) {
        this.messageSubject = messageSubject;
    }

    public String getMessageBodyDD() {
        return "Alert_messageBody";
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getTitleDD() {
        return "Alert_title";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
