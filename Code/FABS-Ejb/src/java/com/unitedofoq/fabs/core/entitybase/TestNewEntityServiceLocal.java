package com.unitedofoq.fabs.core.entitybase;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

@Local
public interface TestNewEntityServiceLocal {

    public OFunctionResult testInit(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult testValidation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult testPost(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult testChangeFunction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult testSaveListAction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
}
