/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.encryption;

import com.unitedofoq.fabs.core.content.AttachmentDTO;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.ejb.Local;

/**
 *
 * @author mostafa
 */
@Local
public interface DataEncryptorServiceLocal {
    
    public void init();
    public String generateSymmetricKey();

    public byte[] getKeyValue();
    public String getSymmetricKey();
    public String getIvKey();
    
    public String textEncryptor(String msg);

    public String textDecryptor(String encMsg);

    public String bigIntEncryptor(BigInteger Msg);

    public BigInteger bigIntDecryptor(String encMsg);

    public String bigDecEncryptor(BigDecimal Msg);
    
     public BigDecimal bigDecDecryptor(String encMsg);

    public com.unitedofoq.fabs.core.function.OFunctionResult encryptionTableHasData(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult encryptionTableHasDataDelete(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    
    public OFunctionResult encryColumnNotFound(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
 
    public void changePublicPrivateKeys(AttachmentDTO entityAttachment,long entityDBID, byte[] firstPublic, com.unitedofoq.fabs.core.security.user.OUser systemUser);
    
    public OFunctionResult encryptOuserPassword(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser); 
    
}