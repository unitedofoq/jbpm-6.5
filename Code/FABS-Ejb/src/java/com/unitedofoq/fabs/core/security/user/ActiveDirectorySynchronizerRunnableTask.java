///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.unitedofoq.fabs.core.security.user;
//
//import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
//import com.unitedofoq.fabs.central.OCentralUser;
//import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
//
//import java.util.List;
//import java.util.TimerTask;
//import javax.ejb.EJB;
//
//
///**
// *
// * @author mostafa
// */
//public class ActiveDirectorySynchronizerRunnableTask extends TimerTask implements Runnable{
//    @EJB
//    private ActiveDirectorySyncronizer activeDirectorySyncronizer;
//    @EJB
//    private OEntityManagerRemote oem;
//    @EJB
//    private OCentralEntityManagerRemote centralOEM;
//
//    ActiveDirectorySynchronizerRunnableTask() {
//    }
//    
//    public void synchronizeWithActiveDirectory() {
//        List<OCentralUser> users = centralOEM.getAdminUsers();
//        for (int i = 0; i < users.size(); i++) {
//            try {
//                activeDirectorySyncronizer.synchUsersFromAD(null, null, oem.getUser(users.get(i).getLoginName(),
//                        users.get(i).getTenant()));
//            } catch (Exception e) {
//                OLog.logException(e, null, "Couldn't Syncronize AD for Tenant " + users.get(i).getTenant().getName());
//            }
//        }
//
//    }
//    // Add business logic below. (Right-click in editor and choose
//    // "Insert Code > Add Business Method")
//
//    public ActiveDirectorySynchronizerRunnableTask(ActiveDirectorySyncronizer activeDirectorySyncronizer, OEntityManagerRemote oem, OCentralEntityManagerRemote centralOEM) {
//        this.activeDirectorySyncronizer = activeDirectorySyncronizer;
//        this.oem = oem;
//        this.centralOEM = centralOEM;
//    }
//
//   
//    @Override
//    public void run() {
//        try{
//      synchronizeWithActiveDirectory();
//        }catch(Exception e){
//            
//        }
//    }
//}
