package com.unitedofoq.fabs.core.i18n;

import java.util.List;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.security.user.OUser;

@Local
public interface TextTranslationServiceRemote {

    public BaseEntity forceLoadEntityTranslation(BaseEntity entity, OUser loggedUser)
            throws TextTranslationException;

    public BaseEntity loadEntityTranslation(BaseEntity entity, OUser loggedUser)
            throws TextTranslationException;

    public void saveEntityTranslation(BaseEntity entity, OUser loggedUser);    
    
    public BaseEntity preEntityTranslationSave(BaseEntity entity, OUser loggedUser);

    public void updateEntityTranslation(BaseEntity entity, OUser loggedUser);
    
    public void updateEntityTranslation(BaseEntity entity, boolean forceTranslation,OUser loggedUser);

    public void removeEntityTranslation(BaseEntity entity, OUser loggedUser);

    public boolean entityNeedsTranslation(BaseEntity entity, OUser loggedUser) ;

    public List<BaseEntity> loadEntityListTranslation(List<BaseEntity> entities, OUser loggedUser)
            throws TextTranslationException;
    
    /**
     *
     * @param transEntityName
     * @param conds
     * @param sorts
     * @param loggedUser
     * @param fieldClassName
     * @return
     */
    public List<Long> getFilterEntitiesDBIDs(String transEntityName, List<String> conds, List<String> sorts, OUser loggedUser, String fieldClassName);
    
    public List entityFromLocal(Class entityClass, List<String> cond, List<String> orderBy,
            List<String> fieldExpressions, OUser loggedUser);
    public String buildQuery(Class entityClass, List<String> conditions,
            List<String> orderBy, List<String> fieldExpressions, OUser loggedUser);
    public BaseEntity getEntityWithTowLevelsOfTranslatedFields(BaseEntity entity, OUser loggedUser);
    
}
