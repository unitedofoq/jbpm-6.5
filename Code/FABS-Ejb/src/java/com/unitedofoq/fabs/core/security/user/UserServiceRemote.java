/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.security.user;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.module.OModule;
import java.util.List;

import javax.ejb.Local;

/**
 *
 * @author nkhalil
 */
@Local
public interface UserServiceRemote {

    public OFunctionResult editUserInLiferay(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public boolean checkUserRole(long userDBID, long roleDBID, OUser loggedUser);

    public OFunctionResult synchUsersAndRolesWithLDAP(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult addUserToLDAP(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult addRoleToLDAP(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult addRoleUserToLDAP(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult editUserInLDAP(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult deleteUserFromLDAP(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult editRoleInLDAP(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult deleteRoleFromLDAP(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult editRoleUserInLDAP(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult deleteRoleUserFromLDAP(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult validateRoleNameUnchanged(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult validateLoginNameUnchanged(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);
//    public com.unitedofoq.fabs.core.function.OFunctionResult OUserCreateAction(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult ORoleCreateAction(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult userChangePassword(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult validateUserExistsInCent(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult parseUserExpression(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public String getUserDisplayedName(String loginName);

    public OUserDTO getUserDTO(String loginName, OUser loggedUser);

    public OFunctionResult resetUserPassowrd(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult updateUser(OUser ouser, OUser lDapUser, OUser loggedUser);

    public OFunctionResult unlockLiferayUser(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult addUser(OUser ouser, OUser loggedUser, OModule systemModule);

    public OUser getUserfromLoginNameAndTenant(String loginName);

    public OUser getLoggedInUser(String loginName);

    public OFunctionResult addUserToCent(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public String getPasswordForUserFromLDAP(OUser loggedUser);

    public List<String> getUserRoles(OUser user);

    public OFunctionResult validateUserNameWithRole(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public OFunctionResult validateRoleNameWithUser(ODataMessage odm, OFunctionParms params, OUser loggedUser);
}
