/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.log;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author nsaleh
 */
@Local        
public interface OLogSetUpServiceRemote {   
    public OFunctionResult loadLogInfoLevel( OUser loggedUser);
    public OFunctionResult loadLogLineOption( OUser loggedUser);
    public OFunctionResult loadLoadEntityOption( OUser loggedUser);
    public OFunctionResult loadLogQueryStatisticsLevel(OUser loggedUser);
    public OFunctionResult clearOLogCache(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult onFabsSetUpUpdate (ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
}
