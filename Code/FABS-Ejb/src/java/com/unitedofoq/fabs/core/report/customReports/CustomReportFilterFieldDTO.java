/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.report.customReports;

// TODO : Added by Belal. Used only by CustomreportFunctionDTO which is potential to be deleted as it's not used.

/**
 *
 * @author MEA
 */
public class CustomReportFilterFieldDTO {

    private long dbid;
    private String fieldName;
    private String operator;
    private String val1;
    private String val2;
    private int sortIndex;
    private String title;
    private long val1DdDBID;
    private boolean mandatory;
    private long val1OentityDBID;
    private String val1FieldExpression;

    public CustomReportFilterFieldDTO() {
    }

    public CustomReportFilterFieldDTO(long dbid,
            String fieldName, 
            String operator, 
            String val1, 
            String val2, 
            int sortIndex, 
            String title, 
            long val1DdDBID, 
            boolean mandatory, 
            long val1OentityDBID, 
            String val1FieldExpression) {
        this.dbid=dbid;
        this.fieldName = fieldName;
        this.operator = operator;
        this.val1 = val1;
        this.val2 = val2;
        this.sortIndex = sortIndex;
        this.title = title;
        this.val1DdDBID = val1DdDBID;
        this.mandatory = mandatory;
        this.val1OentityDBID = val1OentityDBID;
        this.val1FieldExpression = val1FieldExpression;
    }

    public long getDbid() {
        return dbid;
    }

    public void setDbid(long dbid) {
        this.dbid = dbid;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getVal1() {
        return val1;
    }

    public void setVal1(String val1) {
        this.val1 = val1;
    }

    public String getVal2() {
        return val2;
    }

    public void setVal2(String val2) {
        this.val2 = val2;
    }

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getVal1DdDBID() {
        return val1DdDBID;
    }

    public void setVal1DdDBID(long val1DdDBID) {
        this.val1DdDBID = val1DdDBID;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    public long getVal1OentityDBID() {
        return val1OentityDBID;
    }

    public void setVal1OentityDBID(long val1OentityDBID) {
        this.val1OentityDBID = val1OentityDBID;
    }

    public String getVal1FieldExpression() {
        return val1FieldExpression;
    }

    public void setVal1FieldExpression(String val1FieldExpression) {
        this.val1FieldExpression = val1FieldExpression;
    }
    
}
