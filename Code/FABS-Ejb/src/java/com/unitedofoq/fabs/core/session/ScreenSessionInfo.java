/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.session;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;

/**
 *
 * @author mmohamed
 */
@Entity
@FABSEntitySpecs(isAuditTrailed=false)
public class ScreenSessionInfo extends BaseEntity {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OUser loggedUser;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OScreen openedScreen;

    @Lob
    @Column(columnDefinition="BLOB")
    private byte[] dataMessage;

    private long screenMode;
    
    private String screenInstId;

    public long getScreenMode() {
        return screenMode;
    }

    public String getScreenModeDD() {
        return "ScreenSessionInfo_screenMode";
    }

    public void setScreenMode(long screenMode) {
        this.screenMode = screenMode;
    }

    public byte[] getDataMessage() {
        return dataMessage;
    }

    public void setDataMessage(byte[] dataMessage) {
        this.dataMessage = dataMessage;
    }

    public OUser getLoggedUser() {
        return loggedUser;
    }

    public String getLoggedUserDD() {
        return "ScreenSessionInfo_loggedUser";
    }

    public void setLoggedUser(OUser loggedUser) {
        this.loggedUser = loggedUser;
    }

    public OScreen getOpenedScreen() {
        return openedScreen;
    }

    public void setOpenedScreen(OScreen openedScreen) {
        this.openedScreen = openedScreen;
    }

    @Column(nullable=false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date openDate ;

    public Date getOpenDate() {
        return openDate;
    }

    public String getOpenDateDD() {
        return "ScreenSessionInfo_openDate";
    }

    public void setOpenDate(Date openDate) {
        this.openDate = openDate;
    }

    /**
     * @return the screenInstId
     */
    public String getScreenInstId() {
        return screenInstId;
    }

    /**
     * @param screenInstId the screenInstId to set
     */
    public void setScreenInstId(String screenInstId) {
        this.screenInstId = screenInstId;
    }

    
}
