/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.function;

import java.util.ArrayList;
import java.util.List;

import com.unitedofoq.fabs.core.security.user.ORoleMenuDTO;

/**
 *
 * @author Melad
 */
public class OMenueDTO {

    private long dbid;
    private long parentMenuDBID;
    private long moduleDBID;
    private String code;
    private String description;
    private String hierCode;
    private byte[] largeImageData;
    private String largeImageType;
    private String largeImageName;
    private long largeImageModuleDBID;
    private byte[] imageData;
    private String imageType;
    private String imageName;
    private long imageModuleDBID;
    private String name;
    private String nameTranslated;
    private boolean reserved;
    private int sortIndex;
    private List<OMenueDTO> childMenues;
    private List<OMenuFunctionDTO> menuFunctions;
    private List<ORoleMenuDTO> roleMenusDBID;
//    private List<OUserDTO> usersDTO;

    public OMenueDTO() {
    }

    public OMenueDTO(long dbid,
            long parentMenuDBID,
            long moduleDBID,
            String code,
            String description,
            String hierCode,
            String largeImageType,
            String largeImageName,
            long largeImageModuleDBID,
            String imageType,
            String imageName,
            long imageModuleDBID,
            String name,
            boolean reserved,
            int sortIndex,
            byte[] largeImageData,
            byte[] imageData) {
        this.dbid = dbid;
        this.parentMenuDBID = parentMenuDBID;
        this.moduleDBID = moduleDBID;
        this.code = code;
        this.description = description;
        this.hierCode = hierCode;
        this.largeImageType = largeImageType;
        this.largeImageName = largeImageName;
        this.largeImageModuleDBID = largeImageModuleDBID;
        this.imageType = imageType;
        this.imageName = imageName;
        this.imageModuleDBID = imageModuleDBID;
        this.name = name;
        this.reserved = reserved;
        this.sortIndex = sortIndex;
        this.largeImageData = largeImageData;
        this.imageData = imageData;
        childMenues=new ArrayList<OMenueDTO>();
        menuFunctions=new ArrayList<OMenuFunctionDTO>();
        roleMenusDBID=new ArrayList<ORoleMenuDTO>();
//        usersDTO=new ArrayList<OUserDTO>();
    }

    public long getDbid() {
        return dbid;
    }

    public void setDbid(long dbid) {
        this.dbid = dbid;
    }

    public long getParentMenuDBID() {
        return parentMenuDBID;
    }

    public void setParentMenuDBID(long parentMenuDBID) {
        this.parentMenuDBID = parentMenuDBID;
    }

    public long getModuleDBID() {
        return moduleDBID;
    }

    public void setModuleDBID(long moduleDBID) {
        this.moduleDBID = moduleDBID;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHierCode() {
        return hierCode;
    }

    public void setHierCode(String hierCode) {
        this.hierCode = hierCode;
    }

    public byte[] getLargeImageData() {
        return largeImageData;
    }

    public void setLargeImageData(byte[] largeImageData) {
        this.largeImageData = largeImageData;
    }

    public String getLargeImageType() {
        return largeImageType;
    }

    public void setLargeImageType(String largeImageType) {
        this.largeImageType = largeImageType;
    }

    public String getLargeImageName() {
        return largeImageName;
    }

    public void setLargeImageName(String largeImageName) {
        this.largeImageName = largeImageName;
    }

    public long getLargeImageModuleDBID() {
        return largeImageModuleDBID;
    }

    public void setLargeImageModuleDBID(long largeImageModuleDBID) {
        this.largeImageModuleDBID = largeImageModuleDBID;
    }

    public byte[] getImageData() {
        return imageData;
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public long getImageModuleDBID() {
        return imageModuleDBID;
    }

    public void setImageModuleDBID(long imageModuleDBID) {
        this.imageModuleDBID = imageModuleDBID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public List<OMenueDTO> getChildMenues() {
        return childMenues;
    }

    public void setChildMenues(List<OMenueDTO> childMenues) {
        this.childMenues = childMenues;
    }

    public List<ORoleMenuDTO> getRoleMenusDBID() {
        return roleMenusDBID;
    }

    public void setRoleMenusDBID(List<ORoleMenuDTO> roleMenusDBID) {
        this.roleMenusDBID = roleMenusDBID;
    }

    public List<OMenuFunctionDTO> getMenuFunctions() {
        return menuFunctions;
    }

    public void setMenuFunctions(List<OMenuFunctionDTO> menuFunctions) {
        this.menuFunctions = menuFunctions;
    }

//    public List<OUserDTO> getUsersDTO() {
//        return usersDTO;
//    }
//
//    public void setUsersDTO(List<OUserDTO> usersDTO) {
//        this.usersDTO = usersDTO;
//    }
}