/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.usermessage;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author bassem
 */
@Local
public interface UserMessageServiceRemote {

    public UserMessage getUserMessage(String userMessageName, OUser loggedUser);

    public UserMessage getUserMessage(String userMessageName, List<String> msgParams, OUser loggedUser);

    public UserMessage getUserMessage(String userMessageName, OUser loggedUser, String... msgParams);

    public ArrayList constructListFromStrings(String... args);

    public OFunctionResult clearUserMessageCache(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult onUserMessageCachedUpdate(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult onUserMessageCachedRemove(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult preUserMessageCachedUpdate(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
}
