/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.report;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.function.OFunction;

/**
 *
 * @author melsayed
 */
@Entity
@DiscriminatorValue(value = "REPORT")
@ParentEntity(fields="report")
@ChildEntity(fields={"functionImage", "menuFunction"})
@VersionControlSpecs(omoduleFieldExpression="report.omodule")
public class ReportFunction extends OFunction 
{
    // <editor-fold defaultstate="collapsed" desc="odataType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ODataType odataType = null;
    /**
     * @return the odataType
     */
    public ODataType getOdataType() {
        return odataType;
    }

    public String getOdataTypeDD() {
        return "ReportFunction_odataType";
    }

    /**
     * @param odataType the odataType to set
     */
    public void setOdataType(ODataType odataType) {
        this.odataType = odataType;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="report">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OReport report = null;
    /**
     * @return the report
     */
    public OReport getReport() {
        return report;
    }

    public String getReportDD() {
        return "ReportFunction_report";
    }

    /**
     * @param report the report to set
     */
    public void setReport(OReport report) {
        this.report = report;
    }
    //</editor-fold>
}
