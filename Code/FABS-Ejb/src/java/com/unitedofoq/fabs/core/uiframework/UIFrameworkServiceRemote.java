/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntityAction;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OMenu;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.session.ScreenSessionInfo;
import com.unitedofoq.fabs.core.setup.FABSSetupLite;
import com.unitedofoq.fabs.core.uiframework.orgchart.OrgChartFunctionLite;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalPage;
import com.unitedofoq.fabs.core.uiframework.portal.PortalPagePortletLite;
import com.unitedofoq.fabs.core.uiframework.portal.PortalPagePortletReceiverLite;
import com.unitedofoq.fabs.core.uiframework.screen.FormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.MultiLevelScreenLevel;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenFunctionDTO;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenInput;
import com.unitedofoq.fabs.core.uiframework.screen.SingleEntityScreen;
import com.unitedofoq.fabs.core.uiframework.screen.UDCScreenFunctionDTO;
import com.unitedofoq.fabs.core.uiframework.screen.dto.OScreenDTO;
import com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenDTMappingAttrDTO;
import com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenInputDTO;
import com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenOutputDTO;
import com.unitedofoq.fabs.core.uiframework.serverjob.resources.ServerJobDTO;
import com.unitedofoq.fabs.core.validation.FABSException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

/**
 *
 * @author mibrahim
 */
@Local
public interface UIFrameworkServiceRemote {

    public OScreen getScreenObject(String screenName, OUser loggedUser);

    /**
     * {@link UIFrameworkService#deductPrsnlzdOScreen(OScreen originalOScreen, OScreen copiedOScreen, OUser loggedUser)
     * }
     */
    public OFunctionResult deductPrsnlzdOScreen(OScreen originalOScreen, BaseEntity copiedOScreen, OUser loggedUser);

    public FormScreen getBasicDetailScreen(OEntity entity, OUser loggedOUser);

    /**
     * {@link UIFrameworkService#getDisplayedActions(OEntity) }
     */
    public ArrayList<OEntityAction> getDisplayedActions(OEntity oEntity, OUser loggedUser);

    /**
     *
     * @param oEntityDBID
     * @param loggedUser
     * @return
     */
    public ArrayList<OEntityActionDTO> getDisplayedDTOActions(long oEntityDBID, OUser loggedUser);

    /**
     * {@link UIFrameworkService#getFieldBallonOEntity(OEntity, List)}
     */
    public OEntity getFieldBallonOEntity(
            OEntity ownerEntity, List<ExpressionFieldInfo> expressionFieldInfos, OUser loggedUser);

    /**
     * {@link UIFrameworkService#getBalloonFieldIndex(java.lang.Class, java.util.List)}
     */
    public int getBalloonFieldIndex(
            Class ownerEntityClass, List<ExpressionFieldInfo> expressionFieldInfos);

    public OScreenDTO getFormScreenDTOObject(String screenName, OUser loggedUser);

    public List<OMenu> getRoleMenus(long roleDBID, OUser loggedUser);

    public java.lang.Class getFieldClass(String className, String fieldName, OUser loggedUser);

    public OFunctionResult validateUDC(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult validateOPortalPage(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    /**
     * Refer to
     * {@link UIFrameworkService#createRequiredScreensOnly(OEntity, OUser)}
     */
    public OFunctionResult createRequiredScreensOnly(OEntity oEntity, OUser loggedUser)
            throws FABSException;

    public OFunctionResult createDefaultTabularScreen(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult regenerateDDs(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public List<BaseEntity> getDBUnionDisplayRecords(BaseEntity parentObject, List<BaseEntity> displayedEntities, OUser loggedUser);

    /**
     * Refer to {@link UIFrameworkService#constructScreenDataLoadingUEConditionsOFR(ArrayList, OUser)
     * }
     */
    public OFunctionResult constructScreenDataLoadingUEConditionsOFR(
            List<String> initialConditions, OUser loggedUser);

    /**
     * Refer to {@link UIFrameworkService#getScreenDataLoadingUEConditions(OScreen, BaseEntity, ArrayList, ODataMessage, OUser)
     * }
     */
    public OFunctionResult getScreenDataLoadingUEConditions(
            OScreen oScreen, BaseEntity parentObject, List<String> initialConditions,
            ODataMessage screenDM, OUser loggedUser);

    /**
     * Refer to {@link UIFrameworkService#sampleScreenDataLoadingUserExit(ODataMessage, OFunctionParms, OUser)
     * }
     */
    public OFunctionResult sampleScreenDataLoadingUserExit(
            ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult saveMultiLevelScreenActOnEntity(
            ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser);

    public void saveScreenSessionInfo(com.unitedofoq.fabs.core.security.user.OUser user, com.unitedofoq.fabs.core.uiframework.screen.OScreen screen, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public void saveScreenSessionInfo(ScreenSessionInfo info, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public void destroyScreenSessionInfo(ScreenSessionInfo info, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public List<ScreenSessionInfo> getScreenSessionInfo(long userID, String screenInstId, OUser loggedUser);

    public boolean hasEntityHasChildren(String childEntityName, long entityDBID, String parentFldExp,
            OUser loggedUser);

    public OEntityDTO getFieldBallonOEntityDTO(
            OEntityDTO ownerEntity, List<ExpressionFieldInfo> expressionFieldInfos, OUser loggedUser);

    public List<MultiLevelScreenLevel> getTreeLevels(long levelScreendDBID, OUser loggedUser);

    public List<BaseEntity> getLevelEntites(String entityClassName, List<String> conditions,
            List<String> displayedFields, List<String> orderBy, OUser loggedUser);

    public List<BaseEntity> getSelfRecursiveLevelEntityChildren(BaseEntity nodeBaseEntity, List<String> conditions, OEntity childOEntity,
            List<String> fldExpInParentEntity,
            OUser loggedUser);

    public List<BaseEntity> getLevelEntityChildren(BaseEntity nodeBaseEntity, List<String> conditions, OEntity parentOEntity,
            OEntity childOEntity,
            List<String> fldExpInParentEntity,
            String displayedField,
            OUser loggedUser);

    /**
     * Refer to {@link UIFrameworkService#getDBIDAsScreenInput(List, boolean, OUser)
     * }
     */
    public ScreenInputDTO getDBIDAsScreenInput(List<ScreenInputDTO> screenInputs,
            boolean setForFilter, OUser loggedUser);

    /**
     * Refer to {@link UIFrameworkService#getVoidAsScreenInput(List, OUser) }
     */
    public ScreenInputDTO getVoidAsScreenInput(
            List<ScreenInputDTO> screenInputs, OUser loggedUser);

    /**
     * Refer to {@link UIFrameworkService#getParentAsScreenInput(String, List, boolean, OUser)
     * }
     */
    public ScreenInput getParentAsScreenInput(String parentFieldName,
            List<ScreenInput> screenInputs, boolean setForFilter, OUser loggedUser);

    /**
     * Refer to {@link UIFrameworkService#getDBIDListAsScreenInput(List, OUser)
     * }
     */
    public ScreenInputDTO getDBIDListAsScreenInput(
            List<ScreenInputDTO> screenInputs, OUser loggedUser);

    /**
     * Refer to {@link UIFrameworkService#getEntityParentFromODMInput(ODataMessage, ScreenInput, SingleEntityScreen, OUser)
     * }
     */
    public OFunctionResult getEntityParentFromODMInput(
            ODataMessage odm, ScreenInputDTO input, OEntityDTO screenEntityDTO, OUser loggedUser);

    public OFunctionResult createDefaultTabularScreenFunction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult createDefaultFormScreenFunction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public FormScreen getBasicDetailScreenByOEntityDTO(OEntityDTO entity, OUser loggedUser);

    public OFunctionResult getParentFromParentDBID(
            OEntityDTO actOnEntity, long parentDBID, OUser loggedUser);

    public OFunctionResult getParentFromParentDBIDDTO(
            OEntityDTO actOnEntity, long parentDBID, OUser loggedUser);

    /**
     * Refer to {@link UIFrameworkService#getParentFromParentDBID(OEntity, long, OUser)
     * }
     */
    public OFunctionResult getParentFromParentDBID(
            OEntity actOnEntity, long parentDBID, OUser loggedUser);

    /**
     * Refer to {@link UIFrameworkService#createTabularScreenDefaultStuff(ODataMessage, OFunctionParms, OUser)
     * }
     */
    public OFunctionResult createTabularScreenDefaultStuff(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public Object getCachedPanel(String screenName, String instance,
            String panelName, OUser loggedUser);

    public void clearScreensCache(OUser loggedUser);

    public void clearScreenCache(String name, OUser loggedUser);

    public void addPanelToCache(String screenName, String instance, String panelName,
            Object panelGrid, OUser loggedUser);

    public void close(String screenName, String instance, OUser loggedUser);

    public Object getCachedComponent(String componentId, OUser loggedUser);

    public void addComponentToCache(String componentId, Object component, OUser loggedUser);

    public void clearComponentsCache(OUser loggedUser);

    public OFunctionResult clearScreenCache(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult ServerJobUserExit(
            ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult testServerJob(
            ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult addOPortletPage(
            long oPortalPageDBID, OUser loggedUser, String plid);

    public java.util.List<com.unitedofoq.fabs.core.uiframework.portal.PortalPagePortletLite> getPortalPagePortlets(long portalPageDBID, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.uiframework.portal.OPortalPageLite getPortalPage(long portalPageDBID, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.uiframework.screen.OScreenLite getOScreenLite(long screenDBID, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public List<PortalPagePortletLite> getToBeRenderList(long screenDBID, OUser loggedUser);

    public List<PortalPagePortletReceiverLite> getPortletReceiversList(long senderDBID, OUser loggedUser);

    public OrgChartFunctionLite getOrgChartFunctionLite(String code, OUser loggedUser);

    public FABSSetupLite getFABSSetupLite(String key, OUser loggedUser);

    public com.unitedofoq.fabs.core.uiframework.screen.dto.OScreenDTO getScreenDTOObject(java.lang.String screenName, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public java.util.ArrayList<com.unitedofoq.fabs.core.entitysetup.dto.OObjectBriefInfoFieldDTO> getBriefInfoFieldDTOs(long objectDBID, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public ArrayList<ScreenInputDTO> getScreenInputDTOs(long screenDBID, OUser loggedUser);

    public ScreenInputDTO getScreenInputDTO(long inputDBID, OUser loggedUser);

    public ScreenDTMappingAttrDTO getScreenInputAttMappingDTO(long inputAttrDBID, OUser loggedUser);

    public ScreenInput getScreenInputIfDataTypeDBIDOrVoid(long dbid, OUser loggedUser);

    public ArrayList<ScreenOutputDTO> getScreenOuputDTOs(long screenDBID, OUser loggedUser);

    public OPortalPage getOportalPage(long dbid, OUser loggedUser);

    public ArrayList<ServerJobDTO> getServerJobDTOs(OUser loggedUser);

    public ServerJobDTO getServerJobDTO(OUser loggedUser, long scheduleExpressionDBID);
//
//    public List<OTenantDTO> getTenantsDTOList();
//
//    public List<ORoleDTO> getRolesDTOList(OTenantDTO oTenantDTO,OUser loggedUser);
//
//    public List<OMenueDTO> getMenusDTOs(OUser loggedUser);

    public List<String> getViewsName(OUser user);

    public List<String> getFiledsNamesInView(String viewName, OUser user);

    public Map<String, String> getFieldsNamesAndDataTypeInView(String viewName, OUser user);

    public List<ScreenFunctionDTO> getScreenFunctionListDTO(OUser loggedUser);

    public List<UDCScreenFunctionDTO> getUDCScreenFunctionListDTO(OUser loggedUser);

    public List<String> getLoginNamesForAllUsersInTenant(OUser user);

    public OFunctionResult userExit1(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult userExit2(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult validateOPortalPageDeletion(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

}
