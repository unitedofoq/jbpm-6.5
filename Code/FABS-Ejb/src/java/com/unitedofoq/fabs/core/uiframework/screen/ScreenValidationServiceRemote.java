/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.screen;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;




/**
 *
 * @author melsayed
 */
@Local
public interface ScreenValidationServiceRemote {
    public OFunctionResult validateAllOScreens(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateOScreen(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateOScreenInputs(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateOScreenOutputs(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateOScreenScreenFunctions(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateOScreenScreenFieldExpression(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateEntityScreens(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateOscreenScreenFields(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateAllOrNonFieldsInGroups(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult preSaveValidateOscreenScreenFields(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateOscreenBasicDetail(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateOutBoxUniqness(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
}
