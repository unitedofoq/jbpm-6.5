/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen.dto;

import java.io.Serializable;

/**
 *
 * @author root
 */
public class OScreenDTO implements Serializable{
    //<editor-fold desc="static fields" defaultstate="collapsed">
      
    public final static long TSM_DEFAULT             = 14;    
    public final static long TSM_YESNO               = 15;    
    public final static long TSM_APPROVEREJECT       = 16;    
    public final static long TSM_APPROVEREJECTREVIEW = 258;    
    public final static long TSM_DISMISS             = 10;    
    public final static long SM_NOMODE               = 0;    
    public final static long SM_ADD                  = 17;    
    public final static long SM_EDIT                 = 18;    
    public final static long SM_VIEW                 = 19;    
    public final static long SM_MANAGED              = 20;    
    public final static long SM_LOOKUP               = 21;    
    public final static long SM_DELETE               = 22;    
    public final static long HAT_USER                = 265;    
    public final static long HAT_SELF                = 266;    
    public final static long HAT_MANAGER             = 267;
    //</editor-fold>
    protected long dbid;
    protected String inlineHelp;
    protected String name;
    protected String header;
    protected String viewPage;
    protected boolean showHeader;
    protected boolean showActiveOnly;
    protected boolean prsnlzd;
    protected long prsnlzdOriginal_DBID;
    protected long prsnlzdUser_DBID;
    protected long screenFilter_DBID;

    public long getScreenFilter_DBID() {
        return screenFilter_DBID;
    }

    public void setScreenFilter_DBID(long screenFilter_DBID) {
        this.screenFilter_DBID = screenFilter_DBID;
    }
    

    public long getPrsnlzdOriginal_DBID() {
        return prsnlzdOriginal_DBID;
    }

    public void setPrsnlzdOriginal_DBID(long prsnlzdOriginal_DBID) {
        this.prsnlzdOriginal_DBID = prsnlzdOriginal_DBID;
    }

    public long getPrsnlzdUser_DBID() {
        return prsnlzdUser_DBID;
    }

    public void setPrsnlzdUser_DBID(long prsnlzdUser_DBID) {
        this.prsnlzdUser_DBID = prsnlzdUser_DBID;
    }    

    public String getInlineHelp() {
        return inlineHelp;
    }

    public void setInlineHelp(String inlineHelp) {
        this.inlineHelp = inlineHelp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getViewPage() {
        return viewPage;
    }

    public void setViewPage(String viewPage) {
        this.viewPage = viewPage;
    }

    public boolean isShowHeader() {
        return showHeader;
    }

    public void setShowHeader(boolean showHeader) {
        this.showHeader = showHeader;
    }

    public boolean isShowActiveOnly() {
        return showActiveOnly;
    }

    public void setShowActiveOnly(boolean showActiveOnly) {
        this.showActiveOnly = showActiveOnly;
    }

    public long getDbid() {
        return dbid;
    }

    public void setDbid(long dbid) {
        this.dbid = dbid;
    }

    public boolean isPrsnlzd() {
        return prsnlzd;
    }

    public void setPrsnlzd(boolean prsnlzd) {
        this.prsnlzd = prsnlzd;
    }

    public OScreenDTO(long dbid, String inlineHelp, String name, String header, String viewPage,
            boolean showHeader, boolean showActiveOnly, boolean prsnlzd, long prsnlzdOriginal_DBID,
            long prsnlzdUser_DBID, long screenFilter_DBID) {
        this.dbid = dbid;
        this.inlineHelp = inlineHelp;
        this.name = name;
        this.header = header;
        this.viewPage = viewPage;
        this.showHeader = showHeader;
        this.showActiveOnly = showActiveOnly;
        this.prsnlzd = prsnlzd;
        this.prsnlzdOriginal_DBID = prsnlzdOriginal_DBID;
        this.prsnlzdUser_DBID = prsnlzdUser_DBID;
        this.screenFilter_DBID = screenFilter_DBID;
    }
}
