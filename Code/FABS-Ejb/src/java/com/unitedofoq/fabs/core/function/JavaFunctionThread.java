/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.function;

import java.lang.reflect.Method;

import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;

/**
 *
 * @author arezk
 */
public class JavaFunctionThread implements Runnable {

    Method functionMethod;
    Object javaFunctionClsObj;
    Object[] args;
    ServerJob serverJob;
    OUser loggedUser;
    EntitySetupServiceRemote entitySetupService;
    UDC notificationType;
    OModule module;

    public JavaFunctionThread(OUser loggedUser,
                                             EntitySetupServiceRemote entitySetupService,
                                             ServerJob serverJob,
                                             Method method,
                                             Object javaFunctionClsObj,
                                             Object[] args,
                                             UDC notificationType,
                                             OModule module) {
        this.functionMethod = method;
        this.javaFunctionClsObj = javaFunctionClsObj;
        this.args = args;
        this.serverJob = serverJob;
        this.loggedUser = loggedUser;
        this.entitySetupService = entitySetupService;
        this.notificationType = notificationType;
        this.module = module;
    }
    
    @Override
    public void run() {
        entitySetupService.runServerJob(module, notificationType, serverJob, 
                functionMethod, javaFunctionClsObj, loggedUser, args);
    }
}
