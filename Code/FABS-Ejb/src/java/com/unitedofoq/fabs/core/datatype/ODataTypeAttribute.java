package com.unitedofoq.fabs.core.datatype;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;

@Entity
@ParentEntity(fields = {"oDataType"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class ODataTypeAttribute extends BaseEntity{

    private Boolean multiValue = Boolean.FALSE;
    public String getMultiValueDD(){
        return "ODataTypeAttribute_multiValue";
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OEntity oentity;
    public String getOentityDD(){
        return "ODataTypeAttribute_oEntity";
    }

    private String primitiveTypeClassPath;
    public String getPrimitiveTypeClassPathDD(){
        return "ODataTypeAttribute_primitiveTypeClassPath";
    }

    @Column(nullable=false)
    private int seq;
    public String getSeqDD() {
        return "ODataTypeAttribute_seq" ;
    }

    @Translatable(translationField="descriptionTranslated")
    private String description;
    
    @Translation(originalField="description")
    @Transient
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    public String getDescriptionTranslatedDD(){
        return "ODataTypeAttribute_description";
    }

    @Column(nullable=false)
    private String name;
    public String getNameDD() {
        return "ODataTypeAttribute_name" ;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private ODataType oDataType;
    public String getODataTypeDD() {
        return "ODataTypeAttribute_oDataType" ;
    }

    /**
     * @return the multiValue
     */
    public Boolean getMultiValue() {
        return multiValue;
    }

    /**
     * @param multiValue the multiValue to set
     */
    public void setMultiValue(Boolean multiValue) {
        this.multiValue = multiValue;
    }

    /**
     * @return the oEntity
     */
    public OEntity getOentity() {
        return oentity;
    }

    /**
     * @param oEntity the oEntity to set
     */
    public void setOentity(OEntity oEntity) {
        this.oentity = oEntity;
    }

    /**
     * @return the primitiveTypeClassPath
     */
    public String getPrimitiveTypeClassPath() {
        return primitiveTypeClassPath;
    }

    /**
     * @param primitiveTypeClassPath the primitiveTypeClassPath to set
     */
    public void setPrimitiveTypeClassPath(String primitiveTypeClassPath) {
        this.primitiveTypeClassPath = primitiveTypeClassPath;
    }

    /**
     * @return the seq
     */
    public int getSeq() {
        return seq;
    }

    /**
     * @param seq the seq to set
     */
    public void setSeq(int seq) {
        this.seq = seq;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the oDataType
     */
    public ODataType getODataType() {
        return oDataType;
    }

    /**
     * @param oDataType the oDataType to set
     */
    public void setODataType(ODataType oDataType) {
        this.oDataType = oDataType;
    }
}
