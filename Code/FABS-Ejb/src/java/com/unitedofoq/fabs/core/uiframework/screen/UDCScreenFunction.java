/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.screen;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.udc.UDCType;

/**
 *
 * @author mmohamed
 */
@Entity
@DiscriminatorValue(value = "UDCSCREEN")
@ParentEntity(fields={"oscreen"})
@VersionControlSpecs(omoduleFieldExpression="oscreen.omodule")
public class UDCScreenFunction extends ScreenFunction {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    UDCType type;

    public UDCType getType() {
        return type;
    }

    public String getTypeDD() {
        return "UDCScreenFunction_type";
    }

    public void setType(UDCType type) {
        this.type = type;
    }
}
