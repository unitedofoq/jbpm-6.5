package com.unitedofoq.fabs.core.process;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;

@Entity
@ParentEntity(fields={"oprocess"})
@ChildEntity(fields={"processFunctions"})
@VersionControlSpecs(versionControlType= VersionControlSpecs.VersionControlType.Parent)
public class OProcessAction extends BaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="displayed">
    @Column(nullable=false)
    private boolean displayed;

    public boolean isDisplayed() {
        return displayed;
    }

    public void setDisplayed(boolean displayed) {
        this.displayed = displayed;
    }
    
    public String getDisplayedDD(){
        return "OProcessAction_displayed" ;
    }
    // </editor-fold>

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private OProcess oprocess;

    @Column(nullable=false)
    private Integer sortIndex;
    public String getSortIndexDD(){
        return "OProcessAction_sortIndex" ;
    }

    @Translatable(translationField="nameTranslated")
    private String name;
    public String getNameTranslatedDD(){
        return "OProcessAction_name";
    }

    @Translation(originalField="name")
   @Transient
    private String nameTranslated;
    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }



    /**
     * @return the oprocess
     */
    public OProcess getOprocess() {
        return oprocess;
    }

    /**
     * @param oprocess the oprocess to set
     */
    public void setOprocess(OProcess oprocess) {
        this.oprocess = oprocess;
    }

    /**
     * @return the sortIndex
     */
    public Integer getSortIndex() {
        return sortIndex;
    }

    /**
     * @param sortIndex the sortIndex to set
     */
    public void setSortIndex(Integer sortIndex) {
        this.sortIndex = sortIndex;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    // <editor-fold defaultstate="collapsed" desc="processFunctions">
    @OneToMany(mappedBy="oprocessAction")
    @JoinColumn
    private List<ProcessFunction> processFunctions ;

    public List<ProcessFunction> getProcessFunctions() {
        return processFunctions;
    }

    public void setProcessFunctions(List<ProcessFunction> processFunctions) {
        this.processFunctions = processFunctions;
    }
    public String getProcessFunctionsDD() {
        return "OProcessAction_processFunctions";
    }
    // </editor-fold>
}
