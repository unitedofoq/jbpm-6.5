/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitybase;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.unitedofoq.fabs.core.function.OFunctionResult;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.validation.FABSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used to loop on all the children tree of a specific entity
 * (@link #grandParent) including the grandParent itself.
 * <br> To just get the list of entities, call {@link #getTreeList()}
 * <br> To do special processing on the entities, you need to provide your own
 * {@link #callBackMethod} and call {@link #getTree()}
 * <br> Children are known as they are annotated by @ChildEntity
 *
 * @author arezk
 */
public class GetEntityChildrenTree {

    /**
     * Object that is the owner of the callBackMethod {@link #callBackMethod}
     */
    private Object callerObject;
    private OEntityManagerRemote oem;

    final static Logger logger = LoggerFactory.getLogger(GetEntityChildrenTree.class);

    public Object getCallerObject() {
        return callerObject;
    }

    public void setCallerObject(Object callerObject) {
        this.callerObject = callerObject;
    }
    /**
     * Call back method to be called for every entity in the
     * {@link #grandParent} parent/child tree (including the
     * {@link #grandParent} itself)
     *
     * <br>
     * If it's null, then, it's required only to list the entity & children;
     * list will be found in {@link #entityChildrenTree}
     *
     * <br>
     * <br> <b>Method signature should be as following</b>:
     * <br> <b>Access</b>: public
     * <br> <b>Return</b>: {@link BaseEntity}, parent Entity
     * <br> If the method returned null, then the children tree loop stops
     * <br> <b>Arguments</b>:
     * <br> {@link BaseEntity} entity: is the entity in the tree
     * <br> String entityFieldExpression: is the field expression of the entity,
     * not including the grandParent
     * <br> GetEntityChildrenTree gect: the instance calling the method
     */
    private Method callBackMethod;

    public Method getCallBackMethod() {
        return callBackMethod;
    }

    public void setCallBackMethod(Method callBackMethod) {
        this.callBackMethod = callBackMethod;
    }
    /**
     * The entity for which we get all the child entity tree
     */
    private BaseEntity grandParent;

    public BaseEntity getGrandParent() {
        return grandParent;
    }

    public void setGrandParent(BaseEntity grandParent) {
        this.grandParent = grandParent;
    }
    private boolean loadNotLoadedChild;

    public boolean isLoadNotLoadedChild() {
        return loadNotLoadedChild;
    }

    public void setLoadNotLoadedChild(boolean loadNotLoadedChild) {
        this.loadNotLoadedChild = loadNotLoadedChild;
    }
    Object custom;

    public Object getCustom() {
        return custom;
    }

    public void setCustom(Object custom) {
        this.custom = custom;
    }
    OUser loggedUser;

    public OUser getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(OUser loggedUser) {
        this.loggedUser = loggedUser;
    }

    /**
     * Class Constructor
     *
     * @param grandParent Refer to {@link #grandParent}
     * @param callBackMethod Is null when calling {@link #getTreeList()}; and
     * must be not null with signature of {@link #callBackMethod} when calling
     * {@link #getTree()}
     * @param callerObject Refer to {@link #callerObject}
     */
    public GetEntityChildrenTree(BaseEntity grandParent, Method callBackMethod, Object callerObject, OEntityManagerRemote oEntityManager) {
        this.grandParent = grandParent;
        this.callBackMethod = callBackMethod;
        this.callerObject = callerObject;
        this.oem = oEntityManager;

    }

    /**
     * Loops on all the grandParent {@link #grandParent} children (including
     * itself) and calls the callBackMethod {@link #callBackMethod} for every
     * entity
     */
    public void getTree()
            throws FABSException {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">

        //logger.trace("Executing Callback Function. Entity: {}, Callback method", grandParent.getClassName(), callBackMethod.getName());
        // </editor-fold>
        getChildren(grandParent, null, "");
        logger.debug("Return");
    }

    private void getChildren(
            BaseEntity entity, BaseEntity entityParent, String entityExpression/*used for logging only*/)
            throws FABSException {
        logger.trace("Entering");
        // Call callback method for the passed entity
        List<String> childrenList;
        try {
            if (entityParent == null) {
                List<String> parentFields = BaseEntity.getParentEntityFieldName(entity.getClass().getName());
                if (parentFields != null && !parentFields.isEmpty()) {
                    entityParent = (BaseEntity) entity.invokeGetter(parentFields.get(0));
                }
            }
            entityParent = (BaseEntity) callBackMethod.invoke(
                    callerObject, entity, entityParent, entityExpression, this);
            if (entityParent == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                if (null != entityParent) {
                    logger.trace("Process Stops By CallBack Method. Entity: {}, Entity Experession: {}", entityParent.getClassName(), entityExpression);
                }
                // </editor-fold>
                logger.debug("Returning");
                return;
            }

            childrenList = BaseEntity.getChildEntityFieldName(entity.getClass().getName());

            for (String childFieldName : childrenList) {
                if (childFieldName.contains(".")) {
                    logger.warn("Child Fields Should Not Contain '.' ");
                    continue;
                }

                List<ExpressionFieldInfo> fieldInfos = BaseEntity.parseFieldExpression(entity.getClass(), childFieldName);

                if (fieldInfos.size() != 1) {
                    logger.warn("Error in Child Field Expression");
                    continue;
                }

                ExpressionFieldInfo fieldInfo = fieldInfos.get(0);

                Class childFieldClassType = BaseEntity.getFieldObjectType(fieldInfo.field);

                List<String> parentName = BaseEntity.getParentEntityFieldName(childFieldClassType.getName());

                if (parentName == null) {
                    if (null != childFieldClassType) {
                        logger.warn("Parent Name Is Null Wrong Parent Child Entity In Class {} Entity: {}", childFieldClassType.getName(), entity);
                    }
                    continue;
                }
                String fieldName = "";
                for (int i = 0; i < parentName.size(); i++) {
                    Class fieldClass = BaseEntity.getClassField(childFieldClassType, parentName.get(i)).getType();
                    Class entityClass = entityParent.getClass();
                    boolean done = false;
                    while (entityClass != null) {
                        if (fieldClass == entityClass) {
                            fieldName = parentName.get(i);
                            done = true;
                            break;
                        }
                        entityClass = entityClass.getSuperclass();
                    }
                    if (done) {
                        break;
                    }
                }
                List<String> conditions = new ArrayList<String>();
                conditions.add(fieldName + ".dbid = " + entity.getDbid());
                conditions.add(OEntityManager.CONFCOND_GET_ACTIVEANDINACTIVE);

                List<BaseEntity> childObjects = oem.loadEntityList(childFieldClassType.getSimpleName(), conditions, null, null, loggedUser);
                        //entity.invokeGetter(childFieldName);

                //Object childObject = entity.invokeGetter(childFieldName);
                if (childObjects == null || childObjects.isEmpty()) // Child Object is null, nothing to porcess
                {
                    continue;
                }

                String childExpression = entityExpression + "." + childFieldName;
                Field childField = BaseEntity.getClassField(entity.getClass(), childFieldName);
                if (childField == null) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    if (null != entity) {
                        logger.warn("Child Field Not Found In Parent. Entity Class: {}, Child Field Name: {}", entity.getClassName(), childFieldName);
                    }
                    // </editor-fold>
                    continue;
                }
                if (BaseEntity.getFieldType(childField).equals(List.class)) {
                    List<BaseEntity> childObjectList = (List<BaseEntity>) childObjects;
                    if (BaseEntity.getFieldObjectType(childField).getName().contains("unitedofoq")) {

                        /* The following Condition is to check if the list is Indirect List or not..
                         * the size check is to make sure that the following list has no values;
                         * If the size is more than 0 the values will be get inside the For loop                         
                         */
                        if (!(childObjectList.getClass().getName().contains("IndirectList") && childObjectList.size() == 0)) {
                            for (BaseEntity child : childObjectList) {
                                // call getChildren with the entity (now as a parent), and its child:
                                getChildren(child, entityParent, childExpression);
                            }
                        }
                    } else {
                        // <editor-fold defaultstate="collapsed" desc="Log">
                        if (null != entityParent) {
                            logger.warn("Child entity is not unitedofoq, something is wrong. Entity: {}, Child: {}", entityParent.getClassName(), childObjects);
                        }
                        // </editor-fold>
                    }
                } else if (childObjects.getClass().getName().contains("IndirectList")) {
                    // Do nothing
                } else {
                    // child is not a list
                    if (childObjects.get(0).getClass().getName().contains("unitedofoq")) {
                        getChildren((BaseEntity) childObjects.get(0), entityParent, childExpression);
                    } else {
                        // <editor-fold defaultstate="collapsed" desc="Log">
                        if (null != entityParent) {
                            logger.warn("Child is not a list, and is not a unitedofoq object. Entity: {}, Child: {}", entityParent.getClassName(), childObjects);
                        }
                        // </editor-fold>
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown : ", ex);
            FABSException fex = new FABSException(loggedUser, null, grandParent, ex);
            logger.debug("Returning");
            return;
        }
    }

    /**
     * Callback method-specific parameter passed to all the method instances
     * within the gect object.
     */
    public Object methodSpecifcParm;

    /**
     * Callback method-specific parameter1 passed to all the method instances
     * within the gect object.
     */
    public Object methodSpecifcParm1;

    /**
     * Callback method-specific parameter2 passed to all the method instances
     * within the gect object.
     */
    public Object methodSpecifcParm2;

    /**
     * Accumulated function result of the call back function if needed
     */
    public OFunctionResult functionResult = new OFunctionResult();

    /**
     * Used if it's required only to list the entity & children without any
     * specific callback function.
     * <br>Mainly used by {@link #getTreeList()}
     */
    public ArrayList<BaseEntity> entityChildrenTree;

    /**
     * Return a list passed to getDeclaredMethod() when accessing any
     * {@link #callBackMethod}
     */
    public static Class[] getCallBackMethodTypes() {
        Class[] types = new Class[4];
        types[0] = BaseEntity.class;
        types[1] = BaseEntity.class;
        types[2] = String.class;
        types[3] = GetEntityChildrenTree.class;
        return types;
    }

    /**
     * Lists all the parent entity and it's children tree in
     * {@link #entityChildrenTree}
     * <br>Calls {@link #listAllCallbackMethod} as a callback function
     *
     * @throws FABSException with {@link #grandParent} as 'concernedEntity';
     * {@link #entityChildrenTree} as 'relatedObjects'; exception as
     * 'originatingJavaException'
     */
    public void getTreeList()
            throws FABSException {
        Method listAllCallback = null;
        try {
            listAllCallback = getClass().getDeclaredMethod("listAllCallbackMethod",
                    GetEntityChildrenTree.getCallBackMethodTypes());
            GetEntityChildrenTree gect = new GetEntityChildrenTree(
                    grandParent, listAllCallback, this, oem);
            gect.setLoggedUser(loggedUser);

            // Initialize 'entityChildrenTree' to be used by 'listAllCallbackMethod'
            gect.entityChildrenTree = new ArrayList<BaseEntity>();
            gect.getTree();

            // Set the result into object 'entityChildrenTree'
            this.entityChildrenTree = gect.entityChildrenTree;
        } catch (Exception ex) {
            FABSException fex = new FABSException(loggedUser, null, grandParent, ex,
                    (List) entityChildrenTree);
        }
    }

    /**
     * Fills the {@link #entityChildrenTree} of the passed 'gect'.
     * <br> Mainly called by {@link #getTreeList()}
     *
     * @param entity
     * <br>Entity of the Entity Children Tree (including parent) Refer to
     * {@link #callBackMethod}
     * @param parent Not Used
     * @param entityFieldExpression Not Used
     * @param gect
     * @return Child Entity 'entity'
     */
    public BaseEntity listAllCallbackMethod(
            BaseEntity entity, BaseEntity parent, String entityFieldExpression, GetEntityChildrenTree gect) {
        if (gect.entityChildrenTree == null) {
            gect.entityChildrenTree = new ArrayList<BaseEntity>();
        }
        if (!gect.entityChildrenTree.contains(entity)) {
            gect.entityChildrenTree.add(entity);
        }
        return entity; // Always Continue
    }
}
