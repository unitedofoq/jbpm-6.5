/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.portal;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;

/**
 *
 * @author tarzy
 */
@Entity
@ParentEntity(fields={"sender"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class PortalPagePortletReceiver extends BaseEntity 
{
    // <editor-fold defaultstate="collapsed" desc="sender">
    /**
     * Portlet from which this portlet receives messages
     */
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    PortalPagePortlet sender ;

    public PortalPagePortlet getSender() {
        return sender;
    }

    public void setSender(PortalPagePortlet senderPortlet) {
        this.sender = senderPortlet;
    }

    public String getSenderDD() {
        return "PortalPagePortletReceiver_senderPortlet";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="receiver">
    /**
     * Portlet which receives messages
     */
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    PortalPagePortlet receiver ;
    public PortalPagePortlet getReceiver() {
        return receiver;
    }
    public void setReceiver(PortalPagePortlet receiver) {
        this.receiver = receiver;
    }
    public String getReceiverDD() {
        return "PortalPagePortletReceiver_receiver";
    }
    // </editor-fold>
}
