/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.data;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ComputedField;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;

import com.unitedofoq.fabs.core.udc.UDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author bassem
 */
@MappedSuperclass
public class PersonBase extends BaseEntity
{ 
    // <editor-fold defaultstate="collapsed" desc="name Management">

    @Transient
    @Translation(originalField = "name")
    private String nameTranslated;
    final static Logger logger = LoggerFactory.getLogger(PersonBase.class);
    public String getNameTranslatedDD() {
        return "Person_name";
    }

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
        parseName(true);
    }
    
    @Transient
    @ComputedField(filterLHSExpression = {"firstName", "middleName", "lastName"})
    @Translatable(translationField = "nameTranslated")
    private String name;

    public String getNameDD() {
        return "Person_name";
    }

    public void constructName(boolean translated) {
        if (translated) {
            logger.trace("Constructing. Middle Name: {} Name: {}", middleNameTranslated, nameTranslated);
            nameTranslated = "";
            if (firstNameTranslated != null) {
                if (!firstNameTranslated.equals("")) {
                    nameTranslated += firstNameTranslated;
                }
            }
            if (middleNameTranslated != null) {
                if (!middleNameTranslated.equals("")) {
                    if (nameTranslated.equals("")) {
                        nameTranslated = middleNameTranslated;
                    } else {
                        nameTranslated += " " + middleNameTranslated;
                    }
                }
            }
            if (lastNameTranslated != null) {
                if (!lastNameTranslated.equals("")) {
                    if (nameTranslated.equals("")) {
                        nameTranslated = lastNameTranslated;
                    } else {
                        nameTranslated += " " + lastNameTranslated;
                    }
                }
            }
        } else {
            logger.trace("Constructing. Middle Name: {} Name: {}", middleNameTranslated, nameTranslated);
            name = "";
            if (firstName != null) {
                if (!firstName.equals("")) {
                    name += firstName;
                }
            }
            if (middleName != null) {
                if (!middleName.equals("")) {
                    if (name.equals("")) {
                        name = middleName;
                    } else {
                        name += " " + middleName;
                    }
                }
            }
            if (lastName != null) {
                if (!lastName.equals("")) {
                    if (name.equals("")) {
                        name = lastName;
                    } else {
                        name += " " + lastName;
                    }
                }
            }
        }
    }

    public void parseName(boolean translated) {
        if (translated) {
            logger.trace("parsing. Middle Name: {} Name: {}", middleNameTranslated, nameTranslated);
            if (nameTranslated != null) {
                if (!nameTranslated.equals("")) {
                    int firstIndexOfSpace = nameTranslated.indexOf(" ");
                    int lastIndexOfSpace = nameTranslated.lastIndexOf(" ");
                    if (firstIndexOfSpace != -1) {
                        firstNameTranslated = (null == nameTranslated.substring(0, firstIndexOfSpace))?
                                "":nameTranslated.substring(0, firstIndexOfSpace);
                        if (lastIndexOfSpace != firstIndexOfSpace) {
                            lastNameTranslated =  (null == nameTranslated.substring(lastIndexOfSpace + 1))?
                                "":nameTranslated.substring(lastIndexOfSpace + 1);
                            middleNameTranslated = (null == nameTranslated.substring(firstIndexOfSpace + 1, lastIndexOfSpace))?
                                "": nameTranslated.substring(firstIndexOfSpace + 1, lastIndexOfSpace);
                        } else {
                            lastNameTranslated = (null == nameTranslated.substring(
                                    firstIndexOfSpace + 1, nameTranslated.length()))?
                                    "":nameTranslated.substring(firstIndexOfSpace + 1, nameTranslated.length());
                        }
                    } else {
                        firstNameTranslated = "";
                        middleNameTranslated = "";
                        lastNameTranslated = nameTranslated;
                    }
                }
            }
        } else {
            logger.trace("Parsing. Middle Name: {} Name: {}", middleNameTranslated, nameTranslated);
            if (name != null) {
                if (!name.equals("")) {
                    int firstIndexOfSpace = name.indexOf(" ");
                    int lastIndexOfSpace = name.lastIndexOf(" ");
                    if (firstIndexOfSpace != -1) {
                        firstName = name.substring(0, firstIndexOfSpace);
                        if (lastIndexOfSpace != firstIndexOfSpace) {
                            lastName = name.substring(lastIndexOfSpace + 1);
                            middleName = name.substring(firstIndexOfSpace + 1, lastIndexOfSpace);
                        } else {
                            lastName = name.substring(firstIndexOfSpace + 1, name.length());
                        }
                    } else {
                        firstName = "";
                        middleName = "";
                        lastName = name;
                    }
                }
            }
        }
    }
    
    // </editor-fold>

    @Override
    protected void PreUpdate() {
        super.PreUpdate();
        // WARNING: Never call parseName, as it causes the full entity to be reloaded
        // from the database before saving the owner employee, for unknown reasons,
        // and it's already parsed in "setName()"
    }
    @Override
    protected void PostLoad() {
        super.PostLoad();
        constructName(false);
    }
    @Override
    protected void PrePersist() {
        super.PrePersist();
        // NOTE: No need to call parseName, as it should be parsed in "setName()"
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
        constructName(false);
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
        constructName(false);
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
        constructName(false);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        parseName(false);
    }

    @Column(nullable=false)
    @Translatable(translationField="firstNameTranslated")
    private String firstName = "";
    public String getFirstNameTranslatedDD(){
        return "Person_firstName";
    }
    
    @Transient
    @Translation(originalField="firstName")
    private String firstNameTranslated;

    public String getFirstNameTranslated() {
        return firstNameTranslated;
    }

    public void setFirstNameTranslated(String firstNameTranslated) {
        this.firstNameTranslated = firstNameTranslated;
        constructName(true);
    }
    
    @Translatable(translationField="middleNameTranslated")
    private String middleName;
    
    @Transient
    @Translation(originalField="middleName")
    private String middleNameTranslated;

    public String getMiddleNameTranslated() {
        return middleNameTranslated;
    }

    public void setMiddleNameTranslated(String middleNameTranslated) {
        this.middleNameTranslated = middleNameTranslated;
        constructName(true);
    }
    
    public String getMiddleNameTranslatedDD(){
        return "Person_middleName";
    }
    
    @Column(nullable=false)
    @Translatable(translationField="lastNameTranslated")
    private String lastName = "";
    
    public String getLastNameTranslatedDD(){
        return "Person_lastName";
    }
    
    @Transient
    @Translation(originalField="lastName")
    private String lastNameTranslated;

    public String getLastNameTranslated() {
        return lastNameTranslated;
    }

    public void setLastNameTranslated(String lastNameTranslated) {
        this.lastNameTranslated = lastNameTranslated;
        constructName(true);
    }

    @Translatable(translationField="addressTranslated")
    private String address;
    
    public String getAddressTranslatedDD(){
        return "Person_address";
    }
    
    @Transient
    @Translation(originalField="address")
    private String addressTranslated;

    public String getAddressTranslated() {
        return addressTranslated;
    }

    public void setAddressTranslated(String addressTranslated) {
        this.addressTranslated = addressTranslated;
    }
    
    @Translatable(translationField="address1Translated")
    private String address1;
    public String getAddress1TranslatedDD(){
        return "Person_address1";
    }
    
    @Transient
    @Translation(originalField="address1")
    private String address1Translated;

    public String getAddress1Translated() {
        return address1Translated;
    }

    public void setAddress1Translated(String address1Translated) {
        this.address1Translated = address1Translated;
    }

    private String phone;
    public String getPhoneDD(){
        return "Person_phone";
    }

    private String phone1;
    public String getPhone1DD(){
        return "Person_phone1";
    }

    private String mobile;
    public String getMobileDD(){
        return "Person_mobile";
    }

    private String mobile1;
    public String getMobile1DD(){
        return "Person_mobile1";
    }

    private String email;
    public String getEmailDD(){
        return "Person_email";
    }

    private String email1;
    public String getEmail1DD(){
        return "Person_email1";
    }

    @Translatable(translationField="additionalInformationTranslated")
    private String additionalInformation;
    
    public String getAdditionalInformationTranslatedDD(){
        return "Person_additionalInformation";
    }
    
    @Transient
    @Translation(originalField="additionalInformation")
    private String additionalInformationTranslated;

    public String getAdditionalInformationTranslated() {
        return additionalInformationTranslated;
    }

    public void setAdditionalInformationTranslated(String additionalInformationTranslated) {
        this.additionalInformationTranslated = additionalInformationTranslated;
    }

    @Temporal(javax.persistence.TemporalType.DATE)

    private Date birthdate;
    public String getBirthdateDD(){
        return "Person_birthdate";
    }
    // attributes from direct assosiation.
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC birthPlace;

    public UDC getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(UDC birthPlace) {
        this.birthPlace = birthPlace;
    }
    
    public String getBirthPlaceDD(){
        return "Person_birthPlace";
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC religion;

    public UDC getReligion() {
        return religion;
    }

    public void setReligion(UDC religion) {
        this.religion = religion;
    }

    public String getReligionDD(){
        return "Person_religion";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC gender;
    public String getGenderDD(){
        return "Person_gender";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC nationality;
    public String getNationalityDD(){
        return "Person_nationality";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC city;
    public String getCityDD(){
        return "Person_city";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC country;
    public String getCountryDD(){
        return "Person_country";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC maritalStatus;
    public String getMaritalStatusDD(){
        return "Person_maritalStatus";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC militaryStatus;
    public String getMilitaryStatusDD(){
        return "Person_militaryStatus";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC salute;
    public String getSaluteDD(){
        return "Person_salute";
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public UDC getCity() {
        return city;
    }

    public void setCity(UDC city) {
        this.city = city;
    }

    public UDC getCountry() {
        return country;
    }

    public void setCountry(UDC country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getFirstName() {
        return firstName;
    }

    public UDC getGender() {
        return gender;
    }

    public void setGender(UDC gender) {
        this.gender = gender;
    }

    public String getLastName() {
        return lastName;
    }

    public UDC getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(UDC maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getMiddleName() {
        return middleName;
    }

    public UDC getMilitaryStatus() {
        return militaryStatus;
    }

    public void setMilitaryStatus(UDC militaryStatus) {
        this.militaryStatus = militaryStatus;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile1() {
        return mobile1;
    }

    public void setMobile1(String mobile1) {
        this.mobile1 = mobile1;
    }

    public UDC getNationality() {
        return nationality;
    }

    public void setNationality(UDC nationality) {
        this.nationality = nationality;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public UDC getSalute() {
        return salute;
    }

    public void setSalute(UDC salute) {
        this.salute = salute;
    }
}
