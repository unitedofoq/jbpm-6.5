/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.report.customReports;

import java.io.ByteArrayOutputStream;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.engine.emitter.csv.CSVRenderOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author bgalal
 */
public class CSVReportGenerator extends AbstractReportGenerator {
    
    private final static Logger LOGGER = LoggerFactory.getLogger(CSVReportGenerator.class);
    
    public CSVReportGenerator(ReportDTO reportDTO) {
        super(reportDTO);
    }
    
    @Override
    public ByteArrayOutputStream generateReport(CustomReportBuilder customReportBuilder) {
        LOGGER.trace("Entering");
        IRunAndRenderTask task = prepareReportDesignHandle(customReportBuilder);
        if(task == null)
            return null;
        //<editor-fold defaultstate="collapsed" desc="CSV Generator">
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        try {
            CSVRenderOption options = new CSVRenderOption();
            options.setOutputFormat(CSVRenderOption.OUTPUT_FORMAT_CSV);
            options.setOutputStream(outStream);
            options.setDelimiter("\t");
            task.setEmitterID("org.eclipse.birt.report.engine.emitter.csv");
            task.setRenderOption(options);
        } catch (Exception ex) {
            LOGGER.error("Exception thrown: ", ex);
        }
        runReport(task);
        //</editor-fold>
        
        LOGGER.trace("Returning");
        return outStream;
    }
}
