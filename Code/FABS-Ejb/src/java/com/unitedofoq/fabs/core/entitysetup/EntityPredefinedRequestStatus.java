/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.entitysetup;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.module.OModule;

/**
 *
 * @author nkhalil
 */
@Entity
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class EntityPredefinedRequestStatus extends ObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OModule omodule;

    public String getOmoduleDD(){
        return "EntityPredefinedRequestStatus_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
// </editor-fold>

    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    EntityPredefinedRequestStatus preRequestStatus;
    @Translatable(translationField="descriptionTranslated")
    private String description;

    @Translation(originalField="description")
    @Transient
    private String descriptionTranslated;

    public String getDescriptionTranslatedDD(){
        return "EntityPredefinedRequestStatus_description";
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    private String name;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EntityPredefinedRequestStatus getPreRequestStatus() {
        return preRequestStatus;
    }

    public void setPreRequestStatus(EntityPredefinedRequestStatus preRequestStatus) {
        this.preRequestStatus = preRequestStatus;
    }
}
