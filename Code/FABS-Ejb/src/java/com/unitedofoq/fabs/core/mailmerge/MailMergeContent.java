/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.mailmerge;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;

/**
 *
 * @author 3dly
 */
@Entity
@ParentEntity(fields = "mailMergeBasic")
public class MailMergeContent extends BaseEntity {

    //<editor-fold defaultstate="collapsed" desc="templateContent">
    @Column
    private String templateContent;

    public String getTemplateContent() {
        return templateContent;
    }

    public void setTemplateContent(String templateContent) {
        this.templateContent = templateContent;
    }

    public String getTemplateContentDD() {
        return "MailMergeContent_templateContent";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="mailMergeBasic">
    @OneToOne
    @JoinColumn(name="mailMergeBasic_DBID")
    private MailMergeBasic mailMergeBasic;

    public MailMergeBasic getMailMergeBasic() {
        return mailMergeBasic;
    }

    public void setMailMergeBasic(MailMergeBasic mailMergeBasic) {
        this.mailMergeBasic = mailMergeBasic;
    }

    public String getMailMergeBasicDD() {
        return "MailMergeContent_mailMergeBasic";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="fieldExpression">
    private String fieldExpression;

    public String getFieldExpression() {
        return fieldExpression;
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }

    public String getFieldExpressionDD() {
        return "MailMergeContent_fieldExpression";
    }
    //</editor-fold>
}
