/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.routing;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author mibrahim
 */
@Local
public interface ORoutingBeanLocal {
    public OFunctionResult startRoutingInstance(ODataMessage oDM, OFunctionParms params, OUser loggedUser);
    public OFunctionResult runTaskAction(ODataMessage oDM, OFunctionParms params, OUser loggedUser);
    public OFunctionResult getRoutingTaskInfo(ODataMessage oDM, OFunctionParms params, OUser loggedUser);
    public OFunctionResult getTaskInbox(OUser loggedUser);
}
