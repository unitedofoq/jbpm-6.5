/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author Rehab
 */
@Entity
@ParentEntity(fields = {"screen"})
@FABSEntitySpecs(indicatorFieldExp="screen.name")
@VersionControlSpecs(versionControlType = VersionControlSpecs.VersionControlType.Parent)
public class EntityFilterScreenAction extends BaseEntity{

    // <editor-fold defaultstate="collapsed" desc="screen">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private SingleEntityScreen screen;

    public SingleEntityScreen getScreen() {
        return screen;
    }

    public void setScreen(SingleEntityScreen screen) {
        this.screen = screen;
    }
    
    public String getScreenDD() {
        return "EntityFilterScreenAction_screen";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entityFilter">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private EntityFilter entityFilter;


    public EntityFilter getEntityFilter() {
        return entityFilter;
    }

    public void setEntityFilter(EntityFilter entityFilter) {
        this.entityFilter = entityFilter;
    }
    
    public String getEntityFilterDD() {
        return "EntityFilterScreenAction_entityFilter";
    }
    // </editor-fold>
}
