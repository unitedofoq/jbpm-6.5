package com.unitedofoq.fabs.core.security.user;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.Transient;

import com.unitedofoq.fabs.central.OTenant;
import com.unitedofoq.fabs.core.delegation.TaskDelegation;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.udc.UDC;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "UTYPE", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("USER")
@ChildEntity(fields = {"delegations"})
@EntityListeners(EncryptionCallbackListener.class)
@VersionControlSpecs(omoduleFieldExpression = "omodule")
public class OUser extends ObjectBaseEntity {
    @Transient
    private long workingTenantID;

    public long getWorkingTenantID() {
        return workingTenantID;
    }

    public void setWorkingTenantID(long workingTenantID) {
        this.workingTenantID = workingTenantID;
    }
    
        
    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OModule omodule;

    public String getOmoduleDD() {
        return "Person_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Languages">
    @Transient
    public final static long ENGLISH = 23;
    @Transient
    public final static long ARABIC = 24;
    // </editor-fold>
    private boolean rtl;

    public String getRtlDD() {
        return "OUser_rtl";
    }

    public boolean isRtl() {
        return rtl;
    }
    
    public String getLatestPassword(){
        return ((newPass != null && !newPass.isEmpty())?newPass:password);
        
    }
    

    public void setRtl(boolean rtl) {
        this.rtl = rtl;
    }
    @Column(unique = true, nullable = false)
    private String loginName;
    //@Transient
    private String password;
    private String email;
    private boolean managerRoleEnabled = true;
    private boolean reserved = false;

    public boolean isManagerRoleEnabled() {
        return managerRoleEnabled;
    }

    public void setManagerRoleEnabled(boolean managerRoleEnabled) {
        this.managerRoleEnabled = managerRoleEnabled;
    }

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date birthDate;
    @OneToMany(mappedBy = "ouser")
    protected List<RoleUser> roleUsers = null;
    public List<RoleUser> getRoleUsers() {
        return roleUsers;
    }

    public void setRoleUsers(List<RoleUser> roleUsers) {
        this.roleUsers = roleUsers;
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC firstLanguage = null;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC secondLanguage = null;

    public UDC getFirstLanguage() {
        return firstLanguage;
    }

    public void setFirstLanguage(UDC firstLanguage) {
        this.firstLanguage = firstLanguage;
    }

    public UDC getSecondLanguage() {
        return secondLanguage;
    }

    public void setSecondLanguage(UDC secondLanguage) {
        this.secondLanguage = secondLanguage;
    }


    /* DD NAMES GETTERS */
    public String getFirstLanguageDD() {
        return "OUser_language";
    }

    public String getSecondLanguageDD() {
        return "OUser_language";
    }

    public String getLoginNameDD() {
        return "OUser_loginName";
    }

    public String getPasswordDD() {
        return "OUser_password";
    }

    public String getReservedDD() {
        return "OUser_reserved";
    }

    public String getEmailDD() {
        return "OUser_email";
    }

    public String getBirthDateDD() {
        return "OUser_birthDate";
    }

    public String getManagerRoleEnabledDD() {
        return "OUser_managerRoleEnabled";
    }

    public String getRoleUsersDD() {
        return "OUser_roleUsers";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OUser other = (OUser) obj;
        if ((this.loginName == null) ? (other.loginName != null) : !this.loginName.equals(other.loginName)) {
            return false;
        }
        if ((this.password == null) ? (other.password != null) : !this.password.equals(other.password)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.loginName != null ? this.loginName.hashCode() : 0);
        hash = 17 * hash + (this.password != null ? this.password.hashCode() : 0);
        return hash;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
    // <editor-fold defaultstate="collapsed" desc="tenant">
    @Transient
    private OTenant tenant;

    public OTenant getTenant() {
        return tenant;
    }

    public void setTenant(OTenant tenant) {
        this.tenant = tenant;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="onBehalfOf">
    @Transient
    private OUser onBehalfOf;

    public OUser getOnBehalfOf() {
        return onBehalfOf;
    }

    public void setOnBehalfOf(OUser onBehalfOf) {
        this.onBehalfOf = onBehalfOf;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="User Prefrences">
    // <editor-fold defaultstate="collapsed" desc="Prefrence 1">
    private Long userPrefrence1;

    /**
     * @return the userPrefrence1
     */
    public Long getUserPrefrence1() {
        return userPrefrence1;
    }

    /**
     * @param userPrefrence1 the userPrefrence1 to set
     */
    public void setUserPrefrence1(Long userPrefrence1) {
        this.userPrefrence1 = userPrefrence1;
    }

    /**
     * @return the userPrefrence1DD
     */
    public String getUserPrefrence1DD() {
        return "OUser_userPrefrence1";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Prefrence 2">
    private String userPrefrence2;

    /**
     * @return the userPrefrence2
     */
    public String getUserPrefrence2() {
        return userPrefrence2;
    }

    /**
     * @param userPrefrence2 the userPrefrence2 to set
     */
    public void setUserPrefrence2(String userPrefrence2) {
        this.userPrefrence2 = userPrefrence2;
    }

    /**
     * @return the userPrefrence2DD
     */
    public String getUserPrefrence2DD() {
        return "OUser_userPrefrence2";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Prefrence 3">
    private Long userPrefrence3;

    /**
     * @return the userPrefrence3
     */
    public Long getUserPrefrence3() {
        return userPrefrence3;
    }

    /**
     * @param userPrefrence1 the userPrefrence3 to set
     */
    public void setUserPrefrence3(Long userPrefrence3) {
        this.userPrefrence3 = userPrefrence3;
    }

    /**
     * @return the userPrefrence3DD
     */
    public String getUserPrefrence3DD() {
        return "OUser_userPrefrence3";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Prefrence 4">
    private String userPrefrence4;

    /**
     * @return the userPrefrence4
     */
    public String getUserPrefrence4() {
        return userPrefrence4;
    }

    /**
     * @param userPrefrence4 the userPrefrence4 to set
     */
    public void setUserPrefrence4(String userPrefrence4) {
        this.userPrefrence4 = userPrefrence4;
    }

    /**
     * @return the userPrefrence4DD
     */
    public String getUserPrefrence4DD() {
        return "OUser_userPrefrence4";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Prefrence 5">
    private Long userPrefrence5;

    /**
     * @return the userPrefrence5
     */
    public Long getUserPrefrence5() {
        return userPrefrence5;
    }

    /**
     * @param userPrefrence5 the userPrefrence5 to set
     */
    public void setUserPrefrence5(Long userPrefrence5) {
        this.userPrefrence5 = userPrefrence5;
    }

    /**
     * @return the userPrefrence5DD
     */
    public String getUserPrefrence5DD() {
        return "OUser_userPrefrence5";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Prefrence 6">
    private String userPrefrence6;

    /**
     * @return the userPrefrence6
     */
    public String getUserPrefrence6() {
        return userPrefrence6;
    }

    /**
     * @param userPrefrence6 the userPrefrence6 to set
     */
    public void setUserPrefrence6(String userPrefrence6) {
        this.userPrefrence6 = userPrefrence6;
    }

    /**
     * @return the userPrefrence6DD
     */
    public String getUserPrefrence6DD() {
        return "OUser_userPrefrence6";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Prefrence 7">
    private Long userPrefrence7;

    /**
     * @return the userPrefrence7
     */
    public Long getUserPrefrence7() {
        return userPrefrence7;
    }

    /**
     * @param userPrefrence7 the userPrefrence1 to set
     */
    public void setUserPrefrence7(Long userPrefrence7) {
        this.userPrefrence7 = userPrefrence7;
    }

    /**
     * @return the userPrefrence7DD
     */
    public String getUserPrefrence7DD() {
        return "OUser_userPrefrence7";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Prefrence 8">
    private String userPrefrence8;

    /**
     * @return the userPrefrence8
     */
    public String getUserPrefrence8() {
        return userPrefrence8;
    }

    /**
     * @param userPrefrence8 the userPrefrence8 to set
     */
    public void setUserPrefrence8(String userPrefrence8) {
        this.userPrefrence8 = userPrefrence8;
    }

    /**
     * @return the userPrefrence8DD
     */
    public String getUserPrefrence8DD() {
        return "OUser_userPrefrence8";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Prefrence 9">
    private Long userPrefrence9;

    /**
     * @return the userPrefrence9
     */
    public Long getUserPrefrence9() {
        return userPrefrence9;
    }

    /**
     * @param userPrefrence9 the userPrefrence9 to set
     */
    public void setUserPrefrence9(Long userPrefrence9) {
        this.userPrefrence9 = userPrefrence9;
    }

    /**
     * @return the userPrefrence1DD
     */
    public String getUserPrefrence9DD() {
        return "OUser_userPrefrence9";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Prefrence 10">
    private String userPrefrence10;

    /**
     * @return the userPrefrence10
     */
    public String getUserPrefrence10() {
        return userPrefrence10;
    }

    /**
     * @param userPrefrence10 the userPrefrence10 to set
     */
    public void setUserPrefrence10(String userPrefrence10) {
        this.userPrefrence10 = userPrefrence10;
    }

    /**
     * @return the userPrefrence10DD
     */
    public String getUserPrefrence10DD() {
        return "OUser_userPrefrence10";
    }
    // </editor-fold>
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currentPass">
    @Transient
    private String currentPass;

    public String getCurrentPass() {
        return currentPass;
    }

    public void setCurrentPass(String currentPass) {
        this.currentPass = currentPass;
    }
    
    public String getCurrentPassDD() {
        return "OUser_currentPass";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="newPass">
    @Transient
    private String newPass;    

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }
    
    public String getNewPassDD() {
        return "OUser_newPass";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="confirmPass">
    @Transient
    private String confirmPass;

    public String getConfirmPass() {
        return confirmPass;
    }

    public void setConfirmPass(String confirmPass) {
        this.confirmPass = confirmPass;
    }
    
    public String getConfirmPassDD() {
        return "OUser_confirmPass";
    }
    // </editor-fold>
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC timeZone = null;

    /**
     * @return the timeZone
     */
    public UDC getTimeZone() {
        return timeZone;
    }

    /**
     * @param timeZone the timeZone to set
     */
    public void setTimeZone(UDC timeZone) {
        this.timeZone = timeZone;
    }
      public String getTimeZoneDD() {
        return "OUser_timeZone";
    }
    
    // <editor-fold defaultstate="collapsed" desc="trackMetaData">
    @Column
    private boolean trackMetaData;

    public boolean isTrackMetaData() {
        return trackMetaData;
    }

    public void setTrackMetaData(boolean trackMetaData) {
        this.trackMetaData = trackMetaData;
    }
    
    public String getTrackMetaDataDD() {
        return "OUser_trackMetaData";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="trackBusinessData">
    @Column
    private boolean trackBusinessData;    

    public boolean isTrackBusinessData() {
        return trackBusinessData;
    }

    public void setTrackBusinessData(boolean trackBusinessData) {
        this.trackBusinessData = trackBusinessData;
    }
    
    public String getTrackBusinessDataDD() {
        return "OUser_trackBusinessData";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="delegations">
    @OneToMany(mappedBy="owner")
    private List<TaskDelegation> delegations;

    public void setDelegations(List<TaskDelegation> delegations) {
        this.delegations = delegations;
    }

    public List<TaskDelegation> getDelegations() {
        return delegations;
    }

    public String getDelegationsDD() {
        return "OUser_delegations";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="displayName">
    @Column
    private String displayName;

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getDisplayNameDD() {
        return "OUser_displayName";
    }
    // </editor-fold>

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC group1;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC group2;

    public UDC getGroup1() {
        return group1;
    }

    public void setGroup1(UDC group1) {
        this.group1 = group1;
    }
    
    public String getGroup1DD() {
        return "OUser_group1";
    }

    public UDC getGroup2() {
        return group2;
    }

    public void setGroup2(UDC group2) {
        this.group2 = group2;
    }
    
    public String getGroup2DD() {
        return "OUser_group2";
    }
    
    private String passwordEnc;

    public String getPasswordEnc() {
        return passwordEnc;
    }

    public void setPasswordEnc(String passwordEnc) {
        this.passwordEnc = passwordEnc;
    }
    
    public String getPasswordEncDD() {
        return "OUser_passwordEnc";
    }
    
    @Transient
    private boolean startupEnc;

    public boolean isStartupEnc() {
        return startupEnc;
    }

    public void setStartupEnc(boolean startupEnc) {
        this.startupEnc = startupEnc;
    }
}
