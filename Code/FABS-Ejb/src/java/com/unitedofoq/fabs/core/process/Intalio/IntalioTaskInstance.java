/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.process.Intalio;

/**
 *
 * @author melsayed
 */
public class IntalioTaskInstance {
    private String instaceId;
    private String taskId;
    private String type;
    private String description;
    private String processId;
    private String creationDate;
    private String userOwner;
    private String formUrl;
    private String userProcessCompleteSOAPAction;
    private String isChainedBefore;

    /**
     * @return the instaceId
     */
    public String getInstaceId() {
        return instaceId;
    }

    /**
     * @param instaceId the instaceId to set
     */
    public void setInstaceId(String instaceId) {
        this.instaceId = instaceId;
    }

    /**
     * @return the taskId
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * @param taskId the taskId to set
     */
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the processId
     */
    public String getProcessId() {
        return processId;
    }

    /**
     * @param processId the processId to set
     */
    public void setProcessId(String processId) {
        this.processId = processId;
    }

    /**
     * @return the creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the userOwner
     */
    public String getUserOwner() {
        return userOwner;
    }

    /**
     * @param userOwner the userOwner to set
     */
    public void setUserOwner(String userOwner) {
        this.userOwner = userOwner;
    }

    /**
     * @return the formUrl
     */
    public String getFormUrl() {
        return formUrl;
    }

    /**
     * @param formUrl the formUrl to set
     */
    public void setFormUrl(String formUrl) {
        this.formUrl = formUrl;
    }

    /**
     * @return the userProcessCompleteSOAPAction
     */
    public String getUserProcessCompleteSOAPAction() {
        return userProcessCompleteSOAPAction;
    }

    /**
     * @param userProcessCompleteSOAPAction the userProcessCompleteSOAPAction to set
     */
    public void setUserProcessCompleteSOAPAction(String userProcessCompleteSOAPAction) {
        this.userProcessCompleteSOAPAction = userProcessCompleteSOAPAction;
    }

    /**
     * @return the isChainedBefore
     */
    public String getIsChainedBefore() {
        return isChainedBefore;
    }

    /**
     * @param isChainedBefore the isChainedBefore to set
     */
    public void setIsChainedBefore(String isChainedBefore) {
        this.isChainedBefore = isChainedBefore;
    }


}
