/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.report.customReports;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author asaied
 */

@Entity
@ChildEntity(fields = {"customreportfilterfields"})
public class CustomReportDynamicFilter extends BaseEntity{
    
    @Column(name = "filterName")
    private String filterName;
    
    
    @ManyToOne(fetch = javax.persistence.FetchType.EAGER)
    @JoinColumn(name = "customreport_DBID")
    private CustomReport customReport;
    
    @OneToMany(mappedBy = "customReportDynamicFilter")
    private List<CustRepFilterFld> customreportfilterfields;

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }
    
    public String getFilterNameDD() {
        return "CustomReportEntityFilter_filterName";
    }
    
    public List<CustRepFilterFld> getCustomreportfilterfields() {
        return customreportfilterfields;
    }

    public void setCustomreportfilterfields(List<CustRepFilterFld> customreportfilterfields) {
        this.customreportfilterfields = customreportfilterfields;
    }

    public String getCustomreportfilterfieldsDD() {
        return "CustomReportFilter_customreportfilterfields";
    }
    
    public CustomReport getCustomReport() {
        return customReport;
    }

    public void setCustomReport(CustomReport customReport) {
        this.customReport = customReport;
    }
    
    public String getCustomReportDD() {
        return "EntityFilter_customReport";
    }

    
}
