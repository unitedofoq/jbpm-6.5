
package com.unitedofoq.fabs.core.uiframework.screen;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;


@Entity
@DiscriminatorValue(value = "EE")
@ChildEntity(fields={"screenInputs", "screenOutputs","screenFilter","screenFunctions"})
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class OPEyeExaminationScreen extends FormScreen  {
}
