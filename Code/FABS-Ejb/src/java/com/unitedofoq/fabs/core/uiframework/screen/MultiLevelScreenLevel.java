/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.screen;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.multimedia.OImage;

/**
 *
 * @author nkhalil
 */
@Entity
@ParentEntity(fields={"levelScreen"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class MultiLevelScreenLevel extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="c2a">
    private boolean c2a;

    public String getC2aDD() {
        return "MultiLevelScreenLevel_c2a";
    }
    public boolean isC2a() {
        return c2a;
    }

    public void setC2a(boolean c2a) {
        this.c2a = c2a;
    }
    // </editor-fold>
    private String fldExpInParentEntity;

    public String getFldExpInParentEntityDD() {
        return "MultiLevelScreenLevel_fldExpInParentEntity";
    }

    public String getFldExpInParentEntity() {
        return fldExpInParentEntity;
    }

    public void setFldExpInParentEntity(String fldExpInParentEntity) {
        this.fldExpInParentEntity = fldExpInParentEntity;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OEntity parentOactOnEntity;

    public String getParentOactOnEntityDD() {
        return "MultiLevelScreenLevel_parentOactOnEntity";
    }

    public OEntity getParentOactOnEntity() {
        return parentOactOnEntity;
    }

    public void setParentOactOnEntity(OEntity parentOactOnEntity) {
        this.parentOactOnEntity = parentOactOnEntity;
    }


    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OImage levelImage;

    public String getLevelImageDD() {
        return "MultiLevelScreenLevel_levelImage";
    }

    public OImage getLevelImage() {
        return levelImage;
    }

    public void setLevelImage(OImage levelImage) {
        this.levelImage = levelImage;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private MultiLevelScreen levelScreen;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OEntity actOnEntity;
    private String displayedField;
    private String parentLevelField;    
    private int levelOrder;
    private String filterExpression;
    private boolean selectable;
    

    public String getActOnEntityDD() {
        return "MultiLevelScreenLevel_actOnEntity";
    }

    public OEntity getActOnEntity() {
        return actOnEntity;
    }

    public void setActOnEntity(OEntity actOnEntity) {
        this.actOnEntity = actOnEntity;
    }

    public String getDisplayedFieldDD() {
        return "MultiLevelScreenLevel_displayedField";
    }

    public String getDisplayedField() {
        return displayedField;
    }

    public void setDisplayedField(String displayedField) {
        this.displayedField = displayedField;
    }

    public String getFilterExpressionDD() {
        return "MultiLevelScreenLevel_lterExpression";
    }

    public String getFilterExpression() {
        return filterExpression;
    }

    public void setFilterExpression(String filterExpression) {
        this.filterExpression = filterExpression;
    }

    public String getLevelScreenDD() {
        return "MultiLevelScreenLevel_levelScreen";
    }

    public MultiLevelScreen getLevelScreen() {
        return levelScreen;
    }

    public void setLevelScreen(MultiLevelScreen levelScreen) {
        this.levelScreen = levelScreen;
    }

    public String getParentLevelFieldDD() {
        return "MultiLevelScreenLevel_parentLevelField";
    }

    public String getParentLevelField() {
        return parentLevelField;
    }

    public void setParentLevelField(String parentLevelField) {
        this.parentLevelField = parentLevelField;
    }

    public String getSelectableDD() {
        return "MultiLevelScreenLevel_selectable";
    }

    public boolean isSelectable() {
        return selectable;
    }

    public void setSelectable(boolean selectable) {
        this.selectable = selectable;
    }

    public String getLevelOrderDD() {
        return "MultiLevelScreenLevel_levelOrder";
    }

    public int getLevelOrder() {
        return levelOrder;
    }

    public void setLevelOrder(int levelOrder) {
        this.levelOrder = levelOrder;
    }


    

    public String getSubLevelFieldDD() {
        return "MultiLevelScreenLevel_subLevelField";
    }
    

}
