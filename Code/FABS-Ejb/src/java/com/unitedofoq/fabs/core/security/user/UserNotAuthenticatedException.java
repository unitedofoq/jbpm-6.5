/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.security.user;

/**
 *
 * @author ahussien
 */
public class UserNotAuthenticatedException extends Exception{

    private String message;

    public UserNotAuthenticatedException(String message)
    {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }

    @Override
    public String getMessage() {
        return message;
    }


}
