/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.entitysetup;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.udc.UDC;

/**
 *
 * @author aelzaher
 */
@Entity
@ParentEntity(fields="oJavaEntity")
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class OJavaEntityField extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="name">
    @Column(nullable = false)
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getNameDD() {
        return "OJavaEntityField_name";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="omodule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OModule omodule;

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }

    public OModule getOmodule() {
        return omodule;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="fieldType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC fieldType;

    public void setFieldType(UDC fieldType) {
        this.fieldType = fieldType;
    }

    public UDC getFieldType() {
        return fieldType;
    }

    public String getFieldTypeDD() {
        return "OJavaEntityField_fieldType";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="fieldEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OEntity fieldEntity;

    public void setFieldEntity(OEntity fieldEntity) {
        this.fieldEntity = fieldEntity;
    }

    public OEntity getFieldEntity() {
        return fieldEntity;
    }

    public String getFieldEntityDD() {
        return "OJavaEntityField_fieldEntity";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="fieldJavaEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private OJavaEntity fieldJavaEntity;

    public void setFieldJavaEntity(OJavaEntity fieldJavaEntity) {
        this.fieldJavaEntity = fieldJavaEntity;
    }

    public OJavaEntity getFieldJavaEntity() {
        return fieldJavaEntity;
    }

    public String getFieldJavaEntityDD() {
        return "OJavaEntityField_fieldJavaEntity";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="fieldRelation">
    /**
    * entity relation with other
    */
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC fieldRelation;

    public void setFieldRelation(UDC fieldRelation) {
        this.fieldRelation = fieldRelation;
    }

    public UDC getFieldRelation() {
        return fieldRelation;
    }

    public String getFieldRelationDD() {
        return "OJavaEntityField_fieldRelation";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="mandatoryField">
    @Column(nullable = false)
    private boolean mandatoryField;

    public void setMandatoryField(boolean mandatoryField) {
        this.mandatoryField = mandatoryField;
    }

    public boolean isMandatoryField() {
        return mandatoryField;
    }

    public String getMandatoryFieldDD() {
        return "OJavaEntityField_mandatoryField";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="uniqueField">
    @Column(nullable = false)
    private boolean uniqueField;

    public void setUniqueField(boolean uniqueField) {
        this.uniqueField = uniqueField;
    }

    public boolean isUniqueField() {
        return uniqueField;
    }

    public String getUniqueFieldDD() {
        return "OJavaEntityField_uniqueField";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="childField">
    @Column
    private boolean childField;

    public void setChildField(boolean childField) {
        this.childField = childField;
    }

    public boolean isChildField() {
        return childField;
    }

    public String getChildFieldDD() {
        return "OJavaEntityField_childField";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="joinColumn">
    @Column(nullable=false)
    private boolean joinColumn;

    public void setJoinColumn(boolean joinColumn) {
        this.joinColumn = joinColumn;
    }

    public boolean isJoinColumn() {
        return joinColumn;
    }

    public String getJoinColumnDD() {
        return "OJavaEntityField_joinColumn";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="notDisplayedField">
    @Column(nullable=false)
    private boolean notDisplayedField;

    public void setNotDisplayedField(boolean notDisplayedField) {
        this.notDisplayedField = notDisplayedField;
    }

    public boolean isNotDisplayedField() {
        return notDisplayedField;
    }

    public String getNotDisplayedFieldDD() {
        return "OJavaEntityField_notDisplayedField";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="displayField">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private DD displayField;

    public void setDisplayField(DD displayField) {
        this.displayField = displayField;
    }

    public DD getDisplayField() {
        return displayField;
    }

    public String getDisplayFieldDD() {
        return "OJavaEntityField_displayField";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="oJavaEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OJavaEntity oJavaEntity;

    public void setOJavaEntity(OJavaEntity oJavaEntity) {
        this.oJavaEntity = oJavaEntity;
    }

    public OJavaEntity getOJavaEntity() {
        return oJavaEntity;
    }

    public String getOJavaEntityDD() {
        return "OJavaEntityField_oJavaEntity";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="mappedBy">
    @Column
    private String mappedBy;

    public void setMappedBy(String mappedBy) {
        this.mappedBy = mappedBy;
    }

    public String getMappedBy() {
        return mappedBy;
    }

    public String getMappedByDD() {
        return "OJavaEntityField_mappedBy";
    }
    // </editor-fold>
        
    // <editor-fold defaultstate="collapsed" desc="mappedBy">
//    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
//    private OJavaEntityField mappedBy;
//
//    public void setMappedBy(OJavaEntityField mappedBy) {
//        this.mappedBy = mappedBy;
//    }
//
//    public OJavaEntityField getMappedBy() {
//        return mappedBy;
//    }
//
//    public String getMappedByDD() {
//        return "OJavaEntityField_mappedBy";
//    }
    // </editor-fold>

}
