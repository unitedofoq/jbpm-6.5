/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.screen;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.udc.UDC;

/**
 *
 * @author nkhalil
 */
@Entity
@ChildEntity(fields={"multiLevelScreenLevels","screenInputs", "screenOutputs","screenFilter","screenFunctions"})
@DiscriminatorValue(value="MultiLevel")
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class MultiLevelScreen extends SingleEntityScreen {
    @OneToMany(mappedBy = "levelScreen")
    private List<MultiLevelScreenLevel> multiLevelScreenLevels;

    public List<MultiLevelScreenLevel> getMultiLevelScreenLevels() {
        return multiLevelScreenLevels;
    }

    public void setMultiLevelScreenLevels(List<MultiLevelScreenLevel> multiLevelScreenLevels) {
        this.multiLevelScreenLevels = multiLevelScreenLevels;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC type;
    private boolean readOnly;

    public String getReadOnlyDD() {
        return "MultiLevelScreen_readOnly";
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public String getTypeDD() {
        return "MultiLevelScreen_type";
    }

    public UDC getType() {
        return type;
    }

    public void setType(UDC type) {
        this.type = type;
    }
}
