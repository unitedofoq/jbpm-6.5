/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.report.customReports;

import com.unitedofoq.fabs.core.content.AttachmentDTO;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.function.JavaFunction;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author bgalal
 */
public class ReportDTO {
    private AttachmentDTO entityAttachment;
    private boolean localized;
    private boolean revertLang;
    private HashMap<String, Object> parametersMap;
    private String whereConditions;
    private String reportName;
    private JavaFunction userExit;
    private OUser loggedUser;
    private boolean applyingFilter;
    private boolean fitPage;
    //for encryption
    private boolean encryptionEnabled;
    private String symmetricKey;
    private String ivKey;
    
    private ODataType screenDataLoadingUEDT;
    private String tannentPassword;
    
    
    private String dataSetName;
    private HashMap<String,String> dataSets;

    public ReportDTO(AttachmentDTO entityAttachment, boolean localized, boolean revertLang,boolean applyingFilter, boolean fitPage, HashMap<String, Object> parametersMap, String whereConditions, String reportName, JavaFunction userExit, OUser loggedUser) {
        this.entityAttachment = entityAttachment;
        this.localized = localized;
        this.revertLang = revertLang;
        this.applyingFilter = applyingFilter;
        this.fitPage = fitPage;
        this.parametersMap = parametersMap;
        this.whereConditions = whereConditions;
        this.reportName = reportName;
        this.userExit = userExit;
        this.loggedUser = loggedUser;
        this.dataSets = new HashMap<String,String>();
    }
    
    public void addDataSetQuery(String dataSetName,String Query){
        dataSets.put(dataSetName, Query);
    }
    
    public String getDataSetQuery(String DataSetName){
        return dataSets.get(DataSetName);
       
    }
    
    public Set<Map.Entry<String,String>> getDataSetIterator(){
        return dataSets.entrySet();
    }
   
    public AttachmentDTO getEntityAttachment() {
        return entityAttachment;
    }

    public void setEntityAttachment(AttachmentDTO entityAttachment) {
        this.entityAttachment = entityAttachment;
    }

    public boolean isLocalized() {
        return localized;
    }

    public void setLocalized(boolean localized) {
        this.localized = localized;
    }

    public boolean isRevertLang() {
        return revertLang;
    }

    public void setRevertLang(boolean revertLang) {
        this.revertLang = revertLang;
    }

    public boolean isApplyingFilter() {
        return applyingFilter;
    }

    public void setApplyingFilter(boolean applyingFilter) {
        this.applyingFilter = applyingFilter;
    }

    public HashMap<String, Object> getParametersMap() {
        return parametersMap;
    }

    public void setParametersMap(HashMap<String, Object> parametersMap) {
        this.parametersMap = parametersMap;
    }

    public String getWhereConditions() {
        return whereConditions;
    }

    public void setWhereConditions(String whereConditions) {
        this.whereConditions = whereConditions;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public JavaFunction getUserExit() {
        return userExit;
    }

    public void setUserExit(JavaFunction userExit) {
        this.userExit = userExit;
    }

    public OUser getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(OUser loggedUser) {
        this.loggedUser = loggedUser;
    }

    public void setEncryptionEnabled(boolean encryptionEnabled) {
        this.encryptionEnabled = encryptionEnabled;
    }

    public boolean isEncryptionEnabled() {
        return encryptionEnabled;
    }

    public void setSymmetricKey(String symmetricKey) {
        this.symmetricKey = symmetricKey;
    }

    public String getSymmetricKey() {
        return symmetricKey;
    }

    public void setIvKey(String ivKey) {
        this.ivKey = ivKey;
    }

    public String getIvKey() {
        return ivKey;
    }
    
    public void setScreenDataLoadingUEDT(ODataType screenDataLoadingUEDT) {
        this.screenDataLoadingUEDT = screenDataLoadingUEDT;
    }

    public ODataType getScreenDataLoadingUEDT() {
        return screenDataLoadingUEDT;
    }

    public void setTannentPassword(String tannentPassword) {
        this.tannentPassword = tannentPassword;
    }

    public String getTannentPassword() {
        return tannentPassword;
    }

    public void setDataSetName(String dataSetName) {
        this.dataSetName = dataSetName;
    }

    public String getDataSetName() {
        return dataSetName;
    }

    public void setFitPage(boolean fitPage) {
        this.fitPage = fitPage;
    }

    public boolean isFitPage() {
        return fitPage;
    }
}
