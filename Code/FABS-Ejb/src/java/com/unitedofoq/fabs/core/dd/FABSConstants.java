/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.dd;

import javax.persistence.Transient;

/**
 *
 * @author lap
 */
public interface FABSConstants {
    @Transient
    int CT_AUTOCOMPLETE = 1446965;
    @Transient
    int CT_BARCODEIMAGE = 1446046;
    @Transient
    int CT_CALENDAR = 11;
    @Transient
    int CT_CHECKBOX = 8;
    // TODO : Linkable field
    @Transient
    int CT_COMMANDLINK = -1;
    @Transient
    int CT_DROPDOWN = 6001;
    @Transient
    int CT_FILE = 132329;
    @Transient
    int CT_FILEBROWSER = 12;
    @Transient
    int CT_FILEDOWNLOAD = 132323;
    @Transient
    int CT_FILEUPLOAD = 132322;
    @Transient
    int CT_IMAGE = 1028031;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Control Types">
    @Transient
    int CT_LABEL = 5;
    @Transient
    int CT_LINK = 133959;
    @Transient
    int CT_LITERALDROPDOWN = 6002;
    @Transient
    int CT_LOOKUPATTRIBUTEBROWSER = 6000; //1027665
    @Transient
    int CT_LOOKUPMULTILEVEL = 6003;
    @Transient
    int CT_LOOKUPSCREEN = 13;
    int CT_LOOKUPTREE = 1027665;
    @Transient
    int CT_MULTISELECTIONLIST = 6013;
    @Transient
    int CT_MULTISELECTMENU = 1028401;
    @Transient
    int CT_PASSWORD = 1029355;
    @Transient
    int CT_RADIOBUTTON = 9;
    @Transient
    int CT_RICHTEXT = 1027763;
    @Transient
    int CT_TABLE = 28;
    @Transient
    int CT_TEXTAREA = 7;
    @Transient
    int CT_TEXTFIELD = 6;
    @Transient
    int CT_URLLINK = 1430357;
    
}
