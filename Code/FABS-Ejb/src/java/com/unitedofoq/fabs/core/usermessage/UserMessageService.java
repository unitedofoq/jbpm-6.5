/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.usermessage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;

import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.i18n.TextTranslationException;
import com.unitedofoq.fabs.core.i18n.TextTranslationServiceRemote;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import java.util.LinkedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author bassem
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote",
        beanInterface = UserMessageServiceRemote.class)
public class UserMessageService implements UserMessageServiceRemote {

    @EJB
    OEntityManagerRemote oem;

    @EJB
    TextTranslationServiceRemote textTranslationService;
    final static Logger logger = LoggerFactory.getLogger(UserMessageService.class);

    public List<UserMessage> loadUserMessageList(UserMessage UserMessage, OUser loggedUser)
            throws UserNotAuthorizedException {
        List<UserMessage> children = new ArrayList<UserMessage>();

        return children;
    }

    private UserMessage getUserMessageNotFound(String userMessageName) {
        logger.trace("Entering");
        UserMessage msgNotFound = new UserMessage();
        msgNotFound.setMessageTitle("User Message Not Found");
        msgNotFound.setMessageTitleTranslated("User Message Not Found");
        msgNotFound.setMessageText("UserMessage: \'" + userMessageName + "\', Not found in DB.");
        msgNotFound.setMessageTextTranslated("UserMessage: \'" + userMessageName + "\', Not found in DB.");
        msgNotFound.setMessageType(UserMessage.TYPE_ERROR);
        msgNotFound.setName("UserMessageNotFound");
        logger.trace("Returnig");
        return msgNotFound;
    }

    private UserMessage getMsgCpy(UserMessage originalUserMessage) {
        logger.trace("Entering");
        UserMessage msgCpy = new UserMessage();
        msgCpy.name = new String(originalUserMessage.getName());    // Don't replace new
        msgCpy.setNewDbid(originalUserMessage.getDbid());    // Don't replace new
        msgCpy.messageText = new String(originalUserMessage.getMessageText()); // Don't replace new
        msgCpy.messageTextTranslated = new String(originalUserMessage.getMessageTextTranslated()); // Don't replace new
        if (originalUserMessage.getMessageTitle() != null) {
            msgCpy.messageTitle = new String(originalUserMessage.getMessageTitle()); // Don't replace new
        }
        if (originalUserMessage.getMessageTitleTranslated() != null) {
            msgCpy.messageTitleTranslated = new String(originalUserMessage.getMessageTitleTranslated()); // Don't replace new
        }
        msgCpy.messageType = originalUserMessage.getMessageType();
        logger.trace("Returning");
        return msgCpy;
    }

    @Override
    public UserMessage getUserMessage(String name, OUser loggedUser) {
        logger.debug("Entering");
        UserMessage userMessage = null;
        userMessage = cachedUserMessage.get(loggedUser.getTenant().getId() + "_"
                + loggedUser.getFirstLanguage().getDbid() + "_" + name);
        if (userMessage == null) {
            logger.debug("UserMessage is Null");
            try {
                userMessage = (UserMessage) oem.loadEntity(
                        UserMessage.class.getSimpleName(),
                        Collections.singletonList("name = '" + name + "'"),
                        null,
                        oem.getSystemUser(loggedUser));

            } catch (Exception ex) {
                logger.error("Exception Thrown:", ex);
            } finally {
            }
            if (userMessage == null) {
                return getUserMessageNotFound(name);
            }
            cachedUserMessage.put(loggedUser.getTenant().getId() + "_"
                    + loggedUser.getFirstLanguage().getDbid() + "_" + name, userMessage);
            logger.debug("User Message Not Found In Cache & Loaded");
        } else {
            try {
                textTranslationService.loadEntityTranslation(userMessage, loggedUser);
            } catch (TextTranslationException ex) {
                logger.error("Exception:", ex);
            }
            logger.debug("User Message Got From Cache");
        }
        logger.debug("Returning");
        return getMsgCpy(userMessage); // Return message copy to avoid caching
        // the original message for the caller. EntityManager doesn't load
        // it again!
    }

    /**
     * Returns user message of name = userMessageName, with the text formatted
     * as following:
     * <br>Every text "<%1>" found in the original (DB) message text will be
     * replaced with msgParams.get(0); and so on for "<%2>" & get(1), etc...
     *
     * @param userMessageName
     * @param msgParams
     * @param loggedUser
     * @return
     */
    @Override
    public UserMessage getUserMessage(String userMessageName, List<String> msgParams, OUser loggedUser) {
        logger.debug("Entering");
        UserMessage msg = getUserMessage(userMessageName, loggedUser);
        if (msg == null) {
            logger.debug("message is Null");
            return getUserMessageNotFound(userMessageName);
        }
        msg = getMsgCpy(msg); // Return message copy to avoid caching
        // the original message for the caller. EntityManager doesn't load
        // it again!
        if (msgParams != null && msgParams.size() > 0) {
            logger.debug("message Params is Not Null and size bigger than zero");
            String msgTxt = msg.getMessageTextTranslated();

            try {
                for (int idx = 0; idx < msgParams.size(); idx++) {
                    if (msgParams.get(idx) == null) {
                        msgTxt = msgTxt.replaceAll("<%" + new Integer(idx + 1) + ">", "N/A");
                    } else {
                        msgTxt = msgTxt.replaceAll("<%" + new Integer(idx + 1) + ">", msgParams.get(idx));
                    }
                }
                if (Pattern.compile("\\<\\%[0-9]*?>").matcher(msgTxt).find()) {
                    logger.error("The number of variables assigned is not the same as the number of specified parameters");
                }
            } catch (Exception ex) {
                logger.error("Exception Thrown: ", ex);
            }
            msg.setMessageText(msgTxt);
            msg.setMessageTextTranslated(msgTxt);
        }
        logger.debug("Returning");
        return msg;
    }

    public ArrayList constructListFromStrings(String... args) {
        logger.debug("Entering");
        ArrayList argsList = new ArrayList();
        argsList.addAll(Arrays.asList(args));
        logger.debug("Entering");
        return argsList;
    }

    //<editor-fold defaultstate="collapsed" desc="Cache">
    @Override
    public OFunctionResult onUserMessageCachedUpdate(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            UserMessage userMessage = (UserMessage) odm.getData().get(0);
            cachedUserMessage.put(loggedUser.getTenant().getId() + "_"
                    + loggedUser.getFirstLanguage().getDbid() + "_" + userMessage.getName(), userMessage);
            logger.error("User Message Cache Updated");
        } catch (Exception ex) {
            logger.error("Exception thrown:", ex);
            oFR.addError(getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult preUserMessageCachedUpdate(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            UserMessage userMessage = (UserMessage) odm.getData().get(0);
            String oldName = ((UserMessage) oem.loadEntity("UserMessage", userMessage.getDbid(), null, null, loggedUser)).getName();
            if (!oldName.equals(userMessage.getName())) {
                cachedUserMessage.remove(loggedUser.getTenant().getId() + "_"
                        + loggedUser.getFirstLanguage().getDbid() + "_" + oldName);
                logger.error("User Message Removed From Cache");
            }
        } catch (Exception ex) {
            logger.error("Exception thrown:", ex);
            oFR.addError(getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult onUserMessageCachedRemove(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            UserMessage userMessage = (UserMessage) odm.getData().get(0);
            cachedUserMessage.remove(loggedUser.getTenant().getId() + "_"
                    + loggedUser.getFirstLanguage().getDbid() + "_" + userMessage.getName());
            logger.debug("User Message Removed From Cache");
        } catch (Exception ex) {
            logger.error("Exception thrown:", ex);
            oFR.addError(getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    public static Hashtable<String, UserMessage> cachedUserMessage = new Hashtable<String, UserMessage>();

    /**
     * Clears {@link #cachedUserMessage}
     */
    @Override
    public OFunctionResult clearUserMessageCache(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            cachedUserMessage.clear();
            logger.debug("User Message Cache Cleared");
        } catch (Exception ex) {
            logger.error("Exception thrown:", ex);
            oFR.addError(getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    //</editor-fold>
    @Override
    public UserMessage getUserMessage(String userMessageName, OUser loggedUser, String... msgParams) {
        List msgParamList = new LinkedList(Arrays.asList(msgParams));
        return getUserMessage(userMessageName, msgParamList, loggedUser);
    }

}
