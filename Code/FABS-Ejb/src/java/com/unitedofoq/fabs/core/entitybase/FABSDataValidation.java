/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.entitybase;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author arezk
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FABSDataValidation {
    enum FABSDataValidationType {
                PercentLTZero, Percent, PositivePercent,
                Decimal, DecimalLTZero, PositiveDecimal,
                MonthNumber, WeekDayNumber, HoursPerDay,
                DaysPerMonth, Number, PositiveNumber, NumberLTZero,
                Years
            }
    FABSDataValidationType validationType();
}
