package com.unitedofoq.fabs.core.entitybase;

import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.entitybase.TestNewEntityServiceLocal",
        beanInterface = TestNewEntityServiceLocal.class)
public class TestNewEntityService implements TestNewEntityServiceLocal {

    @EJB
    private DataTypeServiceRemote dataTypeService;
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private UserMessageServiceRemote userMessageService;

    @Override
    public OFunctionResult testInit(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();

        ODataMessage dm = new ODataMessage(dataTypeService.loadODataType("SingleObjectValue", loggedUser),
                Collections.singletonList((Object) "initFuncPass"));
        ofr.setReturnedDataMessage(dm);

        return ofr;
    }

    @Override
    public OFunctionResult testValidation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        TestNewEntity testNewEntity = (TestNewEntity) odm.getData().get(0);
        testNewEntity.setStrField1val(testNewEntity.getStrField1());
        return ofr;
    }

    @Override
    public OFunctionResult testPost(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        TestNewEntity testNewEntity = (TestNewEntity) odm.getData().get(0);
        testNewEntity.setStrField1post(testNewEntity.getStrField1());
        return ofr;
    }

    @Override
    public OFunctionResult testChangeFunction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        TestNewEntity testNewEntity = (TestNewEntity) odm.getData().get(0);
        Object newVal = odm.getData().get(2);
        if (newVal != null) {

            testNewEntity.setStrField1ch(newVal.toString() + (Math.random() * 100000));
            ofr.setReturnedDataMessage(odm);
        }
        return ofr;
    }

    @Override
    public OFunctionResult testSaveListAction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            List<BaseEntity> toBeSaved = new LinkedList<>();
            TestNewEntity masterEntity = new TestNewEntity();
            masterEntity.setDateField1(new Date());
            double random = Math.random() * 100000;
            masterEntity.setDecField1(BigDecimal.valueOf(random));
            masterEntity.setStrField1("Field1 by user " + loggedUser.getLoginName());
            masterEntity.setStrField1Tr("Field1Tr");
            masterEntity.setStrField1TrTranslated("Field1Trعربي");
            for (int i = 0; i < 1000; i++) {
                TestNewEntity element = (TestNewEntity) masterEntity.createNewInstance();
                masterEntity.setDecField1(BigDecimal.valueOf(Math.random() * 100000));
                element.setStrField1(element.getStrField1() + i);
                toBeSaved.add(element);
            }
            oem.saveEntityList(toBeSaved, loggedUser);
            ofr.addSuccess(userMessageService.getUserMessage("Success", loggedUser));
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return ofr;
    }

}
