package com.unitedofoq.fabs.core.security.user;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import java.util.List;
import javax.ejb.Local;

@Local
public interface LdapService {

    public boolean checkLdapConnection(OUser loggedUser);

    public boolean addUser(OUser user, OUser loggedUser);

    public boolean checkUserExistence(OUser user, OUser loggedUser);

    public boolean checkRoleExistence(ORole role, OUser loggedUser);

    public boolean addRoleUser(String userName, String roleName, OUser loggedUser);

    public boolean addRole(ORole role, OUser loggedUser);

    public boolean updateUser(OUser user, OUser loggedUser);

    public boolean updateRoleUser(String userName, String oldRoleName, String newRoleName, OUser loggedUser);

    public boolean updateRole(ORole role, OUser loggedUser);

    public boolean deleteRoleUser(String userName, String roleName, OUser loggedUser);

    public boolean deleteUser(OUser user, OUser loggedUser);

    public boolean deleteRole(ORole role, OUser loggedUser);

    public boolean changeUserPassword(OUser user, String newPassWord, OUser loggedUser);

    public String getPasswordFromLDAP(String loginName, OUser loggedUser);

    public List<OUser> getLdapUsers(OUser loggedUser);

    public List<RoleUser> getLdapRoleUsers(OUser loggedUser);

    public List<ORole> getLdapRoles(OUser loggedUser);

    public OFunctionResult validateLDAPConnection(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

}
