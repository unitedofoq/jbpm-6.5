package com.unitedofoq.fabs.core.dd;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.filter.OFilterServiceLocal;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.i18n.TextTranslationException;
import com.unitedofoq.fabs.core.i18n.TextTranslationServiceRemote;
import com.unitedofoq.fabs.core.report.OReport;
import com.unitedofoq.fabs.core.report.OReportField;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.dd.DDServiceRemote",
        beanInterface = DDServiceRemote.class)
public class DDService implements DDServiceRemote {

    @EJB
    public OEntityManagerRemote oem;
    @EJB
    public UserMessageServiceRemote userMessageServiceRemote;
    @EJB
    public OFunctionServiceRemote functionService;
    @EJB
    public EntitySetupServiceRemote entitySetupService;
    @EJB
    TextTranslationServiceRemote textTranslationService;
    @EJB
    private OFilterServiceLocal filterService;
    //<editor-fold defaultstate="collapsed" desc="Cache">
    public static Hashtable<String, DD> cachedDD = new Hashtable<String, DD>();

    /**
     * Clears {@link #cachedDD}
     */
    @Override
    public OFunctionResult clearDDCache(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        logger.debug("Clearing DD");
        try {
            cachedDD.clear();
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.debug("DD cache cleared");
            // </editor-fold>
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown");
            // </editor-fold>
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            oFR.setReturnedDataMessage(odm);
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult clearMyDDCache(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        logger.debug("Clearing My DD");
        try {

            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.debug("My DD cache cleared");
            // </editor-fold>
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Clearing My DD Exception");
            logger.error("Exception thrown: ", ex);
            // </editor-fold>

        } finally {
            oFR.setReturnedDataMessage(odm);
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult preDDCachedUpdate(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            DD dd = (DD) odm.getData().get(0);
            //String oldName = ((DD) oem.loadEntity("DD", dd.getDbid(), null, null, loggedUser)).getName();
            String ddQuery = "Select entity from DD entity where entity.dbid = " + dd.getDbid();
            String oldName = ((DD) oem.executeEntityQuery(ddQuery, loggedUser)).getName();
            if (!oldName.equals(dd.getName())) {
                cachedDD.remove(loggedUser.getTenant().getId() + "_"
                        + loggedUser.getFirstLanguage().getDbid() + "_" + oldName);
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.trace("DD Removed From Cache. DD: {}", dd);
                // </editor-fold>
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult onDDCachedUpdate(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            DD dd = (DD) odm.getData().get(0);
            cachedDD.put(loggedUser.getTenant().getId() + "_"
                    + loggedUser.getFirstLanguage().getDbid() + "_" + dd.getName(), dd);
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.trace("DD Updated From Cache. DD: {}", dd);
            // </editor-fold>
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult onDDCachedRemove(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            DD dd = (DD) odm.getData().get(0);
            cachedDD.remove(loggedUser.getTenant().getId() + "_"
                    + loggedUser.getFirstLanguage().getDbid() + "_" + dd.getName());
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.trace("DD Removed From Cache. DD: {}", dd);
            // </editor-fold>
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            // </editor-fold>
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }
    //</editor-fold>

    final static Logger logger = LoggerFactory.getLogger(DDService.class);

    /**
     * Gets the DD corresponding to the passed DD 'name'
     *
     * @param name The DD 'name'
     * @param loggedUser The user requesting the DD retrieval
     *
     * @return The corresponding DD
     */
    @Override
    public DD getDD(String name, OUser loggedUser) {
        logger.debug("Entering: getDD");
        if (name == null || "".equals(name)) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.debug("DD Name Is Invalid DDname: {}", name);
            // </editor-fold>
            logger.debug("Entering: getDD");
            return null;
        }
        //  retrieve DD
        DD dd = cachedDD.get(loggedUser.getTenant().getId() + "_"
                + loggedUser.getFirstLanguage().getDbid() + "_" + name);
        if (dd != null) {
            try {
                textTranslationService.loadEntityTranslation(dd, loggedUser);
            } catch (TextTranslationException ex) {
                logger.error("Exception thrown", ex);
            }
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.debug("DD Got From Cache DD: {}", dd);
            logger.debug("Returning from: getDD");
            return dd;
        }
        try {
            dd = (DD) oem.loadEntity("DD",
                    Collections.singletonList("name='" + name + "'"),
                    null, loggedUser);
            if (dd != null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.debug("DD Not Found In Cache & Loaded. DD: {}", dd);
                // </editor-fold>
                cachedDD.put(loggedUser.getTenant().getId() + "_"
                        + loggedUser.getFirstLanguage().getDbid() + "_" + name, dd);
            } else {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.debug("DD Name Not Found in DB. DD: {}", dd);
                // </editor-fold>
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
        } finally {
            logger.debug("Returning from: getDD");
            return dd;
        }
    }

    /**
     *
     * @param name
     * @param loggedUser
     * @return
     */
    @Override
    public String getDDLabel(String name, OUser loggedUser) {
        logger.debug("Entering: getDDLabel");
        String label = "";
        try {
            if (loggedUser.getFirstLanguage().getDbid() == 23)//English
            {
                label = (String) oem.executeEntityNativeQuery("select label from dd where name = '"
                        + name + "'", loggedUser);
            } else {
                label = (String) oem.executeEntityNativeQuery("select t0.label from ddtranslation t0, dd t1  where t1.name = '"
                        + name + "' and t0.language_dbid = " + loggedUser.getFirstLanguage().getDbid() + " and t0.entitydbid = t1.dbid", loggedUser);
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.debug("No Label is Found. DD Name: {}", name);
            // </editor-fold>
        } finally {
            logger.debug("Returning from: getDDLabel");
            return label;
        }
    }

    @Override
    public String getDDReportLabel(String name, OUser loggedUser) {
        logger.debug("Entering: getDDLabel");
        String label = "";
        try {
            label = (String) oem.executeEntityNativeQuery("select label from DDReport where name = '"
                    + name + "' and langdbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
        } finally {
            logger.debug("Returning from: getDDLabel");
            return label;
        }
    }

    /**
     * Gets the DD name Calls the BaseEntity.getDDName()
     */
    public String getDDName(BaseEntity entity, String fieldExpression) {
        logger.debug("Entering: getDDName");
        logger.debug("Returning from: getDDName");
        return BaseEntity.getFieldDDName(entity, fieldExpression);
    }

    public OFunctionResult validateDD(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateDD");
        OFunctionResult oFR = new OFunctionResult();
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
            OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                    odm, loggedUser, DD.class);
            if (inputValidationErrors != null) {
                return inputValidationErrors;
            }
            // </editor-fold>

            DD dd = (DD) odm.getData().get(0);
            //oFR.append(validateDDnotInEntity(loggedUser, dd));
            //oFR.append(validateDDnotUsed(loggedUser, dd));
            //oFR.append(ddDataValidation(loggedUser, dd));
            logger.debug("Returning from: validateDD");
            return oFR;
        } catch (Exception ex) {
            if (odm != null) {
                logger.debug("OEntity: {}", (OEntity) odm.getData().get(0));
            }
            logger.error("Exception thrown", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
            logger.debug("Returning from: validateDD");
            return oFR;
        }
    }

    public OFunctionResult validateDDnotInEntity(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateDDnotInEntity");
        OFunctionResult oFR = new OFunctionResult();
        try {
            oem.getEM(loggedUser);

            // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
            OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                    odm, loggedUser, DD.class);
            if (inputValidationErrors != null) {
                logger.debug("Returning from: validateDDnotInEntity");
                return inputValidationErrors;
            }
            // </editor-fold>
            DD dd = (DD) odm.getData().get(0);
            if (!dd.getName().contains("_")) {
                //Error Message
                //TODO: Ask Bassem about it.
                logger.debug("Returning from: validateDDnotInEntity");
                return oFR;
            }
            String ddname = dd.getName();
            String fieldExp = ddname.substring(ddname.indexOf("_") + 1);
            String entityName = ddname.substring(0, ddname.indexOf("_"));
            List<OEntity> entities = oem.loadEntityList(OEntity.class.getSimpleName(),
                    Collections.singletonList("entityClassPath like '%" + entityName + "'"),
                    null, null, loggedUser);
            if (entities == null || entities.size() == 0) {
                UserMessage errorMsg = userMessageServiceRemote.getUserMessage("EntityError",
                        Collections.singletonList(entityName), loggedUser);
            }
            boolean found = false;
            for (OEntity oEntity : entities) {
                Class oEntityCls = null;
                try {
                    oEntityCls = Class.forName(oEntity.getEntityClassPath());
                } catch (ClassNotFoundException ex) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.error("Exception thrown", ex);
                    // </editor-fold>
                    // Don't inform user, it's internal error oFR.addError(userMessageServiceRemote.getUserMessage(
                    // "SystemInternalError", loggedUser));
                    continue;
                }
                if (BaseEntity.getFieldDDName(oEntityCls, fieldExp) != null) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                UserMessage errorMsg = userMessageServiceRemote.getUserMessage("DDnotinEntity",
                        Collections.singletonList(dd.getName()), loggedUser);
                oFR.addWarning(errorMsg);
            }

        } catch (Exception ex) {
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
            logger.debug("Returning from: validateDDnotInEntity");
            logger.error("Exception thrown", ex);
            return oFR;
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning from: validateDDnotInEntity");
        }
        return oFR;
    }

    public OFunctionResult validateDDnotUsed(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateDDnotUsed");
        OFunctionResult oFR = new OFunctionResult();
        try {
            oem.getEM(loggedUser);

            // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
            OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                    odm, loggedUser, DD.class);
            if (inputValidationErrors != null) {
                return inputValidationErrors;
            }
            // </editor-fold>
            DD dd = (DD) odm.getData().get(0);
            String ddname = dd.getName();
            List<OReportField> reportFields;
            boolean found = false;
            List<String> conditions = new ArrayList();
            conditions.add("dd.dbid=" + dd.getDbid());
            List result = oem.loadEntityList("ScreenField",
                    conditions, null,
                    null, loggedUser);
            if (result == null || result.size() == 0) {
                found = false;
            } else {
                found = true;
            }
            if (!found) {
                if (ddname.indexOf("_") == -1) {
                    UserMessage errorMsg = userMessageServiceRemote.getUserMessage("DDNotUsed",
                            Collections.singletonList(dd.getName()), loggedUser);
                    oFR.addWarning(errorMsg);
                    logger.debug("Returning from: validateDDnotUsed");
                    return oFR;
                }
                String fieldExp = ddname.substring(ddname.indexOf("_") + 1);
                String entityName = ddname.substring(0, ddname.indexOf("_"));
                OEntity entity = entitySetupService.loadOEntityByClassName(entityName, loggedUser);
                if (entity != null) {
                    List<String> reportconditions = new ArrayList();
                    reportconditions.add("oactOnEntity.dbid=" + entity.getDbid());
                    List<OReport> reportresult = oem.loadEntityList(OReport.class.getSimpleName(),
                            reportconditions, null,
                            null, loggedUser);
                    boolean used = false;
                    if (reportresult != null || reportresult.size() > 0) {
                        for (int i = 0; i < reportresult.size(); i++) {
                            reportFields = reportresult.get(i).getFields();
                            for (int j = 0; j < reportFields.size(); j++) {
                                if (fieldExp.equalsIgnoreCase(reportFields.get(j).getFieldExpression())) {
                                    //dd is used
                                    used = true;
                                    break;
                                }

                            }
                            if (used) {
                                break;
                            }
                        }
                    }
                    if (!used) {
                        UserMessage errorMsg = userMessageServiceRemote.getUserMessage("DDNotUsed",
                                Collections.singletonList(dd.getName()), loggedUser);
                        oFR.addWarning(errorMsg);
                    }
                } else {
                    logger.warn("Entity Not Found. Entity Name: {}", entityName);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
            logger.debug("Returning from: validateDDnotUsed");
            return oFR;
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning from: validateDDnotUsed");
        }
        return oFR;
    }

    public OFunctionResult ddDataValidation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: ddDataValidation");
        OFunctionResult oFR = new OFunctionResult();
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
            OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                    odm, loggedUser, DD.class);
            if (inputValidationErrors != null) {
                logger.debug("Returning from: ddDataValidation");
                return inputValidationErrors;
            }
            // </editor-fold>
            DD dd = (DD) odm.getData().get(0);
            UserMessage errorMsg;
            //  if controlType is NULL...
            if (dd.getControlType() == null) {
                //  'controlType' must NOT be NULL...
                errorMsg = userMessageServiceRemote.getUserMessage(
                        "ControlTypeNull", Collections.singletonList(dd.getName()), loggedUser);
                oFR.addError(errorMsg);
                logger.debug("Returning from: ddDataValidation");
                return oFR;
            }
            //Lookup dropdown Filter cannot be Null for Literal Dropdown Control Type
            if ((dd.getLookupFilter() == null
                    || "".equals(dd.getLookupFilter())) && dd.getControlType().getDbid() == DD.CT_LITERALDROPDOWN) {
                errorMsg = userMessageServiceRemote.getUserMessage(
                        "DDFilterhasNoValue", Collections.singletonList(dd.getName()), loggedUser);
                oFR.addError(errorMsg);
                logger.debug("Returning from: ddDataValidation");
                return oFR;
            }
            //Lookup dropdown Filter cannot have a value for a non lookup control
            if (dd.getLookupFilter() != null && !"".equals(dd.getLookupFilter())
                    && (dd.getControlType().getDbid() != DD.CT_LITERALDROPDOWN
                    && dd.getControlType().getDbid() != DD.CT_DROPDOWN
                    && dd.getControlType().getDbid() != DD.CT_LOOKUPSCREEN
                    && dd.getControlType().getDbid() != DD.CT_LOOKUPATTRIBUTEBROWSER
                    && dd.getControlType().getDbid() != DD.CT_LOOKUPMULTILEVEL
                    && dd.getControlType().getDbid() != DD.CT_LOOKUPTREE
                    && dd.getControlType().getDbid() != DD.CT_AUTOCOMPLETE)) {
                errorMsg = userMessageServiceRemote.getUserMessage(
                        "DDFilterhasValue", Collections.singletonList(dd.getName()), loggedUser);
                oFR.addError(errorMsg);
                logger.debug("Returning from: ddDataValidation");
                return oFR;
            }
            //Lookup drop down Display field cannot have a value for a non Dropdown Control
            if (dd.getLookupDropDownDisplayField() != null && !"".equals(dd.getLookupDropDownDisplayField())
                    && dd.getControlType().getDbid() != DD.CT_DROPDOWN
                    && dd.getControlType().getDbid() != DD.CT_AUTOCOMPLETE) {
                errorMsg = userMessageServiceRemote.getUserMessage(
                        "DDDisplayFieldhasValue", Collections.singletonList(dd.getName()), loggedUser);
                oFR.addError(errorMsg);
                logger.debug("Returning from: ddDataValidation");
                return oFR;
            }
            //Lookup drop down Display field cannot be Null for a Dropdown Control
            if ((dd.getLookupDropDownDisplayField() == null
                    || dd.getLookupDropDownDisplayField().equals(""))
                    && dd.getControlType().getDbid() == DD.CT_DROPDOWN) {
                errorMsg = userMessageServiceRemote.getUserMessage(
                        "DDDisplayFieldhasNoValue", Collections.singletonList(dd.getName()), loggedUser);
                oFR.addError(errorMsg);
                logger.debug("Returning from: ddDataValidation");
                return oFR;
            }
            if (dd.getLookupScreen() != null) // Lookup Screen is set
            {
                // Lookup Screen cannot have a value for a non LookupScreen or Multilevel Control
                if (dd.getLookupScreen().getDbid() != 0 // Not ForcedInitialized
                        && dd.getControlType().getDbid() != DD.CT_LOOKUPSCREEN
                        && dd.getControlType().getDbid() != DD.CT_LOOKUPMULTILEVEL
                        && dd.getControlType().getDbid() != DD.CT_LOOKUPTREE
                        && dd.getControlType().getDbid() != DD.CT_LOOKUPTREE) {
                    errorMsg = userMessageServiceRemote.getUserMessage(
                            "DDLookupScreenhasValue", Collections.singletonList(dd.getName()), loggedUser);
                    oFR.addError(errorMsg);
                    logger.debug("Returning from: ddDataValidation");
                    return oFR;
                }
            } else {
                // Lookup Screen is null
                // Lookup Screen cannot be Null for LookupScreen or Multilevel Control
                if (dd.getControlType().getDbid() == DD.CT_LOOKUPSCREEN
                        || dd.getControlType().getDbid() == DD.CT_LOOKUPMULTILEVEL) {
                    errorMsg = userMessageServiceRemote.getUserMessage(
                            "DDLookupScreenhasNoValue", Collections.singletonList(dd.getName()), loggedUser);
                    oFR.addError(errorMsg);
                    logger.debug("Returning from: ddDataValidation");
                    return oFR;
                }
            }
            if (dd.getLookupFilter() != null && !dd.getLookupFilter().equals("")) {
                List<String> lookupFilter = new ArrayList();
                lookupFilter = filterService.parseDDLookupFilter(dd.getLookupFilter(), loggedUser);
                for (String value : lookupFilter) {
                    if (value.contains(",")) {
                        String[] result = value.split(",");
                        for (int index = 0; index < result.length; index++) {
                            if (result[index] == null || result[index].equals("")) {
                                errorMsg = userMessageServiceRemote.getUserMessage(
                                        "DDLookUpValueError", Collections.singletonList(dd.getName()), loggedUser);
                                oFR.addError(errorMsg);
                                logger.debug("Returning from: ddDataValidation");
                                return oFR;
                            }
                        }
                    } else if (dd.getControlType().getDbid() == DD.CT_LITERALDROPDOWN) {
                        errorMsg = userMessageServiceRemote.getUserMessage(
                                "DDLookUpValueError", Collections.singletonList(dd.getName()), loggedUser);
                        oFR.addError(errorMsg);
                        logger.debug("Returning from: ddDataValidation");
                        return oFR;
                    }
                }
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
            logger.debug("Returning from: ddDataValidation");
            return oFR;
        }
        logger.debug("Returning from: ddDataValidation");
        return oFR;
    }

    /**
     * Create Field Default DD as following: <br>1. Name is Entity Class Name +
     * "_" + Field Name <br>2. Text field of size = 20 <br>3. Module is of the
     * oEntity <br>4. Header, Label, and Translations = Field Name
     *
     * @param field
     * @param oEntity
     * @param loggedUser
     * @return oFR with returned value = created DD in case of success
     */
    @Override
    public OFunctionResult createFieldDefaultDD(String ddName, Field field, OEntity oEntity,
            OUser loggedUser) {
        logger.debug("Entering: createFieldDefaultDD, First");
        OFunctionResult oFR = new OFunctionResult();
        try {
            DD fieldDD = new DD();
            String fieldName = field.getName();
            fieldDD.setName(ddName);
            fieldDD.setControlSize(20);
            fieldDD.setLabel(fieldName);
            fieldDD.setLabelTranslated(fieldName);
            fieldDD.setHeader(fieldName);
            fieldDD.setHeaderTranslated(fieldName);
            UDC controlType = (UDC) oem.loadEntity("UDC",
                    Collections.singletonList("value='Text Field'"),
                    null, oem.getSystemUser(loggedUser));
            fieldDD.setControlType(controlType);
            fieldDD.setOmodule(oEntity.getOmodule());

            oFR.append(entitySetupService.callEntityCreateAction(fieldDD, loggedUser));
            if (oFR.getErrors().isEmpty()) {
                oFR.addReturnValue(fieldDD);
            }

        } catch (Exception ex) {

            if (field != null && oEntity != null) {
                logger.debug("Field: {} OEntiity: {}", field.getName(), oEntity.getEntityClassName());
            }
            // <editor-fold defaultstate="collapsed" desc="Log">

            logger.error("Exception thrown", ex);
            // </editor-fold>

            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning from: ddDataValidation, First");
            return oFR;
        }
    }

    @Override
    public OFunctionResult createFieldDefaultDD(String ddName, Field field,
            OEntityDTO oEntity, OUser loggedUser) {
        logger.debug("Entering: createFieldDefaultDD, Second");
        OFunctionResult oFR = new OFunctionResult();
        try {
            DD fieldDD = new DD();
            String fieldName = field.getName();
            fieldDD.setName(ddName);
            fieldDD.setControlSize(20);
            fieldDD.setLabel(fieldName);
            fieldDD.setLabelTranslated(fieldName);
            fieldDD.setHeader(fieldName);
            fieldDD.setHeaderTranslated(fieldName);
            UDC controlType = (UDC) oem.loadEntity("UDC",
                    Collections.singletonList("value='Text Field'"),
                    null, oem.getSystemUser(loggedUser));
            fieldDD.setControlType(controlType);
            OEntity entity = entitySetupService.loadOEntity(oEntity.getEntityClassPath(), loggedUser);
            fieldDD.setOmodule(entity.getOmodule());

            oFR.append(entitySetupService.callEntityCreateAction(fieldDD, loggedUser));
            if (oFR.getErrors().isEmpty()) {
                oFR.addReturnValue(fieldDD);
            }

        } catch (Exception ex) {
            if (field != null && oEntity != null) {
                logger.debug("Field: {} OEntiity: {}", field.getName(), oEntity.getEntityClassName());
            }
            logger.error("Exception thrown", ex);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning from: ddDataValidation, Second");
            return oFR;
        }
    }

    public OFunctionResult validateDDLookUpValue(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateDDLookUpValue");
        OFunctionResult oFR = new OFunctionResult();
        DD dd = (DD) odm.getData().get(0);
        String lookUpFilter = dd.getLookupFilter();
        List<String> lookupFilter = new ArrayList();
        lookupFilter = filterService.parseDDLookupFilter(dd.getLookupFilter(), loggedUser);
        logger.debug("Returning from: validateDDLookUpValue");
        return oFR;
    }

    public OFunctionResult checkIfHelpTextNull(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateDDLookUpValue");
        OFunctionResult oFR = new OFunctionResult();
        DD dd = (DD) odm.getData().get(0);
        if (dd.isEnableInlineHelp() && (null == dd.getInlineHelpTranslated() || dd.getInlineHelpTranslated().equals(""))) {
            oFR.addError(userMessageServiceRemote.getUserMessage("HelpTextIsEmpty", loggedUser));
        }
        logger.debug("Returning from: validateDDLookUpValue");
        return oFR;
    }
}
