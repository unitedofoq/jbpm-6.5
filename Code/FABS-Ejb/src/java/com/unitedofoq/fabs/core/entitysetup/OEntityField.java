package com.unitedofoq.fabs.core.entitysetup;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields = {"oentity"})
public class OEntityField extends BaseEntity {
    //<editor-fold defaultstate="collapse" desc="oentity">

    @ManyToOne//(fetch = javax.persistence.FetchType.LAZY)
    private OEntity oentity;

    public OEntity getOentity() {
        return oentity;
    }

    public void setOentity(OEntity oentity) {
        this.oentity = oentity;
    }

    public String getOentityDD() {
        return "OEntityField_oentity";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="fieldExpression">
    private String fieldExpression;

    public String getFieldExpression() {
        return fieldExpression;
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }

    public String getFieldExpressionDD() {
        return "OEntityField_fieldExpression";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="securityTag">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC securityTag;

    public UDC getSecurityTag() {
        return securityTag;
    }

    public void setSecurityTag(UDC securityTag) {
        this.securityTag = securityTag;
    }

    public String getSecurityTagDD() {
        return "OEntityField_securityTag";
    }
    //</editor-fold>
    private boolean encrypted;

    public boolean isEncrypted() {
        return encrypted;
    }

    public void setEncrypted(boolean encrypted) {
        this.encrypted = encrypted;
    }

    public String getEncryptedDD() {
        return "OEntityField_encrypted";
    }
}