package com.unitedofoq.fabs.core.i18n;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.udc.UDC;

@Entity
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class TextTranslation extends ObjectBaseEntity{

    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OModule omodule;

    public String getOmoduleDD(){
        return "TextTranslation_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
// </editor-fold>

    @Column(nullable=false, unique=true)
    private long entityDBID;

    @Column(nullable=false, unique=true)
    private String fieldExpression;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="language_dbid", nullable=false, unique=true)
    private UDC language = null;

    @Column(nullable=false)
    private String textValue;

    /* DD NAMES GETTERS */
    public String getEntityDBIDDD()      { return "TextTranslation_entityDBID"; }
    public String getFieldExpressionDD() { return "TextTranslation_fieldExpression"; }
    public String getLanguageDD()        { return "TextTranslation_language"; }
    public String getTextValueDD()       { return "TextTranslation_textValue"; }

    /*  GETTERS  */
    public long   getEntityDBID()      { return entityDBID; }
    public String getFieldExpression() { return fieldExpression; }
    public UDC    getLanguage()        { return language; }
    public String getTextValue()       { return textValue; }

    /*  SETTERS  */
    public void setEntityDBID(long entityDBID)             { this.entityDBID = entityDBID; }
    public void setFieldExpression(String fieldExpression) { this.fieldExpression = fieldExpression; }
    public void setLanguage(UDC language)                  { this.language = language; }
    public void setTextValue(String textValue)             { this.textValue = textValue; }
}
