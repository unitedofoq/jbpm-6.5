/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.entitysetup;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;


/**
 *
 * @author mmohamed
 */
@Entity
@ParentEntity(fields={"entity"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class OEntityHatField extends BaseEntity {

    private String fieldExpression;

    public String getFieldExpression() {
        return fieldExpression;
    }

    public String getFieldExpressionDD() {
        return "OEntityHatField_fieldExpression";
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OEntity entity;

    public OEntity getEntity() {
        return entity;
    }

    public void setEntity(OEntity entity) {
        this.entity = entity;
    }
}
