package com.unitedofoq.fabs.core.report;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.multimedia.OImage;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@ChildEntity(fields = {"fields", "groups", "reportFilter", "reportFunctions"})
@DiscriminatorColumn(name="DTYPE")
@VersionControlSpecs(omoduleFieldExpression = "omodule")
//@FABSEntitySpecs(customCascadeFields={"reportFilter","layoutType", "oactOnEntity", "logo"})
public class OReport extends ObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OModule omodule;

    public String getOmoduleDD(){
        return "OReport_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="name">
    @Column(unique = true, name = "name", nullable = false)
    private String name;

    public String getName() {
        return name;
    }

    public String getNameDD() {
        return "OReport_name";
    }

    public void setName(String name) {
        this.name = name;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="onePercent">
    private boolean onePercent;

    public String getOnePercentDD() {
        return "OReport_onePercent";
    }

    public boolean isOnePercent() {
        return onePercent;
    }

    public void setOnePercent(boolean onePercent) {
        this.onePercent = onePercent;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="title">
    @Translatable(translationField = "titleTranslated")
    private String title;

    public String getTitle() {
        return title;
    }

    public String getTitleDD() {
        return "OReport_title";
    }

    public void setTitle(String title) {
        this.title = title;
    }
    @Transient
    @Translation(originalField = "title")
    private String titleTranslated;

    public String getTitleTranslatedDD() {
        return "OReport_title";
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    private String description;

    public String getDescription() {
        return description;
    }

    public String getDescriptionTranslatedDD() {
        return "OReport_description";
    }

    public void setDescription(String description) {
        this.description = description;
    }
    @Translation(originalField = "description")
    @Transient
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="subTitle">
    @Translatable(translationField = "subTitleTranslated")
    private String subTitle;

    public String getSubTitle() {
        return subTitle;
    }

    public String getSubTitleDD() {
        return "OReport_subTitle";
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }
    @Transient
    @Translation(originalField = "subTitle")
    private String subTitleTranslated;

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="header">
    private String header;

    public String getHeader() {
        return header;
    }

    public String getHeaderDD() {
        return "OReport_header";
    }

    public void setHeader(String header) {
        this.header = header;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="footer">
    private String footer;

    public String getFooter() {
        return footer;
    }

    public String getFooterDD() {
        return "OReport_footer";
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="pageHeader">
    private String pageHeader;

    public String getPageHeader() {
        return pageHeader;
    }

    public String getPageHeaderDD() {
        return "OReport_pageHeader";
    }

    public void setPageHeader(String pageHeader) {
        this.pageHeader = pageHeader;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="pageFooter">
    private String pageFooter;

    public String getPageFooter() {
        return pageFooter;
    }

    public String getPageFooterDD() {
        return "OReport_pageFooter";
    }

    public void setPageFooter(String pageFooter) {
        this.pageFooter = pageFooter;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="landscape">
    private boolean landscape;

    public boolean isLandscape() {
        return landscape;
    }

    public String getLandscapeDD() {
        return "OReport_landscape";
    }

    public void setLandscape(boolean landscape) {
        this.landscape = landscape;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="layoutType">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private OReportLayout layoutType;

    public OReportLayout getLayoutType() {
        return layoutType;
    }

    public String getLayoutTypeDD() {
        return "OReport_layoutType";
    }

    public void setLayoutType(OReportLayout layoutType) {
        this.layoutType = layoutType;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="oactOnEntity">
    @JoinColumn(name = "OEntity_DBID")
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private OEntity oactOnEntity;

    public OEntity getOactOnEntity() {
        return oactOnEntity;
    }

    public String getOactOnEntityDD() {
        return "OReport_oactOnEntity";
    }

    public void setOactOnEntity(OEntity oactOnEntity) {
        this.oactOnEntity = oactOnEntity;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fields">
    @OneToMany(mappedBy = "oreport")
    private List<OReportField> fields;

    public List<OReportField> getFields() {
        List<OReportField> notDeletedFields = new ArrayList<OReportField>();
        for (OReportField field : fields) {
            if (!field.isInActive()) {
                notDeletedFields.add(field);
            }
        }
        return notDeletedFields;
    }

    public String getFieldsDD() {
        return "OReport_fields";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="sort">
    private String sort;

    public String getSort() {
        return sort;
    }

    public String getSortDD() {
        return "OReport_sort";
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="filter">
    private String filter;

    public String getFilter() {
        return filter;
    }

    public String getFilterDD() {
        return "OReport_filter";
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="userFilters">
    @OneToMany(mappedBy = "oreport")
    private List<OReportUserFilter> userFilters;

    public List<OReportUserFilter> getUserFilters() {
        return userFilters; 
    }
    
    public void setUserFilters(List<OReportUserFilter> userFilters) {
        this.userFilters = userFilters; 
    }

    public String getUserFiltersDD() {
        return "OReport_userFilters";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="groups">
    @OneToMany(mappedBy = "oreport")
    private List<OReportGroup> groups;

    public List<OReportGroup> getGroups() {
        List<OReportGroup> notDeletedGroups = new ArrayList<OReportGroup>();
        for (OReportGroup group : groups) {
            if (!group.isInActive()) {
                notDeletedGroups.add(group);
            }
        }
        return notDeletedGroups;
    }

    public String getGroupsDD() {
        return "OReport_groups";
    }

    public void setGroups(List<OReportGroup> groups) {
        this.groups = groups;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="inputs">
//    @OneToMany(mappedBy="oreport")
    private List<OReportInput> inputs;

    public List<OReportInput> getInputs() {
        return inputs;
    }

    public String getInputsDD() {
        return "OReport_inputs";
    }

    public void setInputs(List<OReportInput> inputs) {
        this.inputs = inputs;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reportGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "ReportGroup_DBID")
    private ReportGroup reportGroup;

    public ReportGroup getReportGroup() {
        return reportGroup;
    }

    public String getReportGroupDD() {
        return "OReport_reportGroup";
    }

    public void setReportGroup(ReportGroup reportGroup) {
        this.reportGroup = reportGroup;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reportFilter">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, mappedBy = "report")
    private ReportFilter reportFilter;

    public ReportFilter getReportFilter() {
        return reportFilter;
    }

    public void setReportFilter(ReportFilter reportFilter) {
        this.reportFilter = reportFilter;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reportFunctions">
    @OneToMany(mappedBy="report")
    private List<ReportFunction> reportFunctions;

    public List<ReportFunction> getReportFunctions() {
        return reportFunctions;
    }

    public String getReportFunctionsDD() {
        return "OReport_reportFunctions";
    }

    public void setReportFunctions(List<ReportFunction> reportFunctions) {
        this.reportFunctions = reportFunctions;
    }
    //</editor-fold>
    @Override
    public void PrePersist() {
        super.PrePersist();
        if (reportFilter != null) {
            if (reportFilter.getReport() == null) {
                reportFilter.setReport(this);
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="reportlogo">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private OImage logo;

    /**
     * @return the logo
     */
    public OImage getLogo() {
        return logo;
    }

    public String getLogoDD() {
        return "OReport_logo";
    }

    /**
     * @param logo the logo to set
     */
    public void setLogo(OImage logo) {
        this.logo = logo;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="customFilterScreen">
    //filter screen of template report only used till now,
    // ORepTemplDSFiltersBean uses SelectManyMenu it i s 1%
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OScreen customFilterScreen;
   
    /**
     * @return the customFilterScreen
     */
    public OScreen getCustomFilterScreen() {
        return customFilterScreen;
    }

    /**
     * @param customFilterScreen the customFilterScreen to set
     */
    public void setCustomFilterScreen(OScreen customFilterScreen) {
        this.customFilterScreen = customFilterScreen;
    }
    /**
     * @return the customFilterScreenDD
     */
    public String getCustomFilterScreenDD() {
        return "OReport_customFilterScreen";
    }
     //</editor-fold>
    /**
     * @return the subTitleTranslated
     */
    public String getSubTitleTranslated() {
        return subTitleTranslated;
    }

    public String getSubTitleTranslatedDD() {
        return "OReport_subTitle";
    }
    
    /**
     * @param subTitleTranslated the subTitleTranslated to set
     */
    public void setSubTitleTranslated(String subTitleTranslated) {
        this.subTitleTranslated = subTitleTranslated;
    }

    /**
     * @return the titleTranslated
     */
    public String getTitleTranslated() {
        return titleTranslated;
    }

    /**
     * @param titleTranslated the titleTranslated to set
     */
    public void setTitleTranslated(String titleTranslated) {
        this.titleTranslated = titleTranslated;
    }
}