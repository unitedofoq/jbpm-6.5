/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.Exception;

import com.unitedofoq.fabs.core.ExceptionHandler.thrower.factory.ExceptionType;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessage;

/**
 *
 * @author mostafa
 */
public class BasicException extends Exception{
    private UserMessage userMessage;
    private boolean logged;
    private ExceptionType exceptionType;
    private String exceptionMessage;
    private OUser loggedUser;

    public BasicException(String exceptionMessage, ExceptionType exceptionType, OUser loggedUser) {
        this.exceptionMessage = exceptionMessage;
        this.exceptionType = exceptionType;
        this.loggedUser = loggedUser;
    }
    
    public BasicException(UserMessage userMessage, ExceptionType exceptionType, OUser loggedUser) {
        this.userMessage = userMessage;
        this.exceptionType = exceptionType;
        this.loggedUser = loggedUser;
    }

    public ExceptionType getExceptionType() {
        return exceptionType;
    }

    public void setExceptionType(ExceptionType exceptionType) {
        this.exceptionType = exceptionType;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    public OUser getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(OUser loggedUser) {
        this.loggedUser = loggedUser;
    }

    
    public UserMessage getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(UserMessage userMessage) {
        this.userMessage = userMessage;
    }

    public boolean isLogged() {
        return logged;
    }

    public void setLogged(boolean logged) {
        this.logged = logged;
    }
    
    public String getMessageCode(){
        return (null == userMessage)? "":userMessage.getName();
    }
}
