/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.scheduler.manager;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

/**
 *
 * @author Hammad
 */
@Local
public interface JavaFunctionSchedulerManagerLocal {

    public OFunctionResult addSchedule(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult deleteSchedule(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult updateSchedule(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult updateCustomAlert(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
}
