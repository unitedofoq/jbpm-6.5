/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.module.OModule;


/**
 *
 * @author fabs
 */
@Entity
@Table(name="OSCREEN")
public class PrsnlzdFormScreen extends BaseEntity 
{
    // <editor-fold defaultstate="collapsed" desc="stype">
    private String stype = "FORM" ;

    public String getStype() {
        return stype;
    }

    public void setStype(String stype) {
        this.stype = stype;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="showActiveOnly">
    private int showActiveOnly;
        /**
     * @return the showActiveOnly
     */
    public int getShowActiveOnly() {
        return showActiveOnly;
    }

    /**
     * @param showActiveOnly the showActiveOnly to set
     */
    public void setShowActiveOnly(int showActiveOnly) {
        this.showActiveOnly = showActiveOnly;
    }

    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="showHeader">
    private int showHeader ;
    
    /**
     * @return the showHeader
     */
    public int getShowHeader() {
        return showHeader;
    }

    /**
     * @param showHeader the showHeader to set
     */
    public void setShowHeader(int showHeader) {
        this.showHeader = showHeader;
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="columnsNo">
    private int columnsNo;
    public String getColumnsNoDD() {
        return "FormScreen_columnsNo";
    }

    public int getColumnsNo() {
        return columnsNo;
    }

    public void setColumnsNo(int columns) {
        this.columnsNo = columns;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OModule omodule;

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }

    public String getOmoduleDD(){
        return "OScreen_omodule";
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="name">
    @Column(nullable=false, unique=true)
    private String name;
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="header">
    private String header;

    /**
     * @return the header
     */
    public String getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(String header) {
        this.header = header;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Personalization Fields: prsnlzdOriginal_DBID, prsnlzdUser_DBID">
    private long prsnlzdOriginal_DBID;
    private long prsnlzdUser_DBID;
    /**
     * @return the prsnlzdOriginal_DBID
     */
    public long getPrsnlzdOriginal_DBID() {
        return prsnlzdOriginal_DBID;
    }

    /**
     * @param prsnlzdOriginal_DBID the prsnlzdOriginal_DBID to set
     */
    public void setPrsnlzdOriginal_DBID(long prsnlzdOriginal_DBID) {
        this.prsnlzdOriginal_DBID = prsnlzdOriginal_DBID;
    }

    /**
     * @return the prsnlzdUser_DBID
     */
    public long getPrsnlzdUser_DBID() {
        return prsnlzdUser_DBID;
    }

    /**
     * @param prsnlzdUser_DBID the prsnlzdUser_DBID to set
     */
    public void setPrsnlzdUser_DBID(long prsnlzdUser_DBID) {
        this.prsnlzdUser_DBID = prsnlzdUser_DBID;
    }
// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="oactOnEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OEntity oactOnEntity = null;
    /**
     * @return the oactOnEntity
     */
    public OEntity getOactOnEntity() {
        return oactOnEntity;
    }

    /**
     * @param oactOnEntity the oactOnEntity to set
     */
    public void setOactOnEntity(OEntity oactOnEntity) {
        this.oactOnEntity = oactOnEntity;
    }
    // </editor-fold>
}