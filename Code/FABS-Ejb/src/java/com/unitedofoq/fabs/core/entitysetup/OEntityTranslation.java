
package com.unitedofoq.fabs.core.entitysetup;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class OEntityTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="title">
    @Column
    private String title;

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
    // </editor-fold>

}
