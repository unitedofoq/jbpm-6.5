/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;

/**
 *
 * @author bassem
 */
@Entity
public class GaugeScreenValue extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="gaugeScreen">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private GaugeScreen gaugeScreen;

    public String getGaugeScreenDD() {
        return "GaugeScreenValue_gaugeScreen";
    }
    
    public GaugeScreen getGaugeScreen() {
        return gaugeScreen;
    }

    public void setGaugeScreen(GaugeScreen gaugeScreen) {
        this.gaugeScreen = gaugeScreen;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="gvalue">
    private double gvalue;

    public String getGvalueDD() {
        return "GaugeScreenValue_gvalue";
    }

    public double getGvalue() {
        return gvalue;
    }

    public void setGvalue(double gvalue) {
        this.gvalue = gvalue;
    }
    
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="color">
    private String color;

    public String getColorDD() {
        return "GaugeScreenValue_color";
    }
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    // </editor-fold>
}
