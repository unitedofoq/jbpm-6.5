/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.navigationmenu;

import com.unitedofoq.fabs.central.OTenantDTO;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.ejb.Local;

/**
 *
 * @author satef
 */
@Local
public interface NMDataLoader {

    public HashMap<String, ConcurrentHashMap> constractDTOCach();

    public HashMap<String, ConcurrentHashMap> ConstractTanentDTOCach(String tanentID);

    public void constractIndexCach(ConcurrentHashMap<Long, ArrayList<NavigationMenuDTO>> role_menu,
            HashMap<Long, ArrayList<OMenuDTO>> menu_submenus, HashMap<Long, ArrayList<OfunctionDTO>> menu_functions);

    public HashMap<String, ArrayList<String>> getMenu_fun_indexs();

    public HashMap<String, ArrayList<String>> getMenu_submenu_indexs();

    public HashMap<String, ArrayList<String>> getRole_menu_indexs();

    public HashMap<String, OTenantDTO> getTenantsDTOList1();

    public List<ORoleDTO> getRolesDTOList(OTenantDTO oTenantDTO);

    public HashMap<String, OTenantDTO> getTenants();

    public HashMap<String, ScreenFunctionDTO> getScreenFunctionListDTO(OUser loggedUser);

    public HashMap<String, Map<String, Object>> getScreensSessionInfo();

    public String constructScreenFunctionsLinks(OfunctionDTO ofunction, OUser loggedUser);

    public ScreenFunctionDTO getScreenFunctionDTO(OUser loggedUser, long dbid);
}
