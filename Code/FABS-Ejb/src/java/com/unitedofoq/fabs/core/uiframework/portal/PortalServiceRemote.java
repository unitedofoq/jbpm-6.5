/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.portal;

import java.util.List;

import javax.ejb.Local;

/**
 *
 * @author ahussien
 */
@Local
public interface PortalServiceRemote {

    public List<OUserPortalPage> getUserPortalPages(String userLoginName);

    public void updateUserPortalPageID(long userPortalPageId, long layoutPlid);
    
}
