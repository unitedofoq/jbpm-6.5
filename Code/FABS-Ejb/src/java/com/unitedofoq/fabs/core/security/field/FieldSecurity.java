/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.security.field;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author ahussien
 */
@Entity
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class FieldSecurity extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OModule omodule;

    public String getOmoduleDD(){
        return "FieldSecurity_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
// </editor-fold>

    @Column(unique=true, nullable=false)
    private String fieldExpression;

    @Column(nullable=false)
    private boolean granted = true;

    public boolean isGranted() {
        return granted;
    }

    public void setGranted(boolean granted) {
        this.granted = granted;
    }

   

    @ManyToMany(fetch = javax.persistence.FetchType.LAZY)
    List<OUser> grantedUsers = null;

    public String getFieldExpression() {
        return fieldExpression;
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }

   

    public List<OUser> getGrantedUsers() {
        return grantedUsers;
    }

    public void setGrantedUsers(List<OUser> grantedUsers) {
        this.grantedUsers = grantedUsers;
    }
}
