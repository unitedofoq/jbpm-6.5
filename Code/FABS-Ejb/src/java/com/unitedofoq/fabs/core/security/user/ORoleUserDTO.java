/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.security.user;

/**
 *
 * @author Melad
 */
public class ORoleUserDTO {
    private long dbid;
    private long oRoleDBID;
    private long oUserDBID;

    public ORoleUserDTO() {
    }

    public ORoleUserDTO(long dbid, long oRoleDBID, long oUserDBID) {
        this.dbid = dbid;
        this.oRoleDBID = oRoleDBID;
        this.oUserDBID = oUserDBID;
    }

    public long getDbid() {
        return dbid;
    }

    public void setDbid(long dbid) {
        this.dbid = dbid;
    }

    public long getoRoleDBID() {
        return oRoleDBID;
    }

    public void setoRoleDBID(long oRoleDBID) {
        this.oRoleDBID = oRoleDBID;
    }

    public long getoUserDBID() {
        return oUserDBID;
    }

    public void setoUserDBID(long oUserDBID) {
        this.oUserDBID = oUserDBID;
    }
    
}
