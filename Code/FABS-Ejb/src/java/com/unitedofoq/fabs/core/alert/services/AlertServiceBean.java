/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.alert.services;

import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.core.alert.entites.dto.AlertDTO;
import com.unitedofoq.fabs.core.alert.entities.Alert;
import com.unitedofoq.fabs.core.alert.entities.AlertCondition;
import com.unitedofoq.fabs.core.alert.entities.AlertParam;
import com.unitedofoq.fabs.core.alert.entities.BaseAlert;
import com.unitedofoq.fabs.core.alert.entities.CustomAlert;
import com.unitedofoq.fabs.core.alert.entities.FiredAlert;
import com.unitedofoq.fabs.core.alert.entities.Reciever;
import com.unitedofoq.fabs.core.comunication.EMailSenderLocal;
import com.unitedofoq.fabs.core.comunication.SMSSenderLocal;
import com.unitedofoq.fabs.core.content.AttachmentDTO;
import com.unitedofoq.fabs.core.content.EntityAttachmentServiceLocal;
import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.encryption.DataEncryptorServiceLocal;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.BaseEntityMetaData;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.function.JavaFunction;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.module.ModuleServiceRemote;
import com.unitedofoq.fabs.core.report.customReports.AbstractReportGenerator;
import com.unitedofoq.fabs.core.report.customReports.CustomReport;
import com.unitedofoq.fabs.core.report.customReports.CustomReportServiceLocal;
import com.unitedofoq.fabs.core.report.customReports.PDFReportGenerator;
import com.unitedofoq.fabs.core.report.customReports.ReportDTO;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserServiceRemote;
import com.unitedofoq.fabs.core.setup.FABSSetup;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.sql.Statement;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

/**
 *
 * @author nkhalil
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.alert.services.AlertServiceLocal",
        beanInterface = AlertServiceLocal.class)
public class AlertServiceBean implements AlertServiceLocal {

    @EJB
    OEntityManagerRemote oem;
    @EJB
    FABSSetupLocal fABSSetupLocal;
    @EJB
    UserMessageServiceRemote usrMsgService;
    @EJB
    EntitySetupServiceRemote entitySetupService;
    @EJB
    UserMessageServiceRemote userMessageService;
    @EJB
    OCentralEntityManagerRemote oCentralEntityManager;
    @EJB
    EMailSenderLocal eMailSender;
    @EJB
    SMSSenderLocal smsService;
    @EJB
    protected ModuleServiceRemote moduleService;
    @EJB
    protected UserServiceRemote userService;
    @EJB
    protected DataTypeServiceRemote dataTypeService;
    @EJB
    protected OFunctionServiceRemote ofunctionService;
    @EJB
    protected DataEncryptorServiceLocal dataEncryptorService;
    @EJB
    EntityAttachmentServiceLocal attachmentService;
    
    @EJB
    private CustomReportServiceLocal customReportService;

    final static Logger logger = LoggerFactory.getLogger(AlertServiceBean.class);

    @Override
    public List<FiredAlert> getAllFiredAlert(OUser loggedUser) {
        logger.debug("Entering");
        List<FiredAlert> alerts = new ArrayList<FiredAlert>();
        try {
            List<String> cond = new ArrayList<String>();
            cond.add("eMail = '" + loggedUser.getEmail() + "'");
            cond.add(OEntityManager.CONFCOND_GET_ACTIVEONLY);

            alerts = oem.loadEntityList(FiredAlert.class.getSimpleName(), cond, null, null, oem.getSystemUser(loggedUser));

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } finally {
            if (alerts != null) {
                logger.debug("Returning with alerts of size: {}", alerts.size());
            }
            return alerts;
        }
    }

    @Override
    public List<AlertDTO> getAlertJobDTOs(OUser oUser) {
        return getAlertJobDTOs(oUser, null);
    }
    
    @Override
    public List<AlertDTO> getAlertJobDTOs(OUser oUser, String alertTitle) {
        logger.debug("Entering");
        List<AlertDTO> alertDTOs = new ArrayList<AlertDTO>();
        String query = "SELECT NEW com.unitedofoq.fabs.core.alert.entites.dto.AlertDTO("
                + "alert.dbid, "
                + "schedule.dbid,"
                + "actOnEntity.dbid,"
                + "alert.messageSubject,"
                + "alert.messageBody,"
                + "alert.title, "
                + "alert.sendViaEmail,"
                + "alert.sendViaSMS,"
                + "actOnEntity.entityClassPath)"
                + "FROM Alert alert "
                + "LEFT JOIN alert.schedule schedule "
                + "LEFT JOIN alert.actOnEntity actOnEntity "
                + "WHERE alert.inActive = false";
        
        if (alertTitle != null) {
            query += " and alert.title = '" + alertTitle + "'";
        }
        
        logger.trace("Query: {}", query);
        try {
            alertDTOs = new ArrayList<AlertDTO>((Vector<AlertDTO>) oem.executeEntityListQuery(query, oUser));
        } catch (Exception ex) {
            logger.error("Exception while executing Query", ex);
        }
        if (alertDTOs != null) {
            logger.debug("Returning with alertDTOs of size: {}", alertDTOs.size());
        } else {
            logger.debug("Returning with Null");
        }

        return alertDTOs;
    }

    @Override
    public boolean validateCondition(BaseEntity baseEntity,
            String fieldExpression, String operator, String conditionValue,
            OUser oUser) {
        logger.debug("Entering");
        try {
            Class actOnEntityCls = Class.forName(baseEntity.getClass().getName());
            int fieldType = getFieldType(fieldExpression, actOnEntityCls, oUser);
            if (oUser != null) {
                logger.debug("ouser: {} fieldExpression: {}", oUser.getLoginName(), fieldExpression);
            } else {
                logger.debug("ouser equals Null");
            }

            if (-1 == fieldType) {
                logger.warn("No Valid Data Type is Found");
                logger.debug("Returning with false");
                return false;
            }
            if (">".equals(operator)) {
                switch (fieldType) {
                    case 0:
                        long longFieldValue = (Long) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        long longCondValue = Long.parseLong(conditionValue);
                        logger.debug("Returning with: {}", (longFieldValue > longCondValue));
                        return (longFieldValue > longCondValue);
                    case 1:
                        int intFieldValue = (Integer) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        int intCondValue = Integer.parseInt(conditionValue);
                        logger.debug("Returning with: {}", (intFieldValue > intCondValue));
                        return (intFieldValue > intCondValue);
                    case 2:
                        short shortFieldValue = (Short) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        short shortCondValue = Short.parseShort(conditionValue);
                        logger.debug("Returning with: {}", (shortFieldValue > shortCondValue));
                        return (shortFieldValue > shortCondValue);
                    case 3:
                        float floatFieldValue = (Float) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        float floatCondValue = Float.parseFloat(conditionValue);
                        logger.debug("Returning with: {}", (floatFieldValue > floatCondValue));
                        return (floatFieldValue > floatCondValue);
                    case 4:
                        double doubleFieldValue = (Double) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        double doubleCondValue = Double.parseDouble(conditionValue);
                        logger.debug("Returning with: {}", (doubleFieldValue > doubleCondValue));
                        return (doubleFieldValue > doubleCondValue);
                    case 5:
                        Date dateFieldValue = (Date) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        Date dateCondValue = new SimpleDateFormat().parse(conditionValue);
                        logger.debug("Returning with: {}", dateFieldValue.after(dateCondValue));
                        return (dateFieldValue.after(dateCondValue));
                    case 6:
                        logger.debug("Returning with false");
                        return false;
                    case 7:
                        logger.debug("Returning false");
                        return false;
                }
            } else if (">=".equals(operator)) {
                switch (fieldType) {
                    case 0:
                        long fieldValue = (Long) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        long condValue = Long.parseLong(conditionValue);
                        logger.debug("Returning with: {}", (fieldValue > condValue) || (fieldValue == condValue));
                        return (fieldValue > condValue || fieldValue == condValue);
                    case 1:
                        int intFieldValue = (Integer) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        int intCondValue = Integer.parseInt(conditionValue);
                        logger.debug("Returning with: {} ", intFieldValue > intCondValue || intFieldValue
                                == intCondValue);
                        return ((intFieldValue > intCondValue) || (intFieldValue
                                == intCondValue));
                    case 2:
                        short shortFieldValue = (Short) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        short shortCondValue = Short.parseShort(conditionValue);
                        logger.debug("Returning with: {}", shortFieldValue > shortCondValue
                                || shortFieldValue == shortCondValue);
                        return (shortFieldValue > shortCondValue
                                || shortFieldValue == shortCondValue);
                    case 3:
                        float floatFieldValue = (Float) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        float floatCondValue = Float.parseFloat(conditionValue);
                        logger.debug("Returning with: {}", floatFieldValue > floatCondValue
                                || floatCondValue == floatFieldValue);
                        return (floatFieldValue > floatCondValue
                                || floatCondValue == floatFieldValue);
                    case 4:
                        double doubleFieldValue = (Double) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        Double doubleCondValue = Double.parseDouble(conditionValue);
                        logger.debug("Returning with: {}", doubleFieldValue > doubleCondValue
                                || doubleCondValue == doubleFieldValue);
                        return (doubleFieldValue > doubleCondValue
                                || doubleCondValue == doubleFieldValue);
                    case 5:
                        Date dateFieldValue = (Date) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        Date dateCondValue = new SimpleDateFormat().parse(conditionValue);
                        logger.debug("Returning with: {}", dateFieldValue.after(dateCondValue)
                                || dateFieldValue.compareTo(dateCondValue) == 0);
                        return (dateFieldValue.after(dateCondValue)
                                || dateFieldValue.compareTo(dateCondValue) == 0);
                    case 6:
                        boolean booleanFieldValue = (Boolean) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        boolean booleanCondValue = Boolean.parseBoolean(conditionValue);
                        logger.debug("Returning with: {}", booleanFieldValue == booleanCondValue);
                        return (booleanFieldValue == booleanCondValue);
                    case 7:
                        String stringFieldValue = (String) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        logger.debug("Returning with: {}", stringFieldValue.equals(conditionValue));
                        return (stringFieldValue.equals(conditionValue));
                }
            } else if ("=".equals(operator)) {
                switch (fieldType) {
                    case 0:
                        long fieldValue = (Long) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        long condValue = Long.parseLong(conditionValue);
                        logger.debug("Returning with: {}", fieldValue == condValue);
                        return (fieldValue == condValue);
                    case 1:
                        int intFieldValue = (Integer) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        int intCondValue = Integer.parseInt(conditionValue);
                        logger.debug("Returning with: {}", intFieldValue == intCondValue);
                        return (intFieldValue == intCondValue);
                    case 2:
                        short shortFieldValue = (Short) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        short shortCondValue = Short.parseShort(conditionValue);
                        logger.debug("Returning with: {}", shortFieldValue == shortCondValue);
                        return (shortFieldValue == shortCondValue);
                    case 3:
                        float floatFieldValue = (Float) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        float floatCondValue = Float.parseFloat(conditionValue);
                        logger.debug("Returning with: {}", floatFieldValue == floatCondValue);
                        return (floatFieldValue == floatCondValue);
                    case 4:
                        Double doubleFieldValue = (Double) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        Double doubleCondValue = Double.parseDouble(conditionValue);
                        logger.debug("Returning with: {}", doubleFieldValue == doubleCondValue);
                        return (doubleFieldValue == doubleCondValue);
                    case 5:
                        Date dateFieldValue = (Date) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        Date dateCondValue = new SimpleDateFormat().parse(conditionValue);
                        logger.debug("Returning with: {}", dateFieldValue.compareTo(dateCondValue) == 0);
                        return (dateFieldValue.compareTo(dateCondValue) == 0);
                    case 6:
                        boolean booleanFieldValue = (Boolean) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        boolean booleanCondValue = Boolean.parseBoolean(conditionValue);
                        logger.debug("Returning with: {}", booleanFieldValue == booleanCondValue);
                        return (booleanFieldValue == booleanCondValue);
                    case 7:
                        String stringFieldValue = (String) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        logger.debug("Returning with: {}", stringFieldValue.equals(conditionValue));
                        return (stringFieldValue.equals(conditionValue));
                }
            } else if ("!=".equals(operator)) {
                switch (fieldType) {
                    case 0:
                        long fieldValue = (Long) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        long condValue = Long.parseLong(conditionValue);
                        logger.debug("Returning with: {}", fieldValue != condValue);
                        return (fieldValue != condValue);
                    case 1:
                        int intFieldValue = (Integer) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        int intCondValue = Integer.parseInt(conditionValue);
                        logger.debug("Returning with: {}", intFieldValue != intCondValue);
                        return (intFieldValue != intCondValue);
                    case 2:
                        short shortFieldValue = (Short) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        short shortCondValue = Short.parseShort(conditionValue);
                        logger.debug("Returning with: {}", shortFieldValue != shortCondValue);
                        return (shortFieldValue != shortCondValue);
                    case 3:
                        float floatFieldValue = (Float) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        float floatCondValue = Float.parseFloat(conditionValue);
                        logger.debug("Returning with: {}", floatFieldValue != floatCondValue);
                        return (floatFieldValue != floatCondValue);
                    case 4:
                        double doubleFieldValue = (Double) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        Double doubleCondValue = Double.parseDouble(conditionValue);
                        logger.debug("Returning with: {}", doubleFieldValue != doubleCondValue);
                        return (doubleFieldValue != doubleCondValue);
                    case 5:
                        Date dateFieldValue = (Date) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        Date dateCondValue = new SimpleDateFormat().parse(conditionValue);
                        logger.debug("Returning with: {}", dateFieldValue.compareTo(dateCondValue) != 0);
                        return (dateFieldValue.compareTo(dateCondValue) != 0);
                    case 6:
                        boolean booleanFieldValue = (Boolean) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        boolean booleanCondValue = Boolean.parseBoolean(conditionValue);
                        logger.debug("Returning with: {}", booleanFieldValue != booleanCondValue);
                        return (booleanFieldValue != booleanCondValue);
                    case 7:
                        String stringFieldValue = (String) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        logger.debug("Returning with: {}", !stringFieldValue.equals(conditionValue));
                        return (!stringFieldValue.equals(conditionValue));
                }
            } else if ("<".equals(operator)) {
                switch (fieldType) {
                    case 0:
                        long fieldValue = (Long) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        long condValue = Long.parseLong(conditionValue);
                        logger.debug("Returning with: {}", fieldValue < condValue);
                        return (fieldValue < condValue);
                    case 1:
                        int intFieldValue = (Integer) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        int intCondValue = Integer.parseInt(conditionValue);
                        logger.debug("Returning with: {}", intFieldValue < intCondValue);
                        return (intFieldValue < intCondValue);
                    case 2:
                        short shortFieldValue = (Short) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        short shortCondValue = Short.parseShort(conditionValue);
                        logger.debug("Returning with: {}", shortFieldValue < shortCondValue);
                        return (shortFieldValue < shortCondValue);
                    case 3:
                        float floatFieldValue = (Float) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        float floatCondValue = Float.parseFloat(conditionValue);
                        logger.debug("Returning with: {}", floatFieldValue < floatCondValue);
                        return (floatFieldValue < floatCondValue);
                    case 4:
                        double doubleFieldValue = (Double) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        Double doubleCondValue = Double.parseDouble(conditionValue);
                        logger.debug("Returning with: {}", doubleFieldValue < doubleCondValue);
                        return (doubleFieldValue < doubleCondValue);
                    case 5:
                        Date dateFieldValue = (Date) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        Date dateCondValue = new SimpleDateFormat().parse(conditionValue);
                        logger.debug("Returning with: {}", dateFieldValue.before(dateCondValue));
                        return (dateFieldValue.before(dateCondValue));
                    case 6:
                        logger.debug("Returning with false");
                        return false;
                    case 7:
                        logger.debug("Returning with false");
                        return false;
                }
            } else if ("=<".equals(operator)) {
                switch (fieldType) {
                    case 0:
                        long fieldValue = (Long) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        long condValue = Long.parseLong(conditionValue);
                        logger.debug("Returning with: {}", fieldValue == condValue || fieldValue < condValue);
                        return (fieldValue == condValue || fieldValue < condValue);
                    case 1:
                        int intFieldValue = (Integer) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        long intCondValue = Integer.parseInt(conditionValue);
                        logger.debug("Returning with: {}", intFieldValue < intCondValue
                                || intFieldValue == intCondValue);
                        return (intFieldValue < intCondValue
                                || intFieldValue == intCondValue);
                    case 2:
                        short shortFieldValue = (Short) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        short shortCondValue = Short.parseShort(conditionValue);
                        logger.debug("Returning with: {}", shortFieldValue < shortCondValue
                                || shortCondValue == shortFieldValue);
                        return (shortFieldValue < shortCondValue
                                || shortCondValue == shortFieldValue);
                    case 3:
                        float floatFieldValue = (Float) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        float floatCondValue = Float.parseFloat(conditionValue);
                        logger.debug("Returning with: {}", floatFieldValue < floatCondValue
                                || floatCondValue == floatFieldValue);
                        return (floatFieldValue < floatCondValue
                                || floatCondValue == floatFieldValue);
                    case 4:
                        double doubleFieldValue = (Double) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        Double doubleCondValue = Double.parseDouble(conditionValue);
                        logger.debug("Returning with: {}", doubleFieldValue < doubleCondValue
                                || doubleCondValue == doubleFieldValue);
                        return (doubleFieldValue < doubleCondValue
                                || doubleCondValue == doubleFieldValue);
                    case 5:
                        Date dateFieldValue = (Date) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        Date dateCondValue = new SimpleDateFormat().parse(conditionValue);
                        logger.debug("Returning with: {}", dateFieldValue.before(dateCondValue)
                                || dateCondValue.compareTo(dateFieldValue) == 0);
                        return (dateFieldValue.before(dateCondValue)
                                || dateCondValue.compareTo(dateFieldValue) == 0);
                    case 6:
                        boolean booleanFieldValue = (Boolean) BaseEntity.
                                getValueFromEntity(baseEntity, fieldExpression);
                        boolean booleanCondValue = Boolean.parseBoolean(conditionValue);
                        logger.debug("Returning with: {}", booleanFieldValue == booleanCondValue);
                        return (booleanFieldValue == booleanCondValue);
                    case 7:
                        logger.debug("Returning with false");
                        return false;
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.debug("Returning with false");
            return false;
        }
        logger.debug("Returning with true");
        return true;
    }

    private int getFieldType(String str, Class actOnEntityCls, OUser oUser) {
        logger.trace("Entering");
        try {
            List<ExpressionFieldInfo> parseFieldExpression = BaseEntityMetaData.parseFieldExpression(actOnEntityCls, str);
            Class fieldClass = parseFieldExpression.get(parseFieldExpression.size() - 1).fieldClass;
            String fieldExp = parseFieldExpression.get(parseFieldExpression.size() - 1).fieldName;
            Field field = BaseEntity.getClassField(fieldClass, fieldExp);
            if (field.getType().isAssignableFrom(Long.class)
                    || field.getType().isAssignableFrom(long.class)) {
                logger.trace("Returning with 0");
                return 0;
            }
            if (field.getType().isAssignableFrom(Integer.class)
                    || field.getType().isAssignableFrom(int.class)) {
                logger.trace("Returning with 1");
                return 1;
            }
            if (field.getType().isAssignableFrom(Short.class)
                    || field.getType().isAssignableFrom(short.class)) {
                logger.trace("Returning with 2");
                return 2;
            }
            if (field.getType().isAssignableFrom(Float.class)
                    || field.getType().isAssignableFrom(float.class)) {
                logger.trace("Returning with 3");
                return 3;
            }
            if (field.getType().isAssignableFrom(Double.class)
                    || field.getType().isAssignableFrom(double.class)) {
                logger.trace("Returning with 4");
                return 4;
            }
            if (field.getType().isAssignableFrom(Date.class)) {
                logger.trace("Returning with 5");
                return 5;
            }
            if (field.getType().isAssignableFrom(Boolean.class)
                    || field.getType().isAssignableFrom(boolean.class)) {
                logger.trace("Returning with 6");
                return 6;
            }
            if (field.getType().isAssignableFrom(String.class)) {
                logger.trace("Returning with 7");
                return 7;
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.trace("Returning with -1");
        return -1;
    }

    @Override
    public boolean fireAlert(AlertDTO alertDTO, OUser loggedUser) {
        logger.debug("Entering");
        boolean sendViaEmail = alertDTO.isSendViaEmail();
        boolean sendViaSMS = alertDTO.isSendViaSMS();
        boolean result = true;
        if (sendViaEmail) {
            FABSSetup fromMail = fABSSetupLocal.loadKeySetup("SMTP_FROM_EMAIL", loggedUser);
            String from = "ofoqfabs@gmail.com";
            if (fromMail != null && fromMail.getSvalue() != null) {
                from = fromMail.getSvalue();
            }
            result &= eMailSender.sendEMail(from, alertDTO, loggedUser);
        }
        if (sendViaSMS) {
            //result &= smsService.sendSMS(alertDTO, loggedUser);
        }

        //Append result for new types of execution - ex. SMS
        logger.debug("Returning with: {}", result);
        return result;
    }

    @Override
    public OFunctionResult attachAlertFunction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            BaseAlert alert = (BaseAlert) odm.getData().get(0);

            JavaFunction javaFunction = new JavaFunction();
            javaFunction.setFunctionName("runAlert");
            javaFunction.setName("runAlert");
            javaFunction.setFunctionClassPath(AlertServiceLocal.class.getCanonicalName());
            String title = alert.getTitle().replaceAll("\\s", "");
            javaFunction.setCode(title);
            javaFunction.setServerJob(true);
            javaFunction.setOmodule(moduleService.loadOModule("System", oem.getSystemUser(loggedUser)));
            javaFunction = (JavaFunction) oem.saveEntity(javaFunction, loggedUser);
            alert.setJavaFunction(javaFunction);
            oem.saveEntity(alert, loggedUser);
            oFR.addSuccess(usrMsgService.getUserMessage("DataSaved", loggedUser));

        } catch (Exception ex) {
            logger.error("Exception while alert function", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public OFunctionResult validateAlertSql(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        UserMessage filterSQLErrorMsg = usrMsgService.getUserMessage("filterSQLError", loggedUser);//alertSqlError
        UserMessage paramSQLErrorMsg = usrMsgService.getUserMessage("paramSQLError", loggedUser);
        UserMessage tosSqlErrorMsg = usrMsgService.getUserMessage("tosSqlError", loggedUser);
        UserMessage ccsSqlErrorMsg = usrMsgService.getUserMessage("ccsSqlError", loggedUser);
        UserMessage attachSqlErrorMsg = usrMsgService.getUserMessage("attachSqlError", loggedUser);
        UserMessage reportParameterSqlErrorMsg = usrMsgService.getUserMessage("reportParameterSqlError", loggedUser);
        UserMessage reportPasswordSqlErrorMsg = usrMsgService.getUserMessage("reportPasswordSqlError", loggedUser);

        CustomAlert customAlert = (CustomAlert) odm.getData().get(0);
        try {
            String filterSQL = customAlert.getFilterSQL().replaceAll("(?i)%dbid%", " 0 ");
            oem.executeEntityListNativeQuery(filterSQL, loggedUser);
        } catch (Exception ex) {
            logger.error(customAlert.getFilterSQL());
            logger.error(filterSQLErrorMsg.getName(), ex);
            oFR.addError(filterSQLErrorMsg, ex);
        }
        if (null != customAlert.getParamSQL() && !"".equals(customAlert.getParamSQL())) {
            String paramSQL = customAlert.getParamSQL().replaceAll("(?i)%dbid%", " 0 ");
            try {
                oem.executeEntityListNativeQuery(paramSQL, loggedUser);
            } catch (Exception ex) {
                logger.error("paramSQL",paramSQL);
                logger.error(paramSQLErrorMsg.getName(), ex);
                oFR.addError(paramSQLErrorMsg, ex);
            }

        }
        String toSQL = customAlert.getToSQL().replaceAll("(?i)%dbid%", " 0 ");
        try {
            oem.executeEntityListNativeQuery(toSQL, loggedUser);
        } catch (Exception ex) {
            logger.error("toSQL",toSQL);
            logger.error(tosSqlErrorMsg.getName(), ex);
            oFR.addError(tosSqlErrorMsg, ex);
        }

        if (null != customAlert.getCcSQL() && !"".equals(customAlert.getCcSQL())) {
            String ccsSQL = customAlert.getCcSQL().replaceAll("(?i)%dbid%", " 0 ");
            try {
                oem.executeEntityListNativeQuery(ccsSQL, loggedUser);
            } catch (Exception ex) {
                logger.error(ccsSQL);
                logger.error(ccsSqlErrorMsg.getName(), ex);
                oFR.addError(ccsSqlErrorMsg, ex);
            }

        }
        if (null != customAlert.getAttachementSQL() && !"".equals(customAlert.getAttachementSQL())) {
            String attachmentSql = customAlert.getAttachementSQL().replaceAll("(?i)%dbid%", " 0 ");
            try {
                oem.executeEntityListNativeQuery(attachmentSql, loggedUser);
            } catch (Exception ex) {
                logger.error("attachmentSql",attachmentSql);
                logger.error(attachSqlErrorMsg.getName(), ex);
                oFR.addError(attachSqlErrorMsg, ex);
            }
        }
        if (null != customAlert.getReportParameterSQL() && !"".equals(customAlert.getReportParameterSQL())) {
            String reportParameterSQL = customAlert.getReportParameterSQL().replaceAll("(?i)%dbid%", " 0 ");
            try {
                oem.executeEntityListNativeQuery(reportParameterSQL, loggedUser);
            } catch (Exception ex) {
                logger.error("reportParameterSQL",reportParameterSQL);
                logger.error(reportParameterSqlErrorMsg.getName(), ex);
                oFR.addError(reportParameterSqlErrorMsg, ex);
            }
        }
        if (null != customAlert.getReportPasswordSQL() && !"".equals(customAlert.getReportPasswordSQL())) {
            String getReportPasswordSQL = customAlert.getReportPasswordSQL().replaceAll("(?i)%dbid%", " 0 ");
            try {
                oem.executeEntityListNativeQuery(getReportPasswordSQL, loggedUser);
            } catch (Exception ex) {
                logger.error("getReportPasswordSQL",getReportPasswordSQL);
                logger.error(reportParameterSqlErrorMsg.getName(), ex);
                oFR.addError(reportParameterSqlErrorMsg, ex);
            }
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public OFunctionResult runAlert(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            for (int i = 0; i < odm.getData().size(); i++) {
                if(odm.getData().get(i) instanceof CustomAlert){
                BaseAlert alert = (BaseAlert) odm.getData().get(i);
                List<AlertDTO> alertDTOs;
                if (alert instanceof CustomAlert) {//Using SQLs
                    CustomAlert customAlert = (CustomAlert) alert;
                    customAlert.setFilterSQL(customAlert.getFilterSQL().replaceAll("(?i)%dbid%",
                            String.valueOf(((BaseEntity) odm.getData().get(0)).getDbid())));
                    alertDTOs = buildAlert(customAlert, loggedUser);
                } else {//Using OEntity
                    alertDTOs = buildAlert((Alert) alert, loggedUser);
                }
                if (null != alertDTOs) {
                    for (AlertDTO alertDTO : alertDTOs) {
                        if (alertDTO != null) {
                            fireAlert(alertDTO, loggedUser);
                        }
                    }
                }
              }
            }
        } catch (Exception ex) {
            logger.error("Exception while running alert", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        logger.debug("Returning");
        return oFR;
    }
    
    private List<AlertDTO> buildAlert(Alert alert, OUser loggedUser) {
        logger.debug("Entering");
        List<BaseEntity> alertEntities = getAlertEntities(alert, loggedUser);
        if (null == alertEntities || alertEntities.isEmpty()) {
            logger.debug("No Data found");
            return new ArrayList<AlertDTO>();
        }
        List<AlertDTO> allAlertDTOs = new ArrayList<AlertDTO>();
        for (BaseEntity entity : alertEntities) {
            AlertDTO alertDTO = setAlertDTOGeneralParameters(alert);
            List<String> alertParam = parseAlertParam(alert.getAlertParams(),
                    entity);
            String subject = buildAlertMessageDetail(alert.getMessageSubject(), alertParam);
            String body = buildAlertMessageDetail(alert.getMessageBody(), alertParam);
            List<String> tos = getReceivers(entity, alert.getRecievers(),
                    "TO");
            List<String> ccs = getReceivers(entity, alert.getRecievers(),
                    "CC");

            alertDTO.setMessageSubject(subject);
            alertDTO.setMessageBody(body);
            alertDTO.setTos(tos);
            alertDTO.setCcs(ccs);
            allAlertDTOs.add(alertDTO);
        }
        logger.debug("Returning");
        return allAlertDTOs;
    }

    private List<AlertDTO> buildAlert(CustomAlert customAlert, OUser loggedUser) {
        logger.debug("Entering");
        List<Long> filtredAlertObjectDBIDs = parseFilterSQL(customAlert.getFilterSQL(), loggedUser);
        if (null == filtredAlertObjectDBIDs || filtredAlertObjectDBIDs.isEmpty()) {
            logger.debug("No Data found");
            return null;
        }
        List<AlertDTO> allAlertDTOs = new ArrayList<AlertDTO>();
        for (Object id : filtredAlertObjectDBIDs) {
            long dbid = 0;

            // Oracle uses BigDecimal instead of Long
            if (id instanceof BigDecimal) {
                dbid = ((BigDecimal) id).longValue();
            } else {
                dbid = (Long) id;
            }

            AlertDTO alertDTO = setAlertDTOGeneralParameters(customAlert);
            List<String> alertParam = parseAlertParam(customAlert.getParamSQL(),
                    dbid, loggedUser);
            String subject = buildAlertMessageDetail(customAlert.getMessageSubject(), alertParam);
            String body = buildAlertMessageDetail(customAlert.getMessageBody(), alertParam);
            if (null == body || "".equals(body)) {
                body = "Empty Body";
            }
            List<String> tos = parseReceiversSQL(customAlert.getToSQL(), dbid, loggedUser);
            List<String> ccs = parseReceiversSQL(customAlert.getCcSQL(), dbid, loggedUser);
            //get attached elements & put it in file
            AttachmentDTO csvAttachement = null;
            AttachmentDTO pdfAttachement = null;
            List<AttachmentDTO> attachmentsList = new ArrayList<AttachmentDTO>();
            String attachementSql = customAlert.getAttachementSQL();
            if (null != attachementSql && !attachementSql.equals("")) {
                List<Object> attachementObjects = parseSQL(attachementSql, dbid, loggedUser);
                attachementSql = attachementSql.replaceAll("(?i)%dbid%", String.valueOf(dbid));
                String[] excelColumns = getSqlColumnsNames(attachementSql, loggedUser);
                csvAttachement = createExcelAttachment(attachementObjects, excelColumns);
                attachmentsList.add(csvAttachement);
            }
            //get reportParameterSql and send it to report to run
            String reportParameterSql = customAlert.getReportParameterSQL();
            Object parameterObject = null;
            if (null != reportParameterSql && !reportParameterSql.equals("")) {
                List<Object> parametersList = parseSQL(reportParameterSql, dbid, loggedUser);

                if (parametersList != null && !parametersList.isEmpty()) {
                    parameterObject = parametersList.get(0);
                } else if(parametersList != null && parametersList.isEmpty()){
                    parameterObject = new BaseEntity();
                }
                reportParameterSql = reportParameterSql.replaceAll("(?i)%dbid%", String.valueOf(dbid));
            }
            String reportPassword =null;
            String reportPasswordSql = customAlert.getReportPasswordSQL();
            if(null != reportPasswordSql && !reportPasswordSql.equals("")){
                List<Object> password = parseSQL(reportPasswordSql, dbid, loggedUser);
                if (password != null && !password.isEmpty()) {
                    reportPassword = password.get(0).toString();
                }
            }
            AttachmentDTO attachmentDTO = null;
            if (null != customAlert.getCustomReport() && customAlert.getCustomReport().getDbid() != 0) {
                try {
                    attachmentDTO = attachmentService.get(customAlert.getCustomReport().getDbid(),
                           customAlert.getCustomReport().getClassName(), loggedUser).get(0);
                } catch (Exception ex) {
                    logger.error("No Attachement", ex);
                }

                pdfAttachement = generateCustomReport(parameterObject, reportParameterSql, attachmentDTO, customAlert.getCustomReport(), reportPassword, loggedUser);
                attachmentsList.add(pdfAttachement);
            }
            
            alertDTO.setMessageSubject(subject);
            alertDTO.setMessageBody(body);
            alertDTO.setTos(tos);
            alertDTO.setCcs(ccs);
            //alertDTO.setFilePath(filePath);
            alertDTO.setAttachments(attachmentsList);
            allAlertDTOs.add(alertDTO);
        }

        logger.debug("Returning");
        return allAlertDTOs;
    }

    private List<String> parseAlertParam(List<AlertParam> alertParams,
            BaseEntity entity) {
        logger.debug("Entering");
        if (null == alertParams || alertParams.isEmpty()) {
            return new ArrayList<String>();
        }
        List<String> alertParamValues = new ArrayList<String>();
        for (AlertParam alertParam : alertParams) {
            String value = "";
            try {
                value = (String) BaseEntity.getValueFromEntity(entity,
                        alertParam.getFieldExpression());
            } catch (Exception e) {
                logger.error("Couldn't get the value", e);
                continue;
            }
            alertParamValues.add(value);
        }
        logger.debug("Returning");
        return alertParamValues;
    }

    private List<String> parseAlertParam(String paramSQL, long filteredDBID,
            OUser loggedUser) {
        logger.debug("Entering");
        if (null == paramSQL || "".equals(paramSQL)) {
            return new ArrayList<String>();
        }
        List<String> alertparam = new ArrayList<String>();
        Vector<Object> data = new Vector<Object>();
        String parSql = "";
        try {
            parSql = paramSQL.replaceAll("(?i)%dbid%", String.valueOf(filteredDBID));
            data.addAll(oem.executeEntityListNativeQuery( parSql, loggedUser));
            for (Object datainst : data) {
                if (datainst instanceof Object[]) {
                    Object[] subdata = (Object[]) data.get(0);
                    for (int i = 0; i < subdata.length; i++) {
                        if (subdata[i] == null){
                            alertparam.add("null");
                        } else {
                            alertparam.add(subdata[i].toString());
                        }
                    }
                } else {
                    alertparam.add(data.get(0).toString());

                }
            }
        } catch (Exception ex) {
            logger.error("Alert Parameters:{}", parSql);
            logger.error("Exception while executing Query", ex);
        }
        logger.debug("Returning");
        return alertparam;
    }

    private String buildAlertMessageDetail(String messageSubject, List<String> alertParam) {
        logger.debug("Entering");
        String subject = messageSubject;
        if (null != alertParam && !alertParam.isEmpty()) {
            int paramIndex = 0;
            for (String param : alertParam) {
                if (null == param) {
                    param = "Null";
                }
                if(null != subject)
                    subject = subject.replaceAll("&lt;%" + ++paramIndex + "%&gt;", param);
            }
        }
        logger.debug("Returning");
        return subject;
    }

    private List<String> parseReceiversSQL(String receiverSQL, long filteredDBID,
            OUser loggedUser) {
        logger.debug("Entering");
        if (null == receiverSQL || "".equals(receiverSQL)) {
            return new ArrayList<String>();
        }
        Vector<Object> data = new Vector<Object>();

        List<String> tos = new ArrayList<String>();
        String rs = "";
        try {
            rs = receiverSQL.replaceAll("(?i)%dbid%" , String.valueOf(filteredDBID));
            data.addAll(oem.executeEntityListNativeQuery(rs , loggedUser));
            for (Object datainst : data) {
                if (datainst instanceof Object[]) {
                    Object[] subdata = (Object[]) datainst;
                    for (int i = 0; i < subdata.length; i++) {
                        if (subdata[i] != null) {
                            tos.add(subdata[i].toString());
                        }
                    }
                } else {
                    if (datainst != null) {
                        tos.add(datainst.toString());
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Alert receiver SQL:{}", rs);
            logger.error("Exception while executing Query", ex);
        }
        logger.debug("Returning");
        return tos;
    }

    private AlertDTO setAlertDTOGeneralParameters(BaseAlert alert) {
        AlertDTO alertDTO = new AlertDTO();
        alertDTO.setTitle(alert.getTitle());
        alertDTO.setSendViaEmail(alert.isSendViaEmail());
        alertDTO.setSendViaSMS(alert.isSendViaSMS());
        return alertDTO;
    }

    private List<Long> parseFilterSQL(String filterSQL, OUser loggedUser) {
        logger.debug("Entering");
        if (null == filterSQL || "".equals(filterSQL)) {
            return new ArrayList<Long>();
        }
        List<Long> filterSQLDBIDs = new ArrayList<Long>();
        try {
            List <Object> temp = oem.executeEntityListNativeQuery(filterSQL, loggedUser);
            //in case db is oracle because native query return dbids big decimal
            if (null != temp && !temp.isEmpty()) {
                if (temp.get(0) instanceof BigDecimal) {
                    for (Object dbids : temp) {
                        filterSQLDBIDs.add(((BigDecimal) dbids).longValue());
                    }
                } else {
                    for (Object dbids : temp) {
                        filterSQLDBIDs.add((Long) dbids);
                    }
                }
            }
            
        } catch (Exception ex) {
            logger.error("Alert Filter SQL:{}", filterSQL);
            logger.error("Exception while executing Query", ex);
        }
        logger.debug("Returning");
        return filterSQLDBIDs;
    }

    private List<BaseEntity> getAlertEntities(Alert alert, OUser loggedUser) {
        logger.debug("Entering");
        List<BaseEntity> alertEntities = new ArrayList<BaseEntity>();
        OEntity alertOEntity = alert.getActOnEntity();
        List<String> conditions = new ArrayList<String>();
        List<AlertCondition> alertConditions = alert.getAlertConditions();
        for (AlertCondition condition : alertConditions) {
            if (condition.getSpecialFunction() != null && condition.getSpecialFunction().getValue() != null
                    && !condition.getSpecialFunction().getValue().equals("")) {
                String fun = condition.getSpecialFunction().getValue();
                conditions.add("EXTRACT(" + fun + " FROM entity." + condition.getFieldExpression() + ") "
                        + condition.getOperator().getValue()
                        + " EXTRACT(" + fun + " FROM " + condition.getConditionValue() + ")");
                continue;
            }
            conditions.add(condition.getFieldExpression()
                    + condition.getOperator().getValue()
                    + condition.getConditionValue());
        }
        conditions.add(OEntityManager.CONFCOND_CASE_INSENSITIVE);
        try {
            alertEntities = oem.loadEntityList(alertOEntity.getEntityClassName(),
                    conditions, null, null, loggedUser);
        } catch (Exception e) {
            logger.error("Exception while executing Query", e);
        }
        logger.debug("Returning");
        return alertEntities;
    }

    private List<String> getReceivers(BaseEntity alertEntity,
            List<Reciever> receivers, String type) {
        if (null == receivers || receivers.isEmpty()) {
            return new ArrayList<String>();
        }
        List<String> receiversDTO = new ArrayList<String>();
        for (Reciever receiver : receivers) {
            if (!receiver.getRecieverType().getValue().equalsIgnoreCase(type)) {
                continue;
            }
            String email = receiver.getEmail();
            if (null != email
                    && !"".equalsIgnoreCase(email)) {
                receiversDTO.add(email);
            } else {
                String emailExpression = receiver.getEmailExpression();
                if (null != emailExpression
                        && !"".equalsIgnoreCase(emailExpression)) {
                    try {
                        String emailValue = (String) BaseEntity.getValueFromEntity(alertEntity,
                                emailExpression);
                        if (null == emailValue || "".equals(emailValue)) {
                            logger.error("There no email in Entity {} for Expression {}",
                                    alertEntity, emailExpression);
                            continue;
                        }
                        receiversDTO.add(emailValue);
                    } catch (Exception e) {
                        logger.error("Error retrieving Expression {} in Entity",
                                emailExpression, alertEntity, e);
                    }

                } else {
                    logger.error("Receiver has no email or email expression");
                }
            }
        }
        return receiversDTO;
    }


    private List<Object> parseSQL(String parameterizedSQL,long dbid, OUser loggedUser) {
        logger.debug("Entering");
        String sql="";
        List<Object> entities = new ArrayList<Object>();
        try {
            sql = parameterizedSQL.replaceAll("(?i)%dbid%", String.valueOf(dbid));
            entities = oem.executeEntityListNativeQuery(sql, loggedUser);
        } catch (Exception ex) {
            logger.error("Alert attachement SQL:{}", sql);
            logger.error("Exception while executing Query", ex);
        }
        logger.debug("Returning");
        return entities;
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private String[] getSqlColumnsNames(String sql, OUser loggedUser) {
        String[] sqlCols = null;
        Connection connect = null;
        ResultSet resultSet = null;
        try {
            DataSource dataSource = ((DataSource) new InitialContext().lookup("jdbc/"
                    + loggedUser.getTenant().getPersistenceUnitName()));
            connect = dataSource.getConnection();
            Statement statement = connect.createStatement();
            resultSet = statement.executeQuery(sql);
            sqlCols = new String[resultSet.getMetaData().getColumnCount()];
            for(int i=0 ; i<resultSet.getMetaData().getColumnCount() ; i++){
                sqlCols[i] = resultSet.getMetaData().getColumnLabel(i+1);
                logger.debug(sqlCols[i]);
            }            
        } catch (Exception ex) {
            logger.error("extractAttachColNames", ex);
        } finally {
            try {
                resultSet.close();
            } catch (Exception ex) {
                 logger.info(ex.getMessage());
            }
            try {
                connect.close();
            } catch (Exception ex) {
                 logger.info(ex.getMessage());
            }
        }
        return sqlCols;
    }

    private AttachmentDTO createExcelAttachment(List<Object> attachementObjects, String[] excelColumns) {
        String excelFileName = new Date().toGMTString() + (int)(Math.random() * 1000) + "alertAttach.xls";//name of excel file
        excelFileName = excelFileName.replaceAll(":", "_");
        excelFileName = excelFileName.replaceAll("/", "_");

        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("Sheet1");
        //iterating r number of rows
        //String className = attachementObjects.get(0).getClass().getSimpleName();
        HSSFRow namesRow = sheet.createRow(0);
        for (int fr = 0; fr < excelColumns.length; fr++) {
            HSSFCell cell = namesRow.createCell(fr);
            cell.setCellValue(excelColumns[fr]);
        }
        for (int r = 1; r <= attachementObjects.size(); r++) {
            HSSFRow row = sheet.createRow(r);
            if (attachementObjects.get(r - 1) instanceof Object[]) {
                Object[] rowObj = (Object[]) attachementObjects.get(r - 1);

                //iterating c number of columns
                for (int c = 0; c < excelColumns.length; c++) {
                    HSSFCell cell = row.createCell(c);
                    if (null != rowObj[c]) {
                        cell.setCellValue(rowObj[c].toString());
                    }
                }
            } else if (attachementObjects.get(r - 1) instanceof Object) {
                Object rowObj = (Object) attachementObjects.get(r - 1);
                HSSFCell cell = row.createCell(0);
                cell.setCellValue(rowObj.toString());
            }
        }

        ByteArrayOutputStream byteOut;
            byteOut = new ByteArrayOutputStream();
        try {
            //write this workbook to an Outputstream.
            wb.write(byteOut);
            return new AttachmentDTO(excelFileName,byteOut);
        } catch (IOException ex) {
            logger.error("outputstream Exception", ex);        
        }
            
            
         
        return null;
    }


    private AttachmentDTO generateCustomReport(Object parameterObjects, String reportParameterSql,
            AttachmentDTO reportFile, CustomReport customReport, String reportPass, OUser loggedUser) {

        HashMap<String, Object> parametersMap = new HashMap<String, Object>();
        if (null != parameterObjects && null != reportParameterSql && !"".equals(reportParameterSql)) {
            String[] paramNames = getSqlColumnsNames(reportParameterSql , loggedUser);
            Object[] paramValues = extractParamValues(parameterObjects);
            String[] paramKeys = prepareParamKeys(paramNames, paramValues);
            if (null != paramValues && paramNames.length == paramValues.length) {
                for (int i = 0; i < paramValues.length; i++) {
                    parametersMap.put(paramKeys[i], paramValues[i]);
                }
            } else {//empty parameters
                for (int i = 0; i < paramValues.length; i++) {
                    parametersMap.put(paramKeys[i], paramValues[0]);
                }
            }
        }
        AbstractReportGenerator report =new PDFReportGenerator(new ReportDTO(reportFile, customReport.isLocalized(), customReport.isRevertLang(),customReport.isApplyingFilter(), customReport.isFitPage(), parametersMap, 
                null, customReport.getReportName(), customReport.getJavaFunctionUserExit(), loggedUser));
        report.setCustomReportService(customReportService);
        ByteArrayInputStream pdfStream = customReportService.generateReport(report);
        ByteArrayOutputStream out;
        String pdfPath = (new Date().toGMTString()
                    .replace(":", "_").replace("/", "_") + (int)(Math.random()*1000) + reportFile.getName().replace(".rptdesign", ".pdf")).replace(" ", "");
        try {
            out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int len = 0;
            while ((len = pdfStream.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            pdfStream.close();
            if (reportPass != null && !reportPass.equals("")) {
                //write pdf
                OutputStream outputStream = new FileOutputStream(pdfPath);
                out.writeTo(outputStream);
                //This is name and path of zip file to be created
                String zipPath = pdfPath.replace("pdf", "zip");
                ZipFile zipFile = new ZipFile(zipPath);
                //Add files to be archived into zip file
                File pdfFile = new File(pdfPath);
                ArrayList<File> filesToAdd = new ArrayList<File>();
                filesToAdd.add(pdfFile);
                //Initiate Zip Parameters which define various properties
                ZipParameters parameters = new ZipParameters();
                parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
                parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
                parameters.setEncryptFiles(true);
                parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
                parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
                //Set password
                parameters.setPassword(reportPass);
                //Now add files to the zip file
                zipFile.addFiles(filesToAdd, parameters);
                pdfFile.delete();

                //read zip file to send it
                File file = new File(zipPath);
                byte[] bytesArray = new byte[(int) file.length()];
                FileInputStream fis = new FileInputStream(file);
                fis.read(bytesArray); //read file into bytes[]
                fis.close();
                ByteArrayInputStream fileInputStream = new ByteArrayInputStream(bytesArray);
                ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
                int ziplen = 0;
                while ((ziplen = fileInputStream.read(bytesArray)) > 0) {
                    byteOut.write(bytesArray, 0, ziplen);
                }
                fileInputStream.close();
                file.delete();
                return new AttachmentDTO(zipPath, byteOut);
            } else {
                return new AttachmentDTO(pdfPath, out);
            }
        } catch (Exception ex) {
            logger.error("outputstream exception", ex);
        }
        return null;
    }

    private Object[] extractParamValues(Object parameterObject) {
        Object[] paramValues = null;

        if (parameterObject instanceof Object[]) {
            paramValues = new Object[((Object[]) parameterObject).length];
            for (int i = 0; i < ((Object[]) parameterObject).length; i++) {
                paramValues[i] = ((Object[]) parameterObject)[i];//.toString();
            }
        } else if (parameterObject instanceof Object) {
            paramValues = new Object[1];
            paramValues[0] = parameterObject;//.toString();
        }
        return paramValues;
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private String[] extractparamNames(String reportParameterSql , OUser loggedUser) {
        String[] paramNames = null;
        Connection connect = null;
        ResultSet resultSet = null;
        try {
            DataSource dataSource = ((DataSource) new InitialContext().lookup("jdbc/"
                    + loggedUser.getTenant().getPersistenceUnitName()));
            connect = dataSource.getConnection();
            Statement statement = connect.createStatement();
            resultSet = statement.executeQuery(reportParameterSql);
            paramNames = new String[resultSet.getMetaData().getColumnCount()];
            for(int i=0 ; i<resultSet.getMetaData().getColumnCount() ; i++){
                paramNames[i] = resultSet.getMetaData().getColumnLabel(i+1);
                logger.debug(paramNames[i]);
            }            
        } catch (Exception ex) {
            logger.error("extractparamNames", ex);
        } 
       finally {

            try {
                resultSet.close();
            } catch (Exception ex) {
                logger.info(ex.getMessage());
            }
            try {
                connect.close();
            } catch (Exception ex) {
                logger.info(ex.getMessage());
            }
        }
        return paramNames;
    }

    private String[] prepareParamKeys(String[] paramNames, Object[] paramValues) {
        if(null == paramValues){
            return null;
        }
        String[] paramKeys = new String[paramValues.length];
        String type = "";
        for (int i = 0; i < paramValues.length; i++) {
            if (paramValues[i] instanceof Long || paramValues[i] instanceof Double) {
                type = "decimal";
            } else if (paramValues[i] instanceof Integer) {
                type = "integer";
            } else if(paramValues[i] instanceof Date ){
                type = "date";
            } else if(paramValues[i] instanceof Boolean){
                type = "boolean";
            } else {
                type = "String";
            }
            if (paramNames.length != 0) {
                paramKeys[i] = paramNames[i] + "#_#" + type;
            }
        }
        return paramKeys;
    }

    @Override
    public OFunctionResult deleteAlertFunctionAndScheduleExpression(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            BaseAlert alert = (BaseAlert) odm.getData().get(0);
            entitySetupService.callEntityDeleteAction(alert.getJavaFunction().getRunningScheduleExpression(), loggedUser);
            entitySetupService.callEntityDeleteAction(alert.getJavaFunction(), loggedUser);
            
        }catch(Exception ex){
            logger.error("Exception while alert function", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        logger.debug("Returning");
        return oFR;
    }
}
