/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitysetup;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.udc.UDC;

/**
 *
 * @author aelzaher
 */
@ChildEntity(fields="oJavaEntityFields")
@Entity
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class OJavaEntity extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="name">
    @Column(nullable = false, unique = true)
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getNameDD() {
        return "OJavaEntity_name";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="omodule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OModule omodule;

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }

    public OModule getOmodule() {
        return omodule;
    }

    public String getOmoduleDD() {
        return "OJavaEntity_omodule";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="packageName">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC packageName;

    public void setPackageName(UDC packageName) {
        this.packageName = packageName;
    }

    public UDC getPackageName() {
        return packageName;
    }

    public String getPackageNameDD() {
        return "OJavaEntity_packageName";
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="superOEntityClass">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OEntity superOEntityClass;

    public void setSuperOEntityClass(OEntity superOEntityClass) {
        this.superOEntityClass = superOEntityClass;
    }
    
    public OEntity getSuperOEntityClass() {
        return superOEntityClass;
    }
    
    public String getSuperOEntityClassDD() {
        return "OJavaEntity_superOEntityClass";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="superOJavaEntityClass">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OJavaEntity superOJavaEntityClass;

    public void setSuperOJavaEntityClass(OJavaEntity superOJavaEntityClass) {
        this.superOJavaEntityClass = superOJavaEntityClass;
    }

    public OJavaEntity getSuperOJavaEntityClass() {
        return superOJavaEntityClass;
    }

    public String getSuperOJavaEntityClassDD() {
        return "OJavaEntity_superOJavaEntityClass";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="abstractClass">
    @Column(nullable = false)
    private boolean abstractClass;

    public void setAbstractClass(boolean abstractClass) {
        this.abstractClass = abstractClass;
    }

    public boolean isAbstractClass() {
        return abstractClass;
    }

    public String getAbstractClassDD() {
        return "OJavaEntity_abstractClass";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="entityType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC entityType;

    public void setEntityType(UDC entityType) {
        this.entityType = entityType;
    }

    public UDC getEntityType() {
        return entityType;
    }

    public String getEntityTypeDD() {
        return "OJavaEntity_entityType";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="parentOJavaEntityClass">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OJavaEntity parentOJavaEntityClass;

    public void setParentOJavaEntityClass(OJavaEntity parentOJavaEntityClass) {
        this.parentOJavaEntityClass = parentOJavaEntityClass;
    }

    public OJavaEntity getParentOJavaEntityClass() {
        return parentOJavaEntityClass;
    }

    public String getParentOJavaEntityClassDD() {
        return "OJavaEntity_parentOJavaEntityClass";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="inheritedType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC inheritedType;

    public void setInheritedType(UDC inheritedType) {
        this.inheritedType = inheritedType;
    }

    public UDC getInheritedType() {
        return inheritedType;
    }

    public String getInheritedTypeDD() {
        return "OJavaEntity_inheritedType";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="status">
    /**
     * Status values are:
     * "new" - for entities which needs fields to be added
     * "changed" - for entities which do not need to add or needs to remove fields so
                   removing is in preUpdate & adding is in postUpdate
     * "built" - for done & built ones
     */
    @Column(nullable = false)
    private String status;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="parentField">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private OJavaEntityField parentField;

    public void setParentField(OJavaEntityField parentField) {
        this.parentField = parentField;
    }

    public OJavaEntityField getParentField() {
        return parentField;
    }

    public String getParentFieldDD() {
        return "OJavaEntity_parentField";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="unityRegardChild">
    @Column(nullable = false)
    private boolean unityRegardChild;

    public void setUnityRegardChild(boolean unityRegardChild) {
        this.unityRegardChild = unityRegardChild;
    }

    public boolean isUnityRegardChild() {
        return unityRegardChild;
    }

    public String getUnityRegardChildDD() {
        return "OJavaEntity_unityRegardChild";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="entityCode">
    @Column
    private String entityCode;

    public void setEntityCode(String entityCode) {
        this.entityCode = entityCode;
    }

    public String getEntityCode() {
        return entityCode;
    }

    public String getEntityCodeDD() {
        return "OJavaEntity_entityCode";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="oJavaEntityFields">
    /**
     * Java Entity Fields
     */
    @OneToMany(mappedBy = "oJavaEntity")
    private List<OJavaEntityField> oJavaEntityFields;

    public void setOJavaEntityFields(List<OJavaEntityField> oJavaEntityFields) {
        this.oJavaEntityFields = oJavaEntityFields;
    }

    public List<OJavaEntityField> getOJavaEntityFields() {
        return oJavaEntityFields;
    }

    public String getOJavaEntityFieldsDD() {
        return "OJavaEntity_oJavaEntityFields";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="createSQLCommand">
    @Column
    private String createSQLCommand;

    public void setCreateSQLCommand(String createSQLCommand) {
        this.createSQLCommand = createSQLCommand;
    }

    public String getCreateSQLCommand() {
        return createSQLCommand;
    }

    public String getCreateSQLCommandDD() {
        return "OJavaEntity_createSQLCommand";
    }
    // </editor-fold>

}
