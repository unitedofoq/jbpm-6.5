/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.orgchart;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author mibrahim
 */
@Entity
@ReadOnly
@NamedQueries(value={
    @NamedQuery(name="getOrgChartFunction",
        query = " SELECT    orgChartFunctionLite " +
                " FROM      OrgChartFunctionLite orgChartFunctionLite" +
                " WHERE     orgChartFunctionLite.ofunCode = :code")})
public class OrgChartFunctionLite implements Serializable {
    @Id
    private String ofunCode;
    private String viewPageCode;
    private int portletHeight;

    /**
     * @return the viewPageCode
     */
    public String getViewPageCode() {
        return viewPageCode;
    }

    /**
     * @param viewPageCode the viewPageCode to set
     */
    public void setViewPageCode(String viewPageCode) {
        this.viewPageCode = viewPageCode;
    }

    /**
     * @return the ofunCode
     */
    public String getOfunCode() {
        return ofunCode;
    }

    /**
     * @param ofunCode the ofunCode to set
     */
    public void setOfunCode(String ofunCode) {
        this.ofunCode = ofunCode;
    }

    /**
     * @return the portletHeight
     */
    public int getPortletHeight() {
        return portletHeight;
    }

    /**
     * @param portletHeight the portletHeight to set
     */
    public void setPortletHeight(int portletHeight) {
        this.portletHeight = portletHeight;
    }
}
