/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.screen;

import com.unitedofoq.fabs.core.datatype.ODataType;

/**
 *
 * @author mmohamed
 */
public class InputNotSupportedException extends Exception{

    public InputNotSupportedException(String message)
    {
        this.message = message;
    }
    
    // <editor-fold defaultstate="collapsed" desc="message">
    private String message;
    @Override
    public String getMessage() {
        return message;
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="oScreen">
    private OScreen oScreen ;

    public OScreen getOScreen() {
        return oScreen;
    }

    public void setOScreen(OScreen oScreen) {
        this.oScreen = oScreen;
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="oDataType">
    private ODataType oDataType ;

    public ODataType getODataType() {
        return oDataType;
    }

    public void setODataType(ODataType oDataType) {
        this.oDataType = oDataType;
    }
    // </editor-fold >

    @Override
    public String toString() {
        return
            (message!=null? "Message: " + message : "")
           +(oDataType!=null? ", Data Type: " + oDataType.toString() : "")
           +(oScreen!=null? ", Screen: " + oScreen.toString() : "")
           ;
    }

}
