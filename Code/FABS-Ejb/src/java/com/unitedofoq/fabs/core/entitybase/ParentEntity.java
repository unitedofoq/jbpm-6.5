/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.entitybase;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;

/**
 * Used to annotate entity to list the entity field(s) mapped to the
 * entity parent(s).
 * <br> It's used for Composition Parent-Child Relationship
 * <br> To get entity children, refer to {@link GetEntityChildrenTree}
 * <br> See Also: {@see ChildEntity}
 *
 * @author arezk
 */

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ParentEntity {
    /**
     * List of the names of fields in the entity that represent parents.
     * e.g. in {@link ScreenField} you should find {@link OScreen} field as parent.
     * Second parent is added if the entity represents an association class
     */
    String[] fields();
}
