package com.unitedofoq.fabs.core.udc;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;

@Entity
@DiscriminatorValue(value = "UDC")
@Table(uniqueConstraints=
    @UniqueConstraint(columnNames={"type_dbid", "value"})
    // Note: don't create it in table, as type_dbid is nullable in UDC Table
    // & we don't need to create it in the table so far
    )
@ParentEntity(fields={"type"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class UDC extends UDCBase
{
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false, name="type_dbid")
    private UDCType type = null;
    public UDCType getType() {
        return type;
    }
    public void setType(UDCType type) {
        this.type = type;
    }
    public String getTypeDD() {
        return "UDC_type";
    }

    @Override
    public String toString() {
        return "UDC[dbid=" + dbid + ", value=" + value + "]";
    }
}
