/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.content;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import org.slf4j.LoggerFactory;
import org.apache.commons.codec.binary.Base64;
import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;
// ...
//String escaped = escapeHtml4(source);
/**
 *
 * @author lap
 */
public class AttachmentDTO implements Serializable{

    final static org.slf4j.Logger logger = LoggerFactory.getLogger(AttachmentDTO.class);
    // <editor-fold defaultstate="collapsed" desc="id">
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="attachment">
    private byte[] attachment;

    public byte[] getAttachment() {
        return attachment;
    }

    public void setAttachment(byte[] attachment) {
        this.attachment = attachment;
    }
    // </editor-fold>  
    // <editor-fold defaultstate="collapsed" desc="attachment mimeType">
    private String mimeType;

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
    // </editor-fold> 
    // <editor-fold defaultstate="collapsed" desc="attachmentName">
    private String name;

    public String getName() {
        return name;
    }
    public String getShortName(){
        return name.substring(0, Math.min(7, name.length()-1))+((name.length()>7)?"...":"");
    }
    
    public String getRawString(){
        return escapeHtml4(new String(this.getAttachment())).replaceAll("\n", "<p></p>");
    }

    public void setName(String name) {
        this.name = name;
    }
    // </editor-fold> 
    
   
    private String attachmentUser;

    public String getAttachmentUser() {
        return attachmentUser;
    }

    public void setAttachmentUser(String attachmentUser) {
        this.attachmentUser = attachmentUser;
    }
    
    private String attachmentDateTime;

    public String getAttachmentDateTime() {
        return attachmentDateTime;
    }

    public void setAttachmentDateTime(String attachmentDateTime) {
        this.attachmentDateTime = attachmentDateTime;
    }
    private String attachementEncoded;
    
    public byte[] getAttachementBytes() {
        return Base64.decodeBase64(attachementEncoded);
    }
    
    public String getAttachementDataEncoded(){
        return attachementEncoded;
    }
    
    public AttachmentDTO(){
        
    }
    
    public AttachmentDTO(String name , String bytesEncoded){
        this.name = name;
        this.attachementEncoded = bytesEncoded;
        
    }
    public AttachmentDTO(String name , byte[] bytesEncoded){
        this(name,Base64.encodeBase64String(bytesEncoded));
    }
    
    public AttachmentDTO(String name , ByteArrayOutputStream stream){
        this(name,stream.toByteArray());
        try {
            stream.close();
        } catch (IOException ex) {
            logger.debug("stream of attachment closed un expectedly", ex);
        }
    }
    
}
