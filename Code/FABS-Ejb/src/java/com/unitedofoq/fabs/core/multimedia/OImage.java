/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.multimedia;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.module.OModule;

/**
 *
 * @author arezk
 */
@Entity
@VersionControlSpecs(omoduleFieldExpression="omodule")
@FABSEntitySpecs(indicatorFieldExp="name")
public class OImage extends BaseEntity{

    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OModule omodule;

    public String getOmoduleDD(){
        return "OImage_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
    // </editor-fold>

    private String imageType;

    public String getImageTypeDD() {
        return "OImage_imageType";
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    @Column(unique=true)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameDD(){
        return "OImage_name";
    }

    @Column(columnDefinition = "BLOB(10485760)") // 10MB size
    private byte[] image;

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getImageDD(){
        return "OImage_image";
    }
}
