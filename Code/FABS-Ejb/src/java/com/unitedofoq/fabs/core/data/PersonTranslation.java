
package com.unitedofoq.fabs.core.data;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class PersonTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="firstName">
    @Column
    private String firstName;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="middleName">
    @Column
    private String middleName;

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getMiddleName() {
        return middleName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="lastName">
    @Column
    private String lastName;

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="address">
    @Column
    private String address;

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="address1">
    @Column
    private String address1;

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress1() {
        return address1;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="additionalInformation">
    @Column
    private String additionalInformation;

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }
    // </editor-fold>

}
