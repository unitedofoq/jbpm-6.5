/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.delegation;

import com.unitedofoq.fabs.bpm.model.Task;
import javax.ejb.Local;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.bpm.common.model.DelegationData;
import java.util.List;
import java.util.Map;

/**
 *
 * @author aelzaher
 */
@Local
public interface DelegationServiceRemote {
    public OFunctionResult validateTaskDelegation (ODataMessage odm, OFunctionParms params, OUser loggedUser);
    public String getDelegate(String loginName, OUser loggedUser);
    public List<TaskDelegation> getDelegationsToUser(Long groupID, OUser loggedUser);
    public List<TaskDelegation> getDelegationsFromUser(Long groupID, OUser loggedUser);
    public List<DelegationData> getDelegationDataList(List<TaskDelegation> delegations, OUser loggedUser);
}
