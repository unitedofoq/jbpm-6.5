/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.routing;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author lap3
 */
@Entity
@ParentEntity(fields={"oroutingInst"})
public class ORoutingTaskInstance extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="owner">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OUser owner;

    /**
     * @return the owner
     */
    public OUser getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(OUser owner) {
        this.owner = owner;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="routingOEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OEntity routingOEntity;

    /**
     * @return the routingOEntity
     */
    public OEntity getRoutingOEntity() {
        return routingOEntity;
    }

    /**
     * @param routingOEntity the routingOEntity to set
     */
    public void setRoutingOEntity(OEntity routingOEntity) {
        this.routingOEntity = routingOEntity;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="previousTask">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private ORoutingTaskInstance previousTask;

    public String getPreviousTaskDD() {
        return "ORoutingTaskInstance_previousTask";
    }
    
    public ORoutingTaskInstance getPreviousTask() {
        return previousTask;
    }

    public void setPreviousTask(ORoutingTaskInstance previousTask) {
        this.previousTask = previousTask;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="actionTaken">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private ORoutingTaskAction actionTaken;

    /**
     * @return the actionTaken
     */
    public ORoutingTaskAction getActionTaken() {
        return actionTaken;
    }

    /**
     * @param actionTaken the actionTaken to set
     */
    public void setActionTaken(ORoutingTaskAction actionTaken) {
        this.actionTaken = actionTaken;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="routingEntityDBID">
    private long routingEntityDBID;

    /**
     * @return the routingEntityDBID
     */
    public long getRoutingEntityDBID() {
        return routingEntityDBID;
    }

    /**
     * @param routingEntityDBID the routingEntityDBID to set
     */
    public void setRoutingEntityDBID(long routingEntityDBID) {
        this.routingEntityDBID = routingEntityDBID;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="comment">
    private String comments;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
    
   
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="assignedTime">
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date assignedTime;
    
    /**
     * @return the assignedTime
     */
    public Date getAssignedTime() {
        return assignedTime;
    }

    /**
     * @param assignedTime the assignedTime to set
     */
    public void setAssignedTime(Date assignedTime) {
        this.assignedTime = assignedTime;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="finished">
    private boolean finished;

    public String getFinishedDD() {
        return "ORoutingTaskInstance_finished";
    }
    
    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
    
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="finishTime">
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date finishTime;
    
    /**
     * @return the finishTime
     */
    public Date getFinishTime() {
        return finishTime;
    }

    /**
     * @param finishTime the finishTime to set
     */
    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="oroutingTask">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ORoutingTask oroutingTask;
    
    /**
     * @return the oroutingTask
     */
    public ORoutingTask getOroutingTask() {
        return oroutingTask;
    }

    /**
     * @param oroutingTask the oroutingTask to set
     */
    public void setOroutingTask(ORoutingTask oroutingTask) {
        this.oroutingTask = oroutingTask;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="oroutingInst">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)   
    private ORoutingInstance oroutingInst;
    
    /**
     * @return the oroutingInst
     */
    public ORoutingInstance getOroutingInst() {
        return oroutingInst;
    }

    /**
     * @param oroutingInst the oroutingInst to set
     */
    public void setOroutingInst(ORoutingInstance oroutingInst) {
        this.oroutingInst = oroutingInst;
    }
    // </editor-fold>
}
