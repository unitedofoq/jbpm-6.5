/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.ExceptionHandler.thrower.main;

import com.unitedofoq.fabs.core.Exception.BasicException;
import com.unitedofoq.fabs.core.ExceptionHandler.thrower.factory.ExceptionFactory;
import com.unitedofoq.fabs.core.ExceptionHandler.thrower.factory.ExceptionType;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessage;

/**
 *
 * @author mostafa
 */
public class ExecptionThrower {
    public static void throwFABSException(UserMessage userMessage,
            ExceptionType exceptionType, OUser loggedUser) throws BasicException{
        ExceptionFactory.throwException(userMessage, exceptionType, loggedUser);
    }
}
