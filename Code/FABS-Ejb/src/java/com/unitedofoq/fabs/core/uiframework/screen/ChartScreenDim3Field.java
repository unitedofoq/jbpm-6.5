/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;

/**
 *
 * @author bassem
 */
@Entity
@ParentEntity(fields = {"chartScreen"})
public class ChartScreenDim3Field extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="dim3FieldExpression">
    private String dim3FieldExpression;

    public String getDim3FieldExpressionDD() {
        return "ChartScreenDim3Field_dim3FieldExpression";
    }
    public String getDim3FieldExpression() {
        return dim3FieldExpression;
    }

    public void setDim3FieldExpression(String dim3FieldExpression) {
        this.dim3FieldExpression = dim3FieldExpression;
    }
    
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="chartScreen">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ChartScreen chartScreen;

    public String getChartScreenDD() {
        return "ChartScreenDim3Field_chartScreen";
    }
    
    public ChartScreen getChartScreen() {
        return chartScreen;
    }

    public void setChartScreen(ChartScreen chartScreen) {
        this.chartScreen = chartScreen;
    }
    // </editor-fold>
    
}
