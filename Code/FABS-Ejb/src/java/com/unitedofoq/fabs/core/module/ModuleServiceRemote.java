/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.module;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author aelzaher
 */
@Local
public interface ModuleServiceRemote {

    public OFunctionResult clearOModuleCache(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OModule loadOModule(String name, OUser loggedUser);

    public OFunctionResult preOModuleCachedUpdate(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult onOModuleCachedUpdate(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult onOModuleCachedRemove(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

}
