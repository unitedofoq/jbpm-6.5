/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.screen.wizard;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.module.OModule;

/**
 *
 * @author mmohamed
 */
@Entity
@ChildEntity(fields={"wizardSteps", "wizardFunctions"})
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class Wizard extends BaseEntity{

    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OModule omodule;

    public String getOmoduleDD(){
        return "Wizard_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
// </editor-fold>

    @Column(nullable=false, unique=true)
    private String name;
    
    @Translatable(translationField="wizardTitleTranslated")
    @Column(nullable=false)
    private String wizardTitle;

    public String getWizardTitle() {
        return wizardTitle;
    }
    
    public String getWizardTitleDD() {
        return "Wizard_wizardTitle";
    }

    public void setWizardTitle(String wizardTitle) {
        this.wizardTitle = wizardTitle;
    }
    
    @Translation(originalField="wizardTitle")
    @Transient
    private String wizardTitleTranslated;

    public String getWizardTitleTranslated() {
        return wizardTitleTranslated;
    }
    
    public String getWizardTitleTranslatedDD() {
        return "Wizard_wizardTitle";
    }

    public void setWizardTitleTranslated(String wizardTitleTranslated) {
        this.wizardTitleTranslated = wizardTitleTranslated;
    }
    
    @OneToMany(mappedBy="wizard")
    private List<WizardStep> wizardSteps;

    public String getName() {
        return name;
    }

    public String getNameDD() {
        return "Wizard_name";
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<WizardStep> getWizardSteps() {
        return wizardSteps;
    }

    public void setWizardSteps(List<WizardStep> wizardSteps) {
        this.wizardSteps = wizardSteps;
    }
    
    // <editor-fold defaultstate="collapsed" desc="wizardFunctions">
    @OneToMany(mappedBy="wizard", fetch= FetchType.LAZY)
    private List<WizardFunction> wizardFunctions;

    public List<WizardFunction> getWizardFunctions() {
        return wizardFunctions;
    }

    public void setWizardFunctions(List<WizardFunction> wizardFunctions) {
        this.wizardFunctions = wizardFunctions;
    }

    public String getWizardFunctionsDD(){
        return "Wizard_wizardFunctions";
    }
    // </editor-fold >

}
