/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.timezone;

import java.util.Date;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author lap3
 */
@Local
public interface TimeZoneServiceLocal {
    public Date getUserCDT(OUser loggedUser);
    public String getSystemCDTAsString();
    public Date getSystemCDT();
    public OFunctionResult testTimeZOne(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser);
}
