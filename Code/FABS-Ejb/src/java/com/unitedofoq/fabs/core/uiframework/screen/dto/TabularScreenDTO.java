/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen.dto;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author root
 */
public class TabularScreenDTO extends SingleEntityScreenDTO {

    private boolean sheet;
    private boolean fastTrack;
    private boolean sideBalloon;
    private String sortExpression;
    private int recordsInPage;
    private boolean basic;

    public boolean isSheet() {
        return sheet;
    }

    public void setSheet(boolean sheet) {
        this.sheet = sheet;
    }

    public boolean isFastTrack() {
        return fastTrack;
    }

    public void setFastTrack(boolean fastTrack) {
        this.fastTrack = fastTrack;
    }

    public boolean isSideBalloon() {
        return sideBalloon;
    }

    public void setSideBalloon(boolean sideBalloon) {
        this.sideBalloon = sideBalloon;
    }

    public String getSortExpression() {
        return sortExpression;
    }

    public void setSortExpression(String sortExpression) {
        this.sortExpression = sortExpression;
    }

    public int getRecordsInPage() {
        return recordsInPage;
    }

    public void setRecordsInPage(int recordsInPage) {
        this.recordsInPage = recordsInPage;
    }

    public boolean isBasic() {
        return basic;
    }

    public void setBasic(boolean basic) {
        this.basic = basic;
    }

    public TabularScreenDTO(boolean sheet, boolean fastTrack, boolean sideBalloon,
            String sortExpression, int recordsInPage, boolean basic, boolean editable,
            boolean removable, String oactOnEntityClassPath, boolean saveAndExit,
            boolean save, long dbid, String inlineHelp, String name, String header,
            String viewPage, boolean showHeader, boolean showActiveOnly, boolean prsnlzd,
            long prsnlzdOriginal_DBID, long prsnlzdUser_DBID, long screenFilter_DBID) {
        super(editable, removable, oactOnEntityClassPath, saveAndExit, save, dbid,
                inlineHelp, name, header, viewPage, showHeader, showActiveOnly, prsnlzd,
                prsnlzdOriginal_DBID, prsnlzdUser_DBID, screenFilter_DBID);
        this.sheet = sheet;
        this.fastTrack = fastTrack;
        this.sideBalloon = sideBalloon;
        this.sortExpression = sortExpression;
        this.recordsInPage = recordsInPage;
        this.basic = basic;
    }

    private static final Map<Long, Map<String, List>> lookupCachedData = new HashMap();

    public static boolean isScreenInMultiSelectionMode(long mainScrInstId) {
        Map<String, List> mainScrLookupCache = lookupCachedData.get(mainScrInstId);
        for (Map.Entry<String, List> lookupScr : mainScrLookupCache.entrySet()) {
            String lookupScrKey = lookupScr.getKey();
            List lookupPickedElements = lookupScr.getValue();
            if (lookupPickedElements.size() > 1) {
                return true;
            }
        }
        return false;
    }

    public static void createLookupMainScr(long mainScrInstId, String lookupScreenName) {
        Map lookupData = lookupCachedData.get(mainScrInstId);
        if (mainScrInstId != 0) {
            if (lookupData == null) {
                lookupData = new HashMap<>();
            }
            if (!lookupData.containsKey(lookupScreenName)) {
                lookupData.put(lookupScreenName, new LinkedList());
            }
            lookupCachedData.put(mainScrInstId, lookupData);
        }
    }

    public static boolean destroyLookupMainScr(long instId) {
        if (lookupCachedData.containsKey(instId)) {
            lookupCachedData.remove(instId);
            return true;
        }
        return false;
    }

    public static List listLookupPickedElement(long mainScrInstId, String lookupScreenName) {
        Map<String, List> lookupData = lookupCachedData.get(mainScrInstId);
        if (lookupData != null) {
            return lookupData.get(lookupScreenName);
        }
        return new LinkedList();
    }

    public static void setLookupPickedElement(long mainScrInstId, String lookupScreenName, List pickedElements) {
        if (mainScrInstId != 0) {
            Map<String, List> lookupData = lookupCachedData.get(mainScrInstId);
            if (lookupData == null) {
                createLookupMainScr(mainScrInstId, lookupScreenName);
                lookupData = lookupCachedData.get(mainScrInstId);
            }
            lookupData.put(lookupScreenName, pickedElements);
            lookupCachedData.put(mainScrInstId, lookupData);
        }
    }

    public static BaseEntity[] markLookupPickedElement(long mainScrInstId, String lookupScreenName, List<BaseEntity> entities) {
        if (mainScrInstId != 0) {
            List lookupDataList = listLookupPickedElement(mainScrInstId, lookupScreenName);
            if (lookupDataList != null && !lookupDataList.isEmpty()
                    && entities != null && !entities.isEmpty()) {
                BaseEntity[] selectedEntities = new BaseEntity[lookupDataList.size()];
                int index = 0;
                Set lookupDataSet = new HashSet(lookupDataList);
                for (BaseEntity entity : entities) {
                    if (lookupDataSet.contains(entity)) {
                        entity.setSelected(true);
                        selectedEntities[index++] = entity;
                    }
                }
                return selectedEntities;
            }
        }
        return new BaseEntity[0];
    }

}
