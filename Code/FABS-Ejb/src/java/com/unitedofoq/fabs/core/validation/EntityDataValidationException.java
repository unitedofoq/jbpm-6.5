/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.validation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.FABSDataValidation.FABSDataValidationType;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author arezk
 */
public class EntityDataValidationException extends FABSException{

    private String fieldName;
    private FABSDataValidationType dataValidationType;

    public EntityDataValidationException(FABSDataValidationType validationType, 
            String fieldName ,OUser loggedUser, String message, BaseEntity concernedEntity) {
        super(loggedUser, message, concernedEntity);
        this.fieldName = fieldName;
        this.dataValidationType = validationType;
    }

    public FABSDataValidationType getDataValidationType() {
        return dataValidationType;
    }

    public void setDataValidationType(FABSDataValidationType dataValidationType) {
        this.dataValidationType = dataValidationType;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
}
