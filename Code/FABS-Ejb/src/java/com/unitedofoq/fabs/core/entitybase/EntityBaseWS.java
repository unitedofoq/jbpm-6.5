/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitybase;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author mustafa
 */
@WebService(serviceName = "EntityBaseWS")
@Stateless()
public class EntityBaseWS {
    @EJB
    private EntityBaseServiceWSLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "setValueInEntity")
    public String setValueInEntity(@WebParam(name = "dbid") String dbid, @WebParam(name = "entityName") String entityName, @WebParam(name = "fieldExpression") String fieldExpression, @WebParam(name = "value") String value, @WebParam(name = "loginName") String loginName) {
        return ejbRef.setValueInEntity(dbid, entityName, fieldExpression, value, loginName);
    }
    
}
