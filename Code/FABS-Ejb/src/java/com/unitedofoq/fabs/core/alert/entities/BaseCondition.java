/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.alert.entities;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;

/**
 *
 * @author root
 */
@MappedSuperclass
public class BaseCondition extends BaseEntity {
    @ManyToOne
    private UDC operator;
    private String fieldExpression;
    private String name;
    @ManyToOne
    private UDC conditionOperator;
    @ManyToOne
    private UDC specialFunction;
    private String conditionValue;
    private int conditionIndex;

    public String getConditionIndexDD() {
        return "BaseCondition_conditionIndex";
    }
    
    public int getConditionIndex() {
        return conditionIndex;
    }

    public void setConditionIndex(int conditionIndex) {
        this.conditionIndex = conditionIndex;
    }
    
    public String getOperatorDD() {
        return "BaseCondition_operator";
    }
    
    public UDC getOperator() {
        return operator;
    }

    public void setOperator(UDC operator) {
        this.operator = operator;
    }

    public String getFieldExpressionDD() {
        return "BaseCondition_fieldExpression";
    }
    
    public String getFieldExpression() {
        return fieldExpression;
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }

    public String getNameDD() {
        return "BaseCondition_name";
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConditionOperatorDD() {
        return "BaseCondition_conditionOperator";
    }
    
    public UDC getConditionOperator() {
        return conditionOperator;
    }

    public void setConditionOperator(UDC conditionOperator) {
        this.conditionOperator = conditionOperator;
    }

    public String getConditionValueDD() {
        return "BaseCondition_conditionValue";
    }
    
    public String getConditionValue() {
        return conditionValue;
    }

    public void setConditionValue(String conditionValue) {
        this.conditionValue = conditionValue;
    }

    public UDC getSpecialFunction() {
        return specialFunction;
    }

    public void setSpecialFunction(UDC specialFunction) {
        this.specialFunction = specialFunction;
    }
    
    public String getSpecialFunctionDD() {
        return "BaseCondition_specialFunction";
    }
}
