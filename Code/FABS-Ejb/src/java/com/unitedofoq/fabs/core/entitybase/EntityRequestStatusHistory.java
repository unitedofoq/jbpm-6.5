/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitybase;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author nkhalil
 */
@Entity
public class EntityRequestStatusHistory extends ObjectBaseEntity {
    private String entityClassPath;
    public String getEntityClassPathDD(){
        return "OEntityRequestStatusHistory_entityClassPath";
    }
    private long entityDBID;
    public String getEntityDBIDDD(){
        return "OEntityRequestStatusHistory_entityDBID";
    }
    private String oldStatus;
    public String getOldStatusDD(){
        return "OEntityRequestStatusHistory_oldStatus";
    }
    private String newStatus;
    public String getNewStatusDD(){
        return "OEntityRequestStatusHistory_newStatus";
    }
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date actionTime;
    public String getActionTimeDD(){
        return "OEntityRequestStatusHistory_actionTime";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OUser requester;
    public String getRequesterDD(){
        return "OEntityRequestStatusHistory_requester";
    }

    public OUser getRequester() {
        return requester;
    }

    public void setRequester(OUser requester) {
        this.requester = requester;
    }
    

    public Date getActionTime() {
        return actionTime;
    }

    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }

    public String getEntityClassPath() {
        return entityClassPath;
    }

    public void setEntityClassPath(String entityClassPath) {
        this.entityClassPath = entityClassPath;
    }

    public long getEntityDBID() {
        return entityDBID;
    }

    public void setEntityDBID(long entityDBID) {
        this.entityDBID = entityDBID;
    }

    public String getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(String newStatus) {
        this.newStatus = newStatus;
    }

    public String getOldStatus() {
        return oldStatus;
    }

    public void setOldStatus(String oldStatus) {
        this.oldStatus = oldStatus;
    }
}
