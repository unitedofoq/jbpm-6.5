/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.alert.entities;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;

/**
 *
 * @author root
 */
@Entity
@ParentEntity(fields={"alert"})
public class AlertCondition extends BaseCondition {
    @ManyToOne
    private Alert alert;
    
    public String getAlertDD() {
        return "AlertCondition_alert";
    }

    public Alert getAlert() {
        return alert;
    }

    public void setAlert(Alert alert) {
        this.alert = alert;
    }
    
}
