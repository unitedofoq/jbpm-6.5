/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.webservice;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.filter.OFilter;

/**
 *
 * @author mmohamed
 */
@Entity
@DiscriminatorValue(value = "WebService")
@ParentEntity(fields = {"service"})
@ChildEntity(fields = {"filterFields"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class WebServiceFilter extends OFilter {
    
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    OWebService service;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    OEntity oactOnEntity;

    public OEntity getOactOnEntity() {
        return oactOnEntity;
    }

    public String getOactOnEntityDD() {
        return "WebServiceFilter_oactOnEntity";
    }

    public void setOactOnEntity(OEntity oactOnEntity) {
        this.oactOnEntity = oactOnEntity;
    }

    public OWebService getService() {
        return service;
    }

    public String getServiceDD() {
        return "WebServiceFilter_service";
    }

    public void setService(OWebService service) {
        this.service = service;
    }


}
