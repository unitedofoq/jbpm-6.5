/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.log.service;

/**
 *
 * @author mostafa
 */
import java.util.Properties;

public class LogSetupConfiguration {

    private StringBuilder LoggerName = null;
    private StringBuilder classFullName = null;
    private StringBuilder configurationFileName = null;

    public StringBuilder getLoggerName() {
        return LoggerName;
    }

    public void setLoggerName(StringBuilder LoggerName) {
        this.LoggerName = LoggerName;
    }

    public StringBuilder getClassFullName() {
        return classFullName;
    }

    public void setClassFullName(StringBuilder classFullName) {
        this.classFullName = classFullName;
    }

    public StringBuilder getConfigurationFileName() {
        return configurationFileName;
    }

    public void setConfigurationFileName(StringBuilder configurationFileName) {
        this.configurationFileName = configurationFileName;
    }

    public LogSetupConfiguration() {

        Properties prop = new Properties();
        try {
            //load a properties file
            //InputStream fis = this.getClass().getClassLoader().getResourceAsStream("log");
            //prop.load(fis);
            //ResourceBundle bundle=ResourceBundle.getBundle("log");
            
            //get the property value and print it out
            LoggerName =new StringBuilder("log4j");// new StringBuilder(prop.getProperty("LoggerName"));
            classFullName =new StringBuilder("com.unitedofoq.fabs.core.log.logger.Log4jLogger"); //new StringBuilder(prop.getProperty("ClassFullName"));
            configurationFileName =new StringBuilder("log4j.properties"); //new StringBuilder(prop.getProperty("configurationFile"));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}