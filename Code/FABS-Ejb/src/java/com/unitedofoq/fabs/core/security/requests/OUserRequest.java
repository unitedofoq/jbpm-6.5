package com.unitedofoq.fabs.core.security.requests;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import com.unitedofoq.fabs.core.udc.UDC;

@Entity
@ParentEntity(fields = "originalEntity")
public class OUserRequest extends RequestsBaseEntity {
    // </editor-fold>
    private boolean rtl;

    public String getRtlDD() {
        return "OUserRequest_rtl";
    }

    public boolean isRtl() {
        return rtl;
    }

    public void setRtl(boolean rtl) {
        this.rtl = rtl;
    }
    @Column( nullable = false)
    private String loginName;
    //@Transient
    private String password;
    @Column(nullable = false)
    private String email;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date birthDate;
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC firstLanguage = null;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC secondLanguage = null;

    public UDC getFirstLanguage() {
        return firstLanguage;
    }

    public void setFirstLanguage(UDC firstLanguage) {
        this.firstLanguage = firstLanguage;
    }

    public UDC getSecondLanguage() {
        return secondLanguage;
    }

    public void setSecondLanguage(UDC secondLanguage) {
        this.secondLanguage = secondLanguage;
    }

    /* DD NAMES GETTERS */
    public String getFirstLanguageDD() {
        return "OUserRequest_language";
    }

    public String getSecondLanguageDD() {
        return "OUserRequest_language";
    }

    public String getLoginNameDD() {
        return "OUserRequest_loginName";
    }

    public String getPasswordDD() {
        return "OUserRequest_password";
    }

    public String getReservedDD() {
        return "OUserRequest_reserved";
    }

    public String getEmailDD() {
        return "OUserRequest_email";
    }

    public String getBirthDateDD() {
        return "OUserRequest_birthDate";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
    
    // <editor-fold defaultstate="collapsed" desc="currentPass">
    @Transient
    private String currentPass;

    public String getCurrentPass() {
        return currentPass;
    }

    public void setCurrentPass(String currentPass) {
        this.currentPass = currentPass;
    }
    
    public String getCurrentPassDD() {
        return "OUserRequest_currentPass";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="newPass">
    @Transient
    private String newPass;    

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }
    
    public String getNewPassDD() {
        return "OUserRequest_newPass";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="confirmPass">
    @Transient
    private String confirmPass;

    public String getConfirmPass() {
        return confirmPass;
    }

    public void setConfirmPass(String confirmPass) {
        this.confirmPass = confirmPass;
    }
    
    public String getConfirmPassDD() {
        return "OUserRequest_confirmPass";
    }
    // </editor-fold>
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC timeZone = null;

    /**
     * @return the timeZone
     */
    public UDC getTimeZone() {
        return timeZone;
    }

    /**
     * @param timeZone the timeZone to set
     */
    public void setTimeZone(UDC timeZone) {
        this.timeZone = timeZone;
    }
      public String getTimeZoneDD() {
        return "OUserRequest_timeZone";
    }
    
    // <editor-fold defaultstate="collapsed" desc="trackMetaData">
    @Column
    private boolean trackMetaData;

    public boolean isTrackMetaData() {
        return trackMetaData;
    }

    public void setTrackMetaData(boolean trackMetaData) {
        this.trackMetaData = trackMetaData;
    }
    
    public String getTrackMetaDataDD() {
        return "OUserRequest_trackMetaData";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="trackBusinessData">
    @Column
    private boolean trackBusinessData;    

    public boolean isTrackBusinessData() {
        return trackBusinessData;
    }

    public void setTrackBusinessData(boolean trackBusinessData) {
        this.trackBusinessData = trackBusinessData;
    }
    
    public String getTrackBusinessDataDD() {
        return "OUserRequest_trackBusinessData";
    }
    // </editor-fold>


    // <editor-fold defaultstate="collapsed" desc="displayName">
    @Column
    private String displayName;

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getDisplayNameDD() {
        return "OUserRequest_displayName";
    }
    // </editor-fold>

    
@ManyToOne
private OUser originalEntity;

  public OUser getOriginalEntity() {
    return originalEntity;
  }
  
  public String getOriginalEntityDD() {
    return "OUserRequest_originalEntity";
  }

  public void setOriginalEntity(OUser originalEntity) {
    this.originalEntity = originalEntity;
  }
    
    @ManyToOne
    private OModule omodule;

    public String getOmoduleDD() {
        return "Person_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }    
    
    // <editor-fold defaultstate="collapsed" desc="Prefrence 1">
    private Long userPrefrence1;

    /**
     * @return the userPrefrence1
     */
    public Long getUserPrefrence1() {
        return userPrefrence1;
    }

    /**
     * @param userPrefrence1 the userPrefrence1 to set
     */
    public void setUserPrefrence1(Long userPrefrence1) {
        this.userPrefrence1 = userPrefrence1;
    }

    /**
     * @return the userPrefrence1DD
     */
    public String getUserPrefrence1DD() {
        return "OUserRequest_userPrefrence1";
    }
    // </editor-fold>
    
}
