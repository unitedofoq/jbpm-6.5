/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.report;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;

/**
 *
 * @author melsayed
 */
@Entity
@FABSEntitySpecs(indicatorFieldExp="oreport.name")
@VersionControlSpecs(omoduleFieldExpression="oreport.omodule")
public class OReportInput extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="inputAttributesMapping">
    @OneToMany()
    private List<InputAttributeMapping> inputAttributesMapping;
    /**
     * @return the inputAttributesMapping
     */
    public List<InputAttributeMapping> getInputAttributesMapping() {
        return inputAttributesMapping;
    }

    public String getInputAttributesMappingDD() {
        return "OReportInput_inputAttributesMapping";
    }

    /**
     * @param inputAttributesMapping the inputAttributesMapping to set
     */
    public void setInputAttributesMapping(List<InputAttributeMapping> inputAttributesMapping) {
        this.setInputAttributesMapping(inputAttributesMapping);
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="sortIndex">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ODataType odataType;

    public ODataType getOdataType() {
        return odataType;
    }

    public void setOdataType(ODataType odataType) {
        this.odataType = odataType;
    }
    

    public String getODataTypeDD() {
        return "OReportInput_odataType";
    }

   
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="oreport">
    @JoinColumn(nullable=false, name="OReport_DBID")
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OReport oreport;
    /**
     * @return the oreport
     */
    public OReport getOreport() {
        return oreport;
    }

    public String getOreportDD() {
        return "OReportInput_oreport";
    }

    /**
     * @param oreport the oreport to set
     */
    public void setOreport(OReport oreport) {
        this.oreport = oreport;
    }
    //</editor-fold>
   
}
