/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.comunication;

import com.unitedofoq.fabs.core.content.AttachmentDTO;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Properties;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Hammad
 */
public class EMailMessage {
    final static org.slf4j.Logger logger = LoggerFactory.getLogger(EMailMessage.class);

    private final static String FROM_KEY = "From";
    private final static String TOS_KEY = "To";
    private final static String CCS_KEY = "CC";
    private final static String SUBJECT_KEY = "Subject";
    private final static String BODY_KEY = "Body";
    private final static String ATTACHEMENT_KEY = "Attachments";
    private final static String ATTACHEMENT_NAME_KEY = "AttachmentsNames";
    private final static String ATTACHEMENT_VALUE_KEY = "AttachmentsValues";
    
    
    String from = "";
    List<String> tos;
    List<String> ccs;
    String subject = "";
    String body = "";
    List<AttachmentDTO> attachments;

    public List<AttachmentDTO> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<AttachmentDTO> attachments) {
        this.attachments = attachments;
    }
    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public List<String> getTos() {
        return tos;
    }

    public void setTos(List<String> tos) {
        this.tos = tos;
    }

    public List<String> getCcs() {
        return ccs;
    }

    public void setCcs(List<String> ccs) {
        this.ccs = ccs;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }



    @Override
    /**
     * We prefer to put the message on queue as String, for easier readability
     * and troubleshooting
     */
    public String toString() {
        logger.debug("Entering: ");

        Properties properties = new Properties();
        properties.setProperty(FROM_KEY, from);
        properties.setProperty(TOS_KEY, listToCSV(tos));
        properties.setProperty(CCS_KEY, listToCSV(ccs));
        properties.setProperty(SUBJECT_KEY, subject);
        properties.setProperty(BODY_KEY, body.replaceAll("\\r|\\n", ""));
        properties.setProperty(ATTACHEMENT_NAME_KEY, listToCSV(AttachmentHelper.getNamesFromAttachments(attachments)));
        properties.setProperty(ATTACHEMENT_VALUE_KEY, listToCSV(AttachmentHelper.getAttachmentDTOFromAttachmentListNameAndAttachmentListData(attachments)));
        StringWriter writer = new StringWriter();
        
        try {
            properties.store(writer, "eMailMessage");
        } catch (IOException ioe) {
            logger.error("Exception thrown while converting EMailMessage object to string", ioe);
            return null;
        }
        
        logger.debug("Returning ");
        return writer.getBuffer().toString();
    }
    
    public static EMailMessage fromString(String message) throws IOException {
        logger.debug("Entering: ");

        Properties properties = new Properties ();
        
        properties.load(new StringReader(message));
        
        EMailMessage eMailMessage = new EMailMessage();
        eMailMessage.setFrom(properties.getProperty(FROM_KEY));
        eMailMessage.setTos(CSVToList(properties.getProperty(TOS_KEY)));
        eMailMessage.setCcs(CSVToList(properties.getProperty(CCS_KEY)));
        eMailMessage.setSubject(properties.getProperty(SUBJECT_KEY));
        eMailMessage.setBody(properties.getProperty(BODY_KEY));
        eMailMessage.setAttachments(AttachmentHelper.getListOFAttachements(CSVToList(properties.getProperty(ATTACHEMENT_NAME_KEY)), CSVToList(properties.getProperty(ATTACHEMENT_VALUE_KEY))));
        logger.debug("Returning ");
        return eMailMessage;
    }
    
    private static List<String> CSVToList(String csv) {
        if ((csv == null) || (csv.trim().length() == 0)) {
            return new ArrayList<String>();
        }
        
        return Arrays.asList(csv.split(","));
    }
    
    private static String listToCSV(List<String> list) {
        if (list != null) {
            return list.toString().replace("[", "").replace("]", "")
                .replace(", ", ",");
        }
        return "";
    }
}
