/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import java.util.List;

import com.unitedofoq.fabs.core.filter.FilterFieldDTO;

/**
 *
 * @author MEA
 */
public class ScreenFunctionDTO {

    private long dbid;
    private String oScreenName;
    private long oScreenDBID;
    private long oScreenModeDBID;
    private long oScreenFilterDBID;
    private boolean oScreenFilterActive;
    private List<FilterFieldDTO> oScreenFilterFilelds;
    private boolean oScreenFilterAlwayesShow;
    private boolean oScreenFilterShowInsideScreen;

    public ScreenFunctionDTO() {
    }

    public ScreenFunctionDTO(long dbid,
            String oScreenName,
            long oScreenDBID,
            long oScreenModeDBID,
            long oScreenFilterDBID,
            boolean oScreenFilterActive,
            boolean oScreenFilterAlwayesShow,
            boolean oScreenFilterShowInsideScreen) {
        this.dbid = dbid;
        this.oScreenName = oScreenName;
        this.oScreenDBID = oScreenDBID;
        this.oScreenModeDBID = oScreenModeDBID;
        this.oScreenFilterDBID = oScreenFilterDBID;
        this.oScreenFilterActive = oScreenFilterActive;
        this.oScreenFilterAlwayesShow = oScreenFilterAlwayesShow;
        this.oScreenFilterShowInsideScreen = oScreenFilterShowInsideScreen;
    }

    public long getDbid() {
        return dbid;
    }

    public void setDbid(long dbid) {
        this.dbid = dbid;
    }

    public String getoScreenName() {
        return oScreenName;
    }

    public void setoScreenName(String oScreenName) {
        this.oScreenName = oScreenName;
    }

    public long getoScreenDBID() {
        return oScreenDBID;
    }

    public void setoScreenDBID(long oScreenDBID) {
        this.oScreenDBID = oScreenDBID;
    }

    public long getoScreenModeDBID() {
        return oScreenModeDBID;
    }

    public void setoScreenModeDBID(long oScreenModeDBID) {
        this.oScreenModeDBID = oScreenModeDBID;
    }

    public long getoScreenFilterDBID() {
        return oScreenFilterDBID;
    }

    public void setoScreenFilterDBID(long oScreenFilterDBID) {
        this.oScreenFilterDBID = oScreenFilterDBID;
    }

    public boolean isoScreenFilterActive() {
        return oScreenFilterActive;
    }

    public void setoScreenFilterActive(boolean oScreenFilterActive) {
        this.oScreenFilterActive = oScreenFilterActive;
    }

    public List<FilterFieldDTO> getoScreenFilterFilelds() {
        return oScreenFilterFilelds;
    }

    public void setoScreenFilterFilelds(List<FilterFieldDTO> oScreenFilterFilelds) {
        this.oScreenFilterFilelds = oScreenFilterFilelds;
    }

    public boolean isoScreenFilterAlwayesShow() {
        return oScreenFilterAlwayesShow;
    }

    public void setoScreenFilterAlwayesShow(boolean oScreenFilterAlwayesShow) {
        this.oScreenFilterAlwayesShow = oScreenFilterAlwayesShow;
    }

    public boolean isoScreenFilterShowInsideScreen() {
        return oScreenFilterShowInsideScreen;
    }

    public void setoScreenFilterShowInsideScreen(boolean oScreenFilterShowInsideScreen) {
        this.oScreenFilterShowInsideScreen = oScreenFilterShowInsideScreen;
    }
}
