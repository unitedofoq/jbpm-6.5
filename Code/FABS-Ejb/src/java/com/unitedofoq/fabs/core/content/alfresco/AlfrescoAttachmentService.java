/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.content.alfresco;

import com.unitedofoq.fabs.core.content.AlfrescoAttachmentServiceLocal;
import com.unitedofoq.fabs.core.content.AttachmentDTO;
import static com.unitedofoq.fabs.core.encryption.DataEncryptorService.loggedUser;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.entitysetup.OObjectBriefInfoField;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Asaied
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.content.AlfrescoAttachmentServiceLocal",
        beanInterface = AlfrescoAttachmentServiceLocal.class)
public class AlfrescoAttachmentService implements AlfrescoAttachmentServiceLocal {

    final static Logger logger = LoggerFactory.getLogger(AlfrescoAttachmentService.class);

    private final AlfrescoConnection alfrescoConnection = new AlfrescoConnection();
    private Session session;
    private Folder folder;

    @EJB
    FABSSetupLocal fabsSetup;

    @EJB
    OEntityManagerRemote oem;

    private String alfrescoFabsConnectionName;
    private String alfrescoUsername;
    private String alfrescoPassword;
    private String alfrescoMainFolder;
    private String alfrescoUrl;

    @Override
    public String put(AttachmentDTO attachment, long tenantID, BaseEntity entity) {
        String documentId = "";
        checkAlfrescoConnection();
        Folder entityFolder = alfrescoConnection.createFolder(session, folder, entity.getClassName());
        String objectName = "";
        try {
            List<String> conditions = new ArrayList<String>();
            conditions.add("entityClassPath='" + entity.getClass().getName() + "'");
            OEntity oentity = (OEntity) oem.loadEntity("OEntity", conditions, null, loggedUser);
            for (OObjectBriefInfoField oObjectBriefInfoField : oentity.getOObjectBriefInfoField()) {
                if (oObjectBriefInfoField.isMasterField()) {
                    objectName = (String) BaseEntity.getValueFromEntity(entity, oObjectBriefInfoField.getFieldExpression());
                    break;
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        if (objectName == null || objectName.equals("")) {
            objectName = entity.getDbid() + "";
        }
        Folder objectFolder = alfrescoConnection.createFolder(session, entityFolder, objectName);

        try {

            Document document = alfrescoConnection.createDocument(session, objectFolder, attachment);
            documentId = alfrescoConnection.convertDocumentToAttachmentDTO(document).getId();
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
        return documentId;
    }

    @Override
    public AttachmentDTO get(long tenantID, String attachmentID) {
        checkAlfrescoConnection();
        AttachmentDTO attachmentDTO = new AttachmentDTO();
        Document document = (Document) alfrescoConnection.getObjectById(session, attachmentID);
        if (document == null) {
            return null;
        }

        try {
            attachmentDTO = alfrescoConnection.convertDocumentToAttachmentDTO(document);
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
        return attachmentDTO;
    }

    @Override
    public void delete(String attachmentID, long tenantID) {
        checkAlfrescoConnection();
        alfrescoConnection.deleteDocument(session, attachmentID);
    }

    @Override
    public void update(AttachmentDTO attachment, long tenantID) {
        checkAlfrescoConnection();
        Map<String, Object> newDocumentProps = new HashMap<String, Object>();
        newDocumentProps.put(PropertyIds.DESCRIPTION, attachment.getDescription());
        alfrescoConnection.getObjectById(session, attachment.getId()).updateProperties(newDocumentProps);

    }

//    public AttachmentDTO getByName(String attachmentName, long tenantID) {
//        checkAlfrescoConnection();
//        AttachmentDTO attachmentDTO = new AttachmentDTO();
//        Folder folder = alfrescoConnection.getFolder(session, session.getRootFolder(), alfrescoMainFolder);
//        Document document = (Document) alfrescoConnection.getObjectByName(session, folder, attachmentName);
//        try {
//            attachmentDTO = alfrescoConnection.convertDocumentToAttachmentDTO(document);
//        } catch (IOException ex) {
//            logger.error(ex.getMessage());
//        }
//        return attachmentDTO;
//    }

    public void checkAlfrescoConnection() {
        if (session == null) {
            try {
                alfrescoFabsConnectionName = fabsSetup.loadKeySetup("Alfresco_Fabs_Connection_Name", loggedUser).getSvalue();
                alfrescoUsername = fabsSetup.loadKeySetup("Alfresco_Username", loggedUser).getSvalue();
                alfrescoPassword = fabsSetup.loadKeySetup("Alfresco_Password", loggedUser).getSvalue();
                alfrescoUrl = fabsSetup.loadKeySetup("Alfresco_Url", loggedUser).getSvalue();

                session = alfrescoConnection.getSession(alfrescoUrl, alfrescoFabsConnectionName, alfrescoUsername, alfrescoPassword);

            } catch (Exception ex) {
                logger.error("Unable to connect to Alfresco {0} " , ex.getMessage());
            }

        }
        alfrescoMainFolder = fabsSetup.loadKeySetup("Fabs_MainFolder", loggedUser).getSvalue();
        folder = alfrescoConnection.getFolder(session, session.getRootFolder(), alfrescoMainFolder);
        if (folder == null) {
            alfrescoConnection.createFolder(session, session.getRootFolder(), alfrescoMainFolder);
        }

    }
}
