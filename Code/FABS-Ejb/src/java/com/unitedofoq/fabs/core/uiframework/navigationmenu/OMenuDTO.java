/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.navigationmenu;

import java.util.ArrayList;
import java.util.List;

import com.unitedofoq.fabs.core.function.OMenu;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author M. Nagy
 */
public class OMenuDTO implements Comparable<OMenuDTO> {

    private Long dbid;
    private String name;
    private String nameTranslated;
    private List<OMenuDTO> childMenues;
    private int sortIndex;
    private String hierCode;
    private List<OfunctionDTO> functions;
    private Long largeImageDbid;
    private String imageURL;
    final static Logger logger = LoggerFactory.getLogger(OMenuDTO.class);
    public OMenuDTO(Long dbid, String name,String nameTranslated, ArrayList<OMenuDTO> child, int sortIndex,
            String hierCode, ArrayList<OfunctionDTO> funs,Long imageDBID) {
        logger.debug("Initializing OMenuDTO");
        this.largeImageDbid = imageDBID;
        this.dbid = dbid;
        this.name = name;
        this.nameTranslated = nameTranslated;
        this.childMenues = child;
        this.sortIndex = sortIndex;
        this.hierCode = hierCode;
        this.functions = funs;
        logger.debug("OMenuDTO Initialized");
    }

    public static OMenuDTO constractOmenuDTO(Object obj,ArrayList<OfunctionDTO> funs,String menuImage){       
        logger.debug("Entering");
            OMenu menu = (OMenu) obj;
            Long DBID = menu.getDbid();
            ArrayList<OfunctionDTO> menuFuns = funs;
            Long imageDBID;
            if(menu.getLargeMenuImage() ==null){
                imageDBID = new Long(0);
            }else{
                imageDBID = menu.getLargeMenuImage().getDbid();
            }
            OMenuDTO omDTO = new OMenuDTO(DBID, menu.getName(),menu.getNameTranslated(), null, menu.getSortIndex(),
                    menu.getHierCode(), menuFuns,imageDBID);
            omDTO.setImageURL(menuImage);
            if(menu.isActive()== true){
                logger.trace("Menu is Active");
                logger.debug("Returning");
                return omDTO;
            }else{
                logger.trace("Menu is not Active");
                logger.debug("Returning");
                return null;
            }
            
    }
    
    public Long getDbid() {
        return dbid;
    }

    /**
     * @param dbid the dbid to set
     */
    public void setDbid(Long dbid) {
        this.dbid = dbid;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the nameTranslated
     */
    public String getNameTranslated() {
        return nameTranslated;
    }

    /**
     * @param nameTranslated the nameTranslated to set
     */
    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }

    /**
     * @return the childMenues
     */
    public List<OMenuDTO> getChildMenues() {
        return childMenues;
    }

    /**
     * @param childMenues the childMenues to set
     */
    public void setChildMenues(List<OMenuDTO> childMenues) {
        this.childMenues = childMenues;
    }

    /**
     * @return the sortIndex
     */
    public int getSortIndex() {
        return sortIndex;
    }

    /**
     * @param sortIndex the sortIndex to set
     */
    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    /**
     * @return the hierCode
     */
    public String getHierCode() {
        return hierCode;
    }

    /**
     * @param hierCode the hierCode to set
     */
    public void setHierCode(String hierCode) {
        this.hierCode = hierCode;
    }

    /**
     * @return the functions
     */
    public List<OfunctionDTO> getFunctions() {
        return functions;
    }

    /**
     * @param functions the functions to set
     */
    public void setFunctions(List<OfunctionDTO> functions) {
        if(this.functions !=null){
            this.functions.clear();
            this.functions.addAll(functions);
        }else{
            this.functions = new ArrayList<OfunctionDTO>();
            this.functions.addAll(functions);
        }
    }

    /**
     * @return the largeImageDbid
     */
    public Long getLargeImageDbid() {
        return largeImageDbid;
    }

    /**
     * @param largeImageDbid the largeImageDbid to set
     */
    public void setLargeImageDbid(Long largeImageDbid) {
        this.largeImageDbid = largeImageDbid;
    }

    /**
     * @return the imageURL
     */
    public String getImageURL() {
        return imageURL;
    }

    /**
     * @param imageURL the imageURL to set
     */
    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    @Override
    public int compareTo(OMenuDTO o) {
     return this.sortIndex - o.getSortIndex();
    }
}
