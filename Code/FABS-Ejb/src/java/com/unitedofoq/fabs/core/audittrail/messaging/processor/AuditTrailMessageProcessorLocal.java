/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.audittrail.messaging.processor;

import com.unitedofoq.fabs.core.audittrail.exception.InvalidAuditTrailObjectException;
import com.unitedofoq.fabs.core.audittrail.main.valueholder.AuditTrailMessageObject;

/**
 *
 * @author mostafa
 */
public interface AuditTrailMessageProcessorLocal {

    public void process(AuditTrailMessageObject auditTrailMessageObject) throws InvalidAuditTrailObjectException;
}
