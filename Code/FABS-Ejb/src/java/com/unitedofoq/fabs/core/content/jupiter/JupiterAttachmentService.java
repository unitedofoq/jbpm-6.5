/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.content.jupiter;

import com.asset.jupiter.client.ejb.document.DocumentEJB;
import com.asset.jupiter.client.ejb.jupiter.JupiterEJB;
import com.asset.jupiter.client.exceptions.JupiterException;
import com.asset.jupiter.client.exceptions.SiteException;
import com.asset.jupiter.client.models.DocumentModel;
import com.asset.jupiter.client.models.FieldModel;
import com.asset.jupiter.client.models.FieldValueModel;
import com.asset.jupiter.client.models.IndexClassModel;
import com.asset.jupiter.client.models.ItemModel;
import com.asset.jupiter.client.models.MediaManagerModel;
import com.asset.jupiter.client.models.MediaServerModel;
import com.asset.jupiter.client.models.RecordValueModel;
import com.asset.jupiter.client.models.TableModel;
import com.asset.jupiter.util.Defines;
import com.unitedofoq.fabs.core.content.db.*;
import com.unitedofoq.fabs.central.EntityManagerWrapperLocal;
import com.unitedofoq.fabs.core.content.AttachmentDTO;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.unitedofoq.fabs.core.Exception.BasicException;
import com.unitedofoq.fabs.core.ExceptionHandler.thrower.factory.ExceptionFactory;
import com.unitedofoq.fabs.core.ExceptionHandler.thrower.factory.ExceptionType;
import com.unitedofoq.fabs.core.content.JupiterAttachmentServiceLocal;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author lap
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.content.JupiterAttachmentServiceLocal",
        beanInterface = JupiterAttachmentServiceLocal.class)
public class JupiterAttachmentService implements JupiterAttachmentServiceLocal {

    final static Logger logger = LoggerFactory.getLogger(JupiterAttachmentService.class);
    private String decodePassword(String pass){
        int saltSize = "unitedofoq".length();
       return new String(Base64.decodeBase64(pass.getBytes())).substring(saltSize);
    }
    private String getDecodedPassword(){
        return decodePassword(jupiterProperties.getProperty("password"));
    }
    private JupiterEJB jupiterEJB;
    private Properties jupiterProperties;
    EntityManager em = null;
    @EJB
    EntityManagerWrapperLocal entityManagerWrapper;

    OUser user = new OUser();
    UserMessage userMessage = new UserMessage();

    @PostConstruct
    public void init() {
        try {
            jupiterEJB = JupiterHelper.getInstance().lookUp();
            jupiterProperties = JupiterHelper.getInstance().getJupiterProperties();
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public String put(AttachmentDTO attachmentDTO, long tenantID, BaseEntity entity) throws BasicException {
        logger.debug("Entering");
        String documentId = "";
        try {
            checkJupiterConnection();
            //login with admin super user
            jupiterEJB.logIn(jupiterProperties.getProperty("userName"), getDecodedPassword());
            //create itemModel of document
            ItemModel document = new ItemModel();

            document.setIndexClass(jupiterProperties.getProperty("indexClassID"));
            document.setItemType(com.asset.jupiter.util.Defines.ITEM_DOCUMENT);
            document.setExt(attachmentDTO.getName().split("\\.")[1]);
            document.setParentId(jupiterProperties.getProperty("knowledgePoolId"));
            document.setParentType(com.asset.jupiter.util.Defines.ITEM_KNOLEDGE_POOL);
            HashMap tableValues = new HashMap();
            document.setTablesValues(tableValues);
            IndexClassModel indexClassModel
                    = jupiterEJB.getIndexClass(jupiterProperties.getProperty("indexClassID"), true);
            HashMap tables = indexClassModel.getTablesList();
            Set key = tables.keySet();
            Iterator tablesIterator = key.iterator();

            while (tablesIterator.hasNext()) {
                String tableId = (String) tablesIterator.next();
                TableModel tableModel = (TableModel) tables.get(tableId);
                if (tableModel.isPrimary()) {
                    TableModel primaryTableModel = tableModel;

                    Vector recordValueList = new Vector();

                    primaryTableModel.setRecordsValueList(recordValueList);

                    RecordValueModel recordValueModel = new RecordValueModel();
                    recordValueList.add(recordValueModel);

                    HashMap fieldValueList = new HashMap();

                    HashMap primaryTableFields
                            = primaryTableModel.getFieldList();
                    Set primaryTableFieldIds = primaryTableFields.keySet();
                    Iterator primaryTableFieldsIterator
                            = primaryTableFieldIds.iterator();
                    while (primaryTableFieldsIterator.hasNext()) {
                        String fieldId
                                = (String) primaryTableFieldsIterator.next();
                        FieldModel fieldModel
                                = (FieldModel) primaryTableFields.get(fieldId);

                        Vector fieldValues = new Vector();
                        fieldModel.setFieldValues(fieldValues);

                        FieldValueModel fieldValueModel
                                = new FieldValueModel(fieldModel);
                        switch (fieldModel.getType()) {
                            case Defines.TYPE_ALPHA_NUMERIC:
                                if (fieldModel.getLabel().equals(jupiterProperties.getProperty("documentName"))) {
                                    fieldValues.add(attachmentDTO.getName().split("\\.")[0]);
                                } else if (fieldModel.getLabel().equals(jupiterProperties.getProperty("documentMimeType"))) {
                                    fieldValues.add(attachmentDTO.getMimeType());
                                }

                                break;

                            default:
                                break;

                        }
                        fieldValueList.put(fieldValueModel.getFieldId(),
                                fieldValueModel);

                    }
                    recordValueModel.setFieldValueList(fieldValueList);
                    tableValues.put(primaryTableModel.getTableId(),
                            primaryTableModel);
                }

            }
            DocumentModel documentModel = jupiterEJB.createDocument(document, true);
            documentId = documentModel.getItemId();
            MediaServerModel mediaServerModel = documentModel.getMediaServerModel();
            DocumentEJB documentEJB = documentModel.getDocumentEJB();
            String locationId = documentEJB.openStreamWriter(mediaServerModel, documentId, Defines.ITEM_DOCUMENT, attachmentDTO.getName().split("\\.")[1], false);
            documentEJB.writeStream(mediaServerModel, locationId, attachmentDTO.getAttachment());
            documentEJB.closeStreamWriter(locationId, mediaServerModel);
            jupiterEJB.closeItem(documentId);
        } catch (java.rmi.RemoteException ex) {
            logger.error("Exception thrown", ex);
            if (ex.getMessage().contains("Session has timed out or was invalidated")) {
                try {
                    jupiterEJB = JupiterHelper.getInstance().lookUp();
                    put(attachmentDTO, tenantID, entity);
                } catch (Exception ex1) {
                    logger.error("Exception thrown", ex1);
                }
            }

        } catch (com.asset.jupiter.client.exceptions.SiteException siteException) {
            logger.error("Exception thrown", siteException);
        } catch (java.io.IOException ioException) {
            logger.error("Exception thrown", ioException);
        } catch (com.asset.jupiter.client.exceptions.JupiterException jupiterException) {

            userMessage.setExceptionDetails(jupiterException.getMessage());
            ExceptionFactory.throwException(userMessage, ExceptionType.SYSTEM, user);
        } catch (Exception exception) {
            logger.error("Exception thrown", exception);
            userMessage.setExceptionDetails(exception.getMessage());
            ExceptionFactory.throwException(userMessage, ExceptionType.SYSTEM, user);
        } finally {
            try {
                if (jupiterEJB != null) {
                    jupiterEJB.logOut();
                }

            } catch (RemoteException ex) {
                logger.error("Exception thrown", ex);
            } catch (JupiterException ex) {
                logger.error("Exception thrown", ex);
            }
        }

        return documentId;
    }

    @Override
    public AttachmentDTO get(long tenantID, String attachmentID) throws BasicException {
        AttachmentDTO attachmentDTO = new AttachmentDTO();
        try {
            checkJupiterConnection();

            jupiterEJB.logIn(jupiterProperties.getProperty("userName"), getDecodedPassword());
            DocumentModel documentModel = jupiterEJB.openDocument(jupiterProperties.getProperty("knowledgePoolId"), Defines.ITEM_KNOLEDGE_POOL, attachmentID, Defines.OPEN_MODE_READ, false, true);
            attachmentDTO = converDocumentModelToDTO(documentModel, attachmentID);

            jupiterEJB.closeItem(attachmentID);

        } catch (RemoteException ex) {
            logger.error("Exception thrown", ex);
            if (ex.getMessage().contains("Session has timed out or was invalidated")) {
                try {
                    jupiterEJB = JupiterHelper.getInstance().lookUp();
                    get(tenantID, attachmentID);
                } catch (Exception ex1) {
                    logger.error("Exception thrown", ex1);
                }
            }
        } catch (com.asset.jupiter.client.exceptions.SiteException siteException) {
            logger.error("Exception thrown", siteException);
        } catch (JupiterException ex) {
            logger.error("Exception thrown", ex);
            OUser user = new OUser();
            UserMessage userMessage = new UserMessage();
            userMessage.setExceptionDetails(ex.getMessage());
            ExceptionFactory.throwException(userMessage, ExceptionType.SYSTEM, user);
        } catch (IOException ioException) {
            logger.error("Exception thrown", ioException);
        } catch (Exception exception) {
            logger.error("Exception thrown", exception);
            userMessage.setExceptionDetails(exception.getMessage());
            ExceptionFactory.throwException(userMessage, ExceptionType.SYSTEM, user);

        } finally {
            try {
                if (jupiterEJB != null) {
                    jupiterEJB.logOut();
                }

            } catch (RemoteException ex) {
                logger.error("Exception thrown", ex);
            } catch (JupiterException ex) {
                logger.error("Exception thrown", ex);
            }

        }

        return attachmentDTO;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void delete(String attachmentID, long tenantID) throws BasicException {
        logger.debug("Entering");
        Vector deletedItems = new Vector();
        deletedItems.add(attachmentID);
        try {
            checkJupiterConnection();
            jupiterEJB.logIn(jupiterProperties.getProperty("userName"), getDecodedPassword());
            jupiterEJB.deleteItems(jupiterProperties.getProperty("knowledgePoolId"), Defines.ITEM_KNOLEDGE_POOL, deletedItems);
        } catch (JupiterException ex) {
            logger.error("Exception thrown", ex);
            OUser user = new OUser();
            UserMessage userMessage = new UserMessage();
            userMessage.setExceptionDetails(ex.getMessage());
            ExceptionFactory.throwException(userMessage, ExceptionType.SYSTEM, user);
        } catch (RemoteException ex) {
            java.util.logging.Logger.getLogger(JupiterAttachmentService.class.getName()).log(Level.SEVERE, null, ex);
            if (ex.getMessage().contains("Session has timed out or was invalidated")) {
                try {
                    jupiterEJB = JupiterHelper.getInstance().lookUp();
                    delete(attachmentID, tenantID);
                } catch (Exception ex1) {
                    logger.error("Exception thrown", ex1);
                }
            }
        } catch (Exception exception) {
            logger.error("Exception thrown", exception);
            userMessage.setExceptionDetails(exception.getMessage());
            ExceptionFactory.throwException(userMessage, ExceptionType.SYSTEM, user);

        } finally {
            try {
                if (jupiterEJB != null) {
                    jupiterEJB.logOut();
                }

            } catch (RemoteException ex) {
                logger.error("Exception thrown", ex);
            } catch (JupiterException ex) {
                logger.error("Exception thrown", ex);
            }
        }
        logger.debug("Returning");
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void update(AttachmentDTO attachment, long tenantID) throws BasicException {
        logger.debug("Entering");
        try {
            checkJupiterConnection();
            jupiterEJB.logIn(jupiterProperties.getProperty("userName"), getDecodedPassword());
            DocumentModel documentModel = jupiterEJB.openDocument(jupiterProperties.getProperty("knowledgePoolId"), Defines.ITEM_KNOLEDGE_POOL, attachment.getId(), Defines.OPEN_MODE_WRITE, false, false);
            HashMap tables = documentModel.getTablesValues();
            Set keys = tables.keySet();
            Iterator tablesIterator = keys.iterator();
            while (tablesIterator.hasNext()) {
                String tableId = (String) tablesIterator.next();
                TableModel tableModel = (TableModel) tables.get(tableId);
                Collection recordValueList = tableModel.getRecordsValueList();
                Iterator recordValueIterator = recordValueList.iterator();
                while (recordValueIterator.hasNext()) {
                    RecordValueModel recordValueModel = (RecordValueModel) recordValueIterator.next();
                    HashMap fieldValueList = recordValueModel.getFieldValueList();
                    Set fieldsKey = fieldValueList.keySet();
                    Iterator fieldsIterator = fieldsKey.iterator();
                    while (fieldsIterator.hasNext()) {
                        String fieldId = (String) fieldsIterator.next();
                        FieldModel fieldModel = (FieldModel) fieldValueList.get(fieldId);

                        if (fieldModel.getLabel().equals(jupiterProperties.getProperty("documentDescription"))) {
                            Vector newValue = new Vector();
                            newValue.add(attachment.getDescription());
                            fieldModel.setFieldValues(newValue);
                        }

                    }
                }
            }
            DocumentModel updatedDocument = (DocumentModel) jupiterEJB.updateItem(documentModel, true, false);
            jupiterEJB.closeItem(updatedDocument.getItemId());
        } catch (java.rmi.RemoteException remoteException) {

            logger.error("Exception thrown", remoteException);
            if (remoteException.getMessage().contains("Session has timed out or was invalidated")) {
                try {
                    jupiterEJB = JupiterHelper.getInstance().lookUp();
                    update(attachment, tenantID);
                } catch (Exception ex1) {
                    logger.error("Exception thrown", ex1);
                }
            }
        } catch (com.asset.jupiter.client.exceptions.SiteException siteException) {

            logger.error("Exception thrown", siteException);

        } catch (com.asset.jupiter.client.exceptions.JupiterException jupiterException) {

            logger.error("Exception thrown", jupiterException);
            userMessage.setExceptionDetails(jupiterException.getMessage());
            ExceptionFactory.throwException(userMessage, ExceptionType.SYSTEM, user);
        } catch (Exception exception) {
            logger.error("Exception thrown", exception);
            userMessage.setExceptionDetails(exception.getMessage());
            ExceptionFactory.throwException(userMessage, ExceptionType.SYSTEM, user);

        } finally {
            try {
                if (jupiterEJB != null) {
                    jupiterEJB.logOut();
                }

            } catch (RemoteException ex) {
                logger.error("Exception thrown", ex);
            } catch (JupiterException ex) {
                logger.error("Exception thrown", ex);
            }
        }
    }

    private AttachmentDTO converDocumentModelToDTO(DocumentModel documentModel, String attachmentID) throws SiteException, RemoteException {
        AttachmentDTO attachmentDTO = new AttachmentDTO();
        HashMap tables = documentModel.getTablesValues();
        Set keys = tables.keySet();
        Iterator tablesIterator = keys.iterator();
        while (tablesIterator.hasNext()) {
            String tableId = (String) tablesIterator.next();
            TableModel tableModel = (TableModel) tables.get(tableId);
            Collection recordValueList = tableModel.getRecordsValueList();
            Iterator recordValueIterator = recordValueList.iterator();
            while (recordValueIterator.hasNext()) {
                RecordValueModel recordValueModel = (RecordValueModel) recordValueIterator.next();
                HashMap fieldValueList = recordValueModel.getFieldValueList();
                Set fieldsKey = fieldValueList.keySet();
                Iterator fieldsIterator = fieldsKey.iterator();
                while (fieldsIterator.hasNext()) {
                    String fieldId = (String) fieldsIterator.next();
                    FieldModel fieldModel = (FieldModel) fieldValueList.get(fieldId);

                    if (fieldModel.getLabel().equals(jupiterProperties.getProperty("documentDescription"))) {
                        attachmentDTO.setDescription((String) ((Vector) fieldModel.getFieldValues()).get(0));
                    } else if (fieldModel.getLabel().equals(jupiterProperties.getProperty("documentMimeType"))) {
                        attachmentDTO.setMimeType((String) ((Vector) fieldModel.getFieldValues()).get(0));
                    }
                }
            }
        }
        MediaServerModel mediaServerModel = documentModel.getMediaServerModel();
        DocumentEJB documentEJB = documentModel.getDocumentEJB();
        MediaManagerModel mediaManagerModel = documentEJB.openStreamReader(mediaServerModel, attachmentID, Defines.ITEM_DOCUMENT);
        byte[] data = documentEJB.readStream(mediaServerModel, mediaManagerModel.getLocationId(), 0, mediaManagerModel.getDocumentSize());

        attachmentDTO.setAttachment(data);
        attachmentDTO.setId(attachmentID);

        attachmentDTO.setName(documentModel.getLabel() + "." + documentModel.getExt());
        return attachmentDTO;
    }

    void checkJupiterConnection() throws Exception {
        if (jupiterEJB == null) {

            throw new Exception("Unable to connect to jupiter, Please check connection");
        }
    }

}
