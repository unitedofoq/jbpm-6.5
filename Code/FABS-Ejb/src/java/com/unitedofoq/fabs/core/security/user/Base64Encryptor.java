/*
 * ============================================================================
 *                   GNU Lesser General Public License
 * ============================================================================
 *
 * Taylor - The Java Enterprise Application Framework.
 * Copyright (C) 2005 John Gilbert jgilbert01@users.sourceforge.net
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 * 
 * John Gilbert
 * Email: jgilbert01@users.sourceforge.net
 */
package com.unitedofoq.fabs.core.security.user;

import com.unitedofoq.fabs.core.usermessage.UserMessage;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * A simple but not very secure Base64 implementation. This is good for testing
 * or small applications that need zero administration. This is the default
 * encryptor.
 *
 * @author jgilbert
 *
 */
public class Base64Encryptor implements Encryptor {
final static Logger logger = LoggerFactory.getLogger(Base64Encryptor.class);
    @Override
    public String encrypt(String s) {
        return new BASE64Encoder().encode(s.getBytes());
    }

    @Override
    public String decrypt(String s) {
        logger.debug("Entering");
        try {
            return new String(new BASE64Decoder().decodeBuffer(s));
        } catch (IOException ex) {
            logger.error("Exception thrown: ",ex);
            throw new RuntimeException(ex);
        }
        finally{
            logger.debug("Returning");
        }
    }
}
