package com.unitedofoq.fabs.core.encryption;

import com.unitedofoq.fabs.core.audittrail.exception.InvalidMethodSignatureException;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntityField;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SensitiveDataFieldEntityListener {

    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private DataEncryptorServiceLocal dataEncyrptor;
    @EJB
    private EntitySetupServiceRemote ess;

    final static Logger logger = LoggerFactory.getLogger(SensitiveDataFieldEntityListener.class);

    @AroundInvoke
    public Object sensitiveEntityInterceptor(InvocationContext ctx)
            throws Exception {
        logger.debug("Entering: sensitiveEntityInterceptor");
        //@FixMe: exception handling
        //Exceptions:- ColumnNotFoundException, SymmetricKeyNotFoundException, 
        //@Todo: handle exceptions
        Object[] params = ctx.getParameters();
        BaseEntity newObject = null;
        OUser loggedUser = null;
        Object result = null;

        if (!ctx.getMethod().getName().contains("saveEntity")) {
            try {
                result = ctx.proceed();
            } catch (InvalidMethodSignatureException ex) {
                logger.error("Exception thrown",ex);
                throw new InvalidMethodSignatureException();
            } catch (Exception ex) {
                logger.error("Exception thrown",ex);
            } finally {
                logger.debug("Returning from: sensitiveEntityInterceptor");
                return result;
            }
        }
        if (ctx.getMethod().getName().contains("saveEntity")) {
            newObject = (BaseEntity) params[0];
            loggedUser = (OUser) params[1];
        } else {
        }
        String encryptedValue;
        String msg;

        if (//!newObject.getClass().getName().contains("com.unitedofoq.fabs")&& 
                !newObject.getClassName().contains("Audit")) {
            List<String> conditions = new ArrayList<String>();
            conditions.add("encrypted = true");
            OEntity entity = oem.getOEntity(newObject.getClassName(), loggedUser);
            conditions.add("oentity.dbid =" + entity.getDbid());
            List<OEntityField> oEntityFields = oem.loadEntityList(OEntityField.class.getSimpleName(),
                    conditions, null, null, loggedUser);

            if (oEntityFields == null || oEntityFields.isEmpty()) {
                logger.debug("Returning from: sensitiveEntityInterceptor");
                return ctx.proceed();
            }

            for (OEntityField field : oEntityFields) {
                List<ExpressionFieldInfo> infos
                        = BaseEntity.parseFieldExpression(Class.forName(
                                        field.getOentity().getEntityClassPath()),
                                field.getFieldExpression());
                ExpressionFieldInfo efi = infos.get(infos.size() - 1);
                Field entityField = efi.field;

                if (entityField == null) {
                    logger.debug("Returning from: sensitiveEntityInterceptor");
                    return ctx.proceed();
                }

                if (ctx.getMethod().getName().contains("saveEntity")) {
                    if (!newObject.isEncyrpted()) {
                        if (entityField.getType() == String.class
                                && entityField.getName() + "Enc" != null) {
                            //encrypt string;
                            msg = (String) BaseEntity.getValueFromEntity(newObject,
                                    entityField.getName());
                            if (null != msg && !msg.isEmpty() && !"".equals(msg)) {
                                encryptedValue = dataEncyrptor.textEncryptor(msg);
                                BaseEntity.setValueInEntity(newObject,
                                        entityField.getName() + "Enc", encryptedValue);
                                BaseEntity.setValueInEntity(newObject,
                                        entityField.getName(), " ");
                            }
                        } else if (entityField.getType() == BigDecimal.class
                                && entityField.getName() + "Enc" != null) {
                            if (BaseEntity.
                                    getValueFromEntity(newObject,
                                            entityField.getName()) != null) {
                                BigDecimal bd = new BigDecimal(BaseEntity.
                                        getValueFromEntity(newObject,
                                                entityField.getName()).toString());
                                if (null != bd) {
                                    String encryptedDValue = dataEncyrptor.bigDecEncryptor(bd);//.toString();
                                    BaseEntity.setValueInEntity(newObject,
                                            entityField.getName() + "Enc", encryptedDValue);
                                    BaseEntity.setValueInEntity(newObject,
                                            entityField.getName(), 0.0);
                                }
                            }
                        } else if (entityField.getType() == Integer.class
                                && entityField.getName() + "Enc" != null) {
                            if (BaseEntity.getValueFromEntity(newObject,
                                    entityField.getName()) != null) {
                                BigInteger msgint = new BigInteger((BaseEntity.
                                        getValueFromEntity(newObject,
                                                entityField.getName())).toString());
                                if (null != msgint) {
                                    String encryptedIntValue = dataEncyrptor.bigIntEncryptor(msgint);//.toString();

                                    BaseEntity.setValueInEntity(newObject,
                                            entityField.getName() + "Enc", encryptedIntValue);
                                    BaseEntity.setValueInEntity(newObject,
                                            entityField.getName(), 0);
                                }
                            }

                        }
                    }
                }
            }
            newObject.setEncyrpted(true);
        }
        logger.debug("Returning from: sensitiveEntityInterceptor");
        return ctx.proceed();
    }
}
