/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.report;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.module.OModule;

/**
 *
 * @author melsayed
 */
@Entity
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class OReportLayout extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OModule omodule;

    public String getOmoduleDD(){
        return "OReportLayout_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
// </editor-fold>

    private String name;
    private String snapshotPath;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    public String getNameDD() {
        return "OReportLayout_name";
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the snapshotPath
     */
    public String getSnapshotPath() {
        return snapshotPath;
    }

    public String getSnapshotPathDD() {
        return "OReportLayout_snapshotPath";
    }

    /**
     * @param snapshotPath the snapshotPath to set
     */
    public void setSnapshotPath(String snapshotPath) {
        this.snapshotPath = snapshotPath;
    }
}
