/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen.dto;

/**
 *
 * @author root
 */
public class SingleEntityScreenDTO extends OScreenDTO{
    protected boolean editable;
    protected boolean removable;
    protected String oactOnEntityClassPath;
    protected boolean saveAndExit;
    protected boolean save;

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isRemovable() {
        return removable;
    }

    public void setRemovable(boolean removable) {
        this.removable = removable;
    }

    public String getOactOnEntityClassPath() {
        return oactOnEntityClassPath;
    }

    public void setOactOnEntityClassPath(String oactOnEntityClassPath) {
        this.oactOnEntityClassPath = oactOnEntityClassPath;
    }

    public boolean isSaveAndExit() {
        return saveAndExit;
    }

    public void setSaveAndExit(boolean saveAndExit) {
        this.saveAndExit = saveAndExit;
    }

    public boolean isSave() {
        return save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }

    public SingleEntityScreenDTO(boolean editable, boolean removable, 
            String oactOnEntityClassPath, boolean saveAndExit, boolean save, 
            long dbid, String inlineHelp, String name, String header, String viewPage,
            boolean showHeader, boolean showActiveOnly, boolean prsnlzd, long prsnlzdOriginal_DBID,
            long prsnlzdUser_DBID, long screenFilter_DBID) {
        super(dbid, inlineHelp, name, header, viewPage, showHeader, showActiveOnly,
                prsnlzd, prsnlzdOriginal_DBID, prsnlzdUser_DBID, screenFilter_DBID);
        this.editable = editable;
        this.removable = removable;
        this.oactOnEntityClassPath = oactOnEntityClassPath;
        this.saveAndExit = saveAndExit;
        this.save = save;
    }

    
    
}
