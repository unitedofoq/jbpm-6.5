package com.unitedofoq.fabs.core.process.jBPM;

import java.util.List;
import java.util.Map;

import org.kie.api.definition.process.Node;
import org.kie.api.io.Resource;

/**
 *
 * @author mostafa
 */
public class ProcessDefinition {

    private String name;
    private List<String> inputs;
    private String packageName;
    private String version;
    private String type;
    private Map<String, Object> metaData;
    private byte[] processImage;
    private Resource resource;
    private Map<String, String> globals;
    private String namespace;
    private Node[] nodes;

    public String getName() {
        return name;
    }

    public String getNameDD() {
        return "ProcessDefinition_name";
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getPackageNameDD() {
        return "ProcessDefinition_packageName";
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getVersion() {
        return version;
    }

    public String getVersionDD() {
        return "ProcessDefinition_version";
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getType() {
        return type;
    }

    public String getTypeDD() {
        return "ProcessDefinition_type";
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, Object> getMetaData() {
        return metaData;
    }

    public void setMetaData(Map<String, Object> metaData) {
        this.metaData = metaData;
    }

    public byte[] getProcessImage() {
        return processImage;
    }

    public void setProcessImage(byte[] processImage) {
        this.processImage = processImage;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public Map<String, String> getGlobals() {
        return globals;
    }

    public void setGlobals(Map<String, String> globals) {
        this.globals = globals;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getNamespaceDD() {
        return "ProcessDefinition_namespace";
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public Node[] getNodes() {
        return nodes;
    }

    public void setNodes(Node[] nodes) {
        this.nodes = nodes;
    }

    public List<String> getInputs() {
        return inputs;
    }

    public void setInputs(List<String> inputs) {
        this.inputs = inputs;
    }
    
}
