package com.unitedofoq.fabs.core.security.user;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.uiframework.navigationmenu.NMEntityListener;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
@EntityListeners(value={NMEntityListener.class})
@ChildEntity(fields={"rolePrivileges", "oroleUsers", "oroleMenus"})
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class ORole extends ObjectBaseEntity 
{
    // <editor-fold defaultstate="collapsed" desc="omodule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
    private OModule omodule;

    public String getOmoduleDD(){
        return "orole_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="name">
    @Column(unique=true,nullable=false)
    private String name;
    public String getNameDD(){
        return "ORole_name";
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="description, translated">
    @Translatable(translationField="descriptionTranslated")
    private String description;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    
    @Translation(originalField="description")
    @Transient
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    public String getDescriptionTranslatedDD(){
        return "ORole_description";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="reserved">
    private boolean reserved = false;

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }
    
    public String getReservedDD(){
        return "ORole_reserved";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="rolePrivileges">
    @OneToMany(mappedBy="orole")
    private List<RolePrivilege> rolePrivileges = null;
    public List<RolePrivilege> getRolePrivileges() {
        return rolePrivileges;
    }

    public void setRolePrivileges(List<RolePrivilege> rolePrivileges) {
        this.rolePrivileges = rolePrivileges;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="oroleUsers">
    @OneToMany(mappedBy="orole")
    private List<RoleUser> oroleUsers = null;
    public List<RoleUser> getOroleUsers() {
        return oroleUsers;
    }

    public void setOroleUsers(List<RoleUser> oroleUsers) {
        this.oroleUsers = oroleUsers;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="oroleMenus">
    @OneToMany(mappedBy="orole")
    private List<RoleMenu> oroleMenus = null;

    public List<RoleMenu> getOroleMenus() {
        return oroleMenus;
    }

    public void setOroleMenus(List<RoleMenu> oroleMenus) {
        this.oroleMenus = oroleMenus;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Static variable">
    /**
     * Administrator Role DBID
     */
    @Transient
    public static final Long RLDBID_ADMIN = 1L ;
    // </editor-fold>
}