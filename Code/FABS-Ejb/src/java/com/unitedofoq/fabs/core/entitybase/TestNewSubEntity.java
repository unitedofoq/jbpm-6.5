package com.unitedofoq.fabs.core.entitybase;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
public class TestNewSubEntity extends BaseEntity {

    private String strField1;

    public String getStrField1() {
        return strField1;
    }

    public String getStrField1DD() {
        return "TestNewSubEntity_strField1";
    }

    public void setStrField1(String strField1) {
        this.strField1 = strField1;
    }

    private String strField2;

    public String getStrField2() {
        return strField2;
    }

    public String getStrField2DD() {
        return "TestNewSubEntity_strField2";
    }

    public void setStrField2(String strField2) {
        this.strField2 = strField2;
    }

    // <editor-fold defaultstate="collapsed" desc="translation">
    @Translatable(translationField = "strField1TrTranslated")
    private String strField1Tr;

    public String getStrField1Tr() {
        return strField1Tr;
    }

    public String getStrField1TrDD() {
        return "TestNewSubEntity_strField1Tr";
    }

    public void setStrField1Tr(String strField1Tr) {
        this.strField1Tr = strField1Tr;
    }

    @Transient
    @Translation(originalField = "strField1Tr")
    private String strField1TrTranslated;

    public String getStrField1TrTranslated() {
        return strField1TrTranslated;
    }

    public void setStrField1TrTranslated(String strField1TrTranslated) {
        this.strField1TrTranslated = strField1TrTranslated;
    }

    @Translatable(translationField = "strField2TrTranslated")
    private String strField2Tr;

    public String getStrField2Tr() {
        return strField2Tr;
    }

    public String getStrField2TrDD() {
        return "TestNewSubEntity_strField2Tr";
    }

    public void setStrField2Tr(String strField2Tr) {
        this.strField2Tr = strField2Tr;
    }

    @Transient
    @Translation(originalField = "strField2Tr")
    private String strField2TrTranslated;

    public String getStrField2TrTranslated() {
        return strField2TrTranslated;
    }

    public void setStrField2TrTranslated(String strField2TrTranslated) {
        this.strField2TrTranslated = strField2TrTranslated;
    }
    // </editor-fold >

    @Column(name = "strField3Col")
    private String strField3;

    public String getStrField3() {
        return strField3;
    }

    public String getStrField3DD() {
        return "TestNewSubEntity_strField3";
    }

    public void setStrField3(String strField3) {
        this.strField3 = strField3;
    }

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateField1;

    public Date getDateField1() {
        return dateField1;
    }

    public String getDateField1DD() {
        return "TestNewSubEntity_dateField1";
    }

    public void setDateField1(Date dateField1) {
        this.dateField1 = dateField1;
    }

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateField2;

    public Date getDateField2() {
        return dateField2;
    }

    public String getDateField2DD() {
        return "TestNewSubEntity_dateField2";
    }

    public void setDateField2(Date dateField2) {
        this.dateField2 = dateField2;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateField3Col")
    private Date dateField3;

    public Date getDateField3() {
        return dateField3;
    }

    public String getDateField3DD() {
        return "TestNewSubEntity_dateField3";
    }

    public void setDateField3(Date dateField3) {
        this.dateField3 = dateField3;
    }

    private BigDecimal decField1;

    public BigDecimal getDecField1() {
        return decField1;
    }

    public String getDecField1DD() {
        return "TestNewSubEntity_decField1";
    }

    public void setDecField1(BigDecimal decField1) {
        this.decField1 = decField1;
    }

    private BigDecimal decField2;

    public BigDecimal getDecField2() {
        return decField2;
    }

    public String getDecField2DD() {
        return "TestNewSubEntity_decField2";
    }

    public void setDecField2(BigDecimal decField2) {
        this.decField2 = decField2;
    }

    @Column(name = "decField3Col")
    private BigDecimal decField3;

    public BigDecimal getDecField3() {
        return decField3;
    }

    public String getDecField3DD() {
        return "TestNewSubEntity_decField3";
    }

    public void setDecField3(BigDecimal decField3) {
        this.decField3 = decField3;
    }

    @ManyToOne
    private TestNewEntity entity;

    public TestNewEntity getEntity() {
        return entity;
    }

    public String getEntityDD() {
        return "TestNewSubEntity_entity";
    }

    public void setEntity(TestNewEntity entity) {
        this.entity = entity;
    }

}
