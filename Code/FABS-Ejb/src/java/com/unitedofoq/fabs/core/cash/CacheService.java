/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.cash;

import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 *
 * @author mmasoud
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.cash.CacheServiceLocal",
        beanInterface = CacheServiceLocal.class)
public class CacheService implements CacheServiceLocal {

    public static Map<OUser, Map<String, Object>> cache = new HashMap<>();

    @Override
    public void cache(String key, Object value, OUser loggedUser) {
        Map<String, Object> innerMap;
        if (cache.get(loggedUser) == null) {
            innerMap = new HashMap<>();
        } else {
            innerMap = cache.get(loggedUser);
        }

        innerMap.put(key, value);
        cache.put(loggedUser, innerMap);
    }

    @Override
    public void decache(String key, OUser loggedUser) {
        if (cache.get(loggedUser) != null) {
            cache.get(loggedUser).remove(key);
        }
    }

    @Override
    public Object getFromCache(String key, OUser loggedUser) {
        Object object = null;
        if(cache.get(loggedUser) != null){
            object = cache.get(loggedUser).get(key);
        }
        return object;
    }
}
