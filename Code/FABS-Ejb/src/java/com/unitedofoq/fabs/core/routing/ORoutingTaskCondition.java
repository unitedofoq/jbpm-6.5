/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.routing;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;

/**
 *
 * @author lap3
 */
@Entity
public class ORoutingTaskCondition extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="outputMapping">
    private String outputMapping;

    public String getOutputMappingDD() {
        return "ORoutingTaskCondition_outputMapping";
    }
    
    public String getOutputMapping() {
        return outputMapping;
    }

    public void setOutputMapping(String outputMapping) {
        this.outputMapping = outputMapping;
    }
    
    
    // </editor-fold>   
    
    // <editor-fold defaultstate="collapsed" desc="mappedTask">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ORoutingTask mappedTask;
    /**
     * @return the mappedTask
     */
    public String getMappedTaskDD() {
        return "ORoutingTaskCondition_mappedTask";
    }
    public ORoutingTask getMappedTask() {
        return mappedTask;
    }

    /**
     * @param mappedTask the mappedTask to set
     */
    public void setMappedTask(ORoutingTask mappedTask) {
        this.mappedTask = mappedTask;
    }
    // </editor-fold>  
    
    // <editor-fold defaultstate="collapsed" desc="taskAction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ORoutingTask taskAction;
    public String getTaskActionDD() {
        return "ORoutingTaskCondition_taskAction";
    }
     /**
     * @return the taskAction
     */
    public ORoutingTask getTaskAction() {
        return taskAction;
    }

    /**
     * @param taskAction the taskAction to set
     */
    public void setTaskAction(ORoutingTask taskAction) {
        this.taskAction = taskAction;
    }
    // </editor-fold>  
}
