/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitybase;

import javax.ejb.Remote;

/**
 *
 * @author mustafa
 */
@Remote
public interface EntityBaseServiceWSLocal {
    
     public String setValueInEntity(String dbid, String entityName, String fieldExpression, String value, String loginName);
     
}
