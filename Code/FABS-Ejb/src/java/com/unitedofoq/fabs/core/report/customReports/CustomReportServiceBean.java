/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.report.customReports;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.encryption.DataEncryptorServiceLocal;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserServiceRemote;
import com.unitedofoq.fabs.core.setup.FABSSetup;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;

import java.io.ByteArrayInputStream;
import java.util.Map;

/**
 *
 * @author lap, bgalal
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.report.customReports.CustomReportServiceLocal",
        beanInterface = CustomReportServiceLocal.class)
public class CustomReportServiceBean implements CustomReportServiceLocal {

    private final static Logger LOGGER = LoggerFactory.getLogger(CustomReportServiceBean.class);
    
    @EJB
    private UserServiceRemote userService;
    @EJB
    private DataTypeServiceRemote dataTypeService;
    @EJB
    private OFunctionServiceRemote functionService;
    @EJB
    private DataEncryptorServiceLocal dataEncryptorService;
    @EJB
    private FABSSetupLocal fABSSetupLocal;
    
    private CustomReportBuilder customReportBuilder;
    
    @Resource
    SessionContext ctx;
    
    @Override
    public ByteArrayInputStream generateReport(AbstractReportGenerator reportGenerator){
        LOGGER.debug("Entering");
        
        //check encryption
        ReportDTO reportDTO = reportGenerator.getReportDTO();
        
        OUser user = reportDTO.getLoggedUser();
        
        boolean encryptionEnabled = isEncryptionEnabled(user);
        
        if(encryptionEnabled) {
            reportDTO.setEncryptionEnabled(true);
            reportDTO.setSymmetricKey(dataEncryptorService.getSymmetricKey());
            reportDTO.setIvKey(dataEncryptorService.getIvKey());
        } else {
            reportDTO.setEncryptionEnabled(false);
        }
        
        //set tannet password
        reportDTO.setTannentPassword(reportDTO.getLoggedUser().getTenant().getConnPassword());
        
        customReportBuilder = new CustomReportBuilder(reportDTO);
        
        LOGGER.trace("Returning");
        return new ByteArrayInputStream(reportGenerator.generateReport(customReportBuilder).toByteArray());
    }
    
    private boolean isEncryptionEnabled(OUser loggedUser) {
        //EnableEncryption Check
        FABSSetup fABSSetup = fABSSetupLocal.loadKeySetup("enableEncryption", loggedUser);
        if (fABSSetup != null && fABSSetup.getSvalue().toLowerCase().equals("true")) {
            return true;
        }
        return false;
    }
    
    //helper private methods
    @Override
    public String reportUserExitJavaFunction(ReportDTO reportDTO) {
        ODataType screenDataLoadingUEDT = dataTypeService.loadODataType("ScreenDataLoadingUE", reportDTO.getLoggedUser());
       for (Map.Entry<String, String> entry : reportDTO.getDataSetIterator()){
            ODataMessage screenDataLoadingUEDM = new ODataMessage();
            screenDataLoadingUEDM.setODataType(screenDataLoadingUEDT);
            List<Object> odmData = new ArrayList<Object>();
            odmData.add(entry.getValue());
            odmData.add(reportDTO.getReportName());
            odmData.add(entry.getKey());
            screenDataLoadingUEDM.setData(odmData);
            OFunctionParms params = new OFunctionParms();
            params.setParams(reportDTO.getParametersMap());
            // Call the function
            if (null != reportDTO.getUserExit()) {
                OFunctionResult functionRetFR = functionService.runFunction(
                        reportDTO.getUserExit(), screenDataLoadingUEDM, params, reportDTO.getLoggedUser());

                if (functionRetFR.getErrors().isEmpty()) {
                    reportDTO.addDataSetQuery(entry.getKey(), (String) functionRetFR.getReturnedDataMessage().getData().get(0));
                }
            }

       }
        return "";
    }
}
