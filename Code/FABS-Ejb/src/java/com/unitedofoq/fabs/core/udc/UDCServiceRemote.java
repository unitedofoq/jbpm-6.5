package com.unitedofoq.fabs.core.udc;

import java.util.List;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;

@Local
public interface UDCServiceRemote {

     public List<UDC> getUDCs(long udcTypeDBID, OUser user);
     public OFunctionResult createUDCTypeDD(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
     public OFunctionResult deleteUDCTypeDD(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public com.unitedofoq.fabs.core.udc.UDCLite getUDC(long udcDBID, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    
    public java.util.List<com.unitedofoq.fabs.core.udc.UDCLite> getLiteUDCs(long typeDBID, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

}
