/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.ExceptionHandler.catcher.main.impl;

import java.lang.reflect.Method;

import javax.ejb.Stateless;

import com.unitedofoq.fabs.core.ExceptionHandler.catcher.main.IMethodExceptionHandler;

import com.unitedofoq.fabs.core.security.user.OUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mostafa
 */
@Stateless
public class NavigationMenuExceptionHandler implements 
        IMethodExceptionHandler  {
final static Logger logger = LoggerFactory.getLogger(NavigationMenuExceptionHandler.class);
    @Override
    public void handle(Class classOfException,Method methodOfException, String exceptionMsg,
            String msgCode, OUser loggedUser) {
        logger.error("Exception thrown",exceptionMsg);
        logger.trace("Class of Exception {} Method of Exception {} Message Code {}",
                classOfException.getName(),
                methodOfException.getName(), msgCode);
    }

}
