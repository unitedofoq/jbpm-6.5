/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.navigationmenu;

import java.util.List;

/**
 *
 * @author MEA
 */
public class ORoleDTO {

    private long oModuleDbid;
    private String name;
    private String description;
    private boolean reserved;
    private List<Long> rolePrivilegesDbids;
    private List<Long> oroleUsersDbids;
    private List<Long> oroleMenusDbids;

    public ORoleDTO() {
    }

    public ORoleDTO(long oModuleDbid, String name, String description, boolean reserved) {
        this.oModuleDbid = oModuleDbid;
        this.name = name;
        this.description = description;
        this.reserved = reserved;
    }
    
    public ORoleDTO(long oModuleDbid, String name, String description, boolean reserved, List<Long> rolePrivilegesDbids, List<Long> oroleUsersDbids, List<Long> oroleMenusDbids) {
        this.oModuleDbid = oModuleDbid;
        this.name = name;
        this.description = description;
        this.reserved = reserved;
        this.rolePrivilegesDbids = rolePrivilegesDbids;
        this.oroleUsersDbids = oroleUsersDbids;
        this.oroleMenusDbids = oroleMenusDbids;
    }

    public long getoModuleDbid() {
        return oModuleDbid;
    }

    public void setoModuleDbid(long oModuleDbid) {
        this.oModuleDbid = oModuleDbid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }

    public List<Long> getRolePrivilegesDbids() {
        return rolePrivilegesDbids;
    }

    public void setRolePrivilegesDbids(List<Long> rolePrivilegesDbids) {
        this.rolePrivilegesDbids = rolePrivilegesDbids;
    }

    public List<Long> getOroleUsersDbids() {
        return oroleUsersDbids;
    }

    public void setOroleUsersDbids(List<Long> oroleUsersDbids) {
        this.oroleUsersDbids = oroleUsersDbids;
    }

    public List<Long> getOroleMenusDbids() {
        return oroleMenusDbids;
    }

    public void setOroleMenusDbids(List<Long> oroleMenusDbids) {
        this.oroleMenusDbids = oroleMenusDbids;
    }
    
    

}
