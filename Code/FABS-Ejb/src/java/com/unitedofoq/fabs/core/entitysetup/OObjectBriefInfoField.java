package com.unitedofoq.fabs.core.entitysetup;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;

/**
 *
 * @author arezk
 */
//Overview: Represents the information displayed in the balloon information panel
@Entity
@ParentEntity(fields={"oObject"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class OObjectBriefInfoField extends BaseEntity{
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private OEntity oObject;
    public String getOObjectDD() {
        return "OObjectBriefInfoField_oObject" ;
    }

//Binded backbean value for this info field
    @Column(nullable=false)
    private String fieldExpression;
    public String getFieldExpressionDD() {
        return "OObjectBriefInfoField_fieldExpression";
    }

//Displayed in the balloon panel or not
    @Column(name = "prim",nullable=false)
    private boolean primary;

    public boolean isPrimary() {
        return primary;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }
    

    public String getPrimaryDD() {
        return "OObjectBriefInfoField_primary";
    }

//Field type in java
    @Column(nullable=false)
    private String fieldJavaType;
    public String getFieldJavaTypeDD() {
        return "OObjectBriefInfoField_fieldJavaType";
    }

//Sorting index
    @Column(nullable=false)
    private int sortIndex;
    public String getSortIndexDD() {
        return "OObjectBriefInfoField_sortIndex";
    }

//Used in header or not
    @Column(nullable=false)
    private boolean masterField;

    public boolean isMasterField() {
        return masterField;
    }

    public void setMasterField(boolean masterField) {
        this.masterField = masterField;
    }

  
    
    public String getMasterFieldDD() {
        return "OObjectBriefInfoField_masterField";
    }

//Override parent title
    @Translatable(translationField="titleOverrideTranslated")
    private String titleOverride;
    
    @Translation(originalField="titleOverride")
    @Transient
    private String titleOverrideTranslated;

    public String getTitleOverrideTranslated() {
        return titleOverrideTranslated;
    }

    public void setTitleOverrideTranslated(String titleOverrideTranslated) {
        this.titleOverrideTranslated = titleOverrideTranslated;
    }

    public String getTitleOverrideTranslatedDD() {
        return "OObjectBriefInfoField_titleOverride";
    }

//Relation Many-To-One(DD)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private DD dd = null;
    public String getDdDD() {
        return "OObjectBriefInfoField_dd";
    }

    /* DD NAMES GETTERS */
    
    public String getOverrideTitle() {
        return getTitleOverride();
    }

    public void setOverrideTitle(String overrideTitle) {
        this.setTitleOverride(overrideTitle);
    }


    public String getFieldExpression() {
        return fieldExpression;
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }

    public String getFieldJavaType() {
        return fieldJavaType;
    }

    public void setFieldJavaType(String fieldJavaType) {
        this.fieldJavaType = fieldJavaType;
    }

    

   

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    /**
     * @return the titleOverride
     */
    public String getTitleOverride() {
        return titleOverride;
    }

    /**
     * @param titleOverride the titleOverride to set
     */
    public void setTitleOverride(String titleOverride) {
        this.titleOverride = titleOverride;
    }

    /**
     * @return the dd
     */
    public DD getDd() {
        return dd;
    }

    /**
     * @param dd the dd to set
     */
    public void setDd(DD dd) {
        this.dd = dd;
    }

    /**
     * @return the oObject
     */
    public OEntity getOObject() {
        return oObject;
    }

    /**
     * @param oObject the oObject to set
     */
    public void setOObject(OEntity oObject) {
        this.oObject = oObject;
    }
}
