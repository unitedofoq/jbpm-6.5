/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen.report;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;

/**
 *
 * @author mostafa
 */
@Entity
@DiscriminatorValue(value = "REPORT")
@ChildEntity(fields={"screenFieldColors"})
@VersionControlSpecs(versionControlType = VersionControlSpecs.VersionControlType.Parent)
public class ReportScreenField extends ScreenField {
    
    // <editor-fold defaultstate="collapsed" desc="groupByField">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    //@ManyToOne(mappedBy="reportScreenField", fetch = javax.persistence.FetchType.LAZY)
    private ReportScreenField groupByField;

    /**
     * @return the groupByField
     */
    public ReportScreenField getGroupByField() {
        return groupByField;
    }

    /**
     * @param groupByField the groupByField to set
     */
    public void setGroupByField(ReportScreenField groupByField) {
        this.groupByField = groupByField;
    }
    
    public String getGroupByFieldDD() {
        return "ReportScreenField_groupByField";
    }
    // </editor-fold> 
    
    // <editor-fold defaultstate="collapsed" desc="screenFieldColors">
    @OneToMany(mappedBy="reportScreenField", fetch = javax.persistence.FetchType.LAZY)
    private List<ScreenFieldColor> screenFieldColors;

    public String getScreenFieldColorsDD() {
        return "ReportScreenField_screenFieldColors";
    }

    public List<ScreenFieldColor> getScreenFieldColors() {
        return screenFieldColors;
    }

    public void setScreenFieldColors(List<ScreenFieldColor> screenFieldColors) {
        this.screenFieldColors = screenFieldColors;
    }
    // </editor-fold> 
    
    
    @Column
    private String aggregationFieldExp;

    public String getAggregationFieldExpDD() {
        return "ReportScreenField_aggregationFieldExp";
    }

    /**
     * @return the aggregationFieldExp
     */
    public String getAggregationFieldExp() {
        return aggregationFieldExp;
    }

    /**
     * @param aggregationFieldExp the aggregationFieldExp to set
     */
    public void setAggregationFieldExp(String aggregationFieldExp) {
        this.aggregationFieldExp = aggregationFieldExp;
    }

}
