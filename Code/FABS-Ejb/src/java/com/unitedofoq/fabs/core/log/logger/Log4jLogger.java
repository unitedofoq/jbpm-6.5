/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.log.logger;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Log4jLogger implements ILogger {

    private Logger currentLogger;
    private String configurationFile;

    public String getConfigurationFile() {
        return configurationFile;
    }

    public void setConfigurationFile(String configurationFile) {
        this.configurationFile = configurationFile;
    }

    public Log4jLogger() {
//        Logger root = Logger.getRootLogger();
////      Layout layout = new ANSIColorLayout();
//        root.addAppender(new ConsoleAppender(layout, ConsoleAppender.SYSTEM_OUT));
//        root.debug("Sample debug message");
        currentLogger = Logger.getRootLogger();
        Layout layout = new ANSIColorLayout();
        currentLogger.addAppender(new ConsoleAppender(layout, ConsoleAppender.SYSTEM_OUT));
        currentLogger.setLevel(Level.ALL);
//        BasicConfigurator.configure(); 
    }

    public void logError(StringBuilder message) {
        currentLogger.error(message);
    }

    public void logInfo(StringBuilder message) {
        currentLogger.info(message);
    }

    public void logWarn(StringBuilder message) {
        currentLogger.warn(message);
    }
}