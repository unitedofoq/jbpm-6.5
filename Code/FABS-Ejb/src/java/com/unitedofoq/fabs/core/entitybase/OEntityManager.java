package com.unitedofoq.fabs.core.entitybase;

import com.unitedofoq.fabs.central.EntityManagerWrapper;
import com.unitedofoq.fabs.central.EntityManagerWrapperLocal;
import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.central.OTenant;
import com.unitedofoq.fabs.core.audittrail.main.AuditTrailManager;
import com.unitedofoq.fabs.core.content.AttachmentDTO;
import com.unitedofoq.fabs.core.content.EntityAttachmentServiceLocal;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.encryption.DataEncryptorServiceLocal;
import com.unitedofoq.fabs.core.encryption.SensitiveDataFieldEntityListener;
import com.unitedofoq.fabs.core.entitybase.dbidservice.DBIDCacheServiceLocal;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntityAction;
import com.unitedofoq.fabs.core.entitysetup.OEntityField;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.i18n.TextTranslationException;
import com.unitedofoq.fabs.core.i18n.TextTranslationServiceRemote;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.security.SecurityServiceRemote;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.security.user.hat.Hat;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import com.unitedofoq.fabs.core.validation.EntityDataValidationException;
import com.unitedofoq.fabs.core.validation.FABSException;
import com.unitedofoq.fabs.core.validation.OEntityDataValidationException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.context.FacesContext;
import javax.interceptor.Interceptors;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OptimisticLockException;
import javax.persistence.Query;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;
import javax.transaction.UserTransaction;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author AbuBakr
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote",
        beanInterface = OEntityManagerRemote.class)
public class OEntityManager implements OEntityManagerRemote, OEntityManagerLocal {

    @EJB
    private DataEncryptorServiceLocal dataEncryptorServiceLocal;
    @EJB
    private EntityManagerWrapperLocal emw;
    @EJB
    private TextTranslationServiceRemote textTranslationService;
    @EJB
    private SecurityServiceRemote securityService;
    @EJB
    private EntityAttachmentServiceLocal attachmentService;
    @EJB
    private FABSSetupLocal setupSerivce;
    @EJB
    private DDServiceRemote ddService;
    @EJB
    private DBIDCacheServiceLocal dbidService;
    @EJB
    private OEntityManagerLocal localEntityManager;

    private final static int JDBC_MAX_BATCH_SIZE = 1000;

    private boolean nestedSortExpression = false;
    final static Logger logger = LoggerFactory.getLogger(OEntityManager.class);
    // <editor-fold defaultstate="collapsed" desc="Entity Manager for Multi-Tenant">
    @EJB
    private OCentralEntityManagerRemote centralOEM;

    /**
     * Make sure a user thread EntityManager is opened and ready for database
     * access
     *
     * <br> Calls
     * {@link OEntityManagerFactory#getEMW(OUser, EntityManagerWrapper.EMType, boolean)}
     * <br> Calls {@link EntityManagerWrapper#requestOpen()}, so, you need to
     * call {@link OEntityManager#closeEM(OUser)} after finish working with it
     *
     * @param loggedUser
     *
     * @return EntityManager: in case of success <br>null: in case of error
     * getting it
     *
     * @throws com.unitedofoq.fabs.core.validation.FABSException <br>In case of
     * no EntityManager can be got <br>In case of exception, exception is
     * attached to FABSExcpetion thrown
     */
    @Override
    public EntityManager getEM(OUser loggedUser)
            throws FABSException {
        logger.debug("Entering");
        logger.debug("Returning");
        return emw.getMultiTenantEntityManager(loggedUser.getTenant().getId());
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Configuration Condition">
    /**
     * "Configuration Condition" is Used in "conditions" or "sorts" passed to
     * the OEntityManager functions to indicate a configuration condition, i.e.
     * the condition is not a regular WHERE condition All Configuration
     * Conditions: Must have the predix CONFCOND_PREFIX
     */
    public static final String CONFCOND_PREFIX = "##";
    /**
     * "Configuration Condition" (See {
     *
     * @see #CONFCOND_PREFIX}) used to indicate that sorting is requested to be
     * Case Insensitive <br> is passed in the "sorts" functions argument
     */
    public static final String CONFCOND_CASE_INSENSITIVE = "##AB";
    /**
     * "Configuration Condition" (See {
     *
     * @see #CONFCOND_PREFIX}) used to indicate that both active and inactive
     * entities are required to be <br> is passed in the "conditions" functions
     * argument
     */
    public static final String CONFCOND_GET_INACTIVEONLY = "##INACTIVEONLY";
    public static final String CONFCOND_GET_ACTIVEONLY = "##ACTIVEONLY";
    public static final String CONFCOND_GET_ACTIVEANDINACTIVE = "##ACTIVEANDINACTIVE";
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Load Entity">

    /**
     * Used to load Entity from database knowing the entity Name and DBID <br>
     * It calls {@link #loadEntity(java.lang.String, java.util.List, java.util.List,
     *          com.unitedofoq.fabs.core.security.user.OUser)
     *          loadEntity(String, List<String>, List<String>, OUser)} with DBID added to
     * the conditinos
     *
     * @param entityName Same of {@link #loadEntity(String, List, List, OUser)}
     * <br>
     * @param dbid BaseEntity.DBID value of the entity <br>
     * @param conditions Same of {@link #loadEntity(String, List, List, OUser)}
     * <br>
     * @param fieldExpressions Same of
     * {@link #loadEntity(String, List, List, OUser)} <br>
     * @param loggedUser Same of {@link #loadEntity(String, List, List, OUser)}
     *
     * @return Same of {@link #loadEntity(String, List, List, OUser)}
     *
     * @throws com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException
     * Same of {@link #loadEntity(String, List, List, OUser)} <br>
     * @throws javax.persistence.EntityExistsException Same of
     * {@link #loadEntity(String, List, List, OUser)} <br>
     * @throws javax.persistence.EntityNotFoundException Same of
     * {@link #loadEntity(String, List, List, OUser)} <br>
     * @throws javax.persistence.NonUniqueResultException Same of
     * {@link #loadEntity(String, List, List, OUser)} <br>
     * @throws javax.persistence.NoResultException Same of
     * {@link #loadEntity(String, List, List, OUser)} <br>
     * @throws javax.persistence.OptimisticLockException Same of
     * {@link #loadEntity(String, List, List, OUser)} <br>
     * @throws javax.persistence.RollbackException Same of
     * {@link #loadEntity(String, List, List, OUser)} <br>
     * @throws javax.persistence.TransactionRequiredException Same of
     * {@link #loadEntity(String, List, List, OUser)}
     *
     * @see #loadEntityList(String, List, List, List, OUser)}
     */
    @Override
    public BaseEntity loadEntity(String entityName, long dbid,
            List<String> conditions, List<String> fieldExpressions, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException {
        logger.info("Entering");
        // Create new list to avoid updating the passed list (keep it AS IS)
        List updatedConditions = new ArrayList();
        if (conditions != null) {
            updatedConditions.addAll(conditions);
        }

        // Add the DBID to the conditiond
        updatedConditions.add("dbid = " + dbid);
        logger.info("Returning");
        return loadEntity(entityName, updatedConditions, fieldExpressions, loggedUser);
    }

    /**
     * Used to load Entity from database knowing the entity Name <br> Only loads
     * entity into {@link BaseEntity} subclasses <br> Caller can list conditions
     * to be included in the query <br> Only the List fields passed in
     * fieldExpressions will be loaded, unless annotated Fetch.EAGER in the
     * entity then EJB will automatically load them
     *
     * @param entityName Same of
     * {@link #loadEntityInternal(String, List, List, OUser)} <br>
     * @param conditions Same of
     * {@link #loadEntityInternal(String, List, List, OUser)} <br>
     * @param fieldExpressions Same of
     * {@link #loadEntityInternal(String, List, List, OUser)} <br>
     * @param loggedUser Required Input. Is the user requesting to load the data
     * to access. All security validations are done against him. If system user
     * is passed, then no security validations are done at all
     *
     * @return entity object: BaseEntity loaded successfully <br> null: Entity
     * not found, probably due to the conditions
     *
     * @throws com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException
     * Mostly thrown by {@link #checkUserAuthorityOnEntityAndAction}
     * @throws javax.persistence.EntityExistsException Same of
     * {@link #loadEntityInternal(String, List, List, OUser)}
     * @throws javax.persistence.EntityNotFoundException Same of
     * {@link #loadEntityInternal(String, List, List, OUser)}
     * @throws javax.persistence.NonUniqueResultException Same of
     * {@link #loadEntityInternal(String, List, List, OUser)}
     * @throws javax.persistence.NoResultException Same of
     * {@link #loadEntityInternal(String, List, List, OUser)}
     * @throws javax.persistence.OptimisticLockException Same of
     * {@link #loadEntityInternal(String, List, List, OUser)}
     * @throws javax.persistence.RollbackException Same of
     * {@link #loadEntityInternal(String, List, List, OUser)}
     * @throws javax.persistence.TransactionRequiredException Same of
     * {@link #loadEntityInternal(String, List, List, OUser)}
     *
     * @see #loadEntity(java.lang.String, long, java.util.List, java.util.List,
     * com.unitedofoq.fabs.core.security.user.OUser) loadEntity(String, long,
     * List<String>, List<String>, OUser)
     * @see #loadEntityList(java.lang.String, java.util.List, java.util.List,
     * java.util.List, com.unitedofoq.fabs.core.security.user.OUser)
     * loadEntityList(String, long, List<String>, List<String>, List<String>,
     * OUser)
     */
    @Override
    public BaseEntity loadEntity(String entityName, List<String> conditions,
            List<String> fieldExpressions, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException {

        logger.info("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        logger.info("Loading Entity. Name: {} ", entityName);
        // </editor-fold>
        // ______________
        // Check Security
        //
        if (!entityName.contains("Lite")) {
            securityService.checkActionEntityAuthority(entityName,
                    OEntityAction.ATDBID_RETRIEVE, loggedUser, loggedUser);
        }
        // No exception thrown, he is authorized

        // ___________
        // Load Entity
        //
        BaseEntity baseEntity = loadEntityInternal(
                entityName, conditions, fieldExpressions, loggedUser);
        // ____________________
        // Post Load Processing
        //
        // No Exceptions Thrown
        if (baseEntity != null) {
            List<BaseEntity> baseEntities = new ArrayList<BaseEntity>();
            baseEntities.add(baseEntity);
            baseEntities = postLoadEntity(baseEntities, conditions,
                    fieldExpressions, loggedUser);
        }
        emw.detach(baseEntity, loggedUser);
        // ______
        // Return
        //
        // <editor-fold defaultstate="collapsed" desc="Log">
//        OLog.logFunctionStatistics(entryTime, loggedUser,
//                "Name", entityName,
//                "Conditions", conditions,
//                "Expression", fieldExpressions);
        // </editor-fold>
        if (baseEntity != null) {
            logger.info("Returning with baseEntity", baseEntity.getClassName());
        } else {
            logger.info("base entity equals Null");
        }
        return textTranslationService.getEntityWithTowLevelsOfTranslatedFields(baseEntity, loggedUser);
    }

    /**
     * Load the entity from database <br> No Security Check <br> Must not be
     * public
     *
     * @param entityName Required Input. Is the unqualified entity name (i.e. is
     * not the full class name). <br>
     * @param conditions Optional Input. List of condition strings. Every string
     * is considered as a separate condition (e.g. <field name> <operator>
     * <value>). Conditions will be all ANDed. {@link #CONFCOND_GET_DELETED} is
     * supported and can be passed in. <br>
     * @param fieldExpressions Optional Input. List of fields names to be loaded
     * in the entity. Every string is one field expression (no comma
     * separation). Field should be a memeber field of the loaded entity. Field
     * subfield (".") is supported (e.g. currency.value). Both single-valued
     * (not list) and multi-valued (list) fields are supported. Handled by {@link #postLoadEntity
     * }
     * <
     * br>
     * @param loggedUser Required Input. Is the user requesting to load the data
     * to access. All security validations are done againest him. If system user
     * is passed, then no security validations are done at all
     *
     * @return entity : If successfully loaded <br> null : In case of error or
     * entity not found, check exceptions as well
     *
     * @throws javax.persistence.EntityExistsException
     * @throws javax.persistence.EntityNotFoundException
     * @throws javax.persistence.NonUniqueResultException
     * @throws javax.persistence.NoResultException
     * @throws javax.persistence.OptimisticLockException
     * @throws javax.persistence.RollbackException
     * @throws javax.persistence.TransactionRequiredException
     */
    private BaseEntity loadEntityInternal(String entityName, List<String> conds,
            List<String> fieldExpressions, OUser loggedUser)
            throws EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException {
        logger.trace("Entering");
        if (loggedUser == null) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Null User Passed. Entity Name: {}", entityName);
            // </editor-fold>
            logger.trace("Returning with Null");
            return null;
        }
        String whereClause = constructEntityWhereClause(conds);
//        List<String> conditions = loadEntityWithEncryptedfields(entityName,conds, fieldExpressions, loggedUser);
//        String whereClause = constructEntityWhereClause(conditions);
        // <editor-fold defaultstate="collapsed" desc="Log">
        Long timeBeforeQuery = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>
        String query = "SELECT entity FROM " + entityName + " entity " + whereClause;

        List<BaseEntity> baseEntities = null;
        try {
            baseEntities = emw.createQuery(query, loggedUser).getResultList();
        } catch (Exception ex) {
            logger.trace("Query: {}", query);
            logger.error("Exception thrown while executing Query", ex);
            logger.trace("Returning with Null");
            return null;
        }

        // MT: This line will detach the result set from the PersistenceContext
        // because in later usage, the result set is manually modified on the
        // the fly, and that causes synchronization exceptions
        emw.detachList(baseEntities, loggedUser);
        // <editor-fold defaultstate="collapsed" desc="Log">
//        OLog.logQueryStatistics(query, loggedUser ,timeBeforeQuery, baseEntities);
        // </editor-fold>
        if (baseEntities == null || baseEntities.isEmpty()) {
            logger.trace("Returning with Null");
            return null;
        } else if (baseEntities.size() > 1) {
            // This was commented because it causes the tx to rollback
//            throw new NonUniqueResultException();
            logger.warn("Non Unique Result Exception. Query: {}", query);
        }
        try {
            logger.trace("Returning");
            if(loggedUser.isStartupEnc()){//case startup with encrypted user
                return baseEntities.get(0);
            }
            return decrypteEntity(baseEntities.get(0), loggedUser);
        } catch (UserNotAuthorizedException ex) {
            logger.error("Exception thrown", ex);
            if (baseEntities != null) {
                logger.trace("Returning with: {}", baseEntities.get(0));
            } else {
                logger.trace("base entities equals Null");
            }
            return textTranslationService.getEntityWithTowLevelsOfTranslatedFields(baseEntities.get(0), loggedUser);
        }
    }

    private void loadAttribute(BaseEntity entity, String expression,
            Boolean forceLoadedListLoading, List<String> conditions, OUser loggedUser) {
        logger.trace("Entering");
        loadAttribute(entity, expression, forceLoadedListLoading, true, conditions, loggedUser);
        logger.trace("Returning");
    }

    /**
     * Load the attribute (of name = "expression" parameter) from database and
     * set it into its owner "entity" (passed parameter) <br>It assumes that
     * single-valued attributes are loaded by default (EAGER FETCH), so, it
     * doesn't load them. <br> It doesn't reload a <br> Since the function
     * doesn't perform complete security validations, then, it must be private
     * and not public
     *
     * @param entity Required Input. Owner entity of the attribute <br>
     * @param expression Required Input. Attribute name. Field subfield (".") is
     * supported (e.g. currency.value). <br>
     * @param forceListLoading If true, not-null not-IndirectList field will be
     * re-loaded <br> if false, not-null not-IndirectList will not be loaded
     * @param loggedUser Required Input. Is the user requesting to load the data
     * to access.
     */
    private void loadAttribute(BaseEntity entity, String expression,
            Boolean forceLoadedListLoading, Boolean forceFieldInitIfNull, List<String> conditions, OUser loggedUser) {
        logger.trace("Entering");
        List<ExpressionFieldInfo> expressionFldsInfo = null;
        Object parentObject = null; // Shouldn't be BaseEntity to be ready for any attribute type
        int expressionFieldsCount = -1;
        int fieldIndex = -1;
        try {
            // Parse field expression in all cases (having "." or not) for code
            // simplicity
            expressionFldsInfo = entity.parseFieldExpression(expression);
            if (expressionFldsInfo == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("Null Expression Field Info. Main Object: {}, Main Expression: {}", entity, expression);
                // </editor-fold>
                logger.trace("Returning");
                return;
            }
            if (expressionFldsInfo.size() < 0) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("Null Expression Field Info. Main Object: {}, Main Expression: {}", entity, expression);
                // </editor-fold>
                return;
            }
            // Check if primitive, no additional processing needed
            if (expressionFldsInfo.get(0).field.getType().isPrimitive()) {
                // <editor-fold defaultstate="collapsed" desc="Log">
//                OLog.logInformation("Primitive Attribute, No Load Needed", loggedUser,
//                        "Main Object", obj,
//                        "Main Expression", expression,
//                        "Field Name", expressionFldsInfo.get(0).fieldName);
                // </editor-fold>
                logger.trace("Returning");
                return;
            }
            // Initialize loop variables
            parentObject = entity;
            expressionFieldsCount = expressionFldsInfo.size();
            boolean fieldIsList = false;
            Field expField = null;
            // Loop on the expression fields
            for (fieldIndex = 0; fieldIndex < expressionFieldsCount; fieldIndex++) {
                expField = expressionFldsInfo.get(fieldIndex).field;
                fieldIsList = BaseEntity.getFieldType(expField).getSimpleName().equals("List");
                if (fieldIsList) {
                    // Field Is List
                    // invokeGetter doesn't retrieve the attribute from database,
                    // as it's done throgh reflection, need to load it manually
                    if (fieldIndex < (expressionFieldsCount - 1)) // List not-leaf field, it's unsupported
                    {
                        // <editor-fold defaultstate="collapsed" desc="Log">
//                        OLog.logError("Expression Inner List-Fields Not Supported Yet", loggedUser,
//                                "Main Object", entity,
//                                "Main Expression", expression,
//                                "Field Name", expressionFldsInfo.get(fieldIndex).fieldName);
                        // </editor-fold>
                        List data = (List) BaseEntity.getValueFromEntity(parentObject,
                                expField.getName());
                        if (null != data) {
                            postLoadEntity(data, null,
                                    Collections.singletonList(
                                            expression.substring(expression.indexOf(".") + 1)),
                                    loggedUser);
                        }
                        logger.trace("Returning");
                        return;
                    }
                    if (!(parentObject instanceof BaseEntity)) // List is not BaseEntity driven, no access supported yet
                    {
                        // <editor-fold defaultstate="collapsed" desc="Log">

                        if (entity != null && expressionFldsInfo != null && parentObject != null) {
                            logger.trace("Supported Objects Must Be Inherited From BaseEntity. Main Object: {}, Field Name: {}, Problem Object: {}", entity.getClassName(),
                                    expressionFldsInfo.get(fieldIndex).fieldName,
                                    parentObject);
                        }
                        if (entity == null) {
                            logger.debug("entity equals Null");
                        }

                        if (expressionFldsInfo == null) {
                            logger.debug("conditions equals Null");
                        }

                        if (parentObject == null) {
                            logger.debug("fieldExpressions equals Null");
                        }
                        // </editor-fold>
                        logger.trace("Returning");
                        return;
                    }
                    // Check the list is not already loaded (for performance-purpose)
                    // In case a child list field included more than once in the
                    // fieldExpressions
                    /**
                     * FIX ME: The following code loads all data, even the soft
                     * deleted ones, and that is why it is commented/ We need to
                     * get the value of the field without invoking the getter
                     * for performance and get undeleted records too.
                     */
                    Class ownerClass = parentObject.getClass();
                    String ownerTypeName = ownerClass.getSimpleName();
                    List<BaseEntity> baseEntities;
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    Long timeBeforeQuery = (Calendar.getInstance()).getTimeInMillis();
                    // </editor-fold>
                    String query = "";

                    if (conditions != null) {//case of tree screen
                        String cond = constructEntityWhereClause(conditions);
                        query = "SELECT entity "
                                + " FROM " + ownerTypeName + " e1"
                                + " JOIN e1." + expField.getName() + " entity" + cond
                                + " AND e1.dbid = " + ((BaseEntity) parentObject).getDbid()
                                + " AND entity.inActive = false";
                    } else {
                        query = "SELECT detail "
                                + " FROM " + ownerTypeName + " master"
                                + " JOIN master." + expField.getName() + " detail"
                                + " WHERE master.dbid = " + ((BaseEntity) parentObject).getDbid()
                                + " AND detail.inActive = false";
                    }

                    baseEntities = emw.createQuery(query, loggedUser).getResultList();
                    emw.detachList(baseEntities, loggedUser);
                    emw.detach(parentObject, loggedUser);
                    ((BaseEntity) parentObject).invokeSetter(expField.getName(),
                            baseEntities, List.class);
                    // Remove the field from forcedInitialized List if already there
                    // as we loaded it from databse, and not any more forced intialized
                    ((BaseEntity) parentObject).removeForcedInitializedField(expField.getName());
                    postLoadEntity(baseEntities, null, null, loggedUser);
                    emw.detachList(baseEntities, loggedUser);
                    emw.detach(parentObject, loggedUser);
                    if (baseEntities == null) {
                        logger.trace("Returning");
                        return;
                    }
                } else {
                    // Field is not list
                    // No need to load it, as it should be already loaded
                    // $$ OneToX Association Fetch is by default EAGER, take
                    // action if it's LAZY

                    // Set loop variables for next expression field
                    if (fieldIndex == (expressionFieldsCount - 1)
                            && !BaseEntity.getFieldType(expField).getName().contains("unitedofoq")) {
                        // Last field in expression
                        // No need for loop variables setting
                        // End the function
                        logger.trace("Returning");
                        return;
                    }
                    try {
                        Object fieldValue = ((BaseEntity) parentObject).invokeGetter(expField);
                        if (fieldValue != null) {
                            // Field Value Is Not Null
                            // Set the field to be a parent of the next field in
                            // the expression
                            parentObject = fieldValue;
                        } else {
                            return;
                        }
                        if (BaseEntity.getFieldType(expField).getName().contains("unitedofoq")) {
                            List<BaseEntity> baseEntitys = new ArrayList<BaseEntity>();
                            baseEntitys.add((BaseEntity) fieldValue);
                            postLoadEntity(baseEntitys, null, null, loggedUser);
                            emw.detachList(baseEntitys, loggedUser);
                            emw.detach(parentObject, loggedUser);
                        }
                    } catch (NoSuchMethodException ex) {
                        // No getter for the field, return
                        // <editor-fold defaultstate="collapsed" desc="Log">
                        if (expField != null) {
                            logger.warn("Field Has No Getter. Attribute Field Name", expField.getName());
                        } else {
                            logger.warn("expField equals Null");
                        }
                        // </editor-fold>
                        logger.trace("Returning");
                        return;
                    }
                }
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            if (entity != null) {
                logger.trace("Entity: {},Expression: {}, Expression Field: {}, parentObject: {}", entity.getClassName(),
                        "", expression, (expressionFldsInfo == null ? "" : expressionFldsInfo.get(fieldIndex)), parentObject);
            }
            // </editor-fold>
        }
        logger.trace("Returning");
    }

    /**
     * Loads specific fields into an already loaded entity <br> Forces reloading
     * already loaded list fields
     *
     * @param entity
     * @param conditions
     * @param fieldExpressions
     * @param loggedUser
     * @return The entity updated, don't use the passed entity as it may not be
     * updated
     * @throws com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException
     * @throws javax.persistence.EntityExistsException
     * @throws javax.persistence.EntityNotFoundException
     * @throws javax.persistence.NonUniqueResultException
     * @throws javax.persistence.NoResultException
     * @throws javax.persistence.OptimisticLockException
     * @throws javax.persistence.RollbackException
     * @throws javax.persistence.TransactionRequiredException
     */
    @Override
    public BaseEntity loadEntityDetail(
            BaseEntity entity, List<String> conditions,
            List<String> fieldExpressions, OUser loggedUser)
            throws UserNotAuthorizedException, EntityExistsException,
            EntityNotFoundException, NonUniqueResultException, NoResultException,
            OptimisticLockException, RollbackException, TransactionRequiredException {
        logger.trace("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        if (entity != null) {
            logger.trace("Loading Entity List. Name: {}", entity.getClassName());
        } else {
            logger.trace("entity equals Null");
        }
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>
        //this is for DataSecurity of tree screen
//        if (conditions != null) {
//            throw new UnsupportedOperationException("Conditions Not supported yet.");
//        }

        if (fieldExpressions != null && !fieldExpressions.isEmpty()) {
            for (String fieldExpression : fieldExpressions) {
                loadAttribute(entity, fieldExpression, true, true, conditions, loggedUser);
            }
        }
        if (entity != null) {
            logger.trace("Returning with: {}", entity.getClassName());
        } else {
            logger.trace("entity equals Null");
        }

        return textTranslationService.getEntityWithTowLevelsOfTranslatedFields(entity, loggedUser);
    }

    /**
     * This function performs the following on the passed baseEntities: <br>1.
     * Load Text Translations. <br>2. Load entity fields listed in
     * fieldExpressions. <br>3. Force Unloaded Lists Initialization. Calls
     * {@link forceListsIntialization} <br> and doesn't re-load already loaded
     * Lists <br> <br> Called by {@link #loadEntity(java.lang.String, java.util.List, java.util.List,
     * com.unitedofoq.fabs.core.security.user.OUser) loadEntity} after loading
     * the entity to do post load actions. <br> <br> Since the function doesn't
     * perform any security validations, then, it must be private and not public
     *
     * @param baseEntities Loaded baseEntities (one or more) to be handled <br>
     * @param conditions Same parameter of {@link #loadEntity(java.lang.String,
     *      java.util.List, java.util.List, com.unitedofoq.fabs.core.security.user.OUser)
     *      loadEntity(String, List<String>, List<String>, OUser)} <br>
     * @param fieldExpressions Same parameter of {@link #loadEntity(java.lang.String,
     *      java.util.List, java.util.List, com.unitedofoq.fabs.core.security.user.OUser)
     *      loadEntity(String, List<String>, List<String>, OUser)} <br>
     * @param loggedUser Same parameter of {@link #loadEntity(java.lang.String,
     *      java.util.List, java.util.List, com.unitedofoq.fabs.core.security.user.OUser)
     *      loadEntity(String, List<String>, List<String>, OUser)}
     * @return Success: passed entity list after handling <br> Empty entity list
     * in case of error, or baseEntityies is null or empty
     *
     * @throws com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException
     */
    private List<BaseEntity> postLoadEntity(
            List<BaseEntity> baseEntities, List<String> conditions,
            List<String> fieldExpressions, OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.trace("Entering");
        return postPersistMergeLoadEntity(baseEntities, conditions, fieldExpressions,
                false /*not saving*/, true, loggedUser);
    }

    private List<BaseEntity> postSaveEntity(
            List<BaseEntity> baseEntities, List<String> conditions, OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.trace("Entering");
        if (baseEntities == null) {
            logger.trace("Returning with new ArrayList");
            return new ArrayList();
        }
        if (baseEntities.isEmpty()) {
            logger.trace("Returning with new ArrayList");
            return new ArrayList();
        }
        if (!(baseEntities.get(0) instanceof BaseEntity)) // No manipulation for Entities that are not inherited from BaseEntity
        // return the passed list
        {
            logger.trace("Returning with baseEntities of size: {}", baseEntities);
            return baseEntities;
        }

        return postPersistMergeLoadEntity(baseEntities, conditions,
                baseEntities.get(0).fieldExpressions,
                true /*saving*/, false, loggedUser);
    }

    private List<BaseEntity> postPersistMergeLoadEntity(
            List<BaseEntity> baseEntities, List<String> conditions,
            List<String> fieldExpressions, Boolean saving, boolean loadTranslation, OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.trace("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>

        List updatedBaseEntities = new ArrayList();

        if (baseEntities == null) {
            return updatedBaseEntities;
        }
        if (baseEntities.isEmpty()) {
            return updatedBaseEntities;
        }
        if (!(baseEntities.get(0) instanceof BaseEntity)) // No manipulation for Entities that are not inherited from BaseEntity
        // return the passed list
        {
            if (baseEntities != null) {
                logger.trace("Returning with baseEntities of size: {}", baseEntities.size());
            } else {
                logger.trace("base entity");
            }

            return baseEntities;
        }

        // Used for performance tuning
        boolean bEntityNeedsTranslation = false;
        if (baseEntities.size() > 0) {
            bEntityNeedsTranslation = textTranslationService.entityNeedsTranslation(baseEntities.get(0), loggedUser);
        }

        for (BaseEntity baseEntity : baseEntities) {
            if (loadTranslation) {
                if (bEntityNeedsTranslation && baseEntity.getDbid() != 0) {
                    try {
                        baseEntity = textTranslationService.loadEntityTranslation(baseEntity, loggedUser);
                    } catch (TextTranslationException ex) {
                        // <editor-fold defaultstate="collapsed" desc="Log">
                        logger.error("Exception thrown", ex);
                        logger.trace("Entity: {}", baseEntity);
                        // </editor-fold>
                    }
                }
            }
            if (fieldExpressions != null && !fieldExpressions.isEmpty()) {
                baseEntity.fieldExpressions = fieldExpressions;
                for (String fieldExpression : fieldExpressions) {
                    try {
                        // For better performance, if expression is direct attribute in the owner
                        // and is already loaded (e.g. using JOIN FETCH in loadEntity) then no need to
                        // use loadAttribute (reload it again)
                        if (!fieldExpression.contains(".")) {
                            // Is direct attribute
                            try {
                                Object attrValue = baseEntity.invokeGetter(fieldExpression);
                                if (attrValue != null) {
                                    if (!attrValue.getClass().getSimpleName().equals("IndirectList")) // Not Null nor IndirectList
                                    // TO DO $$ Review                                        if (attrValue.getClass().getSimpleName().equals("ArrayList") &&
                                    //                                            !baseEntity.isFieldIntializationForced(fieldExpression))
                                    // Is ArrayList & intitialization not forced
                                    {
                                        continue;
                                    }
                                }
                            } catch (NoSuchMethodException ex) {
                                // No getter for the attribute, strange, shouldn't happen
                                // <editor-fold defaultstate="collapsed" desc="Log">
                                logger.error("Exception thrown", ex);
                                logger.trace("BaseEntity: {} ,Field Expression: {}", baseEntity, fieldExpression);
                                // </editor-fold>
                            }
                        }
                        // Field is either an entity direct-field, or has "." but
                        // is null or IndirectList
                        if (saving) {
                            loadAttribute(baseEntity, fieldExpression, false,
                                    false /*Don't force init if null, as this will save
                                     the null child that because it's forced intialized*/,
                                    null, loggedUser);
                        } else {
                            loadAttribute(baseEntity, fieldExpression, false, true, null, loggedUser);
                        }
                    } catch (Exception ex) {
                        // <editor-fold defaultstate="collapsed" desc="Log">
                        logger.error("Exception thrown", ex);
                        if (baseEntity != null) {
                            logger.trace("BaseEntity: {}", baseEntity.getClassName());
                        } else {
                            logger.trace("base entity equals Null");
                        }
                        // </editor-fold>
                    }
                }
            }
            // ((BaseEntity) baseEntity).forceEmptyListsIntialization() ; not needed
            // as the loadAttribute takes care of initializing the fields used in expressions

            updatedBaseEntities.add(baseEntity);
        }

        if (updatedBaseEntities != null) {
            logger.trace("Returning with updatedBaseEntities of size: {}", updatedBaseEntities.size());
        } else {
            logger.trace("updatedBaseEntities equals Null");
        }
        return updatedBaseEntities;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Load Entity List">

    @Override
    public List loadEntityList(String entityName, List<String> conditions,
            List<String> fieldExpressions, List<String> sortExpressions, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException {

        logger.debug("Entering");
        if (conditions == null) {
            conditions = new ArrayList();
        }
        // <editor-fold defaultstate="collapsed" desc="Log">
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        if (entityName != null && conditions != null && fieldExpressions != null) {
            logger.debug("Loading Entity List. Name: {}, Conditions size: {}, Expressions size:{}", entityName, conditions.size(), fieldExpressions.size());
        }
        if (entityName == null) {
            logger.debug("entity equals Null");
        }

        if (conditions == null) {
            logger.debug("conditions equals Null");
        }

        if (fieldExpressions == null) {
            logger.debug("fieldExpressions equals Null");
        }

        // </editor-fold>
        if (!entityName.contains("Translation")) {
            securityService.checkActionEntityAuthority(entityName,
                    OEntityAction.ATDBID_RETRIEVE, loggedUser, loggedUser);
        }
        // No exception thrown, he is authorized

        // <editor-fold defaultstate="collapsed" desc="Log">
        Long timeBeforeQuery = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>

        List<String> loadCond = conditions;
        String whereClause = constructEntityWhereClause(loadCond);
        String orderByClause = constructEntityOrderByClause(sortExpressions);

        String query;// DISTINCT
        query = "SELECT DISTINCT entity FROM " + entityName + " entity "
                + whereClause
                + orderByClause;
        logger.debug("Query: {}", query);
        List baseEntities = null;
        try {
            baseEntities = emw.createQuery(query, loggedUser).getResultList();
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning with new ArrayList");
            return new ArrayList();
        }

        // MT: This line will detach the result set from the PersistenceContext
        // because in later usage, the result set is manually modified on the
        // the fly, and that causes synchronization exceptions
        emw.detachList(baseEntities, loggedUser);
        if (baseEntities == null) {
            logger.debug("Returning with new ArrayList");
            return new ArrayList();
        }

        baseEntities = postLoadEntity(baseEntities, conditions, fieldExpressions, loggedUser);
        if (!baseEntities.isEmpty()
                && !((BaseEntity) baseEntities.get(0)).getClass().
                getName().contains("com.unitedofoq.fabs")) {
            decrypteEntityList(baseEntities, loggedUser);

        }
        if (baseEntities != null) {
            logger.debug("Returning with baseEntities of size: {}", baseEntities.size());
        } else {
            logger.debug("Base entity equals Null");
        }
        return baseEntities;
    }

    @Override
    //@Interceptors({SensitiveDataFieldEntityListener.class})
    public List loadEntityList(String entityName, List<String> conditions,
            List<String> fieldExpressions, List<String> sortExpressions,
            int startIndex, int maxResult, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException {

        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        if (entityName != null && conditions != null && fieldExpressions != null) {
            logger.debug("Loading Entity List. Name: {}, Conditions size: {}, Expressions size:{}", entityName, conditions.size(), fieldExpressions.size());
        }
        if (entityName == null) {
            logger.debug("entity equals Null");
        }

        if (conditions == null) {
            logger.debug("conditions equals Null");
        }

        if (fieldExpressions == null) {
            logger.debug("fieldExpressions equals Null");
        }
        // </editor-fold>
        if (!entityName.contains("Translation")) {
            securityService.checkActionEntityAuthority(entityName,
                    OEntityAction.ATDBID_RETRIEVE, loggedUser, loggedUser);
        }
        // No exception thrown, he is authorized

        // <editor-fold defaultstate="collapsed" desc="Log">
        Long timeBeforeQuery = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>

        List<String> loadCond = conditions;
        String whereClause = constructEntityWhereClause(loadCond);
//        whereClause += " OR ((entity.parentMenu.name IS NULL) AND (entity.inActive = false)) ";
        int count = StringUtils.countMatches(sortExpressions.get(0), ".");
        if (count > 1) {
            nestedSortExpression = true;
        }
        String query;// DISTINCT
        String joinQueryForSorting = constructNativeQueryForSorting(sortExpressions);

        if (nestedSortExpression) {
            String expression = sortExpressions.get(0);
            expression = "lower(" + expression.substring(expression.indexOf(".") + 1, expression.length());
            sortExpressions.set(0, expression);
        }
        String orderByClause = constructEntityOrderByClause(sortExpressions);
        if (!orderByClause.equals("") && !orderByClause.toLowerCase().contains("dbid")) {
            orderByClause += ", entity.dbid asc";
        }
        if (joinQueryForSorting != null) {
            query = "SELECT DISTINCT entity FROM " + entityName + " entity " + joinQueryForSorting
                    + whereClause
                    + orderByClause;
        } else {
            query = "SELECT DISTINCT entity FROM " + entityName + " entity "
                    + whereClause
                    + orderByClause;
        }
        List baseEntities = null;
        logger.debug("Query: {}", query);
        // incase we need to load all the entities
        if (startIndex == -1 || maxResult == -1) {
            baseEntities = emw.createQuery(query, loggedUser).getResultList();
        } else {
            baseEntities = emw.createQuery(query, loggedUser).setFirstResult(startIndex).setMaxResults(maxResult).getResultList();
        }
        emw.detachList(baseEntities, loggedUser);
        if (baseEntities == null) {
            logger.debug("Returning with new ArrayList");
            return new ArrayList();
        }
        baseEntities = postLoadEntity(baseEntities, conditions, fieldExpressions, loggedUser);
        decrypteEntityList(baseEntities, loggedUser);

        // <editor-fold defaultstate="collapsed" desc="Log">
        if (baseEntities != null) {
            logger.debug("Returning with baseEntities of size: {}", baseEntities.size());
        } else {
            logger.debug("base entity equals Null");
        }
        return baseEntities;
    }

    @Override
    //@Interceptors({SensitiveDataFieldEntityListener.class})
    public List loadEntityList(String entityName, List<Long> dbids, List<String> conditions,
            List<String> fieldExpressions, List<String> sortExpressions, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">fieldExpressions
        conditions.add("inActive = false");
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        logger.debug("Loading Entity List, 1 SQL. Name: {}, DBIDs: {}", entityName, dbids);
        logger.debug("Conditions: {}, Expression: {}", conditions, fieldExpressions);
        // </editor-fold>

        if (dbids.isEmpty()) {
            logger.debug("Returning with: new Arraylist");
            return new ArrayList();
        }

        String orderByClause = constructEntityOrderByClause(sortExpressions);
        //FIXME: need to construct the where clause in standard way, like other functions
        String query = "SELECT entity FROM "
                + entityName
                + " entity WHERE entity.dbid IN (";
        for (int dbidindex = 0; dbidindex < dbids.size(); dbidindex++) {
            query += dbids.get(dbidindex);
            if (dbidindex < (dbids.size() - 1)) {
                query += ", ";
            }
        }
        query += ")";
        query += orderByClause;

        // <editor-fold defaultstate="collapsed" desc="Log">
        Long timeBeforeQuery = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>
        logger.debug("Query: {}", query);
        List baseEntities = null;

        baseEntities = emw.createQuery(query, loggedUser).getResultList();
        emw.detachList(baseEntities, loggedUser);
        baseEntities = postLoadEntity(baseEntities, conditions, fieldExpressions, loggedUser);
        decrypteEntityList(baseEntities, loggedUser);
        if (baseEntities != null) {
            logger.debug("Returning with baseEntities of size: {}", baseEntities.size());
        } else {
            logger.debug("base entity equals Null");
        }
        return baseEntities;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Change Activation Status of Entity">

    /**
     * Private function for entity deletion, don't make it public otherwise
     * there will be a security hole <br>It doesn't call the OEntityAction, and
     * has nothing to do with it
     *
     * @param entity
     * @param loggedUser
     * @param checkSecurity
     * @throws com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException
     * @throws javax.persistence.EntityExistsException
     * @throws javax.persistence.EntityNotFoundException
     * @throws javax.persistence.NonUniqueResultException
     * @throws javax.persistence.NoResultException
     * @throws javax.persistence.OptimisticLockException
     * @throws javax.persistence.RollbackException
     * @throws javax.persistence.TransactionRequiredException
     */
//    @Interceptors(AuditTrailManager.class)
    private void activateDeactivateEntityInternal(BaseEntity entity, OUser loggedUser, boolean checkSecurity)
            throws Exception, UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException, FABSException {
        logger.trace("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.trace("Deleting Entity. Entity: {}", entity);
        // </editor-fold>

        // Check security
        if (checkSecurity) {
            securityService.checkActionEntityAuthority(entity.getClass().getSimpleName(),
                    OEntityAction.ATDBID_ACTIVATION, loggedUser, loggedUser);
        }
        // No exception thrown, he is authorized

        // <editor-fold defaultstate="collapsed" desc="Log">
        Long timeBeforeQuery = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>

        // load saved value
        BaseEntity savedEntity = loadEntity(entity.getClassName(), entity.getDbid(), null, null, loggedUser);
        // if true
        if (savedEntity.isInActive()) {
            entity.setInActive(false);
        } else {
            entity.setInActive(true);
        }

        try {
            localEntityManager.saveEntityInternal(entity, loggedUser, false);
        } catch (Exception ex) {
            if (ex instanceof FABSException) {
                throw new FABSException(loggedUser, ex);
            }
            logger.error("Exception thrown", ex);
        }
        logger.trace("Returning");
    }

    @Override
    public void activateDeactivateEntityList(List<BaseEntity> entities, OUser loggedUser)
            throws Exception, UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException, FABSException {
        logger.debug("Entering");
        securityService.checkActionEntityAuthority(entities.get(0).getClass().getSimpleName(),
                OEntityAction.ATDBID_ACTIVATION, loggedUser, loggedUser);
        // No exception thrown, he is authorized

        for (BaseEntity baseEntity : entities) // Security is checked internally, enhancement required to check it once
        {
            activateDeactivateEntityInternal(baseEntity, loggedUser, false);
        }
        logger.debug("Returning");
    }

    /**
     * Set the entity deleted = true, and save it
     *
     * @param entity
     * @param loggedUser
     * @return
     */
    @Override
    @Interceptors(AuditTrailManager.class)
    public void activateDeactivateEntity(BaseEntity entity, OUser loggedUser)
            throws UserNotAuthorizedException, FABSException {
        logger.debug("Entering");
        //FIXME: save the deletion value to be rolled back in case save failed (e.g
        // for security reasons)
        try {
            activateDeactivateEntityInternal(entity, loggedUser, true);
        } catch (Exception ex) {
            if (ex instanceof UserNotAuthorizedException) {
                throw new UserNotAuthorizedException((UserNotAuthorizedException) ex);
            } else if (ex instanceof FABSException) {
                throw new FABSException((FABSException) ex);
            } else {
                throw new FABSException(loggedUser, null, entity, ex);
            }
        }
        logger.debug("Returning");
    }

    /**
     * Calls {@link #deleteEntity(BaseEntity, OUser, boolean)} with
     * checkSecurity = true.
     */
    @Override
    @Interceptors(AuditTrailManager.class)
    public void deleteEntity(BaseEntity entity, OUser loggedUser)
            throws UserNotAuthorizedException, FABSException {
        logger.debug("Entering");
        deleteEntityInternal(entity, loggedUser, true /*Check Security*/);
        logger.debug("Returning");
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void deleteCorruptedData(BaseEntity entity, OUser loggedUser) {
        logger.debug("Entering");
        try {
            String query = "DELETE FROM " + entity.getClass().getSimpleName()
                    + " entity WHERE entity.dbid = " + entity.getDbid();
            logger.trace("Query: {}", query);
            emw.createQuery(query, loggedUser).executeUpdate();
            textTranslationService.removeEntityTranslation(entity, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.trace("Returning");
    }

    /**
     * Permanently removes the entity record (without it's children/composed
     * entities) from database. <br>Logs the transaction in EntityAuditTrail as
     * {@link EntityAuditTrail.ActionType#TERMINATE} <br>Removes the related
     * text translations as well <br>It doesn't call the OEntityAction, and has
     * nothing to do with it. <br>Don't make it public as it will cause security
     * hole because of 'checkSecurity'
     *
     * @param entity
     * @param loggedUser
     * @param checkSecurity If true then loggedUser authority on both Entity &
     * HardDeletion Action will be validated, if false, nothing will be
     * validated
     * @throws com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException
     * @throws com.unitedofoq.fabs.core.validation.FABSException
     */
//    @Interceptors(AuditTrailManager.class)
    private void deleteEntityInternal(BaseEntity entity, OUser loggedUser, boolean checkSecurity)
            throws UserNotAuthorizedException, FABSException {
        logger.trace("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        if (entity != null) {
            logger.trace("Deleting Entity. Entity: {}", entity.getClassName());
        } else {
            logger.trace("entity equals Null");
        }
        // </editor-fold>

        // Check security
        if (checkSecurity) {
            securityService.checkActionEntityAuthority(entity.getClass().getSimpleName(),
                    OEntityAction.ATDBID_DELETE, loggedUser, loggedUser);
        }
        // No exception thrown, he is authorized

        // <editor-fold defaultstate="collapsed" desc="Log">
        Long timeBeforeQuery = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>

        try {
            String query = "DELETE FROM " + entity.getClass().getSimpleName()
                    + " entity WHERE entity.dbid = " + entity.getDbid();
            // <editor-fold defaultstate="collapsed" desc="Delete">
            // Never use em.remove(em.merge(entity)); as it has a lot of problems
            logger.trace("Query: {}", query);
            emw.createQuery(query, loggedUser).executeUpdate();
            // </editor-fold>

            textTranslationService.removeEntityTranslation(entity, loggedUser);
//
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            throw new FABSException(loggedUser, ex);
        }
        logger.trace("Returning");
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Save Entity & List">

    @Override
    @Interceptors(SensitiveDataFieldEntityListener.class)
    public List<BaseEntity> saveBatchForImport(List<BaseEntity> entities, OUser loggedUser) throws FABSException {
        logger.debug("Entering");
        for (BaseEntity entity : entities) {
            try {

                entity = restoreForcedIntializedFieldsValues(entity, loggedUser);
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown", ex);
                logger.debug("Entity", entity);
                // </editor-fold>
            }

            // <editor-fold defaultstate="collapsed" desc="DBID Generation">
            try {
                entity = dbidService.createNewDbid(entity, loggedUser);
            } catch (Exception ex) {
                if (ex instanceof FABSException) {
                    throw new FABSException(loggedUser, ex);
                }
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown", ex);
                logger.debug("Entity", entity);
                // </editor-fold>
            }
            // </editor-fold>

            entity = textTranslationService.preEntityTranslationSave(entity, loggedUser);
        }
        for (int i = 0; i < entities.size(); i++) {
            emw.persist(entities.get(i), loggedUser);
            //if(i%20 == 0){
            //}
        }

        emw.flush(loggedUser);
        emw.clear(loggedUser);
        for (int i = 0; i < entities.size(); i++) {
            try {
                textTranslationService.saveEntityTranslation(entities.get(i), loggedUser);
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown", ex);
                if (entities != null) {
                    logger.debug("Entity", entities.get(i));
                } else {
                    logger.debug("entity equals Null");
                }
                // </editor-fold>
            }
            if (i % 20 == 0) {
                emw.flush(loggedUser);
                emw.clear(loggedUser);
            }
        }
        if (entities != null) {
            logger.debug("Returning with lists of size: {}", entities.size());
        } else {
            logger.debug("entities equals Null");
        }
        return entities;
    }

    // <editor-fold defaultstate="collapsed" desc="Save Entity & List">
    /**
     * Private function for entity saving, don't make it public otherwise there
     * will be a security hole. <br> It saves both updated (merge) and new
     * (persist) entities <br> It performs the following: <br> 1. Data Native
     * Validation. Calls {@link BaseEntity#dataNativeValidation()}
     *
     * @param entity
     * @param loggedUser
     * @param checkSecurity
     * @return The Merged Entity, note that some relations may be refreshed
     * during merge
     * @throws com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException
     * @throws javax.persistence.EntityExistsException
     * @throws javax.persistence.EntityNotFoundException
     * @throws javax.persistence.NonUniqueResultException
     * @throws javax.persistence.NoResultException
     * @throws javax.persistence.OptimisticLockException
     * @throws javax.persistence.RollbackException
     * @throws javax.persistence.TransactionRequiredException
     */
//    @Interceptors(AuditTrailManager.class)
    @Override
    @Interceptors({AuditTrailManager.class, SensitiveDataFieldEntityListener.class})
    public BaseEntity saveEntityInternal(BaseEntity entity, OUser loggedUser, boolean batch)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException,
            OEntityDataValidationException, EntityDataValidationException, FABSException {
        logger.trace("Entering");

        try {
            if (entity != null) {
                logger.trace("Saving Entity. Entity: {}", entity.getClassName());
            } else {
                logger.trace("entity equals Null");
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }

        if (!batch) {
            checkSecurity(entity, loggedUser);
        }
        entity.dataNativeValidation(loggedUser);
        if (entity.getDbid() > 0) {
            try {
                entity = restoreForcedIntializedFieldsValues(entity, loggedUser);
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown", ex);
                logger.trace("Entity", entity);
                // </editor-fold>
            }

            try {
                textTranslationService.updateEntityTranslation(entity, loggedUser);
                entity = textTranslationService.loadEntityTranslation(entity, loggedUser);
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown", ex);
                logger.trace("Entity", entity);
                // </editor-fold>
            }

            if (0 != loggedUser.getDbid()
                    && 0 == loggedUser.getWorkingTenantID()) {
                entity.setLastMaintUser(loggedUser);
            }
            entity.setLastMaintDTime(new Date());
            emw.merge(entity, loggedUser);
        } else {
            try {
                entity = restoreForcedIntializedFieldsValues(entity, loggedUser);
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown", ex);
                logger.trace("Entity", entity);
                // </editor-fold>
            }
            // <editor-fold defaultstate="collapsed" desc="DBID Generation">

            try {
//                if (!batch) {
//                    entity = dbidGenerationService.createNewDbid(entity, loggedUser);
                entity = dbidService.createNewDbid(entity, loggedUser);
//                }
            } catch (Exception ex) {
                if (ex instanceof FABSException) {
                    throw new FABSException(loggedUser, ex);
                }
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown", ex);
                logger.trace("Entity", entity);
                // </editor-fold>
            }
            emw.persist(entity, loggedUser);
            // </editor-fold>
            entity = textTranslationService.preEntityTranslationSave(entity, loggedUser);

            if (0 != loggedUser.getDbid()
                    && 0 == loggedUser.getWorkingTenantID()) {
                entity.setLastMaintUser(loggedUser);
            }
            entity.setLastMaintDTime(new Date());
            try {
                textTranslationService.saveEntityTranslation(entity, loggedUser);
                entity = textTranslationService.loadEntityTranslation(entity, loggedUser);
            } catch (Exception ex) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown", ex);
                logger.trace("Entity", entity);
                // </editor-fold>
            }
        }
        if (!batch) {
            emw.flush(loggedUser);
            emw.detach(entity, loggedUser);
        }

        logger.trace("Returning");
        return entity;
    }

    private void checkSecurity(BaseEntity entity, OUser loggedUser) throws UserNotAuthorizedException {
        // Check Authority
        Long actionTypeDBID;
        if (entity.getDbid() > 0) // Update persisted entity
        {
            actionTypeDBID = OEntityAction.ATDBID_UPDATE;
        } else // Add new entity
        {
            actionTypeDBID = OEntityAction.ATDBID_CREATE;
        }
        securityService.checkActionEntityAuthority(entity.getClass().getSimpleName(),
                actionTypeDBID, loggedUser, loggedUser);
        // No exception thrown, he is authorized
    }

    @Override
    public BaseEntity saveEntity(BaseEntity entity, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException,
            OEntityDataValidationException, EntityDataValidationException, FABSException {
        logger.debug("Entering");

        //Removed in R18.1.9  as it's handeled in the front end  - Islam
        // cleanEntity(entity, loggedUser);
        entity = localEntityManager.saveEntityInternal(entity, loggedUser, false);
        List<BaseEntity> savedList = new ArrayList<BaseEntity>();
        savedList.add(entity);
        if (entity != null) {
            savedList = postSaveEntity(savedList, null, loggedUser);
            decrypteEntity(entity, loggedUser);
            if (savedList != null && savedList.size() != 0) {
                logger.debug("Returning with: {}", savedList.get(0));
            } else {
                logger.debug("saved list equals Null");
            }
        }
        return savedList.get(0);
    }

    @Override
    public List<BaseEntity> saveEntityList(List<BaseEntity> entities, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException,
            OEntityDataValidationException, EntityDataValidationException, FABSException {
        logger.debug("Entering");
        checkEntityListSecurity(entities, loggedUser);
        List<BaseEntity> savedList = new ArrayList<BaseEntity>();
        Map<String, Long> tableDbidMap = new HashMap();
        for (BaseEntity baseEntity : entities) {
//            boolean newEntity = baseEntity.getDbid() == 0;
            if (savedList.size() % JDBC_MAX_BATCH_SIZE == 0) {
                emw.flush(loggedUser);
                emw.detachList(entities, loggedUser);
            }
            savedList.add(localEntityManager.saveEntityInternal(baseEntity, loggedUser, true));
//            dbidGenerationService.assignDbidForBulk(baseEntity, tableDbidMap, newEntity, loggedUser);
        }
        emw.flush(loggedUser);
        emw.detachList(entities, loggedUser);
        tableDbidMap.clear();
        savedList = postSaveEntity(savedList, null, loggedUser);
        if (savedList != null) {
            logger.debug("Returning with list of size: {}", savedList.size());
        } else {
            logger.debug("saved list equals Null");
        }
        return savedList;
    }
    // </editor-fold>

    private BaseEntity restoreForcedIntializedFieldsValues(BaseEntity masterObject, OUser loggedUser)
            throws Exception {
        logger.trace("Entering");

        List<String> forcedInitializedListNames = masterObject.getForcedInitializedFields();
        if (forcedInitializedListNames == null) {
            if (masterObject != null) {
                logger.trace("Returning with: {}", masterObject.getClassName());
            }
            return masterObject;
        }

        BaseEntity fieldsOriginals = masterObject.getForcedFieldsOriginal();

        for (String detailedObjectExpression : forcedInitializedListNames) {
            Field detailedObjectField
                    = BaseEntity.getClassField(masterObject.getClass(),
                            detailedObjectExpression);
            if (detailedObjectField.getAnnotation(OneToMany.class) != null
                    || detailedObjectField.getAnnotation(ManyToMany.class) != null) {
                Object originalList = fieldsOriginals.invokeGetter(detailedObjectField);
                List objList = (List) masterObject.invokeGetter(detailedObjectField);
                if (objList != null) {
                    if (objList.isEmpty()) {
                        // Validate the case of cascaded type found, where user
                        // may need to reset the list to empty and save this
                        OneToOne one2OneAnnot = detailedObjectField.getAnnotation(OneToOne.class);
                        if (one2OneAnnot != null) {
                            if (one2OneAnnot.cascade() != null) {
                                // Case not supported
                                // FIXME: support this case
                                // <editor-fold defaultstate="collapsed" desc="Log">
                                logger.warn("Case Not Supported. Field: {}", detailedObjectField);
                                // </editor-fold>
                            }
                        }
                        ManyToOne many2OneAnnot = detailedObjectField.getAnnotation(ManyToOne.class);
                        if (many2OneAnnot != null) {
                            if (many2OneAnnot.cascade() != null) {
                                // Case not supported
                                // FIXME: support this case
                                // <editor-fold defaultstate="collapsed" desc="Log">
                                logger.warn("Case Not Supported. Field: {}", detailedObjectField);
                                // </editor-fold>
                            }
                        }
                        // User mostly didn't change the value (doesn't need to change it)
                        // Restore the list to the original value (which could be indirectlist)
                        masterObject.invokeSetter(detailedObjectField, originalList);
                    }
                } else {
                    // User/Code changed it to null
                    // Leave it as is, don't restore value, as when we force initialization
                    // we set it to new object and not null
                }
                //TODO $$ should we remove the field from the forced list?
            } else // ManyToOne or OneToOne
            {
                // Always test for PersonalInfo
                Object obj = masterObject.invokeGetter(detailedObjectField);
                // Check if user didn't change the value
                if (obj != null) {
                    if (obj.getClass().getName().contains("unitedofoq")
                            && ((BaseEntity) obj).getDbid() <= 0) {
                        BaseEntity objAsBaseEntity = (BaseEntity) obj;
                        if (!objAsBaseEntity.isChanged()) {
                            // User didn't change the value, restore the value to null
                            Object input = null;
                            masterObject.invokeSetter(detailedObjectField, input);
                        }
                    }
                }
            }
        }
        if (masterObject != null) {
            logger.trace("Returning with: {}", masterObject.getClassName());
        } else {
            logger.trace("maseter object equals Null");
        }
        return masterObject;
    }

    @Override
    public void executeUpdateQuery(String query, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException {
        logger.debug("Entering");
        //TODO: we need to prase the query for security check

        // <editor-fold defaultstate="collapsed" desc="Log">
        Long timeBeforeQuery = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>

        emw.createQuery(query, loggedUser).executeUpdate();
        // MT: This line will detach the result set from the PersistenceContext
        // because in later usage, the result set is manually modified on the
        // the fly, and that causes synchronization exceptions
//        emw.clear(loggedUser);
        logger.debug("Returning");
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object executeEntityQuery(String query, OUser loggedUser)
            throws UserNotAuthorizedException, EntityExistsException,
            EntityNotFoundException, NonUniqueResultException, NoResultException,
            OptimisticLockException, RollbackException, TransactionRequiredException {
        logger.debug("Entering");

        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.debug("Executing Query. Query: {}", query);
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        Long timeBeforeQuery = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>

        List baseEntities = emw.createQuery(query, loggedUser).getResultList();
        emw.detachList(baseEntities, loggedUser);
        if (baseEntities.size() == 1) // One Entity returned as expected
        {
            if (baseEntities.get(0) instanceof BaseEntity) {
                // Return is one of our entities
                // Check security
                securityService.checkActionEntityAuthority(
                        baseEntities.get(0).getClass().getSimpleName(),
                        OEntityAction.ATDBID_RETRIEVE, loggedUser, loggedUser);
                // No exception thrown, he is authorized

                baseEntities = postLoadEntity(baseEntities, null, null, loggedUser);
            }
        }

        if (baseEntities.size() > 1) // More than one record returned
        {
//            throw new NonUniqueResultException("NonUniqueResultException");
            logger.warn("None Unique Result Was Returned. Query: {}", query);
        }

        // <editor-fold defaultstate="collapsed" desc="Log">
        // </editor-fold>
        if (baseEntities.isEmpty()) // Nothing returned
        {
            logger.debug("Returning with: Null");
            return null;
        }
        if (baseEntities != null && baseEntities.size() != 0) {
            logger.debug("Returning with: {}", baseEntities.get(0));
        }
        return baseEntities.get(0);
    }

    @Override
    public List executeEntityListQuery(String query, OUser loggedUser)
            throws UserNotAuthorizedException, EntityExistsException,
            EntityNotFoundException, NonUniqueResultException, NoResultException,
            OptimisticLockException, RollbackException, TransactionRequiredException {
        logger.trace("Entering");

        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.trace("Executing Query. Query: {}", query);
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        Long timeBeforeQuery = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>

        List baseEntities = emw.createQuery(query, loggedUser).getResultList();
        emw.detachList(baseEntities, loggedUser);
        if (!baseEntities.isEmpty()) {
            if (baseEntities.get(0) instanceof BaseEntity) // Return is one of our entities
            {
                securityService.checkActionEntityAuthority(
                        baseEntities.get(0).getClass().getSimpleName(),
                        OEntityAction.ATDBID_RETRIEVE, loggedUser, loggedUser);
                // No exception thrown, he is authorized

                baseEntities = postLoadEntity(baseEntities, null, null, loggedUser);
            }
        }
        if (baseEntities != null) {
            logger.trace("Returning with list of size: {}", baseEntities.size());
        } else {
            logger.trace("base entity equals Null");
        }
        return baseEntities;
    }

    public void refresh(BaseEntity entity, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException {
        logger.debug("Entering");
        securityService.checkActionEntityAuthority(entity.getClass().getSimpleName(),
                OEntityAction.ATDBID_UPDATE, loggedUser, loggedUser);
        // No exception thrown, he is authorized
        emw.refresh(entity, loggedUser);
        logger.debug("Returning");
    }

    /**
     * Return Granted system user with tenant, lanugage, and onBehalfOf set.
     * Return the loggedUser if it's system user Calls
     * {@link OCentralEntityManagerRemote#getGrantedUser(com.unitedofoq.fabs.central.OTenant)}
     *
     * @param loggedUser Logged User for which a granted system user is
     * required. <br>Must not be null
     * @return Successful: Granted system user <br>null: error
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public OUser getSystemUser(OUser loggedUser) {
        logger.debug("Entering");
        //return em.find(OUser.class, Long.parseLong("1")); //NON Multi-Tenant Compatible
        if (loggedUser == null) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Null Logged User Passed");
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        }
        if (centralOEM.isSystemUser(loggedUser)) {
            logger.debug("Returning with: {}", loggedUser.getLoginName());
            return loggedUser;
        }
        OUser grantedUser = centralOEM.getGrantedUser(loggedUser.getTenant());
        // Set the granted user language same like the loggedUser data retrieved
        // is properly translated
        grantedUser.setLoginName(loggedUser.getLoginName());
        grantedUser.setFirstLanguage(loggedUser.getFirstLanguage());
        grantedUser.setSecondLanguage(loggedUser.getSecondLanguage());
        grantedUser.setOnBehalfOf(loggedUser);
        grantedUser.setWorkingTenantID(loggedUser.getWorkingTenantID());
        grantedUser.setEmail(loggedUser.getEmail());
        if (grantedUser != null) {
            logger.debug("Returning with: {}", grantedUser.getLoginName());
        } else {
            logger.debug("granted user equals Null");
        }
        return grantedUser;
    }

    @Override
    public List executeEntityListNativeQuery(String query, OUser loggedUser) {
        logger.debug("Entering");
        logger.warn("Query: {}", query);
        List returnedList = emw.createNativeQuery(query, loggedUser).getResultList();
        // MT: This line will detach the result set from the PersistenceContext
        // because in later usage, the result set is manually modified on the
        // the fly, and that causes synchronization exceptions
//        emw.detachList(returnedList, loggedUser);
        if (returnedList != null) {
            logger.debug("Returning with list of size: {}", returnedList.size());
        } else {
            logger.debug("returned list equals Null");
        }
        return returnedList;
    }

    @Override
    public Object executeEntityNativeQuery(String query, OUser loggedUser)
            throws UserNotAuthorizedException, EntityExistsException,
            EntityNotFoundException, NonUniqueResultException, NoResultException,
            OptimisticLockException, RollbackException, TransactionRequiredException {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.debug("Executing Query. Query: {}", query);
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        Long timeBeforeQuery = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>
        List baseEntities = emw.createNativeQuery(query, loggedUser).getResultList();

        if (baseEntities.size() == 1) // One Entity returned as expected
        {
            if (baseEntities.get(0) instanceof BaseEntity) {
                // Return is one of our entities
                // Check security
                securityService.checkActionEntityAuthority(
                        baseEntities.get(0).getClass().getSimpleName(),
                        OEntityAction.ATDBID_RETRIEVE, loggedUser, loggedUser);
                // No exception thrown, he is authorized

                baseEntities = postLoadEntity(baseEntities, null, null, loggedUser);
            }
        }

        if (baseEntities.size() > 1) // More than one record returned
        {
//            throw new NonUniqueResultException("NonUniqueResultException");
            logger.warn("Non Unique Result Was Returned. Query:{}", query);
        }

        if (baseEntities.isEmpty()) // Nothing returned
        {
            logger.debug("Returning with Null");
            return null;
        }
        if (baseEntities != null && baseEntities.size() != 0) {
            logger.debug("Returning with: {}", baseEntities.get(0));
        }
        return baseEntities.get(0);
    }

    @Override
    public boolean executeEntityUpdateNativeQuery(String query, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException {
        logger.debug("Entering");
        int execute = emw.createNativeQuery(query, loggedUser).executeUpdate();
        logger.debug("execute: {}", execute);
        logger.debug("Returning with {]}", (execute != 0));
        return execute != 0;
    }

    public Field getEntityChildField(BaseEntity parentEnitty, Class childEntityClass) {
        logger.debug("Entering");
        logger.warn("Function Not Supported");
        logger.debug("Returning with Null");
        return null;
    }

    @Override
    public OEntity getOEntity(String entityName, OUser loggedUser) {
        logger.debug("Entering");
        try {
            String query = "SELECT oe FROM OEntity oe "
                    + " WHERE oe.entityClassPath LIKE '%." + entityName + "'";
            logger.debug("Query: {}", query);
            List<OEntity> oEntity = (List<OEntity>) emw.createQuery(query, loggedUser).getResultList();
            emw.detachList(oEntity, loggedUser);
            if (oEntity.size() > 1) // More than one record returned
            {
                logger.warn("None Unique Result Was Returned. Query: {}", query);
            }

            if (oEntity.isEmpty()) // Nothing returned
            {
                logger.debug("Returning with Null");
                return null;
            }
            if (oEntity != null && oEntity.size() != 0) {
                logger.debug("Returning with: {}", oEntity.get(0));
            }
            return oEntity.get(0);
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.debug("Entity Name: {}", entityName);
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        }
    }

    @Override
    public Long createNumberNamedQuery(
            String namedQueryName, OUser loggedUser, Object... params) {
        logger.debug("Entering");
        try {
            Query query = createNamedQuery(namedQueryName, loggedUser, params);

            // <editor-fold defaultstate="collapsed" desc="Log">
            Long timeBeforeQuery = (Calendar.getInstance()).getTimeInMillis();
            // </editor-fold>

            List<Long> result = (List<Long>) query.getResultList();
            if (result.size() > 1) // More than one record returned
            {
                logger.warn("None Unique Result Was Returned. Query: {}", query);
            }
            if (result != null || result.size() != 0) {
                logger.debug("Returning with: {}", result.get(0));
            }
            return result.get(0);
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.debug("Query Name: {} Parameters: {}", namedQueryName, params);
            // </editor-fold>
            logger.debug("Returning with: Null");
            return null;
        }
    }

    @Override
    public Long createNumberQuery(String query, OUser loggedUser) {
        logger.debug("Entering");
        try {
            // <editor-fold defaultstate="collapsed" desc="Log">
            Long timeBeforeQuery = (Calendar.getInstance()).getTimeInMillis();
            // </editor-fold>

            List<Long> resultNumber = (List<Long>) emw.createQuery(query, loggedUser).getResultList();
            if (resultNumber.size() > 1) // More than one record returned
            {
                logger.warn("None Unique Result Was Returned. Query: {}", query);
            }
            // MT: This line will detach the result set from the PersistenceContext
            // because in later usage, the result set is manually modified on the
            // the fly, and that causes synchronization exceptions

            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.debug("Query: {}", query);
            // </editor-fold>
            if (resultNumber != null && resultNumber.size() != 0) {
                logger.debug("Returning with: {}", resultNumber.get(0));
            }
            return resultNumber.get(0);

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.debug("Query: {}", query);
            // </editor-fold>
            logger.debug("Returning with: Null");
            return null;
        }
    }

    /**
     * Just runs the query and return the result. If no error, and no records
     * found, returned list is empty (not null)
     *
     * @param query
     * @param loggedUser
     * @return
     */
    @Override
    public List createViewListQuery(String query, OUser loggedUser) {
        logger.debug("Entering");
        if (!loggedUser.equals(getSystemUser(loggedUser))) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Non System User Is Not Supported, No Security Handling. Query: {}", query);
            // </editor-fold>
        }
        try {
            // <editor-fold defaultstate="collapsed" desc="Log">
            Long timeBeforeQuery = (Calendar.getInstance()).getTimeInMillis();
            // </editor-fold>
            Query createQuery = emw.createQuery(query, loggedUser);
            List list = createQuery.getResultList();
            // MT: This line will detach the result set from the PersistenceContext
            // because in later usages, the result can get manually modified on the
            // the fly, which causes synchronization exception
            emw.detachList(list, loggedUser);
            if (list == null) {
                list = new ArrayList();
            }

            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.debug("Query: {}", query);
            // </editor-fold>
            if (list != null) {
                logger.debug("Returning with list of size: {}", list.size());
            }
            return list;

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.debug("Query: {}", query);
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        }

    }

    /**
     * Construct a string with all the order by expressions mentioned in the
     * 'sorts' parameter <br> Facts & Assumptions: <br> - it puts "entity."
     * before the expression <br> - SortExpression has ColumnName +
     * "ASC"/"DESC"/None (ASC By Default)
     *
     * @param sorts
     * @return
     */
    private String constructEntityOrderByClause(List<String> sortExpressions) {
        logger.trace("Entering");
        String orderByClause = "";
        if (sortExpressions != null && sortExpressions.size() > 0) {
            orderByClause = " ORDER BY ";
            for (int sortIndex = 0; sortIndex < sortExpressions.size(); sortIndex++) {
                if (sortIndex != 0) // Not the first one in the list
                // put comma before this new order
                {
                    orderByClause += " , ";
                }
                int index = sortExpressions.get(sortIndex).lastIndexOf(" ");
                String fieldName = sortExpressions.get(sortIndex);
                String sortOrder = "";
                if (index > 0) {
                    sortOrder = fieldName.substring(index + 1);
                    fieldName = fieldName.substring(0, index);
                }

                if (fieldName.startsWith("CASE")) {
                    fieldName = fieldName.substring(5);
                    orderByClause += "CASE ";
                }
                if (fieldName.trim().toLowerCase().startsWith("lower(concat(")) {
                    String tempFieldName = fieldName.substring(6, fieldName.length() - 1);
                    if (tempFieldName.toLowerCase().trim().startsWith("concat(")) {
                        //<editor-fold defaultstate="collapsed" desc="SQL Function">
                        // only one function is supported
                        int firstIndex = tempFieldName.indexOf("("), lastIndex = tempFieldName.lastIndexOf(")");
                        String newCondition = "", fields[] = tempFieldName.substring(firstIndex + 1, lastIndex).split(",");
                        for (int j = 0; j < fields.length; j++) {
                            if (!fields[j].trim().startsWith("'")) {
                                if (!fields[j].trim().contains(".")) {
                                    fields[j] = "entity." + fields[j].trim();
                                } else {
                                    fields[j] = fields[j].trim();
                                }
                            }
                            newCondition = newCondition + fields[j] + ",";
                        }
                        tempFieldName = tempFieldName.substring(0, firstIndex + 1) + newCondition.substring(0,
                                newCondition.length() - 1) + tempFieldName.substring(lastIndex);
                        //</editor-fold>
                    }
                    fieldName = "lower(" + tempFieldName + ")";
                    orderByClause += fieldName + " " + sortOrder;
                } else if (fieldName.contains("lower(")) {
                    if (!fieldName.contains(".")) {
                        orderByClause += "lower(entity." + fieldName.substring(fieldName.indexOf("(") + 1) + " " + sortOrder;
                    } else {
                        orderByClause += "lower(" + fieldName.substring(fieldName.indexOf("(") + 1) + " " + sortOrder;
                    }
                } else {
                    //removing "entity." causes syntax errors while query parsing
                    // for more detais visit https://unitedofoq.atlassian.net/browse/FABS-241
                    //issue number FABS-241
//                        if (!fieldName.contains(".")) {
                    orderByClause += "entity." + fieldName + " " + sortOrder;
//                        } else {
//                            orderByClause += fieldName + " " + sortOrder;
//                        }
                }
                // disable showing null values we can use (isNull(fieldName))
            }
        }
        logger.trace("Returning with: {}", orderByClause);
        return orderByClause;
    }

    private String constructNativeQueryForSorting(List<String> sortExpressions) {
        logger.trace("Entering");
        String query = " left join ";
        if (sortExpressions != null && !sortExpressions.isEmpty() && sortExpressions.get(0).contains(".")) {
            for (int sortIndex = 0; sortIndex < sortExpressions.size(); sortIndex++) {
                if (sortExpressions.get(sortIndex).startsWith("CASE ")) {
                    logger.trace("Returning");
                    return null;
                }
                int dotLastIndex = sortExpressions.get(sortIndex).lastIndexOf(".");
                String toBeJoinedEntity = sortExpressions.get(sortIndex);
                if (toBeJoinedEntity.trim().toLowerCase().startsWith("lower(concat(")) {
                    String tempFieldName = toBeJoinedEntity.substring(6, toBeJoinedEntity.length() - 1);
                    if (tempFieldName.toLowerCase().trim().startsWith("concat(")) {
                        int firstIndex = tempFieldName.indexOf("("), lastIndex = tempFieldName.lastIndexOf(")");
                        String newCondition = "", fields[] = tempFieldName.substring(firstIndex + 1, lastIndex).split(",");
                        for (int j = 0; j < fields.length; j++) {
                            if (!fields[j].trim().startsWith("'")) {
                                fields[j] = fields[j].trim();
                            }
                            newCondition = newCondition + fields[j] + ",";
                        }
                        tempFieldName = tempFieldName.substring(0, firstIndex + 1) + newCondition.substring(0,
                                newCondition.length() - 1) + tempFieldName.substring(lastIndex);
                        //</editor-fold>
                    }
                    toBeJoinedEntity = tempFieldName;
                } else if (toBeJoinedEntity.contains("lower(")) {
                    toBeJoinedEntity = toBeJoinedEntity.substring(toBeJoinedEntity.indexOf("(") + 1, toBeJoinedEntity.lastIndexOf(".")) + " ";
                } else if (toBeJoinedEntity.contains(".")) {
                    toBeJoinedEntity = toBeJoinedEntity.substring(0, toBeJoinedEntity.lastIndexOf("."));
                } else {
                    toBeJoinedEntity = toBeJoinedEntity + " ";
                }
                if (sortIndex == 0) {
                    String ee;
                    if (toBeJoinedEntity.contains(".")) {
                        if (nestedSortExpression) {
                            ee = toBeJoinedEntity.substring(
                                    toBeJoinedEntity.lastIndexOf(".") + 1, toBeJoinedEntity.length());
                        } else {
                            ee = toBeJoinedEntity.substring(0,
                                    toBeJoinedEntity.indexOf("."));
                        }
                    } else {
                        ee = toBeJoinedEntity;
                    }
                    query += "entity." + toBeJoinedEntity + " " + ee + " ";
                } else {
                    query += " , " + "entity." + toBeJoinedEntity + " " + toBeJoinedEntity + " ";
                }
            }
            logger.trace("Returning");
            return query;
        } else {
            logger.trace("Returning");
            return null;
        }

    }

    private String constructEntityWhereClause(List<String> conditions) {
        logger.trace("Entering");
        String whereClause = "";
        if (conditions == null || conditions.isEmpty()) {
            whereClause = "(entity.inActive = false) ";
            logger.trace("Returning");
            return " WHERE " + whereClause;
        }

        if (conditions.contains(CONFCOND_CASE_INSENSITIVE)) {
            // Use case insensitive for LIKE
            whereClause = constructCaseInsensitiveEntityWhereClause(conditions);
        } else {
            whereClause = constructEntityCaseSensitiveWhereClause(conditions);
        }

        String delCond = "";
        if (conditions.contains(CONFCOND_GET_INACTIVEONLY)) {
            delCond = "entity.inActive = true";
        } else if (conditions.contains(CONFCOND_GET_ACTIVEANDINACTIVE)) {
            delCond = "";
        } else {
            delCond = "entity.inActive = false";
        }

        if (whereClause.equals("") && !delCond.equals("")) {
            whereClause = " (" + delCond + ") ";
        } else if (!delCond.equals("")) {
            whereClause += " AND (" + delCond + ")";
        }

        if (whereClause != null && !whereClause.equals("")) {
            whereClause = " WHERE " + whereClause;
        }
        logger.trace("Returning");
        return whereClause;
    }

    private String constructEntityCaseSensitiveWhereClause(List<String> conditions) {
        logger.trace("Entering");
        if (conditions == null) {
            logger.trace("Returning");
            return "";
        }
        if (conditions.isEmpty()) {
            logger.trace("Returning");
            return "";
        }

        String whereClause = "";
        String condition = "";
        //whereClause+= " WHERE ";
        for (int i = 0; i < conditions.size(); i++) {
            condition = conditions.get(i).trim();
            if (condition.startsWith(CONFCOND_PREFIX)) // Configuration condition
            // Do nothing, don't include it in the where condition
            {
                continue;
            }

            if ("".equals(condition)) // Empty condition
            // Do Nothing
            {
                continue;
            }

//            if (condition.contains("=") == true
//                    && condition.substring(0, condition.indexOf("=")).toLowerCase().contains("dbid")
//                    && NumberUtils.isNumber(condition.substring(condition.indexOf("=") + 1).trim())) {
//                condition += "L";
//            } else if (condition.toLowerCase().contains(" in ") == true
//                    && condition.substring(0, condition.toLowerCase().indexOf(" in ")).toLowerCase().contains("dbid")) {
//                String tempString =
//                        condition.substring(condition.indexOf("(") + 1, condition.lastIndexOf(")"));
//                String newTmp = "";
//                String[] tempStringArray = tempString.split(",");
//                for (String string : tempStringArray) {
//                    if (!NumberUtils.isNumber(string)) {
//                        continue;
//                    }
//                    newTmp += string + "L,";
//                }
//
//                if (newTmp.endsWith(",")) {
//                    newTmp = newTmp.substring(0, newTmp.length() - 1);
//                }
//                if (newTmp.equals("")) {
//                    newTmp = tempString;
//                }
//                condition = condition.substring(0, condition.indexOf("(") + 1)
//                        + newTmp.substring(0, newTmp.length() - 1) + ")";
//            }
            if (i != 0 && !whereClause.equals("")) {
                whereClause += " AND ";
            }

            boolean addNot = false;
            if (condition.startsWith("NOT ")) {
                condition = condition.substring(4);
                addNot = true;
            }

            if (condition.trim().startsWith("concat(")) {
                //<editor-fold defaultstate="collapsed" desc="SQL Function">
                // only one function is supported
                int firstIndex = condition.indexOf("("), lastIndex = condition.lastIndexOf(")");
                String newCondition = "", fields[] = condition.substring(firstIndex + 1, lastIndex).split(",");
                for (int j = 0; j < fields.length; j++) {
                    if (!fields[j].trim().startsWith("'")) {
                        fields[j] = "entity." + fields[j].trim();
                    }
                    newCondition = newCondition + fields[j] + ",";
                }
                condition = condition.substring(0, firstIndex + 1) + newCondition.substring(0,
                        newCondition.length() - 1) + condition.substring(lastIndex);
                //</editor-fold>
                whereClause += " (" + (addNot ? " NOT " : "") + condition + ") ";
            } else if (condition.trim().startsWith("EXTRACT")) {//alert special function
                whereClause += condition;
            } else {
                whereClause += " (" + (addNot ? " NOT " : "") + "entity." + condition + ") ";
            }
        }
        logger.trace("Returning");
        return whereClause;
    }

    private String constructCaseInsensitiveEntityWhereClause(List<String> conditions) {
        logger.trace("Entering");
        /*"dbid not in (select mm.employee.dbid From EmployeeDailyAttendance mm Where  mm.employee.positionSimpleMode.unit.dbid = 491262 And mm.dailyDate = '2013-04-01')"*/
        if (conditions == null) {
            logger.trace("Returning");
            return "";
        }
        if (conditions.isEmpty()) {
            logger.trace("Returning");
            return "";
        }
        if (conditions.size() == 1 && conditions.contains(CONFCOND_CASE_INSENSITIVE)) {
            logger.trace("Returning");
            return "";
        }

        StringBuilder whereClause = new StringBuilder();
        boolean conditionQueryStart = true;
        for (String singleCondition : conditions) {
            if (conditionQueryStart && !whereClause.toString().equals("")) {
                conditionQueryStart = false;
            }
            whereClause.append(transformRawConditionToWhereClause(conditionQueryStart, singleCondition));

        }
        logger.trace("Returning");
        String finalWhereClauseString = whereClause.toString();
        return (finalWhereClauseString.equals(" WHERE ")) ? "" : finalWhereClauseString;
    }

    public String transformRawConditionToWhereClause(boolean conditionQueryStart, String rawCondition) {

        StringBuilder whereClause = new StringBuilder();
        String condFieldPart = "";
        String condNonFieldPart = "";

        String condition = rawCondition.trim();

        if (condition.startsWith(CONFCOND_PREFIX)) // Configuration condition
        // Do nothing, don't include it in the where condition
        {
            return "";
        }
        //<editor-fold defaultstate="collapsed" desc="unknown commented code">
        //NOT dbid in (1000001444030,1000001444031,1000001444030)
        //oScreen.dbid = 1000001444015, ##ACTIVEONLY, ##AB, NOT dbid in (1000001444030)]
//            if (condition.contains("=") == true
//                    && condition.substring(0, condition.indexOf("=")).toLowerCase().contains("dbid")
//                    && NumberUtils.isNumber(condition.substring(condition.indexOf("=") + 1).trim())) {
//                condition += "L";
//            } else if (condition.toLowerCase().contains(" in ") == true
//                    && condition.substring(0, condition.toLowerCase().indexOf(" in ")).toLowerCase().contains("dbid")) {
//                String tempString =
//                        condition.substring(condition.indexOf("(") + 1, condition.lastIndexOf(")"));
//                String newTmp = "";
//                String[] tempStringArray = tempString.split(",");
//                for (String string : tempStringArray) {
//                    if (!NumberUtils.isNumber(string)) {
//                        continue;
//                    }
//                    newTmp += string + "L,";
//                }
//
//                if (newTmp.endsWith(",")) {
//                    newTmp = newTmp.substring(0, newTmp.length() - 1);
//                }
//                if (newTmp.equals("")) {
//                    newTmp = tempString;
//                }
//
//                condition = condition.substring(0, condition.indexOf("(") + 1)
//                        + newTmp + ")";
//            }
//            </editor-fold>
        if (!conditionQueryStart) {
            whereClause.append(" AND ");
        }
        // condFieldPart = condition.substring(0, condition.indexOf(" "));
        //condNonFieldPart = condition.substring(condition.indexOf(" "), condition.length());

        whereClause.append(" (");
        //<editor-fold defaultstate="collapsed" desc="if we have or condition instide the query">

        if (StringUtils.countMatches(condition, CONDITIONAL_SHADOWED_OR) > 0) {
            //then we have or condition inside that condition
            String[] miniConditions = condition.split(CONDITIONAL_SHADOWED_OR);
            String[] miniWhereClauses = new String[miniConditions.length];
            for (int i = 0; i < miniConditions.length; i++) {
                miniWhereClauses[i] = transformRawConditionToWhereClause(true, miniConditions[i]);
            }
            return whereClause + StringUtils.join(miniWhereClauses, " OR ") + ") ";

        }
        //</editor-fold>
        if (!(condition.contains("=")
                || condition.contains("<")
                || condition.contains(">")
                || condition.contains("<>"))
                && condition.toUpperCase().contains("LIKE")) {
            condFieldPart = condition.substring(0,
                    condition.toUpperCase().indexOf(" LIKE "));
            condNonFieldPart = condition.substring(
                    condition.toUpperCase().indexOf(" LIKE "), condition.length());

            if (condFieldPart.startsWith("NOT ")) {
                condFieldPart = condFieldPart.substring(4);
                whereClause.append("NOT ");
            }

            if (condFieldPart.toLowerCase().trim().startsWith("concat(")) {
                //<editor-fold defaultstate="collapsed" desc="SQL Function">
                // only one function is supported
                int firstIndex = condFieldPart.indexOf("("), lastIndex = condFieldPart.lastIndexOf(")");
                String newCondition = "", fields[] = condFieldPart.substring(firstIndex + 1, lastIndex).split(",");
                for (int j = 0; j < fields.length; j++) {
                    if (!fields[j].trim().startsWith("'")) {
                        fields[j] = "entity." + fields[j].trim();
                    }
                    newCondition = newCondition + fields[j] + ",";
                }
                condFieldPart = condFieldPart.substring(0, firstIndex + 1) + newCondition.substring(0,
                        newCondition.length() - 1) + condFieldPart.substring(lastIndex);
                //</editor-fold>
                whereClause.append("lower(").append(condFieldPart).append(") ").append(condNonFieldPart);
            } else {
                whereClause.append("lower(entity.").append(condFieldPart).append(") ").append(condNonFieldPart);
            }
        } else {
            if (condition.startsWith("NOT ")) {
                condition = condition.substring(4);
                whereClause.append("NOT ");
            }
            if (condFieldPart.trim().startsWith("concat(")) {
                //<editor-fold defaultstate="collapsed" desc="SQL Function">
                // only one function is supported
                String oldCondFieldPart = condFieldPart;
                int firstIndex = condFieldPart.indexOf("("), lastIndex = condFieldPart.lastIndexOf(")");
                String newCondition = "", fields[] = condFieldPart.substring(firstIndex, lastIndex).split(",");
                for (int j = 0; j < fields.length; j++) {
                    if (!fields[j].trim().startsWith("'")) {
                        fields[j] = "entity." + fields[j].trim();
                    }
                    newCondition = newCondition + fields[j] + ",";
                }
                condFieldPart = condFieldPart.substring(0, firstIndex + 1) + newCondition.substring(0,
                        newCondition.length() - 1) + condFieldPart.substring(lastIndex);
                //</editor-fold>
                whereClause.append("").append(condition.replace(oldCondFieldPart, condFieldPart));
            } else if (condition.trim().startsWith("EXTRACT")) {//alert special function
                whereClause.append(condition);
            } else {
                whereClause.append("entity.").append(condition);
            }
        }
        whereClause.append(") ");
        return whereClause.toString();
    }

    @Override
    public Query createNamedQuery(String namedQueryName, OUser loggedUser, Object... params) {
        logger.debug("Entering");
        Query query;
        query = emw.createNamedQuery(namedQueryName, loggedUser);

        // Set parameters if found
        if (params.length % 2 != 0) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Named Query Parameters Error - Size Should Be Even. Query Name: {} Parameters:{}", namedQueryName, params);
            // </editor-fold>

        } else {
            for (int paramIndex = 0; paramIndex < params.length; paramIndex += 2) {
                query.setParameter((String) params[paramIndex],
                        params[paramIndex + 1]);
            }
        }
        logger.debug("Returning");
        return query;
    }

    @Override
    public List createListNamedQuery(
            String namedQueryName, OUser loggedUser, Object... params)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException {
        logger.debug("Entering");
        try {
            Query query = createNamedQuery(namedQueryName, loggedUser, params);
            List baseEntities = null;

            // <editor-fold defaultstate="collapsed" desc="Log">
            Long timeBeforeQuery = (Calendar.getInstance()).getTimeInMillis();
            // </editor-fold>

            baseEntities = query.getResultList();
            emw.detachList(baseEntities, loggedUser);
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.debug("Query: {}", query);
            // </editor-fold>

            if (baseEntities == null) {
                return new ArrayList();
            }

            if (baseEntities.size() > 0) {
                baseEntities = postLoadEntity(baseEntities, null, null, loggedUser);
            }

            return baseEntities;

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.debug("Query Name: {} Parameters: {}", namedQueryName, params);
            // </editor-fold>
            logger.debug("Returning");
            return null;
        }
    }

    public List<String> getSelfFieldsExpressions(OEntity entity) {
        logger.debug("Entering");
        List<String> selfFieldsExps = new ArrayList<String>();
        try {
            BaseEntity baseEntity = (BaseEntity) Class.forName(entity.getEntityClassPath()).newInstance();

            if (baseEntity.getClass().getAnnotation(Hat.class) != null) {
                Hat hat = (Hat) baseEntity.getClass().getAnnotation(Hat.class);

                if (hat.type()
                        != null && hat.type().length > 0) {
                    for (int i = 0; i < hat.type().length; i++) {
                        Hat.HatType type = hat.type()[i];
                        if (type == Hat.HatType.SELF) {
                            selfFieldsExps.add("dbid");
                            break;
                        }
                    }

                }
            }
            List<Field> selfFields = BaseEntity.getSelfFields(baseEntity.getClass());
            for (Field fld : selfFields) {
                try {
                    if (fld.getType().newInstance() instanceof BaseEntity) {
                        selfFieldsExps.add(fld.getName() + ".dbid");

                    } else {
                        //NOT Supported
                    }
                } catch (InstantiationException ex) {
                    logger.error("Exception thrown", ex);
                } catch (IllegalAccessException ex) {
                    logger.error("Exception thrown", ex);
                }

            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
        return selfFieldsExps;
    }

    /**
     * Load the entity with all its direct children
     *
     * @param entityName Temporary, to be class path or Class object later on
     * @param conditions
     * @param loggedUser
     * @return
     */
    public BaseEntity loadFullEntity(String entityName, ArrayList<String> conditions, OUser loggedUser) {
        logger.debug("Entering");
        List<String> fieldExpressions = new ArrayList<String>();
        List<String> childFieldNames = BaseEntity.getChildEntityFieldName(OEntity.class
                .getName());
        for (String childFieldName : childFieldNames) {
            fieldExpressions.add(childFieldName);
        }
        BaseEntity object = null;

        try {
            object = (BaseEntity) loadEntity(entityName, conditions, fieldExpressions, loggedUser);
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.debug("Entity Name: {} ", entityName);
            // </editor-fold>
        }
        logger.debug("Returning");
        return object;
    }

    /**
     * Load the user from its tenant database. This should be the only function
     * in the system to load a user for security and query purposes (not
     * necessary for maintenance) <br>Sets the returned user tenant, so it can
     * be used later on in any query or security
     *
     * @param loginName User Login Name
     * @param userTenant User's Tenant
     * @return User: if login name is found <br>null: Error, or user not found
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public OUser getUser(String loginName, OTenant userTenant) {
        logger.debug("Entering");
        OUser loggedUser = null;
        OUser tenantGrantedUser = null;
        try {
            tenantGrantedUser = centralOEM.getGrantedUser(userTenant);
            tenantGrantedUser.setLoginName(loginName);
            //FIXME: need to set the granted user language to be same like loginName
            // preference language to get the proper translatable fields of loggedUser OUser
//            tenantGrantedUser.setLoginName(loginName);
            loggedUser = (OUser) loadEntity(
                    OUser.class
                    .getSimpleName(),
                    Collections.singletonList("loginName = '" + loginName + "'"),
                    null,
                    tenantGrantedUser);
            if (loggedUser
                    == null) // User not found or is deleted
            {
                FacesContext facesContext = FacesContext.getCurrentInstance();
                String errorPageLocation = "/userNotFoundInDB.iface";
                if(facesContext == null ){
                    return null;
                }
                facesContext.setViewRoot(facesContext.getApplication().getViewHandler().createView(facesContext, errorPageLocation));
                facesContext.getPartialViewContext().setRenderAll(true);
                facesContext.renderResponse();

                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.debug("loginName: {} Tenant:{}", loginName, userTenant);
                // </editor-fold>
                logger.debug("Returning");
                return null;
            }

            loggedUser.setTenant(userTenant);
            logger.debug("Returning");
            return loggedUser;
        } catch (UserNotAuthorizedException ex) {
            // Should not happen as system user has full authority
            logger.error("Exception thrown", ex);

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.debug("loginName: {} Tenant:{}", loginName, userTenant);
            // </editor-fold>

        }
        logger.debug("Returning");
        return null;   //Exception, error
    }
    
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public OUser getUserEnc(String loginName, OTenant userTenant) {
        logger.debug("Entering");
        OUser loggedUser = null;
        OUser tenantGrantedUser = null;
        try {
            tenantGrantedUser = centralOEM.getGrantedUser(userTenant);
            tenantGrantedUser.setLoginName(loginName);
            tenantGrantedUser.setStartupEnc(true);
            loggedUser = (OUser) loadEntity(
                    OUser.class
                    .getSimpleName(),
                    Collections.singletonList("loginName = '" + loginName + "'"),
                    null,
                    tenantGrantedUser);
            if(loggedUser != null)
                loggedUser.setTenant(userTenant);
            logger.debug("Returning");
            return loggedUser;
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.debug("loginName: {} Tenant:{}", loginName, userTenant);
            // </editor-fold>

        }
        logger.debug("Returning");
        return null;   //Exception, error
    }

    public OUser getUserByDBID(long dbid, OUser loggedUser) {
        String loginName = getUserLoginNameByDBID(dbid, loggedUser);
        return getUserForLoginName(loginName);
    }

    private String getUserLoginNameByDBID(long dbid, OUser loggedUser) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT u.loginName FROM OUser u WHERE u.dbid = ").append(dbid);
        String loginName = (String) emw.createQuery(query.toString(), loggedUser).getSingleResult();
        return loginName;
    }

    /**
     * Returns
     * {@link #getUser(java.lang.String, com.unitedofoq.fabs.central.OTenant)}
     * after getting the tenant of the {
     *
     * @param loginName}
     * @param loginName User Login Name
     * @return User: if login name is found <br>null: Error, or user not found
     */
    public OUser getUserForLoginName(String loginName) {
        logger.debug("Entering");
        OTenant loginNameTenant = null;
        loginNameTenant = centralOEM.getUserTenant(loginName);
        if (loginNameTenant == null) {
            logger.debug("Returning");
            return null;
        }
        logger.debug("Returning");
        return getUser(loginName, loginNameTenant);
    }

    /**
     * Search for entity of type 'searchByValues' in 'em' using: <br>1. The
     * first known unique field found, or if not found <br>2. The first known
     * unique constraint
     *
     * @param searchByValues
     * @param em
     * @return Entity: When found <br>null: No entity found
     *
     * @throws FABSException
     */
    public BaseEntity findEntityByUnique(BaseEntity searchByValues, OUser loggedUser)
            throws FABSException {
        logger.debug("Entering");
        // Construct where condition using Unique Fields
        Class entityClass = searchByValues.getClass();
        String whereClause = "";

        List<Field> selectedUniqueFields = new ArrayList<Field>();
        List<Field> entityUniqueFields = BaseEntity.getUniqueFields(entityClass);
        // Check first any unique field
        if (entityUniqueFields.size() > 0) {
            // Unique Fields Found
            // Select the first one, one is enough
            // TODO: enhancement, loop on fields and get the first supported type in this function
            selectedUniqueFields.add(entityUniqueFields.get(0));
        } else {
            // No Unique Fields Found
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Search By Unique Constraint Is Not Supported Yet. Entity Class:{} ", entityClass);
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        }
        if (selectedUniqueFields.isEmpty()) {
            // No Unique Fields Found For Entity
            throw new FABSException(loggedUser, "No Unique Fields Found For Entity",
                    searchByValues, null, Collections.singletonList(entityClass), null);
        }
        for (Field uniqueField : selectedUniqueFields) {
            String cond = "";
            try {
                Object fieldValue = searchByValues.invokeGetter(uniqueField);

                if (fieldValue == null) {
                    throw new FABSException(loggedUser, "Unique Column Value Is Null",
                            searchByValues, null, Collections.singletonList(uniqueField), null);

                }

                if (uniqueField.getType() == String.class) {
                    cond = " entity." + uniqueField.getName() + " = '" + (String) fieldValue + "'";
                } else if (uniqueField.getType() == Boolean.class
                        || uniqueField.getType() == Long.class
                        || uniqueField.getType() == Integer.class
                        || uniqueField.getType().isPrimitive()) {
                    cond = " entity." + uniqueField.getName() + " = " + fieldValue;
                } else {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    if (uniqueField != null) {
                        logger.warn("Unsopperted Type In Search. Field: {} Field Type: {} ", uniqueField, uniqueField.getType());
                    }
                    // </editor-fold>
                    // Including uniqueField.getType().getName().contains("unitedofoq")
                    logger.debug("Returning");
                    return null;
                }
            } catch (NoSuchMethodException ex) {
                throw new FABSException(loggedUser, "Unique Field Has No Getter",
                        searchByValues, ex, Collections.singletonList(uniqueField), null);
            }

            if (!"".equals(whereClause)) {
                whereClause = " AND " + cond;
            } else {
                whereClause = cond;
            }
        }
        whereClause = " WHERE " + whereClause;

        // <editor-fold defaultstate="collapsed" desc="Log">
        Long timeBeforeQuery = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>

        String query = "SELECT entity FROM " + entityClass.getSimpleName() + " entity " + whereClause;
        List<BaseEntity> baseEntities = emw.createQuery(query, loggedUser).getResultList();
        // MT: This line will detach the result set from the PersistenceContext
        // because in later usage, the result set is manually modified on the
        // the fly, and that causes synchronization exceptions
        emw.detachList(baseEntities, loggedUser);
        // <editor-fold defaultstate="collapsed" desc="Log">
        logger.debug("Query: {}", query);
        // </editor-fold>

        if (baseEntities.isEmpty()) {
            logger.debug("Returning");
            return null;
        } else if (baseEntities.size() > 1) {
            throw new NonUniqueResultException();
        }
        if (baseEntities != null && baseEntities.size() != 0) {
            logger.debug("Returning with: {}", baseEntities.get(0));
        }
        return baseEntities.get(0);
    }

    /**
     * Calls {@link OEntityManagerFactory#closeEM(OUser) }
     */
    @Override
    public void closeEM(OUser loggedUser) {
        logger.debug("Entering");
        logger.debug("Returning");
    }

    /**
     * This method is called to do post load on base entities that were loaded
     * through query and not the entity manage normal methods.
     *
     * @param baseEntities
     * @param conditions
     * @param fieldExpressions
     * @param loggedUser
     * @return
     */
    @Override
    public List<BaseEntity> callPostLoadForQueryLoadedList(List<BaseEntity> baseEntities,
            List<String> conditions, List<String> fieldExpressions, OUser loggedUser) {
        logger.debug("Entering");
        try {
            List<BaseEntity> postLoadedEntities = postLoadEntity(baseEntities, conditions, fieldExpressions, loggedUser);
            // MT: This line will detach the result set from the PersistenceContext
            // because in later usage, the result set gets manually modified on the
            // the fly, and that causes synchronization exceptions
            emw.detachList(postLoadedEntities, loggedUser);
            return postLoadedEntities;
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            logger.debug("Returning");
            return baseEntities;
        }
    }

    @Override
    public void deleteEntityList(List<BaseEntity> entityList, OUser loggedUser) {
        logger.debug("Entering");
        try {
            securityService.checkActionEntityAuthority(entityList.get(0).getClass().getSimpleName(),
                    OEntityAction.ATDBID_ACTIVATION, loggedUser, loggedUser);

            // No exception thrown, he is authorized
            for (BaseEntity baseEntity : entityList) {
                deleteEntity(baseEntity, loggedUser);
            }

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
        }
        logger.debug("Returning");
    }

    //<editor-fold defaultstate="collapsed" desc="getTotalNumberOfEntitiesRecords">
    /**
     * Calculate the total Number of records For the passed oentity using the
     * passed conditions, calls {@link OEntityManagerRemote#closeEM(OUser)} if
     * succeeded. <br>Calls
     * {@link OEntityManagerRemote#requestCommitTransaction(OUser, UserTransaction)}
     *
     * @param oEntity
     * @param conditions
     * @param loggedUser
     *
     * @return Number of records for The passed Entity using the passed
     * conditions
     */
    @Override
    public long getTotalNumberOfEntityRecords(OEntity oEntity, ArrayList<String> conditions, OUser loggedUser) {
        logger.debug("Entering");
        // Call EntityManager.getTotalNumberOfEntities(EntityName);
        String whereCluase = constructEntityWhereClause(conditions);

        String query = "SELECT COUNT(DISTINCT entity) FROM " + oEntity.getEntityClassName() + " entity " + whereCluase;
        long numberOfRecords = createNumberQuery(query, loggedUser);
        logger.debug("Returning");
        return numberOfRecords;
    }

    @Override
    public long getTotalNumberOfEntityDTORecords(OEntityDTO oEntity,
            ArrayList<String> conditions, OUser loggedUser) {
        logger.debug("Entering");
        // Call EntityManager.getTotalNumberOfEntities(EntityName);
        String whereCluase = constructEntityWhereClause(conditions);

        String query = "SELECT COUNT(DISTINCT entity) FROM "
                + oEntity.getEntityClassName() + " entity " + whereCluase;
        long numberOfRecords = createNumberQuery(query, loggedUser);
        logger.debug("Returning");
        return numberOfRecords;
    }
    //</editor-fold>

    @Override
    public List executeQueryWithParameters(String queryString, OUser loggedUser, String... params) {
        logger.debug("Entering");
        Query query = emw.createQuery(queryString, loggedUser);

        // Set parameters if found
        if (params.length % 2 != 0) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Named Query Parameters Error - Size Should Be Even. Query Name: {}, Parameters: {}", queryString, params);
            // </editor-fold>

        } else {
            for (int paramIndex = 0; paramIndex < params.length; paramIndex += 2) {
                query.setParameter((String) params[paramIndex], params[paramIndex + 1]);
            }
        }
        List queryResultList = query.getResultList();
        emw.detachList(queryResultList, loggedUser);
        if (query != null && query.getResultList() != null) {
            logger.debug("Returning with querylist of size: {}", queryResultList.size());
        }

        return queryResultList;
    }

    @Override
    public BaseEntity merge(BaseEntity entity, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException,
            OEntityDataValidationException, EntityDataValidationException, FABSException {
        logger.debug("Entering");

        //Removed in R18.1.9  as it's handeled in the front end  - Islam
        // cleanEntity(entity, loggedUser);
        if (entity.getDbid() > 0) {
            entity = emw.merge(entity, loggedUser);
            emw.flush(loggedUser);
        } else {
            emw.persist(entity, loggedUser);
            emw.flush(loggedUser);
        }
        emw.detach(entity, loggedUser);
        if (null != entity) {
            logger.debug("Returning with entity", entity.getClassName());
        }
        return entity;
    }

    private BaseEntity decrypteEntity(BaseEntity entity, OUser loggedUser)
            throws EntityNotFoundException, NonUniqueResultException,
            RollbackException, EntityExistsException, TransactionRequiredException,
            UserNotAuthorizedException, OptimisticLockException, NoResultException {
        logger.trace("Entering");

        if (entity == null || !isEncryptionEnabled(loggedUser)) {
            logger.trace("Returning with entity: {}", entity);
            return entity;
        }

//        if (entity.getClass().getName().contains("com.unitedofoq.fabs")) {
//            logger.trace("Returning with entity: {}", entity);
//            return entity;
//        }
        ArrayList<String> conditions = new ArrayList<String>();
        conditions.add("encrypted = true");
        OEntity oentity = getOEntity(entity.getClassName(), loggedUser);
        if (null == oentity) {
            logger.trace("Returning with entity: {}", entity);
            return entity;
        }
        conditions.add("oentity.dbid =" + oentity.getDbid());
        List<OEntityField> oEntityFields = loadEntityList(OEntityField.class.getSimpleName(),
                conditions, null, null, loggedUser);
        //--------------------------
        if (null != oEntityFields && oEntityFields.size() != 0) {
            AttachmentDTO attachmentDTO = attachmentService.getByName("privateKey.key", loggedUser, entity.getClassName());
            if (null == attachmentDTO) {
                logger.warn("Encryption Keys Not Found");
                return entity;
            }
        }
        //--------------------------
        for (OEntityField field : oEntityFields) {
            List<ExpressionFieldInfo> infos = null;
            try {
                infos = BaseEntity.parseFieldExpression(Class.forName(
                        field.getOentity().getEntityClassPath()),
                        field.getFieldExpression());
            } catch (ClassNotFoundException ex) {
                logger.error("Exception thrown", ex);
            }
            ExpressionFieldInfo efi = infos.get(infos.size() - 1);
            Field entityField = efi.field;
            if (entityField.getType() == String.class) {
                try {
                    if (entityField.getAnnotation(Translation.class) != null) {
                        String txtTranslated = (String) BaseEntity.
                                getValueFromEntity(entity,
                                        entityField.getName() + "Enc");
                        if (null != txtTranslated) {
                            String decrtxt = dataEncryptorServiceLocal.
                                    textDecryptor(txtTranslated);
                            BaseEntity.setValueInEntity(entity,
                                    entityField.getName(), decrtxt);
                        }

                        String txt = (String) BaseEntity.
                                getValueFromEntity(entity,
                                        entityField.getName().substring(0,
                                                entityField.getName().indexOf("Translated")));
                        if (null != txt) {
                            String decrtxt = dataEncryptorServiceLocal.
                                    textDecryptor(txt);
                            BaseEntity.setValueInEntity(entity,
                                    entityField.getName().substring(0,
                                            entityField.getName().indexOf("Translated")), decrtxt);
                        }

                    } else {
                        String txt = (String) BaseEntity.
                                getValueFromEntity(entity,
                                        entityField.getName() + "Enc");
                        if (null != txt) {
                            String decrtxt = dataEncryptorServiceLocal.
                                    textDecryptor(txt);
                            BaseEntity.setValueInEntity(entity,
                                    entityField.getName(), decrtxt);
                        }
                    }

                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }
            } else if (entityField.getType() == BigDecimal.class) {
                String decrDec;
                try {
                    Object txtObj = BaseEntity.
                            getValueFromEntity(entity,
                                    entityField.getName() + "Enc");
                    if (txtObj != null) {
                        String txt = txtObj.toString();
                        if (null != txt) {
                            decrDec = dataEncryptorServiceLocal.
                                    bigDecDecryptor(txt).toString();
                            BaseEntity.setValueInEntity(entity,
                                    entityField.getName(), decrDec);
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }

            } else if (entityField.getType() == Integer.class) {
                try {
                    if (BaseEntity.
                            getValueFromEntity(entity,
                                    entityField.getName()) != null) {
                        String txt = BaseEntity.
                                getValueFromEntity(entity,
                                        entityField.getName() + "Enc").toString();
                        if (null != txt) {
                            String decrint = dataEncryptorServiceLocal.
                                    bigIntDecryptor(txt).toString();
                            BaseEntity.setValueInEntity(entity,
                                    entityField.getName(), Integer.parseInt(decrint));
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }
            }
        }
        entity.setEncyrpted(false);
        if (null != entity) {
            logger.trace("Returning with entity: {}", entity.getClassName());
        }
        return entity;
    }

    private List<BaseEntity> decrypteEntityList(List<BaseEntity> entityList, OUser loggedUser)
            throws EntityNotFoundException, NonUniqueResultException,
            RollbackException, EntityExistsException, TransactionRequiredException,
            UserNotAuthorizedException, OptimisticLockException, NoResultException {
        logger.trace("Entering");
        if (entityList == null || entityList.isEmpty()) {
            logger.trace("Returning with empty list");
            return entityList;
        }
        if (!isEncryptionEnabled(loggedUser)) {
            logger.trace("Returning with entity list: {}", entityList);
            return entityList;
        }
        List<OEntityField> oEntityFields = getEncryptionFields(entityList.get(0), loggedUser);

        for (OEntityField field : oEntityFields) {
            List<ExpressionFieldInfo> infos = null;
            try {
                infos = BaseEntity.parseFieldExpression(Class.forName(
                        field.getOentity().getEntityClassPath()),
                        field.getFieldExpression());
            } catch (ClassNotFoundException ex) {
                logger.error("Exception thrown", ex);
            }
            ExpressionFieldInfo efi = infos.get(infos.size() - 1);
            Field entityField = efi.field;
            if (entityField.getType() == String.class) {
                try {
                    if (entityField.getAnnotation(Translation.class) != null) {
                        decryptTranslationFieldList(entityList, entityField);
                    } else {
                        decryptTextFieldList(entityList, entityField);
                    }

                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }
            } else if (entityField.getType() == BigDecimal.class) {
                try {
                    decryptDecimalFieldList(entityList, entityField);
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }

            } else if (entityField.getType() == Integer.class) {
                try {
                    decryptIntegerFieldList(entityList, entityField);
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                }
            }
        }
        return entityList;
    }

    private boolean isEncryptionEnabled(OUser loggedUser)
            throws EntityNotFoundException, RollbackException, OptimisticLockException, TransactionRequiredException, UserNotAuthorizedException, NonUniqueResultException, NoResultException, EntityExistsException {
        String encryptionStatus = setupSerivce.getKeyValue("enableEncryption", loggedUser);
        return encryptionStatus != null && encryptionStatus.equals("true");
    }

    private void decryptIntegerFieldList(List<BaseEntity> entityList, Field entityField) throws IllegalArgumentException, InvocationTargetException, NoSuchMethodException, IllegalAccessException {
        for (BaseEntity entity : entityList) {
            if (BaseEntity.
                    getValueFromEntity(entity,
                            entityField.getName()) != null) {
                String txt = BaseEntity.
                        getValueFromEntity(entity,
                                entityField.getName() + "Enc").toString();
                if (null != txt) {
                    String decrint = dataEncryptorServiceLocal.
                            bigIntDecryptor(txt).toString();
                    BaseEntity.setValueInEntity(entity,
                            entityField.getName(), Integer.parseInt(decrint));
                }
            }
            entity.setEncyrpted(false);
        }
    }

    private void decryptDecimalFieldList(List<BaseEntity> entityList, Field entityField) throws NoSuchMethodException, InvocationTargetException, IllegalArgumentException, IllegalAccessException {
        for (BaseEntity entity : entityList) {
            Object txtObj = BaseEntity.
                    getValueFromEntity(entity,
                            entityField.getName() + "Enc");
            if (txtObj != null) {

                String txt = txtObj.toString();
                if (null != txt) {
                    String decrDec = dataEncryptorServiceLocal.
                            bigDecDecryptor(txt).toString();
                    BaseEntity.setValueInEntity(entity,
                            entityField.getName(), decrDec);
                }
            }
            entity.setEncyrpted(false);
        }
    }

    private List<OEntityField> getEncryptionFields(BaseEntity entity, OUser loggedUser) throws UserNotAuthorizedException {
        List<String> conditions = new LinkedList<>();
        conditions.add("encrypted = true");
        OEntity oentity = getOEntity(entity.getClassName(), loggedUser);
        if (null == oentity) {
            return new LinkedList<>();
        }
        conditions.add("oentity.dbid =" + oentity.getDbid());
        return loadEntityList(OEntityField.class.getSimpleName(),
                conditions, null, null, loggedUser);
    }

    private void decryptTextFieldList(List<BaseEntity> entityList, Field entityField) throws IllegalAccessException, NoSuchMethodException, IllegalArgumentException, InvocationTargetException {
        for (BaseEntity entity : entityList) {
            String txt = (String) BaseEntity.
                    getValueFromEntity(entity,
                            entityField.getName() + "Enc");
            if (null != txt) {
                String decrtxt = dataEncryptorServiceLocal.
                        textDecryptor(txt);
                BaseEntity.setValueInEntity(entity,
                        entityField.getName(), decrtxt);
            }
            entity.setEncyrpted(false);
        }
    }

    private void decryptTranslationFieldList(List<BaseEntity> entityList, Field entityField) throws InvocationTargetException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException {
        decryptTextFieldList(entityList, entityField);
        for (BaseEntity entity : entityList) {
            String txt = (String) BaseEntity.
                    getValueFromEntity(entity,
                            entityField.getName().substring(0,
                                    entityField.getName().indexOf("Translated")));
            if (null != txt) {
                String decrtxt = dataEncryptorServiceLocal.
                        textDecryptor(txt);
                BaseEntity.setValueInEntity(entity,
                        entityField.getName().substring(0,
                                entityField.getName().indexOf("Translated")), decrtxt);
            }
        }
    }
//Removed in R18.1.9  as it's handeled in the front end  - Islam
//    private void cleanEntity(BaseEntity entity, OUser loggedUser) {
//        List<Field> allFields = entity.getAllFields();
//        for (Field field : allFields) {
//            if (field.getType().getSimpleName().equals("String")) {
//                DD fieldDD = ddService.getDD(entity.getFieldDDName(field.getName()), loggedUser);
//                try {
//                    String value = (String) entity.invokeGetter(field);
//                    if (value != null) {
//                        Boolean htmlAllowed;
//                        if (fieldDD != null) {
//                            htmlAllowed = fieldDD.getHtmlAllowed() != null
//                                    ? fieldDD.getHtmlAllowed()
//                                    : false;
//                        } else {
//                            StringBuilder st = new StringBuilder();
//                            st.append("CustomAlert")
//                                    .append("MailMergeContent");
//                            htmlAllowed = st.toString().contains(entity.getClass().getSimpleName())
//                                    ? true
//                                    : false;
//                        }
//                        //Construct list of image sources before filteration..
//                        List<String> imageTagsSources = new ArrayList<String>();
//                        if (value.contains("<img")) {
//                            String temp = value.substring(value.indexOf("<img"));
//                            while (temp.contains("<img")) {
//                                imageTagsSources.add(temp.substring(temp.indexOf("src"), temp.indexOf("style=")));
//                                temp = temp.substring(temp.indexOf(">"));
//                                if (temp.contains("<img") && temp.length() > 8) {
//                                    temp = temp.substring(temp.indexOf("<img"));
//                                }
//                            }
//                        }
//                        value = htmlAllowed
//                                ? Jsoup.clean(value, Whitelist.relaxed()
//                                        .addAttributes(":all", "dir", "align", "border", "cellpadding", "cellspacing",
//                                                "style")).replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&amp;", "&")
//                                : Jsoup.clean(value, Whitelist.none()).replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&amp;", "&");
//
//                        //Add image sources after filteration..
//                        if (imageTagsSources.size() > 0) {
//                            int sourcesCounter = 0;
//                            StringBuilder tempValue = new StringBuilder(value.substring(0, value.indexOf("<img alt=\"\"")));
//                            while (sourcesCounter < imageTagsSources.size()) {
//                                tempValue.append(" <img alt=\"\"" + imageTagsSources.get(sourcesCounter));
//                                tempValue.append(value.substring(value.indexOf("style="), value.indexOf(";\">") + 3));
//                                sourcesCounter++;
//                            }
//                            if (!tempValue.substring(tempValue.length() - 4).equals(value.substring(value.length() - 4))) {
//                                tempValue.append(value.substring(value.lastIndexOf("px;\">") + 5));
//                            }
//                            value = tempValue.toString();
//                        }
//
//                        entity.invokeSetter(field, value);
//
//                    }
//
//                } catch (NoSuchMethodException ex) {
//                    logger.debug("this field has no getter or setter", field.getName());
//                }
//            }
//        }
//    }

    @Override
    public List<String> getUsersLoginNamesForGroup(String groupName, OUser loggedUser) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT ru.ouser.loginName FROM RoleUser ru WHERE ru.orole.name = '").append(groupName).append("'");
        List<String> groupUsers = emw.createQuery(query.toString(), loggedUser).getResultList();
        return groupUsers;
    }

    private void checkEntityListSecurity(List<BaseEntity> entities, OUser loggedUser) throws UserNotAuthorizedException {
        Set<String> securedEntitiesClassList = new HashSet<>();
        for (BaseEntity entity : entities) {
            if (securedEntitiesClassList.contains(entity.getClassName())) {
                continue;
            }
            securedEntitiesClassList.add(entity.getClassName());
            checkSecurity(entity, loggedUser);
        }

    }

    @Override
    public BaseEntityTranslation getEntityTranslation(String entityName, long dbid, OUser loggedUser) {
        StringBuilder query = new StringBuilder("SELECT entityTranslation FROM ").append(entityName).append("Translation entityTranslation WHERE entityTranslation.entityDBID = ").append(dbid);
        try {
            BaseEntityTranslation entityTranslation = null;
            List<BaseEntityTranslation> entityTranslationResults = (List<BaseEntityTranslation>) emw.createQuery(query.toString(), loggedUser).getResultList();
            if (entityTranslationResults != null && !entityTranslationResults.isEmpty()) {
                entityTranslation = entityTranslationResults.get(0);
            }
            return entityTranslation;
        } catch (NoResultException ex) {
            logger.debug(null, ex);
            return null;
        }
    }
}
