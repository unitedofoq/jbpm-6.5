/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.entitysetup;

/**
 *
 * @author melsayed
 */
public class OEntityTreeDDField {
    private Class fieldType;
    private String DDID;
    private String fieldExpression;

    /**
     * @return the fieldType
     */
    public Class getFieldType() {
        return fieldType;
    }

    /**
     * @param fieldType the fieldType to set
     */
    public void setFieldType(Class fieldType) {
        this.fieldType = fieldType;
    }

    /**
     * @return the DDID
     */
    public String getDDID() {
        return DDID;
    }

    /**
     * @param DDID the DDID to set
     */
    public void setDDID(String DDID) {
        this.DDID = DDID;
    }

    /**
     * @return the fieldExpression
     */
    public String getFieldExpression() {
        return fieldExpression;
    }

    /**
     * @param fieldExpression the fieldExpression to set
     */
    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }
}
