/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitybase;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;

import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.hat.Hat;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author aelzaher
 */
public class BaseEntityMetaData {

    final static Logger logger = LoggerFactory.getLogger(BaseEntityMetaData.class);
    //<editor-fold defaultstate="collapsed" desc="Cache">
    public static Hashtable<String, List<Field>> allFields = new Hashtable<String, List<Field>>();
    public static Hashtable<String, List<Field>> translatableFields = new Hashtable<String, List<Field>>();
    public static Hashtable<String, List<Field>> selfFields = new Hashtable<String, List<Field>>();
    public static Hashtable<String, List<Field>> uniqueFields = new Hashtable<String, List<Field>>();
    public static Hashtable<String, List<Field>> mandatoryFields = new Hashtable<String, List<Field>>();
    public static Hashtable<String, List<Field>> xToOneRelatedFields = new Hashtable<String, List<Field>>();
    public static Hashtable<String, List<String>> xToManyRelatedEntities = new Hashtable<String, List<String>>();
    public static Hashtable<String, List<String>> relatedEntities = new Hashtable<String, List<String>>();
    public static Hashtable<String, List<ExpressionFieldInfo>> fieldExpressionParsed = new Hashtable<String, List<ExpressionFieldInfo>>();
    public static Hashtable<String, List<Field>> objectTypeFields = new Hashtable<String, List<Field>>();
    public static Hashtable<String, List<String>> tableName = new Hashtable<String, List<String>>();
    public static Hashtable<String, String> vcModuleExpression = new Hashtable<String, String>();
    public static Hashtable<String, String> fieldDDName = new Hashtable<String, String>();
    public static Hashtable<String, String> fieldColumnName = new Hashtable<String, String>();
    public static Hashtable<String, List<Class>> superClasses = new Hashtable<String, List<Class>>();
    public static Hashtable<String, List<String>> parentFieldName = new Hashtable<String, List<String>>();
    public static Hashtable<String, List<String>> childFieldName = new Hashtable<String, List<String>>();
    public static Hashtable<String, List<String>> childEntityClassPath = new Hashtable<String, List<String>>();
    public static Hashtable<String, List<String>> parentClassPath = new Hashtable<String, List<String>>();
    public static Hashtable<String, List<String>> parentClassName = new Hashtable<String, List<String>>();
    public static Hashtable<String, FABSEntitySpecs> fabsEntitySpecs = new Hashtable<String, FABSEntitySpecs>();
    public static Hashtable<String, Hashtable<String, List<Field>>> classFieldsInSameTable
            = new Hashtable<String, Hashtable<String, List<Field>>>();
    public static Hashtable<String, List<String>> discrimnatorFieldTableData
            = new Hashtable<String, List<String>>();
//    public static Class getFieldType(Field field) {
//    public static Class getFieldObjectType(Field field)
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Field">
    //<editor-fold defaultstate="collapsed" desc="Cached">
    // <editor-fold defaultstate="collapsed" desc="All">
    /**
     * Get a list of ALL class fields Including fields of super classes, public,
     * private, and protected <br> Is used to get the fields Is used instead of:
     * 1- getDeclaredFields: doesn't get the super class fields 2- getAllFields:
     * gets only public fields
     */
    public static List<Field> getAllFields(BaseEntity entity) {
        return getClassFields(entity.getClass());
    }

    /**
     * Get all the fields of the class and its super classes
     *
     * If you have the entity instance, then call BaseEntity.getAllFields()
     * instead, it's the same functionality, but probably of a better
     * performance
     */
    public static List<Field> getClassFields(Class cls) {
        logger.debug("Entering");
        long x = System.currentTimeMillis();
        List<Field> allFieldsLoc;
        String className = cls.getSimpleName();
        allFieldsLoc = allFields.get(className);
        allFields.clear();
        if (allFieldsLoc != null) {
            logger.debug("Returning with allFieldLoc of size: {}", allFieldsLoc.size());
            return allFieldsLoc;
        }
        allFieldsLoc = new ArrayList<Field>();
        allFieldsLoc.addAll(Arrays.asList(cls.getDeclaredFields()));
        Class superClass = cls.getSuperclass();
        if (superClass == null) {
            logger.debug("Returning with allFieldLoc of size: {}", allFieldsLoc.size());
            return allFieldsLoc;
        }
        while ((superClass.getName().contains("unitedofoq"))) {
            allFieldsLoc.addAll(Arrays.asList(superClass.getDeclaredFields()));
            superClass = superClass.getSuperclass();
        }
        allFields.put(className, allFieldsLoc);
        if (allFieldsLoc != null) {
            logger.debug("Returning with allFieldLoc of size: {}", allFieldsLoc.size());
        }
        return allFieldsLoc;
    }

    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="Translatable">
    /**
     * Get a list of ALL class Translatable fields Including fields of super
     * classes, public, private, and protected
     */
    public static List<Field> getTranslatableFields(BaseEntity entity) {
        // Generate the list
        logger.debug("Entering");
        long x = System.currentTimeMillis();
        List<Field> translatableField = translatableFields.get(entity.getClassName());
        if (translatableField != null) {
            logger.debug("Returning with translatableField of size: {}", translatableField.size());
            return translatableField;
        }
        translatableField = new ArrayList<Field>();
        Field field;
        List<Field> fields = BaseEntityMetaData.getClassFields(entity.getClass());
        for (int i = 0; i < fields.size(); i++) {
            field = fields.get(i);
            if (field.getAnnotation(Translatable.class) != null) {
                translatableField.add(field);
            }
        }
        translatableFields.put(entity.getClassName(), translatableField);
        logger.debug("Returning with translatableField of size: {}", translatableField.size());
        return translatableField;
    }
    // </editor-fold >

    //<editor-fold defaultstate="collapsed" desc="Self">
    public static List<Field> getSelfFields(Class cls) {
        logger.debug("Entering");
        long x = System.currentTimeMillis();
        List<Field> selfField = selfFields.get(cls.getSimpleName());
        if (selfField != null) {
            logger.debug("Returning with selfField of size: {}", selfField.size());
            return selfField;
        }
        List<Field> fields = getClassFields(cls);
        selfField = new ArrayList<Field>();
        for (Field fld : fields) {
            if (fld.getAnnotation(Hat.class) != null) {
                Hat hat = (Hat) fld.getAnnotation(Hat.class);
                if (hat.type() != null && hat.type().length > 0) {
                    for (int i = 0; i < hat.type().length; i++) {
                        Hat.HatType type = hat.type()[i];
                        if (type == Hat.HatType.SELF) {
                            selfField.add(fld);
                            break;
                        }
                    }

                }

            }
        }
        selfFields.put(cls.getSimpleName(), selfField);
        if (selfField != null) {
            logger.debug("Returning with selfField of size: {}", selfField.size());
        }

        return selfField;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Unique">
    /**
     * Returns list of the unique fields in the class <br> It doesn't return the
     * unique constraint fields
     *
     * @param entityClass
     * @return Empty List if no unqiue fields found or Error, otherwise, filled
     * list
     */
    public static List getUniqueFields(Class entityClass) {
        logger.debug("Entering");
        long x = System.currentTimeMillis();
        List<Field> uniqueFieldNames = uniqueFields.get(entityClass.getSimpleName());
        if (uniqueFieldNames != null) {
            logger.debug("Returning with uniqueFieldNames of size: {}", uniqueFieldNames.size());
            return uniqueFieldNames;
        }
        List<Field> entityFields = getClassFields(entityClass);
        uniqueFieldNames = new ArrayList<Field>();
        for (Field field : entityFields) {
            if (isFieldUnique(field)) {
                uniqueFieldNames.add(field);
            }
        }
        uniqueFields.put(entityClass.getSimpleName(), uniqueFieldNames);
        logger.debug("Returning with uniqueFieldNames of size: {}", uniqueFieldNames.size());
        return uniqueFieldNames;
    }

    public static List<Field> getUniqueFields(BaseEntity entity) {
        return (List<Field>) getUniqueFields(entity.getClass());
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Mandatory">
    /**
     * Get a list of ALL class Mandatory fields (nullable = false) Including
     * fields of super classes, public, private, protected, and join columns
     */
    public static List<Field> getMandatoryFields(BaseEntity entity) {
        logger.debug("Entering");
        // Generate the list
        long x = System.currentTimeMillis();
        List<Field> mandatoryField = mandatoryFields.get(entity.getClassName());
        if (mandatoryField != null) {
            logger.debug("Returning with mandatoryField of size: {}", mandatoryField.size());
            return mandatoryField;
        }
        mandatoryField = new ArrayList<Field>();
        List<Field> fields = getAllFields(entity);
        Field field;
        for (int i = 0; i < fields.size(); i++) {
            field = fields.get(i);
            if (isFieldMandatory(field)) {
                mandatoryField.add(field);
            }
        }
        mandatoryFields.put(entity.getClassName(), mandatoryField);
        if (mandatoryField != null) {
            logger.debug("Returning with mandatoryField of size: {}", mandatoryField.size());
        }

        return mandatoryField;
    }
    // </editor-fold >

    //<editor-fold defaultstate="collapsed" desc="XToOneRelated">
    /**
     * Return a List<Field> of OneToOne & ManyToOne Related Fields
     */
    public static List getXToOneRelatedFields(Class entityClass) {
        logger.debug("Entering");
        long x = System.currentTimeMillis();
        List relatedFields = xToOneRelatedFields.get(entityClass.getSimpleName());
        if (relatedFields != null) {
            logger.debug("Returning with relatedFields of size: {}", relatedFields.size());
            return relatedFields;
        }
        relatedFields = new ArrayList<Field>();
        List<Field> fields = getClassFields(entityClass);
        for (Field field : fields) {
            if (field.getAnnotation(OneToOne.class) != null
                    || field.getAnnotation(ManyToOne.class) != null) {
                //if (!(field.getType().getName().equalsIgnoreCase(entityClass.getName())))
                relatedFields.add(field);
            }
        }
        xToOneRelatedFields.put(entityClass.getSimpleName(), (List<Field>) relatedFields);
        logger.debug("Returning with relatedFields of size: {}", relatedFields.size());
        return relatedFields;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="DDName">
    /**
     * Returns {@link #getFieldDDName(Class, String)}
     */
    public static String getFieldDDName(
            BaseEntity entity, String fieldExpreission) {
        return getFieldDDName(entity.getClass(), fieldExpreission);
    }

    /**
     * Calls the get<field name>DD() function of the most leaf field in the
     * fieldExpression
     *
     * <br>fieldExpression left most field (if has ".") should be owned by
     * entity. <br>If you are sure it's a field name and not field expression,
     * you better use getFieldDDName(String fieldName).
     *
     * @param entity BaseEntity to which the first (most left) field in the
     * expression belongs
     * @param fieldExpression the field expression, which allows nested fields
     * (i.e. contains ".")
     *
     * @return Field DD Name <br> "" : Field Has No DD <br> null: Error
     */
    public static String getFieldDDName(
            Class entityCls, String fieldExpreission) {
        logger.debug("Entering");
        long x = System.currentTimeMillis();
        if (fieldExpreission == null) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Field Expression is Null. Entity Class: {}", entityCls);
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        }
        String ddName = fieldDDName.get(entityCls.getSimpleName() + "_" + fieldExpreission);
        if (ddName != null) {
            logger.debug("Returning with ddName: {}", ddName);
            return ddName;
        }
        ExpressionFieldInfo leafFieldInfo = null;
        try {
            // Use parseFieldExpression to parse the field expression to make
            // sure of the field owner class (could be a super class) to get
            // the method from
            List<ExpressionFieldInfo> fieldHierarchy
                    = parseFieldExpression(entityCls, fieldExpreission);
            if (fieldHierarchy == null) {
                // Error
                logger.debug("Returning with empty string");
                return "";
            }
            leafFieldInfo = fieldHierarchy.get(fieldHierarchy.size() - 1);
            Field leafField = leafFieldInfo.field;
            String leafFieldName = leafFieldInfo.fieldName;
            String leafFieldDDGetterMethodName
                    = "get"
                    + Character.toString(leafFieldName.charAt(0)).toUpperCase()
                    + leafFieldName.substring(1)
                    + "DD";
            Method leafFieldDDGetterMethod = leafField.getDeclaringClass().getMethod(
                    leafFieldDDGetterMethodName);
            // Use leafFieldInfo.fieldClass and not Method.getDeclaringClass
            // as the method may be decaled in ancestor abstract class which
            // can not be instantiated
            ddName = (String) leafFieldDDGetterMethod.invoke(
                    leafFieldInfo.fieldClass.newInstance());
            fieldDDName.put(entityCls.getSimpleName() + "_" + fieldExpreission, ddName);
            logger.debug("Returning with ddName: {}", ddName);
            return ddName;
        } catch (NoSuchMethodException ex) {
            // No DD getter
            // Note: Don't log the exception, it's the caller responsibility to determine
            // whether it's expected or not
            logger.debug("Returning with empty string");
            return "";
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            logger.trace("Entity Class: {}, Field Expression: {}", entityCls, fieldExpreission);
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="columnName">
    /**
     * Returns the column name of the field in the database table, in lower
     * case. The column name is concluded from the field specifications. No
     * validation is done that the column really exists in the database.
     *
     * @param field
     * @return null: if error <br>Empty: if list, or Transient
     *
     * @see #getEntityTableName()
     */
    public static String getFieldColumnName(Field field) {
        if (field == null) {
            return null;
        }
        if (field.getAnnotation(Transient.class) != null) {
            return "";
        }
        if (getFieldType(field) == List.class) {
            return "";
        }
        Annotation tempAnnot = field.getAnnotation(Column.class);
        if (tempAnnot != null) {
            String colName = ((Column) tempAnnot).name();
            if (colName != null && !colName.isEmpty()) {
                return colName.toLowerCase();
            }
            return field.getName().toLowerCase();
        }
        tempAnnot = field.getAnnotation(JoinColumn.class);
        if (tempAnnot != null) {
            if (getFieldType(field) == List.class) {
                return "";
            }
            String colName = ((JoinColumn) tempAnnot).name();
            if (colName != null && !colName.equals("")) // is "" for some parent fields!
            {
                return colName.toLowerCase();
            }
            return (field.getName() + "_DBID").toLowerCase();
        }
        if (BaseEntity.class.isAssignableFrom(field.getType())) {
            return (field.getName() + "_DBID").toLowerCase();
        }
        return field.getName().toLowerCase();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ClassObjectTypeFields">
    /**
     * Return a list of the entity fields that are either of type objectTypeCls
     * or are lists of element type objectTypeCls
     *
     * @param objectTypeCls
     * @return
     */
    public static List<Field> getClassObjectTypeFields(Class cls, Class objectTypeCls) {
        logger.debug("Entering");
        long x = System.currentTimeMillis();
        try {
            List<Field> objTypeFields = objectTypeFields.get(cls.getSimpleName() + "_" + objectTypeCls.getSimpleName());
            if (null != cls && null != objectTypeCls) {
                logger.trace("ClassName {} ObjectType {}", cls.getSimpleName(), objectTypeCls.getSimpleName());
            }
            if (objTypeFields != null) {
                logger.debug("Returning with objTypeFields of size: {}", objTypeFields.size());
                return objTypeFields;
            }
            List<Field> clsFields = getClassFields(cls);
            objTypeFields = new ArrayList<Field>();
            int iAllFieldsCount = clsFields.size();
            for (int i = 0; i < iAllFieldsCount; i++) {
                if (getFieldObjectType(clsFields.get(i)).equals(objectTypeCls)) {
                    objTypeFields.add(clsFields.get(i));
                }
            }
            objectTypeFields.put(cls.getSimpleName() + "_" + objectTypeCls.getSimpleName(), objTypeFields);
            if (null != cls && null != objectTypeCls) {
                logger.trace("ClassName {} ObjectType {}", cls.getSimpleName(), objectTypeCls.getSimpleName());
            }
            if (objTypeFields != null) {
                logger.debug("Returning with objTypeFields of size: {}", objTypeFields.size());
            }

            return objTypeFields;
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.trace("Class: {},Object Type Class: {}", cls, objectTypeCls);
            logger.debug("Returning with Null");
            return null;
        }
    }
    //</editor-fold>

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="FieldType">
    /**
     * This function returns the following: - Field type if it's not a list -
     * 'List' if the field is list
     */
    public static Class getFieldType(Field field) {
        if (field.getAnnotation(OneToMany.class) != null
                || field.getAnnotation(ManyToMany.class) != null) {
            return List.class;
        } else {
            return field.getType();
        }
    }

    /**
     * This function returns the following: - Field type if it's not a list -
     * List Element type if the field is list
     */
    public static Class getFieldObjectType(Field field) {
        logger.debug("Entering");
        if (field.getAnnotation(OneToMany.class) != null
                || field.getAnnotation(ManyToMany.class) != null) {
            Type returnType = field.getGenericType();
            // Should be true (returnType instanceof ParameterizedType)
            ParameterizedType type = (ParameterizedType) returnType;
            Type[] typeArguments = type.getActualTypeArguments();
            if (typeArguments.length > 1) {
                logger.warn("Unsupported more than one in the argument. Field: {}", field);
                logger.debug("Returning with Null");
                return null;
            } else {
                logger.debug("Returning");
                return (Class) typeArguments[0];
            }
        } else {
            if (null != field.getType()) {
                logger.debug("Returning with: {}", field.getType());
            }
            return field.getType();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ClassField">
    /**
     * Returns the Field object corresponding to fieldName
     *
     * Search for the field in the cls and its super classes as well Return null
     * if not found at all
     *
     * @param cls the class to which the fields of fieldName belongs
     * @param fieldName is a direct field and not a field expression, i.e.
     * shouldn't contain "." otherwise it won't be found
     */
    public static Field getClassField(Class cls, String fieldName) {
        Class superClass = cls;
        Field fld = null;
        boolean bFieldFound = false;
        while (!bFieldFound) {
            try {
                // Use native function getDeclaredField() instead of
                // looping on BaseEntity.getClassFields for performance
                // and systematic purposes
                fld = superClass.getDeclaredField(fieldName);
                // no exception thown, so, field should be got
                bFieldFound = true;
                return fld;
            } catch (NoSuchFieldException ex) {
                if (superClass == null) // No super class
                {
                    return null;
                } else if (superClass.getName().contains("unitedofoq")) // Super class still OFOQ. search in ancestors
                {
                    superClass = superClass.getSuperclass();
                } else // Super class is not OFOQ, no hope to find field
                {
                    return null;
                }
            }
        }
        return fld;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="is">
    /**
     * Return true if the field is not nullable
     *
     * It considers both the Column & JoinColumn
     *
     * @param field The field being checked
     */
    public static boolean isFieldMandatory(Field field) {
        Annotation annot;
        // Check if field is Column
        annot = field.getAnnotation(Column.class);
        if (annot != null) {
            Column column = (Column) annot;
            if (!column.nullable()) {
                return true;
            } else {
                return false;
            }
        }

        // Check if field is JoinColumn
        annot = field.getAnnotation(JoinColumn.class);
        if (annot != null) {
            JoinColumn joinColumn = (JoinColumn) annot;
            if (!joinColumn.nullable()) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static Boolean isFieldOneToOneChild(Class entityClass, String fieldName) {
        List<String> childFieldNames
                = getChildEntityFieldName(entityClass.getName());
        if (childFieldNames.indexOf(fieldName) < 0) // Field is not a child field
        {
            return false;
        }
        // Field is child
        Field field = getClassField(entityClass, fieldName);
        if (field.getAnnotation(OneToOne.class) == null) // Field is not OneToOne
        {
            return false;
        }

        // Field is one to one child
        return true;
    }

    public static boolean isFieldUnique(Field field) {
        if (((field.getAnnotation(Column.class) != null) && (field.getAnnotation(Column.class).unique()))
                || ((field.getAnnotation(JoinColumn.class) != null) && (field.getAnnotation(JoinColumn.class).unique()))) {
            return true;
        }
        return false;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ParentEntityFieldName">
    /**
     * Get a list of Parent Entities Field Names in & of the entity of
     * entityClassPath. <br>Parents are defined using
     *
     * @ParentEntity annotation <br>No exceptions caught inside, caller should
     * take care of that
     *
     * @param entityClassPath Full class path of the child entity
     *
     * @return null: No parent found, or error List<String>: List of parents
     * found
     */
    public static List getParentEntityFieldName(String entityClassPath) {
        logger.debug("Entering");
        if (entityClassPath == null) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Null entityClassPth Passed");
            logger.debug("Returning with Null");
            // </editor-fold>
            return null;
        }
        List<String> parentEntityFieldNames = new ArrayList();
        try {
            Class entityClass = Class.forName(entityClassPath);
            ParentEntity parentEntityAnnot = (ParentEntity) entityClass.getAnnotation(ParentEntity.class);
            if (parentEntityAnnot != null) // Parent Entity Annotation found
            // Get the parents
            {
                Collections.addAll(parentEntityFieldNames, parentEntityAnnot.fields());
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            logger.trace("Entity Class Path: {}", entityClassPath);
            // </editor-fold>
        }
        if (parentEntityFieldNames.isEmpty()) // Entity Has No Parent
        // Return null
        {
            logger.debug("Returning with Null");
            return null;
        } else {
            if (parentEntityFieldNames != null) {
                logger.debug("Returning with parentEntityFieldNames of size: {}", parentEntityFieldNames.size());
            }
            return parentEntityFieldNames;
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="MeasuarableFieldUnit">
    public static String getMeasuarableFieldUnit(String fieldExpression, Class entityClass) {
        List<ExpressionFieldInfo> expressionFieldInfos = parseFieldExpression(entityClass,
                fieldExpression);
        ExpressionFieldInfo expressionFieldInfo = expressionFieldInfos.get(expressionFieldInfos.size() - 1);
        Field field = expressionFieldInfo.field;
        if (field.getAnnotation(MeasurableField.class) != null) {
            MeasurableField measurableField = field.getAnnotation(MeasurableField.class);
            return measurableField.name();
        }
        return null;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="UniqueConstraintFieldNames">
    /**
     * Returns list of the fields names of the first found unique constraint
     *
     * @param entityClass
     * @return Empty List if no unqiue fields found or in case in error,
     * otherwise, filled list
     */
    public static List getUniqueConstraintFieldNames(Class entityClass) {
        logger.debug("Entering");
        List<String> uniqueFieldNames = new ArrayList<String>();
        Table tableAnnotation = (Table) entityClass.getAnnotation(Table.class);
        if (tableAnnotation == null) {
            if (uniqueFieldNames != null) {
            logger.debug("Returning with uniqueFieldName of size: {}", uniqueFieldNames.size());
            }

            return uniqueFieldNames;
        }
        UniqueConstraint[] tableUniqueConstraints = tableAnnotation.uniqueConstraints();
        // If contains unique contstrains:
        if (tableUniqueConstraints.length == 0) {
            if (uniqueFieldNames != null) {
            logger.debug("Returning with uniqueFieldName of size: {}", uniqueFieldNames.size());
            }
            return uniqueFieldNames;
        }
        if (tableUniqueConstraints.length > 1) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.debug("More Than One Unique Constraint Found, Will Return First One Only. Entity Class: {}", entityClass);
            // </editor-fold>
        }
        String[] uniqueConstraintColumnName = tableUniqueConstraints[0].columnNames();
        for (String uniqueColumnName : uniqueConstraintColumnName) {
            uniqueFieldNames.add(uniqueColumnName);
        }
        if (uniqueFieldNames != null) {
            logger.debug("Returning with uniqueFieldName of size: {}", uniqueFieldNames.size());
        }

        return uniqueFieldNames;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ChildFieldInParentEntity">
    String chiledFieldInParentEntityName = null;

    public static Field getChildFieldInParentEntity(
            BaseEntity childEntity, Field parentFieldInChildEntity, BaseEntity parentEntity) {
        logger.debug("Entering");
        // Validate arguments
        if (childEntity == null || parentFieldInChildEntity == null || parentEntity == null) {
            // <editor-fold defaultstate="collapsed" desc="log">
            logger.warn("Invalid Argument. childEntity: {}, parentFieldInChildEntity: {}, parentEntity: {} ", childEntity, parentFieldInChildEntity, parentEntity);
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        }
        // Get the parent child field names
        List<String> childFieldNames = getChildEntityFieldName(
                parentEntity.getClass().getName());
        for (String childFieldName : childFieldNames) {
            Field childFieldInParentEntity = getClassField(
                    parentEntity.getClass(), childFieldName);
            Class childFieldClass = getFieldObjectType(childFieldInParentEntity);
            if (!childFieldClass.equals(childEntity.getClass())) // Field class is not the same class as childEntity
            {
                continue;
            }

            // Check if it's OneToMany
            OneToMany one2MAnnot = childFieldInParentEntity.getAnnotation(OneToMany.class);
            if (one2MAnnot != null) {
                if (one2MAnnot.mappedBy().equals(parentFieldInChildEntity.getName())) {

                    if (childFieldInParentEntity != null) {
                       logger.debug("Returning with entity with Name: {}", childFieldInParentEntity.getName());
                    }
                    else{
                        logger.debug("Returning with Null child Field In Parent Entity");
                    }
                    return childFieldInParentEntity;
                }
            }

            // Check if it's OneToOne
            OneToOne one2OneAnnot = childFieldInParentEntity.getAnnotation(OneToOne.class);
            if (one2OneAnnot != null) {
                if (one2OneAnnot.mappedBy().equals(parentFieldInChildEntity.getName())) {
                    if (childFieldInParentEntity != null) {
                        logger.debug("Returning with entity with Name: {}", childFieldInParentEntity.getName());
                    }else{
                        logger.debug("Returning with Null child Field In Parent Entity");
                    }
                    return childFieldInParentEntity;
                }
            }

            // <editor-fold defaultstate="collapsed" desc="log">
            if (one2MAnnot == null && one2OneAnnot == null) {
                if (null != childFieldInParentEntity) {
                    logger.warn("Child Field Is Neither OneToMany nor OneToOne! Field: {}", childFieldInParentEntity.getName());
                }
            }
            // </editor-fold>
        }
        logger.debug("Returning with Null");
        return null;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ChildEntityFieldName">
    /**
     * Get a list of Child Entities Fields Names in & of the entity of
     * entityClassPath. <br> Children are defined using
     *
     * @ChildEntity annotation <br> Super class children are included
     * @param entityClassPath
     * @return <br> List of all children fields names if successful <br> Empty
     * List: if not children found <br> Null: Error
     */
    public static List<String> getChildEntityFieldName(String entityClassPath) {
        long x = System.currentTimeMillis();
        Class entityClass = null;
        try {
            entityClass = Class.forName(entityClassPath);
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            logger.trace("Entity Class Path: {}", entityClassPath);
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        }
        List<String> childEntityFieldNames = childFieldName.get(entityClassPath);
        if (childEntityFieldNames != null) {
            logger.debug("Returning with childEntityFieldNames of size: {}", childEntityFieldNames.size());
            return childEntityFieldNames;
        }
        childEntityFieldNames = new ArrayList();
        ChildEntity childEntityAnnot = (ChildEntity) entityClass.getAnnotation(ChildEntity.class);
        if (childEntityAnnot == null) {
            if (childEntityFieldNames != null) {
            logger.debug("Returning with childEntityFieldNames of size: {}", childEntityFieldNames.size());
            }
            return childEntityFieldNames;
        }
        Collections.addAll(childEntityFieldNames, childEntityAnnot.fields());

        // Get the children of the superclass if found
        Class superClass = entityClass.getSuperclass();
        while (superClass != null) {
            if (!superClass.getName().contains("unitedofoq")) {
                break;
            }

            // Super class still OFOQ. search in ancestors
            superClass.getSuperclass().getAnnotation(ChildEntity.class);
            Annotation[] classAnnotations = superClass.getAnnotations();
            for (Annotation annotation : classAnnotations) {
                if (annotation instanceof ChildEntity) {
                    for (String fld : ((ChildEntity) annotation).fields()) {
                        if (childEntityFieldNames.contains(fld)) {
                            continue;
                        } else {
                            childEntityFieldNames.add(fld);
                        }
                    }
//                    Collections.addAll(childEntityFieldNames,
//                            ((ChildEntity) annotation).fields());
                    break;
                }
            }
            // Get superClass's superClass
            superClass = superClass.getSuperclass();
        }
        childFieldName.put(entityClassPath, childEntityFieldNames);
        if (childEntityFieldNames != null) {
            logger.debug("Returning with childEntityFieldNames of size: {}", childEntityFieldNames.size());
        }

        return childEntityFieldNames;
    }
    //</editor-fold>

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Class">
    //<editor-fold defaultstate="collapsed" desc="Cached">
    //<editor-fold defaultstate="collapsed" desc="super">
    public static List<Class> getSuperClasses(Class cls) {
        logger.debug("Entering");
        long x = System.currentTimeMillis();
        List<Class> inheritanceParents = superClasses.get(cls.getSimpleName());
        if (inheritanceParents != null) {
            logger.debug("Returning with inheritanceParents of size: {}", inheritanceParents.size());
            return inheritanceParents;
        }
        inheritanceParents = new ArrayList<Class>();
        if (cls.getSuperclass() != null && !(cls.getSuperclass() == BaseEntity.class
                || cls.getSuperclass() == BusinessObjectBaseEntity.class
                || cls.getSuperclass() == ObjectBaseEntity.class)) {
            inheritanceParents.add(cls.getSuperclass());
            inheritanceParents.addAll(getSuperClasses(cls.getSuperclass()));
        }
        superClasses.put(cls.getSimpleName(), inheritanceParents);
        if (inheritanceParents != null) {
            logger.debug("Returning with inheritanceParents of size: {}", inheritanceParents.size());
        }

        return inheritanceParents;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getParentClassPath">
    /**
     * Get list of Parent Entity Class Paths in & of the entity of
     * entityClassPath. <br> Parent is defined using
     *
     * @ParentEntity annotation
     *
     * @param entityClassPath Full class path of the child entity
     *
     * @return null: No parent found <br>Else the parent found
     */
    public static List getParentClassPath(String entityClassPath) {
        logger.debug("Entering");
        long x = System.currentTimeMillis();
        try {
            List<String> parentEntityFieldNames = parentClassPath.get(entityClassPath);
            if (parentEntityFieldNames != null) {
                logger.debug("Returning with parentEntityFieldNames of size: {}", parentEntityFieldNames.size());
                return parentEntityFieldNames;
            }
            parentEntityFieldNames = getParentEntityFieldName(entityClassPath);
            if (parentEntityFieldNames == null) {
                logger.debug("Returning with Null");
                return null;
            }
            ArrayList<String> parentClassPaths = new ArrayList<String>();
            for (String parentEntityFieldName : parentEntityFieldNames) {
                parentClassPaths.add(getClassField(Class.forName(entityClassPath), parentEntityFieldName).getType().getName());
            }
            parentClassPath.put(entityClassPath, parentClassPaths);
            if (parentClassPaths != null) {
                logger.debug("Returning with parentClassPaths of size: {}", parentClassPaths.size());
            }

            return parentClassPaths;
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            logger.trace("Entity Class Path: {}", entityClassPath);
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ParentClassName">
    /**
     * Returns list of parent class names, based on call to
     * {@link BaseEntity#getParentClassPath(String) }
     */
    public static List getParentClassName(String entityClassPath) {
        logger.debug("Entering");
        long x = System.currentTimeMillis();
        try {
            List<String> parentEntityClassNames = parentClassName.get(entityClassPath);
            if (parentEntityClassNames != null) {
                logger.debug("Returning with parentEntityClassNames of size: {}", parentEntityClassNames.size());
                return parentEntityClassNames;
            }
            parentEntityClassNames = new ArrayList<String>();
            List<String> parentEntityClassPaths = getParentClassPath(entityClassPath);
            if (parentEntityClassPaths != null && !parentEntityClassPaths.isEmpty()) {
                if (parentEntityClassPaths.size() == 1) {
                    // Entity has only one parent
                    parentEntityClassNames.add(parentEntityClassPaths.get(0).substring(
                            parentEntityClassPaths.get(0).lastIndexOf(".") + 1));
                } else if (parentEntityClassPaths.size() == 2) {
                    // Entity has two parents
                    parentEntityClassNames.add(parentEntityClassPaths.get(0).substring(
                            parentEntityClassPaths.get(0).lastIndexOf(".") + 1));
                    parentEntityClassNames.add(parentEntityClassPaths.get(1).substring(
                            parentEntityClassPaths.get(1).lastIndexOf(".") + 1));
                }
            }
            parentClassName.put(entityClassPath, parentEntityClassNames);
            if (parentEntityClassNames != null) {
                logger.debug("Returning with parentEntityClassNames of size: {}", parentEntityClassNames.size());
            }

            return parentEntityClassNames;
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            logger.trace("Entity Class Path: {}", entityClassPath);
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ChildEntityClassPath">
    /**
     * Get a list of Child Entities Fields Class Paths in & of the entity of
     * entityClassPath. <br> Children are defined using
     *
     * @ChildEntity annotation <br> If child entity field is a list, then the
     * field element type class path is got
     * @param entityClassPath
     * @return
     */
    public static List getChildEntityClassPath(String entityClassPath) {
        logger.debug("Entering");
        long x = System.currentTimeMillis();
        List<String> childEntityClassPaths = childEntityClassPath.get(entityClassPath);
        if (childEntityClassPaths != null) {
            logger.debug("Returning with childEntityClassPaths of size: {}", childEntityClassPaths.size());
            return childEntityClassPaths;
        }
        childEntityClassPaths = new ArrayList();
        List<String> childEntityFieldNames = getChildEntityFieldName(entityClassPath);
        try {
            if (childEntityFieldNames == null) {
                logger.debug("Returning with Null");
                return null;
            }

            Class entityClass = Class.forName(entityClassPath);
            for (String childEntityFieldName : childEntityFieldNames) {
                Class childFieldClass;
                childFieldClass = getFieldObjectType(
                        getClassField(entityClass, childEntityFieldName));
                childEntityClassPaths.add(childFieldClass.getName());
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("EXception thrown: ", ex);
            logger.trace("Entity Class Path: {}", entityClassPath);
            // </editor-fold>
        }
        childEntityClassPath.put(entityClassPath, childEntityClassPaths);
        if (childEntityClassPaths != null) {
            logger.debug("Returning with childEntityClassPaths of size: {}", childEntityClassPaths.size());
        }

        return childEntityClassPaths;
    }
    //</editor-fold>

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="getAnnotation">
    /**
     * Returns the annotation if found, checking all super classes as well
     *
     * @param entityClass Class for which annotation is being checked
     * @param annotClass Class for which we search
     * @return Annotation class found, success <br>null: Error or no annotation
     * found
     */
    public static Annotation getAnnotation(Class entityClass, Class annotClass) {
        logger.debug("Entering");
        try {
            Annotation annot = null;
            annot = entityClass.getAnnotation(annotClass);
            if (annot != null) {
                // annotClass is defined for the entity class
                logger.debug("Returning");
                return annot;
            }
            Class superCls = entityClass.getSuperclass();
            while (superCls != null) {
                if (superCls.getSimpleName().equals("BaseEntity")) {
                    logger.debug("Returning with Null");
                    return null; // annotClass is not defined for entityClass
                }
                annot = superCls.getAnnotation(annotClass);
                if (annot != null) {
                    // annotClass is defined for the entity class
                    logger.debug("Returning");
                    return annot;
                }
                // Check more super classes
                superCls = superCls.getSuperclass();
            }
            logger.debug("Returning with Null");
            return null; // annotClass is not defined for entityClass
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            logger.trace("entityClass: {}, annotClass: {}", entityClass, annotClass);
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;   // Error
        }

    }
    //</editor-fold>

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="parseFieldExpression">
    /**
     * Return a Field list of the passed field expression, first element is the
     * most left field
     *
     * It supports nested fields (i.e. expression contains ".") It supports
     * fields of all types (including lists) It supports fields of class
     * ancestors First field in the expression should be in the cls
     *
     * @param ownerClass Class to which the first (most left) field in the
     * expression belongs
     * @param fieldExpression the field expression, which allows nested fields
     * (i.e. contains ".")
     * @return List of ExpressionFieldInfo objects <br> null : Error
     */
    public static List<ExpressionFieldInfo> parseFieldExpression(
            Class ownerClass, String fieldExpression) {
        logger.debug("Entering");
        long x = System.currentTimeMillis();
        List<ExpressionFieldInfo> fieldHierarchy = fieldExpressionParsed.get(ownerClass.getSimpleName()
                + "_" + fieldExpression);
        if (fieldHierarchy != null) {
            logger.debug("Returning with fieldHierarchy of size: {}", fieldHierarchy.size());
            return fieldHierarchy;
        }
        fieldHierarchy = new ArrayList<ExpressionFieldInfo>();
        if (fieldExpression == null) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Field Expression is Null. Field Class: {}", ownerClass);
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        }
        if (fieldExpression.contains(".")) {
            StringTokenizer stringT = new StringTokenizer(fieldExpression, ".");
            Class fldClass = ownerClass;
            while (stringT.hasMoreTokens()) {
                ExpressionFieldInfo efi = new ExpressionFieldInfo();
                efi.fieldParsedExpression = fieldExpression;
                efi.fieldName = stringT.nextToken();
                efi.fieldClass = fldClass;
                efi.field = getClassField(efi.fieldClass, efi.fieldName);
                if (efi== null && efi.field == null) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.warn("Field Not Found. Field Class: {}, FieldName: {}", fldClass, efi.fieldName);
                    // </editor-fold>
                    logger.debug("Returning with Null");
                    return null; // Error: no further processing can be done
                }
                Translation translation = efi.field.getAnnotation(Translation.class);
                if (translation != null) {
                    Field originalField = getClassField(efi.fieldClass, translation.originalField());
                    if (originalField == null) {
                        logger.warn("Original Field For Translated Field Is Not Found in Entity. Entity: {}, Field: {}", ownerClass, fieldExpression);
                    } else {
                        efi.originalField = originalField;
                    }
                }
                fieldHierarchy.add(efi);
                fldClass = getFieldObjectType(efi.field);
            }
        } else {
            // Field expression has no ".", it's direct field name
            ExpressionFieldInfo efi = new ExpressionFieldInfo();
            efi.fieldParsedExpression = fieldExpression;
            efi.fieldName = fieldExpression;
            efi.fieldClass = ownerClass;
            efi.field = getClassField(ownerClass, fieldExpression);
            if (efi== null && efi.field == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("Field Not Found. Field Class: {}, Field Name: {}", ownerClass, efi.fieldName);
                // </editor-fold>
                logger.debug("Returning with Null");
                return null; // Error: no further processing can be done
            }
            Translation translation = efi.field.getAnnotation(Translation.class);
            if (translation != null) {
                Field originalField = getClassField(efi.fieldClass, translation.originalField());
                if (originalField == null) {
                    logger.warn("Original Field For Translated Field Is Not Found in Entity. Entity: {}, Field: {}", ownerClass, fieldExpression);
                } else {
                    efi.originalField = originalField;
                }
            }
            fieldHierarchy.add(efi);
        }
        fieldExpressionParsed.put(ownerClass.getSimpleName() + "_" + fieldExpression, fieldHierarchy);
        if (fieldHierarchy != null) {
            logger.debug("Returning with fieldHierarchy of size: {}", fieldHierarchy.size());
        }

        return fieldHierarchy;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Entity">
    //<editor-fold defaultstate="collapsed" desc="RelatedEntities">
    public static List getRelatedEntities(Class entityClass) {
        logger.debug("Entering");
        long x = System.currentTimeMillis();
        List relatedEntitiesList = relatedEntities.get(entityClass.getSimpleName());
        if (relatedEntitiesList != null) {
            logger.debug("Returning with relatedEntitiesList of size: {}", relatedEntitiesList.size());
            return relatedEntitiesList;
        }
        relatedEntitiesList = new ArrayList<String>();
        List<Field> fields = getClassFields(entityClass);
        for (Field field : fields) {
            if (field.getAnnotation(OneToOne.class) != null
                    || field.getAnnotation(ManyToOne.class) != null
                    || field.getAnnotation(OneToMany.class) != null
                    || field.getAnnotation(ManyToMany.class) != null) {
                String type = "";
                String fieldGenericType = field.getGenericType().toString();
                if (!fieldGenericType.startsWith("class")) {
                    int startIndex = fieldGenericType.indexOf("<") + 1;
                    int endIndex = fieldGenericType.indexOf(">");
                    type = fieldGenericType.substring(startIndex, endIndex);
                } else {
                    type = field.getType().getName();
                }
                relatedEntitiesList.add(type);
            }
        }
        relatedEntities.put(entityClass.getSimpleName(), relatedEntitiesList);
        if (relatedEntitiesList != null) {
            logger.debug("Returning with relatedEntitiesList of size: {}", relatedEntitiesList.size());
        }

        return relatedEntitiesList;
    }

    /**
     * Return a List<String> of OneToMany & ManyToMany Related Fields Names
     */
    public static List getXToManyRelatedEntities(Class entityClass) {
        long x = System.currentTimeMillis();
        List relatedEntities = xToManyRelatedEntities.get(entityClass.getSimpleName());
        if (relatedEntities != null) {
            logger.debug("Returning withrelatedEntities of size: {}", relatedEntities.size());
            return relatedEntities;
        }
        relatedEntities = new ArrayList<String>();
        List<Field> fields = getClassFields(entityClass);
        for (Field field : fields) {
            if (field.getAnnotation(OneToMany.class) != null
                    || field.getAnnotation(ManyToMany.class) != null) {
                String type = "";
                String fieldGenericType = field.getGenericType().toString();
                if (!fieldGenericType.startsWith("class")) {
                    int startIndex = fieldGenericType.indexOf("<") + 1;
                    int endIndex = fieldGenericType.indexOf(">");
                    type = fieldGenericType.substring(startIndex, endIndex);
                } else {
                    type = field.getType().getName();
                }
                if (!(type.equalsIgnoreCase(entityClass.getName()))) {
                    relatedEntities.add(type);
                }
            }
        }
        xToManyRelatedEntities.put(entityClass.getSimpleName(), relatedEntities);
        if (relatedEntities != null) {
            logger.debug("Returning with relatedEntities of size: {}", relatedEntities.size());
        }

        return relatedEntities;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="EntityTableName">
    public static List<String> getEntityTableName(Class myClass) {
        logger.debug("Entering");
        long x = System.currentTimeMillis();
        List<String> mainDBTablesNames = tableName.get(myClass.getSimpleName());
        if (mainDBTablesNames != null) {
            logger.debug("Returning with mainDBTablesNames of size: {}", mainDBTablesNames.size());
            return mainDBTablesNames;
        }
        mainDBTablesNames = new ArrayList<String>();
        if (myClass.getAnnotation(Table.class) != null
                && ((Table) myClass.getAnnotation(Table.class)).name().isEmpty() == false) {
            // @Table is mentioned explicitly in the entity
            mainDBTablesNames.add(((Table) myClass.getAnnotation(Table.class)).name().toLowerCase());
            tableName.put(myClass.getSimpleName(), mainDBTablesNames);
            if (mainDBTablesNames != null) {
                logger.debug("Returning with mainDBTablesNames of size: {}", mainDBTablesNames.size());
            }

            return mainDBTablesNames;
        }

        Class superClass = myClass.getSuperclass();
        Inheritance inhAnnot;
        ArrayList<Class> superClasses = new ArrayList<Class>();
        while (superClass != null && !superClass.getSimpleName().equals("BaseEntity")) {
            superClasses.add(superClass);
            inhAnnot = (Inheritance) superClass.getAnnotation(Inheritance.class);
            if (inhAnnot != null) {
                if (inhAnnot.strategy().equals(InheritanceType.SINGLE_TABLE)) {
                    // Inheritance Annotation found, for single table
                    // Return super class table name
                    if (superClass.getAnnotation(Table.class) != null) // @Table is mentioned explicitly in the entity
                    {
                        mainDBTablesNames.add(((Table) superClass.getAnnotation(Table.class)).name().toLowerCase());
                        tableName.put(myClass.getSimpleName(), mainDBTablesNames);
                        if (mainDBTablesNames != null) {
                            logger.debug("Returning with mainDBTablesNames of size: {}", mainDBTablesNames.size());
                        }
                        return mainDBTablesNames;
                    } else {
                        mainDBTablesNames.add(superClass.getSimpleName().toLowerCase());
                        tableName.put(myClass.getSimpleName(), mainDBTablesNames);
                        if (mainDBTablesNames != null) {
                            logger.debug("Returning with mainDBTablesNames of size: {}", mainDBTablesNames.size());
                        }

                        return mainDBTablesNames;
                    }
                } else {
                    // Inheritance Annotation found, but not single table
                    // Add myClass Table Name
                    mainDBTablesNames.add(myClass.getSimpleName().toLowerCase());

                    // Add Super Classes Table Names
                    for (Class loopSuperCls : superClasses) {
                        if (loopSuperCls.getAnnotation(Table.class) != null) {
                            // @Table is mentioned explicitly in the entity
                            mainDBTablesNames.add(((Table) loopSuperCls.getAnnotation(Table.class)).name().toLowerCase());
                        } else {
                            mainDBTablesNames.add(loopSuperCls.getSimpleName().toLowerCase());
                        }
                    }
                    tableName.put(myClass.getSimpleName(), mainDBTablesNames);
                    if (mainDBTablesNames != null) {
                        logger.debug("Returning with mainDBTablesNames of size: {}", mainDBTablesNames.size());
                    }

                    return mainDBTablesNames;
                }
            }
            // No Inheritance annotation found in super class
            // Go to the next super class
            superClass = superClass.getSuperclass();
        }
        // No @Table is mentioned & no Inheritance annotation for super chain
        mainDBTablesNames.add(myClass.getSimpleName().toLowerCase());
        tableName.put(myClass.getSimpleName(), mainDBTablesNames);
        if (mainDBTablesNames != null) {
            logger.debug("Returning with mainDBTablesNames of size: {}", mainDBTablesNames.size());
        }

        return mainDBTablesNames;
    }
    //</editor-fold>

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Version Control">
    /**
     * Return the full expression of the Version Control OModule
     *
     * <br>Example: returns "oscreen.omodule" for ScreenOutput entity
     *
     * @return Expression in case of success <br>"": In Entity has not VC
     * Expression <br>null: error
     *
     * Must Be consistent with {@link EntityBaseService#changeEntityForModuleChange(BaseEntity, OModule, OModule, String, long, OUser)
     * }
     * in multi-parent decision
     */
    public static String getVCModuleExpression(BaseEntity entity) {
        logger.debug("Entering");
        try {
            String vc = vcModuleExpression.get(entity.getClassName());
            if (vc != null) {
                logger.debug("Returning with: {}", vc);
                return vc;
            }
            VersionControlSpecs vcspecs = (VersionControlSpecs) entity.getClass().getAnnotation(VersionControlSpecs.class);
            if (vcspecs == null) {
                vcModuleExpression.put(entity.getClassName(), "");
                logger.debug("Returning with empty string");
                return "";
            }
            if (vcspecs.versionControlType() == VersionControlSpecs.VersionControlType.Self) {
                vc = vcspecs.omoduleFieldExpression();
                vcModuleExpression.put(entity.getClassName(), vc);
                logger.debug("Returning with: {}", vc);
                return vc;
            }

            // Not VersionControlType.Self, Assuming VersionControlType.Parent
            // Get Parent Annotation
            // FIXME: use getParentEntityFieldName(className) instead of getting annotation
            ParentEntity parentEntityAnnot = entity.getClass().getAnnotation(ParentEntity.class);
            if (parentEntityAnnot == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                if (null != entity) {
                    logger.warn("Version Control Of Type Parent, While Entity Has No Parent Annotatio. this: {}", entity.getClassName());
                }
                // </editor-fold>
                logger.debug("Returning with Null");
                return null;
            }
            String[] parentFieldNames = parentEntityAnnot.fields();
            if (parentFieldNames == null || parentFieldNames.length == 0) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                if (entity != null) {
                    logger.warn("VC Of Type Parent, While Entity Has No Parent Fields In Annotation. this: ", entity.getClassName());
                }
                // </editor-fold>
                logger.debug("Returning with Null");
                return null;
            }
            String parentFieldName = null;
            if (parentFieldNames.length == 1) {
                // Only one parent found
                parentFieldName = parentFieldNames[0];
            } else if (parentFieldNames.length == 2) {
                // Two parent found
                // Must Be consistent with {@link EntityBaseService#changeEntityForModuleChange(BaseEntity, OModule, OModule, String, long, OUser) }
                // in multi-parent decision
                // <editor-fold defaultstate="collapsed" desc="Decide which parent to get the proper module from">
                BaseEntity frstParent = (BaseEntity) invokeGetter(entity, parentFieldNames[0]);
                BaseEntity scndParent = (BaseEntity) invokeGetter(entity, parentFieldNames[1]);
                if (frstParent == null || scndParent == null) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    if (null != entity.getClassName()) {
                        logger.warn("Improper Entity State. Atl least One parent is null. "
                                + " Can not decide which parent to get module from. "
                                + " Assuming first one. this: {}, Parent Fields: {}, First Parent: {}, Second Parent: {}",
                                entity.getClassName(), parentFieldNames, frstParent, scndParent);
                    }
                    // </editor-fold>
                    parentFieldName = parentFieldNames[0];
                } else {
                    // The logic here is to set the parent field name to the
                    // parent of non-System module
                    OModule frstParentModule = frstParent.getModule();
                    OModule scndParentModule = scndParent.getModule();
                    long frstParentModuleDBID = (frstParentModule == null ? // Parent has no module
                            0 : frstParentModule.getDbid());
                    long scndParentModuleDBID = (scndParentModule == null ? // Parent has no module
                            0 : scndParentModule.getDbid());
                    if (frstParentModuleDBID == scndParentModuleDBID) {
                        if (frstParentModuleDBID == 0) {
                            // <editor-fold defaultstate="collapsed" desc="Log">
                            if (null != entity) {
                                logger.warn("Entity Has Module, While Parents Don't Have. this: {}, Parent Fields: {}, First Parent: {}, Second Parent: {}",
                                        entity.getClassName(), parentFieldNames, frstParent, scndParent);
                            }
                            // </editor-fold>
                        }
                        parentFieldName = parentFieldNames[0];
                    } else {
                        if (frstParentModuleDBID != 1) // Not System
                        {
                            parentFieldName = parentFieldNames[0];
                        } else // First parent module is System, select the second one
                        // Which can be either system or non-system
                        {
                            parentFieldName = parentFieldNames[1];
                        }
                    }
                }
                // </editor-fold>
            } else {
                // Error, more than two parents found, case not supported, assume first field
                parentFieldName = parentFieldNames[0];
            }

            Class parentCls = null;
            BaseEntity parentEntity = (BaseEntity) invokeGetter(entity, parentFieldName);
            if (parentEntity == null) {
                // Parent is null, get the class in the hardway
                parentCls = getFieldObjectType(getClassField(entity.getClass(), parentFieldName));
                // Instanciate a temporary object to get annotations from any way
                parentEntity = (BaseEntity) parentCls.newInstance();
            } else {
                // Parent is not null, use it to get the class
                parentCls = parentEntity.getClass();
            }

            String parentModuleExp = getVCModuleExpression(parentEntity);
            if (parentModuleExp == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                if (null != entity.getClassName() && null != parentEntity.getClassName()) {
                    logger.warn("Null Parent VC Information. this: {}, parent: {}", entity.getClassName(), parentEntity.getClassName());
                }
                // </editor-fold>
                logger.debug("Returning with Null");
                return null;
            }
            if ("".equals(parentModuleExp)) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                if (entity != null) {
                    logger.warn("Parent Has No VC Annotation While Mentioned In Child It is Parent VC. this:{}, parent: {}",
                            entity.getClassName(), parentEntity.getClassName());
                }
                // </editor-fold>
                logger.debug("Returning with Null");
                return null;
            }
            vc = parentFieldName + "." + parentModuleExp;
            vcModuleExpression.put(entity.getClassName(), vc);
            logger.debug("Returning with: {}", vc);
            return vc;

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: ", ex);
            logger.trace("this: {}", entity.getClassName());
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        }
    }

    /**
     * Calls {@link BaseEntity#getVCModuleExpression() }
     */
    public static String getVCModuleExpression(Class entityClass) {
        logger.debug("Entering");
        long x = System.currentTimeMillis();
        try {
            String vc = vcModuleExpression.get(entityClass.getSimpleName());
            if (vc != null) {
                logger.debug("Returning with: {}", vc);
                return vc;
            }
            BaseEntity baseEntity = (BaseEntity) entityClass.newInstance();
            vc = getVCModuleExpression(baseEntity);
            logger.debug("Returning with: {}", vc);
            return vc;
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown: {}", ex);
            if (null != entityClass) {
                logger.trace("EntityClass: {}", entityClass.getName());
            }
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Getter">
    /**
     * Invokes the getter of the field (fieldName) which is declared in the
     * entity or one of it's super classes
     *
     * It considers the boolean field
     *
     * @param fieldExp The name of the field which is got. It supports child
     * fields, i.e. "."
     *
     * @return Field Value in case of Success <br>Null in case of field value is
     * null, error (field not found), one of the expression chain is null
     */
    public static Object invokeGetter(BaseEntity entity, String fieldExp)
            throws NoSuchMethodException {
        logger.debug("Entering");
        if (fieldExp == null) {
            logger.debug("Returning with Null");
            return null;
        }

        if (fieldExp.contains(".")) {
            // Expression, get the full expression values
            while (fieldExp.indexOf(".") != -1) {
                String currentField = fieldExp.substring(0, fieldExp.indexOf("."));
                fieldExp = fieldExp.substring(fieldExp.indexOf(".") + 1);
                entity = (BaseEntity) entity.invokeGetter(currentField);
                if (entity == null) {
                    logger.debug("Returning with Null");
                    return null;
                }
            }

            //  At this point, 'fieldName' contains the the last field.
            return entity.invokeGetter(fieldExp);
        } else {
            Field field = getClassField(entity.getClass(), fieldExp);
            if (field == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                if (null != entity) {
                    logger.warn("Field Not Found In Class. Field Name: {}, Class: {}", fieldExp, entity.getClassName());
                }
                // </editor-fold>
                logger.debug("Returning with Null");
                return null;
            }
            String methodName;
            if (field.getType().getName().equals("boolean")) {
                methodName = "is";
            } else {
                methodName = "get";
            }

            // Set first letter to uppercase
            methodName += Character.toString(fieldExp.charAt(0)).toUpperCase()
                    + fieldExp.substring(1);

            try {
                Method method = entity.getClass().getMethod(methodName);
                logger.debug("Returning ");
                return method.invoke(entity);
            } catch (Exception ex) {
                if (ex.getClass().getName().equals("java.lang.NoSuchMethodException")) {
                    throw new NoSuchMethodException(ex.getMessage());
                } else {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.error("Exception thrown: ", ex);
                    logger.trace("Field Name: {}", fieldExp);
                    // </editor-fold>
                }
            }
            logger.debug("Returning with Null");
            return null;
        }
    }

    public static Object getFieldValue(BaseEntity entity, Field field)
            throws NoSuchMethodException {
        logger.debug("Entering");
        try {
            Object fieldValue = field.get(entity);
            logger.debug("Returning with");
            return fieldValue;
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    /**
     * Invokes the getter of the field, of the BaseEntity
     */
    public static Object invokeGetter(BaseEntity entity, Field field)
            throws NoSuchMethodException {
        return invokeGetter(entity, field.getName());
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Setter">
    /**
     * Invokes the setter of the field (fieldName) which is declared in the
     * entity or one of it's super classes
     *
     * It doesn't call em.clear(), so, the set value will be persisted if entity
     * is persisted
     *
     * @param fieldName The name of the field which is set Is a direct field and
     * not a field expression, i.e. shouldn't contain "."
     * @param value Should be of the same type like the field's
     * @param types field setter function parameter types Usually is not needed,
     * even if the field is a list Is passed to getMethod()
     */
    public static void invokeSetter(BaseEntity entity, String fieldName, Object value, Class... types)
            throws NoSuchMethodException {
        logger.debug("Entering");
        if (fieldName.contains(".")) {
            System.out.println("Calling setter of "
                    + entity.getClass().getName() + "." + fieldName);
            throw new UnsupportedOperationException(
                    "Nested Field Expression Not Supported Yet");
        }

        String methodName = "set"
                + Character.toString(fieldName.charAt(0)).toUpperCase()
                + fieldName.substring(1);

        if (types.length == 0) {
            // No types passed
            types = new Class[1];
            // Get the types if the field is list
            Field field = getClassField(entity.getClass(), fieldName);
            types[0] = getFieldType(field);
        }

        try {
            Method method = entity.getClass().getMethod(methodName, types);
            if (types[0].getName().equalsIgnoreCase("short")) {
                method.invoke(entity, new Short(value.toString()));
            } else if (types[0].getName().equalsIgnoreCase("java.math.BigDecimal")) {
                method.invoke(entity, new BigDecimal(value.toString()));
            } else {
                method.invoke(entity, value);
            }
        } catch (Exception ex) {
            if (ex.getClass().getName().equals("java.lang.NoSuchMethodException")) {
                throw new NoSuchMethodException(ex.getMessage());
            } else {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown: ", ex);
                logger.trace("Field Name: {}", fieldName);
                // </editor-fold>
            }
        }
    }

    /**
     * Invokes the setter of the field (fieldName) which is declared in the
     * entity or one of it's super classes. Uses invokeSetter(String fieldName,
     * Object value, Class ... types)
     */
    public static void invokeSetter(BaseEntity entity, Field field, Object value) throws NoSuchMethodException {
        invokeSetter(entity, field.getName(), value);
    }

    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="DB Data">
    public static Hashtable<String, List<Field>> getClassFieldsInSameTable(Class entityClass) {
        Hashtable<String, List<Field>> table;
        table = classFieldsInSameTable.get(entityClass.getName());
        if (table != null) {
            return table;
        }
        Field field = null;
        try {
            field = entityClass.getField("dbid");
        } catch (Exception ex) {
            logger.info("Exception: java.lang.NoSuchFieldException: dbid");
        }
        table = getClassFieldsInSameTableImpl(entityClass);
        if (field != null) {
            for (Map.Entry<String, List<Field>> entry : table.entrySet()) {
                String string = entry.getKey();
                List<Field> list = entry.getValue();
                if (!list.contains(field)) {
                    list.add(field);
                }
            }
        }
        classFieldsInSameTable.put(entityClass.getName(), table);
        return table;
    }

    public static Hashtable<String, List<Field>> getClassFieldsInSameTableImpl(Class entityClass) {
        Hashtable<String, List<Field>> result = new Hashtable<String, List<Field>>();
        List<Field> fields;
        if (entityClass.getSimpleName().equalsIgnoreCase("PositionDirectParent")) {
            int t = 0;
        }
        Class currentClass = entityClass.getSuperclass();
        Class lastClass = null;
        List<Field> dbid = new ArrayList<Field>();
        Field tempDbid = getClassField(currentClass, "dbid");
        if (tempDbid != null) {
            dbid.add(tempDbid);
        } else {
            tempDbid = getClassField(currentClass, "entityDBID");
            dbid.add(tempDbid);
            tempDbid = getClassField(currentClass, "language");
            dbid.add(tempDbid);
        }

        while (true) {
            if (currentClass.getAnnotation(MappedSuperclass.class) != null) {
                break;
            }
            Annotation inheritance = currentClass.getAnnotation(Inheritance.class);
            if (inheritance != null) {
                Method method = inheritance.annotationType().getDeclaredMethods()[0];
                try {
                    String value = ((InheritanceType) method.invoke(
                            inheritance)).name();
                    if (value != null) {
                        if (value.contains("JOINED")) {
                            lastClass = currentClass;
                            break;
                        }
                    }
                } catch (Exception ex) {
                }
            }
            currentClass = currentClass.getSuperclass();
        }
        if (lastClass != null) {
            currentClass = entityClass;
            while (!lastClass.getName().equals(currentClass.getName())) {
                fields = new ArrayList<Field>();
                fields.addAll(Arrays.asList(currentClass.getDeclaredFields()));
                for (int i = dbid.size() - 1; i > -1; i--) {
                    Field field = dbid.get(i);
                    if (!fields.contains(field)) {
                        fields.add(0, field);
                    }
                }
                for (int i = 0; i < fields.size(); i++) {
                    Field currentField = fields.get(i);
                    if (currentField.getType() == List.class
                            || currentField.getAnnotation(Transient.class) != null
                            || (currentField.getAnnotation(OneToOne.class) != null
                            && (currentField.getAnnotation(OneToOne.class).mappedBy() != null
                            && !currentField.getAnnotation(OneToOne.class).mappedBy().isEmpty()))) {
                        fields.remove(i);
                        i--;
                    } else if (currentField.getAnnotation(Column.class) != null
                            || currentField.getAnnotation(ManyToOne.class) != null
                            || currentField.getAnnotation(OneToOne.class) != null
                            || (!Modifier.isFinal(currentField.getModifiers())
                            && !Modifier.isTransient(currentField.getModifiers())
                            && !Modifier.isStatic(currentField.getModifiers()))) {
                    } else {
                        fields.remove(i);
                        i--;
                    }
                }
                result.put(getEntityTableName(currentClass).get(0), fields);
                currentClass = currentClass.getSuperclass();
            }
            result.putAll(getClassFieldsInSameTable(lastClass));
        } else {
            fields = getClassFields(entityClass);
            for (int i = 0; i < fields.size(); i++) {
                Field currentField = fields.get(i);
                if (currentField.getType() == List.class
                        || currentField.getAnnotation(Transient.class) != null
                        || (currentField.getAnnotation(OneToOne.class) != null
                        && (currentField.getAnnotation(OneToOne.class).mappedBy() != null
                        && !currentField.getAnnotation(OneToOne.class).mappedBy().isEmpty()))) {
                    fields.remove(i);
                    i--;
                } else if (currentField.getAnnotation(Column.class) != null
                        || currentField.getAnnotation(ManyToOne.class) != null
                        || currentField.getAnnotation(OneToOne.class) != null
                        || (!Modifier.isFinal(currentField.getModifiers())
                        && !Modifier.isTransient(currentField.getModifiers())
                        && !Modifier.isStatic(currentField.getModifiers()))) {
                } else {
                    fields.remove(i);
                    i--;
                }
            }
            result.put(getEntityTableName(entityClass).get(0), fields);
        }
        return result;
    }

    public static List<String> getDiscrimnatorFieldTableData(Class entityClass) {
        List<String> data = new ArrayList<String>();
        // tableName, discriminatorField, discriminatorValue
        // Table Per Class is not handled
        String discField = null, discValue = null;
        Class currentOne = entityClass;
        String currentTableName = "";
        Annotation discriminatorValue = currentOne.getAnnotation(DiscriminatorValue.class);
        if (discriminatorValue != null) {
            discValue = ((DiscriminatorValue) discriminatorValue).value();
        }
        if (discValue == null) {
            discValue = currentOne.getSimpleName();
        }
        while (currentOne != null) {
            Annotation discriminatorField = currentOne.getAnnotation(DiscriminatorColumn.class);
            if (discriminatorField != null) {
                discField = ((DiscriminatorColumn) discriminatorField).name();
                currentTableName = getEntityTableName(currentOne).get(0);
            }
            currentOne = currentOne.getSuperclass();
        }
        if (discField == null) {
            return data;
        }
        data.add(currentTableName);
        data.add(discField);
        data.add(discValue);
        return data;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Entity Specification">
    public static FABSEntitySpecs getFabsEntitySpecs(Class entityClass) {
        FABSEntitySpecs entitySpecs = fabsEntitySpecs.get(entityClass.getName());
        if (entitySpecs != null) {
            return entitySpecs;
        }
        FABSEntitySpecs toBeCachedSpecs = (FABSEntitySpecs) entityClass.getAnnotation(FABSEntitySpecs.class);
        if (toBeCachedSpecs != null) {
            fabsEntitySpecs.put(entityClass.getName(), toBeCachedSpecs);
        }
        return toBeCachedSpecs;
    }
    //</editor-fold>
}
