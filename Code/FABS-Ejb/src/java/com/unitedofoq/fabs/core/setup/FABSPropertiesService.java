/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.setup;

import com.unitedofoq.fabs.core.usermessage.UserMessage;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author aelzaher
 */
@Stateless
@EJB(name="java:global/ofoq/com.unitedofoq.fabs.core.setup.FABSPropertiesServiceLocal",
        beanInterface=FABSPropertiesServiceLocal.class)
public class FABSPropertiesService implements FABSPropertiesServiceLocal {

    final static Logger logger = LoggerFactory.getLogger(FABSPropertiesService.class);
    public static String contextName = "";

    public String getContextName() {
        logger.debug("Entering");
        if (!contextName.isEmpty()) {
            logger.debug("Returning");
            return contextName;
        }
        try {
            String currentLiferayHome = System.getProperty("liferay.home");
            String userDirectory = System.getProperty("user.dir");
            if (currentLiferayHome != null && !currentLiferayHome.isEmpty()) {
                if (userDirectory.toLowerCase().contains("glassfish")) {
                    String currentDomainHome = System.getProperty("catalina.home");
                    if (currentDomainHome != null && !currentDomainHome.isEmpty()) {
                        Properties properties = new Properties();
                        properties.load(new FileReader(currentDomainHome + "/config/FABS.properties"));
                        contextName = properties.getProperty("CONTEXT_NAME");
                        System.out.println("Context Name Was Set " + contextName + " "
                                + getCurDateTime());
                    }
                } else {
                    if (userDirectory != null && !userDirectory.isEmpty()) {
                        Properties properties = new Properties();
                        properties.load(new FileReader(userDirectory + "/config/FABS.properties"));
                        contextName = properties.getProperty("CONTEXT_NAME");
                        System.out.println("Context Name Was Set " + contextName + " "
                                + getCurDateTime());
                    } else {
                        System.out.println("Context Name Was Not Loaded \"Not Glassfish or Weblogic Server\" "
                            + getCurDateTime());
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        }
        if (contextName == null || contextName.isEmpty()) {
            // Default Value
            logger.trace("contextName is Null, I will set current date as a Default value");
            System.out.println("Context Name Was Set /FABS-Web/ "
                    + getCurDateTime());
            contextName = "/FABS-Web/";
        }
        logger.debug("Returning");
        return contextName;
    }
    
    public void setContextName(String newContextName) {
        logger.debug("Entering");
        if (newContextName == null || newContextName.isEmpty()) {
            logger.trace("newContextName is Null, I will set current date as a Default value");
            System.out.println("Context Name Was Not Set \"Invalid Context Name\" "
                    + getCurDateTime());
        }
        try {
            String currentLiferayHome = System.getProperty("liferay.home");
            String userDirectory = System.getProperty("user.dir");
            if (currentLiferayHome != null && !currentLiferayHome.isEmpty()) {
                if (userDirectory.contains("Glassfish") || userDirectory.contains("glassfish")) {
                    String currentDomainHome = System.getProperty("catalina.home");
                    if (currentDomainHome != null && !currentDomainHome.isEmpty()) {
                        Properties properties = new Properties();
                        File f = new File(currentDomainHome + "config/FABS.properties");
                        if (!f.exists()) {
                            f.createNewFile();
                        }
                        properties.load(new FileReader(currentDomainHome + "config/FABS.properties"));
                        properties.setProperty("CONTEXT_NAME", newContextName);
                        System.out.println("Context Name Set " + newContextName + " "
                                + getCurDateTime());
                    }
                } else {
                    // error not glassfish server
                    logger.error("currentLiferayHome is Null or Empty");
                    System.out.println("Context Name Was Not Set \"Not Glassfish Server\" "
                            + getCurDateTime());
                }
            }
        } catch (Exception ex) {
            logger.error("exception thrown: ",ex);
        }
        contextName = newContextName;
    }

    public static String getCurDateTime() {
        logger.debug("Entering");
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        logger.debug("Returning");
        return sdf.format(cal.getTime()) + ", " + Thread.currentThread().getId() + ", ";
    }
}
