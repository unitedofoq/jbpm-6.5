package com.unitedofoq.fabs.core.report;

import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitysetup.OEntity;

@Entity
@VersionControlSpecs(omoduleFieldExpression = "omodule")
@DiscriminatorValue(value = "CROSSTAB")
public class CrossTabReport extends OReport {

    @Transient
    // <editor-fold defaultstate="collapsed" desc="relEntity">
    private Class relEntity;

    public Class getRelEntity() {
        return relEntity;
    }

    public String getRelEntityDD() {
        return "CrossTabReport_relEntity";
    }

    public void setRelEntity(Class relEntity) {
        this.relEntity = relEntity;
    }
    //</editor-fold>
    //  Example Value: "class com.unitedofoq.otms.payroll.foundation.employee.EmployeePeriodicSalaryElement"


    @Column(nullable=false)
    // <editor-fold defaultstate="collapsed" desc="relEntityAttribute">
    private String relEntityAttribute;

    public String getRelEntityAttribute() {
        if (relEntityAttribute == null || relEntityAttribute.equals(""))
            relEntityAttribute = "periodicSalaryElements";
        return relEntityAttribute;
    }

    public String getRelEntityAttributeDD() {
        return "CrossTabReport_relEntityAttribute";
    }

    public void setRelEntityAttribute(String relEntityAttribute) {
        this.relEntityAttribute = relEntityAttribute;
    }
    //</editor-fold>
    //  Example Value: "periodicSalaryElements"


    @Column(nullable=false)
    // <editor-fold defaultstate="collapsed" desc="crossEntityAttribute">
    private String crossEntityAttribute;

    public String getCrossEntityAttribute() {
        if (crossEntityAttribute == null || crossEntityAttribute.equals(""))
            crossEntityAttribute = "salaryElement";
        return crossEntityAttribute;
    }

    public String getCrossEntityAttributeDD() {
        return "CrossTabReport_crossEntityAttribute";
    }

    public void setCrossEntityAttribute(String crossEntityAttribute) {
        //  crossEntityAttribute = relEntityAttribute + <cross entity field>
        if (relEntityAttribute != null  && !relEntityAttribute.equals(""))
            this.crossEntityAttribute = crossEntityAttribute.substring(relEntityAttribute.length() + 1);
        else
            this.crossEntityAttribute = crossEntityAttribute;
    }
    //</editor-fold>
    //  Example Value: "salaryElement"


    @Column(nullable=false)
    // <editor-fold defaultstate="collapsed" desc="labelExpression">
    private String labelExpression;

    public String getLabelExpression() {
        if (labelExpression == null || labelExpression.equals(""))
            labelExpression = "description";
        return labelExpression;
    }

    public String getLabelExpressionDD() {
        return "CrossTabReport_labelExpression";
    }

    public void setLabelExpression(String labelExpression) {
        this.labelExpression = labelExpression;

        //  labelExpression = crossEntityAttribute + <label field>
        if (relEntityAttribute != null && crossEntityAttribute != null
             && !relEntityAttribute.equals("")
             && !crossEntityAttribute.equals("")
             && this.labelExpression.indexOf(relEntityAttribute) != -1
             && this.labelExpression.indexOf(crossEntityAttribute) != -1) {
            this.labelExpression = this.labelExpression.substring(relEntityAttribute.length() + 1);
            this.labelExpression = this.labelExpression.substring(crossEntityAttribute.length() + 1);
        }
    }
    //</editor-fold>
    //  Example Value: "description"


    @Column(nullable=false)
    // <editor-fold defaultstate="collapsed" desc="fieldExpression">
    private String fieldExpression;

    public String getFieldExpression() {
        if (fieldExpression == null || fieldExpression.equals(""))
            fieldExpression = "localCurrencyNetAmount";
        return fieldExpression;
    }

    public String getFieldExpressionDD() {
        return "CrossTabReport_fieldExpression";
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;

        //  fieldExpression = relEntityAttribute + <field>
        if (relEntityAttribute != null && !relEntityAttribute.equals("")
             && this.fieldExpression.indexOf(relEntityAttribute) != -1)
            this.fieldExpression = this.fieldExpression.substring(relEntityAttribute.length() + 1);
    }
    //</editor-fold>
    //  Example Value: "localCurrencyNetAmount"


    @Transient
    // <editor-fold defaultstate="collapsed" desc="associateField">
    private Class associateField;

    public Class getAssociateField() {
        return associateField;
    }

    public String getAssociateFieldDD() {
        return "CrossTabReport_associateField";
    }

    public void setAssociateField(Class associateField) {
        this.associateField = associateField;
    }
    //</editor-fold>
    //  Example Value: "class com.unitedofoq.otms.payroll.CalculatedPeriod"


    @Column(nullable=false)
    // <editor-fold defaultstate="collapsed" desc="associateFieldAttribute">
    private String associateFieldAttribute;

    public String getAssociateFieldAttribute() {
        if (associateFieldAttribute == null || associateFieldAttribute.equals(""))
            associateFieldAttribute = "payCalculatedPeriod.code";
        return associateFieldAttribute;
    }

    public String getAssociateFieldAttributeDD() {
        return "CrossTabReport_associateFieldAttribute";
    }

    public void setAssociateFieldAttribute(String associateFieldAttribute) {
        this.associateFieldAttribute = associateFieldAttribute;
    }
    //</editor-fold>
    //  Example Value: "payCalculatedPeriod.code"


    // <editor-fold defaultstate="collapsed" desc="associateFieldTitleOverride">
    private String associateFieldTitleOverride;

    public String getAssociateFieldTitleOverride() {
        if (associateFieldTitleOverride == null || associateFieldTitleOverride.equals(""))
            associateFieldTitleOverride = "Calc. Period";
        return associateFieldTitleOverride;
    }

    public String getAssociateFieldTitleOverrideDD() {
        return "CrossTabReport_associateFieldTitleOverride";
    }

    public void setAssociateFieldTitleOverride(String associateFieldTitleOverride) {
        this.associateFieldTitleOverride = associateFieldTitleOverride;
    }
    //</editor-fold>
    //  Example Value: "Calc. Period"


    @Transient
    // <editor-fold defaultstate="collapsed" desc="associateFieldColumnWidth">
    private int associateFieldColumnWidth;

    public int getAssociateFieldColumnWidth() {
        return associateFieldColumnWidth;
    }

    public String getAssociateFieldColumnWidthDD() {
        return "CrossTabReport_associateFieldColumnWidth";
    }

    public void setAssociateFieldColumnWidth(int associateFieldColumnWidth) {
        this.associateFieldColumnWidth = associateFieldColumnWidth;
    }
    //</editor-fold>


    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    // <editor-fold defaultstate="collapsed" desc="detailEntity">
    private OEntity detailEntity;

    public OEntity getDetailEntity() {
        return detailEntity;
    }

    public String getDetailEntityDD() {
        return "CrossTabReport_detailEntity";
    }

    public void setDetailEntity(OEntity detailEntity) {
        this.detailEntity = detailEntity;
    }
    //</editor-fold>
    //  Example Value: "class com.unitedofoq.otms.payroll.costcenter.CostCenter"


    // <editor-fold defaultstate="collapsed" desc="groupByEntityAttribute">
    private String groupByEntityAttribute;

    public String getGroupByEntityAttribute() {
        return groupByEntityAttribute;
    }

    public String getGroupByEntityAttributeDD() {
        return "CrossTabReport_groupByEntityAttribute";
    }

    public void setGroupByEntityAttribute(String groupByEntityAttribute) {
        this.groupByEntityAttribute = groupByEntityAttribute;
    }
    //</editor-fold>
    //  Example Value: "payroll.costCenter"


    // <editor-fold defaultstate="collapsed" desc="viewGrandTotalColumn">
    private boolean viewGrandTotalColumn;

    public boolean isViewGrandTotalColumn() {
        return viewGrandTotalColumn;
    }

    public String getViewGrandTotalColumnDD() {
        return "CrossTabReport_viewGrandTotalColumn";
    }

    public void setViewGrandTotalColumn(boolean viewGrandTotalColumn) {
        this.viewGrandTotalColumn = viewGrandTotalColumn;
    }
    //</editor-fold>

}