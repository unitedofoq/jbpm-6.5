package com.unitedofoq.fabs.core.i18n;

public class TextTranslationException extends Exception{

    private String message;

    public TextTranslationException(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }

    @Override
    public String getMessage() {
        return message;
    }


}
