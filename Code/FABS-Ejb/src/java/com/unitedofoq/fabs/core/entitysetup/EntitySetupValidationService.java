/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitysetup;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ComputedField;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.MeasurableField;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author arezk
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.entitysetup.EntitySetupValidationServiceRemote",
        beanInterface = EntitySetupValidationServiceRemote.class)
public class EntitySetupValidationService implements EntitySetupValidationServiceRemote {

    @EJB
    private UserMessageServiceRemote userMessageService;
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private OFunctionServiceRemote functionService;
    @EJB
    private DDServiceRemote ddService;
    @EJB
    private DataTypeServiceRemote dataTypeService;

    final static Logger logger = LoggerFactory.getLogger(EntitySetupValidationService.class);

    @Override
    public OFunctionResult validateEntityGeneral(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OEntity.class);
        if (inputValidationErrors != null) {
            logger.debug("Returning");
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        try {
            EntityManager oemEM = oem.getEM(loggedUser);

            OEntity oEntity = (OEntity) odm.getData().get(0);
            Class entityClass = null;
            Object entityClassObject = null;

            try {
                entityClass = Class.forName(oEntity.getEntityClassPath());
                entityClassObject = entityClass.newInstance();
            } catch (ClassNotFoundException cnf) {
                oFR.addError(userMessageService.getUserMessage("EntityNotFound",
                        Collections.singletonList(oEntity.getEntityClassPath()), loggedUser));
                logger.debug("Returning");
                return oFR;
            }

            // Entity must have @Entity
            Entity entityAnnotation = (Entity) entityClass.getAnnotation(Entity.class);
            if (entityAnnotation == null) {
                oFR.addError(userMessageService.getUserMessage("EntityAnnotationNotFound",
                        Collections.singletonList(oEntity.getEntityClassPath()), loggedUser));
                logger.debug("Returning");
                return oFR;
            }

            if (!BaseEntity.class.isInstance(entityClassObject)) {
                oFR.addError(userMessageService.getUserMessage("EntityMustExtendBaseEntity",
                        Collections.singletonList(oEntity.getEntityClassPath()), loggedUser));
                logger.debug("Returning");
                return oFR;
            }

            // 'abstract' classes shouldn't be instantiated
            int mod = entityClass.getModifiers();
            if (Modifier.isAbstract(mod)) {
                oFR.addError(userMessageService.getUserMessage("EntityClassIsAbstract",
                        Collections.singletonList(oEntity.getEntityClassPath()), loggedUser));
                logger.debug("Returning");
                return oFR;
            }

            // @MappedSuperClass entities shouldn't be added to "Manage OEntities" Screen
            MappedSuperclass mappedSuperclass = (MappedSuperclass) entityClass.getAnnotation(MappedSuperclass.class);
            if (mappedSuperclass != null) {
                oFR.addError(userMessageService.getUserMessage("MappedSuperClassError",
                        Collections.singletonList(oEntity.getEntityClassPath()), loggedUser));
                logger.debug("Returning");
                return oFR;
            }

            oFR = validateParentFieldExists(oEntity, oFR, loggedUser);
            oFR = validateChildFieldExists(oEntity, oFR, loggedUser);
            oFR = validateEntityFieldsNames(oEntity, oFR, loggedUser);
            oFR = validateOEntityDT(oEntity, oFR, loggedUser);
            oFR = validateEntityDDs(oEntity, oFR, loggedUser);
            oFR = validateEntityTranslation(oEntity, oFR, loggedUser);
            oFR = validateEntityFABSEntitySpecs(oEntity, oFR, loggedUser);
            oFR = validateEntityComputedFields(oEntity, oFR, loggedUser);
            oFR = validateEntityMeasurableField(oEntity, oFR, loggedUser);
            oFR = validateVersionControl(oEntity, oFR, loggedUser);
            logger.debug("Returning");
            return oFR;

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            if (null != odm && null != odm.getData() && 0!= odm.getData().size()) {
                logger.trace("OEntity: {}", (OEntity) odm.getData().get(0));
            }
            // </editor-fold>
            oFR.addError(userMessageService.getUserMessage("SystemInternalError", loggedUser), ex);
            logger.debug("Returning");
            return oFR;

        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
        }
    }

    private OFunctionResult validateParentFieldExists(OEntity oEntity, OFunctionResult oFR, OUser loggedUser) {
        logger.trace("Entering");
        String entityClassPath = null;
        try {
            entityClassPath = oEntity.getEntityClassPath();
            List<String> parentFieldNameList = (List<String>) BaseEntity.getParentEntityFieldName(entityClassPath);
            if (parentFieldNameList == null) {
                logger.trace("Returning");
                return oFR;
            }

            BaseEntity theEntity = (BaseEntity) Class.forName(entityClassPath).newInstance();

            for (String parentFieldName : parentFieldNameList) {
                Field ParentField = BaseEntity.getClassField(theEntity.getClass(), parentFieldName);
                if (ParentField == null) {
                    oFR.addError(userMessageService.getUserMessage(
                            "ParentFieldNotFound", userMessageService.
                            constructListFromStrings(entityClassPath, parentFieldName), loggedUser));
                    logger.trace("Returning");
                    return oFR;
                }
                ManyToOne manyToOne = ParentField.getAnnotation(ManyToOne.class);
                OneToOne oneToOne = ParentField.getAnnotation(OneToOne.class);
                // Check If ManyToOne or OneToOne is found above the Parent Field:
                if (manyToOne == null && oneToOne == null) {
                    UserMessage errorMsg = userMessageService.getUserMessage(
                            "InvalidParentFieldRelation", userMessageService.
                            constructListFromStrings(entityClassPath, parentFieldName), loggedUser);
                    oFR.addError(errorMsg);
                    logger.trace("Returning");
                    return oFR;
                }

                // Parent entity has this entity as a child
                boolean valid = false;
                oFR = validateChildRelationFoundInParent(ParentField, theEntity, oFR, loggedUser);
                if (manyToOne != null) {
                    if (!manyToOne.optional()) {
                        valid = true;
                    }
                }

                if (oneToOne != null) {
                    if (!oneToOne.optional()) {
                        valid = true;
                    }
                }
                if (ParentField.getAnnotation(Column.class) != null
                        || ParentField.getAnnotation(JoinColumn.class) != null) {

                    if (ParentField.getAnnotation(Column.class) != null) {
                        Column columnAnnotation = ParentField.getAnnotation(Column.class);
                        if (!columnAnnotation.nullable()) {
                            valid = true;
                        }
                    } else {
                        JoinColumn joinColumnAnnotation = ParentField.getAnnotation(JoinColumn.class);
                        if (!joinColumnAnnotation.nullable()) {
                            valid = true;
                        }
                    }
                }

                if (!valid) {
                    oFR.addError(userMessageService.getUserMessage(
                            "ParentFieldMustNotBeNullable", userMessageService.
                            constructListFromStrings(entityClassPath, parentFieldName), loggedUser));
                }
            }
            //FIXME: validate that if entity has more than one parent, and 
            // entity has module expression, then, both parents must have module 
            // expression
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Class", entityClassPath);
            oFR.addError(userMessageService.getUserMessage("ValidationFunctionError",
                    userMessageService.constructListFromStrings(oEntity.getEntityClassPath(),
                            "validateParentFieldExists", ex.getClass().getName()), loggedUser));
        }
        logger.trace("Returning");
        return oFR;
    }

    private OFunctionResult validateChildFieldExists(OEntity oEntity, OFunctionResult oFR, OUser loggedUser) {
        logger.trace("Entering");
        String parentEntityClassPath = null;
        try {
            parentEntityClassPath = oEntity.getEntityClassPath();
            List<String> entityChildren = BaseEntity.getChildEntityFieldName(parentEntityClassPath);
            if (entityChildren == null) {
                logger.trace("Returning");
                return oFR;
            }

            // Instantiate the parent field's Class Object:
            BaseEntity entity = (BaseEntity) Class.forName(oEntity.getEntityClassPath()).newInstance();
            List<String> notFoundChildren = new ArrayList<String>();

            // Loop on children
            for (String childName : entityChildren) {
                Field field = BaseEntity.getClassField(entity.getClass(), childName);
                if (field == null) {
                    notFoundChildren.add(childName);
                    continue;
                }

                // Validate child entity has this entity as parent
                oFR = validateParentRelationFoundInChild(field, entity, oFR, loggedUser);

                // Validate child relation is OneToOne or OneToMany
                OneToOne oneToOneAnnotation = field.getAnnotation(OneToOne.class);
                OneToMany oneToManyAnnotation = field.getAnnotation(OneToMany.class);

                if ((oneToOneAnnotation == null && oneToManyAnnotation == null)) {
                    oFR.addError(userMessageService.getUserMessage("InvalidChildFieldRelation",
                            userMessageService.constructListFromStrings(parentEntityClassPath, childName), loggedUser));
                }

                // Validate child field in this entity has "mappedby"
                if (oneToOneAnnotation != null) {
                    if (oneToOneAnnotation.mappedBy().length() < 1) {
                        oFR.addError(userMessageService.getUserMessage("ChildRelationMustHaveMappedBy",
                                userMessageService.constructListFromStrings(parentEntityClassPath, childName), loggedUser));
                    }
                }
                if (oneToManyAnnotation != null) {
                    if (oneToManyAnnotation.mappedBy().length() < 1) {
                        oFR.addError(userMessageService.getUserMessage("ChildRelationMustHaveMappedBy",
                                userMessageService.constructListFromStrings(parentEntityClassPath, childName), loggedUser));
                    }
                }
            }
            if (notFoundChildren.size() > 0) {
                for (String notFoundChild : notFoundChildren) {
                    oFR.addError(userMessageService.getUserMessage("ChildFieldNotFoundInJavaEntity",
                            userMessageService.constructListFromStrings(parentEntityClassPath, notFoundChild), loggedUser));
                }
            }

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("Class", parentEntityClassPath);
            oFR.addError(userMessageService.getUserMessage("ValidationFunctionError",
                    userMessageService.constructListFromStrings(oEntity.getEntityClassPath(),
                            "validateChildFieldExists", ex.getClass().getName()), loggedUser));
        }
        logger.trace("Returning");
        return oFR;
    }

    private OFunctionResult validateChildRelationFoundInParent(Field parentField, BaseEntity entity,
            OFunctionResult oFR, OUser loggedUser) {
        logger.trace("Entering");
        try {
            // Don't Instantiate Parent Class, as it may be abstract or 
            // mapped super class
            Class parentFieldClass = parentField.getType();
            List<Field> fieldsOfParentFieldType = BaseEntity.getClassObjectTypeFields(
                    parentFieldClass, entity.getClass());

            // Get Children Fields Names:
            ChildEntity childEntityAnnotation = (ChildEntity) parentFieldClass.getAnnotation(ChildEntity.class);
            if (childEntityAnnotation == null) {
                oFR.addError(userMessageService.getUserMessage("ParentEntityDoesntHaveChildEntityAnnotation",
                        userMessageService.constructListFromStrings(
                                entity.getClass().getName()), loggedUser));
                logger.trace("Returning");
                return oFR;
            }
            String[] childrenFieldsNames = childEntityAnnotation.fields();
            boolean found = false;
            for (String childFieldName : childrenFieldsNames) {
                for (Field potentialChildField : fieldsOfParentFieldType) {
                    if (childFieldName.equals(potentialChildField.getName())) {
                        found = true;
                        break;
                    }
                }
            }
            // Parent Field Found in Entity:
            if (!found) {
                oFR.addError(userMessageService.getUserMessage("ParentFieldNotFoundInParentEntityAsAChild",
                        userMessageService.constructListFromStrings(
                                entity.getClass().getName(), parentField.getName()), loggedUser));
            }
        } catch (Exception ex) {
            oFR.addError(userMessageService.getUserMessage("ValidationFunctionError",
                    userMessageService.constructListFromStrings(entity.getClass().getName(),
                            "validateChildRelationFoundInParent", ex.toString()), loggedUser));
            logger.error("Exception thrown", ex);
            logger.trace("Returning");
            return oFR;
        }
        logger.trace("Returning");
        return oFR;
    }

    private OFunctionResult validateParentRelationFoundInChild(Field childFieldInParentEntity, BaseEntity parentEntity,
            OFunctionResult oFR, OUser loggedUser) {
        logger.trace("Entering");
        try {
            // Instantiate Child Class:
            BaseEntity childEntity = (BaseEntity) BaseEntity.getFieldObjectType(childFieldInParentEntity).newInstance();

            // Get Parent Fields Names:
            ParentEntity parentEntityAnnotation = childEntity.getClass().getAnnotation(ParentEntity.class);
            if (parentEntityAnnotation == null) {
                oFR.addError(userMessageService.getUserMessage("ChildEntityDoesntHaveParentEntityAnnotation",
                        userMessageService.constructListFromStrings(parentEntity.getClass().getName(),
                                childFieldInParentEntity.getName()), loggedUser));
                logger.trace("Returning");
                return oFR;
            }

            String[] parentFieldsNames = parentEntityAnnotation.fields();
            if (parentFieldsNames.length == 0) {
                // FIXME: Error
            }

            // Check if Parent Field not Found in Child Entity:
            boolean childFieldFoundInChildEntityAsAParent = false;
            for (String parentFieldName : parentFieldsNames) {
                Field parentField = BaseEntity.getClassField(childEntity.getClass(), parentFieldName);
                if (parentField != null) {
                    // Check the parent field is of the same type like the passed parent entity
                    Class parentFieldClass = BaseEntity.getFieldObjectType(parentField);
                    if (parentFieldClass.isInstance(parentEntity)) {
                        childFieldFoundInChildEntityAsAParent = true;
                        break;
                    }
                }
            }
            if (!childFieldFoundInChildEntityAsAParent) {
                oFR.addError(userMessageService.getUserMessage("ChildFieldNotFoundInChildEntityAsAParent",
                        userMessageService.constructListFromStrings(
                                parentEntity.getClass().getName(), childFieldInParentEntity.getName()), loggedUser));
            }
        } catch (Exception ex) {
            oFR.addError(userMessageService.getUserMessage("ValidationFunctionError",
                    userMessageService.constructListFromStrings(parentEntity.getClass().getName(),
                            "validateParentRelationFoundInChild", ex.getClass().getName()), loggedUser));
            logger.error("Exception thrown", ex);
        }
        logger.trace("Returning");
        return oFR;
    }

    private OFunctionResult validateEntityFieldsNames(OEntity oEntity, OFunctionResult oFR, OUser loggedUser) {
        logger.trace("Entering");
        try {
            BaseEntity theEntity = (BaseEntity) Class.forName(oEntity.getEntityClassPath()).newInstance();
            List<Field> theEntityFields = Arrays.asList(theEntity.getClass().getDeclaredFields());
            for (Field field : theEntityFields) {
                if (field.isSynthetic()
                        || field.isEnumConstant()
                        || field.getName().contains("_persistence")) {
                    continue;
                }

                // Validate first two letters are lowercase
                int fieldModifiers = field.getModifiers();
                // Exclude final static fields, as they don't subject to 
                // the first two letters rule
                if (Modifier.isFinal(fieldModifiers) && Modifier.isStatic(fieldModifiers)) {
                    continue;
                }

                if (Character.getType(field.getName().charAt(0)) == Character.UPPERCASE_LETTER
                        || Character.getType(field.getName().charAt(1)) == Character.UPPERCASE_LETTER) {
                    // One of first & second letters is uppercase
                    oFR.addError(userMessageService.getUserMessage("InvalidFieldName",
                            userMessageService.constructListFromStrings(oEntity.getEntityClassPath(), field.getName()),
                            loggedUser));
                }
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.trace("OEnity: {}", oEntity);
            // </editor-fold>
            oFR.addError(userMessageService.getUserMessage("ValidationFunctionError",
                    userMessageService.constructListFromStrings(oEntity.getEntityClassPath(),
                            "validateEntityFieldsNames", ex.getClass().getName()), loggedUser));
        }
        logger.trace("Returning");
        return oFR;
    }

    private OFunctionResult validateOEntityDT(OEntity oEntity, OFunctionResult oFR, OUser loggedUser) {
        logger.trace("Entering");
        try {
            String entityName = oEntity.getEntityClassName();
            ODataType datatype = dataTypeService.loadODataType(entityName, loggedUser);
            if (datatype == null) {
                oFR.addError(userMessageService.getUserMessage(
                        "OEntityDataTypeNotFound", Collections.singletonList(entityName), loggedUser));
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.trace("OEnity: {}", oEntity);
            // </editor-fold>
            oFR.addError(userMessageService.getUserMessage("ValidationFunctionError",
                    userMessageService.constructListFromStrings(oEntity.getEntityClassPath(),
                            "validateOEntityDT", ex.getClass().getName()), loggedUser));
        }
        logger.trace("Returning");
        return oFR;
    }

    private OFunctionResult validateEntityDDs(OEntity oEntity, OFunctionResult oFR, OUser loggedUser) {
        logger.trace("Entering");
        try {
            String entityClassPath = oEntity.getEntityClassPath();
            BaseEntity theEntity = (BaseEntity) Class.forName(entityClassPath).newInstance();
            List<Method> entityMethods = Arrays.asList(
                    theEntity.getClass().getDeclaredMethods());
            for (Method method : entityMethods) {
                String methodName = method.getName();
                if (!methodName.startsWith("get")) {
                    continue;
                }
                if (!methodName.endsWith("DD")) {
                    continue;
                }
                // method name is getXyxDD
                // check if this method has a corresponding field:
                String fieldName = methodName.substring(3, methodName.lastIndexOf("DD"));
                // Convert first letter to lower case:
                fieldName = fieldName.substring(0, 1).toLowerCase() + fieldName.substring(1, fieldName.length());
                try {
                    theEntity.invokeGetter(fieldName);  // Will throw exception if not found
//                    Class[] types = new Class[2];
//                    types[0] = void.class;
//                    types[1] = String.class;
                    Method ddGetter = theEntity.getClass().getMethod(methodName, new Class[0]);
                    if (ddGetter != null) {
                        Object returnedValue = ddGetter.invoke(theEntity, (Object) null);
                        if (returnedValue != null) {
                            String returnedValueString = (String) returnedValue;
                            if (!returnedValueString.substring(0, returnedValueString.indexOf("_")).equals(theEntity.getClassName())) {
                                oFR.addError(userMessageService.getUserMessage("InvalidDDGetterReturnValue",
                                        userMessageService.constructListFromStrings(theEntity.getClassName(), returnedValueString),
                                        loggedUser));
                                logger.trace("Returning");
                                return oFR;
                            }
                            if (!returnedValueString.substring(returnedValueString.indexOf("_") + 1).equals(fieldName)) {
                                oFR.addError(userMessageService.getUserMessage("InvalidDDGetterReturnValue",
                                        userMessageService.constructListFromStrings(theEntity.getClassName(), returnedValueString),
                                        loggedUser));
                                logger.trace("Returning");
                                return oFR;
                            }
                        }
                    }

                } catch (Exception ex) {
                    oFR.addError(userMessageService.getUserMessage("InvalidDDGetter",
                            userMessageService.constructListFromStrings(entityClassPath, methodName), loggedUser));
                }
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.trace("OEnity: {}", oEntity);
            // </editor-fold>
            oFR.addError(userMessageService.getUserMessage("ValidationFunctionError",
                    userMessageService.constructListFromStrings(oEntity.getEntityClassPath(),
                            "validateEntityDDs", ex.getClass().getName()), loggedUser));
        }
        logger.trace("Returning");
        return oFR;
    }

    private OFunctionResult validateEntityTranslation(OEntity oEntity, OFunctionResult oFR, OUser loggedUser) {
        logger.trace("Entering");
        String entityClassPath = oEntity.getEntityClassPath();
        try {
            BaseEntity theEntity = (BaseEntity) Class.forName(entityClassPath).newInstance();
            List<Field> entityFields = Arrays.asList(
                    theEntity.getClass().getDeclaredFields());
            for (Field field : entityFields) {
                if (field.isSynthetic()
                        || field.isEnumConstant()
                        || field.getName().contains("_persistence")) {
                    continue;
                }
                field.setAccessible(true);
                Annotation translatable = field.getAnnotation(Translatable.class);
                if (translatable == null) {
                    continue;
                }
                // Validate translatable field has no DD getter
                String translatableFieldName = field.getName();
                if (!ddService.getDDName(theEntity, translatableFieldName).equals("")) {
                    oFR.addError(userMessageService.getUserMessage("TranslatableFieldHasDDGetter",
                            userMessageService.constructListFromStrings(
                                    entityClassPath, translatableFieldName), loggedUser));
                }

                // Validate translatable field has a translated field
                String translatedFieldName = translatableFieldName + "Translated";
                try {
                    theEntity.getClass().getDeclaredField(translatedFieldName);
                } catch (NoSuchFieldException nsm) {
                    oFR.addError(userMessageService.getUserMessage("TranslatableFieldHasNoTranslatedField",
                            userMessageService.constructListFromStrings(entityClassPath, translatableFieldName), loggedUser));
                }
//                String modifiers = Modifier.toString(theEntity.getClass().
//                        getDeclaredField(translatedFieldName).getModifiers());
                Field translatedField = theEntity.getClass().getDeclaredField(translatedFieldName);
                if (translatedField.getAnnotation(Transient.class) == null) {
                    oFR.addError(userMessageService.getUserMessage("TranslatedFieldIsNotTransient",
                            userMessageService.constructListFromStrings(entityClassPath, translatedFieldName), loggedUser));
                }
//                if (modifiers.indexOf("TRANSIENT") == -1)
//                    oFR.addError(userMessageService.getUserMessage("TranslatedFieldIsNotTransient",
//                        userMessageService.constructListFromStrings(entityClassPath, translatedFieldName), loggedUser));

                // Validate translated field has a getter
                try {
                    theEntity.invokeGetter(translatedFieldName);
                } catch (NoSuchMethodException nsm) {
                    oFR.addError(userMessageService.getUserMessage("TranslatableFieldHasNoTranslatedField",
                            userMessageService.constructListFromStrings(entityClassPath, translatedFieldName), loggedUser));
                }

                // Validate translated field has a setter
                try {
                    String methodName = "set"
                            + Character.toString(translatedFieldName.charAt(0)).toUpperCase()
                            + translatedFieldName.substring(1);
                    theEntity.getClass().getMethod(methodName, new Class[]{String.class});
                } catch (NoSuchMethodException nsm) {
                    oFR.addError(userMessageService.getUserMessage("TranslatedFieldHasNoSetter",
                            userMessageService.constructListFromStrings(entityClassPath, translatedFieldName), loggedUser));
                }

                // Validate translated field has a DD getter
                String translatedFieldDDName = ddService.getDDName(theEntity, translatedFieldName);
                if (translatedFieldDDName.equals("")) {
                    oFR.addError(userMessageService.getUserMessage("TranslatableFieldHasNoDDGetter",
                            userMessageService.constructListFromStrings(entityClassPath, translatedFieldName), loggedUser));
                }
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.trace("OEnity: {}", oEntity);
            // </editor-fold>
            oFR.addError(userMessageService.getUserMessage("ValidationFunctionError",
                    userMessageService.constructListFromStrings(oEntity.getEntityClassPath(),
                            "validateEntityTranslation", ex.getClass().getName()), loggedUser));
        }
        logger.trace("Returning");
        return oFR;
    }

    private OFunctionResult validateEntityFABSEntitySpecs(OEntity oEntity, OFunctionResult oFR, OUser loggedUser) {
        logger.trace("Entering");
        try {
            String entityClassPath = oEntity.getEntityClassPath();
            BaseEntity theEntity = (BaseEntity) Class.forName(entityClassPath).newInstance();
            FABSEntitySpecs fABSEntitySpecs = theEntity.getClass().getAnnotation(FABSEntitySpecs.class);
            if (fABSEntitySpecs == null) {
                logger.trace("Returning");
                return oFR;
            }

            // Validate historyImageOriginalEntityField attribute is found
            String hiOrgEntityField = fABSEntitySpecs.historyImageOriginalEntityField();
            if (hiOrgEntityField.length() == 0) {
                logger.trace("Returning");
                return oFR;
            }

            // Validate historyImageOriginalEntityField field is found
            try {
                theEntity.getClass().getDeclaredField(hiOrgEntityField);
            } catch (NoSuchFieldException nsf) {
                oFR.addError(userMessageService.getUserMessage(
                        "FABSEntitySpecFieldNotFound", userMessageService.
                        constructListFromStrings(entityClassPath, hiOrgEntityField), loggedUser));
                logger.trace("Returning");
                return oFR;
            }

            // Validate HImage original field has OneToOne or OneToMany annotation
            String mappedByValue = null;
            Field hiField = theEntity.getClass().getDeclaredField(fABSEntitySpecs.historyImageOriginalEntityField());
            OneToOne oneToOneAnnotation = hiField.getAnnotation(OneToOne.class);
            OneToMany oneToManyAnnotation = hiField.getAnnotation(OneToMany.class);
            if ((oneToOneAnnotation == null && oneToManyAnnotation == null)) {
                oFR.addError(userMessageService.getUserMessage(
                        "IncorrectHistoryImageRelation", userMessageService.
                        constructListFromStrings(entityClassPath, hiField.getName()), loggedUser));
                logger.trace("Returning");
                return oFR;

                // Validate HImage original field in this entity has "mappedby"
            } else {
                if (oneToOneAnnotation != null) {
                    mappedByValue = oneToOneAnnotation.mappedBy();
                    if (mappedByValue.length() < 1) {
                        oFR.addError(userMessageService.getUserMessage(
                                "HistoryImageRelationMustHaveMappedBy", userMessageService.
                                constructListFromStrings(entityClassPath, hiField.getName()), loggedUser));
                        logger.trace("Returning");
                        return oFR;
                    }
                }
                if (oneToManyAnnotation != null) {
                    mappedByValue = oneToManyAnnotation.mappedBy();
                    if (mappedByValue.length() < 1) {
                        oFR.addError(userMessageService.getUserMessage("HistoryImageRelationMustHaveMappedBy",
                                userMessageService.constructListFromStrings(entityClassPath, hiField.getName()), loggedUser));
                        logger.trace("Returning");
                        return oFR;
                    }
                }
            }

            // Validate history Image has this image as the original field
            Class hiFieldClass = BaseEntity.getFieldObjectType(hiField);
            Field hiFieldInHistoryImage = null;
            try {
                hiFieldInHistoryImage = hiFieldClass.getDeclaredField(mappedByValue);
            } catch (NoSuchFieldException nsf) {
                oFR.addError(userMessageService.getUserMessage(
                        "MissingOriginalFieldInHistoryImage", userMessageService.
                        constructListFromStrings(entityClassPath, hiField.getName()), loggedUser));
                logger.trace("Returning");
                return oFR;
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.trace("OEnity: {}", oEntity);
            // </editor-fold>
            oFR.addError(userMessageService.getUserMessage("ValidationFunctionError",
                    userMessageService.constructListFromStrings(oEntity.getEntityClassPath(),
                            "validateEntityFABSEntitySpecs", ex.getClass().getName()), loggedUser));
        }
        logger.trace("Returning");
        return oFR;
    }

    private OFunctionResult validateEntityComputedFields(OEntity oEntity, OFunctionResult oFR, OUser loggedUser) {
        logger.trace("Entering");
        String entityClassPath = oEntity.getEntityClassPath();
        try {
            BaseEntity theEntity = (BaseEntity) Class.forName(entityClassPath).newInstance();
            List<Field> entityFields = Arrays.asList(
                    theEntity.getClass().getDeclaredFields());
            for (Field field : entityFields) {
                if (field.isSynthetic()
                        || field.isEnumConstant()
                        || field.getName().contains("_persistence")) {
                    continue;
                }
                ComputedField computedField = field.getAnnotation(ComputedField.class);
                if (computedField == null) {
                    continue;
                }
                List<String> computedFieldNames = Arrays.asList(
                        computedField.filterLHSExpression());
                for (String computedFieldName : computedFieldNames) {
                    try {
                        theEntity.getClass().getDeclaredField(computedFieldName);
                    } catch (NoSuchFieldException nsm) {
                        oFR.addError(userMessageService.getUserMessage("MissingComputedField",
                                userMessageService.constructListFromStrings(
                                        entityClassPath, computedFieldName), loggedUser));
                        continue;
                    }
                }
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.trace("OEnity: {}}", oEntity);
            // </editor-fold>
            oFR.addError(userMessageService.getUserMessage("ValidationFunctionError",
                    userMessageService.constructListFromStrings(oEntity.getEntityClassPath(),
                            "validateEntityComputedFields", ex.getClass().getName()), loggedUser));
        }
        logger.trace("Returning");
        return oFR;
    }

    private OFunctionResult validateEntityMeasurableField(OEntity oEntity, OFunctionResult oFR, OUser loggedUser) {
        logger.trace("Entering");
        try {
            String entityClassPath = oEntity.getEntityClassPath();
            BaseEntity theEntity = (BaseEntity) Class.forName(entityClassPath).newInstance();
            List<Field> entityFields = Arrays.asList(
                    theEntity.getClass().getDeclaredFields());
            for (Field field : entityFields) {
                if (field.isSynthetic()
                        || field.isEnumConstant()
                        || field.getName().contains("_persistence")) {
                    continue;
                }
                MeasurableField measurableField = field.getAnnotation(MeasurableField.class);
                if (measurableField == null) {
                    continue;
                }
                String measurableFieldName = measurableField.name();
                try {
                    Field measuredField = theEntity.getClass().getDeclaredField(measurableFieldName
                            .substring(0, measurableFieldName.lastIndexOf(".")));
                    if (!measuredField.getType().getSimpleName().equals("UDC")) {
                        oFR.addError(userMessageService.getUserMessage("MeasuredFieldNotUDC",
                                userMessageService.constructListFromStrings(
                                        entityClassPath, measuredField.getName()), loggedUser));
                        logger.trace("Returning");
                        return oFR;
                    }
                } catch (NoSuchFieldException nsm) {
                    oFR.addError(userMessageService.getUserMessage("MissingMeasuredField",
                            userMessageService.constructListFromStrings(entityClassPath,
                                    measurableFieldName.substring(0, measurableFieldName.lastIndexOf("."))), loggedUser));
                    logger.trace("Returning");
                    return oFR;
                }
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.trace("OEnity: {}", oEntity);
            // </editor-fold>
            oFR.addError(userMessageService.getUserMessage("ValidationFunctionError",
                    userMessageService.constructListFromStrings(oEntity.getEntityClassPath(),
                            "validateEntityMeasurableField", ex.getClass().getName()), loggedUser));
        }
        logger.trace("Returning");
        return oFR;
    }

    
     private OFunctionResult validateVersionControl(OEntity oEntity, OFunctionResult oFR, OUser loggedUser) {
        logger.trace("Entering");
        try {
            oem.getEM(loggedUser);
            String entityClassPath = oEntity.getEntityClassPath();
            BaseEntity theEntity = (BaseEntity) Class.forName(entityClassPath).newInstance();
            Class entityClass = theEntity.getClass();
            VersionControlSpecs vcs = (VersionControlSpecs) entityClass.getAnnotation(VersionControlSpecs.class);
            if (vcs == null) {
                logger.trace("Returning");
                return oFR;
            }

            // Check if it's type is 'Parent'
            if (vcs.versionControlType() == VersionControlSpecs.VersionControlType.Parent) {
                // If no @ParentEntity is found
                // If entity has a Parent, It's parent must have a @VersionControlSpecs
                List parents = BaseEntity.getParentEntityFieldName(entityClassPath);
                if (parents == null) {
                    oFR.addError(userMessageService.getUserMessage("VCSErrorParentRequired",
                            Collections.singletonList(entityClassPath), loggedUser));
                    logger.trace("Returning");
                    return oFR;
                }
                logger.trace("Returning");
                return oFR;
            }

            // If FieldExpression is empty
            String omoduleFieldExpression = vcs.omoduleFieldExpression();
            if (omoduleFieldExpression.equals("")) {
                oFR.addError(userMessageService.getUserMessage("VCSInvalidFieldExpression",
                        Collections.singletonList(entityClassPath), loggedUser));
                logger.trace("Returning");
                return oFR;
            }

            List<ExpressionFieldInfo> expFldsInfo = theEntity.parseFieldExpression(omoduleFieldExpression);
            if (expFldsInfo == null) {
                oFR.addError(userMessageService.getUserMessage("VCSInvalidFieldExpression",
                        Collections.singletonList(entityClassPath), loggedUser));
                logger.trace("Returning");
                return oFR;
            }

            if (!expFldsInfo.get(expFldsInfo.size() - 1).field.getType().getSimpleName().equals("OModule")) {
                oFR.addError(userMessageService.getUserMessage("VCSErrorOModuleFieldTypeNotOModule",
                        Collections.singletonList(entityClassPath), loggedUser));
                logger.trace("Returning");
                return oFR;
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.trace("OEnity: {}", oEntity);
            // </editor-fold>
        } finally {
            oem.closeEM(loggedUser);
            logger.trace("Returning");
            return oFR;
        }
    }
    @Override
    public OFunctionResult validateAllEntitiesGeneral(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.trace("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            ArrayList<OEntity> allOEntities = (ArrayList<OEntity>) oem.loadEntityList("OEntity",
                    null, null, null, loggedUser);
            for (OEntity entity : allOEntities) {
                odm.getData().set(0, entity);
                oFR.append(validateEntityGeneral(odm, functionParms, oem.getSystemUser(loggedUser)));
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.trace("ODM", odm.toString());
            // </editor-fold>
            logger.trace("Returning");
            return oFR;
        } finally {
            oem.closeEM(loggedUser);
            logger.trace("Returning");
            return oFR;
        }
    }
}
