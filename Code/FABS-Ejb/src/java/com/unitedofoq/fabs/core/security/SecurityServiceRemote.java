/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.security;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntityAction;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO;
import com.unitedofoq.fabs.core.security.privilege.OPrivilege;
import com.unitedofoq.fabs.core.security.privilege.PrivilegeException;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.security.user.UserPrivilege.ConstantUserPrivilege;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import java.util.List;

/**
 *
 * @author ahussien
 */
@Local
public interface SecurityServiceRemote {

    // @Deprecated
    //public boolean checkAuthorithy(OEntity entity, OUser loggedUser, long screenModeId);

    public void checkPrivilegeAuthorithy(String privilegeName, OUser checkedUser, OUser loggedUser)
        throws UserNotAuthorizedException, PrivilegeException ;
    public void checkPrivilegeAuthorithy(OPrivilege privilege, OUser checkedUser, OUser loggedUser)
        throws UserNotAuthorizedException, PrivilegeException ;

    /**
     * Refer to {@link SecurityService#checkEntityAccessAuthority(OEntity, OUser, OUser) }
     */
    public void checkEntityAccessAuthority(OEntity entity, OUser checkedUser, OUser loggedUser)
        throws UserNotAuthorizedException, PrivilegeException ;
    /**
     * Refer to {@link SecurityService#checkActionOnlyAuthority(OEntityAction, OUser, OUser) }
     */
    public boolean checkActionOnlyAuthority(OEntityAction action, OUser checkedUser, OUser loggedUser)
        throws UserNotAuthorizedException, PrivilegeException ;
    
    /**
     * Refer to {@link SecurityService#checkActionOnlyAuthority(OEntityActionDTO, OUser, OUser) }
     */
    public boolean checkActionOnlyAuthority(OEntityActionDTO action, OUser checkedUser, OUser loggedUser)
        throws UserNotAuthorizedException, PrivilegeException ;

    /**
     * Refer to {@link SecurityService#checkActionEntityAuthority(String, Long, OUser, OUser) }
     */
    public void checkActionEntityAuthority
            (String entityName, Long actionTypeDBID, OUser checkedUser, OUser loggedUser) 
    throws UserNotAuthorizedException ;

    /**
     * Refer to {@link SecurityService#checkActionEntityAuthority(OEntityAction, OUser, OUser) }
     */
    public void checkActionEntityAuthority
            (OEntityAction entityAction, OUser checkedUser, OUser loggedUser)
    throws UserNotAuthorizedException ;
    public String getEntityFieldsTagMsg(Class entityClass,
            List<ScreenField> screenFields, OUser loggedUser);
    public boolean checkPrivilegeAuthority(ConstantUserPrivilege privilege, OUser loggedUser);
}
