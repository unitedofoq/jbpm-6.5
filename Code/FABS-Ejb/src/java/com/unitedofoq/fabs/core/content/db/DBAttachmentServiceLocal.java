/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.content.db;

import com.unitedofoq.fabs.core.content.AttachmentDTO;
import com.unitedofoq.fabs.core.content.AttachmentServiceLocal;
import javax.ejb.Local;

/**
 *
 * @author shamiaa
 */
@Local
public interface DBAttachmentServiceLocal extends AttachmentServiceLocal {

    AttachmentDTO getByName(String attachmentName, long tenantID);
}
