/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.udc;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.module.OModule;
import javax.persistence.Column;

/**
 *
 * @author htawab
 */
@Entity
@DiscriminatorValue(value = "UDCType")
@ChildEntity(fields={"childUDCs"})
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class UDCType extends UDCBase
{

    @Override
    public String getCodeDD(){
        return "UDCType_code";
    }
    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OModule omodule;

    public String getOmoduleDD(){
        return "UDCType_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="childUDCs">
    @OneToMany(mappedBy="type")
    private List<UDC> childUDCs;

    public List<UDC> getChildUDCs() {
        return childUDCs;
    }

    public void setChildUDCs(List<UDC> childUdcs) {
        this.childUDCs = childUdcs;
    }
    // </editor-fold>

    @Override
    public String toString() {
        return "UDCType[dbid=" + dbid + ", value=" + value + "]";
    }
}
