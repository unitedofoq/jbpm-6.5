/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.security.user;

import com.unitedofoq.fabs.bpm.model.Task;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.process.TaskFunction;

/**
 *
 * @author ahussien
 */
public class Inbox extends BaseEntity {

    private long id;
    private boolean process;
    private String state;
    private String text;
    private String title;
    private String subject;
    private String fromMail;
    private String date;
    private String fromUser;
    private String assignee;
    private int colored = 0;
    private TaskFunction function;
    private String initiator;
    private Task task;

    public String getInitiator() {
        return initiator;
    }
    
    public String getInitiatorDD() {
        return "Inbox_initiator";
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }
    
    
    public boolean isProcess() {
        return process;
    }

    public void setProcess(boolean process) {
        this.process = process;
    }

    

    public String getDate() {
        return date;
    }

    public String getDateDD(){
        return "Inbox_date";
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFromMail() {
        return fromMail;
    }
    public String getFromMailDD() {
        return "Inbox_fromMail";
    }

    public void setFromMail(String fromMail) {
        this.fromMail = fromMail;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

   
    public String getProcessDD() {
        return "Inbox_process";
    }

   

    public String getState() {
        return state;
    }
    public String getStateDD() {
        return "Inbox_state";
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSubject() {
        return subject;
    }
    public String getSubjectDD() {
        return "Inbox_subject";
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public String getTextDD() {
        return "Inbox_text";
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public String getTitleDD() {
        return "Inbox_title";
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the fromUser
     */
    public String getFromUser() {
        return fromUser;
    }

    /**
     * @param fromUser the fromUser to set
     */
    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public int getColored() {
        return colored;
    }

    public void setColored(int colored) {
        this.colored = colored;
    }

    public TaskFunction getFunction() {
        return function;
    }

    public void setFunction(TaskFunction function) {
        this.function = function;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }
    
}
