package com.unitedofoq.fabs.core.report;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;

@Entity
@VersionControlSpecs(omoduleFieldExpression="oreport.oactOnEntity.omodule")
public class OReportUserFilter extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="oreport">
    @JoinColumn(nullable=false, name="OReport_DBID")
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OReport oreport;

    public OReport getOreport() {
        return oreport;
    }

    public String getOreportDD() {
        return "OReportUserFilter_oreport";
    }

    public void setOreport(OReport oreport) {
        this.oreport = oreport;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="userFilterExpression">
    @Column(nullable=false)
    private String userFilterExpression;

    public String getUserFilterExpression() {
        return userFilterExpression;
    }

    public String getUserFilterExpressionDD() {
        return "OReportUserFilter_userFilterExpression";
    }

    public void setUserFilterExpression(String userFilterExpression) {
        this.userFilterExpression = userFilterExpression;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="sortIndex">
    @Column(nullable=false)
    private Integer sortIndex;

    public Integer getSortIndex() {
        return sortIndex;
    }

    public String getSortIndexDD() {
        return "OReportUserFilter_sortIndex";
    }

    public void setSortIndex(Integer sortIndex) {
        this.sortIndex = sortIndex;
    }
    //</editor-fold>

}