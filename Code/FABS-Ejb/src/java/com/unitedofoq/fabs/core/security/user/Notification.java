/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.security.user;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mahmed
 */
@Entity
@Table(name = "processnotification")
public class Notification extends BaseEntity {

    @Column(name = "SENDER")
    private String sender;
    @Column(name = "RECIEVER")
    private String reciever;
    @Column(name = "MESSAGE")
    private String message;
    @Column(name = "SENDDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sendDate;
    @Column(name = "SCREENNAME")
    private String screenName;
    @Column(name = "ENTITYNAME")
    private String entityName;
    @Column(name = "ENTITYDBID")
    private Long entitydbid;
   
    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSenderDD(){
        return "Notification_sender";
    }
    
    public String getReciever() {
        return reciever;
    }

    public void setReciever(String reciever) {
        this.reciever = reciever;
    }

    public String getRecieverDD(){
        return "Notification_reciever";
    }
    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageDD(){
        return "Notification_message";
    }
    
    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }
    
    public String getSendDateDD() {
        return "Notification_sendDate";
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public Long getEntitydbid() {
        return entitydbid;
    }

    public void setEntitydbid(Long entitydbid) {
        this.entitydbid = entitydbid;
    }
    
}
