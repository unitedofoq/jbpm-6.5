/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen.report;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.uiframework.screen.TabularScreen;

/**
 *
 * @author mostafa
 */
@Entity
@DiscriminatorValue(value = "TABULARREPORT")
@ChildEntity(fields = {"screenInputs", "screenOutputs", "screenFilter", "screenFunctions",
    "screenControlsGroups", "screenFields", "screenActions", "reportFields"})
@VersionControlSpecs(omoduleFieldExpression = "omodule")
public class TabularReportScreen extends TabularScreen {
//    
//    // <editor-fold defaultstate="collapsed" desc="reportFields">
//    @OneToMany(mappedBy = "tabularReportScreen", fetch = FetchType.LAZY)
//    private List<ReportScreenField> reportFields;
//
//    public List<ReportScreenField> getReportFields() {
//        return reportFields;
//    }
//
//    public void setReportFields(List<ReportScreenField> reportFields) {
//        this.reportFields = reportFields;
//    }
//    public String getReportFieldsDD() {
//        return "TabularReportScreen_reportFields";
//    }    
//    // </editor-fold>
//    
}
