/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.mailmerge;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;

/**
 *
 * @author 3dly
 */
@Entity
@ParentEntity(fields = "oactOnEntity")
@ChildEntity(fields = {"mailMergeField", "mailMergeContent"})
public class MailMergeBasic extends BaseEntity {

    @Translation(originalField = "templateName")
    @Transient
    private String templateNameTranslated;
    //<editor-fold defaultstate="collapsed" desc="templateName">
    @Translatable(translationField = "templateNameTranslated")
    @Column
    private String templateName;

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public void setTemplateNameTranslated(String templateNameTranslated) {
        this.templateNameTranslated = templateNameTranslated;
    }

    public String getTemplateName() {
        return templateName;
    }

    public String getTemplateNameTranslated() {
        return templateNameTranslated;
    }

    public String getTemplateNameTranslatedDD() {
        return "MailMergeBasicTranslation_templateName";
    }
    //</editor-fold>
    @Translation(originalField = "templateDescription")
    @Transient
    private String templateDescriptionTranslated;
    //<editor-fold defaultstate="collapsed" desc="templateDescription">
    @Translatable(translationField = "templateDescriptionTranslated")
    @Column
    private String templateDescription;

    public void setTemplateDescription(String templateDescription) {
        this.templateDescription = templateDescription;
    }

    public void setTemplateDescriptionTranslated(String templateDescriptionTranslated) {
        this.templateDescriptionTranslated = templateDescriptionTranslated;
    }

    public String getTemplateDescription() {
        return templateDescription;
    }

    public String getTemplateDescriptionTranslated() {
        return templateDescriptionTranslated;
    }

    public String getTemplateDescriptionTranslatedDD() {
        return "MailMergeBasicTranslation_templateDescription";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="oactOnEntity">
    @ManyToOne
    protected OEntity oactOnEntity;

    public OEntity getOactOnEntity() {
        return oactOnEntity;
    }

    public void setOactOnEntity(OEntity oactOnEntity) {
        this.oactOnEntity = oactOnEntity;
    }

    public String getOactOnEntityDD() {
        return "MailMergeBasic_oactOnEntity";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="mailMergeField">
//    @OneToMany
//    private List<MailMergeField> mailMergeField;
//
//    public List<MailMergeField> getMailMergeField() {
//        return mailMergeField;
//    }
//
//    public void setMailMergeField(List<MailMergeField> mailMergeField) {
//        this.mailMergeField = mailMergeField;
//    }
//
//    public String getMailMergeFieldDD() {
//        return "MailMergeBasic_mailMergeField";
//    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="mailmergeContent">
    @OneToOne(mappedBy="mailMergeBasic")
    private MailMergeContent mailMergeContent;

    public MailMergeContent getMailMergeContent() {
        return mailMergeContent;
    }

    public void setMailMergeContent(MailMergeContent mailMergeContent) {
        this.mailMergeContent = mailMergeContent;
    }

    public String getMailMergeContentDD() {
        return "MailMergeBasic_mailMergeContent";
    }
    //</editor-fold>
}
