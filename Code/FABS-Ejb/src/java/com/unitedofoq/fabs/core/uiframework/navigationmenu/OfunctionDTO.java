/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.navigationmenu;

import com.unitedofoq.fabs.core.function.OMenuFunction;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author tarzy
 */
public class OfunctionDTO implements Comparable<OfunctionDTO> {
    final static Logger logger = LoggerFactory.getLogger(OfunctionDTO.class);
    private Long functiondbid;
    private String functionName;
    private int langdbid;
    private String fType;
    private Long menudbid;
    private int sortIndex;
    private String functionScreenURL;

    public OfunctionDTO(Long id, String name, String ftype, Long menudbid, int langid,int sortIndex) {
        this.functionName = name;
        this.functiondbid = id;
        this.langdbid = langid;
        this.fType = ftype;
        this.menudbid = menudbid;
        this.sortIndex = sortIndex;
    }

    public String getFunctionScreenURL() {
        return functionScreenURL;
    }

    public void setFunctionScreenURL(String functionScreenURL) {
        this.functionScreenURL = functionScreenURL;
    }
    

    public static OfunctionDTO constractOfunDTO(OMenuFunction fun, int LangID) {
        logger.debug("Entering");
        if (fun.getOfunction() != null) {
            if (fun.isInActive() == false && fun.getOfunction().isActive() == true) {
                Long funDBID = fun.getOfunction().getDbid();
                Long menuDBID = fun.getOmenu().getDbid();
                int sortInd = fun.getSortIndex();
logger.debug("Returning");
                return new OfunctionDTO(funDBID, fun.getOfunction().getName(), null, menuDBID, LangID,sortInd);
            } else {
                logger.debug("Returning with Null");
                return null;
            }
        } else {
            logger.debug("Returning with Null");
            return null;
        }
    }

    /**
     * @return the functiondbid
     */
    public long getFunctiondbid() {
        return functiondbid;
    }

    /**
     * @param functiondbid the functiondbid to set
     */
    public void setFunctiondbid(Long functiondbid) {
        this.functiondbid = functiondbid;
    }

    /**
     * @return the functionName
     */
    public String getFunctionName() {
        return functionName;
    }

    /**
     * @param functionName the functionName to set
     */
    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    /**
     * @return the langdbid
     */
    public int getLangdbid() {
        return langdbid;
    }

    /**
     * @param langdbid the langdbid to set
     */
    public void setLangdbid(int langdbid) {
        this.langdbid = langdbid;
    }

    /**
     * @return the fType
     */
    public String getfType() {
        return fType;
    }

    /**
     * @param fType the fType to set
     */
    public void setfType(String fType) {
        this.fType = fType;
    }

    /**
     * @return the menudbid
     */
    public long getMenudbid() {
        return menudbid;
    }

    /**
     * @param menudbid the menudbid to set
     */
    public void setMenudbid(Long menudbid) {
        this.menudbid = menudbid;
    }

    /**
     * @return the sortIndex
     */
    public int getSortIndex() {
        return sortIndex;
    }

    /**
     * @param sortIndex the sortIndex to set
     */
    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    @Override
    public int compareTo(OfunctionDTO o) {
        return this.sortIndex- o.getSortIndex();
    }
}
