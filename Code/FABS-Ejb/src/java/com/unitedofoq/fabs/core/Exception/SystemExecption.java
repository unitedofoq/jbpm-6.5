/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.Exception;

import com.unitedofoq.fabs.core.ExceptionHandler.thrower.factory.ExceptionType;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessage;

/**
 *
 * @author mostafa
 */
public class SystemExecption extends BasicException {

    public SystemExecption(UserMessage userMessage, OUser loggedUser) {
        super(userMessage, ExceptionType.SYSTEM, loggedUser);
    }
    
    public SystemExecption(String message, OUser loggedUser) {
        super(message, ExceptionType.SYSTEM, loggedUser);
    }
}
