/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.function;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;

/**
 *
 * @author arezk
 */
@Entity
@DiscriminatorValue(value = "FIELDEXP")
@ChildEntity(fields={"functionImage", "menuFunction"})
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class FieldExpParserFunction extends JavaFunction
{
    //<editor-fold defaultstate="collapsed" desc="rootExpression">
    @Column(unique=true)
    private String rootExpression;

    public String getRootExpression() {
        return rootExpression;
    }

    public void setRootExpression(String rootExpression) {
        this.rootExpression = rootExpression;
    }
    
    public String getRootExpressionDD(){
        return "FieldExpParserFunction_rootExpression";
    }
    //</editor-fold>
}
