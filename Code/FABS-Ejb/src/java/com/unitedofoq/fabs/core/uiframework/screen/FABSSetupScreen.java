/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.screen;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.setup.FABSSetupScreenKey;

/**
 *
 * @author nkhalil
 */
@Entity
@DiscriminatorValue(value = "FABSSetup")
@ChildEntity(fields={"fABSSetupScreenKeys","screenInputs", "screenOutputs",
    "screenFilter","screenFunctions","screenControlsGroups"})
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class FABSSetupScreen extends FormScreen{
    @OneToMany(mappedBy = "setupScreen")
    private List<FABSSetupScreenKey> fABSSetupScreenKeys;
    public String getFABSSetupScreenKeyiesDD() {
        return "FABSSetupScreen_fABSSetupScreenKeys";
    }

    public List<FABSSetupScreenKey> getFABSSetupScreenKeys() {
        return fABSSetupScreenKeys;
    }

    public void setFABSSetupScreenKeyies(List<FABSSetupScreenKey> fABSSetupScreenKeys) {
        this.fABSSetupScreenKeys = fABSSetupScreenKeys;
    }

}
