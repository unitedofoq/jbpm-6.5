/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.scheduler;

import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.central.OCentralUser;
import com.unitedofoq.fabs.core.alert.entities.Alert;
import com.unitedofoq.fabs.core.alert.entities.BaseAlert;
import com.unitedofoq.fabs.core.alert.entities.CustomAlert;
import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.serverjob.resources.ServerJobDTO;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.ScheduleBuilder;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Hammad
 */
public class JavaFunctionScheduler {

    final static org.slf4j.Logger logger = LoggerFactory.getLogger(JavaFunctionScheduler.class);
    private DataTypeServiceRemote dataTypeService;
    private OEntityManagerRemote oem;
    private OCentralEntityManagerRemote centralOEM;
    private EntitySetupServiceRemote entitySetupService;
    private UIFrameworkServiceRemote uIFrameworkService;
    private List<OUser> adminUsersList;
    private SchedulerFactory schFactory = null;
    private Scheduler sch = null;
    private HashMap<Long, JobKey> jobsMap = new HashMap<Long, JobKey>();

    public JavaFunctionScheduler(DataTypeServiceRemote dataTypeService,
            OEntityManagerRemote oem, OCentralEntityManagerRemote centralOEM,
            EntitySetupServiceRemote entitySetupService, //TimeZoneServiceLocal timeZoneService,
            UIFrameworkServiceRemote uIFrameworkService) {
        logger.debug("Entering");
        this.dataTypeService = dataTypeService;
        this.oem = oem;
        this.centralOEM = centralOEM;
        this.entitySetupService = entitySetupService;
        this.uIFrameworkService = uIFrameworkService;

        try {
            schFactory = new StdSchedulerFactory();
            sch = schFactory.getScheduler();
            sch.start();
        } catch (SchedulerException ex) {
            logger.trace("Failed to start scheduler, will not run any scheduled jobs");
            logger.error("Exception thrown: ", ex);
        }
    }

    public void cleanup() {
        logger.debug("Entering");
        if (sch == null) {
            logger.trace("Scheduler is not initialized... Nothing to do");
            logger.debug("Returning");
            return;
        }
        logger.trace("Attempting to shutdown Scheduler");
        try {
            sch.shutdown(true);
            logger.trace("Scheduler shutdown completed");
        } catch (SchedulerException ex) {
            logger.trace("Scheduler failed to shutdown");
            logger.error("Exception thrown: ", ex);
        }
        logger.debug("Returning");
    }

    public void LoadServerJobs() {
        logger.debug("Entering");
        if (sch == null) {
            logger.trace("Scheduler is not initialized... Nothing to do");
            logger.debug("Returning");
            return;
        }
        logger.trace("Acquiring Admin Users");
        List<OCentralUser> users = centralOEM.getAdminUsers();
        adminUsersList = new ArrayList<OUser>();
        for (int i = 0; i < users.size(); i++) {
            adminUsersList.add(oem.getUser(users.get(i).getLoginName(),
                    users.get(i).getTenant()));
        }
        logger.trace("Admin Users Acquired: {}", adminUsersList);
        if (adminUsersList.isEmpty()) {
            logger.trace("No Admin Users.....No Server Jobs Will Be Running");
            logger.debug("Returning");
            return;
        }

        for (OUser oUser : adminUsersList) {
            if (null == oUser) {
                continue;
            }

            UDC startedUDC = getUDCStatus(true, oUser);
            UDC completedUDC = getUDCStatus(false, oUser);

            logger.debug("Start to Create ServerJobs for user");
            List<ServerJobDTO> serverJobsDTOs = uIFrameworkService.getServerJobDTOs(oUser);
            for (ServerJobDTO serverJobDTO : serverJobsDTOs) {
                addServerJob(serverJobDTO, oUser, startedUDC, completedUDC);
            }
            logger.trace("End of Creation JavaFunctionVO object for the user: {}", oUser);
            oem.closeEM(oUser);
        }
    }

    public UDC getUDCStatus(boolean isStarted, OUser oUser) {
        logger.debug("Entering");
        UDC udcStatus = null;

        try {
            if (isStarted) {
                udcStatus = (UDC) oem.loadEntity("UDC",
                        Collections.singletonList("code = 'JobStarted'"), null, oUser);
            } else {
                udcStatus = (UDC) oem.loadEntity("UDC",
                        Collections.singletonList("code = 'JobCompleted'"), null, oUser);
            }
        } catch (Exception ex) {
            logger.trace("Failed to load UDC status");
            logger.error("Exception thrown: ", ex);
        }
        logger.debug("Returning");
        return udcStatus;
    }

    public void addServerJob(ServerJobDTO serverJobDTO, OUser oUser) {
        addServerJob(serverJobDTO, oUser, getUDCStatus(true, oUser), getUDCStatus(false, oUser));
    }

    private void addServerJob(ServerJobDTO serverJobDTO, OUser oUser, UDC startedUDC, UDC completedUDC) {
        logger.trace("Entering");
        if (null != serverJobDTO) {
            logger.trace("Creating server job: {}", serverJobDTO.getCode());
        }
        if (0 != serverJobDTO.getScheduleExpressionActOnEntityDBID()) {
            List<BaseEntity> serverJobEntities = null;
            try {
                serverJobEntities = oem.loadEntityList(
                        serverJobDTO.getScheduleExpressionActOnEntityClassPath()
                        .substring(serverJobDTO.getScheduleExpressionActOnEntityClassPath().
                                lastIndexOf(".") + 1),
                        null, null, null, oUser);
            } catch (Exception ex) {
                if (null != serverJobDTO) {
                    logger.trace("Job initialization for Server job: {} failed while loading entities with exception", serverJobDTO.getCode());
                }
                logger.error("Exception thrown: ", ex);
            }
            if (serverJobEntities == null) {
                logger.trace("Returning");
                return;
            }
            for (BaseEntity baseEntity : serverJobEntities) {
                scheduleServerJob(oUser, serverJobDTO, baseEntity, startedUDC, completedUDC);
                if (null != serverJobDTO) {
                    logger.trace("Server job: {} loaded successfully", serverJobDTO.getCode());
                }
            }
        } else {
            scheduleServerJob(oUser, serverJobDTO, null, startedUDC, completedUDC);
            if (null != serverJobDTO) {
                logger.trace("Server job: {} loaded successfully", serverJobDTO.getCode());
            }
        }
        logger.trace("Returning");
    }

    private void scheduleServerJob(OUser oUser, ServerJobDTO serverJobDTO, BaseEntity baseEntity, UDC startedUDC,
            UDC completedUDC) {
        logger.trace("Entering");
        Object javaFunctionClassObject;

        try {
            InitialContext ic = new InitialContext();
            javaFunctionClassObject = ic.lookup("java:global/ofoq/"
                    + serverJobDTO.getFunctionClassPath());
        } catch (NamingException ex) {
            if (null != serverJobDTO) {
                logger.trace("Job initialization for Server job: {} failed while loading entities with exception", serverJobDTO.getCode());
            }
            logger.error("Exception thrown: ", ex);
            logger.trace("Returning");
            return;
        }

        ODataMessage odm = null;
        if (baseEntity != null) {
            odm = getEntityODM(baseEntity, oUser,
                    serverJobDTO.getScheduleExpressionActOnEntityClassPath().substring(serverJobDTO.getScheduleExpressionActOnEntityClassPath().lastIndexOf(
                                    ".") + 1));
        } else {
            odm = new ODataMessage();
        }

        Class[] parameterTypes = new Class[3];
        Object[] args = new Object[3];

        parameterTypes[0] = ODataMessage.class;
        args[0] = odm;

        parameterTypes[1] = OFunctionParms.class;
        args[1] = new OFunctionParms();

        parameterTypes[2] = OUser.class;
        args[2] = oUser;

        Method functionMethod;

        try {
            functionMethod = javaFunctionClassObject.getClass().getMethod(serverJobDTO.getFunctionName(), parameterTypes);
            if(serverJobDTO.getBaseAlertDBID() != 0){
                BaseAlert baseAlert = (BaseAlert) oem.loadEntity(BaseAlert.class.getSimpleName(), serverJobDTO.getBaseAlertDBID(), null, null, oUser);
                List data = new ArrayList();
                if(baseAlert instanceof CustomAlert){
                    data.add((CustomAlert) baseAlert);
                } else if (baseAlert instanceof Alert){
                    data.add((Alert) baseAlert);
                }
                odm.setData(data);
                args[0] = odm;
            }
        } catch (NoSuchMethodException ex) {
            if (null != serverJobDTO) {
                logger.trace("Job initialization for Server job: {} failed while loading entities with exception", serverJobDTO.getCode());
            }
            logger.error("Exception thrown: ", ex);
            logger.trace("Returning");
            return;
        } catch (SecurityException ex) {
            if (null != serverJobDTO) {
                logger.trace("Job initialization for Server job: {} failed while loading entities with exception", serverJobDTO.getCode());
            }
            logger.error("Exception thrown: ", ex);
            logger.trace("Returning");
            return;
        }catch(Exception ex){
            logger.error("Exception thrown: ", ex);
            logger.trace("Returning");
            return;
        }

        JobDetail jobDetail = JobBuilder.newJob(JavaFunctionSchedulerJob.class).withIdentity(String.valueOf(serverJobDTO.getDbid())).build();

        JobDataMap jobDataMap = jobDetail.getJobDataMap();
        jobDataMap.put("oUser", oUser);
        jobDataMap.put("entitySetupService", entitySetupService);
        jobDataMap.put("functionMethod", functionMethod);
        jobDataMap.put("javaFunctionClassObject", javaFunctionClassObject);
        jobDataMap.put("args", args);
        jobDataMap.put("serverJobDTO", serverJobDTO);
        jobDataMap.put("startedUDC", startedUDC);
        jobDataMap.put("completedUDC", completedUDC);

        Trigger trigger = getTrigger(serverJobDTO, baseEntity);

        try {
            jobsMap.put(serverJobDTO.getScheduleExpressionDBID(), jobDetail.getKey());
            sch.scheduleJob(jobDetail, trigger);
        } catch (SchedulerException ex) {
            if(null!=serverJobDTO)
            logger.trace("Job initialization for Server job: {} failed while loading entities with exception", serverJobDTO.getCode());
            logger.error("Exception thrown: ", ex);
        }
        logger.trace("Returning");
    }

    private Trigger getTrigger(ServerJobDTO serverJobDTO, BaseEntity baseEntity) {
        logger.trace("Entering");
        Date plannedStartDate = null, plannedEndDate = null;
        String recurUnitValue = null;

        if (baseEntity != null) {
            String plannedStartDateExpression = serverJobDTO.getScheduleExpressionLannedStartDateExpression();
            String plannedEndDateExpression = serverJobDTO.getScheduleExpressionPlannedEndDateExpression();
            String recurUnitExpression = serverJobDTO.getScheduleExpressionRecurUnitExpression();

            if ((recurUnitExpression != null) && !"".equals(recurUnitExpression)) {
                try {
                    recurUnitValue = ((UDC) BaseEntity.getValueFromEntity(baseEntity,
                            recurUnitExpression)).getCode();
                } catch (Exception ex) {
                    logger.trace("Couldnt retrieve recurUnitValue from entity");
                    logger.error("Exception thrown: ", ex);
                    logger.trace("Returning");
                }
            }

            if ((plannedStartDateExpression != null) && !"".equals(plannedStartDateExpression)) {
                try {
                    plannedStartDate = (Date) BaseEntity.getValueFromEntity(baseEntity,
                            plannedStartDateExpression);
                } catch (Exception ex) {
                    logger.trace("Couldnt retrieve plannedStartDate from entity");
                }
            }

            if ((plannedEndDateExpression != null) && !"".equals(plannedEndDateExpression)) {
                try {
                    plannedEndDate = (Date) BaseEntity.getValueFromEntity(baseEntity,
                            plannedEndDateExpression);
                } catch (Exception ex) {
                    logger.trace("Couldnt retrieve plannedEndDate from entity");
                    logger.error("Exception thrown: ", ex);
                }
            }
        }

        TriggerBuilder triggerBuilder = TriggerBuilder.newTrigger();

        if ((recurUnitValue == null) || ("".equals(recurUnitValue))) {
            if (0 == serverJobDTO.getScheduleExpressionRecurUnitDBID()) {
                logger.trace("Returning with Null");
                return null;
            }
            recurUnitValue = serverJobDTO.getScheduleExpressionRecurUnitCode();
            if ((recurUnitValue == null) || ("".equals(recurUnitValue))) {
                logger.trace("Returning with Null");
                return null;
            }
        }

        try {
            ScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(recurUnitValue);
            triggerBuilder.withSchedule(scheduleBuilder);
        } catch (Exception ex) {
            if(null!=serverJobDTO)
            logger.trace("Job initialization for Server job: {} failed while loading entities with exception", serverJobDTO.getCode());
            logger.error("Exception thrown: ", ex);
            logger.debug("Returning");
        }

        if (plannedStartDate == null) {
            plannedStartDate = serverJobDTO.getScheduleExpressionPlannedStartDate();
        }

        if (plannedStartDate != null) {
            Date triggerStartDate = new Date(plannedStartDate.getTime());
            
            // If plannedStartDate is in the past, we use now + 1min, to fix Bug #6838
            Date now = Calendar.getInstance().getTime();
            if (triggerStartDate.before(now)) {
                long t= Calendar.getInstance().getTimeInMillis();
                triggerStartDate=new Date(t + 60000);
            }
            triggerBuilder.startAt(triggerStartDate);
        }

        if (plannedEndDate == null) {
            plannedEndDate = serverJobDTO.getScheduleExpressionPlannedEndDate();
        }

        if (plannedEndDate != null) {
            triggerBuilder.endAt(plannedEndDate);
        }

        try {
            logger.trace("Returning");
            return triggerBuilder.build();
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            logger.trace("Returning with Null");
            return null;
        }
    }

    public void cancelJob(long scheduleExpressionDBID) {
        logger.debug("Entering");
        logger.trace("Cancel job for scheduleExpression ");
        try {
            JobKey jobKey = jobsMap.get(scheduleExpressionDBID);
            sch.deleteJob(jobKey);
        } catch (Exception ex) {
            logger.trace("Failed to cancel job");
            logger.error("Exception thrown: ", ex);
            logger.debug("Returning");
            return;
        }
        logger.trace("Successfully canceled job for scheduleExpression");
    }

    private ODataMessage getEntityODM(BaseEntity baseEntity, OUser loggedUser,
            String actonEntityEntityClassName) {
        logger.trace("Returning");
        ODataMessage odm = new ODataMessage();
        List odmData = new ArrayList();
        odmData.add(baseEntity);
        odmData.add(loggedUser);
        odm.setData(odmData);
        odm.setODataType(getActOnEntityDataType(loggedUser, actonEntityEntityClassName));
        logger.debug("Returning");
        return odm;
    }

    private ODataType getActOnEntityDataType(OUser loggedUser, String actonEntityEntityClassName) {
        return dataTypeService.loadODataType(actonEntityEntityClassName, loggedUser);
    }
}
