/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.security.privilege;

/**
 *
 * @author nsaleh
 */
public class PrivilegeException extends Exception {
    private String message;

    public PrivilegeException(String message)
    {
        this.message = message;
    }

    public PrivilegeException(PrivilegeException pex)
    {
        this.message = pex.getMessage();
        this.oPrivilege = pex.getOPrivilege() ;
        this.privilegeName = pex.privilegeName ;
    }

    @Override
    public String getMessage() {
        return message;
    }

    private OPrivilege oPrivilege ;

    public OPrivilege getOPrivilege() {
        return oPrivilege;
    }

    public void setOPrivilege(OPrivilege oPrivilege) {
        this.oPrivilege = oPrivilege;
    }

    private String privilegeName ;

    public String getPrivilegeName() {
        return privilegeName;
    }

    public void setPrivilegeName(String privilegeName) {
        this.privilegeName = privilegeName;
    }

    @Override
    public String toString() {
        return
            (message!=null? "Message: " + message : "")
           +(oPrivilege!=null? ", OPrivilege: " + oPrivilege.toString() : "")
           +(privilegeName!=null? ", Privilege Name: " + privilegeName.toString() : "")
           ;
    }
}
