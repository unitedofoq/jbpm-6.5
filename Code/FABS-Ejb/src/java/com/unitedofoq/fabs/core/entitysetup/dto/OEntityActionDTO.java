/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitysetup.dto;

import java.io.Serializable;

/**
 *
 * @author root
 */
public class OEntityActionDTO implements Serializable {
    // <editor-fold defaultstate="collapsed" desc="Action Type Static Fields">

    /**
     * Action Type UDC DBID of "Create" Action
     */
    public static final Long ATDBID_CREATE = 121L;
    /**
     * Action Type UDC DBID of "Retrieve" Action
     */
    public static final Long ATDBID_RETRIEVE = 122L;
    /**
     * Action Type UDC DBID of "Update" Action
     */
    public static final Long ATDBID_UPDATE = 123L;
    /**
     * Action Type UDC DBID of "Delete" Action
     */
    public static final Long ATDBID_ACTIVATION = 124L;
    /**
     * Action Type UDC DBID of "Duplicate" Action
     */
    public static final Long ATDBID_DUPLICATE = 1027861L;
    /**
     * Action Type UDC DBID of "Parent Pre Save" Action
     */
    public static final Long ATDBID_ParentPreSave = 125L;
    /**
     * Action Type UDC DBID of "Hard Remove" Action
     */
    public static final Long ATDBID_DELETE = 126L;
    // </editor-fold>
    private long dbid;
    private String name;
    private long ofuntionDBID;
    private String ofunctionName;
    private long oownerEntityDBID;
    private String oownerEntityClassPath;
    private long entityActionPrivilegeDBID;
    private String entityActionPrivilegeName;
    private int sortIndex;
    private boolean displayed;
    private String ofunctionType;
    private long parentValidationDBID;
    private String parentValidationName;
    private long actionTypeDBID;
    private String ofunctionScreenName;
    private long screenMode;
    private String displayCondition;

    public String getOownerEntityClassPath() {
        return oownerEntityClassPath;
    }

    public void setOownerEntityClassPath(String oownerEntityClassPath) {
        this.oownerEntityClassPath = oownerEntityClassPath;
    }

    public long getEntityActionPrivilegeDBID() {
        return entityActionPrivilegeDBID;
    }

    public void setEntityActionPrivilegeDBID(long entityActionPrivilegeDBID) {
        this.entityActionPrivilegeDBID = entityActionPrivilegeDBID;
    }

    public String getEntityActionPrivilegeName() {
        return entityActionPrivilegeName;
    }

    public void setEntityActionPrivilegeName(String entityActionPrivilegeName) {
        this.entityActionPrivilegeName = entityActionPrivilegeName;
    }

    public long getParentValidationDBID() {
        return parentValidationDBID;
    }

    public void setParentValidationDBID(long parentValidationDBID) {
        this.parentValidationDBID = parentValidationDBID;
    }

    public String getParentValidationName() {
        return parentValidationName;
    }

    public void setParentValidationName(String parentValidationName) {
        this.parentValidationName = parentValidationName;
    }

    public long getActionTypeDBID() {
        return actionTypeDBID;
    }

    public void setActionTypeDBID(long actionTypeDBID) {
        this.actionTypeDBID = actionTypeDBID;
    }

    public long getScreenMode() {
        return screenMode;
    }

    public void setScreenMode(long screenMode) {
        this.screenMode = screenMode;
    }

    public long getDbid() {
        return dbid;
    }

    public void setDbid(long dbid) {
        this.dbid = dbid;
    }

    public long getOownerEntityDBID() {
        return oownerEntityDBID;
    }

    public void setOownerEntityDBID(long oownerEntityDBID) {
        this.oownerEntityDBID = oownerEntityDBID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getOfuntionDBID() {
        return ofuntionDBID;
    }

    public void setOfuntionDBID(long ofuntionDBID) {
        this.ofuntionDBID = ofuntionDBID;
    }

    public String getOfunctionName() {
        return ofunctionName;
    }

    public void setOfunctionName(String ofunctionName) {
        this.ofunctionName = ofunctionName;
    }

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public boolean isDisplayed() {
        return displayed;
    }

    public void setDisplayed(boolean displayed) {
        this.displayed = displayed;
    }

    public String getOfunctionType() {
        return ofunctionType;
    }

    public void setOfunctionType(String ofunctionType) {
        this.ofunctionType = ofunctionType;
    }

    public String getOfunctionScreenName() {
        return ofunctionScreenName;
    }

    public void setOfunctionScreenName(String ofunctionScreenName) {
        this.ofunctionScreenName = ofunctionScreenName;
    }

    public String getDisplayCondition() {
        return displayCondition;
    }

    public void setDisplayCondition(String displayCondition) {
        this.displayCondition = displayCondition;
    }

    public OEntityActionDTO(long dbid, String name, long ofuntionDBID,
            String ofunctionName, long oownerEntityDBID,
            String oownerEntityClassPath, long entityActionPrivilegeDBID,
            String entityActionPrivilegeName, int sortIndex, boolean displayed,
            long parentValidationDBID, String parentValidationName,
            long actionTypeDBID,String displayCondition) {
        this.dbid = dbid;
        this.name = name;
        this.ofuntionDBID = ofuntionDBID;
        this.ofunctionName = ofunctionName;
        this.oownerEntityDBID = oownerEntityDBID;
        this.oownerEntityClassPath = oownerEntityClassPath;
        this.entityActionPrivilegeDBID = entityActionPrivilegeDBID;
        this.entityActionPrivilegeName = entityActionPrivilegeName;
        this.sortIndex = sortIndex;
        this.displayed = displayed;
        this.parentValidationDBID = parentValidationDBID;
        this.parentValidationName = parentValidationName;
        this.actionTypeDBID = actionTypeDBID;
        this.displayCondition=displayCondition;
    }
    
    public OEntityActionDTO(long dbid, String name, long ofuntionDBID,
            String ofunctionName, long oownerEntityDBID,
            String oownerEntityClassPath, long entityActionPrivilegeDBID,
            String entityActionPrivilegeName, int sortIndex, boolean displayed,
            long parentValidationDBID, String parentValidationName,
            long actionTypeDBID) {
        this.dbid = dbid;
        this.name = name;
        this.ofuntionDBID = ofuntionDBID;
        this.ofunctionName = ofunctionName;
        this.oownerEntityDBID = oownerEntityDBID;
        this.oownerEntityClassPath = oownerEntityClassPath;
        this.entityActionPrivilegeDBID = entityActionPrivilegeDBID;
        this.entityActionPrivilegeName = entityActionPrivilegeName;
        this.sortIndex = sortIndex;
        this.displayed = displayed;
        this.parentValidationDBID = parentValidationDBID;
        this.parentValidationName = parentValidationName;
        this.actionTypeDBID = actionTypeDBID;
    }

    public OEntityActionDTO(long dbid, long oownerEntityDBID, String name,
            long ofuntionDBID, String ofunctionName, int sortIndex,
            boolean displayed, String displayCondition) {
        this.dbid = dbid;
        this.oownerEntityDBID = oownerEntityDBID;
        this.name = name;
        this.ofuntionDBID = ofuntionDBID;
        this.ofunctionName = ofunctionName;
        this.sortIndex = sortIndex;
        this.displayed = displayed;
        this.displayCondition=displayCondition;
    }
    
   public OEntityActionDTO(long dbid, long oownerEntityDBID, String name,
            long ofuntionDBID, String ofunctionName, int sortIndex,
            boolean displayed) {
        this.dbid = dbid;
        this.oownerEntityDBID = oownerEntityDBID;
        this.name = name;
        this.ofuntionDBID = ofuntionDBID;
        this.ofunctionName = ofunctionName;
        this.sortIndex = sortIndex;
        this.displayed = displayed;
    }
    
    public OEntityActionDTO() {
    }
    //<editor-fold defaultstate="collapsed" desc="Display Condition">
//    private String displayCondition;
//
//    public String getDisplayCondition() {
//        return displayCondition;
//    }
//
//    public void setDisplayCondition(String displayCondition) {
//        this.displayCondition = displayCondition;
//    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Display Condition">
//    private String displayConditionFinal;
//
//    public String getDisplayConditionFinal() {
//        return displayConditionFinal;
//    }
//
//    public void setDisplayConditionFinal(String displayConditionFinal) {
//        this.displayConditionFinal = displayConditionFinal;
//    }
    //</editor-fold>
}
