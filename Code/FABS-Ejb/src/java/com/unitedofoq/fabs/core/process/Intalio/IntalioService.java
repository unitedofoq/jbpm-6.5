/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.process.Intalio;

import java.util.List;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.security.user.Inbox;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;

/**
 *
 * @author melsayed
 */
public interface IntalioService {
    public void claimTask(String task, OUser user);
    public void completeTask(String task, OUser user);
    public void exitTask(String task, OUser user);
    public void failTask(String task, OUser user);
    public void resumeTask(String task, OUser user);
    public void startTask(String task, OUser user);
    public void delegateTask(String task, OUser user, String delegatedUser);
    public void releaseTask(String task, OUser user);
    public void setTaskOutputAndComplete(String task, String output, OUser user);
    public void setTaskOutput(String task, String output, OUser user);
    public void skipTask(String task, OUser user);
    public List<IntalioTaskInstance> getUserOutbox(String userName, String password);
    public List<IntalioTaskInstance> getUserInbox(String userName, String password);
    public String getTaskInput(String task, OUser user);
    public String getNextHTScreen(String currentTask,  String userName, String password);
    public String getUserNextHTScreen(String currentTask, String user, OUser loggedUser);
    public List<IntalioTaskInstance> getUserCompletedTasks(String selectedUser, OUser user);
    public List<IntalioTaskInstance> getUserCompletedTasksInfo(OUser selectedUser, OUser user);
    public OScreen getTaskScreen(String taskInfo, OUser user);
    public List<String> getUserClaimedTasks(OUser selectedUser, OUser user);
    public List<String> getUserOpenTasks(OUser selectedUser, OUser user);
    public List<String> getUserWorkableTasksInfo(OUser selectedUser, OUser user);

    public List<Inbox> getUserInbox(OUser loggedUser);
    /***/
    public List<IntalioTask> getUserWorkableTasks(OUser selectedUser, OUser user);
    /***/

    public String getStringInstance(String taskID, OUser user);
    public IntalioTask getTaskInstanceDetails(String taskID, OUser user);
    public String getProcessNameFromURL(String processWSDLURL);
    public String getProcessState(long processId, OUser user);
    public List<IntalioTask> getProcessTasks(Long processId, OUser user);
    public void restartProcessInstance(long processId, OUser user);
    public void resumeProcessInstance(long processId, OUser user);
    public long startProcess(IntalioProcess process, ODataMessage processInput, OUser user);
    public void suspendProcess(long processId, OUser user);
    public void terminateProcess(long processId, OUser user);
    public List<IntalioProcessInstance> getRunningProcessInstances(); // Completed
    public String getProcessEngineURL();    // Completed
    public IntalioProcessInstance getProcessInstanceByProcessID(Long processID, OUser user);
    public Long getTaskProcessID(String task, OUser user);

}
