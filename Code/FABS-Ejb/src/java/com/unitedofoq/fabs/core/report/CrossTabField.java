package com.unitedofoq.fabs.core.report;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.validation.OEntityDataValidationException;

@Entity
@DiscriminatorValue(value = "CROSS")
@ParentEntity(fields={"oreport"})
@ChildEntity(fields={"crosstabFieldColumns"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class CrossTabField extends OReportField {

    @JoinColumn(nullable=false)
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    // <editor-fold defaultstate="collapsed" desc="crossEntity">
    private OEntity crossEntity;

    public OEntity getCrossEntity() {
        return crossEntity;
    }

    public String getCrossEntityDD() {
        return "CrossTabField_crossEntity";
    }

    public void setCrossEntity(OEntity crossEntity) {
        this.crossEntity = crossEntity;
    }
    //</editor-fold>


    @OneToMany(mappedBy="crosstabField")
    // <editor-fold defaultstate="collapsed" desc="crosstabFieldColumns">
    private List<CrosstabFieldColumn> crosstabFieldColumns;

    public List<CrosstabFieldColumn> getCrosstabFieldColumns() {
        List<CrosstabFieldColumn> notDeletedColumns = new ArrayList<CrosstabFieldColumn>();
        for (CrosstabFieldColumn crosstabFieldColumn : crosstabFieldColumns) {
            if (!crosstabFieldColumn.isInActive())
                notDeletedColumns.add(crosstabFieldColumn);
        }
        return notDeletedColumns;
    }

    public void setCrosstabFieldColumns(List<CrosstabFieldColumn> crosstabFieldColumns) {
        this.crosstabFieldColumns = crosstabFieldColumns;
    }

    public String getCrosstabFieldColumnsDD() {
        return "CrossTabField_crosstabFieldColumns";
    }
    //</editor-fold>


    // <editor-fold defaultstate="collapsed" desc="viewTotalColumn">
    private boolean viewTotalColumn;

    public boolean isViewTotalColumn() {
        return viewTotalColumn;
    }

    public String getViewTotalColumnDD() {
        return "CrossTabField_viewTotalColumn";
    }

    public void setViewTotalColumn(boolean viewTotalColumn) {
        this.viewTotalColumn = viewTotalColumn;
    }
    //</editor-fold>


    @Override
    public void dataNativeValidation(OUser loggedUser) throws OEntityDataValidationException {
        if (super.getFieldExpression() == null)
            super.setFieldExpression("");
    }
}
