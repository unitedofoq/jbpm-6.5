/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.multitanantuser;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;

/**
 *
 * @author MEA
 */
public class UserTenantRoleTempEntity extends BaseEntity {

    //<editor-fold defaultstate="collapsed" desc="dbid">
    //this Method is used if we want to set the dbid but not use the original setDbid();
    public void setCustomDbid(long dbid) {
        this.dbid = dbid;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="User">
    private String user;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUserDD() {
        return "UserTenantRoleTempEntity_user";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Roles">
    private String roles;

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String getRolesDD() {
        return "UserTenantRoleTempEntity_roles";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Tenant">
    private String tenant;

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public String getTenantDD() {
        return "UserTenantRoleTempEntity_tenant";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Active">
    protected boolean active;// = false;

    public String getActiveDD() {
        return "UserTenantRole_active";
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    //</editor-fold>
}