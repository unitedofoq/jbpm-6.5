/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.filter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;



import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.function.FieldExpParserFunction;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.i18n.Translation;

import com.unitedofoq.fabs.core.report.ReportFilter;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.timezone.TimeZoneServiceLocal;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.screen.EntityFilter;
import com.unitedofoq.fabs.core.uiframework.screen.EntityFilterScreenAction;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenFilter;
import com.unitedofoq.fabs.core.uiframework.screen.SingleEntityScreen;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.fabs.core.webservice.WebServiceFilter;
import java.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mmohamed
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.filter.OFilterServiceLocal",
        beanInterface = OFilterServiceLocal.class)
public class OFilterServiceBean implements OFilterServiceLocal {

    @EJB
    EntitySetupServiceRemote entitySetupServiceRemote;
    @EJB
    OEntityManagerRemote oem;
    @EJB
    OFunctionServiceRemote functionService;
    @EJB
    UserMessageServiceRemote userMessageServiceRemote;
    @EJB
    private TimeZoneServiceLocal timeZoneService;
    final static Logger logger = LoggerFactory.getLogger(OFilterServiceBean.class);

    private String parseFieldExpDDLookupFilter(String filterValue, OUser loggedUser) {
        logger.trace("Entering");
        String parsedExp = null;
        try {
            if (filterValue.contains("{*")) {
                String valueAfterEq = filterValue.substring(filterValue.indexOf("{*"));
                int rootEndIndex = valueAfterEq.indexOf(".");
                // "." not found
                if (rootEndIndex == -1) {
                    rootEndIndex = valueAfterEq.indexOf("}");
                }
                String rootExp = valueAfterEq.substring(valueAfterEq.indexOf("{*") + 2, rootEndIndex);
                List<String> rootExpConds = new ArrayList<String>();
                rootExpConds.add("rootExpression = '" + rootExp + "'");
                rootExpConds.add(OEntityManager.CONFCOND_CASE_INSENSITIVE);

                // Load the FieldExpParserFunction for this rootExpression
                FieldExpParserFunction fieldExpParserFunction = (FieldExpParserFunction) oem.loadEntity(FieldExpParserFunction.class.getSimpleName(),
                        rootExpConds, null, loggedUser);
                // Throw error and continue
                if (fieldExpParserFunction == null) {
                    logger.warn("FieldExpRepFunction is null. rootExpression: {}", rootExp);
                    logger.trace("Returning with Null");
                    return null;
                }

                // Execute the fieldExpRepFunction function
                OFunctionResult oFR = new OFunctionResult();
                OFunctionParms functionParams = new OFunctionParms();
                functionParams.getParams().put("filterFieldExpression", valueAfterEq);
                OFunctionResult invocationResult = functionService.runFunction(fieldExpParserFunction, new ODataMessage(),
                        functionParams, loggedUser);

                String returnedExp = null;
                if (!(invocationResult.getReturnedDataMessage().getData().isEmpty())) {
                    returnedExp = (String) invocationResult.
                            getReturnedDataMessage().getData().get(0);
                }
                // Throw error and continue
                if (returnedExp == null) {
                    if(null!=fieldExpParserFunction)
                    logger.warn("Returned Expression from FieldExpRepFunction is null. rootExpression: {}, Function name: {}", rootExp, fieldExpParserFunction.getFunctionName());
                    logger.trace("Returning with Null");
                    return null;
                }

                // Return The Parsed Expression
                parsedExp = filterValue.substring(0, filterValue.indexOf("{*")) + returnedExp;
                oFR.append(invocationResult);
            } else {
                logger.warn("You must pass a FieldExpressionParser compatible filterValue, e.g. *{Employee}");
                return new String();
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("currentFilterString: {}, Filter Value: {}", filterValue, filterValue);
        }
        logger.trace("Returning with : {}", parsedExp);
        return parsedExp;
    }

    @Override
    public List<String> parseDDLookupFilter(String filterValue, OUser loggedUser) {
        logger.debug("Entering");
        if (filterValue != null && !filterValue.equals("")) {
            //  to hold the parsed conditions
            List<String> conditions = new ArrayList();
            String tmpLookupFilter = filterValue;

            //  while the conditions delimiter still exists in 'lookupFilter'...
            while (tmpLookupFilter.indexOf(";") != -1) {
                int delimiterIndex = tmpLookupFilter.indexOf(";");
                String condition = tmpLookupFilter.substring(0, delimiterIndex);
                if (condition.contains("{*")) {
                    condition = parseFieldExpDDLookupFilter(condition, loggedUser);
                }
                conditions.add(condition);
                tmpLookupFilter = tmpLookupFilter.substring(delimiterIndex + 1);
            }

            //  Add the last condition that was in 'lookupFilter'
            if (tmpLookupFilter.contains("{*")) {
                conditions.add(parseFieldExpDDLookupFilter(tmpLookupFilter, loggedUser));
            } else {
                conditions.add(tmpLookupFilter);
            }

            //  return the ArrayList containing the parsed conditions
            if(conditions!=null)
            logger.debug("Returning with consitions of size", conditions.size());
            return conditions;
        } else {
            logger.debug("Returning with new ArrayList");
            return new ArrayList();
        }
    }

    @Override
    public List<String> getFilterConditions(OFilter filter, UserSessionInfo userSesssionInfo, OUser loggedUser)
            throws FilterDDRequiredException {
        logger.debug("Entering");
        OEntityDTO actOnEntity = null;

        if (filter instanceof ReportFilter) {
            actOnEntity = entitySetupServiceRemote.getOReportEntityDTO(
                    ((ReportFilter) filter).getReport().getDbid(), loggedUser);
        } else if (filter instanceof WebServiceFilter) {
            actOnEntity = entitySetupServiceRemote.getWebServiceEntityDTO(
                    ((WebServiceFilter) filter).getDbid(), loggedUser);

        } else if (filter instanceof ScreenFilter) {
            if (((ScreenFilter) filter).getScreen() instanceof SingleEntityScreen) {
                SingleEntityScreen currentScreen = (SingleEntityScreen) ((ScreenFilter) filter).getScreen();
                actOnEntity = entitySetupServiceRemote.getOScreenEntityDTO(
                        currentScreen.getDbid(), loggedUser);
            }
        } else if(filter instanceof EntityFilter){
            actOnEntity = entitySetupServiceRemote.loadOEntityDTOByDBID(((EntityFilter)filter).getOentity().getDbid(), loggedUser);
        }

        if (filter.getFilterFields() == null || filter.getFilterFields().isEmpty()) {
            logger.debug("Returning with new ArrayList");
            return new ArrayList<String>();
        }

        /**
         * FieldExpressionParser
         */
        List<FilterField> filterFields = filter.getFilterFields();
        for (FilterField filterField : filterFields) {
            String filterFieldValue = filterField.getFieldValue();
            try {
                if (filterFieldValue != null && filterFieldValue.startsWith("{*")) {
                    int rootEndIndex = filterFieldValue.indexOf(".");
                    // "." not found
                    if (rootEndIndex == -1) {
                        rootEndIndex = filterFieldValue.indexOf("}");
                    }
                    String rootExp = filterFieldValue.substring(2, rootEndIndex);
                    List<String> rootExpConds = new ArrayList<String>();
                    rootExpConds.add("rootExpression = '" + rootExp + "'");
                    rootExpConds.add(OEntityManager.CONFCOND_CASE_INSENSITIVE);

                    // Load the FieldExpParserFunction for this rootExpression
                    FieldExpParserFunction fieldExpParserFunction = (FieldExpParserFunction) oem.loadEntity(FieldExpParserFunction.class.getSimpleName(),
                            rootExpConds, null, oem.getSystemUser(loggedUser));
                    // Throw error and continue
                    if (fieldExpParserFunction == null) {
                        logger.warn("FieldExpRepFunction is null. rootExpression: {}", rootExp);
                        continue;
                    }

                    // Execute the fieldExpRepFunction function
                    OFunctionResult oFR = new OFunctionResult();
                    OFunctionParms functionParams = new OFunctionParms();
                    functionParams.getParams().put("userSessionInfo", userSesssionInfo);
                    functionParams.getParams().put("filterFieldExpression", filterFieldValue);
                    OFunctionResult invocationResult = functionService.runFunction(fieldExpParserFunction, new ODataMessage(),
                            functionParams, loggedUser);

                    String returnedExp = (String) invocationResult.
                            getReturnedDataMessage().getData().get(0);
                    // Throw error and continue
                    if (returnedExp == null) {
                        if(null!=fieldExpParserFunction)
                        logger.warn("Returned Expression from FieldExpRepFunction is null. rootExpression: {}, Function name: {}", rootExp, fieldExpParserFunction.getFunctionName());
                        continue;
                    }
                    // Set the parsed field value in the original condition
                    filterField.setFieldValue(returnedExp);

                    oFR.append(invocationResult);
                }
            } catch (Exception ex) {
                logger.error("Exception thrown",ex);
                if(filter!=null)
                logger.trace("currentFilterString: {}, Filter Name: {}", filterFieldValue,filter.getFilterName());
            }
        }

        List<String> filterList = new ArrayList<String>();
        for (int i = 0; i < filter.getFilterFields().size(); i++) {
            String currentFilterString = null;
            String currentFilterString2 = null;
            try {
                FilterField ff = filter.getFilterFields().get(i);
                if (ff.getFieldValue() != null && !ff.getFieldValue().equals("") && !ff.isInActive()) {
                    currentFilterString = ff.getFieldExpression();
                    //String value = ff.getFieldValue();
                    if (ff.getFieldValue() != null && !ff.getFieldValue().equals("")) {
                        if (ff.getFieldExpression().contains(".") && (ff.getFieldDD().getControlType().getDbid() == DD.CT_DROPDOWN || ff.getFieldDD().getControlType().getDbid() == DD.CT_LOOKUPSCREEN)) {
                            String filterExp = getLookupEntityExpression(actOnEntity, currentFilterString) + ".dbid";
                            if (!ff.isFromTo()) {
                                String operator = ff.getOperator();
                                if (!operator.contains("!")) {
                                    currentFilterString = filterExp + " " + operator + " " + ff.getFieldValue();
                                } else {
                                    currentFilterString = "NOT " + filterExp + " " + operator.trim().substring(1) + " "
                                            + ff.getFieldValue();
                                }
                            } else {
                                if (ff.getFieldValue() != null && !ff.getFieldValue().equals("") && !ff.getFieldValueTo().equals("") && ff.getFieldValue() != null) {
                                    currentFilterString = filterExp + " Between " + ff.getFieldValue() + " AND " + ff.getFieldValueTo();
                                } else {
                                    logger.warn("From-To Structure Error");
                                }
                            }
                        } else {
                            List<ExpressionFieldInfo> list = BaseEntity.parseFieldExpression(Class.forName(actOnEntity.getEntityClassPath()), ff.getFieldExpression());
                            for (int j = 0; j < list.size(); j++) {
                                if (ff.getFieldExpression().endsWith(list.get(j).fieldName)) {
                                    if (list.get(j).field.getType() == String.class) {
                                        if (list.get(j).field.getAnnotation(Translation.class) != null) {
                                            String originalField = ((Translation) list.get(j).field.getAnnotation(Translation.class)).originalField();
                                            if (currentFilterString.contains(".")) {
                                                currentFilterString = currentFilterString.substring(0, currentFilterString.lastIndexOf(".") + 1) + originalField;
                                            } else {
                                                currentFilterString = originalField;
                                            }
                                        } else {

                                        }
                                        String operator = ff.getOperator();
                                        if(!ff.isFromTo()){
                                            if(operator.equals("IN")){
                                                currentFilterString = currentFilterString + " " + operator + " ("
                                                        + ff.getFieldValue() + ")";
                                            } else if (!operator.contains("!")) {
                                                currentFilterString = currentFilterString + " " + operator + " '"
                                                        + ff.getFieldValue() + "'";
                                            } else {
                                                currentFilterString = "NOT " + currentFilterString + " " + operator.trim().substring(1)
                                                        + " '" + ff.getFieldValue() + "'";
                                            }
                                        } else {
                                            if (ff.getFieldValue() != null && !ff.getFieldValue().equals("") && !ff.getFieldValueTo().equals("") && ff.getFieldValue() != null) {
                                                String fromVal = ff.getFieldValue();
                                                String toVal = ff.getFieldValueTo();
                                                currentFilterString += " Between '" + fromVal + "' And '" + toVal+"'";
                                            } else {
                                                logger.warn("From-To Structure Error");
                                            }
                                        }
                                    } else if (list.get(j).field.getType() == Long.class
                                            || list.get(j).field.getType() == long.class
                                            || list.get(j).field.getType() == int.class
                                            || list.get(j).field.getType() == Integer.class
                                            || list.get(j).field.getType() == double.class
                                            || list.get(j).field.getType() == Double.class
                                            || list.get(j).field.getType() == BigDecimal.class) {
                                        if (!ff.isFromTo()) {
                                            String operator = ff.getOperator();
                                            if (!operator.contains("!")) {
                                                currentFilterString = currentFilterString + " " + operator + " "
                                                        + ff.getFieldValue() + "";
                                            } else {
                                                currentFilterString = "NOT " + currentFilterString + " " + operator.trim().substring(1)
                                                        + " " + ff.getFieldValue() + "";
                                            }
                                        } else {
                                            if (ff.getFieldValue() != null && !ff.getFieldValue().equals("") && !ff.getFieldValueTo().equals("") && ff.getFieldValue() != null) {
                                                String fromVal = ff.getFieldValue();
                                                String toVal = ff.getFieldValueTo();
                                                currentFilterString += " Between " + fromVal + " And " + toVal;
                                            } else {
                                                logger.warn("From-To Structure Error");
                                            }
                                        }
                                    } else if (list.get(j).field.getType() == Boolean.class || list.get(j).field.getType() == boolean.class) {
                                        String operator = ff.getOperator();
                                        if (!operator.contains("!")) {
                                            currentFilterString = currentFilterString + " " + operator + " "
                                                    + ff.getFieldValue() + "";
                                        } else {
                                            currentFilterString = "NOT " + currentFilterString + " " + operator.trim().substring(1)
                                                    + " " + ff.getFieldValue() + "";
                                        }
                                    } else if (list.get(j).field.getType() == Date.class) {
                                        if (!ff.isFromTo()) {
                                            String fieldValue = ff.getFieldValue();
                                            if (fieldValue.equalsIgnoreCase("#current_date")) {
                                                FilterField.dateFormat.format(timeZoneService.getUserCDT(loggedUser)); //new Date()
                                            }
                                            String operator = ff.getOperator();
                                            if (!operator.contains("!")) {
                                                currentFilterString = currentFilterString + " " + operator + " '"
                                                        + fieldValue + "'";
                                            } else {
                                                currentFilterString = "NOT " + currentFilterString + " " + operator.trim().substring(1)
                                                        + " " + fieldValue + "";
                                            }
                                        } else {
                                            if (ff.getFieldValue() != null && !ff.getFieldValue().equals("") && !ff.getFieldValueTo().equals("") && ff.getFieldValue() != null) {
                                                String fromVal = ff.getFieldValue();
                                                String toVal = ff.getFieldValueTo();

                                                currentFilterString2 = currentFilterString + " >= '" + fromVal + "'";
                                                currentFilterString += " <= '" + toVal + "'";

                                            } else {
                                                logger.warn("From-To Structure Error");
                                            }
                                        }
                                    } else {
                                        logger.warn("Unknown Filter Field Type");
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (ClassNotFoundException ex) {
                logger.error("Exception thrown",ex);
                //OLog.logError(ex.getStackTrace());
            }

            if (currentFilterString != null) {
                filterList.add(currentFilterString);
            }
            if (currentFilterString2 != null) {
                filterList.add(currentFilterString2);
            }

        }

        if (!filterList.isEmpty()) {
            filterList.add(OEntityManager.CONFCOND_CASE_INSENSITIVE);
        }
        if(filterList!=null)
        logger.debug("Returning with filterList of size: {}",filterList.size());
        return filterList;

    }

    @EJB
    UIFrameworkServiceRemote uiFrameWorkFacade;

    private String getLookupEntityExpression(OEntityDTO actoOnEntity, String fieldExpression) {
        logger.trace("Entering");
        try {
            Class actOnEntityCls = Class.forName(actoOnEntity.getEntityClassPath());
            List<ExpressionFieldInfo> infos = BaseEntity.parseFieldExpression(actOnEntityCls, fieldExpression);
            int index = uiFrameWorkFacade.getBalloonFieldIndex(actOnEntityCls, infos);
            String fldExpression = "";
            for (int fieldIndex = 0; fieldIndex <= index; fieldIndex++) {
                if (fieldIndex != 0) {
                    fldExpression += ".";
                }
                fldExpression += infos.get(fieldIndex).fieldName;
            }
            logger.trace("Returning with: {}",fldExpression);
            return fldExpression;
        } catch (ClassNotFoundException ex) {
            logger.error("Exception thrown",ex);
        }
        logger.trace("returning with empty string");
        return "";
    }

    @Override
    public List<String> getFilterConditionsForProcess(OFilter filter, OUser loggedUser) {
        logger.debug("Entering");
        if (filter == null) {
            logger.debug("Entering");
            return new ArrayList<String>();
        }
        List<String> conds = new ArrayList<String>();
        for (int i = 0; i < filter.getFilterFields().size(); i++) {
            FilterField filterField = filter.getFilterFields().get(i);
            if (filterField.getFieldExpression().equalsIgnoreCase("initiator")) {
                String id = "0";
                try {
                    List<Long> ids = oem.executeEntityListNativeQuery("select processinstanceid from outbox "
                            + "where user_dbid ='" + loggedUser.getDbid()
                            + "' and not processinstanceid is null and process = 1", loggedUser);
                    for (int j = 0; j < ids.size(); j++) {
                        id += "," + ids.get(j);
                    }
                } catch (Exception ex) {
                    logger.error("Exception thrown",ex);
                }
                conds.add("id in (" + id + ")");
            } else if (filterField.getFieldExpression().equalsIgnoreCase("currentexecuters")) {
            } else {
                conds.add(filterField.getFieldExpression() + " = '" + filterField.getFieldValue() + "'");
            }
        }
        if(conds!=null)
        logger.debug("Returning with conds of size: {}",conds.size());
        return conds;
    }

    @Override
    public OFunctionResult validateOneEntityFilterOnScreen(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering: validateOneEntityFilterOnScreen");
        OFunctionResult oFR = new OFunctionResult();
        EntityFilterScreenAction efsa = (EntityFilterScreenAction) odm.getData().get(0);
        try {
            List<EntityFilterScreenAction> efsal = oem.loadEntityList(EntityFilterScreenAction.class.getSimpleName(), 
                    Collections.singletonList("screen.dbid = "+efsa.getScreen().getDbid())
                    , null, null, loggedUser);
            if (efsal != null && efsal.size() > 0) {
                oFR.addError(userMessageServiceRemote.getUserMessage("OneFilterOnScreen", loggedUser));
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning from: validateOneEntityFilterOnScreen");
        return oFR;
    }

}
