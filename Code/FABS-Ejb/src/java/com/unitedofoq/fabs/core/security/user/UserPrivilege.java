/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.security.user;

/**
 *
 * @author lap
 */
public class UserPrivilege {
    public enum ConstantUserPrivilege {

        option_button,
        option_managepageportlets,
        option_exportcurrentpage,
        option_printpage,
        option_exceltemplate,
        option_screenfldsmenuitem,
        option_entityfiltermenuitem,
        option_reloadgear,
        option_exportscreenmetadata,
        option_exportscreendata,
        option_screentransmenuitem,
        option_importdata,
        option_showoptions,
        option_exportPdf,
        option_exportExcel,
        option_exportCsv,
        Balloon_ActionAttatchement,
        Balloon_ActionFormDesigner,
        Balloon_ViewAttatchement,
        Attachment_deleteButton,
        Attachment_downloadButton,
        Attachment_uploadPnl,
        Attachment_viewPnl,
        Attachment_viewAll,
        Attachment_PreSave
    }
}
