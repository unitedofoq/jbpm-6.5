/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.function;

import com.unitedofoq.fabs.core.alert.entities.BaseAlert;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.process.ProcessButtonJavaFunction;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author nsaleh
 */
@Entity
@DiscriminatorValue(value = "JAVA")
@ChildEntity(fields = {"functionImage", "menuFunction", "runningScheduleExpression"})
@VersionControlSpecs(omoduleFieldExpression = "omodule")
public class JavaFunction extends OFunction {

    @OneToOne(mappedBy = "javaFunction")
    private BaseAlert baseAlert;

    public String getBaseAlertDD() {
        return "JavaFunction_baseAlert";
    }

    public BaseAlert getBaseAlert() {
        return baseAlert;
    }

    public void setBaseAlert(BaseAlert baseAlert) {
        this.baseAlert = baseAlert;
    }

    // <editor-fold defaultstate="collapsed" desc="Running Schedule Expression">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, mappedBy = "javaFunction")
    private ScheduleExpression runningScheduleExpression;

    public String getRunningScheduleExpressionDD() {
        return "JavaFunction_runningScheduleExpression";
    }

    public ScheduleExpression getRunningScheduleExpression() {
        return runningScheduleExpression;
    }

    public void setRunningScheduleExpression(ScheduleExpression runningScheduleExpression) {
        this.runningScheduleExpression = runningScheduleExpression;
    }
    // <editor-fold defaultstate="collapsed" desc="notificationType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC notificationType;

    public UDC getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(UDC notificationType) {
        this.notificationType = notificationType;
    }

    public String getNotificationTypeDD() {
        return "JavaFunction_notificationType";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="omodule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private OModule omodule;

    public String getOmoduleDD() {
        return "JavaFunction_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="functionClassPath">
    @Column(nullable = false)
    private String functionClassPath;

    public String getFunctionClassPath() {
        return functionClassPath;
    }

    public void setFunctionClassPath(String classPath) {
        this.functionClassPath = classPath;
    }

    public String getFunctionClassPathDD() {
        return "JavaFunction_functionClassPath";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="functionName">
    @Column(nullable = false, unique = true)
    private String functionName;

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public String getFunctionNameDD() {
        return "JavaFunction_functionName";
    }
    // </editor-fold>

    @OneToMany(mappedBy = "javaFunction")
    private List<ProcessButtonJavaFunction> processButtonJavaFunction;

    public List<ProcessButtonJavaFunction> getProcessButtonJavaFunction() {
        return processButtonJavaFunction;
    }

    public void setProcessButtonJavaFunction(List<ProcessButtonJavaFunction> processButtonJavaFunction) {
        this.processButtonJavaFunction = processButtonJavaFunction;
    }
}
