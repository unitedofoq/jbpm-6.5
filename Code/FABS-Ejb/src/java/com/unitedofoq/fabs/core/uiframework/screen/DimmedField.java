/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author rsamir
 */
@Entity
@ParentEntity(fields = {"screenFieldValue"})
public class DimmedField extends BaseEntity{
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private ScreenFieldValue screenFieldValue;

    public ScreenFieldValue getScreenFieldValue() {
        return screenFieldValue;
    }
    
    public String getScreenFieldValueDD() {
        return "DimmedField_screenFieldValue";
    }

    public void setScreenFieldValue(ScreenFieldValue screenFieldValue) {
        this.screenFieldValue = screenFieldValue;
    }
    
    private String screenField;

    public String getScreenField() {
        return screenField;
    }

    public void setScreenField(String screenField) {
        this.screenField = screenField;
    }
    
    public String getScreenFieldDD() {
        return "DimmedField_screenField";
    }
    
}
