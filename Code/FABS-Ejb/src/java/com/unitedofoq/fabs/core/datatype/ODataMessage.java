/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.datatype;

import java.io.Serializable;
import java.util.List;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;

/**
 *
 * @author melsayed
 */
public class ODataMessage implements Serializable
{
    public ODataMessage() {
    }
    
    public ODataMessage(ODataType oDataType, List<Object> data) {
        this.oDataType = oDataType;
        this.data = data;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append("\n\t\t*#* Data Type" + "\n");

        if (oDataType == null) {
            result.append("\t\t\tnull!\n") ;
        } else {
            result.append("\t\t\tName: ").append(oDataType.getName()).append("\n");
            result.append("\t\t\tDescription: ").append(oDataType.getDescription()).append("\n");
        }

        result.append("\t\t*#* Data" + "\n");

        if (data == null) {
            result.append("\t\t\tnull!\n") ;
        } else {
            if(!data.isEmpty()) {
                result.append("\t\t\tSize = ").append(data.size()).append("\n");
                for(int dataIndex = 0 ; dataIndex < data.size(); dataIndex ++)
                {
                    Object object = data.get(dataIndex) ;
                    if (object == null) {
                        result.append("\t\t\tObject Is Null\n") ;
                        continue ;
                    }
                    result.append("\t\t\t").append(dataIndex + 1).append(") Class: ")
                            .append(object.getClass().getName()).append("\n");
                    result.append("\t\t\t").append(dataIndex + 1).append(") Value: ")
                            .append(object).append("\n");
                }
            } else {
                result.append("\t\t\tData size equals zero" + "\n");
            }
        }
        return result.toString();
    }

    // <editor-fold defaultstate="collapsed" desc="oDataType">
    private ODataType oDataType;

    /**
     * @return the oDataType
     */
    public ODataType getODataType() {
        return oDataType;
    }

    /**
     * @param oDataType the oDataType to set
     */
    public void setODataType(ODataType oDataType) {
        this.oDataType = oDataType;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="data">
    private List<Object> data;
    /**
     * @return the data
     */
    public List<Object> getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(List<Object> data) {
        this.data = data;
    }
    // </editor-fold>
    
    public boolean dataIsNotEmpty() {
        return (    data != null 
                &&  !data.isEmpty()) ;
    }
    
    public boolean dataIsBaseEntity() {
        return (    data != null 
                &&  !data.isEmpty()
                &&  data.get(0) instanceof BaseEntity) ;
    }
}
