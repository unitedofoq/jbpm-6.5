/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.comunication;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.slf4j.LoggerFactory;

/**
 * @author Hammad
 */
public class JMSSender {

    final static org.slf4j.Logger logger = LoggerFactory.getLogger(JMSSender.class);

    /**
     *
     * @param messageData the value of messageData
     * @param eMailSender the value of eMailSender
     * @throws JMSException
     */
    public static void sendJMSMessageToQueue(String messageData, Queue queue, ConnectionFactory queueFactory, String messageType) throws JMSException {

        logger.debug("Entering: ");
        Connection connection = null;
        Session session = null;
        try {
            connection = queueFactory.createConnection();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = session.createProducer(queue);
            TextMessage tm = session.createTextMessage(messageData);
            tm.setJMSType(messageType);
            messageProducer.send(tm);
            
        } catch (JMSException e) {
            logger.error("Exception thrown while sending message to queue", e);
        } 
        finally {

            try {
                session.close();
            } catch (JMSException e) {
                logger.error("Exception thrown while closing JMS session", e);
            }
            try {
                connection.close();
            } catch (Exception ex) {
                logger.info(ex.getMessage());
            }

        }
        logger.debug("Returning ");
    }
}
