/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework;

import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntityAction;
import com.unitedofoq.fabs.core.entitysetup.OObjectBriefInfoField;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.entitysetup.dto.OObjectBriefInfoFieldDTO;
import com.unitedofoq.fabs.core.filter.FilterFieldDTO;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.function.OMenu;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.SecurityServiceRemote;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.RoleMenu;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.session.ScreenSessionInfo;
import com.unitedofoq.fabs.core.setup.FABSSetupLite;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import com.unitedofoq.fabs.core.timezone.TimeZoneServiceLocal;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.udc.UDCBase;
import com.unitedofoq.fabs.core.uiframework.orgchart.OrgChartFunctionLite;
import com.unitedofoq.fabs.core.uiframework.portal.*;
import com.unitedofoq.fabs.core.uiframework.screen.*;
import com.unitedofoq.fabs.core.uiframework.screen.dto.OScreenDTO;
import com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenDTMappingAttrDTO;
import com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenInputDTO;
import com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenOutputDTO;
import com.unitedofoq.fabs.core.uiframework.serverjob.resources.ServerJobDTO;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.fabs.core.validation.FABSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.naming.InitialContext;
import javax.persistence.OneToMany;
import javax.sql.DataSource;
import java.lang.reflect.Field;
import java.sql.Connection;

/**
 *
 * @author mibrahim
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote",
        beanInterface = UIFrameworkServiceRemote.class)
public class UIFrameworkService implements UIFrameworkServiceRemote {

    final static Logger logger = LoggerFactory.getLogger(UIFrameworkService.class);
    @EJB
    private OCentralEntityManagerRemote centralOEM;
    @EJB
    FABSSetupLocal fABSSetupLocal;
    @EJB
    UIFrameworkServiceRemote uIFrameworkService;
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private SecurityServiceRemote securityService;
    @EJB
    private EntitySetupServiceRemote entitySetupService;
    @EJB
    private UserMessageServiceRemote usrMsgService;
    @EJB
    protected DDServiceRemote ddService;
    @EJB
    protected DataTypeServiceRemote dataTypeService;
    @EJB
    private OFunctionServiceRemote functionService;
    @EJB
    private TimeZoneServiceLocal timeZoneService;
    //<editor-fold defaultstate="collapsed" desc="Screen Cache">
    public static Hashtable<String, Object> componentCache = new Hashtable<String, Object>();

    @Override
    public Object getCachedComponent(String componentId, OUser loggedUser) {
        logger.debug("Entering");
        logger.debug("Returning with Null");
        return null;
    }

    @Override
    public void addComponentToCache(String componentId, Object component, OUser loggedUser) {

    }

    @Override
    public void clearComponentsCache(OUser loggedUser) {
    }

    @Override
    public OFunctionResult clearScreenCache(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        logger.debug("Returning");
        return oFR;
    }
    //<editor-fold defaultstate="collapsed" desc="Panel Cache (Not Used Yet)">
    /**
     *
     * @param screenName
     * @param instance //screenCache (ScreenName+"_"+user_id) // List {
     * //Integer LANG //Many Hashtable<String, Object> // INSTANCE String //
     * PORTLET String //	SelectedList HashMap // } Any cached panel should apply
     * ICEFACES life cycle specially when cloning
     * @param panelName
     * @param loggedUser
     */
    public static Hashtable<String, List<Object>> screenCache = new Hashtable<String, List<Object>>();

    @Override
    public Object getCachedPanel(String screenName, String instance,
            String panelName, OUser loggedUser) {
        logger.debug("Entering");
        logger.debug("Returning with Null");
        return null;
    }

    @Override
    public void addPanelToCache(String screenName, String instance, String panelName,
            Object panelGrid, OUser loggedUser) {
    }

    @Override
    public void clearScreensCache(OUser loggedUser) {
    }

    public static String updateIds(String screenName, String instance, OUser loggedUser) {

        return instance;
    }

    @Override
    public void clearScreenCache(String screenName, OUser loggedUser) {
    }

    @Override
    public void close(String screenName, String instance, OUser loggedUser) {
    }

    //</editor-fold>
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="getScreenObject">
    /**
     * Called to get object of OScreen which has specific screnName Returns
     * object of OScreen which has the passed screen name
     *
     * @param screenName String value contains the name of the screen
     * @param loggedUser object of OUser that is already logged in
     * @return object of OScreen and null if there is no screen with the passed
     * screenName
     */
    @Override
    public OScreen getScreenObject(String screenName, OUser loggedUser) {
        logger.debug("Entering");
        OScreen oScreen = null;
        OUser screenUser;
        BaseEntity copyOScreen = null;
        try {
            oem.getEM(loggedUser);
            // <editor-fold defaultstate="collapsed" desc="getting system user">
            if (loggedUser.getDbid() == 0) {
                logger.trace("logged user DBID equals 0");
                screenUser = loggedUser.getOnBehalfOf();
            } else {
                logger.trace("logged user DBID not equals 0");
                screenUser = loggedUser;
            }
            // </editor-fold >

            // <editor-fold defaultstate="collapsed" desc="Loading Entity with its related inputs and output">
            // Set fieldExpressions
            List<String> fieldExpressions = new ArrayList<String>();
            fieldExpressions.add("screenInputs");
            fieldExpressions.add("screenOutputs");

            // Load the entity
            oScreen = (OScreen) oem.loadEntity(OScreen.class.getSimpleName(),
                    Collections.singletonList("name = '" + screenName + "'"),
                    fieldExpressions, oem.getSystemUser(loggedUser));
            logger.trace("Loading the entity: {}", oScreen);
            // </editor-fold>

            if (oScreen == null) { //1150002036 --1150002037
                logger.debug("Returning with Null as oscreen equals Null");
                return null;
            }

            // <editor-fold defaultstate="collapsed" desc="load user personalized screen">
            //load the personalized screen if the logged user has his own personalized screen
            if (oScreen.isPrsnlzd()) {
                logger.trace("load the personalized screen if the logged user has his own personalized screen");
                ArrayList<String> copyConditions = new ArrayList<String>();
                copyConditions.add("prsnlzdOriginal_DBID = " + oScreen.getDbid());
                copyConditions.add("prsnlzdUser_DBID = " + screenUser.getDbid());
                if (oScreen instanceof TabularScreen) {
                    copyOScreen = (PrsnlzdTabularScreen) oem.loadEntity(
                            PrsnlzdTabularScreen.class.getSimpleName(), copyConditions, null, loggedUser);
                } else if (oScreen instanceof FormScreen) {
                    copyOScreen = (PrsnlzdFormScreen) oem.loadEntity(
                            PrsnlzdFormScreen.class.getSimpleName(), copyConditions, null, loggedUser);
                } else {
                    logger.error("Screen is not FormScreen nor TabularScreen");
                }

                if (copyOScreen != null) {
                    OFunctionResult receivedOFR = null;
                    if (oScreen instanceof SingleEntityScreen) {
                        receivedOFR = deductPrsnlzdOScreen(oScreen, copyOScreen, loggedUser);
                    }
                    oScreen = (OScreen) receivedOFR.getReturnValues().get(0);
                }
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Loading Entity details for oActions, oObjectBriefInfoField, entityAttrInit, screenActions ">
            if (oScreen instanceof SingleEntityScreen) {

            }
            // </editor-fold>

            if (oScreen == null) {
                logger.debug("Returning with Null as screen equals Null");
                return null;
            }

            // <editor-fold defaultstate="collapsed" desc="Load screen inputs & outputs full information">
            List<String> attrMappingExpression = new ArrayList<String>();
            attrMappingExpression.add("attributesMapping");
            if (oScreen.getScreenOutputs() != null) {
                for (int i = 0; i < oScreen.getScreenOutputs().size(); i++) {
                    ScreenOutput scrOutput = oScreen.getScreenOutputs().get(i);
                    scrOutput = (ScreenOutput) oem.loadEntityDetail(scrOutput,
                            null, attrMappingExpression, oem.getSystemUser(loggedUser));
                    oScreen.getScreenOutputs().set(i, scrOutput);
                }
            }

            if (oScreen.getScreenInputs() != null) {
                for (int i = 0; i < oScreen.getScreenInputs().size(); i++) {
                    oScreen.getScreenInputs().set(i,
                            (ScreenInput) oem.loadEntityDetail(
                                    oScreen.getScreenInputs().get(i), null,
                                    attrMappingExpression, oem.getSystemUser(loggedUser)));
                }
            }
            // </editor-fold>

            List<String> hatFieldsExp = new ArrayList<String>();
            hatFieldsExp.add("entityHatFields");
            if (oScreen instanceof SingleEntityScreen) {
                loadScreenFields(((SingleEntityScreen) oScreen), oem.getSystemUser(loggedUser));
            }

            // <editor-fold defaultstate="collapsed" desc="Loading screen fields if the screen is personalized for the logged user">
            if (oScreen.isPrsnlzd() && copyOScreen != null) {
                if (oScreen instanceof SingleEntityScreen) {
                    SingleEntityScreen copySimulator = new SingleEntityScreen();
                    ArrayList<String> copyConditions2 = new ArrayList<String>();
                    copyConditions2.add("prsnlzdOriginal_DBID = " + oScreen.getDbid());
                    copyConditions2.add("prsnlzdUser_DBID = " + screenUser.getDbid());
                    copySimulator = (SingleEntityScreen) oem.loadEntity(
                            OScreen.class.getSimpleName(), copyConditions2, null, loggedUser);
                    loadScreenFields(copySimulator, loggedUser);
                    if (copySimulator.getScreenFields() != null) {
                        List<ScreenField> deductedScreens = deductPrsnlzdOScreenFieldsList(((SingleEntityScreen) oScreen).getScreenFields(), ((SingleEntityScreen) copySimulator).getScreenFields(), loggedUser);
                        ((SingleEntityScreen) oScreen).setScreenFields(deductedScreens);
                    }
                }
            }
            // </editor-fold>

        } catch (UserNotAuthorizedException ex) {
            System.out.println(ex.toString());
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
//        oem.closeEM(loggedUser);
        logger.debug("Returning");
        return oScreen;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="deductPrsnlzdOScreen">
    /**
     * Called to compare two screen object one is the original and the other is
     * copy of original for the logged user Returns OFunctionResult which
     * contains in the returnedValue the original OScreen after updating its
     * properties values according to the copy one Functionality : loop for
     * properties in the original and check if the copy has value for each one
     * the save the copy value But if copy value is null (User did not
     * personalize this property) save the original value
     *
     * @param originalOScreen String value contains the name of the screen
     * @param copiedOScreen String value contains the name of the screen
     * @param loggedUser object of OUser that is already logged in
     * @return OFunctionResult which contains in the returnedValue the original
     * OScreen after updating its properties values according to the copy one
     */
    @Override
    public OFunctionResult deductPrsnlzdOScreen(OScreen originalOScreen, BaseEntity copiedOScreen, OUser loggedUser) {
        Object fieldValue = null;
        OFunctionResult ofr = new OFunctionResult();
        try {
            FABSEntitySpecs entityFABSSpecs = originalOScreen.getClass().getAnnotation(FABSEntitySpecs.class);
            String[] prsnlzableFieldsNames = entityFABSSpecs.personalizableFields();

            if (entityFABSSpecs != null && prsnlzableFieldsNames != null) {
                for (String personalizableField : prsnlzableFieldsNames) {
                    fieldValue = BaseEntity.getValueFromEntity(copiedOScreen, personalizableField);
                    Field currentField = BaseEntity.getClassField(originalOScreen.getClass(), personalizableField);
                    if (fieldValue != null) {
                        if (currentField.getType() == boolean.class) {
                            //null value is saved 2 if the field is boolean
                            if (!fieldValue.toString().equalsIgnoreCase("2")) {
                                // if value not null (not 2) then save in value true or false
                                if (fieldValue.toString().equalsIgnoreCase("1")) {
                                    originalOScreen.invokeSetter(personalizableField, true);
                                } else {
                                    originalOScreen.invokeSetter(personalizableField, false);
                                }
                            }
                        } else {
                            originalOScreen.invokeSetter(personalizableField, fieldValue);
                        }
                    }
                }
            } else {
                ofr.addError(usrMsgService.getUserMessage("Annotation needed in entity", loggedUser), null);
            }

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        List<OScreen> returnedList = new ArrayList();
        returnedList.add(originalOScreen);
        ofr.setReturnValues(returnedList);
        return ofr;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="deductPrsnlzdOScreenFieldsList">
    /**
     * Called to compare fields of two screen object one is the original and the
     * other is copy of original for the logged user Returns list if screen
     * fields after updating original fields with personalized fields
     *
     * @param originalOScreenFields List of ScreenFields
     * @param copiedScreenFields List of ScreenFields
     * @param loggedUser object of OUser that is already logged in
     * @return List of ScreenFields
     */
    //@Override
    private List<ScreenField> deductPrsnlzdOScreenFieldsList(List<ScreenField> originalScreenFields, List<ScreenField> copiedScreenFields, OUser loggedUser) {
        try {
            List<ScreenField> returnedScreenFields = new ArrayList<ScreenField>();
            List<ScreenField> updatedCopiedScreenFields = new ArrayList<ScreenField>();
            boolean fieldFound;
            logger.trace("loop to add new added fields to personalized screen which doesnot exist in originals screen fields");
            //loop to add new added fields to personalized screen which doesnot exist in originals screen fields
            // new fields has prsnlzdOriginal_DBID = -1
            for (BaseEntity copiedField : copiedScreenFields) {
                if (((ScreenField) copiedField).getPrsnlzdOriginal_DBID() != -1) {
                    updatedCopiedScreenFields.add((ScreenField) copiedField);
                    continue;
                }
                if (!((ScreenField) copiedField).isPrsnlzdRemoved()) {
                    returnedScreenFields.add((ScreenField) copiedField);
                }
            }
            logger.trace("loop for original fields to get field that is found in copied screen fields");
            //loop for original fields to get field that is found in copied screen fields
            for (ScreenField originalField : originalScreenFields) {
                fieldFound = false;
                for (ScreenField fieldFromCopy : updatedCopiedScreenFields) {
                    if (originalField.getDbid() == fieldFromCopy.getPrsnlzdOriginal_DBID()) {
                        fieldFound = true;
                        break;
                    }
                }
                //if fields found copy updated values
                if (fieldFound) {
                    logger.trace("fields found copy updated values");
                    for (ScreenField copiedField : updatedCopiedScreenFields) {
                        if (!copiedField.getFieldExpression().equals(originalField.getFieldExpression())) {
                            continue;
                        }

                        if (!copiedField.isPrsnlzdRemoved()) {
                            FABSEntitySpecs entityFABSSpecs = originalField.getClass().getAnnotation(FABSEntitySpecs.class);
                            String[] prsnlzableFieldsNames = entityFABSSpecs.personalizableFields();
                            if (entityFABSSpecs != null && prsnlzableFieldsNames != null) {
                                for (String personalizableField : prsnlzableFieldsNames) {
                                    if (personalizableField.equals("controlSize")) {
                                        ((ScreenField) originalField).getDd().setControlSize(Integer.valueOf((copiedField.invokeGetter(personalizableField)).toString()));
                                        originalField.invokeSetter(personalizableField, copiedField.invokeGetter(personalizableField));
                                    } else {
                                        originalField.invokeSetter(personalizableField, copiedField.invokeGetter(personalizableField));
                                    }
                                }
                            }
                            returnedScreenFields.add(originalField);
                        }
                        break;
                    }
                } // field not found in copy -- then add original field to the list of fields that will be returned
                else {
                    returnedScreenFields.add(originalField);
                }
            }

            //loop to sort fields using sort index
            // <editor-fold defaultstate="collapsed" desc="Sort Fields">
            Collections.sort(returnedScreenFields, new Comparator() {
                public int compare(Object o1, Object o2) {
                    ScreenField field1 = (ScreenField) o1;
                    ScreenField field2 = (ScreenField) o2;
                    Integer index1 = field1.getSortIndex();
                    Integer index2 = field2.getSortIndex();
                    if (index1.compareTo(index2) != 0) {
                        return index1.compareTo(index2);
                    } else {
                        return 0;
                    }
                }
            });
            // </editor-fold>
            return returnedScreenFields;

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.debug("Returning with Null");
            return null;
        }

    }
    // </editor-fold>

    private void loadScreenFields(SingleEntityScreen oScreen, OUser loggedUser) {
        try {
            logger.debug("Entering");
            List<String> screenFieldsLoadingConditions = new ArrayList<String>();
            screenFieldsLoadingConditions.add("inActive = false");
            screenFieldsLoadingConditions.add("oScreen.dbid = " + oScreen.getDbid());
            // if screen is personalized
            if (oScreen.getPrsnlzdOriginal_DBID() != 0) {
                logger.trace("screen is personalized");
                OUser screenUser;
                if (loggedUser.getDbid() == 0) {
                    logger.trace("Logged user is DBID equals 0");
                    screenUser = loggedUser.getOnBehalfOf();
                } else {
                    logger.trace("Logged user is Not DBID equals 0");
                    screenUser = loggedUser;
                }
                screenFieldsLoadingConditions.add("prsnlzdUser_DBID = " + screenUser.getDbid());
            }

            List<ScreenField> screenFields = oem.loadEntityList(
                    "ScreenField",
                    screenFieldsLoadingConditions,
                    null,
                    Collections.singletonList("sortIndex"),
                    oem.getSystemUser(loggedUser));
            logger.trace("Screen Fields: ", screenFields);
            if (screenFields == null) {
                logger.trace("Screen Fields is Null");
                return;
            }
            if (screenFields.isEmpty()) {
                logger.trace("screen Fields is Empty");
                return;
            }
            logger.trace("Looping on Screen Fields");
            // Load DDs manually to guaranatee proper translation
            for (ScreenField screenField : screenFields) {
                DD dd = screenField.getDd();
                if (dd == null) //FIXME: log error
                {
                    logger.trace("DD is Null");
                    continue;
                }
                dd = ddService.getDD(dd.getName(), oem.getSystemUser(loggedUser));
                // MT: commented for New Object was found during synchronization ex
                if (null != dd) {
                    logger.trace("Setting DD: {} in Screen Field: {}", dd.getName(), screenField);
                }
                screenField.setDd(dd);
            }
            // MT: commented for New Object was found during synchronization ex
            oScreen.setScreenFields(screenFields);
            logger.debug("Returning");
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        logger.debug("Returning");
    }

    /**
     * BEGIN FUNCTION getBasicDetailScreen(OEntity entity, OUser loggedUser)
     * RETURN FormScreen RETURN(FormScreen) otmsManager.createQuery("SELECT o
     * FROM OUser WHERE o.actingOn.dbid='" + entity.getDBID() + " and
     * basicDetail = 1", loggedUser).getSingleResult(); END FUNCTION
     */
    @Override
    public FormScreen getBasicDetailScreen(OEntity entity, OUser loggedUser) {
        try {
            logger.debug("Entering");
            List<String> conditions = new ArrayList<String>();
            Collections.addAll(conditions, "oactOnEntity.dbid=" + entity.getDbid(), "basicDetail=1");
            FormScreen basicDetailScreen = (FormScreen) oem.loadEntity("FormScreen", conditions,
                    null, oem.getSystemUser(loggedUser));
            if (basicDetailScreen == null) {
                logger.error("Entity Has No Basic Detail Screen");
            }
            return basicDetailScreen;
        } catch (UserNotAuthorizedException ex) {
            logger.error("exception thrown: User Not Authorized", ex);
        } catch (Exception ex) {
            logger.error("exception thrown: ", ex);
        }
        logger.debug("Returning");
        return null;
    }

    @Override
    public FormScreen getBasicDetailScreenByOEntityDTO(OEntityDTO entity, OUser loggedUser) {
        try {
            logger.debug("Entering");
            List<String> conditions = new ArrayList<String>();
            Collections.addAll(conditions, "oactOnEntity.dbid=" + entity.getDbid(), "basicDetail=1");
            FormScreen basicDetailScreen = (FormScreen) oem.loadEntity("FormScreen", conditions,
                    null, oem.getSystemUser(loggedUser));
            if (basicDetailScreen == null) {
                logger.error("Entity Has No Basic Detail Screen");
            }
            return basicDetailScreen;
        } catch (UserNotAuthorizedException ex) {
            logger.error("Exception thown: User Not Authorized ", ex);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        logger.debug("Returning");
        return null;
    }

    public List<OMenu> getRoleMenus(long roleDBID, OUser loggedUser) {
        logger.debug("Entering");
        List<OMenu> menus = new ArrayList<OMenu>();
        List<RoleMenu> roleMenus = null;
        try {
            roleMenus = oem.loadEntityList(RoleMenu.class.getSimpleName(),
                    Collections.singletonList("oRole.dbid = " + roleDBID),
                    null, null, loggedUser);
            logger.trace("Adding Role Menus to Menu");
            for (RoleMenu roleMenu : roleMenus) {
                menus.add(roleMenu.getOmenu());
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.debug("Returning");
            return null;
        }
        logger.debug("Returning");
        return menus;
    }

    /**
     * Returns a list of the oEntity displayed actions It doesn't load the
     * actions from database, it assumes actions are already loaded in the
     * oEntity
     *
     * @param oEntity
     * @return Displayed actions list <br> Empty list: no displayed actions
     * found <br> null: Error
     */
    @Override
    public ArrayList<OEntityAction> getDisplayedActions(OEntity oEntity, OUser loggedUser) {
        logger.debug("Entering");
        if (oEntity == null) // Error
        {
            logger.debug("Entering");
            logger.debug("Returning with Null as as oentity equals Null");
            return null;
        }
        ArrayList<OEntityAction> displayedActions = new ArrayList<OEntityAction>();
        ArrayList<OEntityAction> allActions = new ArrayList<OEntityAction>();
        try {
            allActions.addAll(entitySetupService.getEntityActions(oEntity, loggedUser));
        } catch (UserNotAuthorizedException ex) {
            logger.error("Exception thrown: User Not Authorized ", ex);
        }
        try {
            if (allActions == null || allActions.getClass().getName().contains("IndirectList")) // no actions in the oEntity
            {
                logger.trace("No Actions in this Entity");
                return allActions;
            }
            logger.trace("Adding Actions in Displayed Action if Action is Displayed");
            for (OEntityAction action : allActions) {
                if (action.isDisplayed() && !action.isInActive()) {
                    displayedActions.add(action);
                }
            }
            ArrayList<OEntityAction> availableActions = new ArrayList<OEntityAction>();
            for (Iterator<OEntityAction> it = allActions.iterator(); it.hasNext();) {
                OEntityAction oea = it.next();
                try {
                    securityService.checkActionOnlyAuthority(
                            oea, loggedUser, oem.getSystemUser(loggedUser));
                    availableActions.add(oea);
                } catch (Exception ex) {
                    logger.error("Exception thrown: ", ex);
                }
            }
            displayedActions = availableActions;
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.debug("Returning with Null");
            return null;
        }
        logger.debug("Returning");
        return displayedActions;
    }

    public ArrayList<OEntityActionDTO> getDisplayedActions(OEntityDTO oEntity, OUser loggedUser) {
        logger.debug("Entering");
        if (oEntity == null) // Error
        {
            logger.error("OEntity equals Null");
            logger.debug("Returning with Null as oentity equals Null");
            return null;
        }
        ArrayList<OEntityActionDTO> allActions = new ArrayList<OEntityActionDTO>();
        ArrayList<OEntityActionDTO> availableActions = new ArrayList<OEntityActionDTO>();
        allActions.addAll(entitySetupService.getEntityActionsDTO(oEntity, loggedUser));
        try {
            if (allActions == null || allActions.getClass().
                    getName().contains("IndirectList")) // no actions in the oEntity
            {
                logger.trace("No Action in the OEntity");
                return allActions;
            }
            logger.trace("Looping on OEntity Action DTO Data Transformation");
            for (Iterator<OEntityActionDTO> it = allActions.iterator(); it.hasNext();) {
                OEntityActionDTO oea = it.next();
                try {
                    securityService.checkActionOnlyAuthority(
                            oea, loggedUser, oem.getSystemUser(loggedUser));
                    availableActions.add(oea);
                } catch (Exception ex) {
                    logger.error("exception thrown: ", ex);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.debug("Returning with Null");
            return null;
        }
        logger.debug("Returning");
        return availableActions;
    }

    /**
     * Returns the index of the field in 'expressionFieldInfos' whose OEntity is
     * what the balloon should work on <br> Examples: <br> for
     * "Employee"."unit.company.name": return is 1 (Ballon of Company) <br> for
     * "Company"."currency.value": return is 0 (Balloon of DD) <br> for
     * "TabularScreen"."actOnEntity.entityClassPath": return is 0 (Balloon of
     * OEntity) <br> for "Employee"."personalInfo.firstName": return is 0
     * (OneToOne Child, Balloon of Employee)
     *
     * @param ownerEntityClass Class of the field (paresed in
     * expressionFieldInfos) owner Entity
     * @param expressionFieldInfos Parsed expressionFieldInfo of the field
     * @return <br> -2: Error <br> -1: Balloon should be displayed for the
     * OEntity of the Owner Entity <br> otherwise: a valid field index in
     * 'expressionFieldInfos' for which OEntity the balloon should be displayed
     */
    public int getBalloonFieldIndex(
            Class ownerEntityClass, List<ExpressionFieldInfo> expressionFieldInfos) {
        logger.debug("Entering");
        if (expressionFieldInfos == null) {
            logger.error("Null Arguments");
            return -2;
        }
        if (expressionFieldInfos.size() == 1) {
            // Is a direct ownerEntity field, no field in the expression to get the OEntity for
            logger.trace("OEntity is of the ownerEntity");
            return -1; // OEntity is of the ownerEntity
        }

        for ( // Start from the second-last filed in the expressionFieldInfos
                int expressionFieldInfoIndex = expressionFieldInfos.size() - 2;
                // Within the expressionFieldInfos boundary
                expressionFieldInfoIndex >= 0;
                // Backword
                expressionFieldInfoIndex--) {
            ExpressionFieldInfo expFieldInfo = expressionFieldInfos.get(expressionFieldInfoIndex);
            if (expressionFieldInfoIndex == 0) {
                // First field in the expression
                // Check if it's OneToOne Child of the owner entity
                logger.trace("Check if it's OneToOne Child of the owner entity");
                if (BaseEntity.isFieldOneToOneChild(ownerEntityClass, expFieldInfo.fieldName)) {
                    // Is OneToOne Child of the owner entity
                    logger.trace("True");
                    logger.debug("Returning with Output -1");
                    return -1;  // OEntity is of the ownerEntity
                } else {
                    logger.debug("Returning with Output 0");
                    logger.trace("False");
                    return 0;   // OEntity of the first field
                }
            }
            Class fieldParentParentCls
                    = BaseEntity.getFieldObjectType(expressionFieldInfos.get(expressionFieldInfoIndex - 1).field);
            // Check if it's OneToOne Child of the parent entity
            logger.trace("Check if it's OneToOne Child of the parent entity");
            if (!BaseEntity.isFieldOneToOneChild(fieldParentParentCls, expFieldInfo.fieldName)) {
                // Is not OneToOne Child of the parent entity
                return expressionFieldInfoIndex;   // OEntity of the current field
            }
            // Is OneToOne child, we should display its parent balloon
            // continue; Check grandparent, may be also OneToOne
        }
        logger.debug("Returning with Output -2");
        return -2; // Error
    }

    public Class getFieldClass(String className, String fieldName, OUser loggedUser) {
        try {
            logger.debug("Entering");
            Field field = BaseEntity.getClassField(Class.forName(className), fieldName);
            logger.debug("Returning");
            return field.getType();
        } catch (ClassNotFoundException ex) {
            logger.error("Exception thrown: ", ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    /**
     * Returns the OEntity which balloon should work on. <br>Refer to
     * {@link #getBalloonFieldIndex(Class, List)} for more information
     *
     * @param ownerEntity OEntity of the field (paresed in expressionFieldInfos)
     * owner Entity
     * @param expressionFieldInfos Parsed expressionFieldInfo of the field
     * @return <br> ownerEntity: Balloon should be displayed for the OEntity of
     * the Owner Entity <br> OEntity of one of the fields in
     * expressionFieldInfos <br> null: Error
     */
    @Override
    public OEntity getFieldBallonOEntity(
            OEntity ownerEntity, List<ExpressionFieldInfo> expressionFieldInfos, OUser loggedUser) {
        logger.debug("Entering");
        if (expressionFieldInfos == null) {
            logger.error("Null Argument");
            logger.debug("Returning with Null");
            return null;
        }
        Class ownerEntityClass = null;
        try {
            ownerEntityClass = Class.forName(ownerEntity.getEntityClassPath());
        } catch (Exception ex) {
            logger.debug("Returning with Null");
            return null;
        }

        try {
            int balloonFieldIndex = getBalloonFieldIndex(ownerEntityClass, expressionFieldInfos);
            switch (balloonFieldIndex) {
                case -2:
                    // Error
                    logger.trace("Ballon Field Index equals -2");
                    logger.debug("Returning with Null");
                    return null;
                case -1:
                    logger.debug("Returning");
                    return ownerEntity;
                default:
                    logger.debug("Returning");
                    return entitySetupService.loadOEntity(
                            expressionFieldInfos.get(balloonFieldIndex).field.getType().getName(),
                            oem.getSystemUser(loggedUser));
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    @Override
    public OEntityDTO getFieldBallonOEntityDTO(
            OEntityDTO ownerEntity, List<ExpressionFieldInfo> expressionFieldInfos, OUser loggedUser) {
        logger.debug("Entering");
        if (expressionFieldInfos == null) {
            logger.error("Null Argument");
            logger.debug("Returning with Null");
            return null;
        }
        Class ownerEntityClass;// = null;
        try {
            ownerEntityClass = Class.forName(ownerEntity.getEntityClassPath());
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.debug("Returning with Null");
            return null;
        }

        try {
            int balloonFieldIndex = getBalloonFieldIndex(ownerEntityClass, expressionFieldInfos);
            switch (balloonFieldIndex) {
                case -2:
                    // Error
                    logger.debug("Ballon Field Index equals -2");
                    logger.debug("Returning with Null");
                    return null;
                case -1:
                    logger.debug("Returning");
                    return ownerEntity;
                default:
                    logger.debug("Returning");
                    return entitySetupService.loadOEntityDTO(
                            expressionFieldInfos.get(balloonFieldIndex).field.getType().getName(),
                            oem.getSystemUser(loggedUser));
            }
        } catch (Exception ex) {
            logger.debug("Exception thrown: ", ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    public OFunctionResult validateUDC(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        if (odm.getData().size() < 3) {
            UserMessage errorMsg = usrMsgService.getUserMessage("CorruptedData", loggedUser);
            OFunctionResult functionResult = new OFunctionResult();
            if (errorMsg != null) {
                functionResult.addError(errorMsg);
            }
            logger.debug("Returning");
            return functionResult;
        }
        OFunctionResult oFR = new OFunctionResult();
        UDCBase uDCBase = (UDCBase) odm.getData().get(0);

        if (uDCBase.isReserved()) {
            oFR.addError(usrMsgService.getUserMessage(
                    "udcreservedError", loggedUser));
        }
        logger.debug("Returning");
        return oFR;
    }

    public OFunctionResult validateOPortalPage(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        if (odm.getData().size() < 2) {
            UserMessage errorMsg = usrMsgService.getUserMessage("CorruptedData", loggedUser);
            if (errorMsg != null) {
                oFR.addError(errorMsg);
            }
            logger.debug("Returning");
            return oFR;
        }
        //OPortalPage oPortalPage = (OPortalPage) odm.getData().get(0);
        List<PortalPagePortlet> portalPagePortlets = (List<PortalPagePortlet>) odm.getData().get(1);
        // <editor-fold defaultstate="collapsed" desc="Validate Portlet Position">
        try {
            Collections.sort(portalPagePortlets, new Comparator() {
                public int compare(Object o1, Object o2) {
                    PortalPagePortlet pagePortlet1 = (PortalPagePortlet) o1;
                    PortalPagePortlet pagePortlet2 = (PortalPagePortlet) o2;
                    String col1 = pagePortlet1.getColumn();
                    String col2 = pagePortlet2.getColumn();
                    String pos1 = String.valueOf(pagePortlet1.getPosition());
                    String pos2 = String.valueOf(pagePortlet2.getPosition());

                    if (col1.compareTo(col2) != 0) {
                        logger.debug("Returning");
                        return col1.compareTo(col2);
                    } else {
                        //
                        if (pos1.compareTo(pos2) == 0) {
                            throw new IllegalArgumentException("There shouldn't be two Portlet in the same column position");
                        }
                        logger.debug("Returning");
                        return pos1.compareTo(pos2);
                    }
                }
            });
        } catch (IllegalArgumentException illegalArgumentException) {
            UserMessage errorMsg = usrMsgService.getUserMessage("portletsinsameposition", loggedUser);
            if (errorMsg != null) {
                oFR.addError(errorMsg);
            }
            logger.error("User Message Service: {}", usrMsgService.getUserMessage("SystemInternalError", loggedUser));
            oFR.addError(usrMsgService.getUserMessage(
                    "SystemInternalError", loggedUser), illegalArgumentException);
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Validate Main Portlet">
        OPortalPage page = portalPagePortlets.get(0).getPortalPage();
        int foundMain = 0;
        logger.debug("Looping on Children Portlets and Checking is main");
        for (PortalPagePortlet childPortlet : portalPagePortlets) {
            if (childPortlet.isInActive()) {
                continue;
            }
            if (childPortlet.isMain()) {
                ++foundMain;
            }

            PortalPagePortlet parentPortlet = childPortlet.getParentPortlet();
            if (parentPortlet != null && parentPortlet.getDbid() != 0) {
                // Validate Main portlet doesn't have Parent Portlet
                logger.trace("Validating Main portlet doesn't have Parent Portlet");
                if (childPortlet.isMain()) {
                    oFR.addError(usrMsgService.getUserMessage(
                            "MainPortletWithParentPortlet", loggedUser));
                }
                // Validate parent portlets in the same page
                logger.trace("Validating parent portlets in the same page");
                OPortalPage parentPortalPage = parentPortlet.getPortalPage();
                if (parentPortalPage != null && parentPortalPage.getDbid() != page.getDbid()) {
                    oFR.addError(usrMsgService.getUserMessage("ParentPortletBelongsToAnotherPage",
                            usrMsgService.constructListFromStrings(childPortlet.getScreen().getName(),
                                    page.getName()), loggedUser));
                }
            }
        }

        if (foundMain != 1) {
            oFR.addError(usrMsgService.getUserMessage("PageMustHaveOneMainPortlet",
                    Collections.singletonList(page.getName()), loggedUser));
            logger.debug("Returning");
            return oFR;
        }
        logger.debug("Returning");
        return oFR;
    }

    public OFunctionResult createRequiredScreensOnly(OEntity oEntity, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            oFR.append(createDefaultTabularScreen(oEntity, loggedUser));
            // If there are errors, continue to create the other objects any way
            logger.debug(" If there are errors, continue to create the other objects any way");

            logger.debug("Check If a FormScreen Already Exists:");
            oFR.append(createFormScreen(oEntity, loggedUser));

            // Check If At Least One OObjectBriefInfoField Already Exists:
            logger.trace("Check If At Least One OObjectBriefInfoField Already Exists");
            List<OObjectBriefInfoField> foundBIFieldList = (List<OObjectBriefInfoField>) oem.loadEntity(
                    "OObjectBriefInfoField",
                    Collections.singletonList("oObject.dbid=" + Long.toString(oEntity.getDbid())),
                    null, oem.getSystemUser(loggedUser));
            logger.trace("Found BI Field List: {}", foundBIFieldList);
            if (foundBIFieldList == null) {
                logger.debug("Found BI Field List equals Null");
                entitySetupService.createDefaultBriefInfoFields(oEntity, loggedUser);
                // FIXME: need to provide OFunctionResult success messages by createDefaultBriefInfoFields
            } else {
                logger.debug("Object Brief Info Fields Are Found, Won't Create It");
            }

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    /**
     *
     * @param oEntity
     * @param loggedUser
     * @return
     */
    private OFunctionResult createFormScreen(OEntity oEntity, OUser loggedUser) {
        logger.debug("Entering");
        // Check if a form screen already exists:
        OFunctionResult oFR = new OFunctionResult();
        try {
            oem.getEM(loggedUser);

            FormScreen formScreen = (FormScreen) oem.loadEntity("FormScreen",
                    Collections.singletonList("oactOnEntity.dbid=" + oEntity.getDbid()),
                    null, oem.getSystemUser(loggedUser));
            if (formScreen != null) {
                // Screen is already created
                logger.trace("Form Screen is already Created");
                oFR.addReturnValue(formScreen);
                logger.debug("Returning");
                return oFR;
            }
            formScreen = new FormScreen();
            String entityClassPath = oEntity.getEntityClassPath();
            BaseEntity theEntity = (BaseEntity) Class.forName(entityClassPath).newInstance();

            // Create Screen
            // Add all Screen Fields to the Form Screen:
            formScreen.setName(theEntity.getClassName());
            formScreen.setOactOnEntity(oEntity);
            formScreen.setColumnsNo(2);
            formScreen.setBasicDetail(false);
            formScreen.setHeader(theEntity.getClassName());
            formScreen.setHeaderTranslated(theEntity.getClassName());
            formScreen.setOmodule(oEntity.getModule());
            formScreen.getViewPage();

            // Save the Screen (ScreenFields will be saved because of cascade ALL in OScreen):
            oFR.append(entitySetupService.callEntityCreateAction(formScreen, loggedUser));
            if (oFR.getErrors().size() > 0) {
                return oFR;
            }
            logger.debug("Creating Form Screen");
            // End Create Screen
            List<Field> entityFieldsList = theEntity.getAllFields();
            List<ScreenField> screenFieldsList = new ArrayList();
            logger.trace("Looping on Fields for its Type and DD");
            int sortIndex = 1;
            if (entityFieldsList != null) {
                for (Field field : entityFieldsList) {
                    String fieldClassName = field.getType().getName();
                    if (fieldClassName.contains("List")) {
                        logger.trace("Field : {}  Type is a List", fieldClassName);
                        continue;
                    }
                    if (fieldClassName.contains("unitedofoq")) {
                        logger.trace("Field : {}  Type is a unitedofoq", fieldClassName);
                        continue;
                    }
                    String fieldDDName = BaseEntity.getFieldDDName(theEntity, field.getName());
                    logger.trace("Field DD Name is: {}", fieldDDName);
                    if (fieldDDName != null && !"".equals(fieldDDName)) {
                        ScreenField screenField = new ScreenField();
                        screenField.setFieldExpression(field.getName());
                        screenField.setOScreen(formScreen);
                        screenFieldsList.add(screenField);
                        DD fieldDD = ddService.getDD(fieldDDName, oem.getSystemUser(loggedUser));
                        if (fieldDD == null) {
                            // Field DD not created yet
                            // Create Field DD
                            logger.trace("This DD is not Created, equals Null, i will Create it");
                            oFR.append(ddService.createFieldDefaultDD(fieldDDName, field, oEntity, loggedUser));
                            if (oFR.getErrors().size() > 0) {
                                logger.trace("There are Errors: {}", oFR.getErrors());
                                logger.debug("Returning");
                                return oFR;
                            }
                            fieldDD = (DD) oFR.getReturnValues().get(0);   //FIXME: should be returned oDM
                            logger.trace("DD is Created with name: {}", fieldDD.getName());
                        }
                        screenField.setDd(fieldDD);
                        screenField.setFieldExpression(field.getName());
                        screenField.setEditable(true);
                        screenField.setOScreen(formScreen);
                        screenField.setSortIndex(sortIndex);

                        oFR.append(entitySetupService.callEntityCreateAction(screenField, loggedUser));
                        if (oFR.getErrors().size() > 0) {
                            logger.trace("There are Errors: {}", oFR.getErrors());
                            return oFR;
                        }
                        logger.trace("Creating Screen Field with Name: {}", screenField.getFieldExpression());
                        screenFieldsList.add(screenField);
                        sortIndex++;
                    } else {
                        //log ddname already exists
                        logger.trace("DD Name Already Exists, No need to ReCreate it");

                    }
                }
            }
            formScreen.setScreenFields(screenFieldsList);
            formScreen.setBasicDetail(true);

            // Create ScreenInput of 'VoidDT'
            ODataType dataType = dataTypeService.loadODataType("VoidDT", loggedUser);
            ScreenInput screenInput = new ScreenInput();
            screenInput.setOdataType(dataType);
            screenInput.setOscreen(formScreen);

            oFR.append(entitySetupService.
                    callEntityCreateAction(screenInput, loggedUser));

            //Not Needed, yeah? oFR.append(entitySetupService.callEntityUpdateAction(formScreen, loggedUser)) ;
            // Create ScreenFunction to open this screen
            logger.trace("Creating ScreenFunction to open this screen");
            ScreenFunction screenFunction = new ScreenFunction();
            Date currentDate = timeZoneService.getUserCDT(loggedUser);
            screenFunction.setCode(Long.toString(currentDate.getTime()));
            screenFunction.setName("Edit " + oEntity.getEntityClassName());
//            screenFunction.setNameTranslated("Edit " + oEntity.getEntityClassName());
            UDC editScreenMode = (UDC) oem.loadEntity("UDC",
                    Collections.singletonList("value='Edit'"), //FIXME: use code instead as value is not unique
                    null, oem.getSystemUser(loggedUser));
            screenFunction.setScreenMode(editScreenMode);
            ODataType oDataType = dataTypeService.loadODataType("DBID", loggedUser);
            screenFunction.setOscreen(formScreen);
            screenFunction.setOdataType(oDataType);

            oFR.append(entitySetupService.callEntityCreateAction(screenFunction, loggedUser));

            // Create "Edit" action in the owner OEntity opens this screen
            logger.trace("Create Edit action in the owner OEntity opens this screen");
            OEntityAction entityAction = new OEntityAction();
            entityAction.setName("Edit");
//            entityAction.setNameTranslated("Edit");
            entityAction.setOfunction(screenFunction);
            entityAction.setOownerEntity(oEntity);
            entityAction.setDisplayed(true);
            entityAction.setSortIndex(1);

            oFR.append(entitySetupService.callEntityCreateAction(entityAction, loggedUser));

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning");
            oem.closeEM(loggedUser);
            return oFR;
        }
    }

    /**
     * Creates OEntity Default Tabular Screen as per the specifications in
     * {@link #createDefaultTabularScreen(OEntity, OUser) }
     *
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return "TabularScreenAddedSccessfully" User message if successfully
     * created, otheriwse, error messages
     */
    @Override
    public OFunctionResult createDefaultTabularScreen(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, TabularScreen.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        try {
            OEntity oEntity = (OEntity) odm.getData().get(0);
            oFR.append(createDefaultTabularScreen(oEntity, loggedUser));
            if (oFR.getErrors().isEmpty()) {
                oFR.addSuccess(usrMsgService.getUserMessage("TabularScreenAddedSccessfully", loggedUser));
            }

        } catch (Exception ex) {
            logger.error("Exception thrown");
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    /**
     * If newScreen is true, then a new Tabular Screen is created for the
     * oEntity as following: <br>1. Is Basic Screen <br>2. Name is "Manage " +
     * Entity Java Class Name <br>3. Editable, Removable, Fast Track, and no
     * Side Balloon <br> <br>Screen Fields & Screen Function are created using
     * {@link #createTabularScreenDefaultStuff(TabularScreen, OUser) } function
     * <br>If oEntity has a parent, then, a displayed action is added to parent
     * entity that opens this tabular screen supporting ParentDBID DataType,
     * along with a function to open it from the action
     *
     * @param oEntity
     * @param foundTabularScreen
     * @param newScreen
     * @param loggedUser
     * @return oFR with returned value = created Screen in case of success
     */
    private OFunctionResult createDefaultTabularScreen(OEntity oEntity, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            oem.getEM(loggedUser);

            // <editor-fold defaultstate="collapsed" desc="Validate that the screen is not already created">
            TabularScreen tabularScreen = (TabularScreen) oem.loadEntity(
                    "TabularScreen",
                    Collections.singletonList("name='Manage" + oEntity.getClassName() + "s'"),
                    null,
                    loggedUser);
            // </editor-fold>

            if (tabularScreen != null) {
                // Screen is already created
                logger.trace("Tabular Screen is already Created");
                oFR.addReturnValue(tabularScreen);
                logger.debug("Returning");
                return oFR;
            }

            tabularScreen = new TabularScreen();
            String entityClassPath = oEntity.getEntityClassPath();
            BaseEntity entityInst = (BaseEntity) Class.forName(entityClassPath).newInstance();

            // <editor-fold defaultstate="collapsed" desc="Create the screen">
            /* Must save the tabularScreen first before saving the Screen Fields,
             Otherwise we get:
             java.lang.IllegalStateException: During synchronization a new object
             was found through a relationship that was not marked cascade PERSIST
             */
            logger.trace("Creating Tabular Screen");
            tabularScreen.setName("Manage" + entityInst.getClassName() + "s");
            tabularScreen.setFastTrack(true);
            tabularScreen.setEditable(true);
            tabularScreen.setRemovable(true);
            tabularScreen.setSideBalloon(false);
            tabularScreen.setBasic(true);
            tabularScreen.setOactOnEntity(oEntity);
            tabularScreen.setHeader("Manage " + entityInst.getClassName());
            tabularScreen.setHeaderTranslated("Manage " + entityInst.getClassName());
            tabularScreen.setOmodule(oEntity.getModule());

            // Save the Screen:
            oFR.append(entitySetupService.callEntityCreateAction(tabularScreen, loggedUser));
            if (oFR.getErrors().size() > 0) {
                return oFR;
            }
            oFR.addReturnValue(tabularScreen);

            logger.trace("Tabular Screen has been Created");

            // Create VoidDT screen input if not created... Is not supported any more
            // It's natively supported for tabular screen of entity has no parent
            // Otherewise, user should support it manually, as it's considered
            // special case
            oFR.append(createTabularScreenDefaultStuff(tabularScreen, loggedUser));
            if (oFR.getErrors().size() > 0)
                /*Do Nothing, Continue*/;

            List<OEntity> parentOEntity = entitySetupService.loadParentOEntity(oEntity, oem.getSystemUser(loggedUser));
            OFunctionResult actionFunctionResult = null;
            Date currentDate = timeZoneService.getUserCDT(loggedUser);
            if (parentOEntity != null
                    && !parentOEntity.isEmpty()
                    && actionFunctionResult != null
                    && actionFunctionResult.getErrors().isEmpty()) {
                // <editor-fold defaultstate="collapsed" desc="CHILD ACTION IN PARENT">
                ScreenFunction parentScreenFunction = new ScreenFunction();
                parentScreenFunction.setCode(Long.toString(currentDate.getTime()) + "0");
                parentScreenFunction.setName("Manage " + entityInst.getClassName() + "s");
                ODataType parentDataType = dataTypeService.loadODataType("ParentDBID", loggedUser);
                parentScreenFunction.setOscreen(tabularScreen);
                parentScreenFunction.setOdataType(parentDataType);
                UDC managedScreenMode = (UDC) oem.loadEntity("UDC",
                        Collections.singletonList("value='Managed'"),
                        null, oem.getSystemUser(loggedUser));
                parentScreenFunction.setScreenMode(managedScreenMode);

                // Save the Screen Function:
                logger.trace("Saving Screen Function");
                OFunctionResult parentScrFunctionResult;
                parentScrFunctionResult = entitySetupService.
                        callEntityCreateAction(parentScreenFunction, loggedUser);
                oFR.append(parentScrFunctionResult);
                if (oFR.getErrors().size() > 0) /*Do Nothing, Continue*/;
                //parent screenfunction created
                logger.trace("Parent Function Created, Parent Entity: {}, "
                        + "Parent Function: {}, Parent DataType: {}", parentOEntity,
                        parentScreenFunction, parentDataType);
                if (parentScrFunctionResult.getErrors().isEmpty()) {
                    logger.trace("There is No Errors");
                    OEntityAction parentEntityAction = new OEntityAction();
                    parentEntityAction.setName(tabularScreen.getName());
//                    parentEntityAction.setNameTranslated(tabularScreen.getName());
                    parentEntityAction.setOfunction(parentScreenFunction);
                    parentEntityAction.setOownerEntity(parentOEntity.get(0)); // FIXME: support two parents
                    parentEntityAction.setDisplayed(true);
                    parentEntityAction.setSortIndex(1);

                    // Save the Action:
                    logger.trace("Saving the Action");
                    oFR.append(entitySetupService.
                            callEntityCreateAction(parentEntityAction, loggedUser));
                    if (oFR.getErrors().size() > 0) /*Do Nothing, Continue*/;
                    //parent entity action created
                    logger.trace("Parent Action Created");
                }
                // </editor-fold>
            }
            logger.trace("Entity has no parent Entity");
            return oFR; //Don't add success message as it's the responsibility
            // of the caller (normally, JavaFunction)

        } catch (Exception ex) {
            logger.error("Exception thrown");
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning");
            oem.closeEM(loggedUser);
            return oFR;
        }
    }

    @Override
    public OFunctionResult createDefaultTabularScreenFunction(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, TabularScreen.class);
        if (inputValidationErrors != null) {
            logger.trace("Returning with: {}", inputValidationErrors);
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        TabularScreen tabularScreen = null;
        OEntityDTO actOnEntity = null;
        try {
            oem.getEM(loggedUser);
            OUser systemUser = oem.getSystemUser(loggedUser);
            tabularScreen = (TabularScreen) (OScreen) odm.getData().get(0);
            long screenDBID = tabularScreen.getDbid();
            actOnEntity = entitySetupService.getOScreenEntityDTO(screenDBID, loggedUser);
            String entityClassPath = actOnEntity.getEntityClassPath();
            BaseEntity entityInst = (BaseEntity) Class.forName(entityClassPath).newInstance();

            List<ScreenFunction> screenFunctions = (List<ScreenFunction>) oem.loadEntityList("ScreenFunction",
                    Collections.singletonList("oscreen.dbid="
                            + tabularScreen.getDbid()), null, null, systemUser);

            if (!screenFunctions.isEmpty()) {
                logger.trace("ScreenFunction already exist");
                logger.trace("Returning with: {}", oFR);
                return oFR;
            }

            List<OEntity> parentOEntity = entitySetupService.loadParentOEntity(
                    actOnEntity, systemUser);

            ScreenFunction screenFunction = new ScreenFunction();

            Date currentDate = timeZoneService.getUserCDT(loggedUser);
            screenFunction.setCode(Long.toString(currentDate.getTime()));
            screenFunction.setName("Manage " + entityInst.getClassName() + "s");
//            screenFunction.setNameTranslated("Manage " + entityInst.getClassName() + "s");
            UDC editScreenMode = (UDC) oem.loadEntity("UDC",
                    Collections.singletonList("value='Managed'"),
                    null, systemUser);
            screenFunction.setScreenMode(editScreenMode);
            ODataType oDataType = null;
            if (parentOEntity != null && !parentOEntity.isEmpty()) {
                oDataType = dataTypeService.loadODataType(// FIXME: support two parents
                        parentOEntity.get(0).getClassName(), systemUser);
            } else {
                oDataType = dataTypeService.loadODataType("VoidDT", systemUser);
                screenFunction.setOdataType(oDataType);
            }
            screenFunction.setOscreen(tabularScreen);
            if (oDataType != null) {
                screenFunction.setOdataType(oDataType);
            } else {
                oFR.addError(usrMsgService.getUserMessage("ParentNotFound", systemUser));
                screenFunction.setOscreen(tabularScreen);
                oFR.append(entitySetupService.callEntityCreateAction(screenFunction, systemUser));
                logger.trace("Returning with: {}", oFR);
                return oFR;
            }

            // Save the Screen Function:
            logger.trace("Saving the Screen");
            oFR.append(entitySetupService.callEntityCreateAction(screenFunction, systemUser));
            logger.trace("Error Creating Screen Function");
            logger.trace("Returning with: {}", oFR);
            return oFR; //FIXME: add success message if no errors/success/warning messages found in it

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            oem.closeEM(loggedUser);
            return oFR;
        }
    }

    @Override
    public OFunctionResult createDefaultFormScreenFunction(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, FormScreen.class);
        if (inputValidationErrors != null) {
            logger.trace("Returning with: {}", inputValidationErrors);
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        FormScreen formScreen = null;
        OEntityDTO actOnEntity = null;
        try {
            oem.getEM(loggedUser);
            formScreen = (FormScreen) (OScreen) odm.getData().get(0);
            if (formScreen.getScreenFunctions() != null && !formScreen.getScreenFunctions().isEmpty()) {
                logger.trace("Returning with: {}", oFR);
                return oFR;
            }
            long screenDBID = formScreen.getDbid();
            actOnEntity = entitySetupService.getOScreenEntityDTO(screenDBID, loggedUser);

            List<ScreenFunction> screenFunctions = (List<ScreenFunction>) oem.loadEntityList("ScreenFunction",
                    Collections.singletonList("oscreen.dbid="
                            + formScreen.getDbid()), null, null, oem.getSystemUser(loggedUser));

            List<OEntity> parentOEntity = entitySetupService.loadParentOEntity(
                    actOnEntity, oem.getSystemUser(loggedUser));

            if (screenFunctions.isEmpty()) {
                logger.trace("Screen Functions is Empty");
                if (parentOEntity == null) {
                    logger.trace("Parent OEntity equals Null");
                    ScreenFunction screenFunction = new ScreenFunction();
                    Date currentDate = timeZoneService.getUserCDT(loggedUser);
                    screenFunction.setCode(Long.toString(currentDate.getTime()));
                    screenFunction.setName("Edit " + actOnEntity.getEntityClassName());
//                    screenFunction.setNameTranslated("Edit " + actOnEntity.getEntityClassName());
                    UDC editScreenMode = (UDC) oem.loadEntity("UDC",
                            Collections.singletonList("value='Edit'"), //FIXME: use code instead as value is not unique
                            null, oem.getSystemUser(loggedUser));
                    screenFunction.setScreenMode(editScreenMode);
                    ODataType oDataType = dataTypeService.loadODataType(actOnEntity.getEntityClassName(), loggedUser);
                    screenFunction.setOscreen(formScreen);
                    screenFunction.setOdataType(oDataType);

                    oFR.append(entitySetupService.callEntityCreateAction(screenFunction, loggedUser));
                }
            } else {
                logger.trace("Screen Function is already exist");
            }
            logger.trace("Returning with: {}", oFR);
            return oFR; //FIXME: add success message if no errors/success/warning messages found in it
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            oem.closeEM(loggedUser);
            return oFR;
        }
    }

    public OFunctionResult regenerateDDs(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
            OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                    odm, loggedUser, OEntity.class);
            if (inputValidationErrors != null) {
                logger.trace("Returning with: {}", inputValidationErrors);
                return inputValidationErrors;
            }
            // </editor-fold>

            OEntity oEntity = (OEntity) odm.getData().get(0);
            BaseEntity theEntity = (BaseEntity) Class.forName(oEntity.getEntityClassPath()).newInstance();
            List<Field> declaredFields = theEntity.getAllFields(); // Don't use theEntity.getClass().getDeclaredFields();
            DD fieldDD = null;
            logger.trace("Looping on Declared Filds");
            for (Field field : declaredFields) {
                String fieldDDName = BaseEntity.getFieldDDName(theEntity, field.getName());
                if (fieldDDName != null) {
                    if (!fieldDDName.equals("")) {
                        fieldDD = ddService.getDD(fieldDDName, loggedUser);
                        if (fieldDD == null) {
                            // Field DD not created yet
                            // Create Field DD
                            logger.trace("this DD: {} is not Found, I Will Create it", fieldDDName);
                            oFR.append(ddService.createFieldDefaultDD(fieldDDName, field, oEntity, loggedUser));
                            if (oFR.getErrors().size() > 0) {
                                logger.error("Errors in Creating this DD: {}", fieldDDName);
                            } else {
                                logger.trace("this DD: {} is Created", fieldDDName);
                                oFR.addSuccess(usrMsgService.getUserMessage("DataSaved", loggedUser));
                            }
                            fieldDD = (DD) oFR.getReturnValues().get(0);
                        }
                    }
                }
            }

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    public List<BaseEntity> getDBUnionDisplayRecords(BaseEntity parentObject, List<BaseEntity> displayedEntities, OUser loggedUser) {
        logger.debug("Entering");

        if (displayedEntities.size() == 0) {
            logger.debug("displayed Entity size is 0");
            logger.trace("Returning with: {}", displayedEntities);
            return displayedEntities;
        }

        List parentFieldNames = BaseEntity.getParentEntityFieldName(displayedEntities.get(0).getClass().getName());
        String entityClassName = displayedEntities.get(0).getClass().getSimpleName();
        logger.trace("Parent Field Names is: {}", parentFieldNames);
        if (parentFieldNames != null && parentFieldNames.size() != 0) {
            try {
                String parentFieldName = parentFieldNames.get(0).toString();

                if (parentObject == null) {
                    parentObject = (BaseEntity) BaseEntity.getValueFromEntity(displayedEntities.get(0), parentFieldName);
                }

                if (parentObject == null) {
                    logger.error("Cannot make aprent presave validation; ParentObject Is Null");
                    return displayedEntities;
                }

                long parentDBID = parentObject.getDbid();

                List<String> conditions = new ArrayList<String>();
                conditions.add(parentFieldName + ".dbid = " + parentDBID);

                String shownIds = "";
                logger.trace("Looping on in Displayed Entities to get shown IDs");
                for (int i = 0; i < displayedEntities.size(); i++) {
                    if (displayedEntities.get(i).getDbid() != 0) {
                        if (!shownIds.equals("")) {
                            shownIds += ",";
                        }
                        shownIds += displayedEntities.get(i).getDbid() + " ";
                    }
                }
                logger.trace("shown IDs is: {}", shownIds);

                if (!shownIds.equals("")) {
                    logger.trace("shown IDs is: {}", shownIds);
                    shownIds = "( " + shownIds + ")";
                    conditions.add("dbid Not In " + shownIds);
                }
                logger.trace("shown IDs is: {}", shownIds);

                List<BaseEntity> nonDiplayedEntities = oem.loadEntityList(
                        entityClassName, conditions, null, null, loggedUser);

                displayedEntities.addAll(nonDiplayedEntities);
            } catch (Exception ex) {
                logger.error("Exception thrown: ", ex);
            }
        }
        logger.trace("Returning with: {}", displayedEntities);
        logger.debug("Returning");
        return displayedEntities;
    }

    /**
     * Utility, helps any ScreenDataLoading User Exit function to build the
     * returned oFR
     *
     * @param initialConditions
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult constructScreenDataLoadingUEConditionsOFR(
            List<String> initialConditions, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            oem.getEM(loggedUser);
            ODataType screenDataLoadingUEDTRet = dataTypeService.loadODataType("ScreenDataLoadingUERet", loggedUser);
            if (screenDataLoadingUEDTRet == null) {
                logger.trace("ScreenDataLoadingUE DataType Not Found");
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
                logger.trace("Returning with: {}", oFR);
                return oFR;
            }
            ODataMessage returnedODM = new ODataMessage();
            returnedODM.setODataType(screenDataLoadingUEDTRet);
            ArrayList<Object> odmData = new ArrayList<Object>(1);
            if (initialConditions == null) {
                initialConditions = new ArrayList<String>();
            }
            List<String> odmDataConditions = new ArrayList<String>(initialConditions);
            // Don't use original list, other code may change it at any time
            odmData.add(odmDataConditions);
            returnedODM.setData(odmData);
            oFR.setReturnedDataMessage(returnedODM);

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    /**
     *
     * @param oScreen
     * @param parentObject
     * @param initialConditions
     * @param screenDM
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult getScreenDataLoadingUEConditions(
            OScreen oScreen, BaseEntity parentObject, List<String> initialConditions,
            ODataMessage screenDM, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {

            // Load the datatype & fill the DataMessage
            ODataType screenDataLoadingUEDT = dataTypeService.loadODataType("ScreenDataLoadingUE", loggedUser);
            if (screenDataLoadingUEDT == null) {
                logger.debug("ScreenDataLoadingUE DataType Not Found");
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
                return oFR;
            }

            if (oScreen.getDataLoadingUserExit() != null) {
                ODataMessage screenDataLoadingUEDM = new ODataMessage();
                screenDataLoadingUEDM.setODataType(screenDataLoadingUEDT);
                List<Object> odmData = new ArrayList<Object>();
                odmData.add(initialConditions);
                odmData.add(parentObject);
                odmData.add(screenDM);
                odmData.add(oScreen);
                screenDataLoadingUEDM.setData(odmData);

                //screen name
                OFunctionParms functionParms = new OFunctionParms();
                functionParms.getParams().put("ScreenName", oScreen.getName());

                // Call the function
                OFunctionResult functionRetFR = functionService.runFunction(
                        oScreen.getDataLoadingUserExit(), screenDataLoadingUEDM, functionParms, loggedUser);
                oFR.append(functionRetFR);
                oFR.setReturnedDataMessage(functionRetFR.getReturnedDataMessage());
                try {
                    if (functionRetFR.getReturnedDataMessage() != null) {
                        initialConditions = (List<String>) functionRetFR.getReturnedDataMessage().getData().get(0);
                    }
                } catch (Exception ex) {
                    logger.error("Exception thrown: ", ex);
                    oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
                }
            }
            if (((SingleEntityScreen) oScreen).getOactOnEntity().getDataLoadingUserExit() != null) {

                functionService.setUserExit(true);
                ODataMessage screenDataLoadingUEDM = new ODataMessage();
                screenDataLoadingUEDM.setODataType(screenDataLoadingUEDT);
                List<Object> odmData = new ArrayList<Object>();
                odmData.add(initialConditions);
                odmData.add(parentObject);
                odmData.add(screenDM);
                odmData.add(oScreen);
                screenDataLoadingUEDM.setData(odmData);

                //screen name
                OFunctionParms functionParms = new OFunctionParms();
                functionParms.getParams().put("ScreenName", oScreen.getName());

                // call function
                OFunctionResult functionRetFROEntity = functionService.runFunction(
                        ((SingleEntityScreen) oScreen).getOactOnEntity().getDataLoadingUserExit(), screenDataLoadingUEDM, functionParms, loggedUser);
                oFR.append(functionRetFROEntity);
                oFR.setReturnedDataMessage(functionRetFROEntity.getReturnedDataMessage());
            }
            // Validate the return
            if (oFR.getErrors().isEmpty()) {
                // Successfull calling of java function
                // Validate returned message
                logger.trace("Successfull calling of java function");
                OFunctionResult validationFR;
                validationFR = functionService.validateJavaFunctionODM(
                        oFR.getReturnedDataMessage(), loggedUser, true /*Allow Null*/, List.class);
                if (validationFR != null && !validationFR.getErrors().isEmpty()) {
                    oFR.append(validationFR);
                }
            }

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            logger.trace("Returning with: {}", oFR);
            return oFR;
        }
    }

    /**
     * Sample function for ScreenDataLoadingUserExit
     *
     * @param oDM
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult sampleScreenDataLoadingUserExit(
            ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
            OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                    oDM, loggedUser, true, List.class, BaseEntity.class, ODataMessage.class, OScreen.class);
            if (inputValidationErrors != null) {
                logger.trace("Returning with: {}", inputValidationErrors);
                return inputValidationErrors;
            }
            // </editor-fold>

            List<String> conditions = new ArrayList<String>();

            // Clear all conditions
            OFunctionResult constructFR = constructScreenDataLoadingUEConditionsOFR(conditions, loggedUser);
            oFR.append(constructFR);
            oFR.setReturnedDataMessage(constructFR.getReturnedDataMessage());
            logger.trace("Returning with: {}", oFR);
            return oFR;

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public void saveScreenSessionInfo(OUser user, OScreen screen, OUser loggedUser) {
        logger.debug("Entering");
        ScreenSessionInfo ssi = new ScreenSessionInfo();
        ssi.setOpenDate(timeZoneService.getUserCDT(loggedUser));
        ssi.setOpenedScreen(screen);
        ssi.setLoggedUser(loggedUser);
        saveScreenSessionInfo(ssi, loggedUser);
        logger.debug("Returning");
    }

    @Override
    public void saveScreenSessionInfo(ScreenSessionInfo info, OUser loggedUser) {
        try {
            oem.getEM(loggedUser);
            List<ScreenSessionInfo> sessioninfos
                    = getScreenSessionInfo(info.getLoggedUser().getDbid(),
                            info.getScreenInstId(), loggedUser);
            if (sessioninfos != null) {
                for (int i = 0; i < sessioninfos.size(); i++) {
                    destroyScreenSessionInfo(sessioninfos.get(i), loggedUser);
                }
            }
            oem.saveEntity(info, oem.getSystemUser(loggedUser));
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {
            logger.debug("Returning");
            oem.closeEM(loggedUser);
        }
    }

    @Override
    public void destroyScreenSessionInfo(ScreenSessionInfo info, OUser loggedUser) {
        try {
            logger.debug("Entering");
            oem.getEM(loggedUser);
            oem.deleteEntity(info, oem.getSystemUser(loggedUser));
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {
            logger.debug("Returning");
            oem.closeEM(loggedUser);
        }
    }

    @Override
    public List<ScreenSessionInfo> getScreenSessionInfo(long userID, String screenInstId, OUser loggedUser) {
        logger.debug("Entering");
        List<String> cond = new ArrayList<String>();
        cond.add("loggedUser.dbid = " + userID);
        cond.add("screenInstId = '" + screenInstId + "'");
        try {
            oem.getEM(loggedUser);
            logger.trace("Returning with: {}", oem.loadEntityList(ScreenSessionInfo.class.getSimpleName(), cond, null, null, oem.getSystemUser(loggedUser)));
            return (List<ScreenSessionInfo>) oem.loadEntityList(ScreenSessionInfo.class.getSimpleName(), cond, null, null, oem.getSystemUser(loggedUser));
        } catch (Exception ex) {
            oem.closeEM(loggedUser);
            logger.error("Exception Thrown: ", ex);
            logger.trace("Returning with Null");
            return null;
        } finally {
            logger.debug("Returning");
            oem.closeEM(loggedUser);
        }
    }

    @Override
    public OFunctionResult saveMultiLevelScreenActOnEntity(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
            OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                    oDM, loggedUser, MultiLevelScreenLevel.class);
            if (inputValidationErrors != null) {
                logger.trace("Returnng with: ", inputValidationErrors);
                return inputValidationErrors;
            }
            // </editor-fold>

            MultiLevelScreenLevel multiLevelScreenLevel = (MultiLevelScreenLevel) oDM.getData().get(0);
            if (multiLevelScreenLevel.getLevelOrder() != 0) {
                oFR.setReturnedDataMessage(oDM);
                logger.debug("Returning");
                return oFR;
            }
            OEntity actonEntity = multiLevelScreenLevel.getActOnEntity();
            MultiLevelScreen levelScreen = multiLevelScreenLevel.getLevelScreen();
            levelScreen.setOactOnEntity(actonEntity);
            oem.saveEntity(levelScreen, loggedUser);
            oFR.setReturnedDataMessage(oDM);
            logger.debug("Returning");
            return oFR;
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            if (ex instanceof FABSException) {
                oFR.addError(usrMsgService.getUserMessage("ModuleError", loggedUser));
                oFR.setReturnedDataMessage(oDM);
                logger.trace("Returning");
                logger.debug("Returning");
                return oFR;
            } else {
                oFR.setReturnedDataMessage(oDM);
                logger.trace("Returning with: ", functionService.constructOFunctionResultFromError("SystemInternalError", ex, loggedUser));
                logger.debug("Returning");
                return functionService.constructOFunctionResultFromError("SystemInternalError", ex, loggedUser);
            }
        }
    }

    @Override
    public boolean hasEntityHasChildren(String childEntityName, long entityDBID, String parentFldExp, OUser loggedUser) {
        try {
            logger.debug("Entering");
            oem.getEM(loggedUser);
            int count = 0;
            String query = "SELECT COUNT(e) FROM " + childEntityName + " e WHERE e." + parentFldExp + ".dbid  = " + entityDBID;
            logger.trace("Query: {}", query);
            count = Integer.parseInt(oem.executeEntityQuery(query, loggedUser).toString());
            return !(count == 0);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            return false;
        } finally {
            logger.debug("Returning");
            oem.closeEM(loggedUser);
        }
    }

    @Override
    public List<MultiLevelScreenLevel> getTreeLevels(long levelScreendDBID, OUser loggedUser) {
        try {
            logger.debug("Entering");
            oem.getEM(loggedUser);
            return oem.loadEntityList(MultiLevelScreenLevel.class.getSimpleName(),
                    Collections.singletonList("levelScreen.dbid=" + levelScreendDBID),
                    null, null, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.debug("Returning with Null");
            return null;
        } finally {
            logger.debug("Returning");
            oem.closeEM(loggedUser);
        }
    }

    @Override
    public List<BaseEntity> getLevelEntites(String entityClassName, List<String> conditions,
            List<String> displayedFields, List<String> orderBy, OUser loggedUser) {
        try {
            logger.debug("Entering");
            oem.getEM(loggedUser);
            return oem.loadEntityList(
                    entityClassName,
                    conditions, displayedFields,
                    orderBy,
                    loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown:", ex);
            logger.debug("Returning with Null");
            return null;
        } finally {
            logger.debug("returning");
            oem.closeEM(loggedUser);
        }
    }

    @Override
    public List<BaseEntity> getSelfRecursiveLevelEntityChildren(BaseEntity nodeBaseEntity, List<String> conditions,
            OEntity childOEntity,
            List<String> fldExpInParentEntity,
            OUser loggedUser) {
        try {
            logger.debug("Entering");
            oem.getEM(loggedUser);
            BaseEntity nodeEntity = oem.loadEntityDetail(nodeBaseEntity,
                    conditions, fldExpInParentEntity,
                    loggedUser);
            Field nodelFLD = BaseEntity.getClassField(
                    Class.forName(childOEntity.getEntityClassPath()),
                    fldExpInParentEntity.get(0));
            if (nodelFLD.getType().equals(List.class)) {
                logger.trace("Returning with: ", (List<BaseEntity>) BaseEntity.getValueFromEntity(nodeEntity,
                        fldExpInParentEntity.get(0)));
                return (List<BaseEntity>) BaseEntity.getValueFromEntity(nodeEntity,
                        fldExpInParentEntity.get(0));
            }
            logger.debug("Returning with Null");
            return null;
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.trace("Returning with Null");
            return null;
        } finally {
            logger.debug("Returning");
            oem.closeEM(loggedUser);
        }
    }

    @Override
    public List<BaseEntity> getLevelEntityChildren(BaseEntity nodeBaseEntity, List<String> conditions, OEntity parentOEntity,
            OEntity childOEntity, List<String> fldExpInParentEntity,
            String displayedField,
            OUser loggedUser) {
        try {
            logger.debug("Entering");
            oem.getEM(loggedUser);
            //Get Parent Entity
            logger.trace("Getting Parent Entity");
            BaseEntity parentEntity = oem.loadEntityDetail(nodeBaseEntity,
                    conditions, fldExpInParentEntity,
                    loggedUser);
            Field levelFLD = BaseEntity.getClassField(
                    Class.forName(parentOEntity.getEntityClassPath()),
                    fldExpInParentEntity.get(0));
            if (levelFLD.getType().equals(List.class)) {
                String parentFldExp = levelFLD.getAnnotation(OneToMany.class).mappedBy();
                Field field = null;
                List<ExpressionFieldInfo> expressionFieldInfos
                        = BaseEntity.parseFieldExpression(
                                Class.forName(childOEntity.getEntityClassPath()),
                                displayedField);
                field = expressionFieldInfos.get(expressionFieldInfos.size() - 1).field;
                String fldExp = (field.getAnnotation(Translation.class) != null)
                        ? field.getAnnotation(Translation.class).originalField() : field.getName();
                List<String> orderBy = null;
                if (expressionFieldInfos.size() == 1) {
                    orderBy = new ArrayList<String>();
                    orderBy.add(fldExp);
                }
                conditions.add(parentFldExp + ".dbid=" + parentEntity.getDbid());
                return oem.loadEntityList(
                        childOEntity.
                        getEntityClassName(),
                        conditions,
                        Collections.singletonList(displayedField),
                        orderBy,
                        loggedUser);
            }
            logger.trace("Returning with Null");
            return null;
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.trace("Returning with Null");
            return null;
        } finally {
            logger.debug("Returning");
            oem.closeEM(loggedUser);
        }
    }

    /**
     * Returns ScreenInput of a DBID DataType if not found in screenInputs
     *
     * @param screenInputs Screen Valid Inputs
     * @param setForFilter
     *
     * @return ScreenInput: Success <br>null: Error or already found in
     * screenInputs
     */
    @Override
    public ScreenInputDTO getDBIDAsScreenInput(
            List<ScreenInputDTO> screenInputs, boolean setForFilter, OUser loggedUser) {
        try {
            logger.debug("Entering");
            // Check if input already found in screenInputs
            logger.trace("Check if input already found in screenInputs");
            for (ScreenInputDTO screenInput : screenInputs) {
                if (screenInput.getOdataTypeDBID() == ODataType.DTDBID_DBID) // DBID DT is already found in inputs
                {
                    logger.trace("Returning with Null");
                    return null;
                }
            }

            ODataType dbidDT = dataTypeService.loadODataType("DBID", loggedUser);

            // DBID DT is NOT fond in inputs, create and return it
            ScreenInputDTO screenInput = new ScreenInputDTO();
            screenInput.setOdataTypeDBID(dbidDT.getDbid());
            screenInput.setOdataTypeName("DBID");

            ScreenDTMappingAttrDTO dtMapAttr = new ScreenDTMappingAttrDTO();
            dtMapAttr.setFieldExpression("dbid");
            dtMapAttr.setForFilter(setForFilter);
            dtMapAttr.setOdataTypeAttributeDBID(dbidDT.getODataTypeAttribute().get(0).getDbid());

            screenInput.getAttributesMapping().add(dtMapAttr);
            logger.trace("Returning screenInput with OdataTypeName: {}", screenInput.getOdataTypeName());
            return screenInput;

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.trace("Returning Null");
            return null;
        }
    }

    /**
     * Returns ScreenInput of a Void DataType if not found in screenInputs
     *
     * @param screenInputs Screen Valid Inputs
     * @param setForFilter
     *
     * @return ScreenInput: Success <br>null: Error or already found in
     * screenInputs
     */
    @Override
    public ScreenInputDTO getVoidAsScreenInput(
            List<ScreenInputDTO> screenInputs, OUser loggedUser) {
        try {
            // Check if input already found in screenInputs
            logger.trace("Checking if input already found in screenInputs");
            for (ScreenInputDTO screenInput : screenInputs) {
                if (screenInput.getOdataTypeDBID() == ODataType.DTDBID_VOID) // DBID DT is already found in inputs
                {
                    logger.trace("Returning with Null as DT DBID is already Found and ");
                    return null;
                }
            }

            // DBID DT is NOT fond in inputs, create and return it
            logger.trace("DT DBID is NOT fond in inputs, create and return it");
            ODataType voidDT = dataTypeService.loadODataType("VoidDT", loggedUser);

            ScreenInputDTO screenInput = new ScreenInputDTO();
            screenInput.setOdataTypeDBID(voidDT.getDbid());

            ScreenDTMappingAttrDTO dtMapAttr = new ScreenDTMappingAttrDTO();
            dtMapAttr.setOdataTypeAttributeDBID(voidDT.getODataTypeAttribute().get(0).getDbid());
            screenInput.getAttributesMapping().add(dtMapAttr);
            logger.trace("Returning screenInput with OdataTypeName: {}", screenInput.getOdataTypeName());
            return screenInput;

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    /**
     * Returns ScreenInput of a ParentDBID DataTypes if not found in
     * screenInputs, and if actOnEntity has parent. Sets forInitialization=true
     *
     * @param parentFieldName
     * @param screenInputs Screen Valid Inputs
     * @param setForFilter
     *
     * @return ScreenInput: Success <br>null: Error or already found in
     * screenInputs, actOnEntity has no parent, or parentFieldName is null
     */
    @Override
    public ScreenInput getParentAsScreenInput(String parentFieldName,
            List<ScreenInput> screenInputs, boolean setForFilter, OUser loggedUser) {
        logger.debug("Entering");
        try {
            if (parentFieldName == null) // Entity has no parents
            {
                logger.trace(" Returning with: Null, as Parent Field Name equals Null");
                return null;
            }

            // Check if input already found in screenInputs
            logger.trace("Checking if input already found in screenInputs");
            for (ScreenInput input : screenInputs) {
                if (input.getOdataType().getDbid() == ODataType.DTDBID_PARENTDBID) // Found in inputs, no need to add it
                {
                    logger.trace("Returning with Null");
                    return null;
                }
            }

            // DBID DT is NOT fond in inputs, create and return it
            logger.trace("DT DBID is NOT fond in inputs, create and return it");
            ODataType parentDBIDDT = dataTypeService.loadODataType("ParentDBID", loggedUser);

            ScreenInput parentDBIDInput = new ScreenInput();
            parentDBIDInput.setOdataType(parentDBIDDT);

            ScreenDTMappingAttr dtMapAttr = new ScreenDTMappingAttr();
            dtMapAttr.setFieldExpression(parentFieldName + ".dbid");
            dtMapAttr.setForFilter(setForFilter);
            //dtMapAttr.setForInitialization(true); Don't do this, generates error in setValueInEntity()
            dtMapAttr.setOdataTypeAttribute(parentDBIDDT.getODataTypeAttribute().get(0));
            parentDBIDInput.getAttributesMapping().add(dtMapAttr);
            logger.trace("Returning with: {}", parentDBIDInput.getClassName());
            return parentDBIDInput;

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.trace("Returning with Null");
            return null;
        }
    }

    /**
     * Returns ScreenInput of a DBIDList DataType if not found in screenInputs
     *
     * @param screenInputs Screen Valid Inputs
     * @param setForFilter
     *
     * @return ScreenInput: Success <br>null: Error or already found in
     * screenInputs
     */
    @Override
    public ScreenInputDTO getDBIDListAsScreenInput(
            List<ScreenInputDTO> screenInputs, OUser loggedUser) {
        try {
            // Check if input already found in screenInputs
            logger.debug("Checking if input already found in screenInputs");
            for (ScreenInputDTO screenInput : screenInputs) {
                if (screenInput.getOdataTypeDBID() == ODataType.DTDBID_DBIDLIST) // DBID DT is already found in inputs
                {
                    logger.trace("Returning with Null");
                    return null;
                }
            }

            // DBID DT is NOT fond in inputs, create and return it
            logger.trace("DT DBID is NOT fond in inputs, create and return it");
            ODataType dbidListDT = dataTypeService.loadODataType("DBIDList", loggedUser);

            ScreenInputDTO screenInput = new ScreenInputDTO();
            screenInput.setOdataTypeDBID(dbidListDT.getDbid());

            ScreenDTMappingAttrDTO dtMapAttr = new ScreenDTMappingAttrDTO();
            dtMapAttr.setOdataTypeAttributeDBID(dbidListDT.getODataTypeAttribute().get(0).getDbid());
            dtMapAttr.setFieldExpression("DBIDList");
            screenInput.getAttributesMapping().add(dtMapAttr);
            logger.trace("Returning screenInput with OdataTypeName: ", screenInput.getOdataTypeName());
            return screenInput;

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    /**
     * Return the proper parent all its BriefInfoField loaded into
     * {@link OFunctionResult#returnValues} first element, based on the passed
     * odm & input of a screenEntityDTO. <br> If screenEntityDTO actOnEntity has
     * no parent, then, it returns empty OFR <br> Else if input is of
     * {@link ODataType#DTDBID_PARENTDBID}, then: <br>1. If actOnEntity has only
     * one parent, it's used <br>2. If actOnEntity has more than one parent,
     * parent is got using
     * {@link #getParentFromParentDBID(OEntity, long, OUser) }
     * <br> Else if input is a parent OBJECT of screenEntityDTO actOnEntity,
     * then it's returned <br> Else, mapped parent input to parent field is
     * checked <br> In case of error, return oFR will hold them in messages
     *
     * <br>Preconditions: <br>1. odm {@link ODataMessage#isValid() }
     * <br>2. All inputs are not null
     *
     * @param odmst
     * @param input
     * @param screenEntityDTO
     * @return
     */
    @Override
    public OFunctionResult getEntityParentFromODMInput(
            ODataMessage odm, ScreenInputDTO input, OEntityDTO screenEntityDTO, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            logger.debug("Entering");
            List<String> actOnParentClsPaths = screenEntityDTO.getEntityParentClassPath();
            if (actOnParentClsPaths == null || actOnParentClsPaths.isEmpty() || odm.getData().isEmpty()) // Entity has no parent
            // Cut it short and return success
            {
                logger.trace("Retutning with: {},Because: actOnParentClsPaths is Empty", oFR);
                return oFR;
            }

            String odmParentClsPath = null;
            Object firstPassedObj = odm.getData().get(0);
            long passedObjectDBID = 0; // Leaving it = 0 means no parent passed

            if (odm.getODataType().getDbid() == ODataType.DTDBID_PARENTDBID) // ParentDBID Is Passed
            {
                logger.trace("Parent DBID: {} is Passed", odm.getODataType().getDbid());
                if (actOnParentClsPaths.size() == 1) {
                    // Entity has only one parent
                    logger.trace("Entity has only one parent");
                    odmParentClsPath = actOnParentClsPaths.get(0);
                    passedObjectDBID = Long.parseLong(firstPassedObj.toString());

                } else if (actOnParentClsPaths.size() > 1) {
                    // Entity has more than one parent
                    logger.trace("Entity has more than one parent");
                    passedObjectDBID = Long.parseLong(firstPassedObj.toString());
                    OFunctionResult parentOFR = getParentFromParentDBID(
                            screenEntityDTO, passedObjectDBID, loggedUser);
                    if (!parentOFR.getErrors().isEmpty()) {
                        // Error found
                        oFR.append(parentOFR);
                        logger.trace("Returning with: {}", oFR);
                        return oFR;
                    }
                    if (parentOFR.getReturnValues().isEmpty()) {
                        // No parent found matches the DBID
                        // Consider it error
                        // <editor-fold defaultstate="collapsed" desc="Log">
                        logger.error("Parent DBID Not Found");
                        // </editor-fold>
                        // FIXME: meaningful error message
                        oFR.addError(usrMsgService.getUserMessage(
                                "SystemInternalError", oem.getSystemUser(loggedUser)));
                        logger.trace("Returning with: {}", oFR);
                        return oFR;
                    }
                    odmParentClsPath = ((BaseEntity) parentOFR.getReturnValues().get(0)).getClassName();
                }
            } else // ParentDBID Is NOT Passed
            {
                // <editor-fold defaultstate="collapsed" desc="Check if parent object is passed ">
                logger.trace("ParentDBID Is NOT Passed");
                if (actOnParentClsPaths.size() == 1) {
                    // Act On Entity has one parent
                    logger.trace("Act On Entity has one parent");
                    if (odm.getData().get(0).getClass().getName().equals(actOnParentClsPaths.get(0))) // Data Message First Element is of parent direct class
                    {
                        passedObjectDBID = ((BaseEntity) firstPassedObj).getDbid();
                        odmParentClsPath = actOnParentClsPaths.get(0);
                    }
                } else if (actOnParentClsPaths.size() == 2) {
                    // Act On Entity has two parent
                    logger.trace("Act On Entity has two parent");
                    if (odm.getData().get(0).getClass().getName().equals(actOnParentClsPaths.get(0))) // Data Message First Element is of first parent direct class
                    {
                        passedObjectDBID = ((BaseEntity) firstPassedObj).getDbid();
                        odmParentClsPath = actOnParentClsPaths.get(0);
                    } else if (odm.getData().get(0).getClass().getName().equals(actOnParentClsPaths.get(1))) // Data Message First Element is of second parent direct class
                    {
                        passedObjectDBID = ((BaseEntity) firstPassedObj).getDbid();
                        odmParentClsPath = actOnParentClsPaths.get(1);
                    }
                } else {
                    logger.trace("Act On Entity has Not One or two parent");
                }
                // </editor-fold>

                if (passedObjectDBID == 0) {
                    // No ParentDBID, nor parent object Passed
                    // No Data Message First Element is of parent class
                    // <editor-fold defaultstate="collapsed" desc="Check input mapping to parent field">
                    logger.trace("No ParentDBID, nor parent object Passed");
                    List<String> parentFieldNames = screenEntityDTO.getEntityParentFieldName();
                    if (parentFieldNames != null && !parentFieldNames.isEmpty()) // Entity has parent(s)
                    {
                        // Loop on the parents and check them
                        logger.trace("Entity has parent");
                        for (int iField = 0; iField < parentFieldNames.size(); iField++) {
                            String parentFieldName = parentFieldNames.get(iField);
                            for (int idx = 0; idx < input.getAttributesMapping().size(); idx++) {
                                ScreenDTMappingAttrDTO attr = input.getAttributesMapping().get(idx);
                                if (!attr.isInActive()
                                        && attr.isForInitialization()
                                        && attr.getFieldExpression().equals(parentFieldName)) // Attribute is for initialization as for parent field name
                                {
                                    if (firstPassedObj instanceof BaseEntity) {
                                        passedObjectDBID = ((BaseEntity) firstPassedObj).getDbid();
                                        odmParentClsPath = actOnParentClsPaths.get(iField);    // Assuming
                                        // both have same sorting (consistence)
                                        break;
                                    }
                                }
                            }
                            if (passedObjectDBID != 0) // A parent is found
                            {
                                break; // Suppress further processing
                            }
                        }
                    }
                }
                // </editor-fold>
            }
            if (passedObjectDBID != 0) // Parent is passed, set it
            {
                if (odmParentClsPath == null) {
                    logger.trace("Parent is passed, i will set it");
                    logger.error("odmParentClsPath Must Be Set In The Code");
                    oFR.addError(usrMsgService.getUserMessage("SystemInternalError", oem.getSystemUser(loggedUser)));
                    logger.trace("Returning with: {} as odmParentClsPath equals Null", oFR);
                    return oFR;
                }
                if (actOnParentClsPaths == null) {
                    // ActOnEntity has no parent
                    logger.error("No Parent Found In The Entity, While A Parent Is Passed, Invalid DataMessage");
                    oFR.addError(
                            usrMsgService.getUserMessage("InvalidInput", oem.getSystemUser(loggedUser)));
                    logger.trace("Returning with: {}", oFR);
                    return oFR;
                }
                try {
                    // setParentObject with BIFs
                    // Make sure brief info fields are loaded in the parent object
                    BaseEntity parentWithBIFs = entitySetupService.
                            loadEntityParentForBriefInfoFields(screenEntityDTO, odmParentClsPath,
                                    passedObjectDBID,
                                    loggedUser);
                    List<Object> returnedValues = new ArrayList<Object>(1);
                    returnedValues.add(parentWithBIFs);
                    oFR.setReturnValues(returnedValues);
                    logger.trace("Returning with: {}", oFR);
                    return oFR;

                } catch (UserNotAuthorizedException ex) {
                    logger.error("Not Authorized Exception");
                    oFR.addError(
                            usrMsgService.getUserMessage("InvalidInput", oem.getSystemUser(loggedUser)));
                    logger.trace("Returning with: {}", oFR);
                    return oFR;
                }
            }

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", oem.getSystemUser(loggedUser)), ex);
        } finally {
            if (null != oFR) {
                logger.trace("Returning with: {}", oFR.getReturnedDataMessage());
            }
            return oFR;
        }
    }

    /**
     * Decide the type of the actOnEntity parent based on the passed parentDBID,
     * loads and returns the parent entity. Supports up to two parents.
     *
     * @param actOnEntity {@link OEntity} of the current entity for which the
     * parent is checked
     * @param parentDBID
     * @return If parentDBID is found, parent entity in
     * {@link OFunctionResult#returnValues} first element, and parent field name
     * as the second element <br>Error in {@link OFunctionResult#errors} in case
     * of exception <br>Empty oFR if case of parent DBID not found in any of the
     * entity parents, or entity has no parents
     */
    @Override
    public OFunctionResult getParentFromParentDBID(
            OEntity actOnEntity, long parentDBID, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            List<String> parentEntityClassNames
                    = BaseEntity.getParentClassName(actOnEntity.getEntityClassPath());
            if (parentEntityClassNames == null || parentEntityClassNames.isEmpty()) {
                // Entity has no parents
                logger.trace("Returning with: {} as there is no Parent Class", oFR);
                return oFR;
            }
            List<String> parentFieldNames = BaseEntity.
                    getParentEntityFieldName(actOnEntity.getEntityClassPath());
            BaseEntity firstParent = oem.loadEntity(parentEntityClassNames.get(0),
                    Collections.singletonList("dbid=" + parentDBID),
                    null, oem.getSystemUser(loggedUser));
            if (firstParent != null) {
                oFR.getReturnValues().add(firstParent);
                oFR.getReturnValues().add(parentFieldNames.get(0));
                return oFR;
            }
            BaseEntity scndParent = null;
            if (parentEntityClassNames.size() > 1) {
                scndParent = oem.loadEntity(parentEntityClassNames.get(1),
                        Collections.singletonList("dbid=" + parentDBID),
                        null, oem.getSystemUser(loggedUser));
            }
            if (scndParent != null) {
                oFR.getReturnValues().add(scndParent);
                oFR.getReturnValues().add(parentFieldNames.get(1));
                return oFR;
            }
            // Parent DBID not found in any of the entity parents
            logger.trace("Returning with Null as Parent DBID not found in any of the entity parents");
            return null;

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult getParentFromParentDBID(
            OEntityDTO actOnEntity, long parentDBID, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            logger.debug("Entering");
            List<String> parentEntityClassNames
                    = BaseEntity.getParentClassName(actOnEntity.getEntityClassPath());
            if (parentEntityClassNames == null || parentEntityClassNames.isEmpty()) {
                // Entity has no parents
                logger.trace("Returning, as Entity has no parents");
                return oFR;
            }
            List<String> parentFieldNames = BaseEntity.
                    getParentEntityFieldName(actOnEntity.getEntityClassPath());
            BaseEntity firstParent = oem.loadEntity(parentEntityClassNames.get(0),
                    Collections.singletonList("dbid=" + parentDBID),
                    null, oem.getSystemUser(loggedUser));
            if (firstParent != null) {
                oFR.getReturnValues().add(firstParent);
                oFR.getReturnValues().add(parentFieldNames.get(0));
                logger.debug("Returning");
                return oFR;
            }
            BaseEntity scndParent = null;
            if (parentEntityClassNames.size() > 1) {
                scndParent = oem.loadEntity(parentEntityClassNames.get(1),
                        Collections.singletonList("dbid=" + parentDBID),
                        null, oem.getSystemUser(loggedUser));
            }
            if (scndParent != null) {
                oFR.getReturnValues().add(scndParent);
                oFR.getReturnValues().add(parentFieldNames.get(1));
                logger.debug("Returning");
                return oFR;
            }
            // Parent DBID not found in any of the entity parents
            logger.debug("Returning with Null, as Parent DBID not found in any of the entity parents");
            return null;

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult getParentFromParentDBIDDTO(
            OEntityDTO actOnEntity, long parentDBID, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            logger.debug("Entering");
            List<String> parentEntityClassNames
                    = BaseEntity.getParentClassName(actOnEntity.getEntityClassPath());
            if (parentEntityClassNames == null || parentEntityClassNames.isEmpty()) {
                // Entity has no parents
                logger.debug("Retruning, as Entity has no parents");
                return oFR;
            }
            List<String> parentFieldNames = BaseEntity.
                    getParentEntityFieldName(actOnEntity.getEntityClassPath());
            BaseEntity firstParent = oem.loadEntity(parentEntityClassNames.get(0),
                    Collections.singletonList("dbid=" + parentDBID),
                    null, oem.getSystemUser(loggedUser));
            if (firstParent != null) {
                oFR.getReturnValues().add(firstParent);
                oFR.getReturnValues().add(parentFieldNames.get(0));
                logger.debug("Returning");
                return oFR;
            }
            BaseEntity scndParent = null;
            if (parentEntityClassNames.size() > 1) {
                scndParent = oem.loadEntity(parentEntityClassNames.get(1),
                        Collections.singletonList("dbid=" + parentDBID),
                        null, oem.getSystemUser(loggedUser));
            }
            if (scndParent != null) {
                oFR.getReturnValues().add(scndParent);
                oFR.getReturnValues().add(parentFieldNames.get(1));
                logger.debug("Returning");
                return oFR;
            }
            // Parent DBID not found in any of the entity parents
            logger.debug("Returning with Null, as Parent DBID not found in any of the entity parents");
            return null;

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    /**
     * Returns {@link #createTabularScreenDefaultStuff(TabularScreen, OUser) }
     *
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult createTabularScreenDefaultStuff(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, TabularScreen.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>
        return createTabularScreenDefaultStuff((TabularScreen) odm.getData().get(0), loggedUser);
    }

    /**
     * Screen Fields are created, if not already created, as following: <br>1.
     * Got using {@link EntitySetupServiceRemote#getDefaultBriefInfoFields(OEntity, OUser)
     * }
     * <
     * br>2. Editable <br>3. Default DD is created, if not already created as
     * following: <br>&nbsp; A. Of oEntity Module <br>&nbsp; B. Text Field of
     * length = 20 <br>&nbsp; C. Label of the field name <br> <br>Screen
     * Function is created with name = "Manage " + Entity Class Name + "s", if
     * not already created
     *
     * @param oEntity
     * @param foundTabularScreen
     * @param newScreen
     * @param loggedUser
     * @return oFR with returned value = created Screen in case of success
     */
    public OFunctionResult createTabularScreenDefaultStuff(TabularScreen tabularScreen, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OEntityDTO oEntity;
        try {
            logger.debug("Entering");
            oem.getEM(loggedUser);
            long screenDBID = tabularScreen.getDbid();
            oEntity = entitySetupService.getOScreenEntityDTO(screenDBID, loggedUser);
            List<Field> entityFieldsList = entitySetupService.getDefaultBriefInfoFields(oEntity, loggedUser);
            BaseEntity entityInst = (BaseEntity) Class.forName(
                    oEntity.getEntityClassPath()).newInstance();

            // <editor-fold defaultstate="collapsed" desc="Create screen field & DDs">
            logger.trace("Creating Screen Fields for Screen");
            // Add all Screen Fields to the Tabular Screen
            List<ScreenField> screenFieldsList = new ArrayList();
            int sortIndex = 1;
            if (entityFieldsList != null) {
                for (Field field : entityFieldsList) {
                    ScreenField screenField = new ScreenField();
                    String fieldDDName = BaseEntity.getFieldDDName(entityInst, field.getName());
                    DD fieldDD = ddService.getDD(fieldDDName, oem.getSystemUser(loggedUser));
                    if (fieldDD == null) {
                        // Field DD not created yet
                        // Create Field DD
                        oFR.append(ddService.createFieldDefaultDD(fieldDDName, field, oEntity, loggedUser));
                        if (oFR.getErrors().size() > 0) {
                            return oFR;
                        }
                        fieldDD = (DD) oFR.getReturnValues().get(0);
                        logger.trace("DD: {} Created", fieldDD.getName());
                    }
                    screenField.setDd(fieldDD);
                    screenField.setFieldExpression(field.getName());
                    screenField.setEditable(true);
                    screenField.setOScreen(tabularScreen);
                    screenField.setSortIndex(sortIndex);

                    oFR.append(entitySetupService.
                            callEntityCreateAction(screenField, loggedUser));
                    logger.trace("Screen Field: {} Created", screenField);
                    screenFieldsList.add(screenField);
                    sortIndex++;
                }
                tabularScreen.setScreenFields(screenFieldsList);
                oFR.append(entitySetupService.callEntityUpdateAction(tabularScreen, loggedUser));
            } else {
                //created Entity has no fields
                if (null != oEntity) {
                    logger.trace("Created Entity: {} Has No Fields", oEntity.getEntityClassPath());
                }
            }
            // </editor-fold>

            List<ScreenFunction> screenFunctions = (List<ScreenFunction>) oem.loadEntityList("ScreenFunction",
                    Collections.singletonList("oscreen.dbid=" + tabularScreen.getDbid()),
                    null, null, oem.getSystemUser(loggedUser));

            List<OEntity> parentOEntity = entitySetupService.
                    loadParentOEntity(oEntity, oem.getSystemUser(loggedUser));
            if (screenFunctions.isEmpty()) {
                logger.trace("Screen Function is Empty");
                // <editor-fold defaultstate="collapsed" desc="Create Screen Function">
                ScreenFunction screenFunction = new ScreenFunction();

                Date currentDate = timeZoneService.getUserCDT(loggedUser);
                screenFunction.setOscreen(tabularScreen);
                screenFunction.setCode(Long.toString(currentDate.getTime()));
                screenFunction.setName("Manage " + entityInst.getClassName() + "s");
//                screenFunction.setNameTranslated("Manage " + entityInst.getClassName() + "s");
                UDC editScreenMode = (UDC) oem.loadEntity("UDC",
                        Collections.singletonList("value='Managed'"),
                        null, oem.getSystemUser(loggedUser));
                screenFunction.setScreenMode(editScreenMode);

                ODataType oDataType = null;
                if (parentOEntity != null && !parentOEntity.isEmpty()) {
                    oDataType = dataTypeService.loadODataType( // FIXME: support two parents
                            parentOEntity.get(0).getClassName(), loggedUser);
                } else {
                    logger.trace("Parent OEntity is Empty or Null");
                    oDataType = dataTypeService.loadODataType("VoidDT", loggedUser);
                }
                if (oDataType != null) {
                    screenFunction.setOdataType(oDataType);
                } else {
                    logger.error("Parent Not Found");
                    oFR.append(entitySetupService.callEntityCreateAction(screenFunction, loggedUser));
                    return oFR;
                }

                // Save the Screen Function:
                oFR.append(entitySetupService.callEntityCreateAction(screenFunction, loggedUser));
                if (oFR.getErrors().size() > 0) {
                    logger.error("Error Creating Screen Function");
                }
                // </editor-fold>
            } else {
                logger.trace("ScreenFunction already exist");
            }

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult ServerJobUserExit(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {

            List<String> conditions = (List<String>) oDM.getData().get(0);
            conditions.add("loggedUser.dbid=" + loggedUser.getDbid());
            OFunctionResult constructFR = uIFrameworkService.
                    constructScreenDataLoadingUEConditionsOFR(conditions, loggedUser);
            oFR.append(constructFR);
            oFR.setReturnedDataMessage(constructFR.getReturnedDataMessage());
            logger.debug("returning");
            return oFR;

        } catch (Exception ex) {
            logger.error("exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }
    //This method has no real usage and for server job testing only

    @Override
    public OFunctionResult testServerJob(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            logger.debug("Entering");
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult addOPortletPage(long oPortalPageDBID, OUser loggedUser,
            String plid) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            oem.executeUpdateQuery("UPDATE OPortalPage p SET p.portletPageID=" + plid
                    + " WHERE p.dbid=" + oPortalPageDBID, oem.getSystemUser(loggedUser));
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public List<PortalPagePortletLite> getPortalPagePortlets(long portalPageDBID, OUser loggedUser) {
        logger.debug("Entering");
        List<PortalPagePortletLite> pagePortletLites = new ArrayList<PortalPagePortletLite>();
        try {
            oem.getEM(loggedUser);
            pagePortletLites = oem.
                    createListNamedQuery("getPagePortlets", loggedUser, "portalPageDBID", portalPageDBID);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return pagePortletLites;
        }
    }

    /**
     *
     * @param portalPageDBID
     * @param loggedUser
     * @return
     */
    @Override
    public OPortalPageLite getPortalPage(long portalPageDBID, OUser loggedUser) {
        logger.debug("Entering");
        List<OPortalPageLite> portalPageLite = new ArrayList<OPortalPageLite>();
        try {
            portalPageLite = oem.createListNamedQuery("getPage", loggedUser, "dbid", portalPageDBID, "langDBID",
                    loggedUser.getFirstLanguage().getDbid());
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {
            logger.debug("Returning");
            return portalPageLite.get(0);
        }
    }

    /**
     *
     * @param screenDBID
     * @param loggedUser
     * @return
     */
    @Override
    public OScreenLite getOScreenLite(long screenDBID, OUser loggedUser) {
        logger.debug("Entering");
        List<OScreenLite> oScreenLite = new ArrayList<OScreenLite>();
        try {
            oem.getEM(loggedUser);
            oScreenLite = oem.
                    createListNamedQuery("getScreen", loggedUser, "portalPageDBID", screenDBID, "langDBID", loggedUser.getFirstLanguage().getDbid());
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return (null == oScreenLite || oScreenLite.size() <= 0) ? null : oScreenLite.get(0);
        }
    }

    @Override
    public List<PortalPagePortletLite> getToBeRenderList(long parentPortletDBID, OUser loggedUser) {
        logger.debug("Entering");
        List<PortalPagePortletLite> toBeRenderedPortlets = new ArrayList<PortalPagePortletLite>();
        try {
            toBeRenderedPortlets = oem.createListNamedQuery("getToBeRenderedList", loggedUser,
                    "parentPortletDBID", parentPortletDBID);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {
            logger.debug("Returning");
            return toBeRenderedPortlets;
        }
    }

    @Override
    public List<PortalPagePortletReceiverLite> getPortletReceiversList(long senderDBID, OUser loggedUser) {
        logger.debug("Entering");
        List<PortalPagePortletReceiverLite> receivers = new ArrayList<PortalPagePortletReceiverLite>();
        try {
            receivers = oem.createListNamedQuery("getPortletReceiversList", loggedUser,
                    "senderDBID", senderDBID);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {
            logger.debug("Returning");
            return receivers;
        }
    }

    @Override
    public OrgChartFunctionLite getOrgChartFunctionLite(String code, OUser loggedUser) {
        logger.debug("Entering");
        List<OrgChartFunctionLite> function = new ArrayList<OrgChartFunctionLite>();
        try {
            function = oem.createListNamedQuery("getOrgChartFunction", loggedUser,
                    "code", code);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {
            logger.debug("Returning");
            return function.get(0);
        }
    }

    @Override
    public FABSSetupLite getFABSSetupLite(String key, OUser loggedUser) {
        logger.debug("entering");
        List<FABSSetupLite> fabsSetup = new ArrayList<FABSSetupLite>();
        try {
            fabsSetup = oem.createListNamedQuery("getFABSSetup", loggedUser,
                    "key", key);
        } catch (Exception ex) {
            logger.error("exception thrown: ", ex);
        } finally {
            logger.debug("Returning");
            return fabsSetup.get(0);
        }
    }

    @Override
    public OScreenDTO getScreenDTOObject(String screenName, OUser loggedUser) {
        OScreenDTO screenDTO = null;
        try {
            logger.debug("Entering");
            screenDTO = (OScreenDTO) oem.executeEntityQuery("SELECT NEW com.unitedofoq.fabs.core.uiframework.screen.dto.OScreenDTO("
                    + " o.dbid,o.inlineHelp, o.name, o.header, o.viewPage,"
                    + " o.showHeader,o.showActiveOnly, o.prsnlzd, o.prsnlzdOriginal_DBID,"
                    + " o.prsnlzdUser_DBID, o.screenFilter.dbid) FROM OScreen o"
                    + " WHERE o.name='" + screenName + "'", loggedUser);
            String query = "SELECT NEW com.unitedofoq.fabs.core.uiframework.screen.dto.OScreenDTO("
                    + " o.dbid,o.inlineHelp, o.name, o.header, o.viewPage,"
                    + " o.showHeader,o.showActiveOnly, o.prsnlzd, o.prsnlzdOriginal_DBID,"
                    + " o.prsnlzdUser_DBID, o.screenFilter.dbid) FROM OScreen o"
                    + " WHERE o.name='" + screenName + "'";
            logger.trace("Query = {}", query);
            if (!loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).getSvalue()) && null != screenDTO) {
                String header = (String) oem.executeEntityQuery("select trans.header from"
                        + " OScreenTranslation trans where trans.entityDBID = "
                        + screenDTO.getDbid()
                        + " and trans.language.dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                screenDTO.setHeader(header);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        return screenDTO;
    }

    @Override
    public OScreenDTO getFormScreenDTOObject(String screenName, OUser loggedUser) {
        OScreenDTO screenDTO = null;
        try {
            screenDTO = (OScreenDTO) oem.executeEntityQuery("SELECT NEW com.unitedofoq.fabs.core.uiframework.screen.dto.OScreenDTO("
                    + " o.dbid,o.inlineHelp, o.name, o.header, o.viewPage,"
                    + " o.showHeader,o.showActiveOnly, o.prsnlzd, o.prsnlzdOriginal_DBID,"
                    + " o.prsnlzdUser_DBID, o.screenFilter.dbid) FROM OScreen o"
                    + " WHERE o.name='" + screenName + "'", loggedUser);
            String query = "SELECT NEW com.unitedofoq.fabs.core.uiframework.screen.dto.OScreenDTO("
                    + " o.dbid,o.inlineHelp, o.name, o.header, o.viewPage,"
                    + " o.showHeader,o.showActiveOnly, o.prsnlzd, o.prsnlzdOriginal_DBID,"
                    + " o.prsnlzdUser_DBID, o.screenFilter.dbid) FROM OScreen o"
                    + " WHERE o.name='" + screenName + "'";
            logger.trace("Query = {}", query);
            if (!loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).getSvalue()) && null != screenDTO) {
                String header = (String) oem.executeEntityQuery("select trans.header from"
                        + " OScreenTranslation trans where trans.entityDBID = "
                        + screenDTO.getDbid()
                        + " and trans.language.dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                screenDTO.setHeader(header);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        logger.debug("Returning");
        return screenDTO;
    }

    @Override
    public ArrayList<OEntityActionDTO> getDisplayedDTOActions(long oEntityDBID, OUser loggedUser) {
        logger.debug("Entering");
        ArrayList<OEntityActionDTO> displayedActions = null;
        Vector<OEntityActionDTO> actionDTOs;
        String query;
        query = "SELECT NEW com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO("
                + "a.dbid,a.oownerEntity.dbid, a.name,a.ofunction.dbid, a.ofunction.name, a.sortIndex,"
                + "a.displayed, a.displayCondition) "//////
                + " FROM OEntityAction a"
                + " WHERE a.displayed = true"
                + " AND a.inActive = false"
                + " AND a.oownerEntity.dbid = " + oEntityDBID
                + " ORDER BY  a.sortIndex";
        logger.trace("Query = {}", query);
        try {
            actionDTOs = (Vector<OEntityActionDTO>) oem.executeEntityListQuery(query, loggedUser);
            displayedActions = new ArrayList<OEntityActionDTO>(actionDTOs);
            for (OEntityActionDTO oEntityActionDTO : displayedActions) {
                if (oEntityActionDTO.getOfuntionDBID() > 0) {
                    String ftype = (String) oem.executeEntityNativeQuery("select FTYPE from ofunction where dbid = "
                            + oEntityActionDTO.getOfuntionDBID(), loggedUser);
                    oEntityActionDTO.setOfunctionType(ftype);
                    if ("SCREEN".equalsIgnoreCase(ftype)) {
                        String screenName = (String) oem.executeEntityQuery("SELECT sf.oscreen.name"
                                + " FROM ScreenFunction sf"
                                + " WHERE sf.dbid = " + oEntityActionDTO.getOfuntionDBID(), loggedUser);
                        long screenMode = (Long) oem.executeEntityQuery("SELECT sf.screenMode.dbid"
                                + " FROM ScreenFunction sf"
                                + " WHERE sf.dbid = " + oEntityActionDTO.getOfuntionDBID(), loggedUser);
                        oEntityActionDTO.setOfunctionScreenName(screenName);
                        oEntityActionDTO.setScreenMode(screenMode);
                    }
                }
            }
            if (!loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).getSvalue()) && null != displayedActions
                    && !displayedActions.isEmpty()) {
                for (OEntityActionDTO oEntityActionDTO : displayedActions) {
                    String actionName = (String) oem.
                            executeEntityQuery("select entityAcTrans.name from"
                                    + " OEntityActionTranslation entityAcTrans where entityAcTrans.entityDBID = "
                                    + oEntityActionDTO.getDbid()
                                    + " and entityAcTrans.language.dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                    if (null != actionName && !"".equals(actionName)) {
                        oEntityActionDTO.setName(actionName);
                    }
                    if (0 < oEntityActionDTO.getOfuntionDBID()) {
                        String functionName = (String) oem.executeEntityQuery("select ofunTrans.name from"
                                + " OFunctionTranslation ofunTrans where ofunTrans.entityDBID = "
                                + oEntityActionDTO.getOfuntionDBID()
                                + " and ofunTrans.language.dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                        if (null != functionName && !"".equals(functionName)) {
                            oEntityActionDTO.setOfunctionName(functionName);
                        }
                    }
                }

            }
            ArrayList<OEntityActionDTO> availableActions = new ArrayList<OEntityActionDTO>();
            for (Iterator<OEntityActionDTO> it = displayedActions.iterator(); it.hasNext();) {
                OEntityActionDTO oea = it.next();
                try {
                    securityService.checkActionOnlyAuthority(
                            oea, loggedUser, oem.getSystemUser(loggedUser));
                    availableActions.add(oea);
                } catch (Exception ex) {
                    logger.error("Exception thrown: ", ex);
                }
            }
            displayedActions = availableActions;
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        //TODO: Add security Check by using method checkPrivilegeAuthorithy
        logger.debug("Returning");
        return displayedActions;
    }

    @Override
    public ArrayList<OObjectBriefInfoFieldDTO> getBriefInfoFieldDTOs(long objectDBID, OUser loggedUser) {
        logger.debug("Entering");
        ArrayList<OObjectBriefInfoFieldDTO> briefInfoFieldDTOs = null;
        String query = null;
        try {
            query = "SELECT NEW com.unitedofoq.fabs.core.entitysetup.dto.OObjectBriefInfoFieldDTO("
                    + "obi.fieldExpression, obi.primary, obi.fieldJavaType, obi.sortIndex,"
                    + "obi.masterField, obi.titleOverride, obi.dd.label,obi.dd.dbid, obi.dbid)"
                    + " FROM OObjectBriefInfoField obi"
                    + " WHERE obi.oObject.dbid = " + objectDBID
                    + " AND obi.inActive = false";
            logger.trace("Query = {}", query);
            Vector<OObjectBriefInfoFieldDTO> dTOs = (Vector<OObjectBriefInfoFieldDTO>) oem.executeEntityListQuery(query, loggedUser);
            briefInfoFieldDTOs = new ArrayList<OObjectBriefInfoFieldDTO>(dTOs);
            if (!loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).getSvalue()) && null != briefInfoFieldDTOs
                    && !briefInfoFieldDTOs.isEmpty()) {
                for (OObjectBriefInfoFieldDTO oObjectBriefInfoFieldDTO : briefInfoFieldDTOs) {
                    String titleOverride = (String) oem.executeEntityQuery("SELECT b.titleOverride"
                            + " FROM OObjectBriefInfoFieldi18n b where b.entityDBID = "
                            + oObjectBriefInfoFieldDTO.getDbid()
                            + " and b.language.dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                    if (null != titleOverride || !"".equals(titleOverride)) {
                        oObjectBriefInfoFieldDTO.setTitleOverride(titleOverride);
                    }
                    String ddLabel = (String) oem.executeEntityQuery("SELECT d.label"
                            + " FROM DDTranslation d where d.entityDBID = "
                            + oObjectBriefInfoFieldDTO.getDdDBID()
                            + " and d.language.dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                    if (null != ddLabel || !"".equals(ddLabel)) {
                        oObjectBriefInfoFieldDTO.setDdLabel(ddLabel);
                    }

                }
            }

        } catch (Exception ex) {
            logger.error("exception thrown: ", ex);
        }
        logger.debug("Returning");
        return briefInfoFieldDTOs;
    }

    @Override
    public ArrayList<ScreenInputDTO> getScreenInputDTOs(long screenDBID, OUser loggedUser) {
        logger.debug("Entering");
        ArrayList<ScreenInputDTO> screenInputDTOs = null;
        String query = null;
        query = "SELECT NEW com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenInputDTO("
                + "inp.inputHeader, inp.odataType.dbid, inp.dbid, inp.inActive,inp.odataType.name)"
                + " FROM ScreenInput inp"
                + " WHERE inp.oscreen.dbid = " + screenDBID
                + " AND inp.inActive = false";
        try {
            logger.trace("Query = {}", query);
            Vector<ScreenInputDTO> dTOs = (Vector<ScreenInputDTO>) oem.executeEntityListQuery(query, loggedUser);
            screenInputDTOs = new ArrayList<ScreenInputDTO>(dTOs);

            String mappingAttrQuery = null;
            for (ScreenInputDTO screenInputDTO : screenInputDTOs) {
                mappingAttrQuery = "SELECT NEW com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenDTMappingAttrDTO("
                        + "mapAttr.fieldExpression, mapAttr.attributeExpression, mapAttr.forFilter,mapAttr.forInitialization"
                        + ",odtA.description, odtA.dbid, mapAttr.inActive, mapAttr.partOfCompKey)"
                        + " FROM ScreenDTMappingAttr mapAttr left join mapAttr.odataTypeAttribute odtA "
                        + " WHERE mapAttr.screenDTMapping.dbid = " + screenInputDTO.getDbid()
                        + " AND mapAttr.inActive = false";
                logger.trace("Query: {}", mappingAttrQuery);
                Vector<ScreenDTMappingAttrDTO> mappDTOs = (Vector<ScreenDTMappingAttrDTO>) oem.executeEntityListQuery(mappingAttrQuery, loggedUser);
                ArrayList<ScreenDTMappingAttrDTO> screenInputMappDTOs = new ArrayList<ScreenDTMappingAttrDTO>(mappDTOs);
                if (!loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                        fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).getSvalue())
                        && null != screenInputMappDTOs && !screenInputMappDTOs.isEmpty()) {
                    for (ScreenDTMappingAttrDTO screenMapAttrDTO : screenInputMappDTOs) {
                        if (screenMapAttrDTO.getOdataTypeAttributeDBID() != 0) {
                            String description = (String) oem.executeEntityQuery("SELECT oDTT.description"
                                    + " FROM ODataTypeAttributeTranslation oDTT where oDTT.entityDBID = "
                                    + screenMapAttrDTO.getOdataTypeAttributeDBID()
                                    + " and oDTT.language.dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                            if (null != description || !"".equals(description)) {
                                screenMapAttrDTO.setOdataTypeAttributeDescription(description);
                            }
                        }
                    }
                }
                screenInputDTO.setAttributesMapping(screenInputMappDTOs);

            }
        } catch (Exception ex) {
            logger.error("exception thrown: ", ex);
        }
        logger.debug("Returning");
        return screenInputDTOs;
    }

    @Override
    public ScreenInputDTO getScreenInputDTO(long inputDBID, OUser loggedUser) {
        logger.debug("Entering");
        ScreenInputDTO screenInputDTO = null;
        String query = null;
        query = "SELECT NEW com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenInputDTO("
                + "inp.inputHeader, inp.odataType.dbid, inp.dbid, inp.inActive,inp.odataType.name)"
                + " FROM ScreenInput inp"
                + " WHERE inp.dbid = " + inputDBID;
        logger.trace("Query: {}", query);
        try {
            screenInputDTO = (ScreenInputDTO) oem.executeEntityQuery(query, loggedUser);
            if (null != screenInputDTO) {
                String mappingAttrQuery = "SELECT NEW com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenDTMappingAttrDTO("
                        + "mapAttr.fieldExpression, mapAttr.attributeExpression, mapAttr.forFilter,mapAttr.forInitialization"
                        + ",odtA.description, odtA.dbid, mapAttr.inActive, mapAttr.partOfCompKey )"
                        + " FROM ScreenDTMappingAttr mapAttr left join mapAttr.odataTypeAttribute odtA "
                        + " WHERE mapAttr.screenDTMapping.dbid = " + screenInputDTO.getDbid()
                        + " AND mapAttr.inActive = false";
                logger.trace("Query: {}", mappingAttrQuery);
                Vector<ScreenDTMappingAttrDTO> mappDTOs = (Vector<ScreenDTMappingAttrDTO>) oem.executeEntityListQuery(mappingAttrQuery, loggedUser);
                ArrayList<ScreenDTMappingAttrDTO> screenInputMappDTOs = new ArrayList<ScreenDTMappingAttrDTO>(mappDTOs);
                if (!loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                        fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).getSvalue())
                        && null != screenInputMappDTOs && !screenInputMappDTOs.isEmpty()) {
                    for (ScreenDTMappingAttrDTO screenMapAttrDTO : screenInputMappDTOs) {
                        if (screenMapAttrDTO.getOdataTypeAttributeDBID() != 0) {
                            String description = (String) oem.executeEntityQuery("SELECT oDTT.description"
                                    + " FROM ODataTypeAttributeTranslation oDTT where oDTT.entityDBID = "
                                    + screenMapAttrDTO.getOdataTypeAttributeDBID()
                                    + " and oDTT.language.dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                            if (null != description || !"".equals(description)) {
                                screenMapAttrDTO.setOdataTypeAttributeDescription(description);
                            }
                        }
                    }
                }
                screenInputDTO.setAttributesMapping(screenInputMappDTOs);
            }

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        return screenInputDTO;
    }

    @Override
    public ScreenDTMappingAttrDTO getScreenInputAttMappingDTO(long inputAttrDBID, OUser loggedUser) {
        logger.debug("Entering");
        String mappingAttrQuery = null;
        ScreenDTMappingAttrDTO screenMapAttrDTO = new ScreenDTMappingAttrDTO();
        try {
            mappingAttrQuery = "SELECT NEW com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenDTMappingAttrDTO("
                    + "mapAttr.fieldExpression, mapAttr.attributeExpression, mapAttr.forFilter,mapAttr.forInitialization"
                    + ",odtA.description, odtA.dbid, mapAttr.inActive, mapAttr.partOfCompKey )"
                    + " FROM ScreenDTMappingAttr mapAttr  left join mapAttr.odataTypeAttribute odtA "
                    + " WHERE mapAttr.dbid = " + inputAttrDBID;
            logger.trace("Query: {}", mappingAttrQuery);
            screenMapAttrDTO = (ScreenDTMappingAttrDTO) oem.executeEntityQuery(mappingAttrQuery, loggedUser);
            if (!loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                    fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).getSvalue())) {
                if (screenMapAttrDTO.getOdataTypeAttributeDBID() != 0) {
                    String description = (String) oem.executeEntityQuery("SELECT oDTT.description"
                            + " FROM ODataTypeAttributeTranslation oDTT where oDTT.entityDBID = "
                            + screenMapAttrDTO.getOdataTypeAttributeDBID()
                            + " and oDTT.language.dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                    if (null != description || !"".equals(description)) {
                        screenMapAttrDTO.setOdataTypeAttributeDescription(description);
                    }
                }
            }

        } catch (Exception ex) {
            logger.error("exception thrown: ", ex);
            return null;
        }
        logger.trace("Returning");
        return screenMapAttrDTO;
    }

    @Override
    public ScreenInput getScreenInputIfDataTypeDBIDOrVoid(long dbid, OUser loggedUser) {
        try {
            logger.debug("Entering");
            // DBID DT is NOT found in inputs, create and return it
            ODataType voidDT = dataTypeService.loadODataType("VoidDT", loggedUser);
            if (dbid == voidDT.getDbid()) {

                ScreenInput screenInput = new ScreenInput();
                screenInput.setOdataType(voidDT);

                ScreenDTMappingAttr dtMapAttr = new ScreenDTMappingAttr();
                dtMapAttr.setOdataTypeAttribute(voidDT.getODataTypeAttribute().get(0));
                screenInput.getAttributesMapping().add(dtMapAttr);

                return screenInput;
            } else {
                ODataType dbidDT = dataTypeService.loadODataType("DBID", loggedUser);
                // DBID DT is NOT found in inputs, create and return it
                ScreenInput screenInput = new ScreenInput();
                screenInput.setOdataType(dbidDT);

                ScreenDTMappingAttr dtMapAttr = new ScreenDTMappingAttr();
                dtMapAttr.setFieldExpression("dbid");
                dtMapAttr.setForFilter(true);
                dtMapAttr.setOdataTypeAttribute(dbidDT.getODataTypeAttribute().get(0));

                screenInput.getAttributesMapping().add(dtMapAttr);
                logger.debug("Returning");
                return screenInput;
            }

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    @Override
    public ArrayList<ScreenOutputDTO> getScreenOuputDTOs(long screenDBID, OUser loggedUser) {
        logger.debug("Entering");
        ArrayList<ScreenOutputDTO> screenInputDTOs = null;
        String query = null;
        query = "SELECT NEW com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenOutputDTO("
                + "outpt.odataType.dbid, outpt.dbid)"
                + " FROM ScreenOutput outpt"
                + " WHERE outpt.oscreen.dbid = " + screenDBID
                + " AND outpt.inActive = false";
        logger.trace("Query: {}", query);
        try {
            Vector<ScreenOutputDTO> dTOs = (Vector<ScreenOutputDTO>) oem.executeEntityListQuery(query, loggedUser);
            screenInputDTOs = new ArrayList<ScreenOutputDTO>(dTOs);

            String mappingAttrQuery = null;
            for (ScreenOutputDTO screenOutputDTO : screenInputDTOs) {
                mappingAttrQuery = "SELECT NEW com.unitedofoq.fabs.core.uiframework.screen.dto.ScreenDTMappingAttrDTO("
                        + "mapAttr.fieldExpression, mapAttr.attributeExpression, mapAttr.forFilter,mapAttr.forInitialization"
                        + ",odtA.description, odtA.dbid, mapAttr.inActive, mapAttr.partOfCompKey)"
                        + " FROM ScreenDTMappingAttr mapAttr left join mapAttr.odataTypeAttribute odtA "
                        + " WHERE mapAttr.screenDTMapping.dbid = " + screenOutputDTO.getDbid()
                        + " AND mapAttr.inActive = false";
                logger.trace("Query: {}", mappingAttrQuery);
                Vector<ScreenDTMappingAttrDTO> mappDTOs = (Vector<ScreenDTMappingAttrDTO>) oem.executeEntityListQuery(mappingAttrQuery, loggedUser);
                ArrayList<ScreenDTMappingAttrDTO> screenInputMappDTOs = new ArrayList<ScreenDTMappingAttrDTO>(mappDTOs);
                if (!loggedUser.getFirstLanguage().getValue().equalsIgnoreCase(
                        fABSSetupLocal.loadKeySetup("DefaultLanguage", loggedUser).getSvalue())
                        && null != screenInputMappDTOs && !screenInputMappDTOs.isEmpty()) {
                    for (ScreenDTMappingAttrDTO screenMapAttrDTO : screenInputMappDTOs) {
                        if (screenMapAttrDTO.getOdataTypeAttributeDBID() != 0) {
                            String description = (String) oem.executeEntityQuery("SELECT oDTT.description"
                                    + " FROM ODataTypeAttributeTranslation oDTT where oDTT.entityDBID = "
                                    + screenMapAttrDTO.getOdataTypeAttributeDBID()
                                    + " and oDTT.language.dbid = " + loggedUser.getFirstLanguage().getDbid(), loggedUser);
                            if (null != description || !"".equals(description)) {
                                screenMapAttrDTO.setOdataTypeAttributeDescription(description);
                            }
                        }
                    }
                }
                screenOutputDTO.setAttributesMapping(screenInputMappDTOs);

            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        logger.debug("Returning");
        return screenInputDTOs;
    }

    @Override
    public OPortalPage getOportalPage(long dbid, OUser loggedUser) {
        logger.debug("Entering");
        OPortalPage page = null;
        try {
            page = (OPortalPage) oem.loadEntity(OPortalPage.class.getSimpleName(),
                    Collections.singletonList("dbid = " + dbid), null, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {
            logger.debug("Returning");
            return page;
        }
    }

    @Override
    public ArrayList<ServerJobDTO> getServerJobDTOs(OUser loggedUser) {
        logger.debug("Entering");
        ArrayList<ServerJobDTO> serverJobDTOs = null;

        //if u have inner entity you MUST use LEFT JOIN not the "." operator
        //cause if u used the "." operator and there is an inner entity==null you willl never get resultSet
        String query = "SELECT NEW com.unitedofoq.fabs.core.uiframework.serverjob.resources.ServerJobDTO("
                + " serverJobEntity.dbid,"
                + " baseAlert.dbid,"
                + " runningScheduleEXPR.dbid,"
                + " actOnEnt.dbid,"
                + " actOnEnt.entityClassPath,"
                + " runningScheduleEXPR.plannedEndDateExpression,"
                + " runningScheduleEXPR.plannedStartDateExpression,"
                + " runningScheduleEXPR.recurUnitExpression,"
                + " runningScheduleEXPR.plannedEndDate,"
                + " runningScheduleEXPR.plannedStartDate,"
                + " recurUnitUDC.dbid ,"
                + " recurUnitUDC.code ,"
                + " serverJobEntity.functionName ,"
                + " serverJobEntity.code,"
                + " serverJobEntity.functionClassPath ,"
                + " notifiType.dbid,"
                + " serverJobEntity.omodule.dbid)"
                + " FROM JavaFunction serverJobEntity "
                + " LEFT JOIN serverJobEntity.baseAlert baseAlert"
                + " LEFT JOIN serverJobEntity.notificationType notifiType"
                + " LEFT JOIN serverJobEntity.runningScheduleExpression runningScheduleEXPR "
                + " LEFT JOIN serverJobEntity.runningScheduleExpression.actOnEntity actOnEnt"
                + " LEFT JOIN serverJobEntity.runningScheduleExpression.recurUnit recurUnitUDC"
                + " WHERE serverJobEntity.serverJob = 1";
        logger.trace("Query: {}", query);
        try {
            serverJobDTOs = new ArrayList<ServerJobDTO>((Vector<ServerJobDTO>) oem.executeEntityListQuery(query, loggedUser));
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        logger.debug("Returning");
        return serverJobDTOs;
    }

    @Override
    public ServerJobDTO getServerJobDTO(OUser loggedUser, long scheduleExpressionDBID) {
        logger.debug("Entering");
        ServerJobDTO serverJobDTO = null;

        //if u have inner entity you MUST use LEFT JOIN not the "." operator
        //cause if u used the "." operator and there is an inner entity==null you willl never get resultSet
        String query = "SELECT NEW com.unitedofoq.fabs.core.uiframework.serverjob.resources.ServerJobDTO("
                + " serverJobEntity.dbid,"
                + " baseAlert.dbid,"
                + " runningScheduleEXPR.dbid,"
                + " actOnEnt.dbid,"
                + " actOnEnt.entityClassPath,"
                + " runningScheduleEXPR.plannedEndDateExpression,"
                + " runningScheduleEXPR.plannedStartDateExpression,"
                + " runningScheduleEXPR.recurUnitExpression,"
                + " runningScheduleEXPR.plannedEndDate,"
                + " runningScheduleEXPR.plannedStartDate,"
                + " recurUnitUDC.dbid ,"
                + " recurUnitUDC.code ,"
                + " serverJobEntity.functionName ,"
                + " serverJobEntity.code,"
                + " serverJobEntity.functionClassPath ,"
                + " notifiType.dbid,"
                + " serverJobEntity.omodule.dbid)"
                + " FROM JavaFunction serverJobEntity "
                + " LEFT JOIN serverJobEntity.baseAlert baseAlert"
                + " LEFT JOIN serverJobEntity.notificationType notifiType"
                + " LEFT JOIN serverJobEntity.runningScheduleExpression runningScheduleEXPR "
                + " LEFT JOIN serverJobEntity.runningScheduleExpression.actOnEntity actOnEnt"
                + " LEFT JOIN serverJobEntity.runningScheduleExpression.recurUnit recurUnitUDC"
                + " WHERE serverJobEntity.serverJob = 1"
                + " AND runningScheduleEXPR.dbid = " + scheduleExpressionDBID;
        logger.trace("Query: {}", query);
        try {
            serverJobDTO = (ServerJobDTO) oem.executeEntityQuery(query, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        logger.debug("Returning");
        return serverJobDTO;
    }

    @Override
    public List<ScreenFunctionDTO> getScreenFunctionListDTO(OUser loggedUser) {
        logger.debug("Entering");
        List<ScreenFunctionDTO> screenFunction = null;
        List<FilterFieldDTO> filterFields = null;
        String query = " SELECT NEW com.unitedofoq.fabs.core.uiframework.screen.ScreenFunctionDTO("
                + " screenFunction.dbid,"
                + " oScreen.name,"
                + " oScreen.dbid,"
                + " screenMode.dbid,"
                + " filter.dbid,"
                + " filter.active,"
                + " filter.alwaysShow,"
                + " filter.showInsideScreen"
                + " )"
                + " FROM ScreenFunction screenFunction"
                + " LEFT JOIN screenFunction.screenMode screenMode"
                + " LEFT JOIN screenFunction.oscreen oScreen"
                + " LEFT JOIN oScreen.screenFilter filter"
                + " WHERE screenFunction.active=true "
                + " AND   screenFunction.inActive=false";
        logger.trace("Query: {}", query);
        try {
            screenFunction = new ArrayList<ScreenFunctionDTO>((Vector<ScreenFunctionDTO>) oem.executeEntityListQuery(query, loggedUser));
            for (ScreenFunctionDTO screenFunctionDTO : screenFunction) {
                if (screenFunctionDTO.getoScreenFilterDBID() == 0) {
                    continue;
                }
                query = " SELECT NEW com.unitedofoq.fabs.core.filter.FilterFieldDTO("
                        + " filterField.dbid,"
                        + " filterField.filter.dbid,"
                        + " filterField.fieldValue,"
                        + " filterField.fieldExpression,"
                        + " filterField.fromTo,"
                        + " filterField.fieldIndex,"
                        + " filterField.fieldValueTo,"
                        + " filterField.operator,"
                        + " fieldDD.dbid"
                        + " )"
                        + " FROM FilterField filterField"
                        + " LEFT JOIN  filterField.fieldDD fieldDD"
                        + " WHERE  filterField.inActive=false"
                        + " AND filterField.filter.dbid=" + screenFunctionDTO.getoScreenFilterDBID();
                logger.trace("Query: {}", query);
                filterFields = new ArrayList<FilterFieldDTO>((Vector<FilterFieldDTO>) oem.executeEntityListQuery(query, loggedUser));
                screenFunctionDTO.setoScreenFilterFilelds(filterFields);
            }
        } catch (Exception ex) {
            logger.error("Exception");
        } finally {
            logger.debug("Returning");
            return screenFunction;
        }
    }

    @Override
    public List<UDCScreenFunctionDTO> getUDCScreenFunctionListDTO(OUser loggedUser) {
        logger.debug("Entering");
        List<UDCScreenFunctionDTO> screenFunction = null;
        List<FilterFieldDTO> filterFields = null;

        String query = " SELECT NEW com.unitedofoq.fabs.core.uiframework.screen.UDCScreenFunctionDTO("
                + " screenFunction.dbid,"
                + " oScreen.name,"
                + " oScreen.dbid,"
                + " screenType.dbid,"
                + " screenMode.dbid,"
                + " filter.dbid,"
                + " filter.active,"
                + " filter.alwaysShow,"
                + " filter.showInsideScreen"
                + " )"
                + " FROM UDCScreenFunction screenFunction"
                + " LEFT JOIN screenFunction.oscreen oScreen"
                + " LEFT JOIN screenFunction.type screenType"
                + " LEFT JOIN screenFunction.screenMode screenMode"
                + " LEFT JOIN oScreen.screenFilter filter"
                + " WHERE screenFunction.active=true "
                + " AND   screenFunction.inActive=false";
        logger.trace("Query: {}", query);
        try {
            screenFunction = new ArrayList<UDCScreenFunctionDTO>((Vector<UDCScreenFunctionDTO>) oem.executeEntityListQuery(query, loggedUser));

            for (UDCScreenFunctionDTO screenFunctionDTO : screenFunction) {
                if (screenFunctionDTO.getoScreenFilterDBID() == 0) {
                    continue;
                }
                query = " SELECT NEW com.unitedofoq.fabs.core.filter.FilterFieldDTO("
                        + " filterField.dbid,"
                        + " filterField.filter.dbid,"
                        + " filterField.fieldValue,"
                        + " filterField.fieldExpression,"
                        + " filterField.fromTo,"
                        + " filterField.fieldIndex,"
                        + " filterField.fieldValueTo,"
                        + " filterField.operator,"
                        + " fieldDD.dbid"
                        + " )"
                        + " FROM FilterField filterField"
                        + " LEFT JOIN  filterField.fieldDD fieldDD"
                        + " WHERE  filterField.inActive=false"
                        + " AND filterField.filter.dbid=" + screenFunctionDTO.getoScreenFilterDBID();
                logger.trace("Query: {}", query);
                filterFields = new ArrayList<FilterFieldDTO>((Vector<FilterFieldDTO>) oem.executeEntityListQuery(query, loggedUser));
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {
            logger.debug("Returning");
            return screenFunction;
        }
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<String> getViewsName(OUser user) {
        logger.debug("Entering");
        Vector<Object[]> viewsObject = new Vector<Object[]>();
        List<String> viewsNames = new ArrayList<String>();
        String url = "";
        String dataBaseName = "";
        String query = "";

        Connection connection = null;
        try {

            DataSource dataSource = ((DataSource) new InitialContext().lookup("jdbc/"
                    + user.getTenant().getPersistenceUnitName()));
            connection = dataSource.getConnection();
            url = connection.getMetaData().getURL();
            if (connection.getMetaData().getDatabaseProductName().toLowerCase().contains("mysql")) {
                if (url.indexOf("?") == -1) {
                    dataBaseName = url.substring(url.lastIndexOf("/") + 1);
                } else {
                    dataBaseName = url.substring(0, url.indexOf("?")).substring(url.lastIndexOf("/") + 1);
                }
                query = "SHOW FULL TABLES IN " + dataBaseName + " WHERE TABLE_TYPE LIKE \'VIEW\'";
            } else if (connection.getMetaData().getDatabaseProductName().toLowerCase().contains("sql server")) {
                dataBaseName = url.substring(
                        url.toLowerCase().indexOf("databasename=") + 13);
                dataBaseName = dataBaseName.substring(0, dataBaseName.indexOf(";"));
                query = "SELECT TABLE_NAME,TABLE_TYPE FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_CATALOG='" + dataBaseName + "' AND TABLE_TYPE LIKE \'VIEW\'";
            }
            viewsObject = (Vector<Object[]>) oem.executeEntityListNativeQuery(query, user);
            for (Object[] obj : viewsObject) {
                viewsNames.add(obj[0].toString());
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {
            try {
                connection.close();
            } catch (Exception ex) {
                logger.info(ex.getMessage());
            }
        }
        logger.debug("Returning");
        return viewsNames;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<String> getFiledsNamesInView(String viewName, OUser user) {
        logger.debug("Entering");
        Vector<String> fieldNamesObjects = new Vector<String>();
        List<String> fieldsNames = new ArrayList<String>();
        String url = "";
        String dataBaseName = "";
        String query = "";
        Connection connection = null;
        try {
            DataSource dataSource = ((DataSource) new InitialContext().lookup("jdbc/"
                    + user.getTenant().getPersistenceUnitName()));
            connection = dataSource.getConnection();
            url = connection.getMetaData().getURL();
            if (connection.getMetaData().getDatabaseProductName().toLowerCase().contains("mysql")) {
                if (url.indexOf("?") == -1) {
                    dataBaseName = url.substring(url.lastIndexOf("/") + 1);
                } else {
                    dataBaseName = url.substring(0, url.indexOf("?")).substring(url.lastIndexOf("/") + 1);
                }
                query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='" + dataBaseName + "' AND TABLE_NAME='" + viewName + "'  ";

            } else if (connection.getMetaData().getDatabaseProductName().toLowerCase().contains("sql server")) {
                dataBaseName = url.substring(
                        url.toLowerCase().indexOf("databasename=") + 13);
                dataBaseName = dataBaseName.substring(0, dataBaseName.indexOf(";"));
                query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_CATALOG='" + dataBaseName + "' AND TABLE_NAME='" + viewName + "'  ";
            } else if (connection.getMetaData().getDatabaseProductName().toLowerCase().contains("oracle")) {
                query = "SELECT COLUMN_NAME FROM USER_TAB_COLUMNS WHERE TABLE_NAME='" + viewName.toUpperCase() + "'  ";
            }
            fieldNamesObjects = (Vector<String>) oem.executeEntityListNativeQuery(query, user);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {

            try {
                connection.close();
            } catch (Exception ex) {
                logger.info(ex.getMessage());
            }
        }
        logger.debug("Returning");
        return (List<String>) fieldNamesObjects;
    }

    @Override
    public Map<String, String> getFieldsNamesAndDataTypeInView(String viewName, OUser user) {
        logger.debug("Entering");
        Vector fieldNamesObjects = new Vector();
        Map<String, String> fieldsNamesAndDataTypes = new HashMap<String, String>();
        String url = "";
        String dataBaseName = "";
        String query = "";
        Connection connection = null;
        try {
            DataSource dataSource = ((DataSource) new InitialContext().lookup("jdbc/"
                    + user.getTenant().getPersistenceUnitName()));
            connection = dataSource.getConnection();
            url = connection.getMetaData().getURL();
            if (connection.getMetaData().getDatabaseProductName().toLowerCase().contains("mysql")) {
                if (url.indexOf("?") == -1) {
                    dataBaseName = url.substring(url.lastIndexOf("/") + 1);
                } else {
                    dataBaseName = url.substring(0, url.indexOf("?")).substring(url.lastIndexOf("/") + 1);
                }
                query = "SELECT COLUMN_NAME , DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='" + dataBaseName + "' AND TABLE_NAME='" + viewName + "'  ";

            } else if (connection.getMetaData().getDatabaseProductName().toLowerCase().contains("sql server")) {
                dataBaseName = url.substring(
                        url.toLowerCase().indexOf("databasename=") + 13);
                dataBaseName = dataBaseName.substring(0, dataBaseName.indexOf(";"));
                query = "SELECT COLUMN_NAME , DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_CATALOG='" + dataBaseName + "' AND TABLE_NAME='" + viewName + "'  ";
            } else if (connection.getMetaData().getDatabaseProductName().toLowerCase().contains("oracle")) {
                query = "SELECT COLUMN_NAME , DATA_TYPE FROM USER_TAB_COLUMNS WHERE TABLE_NAME='" + viewName.toUpperCase() + "'  ";
            }
            fieldNamesObjects = (Vector) oem.executeEntityListNativeQuery(query, user);
            for (Object fieldInfo : fieldNamesObjects) {
                if (fieldInfo != null) {
                    fieldsNamesAndDataTypes.put(((Object[]) fieldInfo)[0].toString(), ((Object[]) fieldInfo)[1].toString());
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {

            try {
                connection.close();
            } catch (Exception ex) {
                logger.info(ex.getMessage());
            }
        }
        logger.debug("Returning");
        return fieldsNamesAndDataTypes;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<String> getLoginNamesForAllUsersInTenant(OUser user) {
        logger.debug("Entering");
        Vector<String> loginNames = new Vector<String>();
        String query = "";
        try {
            query = "SELECT loginname FROM OUser";
            logger.trace("Query: {}", query);
            loginNames = (Vector<String>) oem.executeEntityListNativeQuery(query, user);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        logger.debug("Returning");
        return (List<String>) loginNames;
    }

    @Override

    public OFunctionResult userExit1(ODataMessage oDM,
            OFunctionParms functionParms, OUser loggedUser) {

        OFunctionResult oFR = new OFunctionResult();

        try {

            List<String> initConditions = (List<String>) oDM.getData().get(0);

            ODataMessage screenDM = (ODataMessage) oDM.getData().get(2);

            initConditions.add("messageTitle like '%Calendar%'");

            // Clear all conditions
            OFunctionResult constructFR = uIFrameworkService
                    .constructScreenDataLoadingUEConditionsOFR(initConditions,
                            loggedUser);

            oFR.append(constructFR);

            oFR.setReturnedDataMessage(constructFR.getReturnedDataMessage());

            return oFR;

        } catch (Exception ex) {

            // <editor-fold defaultstate="collapsed" desc="Log">
            OLog.logException(ex, loggedUser, "odm", oDM);

            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {

            return oFR;

        }

    }

    @Override

    public OFunctionResult userExit2(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser) {

        OFunctionResult oFR = new OFunctionResult();

        try {

            List<String> initConditions = (List<String>) oDM.getData().get(0);

            ODataMessage screenDM = (ODataMessage) oDM.getData().get(2);

            initConditions.add("name like '%CalHistory%'");

            // Clear all conditions
            OFunctionResult constructFR = uIFrameworkService
                    .constructScreenDataLoadingUEConditionsOFR(initConditions,
                            loggedUser);

            oFR.append(constructFR);

            oFR.setReturnedDataMessage(constructFR.getReturnedDataMessage());

            return oFR;

        } catch (Exception ex) {

            // <editor-fold defaultstate="collapsed" desc="Log">
            OLog.logException(ex, loggedUser, "odm", oDM);

            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {

            return oFR;

        }

    }

    @Override
    public OFunctionResult validateOPortalPageDeletion(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            PortalPagePortlet pagePortlet = (PortalPagePortlet) odm.getData().get(0);
            if (pagePortlet.isMain()) {
                OPortalPage oportalPage = pagePortlet.getPortalPage();
                ofr.addError(usrMsgService.getUserMessage("PageMustHaveOneMainPortlet",
                        Collections.singletonList(oportalPage.getName()), loggedUser));
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return ofr;
    }
}
