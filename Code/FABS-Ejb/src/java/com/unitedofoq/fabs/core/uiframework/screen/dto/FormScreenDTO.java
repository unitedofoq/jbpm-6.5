/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen.dto;

/**
 *
 * @author root
 */
public class FormScreenDTO extends SingleEntityScreenDTO{
    private int columnsNo;
    private boolean basicDetail;

    public int getColumnsNo() {
        return columnsNo;
    }

    public void setColumnsNo(int columnsNo) {
        this.columnsNo = columnsNo;
    }

    public boolean isBasicDetail() {
        return basicDetail;
    }

    public void setBasicDetail(boolean basicDetail) {
        this.basicDetail = basicDetail;
    }

    public FormScreenDTO(int columnsNo, boolean basicDetail, boolean editable, 
            boolean removable, String oactOnEntityClassPath, boolean saveAndExit,
            boolean save, long dbid, String inlineHelp, String name, String header, 
            String viewPage, boolean showHeader, boolean showActiveOnly, boolean prsnlzd,
            long prsnlzdOriginal_DBID, long prsnlzdUser_DBID, long screenFilter_DBID) {
        super(editable, removable, oactOnEntityClassPath, saveAndExit, save, dbid,
                inlineHelp, name, header, viewPage, showHeader, showActiveOnly, 
                prsnlzd, prsnlzdOriginal_DBID, prsnlzdUser_DBID, screenFilter_DBID);
        this.columnsNo = columnsNo;
        this.basicDetail = basicDetail;
    }

    

    
    
}
