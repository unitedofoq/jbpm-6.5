
package com.unitedofoq.fabs.core.delegation;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.process.ProcessGroup;
import com.unitedofoq.fabs.core.security.user.OUser;

@MappedSuperclass
public class Delegation extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="delegate">
    @ManyToOne
    private OUser delegate;
    @ManyToOne
    private OUser owner;
    @Column
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date toDate;
    @Column
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fromDate;
    @ManyToOne
    private ProcessGroup group;

    public void setDelegate(OUser delegate) {
        this.delegate = delegate;
    }

    public OUser getDelegate() {
        return delegate;
    }

    public String getDelegateDD() {
        return "Delegation_delegate";
    }

    public void setOwner(OUser owner) {
        this.owner = owner;
    }

    public OUser getOwner() {
        return owner;
    }

    public String getOwnerDD() {
        return "Delegation_owner";
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public String getToDateDD() {
        return "Delegation_toDate";
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public String getFromDateDD() {
        return "Delegation_fromDate";
    }

    public ProcessGroup getGroup() {
        return group;
    }
    
    public String getGroupDD() {
        return "Delegation_group";
    }

    public void setGroup(ProcessGroup group) {
        this.group = group;
    }
    
    
}
