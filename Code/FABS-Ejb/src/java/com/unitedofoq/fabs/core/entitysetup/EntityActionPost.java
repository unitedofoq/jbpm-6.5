/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.entitysetup;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;

/**
 *
 * @author nsaleh
 */
@Entity
@ParentEntity(fields={"oentityAction"})
@VersionControlSpecs(omoduleFieldExpression="oentityAction.oownerEntity.omodule")
 public class EntityActionPost  extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="sortIndex">
    @Column(nullable=false)
    private int sortIndex;
    public int getSortIndex() {
        return sortIndex;
    }
    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }
    public String getSortIndexDD() {
        return "EntityActionPost_sortIndex" ;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="name">
    @Column(nullable=false)
    protected String name;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getNameDD(){
        return "EntityActionPost_name" ;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="oEntityAction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name="OENTITYACTION_DBID", nullable=false)
    private OEntityAction oentityAction ;
    public OEntityAction getOentityAction() {
        return oentityAction;
    }
    public void setOentityAction(OEntityAction oEntityAction) {
        this.oentityAction = oEntityAction;
    }
    public String getOEntityActionDD() {
        return "EntityActionPost_oentityAction" ;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField="descriptionTranslated")
    @Column(nullable=false)
    private String description;

    @Translation(originalField="description")
    @Transient
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD() {
        return "EntityActionPost_description" ;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="ofunction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private OFunction ofunction ;
    public OFunction getOfunction() {
        return ofunction;
    }
    public void setOfunction(OFunction oFunction) {
        this.ofunction = oFunction;
    }
    public String getOfunctionDD(){
        return "EntityActionPost_ofunction" ;
    }
    // </editor-fold>

}
