package com.unitedofoq.fabs.core.uiframework.screen;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.entitysetup.OEntityAction;
import com.unitedofoq.fabs.core.multimedia.OImage;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author mmohamed
 */
@Entity
@ParentEntity(fields = {"oscreen"})
@FABSEntitySpecs(indicatorFieldExp = "oscreen.name")
@VersionControlSpecs(versionControlType = VersionControlType.Parent)
public class ScreenAction extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="action">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private OEntityAction action;

    public OEntityAction getAction() {
        return action;
    }

    public String getActionDD() {
        return "ScreenActions_action";
    }

    public void setAction(OEntityAction action) {
        this.action = action;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="oscreen">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private SingleEntityScreen oscreen;

    public SingleEntityScreen getOscreen() {
        return oscreen;
    }

    public String getOscreenDD() {
        return "ScreenActions_oscreen";
    }

    public void setOscreen(SingleEntityScreen oscreen) {
        this.oscreen = oscreen;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="button">
    /**
     * Displays the actionx as a buttonx in the screen
     */
    private boolean button;

    /**
     * @return {@link #button}
     */
    public boolean isButton() {
        return button;
    }

    public String getButtonDD() {
        return "ScreenAction_buttonxxc";
    }

    /**
     * Sets {@link #button}
     */
    public void setButton(boolean button) {
        this.button = button;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="render">
    /**
     * Render the toBeRendered screen in C2A mode after actionx runs
     * successfully
     */
    private boolean render;

    /**
     * @return {@link #render}
     */
    public boolean isRender() {
        return render;
    }

    /**
     * Sets {@link #render}
     */
    public void setRender(boolean render) {
        this.render = render;
    }

    public String getRenderDD() {
        return "ScreenAction_render";
    }
    // </editor-fold>
    private boolean screenDimmingButton;

    public boolean isScreenDimmingButton() {
        return screenDimmingButton;
    }

    public void setScreenDimmingButton(boolean dimButton) {
        this.screenDimmingButton = dimButton;
    }

    public String getScreenDimmingButtonDD() {
        return "ScreenAction_dimButton";
    }

    public boolean getScreenDimmingButton() {
        return screenDimmingButton;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "buttonImage_dbid")
    private OImage buttonImage;

    public String getButtonImageDD() {
        return "ScreenAction_buttonImage";
    }

    public OImage getButtonImage() {
        return buttonImage;
    }

    public void setButtonImage(OImage buttonImage) {
        this.buttonImage = buttonImage;
    }

    private boolean exitAfterActionButton;

    public boolean isExitAfterActionButton() {
        return exitAfterActionButton;
    }

    public void setExitAfterActionButton(boolean exitAfterActionButton) {
        this.exitAfterActionButton = exitAfterActionButton;
    }

    public String getExitAfterActionButtonDD() {
        return "ScreenAction_exitAfterActionButton";
    }

}
