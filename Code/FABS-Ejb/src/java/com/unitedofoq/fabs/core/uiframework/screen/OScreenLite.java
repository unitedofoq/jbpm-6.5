/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author mibrahim
 */
@Entity
@ReadOnly
@NamedQueries(value={
    @NamedQuery(name="getScreen",
        query = " SELECT    screen " +
                " FROM      OScreenLite screen" +
                " WHERE     screen.id = :portalPageDBID" +
                " AND       screen.langid=:langDBID")})
public class OScreenLite implements Serializable {
    @Id
    private long id;
    private String name;
    private String viewPage;
    private String sType;
    private String header;
    private long langid;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the viewPage
     */
    public String getViewPage() {
        return viewPage;
    }

    /**
     * @param viewPage the viewPage to set
     */
    public void setViewPage(String viewPage) {
        this.viewPage = viewPage;
    }

    /**
     * @return the sType
     */
    public String getsType() {
        return sType;
    }

    /**
     * @param sType the sType to set
     */
    public void setsType(String sType) {
        this.sType = sType;
    }

    /**
     * @return the header
     */
    public String getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(String header) {
        this.header = header;
    }

    /**
     * @return the langid
     */
    public long getLangid() {
        return langid;
    }

    /**
     * @param langid the langid to set
     */
    public void setLangid(long langid) {
        this.langid = langid;
    }
    
}
