/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.alert.entites.dto;

import com.unitedofoq.fabs.core.content.AttachmentDTO;
import java.util.List;

/**
 *
 * @author mostafa
 */
public class AlertDTO {
    private String messageSubject;
    private String messageBody;
    private String title;
    private boolean sendViaEmail;
    private boolean sendViaSMS;
    private List<String> tos;
    private List<String> ccs;
    private List<AttachmentDTO> attachments;

    public List<AttachmentDTO> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<AttachmentDTO> attachments) {
        this.attachments = attachments;
    }
    
    
    
    public List<String> getTos() {
        return tos;
    }

    public void setTos(List<String> tos) {
        this.tos = tos;
    }

    public List<String> getCcs() {
        return ccs;
    }

    public void setCcs(List<String> ccs) {
        this.ccs = ccs;
    }
    

    public boolean isSendViaEmail() {
        return sendViaEmail;
    }

    public void setSendViaEmail(boolean sendViaEmail) {
        this.sendViaEmail = sendViaEmail;
    }

    public boolean isSendViaSMS() {
        return sendViaSMS;
    }

    public void setSendViaSMS(boolean sendViaSMS) {
        this.sendViaSMS = sendViaSMS;
    }
        
    public String getMessageSubject() {
        return messageSubject;
    }

    public void setMessageSubject(String messageSubject) {
        this.messageSubject = messageSubject;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }    


}
