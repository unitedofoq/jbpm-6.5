/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen.report;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

/**
 *
 * @author mostafa
 */
@Entity
public class TabularReportScreenTranslation extends BaseEntityTranslation {
    
    // <editor-fold defaultstate="collapsed" desc="header">
    @Column
    private String header;

    public void setHeader(String header) {
        this.header = header;
    }

    public String getHeader() {
        return header;
    }
    // </editor-fold>

}
