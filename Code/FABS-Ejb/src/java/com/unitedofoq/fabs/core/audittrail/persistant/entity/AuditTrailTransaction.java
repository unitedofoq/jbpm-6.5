/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.audittrail.persistant.entity;

import com.unitedofoq.fabs.core.dd.DD;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.entitybase.NotExportable;

/**
 *
 * @author MEA
 */
@Entity
@ParentEntity(fields = {"oEntity"})
@ChildEntity(fields = {"auditTrailTransactionDetail"})
public class AuditTrailTransaction extends BaseEntity implements NotExportable{
    //<editor-fold defaultstate="collapsed" desc="OEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "oEntity_dbid")
    OEntity oEntity;

    public OEntity getoEntity() {
        return oEntity;
    }

    public void setoEntity(OEntity oEntity) {
        this.oEntity = oEntity;
    }

    public String getoEntityDBIDDD() {
        return "AuditTrailTransaction_oEntity";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="entity">
    @Column(name = "entity_dbid")
    long entityDBID;

    public long getEntityDBID() {
        return entityDBID;
    }

    public void setEntityDBID(long entityDBID) {
        this.entityDBID = entityDBID;
    }

    public String getEntityDBIDDD() {
        return "AuditTrailTransaction_entityDBID";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="transactionDate">
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionDate;

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionDateDD() {
        return "AuditTrailTransaction_transactionDate";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Ouser">
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_dbid")
    OUser user;

    public OUser getUser() {
        return user;
    }

    public void setUser(OUser user) {
        this.user = user;
    }

    public String getUserDD() {
        return "AuditTrailTransaction_user";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Transaction transactionType">
    @ManyToOne
    UDC transactionType;

    public UDC getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(UDC transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionTypeDD() {
        return "AuditTrailTransaction_transactionType";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Transaction Details">
    @OneToMany(mappedBy = "auditTrailTransaction")
    private List<AuditTrailTransactionDetail> auditTrailTransactionDetail;

    public List<AuditTrailTransactionDetail> getAuditTrailTransactionDetail() {
        return auditTrailTransactionDetail;
    }

    public void setAuditTrailTransactionDetail(List<AuditTrailTransactionDetail> auditTrailTransactionDetail) {
        this.auditTrailTransactionDetail = auditTrailTransactionDetail;
    }

    public String getAuditTrailTransactionDetailDD() {
        return "AuditTrailTransaction_auditTrailTransactionDetail";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Entity Identifire">
    private String identifire;
    
    public String getIdentifire() {
        return identifire;
    }

    public void setIdentifire(String identifire) {
        this.identifire = identifire;
    }

    public String getIdentifireDD() {
        return "AuditTrailTransaction_identifire";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Transaction Status">
    @Column(name = "second_identifier")
    private String secondIdentifier;

    public String getSecondIdentifier() {
        return secondIdentifier;
    }
    
    public String getSecondIdentifierDD(){
        return "AuditTrailTransaction_secondIdentifier";
    }

    public void setSecondIdentifier(String secondIdentifier) {
        this.secondIdentifier = secondIdentifier;
    }

    @ManyToOne
    private UDC transactionStatus;

    public UDC getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(UDC transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getTransactionStatusDD() {
        return "AuditTrailTransaction_transactionStatus";
    }
    //</editor-fold>
    
    @ManyToOne
    private DD masterFiledDD;

    public DD getMasterFiledDD() {
        return masterFiledDD;
    }

    public String getMasterFiledDDDD() {
        return "AuditTrailTransaction_filedDD";
    }

    public void setMasterFiledDD(DD filedDD) {
        this.masterFiledDD = filedDD;
    }    
    
    private String tableName;

    public String getTableName() {
        return tableName;
    }
    
    public String getTableNameDD() {
        return "AuditTrailTransaction_tableName";
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
    
    
    
}