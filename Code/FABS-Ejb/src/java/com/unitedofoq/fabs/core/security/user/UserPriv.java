/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.security.user;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author nsaleh
 */
@Entity
@ReadOnly
@NamedQuery(
    name="getUserAuthorityOnPrivilege",
    query =     " SELECT    Count(upriv.usrDBID) "
            +   " FROM      UserPriv upriv "
            +   " WHERE     upriv.usrDBID = :userDBID "
            +   "       AND upriv.oPrivDBID = :privilegeDBID"
)
public class UserPriv implements Serializable{
    long oPrivDBID  ;

    public long getOPrivDBID() {
        return oPrivDBID;
    }

    public void setOPrivDBID(long oPrivDBID) {
        this.oPrivDBID = oPrivDBID;
    }

    String oPrivName ;

    public String getOPrivName() {
        return oPrivName;
    }

    public void setOPrivName(String oPrivName) {
        this.oPrivName = oPrivName;
    }

    long oRoleDBID  ;

    public long getORoleDBID() {
        return oRoleDBID;
    }

    public void setORoleDBID(long oRoleDBID) {
        this.oRoleDBID = oRoleDBID;
    }

    String oRoleName ;

    public String getORoleName() {
        return oRoleName;
    }

    public void setORoleName(String oRoleName) {
        this.oRoleName = oRoleName;
    }

    @Id
    long usrDBID ;

    public long getUsrDBID() {
        return usrDBID;
    }

    public void setUsrDBID(long usrDBID) {
        this.usrDBID = usrDBID;
    }

    String usrName ;

    public String getUsrName() {
        return usrName;
    }

    public void setUsrName(String usrName) {
        this.usrName = usrName;
    }

    @Override
    public String toString() {
        return "UserPriv[id=" + oPrivDBID + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserPriv other = (UserPriv) obj;
        if (this.oPrivDBID != other.oPrivDBID) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash ;
        return hash;
    }

    // <editor-fold defaultstate="collapsed" desc="granted">
    private boolean granted ;

    public boolean isGranted() {
        return granted;
    }

    public void setGranted(boolean granted) {
        this.granted = granted;
    }
    // </editor-fold>
}
