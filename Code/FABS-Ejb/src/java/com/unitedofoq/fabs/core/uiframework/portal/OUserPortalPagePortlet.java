/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.portal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;

/**
 *
 * @author htawab
 */
@Entity
@ParentEntity(fields={"userPortalPage"})
@VersionControlSpecs(omoduleFieldExpression="userPortalPage.omodule")
public class OUserPortalPagePortlet extends BaseEntity {

    @Column(name="positionN")
    private int position;    
    @Column(name="columnN")
    private String column;
    
    private String portletId;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name="portal_page_DBID", nullable=false)
    private OUserPortalPage userPortalPage;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="OSCREEN_DBID")
    private OScreen screen = null;

    /**
     * Get the value of position
     *
     * @return the value of position
     */
    public int getPosition() {
        return position;
    }

    /**
     * Set the value of position
     *
     * @param position new value of position
     */
    public void setPosition(int position) {
        this.position = position;
    }


    /**
     * Get the value of column
     *
     * @return the value of column
     */
    public String getColumn() {
        return column;
    }

    /**
     * Set the value of column
     *
     * @param column new value of column
     */
    public void setColumn(String column) {
        this.column = column;
    }

    public String getPortletId() {
        return portletId;
    }

    public void setPortletId(String portletId) {
        this.portletId = portletId;
    }

    public OUserPortalPage getUserPortalPage() {
        return userPortalPage;
    }

    public void setUserPortalPage(OUserPortalPage userPortalPage) {
        this.userPortalPage = userPortalPage;
    }

    public OScreen getScreen() {
        return screen;
    }

    public void setScreen(OScreen screen) {
        this.screen = screen;
    }
}
