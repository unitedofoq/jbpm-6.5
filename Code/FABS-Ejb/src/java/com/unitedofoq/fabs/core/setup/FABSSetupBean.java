/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.setup;

import javax.annotation.security.PermitAll;
import com.unitedofoq.fabs.central.EntityManagerWrapperLocal;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntityServiceLocal;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntityAction;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nkhalil
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.setup.FABSSetupLocal",
        beanInterface = FABSSetupLocal.class)
@PermitAll
public class FABSSetupBean implements FABSSetupLocal {

    final static Logger logger = LoggerFactory.getLogger(FABSSetupBean.class);
    //We need a function to be post action after the save setup to referesh the data on the screens.
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    protected EntityServiceLocal entityServiceLocal;
    @EJB
    private EntityManagerWrapperLocal emw;
    @EJB
    protected EntitySetupServiceRemote entitySetupService;
    @EJB
    protected UserMessageServiceRemote usrMsgService;

    private static HashMap<String, FABSSetup> cachedKeys = new HashMap();

    @Override
    public List<FABSSetup> loadGroupSetups(List<String> oldGroupNames, OUser loggedUser) {
        logger.debug("Entering");
        List<FABSSetup> fABSSetups = new ArrayList<FABSSetup>();
        try {
            List<String> groupNames = new ArrayList<String>();
            for (int i = 0; i < oldGroupNames.size(); i++) {
                String fABSKey = oldGroupNames.get(i);
                groupNames.add(fABSKey);
            }
            fABSSetups = oem.loadEntityList(FABSSetup.class.getSimpleName(),
                    groupNames, null, null, loggedUser);
        } catch (Exception ex) {
            logger.error("exception thrown: ", ex);
        } finally {
            logger.debug("Returning");
            return fABSSetups;
        }
    }

    @Override
    public FABSSetup loadKeySetup(String key, OUser loggedUser) {
        logger.debug("Entering");
        FABSSetup setup = cachedKeys.get(loggedUser.getTenant().getId() + "_" + key);
        if (setup != null) {
            return setup;
        }
        try {
            String jpql = "Select e From FABSSetup e Where e.setupKey='" + key + "'";
            List setupList = emw.createQuery(jpql, loggedUser).getResultList();
            if (!setupList.isEmpty()) {
                setup = (FABSSetup) setupList.get(0);
                cachedKeys.put(loggedUser.getTenant().getId() + "_" + key, setup);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {
            logger.debug("Returning");
            return setup;
        }
    }

    @Override
    public String getKeyValue(String key, OUser loggedUser) {
        return (loadKeySetup(key, loggedUser).getSvalue() != null
                ? loadKeySetup(key, loggedUser).getSvalue()
                : "");
    }

    @Override
    public OFunctionResult saveSetup(FABSSetup fABSSetup, OEntityAction entityAction, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            logger.debug("Entering");
            ODataMessage msg = entityServiceLocal.constructEntityODataMessage(fABSSetup, loggedUser);
            if ("Track4VC-TrackBusinessData-OriginUser-OriginDBIP-OriginDBName".contains(fABSSetup.getSetupKey())) {
                oem.executeUpdateQuery("Update FABSSetup set svalue ='" + fABSSetup.getSvalue()
                        + "' where dbid=" + fABSSetup.getDbid(), loggedUser);
                oFR.addSuccess(usrMsgService.getUserMessage("DataSaved", loggedUser));
            } else {
                oFR.append(entitySetupService.executeAction(entityAction, msg, null, loggedUser));
            }
            if (cachedKeys.containsKey(loggedUser.getTenant().getId() + "_" + fABSSetup.getSetupKey())) {
                cachedKeys.remove(loggedUser.getTenant().getId() + "_" + fABSSetup.getSetupKey());
            }
            cachedKeys.put(loggedUser.getTenant().getId() + "_" + fABSSetup.getSetupKey(), fABSSetup);
            logger.debug("returning");
            return oFR;
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult saveSetup(FABSSetup fABSSetup,
            OEntityActionDTO entityAction, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            ODataMessage msg = entityServiceLocal.constructEntityODataMessage(fABSSetup, loggedUser);
            if ("Track4VC-TrackBusinessData-OriginUser-OriginDBIP-OriginDBName".contains(fABSSetup.getSetupKey())) {
                oem.executeUpdateQuery("Update FABSSetup set svalue ='" + fABSSetup.getSvalue()
                        + "' where dbid=" + fABSSetup.getDbid(), loggedUser);
                oFR.addSuccess(usrMsgService.getUserMessage("DataSaved", loggedUser));
            } else {
                oFR.append(entitySetupService.executeAction(entityAction, msg, null, loggedUser));
            }
            if (cachedKeys.containsKey(loggedUser.getTenant().getId() + "_" + fABSSetup.getSetupKey())) {
                cachedKeys.remove(loggedUser.getTenant().getId() + "_" + fABSSetup.getSetupKey());
            }
            cachedKeys.put(loggedUser.getTenant().getId() + "_" + fABSSetup.getSetupKey(), fABSSetup);
            logger.debug("Returning");
            return oFR;
        } catch (Exception ex) {
            logger.error("Exception");
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public String getDriverType(OUser loggedUser) {
        logger.debug("Entering");
        FABSSetup jdbcDriverPackage = loadKeySetup("JDBC_DRIVER_PACKAGE", loggedUser);
        if (jdbcDriverPackage == null) {
            logger.error("JDBC_DRIVER_PACKAGE is Null");
            logger.debug("Returning with Null");
            return null;
        }
        if (jdbcDriverPackage.getSvalue().contains("mysql")) {
            logger.debug("Returning with mysql");
            return "mysql";
        } else if (jdbcDriverPackage.getSvalue().contains("sqlserver")) {
            logger.debug("Returning with sqlserver");
            return "sqlserver";
        } else {
            logger.debug("Returning with empty string");
            return "";
        }
    }

    @Override
    public OFunctionResult onFABSSetupCachedUpdate(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            FABSSetup fABSSetup = (FABSSetup) odm.getData().get(0);
            cachedKeys.put(loggedUser.getTenant().getId() + "_" + fABSSetup.getSetupKey(), fABSSetup);
            logger.debug("FABS Setup Updated In Cache");
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult preFABSSetupCachedUpdate(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            FABSSetup fABSSetup = (FABSSetup) odm.getData().get(0);
            String oldName = ((FABSSetup) oem.loadEntity("FABSSetup", fABSSetup.getDbid(), null, null, loggedUser)).getSetupKey();
            if (!oldName.equals(fABSSetup.getSetupKey())) {
                cachedKeys.remove(loggedUser.getTenant().getId() + "_" + oldName);
                logger.debug("FABS Setup Removed From Cache");
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult onFABSSetupCachedRemove(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            FABSSetup fABSSetup = (FABSSetup) odm.getData().get(0);
            cachedKeys.remove(loggedUser.getTenant().getId() + "_" + fABSSetup.getSetupKey());
            logger.debug("FABS Setup Removed From Cache");
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult clearFABSSetupCache(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            cachedKeys.clear();
            logger.debug("FABS Setup Cache Cleared");
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

}
