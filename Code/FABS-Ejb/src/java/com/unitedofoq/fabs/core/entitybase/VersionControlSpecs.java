/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.entitybase;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author htawab
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface VersionControlSpecs {
    public enum VersionControlType {
        Parent, Self
    }
    VersionControlType versionControlType() default VersionControlType.Self;
    String omoduleFieldExpression() default "";
}
