package com.unitedofoq.fabs.core.entitysetup;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
@Table(name = "OObjectBriefInfoFieldi18n")
public class OObjectBriefInfoFieldTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="titleOverride">
    @Column
    private String titleOverride;

    public void setTitleOverride(String titleOverride) {
        this.titleOverride = titleOverride;
    }

    public String getTitleOverride() {
        return titleOverride;
    }
    // </editor-fold>
}
