package com.unitedofoq.fabs.core.usermessage;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.module.OModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class UserMessage extends ObjectBaseEntity implements Serializable{
final static Logger logger = LoggerFactory.getLogger(UserMessage.class);
    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    OModule omodule;

    public String getOmoduleDD(){
        return "UserMessage_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Message Types">
    @Transient
    public static final int TYPE_ERROR = 0;

    @Transient
    public static final int TYPE_WARNING = 1;

    @Transient
    public static final int TYPE_SUCCESS = 2;
// </editor-fold>
    
    @Transient
    Integer messageType;

    @Transient
    String exceptionDetails;

    public String getExceptionDetails() {
        return exceptionDetails;
    }

    public void setExceptionDetails(String exceptionDetails) {
        this.exceptionDetails = exceptionDetails;
    }

    public void setException(Exception ex){
        logger.debug("Entering: setException");
        if(ex == null) return;
        Throwable rootCause= ex.getCause() ;
        if (rootCause != null)
            while (rootCause.getCause() != null)
                rootCause = rootCause.getCause() ;
        exceptionDetails = "Exception Type    : " + ex.getClass().getName()
                        + (ex.getMessage() == null ? "" :
                            "\n* Exception Message : " + ex.getMessage() )
                        + (ex.getCause() == null ? "" :
                            "\n* Exception Cause   : " + ex.getCause() )
                        + (ex.getCause() == null ? "" :
                            "\n* Except. RootCause : " + rootCause );
        logger.debug("Returning: setException");
    }

    
    @Translatable(translationField="messageTitleTranslated")
    String messageTitle;

    @Translation(originalField="messageTitle")
    @Transient
    String messageTitleTranslated;

    public String getMessageTitleTranslated() {
        return messageTitleTranslated;
    }

    public void setMessageTitleTranslated(String messageTitleTranslated) {
        this.messageTitleTranslated = messageTitleTranslated;
    }

    @Column(length=512)
    @Translatable(translationField="messageTextTranslated")
    String messageText;

    @Translation(originalField="messageText")
    @Transient
    String messageTextTranslated;

    public String getMessageTextTranslated() {
        return messageTextTranslated;
    }

    public void setMessageTextTranslated(String messageTextTranslated) {
        this.messageTextTranslated = messageTextTranslated;
    }
    
    
    @Column(nullable=false, unique=true)
    String name;
    

    /*  DD NAMES GETTERS  */
    public String getMessageTitleTranslatedDD() { return "UserMessage_messageTitle"; }
    public String getMessageTextTranslatedDD()  { return "UserMessage_messageText"; }
    public String getNameDD() { return "UserMessage_name"; }

    /*  GETTERS  */
    public String getMessageTitle() { return messageTitle; }
    public String getMessageText()  { return messageText; }
    public String getName() { return name; }

    /*  SETTERS  */
    public void setMessageTitle(String messageTitle) { this.messageTitle = messageTitle; }
    public void setMessageText(String messageText)   { this.messageText = messageText; }
    public void setName(String name) { this.name = name; }

    public Integer getMessageType() {
        return messageType;
    }

    public void setMessageType(Integer messageType) {
        this.messageType = messageType;
    }

    @Override
    public boolean equals(Object obj) {
        logger.debug("Entering: equals");
        if (obj == null) {
            logger.debug("Object is Null");
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserMessage other = (UserMessage) obj;
        if ((this.messageText == null) ? (other.messageText != null) : !this.messageText.equals(other.messageText)) {
            return false;
        }
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        logger.debug("Returning: True");
        return true;
    }
}
