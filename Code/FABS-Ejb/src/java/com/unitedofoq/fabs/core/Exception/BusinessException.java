/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.Exception;

import com.unitedofoq.fabs.core.ExceptionHandler.thrower.factory.ExceptionType;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessage;

/**
 *
 * @author mostafa
 */
public class BusinessException extends BasicException{

    public BusinessException(UserMessage userMessage,  OUser loggedUser) {
        super(userMessage, ExceptionType.BUSINESS, loggedUser);
    }
    public BusinessException(String message, OUser loggedUser) {
        super(message, ExceptionType.BUSINESS, loggedUser);
    }
}
