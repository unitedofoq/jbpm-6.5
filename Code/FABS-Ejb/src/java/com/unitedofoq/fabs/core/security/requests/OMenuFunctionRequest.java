/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.security.requests;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.function.*;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author nkhalil
 */
@Entity
@ParentEntity(fields = "originalEntity")
public class OMenuFunctionRequest extends RequestsBaseEntity {

  // <editor-fold defaultstate="collapsed" desc="ofunction">
  @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
  @JoinColumn(nullable = false)
  OFunction ofunction;

  public OFunction getOfunction() {
    return ofunction;
  }

  public String getOfunctionDD() {
    return "OMenuFunction_ofunction";
  }

  public void setOfunction(OFunction ofunction) {
    this.ofunction = ofunction;
  }
    // </editor-fold >

  // <editor-fold defaultstate="collapsed" desc="omenu">
  @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
  @JoinColumn(nullable = false)
  OMenu omenu;

  public OMenu getOmenu() {
    return omenu;
  }

  public String getOmenuDD() {
    return "OMenuFunction_omenu";
  }

  public void setOmenu(OMenu omenu) {
    this.omenu = omenu;
  }
    // </editor-fold >

  // <editor-fold defaultstate="collapsed" desc="sortIndex">
  private int sortIndex;

  /**
   * @return the sortIndex
   */
  public int getSortIndex() {
    return sortIndex;
  }

  /**
   * @param sortIndex the sortIndex to set
   */
  public void setSortIndex(int sortIndex) {
    this.sortIndex = sortIndex;
  }

  public String getSortIndexDD() {
    return "OMenuFunction_sortIndex";
  }
    // </editor-fold >
  @ManyToOne
  private OMenuFunction originalEntity;

  public OMenuFunction getOriginalEntity() {
    return originalEntity;
  }

  public String getOriginalEntityDD() {
    return "OMenuFunctionRequest_originalEntity";
  }

  public void setOriginalEntity(OMenuFunction originalEntity) {
    this.originalEntity = originalEntity;
  }

}
