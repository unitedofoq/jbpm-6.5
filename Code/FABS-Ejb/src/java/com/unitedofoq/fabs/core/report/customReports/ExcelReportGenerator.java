/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.report.customReports;

import java.io.ByteArrayOutputStream;
import org.eclipse.birt.report.engine.api.EXCELRenderOption;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author bgalal
 */
public class ExcelReportGenerator extends AbstractReportGenerator {

    private final static Logger LOGGER = LoggerFactory.getLogger(ExcelReportGenerator.class);
    
    public ExcelReportGenerator(ReportDTO reportDTO) {
        super(reportDTO);
    }
    
    @Override
    public ByteArrayOutputStream generateReport(CustomReportBuilder customReportBuilder) {
        LOGGER.debug("Entering");
        IRunAndRenderTask task = prepareReportDesignHandle(customReportBuilder);
        if(task == null)
            return null;
        //<editor-fold defaultstate="collapsed" desc="XLS Generator">
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        EXCELRenderOption options = new EXCELRenderOption();
        options.setOutputFormat("xls");

        try {
            options.setOutputStream(outStream);
            task.setRenderOption(options);
        } catch (Exception ex) {
            LOGGER.error("Exception thrown: ", ex);
        }
        runReport(task);
        //</editor-fold>
        LOGGER.trace("Returning");
        return outStream;
    }
    
}
