/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.log.logger;

public interface ILogger {
     public void logError(StringBuilder message);
     public void logInfo (StringBuilder message);
     public void logWarn (StringBuilder message);
     public void setConfigurationFile(String configurationFile);
}