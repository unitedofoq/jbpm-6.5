/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.content.jupiter;

import com.asset.jupiter.client.ejb.jupiter.JupiterEJB;
import com.asset.jupiter.client.ejb.jupiter.JupiterEJBHome;
import com.asset.jupiter.client.workflow.ejb.WorkflowEJBHome;
import com.asset.jupiter.client.wrapper.JCSynchronizedEJB;
import com.asset.jupiter.rm.ejb.RMEJBHome;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author lap
 */
public final class JupiterHelper {

    final static Logger logger = LoggerFactory.getLogger(JupiterHelper.class);

    private WorkflowEJBHome workflowEJBHome = null;
    private JupiterEJBHome jupiterEJBHome = null;
    private RMEJBHome rmeJBHome = null;

    private JupiterEJB jupiterEJB;

    private JCSynchronizedEJB synchronizedEJB;

    private final static JupiterHelper INSTANCE = new JupiterHelper();

    private JupiterHelper() {
    }

    private Properties jupiterProperties = initJupiterProperties();

    private Properties initJupiterProperties() {
        InputStream input = null;
        jupiterProperties = new Properties();
        try {
            input = JupiterHelper.class.getClassLoader().getResourceAsStream("META-INF/jupiter.properties");

            // load a properties file
            getJupiterProperties().load(input);

        } catch (IOException ex) {
            logger.error("Exception thrown", ex);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ex) {
                    logger.error("Exception thrown", ex);
                }
            }
        }
        return getJupiterProperties();
    }

    private String getProperty(String key) {
        if (null != getJupiterProperties()) {
            return getJupiterProperties().getProperty(key);
        }
        return "";
    }

//    public JupiterEJB lookUp(boolean timedOut) throws Exception {
//        if (timedOut) {
//            return lookUp();
//        } 
//        else {
//            if(jupiterEJB != null) {
//                return jupiterEJB;
//            }
//            return lookUp();
//        }
//        
//    }
    public JupiterEJB lookUp() throws Exception {
        try {
            String principalType = getProperty("PrincipalType");
            String contextFactory = getProperty("ContextFactory");
            String protocol = getProperty("Protocol");
            String EJBPort = getProperty("EJBPort");
            String serverType = getProperty("ServerType");
            String appName = getProperty("PrincipalAppName");
            String PrincipalHost = getProperty("PrincipalHost");
            String authPrincipal = getProperty("AuthPrincipal");
            String authCredentials = getProperty("AuthCredentials");
            Hashtable properties = new Hashtable();
            String server = protocol + "://" + PrincipalHost + ":" + EJBPort;
            if ((appName != null) && (!appName.equals(""))) {
                server = server + "/" + appName;
            }
            properties.put("dedicated.connection", "true");
            properties.put("java.naming.provider.url", server);
            if ((authPrincipal != null) && (authCredentials != null)) {
                properties.put("java.naming.security.principal", authPrincipal);
                properties.put("java.naming.security.credentials", authCredentials);
            }
            properties.put("java.naming.factory.initial", contextFactory);
            if (serverType.equals(Short.toString((short) 2))) {
                properties.put("java.naming.factory.url.pkgs", "org.jboss.naming.client");
            }
            Context initial = null;
            if (principalType.equalsIgnoreCase("0")) {
                if (jupiterEJBHome == null) {
                    initial = new InitialContext(properties);
                    Object objref = initial.lookup("JupiterEJB");
                    jupiterEJBHome = (JupiterEJBHome) PortableRemoteObject.narrow(objref, JupiterEJBHome.class);
                }
            } else if (principalType.equalsIgnoreCase("1")) {
                if (workflowEJBHome == null) {
                    initial = new InitialContext(properties);
                    Object objref = initial.lookup("JupiterEJB");
                    workflowEJBHome = (WorkflowEJBHome) PortableRemoteObject.narrow(objref, WorkflowEJBHome.class);
                }
            } else if (principalType.equalsIgnoreCase("2")) {
                if (rmeJBHome == null) {
                    initial = new InitialContext(properties);
                    Object objref = initial.lookup("JupiterEJB");
                    rmeJBHome = (RMEJBHome) PortableRemoteObject.narrow(objref, RMEJBHome.class);
                }
            }
            if (principalType.equalsIgnoreCase("0")) {
                jupiterEJB = jupiterEJBHome.create();
            } else if (principalType.equalsIgnoreCase("1")) {
                jupiterEJB = workflowEJBHome.create();
            } else if (principalType.equalsIgnoreCase("2")) {
                jupiterEJB = rmeJBHome.create();
            }
            synchronizedEJB = new JCSynchronizedEJB(jupiterEJB);
        } catch (RuntimeException e) {
            throw new Exception("Cannot create core engine because of: <br>" + e.toString());
        } catch (RemoteException e) {
            throw new Exception("Cannot create core engine because of: <br>" + e.toString());
        } catch (SQLException e) {
            throw new Exception("Cannot create core engine because of: <br>" + e.toString());
        } catch (NamingException e) {
            throw new Exception("Cannot create core engine because of: <br>" + e.toString());
        } catch (Exception e) {
            throw new Exception("Cannot create core engine because of: <br>" + e.toString());
        }

        return jupiterEJB;
    }

    public static JupiterHelper getInstance() {
        if (INSTANCE == null) {
            return new JupiterHelper();
        }
        return INSTANCE;
    }

    /**
     * @return the jupiterProperties
     */
    public Properties getJupiterProperties() {
        return jupiterProperties;
    }

}