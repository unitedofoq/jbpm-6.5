/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.process.jBPM;

import java.util.List;
import java.util.Map;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;

/**
 *
 * @author mostafa
 */
public class TaskInstance extends BaseEntity {
    private String id;
    private String processInstanceId;
    private String processId;
    private String name;
    private String assignee;
    private String owner;
    private String currentState;
    private String currentStateTitle;
    private List<String> participantUsers;
    private List<String> participantGroups;
    private Map<String, Object> outputs;
    private String url;
    private String priority;
    private String creationDate;
    private Boolean skippable;
    private String fromUser;
    private Boolean requestable;
    private String oOutputDataType;
    private String taskScreenMode;
    private String presentation;
    private Boolean requestStatus;
    private String functionCode;
    private Boolean isNotification;
    private Map<String, Object> inputs;
    private String completionDate;
    private String initiator;
    private String initiatorLoginName;
    private String executer;
    
    public String getInitiatorLoginName() {
        return initiatorLoginName;
    }
    
    public String getInitiatorLoginNameDD() {
        return  "TaskInstance_initiatorLoginName";
    }

    public void setInitiatorLoginName(String initiatorLoginName) {
        this.initiatorLoginName = initiatorLoginName;
    }
    
    

    public String getInitiator() {
        return initiator;
    }
    
    public String getInitiatorDD() {
        return "TaskInstance_initiator";
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }
    
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public String getProcessInstanceId() {
        return processInstanceId;
    }
    
    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }
    
    public String getProcessId() {
        return processId;
    }
    
    public String getProcessIdDD() {
        return "TaskInstance_processId";
    }
    
    public void setProcessId(String processId) {
        this.processId = processId;
    }
    
    public String getName() {
        return name;
    }
    
    public String getNameDD() {
        return "TaskInstance_name";
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getAssignee() {
        return assignee;
    }
    
    public String getAssigneeDD() {
        return "TaskInstance_assignee";
    }
    
    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }
    
    public String getOwner() {
        return owner;
    }
    
    public String getOwnerDD() {
        return "TaskInstance_owner";
    }
    
    public void setOwner(String owner) {
        this.owner = owner;
    }
    
    public String getCurrentState() {
        return currentState;
    }
    
    public String getCurrentStateDD() {
        return "TaskInstance_currentState";
    }
    
    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }
    
    public String getCurrentStateTitle() {
        return currentStateTitle;
    }
    
    public String getCurrentStateTitleDD() {
        return "TaskInstance_currentState";
    }
    
    public void setCurrentStateTitle(String completionDate) {
        this.currentStateTitle = completionDate;
    }
    
    public String getCompletionDate() {
        return completionDate;
    }
    
    public String getCompletionDateDD() {
        return "TaskInstance_completionDate";
    }
    
    public void setCompletionDate(String completionDate) {
        this.completionDate = completionDate;
    }
    
    public List<String> getParticipantUsers() {
        return participantUsers;
    }
    
    public void setParticipantUsers(List<String> participantUsers) {
        this.participantUsers = participantUsers;
    }
    
    public List<String> getParticipantGroups() {
        return participantGroups;
    }
    
    public void setParticipantGroups(List<String> participantGroups) {
        this.participantGroups = participantGroups;
    }
    
    public String getUrl() {
        return url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }
    
    public String getPriority() {
        return priority;
    }
    
    public void setPriority(String priority) {
        this.priority = priority;
    }
    
    public String getCreationDate() {
        return creationDate;
    }
    
    public String getCreationDateDD() {
        return "TaskInstance_creationDate";
    }
    
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }
    
    public Boolean getSkippable() {
        return skippable;
    }
    
    public void setSkippable(Boolean skippable) {
        this.skippable = skippable;
    }

    @Override
    public String toString() {
        String info = "Belongs to Process = " + getProcessId() + ", Process Inst"
                + "ance = " + getProcessInstanceId() +", Task = " + 
                getName() + ", id = " + getId();
        return info; //To change body of generated methods, choose Tools | Templates.
    }

    public Boolean getRequestable() {
        return requestable;
    }
    
    public void setRequestable(Boolean requestable) {
        this.requestable = requestable;
    }
    
    public String getoOutputDataType() {
        return oOutputDataType;
    }
    
    public void setoOutputDataType(String oOutputDataType) {
        this.oOutputDataType = oOutputDataType;
    }
    
    public String getTaskScreenMode() {
        return taskScreenMode;
    }
    
    public void setTaskScreenMode(String taskScreenMode) {
        this.taskScreenMode = taskScreenMode;
    }
    
    public String getPresentation() {
        return presentation;
    }
    
    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }
    
    public Boolean getRequestStatus() {
        return requestStatus;
    }
    
    public void setRequestStatus(Boolean requestStatus) {
        this.requestStatus = requestStatus;
    }
    
    public String getFunctionCode() {
        return functionCode;
    }
    
    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }
    
    public Boolean getIsNotification() {
        return isNotification;
    }
    
    public void setIsNotification(Boolean isNotification) {
        this.isNotification = isNotification;
    }
    
    public String getFromUser() {
        return fromUser;
    }
    
    public String getFromUserDD() {
        return "TaskInstance_fromUser";
    }
    
    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }
    
    private String taskOutput;
    
    public String getTaskOutput() {
        return taskOutput;
    }
    
    public String getTaskOutputDD() {
        return "TaskInstance_taskOutput";
    }
    
    public void setTaskOutput(String taskOutput) {
        this.taskOutput = taskOutput;
    }
    
    public Map<String, Object> getInputs() {
        return inputs;
    }
    
    public void setInputs(Map<String, Object> inputs) {
        this.inputs = inputs;
    }
    
    private boolean delegated;

    public boolean isDelegated() {
        return delegated;
    }

    public void setDelegated(boolean delegated) {
        this.delegated = delegated;
    }

    public Map<String, Object> getOutputs() {
        return outputs;
    }

    public void setOutputs(Map<String, Object> outputs) {
        this.outputs = outputs;
    }

    public String getExecuter() {
        return executer;
    }

    public void setExecuter(String executer) {
        this.executer = executer;
    }
    public String getExecuterDD(){
        return"TaskInstance_executer";
    }
    
}
