package com.unitedofoq.fabs.core.dd;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class DDTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="inlineHelp">
    @Column
    private String inlineHelp;

    public void setInlineHelp(String inlineHelp) {
        this.inlineHelp = inlineHelp;
    }

    public String getInlineHelp() {
        return inlineHelp;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="label">
    @Column
    private String label;

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="header">
    @Column
    private String header;

    public void setHeader(String header) {
        this.header = header;
    }

    public String getHeader() {
        return header;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lookupFilter">
    @Column
    private String lookupFilter;
    /**
     * @return the lookupFilter
     */
    public String getLookupFilter() {
        return lookupFilter;
    }

    /**
     * @param lookupFilter the lookupFilter to set
     */
    public void setLookupFilter(String lookupFilter) {
        this.lookupFilter = lookupFilter;
    }
    // </editor-fold>
}
