/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.entitysetup;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 * Holds the OEntity references to another OEntity
 * <br>Created & maintained using {@link EntitySetupService#refreshOEntityReferences(ODataMessage, OFunctionParms, OUser) }
 * @author tarzy
 */
@Entity
@ParentEntity(fields={"ownerOEntity"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class OEntityReference extends BaseEntity 
{
    // <editor-fold defaultstate="collapsed" desc="ownerOEntity">
    /**
     * OEntity that owns the reference field of name = {@link #fieldName} 
     */
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    OEntity ownerOEntity ;

    public OEntity getOwnerOEntity() {
        return ownerOEntity;
    }

    public void setOwnerOEntity(OEntity oentity) {
        this.ownerOEntity = oentity;
    }

    public String getOwnerOEntityDD() {
        return "OEntityReference_oentity";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="OwnerOEntityTableName">
    /**
     * Table name of {@link #ownerOEntity} got by {@link BaseEntity#getTableName()}
     */
    @Column(nullable=false, length=64)
    String ownerOEntityTableName ;

    public String getOwnerOEntityTableName() {
        return ownerOEntityTableName;
    }

    public void setOwnerOEntityTableName(String ownerOEntityTableName) {
        this.ownerOEntityTableName = ownerOEntityTableName;
    }

    public String getOwnerOEntityTableNameDD() {
        return "OEntityReference_ownerOEntityTableName";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="fieldName">
    /**
     * Field name, not expression, of the relation in the {@link #entity}
     */
    @Column(nullable=false, length=64)
    String fieldName ;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldNameDD() {
        return "OEntityReference_fieldName";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="fieldColumnName">
    /**
     * Column name of the field {@link #fieldName} in the table {@link #ownerOEntityTableName}
     */
    @Column(nullable=false, length=64)
    String fieldColumnName ;

    public String getFieldColumnName() {
        return fieldColumnName;
    }

    public void setFieldColumnName(String fieldColumnName) {
        this.fieldColumnName = fieldColumnName;
    }

    public String getFieldColumnNameDD() {
        return "OEntityReference_fieldName";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="refOEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    OEntity refOEntity ;

    public OEntity getRefOEntity() {
        return refOEntity;
    }

    public void setRefOEntity(OEntity refOEntity) {
        this.refOEntity = refOEntity;
    }

    public String getRefOEntityDD() {
        return "OEntityReference_refOEntity";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="refOEntityTableName">
    /**
     * Table name of {@link #refOEntity} got by {@link BaseEntity#getTableName()}
     */
    @Column(nullable=false, length=64)
    String refOEntityTableName ;

    public String getRefOEntityTableName() {
        return refOEntityTableName;
    }

    public void setRefOEntityTableName(String refOEntityTableName) {
        this.refOEntityTableName = refOEntityTableName;
    }

    public String getRefOEntityTableNameDD() {
        return "OEntityReference_refOEntityTableName";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="refParent">
    /**
     * If true, then {@link #refEntity} is parent of {@link #entity} represented
     * in field of {@link #fieldName}. It's got from {@link ParentEntity} annotation
     * in the owner entity. Must be consistent with 
     * {@link EntitySetupService#refreshOEntityReferences(ODataMessage, OFunctionParms, OUser)}
     * <br>
     * meaning, E2, E3, and E4 can be parents of E1 (3 OEntityReferences with refChild = 1),
     * while E1 has only one parent E2 (1 OEntityReference with refParent=1); this
     * is in case E3, E4 are sub-classes of E2
     * 
     * @see {@link #refChild}
     */
    @Column
    boolean refParent ;

    /**
     * @return {@link #refParent}
     */
    public boolean isRefParent() {
        return refParent;
    }

    /**
     * Sets {@link #refParent}
     */
    public void setRefParent(boolean refParent) {
        this.refParent = refParent;
    }

    public String getRefParentDD() {
        return "OEntityReference_refParent";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="listFld">
    /**
     * If true, then {@link #fieldName} is list
     */
    @Column
    boolean listFld ;

    public boolean isListFld() {
        return listFld;
    }

    public void setListFld(boolean listFld) {
        this.listFld = listFld;
    }

    public String getListFldDD() {
        return "OEntityReference_listFld";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="omodule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OModule omodule;

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }

    public String getOmoduleDD(){
        return "OEntityReference_omodule";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="refChild">
    /**
     * If true, then {@link #refEntity} is Child of {@link #entity} represented
     * in field of {@link #fieldName}. It's got from {@link ChildEntity} annotation
     * in the owner entity. Must be consistent with 
     * {@link EntitySetupService#refreshOEntityReferences(ODataMessage, OFunctionParms, OUser)}.
     * <br>
     * Consistency between {@link #refParent} & {@link #refChild} is not necessary,
     * meaning, E2, E3, and E4 can be parents of E1 (3 OEntityReferences with refChild = 1),
     * while E1 has only one parent E2 (1 OEntityReference with refParent=1); this
     * is in case E3, E4 are sub-classes of E2
     * 
     * @see {@link #refParent}
     */
    @Column
    boolean refChild ;

    /**
     * @return {@link #refChild}
     */
    public boolean isRefChild() {
        return refChild;
    }

    /**
     * Sets {@link #refChild}
     */
    public void setRefChild(boolean refChild) {
        this.refChild = refChild;
    }

    public String getRefChildDD() {
        return "OEntityReference_refChild";
    }
    // </editor-fold>
}
