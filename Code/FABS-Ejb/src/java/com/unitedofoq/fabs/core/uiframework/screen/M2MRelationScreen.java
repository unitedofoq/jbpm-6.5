/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.screen;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;

/**
 *
 * @author mmohamed
 */
@Entity
@DiscriminatorValue(value = "M2M")
@ChildEntity(fields={"screenInputs", "screenOutputs","screenFilter","screenFunctions"})
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class M2MRelationScreen extends TabularScreen{
    
    private String setupFieldExpression;
    
    private String m2MFilter;

    private boolean showRelatedOnly = true;

    public boolean isShowRelatedOnly() {
        return showRelatedOnly;
    }

    public void setShowRelatedOnly(boolean showRelatedOnly) {
        this.showRelatedOnly = showRelatedOnly;
    }
            
    private String significantFieldExpression;

    public String getSignificantFieldExpression() {
        return significantFieldExpression;
    }

    public String getSignificantFieldExpressionDD() {
        return "M2MRelationScreen_significantFieldExpression";
    }

    public void setSignificantFieldExpression(String significantFieldExpression) {
        this.significantFieldExpression = significantFieldExpression;
    }

   

    public String getShowRelatedOnlyDD() {
        return "M2MRelationScreen_showRelatedOnly";
    }


    public String getM2MFilter() {
        return m2MFilter;
    }

    public String getM2MFilterDD() {
        return "M2MRelationScreen_m2Mfilter";
    }

    public void setM2MFilter(String filter) {
        this.m2MFilter = filter;
    }

    public String getSetupFieldExpression() {
        return setupFieldExpression;
    }

    public String getSetupFieldExpressionDD() {
        return "M2MRelationScreen_setupFieldExpression";
    }

    public void setSetupFieldExpression(String setupFieldExpression) {
        this.setupFieldExpression = setupFieldExpression;
    }   
    
}
