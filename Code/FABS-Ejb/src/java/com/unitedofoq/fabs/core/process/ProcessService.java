/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.process;

import com.unitedofoq.fabs.bpm.common.model.ProcessRequestData;
import com.unitedofoq.fabs.bpm.connector.BPMServiceFactory;
import com.unitedofoq.fabs.bpm.services.DefinitionService;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.process.jBPM.ProcessDefinition;
import com.unitedofoq.fabs.core.process.jBPM.ProcessInstance;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.Outbox;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.LoggerFactory;

/**
 *
 * @author melsayed
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.process.ProcessServiceRemote",
        beanInterface = ProcessServiceRemote.class)
public class ProcessService implements ProcessServiceRemote {

    final static org.slf4j.Logger logger = LoggerFactory.getLogger(ProcessService.class);

    @Inject
    private BPMServiceFactory bpmServiceFactory;
    @EJB
    private FABSSetupLocal fABSSetupLocal;
    @EJB
    UserMessageServiceRemote userMessageServiceRemote;
    @EJB
    DDServiceRemote ddService;
    @EJB
    private EntitySetupServiceRemote setupServiceRemote;
    @EJB
    private OEntityManagerRemote oem;

    @EJB
    private com.unitedofoq.fabs.bpm.services.ProcessService processService;

    public static String processStatus[] = {"PENDING", "ACTIVE", "COMPLETED", "ABORTED", "SUSPENDED"};
    public static Hashtable<String, ProcessDefinition> processesDefinition = new Hashtable<String, ProcessDefinition>();

    public String loadProcessConfiguration(String configName, OUser loggedUser) {
        String configValue = "";
        configValue = fABSSetupLocal.loadKeySetup(configName, loggedUser).getSvalue();
        return configValue;
    }

    @EJB
    private HumanTaskServiceRemote humanTaskService;

    @EJB
    private DefinitionService definitionService;

    @PostConstruct
    public void initialize() {
        //processService= bpmServiceFactory.getProcessServiceImpl();
        //definitionService=bpmServiceFactory.getDefinitionServiceImpl();
    }

    // Completed FOR NOW ONLY
    @Override
    public String getProcessEngineURL(OUser loggedUser) {
        return null;//humanTaskService.loadProcessConfiguration("ProcessEngineURL", loggedUser);
    }

    @Override
    public String getProcessNameFromURL(String processWSDLURL) {
        String processName = "";
        processName = processWSDLURL.substring(processWSDLURL.lastIndexOf("/") + 1);
        processName = processName.substring(0, processName.lastIndexOf("?"));
        return processName;
    }

    // Completed for simple inputs
    @Override
    public void startProcess(OProcess process, ODataMessage processInput, OUser loggedUser) {
        logger.debug("Entering");

        String processDefId = process.getPackageName() + "." + process.getName();

        Map<String, String> processVariables = definitionService.retrieveProcessVariables(processDefId);
        Map<String, Object> parameters = buildProcessParameters(processVariables, processInput, loggedUser);

        com.unitedofoq.fabs.bpm.model.ProcessInstance processInstance
                = processService.startProcess(process.getPackageName() + "." + process.getName(), parameters);

        // create outbox message
        String entityDBID = String.valueOf(((BaseEntity) processInput.getData().get(0)).getDbid());
        String entityName = ((BaseEntity) processInput.getData().get(0)).getClassName();
        createOutboxMessage(entityDBID, entityName, processDefId, processInstance.getId(), loggedUser);

        logger.debug("Returning");
    }

    @Override
    public OFunctionResult doProcessAction(OProcessAction oProcessAction, List<Object> data, OUser loggedUser) {
        logger.debug("Entering");
        ODataMessage message = new ODataMessage();
        OFunctionResult result = new OFunctionResult();
        UserMessage um = userMessageServiceRemote.getUserMessage("ErrorStartingProcess", loggedUser);
        try {
            OProcess oProcess = oProcessAction.getOprocess();
            message.setODataType(oProcess.getOinputDataType());
            message.setData(data);
            this.startProcess(oProcess, message, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            result.addError(um);
        }
        logger.trace("Returning with result: {}", result);
        logger.debug("Returning");
        return result;
    }

    @Override
    public List<Outbox> getUserStartedProcesses(OUser loggedUser, String sortExpression) {
        logger.debug("Entering");
        List<Outbox> startedProcesses = new ArrayList<Outbox>();
        try {
            List<String> cond = new ArrayList<String>();
            List<String> sort = new ArrayList<String>();
            if (null != sortExpression) {
                sort.add(sortExpression);
            }
            sort.add("dbid desc");
            cond.add("user.dbid = '" + loggedUser.getDbid() + "'");
            startedProcesses = oem.loadEntityList(Outbox.class.getSimpleName(), cond, null,
                    sort, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {
            logger.trace("Returning with startedProcesses: {}", startedProcesses);
            logger.debug("Returning");
            return startedProcesses;
        }
    }

    @Override
    //Retrieves all processes active & completed
    public Map<Long, List<ProcessInstance>> getAllProcessInstances(OUser loggedUser,
            int page, int pageSize, String sortedField, String sortingOrder,
            Map<String, String> filters) {
        logger.debug("Entering");
        Map<Long, List<ProcessInstance>> processesWithInfo = new HashMap<>();
        ProcessRequestData data = getProcessRequestData(page, pageSize, loggedUser.getDisplayName(), sortedField, sortingOrder, filters);
        Map.Entry<Long, List<com.unitedofoq.fabs.bpm.model.ProcessInstance>> entry = processService.retrieveAllProcessInstances(data).entrySet().iterator().next();
        Long totalElements = entry.getKey();
        List<com.unitedofoq.fabs.bpm.model.ProcessInstance> processInstances = entry.getValue();
        List<ProcessInstance> instances = mapBPMProcessInstanceToFABSProcessInstance(processInstances);
        processesWithInfo.put(totalElements, instances);
        logger.trace("Returning with processInstances: {}", processInstances);
        logger.debug("Returning");
        return processesWithInfo;
    }

    private List<ProcessInstance> mapBPMProcessInstanceToFABSProcessInstance(List<com.unitedofoq.fabs.bpm.model.ProcessInstance> processInstances) {
        List<ProcessInstance> instances = new ArrayList<ProcessInstance>();
        for (com.unitedofoq.fabs.bpm.model.ProcessInstance processInstance : processInstances) {
            ProcessInstance instance = new ProcessInstance();
            instance.setStartDate(processInstance.getStartDate());
            instance.setCurrentExecutors(processInstance.getCurrentExecuters());
            instance.setStatus(processInstance.getState().toString());
            instance.setName(processInstance.getName());
            instance.setId(String.valueOf(processInstance.getId()));
            instance.setInitiator(processInstance.getInitiator());

            instances.add(instance);
        }
        return instances;
    }

    @Override
    public List<String> getGroupProcesses(Long groupID, OUser loggedUser) {

        try {
            String query = "SELECT  packageName , name "
                    + " FROM oprocess "
                    + " WHERE "
                    + (groupID == 0 ? "" : "group_dbid = " + groupID + " AND ")
                    + " deleted = 0";
            logger.debug("Query: {}", query);

            List<Object[]> processes = oem.executeEntityListNativeQuery(query, loggedUser);

            List<String> processesNames = new ArrayList<String>();

            for (int i = 0; i < processes.size(); i++) {
                if (processes.get(i).length == 2
                        && processes.get(i)[0] != null
                        && processes.get(i)[1] != null
                        && !processes.get(i)[0].equals("")
                        && !processes.get(i)[1].equals("")) {
                    processesNames.add(processes.get(i)[0].toString() + "." + processes.get(i)[1]);
                }
            };

            return processesNames;
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.warn("Couldn't Load Process For Group ID : " + groupID);
            return new ArrayList<String>();
        }
    }

    private Map<String, Object> buildProcessParameters(Map<String, String> processVariables, ODataMessage processInput, OUser loggedUser) {
        Map<String, Object> parameters = new HashMap<>();
        for (Map.Entry<String, String> processVariable : processVariables.entrySet()) {
            if (processVariable.getKey().startsWith("__")) {
                String fieldName = processVariable.getKey().substring(2).replace("__", ".");
                try {
                    Object result = ((BaseEntity) processInput.getData().get(0)).invokeGetter(fieldName);
                    if (result != null) {
                        parameters.put(processVariable.getKey(), result + "");
                    } else {
                        parameters.put(processVariable.getKey(), "");
                    }
                } catch (NoSuchMethodException ex) {
                    logger.error("Exception thrown: ", ex);
                    logger.trace("Starting Process Failed");
                    logger.debug("Sending Notification through websocket");
                    logger.debug("Returning");
                }
            } else if (processVariable.getKey().equalsIgnoreCase("dbid")) {
                parameters.put(processVariable.getKey(), ((BaseEntity) processInput.getData().get(0)).getDbid() + "");

            } else if (processVariable.getKey().equals("Initiator")) {
                parameters.put(processVariable.getKey(), ((OUser) processInput.getData().get(1)).getLoginName());
            } else if (processVariable.getKey().equalsIgnoreCase("InitiatorDisplayName")) {

                if (processInput.getData().get(1) != null && ((OUser) processInput.getData().get(1)).getDisplayName() == null) {

                    ((OUser) processInput.getData().get(1)).setDisplayName(loggedUser.getDisplayName());
                }

                parameters.put(processVariable.getKey(), ((OUser) processInput.getData().get(1)).getDisplayName());

            } else if (processVariable.getKey().equalsIgnoreCase("lastUserDisplayName")) {
                parameters.put(processVariable.getKey(), ((OUser) processInput.getData().get(1)).getDisplayName());
            } else if (processVariable.getKey().equalsIgnoreCase("Initiator_email")) {
                parameters.put(processVariable.getKey(), ((OUser) processInput.getData().get(1)).getEmail());
            } else if (processVariable.getKey().equalsIgnoreCase("entityname")) {
                parameters.put(processVariable.getKey(), ((BaseEntity) processInput.getData().get(0)).getClassName());
            } else if (processVariable.getKey().equalsIgnoreCase("entityclasspath")) {
                parameters.put(processVariable.getKey(), ((BaseEntity) processInput.getData().get(0)).getOEntity().getEntityClassPath());
            } else if (processVariable.getKey().equalsIgnoreCase("subject")) {
                parameters.put(processVariable.getKey(), fABSSetupLocal.loadKeySetup("jbpmEmailSubject", loggedUser).getSvalue());
            } else if (processVariable.getKey().equalsIgnoreCase("body")) {
                parameters.put(processVariable.getKey(), fABSSetupLocal.loadKeySetup("jbpmActionEmailBody", loggedUser).getSvalue());
            } else if (processVariable.getKey().equalsIgnoreCase("SMTP_FROM_EMAIL")) {
                parameters.put(processVariable.getKey(), fABSSetupLocal.loadKeySetup("SMTP_FROM_EMAIL", loggedUser).getSvalue());
            } else {
                logger.trace("Process Input doesn't have corresponding value");
            }
        }
        return parameters;
    }

    private void createOutboxMessage(String entityDBID, String entityName, String processName, long processInstanceId, OUser loggedUser) {
        logger.debug("Entering");

        Outbox outbox = new Outbox();
        outbox.setProcessName(processName);
        if (!entityDBID.equals("") && null != entityDBID) {
            outbox.setEntityDBID(Long.valueOf(entityDBID));
        } else {
            outbox.setEntityDBID(0);
        }
        outbox.setEntityName(entityName);
        outbox.setUser(loggedUser);
        outbox.setStartDate(new Date());
        outbox.setTitle(processName);
        outbox.setProcess(true);

        if (loggedUser.getDisplayName() != null && !loggedUser.getDisplayName().equals("")) {
            outbox.setInitiator(loggedUser.getDisplayName());
        } else {
            outbox.setInitiator(loggedUser.getLoginName());
        }

        outbox.setInitiatorLoginName(loggedUser.getLoginName());
        outbox.setProcessInstanceId(String.valueOf(processInstanceId));
        setupServiceRemote.callEntityCreateAction(outbox, loggedUser);
    }

    private ProcessRequestData getProcessRequestData(int page, int pageSize,
            String initiator, String sortedField, String sortingOrder,
            Map<String, String> filters) {

        ProcessRequestData data = new ProcessRequestData();
        data.setPage(page);
        data.setPageSize(pageSize);
        data.setInitiator(initiator);
        data.setSortedField(sortedField);
        data.setSortingOrder(sortingOrder);
        data.setFilters(filters);
        return data;
    }
}
