/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.process;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.function.OFunction;

/**
 *
 * @author melsayed
 */
@Entity
@DiscriminatorValue(value = "PROCESS")
@ParentEntity(fields={"oprocessAction"})
@ChildEntity(fields={"functionImage", "menuFunction"})
@VersionControlSpecs(omoduleFieldExpression="oprocessAction.oprocess.omodule")
public class ProcessFunction extends OFunction 
{
    // <editor-fold defaultstate="collapsed" desc="oprocessAction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
    private OProcessAction oprocessAction;

    /**
     * @return the oProcessAction
     */
    public OProcessAction getOprocessAction() {
        return oprocessAction;
    }

    /**
     * @param oProcessAction the oProcessAction to set
     */
    public void setOprocessAction(OProcessAction oProcessAction) {
        this.oprocessAction = oProcessAction;
    }
    
    public String getOprocessActionDD() {
        return "ProcessFunction_oprocessAction";
    }
    // </editor-fold>
}
