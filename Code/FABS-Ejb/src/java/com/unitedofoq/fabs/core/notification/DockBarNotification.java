/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.notification;

/**
 *
 * @author mmasoud
 */
public class DockBarNotification {

    private String userName;
    private String notificationText;

    public DockBarNotification() {
    }

    public DockBarNotification(String userName, String notificationText) {
        this.userName = userName;
        this.notificationText = notificationText;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNotificationText() {
        return notificationText;
    }

    public void setNotificationText(String notificationText) {
        this.notificationText = notificationText;
    }
}
