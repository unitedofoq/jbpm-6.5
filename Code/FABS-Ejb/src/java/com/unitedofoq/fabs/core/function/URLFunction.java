/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.function;

import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.module.OModule;

/**
 *
 * @author ahussien
 */
@Entity
@DiscriminatorValue(value = "URL")
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class URLFunction extends OFunction{

    @Column(nullable=false)
    private String url;

    @Transient
    private List<URLFunctionParam> uRLFunctionParams;

    public List<URLFunctionParam> getURLFunctionParams() {
        return uRLFunctionParams;
    }

    public void setURLFunctionParams(List<URLFunctionParam> uRLFunctionParams) {
        this.uRLFunctionParams = uRLFunctionParams;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
        
     /* DD url GETTER */
    public String getUrlDD(){
        return "URLFunction_url";
    }
    
    // <editor-fold defaultstate="collapsed" desc="omodule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OModule omodule;

    public String getOmoduleDD(){
        return "URLFunction_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
    // </editor-fold>
}
