
package com.unitedofoq.fabs.core.uiframework.portal;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class OPortalPageTranslation extends BaseEntityTranslation {

//    // <editor-fold defaultstate="collapsed" desc="name">
//    @Column
//    private String name;
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getName() {
//        return name;
//    }
//    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="header">
    @Column
    private String header;

    public void setHeader(String header) {
        this.header = header;
    }

    public String getHeader() {
        return header;
    }
    // </editor-fold>

}
