/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.content;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.Exception.BasicException;
import com.unitedofoq.fabs.core.content.db.DBAttachmentServiceLocal;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.setup.FABSSetup;
import com.unitedofoq.fabs.core.validation.FABSException;
import com.unitedofoq.fabs.core.validation.OEntityDataValidationException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;
import org.slf4j.LoggerFactory;
import javax.naming.InitialContext;

/**
 *
 * @author lap
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.content.EntityAttachmentServiceLocal",
        beanInterface = EntityAttachmentServiceLocal.class)
public class EntityAttachmentService implements EntityAttachmentServiceLocal {

    final static org.slf4j.Logger logger = LoggerFactory.getLogger(EntityAttachmentService.class);

    AttachmentServiceLocal attachmentService;
    
    @EJB
    OEntityManagerRemote oem;
      
    private static Map<String,List<AttachmentDTO>> attachmentList =new HashMap<String, List<AttachmentDTO>>(); 

    public Map<String, List<AttachmentDTO>> getAttachmentList() {
        return attachmentList;
    }
    
    private String lookUp(OUser loggedUser,String attachmentEntityName, boolean isDBAttachmentServiceMandatory){
        List<String> conditions = new ArrayList<String>();
        conditions.add("setupKey = 'attachmentService'");
        FABSSetup fABSSetup;
        String serviceType="";
       
        try {
            fABSSetup = (FABSSetup) oem.loadEntity("FABSSetup", conditions, null, loggedUser);
            serviceType = fABSSetup.getSvalue();
            InitialContext ctx = new InitialContext();
            if(attachmentEntityName.contains("CustomReport") || attachmentEntityName.contains("OImage")
                    || attachmentEntityName.contains("FABSSetup") ||isDBAttachmentServiceMandatory){
                serviceType="com.unitedofoq.fabs.core.content.db.DBAttachmentServiceLocal";
            }
            String ejbJNDIName = "java:global/ofoq/" + serviceType;
            
            attachmentService = (AttachmentServiceLocal) ctx.lookup(ejbJNDIName);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        } 
        return serviceType;
    }

    public void setAttachmentList(Map<String, List<AttachmentDTO>> attachmentList) {
        this.attachmentList = attachmentList;
    }
    
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)

    public AttachmentDTO put(AttachmentDTO attachmentDTO, String attachmentEntityName,long entityDBID, BaseEntity entity, OUser loggedUser) throws BasicException{
        String contentManagement = lookUp(loggedUser, attachmentEntityName, false);
        String attachmentId = attachmentService.put(attachmentDTO, loggedUser.getTenant().getId(), entity);
        EntityAttachments attachment = new EntityAttachments();
        attachment.setAttachmentID(attachmentId);
        attachment.setEntityDBID(entityDBID);
        attachment.setEntityClsName(attachmentEntityName);

        attachment.setContentManagement(contentManagement.substring(contentManagement.lastIndexOf(".")+1));
        attachment.setAttachmentUser(loggedUser.getLoginName());
        AttachmentDTO attchmentDTO = null;
        if(!attachmentId.equals("")){
            try {
                EntityAttachments entityAttachment = (EntityAttachments) oem.saveEntity(attachment, loggedUser);
                attachmentDTO.setId(attachmentId);
                attachmentDTO.setAttachmentDateTime(entityAttachment.getLastMaintDTime().toString());
                attachmentDTO.setAttachmentUser(entityAttachment.getAttachmentUser());
            } catch (UserNotAuthorizedException ex) {
                logger.error("Exception thrown", ex);
            } catch (EntityExistsException ex) {
                logger.error("Exception thrown", ex);
            } catch (EntityNotFoundException ex) {
                logger.error("Exception thrown", ex);
            } catch (NonUniqueResultException ex) {
                logger.error("Exception thrown", ex);
            } catch (NoResultException ex) {
                logger.error("Exception thrown", ex);
            } catch (OptimisticLockException ex) {
                logger.error("Exception thrown", ex);
            } catch (RollbackException ex) {
                logger.error("Exception thrown", ex);
            } catch (TransactionRequiredException ex) {
                logger.error("Exception thrown", ex);
            } catch (OEntityDataValidationException ex) {
                logger.error("Exception thrown", ex);
            } catch (FABSException ex) {
                logger.error("Exception thrown", ex);
            }
            
        }
            
        return attachmentDTO;
    }

    @Override
    public List<AttachmentDTO> get(long entityDBID,String attachmentEntityName ,OUser loggedUser) throws BasicException{
        String contentManagement = lookUp(loggedUser, attachmentEntityName, false);
        List<AttachmentDTO> attachmentDTO = new ArrayList<AttachmentDTO>();
        List<String> conditions = new ArrayList<String>();
        conditions.add("entityDBID=" + entityDBID);
        conditions.add("contentManagement='"+contentManagement.substring(contentManagement.lastIndexOf(".")+1)+"'");
        List<EntityAttachments> eAs = new ArrayList<EntityAttachments>();
       
        try {
            eAs = oem.loadEntityList(EntityAttachments.class.getSimpleName(), conditions, null, null, loggedUser);
            for (EntityAttachments attachmentIDs : eAs) {
                AttachmentDTO attachmentdto = attachmentService.get(loggedUser.getTenant().getId(), attachmentIDs.getAttachmentID());
                if (attachmentdto != null) {
                    if (attachmentIDs.getLastMaintDTime() != null){
                    attachmentdto.setAttachmentDateTime(attachmentIDs.getLastMaintDTime().toString());
                    }
                    if(attachmentIDs.getAttachmentUser() != null){
                    attachmentdto.setAttachmentUser(attachmentIDs.getAttachmentUser());
                    }
                    attachmentDTO.add(attachmentdto);
                }
            }
        } catch (UserNotAuthorizedException ex) {
            logger.error("Exception thrown", ex);
        } catch (EntityExistsException ex) {
            logger.error("Exception thrown", ex);
        } catch (EntityNotFoundException ex) {
            logger.error("Exception thrown", ex);
        } catch (NonUniqueResultException ex) {
            logger.error("Exception thrown", ex);
        } catch (NoResultException ex) {
            logger.error("Exception thrown", ex);
        } catch (OptimisticLockException ex) {
            logger.error("Exception thrown", ex);
        } catch (RollbackException ex) {
            logger.error("Exception thrown", ex);
        } catch (TransactionRequiredException ex) {
            logger.error("Exception thrown", ex);
        }
        return attachmentDTO;
    }

    @Override
    public AttachmentDTO getAttachmentById(String attachmentID, OUser loggedUser,String attachmentEntityName) throws BasicException {
        lookUp(loggedUser,attachmentEntityName, false);
        AttachmentDTO attachmentDTO = new AttachmentDTO();
        try {
            attachmentDTO = attachmentService.get(loggedUser.getTenant().getId(), attachmentID);
            
            List<String> conditions = new ArrayList<String>();
             conditions.add("attachmentID = '"+ attachmentID+"'");
             EntityAttachments entityAttachments = (EntityAttachments) oem.loadEntity("EntityAttachments", conditions, null, loggedUser);
             attachmentDTO.setAttachmentUser(entityAttachments.getAttachmentUser());
        }  catch (EntityExistsException ex) {
            logger.error("Exception thrown", ex);
        } catch (EntityNotFoundException ex) {
            logger.error("Exception thrown", ex);
        } catch (NonUniqueResultException ex) {
            logger.error("Exception thrown", ex);
        } catch (NoResultException ex) {
            logger.error("Exception thrown", ex);
        } catch (OptimisticLockException ex) {
            logger.error("Exception thrown", ex);
        } catch (RollbackException ex) {
            logger.error("Exception thrown", ex);
        } catch (TransactionRequiredException ex) {
            logger.error("Exception thrown", ex);
        } catch (UserNotAuthorizedException ex) {
            logger.error("Exception thrown", ex);
        }
        return attachmentDTO;
    }
    
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void deleteAll(long entityDBID, OUser loggedUser,String attachmentEntityName) {
        List<String> conditions = new ArrayList<String>();
        conditions.add("entityDBID=" + entityDBID);
        List<EntityAttachments> eAIDs = new ArrayList<EntityAttachments>();
        try {
            eAIDs = oem.loadEntityList(EntityAttachments.class.getSimpleName(),
                    conditions, null, null, loggedUser);
            for (EntityAttachments attachmentIDs : eAIDs) {
                delete(attachmentIDs.getAttachmentID(), loggedUser,attachmentEntityName);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }

    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void update(AttachmentDTO attachment, OUser loggedUser,String attachmentEntityName) throws BasicException{
        lookUp(loggedUser,attachmentEntityName, false);
        attachmentService.update(attachment , loggedUser.getTenant().getId());
       
        List<String> conditions = new ArrayList<String>();
             conditions.add("attachmentID = '"+ attachment.getId()+"'");
        try {
            EntityAttachments entityAttachment = (EntityAttachments) oem.loadEntity("EntityAttachments", conditions, null, loggedUser);
            entityAttachment.setDescription(attachment.getDescription());
            oem.saveEntity(entityAttachment, loggedUser);
        } catch (UserNotAuthorizedException ex) {
            logger.error("Exception thrown", ex);
        } catch (EntityExistsException ex) {
            logger.error("Exception thrown", ex);
        } catch (EntityNotFoundException ex) {
            logger.error("Exception thrown", ex);
        } catch (NonUniqueResultException ex) {
            logger.error("Exception thrown", ex);
        } catch (NoResultException ex) {
            logger.error("Exception thrown", ex);
        } catch (OptimisticLockException ex) {
            logger.error("Exception thrown", ex);
        } catch (RollbackException ex) {
            logger.error("Exception thrown", ex);
        } catch (TransactionRequiredException ex) {
            logger.error("Exception thrown", ex);
        } catch (OEntityDataValidationException ex) {
            logger.error("Exception thrown", ex);
        } catch (FABSException ex) {
            logger.error("Exception thrown", ex);
        }
            
       
        
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void delete(String attachmentID,OUser loggedUser,String attachmentEntityName) throws BasicException {
            lookUp(loggedUser,attachmentEntityName, false);
            attachmentService.delete(attachmentID, loggedUser.getTenant().getId());
        try {
             List<String> conditions = new ArrayList<String>();
             conditions.add("attachmentID = '"+ attachmentID+"'");
             EntityAttachments entityAttachments = (EntityAttachments) oem.loadEntity("EntityAttachments", conditions, null, loggedUser);
            oem.deleteEntity(entityAttachments, loggedUser);
        } catch (UserNotAuthorizedException ex) {
            logger.error("Exception thrown", ex);
        } catch (EntityExistsException ex) {
            logger.error("Exception thrown", ex);
        } catch (EntityNotFoundException ex) {
            logger.error("Exception thrown", ex);
        } catch (NonUniqueResultException ex) {
            logger.error("Exception thrown", ex);
        } catch (NoResultException ex) {
            logger.error("Exception thrown", ex);
        } catch (OptimisticLockException ex) {
            logger.error("Exception thrown", ex);
        } catch (RollbackException ex) {
            logger.error("Exception thrown", ex);
        } catch (TransactionRequiredException ex) {
            logger.error("Exception thrown", ex);
        } catch (FABSException ex) {
            logger.error("Exception thrown", ex);
        }
        
    }
    
    @Override
    public boolean hasAttachments(String attachmentName, OUser loggedUser,String attachmentEntityName) {
        lookUp(loggedUser,attachmentEntityName, true);
        AttachmentDTO attachmentDTO = getByName(attachmentName, loggedUser, attachmentEntityName);
        if(attachmentDTO != null) {
            return true;
        }
        return false;
    }
    
    @Override
    public AttachmentDTO getByName(String attachmentName, OUser loggedUser,String attachmentEntityName){
        lookUp(loggedUser,attachmentEntityName, true);
        AttachmentDTO attachmentDTO = ((DBAttachmentServiceLocal)attachmentService).getByName(attachmentName, loggedUser.getTenant().getId());
        return attachmentDTO;
    }

    @Override
    public AttachmentDTO putPreSave(AttachmentDTO attachmentDTO, BaseEntity entity, OUser loggedUser) {
            List<AttachmentDTO> attachment = attachmentList.get(entity.getInstID()+"");
            if (attachment == null) {
                attachment = new ArrayList<AttachmentDTO>();
            }
            attachment.add(attachmentDTO);
            attachmentList.put(entity.getInstID()+"", attachment);

        return attachmentDTO;
    }

    @Override
    public List<AttachmentDTO> getPreSave(BaseEntity entity, OUser loggedUser) {
   
        return attachmentList.get(entity.getInstID() + "");
    }

    @Override
    public void clearAttachmentListBeforeSave(BaseEntity entity, OUser loggedUser) {
        attachmentList.remove(entity.getInstID()+"");
    }

    @Override
    public void deleteForPresave(BaseEntity entity, AttachmentDTO attachmentDTO, OUser loggedUser) {
        List<AttachmentDTO> attachment=attachmentList.get(entity.getInstID()+"");
        attachment.remove(attachmentDTO);
        attachmentList.put(entity.getInstID()+"", attachment);
    }
}
