/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.udc.dto;

/**
 *
 * @author root
 */
public class UDCDTO {
    private String value;
    private String code;
    private boolean reserved;

    public UDCDTO(String value, String code, boolean reserved) {
        this.value = value;
        this.code = code;
        this.reserved = reserved;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }
    
}
