/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.portal;

import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;

/**
 *
 * @author htawab
 */
@Entity
@ChildEntity(fields={"portlets", "portalPageFunctions"})
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class OPortalPage extends ObjectBaseEntity{

    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OModule omodule;

    public String getOmoduleDD(){
        return "OPortalPage_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
// </editor-fold>

    protected String portletPageID;
//    @Transient
//    @Translation(originalField="name")
//    private String nameTranslated;
//    public String getNameTranslated() {
//        return nameTranslated;
//    }
//
//    public void setNameTranslated(String nameTranslated) {
//        this.nameTranslated = nameTranslated;
//    }
//    @Translatable(translationField="nameTranslated")
    protected String name;

    public String getNameDD(){
        return "OPortalPage_name";
    }

    @OneToMany(mappedBy = "portalPage")
    protected List<PortalPagePortlet> portlets = null;

    /**
     * Get the value of portlets
     *
     * @return the value of portlets
     */
    public List<PortalPagePortlet> getPortlets() {
        return portlets;
    }

    /**
     * Set the value of portlets
     *
     * @param portlets new value of portlets
     */
    public void setPortlets(List<PortalPagePortlet> portlets) {
        this.portlets = portlets;
    }


    /**
     * Get the value of portletPageID
     *
     * @return the value of portletPageID
     */
    public String getPortletPageID() {
        return portletPageID;
    }

    /**
     * Set the value of portletPageID
     *
     * @param portletPageID new value of portletPageID
     */
    public void setPortletPageID(String portletPageID) {
        this.portletPageID = portletPageID;
    }

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

    // <editor-fold defaultstate="collapsed" desc="layoutTemplate">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC layoutTemplate;

    public UDC getLayoutTemplate() {
        return layoutTemplate;
    }

    public String getLayoutTemplateDD() {
        return "OPortalPage_layoutTemplate";
    }

    public void setLayoutTemplate(UDC layoutTemplate) {
        this.layoutTemplate = layoutTemplate;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Page Header">
    @Transient
    @Translation(originalField="header")
    private String headerTranslated;

    @Translatable(translationField="headerTranslated")
    protected String header;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getHeaderTranslated() {
        return headerTranslated;
    }

    public void setHeaderTranslated(String headerTranslated) {
        this.headerTranslated = headerTranslated;
    }

    public String getHeaderTranslatedDD(){
        return "OPortalPage_header";
    }
    // </editor-fold>
    
    
    //RDO
    // <editor-fold defaultstate="collapsed" desc="Page Screen Instances">
    @Transient
    private HashMap<String,OScreen> pageScreenInstancesList;

    /**
     * @return the pageScreenInstancesList
     */
    public HashMap<String,OScreen> getPageScreenInstancesList() {
        return pageScreenInstancesList;
    }

    /**
     * @param pageScreenInstancesList the pageScreenInstancesList to set
     */
    public void setPageScreenInstancesList(HashMap<String,OScreen> pageScreenInstancesList) {
        this.pageScreenInstancesList = pageScreenInstancesList;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="portalPageFunctions">
    @OneToMany(mappedBy = "portalPage", fetch = FetchType.LAZY)
    private List<PortalPageFunction> portalPageFunctions;

    public List<PortalPageFunction> getPortalPageFunctions() {
        return portalPageFunctions;
    }

    public void setPortalPageFunctions(List<PortalPageFunction> portalPageFunctions) {
        this.portalPageFunctions = portalPageFunctions;
    }

    public String getPortalPageFunctionsDD() {
        return "OPortalPage_portalPageFunctions";
    }
    // </editor-fold >

}
