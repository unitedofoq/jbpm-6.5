/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author arezk
 */
@Entity
@DiscriminatorValue(value = "SCREEN")
@ParentEntity(fields = {"oscreen"})
@ChildEntity(fields = {"menuFunction"})
@VersionControlSpecs(omoduleFieldExpression = "oscreen.omodule")
public class ScreenFunction extends OFunction {

    // <editor-fold defaultstate="collapsed" desc="oscreen">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private OScreen oscreen;

    public OScreen getOscreen() {
        return oscreen;
    }

    public void setOscreen(OScreen oscreen) {
        this.oscreen = oscreen;
    }

    public String getOscreenDD() {
        return "ScreenFunction_oScreen";
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="screenMode">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC screenMode = null;

    public String getScreenModeDD() {
        return "ScreenFunction_screenMode";
    }

    public UDC getScreenMode() {
        return screenMode;
    }

    public void setScreenMode(UDC screenMode) {
        this.screenMode = screenMode;
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="odataType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ODataType odataType = null;

    public ODataType getOdataType() {
        return odataType;
    }

    public void setOdataType(ODataType odataType) {
        this.odataType = odataType;
    }

    public String getOdataTypeDD() {
        return "ScreenFunction_odataType";
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="runInPopup">
    /**
     * Default is false
     */
    @Transient
    private boolean runInPopup = false;

    public boolean isRunInPopup() {
        return runInPopup;
    }

    public void setRunInPopup(boolean runInPopup) {
        this.runInPopup = runInPopup;
    }
    // </editor-fold >
}
