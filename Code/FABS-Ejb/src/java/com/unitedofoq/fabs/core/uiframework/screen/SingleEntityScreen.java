/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.screen;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nkhalil
 */
@Entity
@DiscriminatorValue(value = "SINGULAR")
@ChildEntity(fields={"screenInputs", "screenOutputs", "screenFilter", "screenFunctions",
    "screenControlsGroups", "screenFields", "screenActions"})
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class SingleEntityScreen extends OScreen{   
        final static Logger logger = LoggerFactory.getLogger(SingleEntityScreen.class);
    // <editor-fold defaultstate="collapsed" desc="screenControlsGroups">
    //fetch=FetchType.EAGER added by Rehab to sove java8 issue with JPA
     @OneToMany(mappedBy = "oscreen" , fetch = FetchType.EAGER)
    private List<ScreenFieldGroup> screenControlsGroups;

    public List<ScreenFieldGroup> getScreenControlsGroups() {
        return screenControlsGroups;
    }

    public void setScreenControlsGroups(List<ScreenFieldGroup> screenControlsGroups) {
        this.screenControlsGroups = screenControlsGroups;
    }
    // </editor-fold>
    @Column(nullable=false)
    private boolean editable;
    @Column(nullable=false)
    private boolean removable;

    // <editor-fold defaultstate="collapsed" desc="action">
    @OneToMany(mappedBy="oscreen")
    private List<ScreenAction> screenActions = null;
    public List<ScreenAction> getScreenActions() {
        return screenActions;
    }
    public void setScreenActions(List<ScreenAction> screenActions) {
        this.screenActions = screenActions;
    }
    // </editor-fold>


    public String getEditableDD()       {   return "SingleEntityScreen_editable";  }
    public String getRemovableDD() {
        return "SingleEntityScreen_removable";
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isRemovable() {
        return removable;
    }

    public void setRemovable(boolean removable) {
        this.removable = removable;
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    protected OEntity oactOnEntity = null;
    @OneToMany(mappedBy="oScreen", fetch= FetchType.LAZY)
    protected List<ScreenField> screenFields = null;
     public OEntity getOactOnEntity() {
        return oactOnEntity;
    }

    public void setOactOnEntity(OEntity oactOnEntity) {
        this.oactOnEntity = oactOnEntity;
    }

    public List<ScreenField> getScreenFields() {
        return screenFields;
    }

    public void setScreenFields(List<ScreenField> screenFields) {
        this.screenFields = screenFields;
    }
    public String getOactOnEntityDD()                {   return "SingleEntityScreen_oActOnEntity";  }
    public String getScreenFieldsDD()                {   return "SingleEntityScreen_screenFields";  }


    private boolean saveAndExit;

    public boolean isSaveAndExit() {
        return saveAndExit;
    }

    public String getSaveAndExitDD() {
        return "SingleEntityScreen_saveAndExit";
    }

    public void setSaveAndExit(boolean saveAndExit) {
        this.saveAndExit = saveAndExit;
    }
    
    @Column(name="tosave")
    private boolean save;

    public boolean isSave() {
        return save;
    }

    public String getSaveDD() {
        return "SingleEntityScreen_save";
    }

    public void setSave(boolean save) {
        this.save = save;
    }
    
    // <editor-fold defaultstate="collapsed" desc="actOnEntityParentClassPath">
    /**
     * Filled by {@link #getActOnEntityParentClassPath() }
     */
    @Transient
    private List<String> actOnEntityParentClassPath = null;

    /**
     * List of strings represents the entity parents class paths, caching it in 
     * {@link #actOnEntityParentClassPath}
     * @return 
     * null, if no {@link #oactOnEntity} or no {@link #oactOnEntity}.{@link OEntity#entityClassPath}
     */
    public List getActOnEntityParentClassPath() {
        logger.debug("Entering");
        if (actOnEntityParentClassPath != null)
        // Already set
            // Return it
            return actOnEntityParentClassPath ;
        
        if (oactOnEntity == null) {
        // No actOnEntity
            // Set to null, and return null
            logger.debug("act on entity equals Null");
            setActOnEntityParentClassPath(null);
            return null ;
        }
        if (oactOnEntity.getEntityClassPath() == null) {
        // Entity Class Path not set yet, happens with force intialized ones
        // If not checked, oactOnEntity.getEntityParentClassPath() called next
        // will throw NullPointerException 
            // Set to null, and return null
            logger.debug("Entity Class Path not set yet");
            setActOnEntityParentClassPath(null);
            logger.debug("Returning with Null");
            return null ;
        }
        setActOnEntityParentClassPath(oactOnEntity.getEntityParentClassPath());
        return actOnEntityParentClassPath;
    }

    public void setActOnEntityParentClassPath(List parentEntityClassPath) {
        logger.debug("Entering");
        this.actOnEntityParentClassPath = parentEntityClassPath;
        if (parentEntityClassPath == null) {
            logger.debug("Returing");
            return ;
        }
    }
    // </editor-fold>
}
