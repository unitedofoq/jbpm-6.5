/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.function;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.module.OModule;

/**
 *
 * @author aelzaher
 */
@Entity
@DiscriminatorValue(value="RULE")
@ChildEntity(fields={"functionImage", "menuFunction"})
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class ORuleFunction extends OFunction
{
    // <editor-fold defaultstate="collapsed" desc="omodule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OModule omodule;

    public String getOmoduleDD(){
        return "ORuleFunction_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="packageName">
    @Column
    private String packageName ;
    
    public String getPackageName() {
        return packageName;
    }
    
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
    
    public String getPackageNameDD(){
        return "ORuleFunction_packageName" ;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="ruleName">
    @Column
    private String ruleName ;
    
    public String getRuleName() {
        return ruleName;
    }
    
    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }
    
    public String getRuleNameDD(){
        return "ORuleFunction_ruleName" ;
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="odataType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ODataType odataType;

    public ODataType getOdataType() {
        return odataType;
    }
    
    public void setOdataType(ODataType odataType) {
        this.odataType = odataType;
    }

    public String getOdataTypeDD() {
        return "ScreenFunction_odataType";
    }
    //</editor-fold>
}
