/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.security.user;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author mibrahim
 */
@Entity
@ReadOnly
public class MenuFunction implements Serializable {
    private String name;
    private String nameTranslated;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    
   @Id
   private long menuDBID;
   private long functionDBID;
   private int sortIndex;
   private String ftype;

    public String getFtype() {
        return ftype;
    }

    public void setFtype(String ftype) {
        this.ftype = ftype;
    }
   
    public long getMenuDBID() {
        return menuDBID;
    }

    public void setMenuDBID(long menuDBID) {
        this.menuDBID = menuDBID;
    }

    public long getFunctionDBID() {
        return functionDBID;
    }

    public void setFunctionDBID(long functionDBID) {
        this.functionDBID = functionDBID;
    }

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }
   
    
}
