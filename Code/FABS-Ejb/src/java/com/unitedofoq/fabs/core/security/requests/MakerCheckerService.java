/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.security.requests;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OMenuFunction;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.RoleMenu;
import com.unitedofoq.fabs.core.security.user.RoleUser;
import com.unitedofoq.fabs.core.security.user.UserServiceBean;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.udc.UDCServiceRemote;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author lap
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.security.requests.MakerCheckerServiceLocal",
  beanInterface = MakerCheckerServiceLocal.class)
public class MakerCheckerService implements MakerCheckerServiceLocal {

  final static Logger logger = LoggerFactory.getLogger(UserServiceBean.class);
  @EJB
  private OEntityManagerRemote oentityManager;
  @EJB
  private EntitySetupServiceRemote entitySetupService;
  @EJB
  private UserMessageServiceRemote userMsgService;
  @EJB
  private UDCServiceRemote uDCService;
  
  public static final long REJECTED_DBID = 1444554;
  public static final long APPROVED_DBID = 1444553;

  public static final long REQ_TYPE_ADD = 1444548;
  public static final long REQ_TYPE_UPDATE = 1444550;
  public static final long REQ_TYPE_DELETE = 1444549;
  
  private static final  String SystemInternalError="SystemInternalError";

  @Override
  public OFunctionResult validateRequest(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
    logger.debug("Entering");
    OFunctionResult oFR = new OFunctionResult();
    try {

      RequestsBaseEntity request = (RequestsBaseEntity) odm.getData().get(0);

      StringBuilder query=new StringBuilder("Select dbid from " + request.getClass().getSimpleName() + " Where actiontaken = 0 And ");
      if (request.getRequestType().getDbid() == REQ_TYPE_ADD) {
          query=createQueryForAddRequest(request, query);
      } else if (request.getRequestType().getDbid() == REQ_TYPE_UPDATE
        || request.getRequestType().getDbid() == REQ_TYPE_DELETE) {
          query=createQueryForUpdateRequest(request, query);
      }

      List requestDBIDs = oentityManager.executeEntityListNativeQuery(query.toString(), oentityManager.getSystemUser(loggedUser));

      if (!(requestDBIDs == null || requestDBIDs.isEmpty())) {
        oFR.addError(userMsgService.getUserMessage("MakerCheckerDuplicate", loggedUser));
      }

    } catch (Exception ex) {
      logger.error("exception thrown: ", ex);
      oFR.addError(userMsgService.getUserMessage(SystemInternalError, loggedUser), ex);
    } finally {
      logger.debug("Returning");
      return oFR;
    }
  }

  @Override
  public OFunctionResult makerCheckerRequestInit(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
    logger.debug("Entering");
    OFunctionResult oFR = new OFunctionResult();
    try {
      RequestsBaseEntity request = (RequestsBaseEntity) odm.getData().get(0);
      BaseEntity originalEntity = (BaseEntity) BaseEntity.getValueFromEntity(request, "originalEntity");

      if (originalEntity == null) {
        getUdc(request, REQ_TYPE_ADD, loggedUser);
      } else {
        getUdc(request, REQ_TYPE_UPDATE, loggedUser);
        request = applyOriginalValues(request, originalEntity);       
      }

      oFR.setReturnValues(Collections.singletonList(request));
      oFR.setReturnedDataMessage(odm);

    } catch (Exception ex) {
      logger.error("exception thrown: ", ex);
      oFR.addError(userMsgService.getUserMessage(SystemInternalError, loggedUser), ex);
    } finally {
      logger.debug("Returning");
      return oFR;
    }
  }

  @Override
  public OFunctionResult deletedRequestAction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
    logger.debug("Entering");
    OFunctionResult oFR = new OFunctionResult();
    try {

      BaseEntity originalEntity = (BaseEntity) odm.getData().get(0);
      RequestsBaseEntity request = null;// = new RequestsBaseEntity();

      request = applyOriginalValues(request, originalEntity);
      BaseEntity.setValueInEntity(request, "originalEntity", originalEntity);
      getUdc(request, REQ_TYPE_DELETE, loggedUser);

      oFR.append(entitySetupService.callEntityCreateAction(request, loggedUser));
      
      if(oFR.getErrors().isEmpty()){
        oFR.addSuccess(userMsgService.getUserMessage("MakerCheckerDelRequest", loggedUser));
      }

      
      
    } catch (Exception ex) {
      logger.error("exception thrown: ", ex);
      oFR.addError(userMsgService.getUserMessage(SystemInternalError, loggedUser), ex);
    } finally {
      logger.debug("Returning");
      return oFR;
    }
  }

  @Override
  public OFunctionResult rejectRequest(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
    logger.debug("Entering");
    OFunctionResult oFR = new OFunctionResult();
    try {

      RequestsBaseEntity request = (RequestsBaseEntity) odm.getData().get(0);
      
     if(request.getAction() !=null && request.getAction().getValue() != null){
        oFR.addError(userMsgService.getUserMessage("ActionValidation", loggedUser));
        return oFR;
      } 
      String query = "Update " + request.getClass().getSimpleName() + " set actionTaken = 1, action_dbid = "
        + REJECTED_DBID + " where dbid =  " + request.getDbid();

      oentityManager.executeEntityUpdateNativeQuery(query, loggedUser);

      oFR.addSuccess(userMsgService.getUserMessage("MakerCheckerRejected", loggedUser));
    } catch (Exception ex) {
      logger.error("exception thrown: ", ex);
      oFR.addError(userMsgService.getUserMessage(SystemInternalError, loggedUser), ex);
    } finally {
      logger.debug("Returning");
      return oFR;
    }
  }

  @Override
  public OFunctionResult approveRequest(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
    logger.debug("Entering");
    OFunctionResult oFR = new OFunctionResult();
    try {

      RequestsBaseEntity request = (RequestsBaseEntity) odm.getData().get(0);
      BaseEntity originalEntity = (BaseEntity) BaseEntity.getValueFromEntity(request, "originalEntity");

      if(request.getAction() !=null && request.getAction().getValue() != null){
        oFR.addError(userMsgService.getUserMessage("ActionValidation", loggedUser));
        return oFR;
      }
      if (request.getRequestType().getDbid() == REQ_TYPE_DELETE) {
        originalEntity.setInActive(true);
        oFR.addReturnValue(entitySetupService.callEntityUpdateAction(originalEntity, loggedUser));
      } else if (request.getRequestType().getDbid() == REQ_TYPE_ADD) {
        originalEntity = getOriginalEntityForAddRequest(originalEntity, request);
        originalEntity = applyRequestValues(originalEntity, request);

        oFR.append(entitySetupService.callEntityCreateAction(originalEntity, loggedUser));

      } else if (request.getRequestType().getDbid() == REQ_TYPE_UPDATE) {

        originalEntity = applyRequestValues(originalEntity, request);

        oFR.append(entitySetupService.callEntityUpdateAction(originalEntity, loggedUser));

      }
      
      String query = "Update " + request.getClass().getSimpleName() + " set actionTaken = 1, action_dbid = "
        + APPROVED_DBID + " where dbid =  " + request.getDbid();

      oentityManager.executeEntityUpdateNativeQuery(query, oentityManager.getSystemUser(loggedUser));

      if(oFR.getErrors().isEmpty()){
      oFR.addSuccess(userMsgService.getUserMessage("MakerCheckerApproved", loggedUser));
      }

    } catch (Exception ex) {
      logger.error("exception thrown: ", ex);
      oFR.addError(userMsgService.getUserMessage(SystemInternalError, loggedUser), ex);
    } finally {
      logger.debug("Returning");
      return oFR;
    }
  }

  private BaseEntity applyRequestValues(BaseEntity baseEntity, RequestsBaseEntity request) {
    if (request instanceof OUserRequest) {
        createOuserInstance(baseEntity, request);
    } else if (request instanceof OMenuFunctionRequest) {
        createOmenuFunctionInstance(baseEntity, request);
    } else if (request instanceof RoleMenuRequest) {
        createRoleMenuInstance(baseEntity, request);
    } else if (request instanceof RoleUserRequest) {
        createRoleUserInstance(baseEntity, request);
    }

    return baseEntity;
  }

  private RequestsBaseEntity applyOriginalValues(RequestsBaseEntity request, BaseEntity baseEntity) {
    if (baseEntity instanceof OUser) {
        request = createOuserForOriginalValues(baseEntity, request);
    } else if (baseEntity instanceof OMenuFunction) {
        request = createOmenuFunctionForOriginalValues(baseEntity, request);
    } else if (baseEntity instanceof RoleMenu) {
        request = createRoleMenuForApplyValues(baseEntity, request);
    } else if (baseEntity instanceof RoleUser) {
        request = createRoleUserForApplyValues(baseEntity, request);
    }

    return request;
  }
  
    @Override
    public OFunctionResult validateUinqunessEmail(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUserRequest ouserRequest = (OUserRequest) odm.getData().get(0);
        try {

            if (ouserRequest != null && !ouserRequest.getRequestType().getValue().equals("Delete")) {
                List conditions = new ArrayList();
                conditions.add("email='" + ouserRequest.getEmail() + "'");
                List<OUser> OUser = (List<OUser>) oentityManager.loadEntityList("OUser", conditions, null, null, loggedUser);
                if (OUser != null && !OUser.isEmpty()) {
                   if(ouserRequest.getRequestType().getValue().equals("Add") ||(OUser.size()>1 || ouserRequest.getOriginalEntity() !=null && OUser.get(0).getDbid() != ouserRequest.getOriginalEntity().getDbid())){
                    oFR.addError(userMsgService.getUserMessage(
                            "ValidateEmailUniqness", loggedUser));
                   }                  
                }
            }

        } catch (Exception e) {

            oFR.addError(userMsgService.getUserMessage(
                    SystemInternalError, loggedUser));
        }

        logger.debug("returning");
        return oFR;
    }

    @Override
    public OFunctionResult validateUinqunessLoginname(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUserRequest ouserRequest = (OUserRequest) odm.getData().get(0);
        try {

            if (ouserRequest != null && !ouserRequest.getRequestType().getValue().equals("Delete")) {

                List conditions1 = new ArrayList();
                conditions1.add("loginName='" + ouserRequest.getLoginName() + "'");
                List<OUserRequest> userRequest = (List<OUserRequest>) oentityManager.loadEntityList("OUserRequest", conditions1, ouserRequest.getFieldExpressions(), null, loggedUser);
                if (userRequest != null && !userRequest.isEmpty() && ouserRequest.getRequestType().getValue().equals("Add")) {

                    oFR.addError(userMsgService.getUserMessage(
                            "ValidateLoginNameUniqness", loggedUser));
                }
            }

        } catch (Exception e) {

            oFR.addError(userMsgService.getUserMessage(
                    SystemInternalError, loggedUser));
        }

        logger.debug("returning");
        return oFR;
    }

    @Override
    public OFunctionResult validateArabicLanguageWithRTL(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUserRequest ouserRequest = (OUserRequest) odm.getData().get(0);
        try {
            if (ouserRequest != null && !ouserRequest.getRequestType().getValue().equals("Delete")) {

                if (ouserRequest.getFirstLanguage().getValue().equals("Arabic") && !ouserRequest.isRtl()) {
                    oFR.addError(userMsgService.getUserMessage(
                            "validateArabicWithRTL", loggedUser));
                    return oFR;
                }
            }
        } catch (Exception e) {
            oFR.addError(userMsgService.getUserMessage(
                    SystemInternalError, loggedUser));
        }
        logger.debug("returning");
        return oFR;
    }
    
    
    //private helper methods 
   
    private void createOuserInstance(BaseEntity baseEntity, RequestsBaseEntity request) {
        OUser user = (OUser) baseEntity;
        OUserRequest userRequest = (OUserRequest) request;
        if (userRequest.getBirthDate() != null) {
            user.setBirthDate(userRequest.getBirthDate());
        }
        if (userRequest.getDisplayName() != null && !userRequest.getDisplayName().equals("")) {
            user.setDisplayName(userRequest.getDisplayName());
        }
        if (userRequest.getEmail() != null && !userRequest.getEmail().equals("")) {
            user.setEmail(userRequest.getEmail());
        }
        if (userRequest.getFirstLanguage() != null && userRequest.getFirstLanguage().getDbid() != 0) {
            user.setFirstLanguage(userRequest.getFirstLanguage());
        }
        if (userRequest.getSecondLanguage() != null && userRequest.getSecondLanguage().getDbid() != 0) {
            user.setSecondLanguage(userRequest.getFirstLanguage());
        }
        if (userRequest.getLoginName() != null && !userRequest.getLoginName().equals("")) {
            user.setLoginName(userRequest.getLoginName());
        }
        if (userRequest.getPassword() != null && !userRequest.getPassword().equals("")) {
            user.setPassword(userRequest.getPassword());
        }
        if (userRequest.getOmodule() != null && userRequest.getOmodule().getDbid() != 0) {
            user.setOmodule(userRequest.getOmodule());
        }
    }
    
    private void createOmenuFunctionInstance(BaseEntity baseEntity, RequestsBaseEntity request) {
        OMenuFunction menuFunction = (OMenuFunction) baseEntity;
        OMenuFunctionRequest menuFunctionRequest = (OMenuFunctionRequest) request;

        if (menuFunctionRequest.getOmenu() != null && menuFunctionRequest.getOmenu().getDbid() != 0) {
            menuFunction.setOmenu(menuFunctionRequest.getOmenu());
        }
        if (menuFunctionRequest.getOfunction() != null && menuFunctionRequest.getOfunction().getDbid() != 0) {
            menuFunction.setOfunction(menuFunctionRequest.getOfunction());
        }
    }
    
    private void createRoleMenuInstance(BaseEntity baseEntity, RequestsBaseEntity request) {
        RoleMenu roleMenu = (RoleMenu) baseEntity;
        RoleMenuRequest roleMenuRequest = (RoleMenuRequest) request;

        if (roleMenuRequest.getOmenu() != null && roleMenuRequest.getOmenu().getDbid() != 0) {
            roleMenu.setOmenu(roleMenuRequest.getOmenu());
        }
        if (roleMenuRequest.getOrole() != null && roleMenuRequest.getOrole().getDbid() != 0) {
            roleMenu.setOrole(roleMenuRequest.getOrole());
        }
    }
    
    private void createRoleUserInstance(BaseEntity baseEntity, RequestsBaseEntity request) {
        RoleUser roleUser = (RoleUser) baseEntity;
        RoleUserRequest roleUserRequest = (RoleUserRequest) request;

        if (roleUserRequest.getOuser() != null && roleUserRequest.getOuser().getDbid() != 0) {
            roleUser.setOuser(roleUserRequest.getOuser());
        }
        if (roleUserRequest.getOrole() != null && roleUserRequest.getOrole().getDbid() != 0) {
            roleUser.setOrole(roleUserRequest.getOrole());
        }
    }
    
    private OUserRequest createOuserForOriginalValues(BaseEntity baseEntity, RequestsBaseEntity request) {
        OUser user = (OUser) baseEntity;
        OUserRequest userRequest;

        if (request == null) {
            request = new OUserRequest();
        }

        userRequest = (OUserRequest) request;
        userRequest.setBirthDate(user.getBirthDate());
        userRequest.setDisplayName(user.getDisplayName());
        userRequest.setEmail(user.getEmail());
        userRequest.setFirstLanguage(user.getFirstLanguage());
        userRequest.setSecondLanguage(user.getFirstLanguage());
        userRequest.setLoginName(user.getLoginName());
        userRequest.setPassword(user.getPassword());
        userRequest.setOmodule(user.getOmodule());
        return userRequest;
    }
    
    private OMenuFunctionRequest createOmenuFunctionForOriginalValues(BaseEntity baseEntity, RequestsBaseEntity request) {
        OMenuFunctionRequest menuFunctionRequest;

        OMenuFunction menuFunction = (OMenuFunction) baseEntity;
        if (request == null) {
            request = new OMenuFunctionRequest();
        }

        menuFunctionRequest = (OMenuFunctionRequest) request;
        menuFunctionRequest.setOmenu(menuFunction.getOmenu());
        menuFunctionRequest.setOfunction(menuFunction.getOfunction());
        return menuFunctionRequest;

    }
    
    private RoleMenuRequest createRoleMenuForApplyValues(BaseEntity baseEntity, RequestsBaseEntity request) {
        RoleMenu roleMenu = (RoleMenu) baseEntity;

        RoleMenuRequest roleMenuRequest;
        if (request == null) {
            request = new RoleMenuRequest();
        }

        roleMenuRequest = (RoleMenuRequest) request;
        roleMenuRequest.setOmenu(roleMenu.getOmenu());
        roleMenuRequest.setOrole(roleMenu.getOrole());
        return roleMenuRequest;
    }
    
    private RoleUserRequest createRoleUserForApplyValues(BaseEntity baseEntity, RequestsBaseEntity request) {
        RoleUser roleUser = (RoleUser) baseEntity;
        RoleUserRequest roleUserRequest;

        if (request == null) {
            request = new RoleUserRequest();
        }

        roleUserRequest = (RoleUserRequest) request;
        roleUserRequest.setOuser(roleUser.getOuser());
        roleUserRequest.setOrole(roleUser.getOrole());
        return roleUserRequest;
    }
    
    private StringBuilder createQueryForAddRequest(RequestsBaseEntity request, StringBuilder query) {
        
        if (request instanceof OUserRequest) {
            query.append(" loginname = '").append(((OUserRequest) request).getLoginName()).append("'");
        } else if (request instanceof OMenuFunctionRequest) {
            query.append(" omenu_dbid = ").append(((OMenuFunctionRequest) request).getOmenu().getDbid()).append(" And ofunction_dbid = ").append(((OMenuFunctionRequest) request).getOfunction().getDbid());

        } else if (request instanceof RoleMenuRequest) {
            query.append(" omenu_dbid = ").append(((RoleMenuRequest) request).getOmenu().getDbid()).append(" And orole_dbid = ").append(((RoleMenuRequest) request).getOrole().getDbid());
        } else if (request instanceof RoleUserRequest) {
            query.append(" ouser_dbid = ").append(((RoleUserRequest) request).getOuser().getDbid()).append(" And orole_dbid = ").append(((RoleUserRequest) request).getOrole().getDbid());
        }
        return query;
    }
    
    private StringBuilder createQueryForUpdateRequest(RequestsBaseEntity request, StringBuilder query) throws NoSuchMethodException {
        
        try {
            query.append(" (originalEntity_dbid = ").append(BaseEntity.getValueFromEntity(request, "originalEntity.dbid"));
            if (request instanceof OUserRequest) {
                query.append(")");
            } else if (request instanceof OMenuFunctionRequest) {
                query.append(" OR (omenu_dbid = ").append(((OMenuFunctionRequest) request).getOmenu().getDbid()).append(" And ofunction_dbid = ").append(((OMenuFunctionRequest) request).getOfunction().getDbid()).append("))");
            } else if (request instanceof RoleMenuRequest) {
                query.append(" OR (omenu_dbid = ").append(((RoleMenuRequest) request).getOmenu().getDbid()).append(" And orole_dbid = ").append(((RoleMenuRequest) request).getOrole().getDbid()).append("))");
            } else if (request instanceof RoleUserRequest) {
                query.append(" OR (ouser_dbid = ").append(((RoleUserRequest) request).getOuser().getDbid()).append(" And orole_dbid = ").append(((RoleUserRequest) request).getOrole().getDbid()).append("))");
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return query;
    }
    
    private void getUdc(RequestsBaseEntity request, long udcType ,OUser loggedUser) {
        try{
        UDC udc = (UDC) oentityManager.loadEntity("UDC", udcType, null, null, loggedUser);
        request.setRequestType(udc);
        request.setActionDate(new Date());
        }catch(Exception e){
            logger.error(e.getMessage());
        }
    }
    
    private BaseEntity getOriginalEntityForAddRequest(BaseEntity originalEntity, RequestsBaseEntity request) {
        if (request instanceof OUserRequest) {
            originalEntity = new OUser();
        } else if (request instanceof OMenuFunctionRequest) {
            originalEntity = new OMenuFunction();
        } else if (request instanceof RoleMenuRequest) {
            originalEntity = new RoleMenu();
        } else if (request instanceof RoleUserRequest) {
            originalEntity = new RoleUser();
        }
        
        return originalEntity;
    }
    
}
