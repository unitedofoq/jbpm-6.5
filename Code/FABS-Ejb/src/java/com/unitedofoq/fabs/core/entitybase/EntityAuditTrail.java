/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.entitybase;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author bassem
 */
@Entity
@FABSEntitySpecs(isAuditTrailed=false)
public class EntityAuditTrail extends ObjectBaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="entityDBID">
    @Column(nullable=false)
    private long entityDBID ;
    public long getEntityDBID() {
        return entityDBID;
    }
    public void setEntityDBID(long entityDBID) {
        this.entityDBID = entityDBID;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="entityClsName">
    @Column(nullable=false)
    private String entityClsName ;
    public String getEntityClsName() {
        return entityClsName;
    }
    public void setEntityClsName(String entityClsName) {
        this.entityClsName = entityClsName;
    }
    
     /* DD entityClsName GETTER */
    public String getEntityClsNameDD(){
        return "EntityAuditTrail_entityClsName";  
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="actionType">
    public enum ActionType {
        CREATE  { public String toString() { return "CREATE"; }   },
        UPDATE  { public String toString() { return "UPDATE"; }   },
        DELETE  { public String toString() { return "DELETE"; }   },
        TERMINATE   { public String toString() { return "TERMINATE"; }   }
    }
    
    @Column(nullable=false)
    private ActionType actionType ;
    public ActionType getActionType() {
        return actionType;
    }
    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
        this.entityActionType = actionType.name();
    }
    
    /* DD actionType GETTER */
    public String getActionTypeDD(){
        return "EntityAuditTrail_actionType";  
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="entityActionType">
    @Transient 
    private String entityActionType ;
     /**
     * @return the entityActionType
     */
    public String getEntityActionType() {
        return actionType.name();
    }
    /**
     * @param entityActionType the entityActionType to set
     */
    public void setEntityActionType(String entityActionType) {
        this.entityActionType = entityActionType;
    }
    /* DD entityActionType GETTER */
    public String getEntityActionTypeDD() {
        return "OEntityAction_entityActionType";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="actor">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    OUser actor ;
    public OUser getActor() {
        return actor;
    }
    public void setActor(OUser actor) {
        this.actor = actor;
    }
    
     /* DD actor GETTER */
    public String getActorDD(){
        return "EntityAuditTrail_actor";  
    }
    
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="actionDTime">
    @Column(nullable=false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date actionDTime;
    public Date getActionDTime() {
        return actionDTime;
    }
    public void setActionDTime(Date actionDTime) {
        this.actionDTime = actionDTime;
    }
    
   /* DD actionDTime GETTER */
    public String getActionDTimeDD(){
        return "EntityAuditTrail_actionDTime";  
    }
    
    // </editor-fold>    
    
    // <editor-fold defaultstate="collapsed" desc="ignoreInSync">
    private OUser ignoredBy ;
    public OUser getIgnoredBy() {
        return ignoredBy;
    }
    public void setIgnoredBy(OUser ignoredBy) {
        this.ignoredBy = ignoredBy;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="ignoringDTime">
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date ignoringDTime;
    public Date getIgnoringDTime() {
        return ignoringDTime;
    }
    public void setIgnoringDTime(Date ignoringDTime) {
        this.ignoringDTime = ignoringDTime;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="ignoringReason">
    private boolean ignoringReason ;
    public boolean isIgnoringReason() {
        return ignoringReason;
    }
    public void setIgnoringReason(boolean ignoringReason) {
        this.ignoringReason = ignoringReason;
    }
    // </editor-fold>
}
