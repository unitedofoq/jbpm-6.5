package com.unitedofoq.fabs.core.udc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.fabs.core.validation.FABSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.udc.UDCServiceRemote",
beanInterface = UDCServiceRemote.class)
public class UDCService implements UDCServiceRemote {
final static Logger logger = LoggerFactory.getLogger(UDCService.class);
    @EJB
    private OFunctionServiceRemote functionService;
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private UserMessageServiceRemote usrMsgService;
    @EJB
    private DDServiceRemote ddService;

    /**
     * gets the UDCs that belong to a specific UDC type.
     *
     * @param udcTypeDBID The DBID of the UDC Type.
     * @param user The user requesting the UDCs.
     *
     * @return The list of UDCs that belong to the passed UDC type.
     */
    public List<UDC> getUDCs(long udcTypeDBID, OUser loggedUser) {
        logger.debug("Entering");
        List<String> conditions = new ArrayList<String>();
        conditions.add("type.dbid='" + udcTypeDBID + "'");

        //  retrieve UDCs
        try {
            logger.debug("Retrning");
            return oem.loadEntityList("UDC", conditions, null,
                    null /*sorts*/,
                    loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    @Override
    public OFunctionResult createUDCTypeDD(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(odm, loggedUser, UDCType.class);
        if (inputValidationErrors != null) {
            logger.trace("Returning with: {}",inputValidationErrors);
            return inputValidationErrors;
        }
        // </editor-fold>
        OFunctionResult oFR = new OFunctionResult();
        try {
            UDCType udcType = (UDCType) odm.getData().get(0);
            if (udcType.getCode().isEmpty()) {
                oFR.addWarning(usrMsgService.getUserMessage(
                        "UDCTypeHasNoCode", Collections.singletonList(udcType.getValue()), loggedUser));
                logger.debug("Returning");
                return oFR;
            }
            DD udcTypeDD = ddService.getDD(udcType.getCode(), loggedUser);
            if (udcTypeDD == null) {
                udcTypeDD = new DD();
            } else if (udcType.getOmodule().equals(udcTypeDD.getOmodule())) {
                logger.debug("Returning");
                return oFR;
            }
            udcTypeDD.setOmodule(udcType.getOmodule());
            udcTypeDD.setName(udcType.getCode());

            udcTypeDD.setLabelTranslated(udcType.getValue());
            udcTypeDD.setHeaderTranslated(udcType.getValue());

            udcTypeDD.setControlType((UDC) oem.loadEntity(UDC.class.getSimpleName(), 6001/*
                     DropDown DBID*/, null, null, loggedUser));

            udcTypeDD.setLookupDropDownDisplayField("UDC.value");
            udcTypeDD.setLookupFilter("type.dbid=" + udcType.getDbid());
            udcTypeDD.setControlSize(20);
            oem.saveEntity(udcTypeDD, loggedUser);

        } catch (Exception ex) {
            if (ex instanceof FABSException) {
                oFR.addError(usrMsgService.getUserMessage("ModuleError", loggedUser));
                logger.error("Exception (FABS Exception) thrown: ",ex);
            } else {
                logger.error("exception thrown: ",ex);
                oFR.addError(usrMsgService.getUserMessage(
                        "SystemInternalError", loggedUser));
            }
        }
        logger.debug("Returning");
        return oFR;
    }

    @Override
    public OFunctionResult deleteUDCTypeDD(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Returning");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(odm, loggedUser, UDCType.class);
        if (inputValidationErrors != null) {
            logger.trace("Returning with: ",inputValidationErrors);
            return inputValidationErrors;
        }
        // </editor-fold>
        OFunctionResult oFR = new OFunctionResult();
        try {
            UDCType udcType = (UDCType) odm.getData().get(0);
            if (udcType.getCode().isEmpty()) {
                oFR.addWarning(usrMsgService.getUserMessage(
                        "UDCTypeHasNoCode", Collections.singletonList(udcType.getValue()), loggedUser));
                logger.debug("Returning");
                return oFR;
            }
            DD udcTypeDD = ddService.getDD(udcType.getCode(), loggedUser);
            if (udcTypeDD == null) {
                logger.debug("Returning as udcTypeDD equals Null");
                return oFR;
            }
            oem.deleteEntity(udcTypeDD, loggedUser);

        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            oFR.addError(usrMsgService.getUserMessage(
                    "SystemInternalError", loggedUser));
        }
        logger.debug("Returning");
        return oFR;
    }
    @Override
    public UDCLite getUDC(long udcDBID, OUser loggedUser) {
logger.debug("Entering");
        UDCLite cLite= new  UDCLite();        
        try {
            oem.getEM(loggedUser);           
            cLite = (UDCLite) oem.
                    createListNamedQuery("getUDC", loggedUser, "dbid", udcDBID,"langid",
                    loggedUser.getFirstLanguage().getDbid());
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return cLite;
        }
    }
    /**
     *
     * @param typeDBID
     * @param loggedUser
     * @return
     */
    @Override
    public List<UDCLite> getLiteUDCs(long typeDBID, OUser loggedUser) {
logger.debug("Entering");
        List<UDCLite> cLites= new  ArrayList<UDCLite>();        
        try {
            oem.getEM(loggedUser);           
            cLites = (List<UDCLite>) oem.
                    createListNamedQuery("getUDCs", loggedUser, "dbid", typeDBID,"langid",
                    loggedUser.getFirstLanguage().getDbid());
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return cLites;
        }
    }
}
