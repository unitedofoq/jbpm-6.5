package com.unitedofoq.fabs.core.uiframework.navigationmenu;

import com.unitedofoq.fabs.core.Exception.BasicException;
import com.unitedofoq.fabs.core.Exception.SystemExecption;
import com.unitedofoq.fabs.core.ExceptionHandler.catcher.factory.ExceptionHandler;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.function.OMenu;
import com.unitedofoq.fabs.core.function.OMenuFunction;

import com.unitedofoq.fabs.core.security.user.ORole;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.RoleMenu;
import com.unitedofoq.fabs.core.security.user.RoleUser;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;
import javax.interceptor.Interceptors;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.primefaces.component.column.Column;
import org.primefaces.component.graphicimage.GraphicImage;
import org.primefaces.component.megamenu.MegaMenu;
import org.primefaces.component.menuitem.MenuItem;
import org.primefaces.component.submenu.Submenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author satef
 */
public class NavMenuManagerImpl {
    
    final static Logger logger = LoggerFactory.getLogger(NavMenuManagerImpl.class);
    @EJB
    OEntityManagerRemote oem;
    @EJB
    NMDataLoader nmdl;
    private static NavMenuManagerImpl singleIns;
    public ConcurrentHashMap<String, ConcurrentHashMap> MainCach = new ConcurrentHashMap<String, ConcurrentHashMap>();
    HashMap<String, ConcurrentHashMap> DTOCach;
    public ExpressionFactory exFactory;
    private String RESOURCES_FOUND = "/resources/found.png";
    public String customedDynamicStyle;
    public HashMap<String, String> imageMap;
    OUser logged_user1;
    
    static {
        singleIns = new NavMenuManagerImpl();
    }
    
    public static NavMenuManagerImpl getSingleInstance() {
        return singleIns;
    }
    
    @Interceptors(ExceptionHandler.class)
    public void Init(NMDataLoader nmdl) throws BasicException {
        logger.debug("Entering");
        exFactory = FacesContext.getCurrentInstance().getApplication().getExpressionFactory();
        buildGUICach(nmdl);
        logger.debug("Returning");
    }
    
    @Interceptors(ExceptionHandler.class)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public HtmlPanelGrid getUserPanel(OUser logged_user) {
        logger.debug("Entering");
        logged_user1 = logged_user;
        int mainMegaMenuItemCount = 0;
        exFactory = FacesContext.getCurrentInstance().getApplication().getExpressionFactory();
        ConcurrentHashMap<String, ArrayList<Submenu>> roleMap = MainCach.get(String.valueOf(logged_user.getTenant().getId()));
        List<RoleUser> ROLES = logged_user.getRoleUsers();
        HashMap<String, Submenu> menus = new HashMap();
        
        for (int i = 0; i < ROLES.size(); i++) {
            logger.trace("Looping on Roles");
            if (ROLES.get(i).getOrole().isActive()) {
                if (!ROLES.get(i).isInActive()) {
                    String roleID = String.valueOf(ROLES.get(i).getOrole().getDbid());
                    List<Submenu> roleMenus = roleMap.get(Long.parseLong(roleID));
                    if (roleMenus != null) {
                        for (int m = 0; m < roleMenus.size(); m++) {
                            menus.put(roleMenus.get(m).getId().substring(roleMenus.get(m).getId().lastIndexOf("_")), roleMenus.get(m));
                        }
                    }
                }
            }
        }
        List<String> menuKeys = new ArrayList(menus.keySet());
        HtmlPanelGrid userPanel = new HtmlPanelGrid();
        MegaMenu megaMenu = new MegaMenu();
        megaMenu.setId("ID_MegaMenu");
        megaMenu.setAutoDisplay(false);
        for (int k = 0; k < menus.size(); k++) {
            logger.trace("Looping on menus");
            int subMenuColumnsCount = 0;
            multiLangSubMenu item = filterMenuFunctions((multiLangSubMenu) menus.get(menuKeys.get(k)), logged_user);
            int itemIndex = Integer.parseInt(item.getId().substring(item.getId().indexOf("-") + 1));
            boolean added = false;
            for (int i = 0; i < megaMenu.getChildren().size(); i++) {
                if (null != megaMenu || null != megaMenu.getChildren() || null != megaMenu.getChildren().get(i)) {
                    logger.trace("Looping on SubMenus: {}", megaMenu.getChildren().get(i).getId());
                }
                String id = megaMenu.getChildren().get(i).getId();
                int index = Integer.parseInt(id.substring(id.indexOf("-") + 1));
                if (itemIndex <= index) {
                    megaMenu.getChildren().add(i, item);
                    added = true;
                    break;
                }
            }
            if (!added) {
                megaMenu.getChildren().add(item);
            }
            if (menus.get(menuKeys.get(k)).getChildCount() > 0) {
                subMenuColumnsCount = subMenuColumnsCount + menus.get(menuKeys.get(k)).getChildCount() - 1;
            }
            calculateULTagPosition(mainMegaMenuItemCount, subMenuColumnsCount, menus.size());
            mainMegaMenuItemCount++;
        }
        megaMenu.setStyleClass("mega_menu_mainx");
        userPanel.getChildren().add(megaMenu);
        logger.debug("Returning");
        return userPanel;
        
    }
    
    @Interceptors(ExceptionHandler.class)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public multiLangSubMenu filterMenuFunctions(multiLangSubMenu item, OUser logged_user) {
        logger.debug("Entering");
        int funLang;
        Long userLang = logged_user.getFirstLanguage().getDbid();
        item.setRendered(true, userLang.toString());
        List<UIComponent> megaItem = item.getChildren();
        for (UIComponent com : megaItem) {
            multiLangSubMenu subItem = (multiLangSubMenu) com.getChildren().get(1);
            subItem.setRendered(true, userLang.toString());
            List<UIComponent> funList = subItem.getChildren();
            logger.trace("Looping on UI Components");
            for (int i = 0; i < funList.size(); i++) {
                funList.get(i).setRendered(true);
                String funId = funList.get(i).getId();
                funId = funId.substring(0, funId.lastIndexOf("-"));
                funLang = Integer.parseInt(funId.substring(funId.lastIndexOf("_") + 1));
                if (funLang != userLang.intValue()) {
                    funList.get(i).setRendered(false);
                }
            }
        }
        logger.debug("Returning");
        return item;
    }
    
    @Interceptors(ExceptionHandler.class)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void buildGUICach(NMDataLoader nmdl) throws BasicException {
        logger.debug("Entering");
        InitialContext context = null;
        try {
            context = new InitialContext();
            nmdl = (NMDataLoader) context.lookup("java:global/ofoq/com.unitedofoq.fabs.core.uiframework.navigationmenu.NMDataLoader");
            MainCach.clear();
            DTOCach = nmdl.constractDTOCach();
        } catch (NamingException ex) {
            logger.error("Exception throws: ", ex);
            throw new SystemExecption(ex.toString(true), null);
        }
        ArrayList<String> tanentList = new ArrayList<String>(DTOCach.keySet());
        for (int i = 0; i < tanentList.size(); i++) {
            logger.trace("Looping on Tanents");
            if (!tanentList.get(i).equals("index") && !tanentList.get(i).equals("images")) {
                ConcurrentHashMap role_menu = DTOCach.get(tanentList.get(i));
                ArrayList<String> roles_List = new ArrayList<String>(role_menu.keySet());
                ConcurrentHashMap<String, ArrayList<Submenu>> GUIInstances = new ConcurrentHashMap(1);
                for (int k = 0; k < roles_List.size(); k++) {
                    logger.trace("Looping on roles_List");
                    ArrayList<NavigationMenuDTO> mainMenus = (ArrayList<NavigationMenuDTO>) role_menu.get(roles_List.get(k));
                    GUIInstances.put(roles_List.get(k), new ArrayList<Submenu>());
                    for (int n = 0; n < mainMenus.size(); n++) {
                        if (null != mainMenus || null != mainMenus.get(n)) {
                            logger.trace("Looping on Main menus, {}", mainMenus.get(n).getMenuName());
                        }
                        System.out.println(mainMenus.get(n).getMenuName());
                        GUIInstances.get(roles_List.get(k)).add(buildMenuItemInstance(mainMenus.get(n)));
                    }
                }
                MainCach.put(tanentList.get(i), GUIInstances);
            }
        }
        logger.debug("Returning");
    }
    
    public void buildTanentGUICach(NMDataLoader nmdl, String tanentID) throws BasicException {
        logger.debug("Entering");
        InitialContext context = null;
        try {
            context = new InitialContext();
            nmdl = (NMDataLoader) context.lookup("java:global/ofoq/com.unitedofoq.fabs.core.uiframework.navigationmenu.NMDataLoader");
        } catch (NamingException ex) {
            logger.error("Exception thrown: ", ex);
            throw new SystemExecption(ex.toString(true), null);
        }
        if (tanentID.contains(" ")) {
            tanentID = tanentID.split(" ")[1];
            logger.trace("Splitting Tanent ID");
        }
        HashMap<String, ConcurrentHashMap> DTOCach1 = nmdl.ConstractTanentDTOCach(tanentID);
        
        ArrayList<String> tanentList = new ArrayList<String>(DTOCach1.keySet());
        for (int i = 0; i < tanentList.size(); i++) {
            if (null != tanentList) {
                logger.trace("Looping on Tanents, {}", tanentList.get(i));
            }
            if (!tanentList.get(i).equals("index") && !tanentList.get(i).equals("images")) {
                ConcurrentHashMap role_menu = DTOCach1.get(tanentList.get(i));
                ArrayList<String> roles_List = new ArrayList<String>(role_menu.keySet());
                ConcurrentHashMap<String, ArrayList<Submenu>> GUIInstances = new ConcurrentHashMap(1);
                for (int k = 0; k < roles_List.size(); k++) {
                    if (null != roles_List) {
                        logger.trace("Looping on roles_List, {}", roles_List.get(k));
                    }
                    ArrayList<NavigationMenuDTO> mainMenus = (ArrayList<NavigationMenuDTO>) role_menu.get(roles_List.get(k));
                    GUIInstances.put(roles_List.get(k), new ArrayList<Submenu>());
                    for (int n = 0; n < mainMenus.size(); n++) {
                        if (null != mainMenus || null != mainMenus.get(n)) {
                            logger.trace("Looping on Main Menus: {}", mainMenus.get(n).getMenuName());
                        }
                        System.out.println(mainMenus.get(n).getMenuName());
                        GUIInstances.get(roles_List.get(k)).add(buildMenuItemInstance(mainMenus.get(n)));
                    }
                }
                MainCach.put(tanentList.get(i), GUIInstances);
            }
        }
        logger.debug("Returning");
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    @Interceptors(ExceptionHandler.class)
    public Submenu buildMenuItemInstance(NavigationMenuDTO NVD) {
        logger.debug("Enetering");
        multiLangSubMenu submenu = new multiLangSubMenu();
        Class[] parms2 = new Class[]{ActionEvent.class};
        MethodExpression functionMethodExpr = exFactory.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),
                "#{" + "DockMenuBean" + ".functionActionListener}", null, parms2);
        MethodExpressionActionListener functionActionListener = new MethodExpressionActionListener(functionMethodExpr);
        Submenu separatorItemLT = new Submenu();
        separatorItemLT.setStyleClass("mega_menu_separator_lt");
        
        int mainMegaMenuItemCount = 0;
        int subMenuColumnsCount = 0;
        
        submenu.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_SubMenu_"
                + NVD.getMenuId() + "-" + NVD.getMenuSortIndex());
        submenu.setLabel(NVD.getMenuName());
        submenu.setSecondLang(NVD.getMenuNameTrans());
        submenu.setFirstLang(NVD.getMenuName());
        submenu.setStyleClass("mega_menu_submenu");
        Column fcolumn = new Column();
        fcolumn.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                + "_Column_" + NVD.getMenuId() + "_-1");
        fcolumn.setStyleClass("megamenu-submenu-subcolumn");
        multiLangSubMenu fSubmenu = new multiLangSubMenu();
        fSubmenu.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                + "_fSubmenu_");
        fSubmenu.setLabel(NVD.getMenuName());
        fSubmenu.setSecondLang(NVD.getMenuNameTrans());
        fSubmenu.setFirstLang(NVD.getMenuName());
        Long menuFunctionDbid;
        int functionLangID;
        List<OfunctionDTO> functions = NVD.getFunctions();
        OfunctionDTO funItem;
        for (int i = 0; i < functions.size(); i++) {
            logger.trace("Looping on Functions");
            funItem = functions.get(i);
            if (funItem != null) {
                menuFunctionDbid = funItem.getFunctiondbid();
                functionLangID = funItem.getLangdbid();
                MenuItem item = new MenuItem();
                item.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                        + "_Function_" + menuFunctionDbid + "_" + functionLangID + "-" + funItem.getSortIndex());
                item.setValue(funItem.getFunctionName());
                item.addActionListener(functionActionListener);
                item.setAjax(true);
                item.setStyleClass("mega_menu_submenu_item");
                int itemIndex = funItem.getSortIndex();
                boolean added = false;
                for (int f = 0; f < fSubmenu.getChildren().size(); f++) {
                    if (null != fSubmenu || null!=fSubmenu.getChildren())
                        logger.trace("Looping on Function SubMenus: {}", fSubmenu.getChildren().get(f));
                    String id = fSubmenu.getChildren().get(f).getId();
                    int index = Integer.parseInt(id.substring(id.lastIndexOf("-") + 1));
                    if (itemIndex <= index) {
                        fSubmenu.getChildren().add(f, item);
                        added = true;
                        break;
                    }
                }
                if (!added) {
                    fSubmenu.getChildren().add(item);
                }
                
            }
        }
        //<editor-fold defaultstate="collapsed" desc="GraphicImage above each function column">
        // create above column image in the column that has no header
        GraphicImage fColumnGImg = new GraphicImage();
        fColumnGImg.setId("IM" + NVD.getMenuId()
                + FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        String fColumnGImgPath = NVD.getImageURL();
        if (fColumnGImgPath != null) {
            if (fColumnGImgPath != null && !fColumnGImgPath.equals("")) {
                if (fColumnGImgPath.contains("/")) {
                    fColumnGImg.setUrl(fColumnGImgPath.substring(fColumnGImgPath.lastIndexOf("/")));
                } else if (fColumnGImgPath.contains("\\")) {
                    String s1 = "/" + fColumnGImgPath.substring(fColumnGImgPath.lastIndexOf("\\") + 1);
                    fColumnGImg.setUrl(s1);
                }
            }
        } else {
            fColumnGImg.setUrl("/resources/default.png");
        }
        fColumnGImg.setStyleClass("megamenu-col-img");
        fcolumn.getChildren().add(fColumnGImg);
        //</editor-fold>
        if (fSubmenu.getChildCount() != 0) {
            fSubmenu.setStyleClass("mega_menu_submenu_header");
            fcolumn.getChildren().add(fSubmenu);
            
            submenu.getChildren().add(fcolumn);
            subMenuColumnsCount++;
        }
        
        List<OMenuDTO> allSubMenus = NVD.getChildMenues();
        if (allSubMenus == null) {
            allSubMenus = new ArrayList<OMenuDTO>();
        }
        for (OMenuDTO subMenu : allSubMenus) {
            logger.trace("Looping on submenus");
            Column column = new Column();
            column.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                    + "_Column_" + subMenu.getDbid() + "_" + subMenu.getSortIndex());
            multiLangSubMenu s = new multiLangSubMenu();
            s.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                    + "_SubMenu2_" + subMenu.getDbid());
            s.setStyleClass("mega_menu_submenu_header");
            s.setLabel(subMenu.getName());
            s.setSecondLang(subMenu.getNameTranslated());
            s.setFirstLang(subMenu.getName());
            //<editor-fold defaultstate="collapsed" desc="GraphicImage above each column">
            // create above column image that has subcolumn header
            GraphicImage columnGImg = new GraphicImage();
            columnGImg.setId("IM" + subMenu.getDbid()
                    + FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
            String columnGImgPath = subMenu.getImageURL();
            if (columnGImgPath != null) {
                if (columnGImgPath != null && !columnGImgPath.equals("")) {
                    if (columnGImgPath.contains("/")) {
                        columnGImg.setUrl(columnGImgPath.substring(columnGImgPath.lastIndexOf("/")));
                    } else if (columnGImgPath.contains("\\")) {
                        String s1 = "/" + columnGImgPath.substring(columnGImgPath.lastIndexOf("\\") + 1);
                        columnGImg.setUrl(s1);
                    }//.substring(1));
                }
            } else {
                columnGImg.setUrl("/resources/default.png");
            }
            columnGImg.setStyleClass("megamenu-col-img");
            column.getChildren().add(columnGImg);
            //</editor-fold>
            column.getChildren().add(s);
            column.setStyleClass("megamenu-submenu-subcolumn");
            subMenuColumnsCount++;
            int itemIndex = subMenu.getSortIndex();
            boolean added = false;
            for (int f = 0; f < submenu.getChildren().size(); f++) {
                logger.trace("Looping on Submenus Children");
                String id = submenu.getChildren().get(f).getId();
                int index = Integer.parseInt(id.substring(id.lastIndexOf("_") + 1));
                if (itemIndex <= index) {
                    submenu.getChildren().add(f, column);
                    added = true;
                    break;
                }
            }
            if (!added) {
                submenu.getChildren().add(column);
            }
            functions = subMenu.getFunctions();
            
            Long functionDbid;
            
            OfunctionDTO subFunItem;
            if (functions == null) {
                functions = new ArrayList<OfunctionDTO>();
            }
            for (int j = 0; j < functions.size(); j++) {
                logger.trace("Looping on Functions");
                subFunItem = functions.get(j);
                if (subFunItem != null) {
                    MenuItem item = new MenuItem();
                    functionDbid = subFunItem.getFunctiondbid();
                    functionLangID = subFunItem.getLangdbid();
                    item.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId()
                            + "_Function_" + functionDbid + "_" + functionLangID + "-" + subFunItem.getSortIndex());
                    item.setValue(subFunItem.getFunctionName());
                    item.addActionListener(functionActionListener);
                    item.setAjax(true);
                    item.setStyleClass("mega_menu_submenu_item");
                    int funitemIndex = subFunItem.getSortIndex();
                    boolean funadded = false;
                    for (int f = 0; f < s.getChildren().size(); f++) {
                        String id = s.getChildren().get(f).getId();
                        int index = Integer.parseInt(id.substring(id.lastIndexOf("-") + 1));
                        if (funitemIndex <= index) {
                            s.getChildren().add(f, item);
                            funadded = true;
                            break;
                        }
                    }
                    if (!funadded) {
                        s.getChildren().add(item);
                    }
                }
            }
        }

        //Adding image column to the menu
        Column mainItemImgColumn = new Column();
        mainItemImgColumn.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_ImgCol");
        GraphicImage mainItemImg = new GraphicImage();
        mainItemImg.setId("IM" + NVD.getMenuId()
                + FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        mainItemImg.setUrl("XXXXXXX");
        mainItemImgColumn.setStyleClass("megamenu-main-img-col");
        mainItemImg.setStyleClass("megamenu-main-img");
        mainItemImgColumn.getChildren().add(mainItemImg);
        return submenu;
    }
    
    @Interceptors(ExceptionHandler.class)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    private String calculateULTagPosition(int mainMegaMenuItemCount, int subMenuColumnsCount, int omenuSize) {
        if (mainMegaMenuItemCount == 0) {
            customedDynamicStyle = "<style id='generatedStyle'>";
        }
        String ulTagSeq = " ";
        for (int i = 0; i < mainMegaMenuItemCount; i++) {
            ulTagSeq += "+ li";
        }
        int maxExpectedSize = 7;
        
        if ((mainMegaMenuItemCount + 1) >= maxExpectedSize) {
            customedDynamicStyle += "\n .mega_menu_main > ul > li:first-child" + ulTagSeq + " > ul {"
                    + "right: -" + (0) + "px !important;"
                    + "left: auto !important;"
                    + "position: absolute!important;"
                    + "width:" + (subMenuColumnsCount - 1) * 195 + "px!important;"
                    + "}";
        } else if (subMenuColumnsCount > (maxExpectedSize - (mainMegaMenuItemCount + 1))) {
            
            customedDynamicStyle += "\n .mega_menu_main > ul > li:first-child" + ulTagSeq + " > ul {"
                    + "right: -" + 135 * (maxExpectedSize - mainMegaMenuItemCount - 1) + "px !important;"
                    + "left: auto !important;"
                    + "position: absolute!important;"
                    + "width:" + (subMenuColumnsCount - 1) * 195 + "px!important;"
                    + "}";
        } else if (subMenuColumnsCount == (maxExpectedSize - (mainMegaMenuItemCount + 1))) {
            customedDynamicStyle += "\n .mega_menu_main > ul > li:first-child" + ulTagSeq + " > ul {"
                    // + "right: -" + 135 * (maxExpectedSize - mainMegaMenuItemCount - 1) + "px !important;"
                    + "left: 0px !important;"
                    + "position: absolute!important;"
                    + "width:" + (subMenuColumnsCount - 1) * 195 + "px!important;"
                    + "}";
            
        }
        if (mainMegaMenuItemCount == omenuSize - 1) {
            customedDynamicStyle += "</style>";
        }
        
        return "";
    }
    
    @Interceptors(ExceptionHandler.class)
    public void deattachMenu(RoleMenu RM) {
        try {
            String tanentName = logged_user1.getTenant().getName();
            buildTanentGUICach(null, tanentName);
        } catch (Exception ex) {
            logger.error("Exception thrown :", ex);
        }
    }
    
    @Interceptors(ExceptionHandler.class)
    public void attachMenu(RoleMenu RM) {
        try {
            String tanentName = logged_user1.getTenant().getName();
            buildTanentGUICach(null, tanentName);
        } catch (Exception ex) {
            logger.error("Exception thrown :", ex);
        }
    }
    
    @Interceptors(ExceptionHandler.class)
    public void updateMenu(OMenu M) {
        try {
            String tanentName = logged_user1.getTenant().getName();
            buildTanentGUICach(null, tanentName);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
    }
    
    @Interceptors(ExceptionHandler.class)
    public void addMenu(OMenu M) {
        try {
            String tanentName = logged_user1.getTenant().getName();
            buildTanentGUICach(null, tanentName);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
    }
    
    @Interceptors(ExceptionHandler.class)
    public void addRole(ORole R) {
        try {
            String tanentName = logged_user1.getTenant().getName();
            buildTanentGUICach(null, tanentName);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
    }
    
    public boolean searchDataCach(int searchType, NavigationMenuDTO obj, String childID) {
        // 1 for search submenu into mainMenu object
        //2 for search mainMenu into role
        boolean result = false;
        if (searchType == 1) {
            ArrayList<OMenuDTO> chileds = (ArrayList<OMenuDTO>) obj.getChildMenues();
            if (chileds != null) {
                for (int i = 0; i < chileds.size(); i++) {
                    if ((chileds.get(i).getDbid()).toString().equals(childID)) {
                        result = true;
                        break;
                    }
                }
            }
        }
        return result;
    }
    
    @Interceptors(ExceptionHandler.class)
    public void attachFunction(OMenuFunction MF) {
        try {
            String tanentName = logged_user1.getTenant().getName();
            buildTanentGUICach(null, tanentName);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
    }
    
    @Interceptors(ExceptionHandler.class)
    public void deattachFunction(OMenuFunction MF) {
        try {
            String tanentName = logged_user1.getTenant().getName();
            buildTanentGUICach(null, tanentName);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
    }
    
    @Interceptors(ExceptionHandler.class)
    public void updateFunction(OMenuFunction MF) {
        try {
            String tanentName = logged_user1.getTenant().getName();
            buildTanentGUICach(null, tanentName);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
    }
    
    @Interceptors(ExceptionHandler.class)
    public void updateFunctionSource(OFunction fun) {
        try {
            String tanentName = logged_user1.getTenant().getName();
            buildTanentGUICach(null, tanentName);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
    }
    
    @Interceptors(ExceptionHandler.class)
    public void onUpdate(Object obj) {
        if (obj instanceof RoleMenu) {
            logger.trace("this is a Role Menu");
            RoleMenu RM = (RoleMenu) obj;
            deattachMenu(RM);
        } else if (obj instanceof OMenuFunction) {
            logger.trace("this is an OMenuFunction");
            OMenuFunction MF = (OMenuFunction) obj;
            updateFunction(MF);
        } else if (obj instanceof OMenu) {
            logger.trace("this is an OMenu");
            OMenu M = (OMenu) obj;
            updateMenu(M);
        } else if (obj instanceof OFunction) {
            logger.trace("this is an OFunction");
            OFunction fun = (OFunction) obj;
            updateFunctionSource(fun);
        }
    }
    
    @Interceptors(ExceptionHandler.class)
    public void onRemove(Object obj) {
        logger.debug("Entering");
        if (obj instanceof RoleMenu) {
            RoleMenu RM = (RoleMenu) obj;
            deattachMenu(RM);
        } else if (obj instanceof OMenuFunction) {
            OMenuFunction MF = (OMenuFunction) obj;
            deattachFunction(MF);
        }
        logger.debug("Returning");
    }
    
    @Interceptors(ExceptionHandler.class)
    public void onAdd(Object obj) {
        logger.debug("Entering");
        if (obj instanceof RoleMenu) {
            RoleMenu RM = (RoleMenu) obj;
            attachMenu(RM);
        } else if (obj instanceof OMenuFunction) {
            OMenuFunction MF = (OMenuFunction) obj;
            attachFunction(MF);
        } else if (obj instanceof OMenu) {
            OMenu M = (OMenu) obj;
            addMenu(M);
        } else if (obj instanceof ORole) {
            ORole R = (ORole) obj;
            addRole(R);
        }
        logger.debug("Returning");
    }
    
    @Interceptors(ExceptionHandler.class)
    public void onUpdateTranslation(Object obj) {
        logger.debug("Entering");
        try {
            String tanentName = logged_user1.getTenant().getName();
            buildTanentGUICach(null, tanentName);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        logger.debug("Returning");
    }
}
