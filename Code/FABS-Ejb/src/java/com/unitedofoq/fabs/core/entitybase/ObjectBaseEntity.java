/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitybase;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 *
 * @author AbuBakr
 */
@MappedSuperclass
public class ObjectBaseEntity extends BaseEntity{
   
    @Column(nullable=false)
    protected boolean active = true;
    public String getActiveDD(){
        return "ObjectBaseEntity_active";
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
