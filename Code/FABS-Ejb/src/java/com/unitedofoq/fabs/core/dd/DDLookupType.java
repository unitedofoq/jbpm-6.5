/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.dd;

/**
 *
 * @author lap
 */
 public enum DDLookupType {

        DDLT_DRPDWN, DDLT_SCREEN, DDLT_FIELDEXP, DDLT_LTRLDRPDN, DDLT_MLTILVLSCRN, DDLT_NONE, DDLT_MULTISELECTIONLIST,
        DDLT_FLTRBLDR, DDLT_TREE,DDLT_AUTOCOMPLETE
    };
