/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.screen;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;

/**
 *
 * @author nkhalil
 */
@DiscriminatorValue(value="MultiList")
@Entity
@ChildEntity(fields={"screenInputs", "screenOutputs","screenFilter","screenFunctions"})
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class MultiListScreen extends SingleEntityScreen {

}
