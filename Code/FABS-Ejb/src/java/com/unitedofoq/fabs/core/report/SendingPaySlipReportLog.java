/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.report;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;

@Entity
public class SendingPaySlipReportLog extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="employee_Name">
    @Column
    private String employee_Name;

    public void setEmployee_Name(String employee_Name) {
        this.employee_Name = employee_Name;
    }

    public String getEmployee_Name() {
        return employee_Name;
    }

    public String getEmployee_NameDD() {
        return "sendingPaySlipReportLog_employee_Name";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="email">
    @Column
    private String email;

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public String getEmailDD() {
        return "sendingPaySlipReportLog_email";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="employee_DBID">
    @Column
    private long employee_DBID;

    public void setEmployee_DBID(long employee_DBID) {
        this.employee_DBID = employee_DBID;
    }

    public long getEmployee_DBID() {
        return employee_DBID;
    }

    public String getEmployee_DBIDDD() {
        return "sendingPaySlipReportLog_employee_DBID";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="status">
    @Column
    private String status;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public String getStatusDD() {
        return "sendingPaySlipReportLog_status";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="message">
    @Column
    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getMessageDD() {
        return "sendingPaySlipReportLog_message";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="user_DBID">
    @Column
    private long user_DBID;

    public void setUser_DBID(long user_DBID) {
        this.user_DBID = user_DBID;
    }

    public long getUser_DBID() {
        return user_DBID;
    }

    public String getUser_DBIDDD() {
        return "sendingPaySlipReportLog_user_DBID";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="month">
    @Column
    private String month;

    public void setMonth(String month) {
        this.month = month;
    }

    public String getMonth() {
        return month;
    }

    public String getMonthDD() {
        return "sendingPaySlipReportLog_month";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="sending_Date">
    @Column
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date sending_Date;

    public void setSending_Date(Date sending_Date) {
        this.sending_Date = sending_Date;
    }

    public Date getSending_Date() {
        return sending_Date;
    }

    public String getSending_DateDD() {
        return "sendingPaySlipReportLog_sending_Date";
    }
    // </editor-fold>

}