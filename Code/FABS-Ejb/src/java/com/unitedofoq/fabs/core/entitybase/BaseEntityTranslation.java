/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitybase;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

import com.unitedofoq.fabs.core.udc.UDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mibrahim
 */
@MappedSuperclass
public class BaseEntityTranslation implements Serializable {

    @Id
    protected long entityDBID;
    final static Logger logger = LoggerFactory.getLogger(BaseEntityTranslation.class);
    /**
     *
     */
    @Id
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    protected UDC language;

    public long getEntityDBID() {
        return entityDBID;
    }

    public void setEntityDBID(long entityDBID) {
        this.entityDBID = entityDBID;
    }

    public UDC getLanguage() {
        return language;
    }

    public void setLanguage(UDC language) {
        this.language = language;
    }

    // <editor-fold defaultstate="collapsed" desc="Getter & Setter Invokes Functions">
    /**
     * Invokes the getter of the field (fieldName) which is declared in the
     * entity or one of it's super classes
     *
     * It considers the boolean field
     *
     * @param fieldExp The name of the field which is got. It supports child
     * fields, i.e. "."
     *
     * @return Field Value in case of Success
     * <br>Null in case of field value is null, error (field not found), one of
     * the expression chain is null
     */
    public Object invokeGetter(String fieldExp)
            throws NoSuchMethodException {
        logger.debug("Entering");
        if (fieldExp == null) {
            logger.debug("Returning with Null");
            return null;
        }
        if (fieldExp.contains(".")) {
            // Expression, get the full expression values
            BaseEntityTranslation entityTranslation = this;
            BaseEntity entity = null;
            while (fieldExp.indexOf(".") != -1) {
                String currentField = fieldExp.substring(0, fieldExp.indexOf("."));
                fieldExp = fieldExp.substring(fieldExp.indexOf(".") + 1);
                if (entity == null) {
                    entity = (BaseEntity) entityTranslation.invokeGetter(currentField);
                } else {
                    entity = (BaseEntity) entity.invokeGetter(currentField);
                }
                if (entity == null) {
                    logger.debug("Returning with Null");
                    return null;
                }
            }
            logger.debug("Returning with Null");
            //  At this point, 'fieldName' contains the the last field.
            return entity.invokeGetter(fieldExp);
        } else {
            Field field = getClassField(getClass(), fieldExp);
            if (field == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("Field Not Found In Class. Field Name: {}, Class: {}", fieldExp, getClass());
                // </editor-fold>
                logger.debug("Returning with Null");
                return null;
            }
            String methodName;
            if (field.getType().getName().equals("boolean")) {
                methodName = "is";
            } else {
                methodName = "get";
            }

            // Set first letter to uppercase
            methodName += Character.toString(fieldExp.charAt(0)).toUpperCase()
                    + fieldExp.substring(1);

            try {
                Method method = getClass().getMethod(methodName);
                logger.debug("Returning ");
                return method.invoke(this);
            } catch (Exception ex) {
                if (ex.getClass().getName().equals("java.lang.NoSuchMethodException")) {
                    throw new NoSuchMethodException(ex.getMessage());
                } else {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.error("Exception thrown: ", ex);
                    logger.trace("Field Name: {}", fieldExp);
                    // </editor-fold>
                }
            }
            logger.debug("Returning with Null");
            return null;
        }
    }

    public Object getFieldValue(Field field)
            throws NoSuchMethodException {
        logger.debug("Entering");
        try {
            Object fieldValue = field.get(this);
            logger.debug("Returning");
            return fieldValue;
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            logger.debug("Returning with Null");
            return null;
        }
    }

    /**
     * Invokes the getter of the field, of the BaseEntity
     */
    public Object invokeGetter(Field field)
            throws NoSuchMethodException {
        return invokeGetter(field.getName());
    }

    /**
     * Invokes the setter of the field (fieldName) which is declared in the
     * entity or one of it's super classes
     *
     * It doesn't call em.clear(), so, the set value will be persisted if entity
     * is persisted
     *
     * @param fieldName The name of the field which is set Is a direct field and
     * not a field expression, i.e. shouldn't contain "."
     * @param value Should be of the same type like the field's
     * @param types field setter function parameter types Usually is not needed,
     * even if the field is a list Is passed to getMethod()
     */
    public void invokeSetter(String fieldName, Object value, Class... types)
            throws NoSuchMethodException {
        if (fieldName.contains(".")) {
            throw new UnsupportedOperationException(
                    "Nested Field Expression Not Supported Yet");
        }

        String methodName = "set"
                + Character.toString(fieldName.charAt(0)).toUpperCase()
                + fieldName.substring(1);

        if (types.length == 0) {
            // No types passed
            types = new Class[1];
            // Get the types if the field is list
            Field field = getClassField(getClass(), fieldName);
            types[0] = getFieldType(field);
        }

        try {
            Method method = getClass().getMethod(methodName, types);
            if (types[0].getName().equalsIgnoreCase("short")) {
                method.invoke(this, new Short(value.toString()));
            } else if (types[0].getName().equalsIgnoreCase("java.math.BigDecimal")) {
                method.invoke(this, new BigDecimal(value.toString()));
            }
            if (types[0].getName().equalsIgnoreCase("java.lang.Long")) {
                method.invoke(this, new Long(value.toString()));
            } else {
                method.invoke(this, value);
            }
        } catch (Exception ex) {
            if (ex.getClass().getName().equals("java.lang.NoSuchMethodException")) {
                throw new NoSuchMethodException(ex.getMessage());
            } else {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.error("Exception thrown: ",ex);
                        logger.trace("Field Name: {}", fieldName);
                // </editor-fold>
            }
        }
    }

    /**
     * Invokes the setter of the field (fieldName) which is declared in the
     * entity or one of it's super classes. Uses invokeSetter(String fieldName,
     * Object value, Class ... types)
     */
    public void invokeSetter(Field field, Object value) throws NoSuchMethodException {
        invokeSetter(field.getName(), value);
    }
    // </editor-fold>

    public static Field getClassField(Class cls, String fieldName) {
        Class superClass = cls;
        Field fld = null;
        boolean bFieldFound = false;
        while (!bFieldFound) {
            try {
                // Use native function getDeclaredField() instead of
                // looping on BaseEntity.getClassFields for performance
                // and systematic purposes
                fld = superClass.getDeclaredField(fieldName);
                // no exception thown, so, field should be got
                bFieldFound = true;
                return fld;
            } catch (NoSuchFieldException ex) {
                if (superClass == null) // No super class
                {
                    return null;
                } else if (superClass.getName().contains("unitedofoq")) // Super class still OFOQ. search in ancestors
                {
                    superClass = superClass.getSuperclass();
                } else // Super class is not OFOQ, no hope to find field
                {
                    return null;
                }
            }
        }
        return fld;
    }

    public static Class getFieldType(Field field) {
        if (field.getAnnotation(OneToMany.class) != null
                || field.getAnnotation(ManyToMany.class) != null) {
            return List.class;
        } else {
            return field.getType();
        }
    }

    public String getClassName() {
        return this.getClass().getSimpleName();
    }

}
