/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.entitysetup;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.security.privilege.OPrivilege;

/**
 *
 * @author arezk
 */
@Entity
@DiscriminatorValue("ENTITYACTION")
@ParentEntity(fields={"entityAction"})
@VersionControlSpecs(versionControlType= VersionControlSpecs.VersionControlType.Parent)
public class EntityActionPrivilege extends OPrivilege
{
    // <editor-fold defaultstate="collapsed" desc="entityAction">
    @JoinColumn(nullable=false)
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private OEntityAction entityAction ;
    public OEntityAction getEntityAction() {
        return entityAction;
    }
    public void setEntityAction(OEntityAction entityAction) {
        this.entityAction = entityAction;
    }
    public String getEntityActionDD() {
        return "EntityActionPrivilege_entityAction";
    }
    // </editor-fold>
}
