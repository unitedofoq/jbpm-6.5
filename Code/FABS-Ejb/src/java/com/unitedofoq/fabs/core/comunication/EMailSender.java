/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.comunication;

import com.unitedofoq.fabs.central.EntityManagerWrapperLocal;
import com.unitedofoq.fabs.core.alert.entites.dto.AlertDTO;
import com.unitedofoq.fabs.core.alert.entities.FiredAlert;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Queue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author melsayed
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.comunication.EMailSenderLocal",
        beanInterface = EMailSenderLocal.class)
public class EMailSender implements EMailSenderLocal {
    @Resource(mappedName = "java:/jms/queue/NotificationQueue")
    private Queue notificationQueue;
    @Resource(mappedName = "java:/jms/NotificationConnectionFactory")
    private ConnectionFactory notificationQueueFactory;
    @EJB
    private EntityManagerWrapperLocal entityManagerWrapper;
    final static Logger logger = LoggerFactory.getLogger(EMailSender.class);


    @Override
    public boolean sendEMail(String from, String to, String subject, String body) {
        logger.debug("Entering");
        return sendEMailWithAttachment(from, to, subject, body, null);
    }

    @Override
    public boolean sendHTMLEMail(String from, String to, String subject, String body, OUser loggedUser) {
        logger.debug("Entering");
        return sendEMail(from, to, subject, body);
    }

    @Override
    public boolean sendHTMLCustomEMail(String from, String to, String subject, String body, OUser loggedUser) {
        logger.debug("Entering");
        return sendEMail(from, to, subject, body);
    }

    @Override
    public boolean sendEMail(String from, String to, String subject, String body, String userEmail, String userPassword, String portNumber, String SMTP) {
        logger.debug("Entering");
        return sendEMail(from, to, subject, body);
    }

    @Override
    public boolean sendEMailWithAttachment(String from, String to, String subject, String body, String userEmail, String userPassword, String portNumber, String SMTP, String filePath) {
        logger.debug("Entering");
        return sendEMailWithAttachment(from, to, subject, body, filePath);
    }

    private boolean sendEMailWithAttachment(String from, String to, String subject, String body, String filePath) {
        logger.debug("Entering");
        EMailMessage eMailMessage = new EMailMessage();
        eMailMessage.setFrom(from);
        eMailMessage.setTos(Collections.singletonList(to));
        eMailMessage.setSubject(subject);
        eMailMessage.setBody(body);
        
        sendEmail(eMailMessage);
        
        logger.debug("Returning ");
        return true;
    }

    @Override
    public List<String> sendEMailToAll(String from, List<String> to, String subject, String body) {
        logger.debug("Entering");
        List<String> sentUsers = new ArrayList<String>();
        if (from == null || to == null || subject == null) {
            logger.debug("Returning with empty array");
            return new ArrayList<String>();
        }
        for (int i = 0; i < to.size(); i++) {
            if (sendEMail(from, to.get(i), subject, body)) {
                sentUsers.add(to.get(i));
            }
        }
        
        logger.debug("Returning with sentusers: {}", sentUsers.size());
        return sentUsers;
    }

    @Override
    public List<String> sendEMailToAll(String from, List<String> to, String subject, String body, String userEmail, String userPassword, String portNumber, String SMTP) {
        logger.debug("Entering");
        return sendEMailToAll(from, to, subject, body);
    }

    @Override
    public boolean sendEMail(String from, AlertDTO alertDTO, OUser loggedUser) {
        logger.debug("Entering");
        
        EMailMessage eMailMessage = new EMailMessage();
        eMailMessage.setFrom(from);
        eMailMessage.setTos(alertDTO.getTos());
        eMailMessage.setCcs(alertDTO.getCcs());
        eMailMessage.setSubject(alertDTO.getMessageSubject());
        eMailMessage.setBody(alertDTO.getMessageBody());
        eMailMessage.setAttachments(alertDTO.getAttachments());
        
        sendEmail(eMailMessage);
        return true;
    }
    
    /*
    This method is now asynchronous, it will just put a message on the queue
    and EmailNotificationMDB will process it
    */
    private void sendEmail(EMailMessage eMailMessage) {
        try {
            if((null != eMailMessage.getTos() && !eMailMessage.getTos().isEmpty()) 
                    || (null != eMailMessage.getCcs() && !eMailMessage.getCcs().isEmpty())) {
                JMSSender.sendJMSMessageToQueue(eMailMessage.toString(), notificationQueue, notificationQueueFactory, "email");
            }
        } catch (JMSException ex) {
            java.util.logging.Logger.getLogger(EMailSender.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        logger.debug("Returning with true");
    }
    
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void persistFiredAlert(String alertType,
            String subject,
            String body,
            String toEmail,
            OEntity actOnEntity,
            long entityDBID,
            OUser loogedUser) {
        logger.debug("Entering");
        FiredAlert firedAlert = new FiredAlert();
        firedAlert.setAlertDate(new Date());
        firedAlert.setTitle(subject);
        firedAlert.setMessageBody(body);
        firedAlert.setMessageSubject(subject);
        firedAlert.seteMail(toEmail);
        firedAlert.setInActive(false);
        firedAlert.setType(alertType);
        firedAlert.setActOnEntity(actOnEntity);
        firedAlert.setEntityDBID(entityDBID);
        try {
            entityManagerWrapper.persist(firedAlert, loogedUser);
            entityManagerWrapper.flush(loogedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
    }
}
