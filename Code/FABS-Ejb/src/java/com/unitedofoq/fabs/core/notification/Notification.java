/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.notification;

import javax.json.JsonObject;
import javax.json.spi.JsonProvider;

/**
 *
 * @author mmasoud
 */
public class Notification {

    private String userLoginName;
    
    private String summary;
    private String detail;
    private String severity;

    public Notification() {
    }

    public Notification(String userLoginName, String summary, String detail, String severity) {
        this.userLoginName = userLoginName;
        this.summary = summary;
        this.detail = detail;
        this.severity = severity;
    }

    public Notification(JsonObject jsonObject) {
        userLoginName = jsonObject.getString("userLoginName");
        summary = jsonObject.getString("summary");
        detail = jsonObject.getString("detail");
        severity = jsonObject.getString("severity");
    }

    public String getUserLoginName() {
        return userLoginName;
    }

    public void setUserLoginName(String userLoginName) {
        this.userLoginName = userLoginName;
    }
    
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public JsonObject toJson() {
        JsonProvider provider = JsonProvider.provider();
        JsonObject jsonObject = provider.createObjectBuilder()
                .add("summary", summary)
                .add("detail", detail)
                .add("severity", severity)
                .build();
        return jsonObject;
    }
}
