package com.unitedofoq.fabs.core.report;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs.VersionControlType;

@Entity
@ParentEntity(fields={"crosstabField"})
@VersionControlSpecs(versionControlType=VersionControlType.Parent)
public class CrosstabFieldColumn extends BaseEntity{

    // <editor-fold defaultstate="collapsed" desc="crosstabField">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CrossTabField crosstabField;

    public CrossTabField getCrosstabField() {
        return crosstabField;
    }

    public String getCrosstabFieldDD() {
        return "CrosstabFieldColumn_crosstabField";
    }

    public void setCrosstabField(CrossTabField crosstabField) {
        this.crosstabField = crosstabField;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="crossEntityDBID">
    @Column(nullable=false)
    private long crossEntityDBID;

    public long getCrossEntityDBID() {
        return crossEntityDBID;
    }

    public String getCrossEntityDBIDDD() {
        return "CrosstabFieldColumn_crossEntityDBID";
    }

    public void setCrossEntityDBID(long crossEntityDBID) {
        this.crossEntityDBID = crossEntityDBID;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="sortIndex">
    @Column(nullable=false)
    private Integer sortIndex;

    public Integer getSortIndex() {
        return sortIndex;
    }

    public String getSortIndexDD() {
        return "CrosstabFieldColumn_sortIndex";
    }

    public void setSortIndex(Integer sortIndex) {
        this.sortIndex = sortIndex;
    }
    //</editor-fold>

}
