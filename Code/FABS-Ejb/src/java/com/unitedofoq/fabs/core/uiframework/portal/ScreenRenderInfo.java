/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.portal;

import com.unitedofoq.fabs.core.uiframework.screen.OScreen;

/**
 *
 * @author tarzy
 */
public class ScreenRenderInfo {

    public ScreenRenderInfo(String screenName, String screenInstId, PortalPagePortletLite portlet) {
        this.screenName = screenName ;
        this.screenInstId = screenInstId;
        this.portlet = portlet ;
    }
    
    /**
     * {@link OScreen#name}
     */
    public String   screenName = "" ;
    
     /**
     * Generated for each screen from {@link OBackBean}
     */
    public String   screenInstId = "" ;

    @Override
    public String toString() {
        return "Screen Name= " + screenName + ", ScreenInstId = " + screenInstId  ;
    }
    
    /**
     * The screen portlet, Must be one object shared (referenced) between all 
     * the {@link OPortalPage} screens ScreenRenderInfos. So, all screens
     * can share information in it
     */
    public PortalPagePortletLite portlet ;
}
