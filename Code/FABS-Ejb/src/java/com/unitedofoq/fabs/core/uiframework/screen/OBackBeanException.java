/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.screen;

/**
 *
 * @author mmohamed
 */
public class OBackBeanException extends Exception{

    @Override
    public String getMessage() {
        return this.message;
    }

    public OBackBeanException(String message) {
        this.message = message;
    }
    
    private String message;
}
