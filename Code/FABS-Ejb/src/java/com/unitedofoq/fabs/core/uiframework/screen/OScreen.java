package com.unitedofoq.fabs.core.uiframework.screen;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.function.FunctionGroupScreen;
import com.unitedofoq.fabs.core.function.JavaFunction;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.uiframework.screen.dto.TabularScreenDTO;
import com.unitedofoq.fabs.core.uiframework.screen.report.TabularReportScreen;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

/**
 * Parent to all screen types, contains common features for OTMS screen types.
 *
 * @author bassem
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "STYPE")
@ChildEntity(fields = {"screenInputs", "screenOutputs", "screenFilter", "screenFunctions"})
@FABSEntitySpecs(indicatorFieldExp = "name")
@VersionControlSpecs(omoduleFieldExpression = "omodule")
public class OScreen extends ObjectBaseEntity {

    boolean blockOnLoad;

    public boolean isBlockOnLoad() {
        return blockOnLoad;
    }

    public void setBlockOnLoad(boolean blockOnLoad) {
        this.blockOnLoad = blockOnLoad;
    }

    public String getBlockOnLoadDD() {
        return "OScreen_blockOnLoad";
    }

    boolean multiSelection;

    public boolean isMultiSelection() {
        return multiSelection;
    }

    public void setMultiSelection(boolean multiSelection) {
        this.multiSelection = multiSelection;
    }

    public String getMultiSelectionDD() {
        return "OScreen_multiSelection";
    }

    boolean printable;

    public boolean isPrintable() {
        return printable;
    }

    public String getPrintableDD() {
        return "OScreen_printable";
    }

    public void setPrintable(boolean printable) {
        this.printable = printable;
    }

    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private OModule omodule;

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }

    public String getOmoduleDD() {
        return "OScreen_omodule";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ScreenFunction">
    @OneToMany(mappedBy = "oscreen", fetch = FetchType.LAZY)
    private List<ScreenFunction> screenFunctions;

    public List<ScreenFunction> getScreenFunctions() {
        return screenFunctions;
    }

    public void setScreenFunctions(List<ScreenFunction> screenFunctions) {
        this.screenFunctions = screenFunctions;
    }

    public String getScreenFunctionsDD() {
        return "OScreen_screenFunctions";
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="inlineHelp">
    private String inlineHelp;

    public String getInlineHelpDD() {
        return "OScreen_inlineHelp";
    }

    public String getInlineHelp() {
        return inlineHelp;
    }

    public void setInlineHelp(String inlineHelp) {
        this.inlineHelp = inlineHelp;
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="screenUsages">
//    @OneToMany(mappedBy = "screen")
//    private List<ScreenUsage> screenUsages;
//
//    public List<ScreenUsage> getScreenUsages() {
//        return screenUsages;
//    }
//
//    public void setScreenUsages(List<ScreenUsage> screenUsages) {
//        this.screenUsages = screenUsages;
//    }
    // </editor-fold >
    @Transient
    public final static long TSM_DEFAULT = 14;
    @Transient
    public final static long TSM_YESNO = 15;
    @Transient
    public final static long TSM_APPROVEREJECT = 16;
    @Transient
    public final static long TSM_APPROVEREJECTREVIEW = 258;
    @Transient
    public final static long TSM_DISMISS = 10;
    @Transient
    public final static long SM_NOMODE = 0;
    @Transient
    public final static long SM_ADD = 17;
    @Transient
    public final static long SM_EDIT = 18;
    @Transient
    public final static long SM_VIEW = 19;
    @Transient
    public final static long SM_MANAGED = 20;
    @Transient
    public final static long SM_LOOKUP = 21;
    @Transient
    public final static long SM_DELETE = 22;
    @Transient
    public final static long HAT_USER = 265;
    @Transient
    public final static long HAT_SELF = 266;
    @Transient
    public final static long HAT_MANAGER = 267;

    // <editor-fold defaultstate="collapsed" desc="screenInputs, screenOutputs">
    @OneToMany(mappedBy = "oscreen", fetch = FetchType.LAZY)
    private List<ScreenOutput> screenOutputs;

    @OneToMany(mappedBy = "oscreen", fetch = FetchType.LAZY)
    private List<ScreenInput> screenInputs;

    public List<ScreenInput> getScreenInputs() {
        return screenInputs;
    }

    public void setScreenInputs(List<ScreenInput> screenInputs) {
        this.screenInputs = screenInputs;
    }

    public List<ScreenOutput> getScreenOutputs() {
        return screenOutputs;
    }

    public void setScreenOutputs(List<ScreenOutput> screenOutputs) {
        this.screenOutputs = screenOutputs;
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="name, header, conditions">
    @Column(nullable = false, unique = true)
    protected String name;

    @Translatable(translationField = "headerTranslated")
    @Column(nullable = false)
    protected String header;

    @Translation(originalField = "header")
    @Transient
    protected String headerTranslated;

    public String getHeaderTranslated() {
        return headerTranslated;
    }

    public void setHeaderTranslated(String headerTranslated) {
        this.headerTranslated = headerTranslated;
    }

    public String getHeaderTranslatedDD() {
        return "OScreen_header";
    }

    public String getNameDD() {
        return "OScreen_name";
    }

    public String getHeader() {
        return header;
    }

    // this method added to remove spaces from header used in tabular Screen Export
    public String getHeaderWithNoSpaces() {
        return header.replace(' ', '_');
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getName() {
        return name;
    }

    public void setName(String screenName) {
        this.name = screenName;
    }
    // </editor-fold>

    protected String viewPage;

    public String getViewPage() {
        if (viewPage == null || "".equals(viewPage)) {
            // Set default view pages
            if (this instanceof RegistrationMedicationScreen) {
                viewPage = "Prescription.iface";
            } else if (this instanceof TabularReportScreen) {
                viewPage = "ReportTabularScreen.iface";
            } else if (this instanceof TabularScreen) {
                viewPage = "TabularScreen.iface";
            } else if (this instanceof FABSSetupScreen) {
                viewPage = "FABSSetup.iface";
            } else if (this instanceof GaugeScreen) {
                viewPage = "GaugeScreen.iface";
            } else if (this instanceof ChartScreen) {
                viewPage = "ChartScreen.iface";
            } else if (this instanceof FormScreen) {
                viewPage = "FormScreen.iface";
            } else if (this instanceof MultiListScreen) {
                viewPage = "MultiListScreen.iface";
            } else if (this instanceof MixScreen) {
                viewPage = "MixScreen.iface";
            } else if (this instanceof MultiLevelScreen) {
                viewPage = "MultiLevelScreen.iface";
            } else if (this instanceof FunctionGroupScreen) {
                viewPage = "FunctionGroupScreen.iface";
            }
        }
        return viewPage;
    }

    public void setViewPage(String viewPage) {
        this.viewPage = viewPage;
    }

    // <editor-fold defaultstate="collapsed" desc="supportSelf, supportManager">
    private boolean supportSelf;

    public boolean isSupportSelf() {
        return supportSelf;
    }

    public String getSupportManagerDD() {
        return "OScreen_supportManager";
    }

    public String geSupportSelfDD() {
        return "OScreen_supportSelf";
    }

    public void setSupportSelf(boolean supportSelf) {
        this.supportSelf = supportSelf;
    }

    private boolean supportManager;

    public boolean isSupportManager() {
        return supportManager;
    }

    public void setSupportManager(boolean supportManager) {
        this.supportManager = supportManager;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Custom Fields">
    private String screenCstmFLD1;
    private String screenCstmFLD2;
    private String screenCstmFLD3;
    private int screenCstmFLD4;
    private int screenCstmFLD5;

    public String getScreenCstmFLD1DD() {
        return "OScreen_screenCstmFLD1";
    }

    public String getScreenCstmFLD1() {
        return screenCstmFLD1;
    }

    public void setScreenCstmFLD1(String screenCstmFLD1) {
        this.screenCstmFLD1 = screenCstmFLD1;
    }

    public String getScreenCstmFLD2DD() {
        return "OScreen_screenCstmFLD2";
    }

    public String getScreenCstmFLD2() {
        return screenCstmFLD2;
    }

    public void setScreenCstmFLD2(String screenCstmFLD2) {
        this.screenCstmFLD2 = screenCstmFLD2;
    }

    public String getScreenCstmFLD3DD() {
        return "OScreen_screenCstmFLD3";
    }

    public String getScreenCstmFLD3() {
        return screenCstmFLD3;
    }

    public void setScreenCstmFLD3(String screenCstmFLD3) {
        this.screenCstmFLD3 = screenCstmFLD3;
    }

    public String getScreenCstmFLD4DD() {
        return "OScreen_screenCstmFLD4";
    }

    public int getScreenCstmFLD4() {
        return screenCstmFLD4;
    }

    public void setScreenCstmFLD4(int screenCstmFLD4) {
        this.screenCstmFLD4 = screenCstmFLD4;
    }

    public String getScreenCstmFLD5DD() {
        return "OScreen_screenCstmFLD5";
    }

    public int getScreenCstmFLD5() {
        return screenCstmFLD5;
    }

    public void setScreenCstmFLD5(int screenCstmFLD5) {
        this.screenCstmFLD5 = screenCstmFLD5;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="screenFilter">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, mappedBy = "screen")
    private ScreenFilter screenFilter;

    public ScreenFilter getScreenFilter() {
        return screenFilter;
    }

    public void setScreenFilter(ScreenFilter screenFilter) {
        this.screenFilter = screenFilter;
    }
    // </editor-fold>

    @Override
    public void PrePersist() {
        super.PrePersist();
        if (screenFilter != null) {
            if (screenFilter.getScreen() == null) {
                screenFilter.setScreen(this);
            }
        }
    }

    @PreDestroy
    public void onPreDestroy() {
        TabularScreenDTO.destroyLookupMainScr(this.getInstID());
    }

    // <editor-fold defaultstate="collapsed" desc="dataLoadingUserExit">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    /**
     * Data Loading User Exit Java Function, is called when loading data so we
     * can customize the loading conditions.
     * <br>Sample function is found in {@link UIFrameworkService#
     * sampleScreenDataLoadingUserExit(ODataMessage, OFunctionParms, OUser)}
     */
    private JavaFunction dataLoadingUserExit;

    /**
     * Getter of {@link #dataLoadingUserExit}
     */
    public JavaFunction getDataLoadingUserExit() {
        return dataLoadingUserExit;
    }

    /**
     * Setter of {@link #dataLoadingUserExit}
     */
    public void setDataLoadingUserExit(JavaFunction dataLoadingUserExit) {
        this.dataLoadingUserExit = dataLoadingUserExit;
    }

    public String getDataLoadingUserExitDD() {
        return "OScreen_dataLoadingUserExit";
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="showHeader">
    private boolean showHeader = true;

    public boolean isShowHeader() {
        return showHeader;
    }

    public void setShowHeader(boolean showHeader) {
        this.showHeader = showHeader;
    }

    public String getShowHeaderDD() {
        return "OScreen_showHeader";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="showActiveOnly">
    private boolean showActiveOnly;

    public boolean isShowActiveOnly() {
        return showActiveOnly;
    }

    public void setShowActiveOnly(boolean showActiveOnly) {
        this.showActiveOnly = showActiveOnly;
    }

    public String getShowActiveOnlyDD() {
        return "OScreen_showActiveOnly";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Personalization Fields: prsnlzdOriginal_DBID, prsnlzdUser_DBID, prsnlzd">
    @Column(nullable = true)
    private long prsnlzdOriginal_DBID;

    @Column(nullable = true)
    private long prsnlzdUser_DBID;

    @Column(nullable = true)
    private boolean prsnlzd;

    /**
     * @return the prsnlzdOriginal_DBID
     */
    public long getPrsnlzdOriginal_DBID() {
        return prsnlzdOriginal_DBID;
    }

    /**
     * @param prsnlzdOriginal_DBID the prsnlzdOriginal_DBID to set
     */
    public void setPrsnlzdOriginal_DBID(long prsnlzdOriginal_DBID) {
        this.prsnlzdOriginal_DBID = prsnlzdOriginal_DBID;
    }

    /**
     * @return the prsnlzdUser_DBID
     */
    public long getPrsnlzdUser_DBID() {
        return prsnlzdUser_DBID;
    }

    /**
     * @param prsnlzdUser_DBID the prsnlzdUser_DBID to set
     */
    public void setPrsnlzdUser_DBID(long prsnlzdUser_DBID) {
        this.prsnlzdUser_DBID = prsnlzdUser_DBID;
    }

    /**
     * @return the prsnlzd
     */
    public boolean isPrsnlzd() {
        return prsnlzd;
    }

    /**
     * @param prsnlzd the prsnlzd to set
     */
    public void setPrsnlzd(boolean prsnlzd) {
        this.prsnlzd = prsnlzd;
    }

    // </ editor-fold>
    public static String getViewPage(String screenDiscrementorValue) {
        String vPage = "";
        // Set default view pages
        if (screenDiscrementorValue.equals("RM")) {
            vPage = "Prescription.iface";
        } else if (screenDiscrementorValue.equals("TABULAR")) {
            vPage = "TabularScreen.iface";
        } else if (screenDiscrementorValue.equals("FABSSetup")) {
            vPage = "FABSSetup.iface";
        } else if (screenDiscrementorValue.equals("GCHART")) {
            vPage = "GaugeScreen.iface";
        } else if (screenDiscrementorValue.equals("CHART")) {
            vPage = "ChartScreen.iface";
        } else if (screenDiscrementorValue.equals("FORM")) {
            vPage = "FormScreen.iface";
        } else if (screenDiscrementorValue.equals("MultiList")) {
            vPage = "MultiListScreen.iface";
        } else if (screenDiscrementorValue.equals("Mix")) {
            vPage = "MixScreen.iface";
        } else if (screenDiscrementorValue.equals("MultiLevel")) {
            vPage = "MultiLevelScreen.iface";
        } else if (screenDiscrementorValue.equals("FunctionGroup")) {
            vPage = "FunctionGroupScreen.iface";
        }

        return vPage;
    }

    boolean showButtonsTitle;

    public boolean isShowButtonsTitle() {
        return showButtonsTitle;
    }

    public String getShowButtonsTitleDD() {
        return "OScreen_showButtonsTitle";
    }

    public void setShowButtonsTitle(boolean showButtonsTitle) {
        this.showButtonsTitle = showButtonsTitle;
    }

    private String helpUrl;

    public String getHelpUrl() {
        return helpUrl;
    }

    public void setHelpUrl(String helpUrl) {
        this.helpUrl = helpUrl;
    }

    public String getHelpUrlDD() {
        return "OScreen_helpUrl";
    }
}
