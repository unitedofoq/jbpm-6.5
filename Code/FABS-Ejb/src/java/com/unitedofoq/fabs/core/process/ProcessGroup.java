/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.process;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Entity;

/**
 *
 * @author lap
 */
@Entity
public class ProcessGroup  extends BaseEntity{

private String description;

    public String getDescription() {
        return description;
    }
    
    public String getDescriptionDD() {
        return "ProcessGroup_description";
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
