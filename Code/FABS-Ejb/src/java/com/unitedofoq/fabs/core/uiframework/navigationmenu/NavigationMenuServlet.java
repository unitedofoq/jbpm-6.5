package com.unitedofoq.fabs.core.uiframework.navigationmenu;




import javax.ejb.EJB;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.unitedofoq.fabs.core.Exception.BasicException;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import org.slf4j.LoggerFactory;

/**
 *
 * @author tarzy
 */
public class NavigationMenuServlet implements ServletContextListener {
        final static org.slf4j.Logger logger = LoggerFactory.getLogger(NavigationMenuServlet.class);
    @EJB
    NMDataLoader nmdl;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.debug("Entering");
        try {
            NavMenuManagerImpl.getSingleInstance().Init(nmdl);
        } catch (BasicException ex) {
            logger.error("Exception thrown: ",ex);
        }
        logger.debug("Returning");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
