/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.content.db;

import com.unitedofoq.fabs.central.EntityManagerWrapperLocal;
import com.unitedofoq.fabs.core.content.AttachmentDTO;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author lap
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.content.db.DBAttachmentServiceLocal",
        beanInterface = DBAttachmentServiceLocal.class)
public class DBAttachmentService implements DBAttachmentServiceLocal {

    final static Logger logger = LoggerFactory.getLogger(DBAttachmentService.class);

    @EJB
    EntityManagerWrapperLocal entityManagerWrapper;
    EntityManager em = null;

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public String put(AttachmentDTO attachmentDTO, long tenantID, BaseEntity entity) {
        logger.debug("Entering");
        em = entityManagerWrapper.getMultiTenantEntityManager(tenantID);
        DBAttachment dbAttachment = convertDTOToEntityAttachment(attachmentDTO);
        em.persist(dbAttachment);
        em.flush();
        return String.valueOf(dbAttachment.getDbid());
    }

    @Override
    public AttachmentDTO get(long tenantID, String attachmentID) {
        em = entityManagerWrapper.getMultiTenantEntityManager(tenantID);
        DBAttachment dbAttachment = new DBAttachment();
        try {
            dbAttachment = (DBAttachment) em.createQuery("SELECT a FROM DBAttachment a WHERE a.dbid = "
                    + attachmentID).getResultList().get(0);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        AttachmentDTO attachmentDTO = convertDBAttachmentToDTO(dbAttachment);
        return attachmentDTO;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void delete(String attachmentID, long tenantID) {
        logger.debug("Entering");
        em = entityManagerWrapper.getMultiTenantEntityManager(tenantID);
        em.createQuery("delete From DBAttachment where dbid = '" + attachmentID + "'").executeUpdate();
        logger.debug("Returning");
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void update(AttachmentDTO attachment, long tenantID) {
        logger.debug("Entering");
        em = entityManagerWrapper.getMultiTenantEntityManager(tenantID);
        DBAttachment dBAttachment = convertDTOToEntityAttachment(attachment);
        dBAttachment.setDbid(Long.valueOf(attachment.getId()));
        em.merge(dBAttachment);
        em.flush();
    }

    @Override
    public AttachmentDTO getByName(String attachmentName, long tenantID) {
        em = entityManagerWrapper.getMultiTenantEntityManager(tenantID);
        List<DBAttachment> eAs = (List<DBAttachment>) em.createQuery("SELECT a FROM DBAttachment a WHERE a.name = '"
                + attachmentName + "'").getResultList();
        DBAttachment eA = null;
        AttachmentDTO attachmentDTO = null;
        if(eAs != null && !eAs.isEmpty()){
            eA = eAs.get(0);
            attachmentDTO = convertDBAttachmentToDTO(eA);
        }
        return attachmentDTO;
    }

    private DBAttachment convertDTOToEntityAttachment(AttachmentDTO attachmentDTO) {
        DBAttachment dbAttachment = new DBAttachment();
        dbAttachment.setAttachment(attachmentDTO.getAttachment());
        dbAttachment.setMimeType(attachmentDTO.getMimeType());
        dbAttachment.setName(attachmentDTO.getName());
        dbAttachment.setDescription(attachmentDTO.getDescription());
        return dbAttachment;
    }

    private AttachmentDTO convertDBAttachmentToDTO(DBAttachment dbAttachment) {
        AttachmentDTO attachmentDTO = new AttachmentDTO();
        attachmentDTO.setId(String.valueOf(dbAttachment.getDbid()));
        attachmentDTO.setAttachment(dbAttachment.getAttachment());
        attachmentDTO.setMimeType(dbAttachment.getMimeType());
        attachmentDTO.setName(dbAttachment.getName());
        attachmentDTO.setDescription(dbAttachment.getDescription());
        return attachmentDTO;
    }
}
