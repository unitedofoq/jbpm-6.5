/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.routing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.timezone.TimeZoneServiceLocal;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.fabs.core.validation.FABSException;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mibrahim
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.routing.ORoutingBeanLocal",
beanInterface = ORoutingBeanLocal.class)
public class ORoutingBean implements ORoutingBeanLocal {
final static org.slf4j.Logger logger = LoggerFactory.getLogger(ORoutingBean.class);
    @EJB
    UserMessageServiceRemote userMessageServiceRemote;
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    protected DataTypeServiceRemote dataTypeService;
    @EJB
    OFunctionServiceRemote oFunctionService;
    @EJB
    protected EntitySetupServiceRemote entitySetupService;
    private List<ORouting> routings;

    @EJB
    protected TimeZoneServiceLocal timeZoneService;
    
    private boolean isScreenHasRouting(OScreen launcherScreen, OUser loggedUser) {
        try {
            logger.debug("Entering");
            routings = oem.loadEntityList(ORouting.class.getSimpleName(),
                    Collections.singletonList("launcherScreen.dbid=" + launcherScreen.getDbid()),
                    null, null, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            logger.debug("Returning with False");
            return false;
        }
        if (null == routings || routings.isEmpty()) {
            logger.debug("Returning with False");
            return false;
        }
        logger.debug("Returning with True");
        return true;
    }

    /**
     * 1. Validate Input for Routing using inputDt and data type of passed
     * entity. 2. Create Routing Instance and set initiator 3. Create Task
     * Instance and set owner,Oentity and assigedTime 4. Save Task Instance to
     * allow inbox to read it.
     */
    @Override
    public OFunctionResult startRoutingInstance(ODataMessage oDM, OFunctionParms params, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            OScreen launcherOScreen = (OScreen) oDM.getData().get(0);
            if (!isScreenHasRouting(launcherOScreen, loggedUser)) {
                logger.debug("Returning");
                return oFR;
            }
            BaseEntity routedEntity = (BaseEntity) oDM.getData().get(1);
            List<ORouting> workingORoutings = getWorkingORoutings(routings, routedEntity, loggedUser);
            if (workingORoutings.isEmpty()) {
                logger.debug("Returning");
                return oFR;
            }
            for (ORouting routing : workingORoutings) {
                String routingName = routing.getLauncherScreen().getName() + "_Routing_" + routing.getDbid();
                routing.setName(routingName);
                oem.saveEntity(routing, loggedUser);
                // <editor-fold defaultstate="collapsed" desc="Create Routing Instance and set initiator">            
                OUser initiator = (OUser) oDM.getData().get(2);
                ORoutingInstance routingInstance = new ORoutingInstance();
                routingInstance.setInitiator(initiator);
                routingInstance.setRouting(routing);
            oem.saveEntity(routingInstance, oem.getSystemUser(loggedUser));
                // </editor-fold>
                // <editor-fold defaultstate="collapsed" desc="Create Task Instance and set owner,Oentity and assigedTime">
                List<String> conditions = new ArrayList<String>();
                conditions.add("routing.dbid=" + routing.getDbid());
                conditions.add("firstTask=true");
                ORoutingTask firstTask = (ORoutingTask) oem.loadEntity(ORoutingTask.class.getSimpleName(),
                        conditions, null, loggedUser);
                ORoutingTaskInstance routingTaskInstance = setRoutingTaskInstanceData(firstTask, routingInstance,
                        routedEntity.getDbid(), null, loggedUser);
                setRoutingTaskInstanceOwner(firstTask, null, routingTaskInstance, loggedUser);
            oem.saveEntity(routingTaskInstance, oem.getSystemUser(loggedUser));
                //</editor-fold>                
            }


        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    private boolean setRoutingTaskInstanceOwner(ORoutingTask task, ORoutingTaskInstance routingTaskInstance,
            ORoutingTaskInstance nextRoutingTaskInstance, OUser loggedUser) {
        logger.debug("Entering");
        try {
            //TODo : calculate owner
            if (task.getOwner() != null) {
                nextRoutingTaskInstance.setOwner(task.getOwner());
            } else {
                // run owner java function to get owner
                ODataMessage oDM = new ODataMessage();
                oDM.setODataType(dataTypeService.loadODataType("ORoutingTaskInstance", oem.getSystemUser(loggedUser)));
                List data = new ArrayList();
                data.add(routingTaskInstance);
                data.add(nextRoutingTaskInstance);
                data.add(loggedUser);
                oDM.setData(data);

                OFunctionResult functionResult = oFunctionService.runFunction(
                        task.getOwnerJavaFunction(), oDM, null, loggedUser);
                if (functionResult.getErrors() == null || !functionResult.getErrors().isEmpty()) {
                    logger.debug("Returning with Null");
                    return false;
                }
                nextRoutingTaskInstance.setOwner((OUser) functionResult.getReturnedDataMessage().getData().get(0));
                logger.debug("Returning with True");
                return true;
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
logger.debug("Returning with False");
            return false;
        }
        logger.debug("Returning with False");
        return false;
    }

    private ORoutingTaskInstance setRoutingTaskInstanceData(ORoutingTask task,
            ORoutingInstance routingInstance, long routedEntityDbid, ORoutingTaskInstance currentTaskInstance, OUser loggedUser) {
   logger.debug("Entering");     
        ORoutingTaskInstance routingTaskInstance = new ORoutingTaskInstance();
        routingTaskInstance.setPreviousTask(currentTaskInstance);
        long screenDBID = task.getTaskScreen().getDbid();
        OEntityDTO entityDTO = entitySetupService.getOScreenEntityDTO(screenDBID, loggedUser);
        OEntity entity = null;
        try {
            entity = entitySetupService.loadOEntity(entityDTO.getEntityClassPath(), loggedUser);
        } catch (UserNotAuthorizedException ex) {
            logger.error("Exception thrown: ",ex);
        }
        routingTaskInstance.setRoutingOEntity(entity);
        routingTaskInstance.setRoutingEntityDBID(routedEntityDbid);
        routingTaskInstance.setAssignedTime(new java.util.Date());
        routingTaskInstance.setOroutingInst(routingInstance);
        routingTaskInstance.setOroutingTask(task);
        logger.debug("Returning");
        return routingTaskInstance;
    }

    /**
     * 1. get action using id 2.get action function to run 3. Run action 4. Send
     * alert 5. Set action Taken 6. Call setNextTaskInstance method 7. set
     * finishTime and comment
     */
    @Override
    public OFunctionResult runTaskAction(ODataMessage oDM, OFunctionParms params, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            // <editor-fold defaultstate="collapsed" desc="Get routing task action and run action">
            String actionID = String.valueOf(oDM.getData().get(0));
            ORoutingTaskAction routingTaskAction = (ORoutingTaskAction) oem.loadEntity(ORoutingTaskAction.class.getSimpleName(),
                    Collections.singletonList("dbid = " + actionID), null, loggedUser);

            OFunction function = routingTaskAction.getFunction();
            if (null != function) {
                OFunctionResult functionResult = oFunctionService.runFunction(
                        function, oDM, params, loggedUser);
                if (functionResult.getErrors() == null || !functionResult.getErrors().isEmpty()) {
                    return functionResult;
                }
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="load routing task inst.">
            String routingTaskInstID = String.valueOf(oDM.getData().get(1));
            ORoutingTaskInstance routingTaskInst = (ORoutingTaskInstance) oem.loadEntity(ORoutingTaskInstance.class.getSimpleName(),
                    Collections.singletonList("dbid = " + routingTaskInstID), null, loggedUser);
            // </editor-fold>
            //Comment
            String comment = String.valueOf(oDM.getData().get(2));

            // <editor-fold defaultstate="collapsed" desc="send alert">
            ORoutingTask task = routingTaskAction.getRoutingTask();
            if (task.isNotifiable()) {
                //TODO:Use new Alert
                //OAlert alert = task.getAlert();
                //alert.setViewDate(timeZoneService.getUserCDT(loggedUser));//new Date(System.currentTimeMillis()));
                //entitySetupService.callEntityUpdateAction(alert, loggedUser);
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="set action taken">
            routingTaskInst.setActionTaken(routingTaskAction);
            routingTaskInst.setFinishTime(new java.util.Date());
            routingTaskInst.setComments(comment);
            routingTaskInst.setFinished(true);
            entitySetupService.callEntityUpdateAction(routingTaskInst, loggedUser);
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Create next routing task instance">
            if (routingTaskAction.getNextTask() == null) {
                if (routingTaskAction.isFinalizeRouting()) {
                    oFR.addSuccess(userMessageServiceRemote.getUserMessage("RoutingFinished", loggedUser));
                    logger.debug("Returning");
                    return oFR;
                } else {
                    oFR.addError(userMessageServiceRemote.getUserMessage("NextTaskNullAndNotEnd", loggedUser));
                    logger.debug("Returning");
                    return oFR;
                }
            } else {
                ORoutingTaskInstance nextTaskInst = setRoutingTaskInstanceData(routingTaskAction.getNextTask(), routingTaskInst.getOroutingInst(),
                        routingTaskInst.getRoutingEntityDBID(), routingTaskInst, loggedUser);
                nextTaskInst.setPreviousTask(routingTaskInst);
                setRoutingTaskInstanceOwner(routingTaskAction.getNextTask(), routingTaskInst, nextTaskInst, loggedUser);
                oem.saveEntity(nextTaskInst, oem.getSystemUser(loggedUser));
                logger.trace("New Task Instance has been Created");
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="update mappedFieldExpression vlaue in entity">
            BaseEntity entity = oem.loadEntity(routingTaskInst.getRoutingOEntity().getEntityClassName(),
                    Collections.singletonList("dbid = " + routingTaskInst.getRoutingEntityDBID()), null, loggedUser);
            if (routingTaskAction.getMapFieldExpression() != null && !routingTaskAction.getMapFieldExpression().isEmpty()) {
                if (routingTaskAction.getUdcValue() != null) {
                    BaseEntity.setValueInEntity(entity, routingTaskAction.getMapFieldExpression(),
                            routingTaskAction.getUdcValue());
                } else {
                    BaseEntity.setValueInEntity(entity, routingTaskAction.getMapFieldExpression(),
                            routingTaskAction.isBoolValue());
                }
            }
            // </editor-fold>

        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    /**
     * 1. load routing task instance 2. load routing task actions
     */
    @Override
    public OFunctionResult getRoutingTaskInfo(ODataMessage oDM, OFunctionParms params, OUser loggedUser) {
     logger.debug("Entering");   
        OFunctionResult oFR = new OFunctionResult();
        List returnedValues = new ArrayList();
        try {
            long taskInstanceDBID = Long.valueOf(oDM.getData().get(0).toString());
            ORoutingTaskInstance taskInstance = (ORoutingTaskInstance) oem.loadEntity(
                    ORoutingTaskInstance.class.getSimpleName(),
                    Collections.singletonList("dbid = " + taskInstanceDBID), null, loggedUser);
            if (taskInstance != null) {
                returnedValues.add(taskInstance);
                List<ORoutingTaskAction> taskActions = oem.loadEntityList(ORoutingTaskAction.class.getSimpleName(),
                        Collections.singletonList("routingTask.dbid = " + taskInstance.getOroutingTask().getDbid()), null, null, loggedUser);
                if (taskActions != null && !taskActions.isEmpty()) {
                    returnedValues.add(taskActions);
                } else {
                    returnedValues.add(new ArrayList());
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        } finally {
            oFR.setReturnValues(returnedValues);
        }
        logger.debug("Returning");
        return oFR;
    }

    /**
     * 1. load routing task instances for user to show in inbox
     */
    @Override
    public OFunctionResult getTaskInbox(OUser loggedUser) {
        List<ORoutingTaskInstance> tasks = new ArrayList<ORoutingTaskInstance>();
        try {
            List<String> condition = new ArrayList<String>();
            condition.add("owner.dbid = " + loggedUser.getDbid());
            condition.add("finished=false");
            tasks = oem.loadEntityList(ORoutingTaskInstance.class.getSimpleName(), condition, null, null, loggedUser);
        } catch (Exception ex) {
            if (!(ex instanceof FABSException)) {
                logger.error("Exception thrown: ",ex);
            }
        } finally {
            OFunctionResult oFR = new OFunctionResult();
            oFR.setReturnValues(tasks);
            logger.debug("Returning");
            return oFR;
        }
    }

    private List<ORouting> getWorkingORoutings(List<ORouting> routings, BaseEntity routedEntity,
            OUser loggedUser) {
        logger.debug("Entering");
        List<ORouting> workingORoutings = new ArrayList<ORouting>();
        try {
            for (ORouting oRouting : routings) {
                String operator = oRouting.getConditionalOp();
                if (null != operator && !"".equals(operator)) {
                    String condFldExp = oRouting.getConditionalFldExp();
                    String constValue = oRouting.getConditionValue();
                    String condValue = (String) BaseEntity.getValueFromEntity(routedEntity, condFldExp);
                    if (null == condValue || null == constValue) {
                        continue;
                    }
                    if ("=".equals(operator)) {
                        if (condValue.equals(constValue)) {
                            workingORoutings.add(oRouting);
                        }
                    } else if ("!=".equals(operator)) {
                        if (!condValue.equals(constValue)) {
                            workingORoutings.add(oRouting);
                        }
                    } else if (">".equals(operator)) {
                        if (Double.parseDouble(condValue)
                                > Double.parseDouble(constValue)) {
                            workingORoutings.add(oRouting);
                        }
                    } else if ("<".equals(operator)) {
                        if (Double.parseDouble(condValue)
                                < Double.parseDouble(constValue)) {
                            workingORoutings.add(oRouting);
                        }
                    } else {
                        logger.trace("Wrong Operator is Found Operator: {}",operator);
                    }
                } else {
                    workingORoutings.add(oRouting);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        }
        logger.debug("Returning");
        return workingORoutings;
    }
}
