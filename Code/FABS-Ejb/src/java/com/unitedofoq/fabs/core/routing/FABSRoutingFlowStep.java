/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.routing;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;

/**
 *
 * @author mibrahim
 */
@Entity
@ParentEntity(fields={"routingFlow"})
public class FABSRoutingFlowStep extends BaseEntity {    
    //<editor-fold desc="routingFlow" defaultstate="collapsed">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private FABSRoutingFlow routingFlow;

    public String getRoutingFlowDD() {
        return "FABSRoutingFlow_routingFlow";
    }
    
    public FABSRoutingFlow getRoutingFlow() {
        return routingFlow;
    }

    public void setRoutingFlow(FABSRoutingFlow routingFlow) {
        this.routingFlow = routingFlow;
    }
     //</editor-fold>
    
    
    
}
