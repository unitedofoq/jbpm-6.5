/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.notification;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 *
 * @author lap
 */
public class NotificationSender {

    public static void sendNotification(String userLoginName, String summary, String detail, String severity) throws MalformedURLException, ProtocolException, IOException {
        String uri = "https://localhost:8181/OTMS-Web/rest/notificationRest/notifyUser";
        URL url = new URL(uri);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");

        String notificaion = "{\"userLoginName\":\"" + userLoginName + "\""
                + ",\"summary\":\"" + summary + "\""
                + ",\"detail\":\"" + detail + "\""
                + ",\"severity\":\"" + severity + "\"}";

        OutputStream os = conn.getOutputStream();
        os.write(notificaion.getBytes());
        os.flush();

        if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + conn.getResponseCode());
        }
        conn.disconnect();
    }
    
    public static void sendNotification(String userName, String notificationText) throws MalformedURLException, ProtocolException, IOException {
        String uri = "https://localhost:8181/OTMS-Web/rest/notificationRest/notifyLiferayUser";
        URL url = new URL(uri);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");

        String notificaion = "{\"userName\":\"" + userName + "\""
                + ",\"notificationText\":\"" + notificationText + "\"}";

        OutputStream os = conn.getOutputStream();
        os.write(notificaion.getBytes());
        os.flush();

        if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + conn.getResponseCode());
        }
        conn.disconnect();
    }

}
