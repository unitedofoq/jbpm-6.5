/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework;

import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.core.comunication.ServerJobInbox;
import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDDataValidationException;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.DBIDGeneration;
import com.unitedofoq.fabs.core.entitybase.EntityAuditTrail;
import com.unitedofoq.fabs.core.entitybase.EntityBaseServiceRemote;
import com.unitedofoq.fabs.core.entitybase.FABSDataValidation.FABSDataValidationType;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.GetEntityChildrenTree;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntityAction;
import com.unitedofoq.fabs.core.entitysetup.ObjectAccessPrivilege;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.function.JavaFunction;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.function.ServerJob;
import com.unitedofoq.fabs.core.security.user.ORole;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.RolePrivilege;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.session.ScreenSessionInfo;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.udc.UDCType;
import com.unitedofoq.fabs.core.uiframework.portal.OPortalPage;
import com.unitedofoq.fabs.core.uiframework.portal.PortalPageFunction;
import com.unitedofoq.fabs.core.uiframework.portal.PortalPagePortlet;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.fabs.core.validation.DateFromToException;
import com.unitedofoq.fabs.core.validation.EntityDataValidationException;
import com.unitedofoq.fabs.core.validation.FABSException;
import com.unitedofoq.fabs.core.validation.OEntityDataValidationException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.OneToOne;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mmohamed
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.uiframework.UIFrameworkEntityManagerRemote",
        beanInterface = UIFrameworkEntityManagerRemote.class)
public class UIFrameworkEntityManager implements UIFrameworkEntityManagerRemote {

    final static Logger logger = LoggerFactory.getLogger(UIFrameworkEntityManager.class);
    @EJB
    OEntityManagerRemote oem;
    @EJB
    FABSSetupLocal fABSSetupLocal;
    @EJB
    OCentralEntityManagerRemote centralOEM;
    @EJB
    DDServiceRemote ddService;
    @EJB
    private EntitySetupServiceRemote entitySetupService;
    @EJB
    private UserMessageServiceRemote usrMsgService;
    @EJB
    private OFunctionServiceRemote functionService;
    @EJB
    private DataTypeServiceRemote dataTypeService;
    @EJB
    private UIFrameworkServiceRemote uiFrameworkService;
    @EJB
    private EntityBaseServiceRemote entityBaseService;

    @Override
    public OFunctionResult saveScreenFieldsDDs(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        // FIXME: Is it a must that we should specifiy the exact class for the datamessage data?
        // Or we can go on with the Parent Class?
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, BaseEntity.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        try {
            ScreenField screenField = (ScreenField) odm.getData().get(0);
            long screenDBID = screenField.getOScreen().getDbid();
            OEntityDTO screenOEntity = entitySetupService.getOScreenEntityDTO(screenDBID, loggedUser);
            if (screenField.getDd() == null || screenField.getDd().getDbid() == 0) {
                if (screenField.getFieldExpression().contains("cc")
                        &&/*the field is CC*/ screenField.getFieldExpression().contains(".")
                        &&/* cc1.value*/ BaseEntity.getClassField(Class.forName(/*Get the Field object*/
                                        screenOEntity.getEntityClassPath()), screenField.getFieldExpression().
                                substring(0, screenField.getFieldExpression().indexOf("."))).
                        getType().equals(UDC.class)) {/*Is it UDC*/

                    UDCType cType = (UDCType) BaseEntity.getValueFromEntity(screenOEntity, "ccType" + screenField.getFieldExpression().substring(
                            screenField.getFieldExpression().indexOf(".") - 1,
                            screenField.getFieldExpression().indexOf(".")));
                    DD dd = ddService.getDD(cType.getCode(), loggedUser);
                    screenField.setDd(dd);
                    odm.getData().set(0, screenField);
                    logger.debug("Automatically Setting ScreenField Empty DD: {}", dd);
                } else {
                    Class baseEntityCls = Class.forName(
                            screenOEntity.getEntityClassPath());
                    DD dd = ddService.getDD(BaseEntity.getFieldDDName(
                            baseEntityCls, screenField.getFieldExpression()), loggedUser);
                    if (dd == null) {
                        logger.error("default dd not found ");
                        oFR.addError(usrMsgService.getUserMessage("defaultDDNotFound", loggedUser));
                        return oFR;
                    }
                    screenField.setDd(dd);
                    odm.getData().set(0, screenField);
                    logger.debug("Automatically Setting ScreenField Empty DD: {}", dd);
                }
            }

            oFR.append(saveEntity(odm, functionParms, loggedUser));
            //FIXME: call ScreenField Update Action
            if (oFR.getErrors().size() > 0) {
                logger.debug("Returning");
                return oFR;
            }

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SaveError", loggedUser), ex);
        }
        logger.debug("Returning");
        return oFR;
    }

    /**
     * Simulates the Cascade (Parent)Merge-(Child)Persist instead of EJB which
     * generates error in this case (obviously, not supported). Using
     * {@link FABSEntitySpecs#customCascadeFields()}
     *
     * @param entity
     */
    private OFunctionResult saveCustomCascadeEntities(BaseEntity entity, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();

        FABSEntitySpecs entityFABSSpecs
                = entity.getClass().getAnnotation(FABSEntitySpecs.class);
        if (entityFABSSpecs == null) // No CustomeCascadeFields found as not FABSEntitySpecs found
        {
            logger.debug("Returning");
            return oFR;
        }
        if (entityFABSSpecs.customCascadeFields() == null
                || entityFABSSpecs.customCascadeFields().length == 0) // No CustomeCascadeFields found
        {
            logger.debug("Returning");
            return oFR;
        }
        logger.debug("There are CustomeCascadeFields");
        logger.debug("Loop on custom cascade fields");
        for (String childFieldName : entityFABSSpecs.customCascadeFields()) {
            BaseEntity childEntity = null;
            try {
                childEntity = (BaseEntity) entity.invokeGetter(childFieldName);

                if (childEntity == null) // Child Entity is not required to be persited nor updated
                // Do nothing
                {
                    continue;
                }

                if (childEntity.getDbid() != 0 || entity.getDbid() == 0) // Not (Parent)Merge-(Child)Persist
                {
                    continue;
                }

                if (!childEntity.isChanged()) // Child entity is not changed, won't be included in transactions
                // Do Nothing
                {
                    continue;
                }

                logger.trace("Savig Custom Cascaded ChildName: {}", childFieldName);

                // Fill the parent field in child entity if it's OneToOne relation
                Field fld = BaseEntity.getClassField(entity.getClass(), childFieldName);
                OneToOne oneTOneAnnot = fld.getAnnotation(OneToOne.class);
                String parentFieldName = null;
                if (oneTOneAnnot != null) {
                    parentFieldName = oneTOneAnnot.mappedBy();

                    logger.trace("Parent Field Is Set In ChildName: {}", childFieldName);
                }

                OFunctionResult childCreationFR = null;
                logger.trace("Save the child entity using its create action");
                // Save the child entity using its create action

                if (parentFieldName != null) {
                    childEntity.invokeSetter(parentFieldName, entity);
                }

                if (childEntity.getDbid() == 0) {
                    childCreationFR = entitySetupService.callEntityCreateAction(childEntity, loggedUser);
                } else if (childEntity.getDbid() != 0 && childEntity.isChanged()) {
                    childCreationFR = entitySetupService.callEntityUpdateAction(childEntity, loggedUser);
                } else {
                    continue;
                }

                if (childCreationFR.getErrors().isEmpty()) {
                    logger.debug("No Errors");
                    // No Errors
                    // Append to main function oFR
                    oFR.append(childCreationFR);
                    logger.debug("Set the saved child entity into the parent entity");
                    // Set the saved child entity into the parent entity
                    entity.invokeSetter(childFieldName, childCreationFR.getReturnedDataMessage().getData().get(0));
                }

            } catch (Exception ex) {
                logger.error("Exception thrown: ", ex);
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
            }
        }
        logger.debug("Returning");
        return oFR;
    }

    /**
     * General function to save/update entity
     *
     * @param odm {
     * @linke BaseEntity} as the first {@link ODataMessage#data} element
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult saveEntity(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering - Saving Entity");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, BaseEntity.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        BaseEntity entity = (BaseEntity) odm.getData().get(0);
        try {
//logger.trace("Rollback & return if error encountered");
            // Rollback & return if error encountered
            // Commit & Reurn if entity DBID changed
            //FIXME: Commented from version r18.1.7 as there's no usage
            // <editor-fold defaultstate="collapsed" desc="Change entity & reference for module update">
//            if (entity.getDbid() != 0) {
            // Updating entity
//                String entityVCModExp = entity.getVCModuleExpression();
//                if (entityVCModExp != null
//                        && !entityVCModExp.equals("") // Entity has module expression
//                        && !entityVCModExp.contains(".") // Module is direct field in the entity
//                        // Ignore indirect module entity as it will be updated in the
//                        // changeEntityForModuleChange call automatically
//                        ) {
//OFunctionResult internalFR = entityBaseService.
//                            changeEntityForModuleChange(odm, loggedUser);
//                    if (internalFR.getErrors().isEmpty()) {
//                        oFR.append(internalFR);
//                        // Get entity with updated DBID
//                        // entity = (BaseEntity) oFR.getReturnedDataMessage().getData().get(0);
//                    } else {
//                        oFR.append(internalFR);
//                        return oFR;
//                    }
//                    BaseEntity returnedEntity = (BaseEntity) internalFR.getReturnedDataMessage().getData().get(0);
//                    if (returnedEntity != null && returnedEntity.getDbid() != entity.getDbid()) {
//                        // Entity changed
//                        // Do nothing else
//                        oFR.addSuccess(usrMsgService.getUserMessage("ModuleOnlyChangedSuccessfully", loggedUser));
//                        oFR.setReturnedDataMessage(internalFR.getReturnedDataMessage());
//                        return oFR;
//                    }
//                }
//            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Save custom cascade children entities & Return Error If Found">
            OFunctionResult saveCustCascFR;
            saveCustCascFR = saveCustomCascadeEntities(entity, loggedUser);
            // Don't append saveCustCascFR to oFR in case of success to avoid
            // double success message (one for cascade & one for entity)
            if (saveCustCascFR.getErrors().size() > 0) {
                oFR.append(saveCustCascFR);
//                oFR.append(rollbackTransaction(loggedUser, utx));
                return oFR;
            }
            // </editor-fold>

            // Save Entity
            BaseEntity savedEntity = null;
            try {
                savedEntity = oem.saveEntity(entity, loggedUser);
            } catch (UserNotAuthorizedException e) {
                oFR.addError(usrMsgService.getUserMessage("UserNotAuthorized", loggedUser));
            }
            if (oFR.getErrors().size() > 0 || savedEntity == null) {
                return oFR;
            }

            // <editor-fold defaultstate="collapsed" desc="Construct retrun DM">
            ODataMessage returnDM = new ODataMessage();
            returnDM.setODataType(odm.getODataType());
            List returnDMList = new ArrayList();
            savedEntity.setChanged(false);
            returnDMList.add(savedEntity);
            if (odm.getData().size() > 1) {
                returnDMList.add(odm.getData().get(1));
                if (odm.getData().size() > 2) {
                    returnDMList.add(odm.getData().get(2));
                }
            } else {
                returnDMList.add(loggedUser);
            }
            returnDM.setData(returnDMList);
            // </editor-fold>

            oFR.addSuccess(usrMsgService.getUserMessage("DataSaved", loggedUser));
            oFR.setReturnedDataMessage(returnDM);

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            String userMsgName = "SaveError";
            if (ex instanceof FABSException) {
                oFR.addError(usrMsgService.getUserMessage("ModuleError", loggedUser));
            } //  if it is a "UserNotAuthorized" error...
            else if (ex.getClass() == UserNotAuthorizedException.class) {
                logger.debug("This user is not Authorized");
                userMsgName = "UserNotAuthorized";
                oFR.addError(usrMsgService.getUserMessage(userMsgName, loggedUser));
            } //  if it is a validation error...
            // <editor-fold defaultstate="collapsed" desc="Consider OEntityDataValidationException">
            else if (ex instanceof OEntityDataValidationException) {
                userMsgName = "UnknownOEntityValidationError";
                oFR.addError(usrMsgService.getUserMessage(
                        userMsgName, loggedUser));
                if (ex.getClass() == DateFromToException.class) {
                    userMsgName = "DateFromToError";
                    oFR.addError(usrMsgService.getUserMessage(
                            userMsgName, loggedUser));
                } else if (ex.getClass() == DDDataValidationException.class) {
                    userMsgName = "DDDataValidationError";
                    oFR.addError(usrMsgService.getUserMessage(
                            userMsgName, loggedUser));
                }
                //  NEW VALIDATION EXCEPTIONS TO BE CONSIDERED HERE.
            } // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Consider EntityDataValidationException">
            else if (ex instanceof EntityDataValidationException) {
                String fieldName = ((EntityDataValidationException) ex).getFieldName();
                FABSDataValidationType validationType = ((EntityDataValidationException) ex).getDataValidationType();

                String errorMessageName = "";
                switch (validationType) {
                    case Decimal:
                        errorMessageName = "InvalidDecimal";
                        break;
                    case DecimalLTZero:
                        errorMessageName = "InvalidDecimalLTZero";
                        break;
                    case HoursPerDay:
                        errorMessageName = "InvalidHoursPerDay";
                        break;
                    case MonthNumber:
                        errorMessageName = "InvalidMonthNumber";
                        break;
                    case Number:
                        errorMessageName = "InvalidNumber";
                        break;
                    case NumberLTZero:
                        errorMessageName = "InvalidNumberLTZero";
                        break;
                    case Percent:
                        errorMessageName = "InvalidPercent";
                        break;
                    case PercentLTZero:
                        errorMessageName = "InvalidPercentLTZero";
                        break;
                    case PositiveDecimal:
                        errorMessageName = "InvalidPositiveDecimal";
                        break;
                    case PositiveNumber:
                        errorMessageName = "InvalidPositiveNumber";
                        break;
                    case PositivePercent:
                        errorMessageName = "InvalidPositivePercent";
                        break;
                    case WeekDayNumber:
                        errorMessageName = "InvalidWeekDayNumber";
                        break;
                    case DaysPerMonth:
                        errorMessageName = "InvalidDaysPerMonth";
                        break;
                    case Years:
                        errorMessageName = "InvalidYearsNumber";
                        break;
                }
                // <editor-fold defaultstate="collapsed" desc="Construct retrun DM">
                UserMessage errorMsg = usrMsgService.getUserMessage(errorMessageName,
                        Collections.singletonList(fieldName), loggedUser);
                ODataMessage returnDM = new ODataMessage();
                returnDM.setODataType(odm.getODataType());
                List returnDMList = new ArrayList();
                entity.setChanged(false);
                returnDMList.add(entity);
                if (odm.getData().size() > 1) {
                    returnDMList.add(odm.getData().get(1));
                } else {
                    returnDMList.add(loggedUser);
                }
                returnDM.setData(returnDMList);
                oFR.setReturnedDataMessage(returnDM);
                oFR.addError(errorMsg);
                // </editor-fold>
                // </editor-fold>
            }
            if (oFR.getErrors().isEmpty()) {
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
            }

//            oFR.append(rollbackTransaction(loggedUser, utx));
        } finally {
            return oFR;
        }
    }

    @Override
    public OFunctionResult addOEntityActions(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OEntity.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        try {
            oem.getEM(loggedUser);
            OEntity entity = (OEntity) odm.getData().get(0);

            // CREATE ACTION:
            OEntityAction createEntityAction = new OEntityAction();
            UDC createActionType = (UDC) oem.loadEntity("UDC", OEntityAction.ATDBID_CREATE,
                    null, null, oem.getSystemUser(loggedUser));
            createEntityAction.setName("Create " + entity.getEntityClassName());
            createEntityAction.setActionType(createActionType);
            createEntityAction.setOownerEntity(entity);

            JavaFunction addUpdatefunction = (JavaFunction) oem.loadEntity(
                    JavaFunction.class.getSimpleName(), 114, null, null, oem.getSystemUser(loggedUser));
            createEntityAction.setOfunction(addUpdatefunction);

            oFR.append(entitySetupService.
                    callEntityCreateAction(createEntityAction, loggedUser));
            if (oFR.getErrors().size() > 0) {
                return oFR;
            }
            logger.debug("Creating Action");

            // UPDATE ACTION:
            OEntityAction updateEntityAction = new OEntityAction();
            UDC updateActionType = (UDC) oem.loadEntity("UDC", OEntityAction.ATDBID_UPDATE,
                    null, null, oem.getSystemUser(loggedUser));
            updateEntityAction.setName("Update " + entity.getEntityClassName());
            updateEntityAction.setActionType(updateActionType);
            updateEntityAction.setOownerEntity(entity);
            updateEntityAction.setOfunction(addUpdatefunction);

            oFR.append(entitySetupService.callEntityCreateAction(updateEntityAction, loggedUser));
            if (oFR.getErrors().size() > 0) {
                return oFR;
            }
            logger.debug("Updating Action Created");

            // DELETE ACTIONS:
            OEntityAction deleteEntityAction = new OEntityAction();
            UDC deleteActionType = (UDC) oem.loadEntity("UDC", OEntityAction.ATDBID_DELETE,
                    null, null, oem.getSystemUser(loggedUser));
            deleteEntityAction.setName("Delete " + entity.getEntityClassName());
            deleteEntityAction.setActionType(deleteActionType);
            deleteEntityAction.setOownerEntity(entity);

            JavaFunction deleteFunction = (JavaFunction) oem.loadEntity(
                    JavaFunction.class.getSimpleName(), 118, null, null, oem.getSystemUser(loggedUser));
            deleteEntityAction.setOfunction(deleteFunction);

            oFR.append(entitySetupService.callEntityCreateAction(deleteEntityAction, loggedUser));
            if (oFR.getErrors().size() > 0) {
                return oFR;
            }
            logger.debug("Deleting Action Created");
            // DUPLICATE ACTION:
            OEntityAction duplicateEntityAction = new OEntityAction();
            UDC duplicateActionType = (UDC) oem.loadEntity("UDC", OEntityAction.ATDBID_DUPLICATE,
                    null, null, oem.getSystemUser(loggedUser));
            duplicateEntityAction.setName("Duplicate " + entity.getEntityClassName());
            duplicateEntityAction.setActionType(duplicateActionType);
            duplicateEntityAction.setOownerEntity(entity);
            JavaFunction duplicateFunction = (JavaFunction) oem.loadEntity(
                    "JavaFunction", Collections.singletonList("functionName like 'duplicateEntityFunction'"),
                    null, oem.getSystemUser(loggedUser));
            duplicateEntityAction.setOfunction(duplicateFunction);
            oFR.append(entitySetupService.callEntityCreateAction(duplicateEntityAction, loggedUser));
            if (oFR.getErrors().size() > 0) {
                return oFR;
            }
            logger.debug("DUPLICATE Action Created");

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            oem.closeEM(loggedUser);
            oFR.setReturnedDataMessage(odm); // Returned value should
            // be of the OEntity as a standard of action calling
            // whereas, the functionResult will have created OEntityAction
            // from the above calls
            // so, we set it manually again after all the appends above
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult addODataType(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = null;
        inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OEntity.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        try {
            OEntity entity = (OEntity) odm.getData().get(0);
            ODataType entityDataType = new ODataType();
            entityDataType.setName(entity.getEntityClassName());
            entityDataType.setOmodule(entity.getOmodule());

            oFR.append(entitySetupService.
                    callEntityCreateAction(entityDataType, loggedUser));

            ODataType entityListDataType = new ODataType();
            entityListDataType.setName(entity.getEntityClassName() + "List");
            entityListDataType.setOmodule(entity.getOmodule());

            oFR.append(entitySetupService.
                    callEntityCreateAction(entityListDataType, loggedUser));
            if (oFR.getErrors().size() > 0) {
                return oFR;
            }
            logger.debug("DataType Created");
            oFR.addSuccess(usrMsgService.getUserMessage("DataSaved", loggedUser));

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            oFR.setReturnedDataMessage(odm); // Returned value should
            // be of the OEntity as a standard of action calling
            // whereas, the functionResult will have created OEntityAction
            // from the above calls
            // so, we set it manually again after all the appends above
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult addPrivilege(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OEntity.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        OEntity oentity = (OEntity) odm.getData().get(0);
        try {
            ObjectAccessPrivilege objectAccessPrivilege = new ObjectAccessPrivilege();
            objectAccessPrivilege.setName(oentity.getEntityClassName() + " Access Privilege");
            objectAccessPrivilege.setAccessedOEntity(oentity);

            oFR.append(entitySetupService.
                    callEntityCreateAction(objectAccessPrivilege, loggedUser));
            if (oFR.getErrors().size() > 0) {
                logger.debug("Returning");
                logger.debug("Errors greater than 0");
                return oFR;
            }

            oFR.addSuccess(usrMsgService.getUserMessage("DataSaved", loggedUser));
            logger.debug("Returning");
            return oFR;

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("UserNotAuthorized", loggedUser));
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult postCreateObjectAccessPrivilege(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, ObjectAccessPrivilege.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        ObjectAccessPrivilege objAccPriv = (ObjectAccessPrivilege) odm.getData().get(0);
        try {
            ORole roleAdmin;
            RolePrivilege rolePrivilege = new RolePrivilege();
            rolePrivilege.setOprivilege(objAccPriv);

            //oFR.addReturnValue(rolePrivilege) ;
            //if(accessedOEntityOModule.getModuleType().getDbid() == 137579){//Admin
            roleAdmin = (ORole) oem.loadEntity(ORole.class.getSimpleName(),
                    Collections.singletonList("dbid = " + ORole.RLDBID_ADMIN),
                    null, oem.getSystemUser(loggedUser));
            rolePrivilege.setOrole(roleAdmin);
            oFR.append(entitySetupService.callEntityCreateAction(rolePrivilege, oem.getSystemUser(loggedUser)));
            if (oFR.getErrors().size() > 0) {
                return oFR;
            }

            RolePrivilege rolePrivilege4E = new RolePrivilege();
            rolePrivilege4E.setOprivilege(objAccPriv);
            ORole role4E;
            role4E = (ORole) oem.loadEntity(ORole.class.getSimpleName(),
                    Collections.singletonList("dbid = 2"), null, oem.getSystemUser(loggedUser));
            rolePrivilege4E.setOrole(role4E);
            oFR.append(entitySetupService.callEntityCreateAction(rolePrivilege4E, oem.getSystemUser(loggedUser)));
            if (oFR.getErrors().size() > 0) {
                return oFR;
            }
            oFR.addSuccess(usrMsgService.getUserMessage("DataSaved", loggedUser));
            logger.debug("Returning");
            return oFR;

        } catch (UserNotAuthorizedException ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("UserNotAuthorized", loggedUser));
            logger.debug("Returning");
            return oFR;
        }
    }

    public OFunctionResult postCreateEntityAction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, ObjectAccessPrivilege.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        ObjectAccessPrivilege entity = (ObjectAccessPrivilege) odm.getData().get(0);
        try {
            ORole role = (ORole) oem.loadEntity(ORole.class.getSimpleName(),
                    Collections.singletonList("dbid = " + ORole.RLDBID_ADMIN),
                    null, oem.getSystemUser(loggedUser));
            RolePrivilege rolePrivilege = new RolePrivilege();
            rolePrivilege.setOrole(role);
            rolePrivilege.setOprivilege(entity);
            oFR.append(entitySetupService.
                    callEntityCreateAction(rolePrivilege, loggedUser));
            if (oFR.getErrors().size() > 0) {
                return oFR;
            }

            oFR.addSuccess(usrMsgService.getUserMessage(
                    "DataSaved", loggedUser));
            logger.debug("Returning");
            return oFR;

        } catch (UserNotAuthorizedException ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage(
                    "UserNotAuthorized", loggedUser));
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult postAddOEntity(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OEntity.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        OEntity oEntity = (OEntity) odm.getData().get(0);
        try {
            oFR.append(uiFrameworkService.createRequiredScreensOnly(oEntity, loggedUser));
            oFR.append(entitySetupService.createOEntityAccessPrivilege(odm, null, loggedUser));

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }
    /**
     * Used as in functionParms of
     * {@link #deleteEntity(ODataMessage, OFunctionParms, OUser)}
     */
    public static final String DELENTITY_DELTREEKEY = "deleteTree";

    /**
     * Soft delete the entity, it set the entity deleted flag to true and update
     * it <br>If 'functionParameters.parameters' has
     * {@link #DELENTITY_DELTREEKEY}, it then should be Boolean <br>. If it's
     * true then the entity children tree will be soft deleted using every
     * entity own DELETION action <br>. if it's false or functionParameters is
     * null, then only the entity will be deleted
     *
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult activateDeactivateEntity(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, BaseEntity.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = null;
        BaseEntity entity = null;
        try {
            // Initialize variables
            logger.trace("Initialize variables");
            oFR = new OFunctionResult();
            entity = (BaseEntity) odm.getData().get(0);

            // Check whether children tree or just the entity required to be deleted
            boolean activateDeactivateTree = true; //Default to change activity tree
            if (functionParms != null) {
                if (functionParms.getParams() != null) {
                    Boolean deleteTreeObj = (Boolean) functionParms.getParams().get(DELENTITY_DELTREEKEY);
                    if (deleteTreeObj != null) // Parameter found
                    {
                        activateDeactivateTree = deleteTreeObj;
                    }
                }
            }

            if (activateDeactivateTree) // Required to delete the children tree
            {
                GetEntityChildrenTree gect = new GetEntityChildrenTree(entity, null, this, oem);
                gect.setLoggedUser(loggedUser);
                gect.getTreeList();
                List<BaseEntity> tobeChangeActivityStatusEntityList = (List<BaseEntity>) gect.entityChildrenTree;
                // <editor-fold defaultstate="collapsed" desc="check if any has refrence">
                for (BaseEntity currentEntity : tobeChangeActivityStatusEntityList) {

                    List<BaseEntity> returnedRefrenceEntity = entityBaseService.loadNonChildEntityReferences(
                            currentEntity,
                            loggedUser);

                    // <editor-fold defaultstate="collapsed" desc="if child has refrence">
                    if (returnedRefrenceEntity != null) {
                        oFR.addError(
                                usrMsgService.getUserMessage(
                                        "NoChangeActivityStatusEntityHasRefrence",
                                        loggedUser));
                        return oFR;
                    }
                    // </editor-fold>
                }

                // Change Activation Status the entity - VERY IMPORTANT - THE ENTITY DELETION
                oem.activateDeactivateEntityList(tobeChangeActivityStatusEntityList, loggedUser);   // Activate-Deactivate
                // </editor-fold>

                OFunctionParms childFunctionParms = new OFunctionParms();
                childFunctionParms.getParams().put(DELENTITY_DELTREEKEY,
                        false /*Don't delete child tree as it will be deleted here*/);

                for (int i = 1 /*Start from children right after the parent*/;
                        i < tobeChangeActivityStatusEntityList.size(); i++) {
                    // Get The Child Entity
                    BaseEntity childEntity = tobeChangeActivityStatusEntityList.get(i);

                    // Load OEntity
                    OEntity childOEntity = entitySetupService.loadOEntityByClassName(
                            childEntity.getClass().getSimpleName(), oem.getSystemUser(loggedUser));
                    if (childOEntity == null) {
                        //FIXME: generate user message error, rollback, and return
                        logger.error("OEntity Not Found");
                        continue;
                    }

                    // Get Activation Action
                    OEntityAction childActivationActionAction = childOEntity.getActivationAction();
                    if (childActivationActionAction == null) {
                        //FIXME: generate user message error, rollback, and return
                        logger.error("Soft Deletion Action Not Found");
                        continue;
                    }

                    // Construct ODM
                    ODataMessage childODM = null;
                    try {
                        childODM = dataTypeService.constructEntityDefaultODM(childEntity, loggedUser);
                    } catch (Exception ex) {
                        // Catch it here to be able to ignore the entity and continue
                        //FIXME: generate user message error, rollback, and return
                        logger.error("Exception thrown: ", ex);
                        continue;
                    }

                    // Call the soft deletion action
                    OFunctionResult childOFuncResult;
                    childOFuncResult = entitySetupService.executeAction(childActivationActionAction, childODM,
                            childFunctionParms, loggedUser);

                    // Check Return
                    oFR.append(childOFuncResult);
                    if (childOFuncResult.getErrors().size() > 0) {
                        //FIXME: generate user message error, rollback, and return
                    }
                }
            }
            if (oFR.getErrors().size() > 0) {
                return oFR;
            }

            // Generate success user message
            oFR.addSuccess(usrMsgService.getUserMessage("DataRemoved", loggedUser));
            return oFR;
        } catch (Exception ex) {
            if (ex instanceof UserNotAuthorizedException) {
                oFR.addError(usrMsgService.getUserMessage("UserNotAuthorized", loggedUser));
            } else if (ex instanceof FABSException) {
                oFR.addError(usrMsgService.getUserMessage("ModuleError", loggedUser));
            } else {
                // Non FABSException
                logger.error("Exception thrown: ", ex);
                UserMessage errorMessage = usrMsgService.getUserMessage("RemoveError", loggedUser);
                errorMessage.setException(ex);
                oFR.addError(errorMessage);
            }
            //oFR.addReturnValue(entity);
            return oFR;
        }
    }

    /**
     * Delete the entity from database. <br>If 'functionParameters.parameters'
     * has {@link #DELENTITY_DELTREEKEY}, it then should be Boolean <br>. If
     * it's true then the entity children tree will be deleted using every
     * entity own hard deletion action <br>. if it's false or functionParameters
     * is null, then just the entity will be deleted
     *
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult deleteEntity(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, BaseEntity.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = null;
        BaseEntity entity = null;
        try {
            logger.trace("Initialize variables");
            // Initialize variables
            oFR = new OFunctionResult();
            entity = (BaseEntity) odm.getData().get(0);

            // Check whether children tree or just the entity required to be deleted
            boolean deleteTree = true; //Default to delete tree
            if (functionParms != null) {
                if (functionParms.getParams() != null) {
                    Boolean deleteTreeObj = (Boolean) functionParms.getParams().get(DELENTITY_DELTREEKEY);
                    if (deleteTreeObj != null) // Parameter found
                    {
                        deleteTree = deleteTreeObj;
                    }
                }
            }

            if (deleteTree) // Required to delete the children tree
            {
                GetEntityChildrenTree gect = new GetEntityChildrenTree(entity, null, this, oem);
                gect.setLoggedUser(loggedUser);
                gect.getTreeList();
                List<BaseEntity> tobeDelEntityList = (List<BaseEntity>) gect.entityChildrenTree;
                logger.trace("Checking if any has refrence");
                // <editor-fold defaultstate="collapsed" desc="check if any has refrence">
                for (BaseEntity currentEntity : tobeDelEntityList) {

                    List<BaseEntity> returnedRefrenceEntity = null;
                    List<String> oFRParams = new ArrayList<String>();
                    oFRParams.add(currentEntity.getClassName());
                    oFRParams.add(String.valueOf(currentEntity.getDbid()));
                    try {
                        returnedRefrenceEntity = entityBaseService.loadNonChildEntityReferences(
                                currentEntity,
                                loggedUser);
                    } catch (Exception ex) {
                        oFR.addError(usrMsgService.getUserMessage("NoDeleteEntityHasRefrence", oFRParams, loggedUser), ex);
                        logger.error("Exception thrown: ", ex);
                        logger.debug("Returning");
                        return oFR;
                    }
                    // <editor-fold defaultstate="collapsed" desc="if has refrence">
                    if (returnedRefrenceEntity != null && !tobeDelEntityList.containsAll(returnedRefrenceEntity)) {
                        int count = 0;
                        for (int i = 0; i < returnedRefrenceEntity.size(); i++) {
                            BaseEntity foundEntity = returnedRefrenceEntity.get(i);
                            if (foundEntity instanceof EntityAuditTrail || foundEntity instanceof ScreenSessionInfo
                                    || foundEntity instanceof DBIDGeneration
                                    || foundEntity instanceof ServerJobInbox || foundEntity instanceof ServerJob
                                    || foundEntity instanceof JavaFunction) {
                                continue;
                            }
                            count++;
                        }
                        if (count != 0) {
                            oFR.addError(usrMsgService.getUserMessage("NoDeleteEntityHasRefrence", oFRParams, loggedUser));
                            return oFR;
                        }
                    }
                    // </editor-fold>
                }
                // </editor-fold>

                OFunctionParms childFunctionParms = new OFunctionParms();
                childFunctionParms.getParams().put(DELENTITY_DELTREEKEY,
                        false /*Don't delete child tree as it will be deleted here*/);

                for (int i = tobeDelEntityList.size() - 1 /*Start from the leaf children*/;
                        i >= 1 /*Don't Delete Grand Parent, will be deleted below*/;
                        i--) {
                    // Get The Child Entity
                    BaseEntity childEntity = tobeDelEntityList.get(i);

                    // Load OEntity
                    OEntity childOEntity = entitySetupService.loadOEntityByClassName(
                            childEntity.getClass().getSimpleName(),
                            oem.getSystemUser(loggedUser));
                    if (childOEntity == null) {
                        //FIXME: generate user message error, rollback, and return
                        logger.error("OEntity Not Found");
                        continue;
                    }

                    // Get Hard Deletion Action
                    OEntityAction childDeletionAction = childOEntity.getDeleteAction();
                    if (childDeletionAction == null) {
                        //FIXME: generate user message error, rollback, and return
                        logger.error("Hard Deletion Action Not Found");
                        continue;
                    }

                    // Construct ODM
                    ODataMessage childODM = null;
                    try {
                        childODM = dataTypeService.constructEntityDefaultODM(childEntity, loggedUser);
                    } catch (Exception ex) {
                        // Catch it here to be able to ignore the entity and continue
                        //FIXME: rollback, and return
                        logger.error("Exception thrown: ", ex);
                        functionService.constructOFunctionResultFromError("SystemInternalError", ex, loggedUser);
                        continue;
                    }

                    // Call the hard deletion action
                    OFunctionResult childOFuncResult = new OFunctionResult();

                    childOFuncResult = entitySetupService.executeAction(childDeletionAction, childODM,
                            childFunctionParms, loggedUser);
                    logger.debug("Returning");
                    // Check Return
                    oFR.append(childOFuncResult);
                    if (childOFuncResult.getErrors().size() > 0) {
                        return oFR;
                    }
                }
            }

            // Delete the entity (parent) after all children deletion (if any)
            // to make sure no relations are there when deleted
            logger.trace("Delete the entity (parent) after all children deletion");
            oem.deleteEntity(entity, loggedUser);

            if (oFR.getErrors().size() > 0) {
                return oFR;
            }

            // Generate success user message
            UserMessage succmsg = usrMsgService.getUserMessage("DataRemoved", loggedUser);
            if (succmsg != null) {
                oFR.addSuccess(succmsg);
            } else {
                logger.debug("User Message Not Found");
            }
            return oFR;

        } catch (Exception ex) {

            if (ex instanceof UserNotAuthorizedException) {
                oFR.addError(usrMsgService.getUserMessage("UserNotAuthorized", loggedUser));
            } else if (ex instanceof FABSException) {
                //FIXME: generate user message error from exception
                oFR.addError(usrMsgService.getUserMessage("RemoveError", loggedUser));
            } else {
                // Non FABSException
                logger.error("Exception thrown: ", ex);
                oFR.addError(usrMsgService.getUserMessage("RemoveError", loggedUser));
            }
            //oFR.addReturnValue(entity);
            return oFR;
        }
    }
    //FIXME: Commented from version r18.1.7 as there's no usage
//    public OFunctionResult modifyDBIDs(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
//        logger.debug("Entering");
//        OFunctionResult oFR = new OFunctionResult();
//        ArrayList<OEntity> allOEntities = null;
//        try {
//            oem.getEM(loggedUser);
//            allOEntities = (ArrayList<OEntity>) oem.loadEntityList("OEntity",
//                    usrMsgService.constructListFromStrings(OEntityManager.CONFCOND_GET_ACTIVEANDINACTIVE),
//                    null, null, loggedUser);
//
//        } catch (Exception ex) {
//            logger.error("Exception thrown: ", ex);
//            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
//            logger.debug("Returning");
//            return oFR;
//        }
//
//        for (OEntity oEntity : allOEntities) {
//            try {
//                Class entityClass = null;
//                Object entityClassObject = null;
//
//                entityClass = Class.forName(oEntity.getEntityClassPath());
//                entityClassObject = entityClass.newInstance();
//
//                // Entity must have @Entity
//                Entity entityAnnotation = (Entity) entityClass.getAnnotation(Entity.class);
//                if (entityAnnotation == null) {
//                    logger.trace("Entity: {} Doesn't have @Entity", oEntity.getEntityClassName());
//                    continue;
//                }
//
//                if (!BaseEntity.class.isInstance(entityClassObject)) {
//                    logger.trace("Entity: {} Doesn't Extend BaseEntity", oEntity.getEntityClassName());
//                    continue;
//                }
//
//                // 'abstract' classes shouldn't be instantiated
//                int mod = entityClass.getModifiers();
//                if (Modifier.isAbstract(mod)) {
//                    logger.trace("Entity: {} is Abstract", oEntity.getEntityClassName());
//                    continue;
//                }
//
//                // @MappedSuperClass entities shouldn't be added to "Manage OEntities" Screen
//                MappedSuperclass mappedSuperclass = (MappedSuperclass) entityClass.getAnnotation(MappedSuperclass.class);
//                if (mappedSuperclass != null) {
//                    logger.trace("Entity: {} MappedSuperClass", oEntity.getEntityClassName());
//                    continue;
//                }
//                VersionControlSpecs vcs = (VersionControlSpecs) entityClass.getAnnotation(VersionControlSpecs.class);
//                if (vcs == null) {
//                    logger.trace("Entity: {} Has no Version Control", oEntity.getEntityClassName());
//                    continue;
//                }
//
//                logger.trace("Entity: {} Has Version Control", oEntity.getEntityClassName());
//                List<BaseEntity> entityList = oem.loadEntityList(oEntity.getEntityClassName(), Collections.singletonList(
//                        OEntityManager.CONFCOND_GET_ACTIVEANDINACTIVE), null, null, loggedUser);
//                for (int i = 0; i < entityList.size(); i++) {
//                    BaseEntity entity = entityList.get(i);
//                    logger.debug("Passing Entity: {}", entity.toString());
//                    BaseEntity toBeMergedEntity = dbidGenerationService.generateDBID(entity, loggedUser);
//                    oem.saveEntity(toBeMergedEntity, loggedUser);
//                }
//            } catch (Exception ex) {
//                if (ex instanceof FABSException) {
//                    oFR.addError(usrMsgService.getUserMessage("ModuleError", loggedUser));
//                }
//                logger.error("Exception thrown: ", ex);
//                continue;
//            }
//        }
//        logger.debug("Returning - Done DBID Modifications");
//        return oFR;
//    }

    /**
     * Loads Duplicate Action for passed entity and executes it, If not found,
     * it generates it, then executes it
     *
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult duplicateEntity(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, BaseEntity.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        BaseEntity entity = (BaseEntity) odm.getData().get(0);
        try {
            oem.getEM(loggedUser);
            OEntity oentity = entitySetupService.loadOEntityByClassName(entity.getClassName(), loggedUser);
            if (oentity == null) {
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
                return oFR;
            }
            ODataMessage actionODM = new ODataMessage();
            actionODM = dataTypeService.constructEntityDefaultODM(oentity, loggedUser);
            OEntityAction duplicateAction = oentity.getAction(OEntityAction.ATDBID_DUPLICATE);
            // If no duplication action is found, call the OEntity Post Action
            logger.trace("no duplication action is found, call the OEntity Post Action");
            if (duplicateAction == null) {
                duplicateAction = (OEntityAction) generateDuplicateAction(
                        actionODM, functionParms, loggedUser).getReturnValues().get(0);

                // Call the Action
                logger.trace("Call the Action");
                actionODM = dataTypeService.constructEntityDefaultODM(entity, loggedUser);
                oFR.append(entitySetupService.executeAction(duplicateAction, actionODM,
                        null, loggedUser));
            } else {
                // Entities that already have duplicate action
                logger.trace("Entities that already have duplicate action");
                oFR.append(entitySetupService.executeAction(duplicateAction, odm,
                        null, loggedUser));
            }
            // Save custom cascade children entities
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {
            oem.closeEM(loggedUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    /**
     * Special Duplication Action for OPortalPage Entity
     *
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult duplicateOPortalPage(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OPortalPage.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        OPortalPage page = (OPortalPage) odm.getData().get(0);
        try {
            OFunctionResult returnedMessage = duplicatePortalPage(page, loggedUser);
            oFR.append(returnedMessage);
            OPortalPage returnedDuplicatedPage = (OPortalPage) returnedMessage.
                    getReturnedDataMessage().getData().get(0);

            List<Integer> parentsPositionsList = new ArrayList<Integer>();
            returnedMessage = duplicatePagePortlets(page.getDbid(), returnedDuplicatedPage, parentsPositionsList, loggedUser);
            oFR.append(returnedMessage);

            returnedMessage = duplicatePortalFunctions(page.getDbid(), returnedDuplicatedPage, loggedUser);
            oFR.append(returnedMessage);

            returnedMessage = updatePagePortals(returnedDuplicatedPage.getDbid(), parentsPositionsList, loggedUser);
            oFR.append(returnedMessage);
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    private OFunctionResult duplicatePortalPage(OPortalPage page, OUser loggedUser) {
        logger.debug("Entering");
        try {
            // Duplicate the Page
            logger.trace("Duplicating the Page");
            OPortalPage duplicatedPage = new OPortalPage();
            duplicatedPage.setName(page.getName() + "_1");
            duplicatedPage.setHeaderTranslated(page.getHeaderTranslated());
            duplicatedPage.setOmodule(page.getOmodule());
            OFunctionResult retOFRPage = entitySetupService.callEntityCreateAction(
                    duplicatedPage, loggedUser);
            return retOFRPage;
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        logger.debug("Returning");
        return new OFunctionResult();
    }

    private OFunctionResult duplicatePortalFunctions(long pageId, OPortalPage page, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            // Duplicate the Functions
            logger.trace("Duplicating the Page");
            List<PortalPageFunction> originalFunctionsList = new ArrayList<PortalPageFunction>();
            originalFunctionsList = (List<PortalPageFunction>) oem.loadEntityList(
                    "PortalPageFunction", Collections.singletonList("portalPage.dbid = "
                            + pageId), null, null, loggedUser);

            for (int functionIndex = 0; functionIndex < originalFunctionsList.size(); functionIndex++) {
                PortalPageFunction currentFunction = originalFunctionsList.get(functionIndex);
                // Make a new function
                PortalPageFunction duplicatedFunction = new PortalPageFunction();
                duplicatedFunction.setOdataType(currentFunction.getOdataType());
                duplicatedFunction.setScreenMode(currentFunction.getScreenMode());
                duplicatedFunction.setCode(currentFunction.getCode() + "_$");
                duplicatedFunction.setName(currentFunction.getName() + "_$");
//                duplicatedFunction.setNameTranslated(currentFunction.getNameTranslated() + "_$");
                duplicatedFunction.setDescription(currentFunction.getDescription());
                duplicatedFunction.setDescriptionTranslated(currentFunction.getDescriptionTranslated());
                duplicatedFunction.setReserved(currentFunction.isReserved());
                duplicatedFunction.setServerJob(currentFunction.isServerJob());
                duplicatedFunction.setMenuFunction(currentFunction.getMenuFunction());
                duplicatedFunction.setPortalPage(page);
                // Create the duplicated Functions
                logger.trace("Create the duplicated Functions");
                OFunctionResult retOFR = entitySetupService.callEntityCreateAction(duplicatedFunction, loggedUser);
                oFR.append(retOFR);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        return oFR;
    }

    private OFunctionResult updatePagePortals(long pageId, List<Integer> parentsPositionsList, OUser loggedUser) {
        logger.info("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            List<String> sort = new ArrayList<String>();
            sort.add("dbid asc");
            // Get duplicated portlets from db after saving
            List<PortalPagePortlet> duplicatedPortletsFromDB = (List<PortalPagePortlet>) oem.loadEntityList(
                    "PortalPagePortlet", Collections.singletonList("portalPage.dbid = "
                            + pageId), null, sort, loggedUser);

            // Set Duplicated Portlets Parents Accoring to parentsPositionsList list
            for (int i = 0; i < duplicatedPortletsFromDB.size(); i++) {
                PortalPagePortlet duplicatedPortlet = duplicatedPortletsFromDB.get(i);
                int parentPosition = parentsPositionsList.get(i);
                // If no Parent
                if (parentPosition == -1) {
                    continue;
                }
                duplicatedPortlet.setParentPortlet(duplicatedPortletsFromDB.get(parentPosition));

                // Update the duplicated Portlet
                oFR.append(entitySetupService.callEntityUpdateAction(duplicatedPortlet, loggedUser));
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        logger.info("Returning");
        return oFR;
    }

    private OFunctionResult duplicatePagePortlets(long pageId, OPortalPage page, List<Integer> parentsPositionsList, OUser loggedUser) {
        logger.info("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            List<PortalPagePortlet> originalPortlets = (List<PortalPagePortlet>) oem.loadEntityList(
                    "PortalPagePortlet", Collections.singletonList("portalPage.dbid = "
                            + pageId), null, null, loggedUser);
            // Duplicate the portlets
            logger.trace("Duplicating the portlets");
            List<PortalPagePortlet> duplicatedPortletsList = new ArrayList<PortalPagePortlet>();
            for (int portletIndex = 0; portletIndex < originalPortlets.size(); portletIndex++) {
                // Construct a map that contains the parent index
                PortalPagePortlet currentPortlet = originalPortlets.get(portletIndex);
                PortalPagePortlet parentPortlet = currentPortlet.getParentPortlet();

                // If no parent, set position to -1 in order to mark the null parent position
                if (parentPortlet == null) {
                    parentsPositionsList.add(-1);
                } else {
                    parentsPositionsList.add(originalPortlets.indexOf(parentPortlet));
                }

                // Make a new portlet
                logger.trace("Creating a new portlet");
                PortalPagePortlet duplicatedPortlet = new PortalPagePortlet();
                duplicatedPortlet.setScreen(currentPortlet.getScreen());
                duplicatedPortlet.setColumn(currentPortlet.getColumn());
                duplicatedPortlet.setHeight(currentPortlet.getHeight());
                duplicatedPortlet.setWidth(currentPortlet.getWidth());
                duplicatedPortlet.setMain(currentPortlet.isMain());
                duplicatedPortlet.setPosition(currentPortlet.getPosition());
                duplicatedPortlet.setPortalPage(page);

//                // Create the duplicated Portlets
                logger.trace("Creating the duplicated Portlets");
                OFunctionResult retOFR = entitySetupService.callEntityCreateAction(duplicatedPortlet, loggedUser);
                oFR.append(retOFR);
                PortalPagePortlet returnedDuplicatedPortlet = (PortalPagePortlet) retOFR.getReturnedDataMessage().getData().get(0);

//                // Add to a list to later assign parents
                logger.trace("Adding to a list to later assign parents");
                duplicatedPortletsList.add(returnedDuplicatedPortlet);
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        }
        logger.info("Returning");
        return oFR;
    }

    /**
     * Generates the duplication action for the passed OEntity
     *
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult generateDuplicateAction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, OEntity.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        OEntity oentity = (OEntity) odm.getData().get(0);

        try {
            oem.getEM(loggedUser);
            OEntityAction duplicateAction = oentity.getAction(OEntityAction.ATDBID_DUPLICATE);
            if (duplicateAction == null) {
                duplicateAction = new OEntityAction();
                UDC duplicateActionType = (UDC) oem.loadEntity("UDC", OEntityAction.ATDBID_DUPLICATE,
                        null, null, oem.getSystemUser(loggedUser));
                duplicateAction.setName("Duplicate " + oentity.getEntityClassName());
                duplicateAction.setActionType(duplicateActionType);
                duplicateAction.setOownerEntity(oentity);

                JavaFunction duplicateFunction = (JavaFunction) oem.loadEntity(
                        "JavaFunction", Collections.singletonList("functionName like 'duplicateEntityFunction'"), null, oem.getSystemUser(loggedUser));
                duplicateAction.setOfunction(duplicateFunction);
                oFR.append(entitySetupService.callEntityCreateAction(duplicateAction, loggedUser));
                if (oFR.getErrors().size() > 0) {
                    return oFR;
                }
                logger.trace("Duplicating Action Created");
                oFR.setReturnValues(Collections.singletonList(duplicateAction));
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
        } finally {
            logger.debug("Returning");
            oem.closeEM(loggedUser);
            return oFR;
        }
    }
}
