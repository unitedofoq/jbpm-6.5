/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.content.alfresco;

import com.unitedofoq.fabs.core.content.AttachmentDTO;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.DocumentType;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.Repository;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.FolderImpl;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.Action;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.exceptions.CmisConnectionException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisUnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Asaied
 */
public class AlfrescoConnection {
    
    final static Logger logger = LoggerFactory.getLogger(AlfrescoConnection.class);
    
    private static Map<String, Session> connections = new ConcurrentHashMap<String, Session>();
    
    public Session getSession(String AlfrescoUrl, String connectionName, String username, String pwd) {
        
        Session session = connections.get(connectionName);
        if (session == null) {
            logger.debug("Not connected, creating new connection to Alfresco with the connection id ({0})",connectionName);

            // No connection to Alfresco available, create a new one
            SessionFactory sessionFactory = SessionFactoryImpl.newInstance();
            Map<String, String> parameters = new HashMap<String, String>();
            parameters.put(SessionParameter.USER, username);
            parameters.put(SessionParameter.PASSWORD, pwd);
            parameters.put(SessionParameter.ATOMPUB_URL,
                    (AlfrescoUrl.endsWith("/")) ? AlfrescoUrl : AlfrescoUrl + "/" + "alfresco/api/-default-/cmis/versions/1.1/atom");
            parameters.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
            parameters.put(SessionParameter.COMPRESSION, "true");
            parameters.put(SessionParameter.CACHE_TTL_OBJECTS, "0");
            
            List<Repository> repositories = sessionFactory.getRepositories(parameters);
            Repository alfrescoRepository = null;
            if (repositories != null && repositories.size() > 0) {
                logger.debug("Found ({0}) Alfresco repositories",repositories.size());
                alfrescoRepository = repositories.get(0);
                logger.debug("Info about the first Alfresco repo [ID={0}][name={1}][CMIS ver supported={2}]", alfrescoRepository.getId(), alfrescoRepository.getName(), alfrescoRepository.getCmisVersionSupported());
            } else {
                throw new CmisConnectionException(
                        "Could not connect to the Alfresco Server, no repository found!");
            }

            // Create a new session with the Alfresco repository
            session = alfrescoRepository.createSession();

            // Save connection for reuse
            connections.put(connectionName, session);
        } else {
            logger.debug("Already connected to Alfresco with the connection id ({0})", connectionName);
        }
        
        return session;
    }
    
    
    public Folder getFolder(Session session, Folder rootFolder, String name) {
        
        Folder root = rootFolder;
        ItemIterable<CmisObject> contentItems = root.getChildren();
        for (CmisObject contentItem : contentItems) {
            if (contentItem instanceof FolderImpl) {
                Folder folder = (FolderImpl) contentItem;
                if (folder.getName().equalsIgnoreCase(name)) {
                    
                    return folder;
                }
            }
        }
        return null;
    }
    
    public Folder createFolder(Session session, Folder parentFolder,String folderName) {

        // Make sure the user is allowed to create a folder under the root folder
        if (parentFolder.getAllowableActions().getAllowableActions().contains(Action.CAN_CREATE_FOLDER) == false) {
            throw new CmisUnauthorizedException("Current user does not have permission to create a sub-folder in "
                    + parentFolder.getPath());
        }

        // Check if folder already exist, if not create it
        Folder newFolder = (Folder) getFolder(session, parentFolder, folderName);
        if (newFolder == null) {
            Map<String, Object> newFolderProps = new HashMap<String, Object>();
            newFolderProps.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
            newFolderProps.put(PropertyIds.NAME, folderName);
            newFolder = parentFolder.createFolder(newFolderProps);
            
        } else {
            logger.debug("Folder already exist: {0} ", newFolder.getPath());
        }
        
        return newFolder;
    }
    
    public void deleteDocument(Session session, String documentId) {
        try {
            getObjectById(session, documentId).delete();            
        } catch (Exception ex) {
            logger.error("Unable to Delete Document {0}" , ex.getMessage());
        }
        
    }
    
    public Document createDocument(Session session, Folder folder, AttachmentDTO attachment) throws IOException {
        // Make sure the user is allowed to create a document in the passed in folder
        if (folder.getAllowableActions().getAllowableActions().contains(Action.CAN_CREATE_DOCUMENT) == false) {
            throw new CmisUnauthorizedException("Current user does not have permission to create a document in "
                    + folder.getPath());
        }

        Document newDocument = null;
        // Setup document metadata
        Map<String, Object> newDocumentProps = new HashMap<String, Object>();
        String typeId = "cmis:document";
        newDocumentProps.put(PropertyIds.OBJECT_TYPE_ID, typeId);
        newDocumentProps.put(PropertyIds.NAME, attachment.getName());

        // Setup document content
        ByteArrayInputStream input = new ByteArrayInputStream(attachment.getAttachment());
        ContentStream contentStream = session.getObjectFactory().createContentStream(
                attachment.getName(), attachment.getAttachment().length, attachment.getMimeType(), input);

        // Check if we need versioning
        VersioningState versioningState = VersioningState.MAJOR;
        DocumentType docType = (DocumentType) session.getTypeDefinition(typeId);
        if (Boolean.TRUE.equals(docType.isVersionable())) {
            logger.debug("Document type {0} is versionable, setting MAJOR version state.",typeId);
            versioningState = VersioningState.MAJOR;
        }
        
        try {
            // Create versioned document object
            newDocument = folder.createDocument(newDocumentProps, contentStream, versioningState);
        } catch (Exception ex) {
            logger.error("Unable to Create Document {0}" , ex.getMessage());
        }
        
        return newDocument;
    }
    
    public AttachmentDTO convertDocumentToAttachmentDTO(Document document) throws IOException {
        AttachmentDTO atchmentDTO = new AttachmentDTO();
        atchmentDTO.setId(document.getId());
        atchmentDTO.setMimeType(document.getContentStream().getMimeType());
        atchmentDTO.setAttachment(org.apache.commons.io.IOUtils.toByteArray(document.getContentStream().getStream()));
        atchmentDTO.setName(document.getContentStream().getFileName());
        atchmentDTO.setDescription(document.getDescription());
        return atchmentDTO;
    }
    
    
    public CmisObject getObjectById(Session session, String objectid) {
        CmisObject object = null;
        
        try {
            
            object = session.getObject(objectid);
        } catch (CmisObjectNotFoundException nfe0) {
            logger.error("Unable get object {0} " , nfe0);
        }
        
        return object;
    }
    
}
