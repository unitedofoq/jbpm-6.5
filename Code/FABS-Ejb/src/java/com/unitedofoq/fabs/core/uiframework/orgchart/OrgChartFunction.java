/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.orgchart;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.udc.UDC;

/**
 *
 * @author bassem
 */
@Entity
@DiscriminatorValue(value = "ORGCHART")
@ChildEntity(fields={"functionImage", "menuFunction"})
@VersionControlSpecs(omoduleFieldExpression = "omodule")
public class OrgChartFunction extends OFunction {
  // <editor-fold defaultstate="collapsed" desc="OModule">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OModule omodule;

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }

    public String getOmoduleDD() {
        return "OrgChartFunction_omodule";
    }
    // </editor-fold>
  // <editor-fold defaultstate="collapsed" desc="viewPage">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC viewPage;
    public String getViewPageDD() {
        return "OrgChartFunction_viewPage";
    }    
    public UDC getViewPage() {
        return viewPage;
    }

    public void setViewPage(UDC viewPage) {
        this.viewPage = viewPage;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="portletHeight">
  private int portletHeight;

    /**
     * @return the portletHeight
     */
    public int getPortletHeight() {
        return portletHeight;
    }

    /**
     * @param portletHeight the portletHeight to set
     */
    public void setPortletHeight(int portletHeight) {
        this.portletHeight = portletHeight;
    }
       /**
     * @return the portletHeight
     */
    public String getPortletHeightDD() {
        return "OrgChartFunction_portletHeight";
    }
    // </editor-fold>
}
