package com.unitedofoq.fabs.core.datatype;

import java.lang.reflect.Field;
import java.util.List;

import javax.ejb.Local;

import org.w3c.dom.Element;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.process.OProcess;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.validation.FABSException;

@Local
public interface DataTypeServiceRemote {

    public String generateXML(Element element);
    public Element generateElement(String XMLstring);

    public String generateXML(ODataMessage dm);

    public String generateSOAPMessage(ODataMessage dm, OProcess proc);
    public SOAPMessage generateProcessSOAPMessage(OProcess process, ODataMessage odm);
    public String printEntityTag(String indentation, Object obj, boolean SOAP, int seq);
    public Field[] getFields(Class cls);
    
    public ODataMessage generateODataMessage(Element data, ODataType dt);
    public Object generateObject(Element node, Class cls);

    public void generateODataType(BaseEntity entity, OUser loggedUser);
    public void generateODataType(OEntity oEntity,OUser loggedUser);

    public List<ODataTypeAttribute> sortAttributes(List<ODataTypeAttribute> attributes);

    /**
     * Refer to {@link DataTypeService#constructEntityDefaultODM(BaseEntity, OUser) }
     */
    public ODataMessage constructEntityDefaultODM(BaseEntity entity, OUser loggedUser)
            throws FABSException ;

    public ODataType loadODataType(String dtName , OUser loggedUser);

    public ODataMessage constructSingleValueObjectDM(Object value, OUser loggedUser);
    
    public byte[] serializeDataMessage(ODataMessage odm, OUser loggOUser);
    
    public ODataMessage deSerializeDataMessage(byte[] odmByte , OUser loggOUser);

    public boolean isODMValid(ODataMessage odm) ;

    public OFunctionResult preODataTypeCachedUpdate(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult clearODataTypeCache(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    
    public OFunctionResult onODataTypeCachedUpdate(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult onODataTypeCachedRemove(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult createMissingDataType(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

}
