/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitysetup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;

import com.unitedofoq.fabs.core.security.user.OUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author arezk
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.entitysetup.EntityServiceLocal",
        beanInterface = EntityServiceLocal.class)
public class EntityService implements EntityServiceLocal {

    @EJB
    private OEntityManagerRemote oem;

    @EJB
    private DataTypeServiceRemote dtService;

    final static Logger logger = LoggerFactory.getLogger(EntityService.class);

    @Override
    public ODataMessage constructEntityODataMessage(BaseEntity entity, OUser loggedUser) {
        logger.debug("Entering");
        try {
            String entityName = entity.getClassName();
            ODataType entityDataType = dtService.loadODataType(entityName, loggedUser);

            if (entityDataType == null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("Entity DataType Not Found. EntityName: {}, Entity: {}", entityName, entity);
                // </editor-fold>
                logger.debug("Returning with Null");
                return null;
            }
            List list = new ArrayList();
            Collections.addAll(list, entity, oem.getSystemUser(loggedUser));
            ODataMessage dataMessage = new ODataMessage();
            dataMessage.setData(list);
            dataMessage.setODataType(entityDataType);
            if (null != dataMessage) {
                logger.debug("Returning with: ", dataMessage.toString());
            }
            return dataMessage;
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            if (null != entity) {
                logger.debug("Entity: {}", entity.getClassName());
            }
            // </editor-fold>
            logger.debug("Returning with Null");
            return null;
        }
    }
    @Override
    public ODataMessage constructEntityListODataMessage(BaseEntity entity, List<String> filterConds, OUser loggedUser) {
        logger.debug("Entering");
        ODataMessage dataMessage = constructEntityODataMessage(entity, loggedUser);
        dataMessage.getData().addAll(filterConds);
        return dataMessage;
    }
}
