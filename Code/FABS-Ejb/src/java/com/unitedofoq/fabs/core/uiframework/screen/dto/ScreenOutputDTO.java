/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mostafa
 */
public class ScreenOutputDTO {

    public ScreenOutputDTO(long odataTypeDBID, long dbid) {
        this.odataTypeDBID = odataTypeDBID;
        this.dbid = dbid;
    }
    
    private long odataTypeDBID;
    private long dbid;
    private List<ScreenDTMappingAttrDTO> attributesMapping = new ArrayList<ScreenDTMappingAttrDTO>();

    /**
     * @return the odataTypeDBID
     */
    public long getOdataTypeDBID() {
        return odataTypeDBID;
    }

    /**
     * @param odataTypeDBID the odataTypeDBID to set
     */
    public void setOdataTypeDBID(long odataTypeDBID) {
        this.odataTypeDBID = odataTypeDBID;
    }

    /**
     * @return the attributesMapping
     */
    public List<ScreenDTMappingAttrDTO> getAttributesMapping() {
        return attributesMapping;
    }

    /**
     * @param attributesMapping the attributesMapping to set
     */
    public void setAttributesMapping(List<ScreenDTMappingAttrDTO> attributesMapping) {
        this.attributesMapping = attributesMapping;
    }

    /**
     * @return the dbid
     */
    public long getDbid() {
        return dbid;
    }

    /**
     * @param dbid the dbid to set
     */
    public void setDbid(long dbid) {
        this.dbid = dbid;
    }
}
