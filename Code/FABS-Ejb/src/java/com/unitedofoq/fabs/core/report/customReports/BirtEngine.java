///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
package com.unitedofoq.fabs.core.report.customReports;
//
import java.io.File;
import java.util.Properties;

import org.eclipse.birt.core.framework.IPlatformContext;
import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.EngineConfig;
import org.eclipse.birt.report.engine.api.EngineConstants;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportEngineFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//
public class BirtEngine {

    private static IReportEngine birtEngine = null;
    private static Properties configProps = new Properties();

    final static Logger logger = LoggerFactory.getLogger(BirtEngine.class);
//private final static String configFile = "BirtConfig.properties";
    public static synchronized void initBirtConfig() {
        loadEngineProps();
    }

    public static IReportEngine getBirtEngine(IPlatformContext context) {
        logger.debug("Entering");
        if (birtEngine == null) {
            loadEngineProps();
            EngineConfig config = new EngineConfig();

            config.getAppContext().put(EngineConstants.APPCONTEXT_CLASSLOADER_KEY, Thread.currentThread().getContextClassLoader());
            config.setPlatformContext(context);

            try {
                Platform.startup(config);
            } catch (Exception ex) {
                logger.error("Exception thrown: ",ex);
            }

            IReportEngineFactory factory = (IReportEngineFactory) Platform
                    .createFactoryObject(IReportEngineFactory.EXTENSION_REPORT_ENGINE_FACTORY);

            try {
                birtEngine = factory.createReportEngine(config);
            } catch (Exception ex) {
                logger.error("Exception thrown: ",ex);
            }

        }
        logger.debug("Returning");
        return birtEngine;
    }

    public static synchronized void destroyBirtEngine() {
        if (birtEngine == null) {
            return;
        }
        birtEngine.shutdown();
        Platform.shutdown();
        birtEngine = null;
    }

    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    private static void loadEngineProps() {
            configProps.setProperty("logDirectory", System.getProperty("java.io.tmpdir"));
            configProps.setProperty("logLevel","INFO");
    }
}
