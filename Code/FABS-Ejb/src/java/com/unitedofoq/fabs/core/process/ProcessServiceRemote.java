/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.process;


import java.util.List;

import javax.ejb.Local;


import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.process.jBPM.ProcessInstance;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.Outbox;
import java.util.Map;

 
/**
 *
 * @author melsayed
 */
@Local
public interface ProcessServiceRemote {

    public String getProcessNameFromURL(String processWSDLURL);

    public void startProcess(OProcess process, ODataMessage processInput, OUser loggedUser);

    public String getProcessEngineURL(OUser loggedUser);

    public OFunctionResult doProcessAction(OProcessAction oProcessAction, List<Object> data, OUser loggedUser);
    
    public List<Outbox> getUserStartedProcesses(OUser loggedUser, String sortExpression);
        
    public Map<Long, List<ProcessInstance>> getAllProcessInstances(OUser loggedUser,
            int page, int pageSize, String sortedField, String sortingOrder,
            Map<String, String> filters);
    
    public List<String> getGroupProcesses (Long groupID , OUser loggedUser);
    
}

