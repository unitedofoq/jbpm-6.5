/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

/**
 *
 * @author mmohamed
 */
@Local
public interface UIFrameworkEntityManagerRemote {

    public OFunctionResult saveEntity(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult saveScreenFieldsDDs(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult activateDeactivateEntity(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult duplicateEntity(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult duplicateOPortalPage(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult generateDuplicateAction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult addOEntityActions(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult addODataType(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult addPrivilege(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult postCreateObjectAccessPrivilege(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult postAddOEntity(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult deleteEntity(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
                    //FIXME: Commented from version r18.1.7 as there's no usage
//    public OFunctionResult modifyDBIDs(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
}
