/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.mailmerge;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author 3dly
 */
@Local
public interface MailMergeServiceLocal {

    public OFunctionResult generateTemplate(
            ODataMessage oDM, OFunctionParms funcParms, OUser loggedUser);
}
