/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.process.Intalio;

/**
 *
 * @author melsayed
 */
public class IntalioProcess {
    private String processId;
    private Long version;
    private String Status;
    private String name;
    private Long activeCount;
    private Long completedCount;
    private Long errorCount;
    private Long failedCount;
    private Long suspendedCount;
    private Long terminatedCount;

    /**
     * @return the processId
     */
    public String getProcessId() {
        return processId;
    }

    /**
     * @param processId the processId to set
     */
    public void setProcessId(String processId) {
        this.processId = processId;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the Status
     */
    public String getStatus() {
        return Status;
    }

    /**
     * @param Status the Status to set
     */
    public void setStatus(String Status) {
        this.Status = Status;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the activeCount
     */
    public Long getActiveCount() {
        return activeCount;
    }

    /**
     * @param activeCount the activeCount to set
     */
    public void setActiveCount(Long activeCount) {
        this.activeCount = activeCount;
    }

    /**
     * @return the completedCount
     */
    public Long getCompletedCount() {
        return completedCount;
    }

    /**
     * @param completedCount the completedCount to set
     */
    public void setCompletedCount(Long completedCount) {
        this.completedCount = completedCount;
    }

    /**
     * @return the errorCount
     */
    public Long getErrorCount() {
        return errorCount;
    }

    /**
     * @param errorCount the errorCount to set
     */
    public void setErrorCount(Long errorCount) {
        this.errorCount = errorCount;
    }

    /**
     * @return the failedCount
     */
    public Long getFailedCount() {
        return failedCount;
    }

    /**
     * @param failedCount the failedCount to set
     */
    public void setFailedCount(Long failedCount) {
        this.failedCount = failedCount;
    }

    /**
     * @return the suspendedCount
     */
    public Long getSuspendedCount() {
        return suspendedCount;
    }

    /**
     * @param suspendedCount the suspendedCount to set
     */
    public void setSuspendedCount(Long suspendedCount) {
        this.suspendedCount = suspendedCount;
    }

    /**
     * @return the terminatedCount
     */
    public Long getTerminatedCount() {
        return terminatedCount;
    }

    /**
     * @param terminatedCount the terminatedCount to set
     */
    public void setTerminatedCount(Long terminatedCount) {
        this.terminatedCount = terminatedCount;
    }
    

}
