/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.portal;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author mibrahim
 */
@Entity
@ReadOnly
@NamedQueries(value = {
    @NamedQuery(name = "getPagePortlets",
            query = " SELECT    portalPagePortletLite "
            + " FROM      PortalPagePortletLite portalPagePortletLite"
            + " WHERE     portalPagePortletLite.portalPageDBID = :portalPageDBID"),
    @NamedQuery(name = "getToBeRenderedList",
            query = " SELECT    portalPagePortletLite "
            + " FROM      PortalPagePortletLite portalPagePortletLite"
            + " WHERE     portalPagePortletLite.parentPortletDBID = :parentPortletDBID")})
public class PortalPagePortletLite implements Serializable, Comparable {

    @Id
    private long pagePortletDBID;
    private long portalPageDBID;
    private boolean main;
    private int position;
    private String pcolumn;
    private String width;
    private String height;
    private long screenDBID;
    private long parentPortletDBID;
    private String screenName;
    @Transient
    private String screenInstId;
    private String screenViewPage;
    private String screenType;
    private String defaultStateUDCValue;

    public long getPagePortletDBID() {
        return pagePortletDBID;
    }

    public void setPagePortletDBID(long pagePortletDBID) {
        this.pagePortletDBID = pagePortletDBID;
    }

    public long getPortalPageDBID() {
        return portalPageDBID;
    }

    public void setPortalPageDBID(long portalPageDBID) {
        this.portalPageDBID = portalPageDBID;
    }

    public boolean isMain() {
        return main;
    }

    public void setMain(boolean main) {
        this.main = main;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getPcolumn() {
        return pcolumn;
    }

    public void setPcolumn(String pcolumn) {
        this.pcolumn = pcolumn;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    /**
     * @return the parentPortletDBID
     */
    public long getParentPortletDBID() {
        return parentPortletDBID;
    }

    /**
     * @param parentPortletDBID the parentPortletDBID to set
     */
    public void setParentPortletDBID(long parentPortletDBID) {
        this.parentPortletDBID = parentPortletDBID;
    }

    /**
     * @return the screenName
     */
    public String getScreenName() {
        return screenName;
    }

    /**
     * @param screenName the screenName to set
     */
    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    /**
     * @return the screenInstId
     */
    public String getScreenInstId() {
        return screenInstId;
    }

    /**
     * @param screenInstId the screenInstId to set
     */
    public void setScreenInstId(String screenInstId) {
        this.screenInstId = screenInstId;
    }

    /**
     * @return the screenViewPage
     */
    public String getScreenViewPage() {
        return screenViewPage;
    }

    /**
     * @param screenViewPage the screenViewPage to set
     */
    public void setScreenViewPage(String screenViewPage) {
        this.screenViewPage = screenViewPage;
    }

    /**
     * @return the screenType
     */
    public String getScreenType() {
        return screenType;
    }

    /**
     * @param screenType the screenType to set
     */
    public void setScreenType(String screenType) {
        this.screenType = screenType;
    }

    /**
     * @return the defaultStateUDCValue
     */
    public String getDefaultStateUDCValue() {
        return defaultStateUDCValue;
    }

    /**
     * @param defaultStateUDCValue the defaultStateUDCValue to set
     */
    public void setDefaultStateUDCValue(String defaultStateUDCValue) {
        this.defaultStateUDCValue = defaultStateUDCValue;
    }

    /**
     * @return the screenDBID
     */
    public long getScreenDBID() {
        return screenDBID;
    }

    /**
     * @param screenDBID the screenDBID to set
     */
    public void setScreenDBID(long screenDBID) {
        this.screenDBID = screenDBID;
    }

    public int compareTo(Object p) {
        return Double.compare(this.position, ((PortalPagePortletLite) p).position);

    }
}
