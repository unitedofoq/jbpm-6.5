/*
 */
package com.unitedofoq.fabs.core.log;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.service.LogManager;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.timezone.TimeZoneServiceLocal;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.validation.FABSException;

/**
 * Class is used to log messages to {@link java.util.logging.Logger}
 *
 * @see http://wiki.eclipse.org/EclipseLink/Examples/JPA/Logging
 */
public class OLog {

    /**
     * Format 'args' as following: <br> * <element>.toString() <br>
     *
     * <object>.toString()
     *
     * @param args Is an array of the following pattern: <br> Element : is
     * string, value = Label of "Output Information" <br> Next Element: is
     * Object, value = "Output Information", <br> will use toString() to get its
     * information
     */
    @EJB
    static OLogSetUpServiceRemote oLogService;
    private static final LogManager myLogger;

    static {
        myLogger = LogManager.getInstance();
    }
    @Deprecated
    public static OLogSetUpServiceRemote getOLogService(OUser loggedUser) {
        if (oLogService == null) {
            try {
                InitialContext ctx = new InitialContext();
                oLogService = (OLogSetUpServiceRemote) ctx.lookup(
                        "java:global/ofoq/com.unitedofoq.fabs.core.log.OLogSetUpServiceRemote");
            } catch (NamingException ex) {
                ex.printStackTrace();
            }
        }
        return oLogService;
    }
    @EJB
    static TimeZoneServiceLocal timeZoneService;
    @Deprecated
    public static TimeZoneServiceLocal getTimeZoneService() {
        if (timeZoneService == null) {
            try {
                InitialContext ctx = new InitialContext();
                timeZoneService = (TimeZoneServiceLocal) ctx.lookup(
                        "java:global/ofoq/com.unitedofoq.fabs.core.timezone.TimeZoneServiceLocal");
            } catch (NamingException ex) {
                ex.printStackTrace();
            }
        }
        return timeZoneService;
    }
    
    private static StringBuilder formatArgs(Object... args) {
        StringBuilder logMsg = new StringBuilder();
        for (int i = 0; i < args.length; i++) {
            if ((i % 2) == 0) {
                /* Odd elements are Objects Values, use toString */
                logMsg = logMsg.append("\n\t* ").append(args[i].toString());
            } else {
                /* Even elements are labels*/
                logMsg = logMsg.append(" :<").append((args[i] == null ? "null" : args[i].toString())).append(">");
            }
        }
        return logMsg;
    }

    /**
     * Logs full information about the passed exception using System.out.println
     * Information includes calling function line information, full exception
     * information, date time, and arguments
     *
     * @param functionName Name for function from which logException is called
     * @param ex Exception for which the information will be displayed
     * @param loggedUser Logged User
     * @param args Passed to {@link #formatArgs(java.lang.Object[])}
     */
    @Deprecated
    public static void logException(Exception ex, OUser loggedUser, Object... args) {
        StringBuilder logMsg = new StringBuilder();
//        LogManager myLogger = LogManager.getInstance();
        try {
            logMsg = logMsg.append("\n>\n*** ").append(getCurDateTime()).append(" Error, Exception");
            if (ex instanceof FABSException) {
                logMsg = logMsg.append(((FABSException) ex).constructLogMessage());
            } else {
                Throwable rootCause = ex.getCause();
                if (rootCause != null) {
                    while (rootCause.getCause() != null) {
                        rootCause = rootCause.getCause();
                    }
                }
                logMsg = logMsg.append(" Type: ").append(ex.getClass().getName())
                        .append(ex.getMessage() == null ? ""
                        : "\n\t* Exception Message : " + ex.getMessage())
                        .append(ex.getCause() == null ? ""
                        : "\n\t* Exception Cause   : " + ex.getCause())
                        .append(ex.getCause() == null ? ""
                        : "\n\t* Except. RootCause : " + rootCause)
                        .append(ex.toString() == null ? ""
                        : "\n\t* Exception String  : " + ex.toString());
                logMsg = logMsg.append(getLoggedUserLog(loggedUser));
            }
            logMsg = logMsg.append(getLogStackTrace(loggedUser));
            logMsg = logMsg.append(formatArgs(args));

            myLogger.logError(logMsg);

            if (ex instanceof FABSException) {
                FABSException fex = (FABSException) ex;
                if (fex.getOriginatingJavaException() != null) // Log stack of original exception not the FABS
                {
                    ex = fex.getOriginatingJavaException();
                }
            }
            PrintStream s = System.err;
            synchronized (s) {
                StackTraceElement[] trace = ex.getStackTrace();
                for (int i = 0; i < 5; i++) {
                    //s.println("\tat " + trace[i]);
                    logMsg = new StringBuilder("\tat " + trace[i]);
                    myLogger.logError(logMsg);
//                    System.out.println(logMsg);
                }
            }
            logMsg = null;
        } catch (Exception internalEx) {
            logMsg = new StringBuilder("*** Internal Exception in logException: "
                    + internalEx.getMessage());
            myLogger.logError(logMsg);
            logMsg = null;
        }
    }
    @Deprecated
    public static void logException(Exception ex, Object... args) {
        StringBuilder logMsg = new StringBuilder();
        try {
            logMsg = logMsg.append("\n>\n*** ").append(getCurDateTime()).append(" Error, Exception");
            if (ex instanceof FABSException) {
                logMsg = logMsg.append(((FABSException) ex).constructLogMessage());
            } else {
                Throwable rootCause = ex.getCause();
                if (rootCause != null) {
                    while (rootCause.getCause()!= null) {
                        rootCause = rootCause.getCause();
                    }
                }
                logMsg = logMsg.append(" Type: ").append(ex.getClass().getName())
                        .append(ex.getMessage() == null ? ""
                        : "\n\t* Exception Message : " + ex.getMessage())
                        .append(ex.getCause() == null ? ""
                        : "\n\t* Exception Cause   : " + ex.getCause())
                        .append(ex.getCause() == null ? ""
                        : "\n\t* Except. RootCause : " + rootCause)
                        .append(ex.toString() == null ? ""
                        : "\n\t* Exception String  : " + ex.toString());
            }
            logMsg = logMsg.append(formatArgs(args));
            myLogger.logError(logMsg);

            if (ex instanceof FABSException) {
                FABSException fex = (FABSException) ex;
                if (fex.getOriginatingJavaException() != null) // Log stack of original exception not the FABS
                {
                    ex = fex.getOriginatingJavaException();
                }
            }
            PrintStream s = System.err;
            synchronized (s) {
                StackTraceElement[] trace = ex.getStackTrace();
                for (int i = 0; i < 5; i++) {
                    //s.println("\tat " + trace[i]);
                    logMsg = new StringBuilder("\tat " + trace[i]);
                    myLogger.logError(logMsg);
                }
            }
            logMsg = null;
        } catch (Exception internalEx) {
            logMsg = new StringBuilder("*** Internal Exception in logException: "
                    + internalEx.getMessage());
            myLogger.logError(logMsg);
            logMsg = null;
        }
    }
    @Deprecated
    public static void logLoadEntitiesInformation(String message, OUser loggedUser, Object... args) {
        OFunctionResult loadOFR = getOLogService(loggedUser).loadLoadEntityOption(loggedUser);
        if (loadOFR != null && loadOFR.getReturnValues() != null && !loadOFR.getReturnValues().isEmpty()) {
            boolean loadOption = Boolean.valueOf(String.valueOf(loadOFR.getReturnValues().get(0)));
            if (loadOption) {
                OFunctionResult oFR = getOLogService(loggedUser).loadLogInfoLevel(loggedUser);
                Level fabsSetupLogLevel = null;
                if (oFR != null && oFR.getReturnValues() != null && !oFR.getReturnValues().isEmpty()) {
                    fabsSetupLogLevel = (Level) oFR.getReturnValues().get(0);
                }
                Logger logger = getServerLogger(fabsSetupLogLevel);
                if (logger == null) {
                    return;
                }
                myLogger.logError(constructLogInformation(message, loggedUser, args));
            }
        }
    }

    /**
     * Calls
     * {@link #logInformation(java.lang.String, com.unitedofoq.fabs.core.security.user.OUser, Level, java.lang.Object[])}
     * with Level.INFO
     */
    @Deprecated
    public static void logInformation(String message, OUser loggedUser, Object... args) {
        myLogger.logInfo(constructLogInformation(message, loggedUser, args));
    }
    @Deprecated
    public static void logInformation(String message, Object... args) {
        myLogger.logInfo(constructLogInformation(message, args));
    }

    /**
     * This Function is used to Replace Normal System.out.println()
     * {@link #logInformation(java.lang.String)} with Level.INFO
     * @param message
     */
    @Deprecated
    public static void logInformation(String message) {
        myLogger.logInfo(new StringBuilder(message));
    }

    /**
     * Logs full information about the passed message using
     * {@link java.util.logging.Logger}.
     *
     * <br> Information includes calling function code-line information,
     * date-time, and arguments <br> Logs only if the log
     * {@link java.util.logging.Level} <= logLevel <br> Logger used is of
     * OLog.class
     *
     * @param message Message to be displayed
     * @param loggedUser Logged User
     * @param logLevel Log {@link java.util.logging.Level} of the message to be
     * displayed
     * @param args Passed to {@link #formatArgs(java.lang.Object[])}
     */
    @Deprecated
    public static void logInformation(
            String message, OUser loggedUser, Level logLevel, Object... args) {
        Logger logger = getLogger(logLevel, loggedUser);
        if (logger == null) {
            return;
        }
        logger = getServerLogger(logLevel);
        if (logger == null) {
            return;
        }
        myLogger.logInfo(constructLogInformation(message, loggedUser, args));
    }
    
    private static StringBuilder constructLogInformation(
            String message, OUser loggedUser, Object... args) {
        StringBuilder logMsg = new StringBuilder("\n>\n... " + getCurDateTime() + " Information: "
                + (message == null ? "" : message));

        logMsg = logMsg.append(getLoggedUserLog(loggedUser));
        logMsg = logMsg.append(getLogStackTrace(loggedUser));
        logMsg = logMsg.append(formatArgs(args));
        return logMsg;
    }
    
    private static StringBuilder constructLogInformation(
            String message, Object... args) {
        StringBuilder logMsg = new StringBuilder("\n>\n... " + getCurDateTime() + " Information: "
                + (message == null ? "" : message));
        logMsg = logMsg.append(formatArgs(args));
        return logMsg;
    }
    /**
     * Calls
     * {@link #logStatisticsInformation(java.lang.String, java.util.logging.Level, java.lang.Object[])}
     * with Level.FINER
     */
    @Deprecated
    public static void logStatisticsInformation(String message, OUser loggedUser, Object... args) {
        logStatisticsInformation(message, loggedUser, Level.FINER, args);
    }

    /**
     * Logs (formatted) one line comma separated information using
     * {@link java.util.logging.Logger} to be used in statistics later on. <br>
     * Logs only if the log {@link java.util.logging.Level} is 'FINER' <br>
     * Logger used is of OLog.class
     *
     * @param message Message to be displayed
     * @param args Passed to {@link #formatArgs(java.lang.Object[])}
     */
    @Deprecated
    public static void logStatisticsInformation(String message, OUser loggedUser, Level logLevel, Object... args) {
//        LogManager myLogger = LogManager.getInstance();
        Logger logger = getLogger(logLevel, loggedUser);
        if (logger == null) {
            return;
        }
        logger = getServerLogger(logLevel);
        if (logger == null) {
            return;
        }

        StringBuilder logMsg;
        logMsg = new StringBuilder("\n... " + getCurDateTime() + ", Statistics, ");
        logMsg = logMsg.append(message == null ? "" : message);
        for (int i = 0; i < args.length; i++) {
            if ((i % 2) == 0) {
                /* Odd elements are Objects Values, use toString */
                if (args[i] == null) {
                    OLog.logInformation("*** Error in logStatisticsInformation, null argument");
                    OLog.logInformation(logMsg.toString());
//                    System.out.println("*** Error in logStatisticsInformation, null argument");
//                    System.out.println(logMsg);
                }
                logMsg = logMsg.append(", " + args[i].toString());
            } else {
                /* Even elements are labels*/
                logMsg = logMsg.append(", " + (args[i] == null ? "null" : args[i].toString()) + "");
            }
        }
        myLogger.logInfo(logMsg);
        logMsg = null;
//        logger.log(logLevel, logMsg);
    }

    /**
     * Calls
     * {@link #logStatisticsInformation(java.lang.String, java.util.logging.Level, java.lang.Object[])}
     * with Level.FINER
     */
    @Deprecated
    public static void logStatisticsInformation(String message, Object... args) {
        logStatisticsInformation(message, Level.FINER, args);
    }

    /**
     * Logs (formatted) one line comma separated information using
     * {@link java.util.logging.Logger} to be used in statistics later on. <br>
     * Logs only if the log {@link java.util.logging.Level} is 'FINER' <br>
     * Logger used is of OLog.class
     *
     * @param message Message to be displayed
     * @param args Passed to {@link #formatArgs(java.lang.Object[])}
     */
    @Deprecated
    public static void logStatisticsInformation(String message, Level logLevel, Object... args) {
//        LogManager myLogger = LogManager.getInstance();
        Logger logger = getServerLogger(logLevel);
        if (logger == null) {
            return;
        }

        StringBuilder logMsg;
        logMsg = new StringBuilder("\n... " + getCurDateTime() + ", Statistics, ");
        logMsg = logMsg.append(message == null ? "" : message);
        for (int i = 0; i < args.length; i++) {
            if ((i % 2) == 0) {
                /* Odd elements are Objects Values, use toString */
                if (args[i] == null) {
                    OLog.logInformation("*** Error in logStatisticsInformation, null argument");
                    OLog.logInformation(logMsg.toString());
//                    System.out.println("*** Error in logStatisticsInformation, null argument");
//                    System.out.println(logMsg);
                }
                logMsg = logMsg.append(", " + args[i].toString());
            } else {
                /* Even elements are labels*/
                logMsg = logMsg.append(", " + (args[i] == null ? "null" : args[i].toString()) + "");
            }
        }
        myLogger.logInfo(logMsg);
        logMsg = null;
//        logger.log(logLevel, logMsg);
    }

    /**
     * Calls
     * {@link #logError(java.lang.String, com.unitedofoq.fabs.core.security.user.OUser, java.util.logging.Level, java.lang.Object[])}
     * with Level.SEVERE.
     */
    @Deprecated
    public static void logError(String message, OUser loggedUser, Object... args) {
        logError(message, loggedUser, Level.SEVERE, args);
    }

    /**
     * Logs (formatted) one line comma separated information using
     * {@link java.util.logging.Logger}. <br> Logs only if the log
     * {@link java.util.logging.Level} <= logLevel <br> Logger used is of
     * OLog.class
     *
     * @param message Message to be displayed
     * @param loggedUser Logged User
     * @param args Passed to {@link #formatArgs(java.lang.Object[])}
     */
    @Deprecated
    public static void logError(String message, OUser loggedUser, Level logLevel, Object... args) {
//        LogManager myLogger = LogManager.getInstance();
        Logger logger = getServerLogger(logLevel);
        if (logger == null) {
            return;
        }

        StringBuilder logMsg;
        logMsg = new StringBuilder("\n>\n*** " + getCurDateTime() + " Error: "
                + (message == null ? "" : message));
        logMsg = logMsg.append(getLoggedUserLog(loggedUser));
        logMsg = logMsg.append(getLogStackTrace(loggedUser));
        logMsg = logMsg.append(formatArgs(args));

        myLogger.logError(logMsg);
        logMsg = null;
//        logger.log(logLevel, logMsg);
    }
    
    private static StringBuilder getLogStackTrace(OUser loggedUser) {
        Thread currentThread = Thread.currentThread();
        StackTraceElement[] sts = currentThread.getStackTrace();
        int logCallerFuncIndex = getStackIndexOfLogCaller(sts);
        if (logCallerFuncIndex >= sts.length || logCallerFuncIndex == -1) {
            return new StringBuilder();
        }
        StackTraceElement ste = sts[logCallerFuncIndex];
        StringBuilder logMsg = new StringBuilder();
        if (loggedUser == null) {
            return new StringBuilder();
        }
        OFunctionResult oFR = getOLogService(null).loadLogLineOption(loggedUser);
        if (oFR.getReturnValues() != null && !oFR.getReturnValues().isEmpty()) {
            if ((String.valueOf(oFR.getReturnValues().get(0))).equals("true")) {
                logMsg = logMsg.append("\n\t* @ Line      : ").append(ste.getClassName())
                        .append(".").append(ste.getMethodName())
                        .append("(")
                        .append(ste.getFileName()).append(":")
                        .append(ste.getLineNumber())
                        .append(")");
                if ((logCallerFuncIndex + 1) >= sts.length) {
                    return logMsg;
                }
                ste = sts[logCallerFuncIndex + 1];
                logMsg = logMsg.append("\n\t* @ Caller    : ").append(ste.getClassName())
                        .append(".").append(ste.getMethodName())
                        .append("(")
                        .append(ste.getFileName()).append(":")
                        .append(ste.getLineNumber())
                        .append(")");
            }
        }
        return logMsg;
    }
    @Deprecated
    public static String getCurDateTime() {

        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");//dd/MM/yyyy
        String currDate = sdfDate.format(new Date());
        return currDate + ", " + Thread.currentThread().getId() + ", ";

//        return getTimeZoneService().getSystemCDTAsString() + ", " + Thread.currentThread().getId() + ", ";
//        Calendar cal = Calendar.getInstance();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
//        return sdf.format(cal.getTime()) + ", " + Thread.currentThread().getId() + ", ";
    }

    /**
     * Calls {@link #logQueryStatistics(java.lang.String, java.lang.Long, java.util.logging.Level, java.lang.Object)
     * }
     * with Level.FINER
     */
    @Deprecated
    public static void logQueryStatistics(String query, OUser loggedUser, Long timeBeforeQuery, Object result) {
        logQueryStatistics(query, loggedUser, timeBeforeQuery, Level.FINER, result);
    }

    /**
     * Logs (formatted) one line comma separated query information using
     * {@link java.util.logging.Logger} to be used in statistics later on. <br>
     * Logs only if the log {@link java.util.logging.Level} <= logLevel <br>
     * Logger used is of OLog.class
     *
     * @param query Query to be logged
     * @param timeBeforeQuery Timer before calling query
     * @param result Result of the query
     */
    @Deprecated
    public static void logQueryStatistics(String query, OUser loggedUser, Long timeBeforeQuery, Level logLevel, Object result) {
//        LogManager myLogger = LogManager.getInstance();
        Logger logger = getLoggerForQueryStatistics(logLevel, loggedUser);
        if (logger == null) {
            return;
        }
        logger = getServerLogger(logLevel);
        if (logger == null) {
            return;
        }
        StringBuilder logMsg = new StringBuilder();
        logMsg = logMsg.append("\n>\n... ").append(getCurDateTime()).append(", Statistics, ")
                .append("Query, ").append(query == null ? "" : query)
                .append(", ").append("Duration, ").append((Calendar.getInstance()).getTimeInMillis() - timeBeforeQuery)
                .append(", Result, ").append(result == null ? "" : result);
        myLogger.logInfo(logMsg);
        logMsg = null;
//        logger.log(logLevel, logMsg);
    }
    
    private static String getCallingFunctionName() {
        // Get function name
        Thread currentThread = Thread.currentThread();
        StackTraceElement[] sts = currentThread.getStackTrace();
        int logCallerFuncIndex = getStackIndexOfLogCaller(sts);
        if (logCallerFuncIndex >= sts.length || logCallerFuncIndex == -1) {
            return "";
        }
        return sts[logCallerFuncIndex].getMethodName();
    }

    /**
     * Calls
     * {@link #logFunctionStatistics(java.lang.Long, java.util.logging.Level, java.lang.Object[])}
     * with Level.FINER
     */
    @Deprecated
    public static void logFunctionStatistics(Long entryTime, OUser loggedUser, Object... args) {
        logFunctionStatistics(entryTime, loggedUser, Level.FINER, args);
    }

    /**
     * Logs (formatted) one line comma separated function execution information
     * using {@link java.util.logging.Logger} to be used in statistics later on.
     * <br> Logs only if the log {@link java.util.logging.Level} <= logLevel
     * <br> Logger used is of OLog.class
     *
     * @param entryTime Entry time of the function
     * @param args Passed to {@link #formatArgs(java.lang.Object[])}
     */
    @Deprecated
    public static void logFunctionStatistics(Long entryTime, OUser loggedUser, Level logLevel, Object... args) {
//        LogManager myLogger = LogManager.getInstance();
        Logger logger = getLogger(logLevel, loggedUser);
        if (logger == null) {
            return;
        }
        logger = getServerLogger(logLevel);
        if (logger == null) {
            return;
        }

        StringBuilder logMsg = new StringBuilder();
        logMsg.append("\n>\n... ").append(getCurDateTime()).append(", Statistics, Function, ")
                .append(getCallingFunctionName())
                .append(", Duration, ").append((Calendar.getInstance()).getTimeInMillis() - entryTime);
        for (int i = 0; i < args.length; i++) {
            if ((i % 2) == 0) {
                /* Odd elements are Objects Values, use toString */
                if (args[i] == null) {
                    OLog.logInformation("*** Error in logStatisticsInformation, null argument");
                    OLog.logInformation(logMsg.toString());
//                    System.out.println("*** Error in logStatisticsInformation, null argument");
//                    System.out.println(logMsg);
                }
                logMsg = logMsg.append(", ").append(args[i].toString());
            } else {
                /* Even elements are labels*/
                logMsg = logMsg.append(", ").append(args[i] == null ? "null" : args[i].toString()).append("");
            }
        }
        myLogger.logInfo(logMsg);
        logMsg = null;
//        logger.log(logLevel, logMsg);
    }

    /**
     * Calls
     * {@link #logFunctionStatistics(java.lang.Long, java.util.logging.Level, java.lang.Object[])}
     * with Level.FINER
     */
    @Deprecated
    public static void logFunctionStatistics(Long entryTime, Object... args) {
        logFunctionStatistics(entryTime, Level.FINER, args);
    }

    /**
     * Logs (formatted) one line comma separated function execution information
     * using {@link java.util.logging.Logger} to be used in statistics later on.
     * <br> Logs only if the log {@link java.util.logging.Level} <= logLevel
     * <br> Logger used is of OLog.class
     *
     * @param entryTime Entry time of the function
     * @param args Passed to {@link #formatArgs(java.lang.Object[])}
     */
    @Deprecated
    public static void logFunctionStatistics(Long entryTime, Level logLevel, Object... args) {
//        LogManager myLogger = LogManager.getInstance();
        Logger logger = getServerLogger(logLevel);
        if (logger == null) {
            return;
        }

        StringBuilder logMsg = new StringBuilder();
        logMsg = logMsg.append("\n>\n... ").append(getCurDateTime()).append(", Statistics, Function, ")
                .append(getCallingFunctionName())
                .append(", Duration, ").append((Calendar.getInstance()).getTimeInMillis() - entryTime);
        for (int i = 0; i < args.length; i++) {
            if ((i % 2) == 0) {
                /* Odd elements are Objects Values, use toString */
                if (args[i] == null) {
                    OLog.logInformation("*** Error in logStatisticsInformation, null argument");
                    OLog.logInformation(logMsg.toString());
//                    System.out.println("*** Error in logStatisticsInformation, null argument");
//                    System.out.println(logMsg);
                }
                logMsg = logMsg.append(", ").append(args[i].toString());
            } else {
                /* Even elements are labels*/
                logMsg = logMsg.append(", ").append(args[i] == null ? "null" : args[i].toString()).append("");
            }
        }
        myLogger.logInfo(logMsg);
        logMsg = null;
//        logger.log(logLevel, logMsg);
    }

    /**
     * Retrurns the logger of OLog.class.getName() if logger level >= logLevel
     * passed, else returns null
     */
    @Deprecated
    public static Logger getLogger(Level logLevel, OUser loggedUser) {
        Logger logger = Logger.getLogger(OLog.class.getName());
        //get system log info level
        OFunctionResult oFR = getOLogService(loggedUser).loadLogInfoLevel(loggedUser);
        Level fabsSetupLogLevel = null;
        if (oFR != null && oFR.getReturnValues() != null && !oFR.getReturnValues().isEmpty()) {
            fabsSetupLogLevel = (Level) oFR.getReturnValues().get(0);
            // Cheat check
            Level loggerLevel = logger.getLevel();
            if (loggerLevel == null) {
                loggerLevel = logger.getParent().getLevel();
            }
            //Uncomment & set to required log level
            logger.setLevel(Level.FINEST);
            if (fabsSetupLogLevel.intValue() > logLevel.intValue()) // Logger level is more than logLevel
            // Don't log
            {
                return null;
            } else // Logger level is less than or equals logLevel
            // OK to log
            {
                return logger;
            }
        }
        return null;
    }

    /**
     * Retrurns the logger of OLog.class.getName() if logger level >= logLevel
     * passed, else returns null
     */
    @Deprecated
    public static Logger getLoggerForQueryStatistics(Level logLevel, OUser loggedUser) {
        Logger logger = Logger.getLogger(OLog.class.getName());
        //get system log info level
        OFunctionResult oFR = getOLogService(loggedUser).loadLogQueryStatisticsLevel(loggedUser);
        Level fabsSetupLogLevel = null;
        if (oFR != null && oFR.getReturnValues() != null && !oFR.getReturnValues().isEmpty()) {
            fabsSetupLogLevel = (Level) oFR.getReturnValues().get(0);
            // Cheat check
            Level loggerLevel = logger.getLevel();
            if (loggerLevel == null) {
                loggerLevel = logger.getParent().getLevel();
            }
            //Uncomment & set to required log level
            logger.setLevel(Level.FINEST);
            if (fabsSetupLogLevel.intValue() > logLevel.intValue()) // Logger level is more than logLevel
            // Don't log
            {
                return null;
            } else // Logger level is less than or equals logLevel
            // OK to log
            {
                return logger;
            }
        }
        return null;
    }
    @Deprecated
    public static Logger getServerLogger(Level logLevel) {
        Logger logger = Logger.getLogger(OLog.class.getName());
        // Cheat check
        Level loggerLevel = logger.getLevel();
        if (loggerLevel == null) {
            loggerLevel = logger.getParent().getLevel();
        }
        //Uncomment & set to required log level
        logger.setLevel(Level.FINEST);
        if (loggerLevel == null || logLevel == null || loggerLevel.intValue() > logLevel.intValue()) // Logger level is more than logLevel
        // Don't log
        {
            return null;
        } else // Logger level is less than or equals logLevel
        // OK to log
        {
            return logger;
        }
    }

    private static int getStackIndexOfLogCaller(StackTraceElement[] sts) {
        for (int iTrace = 0; iTrace < sts.length; iTrace++) {
            StackTraceElement ste = sts[iTrace];
            if (ste.getFileName().equals("OLog.java")) {
                // Execlude all calls from OLog.java, start right after them
                for (; iTrace < sts.length; iTrace++) {
                    ste = sts[iTrace];
                    if (!ste.getFileName().equals("OLog.java")
                            && !ste.getFileName().equals("FABSException.java")) // Function is not in OLog nor in FABSException
                    {
                        break;
                    }
                }
                return iTrace;
            }
        }
        return -1;
    }

    private static StringBuilder getLoggedUserLog(OUser loggedUser) {
        StringBuilder logMsg = new StringBuilder();
        if (loggedUser == null) {
            return new StringBuilder();
        }
        if (loggedUser.getDbid() == 0) {
            logMsg = logMsg.append(
                    "\n\t* Logged User : SYSTEM ");
        } else {
            logMsg = logMsg.append("\n\t* Logged User : ").append(loggedUser.getDbid())
                    .append(", ").append(loggedUser.getLoginName());
        }
        OUser onBehalfOf = loggedUser.getOnBehalfOf();
        if (onBehalfOf != null) {
            logMsg = logMsg.append("(On Behalf Of: ").append(onBehalfOf.getDbid())
                    .append(", ").append(onBehalfOf.getLoginName()).append(")");
        }
        return logMsg;
    }
    @Deprecated
    public static void logFABSTrace(Level logLevel, OUser loggedUser) {
//        LogManager myLogger = LogManager.getInstance();
        Logger logger = getLogger(logLevel, loggedUser);
        if (logger == null) {
            return;
        }
        logger = getServerLogger(logLevel);
        if (logger == null) {
            return;
        }

        StringBuilder logMsg = new StringBuilder();
        logMsg = logMsg.append("\n>\n... ").append(getCurDateTime()).append(" FABS Trace: ");

        StackTraceElement[] sts = Thread.currentThread().getStackTrace();
        int stsSize = sts.length;
        for (int stsIndex = 0; stsIndex < stsSize; stsIndex++) {
            StackTraceElement ste = sts[stsIndex];
            if (!ste.getClassName().contains("unitedofoq")) {
                continue;
            }
            logMsg = logMsg.append("\n\t* [").append(stsIndex).append("], ")
                    .append(ste.getClassName()).append(".")
                    .append(ste.getMethodName()).append("(").append(ste.getFileName())
                    .append(":").append(ste.getLineNumber()).append(")");
        }
//        logger.log(logLevel, logMsg);
        myLogger.logInfo(logMsg);
        logMsg = null;
    }
    @Deprecated
    public static StringBuilder getFunctionCaller(String functionStartWith) {
        StackTraceElement[] sts = Thread.currentThread().getStackTrace();
        int stsSize = sts.length;
        for (int stsIndex = 0; stsIndex < stsSize; stsIndex++) {
            StackTraceElement ste = sts[stsIndex];
            if (!ste.getMethodName().startsWith(functionStartWith)) {
                return new StringBuilder(ste.getClassName()).append(".")
                        .append(ste.getMethodName())
                        .append("(").append(ste.getFileName()).append(")");
            }
        }
        return new StringBuilder();
    }
    @Deprecated
    public static void logOFR(ArrayList<UserMessage> messagesList, OUser loggedUser) {
//        LogManager myLogger = LogManager.getInstance();
        ArrayList<UserMessage> successes = new ArrayList<UserMessage>();
        ArrayList<UserMessage> warnings = new ArrayList<UserMessage>();
        ArrayList<UserMessage> errors = new ArrayList<UserMessage>();
        StringBuilder logMsg = new StringBuilder();
        if (messagesList.size() > 0) {
            for (UserMessage userMessage : messagesList) {
                if (userMessage != null && userMessage.getMessageType() != null) {
                    switch (userMessage.getMessageType()) {
                        case UserMessage.TYPE_SUCCESS:
                            successes.add(userMessage);
                            continue;
                        case UserMessage.TYPE_WARNING:
                            warnings.add(userMessage);
                            continue;
                        case UserMessage.TYPE_ERROR:
                            errors.add(userMessage);
                            continue;
                    }
                }
            }
            // Success
            if (successes.size() > 0) {
                Iterator<UserMessage> it = successes.iterator();
                while (it.hasNext()) {
                    UserMessage msg = it.next();
                    logMsg = new StringBuilder("Success Message: ")
                            .append(msg.getMessageText()).append("user:").append(loggedUser.toString())
                            .append("DBID :").append(msg.getDbid())
                            .append("Name").append(msg.getName())
                            .append("Title").append(msg.getMessageTitle())
                            .append("Text").append(msg.getMessageText());
                    myLogger.logInfo(logMsg);
                    logMsg = null;
//                    OLog.logInformation("Success Message: " + msg.getMessageText(), loggedUser,
//                            "DBID", msg.getDbid(),
//                            "Name", msg.getName(),
//                            "Title", msg.getMessageTitle(),
//                            "Text", msg.getMessageText());
                }
            }

            // Warning
            if (warnings.size() > 0) {
                Iterator<UserMessage> it = warnings.iterator();
                while (it.hasNext()) {
                    UserMessage msg = it.next();
                    logMsg = new StringBuilder("Warning Message: ")
                            .append(msg.getMessageText()).append("user:").append(loggedUser.toString())
                            .append("DBID :").append(msg.getDbid())
                            .append("Name").append(msg.getName())
                            .append("Title").append(msg.getMessageTitle())
                            .append("Text").append(msg.getMessageText());
                    myLogger.logWarn(logMsg);
                    logMsg = null;
//                    OLog.logInformation("Warning Message: " + msg.getMessageText(), loggedUser,
//                            "DBID", msg.getDbid(),
//                            "Name", msg.getName(),
//                            "Title", msg.getMessageTitle(),
//                            "Text", msg.getMessageText());
                }
            }

            // Error
            if (errors.size() > 0) {
                Iterator<UserMessage> it = errors.iterator();
                while (it.hasNext()) {
                    UserMessage msg = it.next();
                    logMsg = new StringBuilder("Error Message: ")
                            .append(msg.getMessageText()).append("user:").append(loggedUser.toString())
                            .append("DBID :").append(msg.getDbid())
                            .append("Name").append(msg.getName())
                            .append("Title").append(msg.getMessageTitle())
                            .append("Text").append(msg.getMessageText());
                    myLogger.logError(logMsg);
                    logMsg = null;
                    //                    OLog.logInformation("Error Message: " + msg.getMessageText(), loggedUser,
//                            "DBID", msg.getDbid(),
//                            "Name", msg.getName(),
//                            "Title", msg.getMessageTitle(),
//                            "Text", msg.getMessageText());
                }
            }
        }
    }
}
