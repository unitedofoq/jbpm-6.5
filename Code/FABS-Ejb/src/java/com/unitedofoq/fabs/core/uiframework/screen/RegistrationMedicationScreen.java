/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;

/**
 *
 * @author bassem
 */
@Entity
@DiscriminatorValue(value = "RM")
@ChildEntity(fields={"screenInputs", "screenOutputs","screenFilter","screenFunctions"})
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class RegistrationMedicationScreen extends TabularScreen {
}
