/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.report.customReports;

import java.io.ByteArrayOutputStream;
import org.eclipse.birt.report.engine.api.EngineException;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author bgalal
 */
public abstract class AbstractReportGenerator {

    private final static Logger LOGGER = LoggerFactory.getLogger(AbstractReportGenerator.class);

    protected ReportDTO reportDTO;

    private CustomReportServiceLocal customReportService;

    public AbstractReportGenerator(ReportDTO reportDTO) {
        this.reportDTO = reportDTO;
    }

    public ReportDTO getReportDTO() {
        return reportDTO;
    }

    public void setCustomReportService(CustomReportServiceLocal customReportService) {
        this.customReportService = customReportService;
    }

    public CustomReportServiceLocal getCustomReportService() {
        return customReportService;
    }

    public abstract ByteArrayOutputStream generateReport(CustomReportBuilder customReportBuilder);

    protected IRunAndRenderTask prepareReportDesignHandle(CustomReportBuilder customReportBuilder) {
        LOGGER.trace("Entering");

        customReportBuilder.buildReportOrientation(reportDTO.isLocalized(), reportDTO.isRevertLang())
                .buildReportQuery(reportDTO.getWhereConditions(), reportDTO.getReportName(), reportDTO.getScreenDataLoadingUEDT());

        //executing the user exit function
        if (null != reportDTO.getUserExit()) {
            customReportService.reportUserExitJavaFunction(reportDTO);
        }

        customReportBuilder.buildDataSetHandle()
                .buildEncryptionKeyInParametersMap()
                .buildParametersValue();

        LOGGER.trace("Returning");
        return customReportBuilder.getRunAndRenderTask();
    }

    protected void runReport(IRunAndRenderTask task) {
        try {
            //run report
            task.run();
        } catch (EngineException ex) {
            LOGGER.error("Exception thrown: ", ex);
        }
        task.close();
    }
}
