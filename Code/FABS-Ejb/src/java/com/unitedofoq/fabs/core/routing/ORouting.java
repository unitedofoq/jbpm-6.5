/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.routing;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;

/**
 *
 * @author lap3
 */
@Entity
@ChildEntity(fields = {"routingTasks", "routingIntances"})
public class ORouting extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="conditionalOp">
    private String conditionalOp;

    public String getConditionalOpDD() {
        return "ORouting_conditionalOp";
    }
    
    public String getConditionalOp() {
        return conditionalOp;
    }

    public void setConditionalOp(String conditionalOp) {
        this.conditionalOp = conditionalOp;
    }
    
    // <editor-fold defaultstate="collapsed" desc="conditionalFldExp">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OScreen launcherScreen;

    public String getLauncherScreenDD() {
        return "ORouting_launcherScreen";
    }
    
    public OScreen getLauncherScreen() {
        return launcherScreen;
    }

    public void setLauncherScreen(OScreen launcherScreen) {
        this.launcherScreen = launcherScreen;
    }
    
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="conditionalFldExp">   
    private String conditionalFldExp;
    public String getConditionalFldExpDD() {
        return "ORouting_conditionalFldExp";
    }

    public String getConditionalFldExp() {
        return conditionalFldExp;
    }

    public void setConditionalFldExp(String conditionalFldExp) {
        this.conditionalFldExp = conditionalFldExp;
    }
    
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="conditionValue">    
    private String conditionValue;

    public String getConditionValueDD() {
        return "ORouting_conditionValue";
    }
    
    public String getConditionValue() {
        return conditionValue;
    }

    public void setConditionValue(String conditionValue) {
        this.conditionValue = conditionValue;
    }
    
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="name">    
    @Column(unique = true)
    private String name;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getNameDD() {
        return "ORouting_name";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    private String description;

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    /**
     * @return the descriptionTranslated
     */
    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    /**
     * @param descriptionTranslated the descriptionTranslated to set
     */
    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getDescriptionTranslatedDD() {
        return "ORouting_description";
    }
    // </editor-fold>    
    
    // <editor-fold defaultstate="collapsed" desc="routingTasks">
    @OneToMany(mappedBy = "routing")
    protected List<ORoutingTask> routingTasks;

    /**
     * @return the routingTasks
     */
    public List<ORoutingTask> getRoutingTasks() {
        return routingTasks;
    }

    /**
     * @param routingTasks the routingTasks to set
     */
    public void setRoutingTasks(List<ORoutingTask> routingTasks) {
        this.routingTasks = routingTasks;
    }

    /**
     * @return the routingTasks
     */
    public String getRoutingTasksDD() {
        return "ORouting_routingTasks";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="routingInstances">
    @OneToMany(mappedBy = "routing")
    private List<ORoutingInstance> routingIntances;

    /**
     * @return the routingIntances
     */
    public List<ORoutingInstance> getRoutingIntances() {
        return routingIntances;
    }

    /**
     * @param routingIntances the routingIntances to set
     */
    public void setRoutingIntances(List<ORoutingInstance> routingIntances) {
        this.routingIntances = routingIntances;
    }
    /**
     * @return the routingIntances
     */
    public String getRoutingIntancesDD() {
        return "ORouting_routingIntances";
    }    
    // </editor-fold>

}
