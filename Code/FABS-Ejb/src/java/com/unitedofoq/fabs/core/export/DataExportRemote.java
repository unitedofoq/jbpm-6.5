/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.export;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.List;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author aelzaher
 */
@Local
public interface DataExportRemote {
    
    public String getUpdateCode(String tableName, List<Field> fields, List entities, OUser loggedUser);

    public String getCreateCode(String tableName, List<Field> fields, List entities, 
            List<String> discriminatorData, OUser loggedUser);

    public String getRemoveCode(String tableName, List<Field> fields, List entities);
    
    public String getUpdateCode(String tableName, List<Field> fields, Object entity, OUser loggedUser);

    public String getCreateCode(String tableName, List<Field> fields, Object entity, 
            List<String> discriminatorData, OUser loggedUser);

    public String getRemoveCode(String tableName, List<Field> fields, Object entity);
    
    public String getEntitySql(BaseEntity entity, OUser loggedUser);
    
    public String getEntitySql(List<BaseEntity> entity, OUser loggedUser);
    
    
    public String getSQLValue(Object object, OUser loggedUser);
    
    public String getFieldSQLValue(Object entity, List<Field> fields, OUser loggedUser);

    public Object getObjectFromSQLValue(Object value, Field field);
                
    public InputStream getEntitySqlAsInputStream(List<BaseEntity> entity, OUser loggedUser);
    public InputStream getEntitySqlAsInputStream(BaseEntity entity, OUser loggedUser);
    
  public OFunctionResult exportEntityData(ODataMessage odm, OFunctionParms params, OUser loggedUser);
}
