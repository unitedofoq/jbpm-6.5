/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;

/**
 *
 * @author arezk
 */
//Overview: Represent the form screen type. Contains the necessary information to build form screen.
//Discriminator "FormScreen"
@Entity
@DiscriminatorValue(value = "FORM")
@ChildEntity(fields={"screenInputs", "screenOutputs","screenFilter","screenFunctions",
    "screenControlsGroups", "screenActions", "screenFields"})
@FABSEntitySpecs(personalizableFields={"header","showActiveOnly","showHeader","columnsNo"})
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class FormScreen extends SingleEntityScreen {
    //No. of columns in the panel
    private int columnsNo;
    public String getColumnsNoDD() {
        return "FormScreen_columnsNo";
    }

    public int getColumnsNo() {
        return columnsNo;
    }

    public void setColumnsNo(int columns) {
        this.columnsNo = columns;
    }
     private boolean basicDetail;

    public boolean isBasicDetail() {
        return basicDetail;
    }

    public void setBasicDetail(boolean basicDetail) {
        this.basicDetail = basicDetail;
    }
    public String getBasicDetailDD()      {   return "FormScreen_basicDetail";  }
    //to be the screen that is popped up during outbox
    public boolean outbox;

    
    public String getOutboxDD(){
        return "FormScreen_outbox";
    
    }
    public boolean isOutbox() {
        return outbox;
    }

    public void setOutbox(boolean outbox) {
        this.outbox = outbox;
    }

}
