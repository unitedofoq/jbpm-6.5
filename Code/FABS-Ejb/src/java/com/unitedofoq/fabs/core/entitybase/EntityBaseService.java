/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitybase;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntityReference;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.timezone.TimeZoneServiceLocal;
import com.unitedofoq.fabs.core.uiframework.screen.FormScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenFieldGroup;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author melsayed
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.entitybase.EntityBaseServiceRemote",
        beanInterface = EntityBaseServiceRemote.class)
public class EntityBaseService implements EntityBaseServiceRemote {

    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private EntitySetupServiceRemote entitySetupService;
    @EJB
    private OFunctionServiceRemote functionService;
    @EJB
    private UserMessageServiceRemote usrMsgService;
    @EJB
    private TimeZoneServiceLocal timeZoneService;

    final static Logger logger = LoggerFactory.getLogger(EntityBaseService.class);

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void addEntityAuditTrail(BaseEntity entity,
            EntityAuditTrail.ActionType actionType, OUser actor) {
        logger.debug("Entering: addEntityAuditTrail");
        FABSEntitySpecs specs = BaseEntity.getFabsEntitySpecs(entity);
        if (specs != null) {
            if (!specs.isAuditTrailed()) {
                logger.debug("Returning from: addEntityAuditTrail");
                return;
            }
        }
        if (null == entity) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Null Argument for user: {}", actor);
            // </editor-fold>
        }
        try {
            oem.getEM(actor);
            EntityAuditTrail eat = new EntityAuditTrail();

            eat.setEntityDBID(entity.getDbid());
            eat.setEntityClsName(entity.getClass().getName());
            eat.setActionType(actionType);
            eat.setActionDTime(timeZoneService.getSystemCDT());

            if (!actor.equals(oem.getSystemUser(actor))) {
                eat.setActor(actor);
            }

            oem.getEM(actor).persist(eat);//saveEntity(eat, oem.getSystemUser(actor));
            oem.closeEM(actor);
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            if (actor != null && entity != null) {
                logger.debug("User: {} Entity: {}", actor.getLoginName(), entity.getClassName());
            }
            // </editor-fold>
            oem.closeEM(actor);
        }
    }

    /**
     * Duplication action JavaFunction for all entities except OPortalPage
     *
     * It calls {@link #duplicateEntity(BaseEntity entity, OUser loggedUser, boolean saveToDB)
     * }
     *
     * @param odm ODataMessage that contains the entity to be duplicated
     * @param funcParams Function Parameters
     * @param loggedUser Logged User
     * @return OFunctionResult with returnedValues has the duplicated entity &
     * user messages set
     */
    @Override
    public OFunctionResult duplicateEntityFunction(ODataMessage odm, OFunctionParms funcParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, BaseEntity.class);
        if (inputValidationErrors != null) {
            logger.debug("Returning");
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        BaseEntity entity = (BaseEntity) odm.getData().get(0);
        try {
            oFR = duplicateEntity(entity, loggedUser, true);
        } catch (Exception ex) {
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult duplicateFormScreenFunction(ODataMessage odm, OFunctionParms funcParms, OUser loggedUser) {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, BaseEntity.class);
        if (inputValidationErrors != null) {
            logger.debug("Returning");
            return inputValidationErrors;
        }
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        BaseEntity entity = (BaseEntity) odm.getData().get(0);
        try {
            oFR = duplicateFormScreen(entity, loggedUser, true);
        } catch (Exception ex) {
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    /**
     * Duplicate the passed entity 'entity'
     *
     * It calls {@link #duplicateEntityCallBackMethod(BaseEntity, BaseEntity, String, GetEntityChildrenTree)
     * }
     *
     * @param entity Entity required to be duplicated
     * @param loggedUser Logged User
     * @param saveToDB True, if it's needed to be saved in database; false if
     * just wanted to return it without saving
     * @return OFunctionResult with returnedValues has the duplicated entity &
     * user messages set
     */
    @Override
    public OFunctionResult duplicateEntity(
            BaseEntity entity, OUser loggedUser, boolean saveToDB) {
        logger.info("Entering");
        Class[] types = new Class[4];
        types[0] = BaseEntity.class;
        types[1] = BaseEntity.class;
        types[2] = String.class;
        types[3] = GetEntityChildrenTree.class;
        Method callBackMethod = null;
        OFunctionResult oFR = new OFunctionResult();
        try {
            callBackMethod = getClass().getDeclaredMethod("duplicateEntityCallBackMethod", types);

            // <editor-fold defaultstate="collapsed" desc="Log">
            if (entity != null && callBackMethod != null) {
                logger.info("Entity passed to duplicateEntity() Entity Name:{} Callback method:{}", entity.getClassName(), callBackMethod.getName());
            }
            // </editor-fold>

            GetEntityChildrenTree gect = new GetEntityChildrenTree(entity, callBackMethod, this, oem);
            gect.setLoggedUser(loggedUser);
            gect.methodSpecifcParm = saveToDB;
            gect.methodSpecifcParm1 = new ArrayList<BaseEntity>();//All new BaseEntitiese (from duplication)
            gect.getTree();

            ArrayList<BaseEntity> newEntitiesList = (ArrayList<BaseEntity>) gect.methodSpecifcParm1;
            for (BaseEntity duplicateEntity : newEntitiesList) {
                gect.functionResult.append(entitySetupService.
                        callEntityCreateAction(duplicateEntity, gect.getLoggedUser()));
                if (!gect.functionResult.getErrors().isEmpty()) {
                    oFR.append(gect.functionResult);
                }
            }

            // Set the duplicated parent in the function result returned values
            gect.functionResult.setReturnValues(
                    Collections.singletonList(newEntitiesList.get(0)));
//        em.flush();
            logger.info("Returning");
            return gect.functionResult;
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            try {
                oFR.append(functionService.constructOFunctionResultFromError("SystemInternalError", ex, loggedUser));
            } catch (Exception e) {
                logger.info("Returning");
                return oFR;
            }
            logger.info("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult duplicateFormScreen(
            BaseEntity entity, OUser loggedUser, boolean saveToDB) {
        logger.info("Entering");
        Class[] types = new Class[4];
        types[0] = BaseEntity.class;
        types[1] = BaseEntity.class;
        types[2] = String.class;
        types[3] = GetEntityChildrenTree.class;
        Method callBackMethod = null;
        OFunctionResult oFR = new OFunctionResult();
        try {
            callBackMethod = getClass().getDeclaredMethod("duplicateEntityCallBackMethod", types);

            // <editor-fold defaultstate="collapsed" desc="Log">
            if (entity != null && callBackMethod != null) {
                logger.warn("Entity passed to duplicateEntity(). Entity Name: {} Callback method: {}", entity.getClassName(), callBackMethod);
            }
            // </editor-fold>

            GetEntityChildrenTree gect = new GetEntityChildrenTree(entity, callBackMethod, this, oem);
            gect.setLoggedUser(loggedUser);
            gect.methodSpecifcParm = saveToDB;
            gect.methodSpecifcParm1 = new ArrayList<BaseEntity>();//All new BaseEntitiese (from duplication)
            gect.getTree();

            ArrayList<BaseEntity> newEntitiesList = (ArrayList<BaseEntity>) gect.methodSpecifcParm1;
            for (int i = 0; i < newEntitiesList.size(); i++) {
                BaseEntity newEntity = newEntitiesList.get(i);
                if (newEntity instanceof ScreenField) {
                    ScreenField screenField = (ScreenField) newEntity;
                    if (screenField.getScreenFieldGroup() != null) {
                        ScreenFieldGroup newOne = null;
                        for (int j = 0; j < newEntitiesList.size(); j++) {
                            BaseEntity again = newEntitiesList.get(j);
                            if (again instanceof ScreenFieldGroup) {
                                if (((ScreenFieldGroup) again).getHeader().equals(
                                        screenField.getScreenFieldGroup().getHeader())) {
                                    newOne = (ScreenFieldGroup) again;
                                }
                            }
                        }
                        screenField.setScreenFieldGroup(newOne);
                    }
                }
            }

            if (newEntitiesList.get(0) instanceof FormScreen) {
                ((FormScreen) newEntitiesList.get(0)).setBasicDetail(false);
            }

            for (BaseEntity duplicateEntity : newEntitiesList) {
                gect.functionResult.append(entitySetupService.
                        callEntityCreateAction(duplicateEntity, gect.getLoggedUser()));
                if (!gect.functionResult.getErrors().isEmpty()) {
                    oFR.append(gect.functionResult);
                }
            }

            // Set the duplicated parent in the function result returned values
            gect.functionResult.setReturnValues(
                    Collections.singletonList(newEntitiesList.get(0)));
//        em.flush();
            logger.info("Returning");
            return gect.functionResult;
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.info("Entity: {} Callback method: {}", entity, callBackMethod);
            // </editor-fold>
            try {
                oFR.append(functionService.constructOFunctionResultFromError("SystemInternalError", ex, loggedUser));
            } catch (Exception e) {
                logger.info("Returning");
                return oFR;
            }
            logger.info("Returning");
            return oFR;
        }
    }

    /**
     * Callback function called to duplicate the 'originalEntity' with all its
     * children tree.
     * <br>All duplicated entities to be saved into database are found in
     * gect.methodSpecifcParm1.
     *
     * If gect.methodSpecifcParm = true, meaning save to database the duplicated
     * entity along with its children, then it appends "_$" to all unique string
     * values; for unique non-string values, it generates error
     *
     * @see GetEntityChildrenTree#callBackMethod
     * @param entity Entity required to be duplicated
     * @param entityParent Parent of 'entity' if known.
     * @param entityFieldExpression Not used
     * @param gect gect.methodSpecifcParm (boolean): when true means need to
     * save the duplicated record in the "same" database , when false means
     * don't save the duplicated entity but just construct & return it.
     *
     * @return Duplicated entity with all duplicated children (if any) set
     * <br> null: Error, refer to gect.functionResult
     */
    public BaseEntity duplicateEntityCallBackMethod(
            BaseEntity originalEntity,
            BaseEntity entityParent,
            String entityFieldExpression,
            GetEntityChildrenTree gect) {
        logger.info("Entering");
        // The getChildren() in GetEntityChildrenTree invokes this code for each one of the children
        BaseEntity duplicatedEntity = null;
        try {
            Class entityClass = originalEntity.getClass();
            duplicatedEntity = (BaseEntity) Class.forName(entityClass.getName()).newInstance();
            List<String> childFieldNames = BaseEntity.getChildEntityFieldName(entityClass.getName());
            boolean addDuplicateToGECTList = true;

            // ___________________________
            // Get all fields to copy them
            //
            List<Field> entityFields = duplicatedEntity.getAllFields();
            for (Field field : entityFields) {
                /*
                 * Don't set (copy) children as they will be set automatically when
                 * this function is called for every one of them.
                 * If we set it now, then we are setting the child of the original
                 * entity and not the child will be created and duplicated
                 */
                if (childFieldNames.contains(field.getName())) // Field is child
                // Ignore it
                {
                    continue;
                }

                /**
                 * Ignore Transient Fields, should be either auto calculated by
                 * setting column fields; and any way not needed in save
                 */
                if (field.getAnnotation(Transient.class) != null) {
                    continue;
                }

                // Get the original field value
                Object value = null;
                try {
                    value = originalEntity.invokeGetter(field);
                } catch (NoSuchMethodException ex) {
                    // If no getter for the field, log the error and continue
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.error("Exception thrown", ex);
                    logger.info("Field: {}", field);
                    // </editor-fold>
                    continue;
                }

                // Consider the unique fields
                if (gect.methodSpecifcParm.equals(true)) // Need to save the duplicated record in the database
                // <editor-fold defaultstate="collapsed" desc="consider unique fields">
                // Update values of unique fields to have postfix "_$" to avoid unique exception
                {
                    if (BaseEntity.isFieldUnique(field)) {
                        // Check if the field class is String
                        if (field.getType() == String.class) {
                            // Append the postfix
                            String stringValue = (String) value + "_1";
                            value = stringValue;
                        } else // <editor-fold defaultstate="collapsed" desc="Log">
                        {
                            logger.warn("Unique Field is not String, Not supported yet. Entity: {} Field: {}", duplicatedEntity, field);
                        }
                        // </editor-fold>
                    }
                }
                // </editor-fold>

                // Copy the field value to the duplicated one
                duplicatedEntity.invokeSetter(field, value);
            }

            if (gect.methodSpecifcParm.equals(true)) {
                // Need to save the duplicated record in the database
                // <editor-fold defaultstate="collapsed" desc="Consider unique constraints">
                List<String> uniqueConstraintColumnName
                        = BaseEntity.getUniqueConstraintFieldNames(entityClass);
                for (String uniqueColumnName : uniqueConstraintColumnName) {
                    Field uniqueField = BaseEntity.getClassField(entityClass, uniqueColumnName);
                    if (uniqueField == null) {
                        // <editor-fold defaultstate="collapsed" desc="Log">
                        logger.warn("Unique Field Not Found. Entity Class: {} Column Name:{}", entityClass, uniqueColumnName);
                        // </editor-fold>
                        continue;
                    }
                    // Check supported fields types in this function
                    if (uniqueField.getType() != String.class) {
                        // <editor-fold defaultstate="collapsed" desc="Log">
                        if (duplicatedEntity != null) {
                            logger.warn("Unique Field is not String, Not supported yet. Entity: {} Field:{} ", duplicatedEntity.getClassName(), uniqueField);
                        }
                        // </editor-fold>
                        continue;
                    }
                    // Get the original field value
                    String value = null;
                    try {
                        value = (String) originalEntity.invokeGetter(uniqueField);
                    } catch (NoSuchMethodException ex) {
                        // If no getter for the field, log the error and continue
                        // <editor-fold defaultstate="collapsed" desc="Log">
                        logger.error("Exception thrown", ex);
                        logger.info("Field: {}", uniqueField);
                        // </editor-fold>
                        continue;
                    }
                    // Update values of unique fields to have postfix "_$" to avoid unique exception
                    value += "_$";
                    // Copy the field value to the duplicated one
                    duplicatedEntity.invokeSetter(uniqueField, value);
                }
                // </editor-fold>
            }

            // Override the assignment of the dbid as a field in the previous code
            duplicatedEntity.setDbid(0);

            // Get parent if any, and set the relation
            List<String> parentFieldNames
                    = BaseEntity.getParentEntityFieldName(duplicatedEntity.getClass().getName());
            String fieldName = "";
            if (parentFieldNames != null) {
                for (int i = 0; i < parentFieldNames.size(); i++) {
                    Class fieldClass = BaseEntity.getClassField(duplicatedEntity.getClass(), parentFieldNames.get(i)).getType();
                    Class entityCls = entityParent.getClass();
                    boolean done = false;
                    while (entityCls != null) {
                        if (fieldClass == entityCls) {
                            fieldName = parentFieldNames.get(i);
                            done = true;
                            break;
                        }
                        entityCls = entityCls.getSuperclass();
                    }
                    if (done) {
                        break;
                    }
                }
            }
            if (!fieldName.isEmpty()) {
                // Entity has parent
                // Get parent field
                Field parentField = BaseEntity.getClassField(entityClass, fieldName);
                if (entityParent == null) {
                    // NO parent entity is passed to the function although it has parent
                    // Check if it's the grand parent (the only case where parent is null)
                    if (!gect.getGrandParent().equals(originalEntity)) {
                        // Entity is not the grand parent, we should have parent entity passed
                        // <editor-fold defaultstate="collapsed" desc="Log">
                        logger.warn("EntityParent is not passed to callback method");
                        // </editor-fold>
                        // TODO: rollback code should be here for transaction handling
                        // add to gect functionResult error message (Internal Error)
                        logger.info("Returning");
                        return null;
                    }
                } else {
                    // A parent entity is passed to the function
                    try {
                        // Set the parent in the duplicated entity
                        duplicatedEntity.invokeSetter(parentField, entityParent);

                        // Set the duplicated entity in the parent
                        Field entityFieldInParent = BaseEntity.getChildFieldInParentEntity(
                                duplicatedEntity, parentField, entityParent);
                        if (null != entityFieldInParent) {
                            // <editor-fold defaultstate="collapsed" desc="Set Child In Parent if Cascaded Pesist">
                            /**
                             * MUST check cascaded persist first, otherwise you
                             * got an exception like EJB5018: An exception was
                             * thrown during an ejb invocation on
                             * [OEntityManager]
                             * javax.ejb.TransactionRolledbackLocalException:
                             * Exception thrown from bean; nested exception is:
                             * java.lang.IllegalStateException: During
                             * synchronization a new object was found through a
                             * relationship that was not marked cascade PERSIST:
                             * ScreenInput[dbid=0, InstanceID=null].
                             */
                            OneToOne one2OneAnnot = entityFieldInParent.getAnnotation(OneToOne.class);
                            if (one2OneAnnot != null) {
                                if (Arrays.binarySearch(one2OneAnnot.cascade(), CascadeType.ALL) >= 0 || Arrays.binarySearch(one2OneAnnot.cascade(), CascadeType.PERSIST) >= 0) // Cascaded Persist is found in parent, Safe to set it in parent
                                {
                                    entityParent.invokeSetter(entityFieldInParent, duplicatedEntity);
                                    addDuplicateToGECTList = false;    // No need to save it manually,
                                    // as it will be save using cascaded persist
                                }
                            }
                            OneToMany one2ManyAnnot = entityFieldInParent.getAnnotation(OneToMany.class);
                            if (one2ManyAnnot != null) {
                                if (Arrays.binarySearch(one2ManyAnnot.cascade(), CascadeType.ALL) >= 0 || Arrays.binarySearch(one2ManyAnnot.cascade(), CascadeType.PERSIST) >= 0) // Cascaded Persist is found in parent, Safe to set it in parent
                                {
                                    List<BaseEntity> childListInParent
                                            = (List<BaseEntity>) entityParent.invokeGetter(entityFieldInParent);
                                    if (null == childListInParent) // List is null
                                    // Create the list
                                    {
                                        childListInParent = new ArrayList<BaseEntity>();
                                    }
                                    childListInParent.add(duplicatedEntity);
                                    entityParent.invokeSetter(entityFieldInParent, childListInParent);
                                    addDuplicateToGECTList = false;    // No need to save it manually,
                                    // as it will be save using cascaded persist
                                }
                            }
                            if (entityFieldInParent.getAnnotation(ManyToOne.class) != null
                                    || entityFieldInParent.getAnnotation(ManyToMany.class) != null) {
                                logger.warn("ManyToOne & ManyToMany Children Not Supported In Duplication. duplicatedEntity:{}, entityParent:{} ", duplicatedEntity, entityParent);
                            }
                            // </editor-fold>
                        } else {
                            // Error: Child Field Is Not Found In Parent
                            // <editor-fold defaultstate="collapsed" desc="Log">
                            logger.warn("Child Field Is Not Found In Parent! duplicatedEntity: {},", duplicatedEntity);
                            logger.warn(" parentField: {}, entityParent:{}", parentField, entityParent);
                            // </editor-fold>
                            // TODO: add to gect functionResult error message (Internal Error)
                        }
                    } catch (NoSuchMethodException ex) {
                        logger.error("Exception thrown", ex);
                        logger.info("Parent Field: {}", parentField);
                    }
                }
            }

            // Add the entity to the list, don't save here
            if (addDuplicateToGECTList) {
                ArrayList<BaseEntity> newEntitiesList = (ArrayList<BaseEntity>) gect.methodSpecifcParm1;
                newEntitiesList.add(duplicatedEntity);
            } else {
                // FIXME: This way the CREATE ACTION won't be called, need to
                // guarantee that somehow
            }

            // If forcedInitializedFields are not set in the duplicated entity, and
            // there are forced intialized fields duplicated, then, at save, you get
            // something like: java.lang.IllegalStateException: During synchronization
            // a new object was found through a relationship that was not marked cascade PERSIST: OScreen[dbid=0, InstanceID=null].
            duplicatedEntity.getForcedInitializedFields().addAll(
                    originalEntity.getForcedInitializedFields());
            duplicatedEntity.forcedFieldsOriginal = originalEntity.forcedFieldsOriginal;

            /* Code Fix:
             Fixing special case for OEntity Duplication
             */
            if (entityParent != null) {
                String entityClassName = originalEntity.getClassName();
                String parentEntityClassName = entityParent.getClassName();
                if ((parentEntityClassName.equals("OEntity") && entityClassName.equals("OEntity"))
                        || (parentEntityClassName.equals("OObject") && entityClassName.equals("OObject"))
                        || (parentEntityClassName.equals("OBusinessObject") && entityClassName.equals("OBusinessObject"))
                        || (parentEntityClassName.equals("OEntityAction") && entityClassName.equals("OEntityAction"))) {
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    if (originalEntity != null && entityParent != null) {
                        logger.warn("Duplicating OEntity, Special Case, Stopping. Entity: {} Entity Parent: {}", originalEntity.getClassName(), entityParent.getClassName());
                    }
                    // </editor-fold>
                    // TODO: add to gect functionResult error message
                    logger.info("Returning");
                    return null;
                }
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.info("Entity: {}", originalEntity);
            // </editor-fold>
            // TODO: add to gect functionResult error message (Internal Error)
        }
        logger.info("Returning");
        return duplicatedEntity;
    }
    //FIXME: Commented from version r18.1.7 as there's no usage
//
//    /**
//     * Private, as no transaction handling is done internally
//     *
//     * @param entity
//     * @param oldModule
//     * @param newModule
//     * @param loggedUser
//     * @param parentFieldName
//     * @param parentNewDBID
//     *
//     * MUST be consistent with {@link BaseEntity#getVCModuleExpression()} in
//     * multi-parent decision
//     */
//    private OFunctionResult changeEntityForModuleChange(
//            BaseEntity entity, OModule oldModule, OModule newModule,
//            String parentTableName, long parentNewDBID, OUser loggedUser) {
//        logger.trace("Entering");
//        OFunctionResult oFR = new OFunctionResult(), internalFR;
//        try {
//            StringBuilder entityModuleChange = new StringBuilder();
//
////            EntityManager oem = oem.getEM(oem.getSystemUser(loggedUser)) ;
//            // <editor-fold defaultstate="collapsed" desc="Get entityOldDBID & generate entityNewDBID">
//            long entityOldDBID = entity.getDbid();
//            long entityNewDBID = dbidGenService.generateDBID(
//                    entity, newModule, oem.getSystemUser(loggedUser));
//            // </editor-fold>
//
//            List<String> entityDBTables = entity.getEntityTableName();
//            // <editor-fold defaultstate="collapsed" desc="Get all master tables of entity into entityDBTables, return on Error">
//            if (entityDBTables.isEmpty()) {
//                // <editor-fold defaultstate="collapsed" desc="Log">
//                logger.warn("getEntityDBTableName Returned Empty List! Entity:{}", entity.getClassName());
//                // </editor-fold>
//                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
//                logger.trace("Returning");
//                return oFR;
//            }
//            // </editor-fold>
//
//            // <editor-fold defaultstate="collapsed" desc="Update entity dbid & parent & module in master tables">
//            for (int iTable = 0; iTable < entityDBTables.size(); iTable++) {
//                String entityDBTable = entityDBTables.get(iTable);
//
//                String moduleColName = null;
//                // <editor-fold defaultstate="collapsed" desc="Get moduleColName if found for the table">
//                // Check & Get the reference from entityDBTable to OModule table
//                {
//                    ArrayList<String> moduleColConds = new ArrayList<String>();
//                    moduleColConds.add("ownerOEntityTableName='" + entityDBTable + "'");
//                    moduleColConds.add("refOEntityTableName='omodule'");
//                    List<OEntityReference> tempOEntityRefs4Module = oem.loadEntityList(
//                            OEntityReference.class.getSimpleName(), moduleColConds, null, null, loggedUser);
//                    if (!tempOEntityRefs4Module.isEmpty()) {
//                        // Table has reference to OModule
//                        // Get reference entity class
//                        Class refEntityClass = Class.forName(tempOEntityRefs4Module.get(0).getOwnerOEntity().getEntityClassPath());
//                        Field refEntityModuleFld = BaseEntity.getClassField(refEntityClass,
//                                BaseEntity.getVCModuleExpression(refEntityClass));
//                        moduleColName = entity.getFieldColumnName(refEntityModuleFld);
//                    }
//                }
//                // </editor-fold>
//
//                String parentColName = null; // in entityDBTable
//                // <editor-fold defaultstate="collapsed" desc="Check & Get the reference from entityDBTable to parent table">
//                if (parentTableName != null) {
//                    ArrayList<String> parentColConds = new ArrayList<String>();
//                    parentColConds.add("ownerOEntityTableName='" + entityDBTable + "'");
//                    parentColConds.add("refOEntityTableName='" + parentTableName + "'");
//                    parentColConds.add("refParent=true");
//                    List<OEntityReference> tempOEntityRefs = oem.loadEntityList(
//                            OEntityReference.class.getSimpleName(), parentColConds, null,
//                            null, oem.getSystemUser(loggedUser));
//                    if (!tempOEntityRefs.isEmpty()) {
//                        // Table has reference to parent table
//                        parentColName = tempOEntityRefs.get(0).getFieldColumnName();
//                    } else {
//                        // <editor-fold defaultstate="collapsed" desc="Log">
//                        logger.warn("Wiered Case: Child table has no reference to parent. Child Table:{}, Parent Table:{} ",
//                                entityDBTable, parentTableName);
//                        // </editor-fold>
//                    }
//                }
//                // </editor-fold>
//
//                // Considering a child has module and the module is different
//                // than oldModule, e.g. ScreenFunction in a module other than of
//                // its screen (e.g. customization)
//                String tableDBIDQueryStr
//                        = " UPDATE " + entityDBTable
//                        + " SET DBID   = " + entityNewDBID
//                        + ((moduleColName == null) // No reference to OModule
//                                ? // Add nothing
//                                ""
//                                : // update module in table
//                                ("   , " + moduleColName + " = " + newModule.getDbid()))
//                        + ((parentColName == null) // No reference to OModule
//                                ? // Add nothing
//                                ""
//                                : // update module in table
//                                ("   , " + parentColName + " = " + parentNewDBID))
//                        + " WHERE DBID = " + entityOldDBID;
////                entityModuleChange.append(tableDBIDQueryStr).append(";\n");
//                // Note: not all tables have custom3 field, can't save old DBID in custom1
//                oem.executeEntityUpdateNativeQuery(tableDBIDQueryStr, oem.getSystemUser(loggedUser));
//                // <editor-fold defaultstate="collapsed" desc="Log">
//                logger.trace("Table Row DBID Changed. Table: {}", entityDBTable);
//                logger.trace("Old DBID: {} New DBID:{}", entityOldDBID, entityNewDBID);
//                if (oldModule != null && newModule != null) {
//                    logger.trace("Old Module: {} New Module: {}", oldModule.getDbid(), newModule.getDbid());
//                }
//
//                logger.trace("Update Query: {}", tableDBIDQueryStr);
//                // </editor-fold>
//                // Reload entity and put it in returned ODM
//            }
//            // </editor-fold>
//
//            List<OEntityReference> oentityRefs = new ArrayList<OEntityReference>();
//            // <editor-fold defaultstate="collapsed" desc="Get master-tables all-entities references">
//            for (String entityDBTable : entityDBTables) {
//                List<OEntityReference> tempOEntityRefs = oem.loadEntityList(
//                        OEntityReference.class.getSimpleName(),
//                        Collections.singletonList("refOEntityTableName= '" + entityDBTable + "'"),
//                        null, null, loggedUser);
//                oentityRefs.addAll(tempOEntityRefs);
//            }
//            // </editor-fold>
//
//            // For children, set their DBIDs & Modules as their parent's
//            // (if different than old entity module) & TTs
//            // For non children, set the FK DBID new value
//            for (OEntityReference oentityRef : oentityRefs) {
//                if (oentityRef.isListFld()) // We only update single-values field,
//                {
//                    continue;
//                }
//
//                // Load related entities
//                List<BaseEntity> relEntities = oem.loadEntityList(
//                        oentityRef.getOwnerOEntity().getEntityClassName(),
//                        Collections.singletonList(oentityRef.getFieldName() + ".dbid=" + entityOldDBID),
//                        null, null, oem.getSystemUser(loggedUser));
//                // FIXME: Change the child module if it has direct module is not supported
//                for (BaseEntity relEntity : relEntities) {
//                    if (oentityRef.isRefParent()) // Reference Entity is child of our entity
//                    {
//                        if (newModule.dbidIsInRange(relEntity.getDbid())) // Entity is already in new module range, no need to change the module
//                        {
//                            continue;
//                        }
//
//                        List<String> relEntityParentFldNames = BaseEntity.getParentEntityFieldName(
//                                relEntity.getClass().getName());
//                        boolean ok2ChangeChildModule = true;
//                        OModule childNewModule = newModule;
//
//                        if (relEntityParentFldNames != null
//                                && relEntityParentFldNames.size() > 1) {
//                            // Special case of having more than one parent
//                            // MUST be consistent with {@link BaseEntity#getVCModuleExpression()}
//                            // in multi-parent decision
//                            // Decide which module to use
//                            OModule scndParentModule = null;
//                            BaseEntity scndParent = null;
//                            if (relEntityParentFldNames.get(0).equals(oentityRef.getFieldName())) // Reference is for first parent
//                            {
//                                scndParent = (BaseEntity) relEntity.invokeGetter(relEntityParentFldNames.get(1));
//                            } else {
//                                scndParent = (BaseEntity) relEntity.invokeGetter(relEntityParentFldNames.get(0));
//                            }
//                            if (scndParent != null) {
//                                scndParentModule = scndParent.getModule();
//                            }
//                            if (scndParentModule != null) {
//                                // Go if: oldModule.getDbid() == 1 && newModule.getDbid() != 1
//                                // Old is system, new is non-system
//                                //
//                                // Go If: oldModule.getDbid() != 1 && scndParentModule.getDbid() == 1
//                                // Old is non-system, second parent is system
//                                //
//                                // N/A: oldModule.getDbid() == 1 && newModule.getDbid() == 1
//                                // Old is system, new module is system
//                                //
//                                if (oldModule.getDbid() != 1 // Old is non-sys
//                                        && scndParentModule.getDbid() != 1 // Second parent is non-sys
//                                        ) {
//                                    if (newModule.getDbid() != 1) // New is non-sys
//                                    {
//                                        // Do nothing, already in non-system range
//                                        // until managing cross-reference between non-system modules
//                                        ok2ChangeChildModule = false;
//                                    } else // New is system
//                                    {
//                                        // Set the module to the second parent module
//                                        // which is not system
//                                        childNewModule = scndParentModule;
//                                    }
//                                }
//                            }
//                        }
//                        if (ok2ChangeChildModule) {
//                            OFunctionResult childFR = changeEntityForModuleChange(
//                                    relEntity, oldModule, childNewModule,
//                                    oentityRef.getRefOEntityTableName(), entityNewDBID, loggedUser);
//                            oFR.append(childFR);
//                        }
//
//                    } else {
//                        // Reference Entity is NOT a child of our entity
//                        // Update it directly
//                        String refUpdateQueryStr
//                                = " UPDATE " + oentityRef.getOwnerOEntityTableName()
//                                + " SET    " + oentityRef.getFieldColumnName() + " = " + entityNewDBID
//                                + " WHERE  " + oentityRef.getFieldColumnName() + " = " + entityOldDBID;
//                        entityModuleChange.append(refUpdateQueryStr).append(";\n");
//                        // Note: not all tables have custom3 field, can't save old DBID in custom1
//                        oem.executeEntityUpdateNativeQuery(refUpdateQueryStr, loggedUser);
//                        // <editor-fold defaultstate="collapsed" desc="Log">
//                        if (oentityRef != null) {
//                            logger.trace("Reference Column DBID Changed. Table: {}, Column Name: {}",
//                                    oentityRef.getOwnerOEntityTableName(), oentityRef.getFieldColumnName());
//                        }
//
//                        logger.trace("Old Column Value:{}, New Column Value: {} ", entityOldDBID, entityNewDBID);
//                        logger.trace("Update Query: {}", refUpdateQueryStr);
//                        // </editor-fold>
//                    }
//                }
//            }
//            oFR.setReturnValues(Collections.singletonList(entityNewDBID));
//            String entityAuditTrailModuleChange = "update EntityAuditTrail set entityDBID="
//                    + entityNewDBID + " where entityDBID=" + entityOldDBID;
//            entityModuleChange.append(entityAuditTrailModuleChange).append(";\n");
//            oem.executeUpdateQuery(entityAuditTrailModuleChange, loggedUser);
//            String entityVersionInfoModuleChange = "update OVCEntityVersionInfo set entityDBID="
//                    + entityNewDBID + " where entityDBID=" + entityOldDBID;
//            entityModuleChange.append(entityVersionInfoModuleChange).append(";\n");
//            oem.executeUpdateQuery(entityVersionInfoModuleChange, loggedUser);
//            String entityVersionTrackModuleChange = "update OVCEntityVersionTrack set entityDBID="
//                    + entityNewDBID + " where entityDBID=" + entityOldDBID;
//            entityModuleChange.append(entityVersionTrackModuleChange).append(";\n");
//            oem.executeUpdateQuery(entityVersionTrackModuleChange, loggedUser);
//        } catch (Exception ex) {
//            // <editor-fold defaultstate="collapsed" desc="Log">
//            logger.error("Exception thrown", ex);
//            if (null != entity.getClassName()) {
//                logger.trace("Entity: {}", entity.getClassName());
//            }
//            // </editor-fold>
//            if (ex instanceof FABSException) {
//                oFR.addError(usrMsgService.getUserMessage("ModuleError", loggedUser));
//            } else {
//                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
//            }
//
//        } finally {
//            oem.closeEM(loggedUser);
//            logger.trace("Returning");
//            return oFR;
//        }
//
//    }

    //FIXME: Commented from version r18.1.7 as there's no usage
//    /**
//     * Changes entity DBID for the new module, only if entity has direct module
//     * field, i.e. module VC expression has no '.'. It Updates all entity
//     * references for the new DBID. Changes & Updates children DBID for the new
//     * module.
//     * <br>Doesn't update the entity into the database, it's the caller
//     * responsibility
//     * <br>Joins & Commits transaction
//     * <br>If entity has no module expression or module is not changed, it does
//     * nothing
//     * <br>Only tables that have OEntity that are included in the references
//     *
//     * @param odm BaseEntity to be updated, original dbid in database, and with
//     * the new module set
//     * @param functionParms
//     * @param loggedUser
//     * @return ReturnedDataMessage = entity with new DBID in data(0)
//     */
//    @Override
//    public OFunctionResult changeEntityForModuleChange(ODataMessage odm, OUser loggedUser) {
//        logger.debug("Entering");
//        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
//        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
//                odm, loggedUser, BaseEntity.class);
//        if (inputValidationErrors != null) {
//            logger.debug("Returning");
//            return inputValidationErrors;
//        }
//        // </editor-fold>
//
//        OFunctionResult oFR = new OFunctionResult();
//        OFunctionResult internalFR = new OFunctionResult();
//        try {
//            BaseEntity entity = (BaseEntity) odm.getData().get(0);
//            oFR.setReturnedDataMessage(odm);
//            oem.getEM(loggedUser);
//
//            // <editor-fold defaultstate="collapsed" desc="Validate entity has module expression & is not new, Return if not">
//            String entityVCModExp = entity.getVCModuleExpression();
//            if (entityVCModExp == null) {
//                if (entity != null) {
//                    logger.warn("Function can not be called for Non-Module Entity. Entity: {}", entity.getClassName());
//                }
//
//                //FIXME: add error user message
//                logger.debug("Returning");
//                return oFR;
//            }
//            if (entityVCModExp.equals("")) {
//                if (entity != null) {
//                    logger.warn("Function can not be called for Non-Module Entity. Entity: {}", entity.getClassName());
//                }
//
//                //FIXME: add error user message
//                logger.debug("Returning");
//                return oFR;
//            }
//            if (entityVCModExp.contains(".")) {
//                if (entity != null) {
//                    logger.warn("Function can not be called for Non-Direct-Module Entity. Entity: {}", entity.getClassName());
//                }
//
//                //FIXME: add error user message
//                logger.debug("Returning");
//                return oFR;
//            }
//            if (entity.getDbid() == 0) {
//                if (entity != null) {
//                    logger.warn("Function can not be called for new entities. Entity: {}", entity.getClassName());
//                }
//
//                //FIXME: add error user message
//                logger.debug("Returning");
//                return oFR;
//            }
//            // </editor-fold>
//
//            OModule entityModuleInDB = null;
//            // <editor-fold defaultstate="collapsed" desc="Get entityModuleInDB">
//            BaseEntity entityInDB = oem.loadEntity(entity.getClassName(),
//                    Collections.singletonList("dbid=" + entity.getDbid()), null,
//                    oem.getSystemUser(loggedUser));
//            if (entityInDB == null) {
//                List<String> conditions = new ArrayList<String>();
//                conditions.add("dbid=" + entity.getDbid());
//                conditions.add(OEntityManager.CONFCOND_GET_INACTIVEONLY);
//
//                entityInDB = oem.loadEntity(entity.getClassName(), conditions, null,
//                        oem.getSystemUser(loggedUser));
//            }
//
//            if (entityInDB == null) {
//                oFR.addError(usrMsgService.getUserMessage("ErrorLoadingEntity", loggedUser));
//                logger.debug("Returning");
//                return oFR;
//            }
//            entityModuleInDB = entityInDB.getModule();
//            // </editor-fold>
//
//            OModule entityNewModule = entity.getModule();
//            // <editor-fold defaultstate="collapsed" desc="Validate that module is changed, return if not">
//            if (entityNewModule == null && entityModuleInDB == null) {
//                // Entity module is NOT changed from the value in the database
//                // Do nothing, and return the entity AS IS
//                logger.debug("Returning");
//                return oFR;
//            }
//            if (entityNewModule.getDbid() == entityModuleInDB.getDbid()) {
//                // Entity module is NOT changed from the value in the database
//                // Do nothing, and return the entity AS IS
//                logger.debug("Returning");
//                return oFR;
//            }
//            // </editor-fold>
//
//            // Entity module is changed from the value in the database
//            //long oldEntityDBID = entity.getDbid();
//            internalFR = changeEntityForModuleChange(entity, entityModuleInDB, entityNewModule,
//                    null/*not having parent of changed module*/, 0/*any value since it's not parent*/,
//                    loggedUser);
//
//            long newEntityDBID = (Long) internalFR.getReturnValues().get(0);
//            // <editor-fold defaultstate="collapsed" desc="Update Entity TextTranslation">
//            //FIXME: updateEntityTranslationOModule should return success or failure
////            textTranslationService.updateEntityTranslationOModule(entity,
////                    oldEntityDBID, newEntityDBID, entityModuleInDB.getDbid(), entityNewModule.getDbid(),
////                    oem.getSystemUser(loggedUser));
//            // </editor-fold>
//
//            // Load the new entity to return it
//            entity = oem.loadEntity(entity.getClassName(), newEntityDBID, null, null, loggedUser);
//            odm.getData().clear();
//            odm.getData().add(entity); //with new DBID
//            oFR.setReturnedDataMessage(odm);
//
//        } catch (Exception ex) {
//            // <editor-fold defaultstate="collapsed" desc="Log">
//            logger.error("Exception thrown", ex);
//            logger.debug("odm: {}", odm);
//            // </editor-fold>
//            if (ex instanceof FABSException) {
//                oFR.addError(usrMsgService.getUserMessage("ModuleError", loggedUser));
//            } else {
//                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
//            }
//        } finally {
//            oem.closeEM(loggedUser);
//            logger.debug("Returning");
//            return oFR;
//        }
//    }
    /**
     * List of parents class paths, if no parents, then the list is empty. If
     * entity has more than one parent, then all the parents trees are got
     *
     * @param entity
     * @return empty list in case of no parents found or error, otherwise, a
     * filled tree
     */
    @Override
    public List<String> getEntityParentsTree(String entityClassPath) {
        logger.debug("Entering");
        List<String> parentsTree = new ArrayList<String>();
        try {
            List<String> parentClassPaths = BaseEntity.getParentClassPath(entityClassPath);
            while (parentClassPaths != null) {
                if (parentClassPaths.size() == 1) {
                    // Only one parent found
                    // Add this parent to tree
                    parentsTree.add(parentClassPaths.get(0));
                    // Get the parent's parents
                    parentClassPaths = BaseEntity.getParentClassPath(parentClassPaths.get(0));

                } else if (parentClassPaths.size() == 2) {
                    // Two parents found
                    // Recusively, adding both parents trees to our tree
                    parentsTree.addAll(getEntityParentsTree(parentClassPaths.get(0)));
                    parentsTree.addAll(getEntityParentsTree(parentClassPaths.get(1)));

                    break; // No need to go further, already got recursively

                } else {
                    // Either parentClassPaths is empty, or more than two parents found
                    // <editor-fold defaultstate="collapsed" desc="Log">
                    logger.warn("More than two parents, case not supported. Root Entity Class Path: {} , Parents: {}", entityClassPath, parentClassPaths);
                    // </editor-fold>
                    break; // Supporess further processing
                }
            }

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            logger.debug("Entity Class Path: {}", entityClassPath);
            // </editor-fold>

        } finally {
            logger.debug("Returning");
            return parentsTree;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="check Entity Refrences">
    /**
     * Check if the entity has reference values in other tables
     *
     * @param entity that the user need to delete, activate, deactivate (or make
     * change)
     * @return BaseEntity that will prevent deletion, activate, deactivate (or
     * making change)
     */
    @Override
    public List<BaseEntity> loadNonChildEntityReferences(BaseEntity entity, OUser loggedUser)
            throws Exception {
        logger.debug("Entering");
        List<BaseEntity> returnedRefrenceEntityList = null;
        //OObjectBriefInfoField infoField = (OObjectBriefInfoField)entity;
        // <editor-fold defaultstate="collapsed" desc="Get EM">
//        try {
//            oem.getEM(loggedUser);
//        } catch (FABSException ex) {
//            // <editor-fold defaultstate="collapsed" desc="log">
//            OLog.logException(ex, loggedUser, "");
//            // </editor-fold>
//        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="load OEntity of passed entity to create condition">
        ArrayList<String> loadOEntityCondition = new ArrayList<String>();
        String entityClassName = entity.getClassName();

        loadOEntityCondition.add("entityClassPath like '%." + entityClassName + "'");
        OEntity oentity = entitySetupService.loadOEntity(
                entity.getClassName(),
                loggedUser);
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="load entities contain foreign keys to loaded OEntity">
        List<OEntityReference> refrenceEntityList = null;
        if (oentity != null) {

            List<String> refrenceEntityListCondition = new ArrayList<String>();
            refrenceEntityListCondition.add("refOEntity.dbid =  " + oentity.getDbid());
            refrenceEntityListCondition.add("refParent = false");
            refrenceEntityListCondition.add("listFld = false");
            refrenceEntityList = oem.loadEntityList(
                    OEntityReference.class.getSimpleName(),
                    refrenceEntityListCondition,
                    null,
                    null,
                    loggedUser);
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="loading data of refrenced entity">
        // if size > 0 of loaded refreces forien keys
        int counter = 0;
        String ownerEntityClassName = "";
        if (refrenceEntityList != null && refrenceEntityList.size() > 0) {
            // loop on refrenceEntityList
            outerloop:
            for (OEntityReference oentityRefrence : refrenceEntityList) {
                if (oentityRefrence.getOwnerOEntity() == null) {
                    logger.warn("ownerOEntity in EntityRefrence returns NULL");
                    continue;
                }
                ownerEntityClassName = oentityRefrence
                        .getOwnerOEntity()
                        .getEntityClassName();
                counter++;
                List<String> oentityRefrenceCondition = new ArrayList<String>();
                if (oentityRefrence.getOwnerOEntity() == null
                        || oentityRefrence.getOwnerOEntity().getEntityClassPath().contains(".Inbox")
                        || oentityRefrence.getOwnerOEntity().getEntityClassPath().contains(".Outbox")
                        || oentityRefrence.getOwnerOEntity().getEntityClassPath().contains(".ProcessInstance")
                        || oentityRefrence.getOwnerOEntity().getEntityClassPath().contains(".TaskInstance")
                        || oentityRefrence.getOwnerOEntity().getEntityClassPath().contains(".TaskProperties")
                        || oentityRefrence.getOwnerOEntity().getEntityClassPath().contains(".TTaskInfo")) {
                    continue;
                }
                // <editor-fold defaultstate="collapsed" desc="check if oentityRefrence isList">
                OEntity refOwnerEntity = null;
                try {
                    refOwnerEntity = entitySetupService.loadOEntity(
                            oentityRefrence.getOwnerOEntity().getEntityClassPath(),
                            loggedUser);
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                    if (oentityRefrence != null && oentityRefrenceCondition != null) {
                        logger.warn("Check OEntity for the presence of the Owner Entity. Owner Entity: {} Loading Condition:{}", oentityRefrence.getOwnerOEntity().getEntityClassName(), oentityRefrenceCondition);
                    }

                    continue;
                }
                if (refOwnerEntity == null || refOwnerEntity.isInActive()) {
                    continue;
                }
                logger.debug("Counter Info. Owner Entity:{} Owner Entity Indx:{}", refOwnerEntity, counter);
                /*
                 * FIXME: remove loading of transient fields from entity refrnce function
                 * FIXME: checking the presence  of table
                 */
                oentityRefrenceCondition.add(
                        oentityRefrence.getFieldName()
                        + ".dbid = " + entity.getDbid());
                // </editor-fold>
                List<BaseEntity> ownerEntity = null;
                try {
                    ownerEntity = oem.loadEntityList(
                            ownerEntityClassName,
                            oentityRefrenceCondition,
                            null,
                            null,
                            loggedUser);
                } catch (Exception ex) {
                    logger.error("Exception thrown", ex);
                    if (oentityRefrence != null && oentityRefrenceCondition != null) {
                        logger.debug("Owner Entity: {} Loading Condition: {}", oentityRefrence.getOwnerOEntity().getEntityClassName(), oentityRefrenceCondition);
                    }

                    continue;
                }
                //  if size of data > 0
                if (ownerEntity != null && ownerEntity.size() > 0) {
                    returnedRefrenceEntityList = new ArrayList<BaseEntity>();
                    returnedRefrenceEntityList.addAll(ownerEntity);
                } // end if size of data > 0
            }// end loop on each
        } // end if size > 0
        // </editor-fold>
        return returnedRefrenceEntityList;
    }
    // </editor-fold>

    @Override
    public OFunctionResult checkInvalidEntityFK(ODataMessage odm, OFunctionParms ofp, OUser loggedUser) {
        logger.debug("Entering");
        OEntity oPassedEntity = null;
        OFunctionResult oFR = new OFunctionResult();
        BaseEntity entity = (BaseEntity) odm.getData().get(0);
        try {
            entity = oem.loadEntity(entity.getClassName(),
                    Collections.singletonList("dbid = " + entity.getDbid()),
                    null,
                    loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            if (entity != null) {
                logger.debug("Passed Entity: {}", entity.getClassName());
            }
        }

        // <editor-fold defaultstate="collapsed" desc="load OEntity of passed entity">
        try {
            oPassedEntity = entitySetupService.loadOEntity(
                    entity.getClassName(),
                    loggedUser);
        } catch (UserNotAuthorizedException ex) {
            logger.error("Exception thrown", ex);
            if (oPassedEntity != null) {
                logger.debug("OEntity: {}", oPassedEntity.getClassName());
            }
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="load entities contain foreign keys to loaded OEntity">
        List<OEntityReference> fkEntityList = null;
        if (oPassedEntity != null) {
            List<String> refrenceEntityListCondition = new ArrayList<String>();
            refrenceEntityListCondition.add("ownerOEntity.dbid =  " + oPassedEntity.getDbid());
            try {
                fkEntityList = oem.loadEntityList(
                        OEntityReference.class.getSimpleName(),
                        refrenceEntityListCondition,
                        null,
                        null,
                        loggedUser);
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
                oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
            }
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="create the native query containing values of foriengkey">
        // loop on refrence fields & construct the query
        String fieldNames = "";
        int indx = 0;
        for (OEntityReference oEntityReference : fkEntityList) {
            indx++;
            if (oEntityReference.getFieldColumnName() != null
                    && !oEntityReference.getFieldColumnName().equals("")) {
                if (indx == fkEntityList.size()) {
                    fieldNames += oEntityReference.getFieldColumnName();
                } else {
                    fieldNames += oEntityReference.getFieldColumnName() + ",";
                }
            } else {

                if (indx == fkEntityList.size()) {
                    fieldNames += "0";
                } else {
                    fieldNames += "0" + ",";
                }
            }
        }
        String selectFKValueQuery = " select ";
        selectFKValueQuery += fieldNames;
        selectFKValueQuery += " from " + fkEntityList.get(0).getOwnerOEntityTableName();
        selectFKValueQuery += " where dbid = " + entity.getDbid();

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="load FK values">
//        EntityManager em = null;
        List<Object[]> xyz = null;
        try {
//            em = oem.getEM(oem.getSystemUser(loggedUser));
            xyz = oem.executeEntityListNativeQuery(selectFKValueQuery, loggedUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
//        List<Object[]> xyz;
        try {
            xyz = oem.executeEntityListNativeQuery(selectFKValueQuery, loggedUser);
            // </editor-fold>
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            xyz = null;
        }

        // <editor-fold defaultstate="collapsed" desc="compare values">
        int dbidFKIndx = 0;

        if (xyz != null
                && xyz.get(0) != null
                && xyz.get(0).length > 0) {
            for (Object x : xyz.get(0)) {
                if (x != null) {
                    Long dbidFK = Long.parseLong(x.toString());
                    if (dbidFK > 0) {
                        String entityClassPath = fkEntityList.get(dbidFKIndx).getRefOEntity().getEntityClassPath();

                        String[] entityClassPathArray = entityClassPath.split("\\.");
                        BaseEntity fkEntity = null;
                        try {
                            fkEntity = oem.loadEntity(entityClassPathArray[entityClassPathArray.length - 1],
                                    Collections.singletonList("dbid = " + dbidFK),
                                    null,
                                    loggedUser);
                        } catch (Exception ex) {
                            logger.error("Exception thrown", ex);
                            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
                        }
                        if (fkEntity == null) {
                            List<String> oFRParams = new ArrayList<String>();
                            // FIXME: returned value
                            String selectReturnedNullBaseEntity = "select entity.dbid ";
                            selectReturnedNullBaseEntity += " from "
                                    + entityClassPathArray[entityClassPathArray.length - 1]
                                    + " entity ";
                            selectReturnedNullBaseEntity += " where entity.dbid = " + dbidFK;
                            Long nativeReturnedBaseEntity = null;
                            try {
                                nativeReturnedBaseEntity = (Long) oem.executeEntityNativeQuery(selectReturnedNullBaseEntity, loggedUser);
                            } catch (Exception ex) {
                                logger.error("Exception thrown", ex);
                            }
                            oFRParams.add(entity.getClassName());
                            oFRParams.add(fkEntityList.get(dbidFKIndx).getRefOEntity().getEntityClassPath());
                            oFRParams.add(dbidFK.toString());
                            if (nativeReturnedBaseEntity != null) {
                                oFR.addError(usrMsgService.getUserMessage("InvalidFK", oFRParams, loggedUser));
                            } else {
                                oFR.addError(usrMsgService.getUserMessage("AbsentFK", oFRParams, loggedUser));
                            }
                        }
                    }
                }
                dbidFKIndx++;
            }
            if (oFR.getErrors() == null || oFR.getErrors().isEmpty()) {
                oFR.addSuccess(usrMsgService.getUserMessage("ValidFK", loggedUser));
            }
        }
        logger.debug("Returning");
        // </editor-fold>
        return oFR;
    }
}
