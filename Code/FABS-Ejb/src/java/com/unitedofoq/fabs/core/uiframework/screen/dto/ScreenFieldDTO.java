/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.screen.dto;

import java.util.List;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;

import com.unitedofoq.fabs.core.uiframework.serverjob.resources.ServerJobDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author root
 */
public class ScreenFieldDTO {
    private long dbid;
    private String fieldExpression;
    private boolean editable;
    private int sortIndex;
    private boolean mandatory;
    private String ddTitleOverride;
    private int controlSize;
    private long prsnlzdOriginal_DBID;
    private long prsnlzdUser_DBID;
    private long dd_DBID;
    private long oscreen_DBID;
    private boolean prsnlzdRemoved;
    private long changeFunctionDBID; 
    private List<ExpressionFieldInfo> expInfos = null;
final static Logger logger = LoggerFactory.getLogger(ScreenFieldDTO.class);
    public ScreenFieldDTO() {
    }

    public ScreenFieldDTO(long dbid, String fieldExpression,
            boolean editable, int sortIndex, boolean mandatory,
            String ddTitleOverride, int controlSize, 
            long prsnlzdOriginal_DBID, long prsnlzdUser_DBID, long dd_DBID,
            long oscreen_DBID, boolean prsnlzdRemoved, long changeFunctionDBID) {
        logger.debug("ScreenFieldDTO Created");
        this.dbid = dbid;
        this.fieldExpression = fieldExpression;
        this.editable = editable;
        this.sortIndex = sortIndex;
        this.mandatory = mandatory;
        this.ddTitleOverride = ddTitleOverride;
        this.controlSize = controlSize;
        this.prsnlzdOriginal_DBID = prsnlzdOriginal_DBID;
        this.prsnlzdUser_DBID = prsnlzdUser_DBID;
        this.dd_DBID = dd_DBID;
        this.oscreen_DBID = oscreen_DBID;
        this.prsnlzdRemoved = prsnlzdRemoved;
        this.changeFunctionDBID = changeFunctionDBID;        
    }
    

    public long getChangeFunctionDBID() {
        return changeFunctionDBID;
    }

    public void setChangeFunctionDBID(long changeFunctionDBID) {
        this.changeFunctionDBID = changeFunctionDBID;
    }
     
    
    public long getDbid() {
        return dbid;
    }

    public void setDbid(long dbid) {
        this.dbid = dbid;
    }
    

    
    /**
     * Get the list of parsed fieldExpression using the {@link #oScreen}
     * actOnEntity
     *
     * @return <br> ExpressionFieldInfo List <br> Null: Error
     */
    public List<ExpressionFieldInfo> getExpInfos(String oentityClassPath) {
        if (expInfos != null) // Previously Parsed, cached
        // Return it
        {
            return expInfos;
        }

        // ___________
        // Validations
        //
        if (fieldExpression == null) {
            logger.error("Field Expression Is Null");
            return null;
        }

        Class actOnEntityCls = null;
        try {
            actOnEntityCls = Class.forName(oentityClassPath);
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
            return null;
        }

        // _________________________
        // Parse the fieldExpression
        //
        expInfos = BaseEntity.parseFieldExpression(actOnEntityCls, fieldExpression);
        return expInfos;
    }

    public String getFieldExpression() {
        return fieldExpression;
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    public String getDdTitleOverride() {
        return ddTitleOverride;
    }

    public void setDdTitleOverride(String ddTitleOverride) {
        this.ddTitleOverride = ddTitleOverride;
    }

    public int getControlSize() {
        return controlSize;
    }

    public void setControlSize(int controlSize) {
        this.controlSize = controlSize;
    }

    public long getPrsnlzdOriginal_DBID() {
        return prsnlzdOriginal_DBID;
    }

    public void setPrsnlzdOriginal_DBID(long prsnlzdOriginal_DBID) {
        this.prsnlzdOriginal_DBID = prsnlzdOriginal_DBID;
    }

    public long getPrsnlzdUser_DBID() {
        return prsnlzdUser_DBID;
    }

    public void setPrsnlzdUser_DBID(long prsnlzdUser_DBID) {
        this.prsnlzdUser_DBID = prsnlzdUser_DBID;
    }

    public boolean isPrsnlzdRemoved() {
        return prsnlzdRemoved;
    }

    public void setPrsnlzdRemoved(boolean prsnlzdRemoved) {
        this.prsnlzdRemoved = prsnlzdRemoved;
    }

    public List<ExpressionFieldInfo> getExpInfos() {
        return expInfos;
    }

    public void setExpInfos(List<ExpressionFieldInfo> expInfos) {
        this.expInfos = expInfos;
    }

    public long getDd_DBID() {
        return dd_DBID;
    }

    public void setDd_DBID(long dd_DBID){
        this.dd_DBID = dd_DBID;
    }

    public long getOscreen_DBID() {
        return oscreen_DBID;
    }

    public void setOscreen_DBID(long oscreen_DBID) {
        this.oscreen_DBID = oscreen_DBID;
    }
    
}
