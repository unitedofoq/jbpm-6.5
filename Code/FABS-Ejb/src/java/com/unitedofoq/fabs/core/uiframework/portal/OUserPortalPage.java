/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework.portal;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author htawab
 */
@Entity
@ChildEntity(fields={"portlets"})
@VersionControlSpecs(omoduleFieldExpression="omodule")
public class OUserPortalPage extends BaseEntity{

    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OModule omodule;

    public String getOmoduleDD(){
        return "OUserPortalPage_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
// </editor-fold>

    @Translation(originalField="name")
    @Transient
    private String nameTranslated;
    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    private long portalPageID;

    @Translatable(translationField="nameTranslated")
    private String name;

    public String getNameTranslatedDD(){
        return "OUserPortalPage_name";
    }
    
    private String layoutType;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy="userPortalPage", fetch=FetchType.EAGER)
    private List<OUserPortalPagePortlet> userPortlets = null;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name="USER_PREF_DBID")
    private OUser user;

    public long getPortalPageID() {
        return portalPageID;
    }

    public void setPortalPageID(long portalPageID) {
        this.portalPageID = portalPageID;
    }

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

    public List<OUserPortalPagePortlet> getUserPortlets() {
        if(userPortlets == null)
            userPortlets = new ArrayList<OUserPortalPagePortlet>();
        return userPortlets;
    }

    public void setUserPortlets(List<OUserPortalPagePortlet> userPortlets) {
        this.userPortlets = userPortlets;
    }

    public String getLayoutType() {
        return layoutType;
    }

    public void setLayoutType(String layoutType) {
        this.layoutType = layoutType;
    }

    public OUser getUser() {
        return user;
    }

    public void setUser(OUser user) {
        this.user = user;
    }

}
