/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.entitybase;

import com.unitedofoq.fabs.central.OTenant;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.validation.EntityDataValidationException;
import com.unitedofoq.fabs.core.validation.FABSException;
import com.unitedofoq.fabs.core.validation.OEntityDataValidationException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.Query;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;

/**
 *
 * @author AbuBakr
 */
@Local
public interface OEntityManagerRemote {

    /**
     * Refer to {@link OEntityManager#callPostLoadForQueryLoadedList(java.util.List, java.util.List, java.util.List, OUser)
     * }
     */
    public List<BaseEntity> callPostLoadForQueryLoadedList(List<BaseEntity> baseEntities, List<String> conditions,
            List<String> fieldExpressions, OUser loggedUser);

    /**
     * Refer to {@link OEntityManager#loadEntity(java.lang.String, long, java.util.List,
     * java.util.List, com.unitedofoq.fabs.core.security.user.OUser) }
     */
    public BaseEntity loadEntity(String entityName, long dbid,
            List<String> conditions, List<String> fieldExpressions, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException;

    /**
     * Refer to {@link OEntityManager#loadEntity(java.lang.String, java.util.List,
     * java.util.List, com.unitedofoq.fabs.core.security.user.OUser) }
     */
    public BaseEntity loadEntity(String entityName, List<String> conditions,
            List<String> fieldExpressions, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException;

    /**
     * Refer to {@link OEntityManager#loadEntityDetail(com.unitedofoq.fabs.core.entitybase.BaseEntity,
     * java.util.List, java.util.List, com.unitedofoq.fabs.core.security.user.OUser)
     * }
     */
    public final static String CONDITIONAL_SHADOWED_OR = "   jjjj_Y-_-ofoq-_-FABS-_-20-_-Y_jjjj   ";

    public BaseEntity loadEntityDetail(
            BaseEntity entity, List<String> condition,
            List<String> fieldExpressions, OUser loggedUser)
            throws UserNotAuthorizedException, EntityExistsException,
            EntityNotFoundException, NonUniqueResultException, NoResultException,
            OptimisticLockException, RollbackException, TransactionRequiredException;

    /**
     * Refer to {@link OEntityManager#loadEntityList(java.lang.String, java.util.List,
     * java.util.List, java.util.List, com.unitedofoq.fabs.core.security.user.OUser)
     * }
     */
    public List loadEntityList(String entityName, List<String> conditions,
            List<String> fieldExpressions, List<String> sorts, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException;

    /**
     * Refer to {@link OEntityManager#loadEntityList(java.lang.String, java.util.List,
     * java.util.List, java.util.List, int, int, com.unitedofoq.fabs.core.security.user.OUser)
     * }
     */
    public List loadEntityList(String entityName, List<String> conditions,
            List<String> fieldExpressions, List<String> sorts, int startIndex, int maxResult, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException;

    /**
     * Refer to {@link OEntityManager#loadEntityList(java.lang.String, java.util.List,
     * java.util.List, java.util.List, java.util.List, com.unitedofoq.fabs.core.security.user.OUser)
     */
    public List loadEntityList(String entityName, List<Long> dbids, List<String> conditions,
            List<String> fieldExpressions, List<String> sorts, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException;

    /**
     * Refer to {@link OEntityManager#deleteEntity(com.unitedofoq.fabs.core.entitybase.BaseEntity,
     * com.unitedofoq.fabs.core.security.user.OUser) }
     */
    public void activateDeactivateEntity(BaseEntity entity, OUser loggedUser)
            throws UserNotAuthorizedException, FABSException;

    /**
     * Refer to {@link OEntityManager#deleteEntityList(java.util.List,
     * com.unitedofoq.fabs.core.security.user.OUser) }
     */
    public void activateDeactivateEntityList(List<BaseEntity> entities, OUser loggedUser)
            throws Exception, UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException, FABSException;

    /**
     * Refer to {@link OEntityManager#saveEntity(com.unitedofoq.fabs.core.entitybase.BaseEntity,
     * com.unitedofoq.fabs.core.security.user.OUser) }
     */
    public BaseEntity saveEntity(BaseEntity entity, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException,
            OEntityDataValidationException, EntityDataValidationException, FABSException;

    /**
     * Refer to {@link OEntityManager#saveEntityList(java.util.List,
     * com.unitedofoq.fabs.core.security.user.OUser)}
     */
    public List<BaseEntity> saveEntityList(List<BaseEntity> entities, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException,
            OEntityDataValidationException, EntityDataValidationException, FABSException;

    /**
     * Refer to {@link OEntityManager#executeUpdateQuery(java.lang.String,
     * com.unitedofoq.fabs.core.security.user.OUser) }
     */
    public void executeUpdateQuery(String query, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException;

    /**
     * Refer to {@link OEntityManager#executeEntityQuery(java.lang.String,
     * com.unitedofoq.fabs.core.security.user.OUser) }
     */
    public Object executeEntityQuery(String query, OUser loggedUser)
            throws UserNotAuthorizedException, EntityExistsException,
            EntityNotFoundException, NonUniqueResultException, NoResultException,
            OptimisticLockException, RollbackException, TransactionRequiredException;

    /**
     * Refer to {@link OEntityManager#executeEntityListQuery(java.lang.String,
     * com.unitedofoq.fabs.core.security.user.OUser) }
     */
    public List executeEntityListQuery(String query, OUser loggedUser)
            throws UserNotAuthorizedException, EntityExistsException,
            EntityNotFoundException, NonUniqueResultException, NoResultException,
            OptimisticLockException, RollbackException, TransactionRequiredException;

    /**
     * Refer to {@link OEntityManager#getSystemUser(OUser) }
     */
    public OUser getSystemUser(OUser loggedUser);

    /**
     * Refer to {@link OEntityManager#getSystemUser() }
     */
    public List executeEntityListNativeQuery(String query, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException;

    public Object executeEntityNativeQuery(String query, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException;

    public boolean executeEntityUpdateNativeQuery(String query, OUser loggedUser)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException;

    public Field getEntityChildField(BaseEntity parentEnitty, Class childEntityClass);

    /**
     * Refer to {@link OEntityManager#createNumberQuery(java.lang.String, OUser)
     * }
     */
    public Long createNumberQuery(String query, OUser loggedUser);

    /**
     * Refer to {@link OEntityManager#createViewListQuery(java.lang.String,
     * com.unitedofoq.fabs.core.security.user.OUser) }
     */
    public List createViewListQuery(String query, OUser loggedUser);

    /**
     * Refer to {@link OEntityManager#createNumberNamedQuery(java.lang.String, OUser, java.lang.Object[])
     * }
     */
    public Long createNumberNamedQuery(
            String namedQueryName, OUser loggedUser, Object... params);

    /**
     * Refer to {@link OEntityManager#createNamedQuery(java.lang.String, OUser, java.lang.Object[])
     * }
     */
    public Query createNamedQuery(
            String namedQueryName, OUser loggedUser, Object... params);

    /**
     * Refer to {@link OEntityManager#createListNamedQuery(java.lang.String,
     * com.unitedofoq.fabs.core.security.user.OUser, java.lang.Object[]) }
     */
    public List createListNamedQuery(
            String namedQueryName, OUser loggedUser, Object... params)
            throws UserNotAuthorizedException,
            EntityExistsException, EntityNotFoundException, NonUniqueResultException,
            NoResultException, OptimisticLockException, RollbackException, TransactionRequiredException;

    /**
     * Refer to {@link OEntityManager#loadFullEntity(String, List, OUser) }
     */
    public BaseEntity loadFullEntity(String entityName, ArrayList<String> conditions, OUser loggedUser);

    public List<String> getSelfFieldsExpressions(OEntity entity);

    /**
     * {@link OEntityManager#getEM(com.unitedofoq.fabs.core.security.user.OUser)
     * }
     */
    public EntityManager getEM(OUser loggedUser) throws FABSException;

    /**
     * {@link OEntityManager#getUser(java.lang.String, com.unitedofoq.fabs.central.OTenant)}
     */
    public OUser getUser(String loginName, OTenant userTenant);
    
    public OUser getUserEnc(String loginName, OTenant userTenant);

    /**
     * {@link OEntityManager#getUserForLoginName(java.lang.String)}
     */
    public OUser getUserForLoginName(String loginName);

    /**
     * {@link OEntityManager#findEntityByUnique(BaseEntity, OUser) }
     */
    public BaseEntity findEntityByUnique(BaseEntity searchByValues, OUser loggedUser)
            throws FABSException;

    public void deleteEntity(BaseEntity entity, OUser loggedUser)
            throws UserNotAuthorizedException, FABSException;

    public void deleteCorruptedData(BaseEntity entity, OUser loggedUser);

    /**
     * Refer to {@link OEntityManager#closeEM(OUser) }
     */
    public void closeEM(OUser user);

    public OEntity getOEntity(String entityName, OUser loggedUser);

    public void deleteEntityList(List<BaseEntity> entityList, OUser loggedUser);

    /**
     * Refer to
     * {@link OEntityManager#getTotalNumberOfEntityRecords(OEntity, ArrayList<String>,
     * OUser ) }
     */
    public long getTotalNumberOfEntityRecords(OEntity oEntity, ArrayList<String> conditions, OUser loggedUser);

    public List<BaseEntity> saveBatchForImport(List<BaseEntity> entities, OUser loggedUser) throws FABSException;

    public java.util.List executeQueryWithParameters(java.lang.String queryString, com.unitedofoq.fabs.core.security.user.OUser loggedUser, java.lang.String... params);

    public long getTotalNumberOfEntityDTORecords(com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO oEntity, java.util.ArrayList<java.lang.String> conditions, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.entitybase.BaseEntity merge(com.unitedofoq.fabs.core.entitybase.BaseEntity entity, com.unitedofoq.fabs.core.security.user.OUser loggedUser) throws com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException, javax.persistence.EntityExistsException, javax.persistence.EntityNotFoundException, javax.persistence.NonUniqueResultException, javax.persistence.NoResultException, javax.persistence.OptimisticLockException, javax.persistence.RollbackException, javax.persistence.TransactionRequiredException, com.unitedofoq.fabs.core.validation.OEntityDataValidationException, com.unitedofoq.fabs.core.validation.EntityDataValidationException, com.unitedofoq.fabs.core.validation.FABSException;

    public List<String> getUsersLoginNamesForGroup(String groupName, OUser loggedUser);

    public BaseEntityTranslation getEntityTranslation(String entityName, long dbid, OUser loggedUser);
}
