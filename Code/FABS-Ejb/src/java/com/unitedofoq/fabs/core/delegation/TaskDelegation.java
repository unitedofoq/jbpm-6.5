
package com.unitedofoq.fabs.core.delegation;

import javax.persistence.Entity;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;

@Entity
@ParentEntity(fields={"owner"})
public class TaskDelegation extends Delegation {
}
