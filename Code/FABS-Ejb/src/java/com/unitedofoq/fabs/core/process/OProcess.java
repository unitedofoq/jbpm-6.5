package com.unitedofoq.fabs.core.process;

import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.function.JavaFunction;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.module.OModule;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
@VersionControlSpecs(omoduleFieldExpression = "omodule")
public class OProcess extends BaseEntity implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="OModule">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private OModule omodule;

    public String getOmoduleDD() {
        return "OProcess_omodule";
    }

    public OModule getOmodule() {
        return omodule;
    }

    public void setOmodule(OModule omodule) {
        this.omodule = omodule;
    }
// </editor-fold>

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private JavaFunction generateXMLFunction;

    public String getGenerateXMLFunctionDD() {
        return "OProcess_generateXMLFunction";
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ODataType oinputDataType;

    public String getOinputDataTypeDD() {
        return "OProcess_oinputDataType";
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ODataType ooutputDataType;

    public String getOoutputDataTypeDD() {
        return "OProcess_ooutputDataType";
    }

    private String wsdlRelativeURL;

    public String getWsdlRelativeURLDD() {
        return "OProcess_wsdlRelativeURL";
    }

    @Transient
    private String fileName;

    public String getFileNameDD() {
        return "OProcess_fileName";
    }

    @Translatable(translationField = "descriptionTranslated")
    private String description;

    public String getDescriptionTranslatedDD() {
        return "OProcess_description";
    }

    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    @Translation(originalField = "name")
    @Transient
    private String nameTranslated;

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }

    @Translatable(translationField = "nameTranslated")
    private String name;

    public String getNameTranslatedDD() {
        return "OProcess_name";
    }

    @OneToMany(mappedBy = "oprocess")
    private List<OProcessAction> oProcessAction;

    public String getWsdlRelativeURL() {
        return wsdlRelativeURL;
    }

    public void setWsdlRelativeURL(String wsdlRelativeURL) {
        this.wsdlRelativeURL = wsdlRelativeURL;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Translation(originalField = "packageName")
    @Transient
    private String packageNameTranslated;

    public String getPackageNameTranslated() {
        return packageNameTranslated;
    }

    public String getPackageNameTranslatedDD() {
        return "OProcess_packageName";
    }

    public void setPackageNameTranslated(String packageNameTranslated) {
        this.packageNameTranslated = packageNameTranslated;
    }

    @Translatable(translationField = "packageNameTranslated")
    private String packageName;

    public String getPackageName() {
        return packageName;
    }

    public String getPackageNameDD() {
        return "OProcess_packageName";
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public ODataType getOinputDataType() {
        return oinputDataType;
    }

    public void setOinputDataType(ODataType oinputDataType) {
        this.oinputDataType = oinputDataType;
    }

    public ODataType getOoutputDataType() {
        return ooutputDataType;
    }

    public void setOoutputDataType(ODataType ooutputDataType) {
        this.ooutputDataType = ooutputDataType;
    }

    public List<OProcessAction> getOProcessAction() {
        return oProcessAction;
    }

    public void setOProcessAction(List<OProcessAction> oProcessAction) {
        this.oProcessAction = oProcessAction;
    }

    public JavaFunction getGenerateXMLFunction() {
        return generateXMLFunction;
    }

    public void setGenerateXMLFunction(JavaFunction generateXMLFunction) {
        this.generateXMLFunction = generateXMLFunction;
    }

    @ManyToOne
    private ProcessGroup group;

    public ProcessGroup getGroup() {
        return group;
    }

    public String getGroupDD() {
        return "OProcess_group";
    }

    public void setGroup(ProcessGroup group) {
        this.group = group;
    }

    @OneToMany(mappedBy = "oprocess")
    private List<ProcessButton> processButtons;

    public List<ProcessButton> getProcessButtons() {
        return processButtons;
    }

    public void setProcessButtons(List<ProcessButton> processButtons) {
        this.processButtons = processButtons;
    }

}
