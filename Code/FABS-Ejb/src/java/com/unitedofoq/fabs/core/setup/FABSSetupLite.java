/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.setup;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author mibrahim
 */
@Entity
@ReadOnly
@NamedQueries(value={
    @NamedQuery(name="getFABSSetup",
        query = " SELECT    setupLite " +
                " FROM      FABSSetupLite setupLite" +
                " WHERE     setupLite.setupKey = :key")})
public class FABSSetupLite implements Serializable {
    @Id
    @Column(name="skey")
    private String setupKey;
    private String svalue;

    /**
     * @return the setupKey
     */
    public String getSetupKey() {
        return setupKey;
    }

    /**
     * @param setupKey the setupKey to set
     */
    public void setSetupKey(String setupKey) {
        this.setupKey = setupKey;
    }

    /**
     * @return the svalue
     */
    public String getSvalue() {
        return svalue;
    }

    /**
     * @param svalue the svalue to set
     */
    public void setSvalue(String svalue) {
        this.svalue = svalue;
    }
}
