/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.multimedia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.unitedofoq.fabs.core.data.PersonPhoto;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionResult;

import com.unitedofoq.fabs.core.module.ModuleServiceRemote;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nkhalil
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.multimedia.OIMageServiceLocal",
beanInterface = OIMageServiceLocal.class)
public class OIMageService implements OIMageServiceLocal {

    private static ExternalContext externalContext;
    private static String systemURL;
    @EJB
    private EntitySetupServiceRemote entitySetupService;
    @EJB
    private ModuleServiceRemote moduleService;
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    UserMessageServiceRemote userMessageService;
    final static Logger logger = LoggerFactory.getLogger(OIMageService.class);
    
    final static String resourcesCachedPath = File.separator + "resources" + File.separator + "cached";
    
    @Override
    public OFunctionResult saveImage(File file, String fileName, String contentType, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            oem.getEM(systemUser);
            OImage image = new OImage();
            byte[] b = new byte[(int) ((File) file).length()];
            InputStream fileInputStream = new FileInputStream((File) file);//arrayResource.open();
            fileInputStream.read(b);
            //Write file to the hard driver
            File f = new File("c:\\" + fileName);
            f.createNewFile();
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(b);

            //Write the image to store it in DB
            image.setImageType(contentType);
            image.setImage(b);
            image.setName(fileName);

            // Load blank omodule
            OModule omodule = moduleService.loadOModule("Business", loggedUser);
            image.setOmodule(omodule);
            OImage imageFromDB = (OImage) oem.loadEntity("OImage", Collections.singletonList("name = '" + fileName + "'"), null, loggedUser);
            if (imageFromDB != null) {
                imageFromDB.setImage(b);
                imageFromDB.setImageType(contentType);
                oFR.append(entitySetupService.callEntityUpdateAction(imageFromDB, systemUser));
            } else {
                oFR.append(entitySetupService.callEntityCreateAction(image, systemUser));
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            if(null!=file)
            logger.trace("File: {}", file.getAbsolutePath());
            oFR.addError(userMessageService.getUserMessage(
                    "SystemInternalError", loggedUser));
        } finally {
            oem.closeEM(systemUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult savePersonPhoto(File file, String fileName, String contentType,OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            oem.getEM(systemUser);
            OImage image = new OImage();
            byte[] b = new byte[(int) ((File) file).length()];
            InputStream fileInputStream = new FileInputStream((File) file);//arrayResource.open();
            fileInputStream.read(b);
            List returnedValues = new ArrayList();
            returnedValues.add(b);
            oFR.setReturnValues(returnedValues);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            if(null!=file)
            logger.trace("File: {}", file.getAbsolutePath());
            oFR.addError(userMessageService.getUserMessage(
                    "SystemInternalError", loggedUser));
        } finally {
            oem.closeEM(systemUser);
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public String getImagePath(String fileName, OUser loggedUser) {
        logger.debug("Entering");
        try {
            externalContext = FacesContext.getCurrentInstance().getExternalContext();
            Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();
            if (request instanceof HttpServletRequest) {
                systemURL = ((HttpServletRequest) request).getRequestURL().toString();
                systemURL = systemURL.substring(0, systemURL.lastIndexOf("/"));
            }
            String realPath = externalContext.getRealPath("/resources/cached/" + fileName);
            File file = new File(realPath);
            if (file.exists() && file.length() != 0) {
                 if(realPath.contains("/")) {
                     logger.debug("Returning");
                    return realPath.substring(realPath.indexOf("/resources/cached/"));
                }
                else{
                     logger.debug("Returning");
                    return realPath.substring(realPath.indexOf("\\resources\\cached\\")).
                            replaceAll(Matcher.quoteReplacement("\\"), "/");
                }
            }
            OImage image = (OImage) oem.loadEntity(OImage.class.getSimpleName(),
                    Collections.singletonList("name='" + fileName + "'"),
                    null, oem.getSystemUser(loggedUser));
            if (image == null) {
                logger.debug("Returning with empty string");
                return "";
            }
            OutputStream outputStream = new FileOutputStream(realPath);
            outputStream.write(image.getImage());
            outputStream.close();
            file = new File(realPath);
            if (file.exists()) {
                if(realPath.contains("/")) {
                    logger.debug("Returning");
                    return realPath.substring(realPath.indexOf("/resources/cached/"));
                }
                else{
                    logger.debug("Returning");
                    return realPath.substring(realPath.indexOf("\\resources\\cached\\")).
                            replaceAll(Matcher.quoteReplacement("\\"), "/");
                }
            } else {
                logger.warn("File Saving Error. File Name: {}", fileName);
                logger.debug("Returning with empty string");
                return "";
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("File Name: {}", fileName);
            logger.debug("Returning with empty string");
            return "";
        }
    }

    @Override
    public String getPersonImagePath(PersonPhoto personPhoto, OUser loggedUser) {
        logger.debug("Entering");
        String realPath = externalContext.getRealPath("/resources/cached/");
        if (personPhoto == null || personPhoto.getPhotoData() == null
                || personPhoto.getPhotoData().length == 0) {
            String url = systemURL + realPath.substring(realPath.indexOf("/resources/cached")) + "/Vacant.jpeg";
            logger.debug("Returning with: {}",url);
            return url;
        }
        try {
            //ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            File file = new File(realPath + "/" + personPhoto.getName());
            if (file.exists() && file.length() != 0) {
                logger.debug("Returning");
                return systemURL + realPath.substring(realPath.indexOf("/resources/cached")) + "/" + personPhoto.getName();
            }

            OutputStream outputStream = new FileOutputStream(realPath + "/" + personPhoto.getName());
            outputStream.write(personPhoto.getPhotoData());
            outputStream.close();
            file = new File(realPath + "/" + personPhoto.getName());
            if (file.exists()) {
                logger.debug("Returning");
                return systemURL + realPath.substring(realPath.indexOf("/resources/cached")) + "/" + personPhoto.getName();
            } else {
                if(null!=personPhoto)
                logger.warn("File Saving Error. File Name: {}", personPhoto.getName());
                logger.debug("Returning with empty string");
                return "";
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            if(personPhoto!=null)
            logger.trace("File Name: {}", personPhoto.getName());
            logger.debug("Returning with empty string");
            return "";
        }
    }

    @Override
    public String getFilePath(byte[] fileBytes, String fileName, OUser loggedUser) {
        logger.debug("Entering");
        try {
            externalContext = FacesContext.getCurrentInstance().getExternalContext();
            Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();
            String sysSep = File.separator;
            if (request instanceof HttpServletRequest) {
                systemURL = ((HttpServletRequest) request).getRequestURL().toString();
                systemURL = systemURL.substring(0, systemURL.lastIndexOf("/"));
            }
            String realPath = externalContext.getRealPath(sysSep + "resources"+sysSep+"cached"+sysSep) + sysSep +fileName;
            File file = new File(realPath);
            if (file.exists() && file.length() != 0) {
                if(realPath.contains("/")) {
                    logger.debug("Returning");
                    return realPath.substring(realPath.indexOf("/resources/cached/"));
                }
                else{
                    logger.debug("Returning");
                    return realPath.substring(realPath.indexOf("\\resources\\cached\\")).
                            replaceAll(Matcher.quoteReplacement("\\"), "/");
                }
            }
            OutputStream outputStream = new FileOutputStream(realPath);
            outputStream.write(fileBytes);
            outputStream.close();
            file = new File(realPath);
            if (file.exists()) {
                 if(realPath.contains("/")) {
                    logger.debug("Returning");
                     return realPath.substring(realPath.indexOf("/resources/cached/"));
                }
                else{
                    logger.debug("Returning"); 
                    return realPath.substring(realPath.indexOf("\\resources\\cached\\")).
                            replaceAll(Matcher.quoteReplacement("\\"), "/");
                }
            } else {
                logger.warn("File Saving Error. File Name: {}", fileName);
                logger.debug("Returning with empty string");
                return "";
            }
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.trace("File Name: {}", fileName);
            logger.debug("Returning with empty string");
            return "";
        }
    }
}
