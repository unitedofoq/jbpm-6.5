/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.audittrail.messaging.processor.impl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.unitedofoq.fabs.central.EntityManagerWrapperLocal;
import com.unitedofoq.fabs.core.audittrail.exception.InvalidAuditTrailObjectException;
import com.unitedofoq.fabs.core.audittrail.main.valueholder.AuditTrailMessageObject;
import com.unitedofoq.fabs.core.audittrail.messaging.processor.AuditTrailMessageProcessorLocal;
import com.unitedofoq.fabs.core.audittrail.persistant.entity.AuditTrailTransaction;
import com.unitedofoq.fabs.core.audittrail.persistant.entity.AuditTrailTransactionDetail;
import com.unitedofoq.fabs.core.audittrail.persistant.entity.TransactionStatus;
import com.unitedofoq.fabs.core.audittrail.persistant.entity.TransactionType;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.entitysetup.OObjectBriefInfoField;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityDTO;
import com.unitedofoq.fabs.core.i18n.TextTranslationServiceRemote;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.udc.UDCServiceRemote;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author MEA
 */
@Stateless
public class AuditTrailMessageProcessor implements AuditTrailMessageProcessorLocal {

    @EJB
    EntityManagerWrapperLocal entityManagerWrapper;
    @EJB
    OEntityManagerRemote oem;
    @EJB
    TextTranslationServiceRemote translationService;
    @EJB
    DDServiceRemote ddService;
    @EJB
    UDCServiceRemote udcService;
    @EJB
    private EntitySetupServiceRemote ess;
    
    final static Logger logger = LoggerFactory.getLogger(AuditTrailMessageProcessor.class);
    
    final static Long DELETE_UDC = 22665535l;
    final static Long ADD_UDC = 22665533l;
    final static Long UPDATE_UDC =22665534l;
    final static Long ACTIVEINACTIVE_UDC=22665536l;
    final static Long SUCCESS_UDC=22665530l;
    final static Long FAIL_UDC=22665531l;
    
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void process(AuditTrailMessageObject auditTrailMessageObject) throws InvalidAuditTrailObjectException {
        logger.info("Entering");
        BaseEntity oldObject = (BaseEntity) auditTrailMessageObject.getOldObject();
        BaseEntity newObject = (BaseEntity) auditTrailMessageObject.getNewObject();
        OUser oUser = (OUser) auditTrailMessageObject.getoUser();
        
        Class currentObjectClass = newObject.getClass();
        String currentObjectSimpleName = currentObjectClass.getSimpleName();
        BaseEntity parentScreenObject = null;
        String parentScreenFieldName = null;
        try {
            Annotation annotation = currentObjectClass.getAnnotation(Class
                    .forName("com.unitedofoq.fabs.core.entitybase.ParentEntity"));
            if (annotation != null) {
                Method method = annotation.annotationType().getDeclaredMethod("fields");
                if (method != null) {
                    String[] paramValues = (String[]) method.invoke(annotation, (Object[]) null);
                    for (String paramValue : paramValues) {
                        if (currentObjectSimpleName.toLowerCase().startsWith(paramValue)) {
                            parentScreenFieldName = paramValue;
                            Field parentScreenField = currentObjectClass.getDeclaredField(parentScreenFieldName);
                            parentScreenObject = (BaseEntity) newObject.invokeGetter(parentScreenField);
                        }
                    }
                }
            }
            //<editor-fold defaultstate="collapsed" desc="Prepare New Auditing Transaction">
            AuditTrailTransaction auditTrailTransaction = new AuditTrailTransaction();

            OEntity oEntity = (OEntity) oem.loadEntity(OEntity.class.getSimpleName(), auditTrailMessageObject.getoEntity().getDbid(), null, null, oUser);
            OEntity parentScreenOEntity = null;
            if(parentScreenObject != null){
                OEntityDTO pSOEntity = ess.loadOEntityDTOByNameForAuditing(parentScreenObject.getClass().getSimpleName(), oUser);
                parentScreenOEntity = (OEntity) oem.loadEntity(OEntity.class.getSimpleName(), pSOEntity.getDbid(), null, null, oUser);
            }
            
            auditTrailTransaction.setoEntity(oEntity);
            if (oEntity != null) {
                logger.info("Entitydbid: {}", oEntity.getDbid());
            } else {
                logger.info("oentity equals Null");
            }

            if (auditTrailMessageObject.getTransactionType() == TransactionType.ADD) {
                auditTrailTransaction.setEntityDBID(newObject.getDbid());
            } else {
                auditTrailTransaction.setEntityDBID(oldObject.getDbid());
            }

            auditTrailTransaction.setTransactionDate(auditTrailMessageObject.getAuditDate());
            auditTrailTransaction.setUser(oUser);
            
            
            Long typeDBID = (auditTrailMessageObject.getTransactionType().equals(TransactionType.DELETE)) ? DELETE_UDC
                        : (auditTrailMessageObject.getTransactionType().equals(TransactionType.ADD)) ? ADD_UDC 
                        : (auditTrailMessageObject.getTransactionType().equals(TransactionType.UPDATE)) ? UPDATE_UDC 
                        : ACTIVEINACTIVE_UDC;
                    
            auditTrailTransaction.setTransactionType((UDC)oem.loadEntity("UDC", typeDBID,null, null, oUser));

            String masterFieldFieldExpression = "dbid";
            String masterFieldValue;// = "";
            
            String parentScreenMasterFieldExpression = "dbid";
            String parentScreenMasterFieldValue;

            for (OObjectBriefInfoField oObjectBriefInfoField : oEntity.getOObjectBriefInfoField()) {
                if (oObjectBriefInfoField.isMasterField()) {
                    masterFieldFieldExpression = oObjectBriefInfoField.getFieldExpression();
                    break;
                }
            }
            
            masterFieldValue = auditTrailMessageObject.getTransactionType() == TransactionType.DELETE
                    ? BaseEntity.getValueFromEntity(oldObject, masterFieldFieldExpression).toString()
                    : BaseEntity.getValueFromEntity(newObject, masterFieldFieldExpression).toString();
            
            auditTrailTransaction.setIdentifire(masterFieldValue);
            
            if(parentScreenOEntity != null){
                for (OObjectBriefInfoField oObjectBriefInfoField : parentScreenOEntity.getOObjectBriefInfoField()) {
                    if (oObjectBriefInfoField.isMasterField()) {
                        parentScreenMasterFieldExpression = oObjectBriefInfoField.getFieldExpression();
                        break;
                    }
                }
                parentScreenMasterFieldValue = BaseEntity.getValueFromEntity(parentScreenObject, parentScreenMasterFieldExpression).toString();
                auditTrailTransaction.setSecondIdentifier(parentScreenMasterFieldValue);
            }
            
            Class baseEntityCls = newObject.getClass();
                    
            DD dd = ddService.getDD(BaseEntity.getFieldDDName(
                    baseEntityCls, masterFieldFieldExpression), oUser);
            
            auditTrailTransaction.setMasterFiledDD(dd);
            List<String> tables = newObject.getEntityTableName();
            
            if (tables != null && !tables.isEmpty()) {
                String tableNames = StringUtils.join(tables, ',');
                auditTrailTransaction.setTableName(tableNames);
            }

            auditTrailTransaction.setLastMaintDTime(new Date());
            oUser = (oUser.getDbid() == 0) ? oUser.getOnBehalfOf() : oUser;
            auditTrailTransaction.setLastMaintUser(oUser);
            auditTrailTransaction.setUser(oUser);
            
//            Long udcDBID = (auditTrailMessageObject.getResult().equals(TransactionType.DELETE.toString())) ? DELETE_UDC
//                        : (auditTrailMessageObject.getResult().equals(TransactionType.ADD.toString())) ? ADD_UDC 
//                        : (auditTrailMessageObject.getResult().equals(TransactionType.UPDATE.toString())) ? UPDATE_UDC 
//                        : ACTIVEINACTIVE_UDC;
            
            Long statusDBID = (auditTrailMessageObject.getResult().equals(TransactionStatus.Succeeded.toString())) ? SUCCESS_UDC
                        :  FAIL_UDC; 
            
            auditTrailTransaction.setTransactionStatus((UDC)oem.loadEntity("UDC", statusDBID,null, null, oUser));
            
            entityManagerWrapper.persist(auditTrailTransaction, oUser);
            entityManagerWrapper.flush(oUser);

            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="Get Entity Fields to be Audited">
            Class entityClass;
            entityClass = auditTrailMessageObject.getTransactionType() == TransactionType.ADD
                    ? newObject.getClass() : oldObject.getClass();

            Hashtable<String, List<Field>> tableFields = BaseEntity.
                    getClassFieldsInSameTable(entityClass);
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Loop Over Entity Fields for Auditing">
            for (Map.Entry<String, List<Field>> entry : tableFields.entrySet()) {
                List<Field> fields = entry.getValue();

                for (Field field : fields) {
                    if (auditTrailMessageObject.getTransactionType() == TransactionType.ADD) {
                        auditNewField(auditTrailTransaction, field, newObject, oUser);
                    } else if (auditTrailMessageObject.getTransactionType() == TransactionType.DELETE) {
                        auditDeleteField(auditTrailTransaction, field, oldObject, oUser);
                    } else {
                        auditUpdateField(auditTrailTransaction, field, newObject, oldObject, oUser);
                    }
                }
            }
            //</editor-fold>

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            throw new InvalidAuditTrailObjectException();
            }
        logger.debug("Returning");
    }

    public void auditNewField(AuditTrailTransaction auditTrailTransaction, Field field, BaseEntity newObject, OUser oUser) {
        logger.debug("Entering");
        try {

            String fieldValue = getFieldvalue(field, newObject, oUser);

            if (fieldValue == null) {
                return;
            }

            doAudit(newObject,auditTrailTransaction, field.getName(), null, fieldValue, oUser);

        } catch (Exception e) {
            logger.error("Exception thrown", e);
        }
        logger.debug("Returning");
    }

    public void auditDeleteField(AuditTrailTransaction auditTrailTransaction, Field field, BaseEntity oldObject, OUser oUser) {
        logger.debug("Entering");
        try {

            String fieldValue = getFieldvalue(field, oldObject, oUser);

            if (fieldValue == null) {
                return;
            }

            doAudit(oldObject, auditTrailTransaction, field.getName(), fieldValue, null, oUser);

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
    }

    public void auditUpdateField(AuditTrailTransaction auditTrailTransaction, Field field, BaseEntity newObject, BaseEntity oldObject, OUser oUser) {
        logger.debug("Entering");
        try {

            String fieldOldValue = getFieldvalue(field, oldObject, oUser);
            String fieldNewValue = getFieldvalue(field, newObject, oUser);

            if (fieldNewValue == null && fieldOldValue == null) {
                logger.debug("Returning");
                return;
            }

            if (fieldOldValue != null && fieldOldValue.equals(fieldNewValue)) {
                logger.debug("Returning");
                return;
            }

            //check for fieldOldValue of format OModule[dbid=1800000000, InstanceID=1276]
            //is not equal to fieldNewValue     OModule[dbid=1800000000, InstanceID=3468] with different InstanceID
            if (fieldOldValue != null
                    && fieldOldValue.contains("[")
                    && fieldOldValue.contains("]")
                    && fieldNewValue != null
                    && fieldNewValue.contains("[")
                    && fieldNewValue.contains("]")
                    && (fieldOldValue.subSequence(fieldOldValue.indexOf("=") + 1, fieldOldValue.indexOf(","))
                    .equals(fieldNewValue.subSequence(fieldNewValue.indexOf("=") + 1, fieldNewValue.indexOf(","))))) {
                logger.debug("Returning from: auditUpdateField");
                return;
            }

            doAudit(newObject, auditTrailTransaction, field.getName(), fieldOldValue, fieldNewValue, oUser);
        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.debug("Returning");
    }

    public void doAudit(BaseEntity object , AuditTrailTransaction auditTrailTransaction, String fieldName, String fieldOldValue, String fieldNewValue, OUser oUser) {
        
        try {
            
            if(fieldName.equalsIgnoreCase("lastMaintDTime") ||  fieldName.equalsIgnoreCase("lastMaintUser")) {
                return ;
            }
            
            AuditTrailTransactionDetail auditTrailTransactionDetail = new AuditTrailTransactionDetail();
            auditTrailTransactionDetail.setAuditTrailTransaction(auditTrailTransaction);
            auditTrailTransactionDetail.setFieldExpression(fieldName);
            auditTrailTransactionDetail.setLastMaintDTime(new Date());
            auditTrailTransactionDetail.setLastMaintUser(oUser);

            Class baseEntityCls = object.getClass();
            DD dd = ddService.getDD(BaseEntity.getFieldDDName(
                    baseEntityCls, fieldName), oUser);

            auditTrailTransactionDetail.setFieldDD(dd);
            if (fieldNewValue != null) {
                if (fieldNewValue.contains(",")) {
                    fieldNewValue = fieldNewValue.substring(fieldNewValue.lastIndexOf(",")+1, fieldNewValue.length()-1).trim().replace("]", "");
                }
            }
            if (fieldOldValue != null) {
                if (fieldOldValue.contains(",")) {
                    fieldOldValue = fieldOldValue.substring(fieldOldValue.lastIndexOf(",")+1, fieldOldValue.length()-1).trim().replace("]", "");
                }
            }
            auditTrailTransactionDetail.setNewValue(fieldNewValue);
            auditTrailTransactionDetail.setOldValue(fieldOldValue);

            entityManagerWrapper.persist(auditTrailTransactionDetail, oUser);
            entityManagerWrapper.flush(oUser);

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
        }
        logger.info("Returning");
    }

    public String getFieldvalue(Field field, BaseEntity object, OUser oUser) {
        logger.debug("Entering");
        try {

            String fieldValue = null;

            if (!field.getType().isPrimitive() && field.getType().getName().contains("unitedofoq")) {
                fieldValue = getValueFromChildEntity(field, object, oUser);
            }

            if (fieldValue == null) {
                fieldValue = object.invokeGetter(field) == null ? null : object.invokeGetter(field).toString();
            }
            logger.info("Returning from: getFieldvalue");
            return checkValueIsNotWithZeroDBID(fieldValue);

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning");
            return null;
        }

    }

    public String getValueFromChildEntity(Field field, BaseEntity parentObject, OUser oUser) {
        logger.debug("Entering");
        try {
            BaseEntity childEntity = (BaseEntity) parentObject.invokeGetter(field);

            if (childEntity == null) {
                logger.debug("Returning");
                return null;
            }

            String entityName = childEntity.getClassName();
            OEntity oEntityType
                    = (OEntity) oem.loadEntity(OEntity.class.getSimpleName(), Collections.singletonList("entityClassPath like '%" + entityName + "'"), null, oUser);
            String masterFieldFieldExpression = "";
            for (OObjectBriefInfoField oObjectBriefInfoField
                    : oEntityType
                    != null ? oEntityType.getOObjectBriefInfoField()
                    : new ArrayList<OObjectBriefInfoField>()) {
                if (oObjectBriefInfoField.isMasterField()) {
                    masterFieldFieldExpression = oObjectBriefInfoField.getFieldExpression();
                    break;
                }
            }

            if (masterFieldFieldExpression.contains("Translated")) {
                translationService.loadEntityTranslation(childEntity, oUser);
            }

            String value = BaseEntity.getValueFromEntity(childEntity, masterFieldFieldExpression).toString();
            logger.debug("Returning");
            return value;

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning");
            return null;
        }
    }

    public String checkValueIsNotWithZeroDBID(String value) {
        logger.debug("Entering: checkValueIsNotWithZeroDBID");
        if (value != null
                && value.contains("[")
                && value.contains("]")
                && value.subSequence(value.indexOf("=") + 1, value.indexOf(",")).equals("0")) {
            value = null;
        }
        logger.debug("Returning");
        return value;
    }
}