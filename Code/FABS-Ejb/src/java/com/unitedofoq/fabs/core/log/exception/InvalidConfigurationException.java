/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.log.exception;

import com.unitedofoq.fabs.core.validation.FABSException;

/**
 *
 * @author mostafa
 */
public class InvalidConfigurationException extends FABSException 
{

    public InvalidConfigurationException() {
        super(null);
    }
    
}
