/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.fabs.core.uiframework;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author melsayed
 */
@Local
public interface UIFrameworkValidationServiceRemote {
    public OFunctionResult validateUDC(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
}
