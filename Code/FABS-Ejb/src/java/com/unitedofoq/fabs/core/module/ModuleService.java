/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.module;

import java.util.Collections;
import java.util.Hashtable;


import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author aelzaher
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.module.ModuleServiceRemote",
        beanInterface = ModuleServiceRemote.class)
public class ModuleService implements ModuleServiceRemote {

    @EJB
    OEntityManagerRemote oem;

    @EJB
    UserMessageServiceRemote usrMsgService;
    final static Logger logger = LoggerFactory.getLogger(ModuleService.class);

    //<editor-fold defaultstate="collapsed" desc="Cache">
    public static Hashtable<String, OModule> cachedOModule = new Hashtable<String, OModule>();

    /**
     * Clears {@link #cachedOModule}
     */
    @Override
    public OFunctionResult clearOModuleCache(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            cachedOModule.clear();
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Module Cache Cleared");
            // </editor-fold>
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult onOModuleCachedUpdate(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            OModule module = (OModule) odm.getData().get(0);
            cachedOModule.put(loggedUser.getTenant().getId() + "_" + module.getModuleTitle(), module);
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Module Updated In Cache. Module: {}", module);
            // </editor-fold>
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult preOModuleCachedUpdate(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            OModule module = (OModule) odm.getData().get(0);
            String oldTitle = ((OModule) oem.loadEntity("OModule", module.getDbid(), null, null, loggedUser)).getModuleTitle();
            if (!oldTitle.equals(module.getModuleTitle())) {
                cachedOModule.remove(loggedUser.getTenant().getId() + "_" + oldTitle);
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("Module Removed From Cache. Module: {}", module);
                // </editor-fold>
            }
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    @Override
    public OFunctionResult onOModuleCachedRemove(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        logger.debug("Entering");
        OFunctionResult oFR = new OFunctionResult();
        try {
            OModule module = (OModule) odm.getData().get(0);
            cachedOModule.remove(loggedUser.getTenant().getId() + "_" + module.getModuleTitle());
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("Module Removed From Cache. Module: {}", module);
            // </editor-fold>
        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.error("Exception thrown", ex);
            // </editor-fold>
            oFR.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            logger.debug("Returning");
            return oFR;
        }
    }

    //</editor-fold>
    @Override
    public OModule loadOModule(String name, OUser loggedUser) {
        logger.debug("Entering");
        OModule module = cachedOModule.get(loggedUser.getTenant().getId() + "_" + name);
        if (module == null) {
            try {
                module = (OModule) oem.loadEntity("OModule",
                        Collections.singletonList("moduleTitle = '" + name + "'"),
                        Collections.singletonList("dbidGeneration"), oem.getSystemUser(loggedUser));
            } catch (Exception ex) {
                logger.error("Exception thrown", ex);
            } finally {
            }
            if (module != null) {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("Module Not Found In Cache & Loaded. Module: {}", module);
                // </editor-fold>
                cachedOModule.put(loggedUser.getTenant().getId() + "_" + name, module);
            } else {
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.warn("OModule Not Found. Module: {}", module);
                // </editor-fold>
            }
        } else {
            // <editor-fold defaultstate="collapsed" desc="Log">
            logger.warn("OModule Not Found. Module: {}", module);
            // </editor-fold>
        }
        logger.debug("Returning");
        return module;
    }

}
