/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.portal;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author mibrahim
 */
@Entity
@ReadOnly
@NamedQueries(value={
 @NamedQuery(name="getPortletReceiversList",
        query = " SELECT    portalPagePortletReceiverLite " +
                " FROM      PortalPagePortletReceiverLite portalPagePortletReceiverLite" +
                " WHERE     portalPagePortletReceiverLite.senderDBID = :senderDBID" )})
public class PortalPagePortletReceiverLite implements Serializable {
    private long senderDBID;
    @Id
    private String receiverScreenName;

    /**
     * @return the senderDBID
     */
    public long getSenderDBID() {
        return senderDBID;
    }

    /**
     * @param senderDBID the senderDBID to set
     */
    public void setSenderDBID(long senderDBID) {
        this.senderDBID = senderDBID;
    }

    /**
     * @return the receiverScreenName
     */
    public String getReceiverScreenName() {
        return receiverScreenName;
    }

    /**
     * @param receiverScreenName the receiverScreenName to set
     */
    public void setReceiverScreenName(String receiverScreenName) {
        this.receiverScreenName = receiverScreenName;
    }
    
}
