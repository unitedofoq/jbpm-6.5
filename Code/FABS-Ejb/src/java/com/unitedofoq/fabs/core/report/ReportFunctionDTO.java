/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.report;

import java.util.List;

import com.unitedofoq.fabs.core.filter.FilterFieldDTO;

/**
 *
 * @author MEA
 */
public class ReportFunctionDTO {
    private long dbid;
    private String name;
    private long reportDBID;
    private String reportName;
    private long reportFilterDBDI;
    private boolean reportFilterInActive;
    private List<FilterFieldDTO> reportFilterFilelds;
    private boolean reportFilterAlwayesShow;
    private boolean reportFilterShowInsideScreen;
    
    private long customFilterScreenDBID;
    private long customFilterScreenName;
    private long customFilterScreenHeader;
    private long customFilterScreenViewPage;
}
