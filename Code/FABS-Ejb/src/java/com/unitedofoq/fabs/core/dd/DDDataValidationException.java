package com.unitedofoq.fabs.core.dd;

import com.unitedofoq.fabs.central.EntityManagerWrapper;
import com.unitedofoq.fabs.core.validation.OEntityDataValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DDDataValidationException extends OEntityDataValidationException {

    final static Logger logger = LoggerFactory.getLogger(DDDataValidationException.class);
    private DD dd;

    public DDDataValidationException(DD dd) {
        this.dd = dd;
    }

    /**
     * @return the dd
     */
    public DD getDd() {
        return dd;
    }

    /**
     * @param dd the dd to set
     */
    public void setDd(DD dd) {
        this.dd = dd;
    }

    @Override
    public String getMessage() {
        message = "DD Data Validation Exception [dbid=" + dd.getDbid() + "]: \n";
        logger.trace("Entering");
        if (dd.getControlType() == null) {
            logger.warn("DD Control Type is null");
            message += "\tControl Type is NULL.";
            logger.info("Returning: getMessage in DD Data Validation Exception");
            logger.trace("Returning with: {}", message);
            return message;
        } else if (dd.getControlType().getDbid() == DD.CT_LOOKUPSCREEN
                || dd.getControlType().getDbid() == DD.CT_LOOKUPMULTILEVEL
                || dd.getControlType().getDbid() == DD.CT_DROPDOWN
                || dd.getControlType().getDbid() == DD.CT_LITERALDROPDOWN
                || dd.getControlType().getDbid() == DD.CT_LOOKUPATTRIBUTEBROWSER) {
            message += "\tControl Type is 'LOOKUP', ";
        } else {
            message += "\tControl Type is not 'LOOKUP', ";
        }

        message += "'lookupDropDownDisplayField' = " + dd.getLookupDropDownDisplayField() + ", " + "'lookupScreen' = " + dd.getLookupScreen() + "\n";

        //  if it is a lookup control...
        if (dd.getControlType().getDbid() == DD.CT_LOOKUPSCREEN
                || dd.getControlType().getDbid() == DD.CT_LOOKUPMULTILEVEL
                || dd.getControlType().getDbid() == DD.CT_DROPDOWN
                || dd.getControlType().getDbid() == DD.CT_LITERALDROPDOWN) {
            //  'lookupDropDownDisplayField' and 'lookupScreen' cannot both be
            //  NULL and they cannot both have values...

            //  if both are NULL...
            if (dd.getLookupDropDownDisplayField() == null && dd.getLookupScreen() == null) {
                logger.warn("DD LookupDropDownDisplayField and LookupScreen is null");
                message += "\t'lookupDropDownDisplayField' and 'lookupScreen' cannot both be NULL.";
            } //  if both have values...
            else if (dd.getLookupDropDownDisplayField() != null
                    && !"".equals(dd.getLookupDropDownDisplayField())
                    && dd.getLookupScreen() != null) {
                logger.warn("DD LookupDropDownDisplayField and LookupScreen and LookupDropDownDisplayField is null");
                message += "\t'lookupDropDownDisplayField' and 'lookupScreen' cannot both have values.";
            }
        } //  if it is NOT a lookup control...
        else {
            //  None of the following fields should have values in them:
            //  'lookupDropDownDisplayField', 'lookupScreen' & 'lookupFilter'
            if (dd.getLookupDropDownDisplayField() != null || dd.getLookupScreen() != null || dd.getLookupFilter() != null) {
                message += "\tNone of the following fields should have values in them: " + "'lookupDropDownDisplayField', 'lookupScreen' & 'lookupFilter'";
            } else {
                message += "\tValue of the 'lookupDropDownDisplayField' field is invalid.";
            }
        }
        logger.trace("Returning with: {}", message);
        return message;
    }
}
