/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.security;

import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ExpressionFieldInfo;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntityAction;
import com.unitedofoq.fabs.core.entitysetup.OEntityField;
import com.unitedofoq.fabs.core.entitysetup.dto.OEntityActionDTO;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.privilege.OPrivilege;
import com.unitedofoq.fabs.core.security.privilege.PrivilegeException;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserEntityAccessPriv;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.security.user.UserPriv;
import com.unitedofoq.fabs.core.security.user.UserPrivilege.ConstantUserPrivilege;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenField;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ahussien
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.core.security.SecurityServiceRemote",
        beanInterface = SecurityServiceRemote.class)
public class SecurityService implements SecurityServiceRemote {

    final static org.slf4j.Logger logger = LoggerFactory.getLogger(SecurityService.class);
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private OCentralEntityManagerRemote centralEM;
    @EJB
    private EntitySetupServiceRemote entitySetupService;

    /**
     * Return without exceptions for any of the following cases:
     * <br>1. checkedUser {@link OCentralEntityManagerRemote#isSystemUser(OUser)
     * }
     * <
     * br>2. Found at least one {@link UserPriv} record with
     * {@link UserPriv#granted} = 1
     *
     * @param oPrivilege
     * @param checkedUser
     * @param loggedUser
     * @throws UserNotAuthorizedException
     * <br>. checkedUser is null
     * <br>. Exception
     * <br>. No {@link UserPriv} records found, in this case,
     * {@link UserNotAuthorizedException#forNA} is true
     * <br>. No {@link UserPriv} records found with {@link UserPriv#granted} = 1
     *
     * @throws PrivilegeException
     */
    @Override
    public void checkPrivilegeAuthorithy(OPrivilege oPrivilege, OUser checkedUser, OUser loggedUser)
            throws UserNotAuthorizedException, PrivilegeException {
    }

    @Override
    public boolean checkPrivilegeAuthority(ConstantUserPrivilege privilege, OUser loggedUser) {
        try {
            checkPrivilegeAuthorithy(privilege.toString(), loggedUser, loggedUser);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * Calls {@link #checkPrivilegeAuthorithy(OPrivilege, OUser)} for privilege
     * got by {@link #getPrivilege(OUser) }
     */
    @Override
    public void checkPrivilegeAuthorithy(String privilegeName, OUser checkedUser, OUser loggedUser)
            throws UserNotAuthorizedException, PrivilegeException {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>

        if (checkedUser == null) {
            logger.debug("Invalid - Null User");
            throw new UserNotAuthorizedException("Invalid - Null User");
        }

        if (centralEM.isSystemUser(checkedUser)) {
            logger.debug("Returning with Authorized user");
            return; // Authorized
        }
        String query = null;
        List<UserPriv> userPrivs = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Log">
            Long timeBeforeQuery = (Calendar.getInstance()).getTimeInMillis();
            // </editor-fold>
            query = " SELECT  entity "
                    + " FROM    UserPriv entity "
                    + " WHERE   entity.usrDBID = " + checkedUser.getDbid()
                    + "   AND   entity.oPrivName = '" + privilegeName + "'";
            userPrivs = oem.createViewListQuery(query, loggedUser);
            logger.trace("Query: {}", query);

        } catch (Exception ex) {
            logger.error("exception thrown: ", ex);
            UserNotAuthorizedException unaex = new UserNotAuthorizedException("For Exception");
            unaex.setOUser(checkedUser);
        }
        if (userPrivs.isEmpty()) {
            // No user privilge records found
            UserNotAuthorizedException ex = new UserNotAuthorizedException();
            ex.setForNA(true);
            ex.setOUser(checkedUser);
            logger.debug("User Preiviledges is Empty");
            // Make sure this block is not enclosed in try catch inside this function
            throw ex;
        }
        // One or more user privilge records found
        for (UserPriv userPriv : userPrivs) {
            if (userPriv.isGranted()) {
                // User is granted
                logger.debug("Returning with Granted user");
                return;    // No exception thrown
            }
        }

        // User not authorized, throw exception
        UserNotAuthorizedException ex = new UserNotAuthorizedException();
        ex.setOUser(checkedUser);
        throw ex;
    }

    /**
     * Check the Entity Access Privilege Authority for the user checkedUser.
     * Returns successfully without exception in the following cases:
     * <br>. User is systemUser
     * <br>. checkedUser is granted on the oentity or one of the oentity parents
     * privileges
     *
     * @param entity
     * @param loggedUser
     *
     * @throws UserNotAuthorizedException In the following cases:
     * <br>. Null value passed
     * <br>. User is revoked or has no authority record; and has no grant at
     * all. In case of no authority record found, then,
     * {@link UserNotAuthorizedException#forNA} is true.
     * <br>. Exception, mentioned in the exception thrown
     */
    @Override
    public void checkEntityAccessAuthority(OEntity oentity, OUser checkedUser, OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.debug("Entering");
        if (checkedUser == null) {
            logger.debug("Invalid - Null User");
            throw new UserNotAuthorizedException("Invalid - Null User");
        }

        if (centralEM.isSystemUser(checkedUser)) // System User, authorised on all
        {
            logger.debug("Returning");
            return;
        }

        List<UserEntityAccessPriv> userEAPrivs = null;
        String query = "";
        try {
            String entityObjectClassPath = oentity.getEntityClassPath();
            List<String> checkedEntityClassPaths = new ArrayList<String>();

            // Include the entity class path
            checkedEntityClassPaths.add(entityObjectClassPath);

            // Include the entity parents class paths
            OFunctionResult tempFR = entitySetupService.getParentsFromReference(
                    entityObjectClassPath, oem.getSystemUser(loggedUser));
            checkedEntityClassPaths.addAll(tempFR.getReturnValues());
            String whereIn = "(";
            for (int i = 0; i < checkedEntityClassPaths.size(); i++) {
                if (i > 0) {
                    whereIn += ", ";
                }
                whereIn += "'" + checkedEntityClassPaths.get(i) + "' ";
            }
            whereIn += ")";
            query = " SELECT    entity "
                    + " FROM      UserEntityAccessPriv entity"
                    + " WHERE     entity.ouserDBID = " + checkedUser.getDbid()
                    + "       AND entity.entityClassPath in " + whereIn;
            logger.trace("Query: {}", query);
            userEAPrivs = oem.createViewListQuery(query, oem.getSystemUser(loggedUser));
            if (userEAPrivs == null) {
                logger.trace("User Priviledges equals Null");
                UserNotAuthorizedException unaex = new UserNotAuthorizedException("For Error");
                unaex.setOUser(checkedUser);
                unaex.setUnauthOnEntity(oentity);
                throw unaex;
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            UserNotAuthorizedException unaex = new UserNotAuthorizedException("For Exception");
            unaex.setOUser(checkedUser);
            unaex.setUnauthOnEntity(oentity);
            throw unaex;
        }
        if (!userEAPrivs.isEmpty()) {
            // One or more user privilge records found
            for (UserEntityAccessPriv userEAPriv : userEAPrivs) {
                if (userEAPriv.isGranted()) // User is granted
                {
                    logger.debug("Returning as Granted User");
                    return;    // No exception thrown
                }
            }
        }
        // User either: 1) has one or more privilege records, but not granted, throw exception
        // or 2) has no privilege records, throw exception
        UserNotAuthorizedException unaex = new UserNotAuthorizedException();
        if (userEAPrivs.isEmpty()) // User has no privilege records at all
        {
            unaex.setForNA(true);
        }
        unaex.setOUser(checkedUser);
        unaex.setUnauthOnEntity(oentity);
        throw unaex;
    }

    /**
     * Returns successfully without exception in the following cases:
     * <br>. User is systemUser, return true
     * <br>. No Action Privilege found, return false
     * <br>. User is granted on the action privilege, return true
     * <br>
     * <br><I>It doesn't check the action entity privilege</I>
     *
     * @param action
     * @param checkedUser
     * @param loggedUser
     *
     * @throws UserNotAuthorizedException In the following cases:
     * <br>. Null User
     * <br>. User Is not authorized, will have all information from
     * {@link #checkPrivilegeAuthorithy(OPrivilege, OUser, OUser) } threw it.
     * Action has privilege, and user has no authority record for it or is
     * revoked
     * <br>. Exception
     *
     * @return true: authorized for being granted on the action privilege
     * <br>false: authorized for the action has no privilege (i.e. public)
     *
     * @throws PrivilegeException If {@link #checkPrivilegeAuthorithy(OPrivilege, OUser, OUser)
     * } threw it
     */
    @Override
    public boolean checkActionOnlyAuthority(OEntityAction action, OUser checkedUser, OUser loggedUser)
            throws UserNotAuthorizedException, PrivilegeException {
        logger.debug("Returning");
        // _____________
        // Validate User
        //
        if (checkedUser == null) {
            logger.debug("Invalid - Null User");
            throw new UserNotAuthorizedException("Invalid - Null User");
        }

        if (centralEM.isSystemUser(checkedUser)) {
            logger.debug("Returning with Null");
            return true;
        }

        OPrivilege actionPrivilege = action.getEntityActionPrivilege();

        // Validate Privilege existence
        if (actionPrivilege == null) {
            // Action Has No Privilege
            // Action is public, return and don't throw exception --> Authorized
            logger.debug("Returning as Action Has No Privilege");
            return false;
        }

        // Privilege is found
        // Check Action Privilege Authority
        try {
            checkPrivilegeAuthorithy(actionPrivilege, checkedUser, loggedUser);
            logger.debug("Returning");
            return true;
        } catch (Exception ex) {
            if (ex.getClass() == UserNotAuthorizedException.class) {
                // User is not authorized on privilege
                // Set additional information in the exception and throw it
                UserNotAuthorizedException unaex = new UserNotAuthorizedException(
                        (UserNotAuthorizedException) ex);
                unaex.setUnauthOnAction(action);
                unaex.setOUser(checkedUser);
                throw unaex;
            } else if (ex.getClass() == PrivilegeException.class) {
                // Privilege exception
                throw new PrivilegeException((PrivilegeException) ex);
            }
            // Any other exception
            logger.error("Exception thrown: ", ex);
            logger.trace("User Not Authorized");

            UserNotAuthorizedException unaex = new UserNotAuthorizedException("For Exception");
            unaex.setUnauthOnAction(action);
            unaex.setOUser(checkedUser);
            throw unaex;
        }
    }

    @Override
    public boolean checkActionOnlyAuthority(OEntityActionDTO action, OUser checkedUser, OUser loggedUser)
            throws UserNotAuthorizedException, PrivilegeException {
        logger.debug("Entering");
        // _____________
        // Validate User
        //
        if (checkedUser == null) {
            logger.debug("Invalid - Null User");
            throw new UserNotAuthorizedException("Invalid - Null User");
        }

        if (centralEM.isSystemUser(checkedUser)) {
            logger.debug("Returning");
            return true;
        }

        OPrivilege actionPrivilege = (OPrivilege) oem.executeEntityQuery(
                "select entity from OPrivilege entity where entity.dbid in ("
                + "select other.entityActionPrivilege.dbid from OEntityAction other where other.dbid ="
                + action.getDbid() + "L)", oem.getSystemUser(loggedUser));

        // Validate Privilege existence
        if (actionPrivilege == null) {
            // Action Has No Privilege
            // Action is public, return and don't throw exception --> Authorized
            logger.debug("Returning as Action Has No Privilege");
            return true;
        }

        // Privilege is found
        // Check Action Privilege Authority
        try {
            checkPrivilegeAuthorithy(actionPrivilege.getName(), checkedUser, loggedUser);
            logger.debug("Returning with True");
            return true;
        } catch (Exception ex) {
            if (ex.getClass() == UserNotAuthorizedException.class) {
                // User is not authorized on privilege
                // Set additional information in the exception and throw it
                UserNotAuthorizedException unaex = new UserNotAuthorizedException(
                        (UserNotAuthorizedException) ex);
//                unaex.setUnauthOnAction(action);
                unaex.setOUser(checkedUser);
                throw unaex;
            } else if (ex.getClass() == PrivilegeException.class) {
                // Privilege exception
                throw new PrivilegeException((PrivilegeException) ex);
            }
            // Any other exception
            logger.error("exception thrown: ", ex);

            logger.trace("User Not Authorized");

            UserNotAuthorizedException unaex = new UserNotAuthorizedException("For Exception");
            unaex.setOUser(checkedUser);
            throw unaex;
        }
    }

    /**
     * Returns successfully without exception in ANY of the following cases:
     * <br>. checkedUser is
     * {@link OCentralEntityManagerRemote#isSystemUser(OUser)}
     * <br>. No action found in the first place, by default granted. Happens in
     * "Retrieve" actions
     *
     * Otherwise, it calls
     * {@link #checkActionEntityAuthority(OEntityAction, OUser, OUser)}
     *
     * @throws UserNotAuthorizedException
     * <br>. checkedUser is null
     * <br>. thrown by
     * {@link #checkActionEntityAuthority(OEntityAction, OUser, OUser)}
     */
    @Override
    public void checkActionEntityAuthority(String entityName, Long actionTypeDBID, OUser checkedUser, OUser loggedUser)
            throws UserNotAuthorizedException {
        if (checkedUser == null) {

            logger.debug("Invalid - Null User");
            throw new UserNotAuthorizedException("Invalid - Null User");
        }

        if (centralEM.isSystemUser(loggedUser)) {
            logger.debug("Returning as Authorized");
            return; // Authorized
        }
        OEntity oentity = entitySetupService.
                loadOEntityByClassName(entityName, oem.getSystemUser(loggedUser));

        if (oentity == null) {
            logger.error("SEVERE: Entity {} is not found in entity manager !!!!", entityName);
        }

        if (oentity != null) {
            OEntityAction entityAction = oentity.getAction(actionTypeDBID);
            // Check action existence of the actionType, return if not found
            if (entityAction == null) {
                // No action found in the first place, by default granted
                logger.debug("Returning as No action found in the first place, by default granted");
                return;
            }

            checkActionEntityAuthority(entityAction, checkedUser, loggedUser);
        }
    }

    /**
     * Returns successfully without exception in ANY of the following cases:
     * <br>. checkedUser is
     * {@link OCentralEntityManagerRemote#isSystemUser(OUser)}
     * <br>. No action found in the first place, by default granted. Happens in
     * "Retrieve" actions
     * <br>. User is GRANTED on the action (alone), not including entity check,
     * and not because action has no privilege
     * <br>. User is granted on the action {@link OEntity} or one of its
     * parents, and action has no privilege
     *
     * @param entityName
     * @param actionTypeDBID
     * @param loggedUser
     * @throws UserNotAuthorizedException
     * <br>. checkedUser is null
     * <br>. Action has privilege, and User is REVOKED on it (alone), not
     * including entity check
     * <br>. Found authority record on entity (including parents) but not
     * granted, and No action privilege found
     * <br>. No authority record on entity (including parents) & no action
     * privilege found
     * <br>. Exception
     */
    @Override
    public void checkActionEntityAuthority(OEntityAction entityAction, OUser checkedUser, OUser loggedUser)
            throws UserNotAuthorizedException {
        logger.debug("Entering");
        // <editor-fold defaultstate="collapsed" desc="Log">
        Long entryTime = (Calendar.getInstance()).getTimeInMillis();
        // </editor-fold>

        if (checkedUser == null) {
            logger.debug("Invalid - Null User");
            // </editor-fold>
            throw new UserNotAuthorizedException("Invalid - Null User");
        }

        if (centralEM.isSystemUser(checkedUser)) {
            logger.debug("Returning as Authorized");
            return; // Authorized
        }
        // Check user is GRANTED on Action, return if so
        try {
            if (checkActionOnlyAuthority(entityAction, checkedUser, loggedUser)) {
                // User is GRANTED on the action (alone), not including entity check,
                // and not because action has no privilegece
                // <editor-fold defaultstate="collapsed" desc="Log">
                logger.trace(" as User is GRANTED on the action (alone), not including entity check,and not because action has no privilegece");
                logger.debug("Returning");
                return;
            }
            // User can access the action for action has no privilege
            // Don't return, should check OEntity privielge

        } catch (UserNotAuthorizedException ex) {
            // User is not authorized on the action
            if (!ex.isForNA()) {
                // User is revoked on the action privilege = has access record
                // with granted = false
                UserNotAuthorizedException throwunaex = new UserNotAuthorizedException(ex);
                logger.error("exception thrown: ", ex);
                throw throwunaex;
            }
            // User has no access record on the action privilege
            // Check entity access control

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            UserNotAuthorizedException throwunaex = new UserNotAuthorizedException("For Exception");
            throwunaex.setOUser(checkedUser);
            throwunaex.setUnauthOnAction(entityAction);
            throw throwunaex;
        }
        // User can access the action for action has no privilege

        // Check user is GRANTED on OEntity parents chain
        try {
            checkEntityAccessAuthority(entityAction.getOownerEntity(), checkedUser, loggedUser);
            // User is granted on the oentity or one of its parents
            // Just continue
        } catch (UserNotAuthorizedException ex) {
            // User is not authorized on the entity including parents chain
            // Or could be caught from 'User is not authorized on action' above
            // User is not authorized on the action
            UserNotAuthorizedException throwunaex = new UserNotAuthorizedException(ex);
            logger.error("exception thrown: ", ex);
            throw throwunaex;

        } catch (Exception ex) {
            logger.error("exception thrown: ", ex);
            UserNotAuthorizedException throwunaex = new UserNotAuthorizedException("For Exception");
            throwunaex.setOUser(checkedUser);
            throwunaex.setUnauthOnEntity(entityAction.getOownerEntity());
            throwunaex.setUnauthOnAction(entityAction);
            throw throwunaex;
        }
        // If no exceptions thown, then he is authorized

        logger.debug("No Exceptions Found, user is Authorized");
        logger.debug("Returning");
    }

    /**
     *
     * @param oentityDBID
     * @param loggedUser
     * @return
     */
    @Override
    public String getEntityFieldsTagMsg(Class entityClass,
            List<ScreenField> screenFields, OUser loggedUser) {
        logger.debug("Entering");
        try {
            int level = 0;
            String msg = null;
            for (ScreenField screenField : screenFields) {
                List<ExpressionFieldInfo> infos
                        = BaseEntity.parseFieldExpression(entityClass,
                                screenField.getFieldExpression());
                ExpressionFieldInfo efi = infos.get(infos.size() - 1);
                OEntity entity = oem.getOEntity(efi.fieldClass.getSimpleName(),
                        loggedUser);
                List<String> conditions = new ArrayList<String>();
                conditions.add("oentity.dbid = " + entity.getDbid());
                conditions.add("fieldExpression = '" + efi.fieldName + "'");
                OEntityField oEntityField = (OEntityField) oem.loadEntity(
                        OEntityField.class.getSimpleName(), conditions,
                        Collections.singletonList("securityTag.valueTranslated"), loggedUser);
                if (null != oEntityField
                        && null != oEntityField.getSecurityTag()) {
                    String code
                            = oEntityField.getSecurityTag().getCode();
                    if (null != code) {
                        int tempLevel = Integer.parseInt(code.substring(1));
                        if (tempLevel > level) {
                            level = tempLevel;
                            msg = oEntityField.getSecurityTag().getValueTranslated();
                        }
                    }
                }
            }
            logger.debug("Returning");
            return msg;

        } catch (Exception ex) {
            logger.error("Exception thrown: ", ex);
            return null;
        }

    }
}
