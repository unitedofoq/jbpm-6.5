/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.filter;

/**
 *
 * @author aelzaher
 */
public class FilterDDRequiredException extends Exception {
    
    private String fieldName;
    
    public FilterDDRequiredException(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public String toString() {
        return "Field " + fieldName + " must have a Data Dictionary.";
    }

    /**
     * @return the fieldName
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * @param fieldName the fieldName to set
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
    
}
