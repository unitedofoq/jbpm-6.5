package ws;

import javax.ejb.Remote;

/**
 * Created by Hamada.
 *
 *
 * This remote interface should be implemented by any application that require additional setup after adding/importing
 * OUser From active directory, the implemented Ejb class should defined with the following @EJB annotation
 * @EJB(name = "java:global/ofoq/ws.ActiveDirectoryRemote"
 *
 */
@Remote
public interface ADBusinessManager {
    /*
    * This function is called after AD Hook add/import user to FABS
    * */
    public void mapBusinessUser(long userDBID, String loginName);
}
