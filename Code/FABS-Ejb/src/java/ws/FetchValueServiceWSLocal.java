/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import javax.ejb.Remote;

/**
 *
 * @author efikry
 */
@Remote
public interface FetchValueServiceWSLocal {

    public String fetchValueFromDB(String dbid, String entityName, String fieldExpression, String loginName);

}
