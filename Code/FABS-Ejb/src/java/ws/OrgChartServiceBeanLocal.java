/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import java.util.List;

import javax.ejb.Local;

import com.unitedofoq.fabs.core.entitysetup.OEntityAction;

/**
 *
 * @author lap3
 */
@Local
public interface OrgChartServiceBeanLocal {
    public List<OEntityAction> getEmployeeEntityActions(String userLoginName);
    public List<OEntityAction> getPositionEntityActions(String userLoginName);
    public List<OEntityAction> getUnitEntityActions(String userLoginName);
    public List getOrgChartDDs(String userLoginName);
    public List runActionForEmployee(String userLoginName, String actionDBID, String entityDBID);
}
