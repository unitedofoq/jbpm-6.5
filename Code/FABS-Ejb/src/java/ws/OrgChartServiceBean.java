/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntityAction;
import com.unitedofoq.fabs.core.function.JavaFunction;
import com.unitedofoq.fabs.core.function.OFunction;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.function.URLFunction;
import com.unitedofoq.fabs.core.i18n.TextTranslationServiceRemote;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkEntityManager;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.portal.PortalPageFunction;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenFunction;
import com.unitedofoq.fabs.core.uiframework.screen.UDCScreenFunction;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author lap3
 */
@Stateless
public class OrgChartServiceBean implements OrgChartServiceBeanLocal {
final static Logger logger = LoggerFactory.getLogger(OrgChartServiceBean.class);
    @EJB
    UIFrameworkServiceRemote uIFrameworkService;
    @EJB
    OEntityManagerRemote oem;
    @EJB
    protected UserMessageServiceRemote usrMsgService;
    @EJB
    protected DataTypeServiceRemote dataTypeService;
    @EJB
    protected OFunctionServiceRemote functionServiceRemote;
    @EJB
    TextTranslationServiceRemote textTranslationService;
    
    public List getOrgChartDDs(String userLoginName) {
        OUser loggedUser = oem.getUserForLoginName(userLoginName);
        List returnedList = new ArrayList();
        try {
            // mobile DD
            DD orgMobileDD = (DD) oem.loadEntity(DD.class.getSimpleName(),
                    Collections.singletonList("dbid=" + 1820005121), null, loggedUser);
            returnedList.add(orgMobileDD.getLabelTranslated());

            // phone DD
            DD orgPhoneDD = (DD) oem.loadEntity(DD.class.getSimpleName(),
                    Collections.singletonList("dbid=" + 1820005122), null, loggedUser);
            returnedList.add(orgPhoneDD.getLabelTranslated());

            // email DD
            DD orgEmailDD = (DD) oem.loadEntity(DD.class.getSimpleName(),
                    Collections.singletonList("dbid=" + 1820005123), null, loggedUser);
            returnedList.add(orgEmailDD.getLabelTranslated());

            // contact DD
            DD orgContactDD = (DD) oem.loadEntity(DD.class.getSimpleName(),
                    Collections.singletonList("dbid=" + 1820005124), null, loggedUser);
            returnedList.add(orgContactDD.getLabelTranslated());

            //employee menu DD
            DD orgEmployeeMenuDD = (DD) oem.loadEntity(DD.class.getSimpleName(),
                    Collections.singletonList("dbid=" + 1820005125), null, loggedUser);
            returnedList.add(orgEmployeeMenuDD.getLabelTranslated());
            
            //position menu DD
            DD orgPositionMenuDD = (DD) oem.loadEntity(DD.class.getSimpleName(),
                    Collections.singletonList("dbid=" + 1820005126), null, loggedUser);
            returnedList.add(orgPositionMenuDD.getLabelTranslated());
            
            //Employee Code DD
            DD empCodeDD = (DD) oem.loadEntity(DD.class.getSimpleName(),
                    Collections.singletonList("dbid=" + 850059841), null, loggedUser);
            returnedList.add(empCodeDD.getLabelTranslated());

            //Unit Name DD
            DD unitNameDD = (DD) oem.loadEntity(DD.class.getSimpleName(),
                    Collections.singletonList("dbid=" + 850059810), null, loggedUser);
            returnedList.add(unitNameDD.getLabelTranslated());


            return returnedList;
        } catch (Exception e) {
        }
        return null;
    }

    public List getEmployeeEntityActions(String userLoginName) {
        //OUser loggedUser = getLoggedUser(userLoginName);
        OUser loggedUser = oem.getUserForLoginName(userLoginName);
        List returnedList = new ArrayList();
        try {
            String entityClassPath = "com.unitedofoq.otms.foundation.employee.Employee";
            OEntity employeeOentity = (OEntity) oem.loadEntity("OEntity",
                    Collections.singletonList("entityClassPath = '" + entityClassPath + "'"), null, loggedUser);
            List<OEntityAction> employeeActions = uIFrameworkService.getDisplayedActions(employeeOentity, loggedUser);
            if (employeeActions != null && !employeeActions.isEmpty()) {
                for (OEntityAction action : employeeActions) {
                    returnedList.add(action.getDbid());
                    returnedList.add(action.getNameTranslated());
                }
            }
            return returnedList;
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        }
        return null;
    }

    public List<OEntityAction> getPositionEntityActions(String userLoginName) {
        OUser loggedUser = oem.getUserForLoginName(userLoginName);
        List returnedList = new ArrayList();
        try {
            String entityClassPath = "com.unitedofoq.otms.foundation.position.Position";
            OEntity positionOentity = (OEntity) oem.loadEntity("OEntity",
                    Collections.singletonList("entityClassPath = '" + entityClassPath + "'"), null, loggedUser);
            List<OEntityAction> positionActions = uIFrameworkService.getDisplayedActions(positionOentity, loggedUser);
            if (positionActions != null && !positionActions.isEmpty()) {
                for (OEntityAction action : positionActions) {
                    returnedList.add(action.getDbid());
                    returnedList.add(action.getNameTranslated());
                }
            }
            return returnedList;
        } catch (Exception e) {
        }
        return null;
    }

    /**
     *
     * @param userLoginName
     * @return the list of actions for Unit, to be displayed on orgchart
     */
    @Override
    public List<OEntityAction> getUnitEntityActions(String userLoginName) {
        OUser loggedUser = oem.getUserForLoginName(userLoginName);
        List returnedList = new ArrayList();
        try {
            String entityClassPath = "com.unitedofoq.otms.foundation.unit.Unit";
            OEntity unitOentity = (OEntity) oem.loadEntity("OEntity",
                    Collections.singletonList("entityClassPath = '" + entityClassPath + "'"), null, loggedUser);
            List<OEntityAction> unitActions = uIFrameworkService.getDisplayedActions(unitOentity, loggedUser);
            if (unitActions != null && !unitActions.isEmpty()) {
                for (OEntityAction action : unitActions) {
                    returnedList.add(action.getDbid());
                    returnedList.add(action.getNameTranslated());
                }
            }
            return returnedList;
        } catch (Exception e) {
        }
        return null;
    }

    public List runActionForEmployee(String userLoginName, String actionDBID, String entityDBID) {
        OUser loggedUser = oem.getUserForLoginName(userLoginName);
        List<String> returnedList = new ArrayList<String>();
        try {
            OEntityAction action = (OEntityAction) oem.loadEntity(OEntityAction.class.getSimpleName(),
                    Collections.singletonList("dbid = " + actionDBID), null, loggedUser);
            OFunction actionFn = action.getOfunction();
            BaseEntity entity = oem.loadEntity("Employee",
                    Collections.singletonList("dbid = " + entityDBID), null, loggedUser);
            if (actionFn instanceof ScreenFunction) {
                returnedList.add("ScreenFn");
                returnedList.add(((ScreenFunction) actionFn).getOscreen().getName());//screen name
                returnedList.add(((ScreenFunction) actionFn).getOscreen().getName() + "_" + entityDBID);//screen instid
                returnedList.add(((ScreenFunction) actionFn).getOscreen().getViewPage()); // view page
                OScreen screen = ((ScreenFunction) actionFn).getOscreen();
                textTranslationService.loadEntityTranslation(screen, loggedUser);
                returnedList.add(screen.getHeaderTranslated()); // title
                returnedList.add(String.valueOf(((ScreenFunction) actionFn).getDbid())); // functiondbid
                return returnedList;
            } else if (actionFn instanceof UDCScreenFunction) {
                return returnedList;
            } else if (actionFn instanceof JavaFunction) {
                // <editor-fold defaultstate="collapsed" desc="run JavaFunction">
                returnedList.add("JavaFn");
                ODataType entityDT = dataTypeService.loadODataType(entity.getClass().getSimpleName(), loggedUser);
                ODataMessage odm = new ODataMessage();
                List data = new ArrayList();
                data.add(entity);
                data.add(loggedUser);
                odm.setODataType(entityDT);
                odm.setData(data);
                functionServiceRemote.runFunction(actionFn, odm, new OFunctionParms(), loggedUser);
                return returnedList;
                // </editor-fold>
            } else if (actionFn instanceof URLFunction) {
                returnedList.add("URLFn");
                ODataMessage odm = new ODataMessage();
                functionServiceRemote.runFunction(actionFn, odm, new OFunctionParms(), loggedUser);
                return returnedList;
            } else if (actionFn instanceof PortalPageFunction) {
                returnedList.add("PortalPagFn");
                returnedList.add(String.valueOf(actionFn.getDbid())); // functiondbid
                // get page url
                String pageHeader = ((PortalPageFunction)actionFn).getPortalPage().getHeader();
                Random rand = new Random();
//                int num = rand.nextInt(10);
                String pageUrl = "/"+ (pageHeader.toLowerCase()).replaceAll(" ", "-");//+ num;
                returnedList.add(pageUrl);
                return returnedList;
            }
        } catch (Exception ex) {
            logger.error("Exception thrown: ",ex);
        }
        return null;
    }
}
