/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author efikry
 */
@Stateless
public class FetchValueServiceWS implements FetchValueServiceWSLocal {

    final static Logger logger = LoggerFactory.getLogger(FetchValueServiceWS.class);
    OUser loggedUser = null;
    @EJB
    private OEntityManagerRemote oem;

    @Override
    public String fetchValueFromDB(String dbid, String entityName, String fieldExpression, String loginName) {
        logger.debug("Entering");
        String value;
        if (dbid == null || dbid.equals("") || entityName == null || entityName.equals("") || fieldExpression == null || fieldExpression.equals("")) {
            return "-1";
        }
        try {
            loggedUser = oem.getUserForLoginName(loginName);

            if (fieldExpression.contains(".")) {
                logger.warn("Multilevel Exps. Not Supported Now");
                logger.debug("Returning");
                return "-1";
            }

            if (loggedUser == null) {
                logger.debug("Returning");
                return "-1";
            }

            BaseEntity entity = oem.loadEntity(entityName, Long.parseLong(dbid), null, null, loggedUser);
            value = ((BaseEntity) entity).invokeGetter(fieldExpression).toString();

        } catch (Exception ex) {
            logger.error("Exception thrown", ex);
            logger.debug("Returning");
            return "-1";
        }
        logger.debug("Returning");
        return value;
    }

}
