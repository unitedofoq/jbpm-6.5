/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.unitedofoq.fabs.core.entitysetup.OEntityAction;

/**
 *
 * @author lap3
 */
@WebService(serviceName = "OrgChartWebService",targetNamespace="http://OrgChartWebService.unitedofoq.com/")
@Stateless()
public class OrgChartWebService {
    @EJB
    private OrgChartServiceBeanLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "getEmployeeEntityActions")
    public List<OEntityAction> getEmployeeEntityActions(@WebParam(name = "userLoginName") String userLoginName) {
        return ejbRef.getEmployeeEntityActions(userLoginName);
    }

    @WebMethod(operationName = "getPositionEntityActions")
    public List<OEntityAction> getPositionEntityActions(@WebParam(name = "userLoginName") String userLoginName) {
        return ejbRef.getPositionEntityActions(userLoginName);
    }

    @WebMethod(operationName = "getUnitEntityActions")
    public List<OEntityAction> getUnitEntityActions(@WebParam(name = "userLoginName") String userLoginName) {
        return ejbRef.getUnitEntityActions(userLoginName);
    }

    @WebMethod(operationName = "getOrgChartDDs")
    public List getOrgChartDDs(@WebParam(name = "userLoginName") String userLoginName) {
        return ejbRef.getOrgChartDDs(userLoginName);
    }

    @WebMethod(operationName = "runActionForEmployee")
    public List runActionForEmployee(@WebParam(name = "userLoginName") String userLoginName, @WebParam(name = "actionDBID") String actionDBID, @WebParam(name = "entityDBID") String entityDBID) {
        return ejbRef.runActionForEmployee(userLoginName, actionDBID, entityDBID);
    }
    
}
