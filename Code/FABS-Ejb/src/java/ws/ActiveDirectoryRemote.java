package ws;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.util.List;
import javax.ejb.Remote;

@Remote
public interface ActiveDirectoryRemote {

    public void addUpdateCentUser(String userId, long tenantDBID, String eligibleTenants);

    public void executeUpdate(String query);

    public List loadEntityList(String entityName, List<String> conditions);

    public BaseEntity loadEntity(String entityName, List<String> conditions);

    public List executeEntityListNativeQuery(String query);

    public Object executeEntityNativeQuery(String query);

    public void executeUpdateQuery(String query);

    public boolean executeEntityUpdateNativeQuery(String query);

    public Long generateDBID(String tableName);

    public String getTenant(String UserName);
}
