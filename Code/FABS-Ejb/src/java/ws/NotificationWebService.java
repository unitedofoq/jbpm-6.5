/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.security.user.Notification;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.screen.ScreenFunction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author mahmed
 */
@WebService(serviceName = "NotificationWebService")
@Stateless()
public class NotificationWebService {

    private static final org.apache.logging.log4j.Logger LOG = LogManager.getLogger(NotificationWebService.class);

    @EJB
    private OEntityManagerRemote oem;

    @WebMethod(operationName = "addNotification")
    public String addNotification(@WebParam(name = "sender") String from, @WebParam(name = "reciever") String to, @WebParam(name = "description") String message, @WebParam(name = "function") String function, @WebParam(name = "dbid") String dbid, @WebParam(name = "entityName") String entityName) {
        LOG.debug("Entering...");
        try {
            OUser loggedUser = oem.getUserForLoginName(from);
            //Adding record in notification table...
            Notification notification = new Notification();
            notification.setSender(loggedUser.getDisplayName());
            notification.setReciever(to);
            notification.setMessage(message);
            notification.setSendDate(new Date());
            //get screen name from screenfunction code
            ScreenFunction screenFunction = (ScreenFunction) oem.executeEntityQuery("SELECT sf FROM ScreenFunction sf WHERE sf.code = \'"+function+"\'", loggedUser);
            String functionName = screenFunction.getOscreen().getName();
            notification.setScreenName(functionName);
            notification.setEntitydbid(Long.parseLong(dbid));
            notification.setEntityName(entityName);
            oem.saveEntity(notification, loggedUser);
            return "1";
        } catch (Exception ex) {
            LOG.error("Exception thrown", ex);
            return "-1";
        }

    }

}
