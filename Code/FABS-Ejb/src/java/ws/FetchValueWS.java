/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;

/**
 *
 * @author efikry
 */
@WebService(serviceName = "FetchValueWS")
@Stateless()
public class FetchValueWS {

    @EJB
    private FetchValueServiceWSLocal fetchValueWSRef;

    @WebMethod(operationName = "fetchValueFromDB")
    public String fetchValueFromDB(@WebParam(name = "dbid") String dbid, @WebParam(name = "entityName") String entityName, @WebParam(name = "fieldExpression") String fieldExpression, @WebParam(name = "loginName") String loginName) {
        return fetchValueWSRef.fetchValueFromDB(dbid, entityName, fieldExpression, loginName);
    }
}
