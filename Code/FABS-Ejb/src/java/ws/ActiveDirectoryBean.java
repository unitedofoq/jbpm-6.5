package ws;

import com.unitedofoq.fabs.central.OCentralEntityManagerRemote;
import com.unitedofoq.fabs.central.OTenant;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitybase.dbidservice.DBIDCacheServiceLocal;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateful;

@Stateful
@EJB(name = "java:global/ofoq/ws.ActiveDirectoryRemote", beanInterface = ActiveDirectoryRemote.class)
public class ActiveDirectoryBean implements ActiveDirectoryRemote {

    private static Logger logger = Logger.getLogger(ActiveDirectoryBean.class.getName());
    @EJB
    private OEntityManagerRemote oEM;
    @EJB
    private OCentralEntityManagerRemote oCentEM;
    private OUser loggedUser = null;
    private OTenant loginNameTenant = null;
    private OUser tenantUser = null;
    @EJB
    private DBIDCacheServiceLocal dbidService;

    @Override
    public void addUpdateCentUser(String userId, long tenantDBID, String eligibleTenants) {
        try {
            // Get tenant Name
            loginNameTenant = oCentEM.getUserTenant(userId);
            if (loginNameTenant == null) { // if no Tenant found, create User on tenantId
                logger.log(Level.INFO,
                        "::: No Tenant Found for User, Add User to Tenant !!" + tenantDBID);
                try {
                    // add the new user to tenantId
                    loginNameTenant = oCentEM.getUserTenant(tenantDBID);
                    tenantUser = new OUser();
                    tenantUser.setLoginName(userId);
                    UDC englishLang = new UDC();
                    englishLang.setDbid(OUser.ARABIC);
                    tenantUser.setRtl(true);
                    tenantUser.setFirstLanguage(englishLang);
                    tenantUser.getFirstLanguage().setDbid(OUser.ARABIC);
                    tenantUser.setTenant(loginNameTenant);
                    oCentEM.addOCentralUser(tenantUser);
                    logger.log(Level.INFO,
                            "::: User Added to Tenant 1 Successfully!!");
                } catch (Exception e) {
                    logger.log(Level.SEVERE,
                            "::: Exception Ceating centeral User", e);
                }
            } else { // Get OTenant Granted User
                tenantUser = oCentEM.getGrantedUser(loginNameTenant);
            }
            // update user default tenant, and eligible tenants
            oCentEM.updateOCentralUserTenant(userId, tenantDBID, eligibleTenants);
            // Get EM for the user tenant
            oEM.getEM(tenantUser);
            loggedUser = tenantUser;
        } catch (Exception ex) {
            logger.log(Level.SEVERE,
                    "::: Exception thrown while loading/creating user", ex);
            return;
        }

    }

    @Override
    public void executeUpdate(String query) {
        try {
            logger.log(Level.INFO, "Executing Query", query);
            oEM.executeUpdateQuery(query, loggedUser);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Exception execute UpdateQuery!!", e);
        }
    }

    @Override
    public List loadEntityList(String entityName, List<String> conditions) {
        List retList = null;
        try {
            logger.log(Level.INFO, "Loading EntityList", entityName);
            retList = oEM.loadEntityList(entityName, conditions, null, null, loggedUser);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Exception loading EntityList " + entityName, e);
        }
        return retList;
    }

    @Override
    public BaseEntity loadEntity(String entityName, List<String> conditions) {
        BaseEntity entity = null;
        try {
            logger.log(Level.INFO, "LoadEntity ", entityName);
            entity = oEM.loadEntity(entityName, conditions, null, loggedUser);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Exception Loading Entity " + entityName, e);
        }
        return entity;
    }

    @Override
    public List executeEntityListNativeQuery(String query) {
        List entityList = null;
        try {
            logger.log(Level.INFO, "executeEntityListNativeQuery", query);
            entityList = oEM.executeEntityListNativeQuery(query, loggedUser);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Exception execute UpdateQuery!!", e);
        }
        return entityList;
    }

    @Override
    public Object executeEntityNativeQuery(String query) {
        Object retObj = null;
        try {
            logger.log(Level.INFO, "Executing Query", query);
            retObj = oEM.executeEntityNativeQuery(query, loggedUser);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Exception execute UpdateQuery!!", e);
        }
        return retObj;
    }

    @Override
    public void executeUpdateQuery(String query) {
        try {
            logger.log(Level.INFO, "Executing Query", query);
            oEM.executeUpdateQuery(query, loggedUser);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Exception UpdateQuery!!", e);
        }

    }

    @Override
    public boolean executeEntityUpdateNativeQuery(String query) {
        boolean result = false;
        try {
            logger.log(Level.INFO, "executeEntityUpdateNativeQuery", query);
            result = oEM.executeEntityUpdateNativeQuery(query, loggedUser);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Exception executeEntityUpdateNativeQuery!!", e);
        }
        return result;
    }

    @Override
    public Long generateDBID(String tableName) {
        return dbidService.generateNewDbid(tableName, true, loggedUser);
    }

    @Override
    public String getTenant(String UserName) {
        return oCentEM.getUserTanantName(UserName);
    }

}
