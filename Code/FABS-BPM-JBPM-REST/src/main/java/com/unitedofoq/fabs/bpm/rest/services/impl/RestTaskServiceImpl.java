package com.unitedofoq.fabs.bpm.rest.services.impl;

import com.unitedofoq.fabs.bpm.common.model.TasksRequestData;
import java.util.List;
import java.util.Map;
import com.unitedofoq.fabs.bpm.model.Task;
import com.unitedofoq.fabs.bpm.model.status.TaskStatus;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.unitedofoq.fabs.bpm.services.TaskService;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.json.simple.JSONArray;
import java.util.HashMap;
import java.util.Iterator;
import org.json.simple.JSONObject;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.bpm.services.impl.RestTaskService", beanInterface = TaskService.class)
public class RestTaskServiceImpl extends AbstractBPMService implements TaskService {

    @Override
    public Map<Long, List<Task>> getTasksAssignedAsPotentialOwner(TasksRequestData data) {
        loadProperties();
        String url = propertiesUtil.getValue("task.get");
        url = MessageFormat.format(url, host, port);
        String entity = converter.convertFromObjectToJson(data);
        JSONObject response = restConnector.sendPostRequest(url, null, 
                userName, password, entity);
        JSONArray jsonArray = (JSONArray) response.get("data");
        Long totalElements = (Long) response.get("totalElements");
        List<Task> tasks =  mapJsonArrayToTaskList(jsonArray);
        Map<Long, List<Task>> TasksWithInfo = new HashMap<>();
        TasksWithInfo.put(totalElements, tasks);
        return TasksWithInfo;
    }
    
    @Override
    public List<Task> getTasksOwned(String userId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void claim(long taskId, String userId, String password) {
    	loadProperties();
        String uri = MessageFormat.format(propertiesUtil.getValue("task.claimTask"), propertiesUtil.getValue("engine.host"), propertiesUtil.getValue("engine.port"), String.valueOf(taskId));
        boolean isAdminUser = false;
        String userName = userId;
        String userPassword = password;
        if(userPassword == null){
            isAdminUser = true;
            userName = this.userName;
            userPassword = this.password;
        }
        restConnector.sendPostRequest(uri, null, userName, userPassword, null);
        if(isAdminUser){
            updateTaskActualOwner(userId, taskId);
        }
        //Validate Response (Success if status code is 200)
    }

    @Override
    public void complete(long taskId, String userId, String password, Map<String, Object> data) {
    	loadProperties();
    	String uri = MessageFormat.format(propertiesUtil.getValue("task.complete"), propertiesUtil.getValue("engine.host"), propertiesUtil.getValue("engine.port"), String.valueOf(taskId), "");
        Map<String, String> mappedData = new HashMap<String, String>();
		if (data != null) {
			for (String strKey : data.keySet()) {
				mappedData.put(strKey,String.valueOf(data.get(strKey)));
			}
		}
        boolean isAdminUser = false;
        String userName = userId;
        String userPassword = password;
        if(userPassword == null){
            isAdminUser = true;
            userName = this.userName;
            userPassword = this.password;
            updateTaskActualOwner(userName, taskId);
        }
        restConnector.sendPostRequest(uri, mappedData, userName, userPassword, null);
        //Validate Response (Success if status code is 200)
        if(isAdminUser){
            updateTaskActualOwner(userId, taskId);
        }
    }

    @Override
    public void delegate(long taskId,String userId, String delegateUserId) {
        // TODO Auto-generated method stub
    	loadProperties();
        String url = propertiesUtil.getValue("task.delegate");
        url = MessageFormat.format(url, host, port,String.valueOf(taskId), delegateUserId);
        JSONObject response = restConnector.sendPostRequest(url, null, userName, password, null);

    }

    @Override
    public void release(long taskId, String userId) {
        // TODO Auto-generated method stub

    }
    
    @Override
    public List<Task> getTasksOwnedByStatus(String userId, List<TaskStatus> statuses) {
        // TODO Auto-generated method stub
        return null;
    } 

    @Override
    public TaskStatus getTaskStatusById(long taskId) {
    	loadProperties();
        String uri = MessageFormat.format(propertiesUtil.getValue("task.getTask"), propertiesUtil.getValue("engine.host"), propertiesUtil.getValue("engine.port"), String.valueOf(taskId));
        JSONObject response = restConnector.sendGetRequest(uri, null, propertiesUtil.getValue("engine.userName"), propertiesUtil.getValue("engine.password"));
        Map<String, String> taskData = (Map<String, String>) response.get("taskData");
        String taskStatus = taskData.get("status");
        taskStatus = taskStatus.toUpperCase();
        TaskStatus status = TaskStatus.valueOf(taskStatus);
        return status;
    }

    @Override
    public void start(long taskId, String userId, String password) {
    	loadProperties();
        boolean isAdminUser = false;
        String userName = userId;
        String userPassword = password;
        if(userPassword == null){
            isAdminUser = true;
            userName = this.userName;
            userPassword = this.password;
            updateTaskActualOwner(userName, taskId);
        }
        String uri = MessageFormat.format(propertiesUtil.getValue("task.startTask"), propertiesUtil.getValue("engine.host"), propertiesUtil.getValue("engine.port"), String.valueOf(taskId));
        restConnector.sendPostRequest(uri, null, userName, userPassword, null);
        if(isAdminUser){
            updateTaskActualOwner(userId, taskId);
        }

    }

    @Override
    public Task getTaskById(long taskId) {
        loadProperties();
        String uri = MessageFormat.format(propertiesUtil.getValue("task.getTask"), propertiesUtil.getValue("engine.host"), propertiesUtil.getValue("engine.port"), String.valueOf(taskId));
        JSONObject response = restConnector.sendGetRequest(uri, null, propertiesUtil.getValue("engine.userName"), propertiesUtil.getValue("engine.password"));
        return mapOneJsonObjectToOneTask(response);
    }

    private List<Task> mapJsonArrayToTaskList(JSONArray jsonArray) {
        List<Task> tasks = new ArrayList<>();
        for (Iterator it = jsonArray.iterator(); it.hasNext();) {
            Object obj = it.next();
            JSONObject json = (JSONObject) obj;
                Task task = mapJsonObjectToTask(json);
                tasks.add(task);
        }
        return tasks;
    }

    private Task mapJsonObjectToTask(JSONObject json) {
        Task task = new Task();
        Long activationTime = (Long) json.get("activationTime");
        if (activationTime != null) {
            task.setActivationTime(new Date(activationTime));
        }
        task.setActualOwnerId((String) json.get("actualOwnerId"));
        String dateString = String.valueOf(json.get("createdOn"));
        if (dateString.contains(":")) {
            task.setCreationTime(dateString);
        } else {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
            Long longDate = Long.valueOf(dateString);
            Date date = new Date(longDate);
            task.setCreationTime(simpleDateFormat.format(date));
        }
        task.setCreatorId((String) json.get("createdByid"));
        task.setDelegated(false);
        task.setDescription((String) json.get("description"));
        Long expirationTime = (Long) json.get("expirationTime");
        if (expirationTime != null) {
            task.setExpirationTime(new Date(expirationTime));
        }
        task.setId((long) json.get("id"));
        task.setName((String) json.get("name"));
        task.setPeriority(Integer.parseInt(Long.toString((long) json.get("priority"))));
        task.setProcessId((String) json.get("processId"));
        task.setProcessInstanceId((long) json.get("processInstanceId"));
        String taskStatus = (String) json.get("status");
        switch (taskStatus) {
            case "Created":
                task.setStatus(TaskStatus.CREATED);
                break;
            case "Ready":
                task.setStatus(TaskStatus.READY);
                break;
            case "Reserved":
                task.setStatus(TaskStatus.RESERVED);
                break;
            case "InProgress":
                task.setStatus(TaskStatus.INPROGRESS);
                break;
            case "Suspended":
                task.setStatus(TaskStatus.SUSPENDED);
                break;
            case "Completed":
                task.setStatus(TaskStatus.COMPLETED);
                break;
            case "Failed":
                task.setStatus(TaskStatus.FAILED);
                break;
            case "Error":
                task.setStatus(TaskStatus.ERROR);
                break;
            case "Exited":
                task.setStatus(TaskStatus.EXITED);
                break;
            case "Obsolete":
                task.setStatus(TaskStatus.OBSOLETE);
                break;
        }
        task.setSubject((String) json.get("subject"));
        Map<String, Object> inputs = (Map<String, Object>) json.get("parameters");
        task.setInputs(inputs);
        return task;
    }
    
    private Task mapOneJsonObjectToOneTask(JSONObject json) {
        Task task = new Task();
        Map<String, Object> taskData = (Map<String, Object>) json.get("taskData");
        Long activationTime = (Long) taskData.get("activationTime");
        if (activationTime != null) {
            task.setActivationTime(new Date(activationTime));
        }
        task.setActualOwnerId((String) taskData.get("actualOwner"));
        Long longDate = (Long) taskData.get("createdOn");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
        Date date = new Date(longDate);
        task.setCreationTime(simpleDateFormat.format(date));
        task.setCreatorId((String) taskData.get("createdBy"));
        task.setDelegated(false);
        task.setDescription((String) json.get("description"));
        Long expirationTime = (Long) taskData.get("expirationTime");
        if (expirationTime != null) {
            task.setExpirationTime(new Date(expirationTime));
        }
        long id = (long) json.get("id");
        task.setId(id);
        task.setName((String) json.get("name"));
        task.setPeriority(Integer.parseInt(Long.toString((long) json.get("priority"))));
        task.setProcessId((String) taskData.get("processId"));
        task.setProcessInstanceId((Long) taskData.get("processInstanceId"));
        String taskStatus = (String) taskData.get("status");
        switch (taskStatus) {
            case "Created":
                task.setStatus(TaskStatus.CREATED);
                break;
            case "Ready":
                task.setStatus(TaskStatus.READY);
                break;
            case "Reserved":
                task.setStatus(TaskStatus.RESERVED);
                break;
            case "InProgress":
                task.setStatus(TaskStatus.INPROGRESS);
                break;
            case "Suspended":
                task.setStatus(TaskStatus.SUSPENDED);
                break;
            case "Completed":
                task.setStatus(TaskStatus.COMPLETED);
                break;
            case "Failed":
                task.setStatus(TaskStatus.FAILED);
                break;
            case "Error":
                task.setStatus(TaskStatus.ERROR);
                break;
            case "Exited":
                task.setStatus(TaskStatus.EXITED);
                break;
            case "Obsolete":
                task.setStatus(TaskStatus.OBSOLETE);
                break;
        }
        task.setSubject((String) json.get("subject"));
        task.setInputs(getTaskInputs(id));
        return task;
    }

    @Override
    public Map<String, Object> getTaskInputs(long taskId) {
    	loadProperties();
        String url = propertiesUtil.getValue("task.taskInputs");
        url = MessageFormat.format(url, host, port, String.valueOf(taskId));
        JSONObject content = restConnector.sendGetRequest(url, null, userName, password);
        return (Map<String, Object>) content.get("contentMap");
    }
        
    @Override
    public List<Task> getProcessInstanceTasks(String processInstanceId) {
    	loadProperties();
        String url=propertiesUtil.getValue("task.tasksByProcessInstanceID");
        String uri = MessageFormat.format(url,host,port,processInstanceId);
        JSONArray processVariables = new JSONArray();
        JSONObject response = restConnector.sendGetRequest(uri,
                null, userName, password);
        if (response != null) {
            processVariables = (JSONArray) response.get("taskSummaryList");
        }
        List<Task> tasks = mapJsonArrayToTaskList(processVariables);
        List<Task> tasksWithInputs = new ArrayList<Task>();
        for (Task task : tasks) {
            task.setInputs(getTaskInputs(task.getId()));
            tasksWithInputs.add(task);
        }
        return tasksWithInputs;
    }

    @Override
    public List<Task> getTasksAssignedAsPotentialOwner(String userId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Task> getTasksAssignedAsPotentialOwnerByProcessId(String userId, String processId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private void updateTaskActualOwner(String actualOwner, long taskId){
        String uri = MessageFormat.format(propertiesUtil.getValue("task.updateActualOwner"), propertiesUtil.getValue("engine.host"), propertiesUtil.getValue("engine.port"), String.valueOf(taskId), actualOwner);
        restConnector.sendPostRequest(uri, null, userName, password, null);
    }
    
    @Override
    public List<String> getTaskPotentialOwnersOfTypeRole(long taskId){
        String url = propertiesUtil.getValue("task.taskPotentialOwnersOfTypeRole");
        StringBuilder uri = new StringBuilder(MessageFormat.format(url, host, port,String.valueOf(taskId)));
        JSONObject response = restConnector.sendPostRequest(uri.toString(), null, userName, password, null);
        return (List<String>) response.get("Roles");
    }
}
