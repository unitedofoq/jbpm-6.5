/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.bpm.rest.integration;

import com.unitedofoq.fabs.bpm.common.connector.RestConnector;
import org.json.simple.JSONObject;

/**
 *
 * @author mmasoud
 */
public class Test {

    public static void main(String[] args) {
        RestConnector connector = new RestConnector(180 * 1000);
        String url = "http://localhost:8186/retrieveAllUserTasks";
        String userName = "admin";
        String password = "admin";
        String entity = "{\"actorId\":\"emp1\",\"page\":0,\"pageSize\":10,\"delegations\":[]}";
        JSONObject json = connector.sendPostRequest(url, null, userName, password, entity);
        System.out.println("");
    }
}
