/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.bpm.rest.services.impl;

import com.unitedofoq.fabs.bpm.model.Task;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import com.unitedofoq.fabs.bpm.services.DefinitionService;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.json.simple.JSONArray;

import org.json.simple.JSONObject;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.bpm.services.impl.RestDefinitionServiceImpl", beanInterface = DefinitionService.class)
@LocalBean
public class RestDefinitionServiceImpl extends AbstractBPMService implements
		DefinitionService, Runnable {

	RestDefinitionServiceImpl(){
	}
	
	@Override
	public Map<String, String> retrieveProcessVariables(String processDefId) {
		loadProperties();
                Thread thread = new Thread(this);
                thread.start();
		String url=propertiesUtil.getValue("process.definition");
		Map<String, String> processVariables = new HashMap<>(); 
		String uri=MessageFormat.format(url,host,port,deploymentId,processDefId);
		
		JSONObject response = restConnector.sendGetRequest(uri.toString(),
				null,userName,password);

		processVariables = (Map<String, String>) response.get("variables");

		return processVariables;
	}

    @Override
    public List<String> retrieveTaskOutputVariables(Task task) {
        loadProperties();
        while(!isContainerCreated()){
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                Logger.getLogger(RestDefinitionServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        String url = propertiesUtil.getValue("task.taskOutputs");
        String uri = MessageFormat.format(url, host, port, deploymentId, task.getProcessId(), task.getName().replace("-", "%2D"));
        JSONObject response = restConnector.sendGetRequest(uri, null, userName, password);
        Map<String,String> outPutsMap  =   (Map<String,String>) response.get("outputs");
        List<String> taskOutputVariables = new ArrayList<>(outPutsMap.keySet());
        return taskOutputVariables;
    }
    
    private void registerContainer(){
        String url = propertiesUtil.getValue("deployment.container");
        url = MessageFormat.format(url, host, port, deploymentId);
        String[] deploymentIdSections = deploymentId.split("\\:");
        String body = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>")
                .append("\n<kie-container container-id=\"")
                .append(deploymentId)
                .append("\">")
                .append("\n\t<release-id>")
                .append("\n\t\t<group-id>")
                .append(deploymentIdSections[0])
                .append("</group-id>")
                .append("\n\t\t<artifact-id>")
                .append(deploymentIdSections[1])
                .append("</artifact-id>")
                .append("\n\t\t<version>")
                .append(deploymentIdSections[2])
                .append("</version>")
                .append("\n\t</release-id>")
                .append("</kie-container>").toString();
        restConnector.sendPutRequest(url, null, userName, password, body);
    }
    
    private boolean isContainerCreated() {
        String url = propertiesUtil.getValue("deployment.container");
        url = MessageFormat.format(url, host, port, deploymentId);
        JSONObject response = restConnector.sendGetRequest(url, null, userName, password);JSONObject containerInfo = null;
        String status = null;
        if (response != null) {
            containerInfo = (JSONObject) response.get("result");
        }
        if (containerInfo != null) {
            status = (String) ((JSONObject) containerInfo.get("kie-container")).get("status");
        }
        
        if ("STARTED".equalsIgnoreCase(status)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void run() {
        if (!isContainerCreated()) {
            registerContainer();
        }
    }

}
