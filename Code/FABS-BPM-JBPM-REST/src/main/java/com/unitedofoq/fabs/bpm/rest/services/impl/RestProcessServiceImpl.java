package com.unitedofoq.fabs.bpm.rest.services.impl;

import com.unitedofoq.fabs.bpm.common.model.ProcessRequestData;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import com.unitedofoq.fabs.bpm.model.ProcessInstance;
import com.unitedofoq.fabs.bpm.model.status.ProcessState;
import com.unitedofoq.fabs.bpm.services.ProcessService;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Iterator;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.bpm.services.impl.RestProcessService", beanInterface = ProcessService.class)
public class RestProcessServiceImpl extends AbstractBPMService implements ProcessService {

    RestProcessServiceImpl() {
    }

    @Override
    public ProcessInstance createProcessInstance(String processId,
            Map<String, Object> parameters) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ProcessInstance startProcessInstance(long processInstanceId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void abortProcessInstance(long processInstanceId) {
        // TODO Auto-generated method stub

    }

    @Override
    public ProcessInstance startProcess(String processId,
            Map<String, Object> parameters) {
        Map<String, String> requestParameters = new HashMap<String, String>();
        if (parameters != null) {
            for (String strKey : parameters.keySet()) {
                requestParameters.put(strKey, String.valueOf(parameters.get(strKey)));
            }
        }
        loadProperties();
        String url = propertiesUtil.getValue("process.start");
        StringBuilder uri = new StringBuilder(MessageFormat.format(url, host, port, deploymentId, processId, "").replace("?", ""));

        JSONObject response = restConnector.sendPostRequest(uri.toString(),
                requestParameters, userName, password, null);
        ProcessInstance processInstance = mapJsonResponseToProcessInstance(response);
        return processInstance;
    }

    private ProcessInstance mapJsonResponseToProcessInstance(JSONObject response) {
        ProcessInstance processInstance = null;
        if (response != null) {
            processInstance = new ProcessInstance();
            processInstance.setId(Long.parseLong(response.get("id").toString()));
            Integer status = Integer.parseInt(response.get("state").toString());
            processInstance.setProcessId(response.get("processId").toString());
            if (status != null) {
                processInstance.setState(ProcessState.values()[status]);
            }
        }
        return processInstance;
    }

    @Override
    public ProcessInstance retrieveActiveProcessInstance(long processInstanceId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<ProcessInstance> retrieveActiveProcessInstances() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<Long, List<ProcessInstance>> retrieveAllProcessInstances(ProcessRequestData data) {
        loadProperties();
        Map<Long, List<ProcessInstance>> processesWithInfo = new HashMap<>();
        String url = propertiesUtil.getValue("process.allProcessInstances");
        StringBuilder uri = new StringBuilder(MessageFormat.format(url, host, port).replace("?", ""));
        if (data.getInitiator().equalsIgnoreCase(userName.trim()) || data.getInitiator().trim().equals("") || data.getInitiator().contains(userName.trim())) {
            data.setInitiator(null);
        }
        String entity = converter.convertFromObjectToJson(data);
        JSONObject response = restConnector.sendPostRequest(uri.toString(),
                null, userName, password, entity);
        JSONArray jsonArray = (JSONArray) response.get("data");
        Long totalElements = (Long) response.get("totalElements");
        List<ProcessInstance> instances = mapJSONObjectToListOfProcessInstances(jsonArray);
        processesWithInfo.put(totalElements, instances);
        return processesWithInfo;
    }
    
    private List<ProcessInstance> mapJSONObjectToListOfProcessInstances(JSONArray jsonArray) {
        List<ProcessInstance> processInstances = new ArrayList<ProcessInstance>();
        for (Iterator it = jsonArray.iterator(); it.hasNext();) {
            Object obj = it.next();
            JSONObject json = (JSONObject) obj;
            ProcessInstance processInstance = mapJsonObjectToProcessInstance(json);
            processInstances.add(processInstance);
        }
        return processInstances;
    }

    @Override
    public String retrieveProcessInitiator(long insId) {
        String url = propertiesUtil.getValue("process.initiatorVariable");
        StringBuilder uri = new StringBuilder(MessageFormat.format(url, host, port, deploymentId, insId));
        JSONObject response = restConnector.sendGetRequest(uri.toString(),
                null, userName, password);
        if (response == null) {
            return "";
        }
        return (String) response.get("result");
    }

    private ProcessInstance mapJsonObjectToProcessInstance(JSONObject json) {
        ProcessInstance processInstance = new ProcessInstance();
        processInstance.setId((long) json.get("id"));
        processInstance.setName((String) json.get("processName"));
        processInstance.setInitiator((String) json.get("initiator"));
        processInstance.setStartDate((String) json.get("startDate"));
        int status = ((Long) json.get("status")).intValue();
        switch (status) {
            case 1:
                processInstance.setState(ProcessState.ACTIVE);
                break;
            case 2:
                processInstance.setState(ProcessState.COMPLETED);
                break;
            case 3:
                processInstance.setState(ProcessState.ABORTED);
                break;
        }
        return processInstance;
    }
}
