package com.unitedofoq.fabs.bpm.rest.services.impl;

import com.unitedofoq.fabs.bpm.common.connector.RestConnector;
import com.unitedofoq.fabs.bpm.services.BPMService;
import com.unitedofoq.fabs.bpm.util.BPMPropertiesUtil;
import com.unitedofoq.fabs.bpm.util.Converter;
import java.text.MessageFormat;
import javax.ejb.EJB;
import org.json.simple.JSONObject;

public abstract class AbstractBPMService implements BPMService {

    protected RestConnector restConnector;
    @EJB
    protected BPMPropertiesUtil propertiesUtil;

    protected final Converter converter = new Converter();

    protected String host;
    protected String port;
    protected String userName;
    protected String password;
    protected String deploymentId;

    public void loadProperties() {

        String requestTimeout = propertiesUtil.getValue("engine.requestConnectionTimeout");
        restConnector = new RestConnector(Integer.parseInt(requestTimeout) * 1000);
        host = propertiesUtil.getValue("engine.host");
        port = propertiesUtil.getValue("engine.port");
        userName = propertiesUtil.getValue("engine.userName");
        password = propertiesUtil.getValue("engine.password");
        deploymentId = getDeploymentId();
    }

    protected String getDeploymentId() {
        String url = propertiesUtil.getValue("process.deploymentId");
        StringBuilder uri = new StringBuilder(MessageFormat.format(url, host, port));
        JSONObject response = restConnector.sendPostRequest(uri.toString(), null, userName, password, null);
        return (String) response.get("deploymentId");
    }

    @Override
    public String getType() {
        return "jbpm_rest";
    }
}
