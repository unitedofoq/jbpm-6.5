/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.jbpm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mmasoud
 */
@XmlRootElement
public class ProcessInstance implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    private Long id;
    
    private int status;
    
    private String processName;
    
    private String initiator;
    
    private String startDate;

    public ProcessInstance(Long id, int status, String processName, String initiator, String startDate) {
        this.id = id;
        this.status = status;
        this.processName = processName;
        this.initiator = initiator;
        this.startDate = startDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getInitiator() {
        return initiator;
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    
}
