/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.jbpm.controller;

import com.unitedofoq.fabs.jbpm.model.PageableDomain;
import com.unitedofoq.fabs.jbpm.model.ProcessInstance;
import com.unitedofoq.fabs.jbpm.model.ProcessRequestData;
import com.unitedofoq.fabs.jbpm.service.ProcessService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mmasoud
 */
@RestController
public class ProcessController {

    @Autowired
    private ProcessService processService;

    @RequestMapping(path = "/retrieveProcessInstances", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public PageableDomain<List<ProcessInstance>> retrieveAllUserTasks(
            @RequestBody ProcessRequestData data) {
        return processService.getAllProcessInstances(data.getInitiator(), data.getSortedField(), data.getSortingOrder(), data.getFilters(), data.getPage(), data.getPageSize());
    }

    @RequestMapping(path = "/retrieveLatestDeploymentId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Map<String, String> retrieveLatestDeploymentId() {
        Map<String, String> deploymentId = new HashMap<>();
        deploymentId.put("deploymentId", processService.getLatestDeploymentId());
        return deploymentId;
    }
}
