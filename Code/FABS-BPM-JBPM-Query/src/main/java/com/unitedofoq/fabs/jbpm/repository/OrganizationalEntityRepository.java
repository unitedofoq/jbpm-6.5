/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.jbpm.repository;

import com.unitedofoq.fabs.jbpm.entity.Organizationalentity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;


/**
 *
 * @author mmasoud
 */
public interface OrganizationalEntityRepository extends PagingAndSortingRepository<Organizationalentity, String>, BaseRepository<Organizationalentity, String> {
}
