/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.jbpm.repository;

import com.unitedofoq.fabs.jbpm.entity.Processinstancelog;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author mmasoud
 */
@Repository
public interface ProcessInstanceRepository extends PagingAndSortingRepository<Processinstancelog, Long>, BaseRepository<Processinstancelog, Long> {
    @Query("SELECT d.deploymentId FROM DeploymentStore d WHERE d.updateDate = (SELECT MAX(d.updateDate) FROM DeploymentStore d WHERE d.deploymentId not like '%guvnor%')")
    public String findLatestDeploymentId();
}
