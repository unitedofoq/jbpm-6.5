/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.jbpm.repository;

import com.unitedofoq.fabs.jbpm.entity.Task;
import java.util.List;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author mmasoud
 */
@Repository
public interface UserInboxRepository extends PagingAndSortingRepository<Task, Long>, BaseRepository<Task, Long> {
    
    @Modifying
    @Query("UPDATE Task t set t.actualOwnerid.id = ?1 WHERE t.id = ?2")
    public void UpdateTaskActualOwnerId(String actualOwnerId, long taskId);
    
    @Query("SELECT p.id FROM Task t JOIN t.potentialOwners p WHERE p.dtype = 'Group' AND t.id = ?1")
    public List<String> findTaskPotentialOwnersOfTypeGroup(long id);
}
