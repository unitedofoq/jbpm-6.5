/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.jbpm.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mmasoud
 */
@Entity
@Table(name = "taskdata")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Taskdata.findAll", query = "SELECT t FROM Taskdata t")})
public class Taskdata implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 255)
    @Column(name = "task_id")
    private String taskId;
    @Size(max = 255)
    @Column(name = "taskname")
    private String taskname;
    @Size(max = 255)
    @Column(name = "requester")
    private String requester;
    @Size(max = 255)
    @Column(name = "lastuser")
    private String lastuser;
    @Size(max = 255)
    @Column(name = "requester_displayname")
    private String requesterDisplayname;
    @Size(max = 45)
    @Column(name = "lastuser_displayname")
    private String lastuserDisplayname;

    public Taskdata() {
    }

    public Taskdata(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskname() {
        return taskname;
    }

    public void setTaskname(String taskname) {
        this.taskname = taskname;
    }

    public String getRequester() {
        return requester;
    }

    public void setRequester(String requester) {
        this.requester = requester;
    }

    public String getLastuser() {
        return lastuser;
    }

    public void setLastuser(String lastuser) {
        this.lastuser = lastuser;
    }

    public String getRequesterDisplayname() {
        return requesterDisplayname;
    }

    public void setRequesterDisplayname(String requesterDisplayname) {
        this.requesterDisplayname = requesterDisplayname;
    }

    public String getLastuserDisplayname() {
        return lastuserDisplayname;
    }

    public void setLastuserDisplayname(String lastuserDisplayname) {
        this.lastuserDisplayname = lastuserDisplayname;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Taskdata)) {
            return false;
        }
        Taskdata other = (Taskdata) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.unitedofoq.fabs.jbpm.entity.Taskdata[ id=" + id + " ]";
    }
    
}
