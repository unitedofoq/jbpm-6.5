/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.jbpm.service;

import com.unitedofoq.fabs.bpm.common.connector.RestConnector;
import com.unitedofoq.fabs.jbpm.common.util.BPMPropertiesUtil;
import com.unitedofoq.fabs.jbpm.entity.Organizationalentity;
import com.unitedofoq.fabs.jbpm.model.PageableDomain;
import com.unitedofoq.fabs.jbpm.model.UserTask;
import com.unitedofoq.fabs.jbpm.repository.OrganizationalEntityRepository;
import com.unitedofoq.fabs.jbpm.repository.TaskSummaryRepository;
import com.unitedofoq.fabs.jbpm.repository.UserInboxRepository;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.Resource;
import org.json.simple.JSONObject;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bgalal
 */
@Component
@Transactional(readOnly = true)
public class UserInboxService {

    @Resource
    private UserInboxRepository userInboxRepository;
    @Resource
    private TaskSummaryRepository taskSummaryRepository;
    @Resource
    private OrganizationalEntityRepository organizationalEntityRepository;
    private RestConnector restConnector;
    private BPMPropertiesUtil bPMPropertiesUtil;

    public UserInboxService() {

        bPMPropertiesUtil = new BPMPropertiesUtil();
        String requestTimeout = bPMPropertiesUtil.getValue("engine.requestConnectionTimeout");
        restConnector = new RestConnector(Integer.parseInt(requestTimeout) * 1000);
    }

    public PageableDomain<List<UserTask>> getAllUserTasks(String creatorId, List<String> groups, String sortedField, String sortingOrder, Map<String, String> filters, int page, int size) {
        Map<String, Object> queryData = getQueryData(creatorId, groups, sortedField, sortingOrder, filters);
        PageableDomain<List<UserTask>> userTasksResp = userInboxRepository
                .findAllUserTasksWithContent((String) queryData.get("filter"), (Map<String, Object>) queryData.get("params"), new PageRequest(page, size));
        setParametersIntoUserTasks(userTasksResp);
        return userTasksResp;
    }

    private Map<String, Object> getTaskParameters(String taskId) {
        String userName = bPMPropertiesUtil.getValue("engine.userName");
        String password = bPMPropertiesUtil.getValue("engine.password");
        String host = bPMPropertiesUtil.getValue("engine.host");
        String port = bPMPropertiesUtil.getValue("engine.port");
        String url = bPMPropertiesUtil.getValue("task.inputs");
        url = MessageFormat.format(url, host, port, taskId);

        JSONObject content = null;
        short trials = 0;

        do {
            content = restConnector.sendGetRequest(url.toString(), null, userName, password);
            trials++;
        } while (content == null && trials < 10);

        System.out.println("trials>>>>" + trials);

        return content == null ? null : (Map<String, Object>) content.get("contentMap");
    }

    private void setParametersIntoUserTasks(PageableDomain<List<UserTask>> userTasksResp) {
        for (UserTask task : userTasksResp.getData()) {
            task.setParameters(getTaskParameters(Long.toString(task.getId())));
        }
    }

    private Map<String, Object> getQueryData(String creatorId, List<String> groups, String sortedField, String sortingOrder, Map<String, String> filterList) {
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> params = new HashMap<>();
        params.put("creatorId", creatorId);
        StringBuilder filter = new StringBuilder();
        if (groups.isEmpty()) {
            filter.append(") ");
        } else {
            filter.append(" OR p.id IN ('").append(groups.get(0));
            for (int index = 1; index < groups.size(); index++) {
                filter.append("', '").append(groups.get(index));
            }
            filter.append("') AND t.createdByid.id = 'null' AND t.status = 'Ready') ");
        }
        if (filterList != null) {
            for (Entry entry : filterList.entrySet()) {
                if (!entry.getKey().equals("status")) {
                    String key = (String) entry.getKey();
                    key = key.split("\\.")[1];
                    params.put(key, entry.getValue());
                    if (key.equals("createdOn")) {
                        filter.append(" AND ").append("CONCAT(CONCAT(cast(cast(t.createdOn as date) as string),' '), SUBSTRING(cast(cast(t.createdOn as time) as string),1,8))").append(" LIKE CONCAT('%', :")
                                .append(key).append(",'%') ");
                    } else {
                        filter.append(" AND ").append("CAST(").append(entry.getKey()).append(" as string)").append(" LIKE CONCAT('%', :")
                                .append(key).append(",'%') ");
                    }
                }
            }
        }
        filter.append(" ORDER BY ").append(sortedField).append(" ").append(sortingOrder);
        data.put("filter", filter.toString());
        data.put("params", params);
        return data;
    }

    public void editActualOwnerManually(String actualOwnerId, long taskId) {
        if (!organizationalEntityRepository.exists(actualOwnerId)) {
            Organizationalentity oEntity = new Organizationalentity(actualOwnerId, "User");
            organizationalEntityRepository.saveAndFlush(oEntity);
        }
        userInboxRepository.UpdateTaskActualOwnerId(actualOwnerId, taskId);
        taskSummaryRepository.UpdateTaskSummaryUserId(actualOwnerId, taskId);
    }

    public List<String> getTaskPotentialOwnersOfTypeGroup(long id) {
        return userInboxRepository.findTaskPotentialOwnersOfTypeGroup(id);
    }

}
