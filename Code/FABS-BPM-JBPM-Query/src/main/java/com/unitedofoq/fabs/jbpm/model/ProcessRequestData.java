/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.jbpm.model;

import java.util.Map;

/**
 *
 * @author mmasoud
 */
public class ProcessRequestData {
    
    private String initiator;
    private int page;
    private int pageSize;
    private String sortedField;
    private String sortingOrder;
    private Map<String, String> filters;

    public ProcessRequestData() {
    }

    public ProcessRequestData(String initiator, int page, int pageSize, String sortedField, String sortingOrder, Map<String, String> filters) {
        this.initiator = initiator;
        this.page = page;
        this.pageSize = pageSize;
        this.sortedField = sortedField;
        this.sortingOrder = sortingOrder;
        this.filters = filters;
    }

    public String getInitiator() {
        return initiator;
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSortedField() {
        return sortedField;
    }

    public void setSortedField(String sortedField) {
        this.sortedField = sortedField;
    }

    public String getSortingOrder() {
        return sortingOrder;
    }

    public void setSortingOrder(String sortingOrder) {
        this.sortingOrder = sortingOrder;
    }

    public Map<String, String> getFilters() {
        return filters;
    }

    public void setFilters(Map<String, String> filters) {
        this.filters = filters;
    }
    
}
