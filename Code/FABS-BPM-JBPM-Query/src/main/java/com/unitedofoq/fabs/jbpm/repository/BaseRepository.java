/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.jbpm.repository;

import com.unitedofoq.fabs.jbpm.model.PageableDomain;
import com.unitedofoq.fabs.jbpm.model.ProcessInstance;
import com.unitedofoq.fabs.jbpm.model.UserTask;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

/**
 *
 * @author bgalal
 */
@NoRepositoryBean
public interface BaseRepository<T, ID extends Serializable> extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {

    List<Long> findListOfUserTaskIdToBeDelegated(String filter, Map<String, Object> params);
    
    PageableDomain<List<UserTask>> findAllUserTasksWithContent(String filter, Map<String, Object> params, Pageable pageable);
    
    PageableDomain<List<ProcessInstance>> findAllProcessInstances(String filter, Map<String, Object> params, Pageable pageable);
}
