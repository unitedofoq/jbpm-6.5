/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.jbpm.repository;

import com.unitedofoq.fabs.jbpm.entity.TaskSummary;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author mmasoud
 */
@Repository
public interface TaskSummaryRepository extends PagingAndSortingRepository<TaskSummary, Long>, BaseRepository<TaskSummary, Long> {
    
    @Modifying
    @Query("UPDATE TaskSummary ts set ts.userId = ?1 WHERE ts.taskId = ?2")
    public void UpdateTaskSummaryUserId(String userId, long taskId);
}
