package com.unitedofoq.fabs.jbpm.model;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

public class PageableDomain<T> {
	
	public PageableDomain(){
		
	}

	public PageableDomain(T data) {
		super();
		this.data = data;
	}

	@JsonUnwrapped
	@Valid
	private T data;
	private int totalPages;
	private int numberOfElements;
	private long totalElements;
	private int size;
	private int number;
	private boolean last;
	private boolean first;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getNumberOfElements() {
		return numberOfElements;
	}

	public void setNumberOfElements(int numberOfElements) {
		this.numberOfElements = numberOfElements;
	}

	public long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(long totalElements) {
		this.totalElements = totalElements;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public boolean isLast() {
		return last;
	}

	public void setLast(boolean last) {
		this.last = last;
	}

	public boolean isFirst() {
		return first;
	}

	public void setFirst(boolean first) {
		this.first = first;
	}

}
