/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.jbpm.service;

import com.unitedofoq.fabs.bpm.common.connector.RestConnector;
import com.unitedofoq.fabs.bpm.common.model.DelegationData;
import com.unitedofoq.fabs.jbpm.entity.Organizationalentity;
import com.unitedofoq.fabs.jbpm.repository.DelegationRepository;
import com.unitedofoq.fabs.jbpm.common.util.BPMPropertiesUtil;
import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author mmasoud
 */
@Component
@Transactional(readOnly = true)
public class DelegationService {

    @Resource
    private DelegationRepository delegationRepository;
    private RestConnector requester;
    private BPMPropertiesUtil bPMPropertiesUtil;

    public DelegationService() {

        bPMPropertiesUtil = new BPMPropertiesUtil();
        String requestTimeout = bPMPropertiesUtil.getValue("engine.requestConnectionTimeout");
        requester = new RestConnector(Integer.parseInt(requestTimeout) * 1000);
    }

    private void delegate(String taskId, String delegateUserId) {
        String userName = bPMPropertiesUtil.getValue("engine.userName");
        String password = bPMPropertiesUtil.getValue("engine.password");
        String host = bPMPropertiesUtil.getValue("engine.host");
        String port = bPMPropertiesUtil.getValue("engine.port");
        String url = bPMPropertiesUtil.getValue("task.delegate");
        url = MessageFormat.format(url, host, port, taskId, delegateUserId);
        requester.sendPostRequest(url.toString(), null, userName, password, null);
    }

    public void applyDelegation(List<DelegationData> delegations) {
        for (DelegationData delegation : delegations) {
            List<Long> userTaskIdsToBeDelegated
                    = getListOfUserTaskIdToBeDelegated(delegation.getOwner(),
                            delegation.getFromTime(), delegation.getToTime(),
                            delegation.getProcessIds());
            delegateAllTasks(delegation.getDelegate(), userTaskIdsToBeDelegated);
        }
    }

    private List<Long> getListOfUserTaskIdToBeDelegated(String creatorId,
            Date fromDate, Date toDate, List<String> processIDs) {
        Map<String, Object> queryData = getQueryData(creatorId, fromDate,
                toDate, processIDs);
        List<Long> taskIDs = delegationRepository.
                findListOfUserTaskIdToBeDelegated(
                        (String) queryData.get("filter"),
                        (Map<String, Object>) queryData.get("params"));
        return taskIDs;
    }

    private Map<String, Object> getQueryData(String creatorId, Date fromDate,
            Date toDate, List<String> processIDs) {
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> params = new HashMap<>();
        params.put("creatorId", new Organizationalentity(creatorId));
        params.put("fromDate", fromDate);
        params.put("toDate", toDate);
        StringBuilder filter = new StringBuilder();
        filter.append("t.createdByid = :creatorId AND ");
        filter.append("t.activationTime >= :fromDate AND ");
        filter.append("t.activationTime <= :toDate");
        if (processIDs != null && !processIDs.isEmpty()) {
            params.put("processId1", processIDs.get(0));
            filter.append(" AND processId IN (:processId1");
            for (int index = 1; index < processIDs.size(); index++) {
                params.put("processId" + (index + 1), processIDs.get(index));
                filter.append(", :processId").append(index + 1);
            }
            filter.append(")");
        }
        data.put("filter", filter.toString());
        data.put("params", params);
        return data;
    }

    private void delegateAllTasks(String targetId, List<Long> taskIds) {
        for (Long taskId : taskIds) {
            delegate(Long.toString(taskId), targetId);
        }
    }
}
