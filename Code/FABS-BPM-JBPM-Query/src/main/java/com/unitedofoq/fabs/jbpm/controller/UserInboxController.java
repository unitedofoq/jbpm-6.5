package com.unitedofoq.fabs.jbpm.controller;

import com.unitedofoq.fabs.bpm.common.model.TasksRequestData;
import com.unitedofoq.fabs.jbpm.service.UserInboxService;
import com.unitedofoq.fabs.jbpm.model.PageableDomain;
import com.unitedofoq.fabs.jbpm.model.UserTask;
import com.unitedofoq.fabs.jbpm.service.DelegationService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserInboxController {

    @Autowired
    private UserInboxService userInboxService;
    @Autowired
    private DelegationService delegationService;

    @RequestMapping(path = "/retrieveAllUserTasks", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public PageableDomain<List<UserTask>> retrieveAllUserTasks(
            @RequestBody TasksRequestData data) {
        delegationService.applyDelegation(data.getDelegations());
        return userInboxService.getAllUserTasks(data.getActorId(), data.getRoles(), data.getSortedField(), data.getSortingOrder(), data.getFilters(), data.getPage(), data.getPageSize());
    }
    
    @RequestMapping(path = "/updateTaskActualOwner", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void updateTaskActualOwner(@RequestParam String actualOwner, @RequestParam long taskId) {
        userInboxService.editActualOwnerManually(actualOwner, taskId);
    }
    
    @RequestMapping(path = "/retrieveTaskPotentialOwnersOfTypeGroup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Map<String, List<String>> retrieveTaskPotentialOwnersOfTypeGroup(@RequestParam long taskId) {
        Map<String, List<String>> Roles = new HashMap<>();
        Roles.put("Roles", userInboxService.getTaskPotentialOwnersOfTypeGroup(taskId));
        return Roles;
    }
}
