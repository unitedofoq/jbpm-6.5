package com.unitedofoq.fabs.jbpm.util;

import java.security.Principal;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import com.unitedofoq.fabs.jbpm.model.PageableDomain;

public class GenericUtils {
	
	public static <T> PageableDomain<T> mapPageToPageableDomain(Page<?> page, PageableDomain<T> pageableDomain){
		pageableDomain.setTotalElements(page.getTotalElements());
		pageableDomain.setTotalPages(page.getTotalPages());
		pageableDomain.setSize(page.getSize());
		pageableDomain.setNumberOfElements(page.getNumberOfElements());
		pageableDomain.setFirst(page.isFirst());
		pageableDomain.setLast(page.isLast());
		pageableDomain.setNumber(page.getNumber());
		return pageableDomain;
	}
	
	public static Timestamp getCurrentTimestamp(){
		long currentTime = System.currentTimeMillis();
		return new Timestamp(currentTime);
	}
	
	public static int getUserIdFromPrincipal (Principal principal){
		return Integer.parseInt(principal.getName());
	}

}
