/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.jbpm.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author bgalal
 */
@Entity
@Table(name = "task")
@XmlRootElement
public class Task implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "archived")
    private Short archived;
    @Size(max = 255)
    @Column(name = "allowedToDelegate")
    private String allowedToDelegate;
    @Size(max = 255)
    @Column(name = "description")
    private String description;
    @Size(max = 255)
    @Column(name = "formName")
    private String formName;
    @Size(max = 255)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "priority")
    private int priority;
    @Size(max = 255)
    @Column(name = "subTaskStrategy")
    private String subTaskStrategy;
    @Size(max = 255)
    @Column(name = "subject")
    private String subject;
    @Column(name = "activationTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date activationTime;
    @Column(name = "createdOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    @Size(max = 255)
    @Column(name = "deploymentId")
    private String deploymentId;
    @Column(name = "documentAccessType")
    private Integer documentAccessType;
    @Basic(optional = false)
    @NotNull
    @Column(name = "documentContentId")
    private long documentContentId;
    @Size(max = 255)
    @Column(name = "documentType")
    private String documentType;
    @Column(name = "expirationTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expirationTime;
    @Column(name = "faultAccessType")
    private Integer faultAccessType;
    @Basic(optional = false)
    @NotNull
    @Column(name = "faultContentId")
    private long faultContentId;
    @Size(max = 255)
    @Column(name = "faultName")
    private String faultName;
    @Size(max = 255)
    @Column(name = "faultType")
    private String faultType;
    @Column(name = "outputAccessType")
    private Integer outputAccessType;
    @Basic(optional = false)
    @NotNull
    @Column(name = "outputContentId")
    private long outputContentId;
    @Size(max = 255)
    @Column(name = "outputType")
    private String outputType;
    @Basic(optional = false)
    @NotNull
    @Column(name = "parentId")
    private long parentId;
    @Column(name = "previousStatus")
    private Integer previousStatus;
    @Size(max = 255)
    @Column(name = "processId")
    private String processId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "processInstanceId")
    private long processInstanceId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "processSessionId")
    private long processSessionId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "skipable")
    private boolean skipable;
    @Size(max = 255)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "workItemId")
    private long workItemId;
    @Size(max = 255)
    @Column(name = "taskType")
    private String taskType;
    @Column(name = "OPTLOCK")
    private Integer optlock;
    @ManyToMany(mappedBy = "taskList")
    private List<Organizationalentity> organizationalentityList;
    @JoinTable(name = "peopleassignments_bas", joinColumns = {
        @JoinColumn(name = "task_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "entity_id", referencedColumnName = "id")})
    @ManyToMany
    private List<Organizationalentity> organizationalentityList1;
    @JoinTable(name = "delegation_delegates", joinColumns = {
        @JoinColumn(name = "task_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "entity_id", referencedColumnName = "id")})
    @ManyToMany
    private List<Organizationalentity> organizationalentityList2;
    @JoinTable(name = "peopleassignments_potowners", joinColumns = {
        @JoinColumn(name = "task_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "entity_id", referencedColumnName = "id")})
    @ManyToMany
    private List<Organizationalentity> potentialOwners;
    @JoinTable(name = "peopleassignments_stakeholders", joinColumns = {
        @JoinColumn(name = "task_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "entity_id", referencedColumnName = "id")})
    @ManyToMany
    private List<Organizationalentity> organizationalentityList4;
    @JoinTable(name = "peopleassignments_exclowners", joinColumns = {
        @JoinColumn(name = "task_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "entity_id", referencedColumnName = "id")})
    @ManyToMany
    private List<Organizationalentity> organizationalentityList5;
    @JoinColumn(name = "actualOwner_id", referencedColumnName = "id")
    @ManyToOne
    private Organizationalentity actualOwnerid;
    @JoinColumn(name = "createdBy_id", referencedColumnName = "id")
    @ManyToOne
    private Organizationalentity createdByid;
    @JoinColumn(name = "taskInitiator_id", referencedColumnName = "id")
    @ManyToOne
    private Organizationalentity taskInitiatorid;

    public Task() {
    }

    public Task(Long id) {
        this.id = id;
    }

    public Task(Long id, int priority, long documentContentId, long faultContentId, long outputContentId, long parentId, long processInstanceId, long processSessionId, boolean skipable, long workItemId) {
        this.id = id;
        this.priority = priority;
        this.documentContentId = documentContentId;
        this.faultContentId = faultContentId;
        this.outputContentId = outputContentId;
        this.parentId = parentId;
        this.processInstanceId = processInstanceId;
        this.processSessionId = processSessionId;
        this.skipable = skipable;
        this.workItemId = workItemId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Short getArchived() {
        return archived;
    }

    public void setArchived(Short archived) {
        this.archived = archived;
    }

    public String getAllowedToDelegate() {
        return allowedToDelegate;
    }

    public void setAllowedToDelegate(String allowedToDelegate) {
        this.allowedToDelegate = allowedToDelegate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getSubTaskStrategy() {
        return subTaskStrategy;
    }

    public void setSubTaskStrategy(String subTaskStrategy) {
        this.subTaskStrategy = subTaskStrategy;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getActivationTime() {
        return activationTime;
    }

    public void setActivationTime(Date activationTime) {
        this.activationTime = activationTime;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getDeploymentId() {
        return deploymentId;
    }

    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }

    public Integer getDocumentAccessType() {
        return documentAccessType;
    }

    public void setDocumentAccessType(Integer documentAccessType) {
        this.documentAccessType = documentAccessType;
    }

    public long getDocumentContentId() {
        return documentContentId;
    }

    public void setDocumentContentId(long documentContentId) {
        this.documentContentId = documentContentId;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public Date getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Date expirationTime) {
        this.expirationTime = expirationTime;
    }

    public Integer getFaultAccessType() {
        return faultAccessType;
    }

    public void setFaultAccessType(Integer faultAccessType) {
        this.faultAccessType = faultAccessType;
    }

    public long getFaultContentId() {
        return faultContentId;
    }

    public void setFaultContentId(long faultContentId) {
        this.faultContentId = faultContentId;
    }

    public String getFaultName() {
        return faultName;
    }

    public void setFaultName(String faultName) {
        this.faultName = faultName;
    }

    public String getFaultType() {
        return faultType;
    }

    public void setFaultType(String faultType) {
        this.faultType = faultType;
    }

    public Integer getOutputAccessType() {
        return outputAccessType;
    }

    public void setOutputAccessType(Integer outputAccessType) {
        this.outputAccessType = outputAccessType;
    }

    public long getOutputContentId() {
        return outputContentId;
    }

    public void setOutputContentId(long outputContentId) {
        this.outputContentId = outputContentId;
    }

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public Integer getPreviousStatus() {
        return previousStatus;
    }

    public void setPreviousStatus(Integer previousStatus) {
        this.previousStatus = previousStatus;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public long getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(long processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public long getProcessSessionId() {
        return processSessionId;
    }

    public void setProcessSessionId(long processSessionId) {
        this.processSessionId = processSessionId;
    }

    public boolean getSkipable() {
        return skipable;
    }

    public void setSkipable(boolean skipable) {
        this.skipable = skipable;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getWorkItemId() {
        return workItemId;
    }

    public void setWorkItemId(long workItemId) {
        this.workItemId = workItemId;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public Integer getOptlock() {
        return optlock;
    }

    public void setOptlock(Integer optlock) {
        this.optlock = optlock;
    }

    @XmlTransient
    public List<Organizationalentity> getOrganizationalentityList() {
        return organizationalentityList;
    }

    public void setOrganizationalentityList(List<Organizationalentity> organizationalentityList) {
        this.organizationalentityList = organizationalentityList;
    }

    @XmlTransient
    public List<Organizationalentity> getOrganizationalentityList1() {
        return organizationalentityList1;
    }

    public void setOrganizationalentityList1(List<Organizationalentity> organizationalentityList1) {
        this.organizationalentityList1 = organizationalentityList1;
    }

    @XmlTransient
    public List<Organizationalentity> getOrganizationalentityList2() {
        return organizationalentityList2;
    }

    public void setOrganizationalentityList2(List<Organizationalentity> organizationalentityList2) {
        this.organizationalentityList2 = organizationalentityList2;
    }

    @XmlTransient
    public List<Organizationalentity> getPotentialOwners() {
        return potentialOwners;
    }

    public void setPotentialOwners(List<Organizationalentity> potentialOwners) {
        this.potentialOwners = potentialOwners;
    }

    @XmlTransient
    public List<Organizationalentity> getOrganizationalentityList4() {
        return organizationalentityList4;
    }

    public void setOrganizationalentityList4(List<Organizationalentity> organizationalentityList4) {
        this.organizationalentityList4 = organizationalentityList4;
    }

    @XmlTransient
    public List<Organizationalentity> getOrganizationalentityList5() {
        return organizationalentityList5;
    }

    public void setOrganizationalentityList5(List<Organizationalentity> organizationalentityList5) {
        this.organizationalentityList5 = organizationalentityList5;
    }

    public Organizationalentity getActualOwnerid() {
        return actualOwnerid;
    }

    public void setActualOwnerid(Organizationalentity actualOwnerid) {
        this.actualOwnerid = actualOwnerid;
    }

    public Organizationalentity getCreatedByid() {
        return createdByid;
    }

    public void setCreatedByid(Organizationalentity createdByid) {
        this.createdByid = createdByid;
    }

    public Organizationalentity getTaskInitiatorid() {
        return taskInitiatorid;
    }

    public void setTaskInitiatorid(Organizationalentity taskInitiatorid) {
        this.taskInitiatorid = taskInitiatorid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Task)) {
            return false;
        }
        Task other = (Task) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.unitedofoq.fabs.jbpm.entity.Task[ id=" + id + " ]";
    }
    
}
