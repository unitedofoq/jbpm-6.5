package com.unitedofoq.fabs.jbpm.repository;

import com.unitedofoq.fabs.jbpm.model.PageableDomain;
import com.unitedofoq.fabs.jbpm.model.ProcessInstance;
import com.unitedofoq.fabs.jbpm.model.UserTask;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author bgalal
 */
public class BaseRepositoryImpl<T, ID extends Serializable>
        extends SimpleJpaRepository<T, ID> implements BaseRepository<T, ID> {

    private final EntityManager entityManager;

    private final JpaEntityInformation entityInformation;

    public BaseRepositoryImpl(JpaEntityInformation entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);

        // Keep the EntityManager around to used from the newly introduced methods.
        this.entityManager = entityManager;
        this.entityInformation = entityInformation;
    }

    @Override
    public List<Long> findListOfUserTaskIdToBeDelegated(String filter, Map<String, Object> params) {
        final StringBuilder jpql = new StringBuilder("SELECT t.id FROM ");
        jpql.append(entityInformation.getEntityName()).append(" t WHERE t.status = 'Ready' AND ");
        jpql.append(filter);
        Query query = entityManager.createQuery(jpql.toString());
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        return query.getResultList();
    }

    @Override
    public PageableDomain<List<UserTask>> findAllUserTasksWithContent(String filter, Map<String, Object> params, Pageable pageable) {
        StringBuilder jpql = new StringBuilder("SELECT new com.unitedofoq.fabs.jbpm.model.UserTask ("
                + "t.id, t.description, t.name, t.priority, t.subject, t.activationTime, CONCAT(CONCAT(cast(cast(t.createdOn as date) as string),' '), SUBSTRING(cast(cast(t.createdOn as time) as string),1,8)), t.expirationTime, t.deploymentId, "
                + "t.processId, t.processInstanceId, t.processSessionId, t.status, t.workItemId, t.taskType, "
                + "t.createdByid.id, t.actualOwnerid.id) FROM ");
        jpql.append(entityInformation.getEntityName()).append(" t JOIN t.potentialOwners p, Taskdata td WHERE t.id = td.taskId AND (t.createdByid.id = :creatorId AND t.status = 'Ready' AND p.dtype = 'User' OR t.actualOwnerid.id = :creatorId AND t.status IN ('Reserved', 'InProgress') AND p.dtype = 'Group'");
        jpql.append(filter);
        
        entityManager.flush();
        
        Query query = entityManager.createQuery(jpql.toString(), UserTask.class);
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        
        int totalElements = query.getResultList().size();
        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        PageableDomain<List<UserTask>> paginatedResult = new PageableDomain<List<UserTask>>(query.getResultList());
        paginatedResult.setTotalElements(totalElements);
        paginatedResult.setTotalPages((totalElements % pageable.getPageSize()) == 0 ? (totalElements / pageable.getPageSize()) : (totalElements / pageable.getPageSize()) + 1);
        paginatedResult.setSize(pageable.getPageSize());
        paginatedResult.setNumberOfElements(query.getResultList().size());
        paginatedResult.setFirst(pageable.getPageNumber() == 0);
        paginatedResult.setLast((paginatedResult.getTotalPages() - 1) == pageable.getPageNumber());
        paginatedResult.setNumber(pageable.getPageNumber());
        return paginatedResult;
    }

    @Override
    public PageableDomain<List<ProcessInstance>> findAllProcessInstances(String filter, Map<String, Object> params, Pageable pageable) {
        StringBuilder jpql = new StringBuilder("SELECT new com.unitedofoq.fabs.jbpm.model.ProcessInstance (");
        jpql.append("p.id, p.status, p.processName, v.value, CONCAT(CONCAT(cast(cast(p.startDate as date) as string),' '), SUBSTRING(cast(cast(p.startDate as time) as string),1,8))) FROM ");
        jpql.append("Processinstancelog p, Variableinstancelog v WHERE v.variableId = 'InitiatorDisplayName' AND p.id = v.processInstanceId ");
        jpql.append(filter);
        Query query = entityManager.createQuery(jpql.toString(), ProcessInstance.class);
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        int totalElements = query.getResultList().size();
        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());
        PageableDomain<List<ProcessInstance>> paginatedResult = new PageableDomain<List<ProcessInstance>>(query.getResultList());
        paginatedResult.setTotalElements(totalElements);
        paginatedResult.setTotalPages((totalElements % pageable.getPageSize()) == 0 ? (totalElements / pageable.getPageSize()) : (totalElements / pageable.getPageSize()) + 1);
        paginatedResult.setSize(pageable.getPageSize());
        paginatedResult.setNumberOfElements(query.getResultList().size());
        paginatedResult.setFirst(pageable.getPageNumber() == 0);
        paginatedResult.setLast((paginatedResult.getTotalPages() - 1) == pageable.getPageNumber());
        paginatedResult.setNumber(pageable.getPageNumber());
        return paginatedResult;
    }
}
