/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.jbpm.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author bgalal
 */
@Entity
@Table(name = "organizationalentity")
@XmlRootElement
public class Organizationalentity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "DTYPE")
    private String dtype;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "id")
    private String id;
    @JoinTable(name = "peopleassignments_recipients", joinColumns = {
        @JoinColumn(name = "entity_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "task_id", referencedColumnName = "id")})
    @ManyToMany
    private List<Task> taskList;
    @ManyToMany(mappedBy = "organizationalentityList1")
    private List<Task> taskList1;
    @ManyToMany(mappedBy = "organizationalentityList2")
    private List<Task> taskList2;
    @ManyToMany(mappedBy = "potentialOwners")
    private List<Task> taskList3;
    @ManyToMany(mappedBy = "organizationalentityList4")
    private List<Task> taskList4;
    @ManyToMany(mappedBy = "organizationalentityList5")
    private List<Task> taskList5;
    @OneToMany(mappedBy = "actualOwnerid")
    private List<Task> taskList6;
    @OneToMany(mappedBy = "createdByid")
    private List<Task> taskList7;
    @OneToMany(mappedBy = "taskInitiatorid")
    private List<Task> taskList8;

    public Organizationalentity() {
    }

    public Organizationalentity(String id) {
        this.id = id;
    }

    public Organizationalentity(String id, String dtype) {
        this.id = id;
        this.dtype = dtype;
    }

    public String getDtype() {
        return dtype;
    }

    public void setDtype(String dtype) {
        this.dtype = dtype;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @XmlTransient
    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }

    @XmlTransient
    public List<Task> getTaskList1() {
        return taskList1;
    }

    public void setTaskList1(List<Task> taskList1) {
        this.taskList1 = taskList1;
    }

    @XmlTransient
    public List<Task> getTaskList2() {
        return taskList2;
    }

    public void setTaskList2(List<Task> taskList2) {
        this.taskList2 = taskList2;
    }

    @XmlTransient
    public List<Task> getTaskList3() {
        return taskList3;
    }

    public void setTaskList3(List<Task> taskList3) {
        this.taskList3 = taskList3;
    }

    @XmlTransient
    public List<Task> getTaskList4() {
        return taskList4;
    }

    public void setTaskList4(List<Task> taskList4) {
        this.taskList4 = taskList4;
    }

    @XmlTransient
    public List<Task> getTaskList5() {
        return taskList5;
    }

    public void setTaskList5(List<Task> taskList5) {
        this.taskList5 = taskList5;
    }

    @XmlTransient
    public List<Task> getTaskList6() {
        return taskList6;
    }

    public void setTaskList6(List<Task> taskList6) {
        this.taskList6 = taskList6;
    }

    @XmlTransient
    public List<Task> getTaskList7() {
        return taskList7;
    }

    public void setTaskList7(List<Task> taskList7) {
        this.taskList7 = taskList7;
    }

    @XmlTransient
    public List<Task> getTaskList8() {
        return taskList8;
    }

    public void setTaskList8(List<Task> taskList8) {
        this.taskList8 = taskList8;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Organizationalentity)) {
            return false;
        }
        Organizationalentity other = (Organizationalentity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.unitedofoq.fabs.jbpm.entity.Organizationalentity[ id=" + id + " ]";
    }
    
}
