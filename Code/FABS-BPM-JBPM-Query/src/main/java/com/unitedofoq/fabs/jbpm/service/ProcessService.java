/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.jbpm.service;

import com.unitedofoq.fabs.jbpm.model.PageableDomain;
import com.unitedofoq.fabs.jbpm.model.ProcessInstance;
import com.unitedofoq.fabs.jbpm.repository.ProcessInstanceRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author mmasoud
 */
@Component
@Transactional(readOnly = true)
public class ProcessService {
    
    @Resource
    private ProcessInstanceRepository processInstanceRepository;
    
    public PageableDomain<List<ProcessInstance>> getAllProcessInstances(String Initiator, String sortedField, String sortingOrder, Map<String, String> filters, int page, int size) {
        Map<String, Object> queryData = getQueryData(Initiator, sortedField, sortingOrder, filters);
        PageableDomain<List<ProcessInstance>> userTasksResp = processInstanceRepository
                .findAllProcessInstances((String) queryData.get("filter")
                        ,(Map<String, Object>) queryData.get("params")
                        ,new PageRequest(page, size));
        return userTasksResp;
    }
    
    public String getLatestDeploymentId(){
        return processInstanceRepository.findLatestDeploymentId();
    }
    
    private Map<String, Object> getQueryData(String Initiator, String sortedField, String sortingOrder, Map<String, String> filterList) {
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> params = new HashMap<>();
        StringBuilder filter = new StringBuilder();
        if(Initiator != null){
            filter.append("AND v.value = '").append(Initiator).append("' ");
        }
        if (filterList != null) {
            for (Map.Entry entry : filterList.entrySet()) {
                String key = (String) entry.getKey();
                key = key.split("\\.")[1];
                   params.put(key, entry.getValue());
                if (key.equals("startDate")) {
                    filter.append(" AND ").append("CONCAT(CONCAT(cast(cast(p.startDate as date) as string),' '), SUBSTRING(cast(cast(p.startDate as time) as string),1,8))").append(" LIKE CONCAT('%', :")
                            .append(key).append(",'%') ");
                } else {
                    filter.append(" AND ").append("CAST(").append(entry.getKey()).append(" as string)").append(" LIKE CONCAT('%', :")
                            .append(key).append(",'%') ");
                }
            }
        }
        filter.append(" ORDER BY ").append(sortedField).append(" ").append(sortingOrder);
        data.put("filter", filter.toString());
        data.put("params", params);
        return data;
    }
}
