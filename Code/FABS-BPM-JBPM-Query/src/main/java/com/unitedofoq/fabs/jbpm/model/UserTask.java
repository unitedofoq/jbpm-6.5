/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.jbpm.model;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bgalal
 */
@XmlRootElement
public class UserTask implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private Long id;

    private String description;

    private String name;

    private int priority;

    private String subject;

    private Date activationTime;

    private String createdOn;
    
    private Date expirationTime;

    private String deploymentId;

    private String processId;

    private long processInstanceId;

    private long processSessionId;

    private String status;

    private long workItemId;

    private String taskType;
    
    private String createdByid;
    
    private String actualOwnerid;

    private Map<String, Object> parameters;

    public UserTask(Long id, String description, String name, int priority, 
            String subject, Date activationTime, String createdOn, Date expirationTime,
            String deploymentId, String processId, long processInstanceId, 
            long processSessionId, String status, long workItemId, 
            String taskType, String createdByid, String actualOwnerid) 
            throws ClassNotFoundException, IOException {
        this.id = id;
        this.description = description;
        this.name = name;
        this.priority = priority;
        this.subject = subject;
        this.activationTime = activationTime;
        this.createdOn = createdOn;
        this.deploymentId = deploymentId;
        this.processId = processId;
        this.processInstanceId = processInstanceId;
        this.processSessionId = processSessionId;
        this.status = status;
        this.workItemId = workItemId;
        this.taskType = taskType;
        this.createdByid = createdByid;
        this.actualOwnerid = actualOwnerid;
        this.expirationTime = expirationTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getActivationTime() {
        return activationTime;
    }

    public void setActivationTime(Date activationTime) {
        this.activationTime = activationTime;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Date getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Date expirationTime) {
        this.expirationTime = expirationTime;
    }

    public String getDeploymentId() {
        return deploymentId;
    }

    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public long getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(long processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public long getProcessSessionId() {
        return processSessionId;
    }

    public void setProcessSessionId(long processSessionId) {
        this.processSessionId = processSessionId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getWorkItemId() {
        return workItemId;
    }

    public void setWorkItemId(long workItemId) {
        this.workItemId = workItemId;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getCreatedByid() {
        return createdByid;
    }

    public void setCreatedByid(String createdByid) {
        this.createdByid = createdByid;
    }

    public String getActualOwnerid() {
        return actualOwnerid;
    }

    public void setActualOwnerid(String actualOwnerid) {
        this.actualOwnerid = actualOwnerid;
    }
    
    

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    @Override
    public String toString() {
        return "com.unitedofoq.fabs.jbpm.model.UserTask[ id=" + id + " ]";
    }
}
