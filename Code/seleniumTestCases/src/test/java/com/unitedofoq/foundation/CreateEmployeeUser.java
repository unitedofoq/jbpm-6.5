package com.unitedofoq.foundation;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import com.unitedofoq.fabsbase.BaseClass;

/**
 * 
 * @author mashraf need to run add employee testcase before create employee user
 */
public class CreateEmployeeUser extends BaseClass {
  private StringBuffer verificationErrors = new StringBuffer();
  private String user_name;
  private String password;
  private String email;
  private String emp_code;
  private String module;
  private String first_language;
  private String second_language;
  private String firstRole;
  private String secondRole;
  private String emp_name;


  @Before
  public void setUp() throws Exception {
    user_name = "TestEmp" + (int) random;
    emp_code = System.getProperty("empcode");
    // emp_code = "Test_202.0";
    password = "pass";
    email = user_name + "@unitedofoq.com";
    module = "FOUNDATION";
    first_language = "English";
    second_language = "Arabic";
    firstRole = "OTMSUser4E";
    secondRole = "coreuser";
    login();
  }

  @Test
  public void testCreateUser() throws Exception {
    // open manage users
    driver.findElement(By.linkText("Foundation")).click();
    driver.findElement(By.linkText("Create Employee's User")).click();
    driverManager.waitForPageToLoad();

    driverManager.switchToFrame("Fun_EmpUser_T_X_0001");
    // new row
    driverManager.waitForElement(By.xpath("//*[@title='New Row']"), reverseCondition);
    driver.findElement(By.xpath("//*[@title='New Row']")).click();
    // enter username
    driverManager.waitForElement(
        By.xpath("//*[@id='tf:Fun_EmpUser_T_X_0001_generalTable:0:loginName_TextField_0']"),
        reverseCondition);
    driverManager.findElementByXpath(
        "//*[@id='tf:Fun_EmpUser_T_X_0001_generalTable:0:loginName_TextField_0']").click();
    driverManager
        .findElementByXpath(
            "//*[@id='tf:Fun_EmpUser_T_X_0001_generalTable:0:loginName_TextField_0']")
        .sendKeys(user_name);
    // enter password
    driverManager.findElementByXpath(
        "//*[@id='tf:Fun_EmpUser_T_X_0001_generalTable:0:password_PasswordInput_1']").click();
    driverManager
        .findElementByXpath(
            "//*[@id='tf:Fun_EmpUser_T_X_0001_generalTable:0:password_PasswordInput_1']")
        .sendKeys(password);
    // choose employee
    driverManager.findElementByXpath("//*[@title='employee.nameTranslated']").click();

    driverManager.switchToLookUp("PER_EmpList_T_X_SM_ONEFilter");
    driverManager.checkFilter(emp_code,
        "tf:PER_EmpList_T_X_SM_ONEFilter_generalTable:column0:filter",
        "//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable_data']/tr/td[2]/div/table/tbody/tr/td/label");
    emp_name = driverManager
        .findElementByXpath(
            "//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable_data']/tr/td[3]/div/table/tbody/tr/td/label")
        .getText();
    driverManager
        .findElementByXpath(
            "//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable_data']/tr/td[2]/div/table/tbody/tr/td")
        .click();
    driverManager.findElementByXpath("//*[@title='Pick']").click();
    driverManager.switchToFrame("Fun_EmpUser_T_X_0001");
    driverManager.checkAttributeValue(emp_name,
        "//*[@id='tf:Fun_EmpUser_T_X_0001_generalTable:0:j_id65_LookupInputText']", "value",
        reverseCondition);
    // select first language
    driverManager.findElementByXpath("//*[@title='firstLanguage.valueTranslated']").click();
    driverManager.switchToLookUp("IndicatorNature");
    driverManager.checkFilter(first_language, "tf:IndicatorNature_generalTable:column1:filter",
        "//*[@id='tf:IndicatorNature_generalTable_data']/tr/td[4]/div/span/span/table/tbody/tr/td/label");
    driverManager
        .findElementByXpath(
            "//*[@id='tf:IndicatorNature_generalTable_data']/tr/td[4]/div/span/span/table/tbody/tr/td")
        .click();
    driverManager.findElementByXpath("//*[@title='Pick']").click();
    driverManager.switchToFrame("Fun_EmpUser_T_X_0001");
    driverManager.checkAttributeValue(first_language,
        "//*[@id='tf:Fun_EmpUser_T_X_0001_generalTable:0:j_id87_LookupInputText']", "value",
        reverseCondition);
    // select second language
    driverManager.findElementByXpath("//*[@title='secondLanguage.valueTranslated']").click();
    driverManager.switchToLookUp("IndicatorNature");
    driverManager.checkFilter(second_language, "tf:IndicatorNature_generalTable:column1:filter",
        "//*[@id='tf:IndicatorNature_generalTable_data']/tr/td[4]/div/span/span/table/tbody/tr/td/label");
    driverManager
        .findElementByXpath(
            "//*[@id='tf:IndicatorNature_generalTable_data']/tr/td[4]/div/span/span/table/tbody/tr/td")
        .click();
    driverManager.findElementByXpath("//*[@title='Pick']").click();
    driverManager.switchToFrame("Fun_EmpUser_T_X_0001");
    driverManager.checkAttributeValue(second_language,
        "//*[@id='tf:Fun_EmpUser_T_X_0001_generalTable:0:j_id109_LookupInputText']", "value",
        reverseCondition);
    // select module
    driverManager
        .findElementByXpath(
            "//*[@id='tf:Fun_EmpUser_T_X_0001_generalTable:0:omodule_moduleTitleTranslated_Drop-down_6']/div[2]/span")
        .click();
    driverManager.findElementByXpath(
        "//*[@id='tf:Fun_EmpUser_T_X_0001_generalTable:0:omodule_moduleTitleTranslated_Drop-down_6_panel']/div/ul/li[text()='"
            + module + "']")
        .click();
    // select email
    driverManager
        .findElementByXpath("//*[@id='tf:Fun_EmpUser_T_X_0001_generalTable:0:email_TextField_7']")
        .click();
    driverManager
        .findElementByXpath("//*[@id='tf:Fun_EmpUser_T_X_0001_generalTable:0:email_TextField_7']")
        .sendKeys(email);
    // save
    driverManager.findElementByXpath(
        "//*[@id='tf:Fun_EmpUser_T_X_0001_generalTable:0:j_id32_rowEditor']/span[2]").click();
    driverManager.waitForSuccess();
    // select the employee
    driverManager
        .findElementByXpath(
            "//*[@id='tf:Fun_EmpUser_T_X_0001_generalTable_data']/tr/td[3]/div/span/span/table/tbody/tr/td")
        .click();
    // choose role
    driverManager.switchToFrame("UserRole");
    // add first role
    driverManager.findElementByXpath("//*[@title='New Row']").click();
    driverManager.waitForElement(
        By.xpath("//button[contains(@id,'tf:UserRole_generalTable:0:orole_name_Lookup__')]"),
        reverseCondition);
    driverManager.findElementByXpath(
        "//button[contains(@id,'tf:UserRole_generalTable:0:orole_name_Lookup__')]").click();

    driverManager.switchToLookUp("LookupRole");
    driverManager.checkFilter(firstRole, "tf:LookupRole_generalTable:column0:filter",
        "//*[@id='tf:LookupRole_generalTable:0:j_id76_ColCommLINK']");
    driverManager
        .findElementByXpath(
            "//*[@id='tf:LookupRole_generalTable_data']/tr/td[3]/div/span/span/table/tbody/tr/td")
        .click();
    driverManager.findElementByXpath("//*[@title='Pick']").click();

    driverManager.switchToFrame("UserRole");
    driverManager.checkAttributeValue(firstRole,
        "//*[@id='tf:UserRole_generalTable:0:j_id59_LookupInputText']", "value", reverseCondition);
    // save first role
    driverManager
        .findElementByXpath("//*[@id='tf:UserRole_generalTable:0:j_id32_rowEditor']/span[2]")
        .click();
    driverManager.waitForSuccess();
    // add second role
    driverManager.findElementByXpath("//*[@title='New Row']").click();
    driverManager.waitForElement(
        By.xpath("//button[contains(@id,'tf:UserRole_generalTable:0:orole_name_Lookup__')]"),
        reverseCondition);
    driverManager.findElementByXpath(
        "//button[contains(@id,'tf:UserRole_generalTable:0:orole_name_Lookup__')]").click();

    driverManager.switchToLookUp("LookupRole");

    driverManager.checkFilter(secondRole, "tf:LookupRole_generalTable:column0:filter",
        "//*[@id='tf:LookupRole_generalTable:0:j_id76_ColCommLINK']");
    driverManager
        .findElementByXpath(
            "//*[@id='tf:LookupRole_generalTable_data']/tr/td[3]/div/span/span/table/tbody/tr/td")
        .click();
    driverManager.findElementByXpath("//*[@title='Pick']").click();

    driverManager.switchToFrame("UserRole");
    driverManager.checkAttributeValue(secondRole,
        "//*[@id='tf:UserRole_generalTable:0:j_id59_LookupInputText']", "value", reverseCondition);
    // save second role
    driverManager
        .findElementByXpath("//*[@id='tf:UserRole_generalTable:0:j_id32_rowEditor']/span[2]")
        .click();
    driverManager.waitForSuccess();
    driver.switchTo().defaultContent();
    logout();
    login(user_name, password);

    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }


}
