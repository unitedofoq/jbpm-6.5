package com.unitedofoq.foundation;


import static junit.framework.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unitedofoq.fabsbase.BaseClass;
import com.unitedofoq.fabscore.Entity;

@ContextConfiguration("classpath:/resources/testConfig.xml")
@RunWith(SpringJUnit4ClassRunner.class)

public class LookUpTree extends BaseClass {
  @Autowired
  JdbcTemplate jdbc;
  String count = "0";

  Entity en = new Entity();

  @Before
  public void setUp() {

    try {


      // select count for units
      count = getUnitCountDB();
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }

    login();
    driver.findElement(By.xpath("//span[contains(.,'Foundation')]")).click();
  }

  public String getUnitCountDB() {
    return jdbc.queryForObject(
        "select COUNT(*) from unit where parentunit_dbid = '491262' and deleted = 0 and active = 1",
        Object.class).toString();
  }

  @Test
  public void companyUnitDisplayingCheck() {
    try {
      driver.findElement(By.xpath("//span[contains(.,'Company Management')]")).click();
      driverManager.waitForPageToLoad();
      driverManager.waitForElement(By.cssSelector("iframe[title='FormUnit']"), reverseCondition);
      driverManager.switchToFrame("FormUnit");
      // open unit tree of parent unit
      driver.findElement(By.xpath("//button[@title='parentUnit.nameTranslated']")).click();

      driverManager.switchToLookUp("FUN_UNITTREE_X_0002_Filter");
      int ec = countingUnits();
      assertEquals(count, ec);
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  public int countingUnits() {
    driverManager.findElementByXpath(".//*[@id='frmScrForm:multiLevelTree:0_0']/span/span[1]")
        .click();
    // count elements using specific classes
    int ec =
        driver.findElements(By.xpath(".//*[@class='ui-tree-toggler ui-icon ui-icon-triangle-1-e']"))
            .size();
    return ec;
  }

}
