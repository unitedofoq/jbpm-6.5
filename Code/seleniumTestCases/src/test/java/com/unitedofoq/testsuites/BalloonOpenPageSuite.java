package com.unitedofoq.testsuites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.unitedofoq.personnel.AddEmployee;
import com.unitedofoq.personnel.BalloonOpenPage;
import com.unitedofoq.personnel.CreateUnit;

@RunWith(Suite.class)
@SuiteClasses({CreateUnit.class, AddEmployee.class, BalloonOpenPage.class})
public class BalloonOpenPageSuite {

}
