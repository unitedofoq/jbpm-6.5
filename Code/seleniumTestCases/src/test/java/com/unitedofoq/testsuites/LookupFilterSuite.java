package com.unitedofoq.testsuites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.unitedofoq.personnel.CreateUnit;
import com.unitedofoq.personnel.LookupFilter;

@RunWith(Suite.class)
@SuiteClasses({CreateUnit.class, LookupFilter.class})
public class LookupFilterSuite {

}
