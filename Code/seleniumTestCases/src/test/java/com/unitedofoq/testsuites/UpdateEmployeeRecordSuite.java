package com.unitedofoq.testsuites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.unitedofoq.personnel.AddEmployee;
import com.unitedofoq.personnel.CreateUnit;
import com.unitedofoq.personnel.UpdateEmployeeRecord;

@RunWith(Suite.class)
@SuiteClasses({CreateUnit.class, AddEmployee.class, UpdateEmployeeRecord.class})
public class UpdateEmployeeRecordSuite {

}
