package com.unitedofoq.testsuites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.unitedofoq.administration.CreateUser;
import com.unitedofoq.administration.RemovePortlet;
import com.unitedofoq.fabscore.Login;

@RunWith(Suite.class)
@SuiteClasses({CreateUser.class, Login.class, RemovePortlet.class})
public class CreateUserSuite {

}
