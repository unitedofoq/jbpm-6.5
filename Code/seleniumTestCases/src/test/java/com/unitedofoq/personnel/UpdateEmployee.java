package com.unitedofoq.personnel;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.unitedofoq.fabsbase.BaseClass;

public class UpdateEmployee extends BaseClass {

  private StringBuffer verificationErrors = new StringBuffer();
  private String emp_code;
  private String emp_gender;
  private String emp_religion;
  private String birth_date;
  private String Previous_Experience_Months;
  private String Previous_Experience_Years;

  @Before
  public void setUp() throws Exception {
    // emp_code = "Test_" + random;
    emp_code = System.getProperty("empcode");
    // unit_name = "TestUnit_627.0";
    emp_gender = "Male";
    emp_religion = "Muslim";
    birth_date = "26/06/1986";
    Previous_Experience_Months = "";
    Previous_Experience_Years = "";
    login();
  }

  @Test
  public void testAddEmployee() throws Exception {
    driver.findElement(By.linkText("Personnel")).click();
    driver.findElement(By.linkText("Employees Management")).click();
    driverManager.waitForPageToLoad();
    // search for employee
    driverManager.switchToFrame("FUN_AddNewEmpWithUnit_T_X_30");
    driverManager.findElementByXpath(
        "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:column2:filter']").clear();
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:column2:filter']")
        .sendKeys(emp_code);
    driverManager
        .findElementByXpath(
            "//*[@id=\"tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:column2:filter\"]")
        .sendKeys(Keys.RETURN);
    driverManager.checkValue(emp_code,
        "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:cell2']/span[1]/table/tbody/tr/td/label");

    driverManager.switchToFrame("Fun_empdetl_F_01");
    // edit birthdate
    driverManager.waitForElement(By.xpath("//*[@id=\"frmScrForm:birthdate_Calendar_5_input\"]"),
        reverseCondition);
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:birthdate_Calendar_5_input\"]").clear();
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:birthdate_Calendar_5_input\"]")
        .sendKeys(birth_date);
    // to close birthday calendar will click on title two times
    driverManager
        .findElementByXpath("//*[@id='frmScrForm:salute_valueTranslated_Drop-down_3']/div[2]/span")
        .click();
    driverManager
        .findElementByXpath("//*[@id='frmScrForm:salute_valueTranslated_Drop-down_3']/div[2]/span")
        .click();
    // edit gender
    driverManager
        .findElementByXpath("//*[@id=\"frmScrForm:gender_valueTranslated_Drop-down_7_label\"]")
        .click();
    driverManager.findElementByXpath(
        "//*[@id='frmScrForm:gender_valueTranslated_Drop-down_7_panel']/div/ul/li[text()='"
            + emp_gender + "']")
        .click();
    // edit religion
    driverManager
        .findElementByXpath("//*[@id=\"frmScrForm:religion_valueTranslated_Drop-down_11_label\"]")
        .click();
    driverManager.findElementByXpath(
        "//*[@id='frmScrForm:religion_valueTranslated_Drop-down_11_panel']/div/ul/li[text()='"
            + emp_religion + "']")
        .click();
    // edit Previous_Experience_Years
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:prevousExperienceYears_TextField_17\"]")
        .clear();
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:prevousExperienceYears_TextField_17\"]")
        .sendKeys(Previous_Experience_Years);
    // edit Previous_Experience_Months
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:prevousExperienceMonths_TextField_18\"]")
        .clear();
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:prevousExperienceMonths_TextField_18\"]")
        .sendKeys(Previous_Experience_Months);
    // save edits
    driverManager.findElementByXpath("//*[@title='Submit']").click();
    driverManager.waitForSuccess();
    // verify edits
    try {
      assertEquals(birth_date,
          driverManager.findElementByXpath("//*[@id=\"frmScrForm:birthdate_Calendar_5_input\"]")
              .getAttribute("value"));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(emp_gender, driverManager
          .findElementByXpath("//*[@id=\"frmScrForm:gender_valueTranslated_Drop-down_7_label\"]")
          .getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(emp_religion, driverManager
          .findElementByXpath("//*[@id=\"frmScrForm:religion_valueTranslated_Drop-down_11_label\"]")
          .getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(Previous_Experience_Years,
          driverManager
              .findElementByXpath("//*[@id=\"frmScrForm:prevousExperienceYears_TextField_17\"]")
              .getAttribute("value"));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(Previous_Experience_Months,
          driverManager
              .findElementByXpath("//*[@id=\"frmScrForm:prevousExperienceMonths_TextField_18\"]")
              .getAttribute("value"));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }

    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }


}
