package com.unitedofoq.personnel;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import com.unitedofoq.fabsbase.BaseClass;

/**
 * 
 * @author mashraf
 * @requires CreateUnit.class to run before it
 */
public class LookupFilter extends BaseClass {
  private StringBuffer verificationErrors = new StringBuffer();
  private String unitName;
  private String positionName;


  @Before
  public void setUp() throws Exception {
    unitName = System.getProperty("unitname");
    // unitName = "TestUnit_767.0";
    positionName = unitName + " Manager";
    login();
  }

  @Test
  public void testLookupFilter() throws Exception {
    driver.findElement(By.linkText("Personnel")).click();
    driver.findElement(By.linkText("Employees Management")).click();
    driverManager.waitForPageToLoad();

    driverManager.switchToFrame("FUN_AddNewEmpWithUnit_T_X_30");
    driverManager.waitForElement(By.xpath("//*[@title='New Row']"), reverseCondition);
    // add new employee
    driverManager.findElementByXpath("//*[@title='New Row']").click();
    driverManager.waitForElement(By.xpath("//button[@title='unitFilter.nameTranslated']"),
        reverseCondition);
    driverManager.findElementByXpath("//button[@title='unitFilter.nameTranslated']").click();
    // pick a unit
    driverManager.switchToLookUp("FUN_UnitList_T_X_0005");
    driverManager.waitForElement(
        By.xpath("//*[@id=\"tf:FUN_UnitList_T_X_0005_generalTable:column0:filter\"]"),
        reverseCondition);
    driverManager.checkFilter(unitName, "tf:FUN_UnitList_T_X_0005_generalTable:column0:filter",
        "//*[@id='tf:FUN_UnitList_T_X_0005_generalTable_data']/tr/td[2]/div/table/tbody/tr/td/label");
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_UnitList_T_X_0005_generalTable_data']/tr/td[2]/div/table/tbody/tr/td")
        .click();
    driverManager.findElementByXpath("//*[@title='Pick']").click();

    driverManager.switchToFrame("FUN_AddNewEmpWithUnit_T_X_30");
    driverManager.checkAttributeValue(unitName,
        "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:j_id87_LookupInputText']", "value",
        reverseCondition);
    // pick a position
    driverManager.findElementByXpath("//button[@title='positionSimpleMode.nameTranslated']")
        .click();

    driverManager.switchToLookUp("FUN_Pos_Lookup_T_0003");
    driverManager.waitForElement(
        By.xpath("//*[@id='tf:FUN_Pos_Lookup_T_0003_generalTable:column0:filter']"),
        reverseCondition);
    driverManager.checkFilter(positionName, "tf:FUN_Pos_Lookup_T_0003_generalTable:column0:filter",
        "//*[@id='tf:FUN_Pos_Lookup_T_0003_generalTable_data']/tr/td[2]/div/table/tbody/tr/td/label");

    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    System.setProperty("unitname", unitName);
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

}
