package com.unitedofoq.personnel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.unitedofoq.fabsbase.BaseClass;

public class PenaltyEntry extends BaseClass {
  private StringBuffer verificationErrors = new StringBuffer();
  private String emp_code;
  private String penalty_type;
  private String penalty_date;
  private String penalty_value;
  private String penalty_reason;
  private String notes;
  private String start_date;
  private String end_date;
  private double p_random;
  private String emp_name;

  @Before
  public void setUp() throws Exception {
    start_date = "11/01/2014";
    end_date = "11/01/2014";
    p_random = Math.floor((Math.random() * 1000) + 1);
    emp_code = "Test_954.0";
    // emp_code = System.getProperty("empcode");
    penalty_type = "Penalty (Days)";
    penalty_date = "11/01/2014";
    penalty_value = "1.000";
    penalty_reason = "Delay";
    notes = emp_code + "_" + start_date + "_" + end_date + "_" + p_random;
    login();
  }

  @Test
  public void testPenaltyEntry() throws Exception {
    driver.findElement(By.linkText("Personnel")).click();
    driver.findElement(By.linkText("Employee's Penalty")).click();
    driverManager.waitForPageToLoad();

    driverManager.switchToFrame("PER_EmpList_T_X_SM_ONEFilter");
    driverManager.findElementByXpath(
        "//*[@id=\"tf:PER_EmpList_T_X_SM_ONEFilter_generalTable:column0:filter\"]").clear();
    driverManager.findElementByXpath(
        "//*[@id=\"tf:PER_EmpList_T_X_SM_ONEFilter_generalTable:column0:filter\"]")
        .sendKeys(emp_code);
    driverManager.findElementByXpath(
        "//*[@id=\"tf:PER_EmpList_T_X_SM_ONEFilter_generalTable:column0:filter\"]")
        .sendKeys(Keys.ENTER);
    driverManager.checkValue(emp_code,
        "//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable_data']/tr/td[2]/div/table/tbody/tr/td/label");

    driverManager.findElementByXpath(
        "//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable_data']/tr/td[2]/div/table/tbody/tr/td")
        .click();
    emp_name = driverManager
        .findElementByXpath(
            "//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable_data']/tr/td[3]/div/table/tbody/tr/td/label")
        .getText();

    driverManager.switchToFrame("PRS_EmpPenHist_T_0001_all");
    driver.findElement(By.xpath("//*[@title='Add']")).click();

    driverManager.switchToLookUp("PRS_EmpPenEnt_F_0001_all");
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:penalty_descriptionTranslated_Drop-down_1_label\"]")
        .click();
    driver.findElement(By.xpath(
        "//*[@id='frmScrForm:penalty_descriptionTranslated_Drop-down_1_panel']/div/ul/li[text()='"
            + penalty_type + "']"))
        .click();
    // waitForElement(By.xpath("//*[@id=\"frmScrForm:penaltyDate_Calendar_2_input\"]"));
    driver.findElement(By.xpath("//*[@id=\"frmScrForm:penaltyDate_Calendar_2_input\"]")).clear();
    driver.findElement(By.xpath("//*[@id=\"frmScrForm:penaltyDate_Calendar_2_input\"]")).clear();
    driver.findElement(By.xpath("//*[@id=\"frmScrForm:penaltyDate_Calendar_2_input\"]"))
        .sendKeys(penalty_date);
    driver.findElement(By.xpath("//*[@id=\"frmScrForm:penaltyDate_Calendar_2_input\"]"))
        .sendKeys(Keys.ENTER);

    driver.findElement(By.xpath("//*[@id=\"frmScrForm:penaltyValue_TextField_3\"]")).clear();
    driver.findElement(By.xpath("//*[@id=\"frmScrForm:penaltyValue_TextField_3\"]"))
        .sendKeys(penalty_value);
    driver.findElement(By.xpath("//*[@id='frmScrForm:slide4']/tbody/tr/td/label")).click();
    driverManager.findElementByXpath("//*[@id='frmScrForm:penaltyReason_value_Drop-down_4']/div[2]/span")
        .click();
    driverManager.findElementByXpath(
        "//*[@id='frmScrForm:penaltyReason_value_Drop-down_4_panel']/div/ul/li[text()='Delay']")
        .click();
    driver.findElement(By.xpath("//*[@title='penaltyNoApprovedby.nameTranslated']")).click();

    driverManager.switchToLookUp("lookup_employee_sc1");
    driver.findElement(By.xpath("//*[@id=\"tf:lookup_employee_sc1_generalTable:column1:filter\"]"))
        .clear();
    driver.findElement(By.xpath("//*[@id=\"tf:lookup_employee_sc1_generalTable:column1:filter\"]"))
        .sendKeys(emp_name);
    driver.findElement(By.xpath("//*[@id=\"tf:lookup_employee_sc1_generalTable:column1:filter\"]"))
        .sendKeys(Keys.ENTER);
    driverManager.checkValue(emp_name,
        "//*[@id='tf:lookup_employee_sc1_generalTable_data']/tr/td[3]/div/table/tbody/tr/td/label");

    driverManager.findElementByXpath(
        "//*[@id='tf:lookup_employee_sc1_generalTable_data']/tr/td[3]/div/table/tbody/tr/td")
        .click();
    driver.findElement(By.xpath("//*[@title='Pick']")).click();

    driverManager.switchToLookUp("PRS_EmpPenEnt_F_0001_all");
    driver.findElement(By.xpath("//button[@title='givenBy.nameTranslated']")).click();

    driverManager.switchToLookUp("lookup_employee_sc1");
    driver.findElement(By.xpath("//*[@id=\"tf:lookup_employee_sc1_generalTable:column1:filter\"]"))
        .clear();
    driver.findElement(By.xpath("//*[@id=\"tf:lookup_employee_sc1_generalTable:column1:filter\"]"))
        .sendKeys(emp_name);
    driver.findElement(By.xpath("//*[@id=\"tf:lookup_employee_sc1_generalTable:column1:filter\"]"))
        .sendKeys(Keys.ENTER);
    driverManager.checkValue(emp_name,
        "//*[@id='tf:lookup_employee_sc1_generalTable_data']/tr/td[3]/div/table/tbody/tr/td/label");

    driverManager.findElementByXpath(
        "//*[@id='tf:lookup_employee_sc1_generalTable_data']/tr/td[3]/div/table/tbody/tr/td")
        .click();
    driver.findElement(By.xpath("//*[@title='Pick']")).click();

    driverManager.switchToLookUp("PRS_EmpPenEnt_F_0001_all");
    driver.findElement(By.xpath("//*[@id=\"frmScrForm:notes_TextField_9\"]")).clear();
    driver.findElement(By.xpath("//*[@id=\"frmScrForm:notes_TextField_9\"]")).sendKeys(notes);
    driver.findElement(By.xpath("//button[@title='Submit & Close']")).click();

    driverManager.switchToFrame("PER_EmpList_T_X_SM_ONEFilter");
    driverManager.findElementByXpath(
        "//*[@id=\"tf:PER_EmpList_T_X_SM_ONEFilter_generalTable:column0:filter\"]").clear();
    driverManager.findElementByXpath(
        "//*[@id=\"tf:PER_EmpList_T_X_SM_ONEFilter_generalTable:column0:filter\"]")
        .sendKeys(emp_code);
    driverManager.findElementByXpath(
        "//*[@id=\"tf:PER_EmpList_T_X_SM_ONEFilter_generalTable:column0:filter\"]")
        .sendKeys(Keys.ENTER);

    driverManager.checkValue(emp_code,
        "//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable_data']/tr/td[2]/div/table/tbody/tr/td/label");

    driverManager.findElementByXpath(
        "//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable_data']/tr/td[2]/div/table/tbody/tr/td")
        .click();

    driverManager.switchToFrame("PRS_EmpPenHist_T_0001_all");
    driverManager.findElementByXpath("//*[@id='tf:PRS_EmpPenHist_T_0001_all_generalTable:column5:filter']")
        .clear();
    driverManager.findElementByXpath("//*[@id='tf:PRS_EmpPenHist_T_0001_all_generalTable:column5:filter']")
        .sendKeys(notes);
    driverManager.findElementByXpath("//*[@id='tf:PRS_EmpPenHist_T_0001_all_generalTable:column5:filter']")
        .sendKeys(Keys.ENTER);
    driverManager.checkValue(notes,
        "//*[@id='tf:PRS_EmpPenHist_T_0001_all_generalTable_data']/tr/td[8]/div/table/tbody/tr/td/label");

    try {
      assertEquals(penalty_type,
          driver
              .findElement(By.xpath(
                  "//*[@id='tf:PRS_EmpPenHist_T_0001_all_generalTable_data']/tr/td[3]/div/table/tbody/tr/td/label"))
          .getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(penalty_date,
          driver
              .findElement(By.xpath(
                  "//*[@id=\"tf:PRS_EmpPenHist_T_0001_all_generalTable:0:j_id54_ColTXT_input\"]"))
          .getAttribute("value"));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(penalty_value,
          driver
              .findElement(By.xpath(
                  "//*[@id='tf:PRS_EmpPenHist_T_0001_all_generalTable_data']/tr/td[5]/div/table/tbody/tr/td/label"))
          .getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(emp_name,
          driver
              .findElement(By.xpath(
                  "//*[@id='tf:PRS_EmpPenHist_T_0001_all_generalTable_data']/tr/td[7]/div/table/tbody/tr/td/label"))
          .getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(penalty_reason,
          driver
              .findElement(By.xpath(
                  "//*[@id='tf:PRS_EmpPenHist_T_0001_all_generalTable_data']/tr/td[6]/div/table/tbody/tr/td/label"))
          .getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

}
