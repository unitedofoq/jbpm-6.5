package com.unitedofoq.personnel;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import com.unitedofoq.fabsbase.BaseClass;

/**
 * 
 * @author mashraf
 * @requires AddEmployee
 */
public class BalloonOpenPage extends BaseClass {

  private StringBuffer verificationErrors = new StringBuffer();
  private String employeeCode;

  @Before
  public void setUp() throws Exception {
    employeeCode = System.getProperty("empcode");
    // employeeCode = "Test_791.0";
    login();
  }

  @Test
  public void testBalloonOpenPage() throws Exception {
    driver.findElement(By.linkText("Personnel")).click();
    driver.findElement(By.linkText("Employees Management")).click();
    driverManager.waitForPageToLoad();
    // search for employee
    driverManager.switchToFrame("FUN_AddNewEmpWithUnit_T_X_30");
    driverManager.checkFilter(employeeCode,
        "tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:column2:filter",
        "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:cell2']/span[1]/table/tbody/tr/td/label");
    // open balloon
    driverManager.findElementByXpath(
        "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:openButton_15']").click();
    // click Employee's income and deduction
    driverManager
        .findElementByXpath(
            "//*[@id='outputBody1']/div[44]/table/tbody/tr/td[1]/div/table[7]/tbody/tr/td[2]/a")
        .click();
    // Thread.sleep(20000);
    driverManager.waitForPageToLoad(3);
    driverManager.switchToFrame("PAY_EmpSalr_T_x_0001");
    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    System.setProperty("empcode", employeeCode);
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }


}
