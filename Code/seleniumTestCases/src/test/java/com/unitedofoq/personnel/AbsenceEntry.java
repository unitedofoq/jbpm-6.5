package com.unitedofoq.personnel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.unitedofoq.fabsbase.BaseClass;

public class AbsenceEntry extends BaseClass {


  private StringBuffer verificationErrors = new StringBuffer();
  private double a_random;
  private String emp_code;
  private String absence_type;
  private String start_date;
  private String start_time;
  private String end_date;
  private String end_time;
  private String notes;

  @Before
  public void setUp() throws Exception {
    a_random = Math.floor((Math.random() * 1000) + 1);
    // emp_code = "Test_462.0";
    emp_code = System.getProperty("empcode");
    absence_type = "Early Leave";
    start_date = "12/02/2014";
    start_time = "10:50";
    end_date = "12/02/2014";
    end_time = "12:30";
    notes = emp_code + "_" + start_date + "_" + end_date + "_" + a_random;
    login();

  }

  @Test
  public void testAbsenceEntry() throws Exception {
    driver.findElement(By.linkText("Personnel")).click();
    driver.findElement(By.linkText("Employee's Absence")).click();
    driverManager.waitForPageToLoad();

    driverManager.switchToFrame("PER_EmpList_T_X_SM_ONEFilter");
    driverManager.findElementByXpath(
        "//*[@id=\"tf:PER_EmpList_T_X_SM_ONEFilter_generalTable:column0:filter\"]").clear();
    driverManager
        .findElementByXpath(
            "//*[@id=\"tf:PER_EmpList_T_X_SM_ONEFilter_generalTable:column0:filter\"]")
        .sendKeys(emp_code);
    driverManager
        .findElementByXpath(
            "//*[@id=\"tf:PER_EmpList_T_X_SM_ONEFilter_generalTable:column0:filter\"]")
        .sendKeys(Keys.ENTER);

    driverManager.checkValue(emp_code,
        "//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable_data']/tr/td[2]/div/table/tbody/tr/td/label");

    driverManager
        .findElementByXpath(
            "//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable_data']/tr/td[2]/div/table/tbody/tr/td")
        .click();

    driverManager.switchToFrame("PRS_EmpAbsHist_T_0001_all");
    driverManager.waitForElement(By.xpath("//*[@title='Add']"), reverseCondition);
    driverManager.findElementByXpath("//*[@title='Add']").click();
    driver.switchTo().defaultContent();

    driverManager.switchToLookUp("PRS_EmpAbsEnt_F_0001_all");
    driverManager
        .findElementByXpath("//*[@id=\"frmScrForm:absence_description_Drop-down_3_label\"]")
        .click();
    driverManager.findElementByXpath(
        "//*[@id='frmScrForm:absence_description_Drop-down_3_panel']/div/ul/li[text()='"
            + absence_type + "']")
        .click();
    System.out.println("-------1------");
    driverManager.waitForElement(By.xpath("//*[@id=\"frmScrForm:startDate_Calendar_1_input\"]"),
        reverseCondition);
    System.out.println("-------2------");
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:startDate_Calendar_1_input\"]").clear();
    System.out.println("-------3------");
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:startDate_Calendar_1_input\"]")
        .sendKeys(start_date + " " + start_time);
    System.out.println("-------4------");
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:startDate_Calendar_1_input\"]")
        .sendKeys(Keys.ENTER);
    System.out.println("-------5------");
    // DM.findElementByXpath("//*[@id='ui-datepicker-div']/div[3]/button[2]")).click();
    driverManager.findElementByXpath("//*[@id='frmScrForm:endDate_Calendar_2_input']").clear();
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:endDate_Calendar_2_input\"]")
        .sendKeys(end_date + " " + end_time);
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:endDate_Calendar_2_input\"]")
        .sendKeys(Keys.ENTER);

    driverManager.findElementByXpath("//*[@id='ui-datepicker-div']/div[3]/button[2]").click();
    String period = driverManager.findElementByXpath("//*[@id='frmScrForm:periodMask_TextField_4']")
        .getAttribute("value");
    driverManager.findElementByXpath("//*[@id='frmScrForm:notes_TextArea_5']").clear();
    driverManager.findElementByXpath("//*[@id='frmScrForm:notes_TextArea_5']").sendKeys(notes);
    driverManager.findElementByXpath("//*[@title='Submit & Close']").click();
    driverManager.waitForElement(By.xpath("//*[@title='Submit & Close']"), reverseCondition);

    driverManager.switchToFrame("PER_EmpList_T_X_SM_ONEFilter");
    driverManager.findElementByXpath(
        "//*[@id=\"tf:PER_EmpList_T_X_SM_ONEFilter_generalTable:column0:filter\"]").clear();
    driverManager
        .findElementByXpath(
            "//*[@id=\"tf:PER_EmpList_T_X_SM_ONEFilter_generalTable:column0:filter\"]")
        .sendKeys(emp_code);
    driverManager
        .findElementByXpath(
            "//*[@id=\"tf:PER_EmpList_T_X_SM_ONEFilter_generalTable:column0:filter\"]")
        .sendKeys(Keys.ENTER);
    driverManager.checkValue(emp_code,
        "//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable_data']/tr/td[2]/div/table/tbody/tr/td/label");

    driverManager
        .findElementByXpath(
            "//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable_data']/tr/td[2]/div/table/tbody/tr/td")
        .click();

    driverManager.switchToFrame("PRS_EmpAbsHist_T_0001_all");
    driverManager
        .findElementByXpath("//*[@id='tf:PRS_EmpAbsHist_T_0001_all_generalTable:column6:filter']")
        .clear();
    driverManager
        .findElementByXpath("//*[@id='tf:PRS_EmpAbsHist_T_0001_all_generalTable:column6:filter']")
        .sendKeys(notes);
    driverManager
        .findElementByXpath("//*[@id='tf:PRS_EmpAbsHist_T_0001_all_generalTable:column6:filter']")
        .sendKeys(Keys.ENTER);
    driverManager.checkValue(notes,
        "//*[@id='tf:PRS_EmpAbsHist_T_0001_all_generalTable:0:j_id78_ColCommLINK']");

    try {
      assertEquals(absence_type,
          driverManager
              .findElementByXpath(
                  "//*[@id='tf:PRS_EmpAbsHist_T_0001_all_generalTable:0:cell0']/span[1]/table/tbody/tr/td/label")
              .getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(start_date + " " + start_time,
          driverManager
              .findElementByXpath(
                  "//*[@id='tf:PRS_EmpAbsHist_T_0001_all_generalTable:0:j_id60_ColTXT_input']")
              .getAttribute("value"));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(end_date + " " + end_time,
          driverManager
              .findElementByXpath(
                  "//*[@id='tf:PRS_EmpAbsHist_T_0001_all_generalTable:0:j_id63_ColTXT_input']")
              .getAttribute("value"));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(period,
          driverManager
              .findElementByXpath(
                  "//*[@id='tf:PRS_EmpAbsHist_T_0001_all_generalTable:0:cell3']/span[1]/table/tbody/tr/td/label")
              .getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

}
