package com.unitedofoq.personnel;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import com.unitedofoq.fabsbase.BaseClass;

/**
 * 
 * @author mashraf
 * @requires AddEmployee.class to run before it
 */
public class EmployeeDataFilter extends BaseClass {

  private StringBuffer verificationErrors = new StringBuffer();
  private String employeeCode;
  private String employeeName;

  @Before
  public void setUp() throws Exception {
    // employeeCode = "Test_190.0";
    employeeCode = System.getProperty("empcode");
    login();
  }

  @Test
  public void testEmployeeDataFilter() throws Exception {
    driver.findElement(By.linkText("Personnel")).click();
    driver.findElement(By.linkText("Employees Management")).click();
    driverManager.waitForPageToLoad();
    // search for employee
    driverManager.switchToFrame("FUN_AddNewEmpWithUnit_T_X_30");
    driverManager.checkFilter(employeeCode,
        "tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:column2:filter",
        "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:cell2']/span[1]/table/tbody/tr/td/label");
    employeeName = driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:cell4']/span[1]/table/tbody/tr/td/label")
        .getText();
    // search by english name
    driverManager.checkFilter(employeeName,
        "tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:column4:filter",
        "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:cell4']/span[1]/table/tbody/tr/td/label");
    // search by number part of English name
    driverManager.checkFilter(employeeName.substring(14),
        "tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:column4:filter",
        "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:cell4']/span[1]/table/tbody/tr/td/label");
    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    System.setProperty("empcode", employeeCode);
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

}
