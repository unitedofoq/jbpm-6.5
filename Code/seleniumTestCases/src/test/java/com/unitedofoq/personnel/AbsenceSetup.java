package com.unitedofoq.personnel;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.unitedofoq.fabsbase.BaseClass;

public class AbsenceSetup extends BaseClass {
  private StringBuffer verificationErrors = new StringBuffer();
  private String absence_title;
  private String unit_of_measure;
  // - Absence Affecting Deduction:
  private String AAD_deduction;
  private String AAD_fixed_percent;
  private String ADD_from_value;
  private String ADD_to_value;
  private String ADD_affect_perc;

  @Before
  public void setUp() throws Exception {
    absence_title = "#Day_Absence_" + random + "#";
    unit_of_measure = "Month";
    // - Absence Affecting Deduction:
    AAD_deduction = "Absence Without Excuse";
    AAD_fixed_percent = "Amount";
    ADD_from_value = "0.000";
    ADD_to_value = "999999.000";
    ADD_affect_perc = "100.000";
    login();
  }

  @Test
  public void testAbsenceSetup() throws Exception {

    // Absence Setup test case:
    // - Absence Setup:
    driver.findElement(By.linkText("Personnel")).click();
    driver.findElement(By.linkText("Absence Setup")).click();
    driverManager.waitForPageToLoad();
    driver.switchTo().defaultContent();
    driver.switchTo().frame(
        driver.findElement(By.cssSelector("iframe[title='PRS_AbsenceList_T_X_0002_OneFiler']")));
    driverManager.waitForElement(By.xpath("//*[@title='New Row']"), reverseCondition);
    driverManager.findElementByXpath("//*[@title='New Row']").click();
    driverManager.waitForElement(
        By.xpath(
            "//*[@id='tf:PRS_AbsenceList_T_X_0002_OneFiler_generalTable:0:descriptionTranslated_TextField_0']"),
        reverseCondition);

    driverManager.findElementByXpath(
        "//*[@id='tf:PRS_AbsenceList_T_X_0002_OneFiler_generalTable:0:descriptionTranslated_TextField_0']")
        .clear();
    driverManager.findElementByXpath(
        "//*[@id='tf:PRS_AbsenceList_T_X_0002_OneFiler_generalTable:0:descriptionTranslated_TextField_0']")
        .sendKeys(absence_title);
    driverManager.findElementByXpath(
        "//*[@id='tf:PRS_AbsenceList_T_X_0002_OneFiler_generalTable:0:j_id48_DrpDown']/div[2]/span")
        .click();
    driverManager.findElementByXpath(
        "//*[@id='tf:PRS_AbsenceList_T_X_0002_OneFiler_generalTable:0:j_id48_DrpDown_panel']/div/ul/li[text()='"
            + unit_of_measure + "']")
        .click();
    driverManager.findElementByXpath(
        "//*[@id='tf:PRS_AbsenceList_T_X_0002_OneFiler_generalTable:0:j_id32_rowEditor']/span[2]")
        .click();
    // // Warning: waitForTextPresent may require manual changes
    driverManager.waitForSuccess();

    driverManager.findElementByXpath(
        "//*[@id=\"tf:PRS_AbsenceList_T_X_0002_OneFiler_generalTable:column0:filter\"]").clear();
    driverManager.findElementByXpath(
        "//*[@id=\"tf:PRS_AbsenceList_T_X_0002_OneFiler_generalTable:column0:filter\"]")
        .sendKeys(absence_title);
    driverManager.findElementByXpath(
        "//*[@id=\"tf:PRS_AbsenceList_T_X_0002_OneFiler_generalTable:column0:filter\"]")
        .sendKeys(Keys.RETURN);
    driverManager.checkValue(absence_title,
        "//*[@id='tf:PRS_AbsenceList_T_X_0002_OneFiler_generalTable:0:cell0']/span[1]/table/tbody/tr/td/label");


    driverManager.findElementByXpath(
        "//*[@id='tf:PRS_AbsenceList_T_X_0002_OneFiler_generalTable_data']/tr/td[2]").click();

    driverManager.switchToFrame("ManageAbsenceAffectingDeductions");

    driverManager.findElementByXpath("//*[@title='New Row']").click();
    driverManager.waitForElement(
        By.xpath(
            "//*[@id='tf:ManageAbsenceAffectingDeductions_generalTable:0:deduction_descriptionTranslated_Drop-down_3']/div[2]/span"),
        reverseCondition);
    driverManager.findElementByXpath(
        "//*[@id='tf:ManageAbsenceAffectingDeductions_generalTable:0:deduction_descriptionTranslated_Drop-down_3']/div[2]/span")
        .click();
    driverManager.findElementByXpath(
        "//*[@id='tf:ManageAbsenceAffectingDeductions_generalTable:0:deduction_descriptionTranslated_Drop-down_3_panel']/div/ul/li[text()='"
            + AAD_deduction + "']")
        .click();
    driverManager.findElementByXpath(
        "//*[@id='tf:ManageAbsenceAffectingDeductions_generalTable:0:j_id63_DrpDown']/div[2]/span")
        .click();
    driverManager.findElementByXpath(
        "//*[@id='tf:ManageAbsenceAffectingDeductions_generalTable:0:j_id63_DrpDown_panel']/div/ul/li[text()='"
            + AAD_fixed_percent + "']")
        .click();
    driverManager.findElementByXpath(
        "//*[@id='tf:ManageAbsenceAffectingDeductions_generalTable:0:fromValue_TextField_0']")
        .clear();
    driverManager.findElementByXpath(
        "//*[@id='tf:ManageAbsenceAffectingDeductions_generalTable:0:fromValue_TextField_0']")
        .sendKeys(ADD_from_value);
    driverManager.findElementByXpath(
        "//*[@id='tf:ManageAbsenceAffectingDeductions_generalTable:0:toValue_TextField_1']")
        .clear();
    driverManager.findElementByXpath(
        "//*[@id='tf:ManageAbsenceAffectingDeductions_generalTable:0:toValue_TextField_1']")
        .sendKeys(ADD_to_value);
    driverManager.findElementByXpath(
        "//*[@id='tf:ManageAbsenceAffectingDeductions_generalTable:0:affectingPercent_TextField_4']")
        .clear();
    driverManager.findElementByXpath(
        "//*[@id='tf:ManageAbsenceAffectingDeductions_generalTable:0:affectingPercent_TextField_4']")
        .sendKeys(ADD_affect_perc);
    driverManager.findElementByXpath(
        "//*[@id='tf:ManageAbsenceAffectingDeductions_generalTable:0:j_id32_rowEditor']/span[2]")
        .click();
    // // Warning: waitForTextPresent may require manual changes
    driverManager.waitForSuccess();

    driverManager.switchToFrame("PRS_AbsenceList_T_X_0002_OneFiler");

    // - Data Verification:
    driverManager.waitForElement(
        By.xpath("//*[@id=\"tf:PRS_AbsenceList_T_X_0002_OneFiler_generalTable:column0:filter\"]"),
        reverseCondition);

    driverManager.findElementByXpath(
        "//*[@id=\"tf:PRS_AbsenceList_T_X_0002_OneFiler_generalTable:column0:filter\"]").clear();
    driverManager.findElementByXpath(
        "//*[@id=\"tf:PRS_AbsenceList_T_X_0002_OneFiler_generalTable:column0:filter\"]")
        .sendKeys(absence_title);
    driverManager.findElementByXpath(
        "//*[@id=\"tf:PRS_AbsenceList_T_X_0002_OneFiler_generalTable:column0:filter\"]")
        .sendKeys(Keys.RETURN);
    driverManager.checkValue(absence_title,
        "//*[@id='tf:PRS_AbsenceList_T_X_0002_OneFiler_generalTable:0:cell0']/span[1]/table/tbody/tr/td/label");

    driverManager.findElementByXpath(
        "//*[@id='tf:PRS_AbsenceList_T_X_0002_OneFiler_generalTable_data']/tr/td[2]").click();
    try {
      assertEquals(unit_of_measure,
          driverManager.findElementByXpath(
              "//*[@id='tf:PRS_AbsenceList_T_X_0002_OneFiler_generalTable:0:j_id42_DrpDown_label']")
          .getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    driver.switchTo().defaultContent();
    driver.switchTo().frame(
        driver.findElement(By.cssSelector("iframe[title='ManageAbsenceAffectingDeductions']")));

    try {
      assertEquals(AAD_deduction,
          driverManager.findElementByXpath(
              "//*[@id='tf:ManageAbsenceAffectingDeductions_generalTable:0:cell2']/span[1]/table/tbody/tr/td/table/tbody/tr/td/label")
          .getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(AAD_fixed_percent,
          driverManager.findElementByXpath(
              "//*[@id='tf:ManageAbsenceAffectingDeductions_generalTable:0:j_id57_DrpDown_label']")
          .getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(ADD_from_value,
          driverManager.findElementByXpath(
              "//*[@id='tf:ManageAbsenceAffectingDeductions_generalTable:0:fromValue_TextField_0']")
          .getAttribute("value"));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(ADD_to_value,
          driverManager.findElementByXpath(
              "//*[@id='tf:ManageAbsenceAffectingDeductions_generalTable:0:toValue_TextField_1']")
          .getAttribute("value"));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(ADD_affect_perc,
          driverManager.findElementByXpath(
              "//*[@id='tf:ManageAbsenceAffectingDeductions_generalTable:0:affectingPercent_TextField_4']")
          .getAttribute("value"));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }



}
