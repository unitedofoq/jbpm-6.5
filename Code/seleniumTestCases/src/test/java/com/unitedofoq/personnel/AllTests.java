package com.unitedofoq.personnel;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.unitedofoq.foundation.CreateEmployeeUser;

@RunWith(Suite.class)
@SuiteClasses({// VacationSetup.class, AbsenceSetup.class,
    CreateUnit.class, AddEmployee.class, CreateEmployeeUser.class
    // VacationEntry.class,
    // AbsenceEntry.class
    // PenaltyEntry.class
})
public class AllTests {

}
