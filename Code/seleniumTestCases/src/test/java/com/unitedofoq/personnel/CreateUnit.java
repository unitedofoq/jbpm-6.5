package com.unitedofoq.personnel;


import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import com.unitedofoq.fabsbase.BaseClass;

public class CreateUnit extends BaseClass {
  // private WebDriver driver;
  private StringBuffer verificationErrors = new StringBuffer();
  private String unit_name;

  @Before
  public void setUp() throws Exception {
    unit_name = "TestUnit_" + random;
    login();
  }

  @Test
  public void testCreateUnit() throws Exception {

    driver.findElement(By.linkText("Foundation")).click();
    driver.findElement(By.linkText("Units Management")).click();
    driverManager.waitForPageToLoad();
    driverManager.switchToFrame("FUN_UnitList_FilterByLevelIndexOne");
    // add new row
    driverManager.waitForElement(By.xpath("//*[@title='New Row']"), reverseCondition);
    driverManager.findElementByXpath("//*[@title='New Row']").click();
    driverManager.waitForElement(
        By.xpath(
            "//*[@id='tf:FUN_UnitList_FilterByLevelIndexOne_generalTable:0:nameTranslated_TextField_0']"),
        reverseCondition);
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_UnitList_FilterByLevelIndexOne_generalTable:0:nameTranslated_TextField_0']")
        .clear();
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_UnitList_FilterByLevelIndexOne_generalTable:0:nameTranslated_TextField_0']")
        .sendKeys(unit_name);
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_UnitList_FilterByLevelIndexOne_generalTable:0:j_id32_rowEditor']/span[2]")
        .click();
    // Warning: waitForTextPresent may require manual changes
    driverManager.waitForSuccess();
    // search for the saved unit
    driverManager.checkFilter(unit_name,
        "tf:FUN_UnitList_FilterByLevelIndexOne_generalTable:column0:filter",
        "//*[@id='tf:FUN_UnitList_FilterByLevelIndexOne_generalTable:0:cell0']/span[1]/table/tbody/tr/td/label");
    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    System.setProperty("unit", unit_name);
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }


}
