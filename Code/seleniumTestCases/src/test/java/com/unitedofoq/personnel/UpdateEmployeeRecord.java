package com.unitedofoq.personnel;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import com.unitedofoq.fabsbase.BaseClass;

public class UpdateEmployeeRecord extends BaseClass {

  private StringBuffer verificationErrors = new StringBuffer();
  private String emp_code;
  private String emp_name_eng;

  @Before
  public void setUp() throws Exception {
    // emp_code = "04582";
    emp_code = System.getProperty("empcode");
    emp_name_eng = "Edited Name";
    login();
  }

  @Test
  public void testAddEmployee() throws Exception {
    driver.findElement(By.linkText("Personnel")).click();
    driver.findElement(By.linkText("Employees Management")).click();
    driverManager.waitForPageToLoad();
    // search for employee
    driverManager.switchToFrame("FUN_AddNewEmpWithUnit_T_X_30");
    driverManager.checkFilter(emp_code,
        "tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:column2:filter",
        "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:cell2']/span[1]/table/tbody/tr/td/label");
    // click edit row
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:j_id32_rowEditor']/span")
        .click();
    driverManager.waitForElement(
        By.xpath(
            "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:englishName_TextField_4']"),
        reverseCondition);
    // edit english name
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:englishName_TextField_4']")
        .clear();
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:englishName_TextField_4']")
        .sendKeys(emp_name_eng);
    // save changes
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:j_id32_rowEditor']/span[2]")
        .click();
    driverManager.waitForSuccess();
    driverManager.checkValue(emp_name_eng,
        "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:cell4']/span[1]/table/tbody/tr/td/label");

    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }


}
