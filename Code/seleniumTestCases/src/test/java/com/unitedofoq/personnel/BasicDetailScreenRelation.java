package com.unitedofoq.personnel;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import com.unitedofoq.fabsbase.BaseClass;

/**
 * 
 * @author mashraf
 * @requires AddEmployee.class before it
 */
public class BasicDetailScreenRelation extends BaseClass {

  private StringBuffer verificationErrors = new StringBuffer();
  private String employeeCode;

  @Before
  public void setUp() throws Exception {
    // employeeCode = "Test_791.0";
    employeeCode = System.getProperty("empcode");
    login();
  }

  @Test
  public void testBasicDetailScreenRelation() throws Exception {
    driver.findElement(By.linkText("Personnel")).click();
    driver.findElement(By.linkText("Employees Management")).click();
    driverManager.waitForPageToLoad();
    // search for employee
    driverManager.switchToFrame("FUN_AddNewEmpWithUnit_T_X_30");
    // search for employee
    driverManager.checkFilter(employeeCode,
        "tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:column2:filter",
        "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:cell2']/span[1]/table/tbody/tr/td/label");

    driverManager.switchToFrame("PRS_EmpVacList_T_0001_all");
    // add vacation
    driverManager.findElementByXpath("//*[@title='Add']").click();
    driverManager.switchToLookUp("PRS_EmpVacEnt_F_0001_all");
    // check if startdate field is available
    driverManager.waitForElement(By.xpath("//*[@id='frmScrForm:startDate_Calendar_1_input']"),
        reverseCondition);
    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    System.setProperty("empcode", employeeCode);
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

}
