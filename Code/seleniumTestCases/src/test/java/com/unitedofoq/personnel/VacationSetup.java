package com.unitedofoq.personnel;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.unitedofoq.fabsbase.BaseClass;

public class VacationSetup extends BaseClass {
  private StringBuffer verificationErrors = new StringBuffer();
  private String vacation_title;
  private String max_value;
  private String refresh_every;
  private String balance_method;
  private String wcalender_vacation = "Yes";
  private String remove_holiday = "Yes";
  private String personal_closing_type = "Personal Closing";

  @Before
  public void setUp() throws Exception {
    vacation_title = "#Hajj_Leave_" + random + "#";
    max_value = "30.000";
    refresh_every = "100.000";
    balance_method = "Year Balance";
    wcalender_vacation = "Yes";
    remove_holiday = "Yes";
    personal_closing_type = "Personal Closing";
    login();
  }

  @Test
  public void testVacationSetup() throws Exception {
    driver.findElement(By.linkText("Personnel")).click();
    System.out.println(driver.getWindowHandle());
    driver.findElement(By.linkText("Vacation Setup")).click();

    driverManager.waitForPageToLoad();

    driverManager.waitForElement(By.cssSelector("iframe[title='PRS_VacList_T_X_0002_OneCompFilter']"),
        reverseCondition);
    driverManager.switchToFrame("PRS_VacList_T_X_0002_OneCompFilter");

    driverManager.waitForElement(By.xpath("//*[@title='New Row']"), reverseCondition);
    driverManager.findElementByXpath("//*[@title='New Row']").click();
    driverManager.waitForElement(
        By.xpath(
            "//*[@id='tf:PRS_VacList_T_X_0002_OneCompFilter_generalTable:0:descriptionTranslated_TextField_0']"),
        reverseCondition);

    driverManager.findElementByXpath(
        "//*[@id=\"tf:PRS_VacList_T_X_0002_OneCompFilter_generalTable:0:descriptionTranslated_TextField_0\"]")
        .clear();
    driverManager.findElementByXpath(
        "//*[@id=\"tf:PRS_VacList_T_X_0002_OneCompFilter_generalTable:0:descriptionTranslated_TextField_0\"]")
        .sendKeys(vacation_title);
    driverManager.findElementByXpath(
        "//*[@id='tf:PRS_VacList_T_X_0002_OneCompFilter_generalTable:0:j_id32_rowEditor']/span[2]")
        .click();
    // Warning: waitForTextPresent may require manual changes
    driverManager.waitForSuccess();

    driverManager.findElementByXpath(
        "//*[@id='tf:PRS_VacList_T_X_0002_OneCompFilter_generalTable:column0:filter']").clear();
    driverManager.findElementByXpath(
        "//*[@id='tf:PRS_VacList_T_X_0002_OneCompFilter_generalTable:column0:filter']")
        .sendKeys(vacation_title);
    driverManager.findElementByXpath(
        "//*[@id='tf:PRS_VacList_T_X_0002_OneCompFilter_generalTable:column0:filter']")
        .sendKeys(Keys.RETURN);
    driverManager.checkValue(vacation_title,
        "//*[@id='tf:PRS_VacList_T_X_0002_OneCompFilter_generalTable:0:cell0']/span[1]/table/tbody/tr/td/label");

    driverManager.findElementByXpath(
        "//*[@id='tf:PRS_VacList_T_X_0002_OneCompFilter_generalTable:0:cell0']/span[1]/table/tbody/tr/td")
        .click();
    // select frame vacation header
    driverManager.switchToFrame("PER_VacHed_F_0001_ALL");

    driverManager.checkAttributeValue(vacation_title,
        "//*[@id='frmScrForm:descriptionTranslated_TextField_1']", "value", reverseCondition);

    driverManager.findElementByXpath("//*[@id='frmScrForm:j_id117_DrpDown']/div[2]/span").click();
    driverManager.findElementByXpath(
        "//*[@id='frmScrForm:j_id117_DrpDown_panel']/div/ul/li[text()='" + balance_method + "']")
        .click();

    driverManager.findElementByXpath("//*[@id='frmScrForm:j_id229_DrpDown']/div[2]/span").click();
    driverManager.findElementByXpath("//*[@id='frmScrForm:j_id229_DrpDown_panel']/div/ul/li[text()='"
        + wcalender_vacation + "']").click();

    driverManager.findElementByXpath("//*[@id='frmScrForm:j_id239_DrpDown']/div[2]/span").click();
    driverManager.findElementByXpath(
        "//*[@id='frmScrForm:j_id239_DrpDown_panel']/div/ul/li[text()='" + remove_holiday + "']")
        .click();

    driverManager.findElementByXpath("//*[@id='frmScrForm:j_id307_DrpDown']/div[2]/span").click();
    driverManager.findElementByXpath("//*[@id='frmScrForm:j_id307_DrpDown_panel']/div/ul/li[text()='"
        + personal_closing_type + "']").click();

    driverManager.findElementByXpath("//*[@id='frmScrForm:maximumValue_TextField_2']").clear();
    driverManager.findElementByXpath("//*[@id='frmScrForm:maximumValue_TextField_2']").sendKeys(max_value);

    driverManager.findElementByXpath("//*[@id='frmScrForm:refreshEvery_TextField_4']").clear();
    driverManager.findElementByXpath("//*[@id='frmScrForm:refreshEvery_TextField_4']").sendKeys(refresh_every);
    driverManager.findElementByXpath("//*[@title='Submit']").click();
    driverManager.waitForSuccess();

    driverManager.switchToFrame("PRS_VacList_T_X_0002_OneCompFilter");

    driverManager.findElementByXpath(
        "//*[@id='tf:PRS_VacList_T_X_0002_OneCompFilter_generalTable:column0:filter']").clear();
    driverManager.findElementByXpath(
        "//*[@id='tf:PRS_VacList_T_X_0002_OneCompFilter_generalTable:column0:filter']")
        .sendKeys(vacation_title);
    driverManager.findElementByXpath(
        "//*[@id='tf:PRS_VacList_T_X_0002_OneCompFilter_generalTable:column0:filter']")
        .sendKeys(Keys.RETURN);
    driverManager.checkValue(vacation_title,
        "//*[@id='tf:PRS_VacList_T_X_0002_OneCompFilter_generalTable:0:cell0']/span[1]/table/tbody/tr/td/label");



    driverManager.findElementByXpath(
        "//*[@id='tf:PRS_VacList_T_X_0002_OneCompFilter_generalTable:0:cell0']/span[1]/table/tbody/tr/td")
        .click();

    driverManager.switchToFrame("PER_VacHed_F_0001_ALL");


    try {
      assertEquals(balance_method,
          driverManager.findElementByXpath("//*[@id=\"frmScrForm:j_id117_DrpDown_label\"]").getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(wcalender_vacation,
          driverManager.findElementByXpath("//*[@id=\"frmScrForm:j_id229_DrpDown_label\"]").getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(remove_holiday,
          driverManager.findElementByXpath("//*[@id=\"frmScrForm:j_id239_DrpDown_label\"]").getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(personal_closing_type,
          driverManager.findElementByXpath("//*[@id=\"frmScrForm:j_id307_DrpDown_label\"]").getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(max_value,
          driverManager.findElementByXpath("//*[@id='frmScrForm:maximumValue_TextField_2']")
              .getAttribute("value"));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(refresh_every,
          driverManager.findElementByXpath("//*[@id='frmScrForm:refreshEvery_TextField_4']")
              .getAttribute("value"));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

}
