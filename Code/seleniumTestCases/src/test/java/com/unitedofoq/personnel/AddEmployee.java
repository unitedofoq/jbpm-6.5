package com.unitedofoq.personnel;


import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.unitedofoq.fabsbase.BaseClass;

/**
 * 
 * @author mashraf need to run add unit before it
 */
public class AddEmployee extends BaseClass {

  private StringBuffer verificationErrors = new StringBuffer();
  private String emp_code;
  private String emp_name_eng;
  private String emp_name_ara;
  private String national_id;
  private String unit_name;
  private String position_name;
  private String hiring_date;
  private String calender_type;
  private String hiring_type;
  private String email;

  @Before
  public void setUp() throws Exception {
    emp_code = "Test_" + random;
    emp_name_eng = "Refaat_Ismail_" + random;
    emp_name_ara = "رفعت_إسماعيل_" + random;
    national_id = "Test_" + random;
    unit_name = System.getProperty("unit");
    // unit_name = "TestUnit_627.0";
    position_name = unit_name + " Manager";
    hiring_date = "01/01/2013";
    calender_type = "normal";
    hiring_type = "Permenant";
    email = emp_name_eng + "@unitedofoq.com";
    login();
  }

  @Test
  public void testAddEmployee() throws Exception {
    driver.findElement(By.linkText("Personnel")).click();
    driver.findElement(By.linkText("Employees Management")).click();
    driverManager.waitForPageToLoad();

    driverManager.switchToFrame("FUN_AddNewEmpWithUnit_T_X_30");
    driverManager.waitForElement(By.xpath("//*[@title='New Row']"), reverseCondition);
    // add new employee
    driverManager.findElementByXpath("//*[@title='New Row']").click();
    driverManager.waitForElement(By.xpath("//button[@title='unitFilter.nameTranslated']"),
        reverseCondition);
    driverManager.findElementByXpath("//button[@title='unitFilter.nameTranslated']").click();
    // pick a unit
    driverManager.switchToLookUp("FUN_UnitList_T_X_0005");
    driverManager.waitForElement(
        By.xpath("//*[@id=\"tf:FUN_UnitList_T_X_0005_generalTable:column0:filter\"]"),
        reverseCondition);
    driverManager
        .findElementByXpath("//*[@id=\"tf:FUN_UnitList_T_X_0005_generalTable:column0:filter\"]")
        .clear();
    driverManager
        .findElementByXpath("//*[@id=\"tf:FUN_UnitList_T_X_0005_generalTable:column0:filter\"]")
        .sendKeys(unit_name);
    driverManager
        .findElementByXpath("//*[@id=\"tf:FUN_UnitList_T_X_0005_generalTable:column0:filter\"]")
        .sendKeys(Keys.RETURN);
    driverManager.checkValue(unit_name,
        "//*[@id='tf:FUN_UnitList_T_X_0005_generalTable_data']/tr/td[2]/div/table/tbody/tr/td/label");
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_UnitList_T_X_0005_generalTable_data']/tr/td[2]/div/table/tbody/tr/td")
        .click();
    driverManager.findElementByXpath("//*[@title='Pick']").click();

    driverManager.switchToFrame("FUN_AddNewEmpWithUnit_T_X_30");
    driverManager.checkAttributeValue(unit_name,
        "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:j_id87_LookupInputText']", "value",
        reverseCondition);
    // pick a position
    driverManager.findElementByXpath("//button[@title='positionSimpleMode.nameTranslated']")
        .click();

    driverManager.switchToLookUp("FUN_Pos_Lookup_T_0003");
    driverManager.waitForElement(
        By.xpath("//*[@id='tf:FUN_Pos_Lookup_T_0003_generalTable:column0:filter']"),
        reverseCondition);
    driverManager
        .findElementByXpath("//*[@id='tf:FUN_Pos_Lookup_T_0003_generalTable:column0:filter']")
        .clear();
    driverManager
        .findElementByXpath("//*[@id='tf:FUN_Pos_Lookup_T_0003_generalTable:column0:filter']")
        .sendKeys(position_name);
    driverManager
        .findElementByXpath("//*[@id='tf:FUN_Pos_Lookup_T_0003_generalTable:column0:filter']")
        .sendKeys(Keys.RETURN);
    driverManager.checkValue(position_name,
        "//*[@id='tf:FUN_Pos_Lookup_T_0003_generalTable_data']/tr/td[2]/div/table/tbody/tr/td/label");
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_Pos_Lookup_T_0003_generalTable_data']/tr/td[2]/div/table/tbody/tr/td")
        .click();
    driverManager.findElementByXpath("//*[@title='Pick']").click();

    driverManager.switchToFrame("FUN_AddNewEmpWithUnit_T_X_30");
    driverManager.checkAttributeValue(position_name,
        "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:j_id95_LookupInputText']", "value",
        reverseCondition);
    // pick a calendar
    driverManager.findElementByXpath("//*[@title='calendar.name']").click();

    driverManager.switchToLookUp("Calendar");
    driverManager.waitForElement(By.xpath("//*[@id=\"tf:Calendar_generalTable:column0:filter\"]"),
        reverseCondition);
    driverManager.findElementByXpath("//*[@id=\"tf:Calendar_generalTable:column0:filter\"]")
        .clear();
    driverManager.findElementByXpath("//*[@id=\"tf:Calendar_generalTable:column0:filter\"]")
        .sendKeys(calender_type);
    driverManager.findElementByXpath("//*[@id='tf:Calendar_generalTable:column0:filter']")
        .sendKeys(Keys.RETURN);
    driverManager.checkValue(calender_type,
        "//*[@id='tf:Calendar_generalTable_data']/tr/td/div/table/tbody/tr/td/label");
    driverManager
        .findElementByXpath("//*[@id='tf:Calendar_generalTable_data']/tr/td/div/table/tbody/tr/td")
        .click();
    driverManager.findElementByXpath("//*[@title='Pick']").click();

    driverManager.switchToFrame("FUN_AddNewEmpWithUnit_T_X_30");
    driverManager.checkAttributeValue(calender_type,
        "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:j_id150_LookupInputText']",
        "value", reverseCondition);
    // choose hiring type
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:hiringType_valueTranslated_Drop-down_7']/div[2]/span")
        .click();
    driverManager.findElementByXpath(
        "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:hiringType_valueTranslated_Drop-down_7_panel']/div/ul/li[text()='"
            + hiring_type + "']")
        .click();
    // enter employee code
    driver.findElement(By.id("tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:code_TextField_2"))
        .clear();
    driver.findElement(By.id("tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:code_TextField_2"))
        .sendKeys(emp_code);
    // enter employee english name
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:englishName_TextField_4']")
        .clear();
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:englishName_TextField_4']")
        .sendKeys(emp_name_eng);
    // enter employee arabic name
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:arabicName_TextField_3']")
        .clear();
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:arabicName_TextField_3']")
        .sendKeys(emp_name_ara);
    // enter a hiring date
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:hiringDate_Calendar_5_input']")
        .clear();
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:hiringDate_Calendar_5_input']")
        .sendKeys(hiring_date);
    // enter a national id
    driverManager.findElementByXpath(
        "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:natID_TextField_9']").clear();
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:natID_TextField_9']")
        .sendKeys(national_id);
    // enter an email
    driverManager.findElementByXpath(
        "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:email_TextField_14']").clear();
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:email_TextField_14']")
        .sendKeys(email);
    // save
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:j_id32_rowEditor']/span[2]")
        .click();
    driverManager.waitForSuccess(120);
    // search for employee
    driverManager.findElementByXpath(
        "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:column2:filter']").clear();
    driverManager
        .findElementByXpath(
            "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:column2:filter']")
        .sendKeys(emp_code);
    driverManager
        .findElementByXpath(
            "//*[@id=\"tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:column2:filter\"]")
        .sendKeys(Keys.RETURN);
    // verify
    driverManager.checkValue(emp_code,
        "//*[@id='tf:FUN_AddNewEmpWithUnit_T_X_30_generalTable:0:cell2']/span[1]/table/tbody/tr/td/label");

    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    System.setProperty("empcode", emp_code);
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }


}
