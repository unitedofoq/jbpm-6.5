package com.unitedofoq.personnel;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.unitedofoq.fabsbase.BaseClass;

public class VacationEntry extends BaseClass {
  private StringBuffer verificationErrors = new StringBuffer();
  double v_random;
  private String emp_code;
  private String start_date;
  private String start_time;
  private String end_date;
  private String end_time;
  private String vacation_type;
  private String balance;
  private String period;
  private String v_notes;
  private int available_balance;

  @Before
  public void setUp() throws Exception {
    v_random = Math.floor((Math.random() * 1000) + 1);
    // random=729.0;
    // emp_code = "Test_462.0";
    emp_code = System.getProperty("empcode");
    start_date = "2/11/2014";
    start_time = "00:00";
    end_date = "4/11/2014";
    end_time = "23:59";
    vacation_type = "Annual Leave";
    balance = "21.00";
    period = "3.00";
    v_notes = emp_code + "_" + start_date + "_" + end_date + "_" + v_random;
    login();
  }

  @Test
  public void testVacationEntry() throws Exception {

    driver.findElement(By.linkText("Personnel")).click();
    driver.findElement(By.linkText("Employee's Vacation")).click();
    driverManager.waitForPageToLoad();
    driverManager.switchToFrame("PER_EmpList_T_X_SM_ONEFilter");
    driverManager.findElementByXpath("//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable:column0:filter']")
        .clear();
    driverManager.findElementByXpath("//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable:column0:filter']")
        .sendKeys(emp_code);
    driverManager.findElementByXpath(
        "//*[@id=\"tf:PER_EmpList_T_X_SM_ONEFilter_generalTable:column0:filter\"]")
        .sendKeys(Keys.RETURN);

    driverManager.checkValue(emp_code,
        "//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable_data']/tr/td[2]/div/table/tbody/tr/td/label");
    driverManager.findElementByXpath(
        "//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable_data']/tr/td[2]/div/table/tbody/tr/td")
        .click();

    driverManager.switchToFrame("PRS_EmpVacList_T_0001_all");
    driverManager.findElementByXpath("//*[@title='Add']").click();

    driverManager.switchToLookUp("PRS_EmpVacEnt_F_0001_all");
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:startDate_Calendar_1_input\"]").clear();
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:startDate_Calendar_1_input\"]")
        .sendKeys(start_date + " " + start_time);
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:startDate_Calendar_1_input\"]")
        .sendKeys(Keys.RETURN);

    // DM.findElementByXpath("//*[@id='ui-datepicker-div']/div[3]/button[2]")).click();
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:endDate_Calendar_2_input\"]").clear();
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:endDate_Calendar_2_input\"]")
        .sendKeys(end_date + " " + end_time);
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:endDate_Calendar_2_input\"]").sendKeys(Keys.RETURN);

    driverManager.findElementByXpath("//*[@id='ui-datepicker-div']/div[3]/button[2]").click();
    driverManager.findElementByXpath("//*[@id='frmScrForm:vacation_description_Drop-down_4']/div[2]/span")
        .click();
    driverManager.findElementByXpath(
        "//*[@id='frmScrForm:vacation_description_Drop-down_4_panel']/div/ul/li[text()='"
            + vacation_type + "']")
        .click();
    driverManager.checkAttributeValue("0.00", "//*[@id=\"frmScrForm:periodMask_TextField_6\"]", "value",
        !reverseCondition);
    // for (int second = 0;; second++) {
    // if (second >= 60)
    // fail("timeout");
    // try {
    // if (!"0.00"
    // .equals(DM.findElementByXpath("//*[@id=\"frmScrForm:periodMask_TextField_6\"]"))
    // .getAttribute("value")))
    // break;
    // } catch (Exception e) {
    // }
    // Thread.sleep(1000);
    // }
    driverManager.checkAttributeValue("0.00", "//*[@id='frmScrForm:balanceMask_TextField_5']", "value",
        !reverseCondition);
    // for (int second = 0;; second++) {
    // if (second >= 60)
    // fail("timeout");
    // try {
    // if (!"0.00"
    // .equals(DM.findElementByXpath("//*[@id='frmScrForm:balanceMask_TextField_5']"))
    // .getAttribute("value")))
    // break;
    // } catch (Exception e) {
    // }
    // Thread.sleep(1000);
    // }

    try {
      assertEquals(balance, driverManager.findElementByXpath("//*[@id='frmScrForm:balanceMask_TextField_5']")
          .getAttribute("value"));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(period, driverManager.findElementByXpath("//*[@id='frmScrForm:periodMask_TextField_6']")
          .getAttribute("value"));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:notes_TextArea_13\"]").clear();
    driverManager.findElementByXpath("//*[@id=\"frmScrForm:notes_TextArea_13\"]").sendKeys(v_notes);
    driverManager.findElementByXpath("//*[@title='Submit & Close']").click();
    driverManager.waitForElement(By.xpath("//*[@title='Submit & Close']"), !reverseCondition);

    driverManager.switchToFrame("PER_EmpList_T_X_SM_ONEFilter");
    driverManager.findElementByXpath("//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable:column0:filter']")
        .clear();
    driverManager.findElementByXpath("//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable:column0:filter']")
        .sendKeys(emp_code);
    driverManager.findElementByXpath("//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable:column0:filter']")
        .sendKeys(Keys.RETURN);
    driverManager.checkValue(emp_code,
        "//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable_data']/tr/td[2]/div/table/tbody/tr/td/label");


    driverManager.findElementByXpath(
        "//*[@id='tf:PER_EmpList_T_X_SM_ONEFilter_generalTable_data']/tr/td[2]/div/table/tbody/tr/td")
        .click();
    driverManager.switchToFrame("PRS_EmpVacList_T_0001_all");
    driverManager.findElementByXpath("//*[@id='tf:PRS_EmpVacList_T_0001_all_generalTable:column7:filter']")
        .clear();
    driverManager.findElementByXpath("//*[@id='tf:PRS_EmpVacList_T_0001_all_generalTable:column7:filter']")
        .sendKeys(v_notes);
    driverManager.findElementByXpath("//*[@id='tf:PRS_EmpVacList_T_0001_all_generalTable:column7:filter']")
        .sendKeys(Keys.RETURN);

    driverManager.checkValue(v_notes,
        "//*[@id='tf:PRS_EmpVacList_T_0001_all_generalTable:0:j_id72_ColCommLINK']");

    try {
      assertEquals(vacation_type,
          driverManager.findElementByXpath(
              "//*[@id='tf:PRS_EmpVacList_T_0001_all_generalTable_data']/tr/td[3]/div/table/tbody/tr/td")
          .getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(start_date + " " + start_time,
          driverManager.findElementByXpath(
              "//*[@id=\"tf:PRS_EmpVacList_T_0001_all_generalTable:0:j_id59_ColTXT_input\"]")
          .getAttribute("value"));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(end_date + " " + end_time,
          driverManager.findElementByXpath(
              "//*[@id=\"tf:PRS_EmpVacList_T_0001_all_generalTable:0:j_id61_ColTXT_input\"]")
          .getAttribute("value"));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals(period,
          driverManager.findElementByXpath(
              "//*[@id='tf:PRS_EmpVacList_T_0001_all_generalTable_data']/tr/td[7]/div/table/tbody/tr/td")
          .getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    available_balance = Integer.parseInt(balance) - Integer.parseInt(period);
    // ERROR: Caught exception [ERROR: Unsupported command [getEval | ${balance}-${period} | ]]
    try {
      assertEquals(available_balance + ".00",
          driverManager.findElementByXpath(
              "//*[@id='tf:PRS_EmpVacList_T_0001_all_generalTable_data']/tr/td[8]/div/table/tbody/tr/td/label")
          .getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }



}
