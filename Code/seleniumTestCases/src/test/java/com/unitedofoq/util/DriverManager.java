package com.unitedofoq.util;

import java.util.NoSuchElementException;
import java.util.Set;

import org.openqa.selenium.Alert;

/**
 * 
 */

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author fabs
 *
 */
public final class DriverManager {

  private WebDriver webDriver;

  public DriverManager(WebDriver webDriver) {
    this.webDriver = webDriver;
  }


  public WebElement findElementByXpath(String xpath) {
    return getWebDriver().findElement(By.xpath(xpath));

  }

  public void waitForElement(By by, boolean reverseCondition) {
    WebDriverWait wait = new WebDriverWait(getWebDriver(), 15);
    if (reverseCondition) {
      wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
    } else {
      try {
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
      } catch (TimeoutException e) {
        getWebDriver().findElement(by);
      }
    }
  }

  public void checkValue(String value, String path) {
    WebDriverWait wait = new WebDriverWait(getWebDriver(), 60);
    wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(path), value));
  }

  public void checkAttributeValue(String value, String path, String attribute,
      boolean reverseCondition) {
    WebDriverWait wait = new WebDriverWait(getWebDriver(), 60);
    try {
      wait.until(elementAttributeEqual(value, path, attribute, reverseCondition));
    } catch (StaleElementReferenceException e) {
      if (!value.equals(getWebDriver().findElement(By.xpath(path)).getAttribute(attribute))) {
        System.out.println("selected value didn't get selected");
      }

    }
  }

  public boolean isElementPresent(By by) {
    try {
      getWebDriver().findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public void switchToFrame(String frame) {
    getWebDriver().switchTo().defaultContent();
    getWebDriver().switchTo()
        .frame(getWebDriver().findElement(By.cssSelector("iframe[title='" + frame + "']")));

  }

  public void checkFilter(String value, String filterPath, String columnPath) {
    waitForElement(By.xpath("//*[@id=\"" + filterPath + "\"]"), false);
    getWebDriver().findElement(By.xpath("//*[@id=\"" + filterPath + "\"]")).clear();
    getWebDriver().findElement(By.xpath("//*[@id=\"" + filterPath + "\"]")).sendKeys(value);
    getWebDriver().findElement(By.xpath("//*[@id=\"" + filterPath + "\"]")).sendKeys(Keys.RETURN);
    checkValue(value, columnPath);
  }

  public void switchToLookUp(String frame) {
    getWebDriver().switchTo().defaultContent();
    waitForElement(By.xpath("//iframe[contains(@src,'" + frame + "')]"), false);
    getWebDriver().switchTo()
        .frame(getWebDriver().findElement(By.xpath("//iframe[contains(@src,'" + frame + "')]")));
  }

  public void waitForPageToLoad() {
    waitForPageToLoad(2);
  }

  public void waitForPageToLoad(int count) {

    String winHandleBefore = getWebDriver().getWindowHandle();
    while (getWebDriver().getWindowHandles().size() < count) {
    }

    Set<String> windowIds = getWebDriver().getWindowHandles(); // get window id of
    // current window
    for (String hnd : windowIds) {
      if (!hnd.equals(winHandleBefore)) {
        getWebDriver().switchTo().window(hnd);
      }
    }
    getWebDriver().switchTo().defaultContent();
  }

  public void waitForSuccess() throws Exception {
    waitForSuccess(60);
  }

  public void waitForSuccess(int s) throws Exception {
    WebDriverWait wait = new WebDriverWait(getWebDriver(), s);
    wait.until(success());
  }

  public void acceptAlert() {
    try {
      // Wait 10 seconds till alert is present
      WebDriverWait wait = new WebDriverWait(getWebDriver(), 10);
      Alert alert = wait.until(ExpectedConditions.alertIsPresent());

      // Accepting alert.
      alert.accept();
      System.out.println("Accepted the alert successfully.");
    } catch (Throwable e) {
      System.err.println("Error came while waiting for the alert popup. " + e.getMessage());
    }
  }

  private static ExpectedCondition<Boolean> success() {
    return new ExpectedCondition<Boolean>() {
      public Boolean apply(WebDriver input) {
        return (Boolean) input.findElement(By.cssSelector("BODY")).getText()
            .matches("^[\\s\\S]*Success\\(es\\)[\\s\\S]*$");
      }
    };
  }

  private static ExpectedCondition<Boolean> elementAttributeEqual(final String value,
      final String path, final String attribute, final boolean reverseCondition) {
    return new ExpectedCondition<Boolean>() {
      public Boolean apply(WebDriver driver) {
        if (!reverseCondition)
          return value.equals(driver.findElement(By.xpath(path)).getAttribute(attribute));
        else
          return !value.equals(driver.findElement(By.xpath(path)).getAttribute(attribute));
      }
    };
  }

  public WebDriver getWebDriver() {
    return webDriver;
  }


}
