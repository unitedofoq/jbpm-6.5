package com.unitedofoq.util;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class FrameUtil {
  private WebDriver driver;
  private int seqance = 1;

  private int text1 = 1;
  private int text2 = 1;
  private DriverManager driverManager;
  private Boolean ismain = true;

  public FrameUtil() {

  }

  public FrameUtil(WebDriver xDriver) {
    driver = xDriver;
    driverManager = new DriverManager(driver);
    System.setProperty("entity", "ay7aga");
    System.setProperty("clspath", "com.unitedofoq.otms.foundation.employee.ay7aga");
  }

  public void waitSwitch(String frame) {
    driverManager.waitForPageToLoad();

  }

  public void Screenfileds(String xfield_expression) {

    Screenfiledsbyrow(xfield_expression);
  }

  public void Screenfileds(List<String> list) {

    for (String item : list) {
      Screenfiledsbyrow(item);
    }
  }

  public void screenPortalPagePortlet(List<String> list) {
    for (String value : list) {
      screenPortalPagePortlet(value);
    }
  }

  public void screenPortalPagePortlet(String value) {
    try {
      driverManager.switchToFrame("PortalPagePortlet");

      driverManager.waitForElement(By.xpath("//*[@title='New Row']"), false);
      driverManager.findElementByXpath("//*[@title='New Row']").click();
      driverManager.waitForElement(By.xpath("//*[@title='screen.name']"), false);
      driverManager.findElementByXpath("//*[@title='screen.name']").click();

      driverManager.switchToLookUp("LookupScreen");
      driverManager.waitForElement(
          By.xpath("//*[@id='tf:LookupScreen_generalTable:column0:filter']"), false);
      cES("//*[@id='tf:LookupScreen_generalTable:column0:filter']", value);

      driverManager.findElementByXpath(
          "//*[@id='tf:LookupScreen_generalTable:0:cell0']/span[1]/table/tbody/tr/td").click();
      driverManager.findElementByXpath("//*[@title='Pick']").click();
      driverManager.switchToFrame("PortalPagePortlet");
      driverManager.waitForElement(
          By.xpath("//input[@id='tf:PortalPagePortlet_generalTable:0:position_TextField_0']"),
          false);
      cS("//input[@id='tf:PortalPagePortlet_generalTable:0:position_TextField_0']",
          String.valueOf(text1++));

      cS("//*[@id='tf:PortalPagePortlet_generalTable:0:column_TextField_1']",
          String.valueOf(text2++));
      if (ismain)
        driver
            .findElement(
                By.xpath(".//*[@id='tf:PortalPagePortlet_generalTable:0:main_CheckBox_3']/div[2]"))
            .click();
      ismain = false;
      driverManager.findElementByXpath(
          "//*[@id='tf:PortalPagePortlet_generalTable:0:j_id32_rowEditor']/span[2]").click();

      driverManager.waitForSuccess();
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  public void Screenfiledsbyrow(String field_expression) {
    try {

      driverManager.switchToFrame("ScreenFieldsEntry");

      driverManager.findElementByXpath("//*[@title='New Row']").click();

      driverManager.waitForElement(By.xpath("//button[@title='fieldExpression']"), false);
      driverManager.findElementByXpath("//button[@title='fieldExpression']").click();

      driverManager.switchToLookUp("FieldsTreeBrowser");

      driverManager.findElementByXpath("//span[contains(text(),'" + field_expression + "')]")
          .click();
      driverManager.findElementByXpath("//*[@id='frmScrForm:j_id4returnSelectedBTN']").click();

      driverManager.switchToFrame("ScreenFieldsEntry");

      // driverManager.checkValue("//*[@id='tf:ScreenFieldsEntry_generalTable:0:j_id61_LookupInputText']",
      // field_expression);

      driverManager.waitForElement(
          By.xpath("//*[@id='tf:ScreenFieldsEntry_generalTable:0:j_id61_LookupInputText']"), false);

      cS("//input[@id='tf:ScreenFieldsEntry_generalTable:0:sortIndex_TextField_3']",
          String.valueOf(seqance++));

      driverManager.findElementByXpath(
          "//*[@id='tf:ScreenFieldsEntry_generalTable:0:j_id32_rowEditor']/span[2]").click();

      driverManager.waitForSuccess();

    } catch (Throwable e) {
      System.out.println(e.getMessage());
    }
  }

  public void Screeninputsbyrow(String input) {
    try {
      driverManager.switchToFrame("ScreenInput");

      driverManager.findElementByXpath("//*[@title='New Row']").click();
      driverManager.waitForElement(By.xpath("//*[@title='odataType.name']"), false);
      driverManager.findElementByXpath("//*[@title='odataType.name']").click();

      driverManager.switchToLookUp("LookupDataType");
      driverManager.waitForElement(
          By.xpath("//*[@id='tf:LookupDataType_generalTable:column0:filter']"), false);
      cES("//*[@id='tf:LookupDataType_generalTable:column0:filter']", input);
      driverManager.waitForElement(By.xpath("//td[contains(.,'VoidDT')]/a"), false);
      driverManager
          .findElementByXpath(
              "//*[@id='tf:LookupDataType_generalTable:1:cell0']/span[1]/table/tbody/tr/td")
          .click();

      driverManager.findElementByXpath("//*[@title='Pick']").click();

      driverManager.switchToFrame("ScreenInput");

      // driverManager.checkValue(input,
      // "//*[@id='tf:ScreenInput_generalTable:0:j_id65_LookupInputText']");
      driverManager.waitForElement(By.xpath("//input[contains(@value,'VoidDT')]"), false);
      driverManager
          .findElementByXpath("//*[@id='tf:ScreenInput_generalTable:0:j_id32_rowEditor']/span[2]")
          .click();

      driverManager.waitForSuccess();
    } catch (Throwable e) {
      System.out.println(e.getMessage());
    }

  }

  public void entityLookup(String entity) {
    driverManager.switchToLookUp("LookupEntity");

    driverManager.waitForElement(By.xpath("//input[contains(@role,'textbox')]"), false);

    cES("//input[contains(@role,'textbox')]", entity);

    driverManager.waitForElement(By.xpath("//td[contains(.,'" + entity + "')]/a"), false);

    driverManager.findElementByXpath(
        ".//*[@id='tf:LookupEntity_generalTable:0:cell0']/span[1]/table/tbody/tr/td").click();

    driverManager.findElementByXpath("//*[@title='Pick']").click();
  }

  public void switchToMenu() {
    driver.switchTo().defaultContent();
    driver.switchTo().frame("DockMenuFrame");
    driverManager.findElementByXpath("//span[contains(.,'Application Builder')]").click();
    driverManager.findElementByXpath("//span[contains(.,'Application Builder')]").click();
  }

  public void cS(String path, String value) {
    driverManager.findElementByXpath(path).clear();
    driverManager.findElementByXpath(path).sendKeys(value);
  }

  public void cES(String path, String value) {
    driverManager.waitForElement(By.xpath(path), false);
    driverManager.findElementByXpath(path).clear();
    driverManager.findElementByXpath(path).sendKeys(value);
    driverManager.findElementByXpath(path).sendKeys(Keys.ENTER);
  }

  public void module(String name) {
    driverManager.findElementByXpath("//span[@class='ui-icon ui-icon-triangle-1-s']").click();
    driverManager.findElementByXpath("//li[contains(.,'" + name + "')]").click();
  }

  public static String get(String value) {
    return System.getProperty(value);
  }

  public void closeBrowser() {
    driver.quit();
  }

  public void openFM(String name, String framname, Boolean wait) {
    driver.switchTo().defaultContent();
    driver.switchTo().frame("DockMenuFrame");
    driverManager.findElementByXpath("//span[contains(.,'Application Builder')]").click();
    driver.findElement(By.xpath("//span[contains(.,'" + name + "')]")).click();
    driverManager.waitForPageToLoad();
    driverManager.switchToFrame(framname);
    driver.manage().window().maximize();
  }

  public void checkFilter(String value, String filterPath, String row) {
    cES(filterPath, value);

    driverManager.checkValue(value, row);
  }
}
