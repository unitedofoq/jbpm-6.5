package com.unitedofoq.administration;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.unitedofoq.fabsbase.BaseClass;

public class CreateUser extends BaseClass {
  private StringBuffer verificationErrors = new StringBuffer();
  private String userName;
  private String password;
  private String email;
  private String module;
  private String preference;
  private String first_language;
  private String second_language;
  private String firstRole;
  private String secondRole;


  @Before
  public void setUp() throws Exception {
    userName = "TestUnit" + (int) random;
    password = "pass";
    email = userName + "@unitedofoq.com";
    module = "FOUNDATION";
    preference = "241875";
    first_language = "English";
    second_language = "Arabic";
    firstRole = "OTMSUser4E";
    secondRole = "coreuser";
    login();
  }

  @Test
  public void testCreateUser() throws Exception {
    // open manage users
    driver.findElement(By.linkText("Administration")).click();
    driver.findElement(By.linkText("Manage Users")).click();
    driver.findElement(By.linkText("Administration")).click();
    driver.findElement(By.linkText("Manage Users")).click();

    driverManager.switchToFrame("ViewUsersList");
    // new row
    driverManager.waitForElement(By.xpath("//*[@title='New Row']"), reverseCondition);
    driver.findElement(By.xpath("//*[@title='New Row']")).click();
    // enter username
    driverManager.waitForElement(
        By.xpath("//*[@id='tf:ViewUsersList_generalTable:0:loginName_TextField_0']"),
        reverseCondition);
    driverManager
        .findElementByXpath("//*[@id='tf:ViewUsersList_generalTable:0:loginName_TextField_0']")
        .click();
    driverManager
        .findElementByXpath("//*[@id='tf:ViewUsersList_generalTable:0:loginName_TextField_0']")
        .sendKeys(userName);
    // enter password
    driverManager
        .findElementByXpath("//*[@id='tf:ViewUsersList_generalTable:0:password_PasswordInput_1']")
        .click();
    driverManager
        .findElementByXpath("//*[@id='tf:ViewUsersList_generalTable:0:password_PasswordInput_1']")
        .sendKeys(password);
    // enter email
    driverManager.findElementByXpath("//*[@id='tf:ViewUsersList_generalTable:0:email_TextField_2']")
        .click();
    driverManager.findElementByXpath("//*[@id='tf:ViewUsersList_generalTable:0:email_TextField_2']")
        .sendKeys(email);
    // select module
    driverManager
        .findElementByXpath(
            "//*[@id='tf:ViewUsersList_generalTable:0:omodule_moduleTitleTranslated_Drop-down_8']/div[2]/span")
        .click();
    driverManager.findElementByXpath(
        "//*[@id='tf:ViewUsersList_generalTable:0:omodule_moduleTitleTranslated_Drop-down_8_panel']/div/ul/li[text()='"
            + module + "']")
        .click();
    // enter preference
    driverManager
        .findElementByXpath("//*[@id='tf:ViewUsersList_generalTable:0:userPrefrence1_TextField_7']")
        .click();
    driverManager
        .findElementByXpath("//*[@id='tf:ViewUsersList_generalTable:0:userPrefrence1_TextField_7']")
        .sendKeys(preference);
    // select first language
    driverManager.findElementByXpath("//button[@title='firstLanguage.valueTranslated']").click();
    driverManager.switchToLookUp("IndicatorNature");
    driverManager.waitForElement(
        By.xpath("//*[@id=\"tf:IndicatorNature_generalTable:column1:filter\"]"), reverseCondition);
    driverManager.findElementByXpath("//*[@id=\"tf:IndicatorNature_generalTable:column1:filter\"]")
        .clear();
    driverManager.findElementByXpath("//*[@id=\"tf:IndicatorNature_generalTable:column1:filter\"]")
        .sendKeys(first_language);
    driverManager.findElementByXpath("//*[@id=\"tf:IndicatorNature_generalTable:column1:filter\"]")
        .sendKeys(Keys.RETURN);
    driverManager.checkValue(first_language,
        "//*[@id='tf:IndicatorNature_generalTable_data']/tr/td[4]/div/span/span/table/tbody/tr/td/label");
    driverManager
        .findElementByXpath(
            "//*[@id='tf:IndicatorNature_generalTable_data']/tr/td[4]/div/span/span/table/tbody/tr/td")
        .click();
    driverManager.findElementByXpath("//*[@title='Pick']").click();
    driverManager.switchToFrame("ViewUsersList");
    driverManager.checkAttributeValue(first_language,
        "//*[@id='tf:ViewUsersList_generalTable:0:j_id126_LookupInputText']", "value",
        reverseCondition);
    // select second language
    driverManager.findElementByXpath("//button[@title='secondLanguage.valueTranslated']").click();
    driverManager.switchToLookUp("IndicatorNature");
    driverManager.waitForElement(
        By.xpath("//*[@id=\"tf:IndicatorNature_generalTable:column1:filter\"]"), reverseCondition);
    driverManager.findElementByXpath("//*[@id=\"tf:IndicatorNature_generalTable:column1:filter\"]")
        .clear();
    driverManager.findElementByXpath("//*[@id=\"tf:IndicatorNature_generalTable:column1:filter\"]")
        .sendKeys(second_language);
    driverManager.findElementByXpath("//*[@id=\"tf:IndicatorNature_generalTable:column1:filter\"]")
        .sendKeys(Keys.RETURN);
    driverManager.checkValue(second_language,
        "//*[@id='tf:IndicatorNature_generalTable_data']/tr/td[4]/div/span/span/table/tbody/tr/td/label");
    driverManager
        .findElementByXpath(
            "//*[@id='tf:IndicatorNature_generalTable_data']/tr/td[4]/div/span/span/table/tbody/tr/td")
        .click();
    driverManager.findElementByXpath("//*[@title='Pick']").click();
    driverManager.switchToFrame("ViewUsersList");
    driverManager.checkAttributeValue(second_language,
        "//*[@id='tf:ViewUsersList_generalTable:0:j_id148_LookupInputText']", "value",
        reverseCondition);
    // save
    driverManager
        .findElementByXpath("//*[@id='tf:ViewUsersList_generalTable:0:j_id34_rowEditor']/span[2]")
        .click();
    driverManager.waitForSuccess();
    // open ballon
    driverManager
        .findElementByXpath("//*[@id='tf:ViewUsersList_generalTable:0:openButton_12']/span")
        .click();
    // choose role
    driverManager
        .findElementByXpath(
            "//*[@id='tf:ViewUsersList_generalTable:0:j_id68actionTablePanel']/table[4]/tbody/tr/td[2]/a")
        .click();

    driverManager.switchToLookUp("UserRole");
    // add new role
    driverManager.waitForElement(By.xpath("//*[@title='New Row']"), reverseCondition);
    // first role
    driverManager.findElementByXpath("//*[@title='New Row']").click();
    driverManager.waitForElement(
        By.xpath("//button[contains(@id,'tf:UserRole_generalTable:0:orole_name_Lookup__')]"),
        reverseCondition);
    driverManager.findElementByXpath(
        "//button[contains(@id,'tf:UserRole_generalTable:0:orole_name_Lookup__')]").click();

    driverManager.switchToLookUp("LookupRole");
    driverManager.waitForElement(By.xpath("//*[@id='tf:LookupRole_generalTable:column0:filter']"),
        reverseCondition);
    driverManager.findElementByXpath("//*[@id='tf:LookupRole_generalTable:column0:filter']")
        .clear();
    driverManager.findElementByXpath("//*[@id='tf:LookupRole_generalTable:column0:filter']")
        .sendKeys(firstRole);
    driverManager.findElementByXpath("//*[@id='tf:LookupRole_generalTable:column0:filter']")
        .sendKeys(Keys.ENTER);
    driverManager.checkValue(firstRole,
        "//*[@id='tf:LookupRole_generalTable:0:j_id76_ColCommLINK']");
    driverManager
        .findElementByXpath(
            "//*[@id='tf:LookupRole_generalTable_data']/tr/td[3]/div/span/span/table/tbody/tr/td")
        .click();
    driverManager.findElementByXpath("//*[@title='Pick']").click();

    driverManager.switchToLookUp("UserRole");
    driverManager.checkAttributeValue(firstRole,
        "//*[@id='tf:UserRole_generalTable:0:j_id60_LookupInputText']", "value", reverseCondition);
    // save first role
    driverManager
        .findElementByXpath("//*[@id='tf:UserRole_generalTable:0:j_id33_rowEditor']/span[2]")
        .click();
    driverManager.waitForSuccess();
    // add second role
    driverManager.findElementByXpath("//*[@title='New Row']").click();
    driverManager.waitForElement(
        By.xpath("//button[contains(@id,'tf:UserRole_generalTable:0:orole_name_Lookup__')]"),
        reverseCondition);
    driverManager.findElementByXpath(
        "//button[contains(@id,'tf:UserRole_generalTable:0:orole_name_Lookup__')]").click();

    driverManager.switchToLookUp("LookupRole");
    driverManager.waitForElement(By.xpath("//*[@id='tf:LookupRole_generalTable:column0:filter']"),
        reverseCondition);
    driverManager.findElementByXpath("//*[@id='tf:LookupRole_generalTable:column0:filter']")
        .clear();
    driverManager.findElementByXpath("//*[@id='tf:LookupRole_generalTable:column0:filter']")
        .sendKeys(secondRole);
    driverManager.findElementByXpath("//*[@id='tf:LookupRole_generalTable:column0:filter']")
        .sendKeys(Keys.ENTER);
    driverManager.checkValue(secondRole,
        "//*[@id='tf:LookupRole_generalTable:0:j_id76_ColCommLINK']");
    driverManager
        .findElementByXpath(
            "//*[@id='tf:LookupRole_generalTable_data']/tr/td[3]/div/span/span/table/tbody/tr/td")
        .click();
    driverManager.findElementByXpath("//*[@title='Pick']").click();
    driverManager.switchToLookUp("UserRole");
    driverManager.checkAttributeValue(secondRole,
        "//*[@id='tf:UserRole_generalTable:0:j_id60_LookupInputText']", "value", reverseCondition);
    // save second role
    driverManager
        .findElementByXpath("//*[@id='tf:UserRole_generalTable:0:j_id33_rowEditor']/span[2]")
        .click();
    driverManager.waitForSuccess();
    driver.switchTo().defaultContent();
    driverManager.findElementByXpath("//*/div[@id='UserRole']/div[1]/div[1]/div/button").click();
    // removePortlet();
    // sign out and login
    logout();
    // login(user_name, password);
    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    System.setProperty("username", userName);
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }


}
