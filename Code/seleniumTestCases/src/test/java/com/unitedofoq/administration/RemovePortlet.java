package com.unitedofoq.administration;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import com.unitedofoq.fabsbase.BaseClass;

public class RemovePortlet extends BaseClass {
  private String closeFrame;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    closeFrame = "ViewUsersList";
    login();
  }

  @Test
  public void testRemovePortlet() throws Exception {
    driver.switchTo().defaultContent();
    driverManager.waitForElement(By.cssSelector("iframe[title='" + closeFrame + "']"),
        reverseCondition);
    driverManager.switchToFrame(closeFrame);
    driverManager.waitForElement(By.xpath("//*[contains(@id,'_Options_button')]"),
        reverseCondition);
    driver.switchTo().defaultContent();
    driverManager.findElementByXpath(
        "//*/div[@id='layout-column_column-1']/div[1]/section/header/menu/div/a/i[1]").click();
    driverManager.findElementByXpath("/html/body/div[1]/div/div/div/*/li[6]/a/span").click();
    driverManager.acceptAlert();
    driver.close();
    // System.out.println(x);

  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }
}
