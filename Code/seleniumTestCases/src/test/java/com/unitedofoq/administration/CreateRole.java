package com.unitedofoq.administration;

import static com.unitedofoq.util.FrameUtil.get;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import com.unitedofoq.fabsbase.BaseClass;
import com.unitedofoq.util.FrameUtil;

public class CreateRole extends BaseClass {

  private FrameUtil fu = new FrameUtil(driver);
  private String rolename = "autotestroll" + String.valueOf(random);

  @Before
  public void setUp() {
    login();

  }

  @Test
  public void check() {
    try {
      // create role
      createRole();
      // create menu for role
      createMenuRole();
      // create access privilege
      createPRole();

    } catch (Exception e) {
      // TODO Auto-generated catch block
      System.out.println(e.getMessage());
    }

  }

  private void createPRole() {
    // open privilege
    driverManager.findElementByXpath(".//*[@id='tf:Role_generalTable:0:openButton_0_0']").click();
    driverManager.findElementByXpath("//a[contains(.,'Privileges')]").click();
    driverManager.switchToLookUp("RolePrivilege");
    driverManager.waitForElement(By.xpath("//button[@id='tf:j_id28_button']"), reverseCondition);
    // add new privilege
    driverManager.findElementByXpath("//button[@id='tf:j_id28_button']").click();
    driverManager.waitForElement(By.xpath("//button[@title='oprivilege.name']"), reverseCondition);
    driverManager.findElementByXpath("//button[@title='oprivilege.name']").click();
    // choose role
    driverManager.switchToLookUp("LookupPrivilege");
    driverManager.waitForElement(By.xpath("//input[@role='textbox']"), reverseCondition);
    fu.cES("//input[@role='textbox']", get("entity"));
    driverManager.waitForElement(
        By.xpath(".//*[@id='tf:LookupPrivilege_generalTable:0:cell0']/span[1]/table/tbody/tr/td"),
        false);
    driverManager
        .findElementByXpath(
            ".//*[@id='tf:LookupPrivilege_generalTable:0:cell0']/span[1]/table/tbody/tr/td")
        .click();
    driverManager.findElementByXpath("//button[@title='Pick']").click();
    driver.switchTo().defaultContent();
    driverManager.switchToLookUp("RolePrivilege");
    driverManager.waitForElement(
        By.xpath("..//*[@id='tf:RolePrivilege_generalTable:0:j_id33_rowEditor']/span[2]"),
        reverseCondition);
    driverManager
        .findElementByXpath(".//*[@id='tf:RolePrivilege_generalTable:0:j_id33_rowEditor']/span[2]")
        .click();
    driverManager.findElementByXpath("//button[@id='tf:j_id30_button']").click();
  }

  private void createMenuRole() {
    fu.cES("//input[@id='tf:Role_generalTable:column0:filter']", rolename);
    driverManager.findElementByXpath(".//*[@id='tf:Role_generalTable:0:openButton_0_0']").click();
    driverManager.findElementByXpath("//a[contains(.,'Menus')]").click();
    driverManager.switchToLookUp("RoleMenu");
    driverManager.waitForElement(By.xpath("//button[@id='tf:j_id28_button']"), reverseCondition);
    driverManager.findElementByXpath("//button[@id='tf:j_id28_button']").click();
    driverManager.waitForElement(By.xpath("//button[@title='omenu.nameTranslated']"),
        reverseCondition);
    driverManager.findElementByXpath("//button[@title='omenu.nameTranslated']").click();
    driverManager.switchToLookUp("MenuTree");
    driverManager.waitForElement(
        By.xpath(".//*[@id='frmScrForm:multiLevelTree:0_4']/span/span[3]/span"), reverseCondition);
    driverManager.findElementByXpath(".//*[@id='frmScrForm:multiLevelTree:0_4']/span/span[3]/span")
        .click();
    driverManager.findElementByXpath("//button[@title='Pick']").click();
    driver.switchTo().defaultContent();
    driverManager.switchToLookUp("RoleMenu");
    driverManager.waitForElement(
        By.xpath(".//*[@id='tf:RoleMenu_generalTable:0:j_id33_rowEditor']/span[2]"),
        reverseCondition);
    driverManager
        .findElementByXpath(".//*[@id='tf:RoleMenu_generalTable:0:j_id33_rowEditor']/span[2]")
        .click();

    driverManager.findElementByXpath(".//*[@id='tf:j_id30_button']").click();
  }

  private void createRole() throws Exception {
    driver.findElement(By.linkText("Administration")).click();
    driver.findElement(By.linkText("User Role Management")).click();
    // wait for page to load
    fu.waitSwitch("Role");
    driverManager.waitForElement(By.xpath("//button[@id='tf:j_id28_button']"), reverseCondition);
    // add new role
    driverManager.findElementByXpath("//button[@id='tf:j_id28_button']").click();
    driverManager.waitForElement(By.xpath("//input[@id='tf:Role_generalTable:0:name_TextField_0']"),
        reverseCondition);
    driverManager.findElementByXpath("//input[@id='tf:Role_generalTable:0:name_TextField_0']")
        .sendKeys(rolename);
    fu.module("FOUNDATION");
    driverManager.findElementByXpath(".//*[@id='tf:Role_generalTable:0:j_id32_rowEditor']/span[2]")
        .click();

    driverManager.waitForSuccess();
  }
}
