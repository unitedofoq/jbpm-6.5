package com.unitedofoq.fabscore;

import org.openqa.selenium.By;



public class Screen extends PersonnelBaseClass {

  public void start(String screenname, String special_screen_name, String special_screen_header,
      String Act_on_Ent) {

    try {
      waitForPopUp(driver);
      waitframeToBeAvailableAndSwitchToIt(screenname);
      findElementByXpath(xby("//*[@title='New Row']"), actiontype.click, "");
      findElementByXpath(xby("//*[@title='oactOnEntity.entityClassPath']"), actiontype.click, "");
      findElementByXpath(
          xby("//*[@id='tf:ManageMultiLevelScreens_generalTable:0:name_TextField_0']"),
          actiontype.clearandsend, special_screen_name);
      findElementByXpath(
          xby("//*[@id='tf:ManageMultiLevelScreens_generalTable:0:headerTranslated_TextField_1']"),
          actiontype.clearandsend, special_screen_header);
      lookup(Act_on_Ent);

      waitframeToBeAvailableAndSwitchToIt(screenname);

    } catch (Throwable e) {
      System.out.println(e.getMessage());
    }
  }

  private void lookup(String Act_on_Ent) throws Exception, InterruptedException {
    try {
      switchToLookUp("LookupEntity", driver);
      findElementByXpath(xby("//*[@id='tf:LookupEntity_generalTable:column0:filter']"),
          actiontype.clearandsendenter, Act_on_Ent);
      waitforelementtext(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']"),
          Act_on_Ent);

      findElementByXpath(
          By.xpath("id('tf:LookupEntity_generalTable_paginator_bottom')/span[4]/span[2]"),
          actiontype.click, "");
      findElementByXpath(
          By.xpath(
              "/html/body/form/table/tbody/tr[2]/td/table/tbody/tr[2]/td/div/div/div/table/tbody/tr[1]/td[3]/div/span/span[1]/table/tbody/tr/td"),
          actiontype.click, "");


      findElementByXpath(By.xpath("//*[@title='Pick']"), actiontype.click, "");
    } catch (Throwable E) {
      System.out.println(E.getMessage());
    }
  }
}
