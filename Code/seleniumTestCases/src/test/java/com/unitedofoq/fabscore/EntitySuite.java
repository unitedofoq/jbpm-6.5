package com.unitedofoq.fabscore;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ DDGeneration.class, Entity.class, FormScreensAutoCreation.class, TabularScreenAutoCreated.class })
public class EntitySuite {

}
