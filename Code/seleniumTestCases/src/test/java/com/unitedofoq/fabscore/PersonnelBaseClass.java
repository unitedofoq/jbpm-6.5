package com.unitedofoq.fabscore;

import static org.junit.Assert.fail;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Predicate;

public class PersonnelBaseClass {
  public double random;
  WebDriver driver;
  public String textelement;
  public String attvalue;
  public boolean matchs = false;

  public enum actiontype {
    sendkey, click, gettext, getattvalue, clear, enter, matchs, clearandsend, clearandsendenter, waitfortext, waitforvisable
  }
  public enum selectortype {
    classname, cssselector, id, linktext, name, partialLinkText, tagName, xpath
  }

  PersonnelBaseClass() {

    // TODO Auto-generated constructor stub
    random = Math.floor((Math.random() * 1000) + 1);
    // System.setProperty("random", String.valueOf(random));

  }

  public void pressenter(String elementname) throws Exception {
    // driver.findElement(By.xpath(elementname)).sendKeys(Keys.ENTER);
    findElementByXpath(By.xpath(elementname), actiontype.enter, "");
  }

  public void Login(WebDriver _driver) {

    driver = _driver;
    driver.get("https://1.1.2.14:8181");
    driver.manage().window().maximize();
    finditbyname("_58_login", "test");
    finditbyname("_58_password", "test");

    finditbyidandclick("_58_login");
    driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    driver.switchTo().frame("DockMenuFrame");


  }

  public void select(String name) {
    driver.switchTo().frame(name);
  }

  public By xby(String frame) {
    return By.xpath(frame);
  }

  public actionvalueparam ab(actiontype action, String value) {
    actionvalueparam x = new actionvalueparam();
    x.action = action;
    x.value = value;
    return x;
  }

  public void finditbyname(String name, String action) {
    WebElement element = driver.findElement(By.name(name));
    element.sendKeys(action);


  }

  public void finditbyid(String id, String action) {
    WebElement element = driver.findElement(By.id(id));
    element.sendKeys(action);

  }

  public void finditbyidandclick(String id) {

    WebElement element = driver.findElement(By.id(id));
    element.submit();
  }

  public void waitForElementClickAble(String name) throws Exception {
    waitForElementClickAble(By.xpath(name));
  }

  public void waitForElementClickAble(By by) throws Exception {
    try {
      WebDriverWait wait = new WebDriverWait(driver, 15, 1000);

      wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    } catch (Throwable e) {
      System.out.println(e.getMessage());
      Thread.sleep(500);
      waitForElementClickAble(by);
    }
  }


  public void waitUntilText(By by, String _regex) {
    try {
      final WebElement webElement = by.findElement(driver);
      final String regex = _regex;

      new FluentWait<WebDriver>(driver).withTimeout(15, TimeUnit.SECONDS)
          .pollingEvery(500, TimeUnit.MILLISECONDS).until(new Predicate<WebDriver>() {

            public boolean apply(WebDriver d) {
              return (webElement.getText().matches(regex));
            }
          });
    } catch (Throwable e) {
      System.out.println(e.getMessage());
    }
  }


  public void waitforelementtext(By by, String text) throws InterruptedException {
    try {
      WebDriverWait wait = new WebDriverWait(driver, 15, 1000);
      wait.until(ExpectedConditions.textToBePresentInElementValue(by, text));
    } catch (Throwable e) {
      System.out.println(e.getMessage());
      Thread.sleep(500);
      waitforelementtext(by, text);
    }
  }

  public void waitframeToBeAvailableAndSwitchToIt(String framename) throws InterruptedException {
    waitForPopUp(driver);
    driver.switchTo().defaultContent();
    By by = By.cssSelector("iframe[title='" + framename + "']");
    try {

      WebDriverWait wait = new WebDriverWait(driver, 15, 1000);
      wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(by));
    } catch (Throwable e) {
      System.out.println(e.getMessage());
      Thread.sleep(500);
      waitframeToBeAvailableAndSwitchToIt(framename);
    }

  }

  public WebElement getElement(String str, selectortype type) {

    switch (type) {
      case classname:
        return driver.findElement(By.className(str));

      case cssselector:
        return driver.findElement(By.cssSelector(str));

      case id:
        return driver.findElement(By.id(str));

      case linktext:
        return driver.findElement(By.linkText(str));

      case name:
        return driver.findElement(By.name(str));

      case partialLinkText:
        return driver.findElement(By.partialLinkText(str));

      case tagName:
        return driver.findElement(By.tagName(str));

      case xpath:
        return driver.findElement(By.xpath(str));


      default:
        return driver.findElement(By.xpath(str));
    }
  }

  public void findElementByXpath(By by, actiontype action, String value) throws Exception {
    try {
      String ElementsName = by.toString();

      waitForElementClickAble(by);


      WebElement element = driver.findElement(by);


      System.out.println("after wait" + ElementsName);
      if (action == actiontype.click) {
        element.click();
      }
      if (action == actiontype.sendkey) {
        element.sendKeys(value);
      }
      if (action == actiontype.gettext) {
        textelement = "";
        textelement = element.getText();
        System.out.println(textelement);
      }
      if (action == actiontype.getattvalue) {
        attvalue = element.getAttribute(value);
      }
      if (actiontype.clear == action) {
        element.clear();
      }
      if (actiontype.enter == action) {
        element.sendKeys(Keys.ENTER);
      }

      if (actiontype.clearandsend == action) {
        element.clear();
        element.sendKeys(value);
      }

      if (actiontype.clearandsendenter == action) {
        element.clear();
        element.sendKeys(value);
        element.sendKeys(Keys.ENTER);
      }
      if (actiontype.matchs == action) {
        matchs = driver.findElement(by).getText().matches(value);


      }
      if (actiontype.waitfortext == action) {
        waitforelementtext(by, value);
      }

      if (actiontype.waitforvisable == action) {
        waitForElementClickAble(by);
      }

      System.out.println("after action" + ElementsName + " " + action.toString() + " " + value);
    } catch (Throwable e) {
      System.out.println(driver.getCurrentUrl());
      System.out.println("frame name" + driver.getWindowHandle().toString());
      System.out.println(by.toString());
      System.out.println(e.getMessage());
      Thread.sleep(500);
      findElementByXpath(by, action, value);
    }
  }

  public void findElementByXpath(List<By> list, actiontype action, String value) {
    for (By item : list) {
      try {
        findElementByXpath(item, action, value);
      } catch (Throwable e) {
        System.out.println(e.getMessage());
      }
    }
  }

  public void findElementByXpath(Map<By, actionvalueparam> map) {
    for (Map.Entry<By, actionvalueparam> xmap : map.entrySet()) {
      By by = xmap.getKey();
      String value = xmap.getValue().value;
      actiontype action = xmap.getValue().action;

      try {
        findElementByXpath(by, action, value);
      } catch (Exception e) {


        System.out.println(e.getMessage());
      }
    }
  }

  public void waitForElement(By by, WebDriver driver) throws Exception {
    for (int second = 0;; second++) {
      if (second >= 60) {
        fail("timeout");
      }
      try {
        if (isElementPresent(by, driver)) {
          break;
        }
      } catch (Exception e) {
      }
      Thread.sleep(1000);
    }
    System.out.println("wait has been finished" + by.toString());
  }

  public void waitForElement(By by, int t, WebDriver driver) throws Exception {
    for (int second = 0;; second++) {
      if (second >= t) {
        fail("timeout");
      }
      try {
        if (isElementPresent(by, driver)) {
          break;
        }
      } catch (Exception e) {
      }
      Thread.sleep(1000);
    }
  }

  private boolean isElementPresent(By by, WebDriver driver) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public void switchToFrame(String frame, WebDriver driver) throws InterruptedException {

    driver.switchTo().defaultContent();
    try {
      waitForElementClickAble(By.cssSelector("//iframe[title='" + frame + "']"));
      driver.switchTo().frame(driver.findElement(By.cssSelector("iframe[title='" + frame + "']")));
    } catch (Throwable e) {
      // TODO Auto-generated catch block
      System.out.println(e.getMessage());
      Thread.sleep(500);
      switchToFrame(frame, driver);
    }


  }

  public void switchToLookUp(String frame, WebDriver driver) {

    driver.switchTo().defaultContent();
    try {



      waitForElementClickAble(By.xpath("//iframe[contains(@src,'" + frame + "')]"));


      driver.switchTo()
          .frame(driver.findElement(By.xpath("//iframe[contains(@src,'" + frame + "')]")));
    } catch (Throwable e) {
      System.out.println(e.getMessage());
    }
  }

  public void waitForPopUp(WebDriver driver) {
    try {
      while (driver.getWindowHandles().size() < 2) {
      }

      Set<String> windowId = driver.getWindowHandles(); // get window id of
                                                        // current window
      for (String hnd : windowId) {
        if (!hnd.equals(windowId)) {
          driver.switchTo().window(hnd);
        }
      }
      driver.switchTo().defaultContent();
      driver.manage().window().maximize();
    } catch (Throwable e) {
      System.out.println(e.getMessage());
    }
  }

  public void waitForSuccess(WebDriver driver) throws Exception {
    for (int second = 0;; second++) {
      if (second >= 60) {
        fail("timeout");
      }
      try {
        if (driver.findElement(By.cssSelector("BODY")).getText()
            .matches("^[\\s\\S]*Success\\(es\\)[\\s\\S]*$")) {
          break;
        }
      } catch (Exception e) {
      }
      Thread.sleep(1000);
    }
  }

  public void waitForSuccess(int s, WebDriver driver) throws Exception {
    for (int second = 0;; second++) {
      if (second >= s) {
        fail("timeout");
      }
      try {
        if (driver.findElement(By.cssSelector("BODY")).getText()
            .matches("^[\\s\\S]*Success\\(es\\)[\\s\\S]*$")) {
          break;
        }
      } catch (Exception e) {
      }
      Thread.sleep(1000);
    }

  }

  public void checkValue(String value, String path, WebDriver driver) throws Exception {
    for (int second = 0;; second++) {
      findElementByXpath(By.xpath(path), actiontype.gettext, "");
      if (second >= 60) {
        fail("timeout");
      }
      try {
        if (value.equals(textelement)) {
          ;
        }
        break;
      } catch (Exception e) {

      }
      Thread.sleep(1000);
    }
  }
}
