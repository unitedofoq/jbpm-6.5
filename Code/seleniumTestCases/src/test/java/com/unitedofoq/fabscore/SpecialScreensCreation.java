package com.unitedofoq.fabscore;



import org.junit.*;
import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;


public class SpecialScreensCreation extends PersonnelBaseClass {
  private WebDriver driver;


  private StringBuffer verificationErrors = new StringBuffer();
  String special_screen_name = "FUN_UNITTREE_X_0001_oneComp_" + random;
  String special_screen_header = "Unit_Tree_Test_" + random;
  String Act_on_Ent = "Unit";
  // Screen Fields:
  String field_expression_1 = "nameTranslated";
  String field_expression_2 = "childUnit";

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();

    Login(driver);
  }

  public void initializevariables() {
    // Tabular Screens:
    System.setProperty("special_screen_name", "FUN_UNITTREE_X_0001_oneComp_" + random);
    System.setProperty("special_screen_header", "Unit_Tree_Test_" + random);
    Act_on_Ent = "Unit";

    // Screen Fields:
    field_expression_1 = "nameTranslated";
    field_expression_2 = "childUnit";


  }
  /*
   * @Test // public void testSpecialScreensCreation() throws Exception { // // Special Screens: //
   * // // ERROR: Caught exception [ERROR: Unsupported command [waitForPopUp | _blank | ]] // //
   * ERROR: Caught exception [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR:
   * Caught exception [ERROR: Unsupported command [waitForFrameToLoad |
   * //iframe[contains(@src,'ManageMultiLevelScreens')] | ]] // // ERROR: Caught exception [ERROR:
   * Unsupported command [selectFrame | //iframe[contains(@src,'ManageMultiLevelScreens')] | ]] //
   * for (int second = 0;; second++) { // if (second >= 60) fail("timeout"); // try { if
   * (isElementPresent(By.xpath("//*[@title='New Row']"))) break; } catch (Exception e) {} //
   * Thread.sleep(1000); // } // // driver.findElement(By.xpath("//*[@title='New Row']")).click();
   * // for (int second = 0;; second++) { // if (second >= 60) fail("timeout"); // try { if
   * (isElementPresent(By.xpath(
   * "//*[@id='tf:ManageMultiLevelScreens_generalTable:0:name_TextField_0']"))) break; } catch
   * (Exception e) {} // Thread.sleep(1000); // } // // driver.findElement(By.xpath(
   * "//*[@id='tf:ManageMultiLevelScreens_generalTable:0:name_TextField_0']")).clear(); //
   * driver.findElement(By.xpath(
   * "//*[@id='tf:ManageMultiLevelScreens_generalTable:0:name_TextField_0']")).sendKeys(
   * special_screen_name); // // ERROR: Caught exception [ERROR: Unsupported command [fireEvent |
   * //*[@id='tf:ManageMultiLevelScreens_generalTable:0:name_TextField_0'] | blur]] //
   * driver.findElement(By.xpath(
   * "//*[@id='tf:ManageMultiLevelScreens_generalTable:0:headerTranslated_TextField_1']")).clear();
   * // driver.findElement(By.xpath(
   * "//*[@id='tf:ManageMultiLevelScreens_generalTable:0:headerTranslated_TextField_1']")).sendKeys(
   * special_screen_header); // // ERROR: Caught exception [ERROR: Unsupported command [fireEvent |
   * //*[@id='tf:ManageMultiLevelScreens_generalTable:0:headerTranslated_TextField_1'] | blur]] //
   * driver.findElement(By.xpath("//*[@title='oactOnEntity.entityClassPath']")).click(); // //
   * ERROR: Caught exception [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR:
   * Caught exception [ERROR: Unsupported command [selectFrame |
   * //iframe[contains(@src,'LookupEntity')] | ]] // for (int second = 0;; second++) { // if (second
   * >= 60) fail("timeout"); // try { if
   * (isElementPresent(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']"))) break; }
   * catch (Exception e) {} // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']")).clear();
   * //
   * driver.findElement(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']")).sendKeys
   * (Act_on_Ent); // // ERROR: Caught exception [ERROR: Unsupported command [keyUp |
   * //*[@id='tf:LookupEntity_generalTable:column0:filter'] | \13]] // driver.findElement(By.xpath(
   * "//*[@id='tf:LookupEntity_generalTable:6:cell0']/span[1]/table/tbody/tr/td")).click(); // try {
   * // assertEquals(Act_on_Ent, driver.findElement(By.xpath(
   * "//*[@id='tf:LookupEntity_generalTable:6:cell0']/span[1]/table/tbody/tr/td/a")).getText(); // }
   * catch (Error e) { // verificationErrors.append(e.toString(); // } //
   * driver.findElement(By.xpath("//*[@title='Pick']")).click(); // // ERROR: Caught exception
   * [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR: Caught exception [ERROR:
   * Unsupported command [selectFrame | //iframe[contains(@src,'ManageMultiLevelScreens')] | ]] //
   * try { // assertEquals("com.unitedofoq.otms.foundation.unit.Unit", driver.findElement(By.xpath(
   * "//*[@id='tf:ManageMultiLevelScreens_generalTable:0:j_id82_LookupInputText']")).getAttribute(
   * "value"); // } catch (Error e) { // verificationErrors.append(e.toString(); // } //
   * driver.findElement(By.xpath(
   * "//*[@id='tf:ManageMultiLevelScreens_generalTable:0:type_valueTranslated_Drop-down_5']/div[2]/span"
   * )).click(); // driver.findElement(By.xpath(
   * "//*[@id='tf:ManageMultiLevelScreens_generalTable:0:type_valueTranslated_Drop-down_5_panel']/div/ul/li[text()='Tree']"
   * )).click(); // driver.findElement(By.xpath(
   * "//*[@id='tf:ManageMultiLevelScreens_generalTable:0:omodule_moduleTitleTranslated_Drop-down_6']/div[2]/span"
   * )).click(); // driver.findElement(By.xpath(
   * "//*[@id='tf:ManageMultiLevelScreens_generalTable:0:omodule_moduleTitleTranslated_Drop-down_6_panel']/div/ul/li[text()='FOUNDATION']"
   * )).click(); // driver.findElement(By.xpath(
   * "//*[@id='tf:ManageMultiLevelScreens_generalTable:0:j_id33_rowEditor']/span[2]")).click(); //
   * // Warning: waitForTextPresent may require manual changes // for (int second = 0;; second++) {
   * // if (second >= 60) fail("timeout"); // try { if
   * (driver.findElement(By.cssSelector("BODY")).getText().matches(
   * "^[\\s\\S]*Success\\(es\\)[\\s\\S]*$")) break; } catch (Exception e) {} // Thread.sleep(1000);
   * // } // //
   * driver.findElement(By.xpath("//*[@id='tf:ManageMultiLevelScreens_generalTable:column0:filter']"
   * )).clear(); //
   * driver.findElement(By.xpath("//*[@id='tf:ManageMultiLevelScreens_generalTable:column0:filter']"
   * )).sendKeys(special_screen_name); // // ERROR: Caught exception [ERROR: Unsupported command
   * [keyUp | //*[@id='tf:ManageMultiLevelScreens_generalTable:column0:filter'] | \13]] //
   * driver.findElement(By.xpath(
   * "//*[@id='tf:ManageMultiLevelScreens_generalTable:0:cell0']/span[1]/table/tbody/tr/td")).click(
   * ); // driver.findElement(By.xpath(
   * "//*[@id='tf:ManageMultiLevelScreens_generalTable:0:openButton_0_0']")).click(); //
   * driver.findElement(By.linkText("Inputs")).click(); // // ERROR: Caught exception [ERROR:
   * Unsupported command [selectWindow | _blank | ]] // // ERROR: Caught exception [ERROR:
   * Unsupported command [selectFrame | //iframe[contains(@src,'ScreenInput')] | ]] // for (int
   * second = 0;; second++) { // if (second >= 60) fail("timeout"); // try { if
   * (isElementPresent(By.xpath("//*[@title='New Row']"))) break; } catch (Exception e) {} //
   * Thread.sleep(1000); // } // // driver.findElement(By.xpath("//*[@title='New Row']")).click();
   * // for (int second = 0;; second++) { // if (second >= 60) fail("timeout"); // try { if
   * (isElementPresent(By.xpath("//*[@title='odataType.name']"))) break; } catch (Exception e) {} //
   * Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath("//*[@title='odataType.name']")).click(); // // ERROR: Caught
   * exception [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR: Caught exception
   * [ERROR: Unsupported command [selectFrame | //iframe[contains(@src,'LookupDataType')] | ]] //
   * for (int second = 0;; second++) { // if (second >= 60) fail("timeout"); // try { if
   * (isElementPresent(By.xpath("//*[@id='tf:LookupDataType_generalTable:column0:filter']"))) break;
   * } catch (Exception e) {} // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath("//*[@id='tf:LookupDataType_generalTable:column0:filter']")).clear(
   * ); // driver.findElement(By.xpath("//*[@id='tf:LookupDataType_generalTable:column0:filter']")).
   * sendKeys("VoidDT"); // // ERROR: Caught exception [ERROR: Unsupported command [keyUp |
   * //*[@id='tf:LookupDataType_generalTable:column0:filter'] | \13]] //
   * driver.findElement(By.xpath(
   * "//*[@id='tf:LookupDataType_generalTable:1:cell0']/span[1]/table/tbody/tr/td")).click(); // try
   * { // assertEquals("VoidDT", driver.findElement(By.xpath(
   * "//*[@id='tf:LookupDataType_generalTable:1:cell0']/span[1]/table/tbody/tr/td/a")).getText(); //
   * } catch (Error e) { // verificationErrors.append(e.toString(); // } //
   * driver.findElement(By.xpath("//*[@id='tf:j_id29returnSelectedBTN']")).click(); // // ERROR:
   * Caught exception [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR: Caught
   * exception [ERROR: Unsupported command [selectFrame | //iframe[contains(@src,'ScreenInput')] |
   * ]] // for (int second = 0;; second++) { // if (second >= 60) fail("timeout"); // try { if
   * ("VoidDT".equals(driver.findElement(By.xpath(
   * "//*[@id='tf:ScreenInput_generalTable:0:j_id57_LookupInputText']")).getAttribute("value")))
   * break; } catch (Exception e) {} // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath("//*[@id='tf:ScreenInput_generalTable:0:j_id33_rowEditor']/span[2]"
   * )).click(); // for (int second = 0;; second++) { // if (second >= 60) fail("timeout"); // try {
   * if (isElementPresent(By.xpath(
   * "//*[@id='tf:ScreenInput_generalTable:0:cell0']/span[1]/table/tbody/tr/td/table/tbody/tr/td")))
   * break; } catch (Exception e) {} // Thread.sleep(1000); // } // // // ERROR: Caught exception
   * [ERROR: Unsupported command [selectWindow | _blank | ]] //
   * driver.findElement(By.xpath("//*[@id='closethick']")).click(); // // ERROR: Caught exception
   * [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR: Caught exception [ERROR:
   * Unsupported command [selectFrame | //iframe[contains(@src,'ManageMultiLevelScreens')] | ]] //
   * driver.findElement(By.linkText("Add Levels")).click(); // // ERROR: Caught exception [ERROR:
   * Unsupported command [selectWindow | _blank | ]] // // ERROR: Caught exception [ERROR:
   * Unsupported command [selectFrame | //iframe[contains(@src,'ManageMultiLevelScreenLevels')] | ]]
   * // for (int second = 0;; second++) { // if (second >= 60) fail("timeout"); // try { if
   * (isElementPresent(By.xpath("//*[@title='New Row']"))) break; } catch (Exception e) {} //
   * Thread.sleep(1000); // } // // driver.findElement(By.xpath("//*[@title='New Row']")).click();
   * // for (int second = 0;; second++) { // if (second >= 60) fail("timeout"); // try { if
   * (isElementPresent(By.xpath("//*[@title='actOnEntity.entityClassPath']"))) break; } catch
   * (Exception e) {} // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath("//*[@title='actOnEntity.entityClassPath']")).click(); // // ERROR:
   * Caught exception [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR: Caught
   * exception [ERROR: Unsupported command [selectFrame | //iframe[contains(@src,'LookupEntity')] |
   * ]] // for (int second = 0;; second++) { // if (second >= 60) fail("timeout"); // try { if
   * (isElementPresent(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']"))) break; }
   * catch (Exception e) {} // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']")).clear();
   * //
   * driver.findElement(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']")).sendKeys
   * (Act_on_Ent); // // ERROR: Caught exception [ERROR: Unsupported command [keyUp |
   * //*[@id='tf:LookupEntity_generalTable:column0:filter'] | \13]] // driver.findElement(By.xpath(
   * "//*[@id='tf:LookupEntity_generalTable:6:cell0']/span[1]/table/tbody/tr/td")).click(); // try {
   * // assertEquals(Act_on_Ent, driver.findElement(By.xpath(
   * "//*[@id='tf:LookupEntity_generalTable:6:cell0']/span[1]/table/tbody/tr/td/a")).getText(); // }
   * catch (Error e) { // verificationErrors.append(e.toString(); // } //
   * driver.findElement(By.xpath("//*[@title='Pick']")).click(); // // ERROR: Caught exception
   * [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR: Caught exception [ERROR:
   * Unsupported command [selectFrame | //iframe[contains(@src,'ManageMultiLevelScreenLevels')] | ]]
   * // driver.findElement(By.xpath("//*[@title='displayedField']")).click(); // // ERROR: Caught
   * exception [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR: Caught exception
   * [ERROR: Unsupported command [selectFrame | //iframe[contains(@src,'FieldsTreeBrowser')] | ]] //
   * for (int second = 0;; second++) { // if (second >= 60) fail("timeout"); // try { if
   * (isElementPresent(By.xpath("//*[@id='frmScrForm:j_id4returnSelectedBTN']"))) break; } catch
   * (Exception e) {} // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath("//span[contains(text(),'" + field_expression_1 + "')]")).click();
   * // driver.findElement(By.xpath("//*[@id='frmScrForm:j_id4returnSelectedBTN']")).click(); // //
   * ERROR: Caught exception [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR:
   * Caught exception [ERROR: Unsupported command [selectFrame |
   * //iframe[contains(@src,'ManageMultiLevelScreenLevels')] | ]] //
   * driver.findElement(By.xpath("//*[@title='parentOactOnEntity.entityClassPath']")).click(); // //
   * ERROR: Caught exception [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR:
   * Caught exception [ERROR: Unsupported command [selectFrame |
   * //iframe[contains(@src,'LookupEntity')] | ]] // for (int second = 0;; second++) { // if (second
   * >= 60) fail("timeout"); // try { if
   * (isElementPresent(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']"))) break; }
   * catch (Exception e) {} // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']")).clear();
   * //
   * driver.findElement(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']")).sendKeys
   * (Act_on_Ent); // // ERROR: Caught exception [ERROR: Unsupported command [keyUp |
   * //*[@id='tf:LookupEntity_generalTable:column0:filter'] | \13]] // driver.findElement(By.xpath(
   * "//*[@id='tf:LookupEntity_generalTable:6:cell0']/span[1]/table/tbody/tr/td")).click(); // try {
   * // assertEquals(Act_on_Ent, driver.findElement(By.xpath(
   * "//*[@id='tf:LookupEntity_generalTable:6:cell0']/span[1]/table/tbody/tr/td/a")).getText(); // }
   * catch (Error e) { // verificationErrors.append(e.toString(); // } //
   * driver.findElement(By.xpath("//*[@title='Pick']")).click(); // // ERROR: Caught exception
   * [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR: Caught exception [ERROR:
   * Unsupported command [selectFrame | //iframe[contains(@src,'ManageMultiLevelScreenLevels')] | ]]
   * // driver.findElement(By.xpath("//*[@title='fldExpInParentEntity']")).click(); // // ERROR:
   * Caught exception [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR: Caught
   * exception [ERROR: Unsupported command [selectFrame |
   * //iframe[contains(@src,'FieldsTreeBrowser')] | ]] // for (int second = 0;; second++) { // if
   * (second >= 60) fail("timeout"); // try { if
   * (isElementPresent(By.xpath("//*[@id='frmScrForm:j_id4returnSelectedBTN']"))) break; } catch
   * (Exception e) {} // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath("//span[contains(text(),'" + field_expression_2 + "')]")).click();
   * // driver.findElement(By.xpath("//*[@id='frmScrForm:j_id4returnSelectedBTN']")).click(); // //
   * ERROR: Caught exception [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR:
   * Caught exception [ERROR: Unsupported command [selectFrame |
   * //iframe[contains(@src,'ManageMultiLevelScreenLevels')] | ]] // driver.findElement(By.xpath(
   * "//*[@id='tf:ManageMultiLevelScreenLevels_generalTable:0:c2a_CheckBox_6']/div[2]")).click(); //
   * driver.findElement(By.xpath(
   * "//*[@id='tf:ManageMultiLevelScreenLevels_generalTable:0:j_id34_rowEditor']/span[2]")).click();
   * // // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | _blank | ]] //
   * driver.findElement(By.xpath("//*[@id='closethick']")).click(); // driver.close(); // }
   */

  @Test
  public void testSpecialScreensCreation() throws Exception {
    driver.findElement(By.linkText("Application Builder")).click();
    driver.findElement(By.linkText("Special Screens")).click();
    waitForPopUp(driver);
    waitframeToBeAvailableAndSwitchToIt("ManageMultiLevelScreens");


    findElementByXpath(xby("//*[@title='New Row']"), actiontype.click, "");

    findElementByXpath(xby("//*[@title='oactOnEntity.entityClassPath']"), actiontype.click, "");



    switchToLookUp("LookupEntity", driver);

    findElementByXpath(xby("//*[@id='tf:LookupEntity_generalTable:column0:filter']"),
        actiontype.clearandsendenter, Act_on_Ent);

    waitforelementtext(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']"),
        Act_on_Ent);
    findElementByXpath(
        xby("//*[@id='tf:LookupEntity_generalTable:6:cell0']/span[1]/table/tbody/tr/td"),
        actiontype.click, "");

    findElementByXpath(xby("//*[@title='Pick']"), actiontype.click, "");


    waitframeToBeAvailableAndSwitchToIt("ManageMultiLevelScreens");


    waitforelementtext(
        xby("//*[@id='tf:ManageMultiLevelScreens_generalTable:0:j_id166_LookupInputText']"),
        "com.unitedofoq.otms.foundation.unit.Unit");

    findElementByXpath(xby("//*[@id='tf:ManageMultiLevelScreens_generalTable:0:name_TextField_0']"),
        actiontype.clearandsend, special_screen_name);
    findElementByXpath(
        xby("//*[@id='tf:ManageMultiLevelScreens_generalTable:0:headerTranslated_TextField_1']"),
        actiontype.clearandsend, special_screen_header);
    findElementByXpath(
        xby("//*[@id='tf:ManageMultiLevelScreens_generalTable:0:type_valueTranslated_Drop-down_5']/div[2]/span"),
        actiontype.click, "");
    findElementByXpath(
        xby("//*[@id='tf:ManageMultiLevelScreens_generalTable:0:type_valueTranslated_Drop-down_5_panel']/div/ul/li[text()='Tree']"),
        actiontype.click, "");
    findElementByXpath(
        xby("//*[@id='tf:ManageMultiLevelScreens_generalTable:0:omodule_moduleTitleTranslated_Drop-down_6']/div[2]/span"),
        actiontype.click, "");
    findElementByXpath(
        xby("//*[@id='tf:ManageMultiLevelScreens_generalTable:0:omodule_moduleTitleTranslated_Drop-down_6_panel']/div/ul/li[text()='FOUNDATION']"),
        actiontype.click, "");
    findElementByXpath(
        xby("//*[@id='tf:ManageMultiLevelScreens_generalTable:0:j_id33_rowEditor']/span[2]"),
        actiontype.click, "");
    waitForSuccess(driver);
    findElementByXpath(xby("//*[@id='tf:ManageMultiLevelScreens_generalTable:column0:filter']"),
        actiontype.clearandsendenter, special_screen_name);

    findElementByXpath(
        xby("//*[@id='tf:ManageMultiLevelScreens_generalTable:0:cell0']/span[1]/table/tbody/tr/td"),
        actiontype.click, "");
    findElementByXpath(xby("//*[@id='tf:ManageMultiLevelScreens_generalTable:0:openButton_0_0']"),
        actiontype.click, "");
    findElementByXpath(By.linkText("Inputs"), actiontype.click, "");

    switchToLookUp("ScreenInput", driver);

    findElementByXpath(xby("//*[@title='New Row']"), actiontype.click, "");

    findElementByXpath(xby("//*[@title='odataType.name']"), actiontype.click, "");
    switchToLookUp("LookupDataType", driver);

    findElementByXpath(xby("//*[@id='tf:LookupDataType_generalTable:column0:filter']"),
        actiontype.clearandsendenter, "VoidDT");

    findElementByXpath(
        xby("//*[@id='tf:LookupDataType_generalTable:1:cell0']/span[1]/table/tbody/tr/td"),
        actiontype.click, "");


    findElementByXpath(xby("//*[@id='tf:j_id29returnSelectedBTN']"), actiontype.click, "");
    switchToLookUp("ScreenInput", driver);
    waitforelementtext(xby("//*[@id='tf:ScreenInput_generalTable:0:j_id66_LookupInputText']"),
        "VoidDT");

    findElementByXpath(xby("//*[@id='tf:ScreenInput_generalTable:0:j_id33_rowEditor']/span[2]"),
        actiontype.click, "");
    waitForElementClickAble(
        "//*[@id='tf:ScreenInput_generalTable:0:cell0']/span[1]/table/tbody/tr/td/table/tbody/tr/td");
    findElementByXpath(xby("//*[@id='closethick']"), actiontype.click, "");

    waitframeToBeAvailableAndSwitchToIt("ManageMultiLevelScreens");
    findElementByXpath(By.linkText("Add Levels"), actiontype.click, "");

    waitframeToBeAvailableAndSwitchToIt("ManageMultiLevelScreenLevels");
    waitForElementClickAble("//*[@title='New Row']");
    findElementByXpath(xby("//*[@title='New Row']"), actiontype.click, "");
    waitForElementClickAble("//*[@title='actOnEntity.entityClassPath']");
    findElementByXpath(By.xpath("//*[@title='actOnEntity.entityClassPath']"), actiontype.click, "");
    switchToLookUp("LookupEntity", driver);
    waitForElementClickAble("//*[@id='tf:LookupEntity_generalTable:column0:filter']");
    findElementByXpath(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']"),
        actiontype.clearandsend, Act_on_Ent);

    findElementByXpath(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']"),
        actiontype.enter, "");

    findElementByXpath(
        By.xpath("//*[@id='tf:LookupEntity_generalTable:6:cell0']/span[1]/table/tbody/tr/td"),
        actiontype.click, "");
    waitforelementtext(
        By.xpath("//*[@id='tf:LookupEntity_generalTable:6:cell0']/span[1]/table/tbody/tr/td/a"),
        Act_on_Ent);
    findElementByXpath(xby("//*[@title='Pick']"), actiontype.click, "");

    switchToLookUp("FieldsTreeBrowser", driver);

    findElementByXpath(By.xpath("//*[@title='displayedField']"), actiontype.click, "");

    waitForElementClickAble("//*[@id='frmScrForm:j_id4returnSelectedBTN']");

    findElementByXpath(By.xpath("//span[contains(text(),'" + field_expression_1 + "')]"),
        actiontype.click, "");
    findElementByXpath(By.xpath("//*[@id='frmScrForm:j_id4returnSelectedBTN']"), actiontype.click,
        "");
    findElementByXpath(By.xpath("//*[@title='displayedField']"), actiontype.click, "");

    waitframeToBeAvailableAndSwitchToIt("ManageMultiLevelScreenLevels");

    findElementByXpath(By.xpath("//*[@title='parentOactOnEntity.entityClassPath']"),
        actiontype.click, "");

    switchToLookUp("LookupEntity", driver);
    // ERROR: Caught exception [ERROR: Unsupported command [selectFrame |
    // //iframe[contains(@src,'LookupEntity')] | ]]
    waitframeToBeAvailableAndSwitchToIt("ManageMultiLevelScreenLevels");
    waitForElementClickAble("//*[@id='tf:LookupEntity_generalTable:column0:filter']");

    findElementByXpath(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']"),
        actiontype.clearandsend, Act_on_Ent);
    findElementByXpath(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']"),
        actiontype.enter, "");

    findElementByXpath(
        By.xpath("//*[@id='tf:LookupEntity_generalTable:6:cell0']/span[1]/table/tbody/tr/td"),
        actiontype.click, "");

    waitforelementtext(
        By.xpath("//*[@id='tf:LookupEntity_generalTable:6:cell0']/span[1]/table/tbody/tr/td/a"),
        Act_on_Ent);
    findElementByXpath(By.xpath("//*[@title='Pick']"), actiontype.click, "");

    waitframeToBeAvailableAndSwitchToIt("ManageMultiLevelScreenLevels");
    findElementByXpath(By.xpath("//*[@title='fldExpInParentEntity']"), actiontype.click, "");

    switchToLookUp("FieldsTreeBrowser", driver);
    waitForElementClickAble(By.xpath("//*[@id='frmScrForm:j_id4returnSelectedBTN']"));


    findElementByXpath(By.xpath("//span[contains(text(),'" + field_expression_2 + "')]"),
        actiontype.click, "");;
    findElementByXpath(By.xpath("//*[@id='frmScrForm:j_id4returnSelectedBTN']"), actiontype.click,
        "");

    waitframeToBeAvailableAndSwitchToIt("ManageMultiLevelScreenLevels");

    findElementByXpath(
        By.xpath("//*[@id='tf:ManageMultiLevelScreenLevels_generalTable:0:c2a_CheckBox_6']/div[2]"),
        actiontype.click, "");
    findElementByXpath(
        By.xpath(
            "//*[@id='tf:ManageMultiLevelScreenLevels_generalTable:0:j_id34_rowEditor']/span[2]"),
        actiontype.click, "");
    // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | _blank | ]]
    findElementByXpath(By.xpath("//*[@id='closethick']"), actiontype.click, "");
    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

}
