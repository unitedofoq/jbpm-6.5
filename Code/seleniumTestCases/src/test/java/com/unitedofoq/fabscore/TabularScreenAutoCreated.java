package com.unitedofoq.fabscore;

import static com.unitedofoq.util.FrameUtil.get;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.unitedofoq.fabsbase.BaseClass;
import com.unitedofoq.util.FrameUtil;

public class TabularScreenAutoCreated extends BaseClass {

  private FrameUtil frameUtil = new FrameUtil(driver);

  @Before
  public void setUp() {
    login();
  }

  @Test
  public void check() {

    try {
      // open frame
      frameUtil.openFM("Tabular Screen Full Picture", "TABULAR", true);
      // search for screen
      frameUtil.cES("//input[@id='tf:TABULAR_generalTable:column0:filter']", get("entity"));
      driverManager.checkValue("Manage" + get("entity") + "s",
          "//td[contains(.,'" + "Manage" + get("entity") + "s" + "')]");
    } catch (Exception e) {
      System.out.println(e.getMessage());
      assert (false);
    }

  }

  @After
  public void closeBrowser() {
    driver.quit();
  }
}
