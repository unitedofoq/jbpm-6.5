package com.unitedofoq.fabscore;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.unitedofoq.fabsbase.BaseClass;

public class Login extends BaseClass {
  private StringBuffer verificationErrors = new StringBuffer();
  private String user_name;
  private String password;


  @Before
  public void setUp() throws Exception {
    user_name = System.getProperty("username");
    password = "pass";
  }

  @Test
  public void testLogin() throws Exception {
    driver.get(testServer);
    driver.manage().window().maximize();
    login(user_name, password);
    driver.switchTo().frame("DockMenuFrame");
    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }
}
