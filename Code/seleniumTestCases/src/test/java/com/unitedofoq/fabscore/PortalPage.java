package com.unitedofoq.fabscore;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PortalPage extends PersonnelBaseClass {
  private WebDriver driver;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();
  String portal_name = "Manage_Position_" + random;
  String portal_header = "Manage_Position_" + random;
  String tabular_screen = System.getProperty("tabular_screen_header");
  String form_screen = System.getProperty("form_screen_header");
  String tree_screen = System.getProperty("special_screen_header");

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();

    Login(driver);

  }

  @Test
  public void testPortalPage() throws Exception {
    driver.findElement(By.linkText("Application Builder")).click();
    driver.findElement(By.linkText("Portal Page")).click();
    waitForPopUp(driver);
    waitframeToBeAvailableAndSwitchToIt("PortalPage");
    waitForElementClickAble("//*[@title='New Row']");

    findElementByXpath(xby("//*[@title='New Row']"), actiontype.click, "");

    waitForElementClickAble("//*[@id='tf:PortalPage_generalTable:0:nameTranslated_TextField_0']");

    findElementByXpath(xby("//*[@id='tf:PortalPage_generalTable:0:nameTranslated_TextField_0']"),
        actiontype.clearandsend, portal_name);

    findElementByXpath(xby("//*[@id='tf:PortalPage_generalTable:0:headerTranslated_TextField_1']"),
        actiontype.clearandsend, portal_header);

    findElementByXpath(
        xby("//*[@id='tf:PortalPage_generalTable:0:omodule_moduleTitleTranslated_Drop-down_3']/div[2]/span"),
        actiontype.click, "");
    findElementByXpath(
        xby("//*[@id='tf:PortalPage_generalTable:0:omodule_moduleTitleTranslated_Drop-down_3_panel']/div/ul/li[text()='FOUNDATION']"),
        actiontype.click, "");
    findElementByXpath(xby("//*[@id='tf:PortalPage_generalTable:0:j_id32_rowEditor']/span[2]"),
        actiontype.click, "");

    waitForSuccess(driver);

    findElementByXpath(xby("//*[@id='tf:PortalPage_generalTable:column0:filter']"),
        actiontype.clearandsendenter, portal_name);

    findElementByXpath(
        xby("//*[@id='tf:PortalPage_generalTable:0:cell0']/span[1]/table/tbody/tr/td"),
        actiontype.click, "");

    waitframeToBeAvailableAndSwitchToIt("PortalPagePortlet");

    waitForElementClickAble("//*[@title='New Row']");
    findElementByXpath(xby("//*[@title='New Row']"), actiontype.click, "");
    waitForElementClickAble("//*[@id='tf:PortalPagePortlet_generalTable:0:position_TextField_0']");

    findElementByXpath(xby("//*[@id='tf:PortalPagePortlet_generalTable:0:position_TextField_0']"),
        actiontype.clearandsend, "1");

    findElementByXpath(xby("//*[@id='tf:PortalPagePortlet_generalTable:0:column_TextField_1']"),
        actiontype.clearandsend, "1");

    findElementByXpath(xby("//*[@title='screen.name']"), actiontype.click, "");

    switchToLookUp("LookupScreen", driver);
    waitForElementClickAble("//*[@id='tf:LookupScreen_generalTable:column0:filter']");

    findElementByXpath(xby("//*[@id='tf:LookupScreen_generalTable:column0:filter']"),
        actiontype.clearandsendenter, tree_screen);

    findElementByXpath(
        xby("//*[@id='tf:LookupScreen_generalTable:0:cell0']/span[1]/table/tbody/tr/td"),
        actiontype.click, "");
    findElementByXpath(xby("//*[@title='Pick']"), actiontype.click, "");

    waitframeToBeAvailableAndSwitchToIt("PortalPagePortlet");

    findElementByXpath(xby("//*[@id='tf:PortalPagePortlet_generalTable:0:main_CheckBox_3']/div[2]"),
        actiontype.click, "");
    findElementByXpath(
        xby("//*[@id='tf:PortalPagePortlet_generalTable:0:j_id32_rowEditor']/span[2]"),
        actiontype.click, "");

    waitForSuccess(driver);
    waitForElementClickAble(By.xpath("//*[@title='New Row']"));

    findElementByXpath(xby("//*[@title='New Row']"), actiontype.click, "");

    waitForElementClickAble("//*[@id='tf:PortalPagePortlet_generalTable:0:position_TextField_0']");

    findElementByXpath(xby("//*[@id='tf:PortalPagePortlet_generalTable:0:position_TextField_0']"),
        actiontype.clearandsend, "1");
    findElementByXpath(xby("//*[@id='tf:PortalPagePortlet_generalTable:0:column_TextField_1']"),
        actiontype.clearandsend, "2");

    findElementByXpath(xby("//*[@title='screen.name']"), actiontype.click, "");

    switchToLookUp("LookupScreen", driver);
    waitForElementClickAble("//*[@id='tf:LookupScreen_generalTable:column0:filter']");

    findElementByXpath(xby("//*[@id='tf:LookupScreen_generalTable:column0:filter']"),
        actiontype.clearandsendenter, tabular_screen);

    findElementByXpath(
        xby("//*[@id='tf:LookupScreen_generalTable:0:cell0']/span[1]/table/tbody/tr/td"),
        actiontype.click, "");
    findElementByXpath(xby("//*[@title='Pick']"), actiontype.click, "");

    waitframeToBeAvailableAndSwitchToIt("PortalPagePortlet");

    findElementByXpath(
        xby("//*[@id='tf:PortalPagePortlet_generalTable:0:j_id32_rowEditor']/span[2]"),
        actiontype.click, "");

    waitForSuccess(driver);

    findElementByXpath(xby("//*[@title='New Row']"), actiontype.click, "");
    waitForElementClickAble("//*[@id='tf:PortalPagePortlet_generalTable:0:position_TextField_0']");

    findElementByXpath(xby("//*[@id='tf:PortalPagePortlet_generalTable:0:position_TextField_0']"),
        actiontype.clearandsend, "2");

    findElementByXpath(xby("//*[@id='tf:PortalPagePortlet_generalTable:0:column_TextField_1']"),
        actiontype.clearandsend, "2");

    findElementByXpath(xby("//*[@title='screen.name']"), actiontype.click, "2");

    switchToLookUp("LookupScreen", driver);

    waitForElementClickAble(By.xpath("//*[@id='tf:LookupScreen_generalTable:column0:filter']"));

    findElementByXpath(xby("//*[@id='tf:LookupScreen_generalTable:column0:filter']"),
        actiontype.clearandsendenter, form_screen);

    findElementByXpath(
        xby("//*[@id='tf:LookupScreen_generalTable:0:cell0']/span[1]/table/tbody/tr/td"),
        actiontype.click, "");
    findElementByXpath(xby("//*[@title='Pick']"), actiontype.click, "");

    waitframeToBeAvailableAndSwitchToIt("PortalPagePortlet");
    findElementByXpath(
        xby("//*[@id='tf:PortalPagePortlet_generalTable:0:j_id32_rowEditor']/span[2]"),
        actiontype.click, "");

    waitForSuccess(driver);

    waitframeToBeAvailableAndSwitchToIt("portalPageFunction");
    findElementByXpath(xby("//*[@title='New Row']"), actiontype.click, "");
    findElementByXpath(
        xby("//*[@id='tf:portalPageFunction_generalTable:0:nameTranslated_TextField_0']"),
        actiontype.clearandsend, portal_header);

    findElementByXpath(xby("//*[@id='tf:portalPageFunction_generalTable:0:code_TextField_2']"),
        actiontype.clearandsend, portal_header);

    findElementByXpath(
        xby("//*[@id='tf:portalPageFunction_generalTable:0:j_id32_rowEditor']/span[2]"),
        actiontype.click, "");

    waitForSuccess(driver);

    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
