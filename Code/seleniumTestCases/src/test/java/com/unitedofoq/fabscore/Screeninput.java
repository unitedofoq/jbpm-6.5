package com.unitedofoq.fabscore;

import java.util.List;

import org.openqa.selenium.WebDriver;


public class Screeninput extends PersonnelBaseClass {
  public Screeninput(WebDriver xdriver, String input) {
    driver = xdriver;

    Screeninputsbyrow(input);
  }

  public Screeninput(WebDriver xdriver, List<String> list) {
    driver = xdriver;
    for (String item : list) {
      Screeninputsbyrow(item);
    }
  }

  public void Screeninputsbyrow(String input) {
    try {
      waitframeToBeAvailableAndSwitchToIt("ScreenInput");

      findElementByXpath(xby("//*[@title='New Row']"), actiontype.click, "");

      findElementByXpath(xby("//*[@title='odataType.name']"), actiontype.click, "");

      switchToLookUp("LookupDataType", driver);


      findElementByXpath(xby("//*[@id='tf:LookupDataType_generalTable:column0:filter']"),
          actiontype.clearandsendenter, input);


      findElementByXpath(
          xby("//*[@id='tf:LookupDataType_generalTable:1:cell0']/span[1]/table/tbody/tr/td"),
          actiontype.click, "");



      findElementByXpath(xby("//*[@title='Pick']"), actiontype.click, "");

      waitframeToBeAvailableAndSwitchToIt("ScreenInput");

      waitforelementtext(xby("//*[@id='tf:ScreenInput_generalTable:0:j_id65_LookupInputText']"),
          input);

      findElementByXpath(xby("//*[@id='tf:ScreenInput_generalTable:0:j_id32_rowEditor']/span[2]"),
          actiontype.click, "");

      waitForSuccess(driver);
    } catch (Throwable e) {
      System.out.println(e.getMessage());
    }

  }
}
