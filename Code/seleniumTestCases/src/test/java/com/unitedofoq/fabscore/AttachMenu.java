package com.unitedofoq.fabscore;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class AttachMenu extends PersonnelBaseClass {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();
  String portal_name = "Manage_Position_";
  String menu_name = "Test_Menu_" + random;
  String menu_code = "testMenu_" + random;

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    Login(driver);

  }

  @Test
  public void testAttachMenu() throws Exception {
    // ERROR: Caught exception [ERROR: Unsupported command [setSpeed | 1000 | ]]
    // ERROR: Caught exception [ERROR: Unsupported command [selectFrame | DockMenuFrame | ]]

    driver.findElement(By.linkText("Application Builder")).click();
    driver.findElement(By.linkText("Portal Page")).click();
    waitForPopUp(driver);
    waitframeToBeAvailableAndSwitchToIt("PortalPage");

    findElementByXpath(xby("//*[@id='tf:PortalPage_generalTable:column0:filter']"),
        actiontype.clearandsendenter, portal_name);
    findElementByXpath(
        xby("//*[@id='tf:PortalPage_generalTable:0:cell0']/span[1]/table/tbody/tr/td"),
        actiontype.click, "");

    waitframeToBeAvailableAndSwitchToIt("portalPageFunction");
    findElementByXpath(xby("//*[@id='tf:portalPageFunction_generalTable:0:openButton_3']"),
        actiontype.click, "");
    driver.findElement(By.linkText("Menu")).click();
    switchToLookUp("FunctionMenus", driver);
    waitForElementClickAble("//*[@title='New Row']");

    findElementByXpath(xby("//*[@title='New Row']"), actiontype.click, "");
    waitForElementClickAble("//*[@title='omenu.name']");
    findElementByXpath(xby("//*[@title='omenu.name']"), actiontype.click, "");

    switchToLookUp("MenuList", driver);

    findElementByXpath(xby("//*[@title='Pick']"), actiontype.click, "");
    switchToLookUp("FunctionMenus", driver);
    findElementByXpath(
        xby("/html/body/form/table/tbody/tr[2]/td/table/tbody/tr[2]/td/div/div/div/table/tbody/tr/td[1]/div/span[1]/span[2]"),
        actiontype.click, "");
    findElementByXpath(xby("//*[@id='tf:j_id31_button']"), actiontype.click, "");
    // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | _blank | ]]
    // ERROR: Caught exception [ERROR: Unsupported command [selectFrame |
    // //iframe[contains(@src,'FunctionMenus')] | ]]
    /*
     * waitframeToBeAvailableAndSwitchToIt("FunctionMenus");
     * findElementByXpath(xby("//*[@id='tf:FunctionMenus_generalTable:0:sortIndex_TextField_2']"),
     * actiontype.clearandsend,"1");
     * 
     * findElementByXpath(xby("//*[@id='tf:FunctionMenus_generalTable:0:j_id34_rowEditor']/span[2]")
     * ,actiontype.click,""); // Warning: waitForTextPresent may require manual changes
     * waitForSuccess(driver);
     */
    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
