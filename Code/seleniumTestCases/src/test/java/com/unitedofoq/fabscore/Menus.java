package com.unitedofoq.fabscore;

import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Menus extends PersonnelBaseClass {
  private WebDriver driver;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();
  private String menu_name;
  private String menu_code;
  private String seq;

  @Before
  public void setUp() throws Exception {
    menu_name = "Test_Menu_" + random;
    menu_code = "testMenu_" + random;
    seq = "4";
    driver = new FirefoxDriver();
    Login(driver);

  }

  @Test
  public void testMenus() throws Exception {
    // Portal Pages:

    driver.findElement(By.linkText("Application Builder")).click();
    driver.findElement(By.linkText("Menus")).click();
    driver.findElement(By.linkText("Application Builder")).click();
    driver.findElement(By.linkText("Menus")).click();

    // waitForElement(By.cssSelector("iframe[title='MenuList']"), driver);
    switchToFrame("MenuList", driver);
    waitForElement(By.xpath("//*[@title='New Row']"), driver);

    driver.findElement(By.xpath("//*[@title='New Row']")).click();
    waitForElementClickAble(
        By.xpath("//*[@id='tf:MenuList_generalTable:0:nameTranslated_TextField_0']"));
    driver.findElement(By.xpath("//*[@id='tf:MenuList_generalTable:0:nameTranslated_TextField_0']"))
        .clear();
    driver.findElement(By.xpath("//*[@id='tf:MenuList_generalTable:0:nameTranslated_TextField_0']"))
        .sendKeys(menu_name);

    driver.findElement(By.xpath("//*[@id='tf:MenuList_generalTable:0:code_TextField_1']")).clear();
    driver.findElement(By.xpath("//*[@id='tf:MenuList_generalTable:0:code_TextField_1']"))
        .sendKeys(menu_code);

    driver.findElement(By.xpath("//*[@title='parentMenu.nameTranslated']")).click();

    switchToLookUp("Menu", driver);

    waitForElementClickAble(By.xpath("//*[@id='tf:Menu_generalTable:column1:filter']"));
    driver.findElement(By.xpath("//*[@id='tf:Menu_generalTable:column1:filter']"))
        .sendKeys("APP_B");
    driver.findElement(By.xpath("//*[@id='tf:Menu_generalTable:column1:filter']"))
        .sendKeys(Keys.ENTER);
    waitForElement(By.xpath("//*[@id='tf:Menu_generalTable_data']/tr/td[4]"), driver);
    checkValue("APP_B", "//*[@id='tf:Menu_generalTable_data']/tr/td[4]", driver);
    driver.findElement(By.xpath("//*[@id='tf:Menu_generalTable_data']/tr/td[4]")).click();

    driver.findElement(By.xpath("//*[@title='Pick']")).click();

    switchToFrame("MenuList", driver);
    checkValue("Application Builder",
        "//*[@id='tf:MenuList_generalTable:0:j_id80_LookupInputText']", driver);
    driver.findElement(By.xpath("//*[@id='tf:MenuList_generalTable:0:sortIndex_TextField_4']"))
        .clear();
    driver.findElement(By.xpath("//*[@id='tf:MenuList_generalTable:0:sortIndex_TextField_4']"))
        .sendKeys(seq);

    driver
        .findElement(By.xpath(
            "//*[@id='tf:MenuList_generalTable:0:omodule_moduleTitleTranslated_Drop-down_10_label']"))
        .click();
    driver
        .findElement(By.xpath(
            "//*[@id='tf:MenuList_generalTable:0:omodule_moduleTitleTranslated_Drop-down_10_panel']/div/ul/li[text()='Testing']"))
        .click();
    driver.findElement(By.xpath("//*[@id='tf:MenuList_generalTable:0:j_id33_rowEditor']/span[2]"))
        .click();
    // Warning: waitForTextPresent may require manual changes
    waitForSuccess(driver);
    driver.switchTo().defaultContent();
    // driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div/div/div/section/header/menu/div/a/i[1]")).click();
    // driver.findElement(By.xpath("/html/body/div[1]/div/div/div/ul/li[6]/a")).click();

  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
