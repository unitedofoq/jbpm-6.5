package com.unitedofoq.fabscore;

import org.junit.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;


public class TabularScreensCreation extends PersonnelBaseClass {
  private WebDriver driver;

  private StringBuffer verificationErrors = new StringBuffer();
  String Act_on_Ent;
  String sort_expression;

  String field_expression_1;
  String field_expression_2;

  @Before
  public void setUp() throws Exception {

    driver = new FirefoxDriver();

    Login(driver);

  }

  public void initializevariables() {
    // Tabular Screens:
    System.setProperty("tabular_screen_name", "Sel_Pos_T_X_01_" + System.getProperty("random"));
    System.setProperty("tabular_screen_header",
        "Position_Tabular_Test_" + System.getProperty("random"));
    Act_on_Ent = "Position";
    sort_expression = "name";
    // Screen Fields:
    field_expression_1 = "nameTranslated";
    field_expression_2 = "headcount";
  }

  public void fromhometotabularscreen() {
    driver.findElement(By.linkText("Application Builder")).click();
    driver.findElement(By.linkText("Tabular Screen Full Picture")).click();

  }

  public void TabularScreens() throws Exception {

    waitForPopUp(driver);
    waitframeToBeAvailableAndSwitchToIt("TABULAR");

    findElementByXpath(By.xpath("//*[@title='New Row']"), actiontype.click, "");
    findElementByXpath(By.xpath("//*[@title='oactOnEntity.entityClassPath']"), actiontype.click,
        "");

    switchToLookUp("LookupEntity", driver);

    findElementByXpath(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']"),
        actiontype.clearandsendenter, Act_on_Ent);

    findElementByXpath(
        By.xpath(
            "/html/body/form/table/tbody/tr[2]/td/table/tbody/tr[2]/td/div/div/div/table/tfoot/tr/td/span[4]/span[2]"),
        actiontype.click, "");

    waitforelementtext(xby("//*[@id='tf:LookupEntity_generalTable:column0:filter']"), Act_on_Ent);

    findElementByXpath(
        By.xpath(
            "/html/body/form/table/tbody/tr[2]/td/table/tbody/tr[2]/td/div/div/div/table/tbody/tr[1]/td[3]/div/span/span[1]/table/tbody/tr/td"),
        actiontype.click, "");

    findElementByXpath(By.xpath("//*[@title='Pick']"), actiontype.click, "");


    waitframeToBeAvailableAndSwitchToIt("TABULAR");
    waitforelementtext(By.xpath("//*[@id='tf:TABULAR_generalTable:0:j_id179_LookupInputText']"),
        "com.unitedofoq.otms.foundation.position.Position");

    findElementByXpath(By.xpath("//*[@id='tf:TABULAR_generalTable:0:name_TextField_0']"),
        actiontype.clearandsend, System.getProperty("tabular_screen_name"));

    findElementByXpath(
        By.xpath("//*[@id='tf:TABULAR_generalTable:0:headerTranslated_TextField_1']"),
        actiontype.clearandsend, System.getProperty("tabular_screen_header"));

    findElementByXpath(By.xpath("//*[@id='tf:TABULAR_generalTable:0:fastTrack_CheckBox_3']/div[2]"),
        actiontype.click, "");

    findElementByXpath(By.xpath("//*[@id='tf:TABULAR_generalTable:0:editable_CheckBox_6']/div[2]"),
        actiontype.click, "");

    findElementByXpath(By.xpath("//*[@id='tf:TABULAR_generalTable:0:removable_CheckBox_7']/div[2]"),
        actiontype.click, "");

    findElementByXpath(By.xpath("//*[@id='tf:TABULAR_generalTable:0:save_CheckBox_8']/div[2]"),
        actiontype.click, "");

    findElementByXpath(
        By.xpath(
            "//*[@id='tf:TABULAR_generalTable:0:omodule_moduleTitleTranslated_Drop-down_11']/div[2]/span"),
        actiontype.click, "");

    findElementByXpath(
        By.xpath(
            "//*[@id='tf:TABULAR_generalTable:0:omodule_moduleTitleTranslated_Drop-down_11_panel']/div/ul/li[text()='Testing']"),
        actiontype.click, "");

    findElementByXpath(
        By.xpath("//*[@id='tf:TABULAR_generalTable:0:showHeader_CheckBox_10']/div[2]/span"),
        actiontype.click, "");
    findElementByXpath(
        By.xpath("//*[@id='tf:TABULAR_generalTable:0:showActiveOnly_CheckBox_11']/div[2]"),
        actiontype.click, "");

    findElementByXpath(By.xpath("//*[@id='tf:TABULAR_generalTable:0:sortExpression_TextField_13']"),
        actiontype.sendkey, sort_expression);

    findElementByXpath(By.xpath("//*[@id='tf:TABULAR_generalTable:0:j_id35_rowEditor']/span[2]"),
        actiontype.click, ""); // driver.findElement(By.xpath("//*[@id='tf:TABULAR_generalTable:0:j_id35_rowEditor']/span[2]")).click();
    waitForSuccess(driver);

    findElementByXpath(By.xpath("//*[@id='tf:TABULAR_generalTable:column0:filter']"),
        actiontype.clearandsend, System.getProperty("tabular_screen_name"));

    pressenter("//*[@id='tf:TABULAR_generalTable:column0:filter']");

  }

  public void screenfilds() throws Exception {
    List<String> x = new ArrayList<String>(Arrays.asList(field_expression_1, field_expression_2));
    new Screenfileds(driver, x);
  }

  public void screeninput() {
    new Screeninput(driver, "VoidDT");
  }

  @Test
  public void testTabularScreensCreation() throws Exception {

    initializevariables();

    fromhometotabularscreen();

    TabularScreens();

    screenfilds();

    screeninput();

    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    // Thread.sleep(20000);
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

}
