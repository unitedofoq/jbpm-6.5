package com.unitedofoq.fabscore;


import java.util.ArrayList;
import java.util.Arrays;

import org.junit.*;
import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;



public class FormScreensCreation extends PersonnelBaseClass {
  private WebDriver driver;


  private StringBuffer verificationErrors = new StringBuffer();

  String form_screen_name = "Sel_Pos_F_X_01_" + System.getProperty("random");
  String form_screen_header = "Position_Form_Test_" + System.getProperty("random");
  String Act_on_Ent = "Position";
  String No_of_Col = "2";
  // Screen Fields:
  String field_expression_1 = "nameTranslated";
  String field_expression_2 = "headcount";

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    Login(driver);
  }
  /*
   * // @Test // public void testFormScreensCreation() throws Exception { // // Tabular Screens: //
   * String form_screen_name = "Sel_Pos_F_X_01_${random}"; // String form_screen_header =
   * "Position_Form_Test_${random}"; // String Act_on_Ent = "Position"; // String No_of_Col = "2";
   * // // Screen Fields: // String field_expression_1 = "nameTranslated"; // String
   * field_expression_2 = "headcount"; // // Test Case: // // ERROR: Caught exception [ERROR:
   * Unsupported command [setSpeed | 1000 | ]] // // ERROR: Caught exception [ERROR: Unsupported
   * command [selectFrame | DockMenuFrame | ]] // driver.findElement(By.linkText(
   * "Application Builder")).click(); // driver.findElement(By.linkText("Form Screen Full Picture"
   * )).click(); // // ERROR: Caught exception [ERROR: Unsupported command [waitForPopUp | _blank |
   * ]] // // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | _blank | ]] // //
   * ERROR: Caught exception [ERROR: Unsupported command [waitForFrameToLoad |
   * //iframe[contains(@src,'FORM')] | ]] // // ERROR: Caught exception [ERROR: Unsupported command
   * [selectFrame | //iframe[contains(@src,'FORM')] | ]] // for (int second = 0;; second++) { // if
   * (second >= 60) fail("timeout"); // try { if (isElementPresent(By.xpath("//*[@title='New Row']"
   * ))) break; } catch (Exception e) {} // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath("//*[@title='New Row']")).click(); // for (int second = 0;;
   * second++) { // if (second >= 60) fail("timeout"); // try { if
   * (isElementPresent(By.xpath("//*[@id='tf:FORM_generalTable:0:name_TextField_0']"))) break; }
   * catch (Exception e) {} // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath("//*[@id='tf:FORM_generalTable:0:name_TextField_0']")).clear(); //
   * driver.findElement(By.xpath("//*[@id='tf:FORM_generalTable:0:name_TextField_0']")).sendKeys(
   * form_screen_name); // // ERROR: Caught exception [ERROR: Unsupported command [fireEvent |
   * //*[@id='tf:FORM_generalTable:0:name_TextField_0'] | blur]] //
   * driver.findElement(By.xpath("//*[@id='tf:FORM_generalTable:0:headerTranslated_TextField_1']")).
   * clear(); //
   * driver.findElement(By.xpath("//*[@id='tf:FORM_generalTable:0:headerTranslated_TextField_1']")).
   * sendKeys(form_screen_header); // // ERROR: Caught exception [ERROR: Unsupported command
   * [fireEvent | //*[@id='tf:FORM_generalTable:0:headerTranslated_TextField_1'] | blur]] //
   * driver.findElement(By.xpath("//*[@title='oactOnEntity.entityClassPath']")).click(); // //
   * ERROR: Caught exception [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR:
   * Caught exception [ERROR: Unsupported command [selectFrame |
   * //iframe[contains(@src,'LookupEntity')] | ]] // for (int second = 0;; second++) { // if (second
   * >= 60) fail("timeout"); // try { if
   * (isElementPresent(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']"))) break; }
   * catch (Exception e) {} // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']")).clear();
   * //
   * driver.findElement(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']")).sendKeys
   * (Act_on_Ent); // // ERROR: Caught exception [ERROR: Unsupported command [keyUp |
   * //*[@id='tf:LookupEntity_generalTable:column0:filter'] | \13]] // driver.findElement(By.xpath(
   * "//*[@id='tf:LookupEntity_generalTable:3:cell0']/span[1]/table/tbody/tr/td")).click(); // try {
   * // assertEquals(Act_on_Ent, driver.findElement(By.xpath(
   * "//*[@id='tf:LookupEntity_generalTable:3:cell0']/span[1]/table/tbody/tr/td/a")).getText()); //
   * } catch (Error e) { // verificationErrors.append(e.toString()); // } //
   * driver.findElement(By.xpath("//*[@title='Pick']")).click(); // // ERROR: Caught exception
   * [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR: Caught exception [ERROR:
   * Unsupported command [selectFrame | //iframe[contains(@src,'FORM')] | ]] // try { //
   * assertEquals("com.unitedofoq.otms.foundation.position.Position",
   * driver.findElement(By.xpath("//*[@id='tf:FORM_generalTable:0:j_id76_LookupInputText']")).
   * getAttribute("value")); // } catch (Error e) { // verificationErrors.append(e.toString()); // }
   * //
   * driver.findElement(By.xpath("//*[@id='tf:FORM_generalTable:0:columnsNo_TextField_5']")).clear()
   * ; // driver.findElement(By.xpath("//*[@id='tf:FORM_generalTable:0:columnsNo_TextField_5']")).
   * sendKeys(No_of_Col); // // ERROR: Caught exception [ERROR: Unsupported command [fireEvent |
   * //*[@id='tf:FORM_generalTable:0:columnsNo_TextField_5'] | blur]] //
   * driver.findElement(By.xpath(
   * "//*[@id='tf:FORM_generalTable:0:omodule_moduleTitleTranslated_Drop-down_8']/div[2]/span")).
   * click(); // driver.findElement(By.xpath(
   * "//*[@id='tf:FORM_generalTable:0:omodule_moduleTitleTranslated_Drop-down_8_panel']/div/ul/li[text()='Testing']"
   * )).click(); //
   * driver.findElement(By.xpath("//*[@id='tf:FORM_generalTable:0:save_CheckBox_7']/div[2]")).click(
   * ); //
   * driver.findElement(By.xpath("//*[@id='tf:FORM_generalTable:0:saveAndExit_CheckBox_8']/div[2]"))
   * .click(); //
   * driver.findElement(By.xpath("//*[@id='tf:FORM_generalTable:0:j_id32_rowEditor']/span[2]")).
   * click(); // // Warning: waitForTextPresent may require manual changes // for (int second = 0;;
   * second++) { // if (second >= 60) fail("timeout"); // try { if
   * (driver.findElement(By.cssSelector("BODY")).getText().matches(
   * "^[\\s\\S]*Success\\(es\\)[\\s\\S]*$")) break; } catch (Exception e) {} // Thread.sleep(1000);
   * // } // //
   * driver.findElement(By.xpath("//*[@id='tf:FORM_generalTable:column0:filter']")).clear(); //
   * driver.findElement(By.xpath("//*[@id='tf:FORM_generalTable:column0:filter']")).sendKeys(
   * form_screen_name); // // ERROR: Caught exception [ERROR: Unsupported command [keyUp |
   * //*[@id='tf:FORM_generalTable:column0:filter'] | \13]] //
   * driver.findElement(By.xpath("//*[@id='tf:FORM_generalTable:0:cell0']/span[1]/table/tbody/tr/td"
   * )).click(); // // ERROR: Caught exception [ERROR: Unsupported command [selectFrame |
   * relative=up | ]] // // ERROR: Caught exception [ERROR: Unsupported command [selectFrame |
   * //iframe[contains(@src,'ScreenFieldsEntry')] | ]] // driver.findElement(By.xpath(
   * "//*[@title='New Row']")).click(); // for (int second = 0;; second++) { // if (second >= 60)
   * fail("timeout"); // try { if (isElementPresent(By.xpath("//*[@title='fieldExpression']")))
   * break; } catch (Exception e) {} // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath("//*[@title='fieldExpression']")).click(); // // ERROR: Caught
   * exception [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR: Caught exception
   * [ERROR: Unsupported command [selectFrame | //iframe[contains(@src,'FieldsTreeBrowser')] | ]] //
   * for (int second = 0;; second++) { // if (second >= 60) fail("timeout"); // try { if
   * (isElementPresent(By.xpath("//*[@id='frmScrForm:j_id4returnSelectedBTN']"))) break; } catch
   * (Exception e) {} // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath("//span[contains(text(),'" + field_expression_1 + "')]")).click();
   * // driver.findElement(By.xpath("//*[@id='frmScrForm:j_id4returnSelectedBTN']")).click(); // //
   * ERROR: Caught exception [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR:
   * Caught exception [ERROR: Unsupported command [selectFrame |
   * //iframe[contains(@src,'ScreenFieldsEntry')] | ]] // for (int second = 0;; second++) { // if
   * (second >= 60) fail("timeout"); // try { if
   * (field_expression_1.equals(driver.findElement(By.xpath(
   * "//*[@id='tf:ScreenFieldsEntry_generalTable:0:j_id53_LookupInputText']")).getAttribute("value")
   * )) break; } catch (Exception e) {} // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath(
   * "//*[@id='tf:ScreenFieldsEntry_generalTable:0:sortIndex_TextField_3']")).clear(); //
   * driver.findElement(By.xpath(
   * "//*[@id='tf:ScreenFieldsEntry_generalTable:0:sortIndex_TextField_3']")).sendKeys("1"); // //
   * ERROR: Caught exception [ERROR: Unsupported command [fireEvent |
   * //*[@id='tf:ScreenFieldsEntry_generalTable:0:sortIndex_TextField_3'] | blur]] //
   * driver.findElement(By.xpath(
   * "//*[@id='tf:ScreenFieldsEntry_generalTable:0:j_id32_rowEditor']/span[2]")).click(); // //
   * Warning: waitForTextPresent may require manual changes // for (int second = 0;; second++) { //
   * if (second >= 60) fail("timeout"); // try { if
   * (driver.findElement(By.cssSelector("BODY")).getText().matches(
   * "^[\\s\\S]*Success\\(es\\)[\\s\\S]*$")) break; } catch (Exception e) {} // Thread.sleep(1000);
   * // } // // driver.findElement(By.xpath("//*[@title='New Row']")).click(); // for (int second =
   * 0;; second++) { // if (second >= 60) fail("timeout"); // try { if
   * (isElementPresent(By.xpath("//*[@title='fieldExpression']"))) break; } catch (Exception e) {}
   * // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath("//*[@title='fieldExpression']")).click(); // // ERROR: Caught
   * exception [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR: Caught exception
   * [ERROR: Unsupported command [selectFrame | //iframe[contains(@src,'FieldsTreeBrowser')] | ]] //
   * for (int second = 0;; second++) { // if (second >= 60) fail("timeout"); // try { if
   * (isElementPresent(By.xpath("//*[@id='frmScrForm:j_id4returnSelectedBTN']"))) break; } catch
   * (Exception e) {} // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath("//span[contains(text(),'" + field_expression_2 + "')]")).click();
   * // driver.findElement(By.xpath("//*[@id='frmScrForm:j_id4returnSelectedBTN']")).click(); // //
   * ERROR: Caught exception [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR:
   * Caught exception [ERROR: Unsupported command [selectFrame |
   * //iframe[contains(@src,'ScreenFieldsEntry')] | ]] // for (int second = 0;; second++) { // if
   * (second >= 60) fail("timeout"); // try { if
   * (field_expression_2.equals(driver.findElement(By.xpath(
   * "//*[@id='tf:ScreenFieldsEntry_generalTable:0:j_id53_LookupInputText']")).getAttribute("value")
   * )) break; } catch (Exception e) {} // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath(
   * "//*[@id='tf:ScreenFieldsEntry_generalTable:0:sortIndex_TextField_3']")).clear(); //
   * driver.findElement(By.xpath(
   * "//*[@id='tf:ScreenFieldsEntry_generalTable:0:sortIndex_TextField_3']")).sendKeys("2"); // //
   * ERROR: Caught exception [ERROR: Unsupported command [fireEvent |
   * //*[@id='tf:ScreenFieldsEntry_generalTable:0:sortIndex_TextField_3'] | blur]] //
   * driver.findElement(By.xpath(
   * "//*[@id='tf:ScreenFieldsEntry_generalTable:0:j_id32_rowEditor']/span[2]")).click(); // //
   * Warning: waitForTextPresent may require manual changes // for (int second = 0;; second++) { //
   * if (second >= 60) fail("timeout"); // try { if
   * (driver.findElement(By.cssSelector("BODY")).getText().matches(
   * "^[\\s\\S]*Success\\(es\\)[\\s\\S]*$")) break; } catch (Exception e) {} // Thread.sleep(1000);
   * // } // // // ERROR: Caught exception [ERROR: Unsupported command [selectFrame | relative=up |
   * ]] // // ERROR: Caught exception [ERROR: Unsupported command [selectFrame |
   * //iframe[contains(@src,'ScreenInput')] | ]] // driver.findElement(By.xpath(
   * "//*[@title='New Row']")).click(); // for (int second = 0;; second++) { // if (second >= 60)
   * fail("timeout"); // try { if (isElementPresent(By.xpath("//*[@title='odataType.name']")))
   * break; } catch (Exception e) {} // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath("//*[@title='odataType.name']")).click(); // // ERROR: Caught
   * exception [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR: Caught exception
   * [ERROR: Unsupported command [selectFrame | //iframe[contains(@src,'LookupDataType')] | ]] //
   * for (int second = 0;; second++) { // if (second >= 60) fail("timeout"); // try { if
   * (isElementPresent(By.xpath("//*[@id='tf:LookupDataType_generalTable:column0:filter']"))) break;
   * } catch (Exception e) {} // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath("//*[@id='tf:LookupDataType_generalTable:column0:filter']")).clear(
   * ); // driver.findElement(By.xpath("//*[@id='tf:LookupDataType_generalTable:column0:filter']")).
   * sendKeys("VoidDT"); // // ERROR: Caught exception [ERROR: Unsupported command [keyUp |
   * //*[@id='tf:LookupDataType_generalTable:column0:filter'] | \13]] //
   * driver.findElement(By.xpath(
   * "//*[@id='tf:LookupDataType_generalTable:1:cell0']/span[1]/table/tbody/tr/td")).click(); // try
   * { // assertEquals("VoidDT", driver.findElement(By.xpath(
   * "//*[@id='tf:LookupDataType_generalTable:1:cell0']/span[1]/table/tbody/tr/td/a")).getText());
   * // } catch (Error e) { // verificationErrors.append(e.toString()); // } //
   * driver.findElement(By.xpath("//*[@id='tf:j_id29returnSelectedBTN']")).click(); // // ERROR:
   * Caught exception [ERROR: Unsupported command [selectWindow | _blank | ]] // // ERROR: Caught
   * exception [ERROR: Unsupported command [selectFrame | //iframe[contains(@src,'ScreenInput')] |
   * ]] // for (int second = 0;; second++) { // if (second >= 60) fail("timeout"); // try { if
   * ("VoidDT".equals(driver.findElement(By.xpath(
   * "//*[@id='tf:ScreenInput_generalTable:0:j_id56_LookupInputText']")).getAttribute("value")))
   * break; } catch (Exception e) {} // Thread.sleep(1000); // } // //
   * driver.findElement(By.xpath("//*[@id='tf:ScreenInput_generalTable:0:j_id32_rowEditor']/span[2]"
   * )).click(); // // Warning: waitForTextPresent may require manual changes // for (int second =
   * 0;; second++) { // if (second >= 60) fail("timeout"); // try { if
   * (driver.findElement(By.cssSelector("BODY")).getText().matches(
   * "^[\\s\\S]*Success\\(es\\)[\\s\\S]*$")) break; } catch (Exception e) {} // Thread.sleep(1000);
   * // } // // driver.close(); // } //
   */


  @Test
  public void testFormScreensCreation() throws Exception {

    formframe();


    new Screenfileds(driver,
        new ArrayList<String>(Arrays.asList(field_expression_1, field_expression_2)));

    new Screeninput(driver, "VoidDT");
  }


  /*
   * private void filedsentry() throws Exception, InterruptedException {
   * switchToFrame("ScreenFieldsEntry", driver); // ERROR: Caught exception [ERROR: Unsupported
   * command [selectFrame | relative=up | ]] // ERROR: Caught exception [ERROR: Unsupported command
   * [selectFrame | //iframe[contains(@src,'ScreenFieldsEntry')] | ]] findElementByXpath(By.xpath(
   * "//*[@title='New Row']"), actiontype.click, "");
   * 
   * for (int second = 0;; second++) { if (second >= 60) fail("timeout"); try { if
   * (isElementPresent(By.xpath("//*[@title='fieldExpression']"))) break; } catch (Exception e) {}
   * Thread.sleep(1000); }
   * 
   * findElementByXpath(By.xpath("//*[@title='fieldExpression']"), actiontype.click,"");
   * 
   * // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | _blank | ]] // ERROR:
   * Caught exception [ERROR: Unsupported command [selectFrame |
   * //iframe[contains(@src,'FieldsTreeBrowser')] | ]] switchToLookUp("FieldsTreeBrowser", driver);
   * for (int second = 0;; second++) { if (second >= 60) fail("timeout"); try { if
   * (isElementPresent(By.xpath("//*[@id='frmScrForm:j_id4returnSelectedBTN']"))) break; } catch
   * (Exception e) {} Thread.sleep(1000); } findElementByXpath(By.xpath("//span[contains(text(),'" +
   * field_expression_1 + "')]"), actiontype.click, "");
   * 
   * findElementByXpath(By.xpath("//*[@title='Pick']"), actiontype.click, "");
   * 
   * 
   * // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | _blank | ]]
   * switchToFrame("ScreenFieldsEntry", driver); // ERROR: Caught exception [ERROR: Unsupported
   * command [selectFrame | //iframe[contains(@src,'ScreenFieldsEntry')] | ]]
   * waitforelementtext(By.xpath(
   * "/html/body/form/table/tbody/tr[2]/td/table/tbody/tr[2]/td/div/div/div/table/tbody/tr[1]/td[3]/div/span/span[2]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[1]/input"
   * ), field_expression_1); for (int second = 0;; second++) { if (second >= 60) fail("timeout");
   * try { if (field_expression_1.equals(driver.findElement(By.xpath(
   * "/html/body/form/table/tbody/tr[2]/td/table/tbody/tr[2]/td/div/div/div/table/tbody/tr[1]/td[3]/div/span/span[2]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[1]/input"
   * )).getAttribute("value"))) break; } catch (Exception e) {} Thread.sleep(1000); }
   * findElementByXpath(By.xpath(
   * "//*[@id='tf:ScreenFieldsEntry_generalTable:0:sortIndex_TextField_3']"),
   * actiontype.clearandsend, "1");
   * 
   * // ERROR: Caught exception [ERROR: Unsupported command [fireEvent |
   * //*[@id='tf:ScreenFieldsEntry_generalTable:0:sortIndex_TextField_3'] | blur]]
   * findElementByXpath(By.xpath(
   * "//*[@id='tf:ScreenFieldsEntry_generalTable:0:j_id32_rowEditor']/span[2]"),actiontype.click,"")
   * ; // Warning: waitForTextPresent may require manual changes
   * waitforelementtext(By.cssSelector("BODY"), "^[\\s\\S]*Success\\(es\\)[\\s\\S]*$"); for (int
   * second = 0;; second++) { if (second >= 60) fail("timeout"); try { if
   * (driver.findElement(By.cssSelector("BODY")).getText().matches(
   * "^[\\s\\S]*Success\\(es\\)[\\s\\S]*$")) break; } catch (Exception e) {} Thread.sleep(1000); }
   * 
   * findElementByXpath(By.xpath("//*[@title='New Row']"),actiontype.click,""); for (int second =
   * 0;; second++) { if (second >= 60) fail("timeout"); try { if
   * (isElementPresent(By.xpath("//*[@title='fieldExpression']"))) break; } catch (Exception e) {}
   * Thread.sleep(1000); }
   * 
   * findElementByXpath(By.xpath("//*[@title='fieldExpression']"),actiontype.click,"");
   * 
   * // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | _blank | ]] // ERROR:
   * Caught exception [ERROR: Unsupported command [selectFrame |
   * //iframe[contains(@src,'FieldsTreeBrowser')] | ]] switchToLookUp("FieldsTreeBrowser", driver);
   * for (int second = 0;; second++) { if (second >= 60) fail("timeout"); try { if
   * (isElementPresent(By.xpath("//*[@id='frmScrForm:j_id4returnSelectedBTN']"))) break; } catch
   * (Exception e) {} Thread.sleep(1000); } findElementByXpath(By.xpath("//span[contains(text(),'" +
   * field_expression_2 + "')]"),actiontype.click,"");
   * findElementByXpath(By.xpath("//*[@id='frmScrForm:j_id4returnSelectedBTN']"),actiontype.click,""
   * );
   * 
   * switchToFrame("ScreenFieldsEntry", driver); // ERROR: Caught exception [ERROR: Unsupported
   * command [selectWindow | _blank | ]] // ERROR: Caught exception [ERROR: Unsupported command
   * [selectFrame | //iframe[contains(@src,'ScreenFieldsEntry')] | ]] waitforelementtext(By.xpath(
   * "/html/body/form/table/tbody/tr[2]/td/table/tbody/tr[2]/td/div/div/div/table/tbody/tr[1]/td[3]/div/span/span[2]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[1]/input"
   * ), field_expression_2); for (int second = 0;; second++) { if (second >= 60) fail("timeout");
   * try { if (field_expression_2.equals(driver.findElement(By.xpath(
   * "/html/body/form/table/tbody/tr[2]/td/table/tbody/tr[2]/td/div/div/div/table/tbody/tr[1]/td[3]/div/span/span[2]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[1]/input"
   * )).getAttribute("value"))) break; } catch (Exception e) {} Thread.sleep(1000); }
   * 
   * 
   * findElementByXpath(By.xpath(
   * "//*[@id='tf:ScreenFieldsEntry_generalTable:0:sortIndex_TextField_3']"),
   * actiontype.clearandsend, "2"); // ERROR: Caught exception [ERROR: Unsupported command
   * [fireEvent | //*[@id='tf:ScreenFieldsEntry_generalTable:0:sortIndex_TextField_3'] | blur]]
   * findElementByXpath(By.xpath(
   * "//*[@id='tf:ScreenFieldsEntry_generalTable:0:j_id32_rowEditor']/span[2]"),actiontype.click,"")
   * ;
   * 
   * waitforelementtext(By.cssSelector("BODY"), "^[\\s\\S]*Success\\(es\\)[\\s\\S]*$"); // Warning:
   * waitForTextPresent may require manual changes for (int second = 0;; second++) { if (second >=
   * 60) fail("timeout"); try { if (driver.findElement(By.cssSelector("BODY")).getText().matches(
   * "^[\\s\\S]*Success\\(es\\)[\\s\\S]*$")) break; } catch (Exception e) {} Thread.sleep(1000); } }
   */

  private void formframe() throws Exception {

    driver.findElement(By.linkText("Application Builder")).click();
    driver.findElement(By.linkText("Form Screen Full Picture")).click();
    waitForPopUp(driver);
    waitframeToBeAvailableAndSwitchToIt("FORM");

    findElementByXpath(By.xpath("//*[@title='New Row']"), actiontype.click, "");
    findElementByXpath(By.xpath("//*[@title='oactOnEntity.entityClassPath']"), actiontype.click,
        "");
    switchToLookUp("LookupEntity", driver);


    findElementByXpath(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']"),
        actiontype.clearandsendenter, Act_on_Ent);
    waitforelementtext(By.xpath("//*[@id='tf:LookupEntity_generalTable:column0:filter']"),
        Act_on_Ent);
    findElementByXpath(
        By.xpath("id('tf:LookupEntity_generalTable_paginator_bottom')/span[4]/span[2]"),
        actiontype.click, "");
    findElementByXpath(
        By.xpath(
            "/html/body/form/table/tbody/tr[2]/td/table/tbody/tr[2]/td/div/div/div/table/tbody/tr[1]/td[3]/div/span/span[1]/table/tbody/tr/td"),
        actiontype.click, "");



    findElementByXpath(By.xpath("//*[@title='Pick']"), actiontype.click, "");

    waitframeToBeAvailableAndSwitchToIt("FORM");

    waitforelementtext(By.xpath("//*[@id='tf:FORM_generalTable:0:j_id185_LookupInputText']"),
        "com.unitedofoq.otms.foundation.position.Position");


    findElementByXpath(By.xpath("//*[@id='tf:FORM_generalTable:0:name_TextField_0']"),
        actiontype.clearandsend, form_screen_name);


    findElementByXpath(By.xpath("//*[@id='tf:FORM_generalTable:0:headerTranslated_TextField_1']"),
        actiontype.clearandsend, form_screen_header);
    findElementByXpath(By.xpath("//*[@id='tf:FORM_generalTable:0:columnsNo_TextField_5']"),
        actiontype.clearandsend, No_of_Col);

    findElementByXpath(
        By.xpath(
            "//*[@id='tf:FORM_generalTable:0:omodule_moduleTitleTranslated_Drop-down_8']/div[2]/span"),
        actiontype.click, "");
    findElementByXpath(
        By.xpath(
            "//*[@id='tf:FORM_generalTable:0:omodule_moduleTitleTranslated_Drop-down_8_panel']/div/ul/li[text()='Testing']"),
        actiontype.click, "");
    findElementByXpath(By.xpath("//*[@id='tf:FORM_generalTable:0:save_CheckBox_7']/div[2]"),
        actiontype.click, "");
    findElementByXpath(By.xpath("//*[@id='tf:FORM_generalTable:0:saveAndExit_CheckBox_8']/div[2]"),
        actiontype.click, "");
    findElementByXpath(By.xpath("//*[@id='tf:FORM_generalTable:0:j_id32_rowEditor']/span[2]"),
        actiontype.click, "");

    waitForSuccess(driver);

    findElementByXpath(By.xpath("//*[@id='tf:FORM_generalTable:column0:filter']"),
        actiontype.clearandsendenter, form_screen_name);
    findElementByXpath(
        By.xpath("//*[@id='tf:FORM_generalTable:0:cell0']/span[1]/table/tbody/tr/td"),
        actiontype.click, "");
  }


  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

}
