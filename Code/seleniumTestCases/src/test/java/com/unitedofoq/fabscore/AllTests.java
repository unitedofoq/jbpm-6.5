package com.unitedofoq.fabscore;

import junit.framework.Test;
import junit.framework.TestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    // FABSVariables.class,
    TabularScreensCreation.class})
public class AllTests {



}
