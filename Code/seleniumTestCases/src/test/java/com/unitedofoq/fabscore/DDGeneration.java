package com.unitedofoq.fabscore;

import static com.unitedofoq.util.FrameUtil.get;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.unitedofoq.fabsbase.BaseClass;
import com.unitedofoq.util.FrameUtil;

public class DDGeneration extends BaseClass {
  private FrameUtil frameUtil = new FrameUtil(driver);

  @Before
  public void setUp() {
    login();
  }

  @Test
  public void check() {
    try {
      // open frame
      frameUtil.openFM("Data Dictionaries", "DDScreen", true);
      // search for entity
      frameUtil.cES("//input[@id='tf:DDScreen_generalTable:column0:filter']", get("entity"));
      driverManager.checkValue(get("entity"), "//td[contains(.,'" + get("entity") + "_Id')]");
    } catch (Exception e) {
      System.out.println(e.getMessage());
      assert (false);
    }
  }

  @After
  public void closeBrowser() {
    driver.quit();
  }
}
