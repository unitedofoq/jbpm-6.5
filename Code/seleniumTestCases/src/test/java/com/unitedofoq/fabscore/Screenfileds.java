package com.unitedofoq.fabscore;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;



public class Screenfileds extends PersonnelBaseClass {
  int seqance = 1;
  String field_expression = "";

  public Screenfileds(WebDriver xdriver, String xfield_expression) {
    driver = xdriver;

    Screenfiledsbyrow(xfield_expression);
  }

  public Screenfileds(WebDriver xdriver, List<String> list) {
    driver = xdriver;
    for (String item : list) {
      Screenfiledsbyrow(item);
    }
  }

  public void Screenfiledsbyrow(String field_expression) {
    try {
      waitframeToBeAvailableAndSwitchToIt("ScreenFieldsEntry");


      findElementByXpath(By.xpath("//*[@title='New Row']"), actiontype.click, "");
      findElementByXpath(By.xpath("//*[@title='New Row']"), actiontype.click, "");

      findElementByXpath(By.xpath("//*[@title='fieldExpression']"), actiontype.click, "");

      switchToLookUp("FieldsTreeBrowser", driver);


      findElementByXpath(xby("//span[contains(text(),'" + field_expression + "')]"),
          actiontype.click, "");
      findElementByXpath(xby("//*[@id='frmScrForm:j_id4returnSelectedBTN']"), actiontype.click, "");

      waitframeToBeAvailableAndSwitchToIt("ScreenFieldsEntry");

      waitforelementtext(
          xby("//*[@id='tf:ScreenFieldsEntry_generalTable:0:j_id61_LookupInputText']"),
          field_expression);


      findElementByXpath(
          xby("//*[@id='tf:ScreenFieldsEntry_generalTable:0:sortIndex_TextField_3']"),
          actiontype.clearandsend, String.valueOf(seqance++));

      findElementByXpath(
          xby("//*[@id='tf:ScreenFieldsEntry_generalTable:0:j_id32_rowEditor']/span[2]"),
          actiontype.click, "");

      waitForSuccess(driver);

    } catch (Throwable e) {
      System.out.println(e.getMessage());
    }
  }
}
