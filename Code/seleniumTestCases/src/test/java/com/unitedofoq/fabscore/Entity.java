package com.unitedofoq.fabscore;

import static com.unitedofoq.util.FrameUtil.get;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;

import com.unitedofoq.fabsbase.BaseClass;
import com.unitedofoq.util.FrameUtil;

public class Entity extends BaseClass {
  private FrameUtil frameUtil = new FrameUtil(driver);



  @Before
  public void setUp() {
    login();
  }

  @Test
  public void startTest() {
    createEntity();

  }

  private void createEntity() {
    try {
      // open entity full picture
      driver.findElement(By.xpath("//span[contains(.,'Application Builder')]")).click();
      driver.findElement(By.xpath("//span[contains(.,'Entity Full Picture')]")).click();
      driverManager.waitForPageToLoad();
      driver.switchTo().defaultContent();
      // open data type portlet
      driver.switchTo().frame("DockMenuFrame");
      driverManager.findElementByXpath("//span[contains(.,'Application Builder')]").click();
      driver.findElement(By.xpath("//span[contains(.,'Data Types')]")).click();
      ((JavascriptExecutor) driver).executeScript("scroll(0,0);");
      driver.switchTo().defaultContent();
      driver.switchTo().frame("DockMenuFrame");
      driverManager.findElementByXpath("//span[contains(.,'Application Builder')]").click();
      driver.findElement(By.xpath("//span[contains(.,'Data Types')]")).click();
      driver.manage().window().maximize();
      // add entity
      driverManager.switchToFrame("OEntityList");
      driver.findElement(By.xpath("//button[@id='tf:j_id29_button']")).click();
      driverManager.waitForElement(
          By.xpath("//input[@id='tf:OEntityList_generalTable:0:titleTranslated_TextField_0']"),
          false);
      driver
          .findElement(
              By.xpath("//input[@id='tf:OEntityList_generalTable:0:titleTranslated_TextField_0']"))
          .sendKeys(System.getProperty("entity"));
      driver
          .findElement(
              By.xpath("//input[@id='tf:OEntityList_generalTable:0:entityClassPath_TextField_1']"))
          .sendKeys(System.getProperty("clspath"));
      driver.findElement(By.xpath("//span[@class='ui-icon ui-icon-triangle-1-s']")).click();
      driver.findElement(By.xpath("//li[contains(.,'FOUNDATION')]")).click();
      driver.findElement(By.xpath("//span[@class='ui-icon ui-icon-check']")).click();
      driverManager.waitForSuccess();
      // search for entity
      driver.findElement(By.xpath("//input[@id='tf:OEntityList_generalTable:column0:filter']"))
          .sendKeys(System.getProperty("entity"));
      driver.findElement(By.xpath("//input[@id='tf:OEntityList_generalTable:column0:filter']"))
          .sendKeys(Keys.ENTER);
      driverManager.checkValue(System.getProperty("entity"),
          "//a[contains(.,'" + System.getProperty("entity") + "')]");
      // check for access privilege
      accessPrivilege();
      // check for entity actions
      entityActions();
      // check for datatype
      checkValidation();

    } catch (Exception e) {
      System.out.println(e.getMessage());
      assert (false);
    }
  }

  private void entityActions() {
    try {
      driverManager.switchToFrame("EntityActions");
      driverManager.waitForElement(By.xpath("//*[@id='tf:j_id25_loadAjax_complete']"),
          reverseCondition);
      driverManager.checkValue("Delete " + get("entity"),
          "//td[contains(.,'Delete " + get("entity") + "')]");
      driverManager.checkValue("Update " + get("entity"),
          "//td[contains(.,'Update " + get("entity") + "')]");
      driverManager.checkValue("Create " + get("entity"),
          "//td[contains(.,'Create " + get("entity") + "')]");
    } catch (Exception e) {
      System.out.println(e.getMessage());
      assert (false);
    }
  }

  private void accessPrivilege() {
    try {
      driver
          .findElement(
              By.xpath("//button[contains(@id,'tf:OEntityList_generalTable:0:openButton_0_0')]"))
          .click();
      driverManager.waitForElement(
          By.xpath("/html/body/div[21]/table/tbody/tr/td[1]/div/table[21]/tbody/tr[1]/td[2]/a"),
          reverseCondition);
      driver
          .findElement(
              By.xpath("/html/body/div[21]/table/tbody/tr/td[1]/div/table[21]/tbody/tr[1]/td[2]/a"))
          .click();

      driverManager.switchToLookUp("ObjectAccessPrivilege");
      driverManager.waitForElement(By.xpath("//input[contains(@role,'textbox')]"),
          reverseCondition);
      driverManager.checkValue(get("entity"), "//td[contains(.,'" + get("entity") + "')]");
      driver.findElement(By.xpath("//button[@id='tf:j_id30_button']")).click();
    } catch (Exception e) {
      System.out.println(e.getMessage());
      assert (false);
    }
  }

  private void checkValidation() {
    try {

      driverManager.switchToFrame("ODataTypeList");
      frameUtil.checkFilter(get("entity"),
          "//input[@id='tf:ODataTypeList_generalTable:column0:filter']",
          "//td[contains(.,'" + get("entity") + "')]");
    } catch (Exception e) {
      System.out.println(e.getMessage());
      assert (false);
    }
  }

  @After
  public void closeBrowser() {
    driver.quit();
  }
}
