package com.unitedofoq.fabsbase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.unitedofoq.util.DriverManager;


public class BaseClass {
  public double random;
  public String testServer;
  public long wait;
  public boolean reverseCondition;
  public DriverManager driverManager;
  public WebDriver driver;

  public BaseClass() {
    // TODO Auto-generated constructor stub
    random = Math.floor((Math.random() * 1000) + 1);
    testServer = "https://1.1.1.11:8181";
    wait = 60;
    driverManager = new DriverManager(new FirefoxDriver());
    driver = driverManager.getWebDriver();
  }


  /**
   * @param driver
   */
  public void login() {
    driver.get(testServer);
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    login("test", "test");
    driver.switchTo().frame("DockMenuFrame");
  }

  public void login(String username, String password) {
    driver.findElement(By.xpath("//input[@name='_58_login']")).sendKeys(username);
    driver.findElement(By.xpath("//input[@name='_58_password']")).sendKeys(password);
    driver.findElement(By.xpath("//button[@type='submit']")).sendKeys(Keys.RETURN);
  }

  public void removePortlet() {
    driver.switchTo().defaultContent();
    driverManager.findElementByXpath(
        "//*/div[@id='layout-column_column-1']/div[1]/section/header/menu/div/a").click();
    driverManager.findElementByXpath("/html/body/div[1]/div/div/div/*/li[6]/a/span").click();
    driverManager.acceptAlert();
  }

  public void logout() {
    driverManager.findElementByXpath("//*[@id='_145_userAvatar']/a/span/span").click();
    driverManager.findElementByXpath("//*[@id='_145_userAvatar']/ul/li[4]/a/span").click();
  }
}
