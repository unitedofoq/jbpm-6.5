/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.mailsenderhandler;

import org.apache.logging.log4j.LogManager;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mahmed
 */
public class MailSenderHandler implements WorkItemHandler {

    private static final org.apache.logging.log4j.Logger LOG = LogManager.getLogger(MailSenderHandler.class);

    @Override
    public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
        LOG.debug("Entering");
        try {
            String from = String.valueOf(workItem.getParameter("from"));
            String to = String.valueOf(workItem.getParameter("to"));
            // encode subject and body to escape whitespaces.
            String subject = URLEncoder.encode(String.valueOf(workItem.getParameter("subject")), "utf-8");
            String body = URLEncoder.encode(String.valueOf(workItem.getParameter("body")), "utf-8");
            String url = "http://localhost:8080/wsdl-locator/wscall?serviceName=sendEMail&from=" + from + "&to=" + to + "&subject=" + subject + "&body=" + body;

            URL urll = new URL(url);
            URLConnection urlConnection = urll.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String output = "";
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                if ((inputLine.contains("Error")) || (inputLine.contains("Exception"))) {
                    LOG.error("Error or exception while reading from input stream");
                    manager.completeWorkItem(workItem.getId(), null);
                    return;
                }
                output = output + inputLine;
            }
            Map<String, Object> map = new HashMap();
            map.put("Result", output);
            manager.completeWorkItem(workItem.getId(), map);
        } catch (MalformedURLException ex) {
            LOG.error("Exception thrown", ex);
            manager.completeWorkItem(workItem.getId(), null);
        } catch (IOException ex) {
            LOG.error("Exception thrown", ex);
            manager.completeWorkItem(workItem.getId(), null);
        }

    }

    @Override
    public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
        LOG.debug("Aborting workitem with id:" + workItem.getId());
    }

}
