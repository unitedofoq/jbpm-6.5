/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.kie.api.task.TaskEvent;
import org.jbpm.services.task.lifecycle.listeners.TaskLifeCycleEventListener;
import org.kie.api.task.model.Task;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 *
 * @author mmasoud
 */
public class TaskMailEvent implements TaskLifeCycleEventListener {

    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(TaskMailEvent.class);

    @Override
    public void beforeTaskUpdatedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskUpdatedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskReassignedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskReassignedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskNotificationEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskNotificationEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskInputVariableChangedEvent(TaskEvent event, Map<String, Object> variables) {
        try {
            Task task = event.getTask();
            Class taskClass = task.getClass();
            Field formName = taskClass.getDeclaredField("formName");
            formName.setAccessible(true);
            String taskName = (String) formName.get(task);
            formName.setAccessible(false);
            String creatorId = task.getTaskData().getCreatedBy().getId();
            String loggedUser = null;
            String groupId = task.getPeopleAssignments().getPotentialOwners().get(1).getId();
            if(groupId != null && !groupId.trim().equalsIgnoreCase("user") && creatorId.trim().equalsIgnoreCase("null")) {
                groupId = groupId.trim();
                loggedUser = ((String) variables.get("from")).trim();
            }
            callService("TASKMANAGEMENT", String.valueOf(task.getId()), taskName.trim(), creatorId.trim(), groupId, loggedUser);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            Logger.getLogger(TaskMailEvent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void afterTaskOutputVariableChangedEvent(TaskEvent event, Map<String, Object> variables) {

    }

    @Override
    public void beforeTaskActivatedEvent(TaskEvent event) {
    }

    @Override
    public void beforeTaskClaimedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskSkippedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskStartedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskStoppedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskCompletedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskFailedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskAddedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskExitedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskReleasedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskResumedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskSuspendedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskForwardedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskDelegatedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskNominatedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskActivatedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskClaimedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskSkippedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskStartedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskStoppedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskCompletedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskFailedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskAddedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskExitedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskReleasedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskResumedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskSuspendedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskForwardedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskDelegatedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskNominatedEvent(TaskEvent event) {

    }
    
    private void callService(String serviceName, String taskId, String taskName, String actorId, String groupId, String loggedUser) {
        try {
            String serviceUrl = null;
            LOGGER.debug("*** Service Name = " + serviceName + "***");
            File config = new File("service.config");

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(config);
            String path = "//service/name[text()='serviceName']/../wsdlurl".replace("serviceName", serviceName);
            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();
            XPathExpression expr = xpath.compile(path);
            Node node = (Node) expr.evaluate(document, XPathConstants.NODE);
            if (node != null) {
                serviceUrl = node.getTextContent();
                LOGGER.debug("ServiceName = " + serviceName);
                LOGGER.debug("WSDL URL    = " + serviceUrl);
            } else {
                LOGGER.debug("Web-Service Doesn't have WSDL URL registered");
            }
            if (serviceUrl != null) {
                sendMail4Task(serviceUrl, taskId, taskName, actorId, groupId, loggedUser);
            } else {
                LOGGER.debug("service URL equals NULL");
            }
        } catch (ParserConfigurationException | SAXException | IOException | DOMException e) {
            LOGGER.debug("some error occured during getting service URL from service.config file");
        } catch (XPathExpressionException ex) {
            Logger.getLogger(TaskMailEvent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void sendMail4Task(String serviceUrl, String taskId, String taskName, String actorId, String groupId, String loggedUser) {
        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();
            CloseableHttpResponse response;
            URIBuilder uriBuilder = new URIBuilder(serviceUrl);
            uriBuilder.addParameter("taskId", taskId);
            uriBuilder.addParameter("taskName", taskName.replace("-", "_"));
            uriBuilder.addParameter("actorId", actorId);
            uriBuilder.addParameter("groupId", groupId);
            uriBuilder.addParameter("loggedUser", loggedUser);
            URI uri = uriBuilder.build();
            HttpGet httpGet = new HttpGet(uri);
            response = client.execute(httpGet);
            response.close();
            client.close();
            LOGGER.debug("Calling " + serviceUrl);
        } catch (IOException ex) {
            LOGGER.error("Exception thrown: " + ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(TaskMailEvent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
