/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author mmasoud
 */
public class TaskMail implements Runnable {

    private static final org.apache.logging.log4j.Logger LOG = org.apache.logging.log4j.LogManager.getLogger(TaskMail.class);

    String init;
    String process;
    long processinst;
    long nodeinst;

    public TaskMail(String initiator, long processInstanceId, long nodeId, String processId) {
        this.init = initiator;
        this.processinst = processInstanceId;
        this.nodeinst = nodeId;
        this.process = processId;
    }

    public void sendTaskMail(String initiator, long processInstanceId, long nodeId, String processId) {
        String serviceName = "TASKMANAGEMENT";

        callService(serviceName, initiator, processInstanceId, nodeId, processId);
    }

    public static void callService(String wsName, String initiator, long processInstanceId, long nodeId, String processId) {
        try {
            String wsdlURL = "";
            String serviceName = wsName.trim();
            if ((serviceName != null) && (serviceName.trim().length() > 0)) {
                LOG.debug("*** Service Name = " + serviceName + "***");
                File config = new File("service.config");

                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document document = builder.parse(config);
                if (document.getChildNodes().getLength() > 0) {
                    if (document.getChildNodes().item(0).getNodeName().trim().equalsIgnoreCase("services")) {
                        Node node = document.getChildNodes().item(0);
                        for (int j = 0; j < node.getChildNodes().getLength(); j++) {
                            Node current = node.getChildNodes().item(j);
                            NodeList childs = current.getChildNodes();
                            NodeList nameNode = childs.item(0).getChildNodes();
                            NodeList wsdlNode = childs.item(1).getChildNodes();
                            String name = nameNode.item(0).getNodeValue().trim();
                            String value = wsdlNode.item(0).getNodeValue().trim();
                            if (name.equalsIgnoreCase(serviceName)) {
                                wsdlURL = value;
                                LOG.debug("ServiceName = " + name);
                                LOG.debug("WSDL URL    = " + wsdlURL);
                                break;
                            }
                        }
                        builder.reset();
                        if ((wsdlURL == null) || (wsdlURL.isEmpty())) {
                            LOG.debug("Task Mail Sender is not configured (please, add \"TASKMANAGEMENT\" URL");
                        }
                        sendMail4Task(initiator, processInstanceId, nodeId, wsdlURL, processId);
                    } else {
                        LOG.debug("Web-Service Doesn't have WSDL URL registered");
                    }
                } else {
                    LOG.debug("Web-Service Doesn't have Any WSDL URL registered");
                }
            } else {
                LOG.debug("Service Name is InValid");
            }
        } catch (ParserConfigurationException | SAXException | IOException | DOMException e) {
            LOG.debug("Web-Service Doesn't have WSDL URL registered");
        }
    }

    public static void sendMail4Task(String initiator, long processInstanceId, long nodeInstanceId, String senderURL, String processId) {
        try {
            String url = senderURL + "?initiator=" + initiator + "&processInstanceId=" + processInstanceId + "&nodeInstanceId=" + nodeInstanceId + "&processId=" + processId;

            LOG.debug("Calling " + url);

            URL oracle = new URL(url);
            URLConnection yc = oracle.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));

            String output = "";
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                output = output + inputLine;
            }
        } catch (IOException ex) {
            LOG.error("Exception thrown: " + ex);
        }
    }

    @Override
    public void run() {
        sendTaskMail(this.init, this.processinst, this.nodeinst, this.process);
    }
}
