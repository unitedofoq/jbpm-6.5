package com.unitedofoq;

import com.liferay.portal.service.UserLocalServiceUtil;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by amaroof on 17/09/18.
 */
public class UrlFilter implements Filter {

    private static final Logger logger = Logger.getLogger(UrlFilter.class.getName());

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        logger.info(">>> filter is on ");
        String url = ((HttpServletRequest) servletRequest).getRequestURL().toString();

        logger.info(">>> full url " + url);
        String[] tokens = url.split("/");

        String entry = tokens[3].toString();
        logger.info(">>> Url entry " + entry);
        String urlUsername = tokens[4].toString();
        logger.info(">>> Url username " + urlUsername);

        String loginUsername = null;

        if (((String[]) servletRequest.getParameterMap().get("_58_redirect")) != null && ((String[]) servletRequest.getParameterMap().get("_58_redirect"))[0] != null) {

            URI redierctURl = null;
            try {
                redierctURl = new URI(((String[]) servletRequest.getParameterMap().get("_58_redirect"))[0]);

            } catch (URISyntaxException ex) {
                Logger.getLogger(UrlFilter.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (redierctURl.getHost() != null && !redierctURl.getHost().equalsIgnoreCase(servletRequest.getRemoteHost())
                    && redierctURl.getPort() > 0 && redierctURl.getPort() != servletRequest.getLocalPort()) {

                HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
                httpResponse.sendRedirect("/c/portal/logout");

                return;
            }
        }
        if (entry.equals("user")) {
            HttpSession session = ((HttpServletRequest) servletRequest).getSession();
            Object UserId = session.getAttribute("USER_ID");

            try {
                loginUsername = UserLocalServiceUtil.getUser(Long.parseLong(UserId.toString())).getScreenName();
                logger.info(">>> login username " + loginUsername);
            } catch (Exception e) {
                filterChain.doFilter(servletRequest, servletResponse);
            }

            if (urlUsername.equals(loginUsername)) {
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
                httpResponse.sendRedirect("/c/portal/logout");
            }
        } else if (entry.equals("web")) {

            if (tokens[4].equals("guest") || loginUsername.equals(urlUsername)) {
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
                httpResponse.sendRedirect("/c/portal/logout");
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }

    }

    @Override
    public void destroy() {

    }
}
