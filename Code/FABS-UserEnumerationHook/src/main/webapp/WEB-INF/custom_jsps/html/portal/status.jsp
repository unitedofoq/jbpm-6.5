<%--
  Created by IntelliJ IDEA.
  User: amaroof
  Date: 30/08/18
  Time: 04:21 م
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Oops!</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">

</head>
<body>
<div class="message">Oops! 404!</div>
<br><br>
<div class="message2">Something went wrong in url ..</div></body>
</html>

<style>

    @import url('https://fonts.googleapis.com/css?family=Roboto:300,500,900|Open+Sans:300,400,700');

    body{
        background:#7AABB2;
    }

    *{
        margin: 0;
    }

    .message{
        text-align: center;
        color: #0145b2;
        font-family: 'Open Sans';
        font-size: 65px;
        padding-top: 100px;
    }

    .message2{
        text-align: center;
        color: #205fc6;
        font-family: 'Open Sans';
        font-size: 30px;
        padding-bottom: 50px
    }


</style>