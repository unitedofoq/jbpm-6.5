/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.primefaces.component.export;

import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.el.PropertyNotFoundException;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import org.primefaces.component.api.DynamicColumn;
import org.primefaces.component.api.UIColumn;
import org.primefaces.component.column.Column;
import org.primefaces.component.columns.Columns;
import org.primefaces.component.datatable.DataTable;

/**
 *
 * @author lap
 */
public class CustomPDFExporter extends PDFExporter {

    final static Logger logger = Logger.getLogger("CustomPDFExporter");

    @Override
    protected void exportCells(DataTable table, PdfPTable pdfTable) {
        for (UIColumn col : table.getColumns()) {
            if (!col.isRendered()) {
                continue;
            }

            if (col instanceof DynamicColumn) {
                ((DynamicColumn) col).applyModel();
            }

            if (col.isExportable()) {
                if (col instanceof Column && !((Column) col).getId().contains("_column_editor") && !((Column) col).getId().contains("bcolumn") && !((Column) col).getId().contains("blncolumn")) {
                    Font font = FontFactory.getFont("Arial.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                    addColumnValue(pdfTable, col.getChildren(), font);
                }
            }
        }
    }

    @Override
    protected void addColumnValue(PdfPTable pdfTable, UIComponent component, Font font) {
        String value = component == null ? "" : exportValue(FacesContext.getCurrentInstance(), component);
        if (value != null && !value.equals("")) {
            Paragraph paragraph = new Paragraph(value, font);
            paragraph.setAlignment(1);
            PdfPCell cell = new PdfPCell();
            cell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            cell.addElement(paragraph);
            cell.setBackgroundColor(Color.GRAY);
            pdfTable.addCell(cell);
        }

    }

    @Override
    protected int getColumnsCount(DataTable table) {
        int count = 0;
        for (UIComponent child : table.getChildren()) {
            if (!child.isRendered()) {
                continue;
            }

            if (child instanceof Column) {
                if (!child.getId().contains("blncolumn") && !child.getId().contains("bcolumn") && !child.getId().contains("_column_editor") && !child.getChildren().isEmpty()) {
                    Column column = (Column) child;

                    if (column.isExportable()) {
                        count++;
                    }
                }
            } else if (child instanceof Columns) {
                Columns columns = (Columns) child;

                if (columns.isExportable()) {
                    count += columns.getRowCount();
                }

            }

        }

        return count;
    }

    @Override
    protected void addColumnValue(PdfPTable pdfTable, List<UIComponent> components, Font font) {
        StringBuilder builder = new StringBuilder();
        String value = "";
        for (UIComponent component : components) {
            Map<String, UIComponent> componentList = component.getFacets();
            for (Map.Entry<String, UIComponent> entry : componentList.entrySet()) {
                List<UIComponent> componentlist1 = entry.getValue().getChildren();

                for (UIComponent component1 : componentlist1) {
                    if (component1.isRendered()) {
                        value = exportValue(FacesContext.getCurrentInstance(), component1);

                        if (value != null && !builder.toString().equals(value) && !value.contains(".component.html.HtmlMessage") && !value.contains("HtmlPanelGrid") && !value.contains("component.message.Message")) {
                            builder.append(value);
                        }
                    }

                    List<UIComponent> componentlist2 = component1.getChildren();

                    for (UIComponent component2 : componentlist2) {
                        if (component2.isRendered()) {

                            try {
                                value = exportValue(FacesContext.getCurrentInstance(), component2);
                            } catch (PropertyNotFoundException ex) {

                                logger.log(Level.WARNING, ex.getMessage());
                            }
                            if (value != null && !builder.toString().equals(value) && !value.contains(".component.html.HtmlMessage") && !value.contains("HtmlPanelGrid") && !value.contains("component.message.Message")) {
                                builder.append(value);
                            }
                        }

                        List<UIComponent> componentlist3 = component2.getChildren();

                        for (UIComponent component3 : componentlist3) {
                            if (component3.isRendered()) {
                                value = exportValue(FacesContext.getCurrentInstance(), component3);
                                if (value != null && !builder.toString().equals(value) && !value.contains(".component.html.HtmlMessage") && !value.contains(".commandbutton.CommandButton") && !value.contains("HtmlPanelGrid") && !value.contains("component.message.Message")) {
                                    builder.append(value);
                                }
                            }
                            List<UIComponent> componentlist4 = component3.getChildren();

                            for (UIComponent component4 : componentlist4) {
                                if (component4.isRendered()) {
                                    value = exportValue(FacesContext.getCurrentInstance(), component4);
                                    if (value != null && !builder.toString().equals(value) && !value.contains(".component.html.HtmlMessage") && !value.contains(".component.UISelectItems") && !value.contains(".commandbutton.CommandButton") && !value.contains("HtmlPanelGrid") && !value.contains("component.message.Message")) {
                                        builder.append(value);
                                    }
                                }

                            }
                        }

                    }
                }

            }
            List<UIComponent> componentList2 = component.getChildren();
            for (UIComponent component1 : componentList2) {
                if (component1.isRendered()) {

                    value = exportValue(FacesContext.getCurrentInstance(), component1);

                    if (value != null && !builder.toString().equals(value) && !value.contains(".component.html.HtmlMessage") && !value.contains(".component.overlaypanel.OverlayPanel") && !value.contains(".component.commandbutton.CommandButton") && !value.contains(".component.roweditor.RowEditor") && !value.contains(".component.celleditor.CellEditor") && !value.contains("HtmlPanelGrid") && !value.contains("component.message.Message")) {
                        builder.append(value);
                    }
                }
                List<UIComponent> componentList3 = component1.getChildren();
                for (UIComponent component2 : componentList3) {
                    if (component2.isRendered()) {

                        value = exportValue(FacesContext.getCurrentInstance(), component2);

                        if (value != null && !builder.toString().equals(value) && !value.contains(".component.html.HtmlMessage") && !value.contains(".component.overlaypanel.OverlayPanel") && !value.contains(".component.commandbutton.CommandButton") && !value.contains(".component.roweditor.RowEditor") && !value.contains(".component.celleditor.CellEditor") && !value.contains("HtmlPanelGrid") && !value.contains("component.message.Message")) {
                            builder.append(value);
                        }
                    }
                }
            }
        }
        PdfPCell cell = new PdfPCell();
        cell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
        cell.addElement(new Paragraph(builder.toString(), font));
        pdfTable.addCell(cell);
    }

    @Override
    protected void addColumnFacets(DataTable table, PdfPTable pdfTable, ColumnType columnType) {
        pdfTable.setWidthPercentage(100);
        for (UIColumn col : table.getColumns()) {
            if (!col.isRendered()) {
                continue;
            }

            if (col instanceof DynamicColumn) {
                ((DynamicColumn) col).applyModel();
            }

            if (col.isExportable()) {
                Font font = FontFactory.getFont("Arial.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                addColumnValue(pdfTable, col.getFacet(columnType.facet()), font);
            }
        }
    }
}
