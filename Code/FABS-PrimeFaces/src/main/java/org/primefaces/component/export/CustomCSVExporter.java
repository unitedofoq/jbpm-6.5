/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.primefaces.component.export;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.el.PropertyNotFoundException;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.component.api.DynamicColumn;
import org.primefaces.component.api.UIColumn;
import org.primefaces.component.column.Column;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.util.Constants;

/**
 *
 * @author lap
 */
public class CustomCSVExporter extends CSVExporter {

    final static Logger logger = Logger.getLogger("CustomPDFExporter");

    @Override
    protected void addColumnFacets(Writer writer, DataTable table, ColumnType columnType) throws IOException {
        boolean firstCellWritten = false;

        for (UIColumn col : table.getColumns()) {
            if (!col.isRendered()) {
                continue;
            }

            if (col instanceof DynamicColumn) {
                ((DynamicColumn) col).applyModel();
            }

            if (col.isExportable()) {
                if (col instanceof Column && !((Column) col).getId().contains("blncolumn") && !((Column) col).getId().contains("bcolumn") && !((Column) col).getId().contains("_column_editor") && !((Column) col).getChildren().isEmpty()) {
                    if (firstCellWritten) {
                        writer.write(",");
                    }

                    addColumnValue(writer, col.getFacet(columnType.facet()));
                    firstCellWritten = true;
                }
            }
        }

        writer.write("\n");
    }

    @Override
    protected void exportCells(DataTable table, Writer writer) throws IOException {
        boolean firstCellWritten = false;

        for (UIColumn col : table.getColumns()) {
            if (!col.isRendered()) {
                continue;
            }

            if (col instanceof DynamicColumn) {
                ((DynamicColumn) col).applyModel();
            }

            if (col.isExportable()) {
                if (col instanceof Column && !((Column) col).getId().contains("_column_editor") && !((Column) col).getId().contains("bcolumn") && !((Column) col).getId().contains("blncolumn")) {
                    if (firstCellWritten) {
                        writer.write(",");
                    }

                    addColumnValue(writer, col.getChildren());
                    firstCellWritten = true;
                }
            }
        }
    }

    @Override
    protected void addColumnValue(Writer writer, List<UIComponent> components) throws IOException {
        StringBuilder builder = new StringBuilder();
        String value = "";
        for (UIComponent component : components) {
            Map<String, UIComponent> componentList = component.getFacets();
            for (Map.Entry<String, UIComponent> entry : componentList.entrySet()) {
                List<UIComponent> componentlist1 = entry.getValue().getChildren();

                for (UIComponent component1 : componentlist1) {
                    if (component1.isRendered()) {
                        value = exportValue(FacesContext.getCurrentInstance(), component1);

                        if (value != null && !builder.toString().equals(value) && !value.contains(".component.html.HtmlMessage") && !value.contains(".component.commandbutton.CommandButton") && !value.contains("HtmlPanelGrid") && !value.contains("component.message.Message")) {
                            value = value.replaceAll("\"", "\"\"");

                            builder.append(value);
                        }
                    }

                    List<UIComponent> componentlist2 = component1.getChildren();

                    for (UIComponent component2 : componentlist2) {
                        if (component2.isRendered()) {

                            try {
                                value = exportValue(FacesContext.getCurrentInstance(), component2);
                            } catch (PropertyNotFoundException ex) {

                                logger.log(Level.WARNING, ex.getMessage());
                            }
                            if (value != null && !builder.toString().equals(value) && !value.contains(".component.html.HtmlMessage") && !value.contains(".component.commandbutton.CommandButton") && !value.contains("HtmlPanelGrid") && !value.contains("component.message.Message")) {
                                value = value.replaceAll("\"", "\"\"");

                                builder.append(value);
                            }
                        }
                        List<UIComponent> componentlist3 = component2.getChildren();

                        for (UIComponent component3 : componentlist3) {
                            if (component3.isRendered()) {
                                value = exportValue(FacesContext.getCurrentInstance(), component3);
                                if (value != null && !builder.toString().equals(value) && !value.contains(".component.html.HtmlMessage") && !value.contains(".commandbutton.CommandButton") && !value.contains("HtmlPanelGrid") && !value.contains("component.message.Message")) {
                                    value = value.replaceAll("\"", "\"\"");

                                    builder.append(value);
                                }
                            }
                            List<UIComponent> componentlist4 = component3.getChildren();

                            for (UIComponent component4 : componentlist4) {
                                if (component4.isRendered()) {
                                    value = exportValue(FacesContext.getCurrentInstance(), component4);
                                    if (value != null && !builder.toString().equals(value) && !value.contains(".component.html.HtmlMessage") && !value.contains(".component.UISelectItems") && !value.contains(".commandbutton.CommandButton") && !value.contains("HtmlPanelGrid") && !value.contains("component.message.Message")) {
                                        value = value.replaceAll("\"", "\"\"");

                                        builder.append(value);
                                    }
                                }

                            }
                        }
                    }
                }
            }

            List<UIComponent> componentList2 = component.getChildren();
            for (UIComponent component1 : componentList2) {
                if (component1.isRendered()) {

                    value = exportValue(FacesContext.getCurrentInstance(), component1);

                    if (value != null && !builder.toString().equals(value) && !value.contains(".component.html.HtmlMessage") && !value.contains(".component.overlaypanel.OverlayPanel") && !value.contains(".component.commandbutton.CommandButton") && !value.contains(".component.roweditor.RowEditor") && !value.contains(".component.celleditor.CellEditor") && !value.contains("HtmlPanelGrid") && !value.contains("component.message.Message")) {

                        value = value.replaceAll("\"", "\"\"");

                        builder.append(value);

                    }
                }
                List<UIComponent> componentList3 = component1.getChildren();
                for (UIComponent component2 : componentList3) {
                    if (component2.isRendered()) {

                        value = exportValue(FacesContext.getCurrentInstance(), component2);

                        if (value != null && !builder.toString().equals(value) && !value.contains(".component.html.HtmlMessage") && !value.contains(".component.overlaypanel.OverlayPanel") && !value.contains(".component.commandbutton.CommandButton") && !value.contains(".component.roweditor.RowEditor") && !value.contains(".component.celleditor.CellEditor") && !value.contains("HtmlPanelGrid") && !value.contains("component.message.Message")) {

                            value = value.replaceAll("\"", "\"\"");

                            builder.append(value);

                        }
                    }
                }
            }
        }

        writer.write("\"" + builder.toString() + "\"");

    }

    @Override
    protected void configureResponse(ExternalContext externalContext, String filename) {
        externalContext.setResponseContentType("text/csv");
        externalContext.setResponseHeader("Expires", "0");
        externalContext.setResponseHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        externalContext.setResponseHeader("Pragma", "public");
        externalContext.setResponseCharacterEncoding("utf-8");
        externalContext.setResponseHeader("Content-disposition", "attachment;filename=" + filename + ".csv");
        externalContext.addResponseCookie(Constants.DOWNLOAD_COOKIE, "true", new HashMap<String, Object>());
    }
}
