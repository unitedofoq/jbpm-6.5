/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.primefaces.component.export;

import javax.faces.FacesException;

/**
 *
 * @author lap
 */
public class ExporterFactory{
    

    public static Exporter getExporterForType(String type) {
        Exporter exporter = null;

        try {
            ExporterType exporterType = ExporterType.valueOf(type.toUpperCase());

            switch(exporterType) {

                case XLS:
                    exporter = new CustomExcelExporter();
                    break;

                case PDF:
                    exporter = new CustomPDFExporter();
                    break;

                case CSV:
                    exporter = new CustomCSVExporter();
                    break;

                case XML:
                    exporter = new XMLExporter();
                    break;

              
            }
        }
        catch(IllegalArgumentException e) {
            throw new FacesException(e);
        }

        return exporter;
    }
}
