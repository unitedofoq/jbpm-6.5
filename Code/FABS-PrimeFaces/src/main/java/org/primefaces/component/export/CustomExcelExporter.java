/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.primefaces.component.export;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.el.PropertyNotFoundException;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.primefaces.component.api.DynamicColumn;
import org.primefaces.component.api.UIColumn;
import org.primefaces.component.column.Column;
import org.primefaces.component.datatable.DataTable;

/**
 *
 * @author lap
 */
public class CustomExcelExporter extends ExcelExporter {

    final static Logger logger = Logger.getLogger("CustomPDFExporter");

    @Override
    protected void exportCells(DataTable table, Sheet sheet) {
        int sheetRowIndex = sheet.getLastRowNum() + 1;
        Row row = sheet.createRow(sheetRowIndex);

        for (UIColumn col : table.getColumns()) {
            if (!col.isRendered()) {
                continue;
            }

            if (col instanceof DynamicColumn) {
                ((DynamicColumn) col).applyModel();
            }

            if (col.isExportable()) {
                if (col instanceof Column && !((Column) col).getId().contains("_column_editor") && !((Column) col).getId().contains("bcolumn") && !((Column) col).getId().contains("blncolumn")) {
                    addColumnValue(row, col.getChildren());
                }
            }
        }
    }

    @Override
    protected void addColumnFacets(DataTable table, Sheet sheet, ColumnType columnType) {
        int sheetRowIndex = columnType.equals(ColumnType.HEADER) ? 0 : (sheet.getLastRowNum() + 1);
        Row rowHeader = sheet.createRow(sheetRowIndex);

        for (UIColumn col : table.getColumns()) {
            if (!col.isRendered()) {
                continue;
            }

            if (col instanceof DynamicColumn) {
                ((DynamicColumn) col).applyModel();
            }

            if (col.isExportable()) {
                if (col instanceof Column && !((Column) col).getId().contains("blncolumn") && !((Column) col).getId().contains("bcolumn") && !((Column) col).getId().contains("_column_editor") && !((Column) col).getChildren().isEmpty()) {
                    addColumnValue(rowHeader, col.getFacet(columnType.facet()));
                }
            }
        }
    }

    @Override
    protected void addColumnValue(Row row, List<UIComponent> components) {
        int cellIndex = row.getLastCellNum() == -1 ? 0 : row.getLastCellNum();
        Cell cell = row.createCell(cellIndex);
        StringBuilder builder = new StringBuilder();
        String value = "";
        for (UIComponent component : components) {
            Map<String, UIComponent> componentList = component.getFacets();

            for (Map.Entry<String, UIComponent> entry : componentList.entrySet()) {
                List<UIComponent> componentlist1 = entry.getValue().getChildren();

                for (UIComponent component1 : componentlist1) {
                    if (component1.isRendered()) {
                        value = exportValue(FacesContext.getCurrentInstance(), component1);

                        if (value != null && !builder.toString().equals(value) && !value.contains("HtmlPanelGrid") && !value.contains("component.message.Message") && !value.contains(".component.html.HtmlMessage")) {
                            builder.append(value);
                        }
                    }

                    List<UIComponent> componentlist2 = component1.getChildren();

                    for (UIComponent component2 : componentlist2) {
                        if (component2.isRendered()) {

                            try {
                                value = exportValue(FacesContext.getCurrentInstance(), component2);
                            } catch (PropertyNotFoundException ex) {

                                logger.log(Level.WARNING, ex.getMessage());
                            }
                            if (value != null && !builder.toString().equals(value) && !value.contains("HtmlPanelGrid") && !value.contains("component.message.Message") && !value.contains(".component.html.HtmlMessage")) {
                                builder.append(value);
                            }
                        }
                        List<UIComponent> componentlist3 = component2.getChildren();

                        for (UIComponent component3 : componentlist3) {
                            if (component3.isRendered()) {
                                value = exportValue(FacesContext.getCurrentInstance(), component3);
                                if (value != null && !builder.toString().equals(value) && !value.contains(".commandbutton.CommandButton") && !value.contains("HtmlPanelGrid") && !value.contains("component.message.Message") && !value.contains(".component.html.HtmlMessage")) {
                                    builder.append(value);
                                }
                            }
                            List<UIComponent> componentlist4 = component3.getChildren();

                            for (UIComponent component4 : componentlist4) {
                                if (component4.isRendered()) {
                                    value = exportValue(FacesContext.getCurrentInstance(), component4);
//
                                    if (value != null && !builder.toString().equals(value) && !value.contains(".component.UISelectItems") && !value.contains(".commandbutton.CommandButton") && !value.contains("HtmlPanelGrid") && !value.contains("component.message.Message") && !value.contains(".component.html.HtmlMessage")) {
                                        builder.append(value);
                                    }
                                }

                            }
                        }
                    }
                }

            }

            List<UIComponent> componentList2 = component.getChildren();
            for (UIComponent component1 : componentList2) {
                if (component1.isRendered()) {

                    value = exportValue(FacesContext.getCurrentInstance(), component1);

                    if (value != null && !builder.toString().equals(value) && !value.contains(".component.html.HtmlMessage") && !value.contains(".component.overlaypanel.OverlayPanel") && !value.contains(".component.commandbutton.CommandButton") && !value.contains(".component.roweditor.RowEditor") && !value.contains(".component.celleditor.CellEditor") && !value.contains("HtmlPanelGrid") && !value.contains("component.message.Message")) {
                        builder.append(value);
                    }
                }
                List<UIComponent> componentList3 = component1.getChildren();
                for (UIComponent component2 : componentList3) {
                    if (component2.isRendered()) {

                        value = exportValue(FacesContext.getCurrentInstance(), component2);

                        if (value != null && !builder.toString().equals(value) && !value.contains(".component.html.HtmlMessage") && !value.contains(".component.overlaypanel.OverlayPanel") && !value.contains(".component.commandbutton.CommandButton") && !value.contains(".component.roweditor.RowEditor") && !value.contains(".component.celleditor.CellEditor") && !value.contains("HtmlPanelGrid") && !value.contains("component.message.Message")) {
                            builder.append(value);
                        }
                    }
                }
            }

        }

        cell.setCellValue(new HSSFRichTextString(builder.toString()));

    }
}
