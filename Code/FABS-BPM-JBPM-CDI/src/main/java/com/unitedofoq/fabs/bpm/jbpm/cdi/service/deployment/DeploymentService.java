/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.bpm.jbpm.cdi.service.deployment;

import com.unitedofoq.fabs.bpm.common.connector.RestConnector;
import com.unitedofoq.fabs.jbpm.common.util.BPMPropertiesUtil;
import java.text.MessageFormat;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import org.jbpm.services.api.DeploymentEvent;
import org.jbpm.services.cdi.Undeploy;

/**
 *
 * @author mmasoud
 */
@ApplicationScoped
public class DeploymentService {

    private BPMPropertiesUtil bPMPropertiesUtil;
    private RestConnector restConnector;

    public DeploymentService() {
        bPMPropertiesUtil = new BPMPropertiesUtil();
        String requestTimeout = bPMPropertiesUtil.getValue("engine.requestConnectionTimeout");
        restConnector = new RestConnector(Integer.parseInt(requestTimeout) * 1000);
    }

    @PostConstruct
    public void init() {
        System.out.println("com.unitedofoq.fabs.bpm.jbpm.cdi.service.deployment.DeploymentService.init()");
    }

    public void unRegisterContainer(@Observes @Undeploy DeploymentEvent event) {
        String userName = bPMPropertiesUtil.getValue("engine.userName");
        String password = bPMPropertiesUtil.getValue("engine.password");
        String host = bPMPropertiesUtil.getValue("engine.host");
        String port = bPMPropertiesUtil.getValue("engine.port");
        String url = bPMPropertiesUtil.getValue("deployment.container");
        String deploymentId = event.getDeploymentId();
        url = MessageFormat.format(url, host, port, deploymentId);
        restConnector.sendDeleteRequest(url, null, userName, password);
    }
}
