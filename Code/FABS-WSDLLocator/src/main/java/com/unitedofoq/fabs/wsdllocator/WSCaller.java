/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.wsdllocator;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import org.apache.logging.log4j.LogManager;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author mahmed
 */
@WebServlet(name = "WSCaller", urlPatterns = {"/WSCall", "/wscall", "/WSDLCall"})
public class WSCaller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final org.apache.logging.log4j.Logger LOG = LogManager.getLogger(WSCaller.class);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOG.debug("Entering");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            out.write(callService(request.getParameter("serviceName"), request.getParameterMap()));
        } finally {
            out.close();
        }
    }

    private String callService(String wsName, Map<String, String[]> values) {
        try {
            String SOAPAction = "", wsdlURL = "", namespace = "", schemaLocation;
            List<String> inputs = new ArrayList<String>();
            String serviceName = wsName.trim();
            if (serviceName != null && serviceName.trim().length() > 0) {
                LOG.debug("*** Service Name = " + serviceName + "***");
                File config = new File("service.config");
                Document document;
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                document = builder.parse(config);
                
                if (document.getChildNodes().getLength() > 0) {
                    if (document.getChildNodes().item(0).getNodeName().trim().equalsIgnoreCase("services")) {
                        String path = "//service/name[text()='serviceName']/../wsdlurl".replace("serviceName", serviceName);
                        XPathFactory xPathfactory = XPathFactory.newInstance();
                        XPath xpath = xPathfactory.newXPath();
                        XPathExpression expr = xpath.compile(path);
                        Node node = (Node) expr.evaluate(document, XPathConstants.NODE);
                        if (node != null) {
                            wsdlURL = node.getTextContent();
                            LOG.debug("ServiceName = " + serviceName);
                            LOG.debug("WSDL URL    = " + wsdlURL);
                        }
                        builder.reset();
                        document = builder.parse(wsdlURL);
                        
                        // added by Belal: support glassfish and wildfly implementation
                        if (document.getChildNodes().getLength() > 0) {
                            WSCallerRequest wsCallerRequest;
                            String result = null;
                            if(document.getElementsByTagName("wsdl:types") != null) {
                                wsCallerRequest = parseForWildFly(document, wsName);
                                result = requestWS(wsdlURL, "", wsCallerRequest.getNamespace(), serviceName, values, wsCallerRequest.getInputs());
                            } else {
                                wsCallerRequest = parseForGlassfish(document, wsName);
                                result = requestWS(wsdlURL, wsCallerRequest.getSoapAction(), wsCallerRequest.getNamespace(), serviceName, values, wsCallerRequest.getInputs());
                            }
                            
                            LOG.debug("Result = " + result);
                            return result;
                        }
                        
                        
                    } else {
                        LOG.debug("Web-Service Doesn't have WSDL URL registered");
                    }
                } else {
                    LOG.debug("Web-Service Doesn't have Any WSDL URL registered");
                }

            } else {
                LOG.debug("Service Name is InValid");
            }
        } catch (Exception e) {
            LOG.error("Exception thrown:" + e);
            LOG.debug("Web-Service Doesn't have WSDL URL registered");
        }
        return "Final";
    }
    
    private WSCallerRequest parseForGlassfish(Document document, String wsName) throws SAXException, IOException, ParserConfigurationException {
        WSCallerRequest wsCallerRequest = new WSCallerRequest();
        Node schemaNode = document.getElementsByTagName("xsd:import").item(0);
        NamedNodeMap nodesAttr = schemaNode.getAttributes();
        Node nameSpaceNode = nodesAttr.getNamedItem("namespace");
        wsCallerRequest.setNamespace(nameSpaceNode.getNodeValue());
        
        Node portTypeNode = document.getElementsByTagName("soap:address").item(0);
        nodesAttr = portTypeNode.getAttributes();
        wsCallerRequest.setSoapAction(nodesAttr.getNamedItem("location").getNodeValue());
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        
        Node schemaLocationNode = nodesAttr.getNamedItem("schemaLocation");
        String schemaLocation = schemaLocationNode.getNodeValue();
        document = builder.parse(schemaLocation);
        if (document.getChildNodes().getLength() > 0) {
            Node inputNode = null;
            NodeList inputTypeNode = document.getElementsByTagName("xs:complexType");
            if (inputTypeNode.getLength() > 0) {
                for (int i = 0; i < inputTypeNode.getLength(); i++) {
                    if (inputTypeNode.item(i).getAttributes().getNamedItem("name").getNodeValue().equalsIgnoreCase(wsName)) {
                        inputNode = inputTypeNode.item(i);
                        break;
                    }
                }
                for (int i = 0; i < inputNode.getChildNodes().getLength(); i++) {
                    Node input = inputNode.getChildNodes().item(i);
                    if (input.getNodeName().equalsIgnoreCase("xs:sequence")) {
                        NodeList allSeq = input.getChildNodes();
                        LOG.debug("Input Count = " + allSeq.getLength());
                        for (int k = 0; k < allSeq.getLength(); k++) {
                            Node currentInputNode = allSeq.item(k);
                            if (currentInputNode != null && currentInputNode.getAttributes() != null) {
                                Node attr = currentInputNode.getAttributes().getNamedItem("name");
                                if (attr != null) {
                                    LOG.debug("input Name = " + attr.getNodeValue());
                                    wsCallerRequest.getInputs().add(attr.getNodeValue());
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }
        return wsCallerRequest;
    }
    
    private WSCallerRequest parseForWildFly(Document document, String wsName) {
        WSCallerRequest wsCallerRequest = new WSCallerRequest();
        
        Node schemaNode = document.getElementsByTagName("xs:schema").item(0);
        NamedNodeMap nodesAttr = schemaNode.getAttributes();
        Node nameSpaceNode = nodesAttr.getNamedItem("targetNamespace");
        wsCallerRequest.setNamespace(nameSpaceNode.getNodeValue());

        Node portTypeNode = document.getElementsByTagName("soap:address").item(0);
        nodesAttr = portTypeNode.getAttributes();
        wsCallerRequest.setSoapAction(nodesAttr.getNamedItem("location").getNodeValue());

        Node inputNode = null;
        NodeList inputTypeNode = document.getElementsByTagName("xs:complexType");
        if (inputTypeNode.getLength() > 0) {
            for (int i = 0; i < inputTypeNode.getLength(); i++) {
                if (inputTypeNode.item(i).getAttributes().getNamedItem("name").getNodeValue().equalsIgnoreCase(wsName)) {
                    inputNode = inputTypeNode.item(i);
                    break;
                }
            }
            for (int i = 0; i < inputNode.getChildNodes().getLength(); i++) {
                Node input = inputNode.getChildNodes().item(i);
                if (input.getNodeName().equalsIgnoreCase("xs:sequence")) {
                    NodeList allSeq = input.getChildNodes();
                    System.out.println("Input Count = " + allSeq.getLength());
                    for (int k = 0; k < allSeq.getLength(); k++) {
                        Node currentInputNode = allSeq.item(k);
                        if (currentInputNode != null && currentInputNode.getAttributes() != null) {
                            Node attr = currentInputNode.getAttributes().getNamedItem("name");
                            if (attr != null) {
                                System.out.println("input Name = " + attr.getNodeValue());
                                wsCallerRequest.getInputs().add(attr.getNodeValue());
                            }
                        }
                    }
                    break;
                }
            }
        }
        
        return wsCallerRequest;
    }

    private static String requestWS(String wsdlURL, String SOAPAction, String namespace, String methodName, Map<String, String[]> values, List<String> inputs) {
        java.io.OutputStream reqStream = null;
        java.io.InputStream resStream = null;
        try {
            LOG.debug("WSDL URL        = " + wsdlURL);
            LOG.debug("SOAP Action     = " + SOAPAction);
            LOG.debug("namespace       = " + namespace);
            LOG.debug("Method Name     = " + methodName);
            for (int i = 0; i < inputs.size(); i++) {
                LOG.debug("Input " + inputs.get(i) + "  --> " + values.get(String.valueOf(inputs.get(i))));
            }
            URL oURL = new URL(wsdlURL);
            java.net.HttpURLConnection con = (java.net.HttpURLConnection) oURL.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            con.setRequestProperty("SOAPAction", SOAPAction);
            String reqXML = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" "
                    + "xmlns:com=\"" + namespace + "\">\n"
                    + "   <soapenv:Header/>\n" + "   <soapenv:Body>\n"
                    + "      <com:" + methodName + ">\n"
                    + generateBody(values, inputs)
                    + "      </com:" + methodName + ">\n"
                    + "   </soapenv:Body>\n</soapenv:Envelope>";
            reqStream = con.getOutputStream();

            reqStream.write(reqXML.getBytes());
            resStream = con.getInputStream();
            Document document;
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse(resStream);
            LOG.debug(document.toString());
            return document.getElementsByTagName("return").item(0).getChildNodes().item(0).getNodeValue();

        } catch (Exception ex) {
            LOG.error("Exception thrown:" + ex);
        } finally {
            try {
                reqStream.close();
                resStream.close();
            } catch (IOException ex) {
                LOG.error("Exception thrown:" + ex);
            }
        }
        return "";
    }

    private static String generateBody(Map<String, String[]> values, List<String> inputs) {
        if (values.size() != inputs.size()) {
            LOG.debug("Input and Values are not equal");
            LOG.debug("Inputs is " + inputs.size());
            LOG.debug("Values is " + values.size());
        }
        String body = "";
        for (int i = 0; i < inputs.size(); i++) {
            if (values.size() > i) {
                body += "<" + inputs.get(i) + ">" + values.get(String.valueOf(inputs.get(i)))[0] + "</" + inputs.get(i) + ">\n";
            } else {
                body += "<" + inputs.get(i) + "></" + inputs.get(i) + ">\n";
            }
        }
        return body;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
