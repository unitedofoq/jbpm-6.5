/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.bpm.common.model;

import java.util.Date;
import java.util.List;

/**
 *
 * @author mmasoud
 */
public class DelegationData {
    
    private String owner;
    private String delegate;
    private Date fromTime;
    private Date toTime;
    private List<String> processIds;

    public DelegationData() {
    }

    public DelegationData(String owner, String delegate, Date fromTime, 
            Date toTime, List<String> processIds) {
        this.owner = owner;
        this.delegate = delegate;
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.processIds = processIds;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getDelegate() {
        return delegate;
    }

    public void setDelegate(String delegate) {
        this.delegate = delegate;
    }

    public Date getFromTime() {
        return fromTime;
    }

    public void setFromTime(Date fromTime) {
        this.fromTime = fromTime;
    }

    public Date getToTime() {
        return toTime;
    }

    public void setToTime(Date toTime) {
        this.toTime = toTime;
    }

    public List<String> getProcessIds() {
        return processIds;
    }

    public void setProcessIds(List<String> processIds) {
        this.processIds = processIds;
    }
}
