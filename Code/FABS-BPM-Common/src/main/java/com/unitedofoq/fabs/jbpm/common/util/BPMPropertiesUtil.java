package com.unitedofoq.fabs.jbpm.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.annotation.PostConstruct;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mmasoud
 */
public class BPMPropertiesUtil {

    private Properties properties;
    private long lastModified = 0;
    private File BpmConfigFile = null;

    public BPMPropertiesUtil() {
        InputStream input = null;
        String fileName = "process_engine_config.properties";
        String filePath = System.getProperty("fabsPropertyPath");
        StringBuilder file = new StringBuilder(filePath).append(File.separator)
                .append(fileName);
        try {
            input = new FileInputStream(file.toString());
            BpmConfigFile = new File(file.toString());
            lastModified = BpmConfigFile.lastModified();
            // load a properties from a file
            properties = new Properties();
            properties.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getValue(String key) {
        return properties.getProperty(key);
    }
}
