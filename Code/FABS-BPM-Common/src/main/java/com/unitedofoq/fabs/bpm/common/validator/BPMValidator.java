/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.bpm.common.validator;

import com.unitedofoq.fabs.bpm.common.exception.BPMException;


/**
 *
 * @author bgalal
 */
public interface BPMValidator {

    public void validate(Object object) throws BPMException;
}
