package com.unitedofoq.fabs.bpm.common.validator;

import com.unitedofoq.fabs.bpm.common.exception.BPMException;
import org.apache.http.HttpStatus;


public class JBPMRestValidator implements BPMValidator {

	@Override
	public void validate(Object object) throws BPMException {
            final Integer statusCode=(Integer)object;
            if (statusCode == HttpStatus.SC_NOT_FOUND){
                return;
            }
            if (statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_CREATED) {
                    throw new BPMException("RestIntegrationException with jbpm");
            }
	}

}
