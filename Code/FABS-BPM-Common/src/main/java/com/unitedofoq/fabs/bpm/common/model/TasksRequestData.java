/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.bpm.common.model;

import java.util.List;
import java.util.Map;

/**
 *
 * @author mmasoud
 */
public class TasksRequestData {
    private String actorId;
    private int page;
    private int pageSize;
    private String sortedField;
    private String sortingOrder;
    private List<DelegationData> delegations;
    private Map<String, String> filters;
    private List<String> roles;
    public TasksRequestData() {
    }

    public TasksRequestData(String actorId, int page, int pageSize,
            List<DelegationData> delegations, String sortedField,
            String sortingOrder, Map<String, String> filters, List<String> groups) {
        this.actorId = actorId;
        this.page = page;
        this.pageSize = pageSize;
        this.delegations = delegations;
        this.sortedField = sortedField;
        this.sortingOrder = sortingOrder;
        this.filters = filters;
        this.roles = roles;
    }

    public String getActorId() {
        return actorId;
    }

    public void setActorId(String actorId) {
        this.actorId = actorId;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List<DelegationData> getDelegations() {
        return delegations;
    }

    public void setDelegations(List<DelegationData> delegations) {
        this.delegations = delegations;
    }

    public String getSortedField() {
        return sortedField;
    }

    public void setSortedField(String sortedField) {
        this.sortedField = sortedField;
    }

    public String getSortingOrder() {
        return sortingOrder;
    }

    public void setSortingOrder(String sortingOrder) {
        this.sortingOrder = sortingOrder;
    }

    public Map<String, String> getFilters() {
        return filters;
    }

    public void setFilters(Map<String, String> filters) {
        this.filters = filters;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
}
