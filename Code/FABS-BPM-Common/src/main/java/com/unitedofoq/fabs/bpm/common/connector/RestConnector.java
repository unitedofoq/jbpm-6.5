package com.unitedofoq.fabs.bpm.common.connector;

import com.google.common.io.CharStreams;
import com.unitedofoq.fabs.bpm.common.validator.JBPMRestValidator;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpEntity;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class RestConnector {

    private CloseableHttpClient client;
    private CloseableHttpResponse response;
    private JBPMRestValidator validator;
    private int requestTimeout;
    private RequestConfig.Builder requestConfig;

    public RestConnector(int requestTimeout) {

        validator = new JBPMRestValidator();
        this.requestTimeout = requestTimeout;

        if (requestTimeout != 0) {
            requestConfig = RequestConfig.custom();
            requestConfig.setConnectTimeout(requestTimeout);
            requestConfig.setConnectionRequestTimeout(requestTimeout);
        }

    }

    public JSONObject sendGetRequest(String url,
            Map<String, String> parameters, String userName, String password) {

        JSONObject json = null;
        HttpGet httpGet = null;
        try {
            client = HttpClientBuilder
                    .create()
                    .setDefaultCredentialsProvider(
                            getProvider(userName, password)).build();

            URI uri = buildURI(url, parameters);
            if (uri != null) {
                httpGet = new HttpGet(uri);
                httpGet.setHeader("Accept", "application/json");

                if (requestConfig != null) {

                    httpGet.setConfig(requestConfig.build());
                }

                response = client.execute(httpGet);
                final Integer statusCode = response.getStatusLine().getStatusCode();
                validator.validate(statusCode);
                json = parseResponseToJsonObject(response);
            }

        } catch (ClientProtocolException e) {
            System.out.println(e.getMessage()); //we need to use log4j
        } catch (IOException e) {
            System.out.println(e.getMessage()); //we need to use log4j
        } finally {
            try {
                synchronized (response) {
                    httpGet.releaseConnection();
                    response.close();
                }
            } catch (IOException e) {
                System.out.println(e.getMessage()); //we need to use log4j
            }
        }
        return json;
    }

    private CredentialsProvider getProvider(String userName,
            String password) {
        final CredentialsProvider provider = new BasicCredentialsProvider();
        final UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(
                userName, password);
        provider.setCredentials(AuthScope.ANY, credentials);
        return provider;
    }

    public JSONObject sendPostRequest(String url, Map<String, String> parameters, String userName, String password, String entity) {

        JSONObject json = null;
        try {
            client = HttpClientBuilder
                    .create()
                    .setDefaultCredentialsProvider(
                            getProvider(userName, password)).build();
            URI uri = buildURI(url, parameters);
            if (uri != null) {
                HttpPost httpPost = new HttpPost(uri);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                if (requestConfig != null) {
                    httpPost.setConfig(requestConfig.build());
                }

                if (entity != null) {
                    httpPost.setEntity(buildEntity(entity));
                }
                response = client.execute(httpPost);
                final Integer statusCode = response.getStatusLine().getStatusCode();
                validator.validate(statusCode);
                json = parseResponseToJsonObject(response);
            }
        } catch (ClientProtocolException e) {
            System.out.println(e.getMessage()); //we need to use log4j
        } catch (IOException e) {
            System.out.println(e.getMessage()); //we need to use log4j
        } finally {
            try {
                response.close();
            } catch (IOException e) {
                System.out.println(e.getMessage()); //we need to use log4j
            }
        }
        return json;
    }

    public int sendPutRequest(String url, Map<String, String> parameters, String userName, String password, String entity) {

        JSONObject json = null;
        Integer statusCode = null;
        try {
            client = HttpClientBuilder
                    .create()
                    .setDefaultCredentialsProvider(
                            getProvider(userName, password)).build();
            URI uri = buildURI(url, parameters);
            if (uri != null) {
                HttpPut httpPut = new HttpPut(uri);
                httpPut.setHeader("Accept", "application/xml");
                httpPut.setHeader("Content-type", "application/xml");

                if (requestConfig != null) {
                    httpPut.setConfig(requestConfig.build());
                }

                if (entity != null) {
                    httpPut.setEntity(buildEntity(entity));
                }
                response = client.execute(httpPut);
                statusCode = response.getStatusLine().getStatusCode();
                validator.validate(statusCode);
            }
        } catch (ClientProtocolException e) {
            System.out.println(e.getMessage()); //we need to use log4j
        } catch (IOException e) {
            System.out.println(e.getMessage()); //we need to use log4j
        } finally {
            try {
                response.close();
            } catch (IOException e) {
                System.out.println(e.getMessage()); //we need to use log4j
            }
        }
        return statusCode.intValue();
    }

    public int sendDeleteRequest(String url, Map<String, String> parameters, String userName, String password) {

        JSONObject json = null;
        Integer statusCode = null;
        try {
            client = HttpClientBuilder
                    .create()
                    .setDefaultCredentialsProvider(
                            getProvider(userName, password)).build();
            URI uri = buildURI(url, parameters);
            if (uri != null) {
                HttpDelete httpDelete = new HttpDelete(uri);
                httpDelete.setHeader("Accept", "application/xml");
                httpDelete.setHeader("Content-type", "application/xml");

                if (requestConfig != null) {
                    httpDelete.setConfig(requestConfig.build());
                }

                response = client.execute(httpDelete);
                statusCode = response.getStatusLine().getStatusCode();
                validator.validate(statusCode);
            }
        } catch (ClientProtocolException e) {
            System.out.println(e.getMessage()); //we need to use log4j
        } catch (IOException e) {
            System.out.println(e.getMessage()); //we need to use log4j
        } finally {
            try {
                response.close();
            } catch (IOException e) {
                System.out.println(e.getMessage()); //we need to use log4j
            }
        }
        return statusCode.intValue();
    }

    private URI buildURI(String url, Map<String, String> parameters) {
        URI uri = null;
        URIBuilder builder = null;
        url = url.replace(" ", "%20").replace("[", "%5B").replace("]", "%5D");
        if (url != null) {
            try {
                builder = new URIBuilder(url);
                if (parameters != null) {
                    Iterator<Map.Entry<String, String>> entries = parameters
                            .entrySet().iterator();
                    while (entries.hasNext()) {
                        Map.Entry<String, String> entry = entries.next();
                        builder.addParameter("map_" + entry.getKey(), "\"" + entry.getValue() + "\"");
                    }
                }
                uri = builder.build();
            } catch (URISyntaxException e1) {
                System.out.println(e1.getMessage()); //we need to use log4j
            }
        }
        return uri;

    }

    private JSONObject parseResponseToJsonObject(CloseableHttpResponse response) {
        JSONObject json = null;
        if (response != null) {
            String result = null;
            InputStreamReader reader = null;
            JSONParser parser = new JSONParser();
            try {
                reader = new InputStreamReader(response.getEntity()
                        .getContent(), "UTF-8");
                result = CharStreams.toString(reader);
                if (result.contains("NOT_FOUND")) {
                    return null;
                }
                json = (JSONObject) parser.parse(result);
            } catch (ParseException e) {
                System.out.println(e.getMessage()); //we need to use log4j
                //return just String Object
                try {
                    result = "{\"result\":\"" + result + "\"}";
                    json = (JSONObject) parser.parse(result);
                } catch (ParseException ex) {
                    System.out.println(ex.getMessage());
                }
            } catch (UnsupportedEncodingException e) {
                System.out.println(e.getMessage()); //we need to use log4j
            } catch (UnsupportedOperationException e) {
                System.out.println(e.getMessage()); //we need to use log4j
            } catch (IOException e) {
                System.out.println(e.getMessage()); //we need to use log4j
            }
        }
        return json;
    }

    private HttpEntity buildEntity(String entity) {
        StringEntity stringEntity = null;
        try {
            stringEntity = new StringEntity(entity);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(RestConnector.class.getName()).log(Level.SEVERE, null, ex);
        }

        HttpEntity httpEntity = stringEntity;
        return httpEntity;
    }
}
