/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.notificationhandler;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;

/**
 *
 * @author ianwar
 */
public interface HandlerExecuter {

    public String executeHandler(WorkItem workItem, WorkItemManager manager);

}
