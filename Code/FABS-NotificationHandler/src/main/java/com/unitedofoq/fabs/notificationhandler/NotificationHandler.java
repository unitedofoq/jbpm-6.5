package com.unitedofoq.fabs.notificationhandler;

import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mahmed
 */
public class NotificationHandler implements WorkItemHandler {

    private static final org.apache.logging.log4j.Logger LOG = LogManager.getLogger(NotificationHandler.class);

    HandlerExecuter handlerExecuter;

    @Override
    public void executeWorkItem(WorkItem wi, WorkItemManager wim) {

        LOG.debug("Execute workitem with id:" + wi.getId());

        String result = "";
        Map<String, Object> map = new HashMap();

        handlerExecuter = new Notification();
        result = handlerExecuter.executeHandler(wi, wim);
        map.put("NotificationResult", result);

        handlerExecuter = new MailSender();
        result = handlerExecuter.executeHandler(wi, wim);
        map.put("mailSenderResult", result);

        wim.completeWorkItem(wi.getId(), map);

    }

    @Override
    public void abortWorkItem(WorkItem wi, WorkItemManager wim) {

        LOG.debug("Aborting workitem with id:" + wi.getId());

    }

}
