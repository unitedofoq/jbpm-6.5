package com.unitedofoq.fabs.notificationhandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import org.apache.logging.log4j.LogManager;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mahmed
 */
public class Notification implements HandlerExecuter {

    private static final org.apache.logging.log4j.Logger LOG = LogManager.getLogger(Notification.class);

    @Override
    public String executeHandler(WorkItem workItem, WorkItemManager manager) {

        LOG.debug("Entering");

        String result = "";

        // extract parameters
        String from = (String) workItem.getParameter("From");
        String to = (String) workItem.getParameter("To");
        String message = (String) workItem.getParameter("Message");
        String function = (String) workItem.getParameter("Function");
        String dbid = String.valueOf(workItem.getParameter("dbid"));
        String entityName = (String) workItem.getParameter("entityName");

        try {
            // encode message to escape whitespaces.
            message = URLEncoder.encode(message, "UTF-8");
            String url = "http://localhost:8080/wsdl-locator/wscall?serviceName=addNotification&sender=" + from + "&reciever=" + to + "&description=" + message + "&function=" + function + "&dbid=" + dbid + "&entityName=" + entityName;
            URL urll = new URL(url);
            URLConnection urlConnection = urll.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String output = "";
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                if ((inputLine.contains("Error")) || (inputLine.contains("Exception"))) {
                    LOG.error("Error or exception while reading from input stream");
                    manager.completeWorkItem(workItem.getId(), null);
                    return "";
                }
                output = output + inputLine;
            }
            result = output;
        } catch (MalformedURLException ex) {
            LOG.error("Exception thrown", ex);
            manager.completeWorkItem(workItem.getId(), null);
        } catch (IOException ex) {
            LOG.error("Exception thrown", ex);
            manager.completeWorkItem(workItem.getId(), null);
        }
        return result;
    }

}
