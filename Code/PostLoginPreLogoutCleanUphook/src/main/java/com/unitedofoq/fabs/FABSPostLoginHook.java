package com.unitedofoq.fabs;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

public class FABSPostLoginHook extends Action {

    private static final Logger LOGGER = Logger.getLogger(FABSPostLoginHook.class.getName());
    /* (non-Java-doc)
     * @see com.liferay.portal.kernel.events.Action#Action()
     */

    public FABSPostLoginHook() {
        super();
    }

    /* (non-Java-doc)
     * @see com.liferay.portal.kernel.events.Action#run(HttpServletRequest arg0, HttpServletResponse arg1)
     */
    public void run(HttpServletRequest request, HttpServletResponse response)
            throws ActionException {
        LOGGER.log(Level.INFO, ":: Post Login CleanUp Hook Enter :::");

        // Get LoggedIn user
        Long userId = Long.parseLong(request.getRemoteUser());

        // Clean User Dashboard/Home screen
        LOGGER.log(Level.FINEST, ":: Calling CleanDashboard :::");
        FABSPortalUtil.cleanDashboard(userId);
        LOGGER.log(Level.FINEST, ":: Exist CleanDashboard :::");

        // Clean all user's layouts created by FABS
        LOGGER.log(Level.FINEST, ":: Calling CleanLayouts :::");
        FABSPortalUtil.cleanFABSLayout(userId);
        LOGGER.log(Level.FINEST, ":: Exit CleanLayouts :::");

        //modification
        FABSPortalUtil.deleteExtraRoles(userId);
        //end of modification

        Random rand = new Random();
        String lfusidcValue = "#lfusid" + (rand.nextLong() * 10000000);
        Cookie lfusidc = new Cookie("lfusidc", lfusidcValue);
        lfusidc.setPath("/");
        response.addCookie(lfusidc);
        try {
            User user = UserLocalServiceUtil.getUserById(userId);
            user.setComments(lfusidcValue);
            setUserCommentDB(userId, lfusidcValue);
        } catch (PortalException ex) {
            LOGGER.log(Level.SEVERE, "userId = " + userId, ex);
        } catch (SystemException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.INFO, ":: Post Login CleanUp Hook Exist :::");
    }

    private void setUserCommentDB(long userId, String comment) {
        try {
            DataSource dataSource = ((DataSource) new InitialContext().lookup("jdbc/LiferayPortal"));
            Connection connect = dataSource.getConnection();
            Statement statement = connect.createStatement();
            String deleteSQL = "delete from user_session where userid = " + userId;
            statement.addBatch(deleteSQL);
            String insertSQL = "insert into user_session (userid, session) values (" + userId + ", '" + comment + "')";
            statement.addBatch(insertSQL);
            statement.executeBatch();
            statement.close();
            connect.close();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }
}
