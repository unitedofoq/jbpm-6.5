/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mmasoud
 */
public class FABSPreLogoutAction extends Action {

    private static final Logger LOGGER = Logger.getLogger(FABSPreLogoutAction.class.getName());
    @Override
    public void run(HttpServletRequest request, HttpServletResponse response) throws ActionException {
        LOGGER.log(Level.INFO, ":: Pre Logout Action excution starting :::");
        try {
            Long userId = Long.parseLong(request.getRemoteUser());
            User user = UserLocalServiceUtil.getUserById(userId);
            user.setComments("empty");
            UserLocalServiceUtil.updateUser(user);
            Cookie[] cookies = request.getCookies();
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("lfusidc")) {
                    cookie.setMaxAge(0);
                    cookie.setPath("/");
                    response.addCookie(cookie);
                }
            }
        } catch (SystemException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } catch (PortalException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.INFO, ":: Pre Logout Action excution ending :::");
    }

}
