/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Rehab
 */
@WebFilter("/*")
public class DisableLiferayRedirectFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (!(request instanceof HttpServletRequest)) {
            chain.doFilter(request, response);
            return;
        }
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        
        if(httpRequest.getParameterMap().get("_58_redirect") != null
                && httpRequest.getParameterMap().get("_58_redirect").length != 0
                && !httpRequest.getParameterMap().get("_58_redirect")[0].equals("")){
            httpRequest.getParameterMap().remove("_58_redirect");
        }
        
        if (httpRequest.getParameterMap().get("_3_redirect") != null
                && httpRequest.getParameterMap().get("_3_redirect").length != 0
                && !httpRequest.getParameterMap().get("_3_redirect")[0].equals("")) {
            HttpSession session = httpRequest.getSession();
            if (session != null) {
                session.invalidate();
            }
            httpRequest.getParameterMap().remove("_3_redirect");
        }
        
        if (httpRequest.isRequestedSessionIdFromURL()) {
            HttpSession session = httpRequest.getSession();
            if (session != null) {
                session.invalidate();
            }
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }

}
