package com.unitedofoq.fabs;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Layout;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.service.RoleServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import java.util.Iterator;


import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Utilty Class to have all the helpers functions needed for 
 * the hooks in this project
 */
public class FABSPortalUtil {

    private static final Logger LOGGER = Logger.getLogger(FABSPortalUtil.class
            .getName());

    /**
     * Cleans up all the layouts created by FABS for this User. Leaves only the default private layout.
     * @param userId, the loggedIn user
     */
    public static void cleanFABSLayout(Long userId) {
        try {
            // Get User
            User user = UserLocalServiceUtil.getUserById(userId);
            LOGGER.log(Level.INFO, "LogedIn User: {0}", user.getFullName());

            // Get Default Private Plid
            long defaultPrivetLayoutPLID = user.getGroup()
                    .getDefaultPrivatePlid();
            LOGGER.log(Level.INFO, "Default  Private Plid : {0}", defaultPrivetLayoutPLID);
            // Get the list of private Layouts for the logedIn User
            List<Layout> layouts = LayoutLocalServiceUtil.getLayouts(
                    user.getGroupId(), true);
            for (Layout layout : layouts) {
                // Skip the parent Layout <Dashboard>, and the first layout, which is the welcome page
                if (layout.getPlid() == defaultPrivetLayoutPLID) {
                    LOGGER.log(Level.INFO, "Skip Default Layout {0} {1}", new Object[]{layout.getPlid(), layout.getName()});
                    continue;
                }
                LOGGER.log(Level.INFO, "Deleting Layout {0} {1}", new Object[]{layout.getPlid(), layout.getName()});
                LayoutLocalServiceUtil.deleteLayout(layout, true,
                        new ServiceContext());
            }
        } catch (PortalException e) {
            LOGGER.log(Level.SEVERE, "Exception In Deleting Layouts");
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        } catch (SystemException e) {
            LOGGER.log(Level.SEVERE, "Exception In Deleting Layouts");
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Cleans up all opened portlets in the default private layout/Dashboard/Home Screen.
     * @param userId, the loggedIn user
     */
    public static void cleanDashboard(Long userId) {
        // Check portal property for home page cleanFABSLayout
        boolean doClean = GetterUtil.getBoolean(
                PropsUtil.get("fabs.homepage.clean"), false);
        LOGGER.log(Level.INFO, "fabs.homepage.clean value is : {0} : ", doClean);
        if (doClean) {
            LOGGER.log(Level.INFO, "CleaUp Hook :::>> start cleaning");
            try {
                // Get User Object
                User user = UserLocalServiceUtil.getUserById(userId);
                // get Home Layout Id
                long privatePlid = user.getGroup().getDefaultPrivatePlid();
                LOGGER.log(Level.INFO, "Home Screen Layout Id ::: {0} ::: ", privatePlid);
                // Load default layout
                Layout layout = LayoutLocalServiceUtil.getLayout(privatePlid);
                
                // Clear the opened portlets
                UnicodeProperties properties = layout.getTypeSettingsProperties();
                Iterator ketyIterator = properties.keySet().iterator();
                while (ketyIterator.hasNext()) {
                    String key = String.valueOf(ketyIterator.next());
                    if(key.contains("column")){
                        properties.setProperty(key, "");
                    }
                }
                layout.setTypeSettingsProperties(properties);
                
                // Update Layout
                LayoutLocalServiceUtil.updateLayout(layout);
                LOGGER.log(Level.INFO, "CleaUp Hook :::>> Done cleaning");
            } catch (PortalException e) {
                LOGGER.log(Level.SEVERE, "::: Exception Cleaning Dashboard :::");
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            } catch (SystemException e) {
                LOGGER.log(Level.SEVERE, "::: Exception Cleaning Dashboard :::");
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    //modification
    public static void deleteExtraRoles (Long userId)
    {
        try {
            User user = UserLocalServiceUtil.getUserById(userId);
            List<Role> roles = user.getRoles();
            if(!user.getScreenName().equals("test")) {
                UserLocalServiceUtil.deleteRoleUser(10162, userId);//delete admin role
                //UserLocalServiceUtil.deleteRoleUser(10165,userId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //end of modification

}
