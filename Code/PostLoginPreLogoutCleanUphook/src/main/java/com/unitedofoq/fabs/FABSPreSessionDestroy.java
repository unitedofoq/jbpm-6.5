package com.unitedofoq.fabs;

import com.liferay.portal.kernel.events.SessionAction;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;
import com.liferay.portal.kernel.events.ActionException;

public class FABSPreSessionDestroy extends SessionAction {
	private static final Logger LOGGER = Logger.getLogger(FABSPreSessionDestroy.class.getName());
	/* (non-Java-doc)
	 * @see com.liferay.portal.kernel.events.SessionAction#SessionAction()
	 */
	public FABSPreSessionDestroy() {
		super();
	}

	/* (non-Java-doc)
	 * @see com.liferay.portal.kernel.events.SessionAction#run(HttpSession arg0)
	 */
	public void run(HttpSession session) throws ActionException {
		LOGGER.log(Level.INFO, ":: Entering PreSessionDestroy");
		// Get user
		Long userId = (Long) session.getAttribute("USER_ID");
		LOGGER.log(Level.INFO, ":: UserId {0}", userId);
		
        // Clean User Dashboard/Home screen
        LOGGER.log(Level.FINEST, ":: Calling CleanDashboard :::");
        FABSPortalUtil.cleanDashboard(userId);
        
        LOGGER.log(Level.FINEST, ":: Calling CleanLayouts :::");
		FABSPortalUtil.cleanFABSLayout(userId);
		LOGGER.log(Level.INFO, ":: Exiting PreSessionDestroy");
	}

}
