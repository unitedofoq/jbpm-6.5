package com.unitedofoq.util;

import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import ws.ActiveDirectoryRemote;

public class NavigationUtil
        implements Serializable {
    private static final long serialVersionUID = 1L;
    private Context context;
    private ActiveDirectoryRemote iADRemote;
    private User lfUser;
    private static final Logger logger = Logger.getLogger(NavigationUtil.class.getName());

    public User getLfUser() {
        return this.lfUser;
    }

    public void setLfUser(User lfUser) {
        this.lfUser = lfUser;
    }

    private Object loadEJBClass(String fClassName) {

        
        try {
            logger.log(Level.INFO, "Loading EJB Class", fClassName);
            Object retObj = this.context.lookup("java:global/ofoq/" + fClassName);
            logger.log(Level.INFO, "Finished Loading EJB Class", fClassName);
            return retObj;
        } catch (NamingException ex) {
            logger.log(Level.SEVERE, "Exception thrown", ex);
        }
        return null;
    }

    public String init(Long lfUsrId) {
        try {
            logger.log(Level.INFO, "Initialize Context");
            this.context = new InitialContext();
            this.iADRemote = ((ActiveDirectoryRemote) loadEJBClass(ActiveDirectoryRemote.class.getName()));
            this.lfUser = UserLocalServiceUtil.getUser(lfUsrId.longValue());
            logger.log(Level.FINE, ":::Liferay User ScreenName :::" + this.lfUser.getScreenName());
            String UserName = this.lfUser.getScreenName();
            String tenant = this.iADRemote.getTenant(UserName);
            logger.log(Level.INFO, "Initialize Context" + tenant);
            return tenant;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception Loading EJb Classes", ex);
            logger.log(Level.SEVERE, "Exit !!");
        }
        return "Exit";
    }
}

