package com.unitedofoq.fabs;

import com.liferay.portal.kernel.events.Action;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;
import java.util.List;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.model.Layout;
import com.liferay.portal.model.LayoutTypePortlet;
import com.liferay.portal.model.Portlet;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.struts.LastPath;
import com.liferay.portal.kernel.util.StringPool;
import javax.servlet.http.HttpSession;
import java.util.Map;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;


public class PostLogin extends Action {

    public void run(HttpServletRequest request, HttpServletResponse res) {
		redirectUserToPrivatePage(request);
		removeOpenedPortletsForLoggedUser(request);       
    }
	
	public void redirectUserToPrivatePage(HttpServletRequest request){
		HttpSession session = request.getSession();
		String userId = request.getRemoteUser();
		try{
			User user = UserLocalServiceUtil
						.getUserById(Long.parseLong(userId));
			String username = user.getScreenName();
			LastPath privatelastPath = new LastPath(StringPool.BLANK, "/user/"+username);
			session.setAttribute(WebKeys.LAST_PATH, privatelastPath);
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		}
	}
	
	public void removeOpenedPortletsForLoggedUser(HttpServletRequest request){
	    User user = null;		
		try{
		    user = UserLocalServiceUtil
						.getUserById(Long.parseLong(request.getRemoteUser()));
			System.out.println("userId: " + request.getRemoteUser());
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		}
		          
        try {
		long layoutDBID = user.getGroup().getDefaultPrivatePlid(); 
		System.out.println("layoutDBID: " + layoutDBID);
		Layout layout = LayoutLocalServiceUtil.getLayout(layoutDBID);
		System.out.println("layout: " + layout);
		LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
		List<String> portletsId = layoutTypePortlet.getPortletIds();
		System.out.println("portletsId: " + portletsId);
		for (String portletID : portletsId) {
			System.out.println("portletID: " + portletID);
            layoutTypePortlet.removePortletId(Long.parseLong(request.getRemoteUser()), portletID);
            LayoutLocalServiceUtil.updateLayout(layout.getGroupId(), layout.isPrivateLayout(), layout.getLayoutId(), layout.getTypeSettings());
        } 
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    cookie.setMaxAge(0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	  


}