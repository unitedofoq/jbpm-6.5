package com.unitedofoq.fabs.auth;


//import com.liferay.portal.liveusers.LiveUsers;
import com.liferay.portal.UserLockoutException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserTracker;
import com.liferay.portal.model.UserTrackerPath;
import com.liferay.portal.model.UserTrackerWrapper;
import com.liferay.portal.security.auth.AuthException;
import com.liferay.portal.security.auth.AuthFailure;
import com.liferay.portal.security.auth.Authenticator;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.UserTrackerLocalServiceUtil;
import com.unitedofoq.fabs.FABSUtil;
import com.unitedofoq.fabs.SessionCountException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Hamada
 *         <p>
 *         FABS Custom authenticator, to limit user to only one active session.
 */
public class FABSOneUserOnSessionAuthenticator implements Authenticator {

    @Override
    public int authenticateByEmailAddress(long companyId, String emailAddress,
                                          String password, Map<String, String[]> headerMap,
                                          Map<String, String[]> parameterMap) throws AuthException {
        logger.log(Level.INFO, "::: FABSOneUserOnSessionAuthenticator.authenticateByEmailAddress");
        User user = null;
        try {
            user = UserLocalServiceUtil.getUserByEmailAddress(companyId, emailAddress);
            if (user == null) {
                return DNE;
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE,
                    "::: Exception in FABSOneUserOnSessionAuthenticator.authenticateUserByUserId", e);
            return DNE;
        }
        checkUserLiveSession(user.getUserId(), companyId);
        return SUCCESS;
    }

    @Override
    public int authenticateByScreenName(long companyId, String screenName,
                                        String password, Map<String, String[]> headerMap,
                                        Map<String, String[]> parameterMap) throws AuthException {

        logger.log(Level.INFO,
                "::: FABSOneUserOnSessionAuthenticator.authenticateByEmailAddress");
        User user = null;
        try {
            user = UserLocalServiceUtil.getUserByScreenName(companyId, screenName);
            if (user == null)
                return DNE;
        } catch (Exception e) {
            logger.log(Level.SEVERE,
                    "::: Exception in FABSOneUserOnSessionAuthenticator.authenticateUserByUserId", e);
            return DNE;
        }

        checkUserLiveSession(user.getUserId(), companyId);
        return SUCCESS;
    }

    @Override
    public int authenticateByUserId(long companyId, long userId,
                                    String password, Map<String, String[]> headerMap,
                                    Map<String, String[]> parameterMap) throws AuthException {

        logger.log(Level.INFO, "::: FABSOneUserOnSessionAuthenticator.authenticateByEmailAddress");

        checkUserLiveSession(userId, companyId);

        return SUCCESS;
    }

    private void checkUserLiveSession(long userId, long companyId) throws AuthException {

        logger.log(Level.INFO, "checkUserLiveSession" + userId);

        if(FABSUtil.isUserLoggedInToAnotherSession(userId))
            throw new SessionCountException("User Already Have an Active Session, Please logout and try again");

        try { // use to debug
            UserTrackerLocalServiceUtil.getUserTrackersCount();
        } catch (SystemException e) {
            e.printStackTrace();
        }


//        StatusLocalServiceUtil.getUserStatus(userId);
//        use the liveUsers to retreive the open sessions
//        Map<String, UserTracker> sessionUsers = LiveUsers.getSessionUsers(
//                companyId);
//
//        List<UserTracker> userTrackers = new ArrayList<UserTracker>(
//                sessionUsers.values());
//
//        for (UserTracker userTracker : userTrackers) {
//            if (userId != userTracker.getUserId()) {
//                continue;
//            }
//            // Throw Authentication Exception
//            throw new AuthException("User Already Have an Active Session, Please logout and try again");
//        }
    }


    private static Logger logger = Logger.getLogger(FABSOneUserOnSessionAuthenticator.class.getName());

}