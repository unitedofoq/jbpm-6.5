package com.unitedofoq.fabs;

import com.liferay.portal.kernel.cache.PortalCache;
import com.liferay.portal.kernel.cache.SingleVMPoolUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.UserTracker;
import com.liferay.portal.model.UserTrackerPath;
import com.liferay.portal.service.UserTrackerLocalServiceUtil;
import com.unitedofoq.fabs.hooks.FABSOneSessionPostLoginAction;

import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Hamada
 *
 */
public class FABSUtil {

    private static final Logger LOGGER = Logger.getLogger(FABSOneSessionPostLoginAction.class.getName());

    private static final PortalCache portalCache = SingleVMPoolUtil.getCache("sessionCache");

    /*
    * Add The newly created session for user to cache
    * */
    public static void add(long userId)
    {
        portalCache.put(String.valueOf(userId), String.valueOf(userId));
    }


    /*
    * remove user session from cache
    * */
    public static void remove(long userId)
    {
        portalCache.remove(String.valueOf(userId));
    }


    /*
    * validate that the user has no session active, to grant him access to the system
    * */
    public static boolean isUserLoggedInToAnotherSession(long userId){
        return portalCache.get(String.valueOf(userId)) != null;
    }

    /*
    * Add User Tracker for the newly created session, tobe used later in validating the new login attempts.
    * */
    public static void addUserTracker(long companyId, long userId, String sessionId){
        try {
            UserTrackerLocalServiceUtil.addUserTracker(companyId, userId, new Date(), sessionId, "",
                    "", "", new ArrayList<UserTrackerPath>());
        } catch (SystemException ex) {
            LOGGER.log(Level.SEVERE, "Exception adding userTracker", ex);
        }
    }

    /*
    * Return userTracker record for the loggedIn user
    * */
    public static UserTracker getUserTracker(long userId){
        UserTracker userTracker = null;
        try {
            userTracker = UserTrackerLocalServiceUtil.getUserTracker(userId);
            return userTracker;
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, "Exception getting userTracker Info", ex);
        }
        return userTracker;
    }

}
