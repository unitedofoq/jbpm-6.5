package com.unitedofoq.fabs.hooks;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.struts.LastPath;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.unitedofoq.fabs.FABSUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Hamada
 *
 */
public class FABSOneSessionPostLoginAction extends Action {


	private static final Log logger = LogFactoryUtil.getLog(FABSOneSessionPostLoginAction.class.getName());

	public void run(HttpServletRequest request, HttpServletResponse response)
			throws ActionException
	{
		logger.debug("::: Post Login OneUserOneSession Hook :::");

		long userId = PortalUtil.getUserId(request);
		FABSUtil.add(userId);

		logger.debug("::: END Post Login OneUserOneSession Hook :::");

		try { // Redirect user to his own homepage
			String screenName = UserLocalServiceUtil.getUser(userId).getScreenName();
			String path = "/user/" + screenName + "/home";
			LastPath lastPath = new LastPath(StringPool.BLANK, path);
            request.getSession().setAttribute(WebKeys.LAST_PATH, lastPath);
		} catch (Exception e) {
			logger.debug("Exception while redirect the user to his home page", e);
		}

	}

}
