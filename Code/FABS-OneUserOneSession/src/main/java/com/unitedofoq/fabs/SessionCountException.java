package com.unitedofoq.fabs;

import com.liferay.portal.security.auth.AuthException;

/**
 * Created by Hamada.
 */
public class SessionCountException extends AuthException {

    public SessionCountException() {}

    public SessionCountException(String msg)
    {
        super(msg);
    }

    public SessionCountException(String msg, Throwable cause)
    {
        super(msg, cause);
    }

    public SessionCountException(Throwable cause)
    {
        super(cause);
    }

}
