package com.unitedofoq.fabs.hooks;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.unitedofoq.fabs.FABSUtil;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.util.PortalUtil;

public class PreLogoutAction
  extends Action
{
  private static final Log logger = LogFactoryUtil.getLog(PreLogoutAction.class);

  public void run(HttpServletRequest request, HttpServletResponse response)
    throws ActionException
  {
    logger.debug("Start post logout action");

    long userId = PortalUtil.getUserId(request);
    // remove user session
    FABSUtil.remove(userId);

    logger.debug("End post logout action");
  }
}
