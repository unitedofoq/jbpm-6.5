/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.bpm.converter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author mmasoud
 */
public class Bpmn2Converter {

    /**
     * @param args the command line arguments
     */
    static NotificationConverter nc = new NotificationConverter();

    public static void main(String[] args) {
        // TODO code application logic here
        File oldProcessesDirectory = new File(args[0]);
        File newProcessesDirectory = new File(args[1]);
        if (oldProcessesDirectory.isDirectory() && newProcessesDirectory.isDirectory()) {
            File[] oldProcessesFiles = oldProcessesDirectory.listFiles();
            File[] newProcessesFiles = newProcessesDirectory.listFiles();
            if (oldProcessesFiles != null && newProcessesFiles != null) {
                int counter = 0;
                int errors = 0;
                int done = 0;
                boolean flag = false;
                for (File newFile : newProcessesFiles) {
                    if (newFile.isFile() && newFile.getPath().endsWith(".bpmn2")) {
                        String[] newFilePathParts = newFile.getPath().split("\\/");
                        String newFileName = newFilePathParts[newFilePathParts.length - 1];
                        for (File oldFile : oldProcessesFiles) {
                            if (oldFile.isFile() && oldFile.getPath().endsWith(".bpmn2")) {
                                String[] oldFilePathParts = oldFile.getPath().split("\\/");
                                String oldFileName = oldFilePathParts[oldFilePathParts.length - 1];
                                if (oldFileName.equals(newFileName)) {
                                    try {
                                        Queue<String> probabilities = getProbabilitiesList(oldFile);
                                        File tempFile = convertInitiator(newFile, newProcessesDirectory.getPath());
                                        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                                        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
                                        Document doc = docBuilder.parse(new FileInputStream(tempFile));
                                        adjustProbability(doc, probabilities);
                                        convertNotifications(doc);
                                        addNewProcessVariablesWithAssignments(doc);
                                        TransformerFactory transformerFactory = TransformerFactory.newInstance();
                                        Transformer transformer = transformerFactory.newTransformer();
                                        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                                        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
                                        DOMSource source = new DOMSource(doc);
                                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                        StreamResult result = new StreamResult(byteArrayOutputStream);
                                        transformer.transform(source, result);
                                        byte[] data = byteArrayOutputStream.toByteArray();
                                        File finalFile = new File(newFile.getPath());
                                        FileOutputStream fileOutputStream = new FileOutputStream(finalFile);
                                        fileOutputStream.write(data);
                                        PrintWriter out = new PrintWriter(fileOutputStream);
                                        out.flush();
                                        out.close();
                                        fileOutputStream.close();
                                        tempFile.delete();
                                    } catch (ParserConfigurationException | SAXException | IOException | TransformerException ex) {
                                        Logger.getLogger(Bpmn2Converter.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    counter++;
                                    flag = true;
                                    break;

                                }
                            }
                        }
                        if (flag) {
                            System.out.println("done: file" + counter + " : " + newFileName);
                            done++;
                        } else {
                            System.out.println("error: file" + counter + " : " + newFileName);
                            errors++;
                        }
                    }
                }
                System.out.println(done + " Files are converted successfully");
                System.out.println(errors + " errors");
            } else {
                System.out.println("\"Old or New or both\" Processes Directory are empty Directories");
            }
        } else {
            System.out.println("\"Old or New or both\" Processes Directory are not Directories");
        }
    }

    private static File convertInitiator(File file, String newDirPath) {
        File newFile = null;
        try {
            newFile = new File(newDirPath + "/tempFile");
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(newFile));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.replace("\n", "");
                if (line.contains("initiator")) {
                    String newLine = line.replace("initiator", "Initiator");
                    bufferedWriter.write(newLine + "\n");
                } else {
                    bufferedWriter.write(line + "\n");
                }
            }

            bufferedReader.close();
            file.delete();
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(Bpmn2Converter.class.getName()).log(Level.SEVERE, null, ex);
        }

        return newFile;
    }

    private static void adjustProbability(Document doc, Queue<String> probabilities) {
        Queue<Node> probabilitesNodeList = getNewProbabilitiesNodeList(doc);
        setProbabilities(probabilitesNodeList, probabilities);
    }

    private static Queue<String> getProbabilitiesList(File file) {
        Queue<String> probabilities = null;
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new FileInputStream(file));
            Queue<String> arrowsIDs = getArrowsIDs(doc, "bpmnElement");
            probabilities = getProbabilitiesByArrowsIDs(arrowsIDs, doc);
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(Bpmn2Converter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return probabilities;
    }

    private static Queue<String> getArrowsIDs(Document doc, String attributeName) {
        String path = "//*[local-name()='BPMNEdge']/@attrName".replace("attrName", attributeName);
        NodeList attributes = (NodeList) parseXPath(doc, path, XPathConstants.NODESET);
        Queue<String> arrowsIDs = new LinkedList<>();
        for (int i = 0; i < attributes.getLength(); i++) {
            Node attribute = attributes.item(i);
            if (attribute != null) {
                arrowsIDs.add(attribute.getTextContent());
            }
        }
        return arrowsIDs;
    }

    private static Queue<String> getProbabilitiesByArrowsIDs(Queue<String> arrowsIDs, Document doc) {
        String path = "//*[local-name()='ElementParameters'][@elementId='temp']//*[local-name()='FloatingParameter']/@value";
        Queue<String> probabilities = new LinkedList<>();
        String temp = null;
        while (!arrowsIDs.isEmpty()) {
            temp = arrowsIDs.poll();
            String tempPath = path.replace("temp", temp);
            Node node = (Node) parseXPath(doc, tempPath, XPathConstants.NODE);
            probabilities.add(node.getTextContent());
        }
        return probabilities;
    }

    private static Queue<Node> getNewProbabilitiesNodeList(Document doc) {
        Queue<Node> probabilities = null;
        Queue<String> arrowsIDs = getArrowsIDs(doc, "bpmnElement");
        probabilities = getProbabilitiesNodeListByArrowsIDs(arrowsIDs, doc);
        return probabilities;
    }

    private static Queue<Node> getProbabilitiesNodeListByArrowsIDs(Queue<String> arrowsIDs, Document doc) {
        String path = "//*[local-name()='ElementParameters'][@elementRef='temp']//*[local-name()='FloatingParameter']/@value";
        Queue<Node> probabilitiesNodeList = new LinkedList<>();
        String temp = null;
        while (!arrowsIDs.isEmpty()) {
            temp = arrowsIDs.poll();
            String tempPath = path.replace("temp", temp);
            Node node = (Node) parseXPath(doc, tempPath, XPathConstants.NODE);
            probabilitiesNodeList.add(node);
        }
        return probabilitiesNodeList;
    }

    private static void setProbabilities(Queue<Node> probabilitiesNodeList, Queue<String> probabilities) {
        while (!probabilitiesNodeList.isEmpty()) {
            Node node = probabilitiesNodeList.poll();
            node.setTextContent(probabilities.poll());
        }
    }

    private static void deleteGroupFromTask(Document doc) {
        NodeList nodeList = doc.getElementsByTagName("bpmn2:dataInputAssociation");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            NodeList secondLevelNodeList = node.getChildNodes();
            for (int j = 0; j < secondLevelNodeList.getLength(); j++) {
                Node secondLevelNode = secondLevelNodeList.item(j);
                if (secondLevelNode.getNodeName().equals("bpmn2:assignment")) {
                    NodeList thirdLevelNodeList = secondLevelNode.getChildNodes();
                    for (int k = 0; k < thirdLevelNodeList.getLength(); k++) {
                        Node thirdLevelNode = thirdLevelNodeList.item(k);
                        if (thirdLevelNode.getNodeName().equals("bpmn2:to")) {
                            if (thirdLevelNode.getTextContent().endsWith("GroupIdInputX")) {
                                node.getParentNode().removeChild(node);
                            }
                        }
                    }
                }
            }
        }
    }

    private static void convertNotifications(Document doc) {
        NodeList oldNotificationNodes = getAllOldNotificationNodes(doc);
        Map<String, Object> notificationsData = getDataFromOldNotification(doc);
        for (int i = 0; i < oldNotificationNodes.getLength(); i++) {
            Map<String, Object> notificationData = new HashMap<>();
            Map<String, Object> notificationParams = new HashMap<>();
            Node oldNotification = oldNotificationNodes.item(i);
            for (Entry entry : notificationsData.entrySet()) {
                String key = (String) entry.getKey();
                if (key.equals("params")) {
                    for (Entry innerEntry : ((Map<String, Object>) entry.getValue()).entrySet()) {
                        Object innerValue = innerEntry.getValue();
                        String newValue = null;
                        if (innerValue instanceof NodeList) {
                            newValue = ((NodeList) innerValue).item(i).getTextContent();
                        } else {
                            newValue = (String) innerValue;
                        }
                        notificationParams.put((String) innerEntry.getKey(), newValue);
                    }
                } else {
                    notificationData.put(key, ((NodeList) entry.getValue()).item(i).getTextContent());
                }
            }
            notificationData.put("params", notificationParams);
            Node newNotification = buildNewNotification(doc, notificationData);
            oldNotification.getParentNode().replaceChild(newNotification, oldNotification);
            Node parentNode = doc.getFirstChild();
            NodeList itemDefenitions = doc.getElementsByTagName("bpmn2:itemDefinition");
            String id = (String) notificationData.get("id");
            deleteOldItemDefenitions(parentNode, itemDefenitions, id);
            for (Entry notificationParam : notificationParams.entrySet()) {
                appendNewItemDefenition(doc, id, "InputXItem", "afterXItem", (String) notificationParam.getKey());
            }
        }
    }

    private static NodeList getAllOldNotificationNodes(Document doc) {
        String path = "//*[local-name()='userTask']/*[local-name()='dataInputAssociation']//*[local-name()='targetRef'][contains(.,'isNotificationInputX')]/../*[local-name()='sourceRef'][text() = 'yes']/../..";
        NodeList notificationNodes = (NodeList) parseXPath(doc, path, XPathConstants.NODESET);
        return notificationNodes;
    }

    private static Object parseXPath(Object root, String xPath, QName returnType) {
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        XPathExpression expr = null;
        try {
            expr = xpath.compile(xPath);
            return expr.evaluate(root, returnType);
        } catch (XPathExpressionException ex) {
            Logger.getLogger(Bpmn2Converter.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    private static Map<String, Object> getDataFromOldNotification(Document doc) {
        Map<String, Object> notificationData = new HashMap<>();
        Map<String, Object> notificationParams = new HashMap<>();
        String xPath = "//*[local-name()='userTask']/*[local-name()='dataInputAssociation']//*[local-name()='targetRef'][contains(.,'isNotificationInputX')]/../*[local-name()='sourceRef'][text() = 'yes']/../../@id";
        NodeList inputs = (NodeList) parseXPath(doc, xPath, XPathConstants.NODESET);
        notificationData.put("id", inputs);
        xPath = "//*[local-name()='userTask']/*[local-name()='dataInputAssociation']//*[local-name()='targetRef'][contains(.,'isNotificationInputX')]/../*[local-name()='sourceRef'][text() = 'yes']/../../*[local-name()='incoming']";
        inputs = (NodeList) parseXPath(doc, xPath, XPathConstants.NODESET);
        notificationData.put("incoming", inputs);
        xPath = "//*[local-name()='userTask']/*[local-name()='dataInputAssociation']//*[local-name()='targetRef'][contains(.,'isNotificationInputX')]/../*[local-name()='sourceRef'][text() = 'yes']/../../*[local-name()='outgoing']";
        inputs = (NodeList) parseXPath(doc, xPath, XPathConstants.NODESET);
        notificationData.put("outgoing", inputs);
        xPath = "//*[local-name()='userTask']/*[local-name()='dataInputAssociation']//*[local-name()='targetRef'][contains(.,'isNotificationInputX')]/../*[local-name()='sourceRef'][text() = 'yes']/../../*[local-name()='dataInputAssociation']/*[local-name()='targetRef'][contains(.,'TaskNameInputX')]/..//*[local-name()='from']";
        NodeList params = (NodeList) parseXPath(doc, xPath, XPathConstants.NODESET);
        notificationParams.put("Message", params);
        xPath = "//*[local-name()='userTask']/*[local-name()='dataInputAssociation']//*[local-name()='targetRef'][contains(.,'isNotificationInputX')]/../*[local-name()='sourceRef'][text() = 'yes']/../../*[local-name()='dataInputAssociation']/*[local-name()='targetRef'][contains(.,'fromInputX')]/../*[local-name()='sourceRef']";
        params = (NodeList) parseXPath(doc, xPath, XPathConstants.NODESET);
        notificationParams.put("From", params);
        xPath = "//*[local-name()='userTask']/*[local-name()='dataInputAssociation']//*[local-name()='targetRef'][contains(.,'isNotificationInputX')]/../*[local-name()='sourceRef'][text() = 'yes']/../../*[local-name()='potentialOwner']//*[local-name()='formalExpression']";
        params = (NodeList) parseXPath(doc, xPath, XPathConstants.NODESET);
        notificationParams.put("To", params);
        xPath = "//*[local-name()='userTask']/*[local-name()='dataInputAssociation']//*[local-name()='targetRef'][contains(.,'isNotificationInputX')]/../*[local-name()='sourceRef'][text() = 'yes']/../../*[local-name()='dataInputAssociation']/*[local-name()='targetRef'][contains(.,'functionInputX')]/../*[local-name()='assignment']/*[local-name()='from']";
        params = (NodeList) parseXPath(doc, xPath, XPathConstants.NODESET);
        notificationParams.put("Function", params);
        notificationParams.put("dbid", "dbid");
        notificationParams.put("entityName", "entityName");
        notificationParams.put("TaskName", "Notification");
        notificationData.put("params", notificationParams);
        return notificationData;
    }

    private static void deleteOldItemDefenitions(Node parent, NodeList itemDefenitions, String id) {
        for (int i = 0; i < itemDefenitions.getLength(); i++) {
            Node itemDefenition = itemDefenitions.item(i);
            if (itemDefenition.getAttributes().getNamedItem("id").getTextContent().contains(id)) {
                parent.removeChild(itemDefenition);
            }
        }
    }

    private static Node buildNewNotification(Document doc, Map<String, Object> inputs) {
        String text = null;
        String attrText = null;
        Map<String, String> attrs = null;
        String id = (String) inputs.get("id");
        String incoming = (String) inputs.get("incoming");
        String outgoing = (String) inputs.get("outgoing");
        Map<String, String> notificationParams = (Map<String, String>) inputs.get("params");
        attrs = new HashMap<>();
        attrs.put("id", id);
        attrs.put("drools:selectable", "true");
        attrs.put("drools:taskName", "Notification");
        attrs.put("color:background-color", "#fafad2");
        attrs.put("color:border-color", "#000000");
        attrs.put("color:color", "#000000");
        attrs.put("name", "Notification");
        Node notificationNode = nc.createNewNode("bpmn2:task", null, attrs, doc);
        Node firstLevelChildNode = nc.createNewNode("bpmn2:extensionElements", null, null, doc);
        attrs = new HashMap<>();
        attrs.put("name", "elementname");
        Node secondLevelChildNode = nc.createNewNode("drools:metaData", null, attrs, doc);
        Node thirdLevelChildNode = nc.createNewNode("drools:metaValue", null, null, doc);
        Node cdataNode = doc.createCDATASection("Notification");
        thirdLevelChildNode.appendChild(cdataNode);
        secondLevelChildNode.appendChild(thirdLevelChildNode);
        firstLevelChildNode.appendChild(secondLevelChildNode);
        notificationNode.appendChild(firstLevelChildNode);
        firstLevelChildNode = nc.createNewNode("bpmn2:incoming", incoming, null, doc);
        notificationNode.appendChild(firstLevelChildNode);
        firstLevelChildNode = nc.createNewNode("bpmn2:outgoing", outgoing, null, doc);
        notificationNode.appendChild(firstLevelChildNode);
        attrs = new HashMap<>();
        attrs.put("id", null);
        firstLevelChildNode = nc.createNewNode("bpmn2:ioSpecification", null, attrs, doc);
        for (Entry param : notificationParams.entrySet()) {
            attrs = new HashMap<>();
            text = new StringBuilder(id)
                    .append("_").append(param.getKey()).append("InputX").toString();
            attrs.put("id", text);
            attrs.put("drools:dtype", "String");
            text = new StringBuilder("_")
                    .append(id).append("_").append(param.getKey())
                    .append("InputXItem").toString();
            attrs.put("itemSubjectRef", text);
            attrs.put("name", (String) param.getKey());
            secondLevelChildNode = nc.createNewNode("bpmn2:dataInput", null, attrs, doc);
            firstLevelChildNode.appendChild(secondLevelChildNode);
        }
        attrs = new HashMap<>();
        attrs.put("id", null);
        secondLevelChildNode = nc.createNewNode("bpmn2:inputSet", null, attrs, doc);
        for (Entry param : notificationParams.entrySet()) {
            text = new StringBuilder(id).append("_")
                    .append(param.getKey()).append("InputX").toString();
            thirdLevelChildNode = nc.createNewNode("bpmn2:dataInputRefs", text, null, doc);
            secondLevelChildNode.appendChild(thirdLevelChildNode);
        }
        firstLevelChildNode.appendChild(secondLevelChildNode);
        attrs = new HashMap<>();
        attrs.put("id", null);
        secondLevelChildNode = nc.createNewNode("bpmn2:outputSet", null, attrs, doc);
        firstLevelChildNode.appendChild(secondLevelChildNode);
        notificationNode.appendChild(firstLevelChildNode);
        for (Entry param : notificationParams.entrySet()) {
            boolean isToStaticParam = true;
            String paramId = (String) param.getKey();
            String paramValue = (String) param.getValue();
            attrs = new HashMap<>();
            attrs.put("id", null);
            firstLevelChildNode = nc.createNewNode("bpmn2:dataInputAssociation", null, attrs, doc);
            if (!paramId.equals("Message")&& !paramId.equals("Function")) {
                if (paramValue.contains("#{")) {
                    paramValue = paramValue.substring(paramValue.indexOf("{") + 1, paramValue.indexOf("}"));
                }
                isToStaticParam = false;
            }
            if (paramId.equals("TaskName") || paramId.equals("Message") || isToStaticParam) {
                text = new StringBuilder(id).append("_")
                        .append(paramId).append("InputX").toString();
                secondLevelChildNode = nc.createNewNode("bpmn2:targetRef", text, null, doc);
                firstLevelChildNode.appendChild(secondLevelChildNode);
                attrs = new HashMap<>();
                attrs.put("id", null);
                secondLevelChildNode = nc.createNewNode("bpmn2:assignment", null, attrs, doc);
                attrs = new HashMap<>();
                attrs.put("xsi:type", "bpmn2:tFormalExpression");
                attrs.put("id", null);
                thirdLevelChildNode = nc.createNewNode("bpmn2:from", null, attrs, doc);
                cdataNode = doc.createCDATASection(paramValue);
                thirdLevelChildNode.appendChild(cdataNode);
                secondLevelChildNode.appendChild(thirdLevelChildNode);
                text = new StringBuilder(id).append("_")
                        .append(paramId).append("InputX").toString();
                attrs = new HashMap<>();
                attrs.put("xsi:type", "bpmn2:tFormalExpression");
                attrs.put("id", null);
                thirdLevelChildNode = nc.createNewNode("bpmn2:to", text, attrs, doc);
                secondLevelChildNode.appendChild(thirdLevelChildNode);
                firstLevelChildNode.appendChild(secondLevelChildNode);
            } else {
                secondLevelChildNode = nc.createNewNode("bpmn2:sourceRef", paramValue, null, doc);
                firstLevelChildNode.appendChild(secondLevelChildNode);
                text = new StringBuilder(id).append("_")
                        .append(paramId).append("InputX").toString();
                secondLevelChildNode = nc.createNewNode("bpmn2:targetRef", text, null, doc);
                firstLevelChildNode.appendChild(secondLevelChildNode);
            }
            notificationNode.appendChild(firstLevelChildNode);
        }
        return notificationNode;
    }

    private static void addNewProcessVariablesWithAssignments(Document doc) {
        Map<String, Object> variables = new HashMap<>();
        Map<String, String> assignments = new HashMap<>();
        assignments.put("actorDisplayName", "input");
        variables.put("InitiatorDisplayName", assignments);
        assignments = new HashMap<>();
        assignments.put("fromDisplayName", "input");
        assignments.put("completedByDisplayName", "output");
        variables.put("lastUserDisplayName", assignments);
        for (Entry variable : variables.entrySet()) {
            appendNewItemDefenition(doc, null, "Item", "beforeXItem", (String) variable.getKey());
            appendNewProcessProperty(doc, (String) variable.getKey());
            appendNewTaskAssignments(doc, variable);
        }
        appendTaskActionsAssignment(doc);
    }

    private static void appendTaskActionsAssignment(Document doc) {
        NodeList userTasks = doc.getElementsByTagName("bpmn2:userTask");
        for (int i = 0; i < userTasks.getLength(); i++) {
            Node userTask = userTasks.item(i);
            String path = "./@id";
            Node taskId = (Node) parseXPath(userTask, path, XPathConstants.NODE);
            path = "./*[local-name()='ioSpecification']";
            Node ioSpecificationNode = (Node) parseXPath(userTask, path, XPathConstants.NODE);
            appendNewItemDefenition(doc, taskId.getTextContent(), "InputXItem", "after" + taskId.getTextContent(), "taskActions");
            Node dataInput = doc.createElement("bpmn2:dataInput");
            Attr attribute = doc.createAttribute("id");
            attribute.setTextContent(new StringBuilder(taskId.getTextContent())
                    .append("_").append("taskActions").append("InputX").toString());
            ((Element) dataInput).setAttributeNode(attribute);
            attribute = doc.createAttribute("drools:dtype");
            attribute.setTextContent("String");
            ((Element) dataInput).setAttributeNode(attribute);
            attribute = doc.createAttribute("itemSubjectRef");
            attribute.setTextContent(new StringBuilder("_")
                    .append(taskId.getTextContent()).append("_").append("taskActions")
                    .append("InputXItem").toString());
            ((Element) dataInput).setAttributeNode(attribute);
            attribute = doc.createAttribute("name");
            attribute.setTextContent("taskActions");
            ((Element) dataInput).setAttributeNode(attribute);
            path = "./*[local-name()='dataInput'][last()]";
            Node lastResult = (Node) parseXPath(ioSpecificationNode, path, XPathConstants.NODE);
            if (lastResult.getNextSibling() != null) {
                ioSpecificationNode.insertBefore(dataInput, lastResult.getNextSibling());
            } else {
                ioSpecificationNode.appendChild(dataInput);
            }
            Node dataInputRefs = doc.createElement("bpmn2:dataInputRefs");
            dataInputRefs.setTextContent(new StringBuilder(taskId.getTextContent()).append("_")
                    .append("taskActions").append("InputX").toString());
            path = "./*[local-name()='inputSet']";
            Node inputSet = (Node) parseXPath(ioSpecificationNode, path, XPathConstants.NODE);
            path = "./*[local-name()='dataInputRefs'][last()]";
            lastResult = (Node) parseXPath(inputSet, path, XPathConstants.NODE);
            if (lastResult.getNextSibling() != null) {
                inputSet.insertBefore(dataInputRefs, lastResult.getNextSibling());
            } else {
                inputSet.appendChild(dataInputRefs);
            }
            Node dataInputAssociation = doc.createElement("bpmn2:dataInputAssociation");
            attribute = doc.createAttribute("id");
            ((Element) dataInputAssociation).setAttributeNode(attribute);
            Node targetRef = doc.createElement("bpmn2:targetRef");
            targetRef.setTextContent(new StringBuilder(taskId.getTextContent()).append("_")
                    .append("taskActions").append("InputX").toString());
            dataInputAssociation.appendChild(targetRef);
            Node assignment = doc.createElement("bpmn2:assignment");
            attribute = doc.createAttribute("id");
            ((Element) assignment).setAttributeNode(attribute);
            Node from = doc.createElement("bpmn2:from");
            ((Element) from).setAttribute("xsi:type", "bpmn2:tFormalExpression");
            attribute = doc.createAttribute("id");
            ((Element) from).setAttributeNode(attribute);
            Node cdataNode = doc.createCDATASection("Approve_Reject");
            from.appendChild(cdataNode);
            assignment.appendChild(from);
            Node to = doc.createElement("bpmn2:to");
            ((Element) to).setAttribute("xsi:type", "bpmn2:tFormalExpression");
            attribute = doc.createAttribute("id");
            ((Element) to).setAttributeNode(attribute);
            to.setTextContent(new StringBuilder(taskId.getTextContent()).append("_")
                    .append("taskActions").append("InputX").toString());
            assignment.appendChild(to);
            dataInputAssociation.appendChild(assignment);
            path = "./*[local-name()='dataInputAssociation'][last()]";
            lastResult = (Node) parseXPath(userTask, path, XPathConstants.NODE);
            if (lastResult.getNextSibling() != null) {
                userTask.insertBefore(dataInputAssociation, lastResult.getNextSibling());
            } else {
                userTask.appendChild(dataInputAssociation);
            }
        }
    }

    private static void appendNewTaskAssignments(Document doc, Entry assignments) {
        String ProcessVariable = (String) assignments.getKey();
        Map<String, String> assignmentsInfos = (Map<String, String>) assignments.getValue();
        NodeList userTasks = doc.getElementsByTagName("bpmn2:userTask");
        for (int i = 0; i < userTasks.getLength(); i++) {
            Node userTask = userTasks.item(i);
            String path = "./@id";
            Node taskId = (Node) parseXPath(userTask, path, XPathConstants.NODE);
            path = "./*[local-name()='ioSpecification']";
            Node ioSpecificationNode = (Node) parseXPath(userTask, path, XPathConstants.NODE);
            for (Entry taskVariable : assignmentsInfos.entrySet()) {
                String variableName = (String) taskVariable.getKey();
                String variableType = (String) taskVariable.getValue();
                if (variableType.trim().equalsIgnoreCase("input")) {
                    appendNewItemDefenition(doc, taskId.getTextContent(), "InputXItem", "after" + taskId.getTextContent(), variableName);
                    Node dataInput = doc.createElement("bpmn2:dataInput");
                    Attr attribute = doc.createAttribute("id");
                    attribute.setTextContent(new StringBuilder(taskId.getTextContent())
                            .append("_").append(variableName).append("InputX").toString());
                    ((Element) dataInput).setAttributeNode(attribute);
                    attribute = doc.createAttribute("drools:dtype");
                    attribute.setTextContent("String");
                    ((Element) dataInput).setAttributeNode(attribute);
                    attribute = doc.createAttribute("itemSubjectRef");
                    attribute.setTextContent(new StringBuilder("_")
                            .append(taskId.getTextContent()).append("_").append(variableName)
                            .append("InputXItem").toString());
                    ((Element) dataInput).setAttributeNode(attribute);
                    attribute = doc.createAttribute("name");
                    attribute.setTextContent(variableName);
                    ((Element) dataInput).setAttributeNode(attribute);
                    path = "./*[local-name()='dataInput'][last()]";
                    Node lastResult = (Node) parseXPath(ioSpecificationNode, path, XPathConstants.NODE);
                    if (lastResult.getNextSibling() != null) {
                        ioSpecificationNode.insertBefore(dataInput, lastResult.getNextSibling());
                    } else {
                        ioSpecificationNode.appendChild(dataInput);
                    }
                    Node dataInputRefs = doc.createElement("bpmn2:dataInputRefs");
                    dataInputRefs.setTextContent(new StringBuilder(taskId.getTextContent()).append("_")
                            .append(variableName).append("InputX").toString());
                    path = "./*[local-name()='inputSet']";
                    Node inputSet = (Node) parseXPath(ioSpecificationNode, path, XPathConstants.NODE);
                    path = "./*[local-name()='dataInputRefs'][last()]";
                    lastResult = (Node) parseXPath(inputSet, path, XPathConstants.NODE);
                    if (lastResult.getNextSibling() != null) {
                        inputSet.insertBefore(dataInputRefs, lastResult.getNextSibling());
                    } else {
                        inputSet.appendChild(dataInputRefs);
                    }
                    Node dataInputAssociation = doc.createElement("bpmn2:dataInputAssociation");
                    attribute = doc.createAttribute("id");
                    ((Element) dataInputAssociation).setAttributeNode(attribute);
                    Node sourceRef = doc.createElement("bpmn2:sourceRef");
                    sourceRef.setTextContent(ProcessVariable);
                    dataInputAssociation.appendChild(sourceRef);
                    Node targetRef = doc.createElement("bpmn2:targetRef");
                    targetRef.setTextContent(new StringBuilder(taskId.getTextContent()).append("_")
                            .append(variableName).append("InputX").toString());
                    dataInputAssociation.appendChild(targetRef);
                    path = "./*[local-name()='dataInputAssociation'][last()]";
                    lastResult = (Node) parseXPath(userTask, path, XPathConstants.NODE);
                    if (lastResult.getNextSibling() != null) {
                        userTask.insertBefore(dataInputAssociation, lastResult.getNextSibling());
                    } else {
                        userTask.appendChild(dataInputAssociation);
                    }
                } else if (variableType.trim().equalsIgnoreCase("output")) {
                    appendNewItemDefenition(doc, taskId.getTextContent(), "OutputXItem", "after" + taskId.getTextContent(), variableName);
                    Node dataOutput = doc.createElement("bpmn2:dataOutput");
                    Attr attribute = doc.createAttribute("id");
                    attribute.setTextContent(new StringBuilder(taskId.getTextContent())
                            .append("_").append(variableName).append("OutputX").toString());
                    ((Element) dataOutput).setAttributeNode(attribute);
                    attribute = doc.createAttribute("drools:dtype");
                    attribute.setTextContent("String");
                    ((Element) dataOutput).setAttributeNode(attribute);
                    attribute = doc.createAttribute("itemSubjectRef");
                    attribute.setTextContent(new StringBuilder("_")
                            .append(taskId.getTextContent()).append("_").append(variableName)
                            .append("OutputXItem").toString());
                    ((Element) dataOutput).setAttributeNode(attribute);
                    attribute = doc.createAttribute("name");
                    attribute.setTextContent(variableName);
                    ((Element) dataOutput).setAttributeNode(attribute);
                    path = "./*[local-name()='dataOutput'][last()]";
                    Node lastResult = (Node) parseXPath(ioSpecificationNode, path, XPathConstants.NODE);
                    if (lastResult.getNextSibling() != null) {
                        ioSpecificationNode.insertBefore(dataOutput, lastResult.getNextSibling());
                    } else {
                        ioSpecificationNode.appendChild(dataOutput);
                    }
                    Node dataOutputRefs = doc.createElement("bpmn2:dataOutputRefs");
                    dataOutputRefs.setTextContent(new StringBuilder(taskId.getTextContent()).append("_")
                            .append(variableName).append("OutputX").toString());
                    path = "./*[local-name()='outputSet']";
                    Node outputSet = (Node) parseXPath(ioSpecificationNode, path, XPathConstants.NODE);
                    path = "./*[local-name()='dataOutputRefs'][last()]";
                    lastResult = (Node) parseXPath(outputSet, path, XPathConstants.NODE);
                    if (lastResult.getNextSibling() != null) {
                        outputSet.insertBefore(dataOutputRefs, lastResult.getNextSibling());
                    } else {
                        outputSet.appendChild(dataOutputRefs);
                    }
                    Node dataOutputAssociation = doc.createElement("bpmn2:dataOutputAssociation");
                    attribute = doc.createAttribute("id");
                    ((Element) dataOutputAssociation).setAttributeNode(attribute);
                    Node sourceRef = doc.createElement("bpmn2:sourceRef");
                    sourceRef.setTextContent(new StringBuilder(taskId.getTextContent()).append("_")
                            .append(variableName).append("OutputX").toString());
                    dataOutputAssociation.appendChild(sourceRef);
                    Node targetRef = doc.createElement("bpmn2:targetRef");
                    targetRef.setTextContent(ProcessVariable);
                    dataOutputAssociation.appendChild(targetRef);
                    path = "./*[local-name()='dataOutputAssociation'][last()]";
                    lastResult = (Node) parseXPath(userTask, path, XPathConstants.NODE);
                    if (lastResult.getNextSibling() != null) {
                        userTask.insertBefore(dataOutputAssociation, lastResult.getNextSibling());
                    } else {
                        userTask.appendChild(dataOutputAssociation);
                    }
                }
            }
        }
    }

    private static void appendNewItemDefenition(Document doc, String id, String type, String place, String item) {
        Node itemDefenition = doc.createElement("bpmn2:itemDefinition");
        StringBuilder itemDefenitionId = new StringBuilder("_");
        if (id != null) {
            itemDefenitionId.append(id).append("_");
        }
        itemDefenitionId.append(item).append(type);
        ((Element) itemDefenition).setAttribute("id", itemDefenitionId.toString());
        ((Element) itemDefenition).setAttribute("structureRef", "String");
        Node parent = doc.getFirstChild();
        if (place != null && place.startsWith("after")) {
            String path = "//*[local-name()='definitions']/*[local-name()='itemDefinition'][contains(@id,'" + type + "') and contains(@id,'" + place.replace("after", "") + "')][last()]";
            Node lastResult = (Node) parseXPath(doc, path, XPathConstants.NODE);
            if (lastResult.getNextSibling() != null) {
                parent.insertBefore(itemDefenition, lastResult.getNextSibling());
            } else {
                parent.appendChild(itemDefenition);
            }
        } else if (place != null && place.startsWith("before")) {
            String path = "//*[local-name()='definitions']/*[local-name()='itemDefinition'][contains(@id,'" + type + "') and contains(@id,'" + place.replace("before", "") + "')][1]";
            Node firstResult = (Node) parseXPath(doc, path, XPathConstants.NODE);
            parent.insertBefore(itemDefenition, firstResult);
        } else {
            parent.appendChild(itemDefenition);
        }
    }

    private static void appendNewProcessProperty(Document doc, String propertyName) {
        Node property = doc.createElement("bpmn2:property");
        ((Element) property).setAttribute("id", propertyName);
        ((Element) property).setAttribute("itemSubjectRef", new StringBuilder("_").append(propertyName).append("Item").toString());
        Node parent = doc.getElementsByTagName("bpmn2:process").item(0);
        String path = "//*[local-name()='definitions']/*[local-name()='process']/*[local-name()='property'][last()]";
        Node lastResult = (Node) parseXPath(doc, path, XPathConstants.NODE);
        if (lastResult.getNextSibling() != null) {
            parent.insertBefore(property, lastResult.getNextSibling());
        } else {
            parent.appendChild(property);
        }
    }
}
