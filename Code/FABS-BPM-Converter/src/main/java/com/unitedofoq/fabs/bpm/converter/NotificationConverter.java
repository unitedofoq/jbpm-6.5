/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.bpm.converter;

import java.util.Map;
import java.util.Map.Entry;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 *
 * @author mmasoud
 */
public class NotificationConverter {

    public Node createNewNode(String name, String text, Map<String, String> attributes, Document doc) {
        Node newNode = doc.createElement(name);
        if (attributes != null) {
            for (Entry attribute : attributes.entrySet()) {
                String attrName = (String) attribute.getKey();
                String attrValue = (String) attribute.getValue();
                if (attrValue == null) {
                    Attr attr = doc.createAttribute(attrName);
                    ((Element) newNode).setAttributeNode(attr);
                } else {
                    ((Element) newNode).setAttribute(attrName, attrValue);
                }
            }
        }
        if (text != null) {
            newNode.setTextContent(text);
        }
        return newNode;
    }
}
