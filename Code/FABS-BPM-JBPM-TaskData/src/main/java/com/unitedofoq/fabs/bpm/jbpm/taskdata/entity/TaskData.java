/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.bpm.jbpm.taskdata.entity;

/**
 *
 * @author mmasoud
 */
public class TaskData{
    private Long taskId;
    private String taskName;
    private String requester;
    private String lastUser;
    private String requesterDisplayName;
    private String lastuserDisplayName;

    public TaskData() {
    }

    public TaskData(Long taskId, String taskName, String requester
            , String lastUser, String requesterDisplayName
            , String lastuserDisplayName) {
        this.taskId = taskId;
        this.taskName = taskName;
        this.requester = requester;
        this.lastUser = lastUser;
        this.requesterDisplayName = requesterDisplayName;
        this.lastuserDisplayName = lastuserDisplayName;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getRequester() {
        return requester;
    }

    public void setRequester(String requester) {
        this.requester = requester;
    }

    public String getLastUser() {
        return lastUser;
    }

    public void setLastUser(String lastUser) {
        this.lastUser = lastUser;
    }

    public String getRequesterDisplayName() {
        return requesterDisplayName;
    }

    public void setRequesterDisplayName(String requesterDisplayName) {
        this.requesterDisplayName = requesterDisplayName;
    }
    
    public String getLastuserDisplayName() {
        return lastuserDisplayName;
    }

    public void setLastuserDisplayName(String lastuserDisplayName) {
        this.lastuserDisplayName = lastuserDisplayName;
    }
    
}
