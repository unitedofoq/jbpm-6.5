/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.bpm.jbpm.taskdata.service;

import com.unitedofoq.fabs.bpm.jbpm.taskdata.entity.TaskData;
import com.unitedofoq.fabs.bpm.jbpm.taskdata.connectors.DBConnector;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 *
 * @author mmasoud
 */
public class TaskService {

    private final DBConnector dbConnector;

    public TaskService() {
        dbConnector = new DBConnector();
    }

    public void insertTaskDataAfterCreation(TaskData taskData) {
        StringBuilder query = new StringBuilder("INSERT INTO taskdata");
        query.append("(task_id, taskname, requester, lastuser, requester_displayname, lastuser_displayname) VALUES");
        query.append("(? ,? ,?, ?, ?, ?)");
        dbConnector.connect();
        dbConnector.insertTaskData(query.toString(), taskData);
        dbConnector.disConnect();
    }

    private String getServiceUrl(String serviceName) {
        String serviceUrl = null;
        try {
            File config = new File("service.config");

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(config);
            String path = "//service/name[text()='serviceName']/../wsdlurl".replace("serviceName", serviceName);
            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();
            XPathExpression expr = xpath.compile(path);
            Node node = (Node) expr.evaluate(document, XPathConstants.NODE);
            if (node != null) {
                serviceUrl = node.getTextContent();
//                LOG.debug("ServiceName = " + serviceName);
//                LOG.debug("WSDL URL    = " + serviceUrl);
            } else {
//                LOG.debug("Web-Service Doesn't have WSDL URL registered");
            }
        } catch (ParserConfigurationException | SAXException | IOException | DOMException | XPathExpressionException e) {
//            LOG.debug("some error occured during getting service URL from service.config file");
        }
        return serviceUrl;
    }
}
