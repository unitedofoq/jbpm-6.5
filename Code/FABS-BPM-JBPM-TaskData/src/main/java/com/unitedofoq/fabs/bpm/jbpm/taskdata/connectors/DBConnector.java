/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.bpm.jbpm.taskdata.connectors;

import com.unitedofoq.fabs.bpm.jbpm.taskdata.entity.TaskData;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author mmasoud
 */
public class DBConnector {

    private Context initContext;
    private Connection connection;
    private DataSource datasource;
    private PreparedStatement preparedStatement;

    public DBConnector() {
        try {
            initContext = new InitialContext();
            datasource = (DataSource) initContext.lookup("java:jboss/datasources/jbpmDS");
        } catch (NamingException ex) {
            Logger.getLogger(DBConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void connect() {
        try {
            connection = datasource.getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(DBConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void disConnect() {
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void insertTaskData(String query, TaskData taskData) {
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, taskData.getTaskId());
            preparedStatement.setString(2, taskData.getTaskName());
            preparedStatement.setString(3, taskData.getRequester());
            preparedStatement.setString(4, taskData.getLastUser());
            preparedStatement.setString(5, taskData.getRequesterDisplayName());
            preparedStatement.setString(6, taskData.getLastuserDisplayName());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DBConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
