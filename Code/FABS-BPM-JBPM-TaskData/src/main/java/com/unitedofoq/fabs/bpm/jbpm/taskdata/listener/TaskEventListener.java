/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.bpm.jbpm.taskdata.listener;

import com.unitedofoq.fabs.bpm.jbpm.taskdata.entity.TaskData;
import com.unitedofoq.fabs.bpm.jbpm.taskdata.service.TaskService;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.jbpm.services.task.lifecycle.listeners.TaskLifeCycleEventListener;
import org.kie.api.task.TaskEvent;

/**
 *
 * @author mmasoud
 */
public class TaskEventListener implements TaskLifeCycleEventListener {

    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(TaskEventListener.class);

    @Override
    public void beforeTaskUpdatedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskUpdatedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskReassignedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskReassignedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskNotificationEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskNotificationEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskInputVariableChangedEvent(TaskEvent event, Map<String, Object> variables) {
        LOGGER.debug("Entering");
        Long taskId = event.getTask().getId();
        String taskName = (String) variables.get("TaskName");
        String lastuser = (String) variables.get("fromDisplayName");
        String requester = (String) variables.get("actorDisplayName");
        TaskService taskService = new TaskService();
        TaskData taskData = new TaskData(taskId, taskName, requester, lastuser
                , requester, lastuser);
        taskService.insertTaskDataAfterCreation(taskData);
    }

    @Override
    public void afterTaskOutputVariableChangedEvent(TaskEvent event, Map<String, Object> variables) {

    }

    @Override
    public void beforeTaskActivatedEvent(TaskEvent event) {
    }

    @Override
    public void beforeTaskClaimedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskSkippedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskStartedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskStoppedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskCompletedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskFailedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskAddedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskExitedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskReleasedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskResumedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskSuspendedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskForwardedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskDelegatedEvent(TaskEvent event) {

    }

    @Override
    public void beforeTaskNominatedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskActivatedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskClaimedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskSkippedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskStartedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskStoppedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskCompletedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskFailedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskAddedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskExitedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskReleasedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskResumedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskSuspendedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskForwardedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskDelegatedEvent(TaskEvent event) {

    }

    @Override
    public void afterTaskNominatedEvent(TaskEvent event) {

    }

}
