package com.adf.orgChart;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;
import oracle.adf.view.faces.bi.component.hierarchyViewer.UIHierarchyViewer;
import org.apache.batik.dom.AbstractNode;
import org.apache.myfaces.trinidad.model.ChildPropertyTreeModel;
import org.apache.myfaces.trinidad.model.TreeModel;

@SessionScoped
public class EmployeeTree {
	private Object instance = null;
	private transient TreeModel model = null;

	private ResultSet resultSet;
	private PreparedStatement prepareStatement;
	private Connection con;
	final static String URL = "jdbc:mysql://1.1.1.141:3306/";
	final static String DB_NAME = "testorgchart";
	final static String DRIVER = "com.mysql.jdbc.Driver";
	final static String USERNAME = "root";
	final static String PASSWORD = "root";
	private List<EmployeeItem> employees;

	public EmployeeTree() {

		ArrayList<EmployeeItem> root = new ArrayList<EmployeeItem>();
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = DriverManager
					.getConnection(URL + DB_NAME, USERNAME, PASSWORD);

			employees = getsubOrdinate("1", root);

			// EmployeeItem node1 = employees.get(0);
			// root.add(node1);
			// List<EmployeeItem> subOrdinates = getsubOrdinate(node1.getId(),
			// root);
			// node1.setChildren(subOrdinates);
		} catch (Exception e) {
			e.printStackTrace();
		}
		setListInstance(root);
	}

        public void doNavigateUp(ActionEvent actionEvent) {
        // Add event code here...
        UIHierarchyViewer hv = (UIHierarchyViewer)actionEvent.getSource();
        TreeModel modelhv = (TreeModel)hv.getValue();
        AbstractNode em = (AbstractNode) modelhv.getRowData();
    }
	public List<EmployeeItem> getsubOrdinate(String managerID,
			ArrayList<EmployeeItem> root) throws InstantiationException,
			IllegalAccessException, ClassNotFoundException, SQLException {

		ResultSet resultSet;
		PreparedStatement prepareStatement = con
				.prepareStatement("select empname,emp_id,dept_id from employee where manager_id=?");
		prepareStatement.setString(1, managerID);
		resultSet = prepareStatement.executeQuery();
		List<EmployeeItem> employees = new ArrayList<EmployeeItem>();
		while (resultSet.next()) {
			String name = resultSet.getString(1);
			String id = resultSet.getString(2);
			String departmentID = resultSet.getString(3);
			EmployeeItem employeeItem = new EmployeeItem(name, id, departmentID);
			employeeItem.setChildren(getsubOrdinate(employeeItem.getId(), root));
			employees.add(employeeItem);
			System.out.println("***********" + name + " " + id);
		}
		if (managerID == "1") {
			root.add(employees.get(0));
		}
		return employees;
	}

	public TreeModel getModel() {
		if (model == null)
			model = new ChildPropertyTreeModel(instance, "children");

		return model;
	}

	public void setListInstance(List instance) {
		this.instance = instance;
		model = null;
	}
        public void doSetAnchor(ActionEvent actionEvent) {
        // Add event code here...

        UIHierarchyViewer hv = (UIHierarchyViewer) actionEvent.getSource();
        TreeModel model = (TreeModel) hv.getValue();
    }
	
}