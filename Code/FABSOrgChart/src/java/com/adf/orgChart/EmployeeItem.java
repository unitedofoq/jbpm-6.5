package com.adf.orgChart;

import java.util.List;


public class EmployeeItem {
	String name, id, deptId;
	private List<EmployeeItem> children;

	public EmployeeItem(String name, String id, String deptId) {
		super();
		this.name = name;
		this.id = id;
		this.deptId = deptId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public void setChildren(List<EmployeeItem> children) {
		this.children = children;
	}

	public List<EmployeeItem> getChildren() {
		return children;
	}
}