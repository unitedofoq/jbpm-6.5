package com.adf.orgChart;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mibrahim
 */
public class EmployeeOrgItem implements Serializable {

    private static final long serialVersionUID = 1L;
    private String positionName;
    private String unitName;
    private String employeeName;
    private String employeeDBID;
    private String email;
    private String mobile;
    private String phone;
    private String hiringDate;
    private String image;
    private int level;
    private int order;
    private int childrenCount;
    private List<EmployeeOrgItem> children;
    private EmployeeOrgItem parent;
    private String id;
    private String style = "width:100%;height:100%;padding:5px;";

    public EmployeeOrgItem() {
    }

    public EmployeeOrgItem(EmployeeOrgItem employeeOrgItem) {
        positionName = employeeOrgItem.getPositionName();
        employeeName = employeeOrgItem.getEmployeeName();
        employeeDBID = employeeOrgItem.getEmployeeDBID();
        email = employeeOrgItem.getEmail();
        mobile = employeeOrgItem.getMobile();
        phone = employeeOrgItem.getPhone();
        hiringDate = employeeOrgItem.getHiringDate();
        image = employeeOrgItem.getImage();
        parent = employeeOrgItem.getParent();
        id = employeeOrgItem.getId();
        Collections.copy(children, employeeOrgItem.getChildren());
    }

    public EmployeeOrgItem(com.unitedofoq.orgchartservicews.EmployeeOrgItem employeeOrgItem, EmployeeOrgItem parentItem) {
        positionName = employeeOrgItem.getPositionName();
        employeeName = employeeOrgItem.getEmployeeName();
        employeeDBID = employeeOrgItem.getEmployeeID();
        email = employeeOrgItem.getEmail();
        mobile = employeeOrgItem.getMobile();
        phone = employeeOrgItem.getPhone();
        hiringDate = employeeOrgItem.getHiringDate();
        image = getPersonImagePath(employeeOrgItem.getImage(), employeeOrgItem.getPhotoName());
        parent = parentItem;
        id = employeeOrgItem.getId();
        if (employeeOrgItem.getEmployeeID() == null || employeeOrgItem.getEmployeeID().equals("")) {
            style = "width:100%;height:100%;padding:5px;background-color: #ff7171";
        }
     }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getHiringDate() {
        return hiringDate;
    }

    public void setHiringDate(String hiringDate) {
        this.hiringDate = hiringDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public List<EmployeeOrgItem> getChildren() {
        return children;
    }

    public void setChildren(List<EmployeeOrgItem> children) {
        this.children = children;
    }

    /**
     * @return the parent
     */
    public EmployeeOrgItem getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(EmployeeOrgItem parent) {
        this.parent = parent;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    public void removeChildEmployee(EmployeeOrgItem emp) {
        getChildren().remove(emp);
        emp.parent = null;
    }

    /**
     * @return the style
     */
    public String getStyle() {
        return style;
    }

    /**
     * @param style the style to set
     */
    public void setStyle(String style) {
        this.style = style;
    }

    /**
     * @return the employeeDBID
     */
    public String getEmployeeDBID() {
        return employeeDBID;
    }

    /**
     * @param employeeDBID the employeeDBID to set
     */
    public void setEmployeeDBID(String employeeDBID) {
        this.employeeDBID = employeeDBID;
    }

    /**
     * @return the level
     */
    public int getLevel() {
        return level;
    }

    /**
     * @param level the level to set
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * @return the order
     */
    public int getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(int order) {
        this.order = order;
    }

    /**
     * @return the childrenCount
     */
    public int getChildrenCount() {
        return childrenCount;
    }

    /**
     * @param childrenCount the childrenCount to set
     */
    public void setChildrenCount(int childrenCount) {
        this.childrenCount = childrenCount;
    }

    private String getPersonImagePath(byte[] photoData, String photoName) {
        if (photoData == null || photoName == null || photoName.equals("")) {
            return "/images/Vacant.jpeg";
        }
        String systemURL = null;
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String realPath = externalContext.getRealPath("/images/");
        if (request instanceof HttpServletRequest) {
            systemURL = ((HttpServletRequest) request).getRequestURL().toString();
            systemURL = systemURL.substring(0, systemURL.lastIndexOf("/"));
        }
        //write the file content
        try {
            FileOutputStream outputStream = new FileOutputStream(realPath + "/" + photoName);
            outputStream.write(photoData);
            outputStream.close();

        } catch (Exception ex) {
            Logger.getLogger(EmployeeViewer.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (photoData == null || photoName == null || photoName.equals("")) {
            String url = "/images/Vacant.jpeg";
            return url;
        } else {
            File file = new File(realPath + "/" + photoName);
            if (file.exists() && file.length() != 0) {
                return systemURL + "/images/" + photoName;
            }
        }
        String url = "/images/Vacant.jpeg";
        return url;
    }
}
