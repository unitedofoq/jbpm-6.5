package com.adf.orgChart;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.el.MethodExpression;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;
import oracle.adf.view.faces.bi.component.hierarchyViewer.UIHierarchyViewer;
import oracle.adf.view.rich.component.rich.RichMenu;
import oracle.adf.view.rich.component.rich.nav.RichCommandMenuItem;
import oracle.adf.view.rich.datatransfer.SimpleTransferable;
import oracle.adf.view.rich.dnd.DnDAction;
import oracle.adf.view.rich.event.DropEvent;
import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.model.ChildPropertyTreeModel;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.model.TreeModel;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


public class EmployeeViewer implements Serializable {

    public static final String ENTITY_DBID_PARAM_NAMR = "entityDBID";
    public static final String VACANT_STYLE = "width:100%;height:100%;padding:5px;background-color: #ff7171";
    public static final String OTMS_DATASOURCE_JNDI = "jdbc/OHR";
    private Object instance = null;
    private transient TreeModel model = null;
    private String htmlDir;
    private List<EmployeeOrgItem> orgChartEmployees;
    private RichMenu employeeMenu;
    private RichMenu positionMenu;
    private String mobileValue;
    private String phoneValue;
    private String emailValue;
    private String contactValue;
    private String employeeCodeValue;
    private String unitNameValue;
    private String userName;
    private String fabsWSDLURL;
    private String otmsWSDLURL;
    private String rootPositionID;
    private String entityName;
//    private boolean isUnit = true;
    private boolean defaultOrgChart = true;
    private boolean empDirectManagerOrgChart=false;
    private enum OrgChartEntity {
        Unit("Unit"), Position("Poition"), Employee("Employee");
        private String value;
        private OrgChartEntity(String val) {
            this.value = val;
        }
    };
    private OrgChartEntity orgChartEntity;
    List<String> orgDDs ;
    public EmployeeViewer() {
        System.out.println("EmployeeViewer  Constructor is Called!");

    }

    @PostConstruct
    public void init() {
        // check params to check the unit dbid
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        
        fabsWSDLURL = (null != fabsWSDLURL)
                ? fabsWSDLURL : params.get("fabsWSDLURL");
        otmsWSDLURL = (null != otmsWSDLURL) ? otmsWSDLURL : params.get("otmsWSDLURL");
        

        if (null != params.get("htmlDir")
                && params.get("htmlDir").equals("rtl")) {
            htmlDir = "ar";
        }
        userName = (null != userName) ? userName : params.get("userName");
        //call fabs webservice to get dds for different labels in the node
        //call getOrgChartDDs in OrgChartServiceBean
        orgDDs = callOrgChartServiceMethod(userName, "getOrgChartDDs");
        if (orgDDs != null && !orgDDs.isEmpty()) {
            mobileValue = orgDDs.get(0);
            phoneValue = orgDDs.get(1);
            emailValue = orgDDs.get(2);
            contactValue = orgDDs.get(3);
            employeeCodeValue = orgDDs.get(6);
            unitNameValue = orgDDs.get(7);
            
        }
        
        // This is the entity Name to run orgChart against (Unit / Position)
        entityName = params.get("entityName");
        setOrgChartEntity(entityName);
        orgChartEmployees = new ArrayList<EmployeeOrgItem>();
        createEmployeeMenu(userName, orgDDs.get(4));
        createPositionUnitMenu(userName, orgDDs.get(5));
        rootPositionID = params.get(ENTITY_DBID_PARAM_NAMR);
        
        populateTree();
    }
    
    private void populateTree() {
        List<EmployeeOrgItem> root = new ArrayList<EmployeeOrgItem>();
        if (rootPositionID != null && !rootPositionID.equals("")) {
            root.add(getChartNodes(userName, rootPositionID));
        } else {
            root.add(getChartNodes(userName, "0"));
        }
        setListInstance(root);
    }

    public TreeModel getModel() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String newRootPositionID = params.get(ENTITY_DBID_PARAM_NAMR);

        if (newRootPositionID != null) {
            // Update model if new root position ID
            if (!newRootPositionID.equals(rootPositionID)) {
                rootPositionID = newRootPositionID;
                populateTree();
            }
            // Due to a defect in ADF with GF4, we cannot use view scoped beans
            // therefore we're using this workaround to refresh model for new
            // requests (non-postback requests)
            else if (!FacesContext.getCurrentInstance().isPostback()) {
                    // Update model if new request
                    populateTree();
                }
        }

        if (model == null) {
            model = new ChildPropertyTreeModel(instance, "children");
        }

        return model;
    }

    private void setListInstance(List instance) {
        this.instance = instance;
        model = null;
    }

    public void doSetAnchor(ActionEvent actionEvent) {
        // Add event code here...

        UIHierarchyViewer hv = (UIHierarchyViewer) actionEvent.getSource();
        TreeModel model = (TreeModel) hv.getValue();
    }

    public DnDAction handleDrop(DropEvent dropEvent) {
        DnDAction proposedAction = DnDAction.NONE;
        boolean bRefreshTable = false;
        if (dropEvent.getDragComponent() instanceof UIHierarchyViewer) {
            proposedAction = dropEvent.getProposedAction();
            UIHierarchyViewer hv = (UIHierarchyViewer) dropEvent.getDragComponent();
            TreeModel model = (TreeModel) hv.getValue();
            Map dropSite = (Map) dropEvent.getDropSite();
            String dropTargetClientRowKey = (String) dropSite.get("clientRowKey");
            EmployeeOrgItem dropTargetEmployee = null;
            if (dropTargetClientRowKey != null) {
                Object dropTargetRowKey = hv.getClientRowKeyManager().getRowKey(FacesContext.getCurrentInstance(),
                        hv, dropTargetClientRowKey);
                model.setRowKey(dropTargetRowKey);
                dropTargetEmployee = (EmployeeOrgItem) model.getRowData();
            }
            SimpleTransferable transfer = (SimpleTransferable) dropEvent.getTransferable();

            RowKeySet keySet = transfer.getData(RowKeySet.class);

            ArrayList<EmployeeOrgItem> hvDataToRemove = new ArrayList<EmployeeOrgItem>();
            ArrayList<ArrayList> hvDataToAdd = new ArrayList<ArrayList>();

            Iterator iterator = keySet.iterator();
            while (iterator.hasNext()) {
                Object rowKey = iterator.next();
                model.setRowKey(rowKey);
                EmployeeOrgItem rowData = (EmployeeOrgItem) model.getRowData();

                // Now change each marker based on the DropEvent's proposed action
                if (rowData == null || rowData == dropTargetEmployee) {
                    continue;
                }

                if (proposedAction == DnDAction.MOVE) {
                    EmployeeOrgItem parentEmp = rowData.getParent();
                    if (dropTargetEmployee != null) {

                        hvDataToRemove.add(rowData);

                        ArrayList<EmployeeOrgItem> al = new ArrayList<EmployeeOrgItem>();
                        al.add(dropTargetEmployee);
                        al.add(rowData);
                        hvDataToAdd.add(al);

                        bRefreshTable = true;
                    } else {
                        if (parentEmp == null) {
                            proposedAction = DnDAction.NONE;
                        } else {
                            //parentEmp.removeChildEmployee(rowData);
                            hvDataToRemove.add(rowData);

                            //AddRootEmployeeToTreeModel(model, rowData);
                            ArrayList<EmployeeOrgItem> al = new ArrayList<EmployeeOrgItem>();
                            al.add(null);
                            al.add(rowData);
                            hvDataToAdd.add(al);

                            bRefreshTable = true;
                        }
                    }
                }
            }

            //remove and add data from/to HV after above loop so that row keys don't change during above loop
            for (int ii = hvDataToRemove.size() - 1; ii >= 0; ii--) {
                EmployeeOrgItem emp = hvDataToRemove.remove(ii);
                EmployeeOrgItem parentEmp = emp.getParent();
                if (parentEmp != null) {
                    parentEmp.removeChildEmployee(emp);
                } else {
                    RemoveRootEmployeeFromTreeModel(model, emp);
                }
            }
            //add data to HV after first removing
            //add data in order from first to last
            int numToAdd = hvDataToAdd.size();
            for (int ii = 0; ii < numToAdd; ii++) {
                ArrayList<EmployeeOrgItem> al = hvDataToAdd.remove(0);
                EmployeeOrgItem parentEmp = al.get(0);
                EmployeeOrgItem emp = al.get(1);
                if (parentEmp != null) {
                    parentEmp.getChildren().add(emp);
                } else {
                    AddRootEmployeeToTreeModel(model, emp);
                }
            }
        }

        if (bRefreshTable) {
            RequestContext currentInstance = RequestContext.getCurrentInstance();
            currentInstance.addPartialTarget(dropEvent.getDragComponent());
        }
        return proposedAction;
    }

    protected void AddRootEmployeeToTreeModel(TreeModel model, EmployeeOrgItem rowData) {
        Object wrappedData = model.getWrappedData();
        rowData.setParent(null);
        if (wrappedData == null) {
            wrappedData = rowData;
        } else if (wrappedData instanceof EmployeeOrgItem) {
            ArrayList arrayList = new ArrayList<EmployeeOrgItem>();
            arrayList.add(wrappedData);
            arrayList.add(rowData);
            wrappedData = arrayList;
        } else if (wrappedData instanceof ArrayList) {
            ((ArrayList) wrappedData).add(new EmployeeOrgItem(rowData));
        }
        model.setWrappedData(wrappedData);
    }

    protected void RemoveRootEmployeeFromTreeModel(TreeModel model, EmployeeOrgItem rowData) {
        Object wrappedData = model.getWrappedData();
        if (wrappedData instanceof EmployeeOrgItem) {
            wrappedData = null;
        } else if (wrappedData instanceof ArrayList) {
            ((ArrayList) wrappedData).remove(rowData);
            if (((ArrayList) wrappedData).size() == 1) {
                wrappedData = ((ArrayList) wrappedData).get(0);
            }
        }
        model.setWrappedData(wrappedData);
    }

    public void employeeNodeAction(ActionEvent ae) {
        String compId = ae.getComponent().getId();

//        String actionType = compId.substring(compId.lastIndexOf("_") - 3, compId.lastIndexOf("_"));
        String actionDBID = compId.substring(compId.lastIndexOf("_") + 1, compId.length());

//        if (actionType.equals("emp")) {
        //HtmlInputHidden empHiddDBID = (HtmlInputHidden) ae.getComponent().getParent().getParent().getChildren().get(3);
        HtmlInputHidden empHiddDBID = (HtmlInputHidden) ae.getComponent().getParent().getChildren().get(0);
        String empDBID = String.valueOf(empHiddDBID.getValue());
        if (empDBID != null && !empDBID.equals("") && !empDBID.equals("null")) {
            List<String> returnedValues = runAction(userName, "runActionForEmployee", actionDBID, empDBID);
            if (returnedValues != null && !returnedValues.isEmpty()) {
                if (returnedValues.get(0).equals("ScreenFn")) {
                    FacesContext fctx = FacesContext.getCurrentInstance();
                    ExtendedRenderKitService erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
                    erks.addScript(fctx, "openScreen({screenName:'" + returnedValues.get(1) + "',screenInstId:'" + returnedValues.get(2)
                            + "',viewPage:'" + returnedValues.get(3) + "',portletTitle:'" + returnedValues.get(4)
                            + "',entityDBID:'" + empDBID
                            + "',funDBID:'" + returnedValues.get(5) + "'});");
                } else if (returnedValues.get(0).equals("PortalPagFn")) {
                    FacesContext fctx = FacesContext.getCurrentInstance();
                    ExtendedRenderKitService erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
                    erks.addScript(fctx, "openPortalPage({userName:'" + userName + "',entityType:'Employee',entityDBID:'"
                            + empDBID + "',funDBID:'" + returnedValues.get(1)
                            + "',pageURL:'" + returnedValues.get(2) + "'});");
                }
            }
        }
//        }
    }

    public void positionNodeAction(ActionEvent ae) {
        String compId = ae.getComponent().getId();
//        String actionType = compId.substring(compId.lastIndexOf("_") - 3, compId.lastIndexOf("_"));
        String actionDBID = compId.substring(compId.lastIndexOf("_") + 1, compId.length());

//        if (actionType.equals("pos")) {
        //HtmlInputHidden posHiddDBID = (HtmlInputHidden) ae.getComponent().getParent().getParent().getChildren().get(4);
        HtmlInputHidden posHiddDBID = (HtmlInputHidden) ae.getComponent().getParent().getChildren().get(0);
        String posDBID = String.valueOf(posHiddDBID.getValue());
        List<String> returnedValues = runAction(userName, "runActionForEmployee", actionDBID, posDBID);
        if (returnedValues != null && !returnedValues.isEmpty()) {
            if (returnedValues.get(0).equals("ScreenFn")) {
                FacesContext fctx = FacesContext.getCurrentInstance();
                ExtendedRenderKitService erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
                erks.addScript(fctx, "openScreen({screenName:'" + returnedValues.get(1) + "',screenInstId:'" + returnedValues.get(2)
                        + "',viewPage:'" + returnedValues.get(3) + "',portletTitle:'" + returnedValues.get(4)
                        + "',entityDBID:'" + posDBID
                        + "',funDBID:'" + returnedValues.get(5) + "'});");
            } else if (returnedValues.get(0).equals("PortalPagFn")) {
                FacesContext fctx = FacesContext.getCurrentInstance();
                ExtendedRenderKitService erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
                erks.addScript(fctx, "openPortalPage({userName:'" + userName + "',entityType:'Position',entityDBID:'"
                        + posDBID + "',funDBID:'" + returnedValues.get(1)
                        + "',pageURL:'" + returnedValues.get(2) + "'});");
            }
        }
//        }
    }

    /**
     * @return the orgChartEmployees
     */
    public List<EmployeeOrgItem> getOrgChartEmployees() {
        return orgChartEmployees;
    }

    /**
     * @param orgChartEmployees the orgChartEmployees to set
     */
    public void setOrgChartEmployees(List<EmployeeOrgItem> orgChartEmployees) {
        this.orgChartEmployees = orgChartEmployees;
    }

    /**
     * @return the htmlDir
     */
    public String getHtmlDir() {
        return htmlDir;
    }

    /**
     * @param htmlDir the htmlDir to set
     */
    public void setHtmlDir(String htmlDir) {
        this.htmlDir = htmlDir;
    }

    private List<String> callOrgChartServiceMethod(String userName, String operationName) {
        try {
            String nameSpace = "http://OrgChartWebService.unitedofoq.com/";

            URL oURL = new URL(fabsWSDLURL);
            HttpURLConnection httsCon = (HttpURLConnection) oURL.openConnection();
            httsCon.setDoOutput(true);
            httsCon.setDoInput(true);
            httsCon.setRequestMethod("POST");
            httsCon.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            httsCon.setRequestProperty("SOAPAction", nameSpace + operationName);
            // For Secuirty We Will Use User, Pass (now not used).

            String soapMessage = " <soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"> "
                    + "  <soap:Header/> "
                    + "  <soap:Body> "
                    + "    <input:" + operationName + " xmlns:input=\"" + nameSpace + "\"> "
                    + "     <userLoginName>" + userName + "</userLoginName> "
                    + "    </input:" + operationName + "> "
                    + "   </soap:Body> "
                    + " </soap:Envelope> ";

            OutputStream reqStream = httsCon.getOutputStream();
            reqStream.write(soapMessage.getBytes());
            InputStream resStream = httsCon.getInputStream();
            byte[] byteBuf = new byte[1024];
            int len = resStream.read(byteBuf);
            String in = "";
            while (len > -1) {
                in += new String(byteBuf, 0, len);
                len = resStream.read(byteBuf);
            }

            int start = 0, end = 0;
            start = in.indexOf("<return");
            end = in.lastIndexOf("</return>");

            String toBeSplit = in.substring(start, in.length());
            String[] inputArr = toBeSplit.split("</return>");
            List<String> values = new ArrayList<String>();
            for (int counter = 0; counter < inputArr.length - 1; counter++) {
                int previosIndex = inputArr[counter].lastIndexOf("\">");
                previosIndex += 2;
                values.add(inputArr[counter].substring(previosIndex, inputArr[counter].length()));
            }
            return values;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private List<String> runAction(String userName, String operationName, String actionID, String entityId) {
        try {
            String nameSpace = "http://OrgChartWebService.unitedofoq.com/";

            URL oURL = new URL(fabsWSDLURL);
            HttpURLConnection httsCon = (HttpURLConnection) oURL.openConnection();
            httsCon.setDoOutput(true);
            httsCon.setDoInput(true);
            httsCon.setRequestMethod("POST");
            httsCon.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            httsCon.setRequestProperty("SOAPAction", nameSpace + operationName);
            // For Secuirty We Will Use User, Pass (now not used).

            String soapMessage = " <soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"> "
                    + "  <soap:Header/> "
                    + "  <soap:Body> "
                    + "    <input:" + operationName + " xmlns:input=\"" + nameSpace + "\"> "
                    + "     <userLoginName>" + userName + "</userLoginName> "
                    + "     <actionDBID>" + actionID + "</actionDBID> "
                    + "     <entityDBID>" + entityId + "</entityDBID> "
                    + "    </input:" + operationName + "> "
                    + "   </soap:Body> "
                    + " </soap:Envelope> ";

            OutputStream reqStream = httsCon.getOutputStream();
            reqStream.write(soapMessage.getBytes());
            InputStream resStream = httsCon.getInputStream();
            byte[] byteBuf = new byte[1024];
            int len = resStream.read(byteBuf);
            String in = "";
            while (len > -1) {
                in += new String(byteBuf, 0, len);
                len = resStream.read(byteBuf);
            }

            int start = 0, end = 0;
            start = in.indexOf("<return");
            end = in.lastIndexOf("</return>");

            String toBeSplit = in.substring(start, in.length());
            String[] inputArr = toBeSplit.split("</return>");
            List<String> values = new ArrayList<String>();
            for (int counter = 0; counter < inputArr.length - 1; counter++) {
                int previosIndex = inputArr[counter].lastIndexOf("\">");
                previosIndex += 2;
                values.add(inputArr[counter].substring(previosIndex, inputArr[counter].length()));
            }
            return values;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private EmployeeOrgItem getChartNodes(String userName, String positionDBID) {
        try {
            URL oURL = new URL(otmsWSDLURL);

            com.unitedofoq.orgchartservicews.OrgChartServiceWS_Service service1 = 
                    new com.unitedofoq.orgchartservicews.OrgChartServiceWS_Service(oURL);
            com.unitedofoq.orgchartservicews.OrgChartServiceWS port1 = service1.getOrgChartServiceWSPort();
            List<com.unitedofoq.orgchartservicews.EmployeeOrgItem> result ;
            
            switch(orgChartEntity){
                case Unit:
                    result = port1.getEmployeeOrgItemByUnit(Long.valueOf(positionDBID), userName);
                    break;
                case Employee:
                    result = port1.getEmployeeOrgItemByDirectManager(Long.valueOf(positionDBID), userName);
                    break;
                case Position: // defualt case is position
                default:
                    result = port1.getEmployeeOrgItem(Long.valueOf(positionDBID), userName);
                    break;
            }
//            
//            if(isUnit) 
//            else
                
            EmployeeOrgItem rootItem = new EmployeeOrgItem(result.get(0), null);
            orgChartEmployees.add(rootItem);
            rootItem.setChildren(getNodeChildren(rootItem, result));
            return rootItem;
        } catch (Exception e) {
            System.out.println("exception");
            return null;
        }
    }

    private List<EmployeeOrgItem> getNodeChildren(EmployeeOrgItem parentItem, List<com.unitedofoq.orgchartservicews.EmployeeOrgItem> allNodes) {
        List<EmployeeOrgItem> employees = new ArrayList<EmployeeOrgItem>();

        for (com.unitedofoq.orgchartservicews.EmployeeOrgItem node : allNodes) {
            if (node.getParent() == null || !node.getParent().getId().equals(parentItem.getId())) {
                continue;
            }
            EmployeeOrgItem empOrgItem = new EmployeeOrgItem(node, parentItem);
            empOrgItem.setChildren(getNodeChildren(empOrgItem, allNodes));
            employees.add(empOrgItem);
            orgChartEmployees.add(empOrgItem);
        }
        return employees;
    }

    private void createEmployeeMenu(String userName, String menuText) {
        employeeMenu = new RichMenu();
        employeeMenu.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "menuId");
        employeeMenu.setText(menuText);

        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        Class[] params = {ActionEvent.class};
        MethodExpression me = FacesContext.getCurrentInstance().getApplication().getExpressionFactory().createMethodExpression(
                elContext, "#{EmployeeViewer.employeeNodeAction}",
                void.class, params);
        List<String> employeeActions = callOrgChartServiceMethod(userName, "getEmployeeEntityActions");
        if (employeeActions != null) {
            for (int actionsCount = 0; actionsCount < employeeActions.size(); actionsCount = actionsCount + 2) {
                RichCommandMenuItem menuActionItem = new RichCommandMenuItem();
                menuActionItem.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_emp_" + employeeActions.get(actionsCount));
                menuActionItem.setText(employeeActions.get(actionsCount + 1));
                menuActionItem.addActionListener(new MethodExpressionActionListener(me));
                employeeMenu.getChildren().add(menuActionItem);
            }
        }
    }

    private void createPositionUnitMenu(String userName, String menuText) {
        positionMenu = new RichMenu();
        positionMenu.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "menuId");
        positionMenu.setText(menuText);

        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        Class[] params = {ActionEvent.class};
        MethodExpression me = FacesContext.getCurrentInstance().getApplication().getExpressionFactory().createMethodExpression(
                elContext, "#{EmployeeViewer.positionNodeAction}",
                void.class, params);
        // Check the entity class, and load the actions based on it.
        String entityActionsMethodName = "getPositionEntityActions";
        
        switch(orgChartEntity){
                case Unit:
                    entityActionsMethodName = "getUnitEntityActions";
                    break;
                case Employee:
                    return;
                    //entityActionsMethodName = "getPositionEntityActions";
//                    break;
                case Position: // defualt case is position
                default:
                    entityActionsMethodName = "getPositionEntityActions";
                    break;
        }
//        
//        
//        if(isUnit)
//            entityActionsMethodName = "getUnitEntityActions";
        List<String> objActions = callOrgChartServiceMethod(userName, entityActionsMethodName);
        if (objActions != null) {
            for (int actionsCount = 0; actionsCount < objActions.size(); actionsCount = actionsCount + 2) {
                RichCommandMenuItem menuActionItem = new RichCommandMenuItem();
                menuActionItem.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId() + "_pos_" + objActions.get(actionsCount));
                menuActionItem.setText(objActions.get(actionsCount + 1));
                menuActionItem.addActionListener(new MethodExpressionActionListener(me));
                positionMenu.getChildren().add(menuActionItem);
            }
        }
    }

    /**
     * @return the employeeMenu
     */
    public RichMenu getEmployeeMenu() {
        return employeeMenu;
    }

    /**
     * @param employeeMenu the employeeMenu to set
     */
    public void setEmployeeMenu(RichMenu employeeMenu) {
        this.employeeMenu = employeeMenu;
    }

    /**
     * @return the mobileValue
     */
    public String getMobileValue() {
        return mobileValue;
    }

    /**
     * @param mobileValue the mobileValue to set
     */
    public void setMobileValue(String mobileValue) {
        this.mobileValue = mobileValue;
    }

    /**
     * @return the phoneValue
     */
    public String getPhoneValue() {
        return phoneValue;
    }

    /**
     * @param phoneValue the phoneValue to set
     */
    public void setPhoneValue(String phoneValue) {
        this.phoneValue = phoneValue;
    }

    /**
     * @return the emailValue
     */
    public String getEmailValue() {
        return emailValue;
    }

    /**
     * @param emailValue the emailValue to set
     */
    public void setEmailValue(String emailValue) {
        this.emailValue = emailValue;
    }

    /**
     * @return the contactValue
     */
    public String getContactValue() {
        if(empDirectManagerOrgChart)
            return "";
        return contactValue;
    }

    /**
     * @param contactValue the contactValue to set
     */
    public void setContactValue(String contactValue) {
        this.contactValue = contactValue;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the fabsWSDLURL
     */
    public String getFabsWSDLURL() {
        return fabsWSDLURL;
    }

    /**
     * @param fabsWSDLURL the fabsWSDLURL to set
     */
    public void setFabsWSDLURL(String fabsWSDLURL) {
        this.fabsWSDLURL = fabsWSDLURL;
    }

    /**
     * @return the otmsWSDLURL
     */
    public String getOtmsWSDLURL() {
        return otmsWSDLURL;
    }

    /**
     * @param otmsWSDLURL the otmsWSDLURL to set
     */
    public void setOtmsWSDLURL(String otmsWSDLURL) {
        this.otmsWSDLURL = otmsWSDLURL;
    }

    /**
     * @return the positionMenu
     */
    public RichMenu getPositionMenu() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        // Entity Name to run orgChart against (Unit / Position)
        String nEntityName = params.get("entityName");
        // update Menu for Position/Unit if the param value changed
        if (nEntityName != null && !nEntityName.equalsIgnoreCase(entityName)) {
            entityName = nEntityName;
            setOrgChartEntity(entityName);
//            if ("Position".equalsIgnoreCase(entityName)) {
//                isUnit = false;
//            } else {
//                isUnit = true;
//            }
            orgChartEmployees = new ArrayList<EmployeeOrgItem>();
            createEmployeeMenu(userName, orgDDs.get(4));
            createPositionUnitMenu(userName, orgDDs.get(5));
        }
        return positionMenu;
    }
    
    private void setOrgChartEntity(String value){
        
        if("Position".equalsIgnoreCase(value)){
//            isUnit = false;
            orgChartEntity = OrgChartEntity.Position;
        } else if ("unit".equalsIgnoreCase(value)){
//            isUnit = true;
            orgChartEntity = OrgChartEntity.Unit;
        } else /*if ("employee".equalsIgnoreCase(value))*/{ // no input work wz employee
//            isUnit = false;
              empDirectManagerOrgChart=true;
              defaultOrgChart=false;
            orgChartEntity = OrgChartEntity.Employee;
        }
    }

    public boolean isDefaultOrgChart() {
        return defaultOrgChart;
    }

    public void setDefaultOrgChart(boolean defaultOrgChart) {
        this.defaultOrgChart = defaultOrgChart;
    }
    
    public boolean isEmpDirectManagerOrgChart() {
        return empDirectManagerOrgChart;
    }

    public void setEmpDirectManagerOrgChart(boolean empDirectManagerOrgChart) {
        this.empDirectManagerOrgChart = empDirectManagerOrgChart;
    }

    public String getEmployeeCodeValue() {
        return employeeCodeValue;
    }

    public void setEmployeeCodeValue(String employeeCodeValue) {
        this.employeeCodeValue = employeeCodeValue;
    }

    public String getUnitNameValue() {
        return unitNameValue;
    }

    public void setUnitNameValue(String unitNameValue) {
        this.unitNameValue = unitNameValue;
    }
    
    
    /**
     * @param positionMenu the positionMenu to set
     */
    public void setPositionMenu(RichMenu positionMenu) {
        this.positionMenu = positionMenu;
    }
}
