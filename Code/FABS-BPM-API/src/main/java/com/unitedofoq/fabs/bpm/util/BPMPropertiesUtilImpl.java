/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.bpm.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author bgalal
 */
@Singleton
@Startup
@EJB(name = "java:global/ofoq/com.unitedofoq.fabs.bpm.util.BPMPropertiesUtil", beanInterface = BPMPropertiesUtil.class)
public class BPMPropertiesUtilImpl implements BPMPropertiesUtil {

	private Properties properties;
	private long lastModified = 0;
	private File BpmConfigFile = null;

	@PostConstruct
	public void init() {
		loadBPMProperties();
	}

	public void loadBPMProperties() {
		InputStream input = null;
		String fileName = "process_engine_config.properties";
		String filePath = System.getProperty("fabsPropertyPath");
		StringBuilder file = new StringBuilder(filePath).append(File.separator)
				.append(fileName);
		try {
			input = new FileInputStream(file.toString());
			BpmConfigFile = new File(file.toString());
			lastModified = BpmConfigFile.lastModified();
			// load a properties from a file
			properties = new Properties();
			properties.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String getValue(String key) {
		if (BpmConfigFile != null) {
			if (BpmConfigFile.lastModified() > lastModified) {
				loadBPMProperties();
				return properties.getProperty(key);
			}
		}
		return properties.getProperty(key);
	}
}
