package com.unitedofoq.fabs.bpm.model;

import com.unitedofoq.fabs.bpm.model.status.TaskMode;
import com.unitedofoq.fabs.bpm.model.status.TaskStatus;

import java.util.*;

/**
 *
 */
public class Task {

	private long id;
	private long processInstanceId;
	private String processId;
	private String name;
	private String description;
	private String subject;
	private TaskStatus status;
	private String actualOwnerId;
	private String creatorId;
	private String creationTime;
	private Date activationTime;
	private Date expirationTime;
	private Integer periority;
	private TaskFunction taskFunction;
	private boolean delegated;
	private boolean requestStatus;
	private TaskMode taskMode;
	private Map<String, Object> inputs;

	public Task() {
	}

	public Task(long id, long processInstanceId, String processId, String name,
			String description, String subject, TaskStatus status,
			String initiatorId, String ownerId, String creatorId,
			String creationTime, Date activationTime, Date expirationTime,
			Integer periority) {
		this.id = id;
		this.processInstanceId = processInstanceId;
		this.processId = processId;
		this.name = name;
		this.description = description;
		this.subject = subject;
		this.status = status;
		this.actualOwnerId = ownerId;
		this.creatorId = creatorId;
		this.creationTime = creationTime;
		this.activationTime = activationTime;
		this.expirationTime = expirationTime;
		this.periority = periority;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(long processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameDD() {
		return "Task_Name";
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public TaskStatus getStatus() {
		return status;
	}

	public void setStatus(TaskStatus status) {
		this.status = status;
	}

	public String getActualOwnerId() {
		return actualOwnerId;
	}

	public void setActualOwnerId(String actualOwnerId) {
		this.actualOwnerId = actualOwnerId;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	public Date getActivationTime() {
		return activationTime;
	}

	public void setActivationTime(Date activationTime) {
		this.activationTime = activationTime;
	}

	public Date getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(Date expirationTime) {
		this.expirationTime = expirationTime;
	}

	public Integer getPeriority() {
		return periority;
	}

	public void setPeriority(Integer periority) {
		this.periority = periority;
	}

	public TaskFunction getTaskFunction() {
		return taskFunction;
	}

	public void setTaskFunction(TaskFunction taskFunction) {
		this.taskFunction = taskFunction;
	}

	public boolean isDelegated() {
		return delegated;
	}

	public void setDelegated(boolean delegated) {
		this.delegated = delegated;
	}

	public boolean isRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(boolean requestStatus) {
		this.requestStatus = requestStatus;
	}

	public TaskMode getTaskMode() {
		return taskMode;
	}

	public void setTaskMode(TaskMode taskMode) {
		this.taskMode = taskMode;
	}

	public Map<String, Object> getInputs() {
		return inputs;
	}

	public void setInputs(Map<String, Object> inputs) {
		this.inputs = inputs;
	}

}
