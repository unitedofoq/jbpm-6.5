package com.unitedofoq.fabs.bpm.connector;

import com.unitedofoq.fabs.bpm.services.AuditService;
import com.unitedofoq.fabs.bpm.services.DefinitionService;
import com.unitedofoq.fabs.bpm.services.ProcessService;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.unitedofoq.fabs.bpm.services.TaskService;
import com.unitedofoq.fabs.bpm.util.BPMPropertiesUtil;

@Stateless
public class BPMServiceFactory {

    @Inject
    @Any
    Instance<TaskService> taskServices;

    @Inject
    @Any
    Instance<DefinitionService> definitionServices;

    @Inject
    @Any
    Instance<ProcessService> processServices;

    @Inject
    @Any
    Instance<AuditService> auditServices;
    
    @Inject
    BPMPropertiesUtil bpmPropertiesUtil;

    private String implementationType=null;
 
	@PostConstruct
	public void initialize() {
        implementationType = bpmPropertiesUtil.getValue("implementation");
	}

    public TaskService getTaskServiceImpl() {
        for (TaskService service : taskServices) {
            if (service.getType().equals(implementationType)) {
                return service;
            }
        }
        return null;
    }

    public DefinitionService getDefinitionServiceImpl() {
        for (DefinitionService service : definitionServices) {
            if (service.getType().equals(implementationType)) {
                return service;
            }
        }
        return null;
    }

    public ProcessService getProcessServiceImpl() {
        for (ProcessService service : processServices) {
            if (service.getType().equals(implementationType)) {
                return service;
            }
        }
        return null;
    }

    public AuditService getAuditServiceImpl() {
        for (AuditService service : auditServices) {
            if (service.getType().equals(implementationType)) {
                return service;
            }
        }
        return null;
    }
}
