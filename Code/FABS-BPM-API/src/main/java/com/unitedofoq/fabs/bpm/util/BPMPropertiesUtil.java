package com.unitedofoq.fabs.bpm.util;

import javax.ejb.Local;

@Local
public interface BPMPropertiesUtil {
	 public String getValue(String key);

}
