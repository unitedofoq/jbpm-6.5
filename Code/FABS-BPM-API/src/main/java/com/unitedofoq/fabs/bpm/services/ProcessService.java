package com.unitedofoq.fabs.bpm.services;

import java.util.*;
import com.unitedofoq.fabs.bpm.model.ProcessInstance;
import com.unitedofoq.fabs.bpm.common.model.ProcessRequestData;
import javax.ejb.Local;

/**
 *
 */
@Local
public interface ProcessService extends BPMService {

    /**
     * @param processId
     * @param parameters
     * @return
     */
    public ProcessInstance createProcessInstance(String processId, Map<String, Object> parameters);

    /**
     * @param processInstanceId
     * @return
     */
    public ProcessInstance startProcessInstance(long processInstanceId);

    /**
     * @param processInstanceId
     */
    public void abortProcessInstance(long processInstanceId);

    /**
     * @param processId
     * @param parameters
     */
    public ProcessInstance startProcess(String processId, Map<String, Object> parameters);

    /**
     * @param processInstanceId
     * @return
     */
    public ProcessInstance retrieveActiveProcessInstance(long processInstanceId);

    public Collection<ProcessInstance> retrieveActiveProcessInstances();
    
    public Map<Long, List<ProcessInstance>> retrieveAllProcessInstances(ProcessRequestData data);
    
    public String retrieveProcessInitiator(long insId);

}
