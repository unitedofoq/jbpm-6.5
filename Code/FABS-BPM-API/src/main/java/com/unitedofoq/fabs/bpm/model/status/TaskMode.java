package com.unitedofoq.fabs.bpm.model.status;

public enum TaskMode {
	APPROVE_REJECT("approve/reject"), DISMISS_DONE("dismiss"), DONE("done");

	private String value;

	private TaskMode(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

}
