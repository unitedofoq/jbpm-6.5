/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.bpm.services;

import com.unitedofoq.fabs.bpm.model.Task;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

/**
 *
 * @author bgalal
 */
@Local
public interface DefinitionService extends BPMService {
    Map<String, String> retrieveProcessVariables(String processDefId);
    List<String> retrieveTaskOutputVariables(Task task);
}
