package com.unitedofoq.fabs.bpm.model.status;

/**
 *
 */
public enum TaskStatus {
	CREATED, COMPLETED, INPROGRESS, SUSPENDED, ERROR, FAILED, EXITED, OBSOLETE, READY, RESERVED
}
