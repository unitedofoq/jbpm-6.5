package com.unitedofoq.fabs.bpm.model.status;

/**
 *
 */
public enum ProcessState {
	PENDING, ACTIVE, COMPLETED, ABORTED, SUSPENDED
	
}
