package com.unitedofoq.fabs.bpm.services;

import javax.ejb.Local;

/**
 * 
 */
@Local
public interface AuditService extends BPMService {
    public long getTaskId(long processInstanceId, long nodeInstanceId);
}