package com.unitedofoq.fabs.bpm.model;

import java.util.Map;

public class Process {

	private String id;
	private Map<String, Object> metadata;

	/**
	 * Default constructor
	 */
	public Process() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<String, Object> getMetadata() {
		return metadata;
	}

	public void setMetadata(Map<String, Object> metadata) {
		this.metadata = metadata;
	}

}
