package com.unitedofoq.fabs.bpm.services;

import com.unitedofoq.fabs.bpm.common.model.TasksRequestData;
import com.unitedofoq.fabs.bpm.model.Task;
import com.unitedofoq.fabs.bpm.model.status.TaskStatus;
import java.util.*;
import javax.ejb.Local;

/**
 *
 */
@Local
public interface TaskService extends BPMService {

    /**
     * @param userId
     * @return
     */
    public Map<Long, List<Task>> getTasksAssignedAsPotentialOwner(TasksRequestData data);
    
    public List<Task> getTasksAssignedAsPotentialOwner(String userId);

    /**
     * @param userId
     * @param processId
     * @return
     */
    public List<Task> getTasksAssignedAsPotentialOwnerByProcessId(String userId, String processId);
    
    /**
     * @param userId
     * @param processIdList
     * @return
     */
//    public List<Task> getTasksAssignedAsPotentialOwnerByProcessIdList(
//            String userId, List<String> processIdList);
    
    /**
     * @param userId
     * @return
     */
    public List<Task> getTasksOwned(String userId);

    /**
     * @param taskId
     * @param userId
     */
    public void claim(long taskId, String userId, String password);

    /**
     * @param taskId
     * @param userId
     * @param data
     */
    public void complete(long taskId, String userId, String password, Map<String, Object> data);

    /**
     * @param taskId
     * @param userId
     * @param delegateUserId
     */
    public void delegate(long taskId,String userId, String delegateUserId);

    public void release(long taskId, String userId);

    public List<Task> getTasksOwnedByStatus(String userId, List<TaskStatus> statuses);

    public TaskStatus getTaskStatusById(long taskId);

    public void start(long taskId, String userId, String password);

    public Task getTaskById(long taskId);
    
    public Map<String, Object> getTaskInputs(long taskId);

    public List<Task> getProcessInstanceTasks(String processInstanceId);
    
    public List<String> getTaskPotentialOwnersOfTypeRole(long taskId);
    
}
