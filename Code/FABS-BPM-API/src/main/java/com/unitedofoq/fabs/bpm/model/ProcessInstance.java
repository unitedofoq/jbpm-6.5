package com.unitedofoq.fabs.bpm.model;

import com.unitedofoq.fabs.bpm.model.status.ProcessState;
import java.util.Date;

public class ProcessInstance {
    //processInstanceId
    private long id;

    private String startDate;
    //who take current actions
    private String currentExecuters;
    private ProcessState state;
    private String name;
    private String processId;
    private String initiator;

    /**
     * Default constructor
     */
    public ProcessInstance() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getCurrentExecuters() {
        return currentExecuters;
    }

    public void setCurrentExecuters(String currentExecuters) {
        this.currentExecuters = currentExecuters;
    }

    public ProcessState getState() {
        return state;
    }

    public void setState(ProcessState state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getInitiator() {
        return initiator;
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

}
