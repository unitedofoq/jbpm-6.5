/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.uploader.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Hammad
 */
public class UploadedFileService {

    private final Logger logger = LoggerFactory.getLogger(UploadedFileService.class);
    private String storageRoot = System.getProperty("java.io.tmpdir") + File.separator + "FABS_Uploads";
    private static UploadedFileService _instance = null;
    
    
    private UploadedFileService() {
        // Singleton class
    }
    
    public static UploadedFileService getInstance(String storageRoot) {
        if (_instance == null) {
            _instance = new UploadedFileService();
            if (storageRoot != null) {
                _instance.storageRoot = storageRoot;
            }
        }
        return _instance;       
    }
    
    public void saveUploadedFile(UploadedFile uploadedFile) {
        String filePath = storageRoot + File.separator + uploadedFile.getId();
        new File(filePath).mkdirs();
        String fileName = filePath + File.separator + uploadedFile.getFileName();

        logger.debug("Storing new uploaded file at {}", fileName);

        OutputStream out = null;
        InputStream fileContentStream = new ByteArrayInputStream(uploadedFile.getContent());

        try {
            out = new FileOutputStream(new File(fileName));
            int read;
            final byte[] bytes = new byte[1024];
            while ((read = fileContentStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            logger.info("File uploaded successfully to {}", fileName);
        } catch (IOException iOException) {
            logger.error("Exception thrown", iOException);
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException iOException) {
                    logger.error("Exception thrown", iOException);
                }
            }
        }
    }

    public UploadedFile getUploadedFileById(String imageId) {
        try {
            String filePath = storageRoot + File.separator + imageId;
            File folder = new File(filePath);
            File file = folder.listFiles()[0];
            byte[] content = Files.readAllBytes(Paths.get(file.getAbsolutePath()));

            UploadedFile uploadedFile = new UploadedFile(imageId);
            uploadedFile.setContent(content);
            uploadedFile.setFileName(file.getName());

            return uploadedFile;
        } catch (IOException iOException) {
            logger.error("Exception thrown", iOException);
            return null;
        }
    }
}
