/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.uploader.servlet;

import com.unitedofoq.fabs.core.uiframework.uploader.service.UploadedFile;
import com.unitedofoq.fabs.core.uiframework.uploader.service.UploadedFileService;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Hammad
 */
@WebServlet(name = "FileViewServlet", urlPatterns = {"/FileViewServlet"})
public class FileViewServlet extends HttpServlet {

    private static final String ERROR_FILE_DOWNLOAD = "An error occured when trying to get the image with id:";
    private static final String IMAGE_PARAMETER_NAME = "imageId";
    private static final long CACHE_AGE_MILISECONDS_TWO_WEEKS = 1209600000;
    private static final String CKEDITOR_CONTENT_EXPIRE = "Expires";
    private static final String CKEDITOR_CONTENT_TYPE = "Content-Type";
    private static final String CKEDITOR_CONTENT_LENGTH = "Content-Length";
    private static final String CKEDITOR_CONTENT_DISPOSITION = "Content-Disposition";
    private static final String CKEDITOR_CONTENT_DISPOSITION_VALUE = "inline; filename=\"";
    private static final String CKEDITOR_HEADER_NAME = "Cache-Control";

    final static Logger logger = LoggerFactory.getLogger(FileViewServlet.class);

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String imageId = request.getParameter(IMAGE_PARAMETER_NAME);

        try {
            String storageRoot = getServletContext().getRealPath("/") + File.separator + getServletContext().getInitParameter("storageRoot");
            UploadedFile uploadedFile = UploadedFileService.getInstance(storageRoot)
                    .getUploadedFileById(imageId);
            if (uploadedFile != null && uploadedFile.getContent().length > 0) {
                byte[] rb = uploadedFile.getContent();
                long expiry = new Date().getTime() + CACHE_AGE_MILISECONDS_TWO_WEEKS;
                response.setDateHeader(CKEDITOR_CONTENT_EXPIRE, expiry);
                response.setHeader(CKEDITOR_HEADER_NAME, "max-age=" + CACHE_AGE_MILISECONDS_TWO_WEEKS);
                response.setHeader(CKEDITOR_CONTENT_TYPE, uploadedFile.getContentType());
                response.setHeader(CKEDITOR_CONTENT_LENGTH, String.valueOf(rb.length));
                response.setHeader(CKEDITOR_CONTENT_DISPOSITION,
                        CKEDITOR_CONTENT_DISPOSITION_VALUE + uploadedFile.getFileName() + "\"");
                response.getOutputStream().write(rb, 0, rb.length);
                response.getOutputStream().flush();
                response.getOutputStream().close();
            }
        } catch (Exception e) {
            response.getOutputStream().close();
            logger.error(ERROR_FILE_DOWNLOAD + imageId, e);
        }
    }
}
