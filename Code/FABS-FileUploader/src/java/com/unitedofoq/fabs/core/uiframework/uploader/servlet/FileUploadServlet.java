/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.core.uiframework.uploader.servlet;

import com.unitedofoq.fabs.core.uiframework.uploader.service.UploadedFileService;
import com.unitedofoq.fabs.core.uiframework.uploader.service.UploadedFile;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Hammad
 */
@WebServlet(name = "FileUploadServlet", urlPatterns = {"/FileUploadServlet"})
public class FileUploadServlet extends HttpServlet {

    private static final String ERROR_FILE_UPLOAD = "An error occurred to the file upload process.";
    private static final String ERROR_NO_FILE_UPLOAD = "No file is present for upload process.";
    private static final String ERROR_INVALID_CALLBACK = "Invalid callback.";
    private static final String CKEDITOR_CONTENT_TYPE = "text/html; charset=UTF-8";
    private static final String CKEDITOR_HEADER_NAME = "Cache-Control";
    private static final String CKEDITOR_HEADER_VALUE = "no-cache";
    private static final String FILE_VIEW_SERVLET_URL = "/FileViewServlet";

    private static final Pattern PATTERN = Pattern.compile("[\\w\\d]*");

    private String errorMessage = "";

    final static Logger logger = LoggerFactory.getLogger(FileUploadServlet.class);

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        UploadedFile uploadedFile = new UploadedFile();
        PrintWriter out = response.getWriter();

        response.setContentType(CKEDITOR_CONTENT_TYPE);
        response.setHeader(CKEDITOR_HEADER_NAME, CKEDITOR_HEADER_VALUE);

        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);

        try {
            List<FileItem> items = upload.parseRequest(request);
            if (!items.isEmpty() && items.get(0) != null) {
                uploadedFile.setContent(items.get(0).get());
                uploadedFile.setContentType(items.get(0).getContentType());
                uploadedFile.setFileName(((DiskFileItem) items.get(0)).getName());
                String storageRoot = getServletContext().getRealPath("/") + File.separator + getServletContext().getInitParameter("storageRoot");
                UploadedFileService.getInstance(storageRoot)
                        .saveUploadedFile(uploadedFile);
            } else {
                errorMessage = ERROR_NO_FILE_UPLOAD;
            }

        } catch (FileUploadException e) {
            errorMessage = ERROR_FILE_UPLOAD;
            logger.error(errorMessage, e);
        }

        // CKEditorFuncNum Is the location to display when the callback
        String callback = request.getParameter("CKEditorFuncNum");
        // verify if the callback contains only digits and letters in order to
        // avoid vulnerability on parsing parameter
        if (!PATTERN.matcher(callback).matches()) {
            callback = "";
            errorMessage = ERROR_INVALID_CALLBACK;
        }
        String pathToFile = request.getContextPath() + FILE_VIEW_SERVLET_URL + "?imageId=" + uploadedFile.getId();
        out.println("<script   type =\"text/javascript\">window.parent.CKEDITOR.tools.callFunction("
                + callback + ",'" + pathToFile + "','" + errorMessage + "')"
        );
        out.println("</script>");
        out.flush();
        out.close();
    }
}
