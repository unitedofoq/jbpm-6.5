package ru.emdev.security.auth.hook.upgrade;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import ru.emdev.security.auth.hook.upgrade.v6_1_1_1.UpgradeUser;

public class UpgradeProcess_6_1_1_1
  extends UpgradeProcess
{
  public int getThreshold()
  {
    return 6111;
  }
  
  protected void doUpgrade()
    throws Exception
  {
    upgrade(UpgradeUser.class);
  }
}
