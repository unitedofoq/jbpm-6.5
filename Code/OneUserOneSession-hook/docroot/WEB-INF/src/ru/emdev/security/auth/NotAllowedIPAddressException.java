package ru.emdev.security.auth;

import com.liferay.portal.security.auth.AuthException;

public class NotAllowedIPAddressException
  extends AuthException
{
  /**
	 * 
	 */
	private static final long serialVersionUID = 8675771130080815335L;

public NotAllowedIPAddressException() {}
  
  public NotAllowedIPAddressException(String msg)
  {
    super(msg);
  }
  
  public NotAllowedIPAddressException(String msg, Throwable cause)
  {
    super(msg, cause);
  }
  
  public NotAllowedIPAddressException(Throwable cause)
  {
    super(cause);
  }
}
