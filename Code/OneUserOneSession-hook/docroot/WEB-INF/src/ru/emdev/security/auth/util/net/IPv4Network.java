package ru.emdev.security.auth.util.net;

public class IPv4Network
  extends IPv4AddressRange
{
  private byte maskCtr;
  private int maskInt;
  
  IPv4Network(IPv4Address address, byte mask)
  {
    super(address, null);
    this.from = address;
    this.maskCtr = mask;
    
    this.maskInt = ((1 << 32 - this.maskCtr) - 1 ^ 0xFFFFFFFF);
  }
  
  public static IPv4Network fromString(String mask)
  {
    int pos = mask.indexOf('/');
    byte maskCtr;
    String addr;
    byte lmaskCtr1;
    if (pos == -1)
    {
      addr = mask;
      lmaskCtr1 = 32;
    }
    else
    {
      addr = mask.substring(0, pos);
      lmaskCtr1 = Byte.parseByte(mask.substring(pos + 1));
    }
    IPv4Address address = IPv4Address.fromString(addr);
    if (address != null) {
      return new IPv4Network(address, lmaskCtr1);
    }
    return null;
  }
  
  public boolean contains(IPv4Address test)
  {
    return (this.from.getAddrInt() & this.maskInt) == (test.getAddrInt() & this.maskInt);
  }
  
  public boolean contains(String addr)
  {
    IPv4Address address = IPv4Address.fromString(addr);
    return (address != null) && (contains(address));
  }
}
