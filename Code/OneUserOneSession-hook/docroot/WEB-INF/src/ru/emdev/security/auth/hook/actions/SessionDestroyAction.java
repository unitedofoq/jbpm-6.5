package ru.emdev.security.auth.hook.actions;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.SessionAction;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.User;
import javax.servlet.http.HttpSession;
import ru.emdev.security.auth.util.SessionCountUtil;

public class SessionDestroyAction
  extends SessionAction
{
  private static final Log _log = LogFactoryUtil.getLog(SessionDestroyAction.class);
  
  public void run(HttpSession session)
    throws ActionException
  {
    _log.debug("Start custom session destroy action");
    
    User user = (User)session.getAttribute("USER");
    long userId = 0L;
    if (user == null)
    {
      String jRemoteUser = null;
      if (GetterUtil.getBoolean(PropsUtil.get("portal.jaas.enable"))) {
        jRemoteUser = (String)session.getAttribute("j_remoteuser");
      }
      if (Validator.isNotNull(jRemoteUser)) {
        userId = GetterUtil.getLong(jRemoteUser);
      } else {
        userId = ((Long)session.getAttribute("USER_ID")).longValue();
      }
    }
    else
    {
      userId = user.getUserId();
    }
    if (userId > 0L) {
      SessionCountUtil.remove(userId, session.getId());
    }
    _log.debug("End custom session destroy action");
  }
}
