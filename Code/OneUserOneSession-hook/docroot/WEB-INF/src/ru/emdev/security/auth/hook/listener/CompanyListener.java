package ru.emdev.security.auth.hook.listener;

import com.liferay.portal.ModelListenerException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.BaseModelListener;
import com.liferay.portal.model.Company;
import com.liferay.portal.util.PortalUtil;
import org.apache.commons.lang.ArrayUtils;
import ru.emdev.security.auth.util.ExpandoUtil;

public class CompanyListener
  extends BaseModelListener<Company>
{
  public static Log _log = LogFactoryUtil.getLog(CompanyListener.class);
  
  public void onAfterUpdate(Company company)
    throws ModelListenerException
  {
    long companyId = company.getCompanyId();
    if (!ArrayUtils.contains(PortalUtil.getCompanyIds(), companyId)) {
      try
      {
        if (_log.isInfoEnabled()) {
          _log.info("Creating user attributes for extended authenticator in company[" + companyId + "]");
        }
        ExpandoUtil.addUserExpandoColumn(companyId, "allowed-ip-addresses", 16);
        

        ExpandoUtil.addUserExpandoColumn(companyId, "access-by-date-enabled", 1);
        

        ExpandoUtil.addUserExpandoColumn(companyId, "access-date-from", 3);
        

        ExpandoUtil.addUserExpandoColumn(companyId, "access-date-to", 3);
        

        ExpandoUtil.addUserExpandoColumn(companyId, "simultaneous-session-count", 9);
      }
      catch (Exception e)
      {
        _log.error("Failed to create user attributes for company[" + companyId + "]", e);
      }
    }
  }
}
