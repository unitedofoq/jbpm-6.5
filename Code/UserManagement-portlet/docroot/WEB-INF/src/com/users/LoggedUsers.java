package com.users;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import ru.emdev.security.auth.util.SessionCountUtil;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class LoggedUsers
 */
public class LoggedUsers extends MVCPortlet {

	
	public void removeUserSession(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		long loggedUserId= ParamUtil.getLong(actionRequest, "loggedUserId");
		SessionCountUtil.remove(loggedUserId);
	}
 

}
