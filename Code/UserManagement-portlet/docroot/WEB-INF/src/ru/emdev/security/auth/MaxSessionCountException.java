package ru.emdev.security.auth;

import com.liferay.portal.security.auth.AuthException;

public class MaxSessionCountException
  extends AuthException
{
  /**
	 * 
	 */
	private static final long serialVersionUID = -4094770318554376878L;

public MaxSessionCountException() {}
  
  public MaxSessionCountException(String msg)
  {
    super(msg);
  }
  
  public MaxSessionCountException(String msg, Throwable cause)
  {
    super(msg, cause);
  }
  
  public MaxSessionCountException(Throwable cause)
  {
    super(cause);
  }
}
