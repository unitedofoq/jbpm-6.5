package ru.emdev.security.auth;

import com.liferay.portal.security.auth.AuthException;

public class NonWorkingTimeException
  extends AuthException
{
  /**
	 * 
	 */
	private static final long serialVersionUID = 504421086196998301L;

public NonWorkingTimeException() {}
  
  public NonWorkingTimeException(String msg)
  {
    super(msg);
  }
  
  public NonWorkingTimeException(String msg, Throwable cause)
  {
    super(msg, cause);
  }
  
  public NonWorkingTimeException(Throwable cause)
  {
    super(cause);
  }
}
