package ru.emdev.security.auth.util.net;

public class IPv4AddressRange
{
  protected IPv4Address from;
  protected IPv4Address to;
  
  IPv4AddressRange(IPv4Address from, IPv4Address to)
  {
    this.from = from;
    this.to = to;
  }
  
  public static IPv4AddressRange fromFirstAndLast(String from, String to)
  {
    if ((IPUtil.isIPv4Range(from)) || (IPUtil.isIPv4Range(to))) {
      throw new IllegalArgumentException("IPv4 address range cannot be specified with CIDR notation");
    }
    IPv4Address fromAddr = IPv4Address.fromString(from);
    IPv4Address toAddr = IPv4Address.fromString(to);
    return fromFirstAndLast(fromAddr, toAddr);
  }
  
  public static IPv4AddressRange fromFirstAndLast(IPv4Address from, IPv4Address to)
  {
    if ((from == null) || (to == null)) {
      throw new IllegalArgumentException("All addresses should be not null");
    }
    return new IPv4AddressRange(from, to);
  }
  
  public boolean contains(IPv4Address test)
  {
    int from = this.from.getAddrInt();
    int to = this.to.getAddrInt();
    return (test != null) && (test.getAddrInt() >= from) && (test.getAddrInt() <= to);
  }
  
  public boolean contains(String addr)
  {
    IPv4Address address = IPv4Address.fromString(addr);
    return (address != null) && (contains(address));
  }
}
