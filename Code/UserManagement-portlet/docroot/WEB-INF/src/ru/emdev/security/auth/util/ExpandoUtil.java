package ru.emdev.security.auth.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.User;
import com.liferay.portlet.expando.model.ExpandoColumn;
import com.liferay.portlet.expando.model.ExpandoTable;
import com.liferay.portlet.expando.service.ExpandoColumnLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoTableLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class ExpandoUtil
{
  private static final Log _log = LogFactoryUtil.getLog(ExpandoUtil.class);
  
  public static long getExpandoTableId(long companyId, String className)
    throws PortalException, SystemException
  {
    ExpandoTable exandoTable = null;
    long tableId = 0L;
    try
    {
      exandoTable = ExpandoTableLocalServiceUtil.getTable(companyId, className, "CUSTOM_FIELDS");
    }
    catch (Exception ex) {}
    if (exandoTable == null) {
      exandoTable = ExpandoTableLocalServiceUtil.addTable(companyId, className, "CUSTOM_FIELDS");
    }
    tableId = exandoTable.getTableId();
    return tableId;
  }
  
  public static ExpandoColumn addUserExpandoColumn(long companyId, String name, int type)
    throws PortalException, SystemException
  {
    long tableId = getExpandoTableId(companyId, User.class.getName());
    
    ExpandoColumn column = ExpandoColumnLocalServiceUtil.getColumn(tableId, name);
    if (column == null) {
      column = ExpandoColumnLocalServiceUtil.addColumn(tableId, name, type);
    }
    return column;
  }
  
  public static boolean isAccessByDateEnabled(long companyId, long userId)
  {
    boolean result = false;
    try
    {
      result = ExpandoValueLocalServiceUtil.getData(companyId, User.class.getName(), "CUSTOM_FIELDS", "access-by-date-enabled", userId, false);
    }
    catch (Exception e)
    {
      _log.error("Can't access to user[" + userId + "] attributes", e);
    }
    return result;
  }
  
  public static Date getDateFrom(long companyId, long userId)
  {
    Date result = null;
    try
    {
      result = ExpandoValueLocalServiceUtil.getData(companyId, User.class.getName(), "CUSTOM_FIELDS", "access-date-from", userId, result);
    }
    catch (Exception e)
    {
      _log.error("Can't access to user[" + userId + "] attributes", e);
    }
    return result;
  }
  
  public static Date getDateTo(long companyId, long userId)
  {
    Date result = null;
    try
    {
      result = ExpandoValueLocalServiceUtil.getData(companyId, User.class.getName(), "CUSTOM_FIELDS", "access-date-to", userId, result);
    }
    catch (Exception e)
    {
      _log.error("Can't access to user[" + userId + "] attributes", e);
    }
    return result;
  }
  
  public static int getSessionCount(long companyId, long userId)
  {
    int result = 0;
    try
    {
      result = ExpandoValueLocalServiceUtil.getData(companyId, User.class.getName(), "CUSTOM_FIELDS", "simultaneous-session-count", userId, result);
    }
    catch (Exception e)
    {
      _log.error("Can't access to user[" + userId + "] attributes", e);
    }
    return result;
  }
  
  public static List<String[]> getAllowedUserIP(long companyId, long userId)
  {
    String[] dataArray = null;
    try
    {
      dataArray = ExpandoValueLocalServiceUtil.getData(companyId, User.class.getName(), "CUSTOM_FIELDS", "allowed-ip-addresses", userId, dataArray);
      


      dataArray = (dataArray != null) && (dataArray.length > 0) ? StringUtil.split(dataArray[0], '\n') : new String[0];
    }
    catch (Exception e)
    {
      _log.error("Can't access to user[" + userId + "] attributes", e);
    }
    List<String[]> result = new LinkedList();
    for (String ip : dataArray) {
      if (!StringUtils.isBlank(ip))
      {
        if (ip.contains("-"))
        {
          String[] range = ip.split("-");
          if ((StringUtils.isNotBlank(range[0])) && (StringUtils.isNotBlank(range[1])))
          {
            range[0] = range[0].trim();
            range[1] = range[1].trim();
            result.add(range);
          }
        }
        result.add(new String[] { ip.trim() });
      }
    }
    return result;
  }
}
