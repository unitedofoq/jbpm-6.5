package ru.emdev.security.auth.hook.upgrade.v6_1_1_1;

import java.util.List;

import ru.emdev.security.auth.hook.listener.CompanyListener;
import ru.emdev.security.auth.util.ExpandoUtil;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.model.Company;
import com.liferay.portal.service.CompanyLocalServiceUtil;

public class UpgradeUser
  extends UpgradeProcess
{
  protected void doUpgrade()
    throws Exception
  {
    List<Company> companies = CompanyLocalServiceUtil.getCompanies();
    for (Company company : companies)
    {
      long companyId = company.getCompanyId();
      try
      {
        if (CompanyListener._log.isInfoEnabled()) {
          CompanyListener._log.info("Creating user attributes for extended authenticator in company[" + companyId + "]");
        }
        ExpandoUtil.addUserExpandoColumn(companyId, "allowed-ip-addresses", 16);
        

        ExpandoUtil.addUserExpandoColumn(companyId, "access-by-date-enabled", 1);
        

        ExpandoUtil.addUserExpandoColumn(companyId, "access-date-from", 3);
        

        ExpandoUtil.addUserExpandoColumn(companyId, "access-date-to", 3);
        

        ExpandoUtil.addUserExpandoColumn(companyId, "simultaneous-session-count", 9);
      }
      catch (Exception e)
      {
        CompanyListener._log.error("Failed to create user attributes for company[" + companyId + "]", e);
      }
    }
  }
}
