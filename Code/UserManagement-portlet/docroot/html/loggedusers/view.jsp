<%@page import="ru.emdev.security.auth.util.SessionCountUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@page import="javax.portlet.ActionRequest"%>
<%@page import="javax.portlet.PortletURL"%>
<portlet:defineObjects />
<liferay-theme:defineObjects />
<theme:defineObjects />

<%
	List<User> loggedUsers = new ArrayList<User>();
	List<User> allUsers = UserLocalServiceUtil.getCompanyUsers(
			themeDisplay.getCompanyId(), 0, Integer.MAX_VALUE);
	for (int userIdx = 0; userIdx < allUsers.size(); userIdx++) {
		if (0 < SessionCountUtil.count(allUsers.get(userIdx)
				.getUserId())) {
			loggedUsers.add(allUsers.get(userIdx));
		}
	}
%>
<liferay-ui:search-container delta="4"
	emptyResultsMessage="Sorry. There are no Active Users Now">
	<liferay-ui:search-container-results
total="<%=loggedUsers.size() %>"
results="<%=ListUtil.subList(loggedUsers, searchContainer.getStart(),
searchContainer.getEnd()) %>"/>

	<liferay-ui:search-container-row
		className="com.liferay.portal.model.User"
		modelVar="user">
		<liferay-ui:search-container-column-text name="Screen Name"
			value="<%=user.getScreenName()%>" />
		<liferay-ui:search-container-column-text name="Email Adress"
			value="<%=user.getEmailAddress()%>" />
			<liferay-ui:search-container-column-jsp  name="Actions" 
			path="/html/loggedusers/actions.jsp"/>
	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator searchContainer="<%=searchContainer%>" />

</liferay-ui:search-container>