<%@page import="com.liferay.portal.model.User"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme"%>
<%@page import="java.util.Locale"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@page import="javax.portlet.ActionRequest"%>
<%@page import="javax.portlet.PortletURL"%>

<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<portlet:defineObjects />
<liferay-theme:defineObjects />
<theme:defineObjects />
<%
	ResultRow row = (ResultRow) 
					request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	User loggedUser = (User)row.getObject();
	
	PortletURL deleteCategoryURL = renderResponse.createActionURL();
	deleteCategoryURL.setParameter("loggedUserId", Long.toString(loggedUser.getUserId()));
	deleteCategoryURL.setParameter(ActionRequest.ACTION_NAME, "removeUserSession");
	

%>
<liferay-ui:icon-menu>
	<liferay-ui:icon image="delete" message = "Delete User" 
	url="<%=deleteCategoryURL.toString() %>"/>
</liferay-ui:icon-menu>