/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.fabs.wihandler;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author mahmed
 */
public class EntityUpdaterWorkItemHandler implements WorkItemHandler {
    
    private static final org.apache.logging.log4j.Logger LOG = LogManager.getLogger(EntityUpdaterWorkItemHandler.class);
    
    @Override
    public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
        LOG.debug("Entering");
        try {
            String dbid = String.valueOf(workItem.getParameter("dbid"));
            String entityName = String.valueOf(workItem.getParameter("entityName"));
            String fieldExpression = String.valueOf(workItem.getParameter("fieldExpression"));
            String value = String.valueOf(workItem.getParameter("value"));
            value=URLEncoder.encode(value, "UTF-8");//encode value to escape whitespaces
            String loginName = String.valueOf(workItem.getParameter("loginName"));
            String url = "http://localhost:8080/wsdl-locator/wscall?serviceName=setValueInEntity&entityName=" + entityName + "&dbid=" + dbid + "&value" + "=" + value + "&fieldExpression=" + fieldExpression + "&loginName=" + loginName;
            URL urll = new URL(url);
            URLConnection urlConnection = urll.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            
            String output = "";
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                if ((inputLine.contains("Error")) || (inputLine.contains("Exception"))) {
                    LOG.error("Error or exception while reading from input stream");
                    manager.completeWorkItem(workItem.getId(), null);
                    return;
                }
                output = output + inputLine;
            }
            Map<String, Object> map = new HashMap();
            map.put("Result", inputLine);
            manager.completeWorkItem(workItem.getId(), map);
        } catch (MalformedURLException ex) {
            LOG.error("Exception thrown: ", ex);
            manager.completeWorkItem(workItem.getId(), null);
        } catch (IOException ex) {
            LOG.error("Exception thrown: ", ex);
            manager.completeWorkItem(workItem.getId(), null);
        }
    }
    
    @Override
    public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
        LOG.debug("Aborting workitem with id:" + workItem.getId());
    }
    
}
